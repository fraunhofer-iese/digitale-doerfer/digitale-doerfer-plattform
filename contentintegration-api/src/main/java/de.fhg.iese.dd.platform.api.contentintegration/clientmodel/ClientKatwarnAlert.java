/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.clientmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ToString
public class ClientKatwarnAlert extends ClientBaseEvent {

    @NotBlank
    private String version;
    @NotBlank
    private String id;
    @NotBlank
    private String incident;
    @NotBlank
    @JsonProperty("provider_id")
    private String providerId;
    @JsonProperty("sender_id")
    private String senderId;
    private List<String> references;
    @NotBlank
    @JsonProperty("content_type")
    private String contentType;
    @NotBlank
    @JsonProperty("event_code")
    private String eventCode;
    @NotBlank
    private String severity;
    @NotNull
    @JsonProperty("msg_type")
    private String msgType;
    @Builder.Default
    private boolean notifiable = true;
    @Builder.Default
    private String scope = "public";

    //we want the geometry as geo json string, not as an object
    private String geometry;
    @NotNull
    private Long sent;
    @NotBlank
    private String status;
    private String note;
    @NotNull
    private Long effective;
    private Long expires;
    @NotBlank
    @JsonProperty("default_language")
    private String defaultLanguage;
    @NotNull
    private Map<String, ClientKatwarnLanguageBlock> languages;

        @Getter
        @Setter
        @SuperBuilder
        @NoArgsConstructor
        @ToString
        public static class ClientKatwarnLanguageBlock {

            @NotBlank
            private String issuer;
            @NotBlank
            private String subject;
            @NotBlank
            private String headline;
            @JsonProperty("event_type")
            private String eventType;
            private String description;
            private List<String> instruction;
            private String web;
            private String contact;
            private ClientKatwarnLocality locality;

            @Getter
            @Setter
            @SuperBuilder
            @NoArgsConstructor
            @ToString
            public static class ClientKatwarnLocality {

                private String prefix;
                private String suffix;
                @NotNull
                private List<String> names;
            }
        }


    @JsonSetter("geometry")
    private void setGeometryAsJsonNode(JsonNode node) {

        this.geometry = node.toString();
    }

}
