/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.clientmodel;

import de.fhg.iese.dd.platform.business.contentintegration.services.WebFeedItemScrapingResult;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.*;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ContentIntegrationClientModelMapper {

    public List<ClientCrawlingConfig> toClientCrawlingConfigs(List<CrawlingConfig> crawlingConfigs) {
        if (CollectionUtils.isEmpty(crawlingConfigs)) {
            return Collections.emptyList();
        }

        return crawlingConfigs.stream()
                .map(this::toClientCrawlingConfig)
                .collect(Collectors.toList());
    }

    public ClientCrawlingConfig toClientCrawlingConfig(CrawlingConfig crawlingConfig) {
        if (crawlingConfig == null) {
            return null;
        }
        
        return ClientCrawlingConfig.builder()
                .id(crawlingConfig.getId())
                .name(crawlingConfig.getName())
                .description(crawlingConfig.getDescription())
                .newsSourceId(crawlingConfig.getNewsSource().getId())
                .sourceUrl(crawlingConfig.getSourceUrl())
                .accessType(crawlingConfig.getAccessType())
                .externalPostType(crawlingConfig.getExternalPostType())
                .authenticationHeaderKey(crawlingConfig.getAuthenticationHeaderKey())
                .authenticationHeaderValue(crawlingConfig.getAuthenticationHeaderValue())
                .basicAuthUsername(crawlingConfig.getBasicAuthUsername())
                .basicAuthPassword(crawlingConfig.getBasicAuthPassword())
                .webFeedParametersMap(toClientFeedParameters(crawlingConfig.getWebFeedParameters()))
                .build();
    }

    public Map<String, List<ClientWebFeedParameter>> toClientFeedParameters(
            Map<String, List<WebFeedParameter>> feedParameters) {
        if (CollectionUtils.isEmpty(feedParameters)) {
            return Collections.emptyMap();
        }
        return feedParameters.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        feedParameter -> toClientWebFeedParameter(feedParameter.getValue())));

    }

    private List<ClientWebFeedParameter> toClientWebFeedParameter(List<WebFeedParameter> webFeedParameters) {
        if (CollectionUtils.isEmpty(webFeedParameters)) {
            return Collections.emptyList();
        }
        return webFeedParameters.stream()
                .map(webFeedParameter -> ClientWebFeedParameter.builder()
                        .tag(webFeedParameter.getTag())
                        .fixedValue(webFeedParameter.getFixedValue())
                        .webFeedValueSources(
                                toClientFeedValueSource(webFeedParameter.getWebFeedValueSources()))
                        .build())
                .collect(Collectors.toList());
    }

    public Map<String, List<WebFeedParameter>> toFeedParameters(
            Map<String, List<ClientWebFeedParameter>> clientFeedParameters) {
        if (CollectionUtils.isEmpty(clientFeedParameters)) {
            return Collections.emptyMap();
        }
        return clientFeedParameters.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        clientFeedParameter -> toWebFeedParameter(clientFeedParameter.getValue())));
    }

    private List<WebFeedParameter> toWebFeedParameter(List<ClientWebFeedParameter> clientWebFeedParameters) {
        if (CollectionUtils.isEmpty(clientWebFeedParameters)) {
            return Collections.emptyList();
        }
        return clientWebFeedParameters.stream()
                .map(clientWebFeedParameter -> WebFeedParameter.builder()
                        .tag(clientWebFeedParameter.getTag())
                        .fixedValue(clientWebFeedParameter.getFixedValue())
                        .webFeedValueSources(toWebFeedValueSource(clientWebFeedParameter.getWebFeedValueSources()))
                        .build())
                .collect(Collectors.toList());
    }

    private List<ClientWebFeedValueSource> toClientFeedValueSource(List<WebFeedValueSource> webFeedValueSources) {
        if (CollectionUtils.isEmpty(webFeedValueSources)) {
            return Collections.emptyList();
        }
        return webFeedValueSources.stream()
                .map(webFeedValueSource -> ClientWebFeedValueSource.builder()
                        .attribute(webFeedValueSource.getAttribute())
                        .type(webFeedValueSource.getType())
                        .child(webFeedValueSource.getChild())
                        .format(webFeedValueSource.getFormat())
                        .regexes(toClientFormattedRegexes(webFeedValueSource.getRegexes()))
                        .build())
                .collect(Collectors.toList());
    }

    private List<WebFeedValueSource> toWebFeedValueSource(List<ClientWebFeedValueSource> clientWebFeedValueSources) {
        if (CollectionUtils.isEmpty(clientWebFeedValueSources)) {
            return Collections.emptyList();
        }
        return clientWebFeedValueSources.stream()
                .map(clientWebFeedValueSource -> WebFeedValueSource.builder()
                        .attribute(clientWebFeedValueSource.getAttribute())
                        .type(clientWebFeedValueSource.getType())
                        .child(clientWebFeedValueSource.getChild())
                        .format(clientWebFeedValueSource.getFormat())
                        .regexes(toFormattedRegexes(clientWebFeedValueSource.getRegexes()))
                        .build())
                .collect(Collectors.toList());
    }

    public List<ClientWebFeedItem> toClientWebFeedItems(List<WebFeedItemScrapingResult> webFeedItemScrapingResults) {

        if (CollectionUtils.isEmpty(webFeedItemScrapingResults)) {
            return Collections.emptyList();
        }

        return webFeedItemScrapingResults.stream()
                .map(this::toClientWebFeedItem)
                .collect(Collectors.toList());
    }

    public ClientWebFeedItem toClientWebFeedItem(WebFeedItemScrapingResult webFeedItemScrapingResult) {

        if (webFeedItemScrapingResult == null) {
            return null;
        }

        return ClientWebFeedItem.builder()
                .attributeValueMap(webFeedItemScrapingResult.toDebugMap())
                .build();
    }

    public List<ClientFormattedRegex> toClientFormattedRegexes(List<FormattedRegex> formattedRegexes) {

        if (CollectionUtils.isEmpty(formattedRegexes)) {
            return Collections.emptyList();
        }

        return formattedRegexes.stream()
                .map(this::toClientFormattedRegex)
                .collect(Collectors.toList());
    }

    public ClientFormattedRegex toClientFormattedRegex(FormattedRegex formattedRegex) {

        if (formattedRegex == null) {
            return null;
        }

        return ClientFormattedRegex.builder()
                .regex(formattedRegex.getRegex())
                .messageFormat(formattedRegex.getMessageFormat())
                .build();
    }

    public List<FormattedRegex> toFormattedRegexes(List<ClientFormattedRegex> formattedRegexes) {

        if (CollectionUtils.isEmpty(formattedRegexes)) {
            return Collections.emptyList();
        }

        return formattedRegexes.stream()
                .map(this::toFormattedRegex)
                .collect(Collectors.toList());
    }

    public FormattedRegex toFormattedRegex(ClientFormattedRegex formattedRegex) {

        if (formattedRegex == null) {
            return null;
        }

        return FormattedRegex.builder()
                .regex(formattedRegex.getRegex())
                .messageFormat(formattedRegex.getMessageFormat())
                .build();
    }

    public KatwarnAlert toKatwarnAlert(ClientKatwarnAlert clientKatwarnAlert) {

        if (clientKatwarnAlert == null) {
            return null;
        }
        Map<String, ClientKatwarnAlert.ClientKatwarnLanguageBlock> clientLanguages = clientKatwarnAlert.getLanguages();
        Map<String, KatwarnAlert.KatwarnLanguageBlock> languages;
        if (CollectionUtils.isEmpty(clientLanguages)) {
            languages = Collections.emptyMap();
        } else {
            languages = clientLanguages.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> toKatwarnLanguageBlock(entry.getValue())));
        }
        KatwarnAlert katwarnAlert = KatwarnAlert.builder()
                .geometry(clientKatwarnAlert.getGeometry())
                .languages(languages)
                .contentType(toEnumValue(KatwarnAlert.KatwarnContentType.class, clientKatwarnAlert.getContentType()))
                .severity(toEnumValue(KatwarnAlert.KatwarnSeverity.class, clientKatwarnAlert.getSeverity()))
                .msgType(toEnumValue(KatwarnAlert.KatwarnMessageType.class, clientKatwarnAlert.getMsgType()))
                .status(toEnumValue(KatwarnAlert.KatwarnStatus.class, clientKatwarnAlert.getStatus()))
                .build();
        BeanUtils.copyProperties(clientKatwarnAlert, katwarnAlert);
        katwarnAlert.setSent(utcSecondsToUtcMilliseconds(clientKatwarnAlert.getSent()));
        katwarnAlert.setEffective(utcSecondsToUtcMilliseconds(clientKatwarnAlert.getEffective()));
        katwarnAlert.setExpires(utcSecondsToUtcMilliseconds(clientKatwarnAlert.getExpires()));
        return katwarnAlert;
    }

    private <T extends Enum<T>> T toEnumValue(Class<T> enumClass, String enumValue) {

        try {
            return Enum.valueOf(enumClass, StringUtils.upperCase(enumValue));
        } catch (IllegalArgumentException e) {
            throw new BadRequestException("No {} found for '{}'", enumClass.getSimpleName(), enumValue);
        }
    }

    private Long utcSecondsToUtcMilliseconds(Long utcSeconds) {

        if (utcSeconds == null) {
            return null;
        } else {
            return utcSeconds * 1000;
        }
    }

    public KatwarnAlert.KatwarnLanguageBlock toKatwarnLanguageBlock(ClientKatwarnAlert.ClientKatwarnLanguageBlock clientKatwarnLanguageBlock) {

        if (clientKatwarnLanguageBlock == null) {
            return null;
        }
        KatwarnAlert.KatwarnLanguageBlock katwarnLanguageBlock = KatwarnAlert.KatwarnLanguageBlock.builder()
                .locality(toKatwarnLocality(clientKatwarnLanguageBlock.getLocality()))
                .build();
        BeanUtils.copyProperties(clientKatwarnLanguageBlock, katwarnLanguageBlock);
        return katwarnLanguageBlock;
    }

    public KatwarnAlert.KatwarnLanguageBlock.KatwarnLocality toKatwarnLocality(ClientKatwarnAlert.ClientKatwarnLanguageBlock.ClientKatwarnLocality clientKatwarnLocality) {

        if (clientKatwarnLocality == null) {
            return null;
        }
        KatwarnAlert.KatwarnLanguageBlock.KatwarnLocality katwarnLocality = new KatwarnAlert.KatwarnLanguageBlock.KatwarnLocality();
        BeanUtils.copyProperties(clientKatwarnLocality, katwarnLocality);
        return katwarnLocality;
    }

}
