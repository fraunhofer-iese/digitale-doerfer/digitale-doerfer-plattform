/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Benjamin Hassenfratz, Mher Ter-Tovmasyan
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.clientmodel;

import java.util.List;
import java.util.Map;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingAccessType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientCrawlingConfig extends ClientBaseEntity {

    private String name;
    private String description;
    private String newsSourceId;
    private String sourceUrl;
    private CrawlingAccessType accessType;
    private PostType externalPostType;
    private String authenticationHeaderKey;
    private String authenticationHeaderValue;
    private String basicAuthUsername;
    private String basicAuthPassword;
    private Map<String, List<ClientWebFeedParameter>> webFeedParametersMap;

}
