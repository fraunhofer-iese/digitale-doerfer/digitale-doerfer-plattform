/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.clientmodel;

import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientExternalPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.IClientPostMappingCustomization;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnEventDetails;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

@Component
@Order(2)
@Log4j2
public class KatwarnPostCustomization implements IClientPostMappingCustomization<ClientExternalPost, ExternalPost> {

    @Autowired
    private IKatwarnService katwarnService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    @Override
    public Class<ClientExternalPost> getRelevantClientPostClass() {

        return ClientExternalPost.class;
    }

    @Override
    public ClientExternalPost customizeMapping(ClientExternalPost clientEntity, ExternalPost post) {

        Map<String, Object> customAttributes = post.getCustomAttributes();
        if (CollectionUtils.isEmpty(customAttributes)) {
            return clientEntity;
        }

        Object rawEventCode = customAttributes.get(IKatwarnService.KATWARN_ATTRIBUTE_EVENT_CODE);
        if (rawEventCode instanceof String eventCode) {
            KatwarnEventDetails eventDetails = katwarnService.findEventDetails(eventCode);
            if (eventDetails != null) {
                clientEntity.setSitePicture(fileClientModelMapper.toClientMediaItem(eventDetails.getIcon()));
            }
        }
        Object rawIssuer = customAttributes.get(IKatwarnService.KATWARN_ATTRIBUTE_ISSUER);
        if (rawIssuer instanceof String issuer) {
            clientEntity.setSiteName(issuer);
        }
        return clientEntity;
    }

}
