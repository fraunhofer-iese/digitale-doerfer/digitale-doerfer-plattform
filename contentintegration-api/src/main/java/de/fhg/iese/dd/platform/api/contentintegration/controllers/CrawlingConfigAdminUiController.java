/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ClientCrawlingConfig;
import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ContentIntegrationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.contentintegration.security.ListCrawlingConfigsAction;
import de.fhg.iese.dd.platform.business.contentintegration.services.ICrawlingConfigService;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui")
@Api(tags = {"contentintegration.adminui.crawlingconfig"})
public class CrawlingConfigAdminUiController extends BaseAdminUiController {

    @Autowired
    private ICrawlingConfigService crawlingConfigService;
    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private ContentIntegrationClientModelMapper contentIntegrationClientModelMapper;

    @ApiOperation(value = "Returns all crawling configs that the caller has access to")
    @ApiExceptions({
            ClientExceptionType.PAGE_PARAMETER_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListCrawlingConfigsAction.class}
    )
    @GetMapping("/crawlingconfig")
    public Page<ClientCrawlingConfig> getCrawlingConfigs(
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of crawling configs", defaultValue = "0")
                    int page,
            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of crawling configs returned in a page", defaultValue = "10")
                    int count
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(ListCrawlingConfigsAction.class, currentPerson);

        checkPageAndCountValues(page, count);

        return crawlingConfigService.findAllCrawlingConfigs(PageRequest.of(page, count, Sort.Direction.ASC, "id"))
                .map(contentIntegrationClientModelMapper::toClientCrawlingConfig);
    }

    @ApiOperation(value = "Returns a crawling config by id")
    @ApiExceptions({
            ClientExceptionType.CRAWLING_CONFIG_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListCrawlingConfigsAction.class}
    )
    @GetMapping(value = "/crawlingconfig/{crawlingConfigId}")
    public ClientCrawlingConfig getCrawlingConfigById(
            @PathVariable String crawlingConfigId
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(ListCrawlingConfigsAction.class, currentPerson);

        final CrawlingConfig crawlingConfig = crawlingConfigService.findCrawlingConfigById(crawlingConfigId);
        return contentIntegrationClientModelMapper.toClientCrawlingConfig(crawlingConfig);
    }

    @ApiOperation(value = "Returns all crawling configs to the given news source")
    @ApiExceptions({
            ClientExceptionType.NEWS_SOURCE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListCrawlingConfigsAction.class}
    )
    @GetMapping("/newssource/{newsSourceId}/crawlingconfig")
    public List<ClientCrawlingConfig> getCrawlingConfigsByNewsSource(
            @PathVariable String newsSourceId
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(ListCrawlingConfigsAction.class, currentPerson);

        final NewsSource newsSource = newsSourceService.findNewsSourceById(newsSourceId);
        final List<CrawlingConfig> crawlingConfigs =
                crawlingConfigService.findAllCrawlingConfigsByNewsSource(newsSource);
        return contentIntegrationClientModelMapper.toClientCrawlingConfigs(crawlingConfigs);
    }

}
