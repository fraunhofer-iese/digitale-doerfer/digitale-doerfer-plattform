/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ClientKatwarnAlert;
import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ContentIntegrationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.contentintegration.events.KatwarnAlertReceivedEvent;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.KatwarnConstants;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static de.fhg.iese.dd.platform.business.contentintegration.services.IKatwarnService.LOG_MARKER_KATWARN;

@RestController
@RequestMapping("/katwarn")
@Api(tags = {"contentintegration.katwarn"})
public class KatwarnAlertController extends BaseController {

    protected static final String HEADER_NAME_API_KEY_KATWARN = "Authorization";

    @Autowired
    private IAppService appService;
    @Autowired
    private Validator validator;
    @Autowired
    private ContentIntegrationClientModelMapper clientModelMapper;

    @ApiOperation(value = "Receives a KATWARN alert",
            notes = "The alert is triggered by the KATWARN system, which authenticates itself via api key.")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "Katwarn uses 'Authorization' as header name for the api key")
    @PostMapping(value = "alerts/{path}", consumes = {"application/vnd.kwrn.v2+json", MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public void onKatwarnAlertReceiveRequest(
            @RequestHeader(name = HEADER_NAME_API_KEY_KATWARN) String apiKey,
            @PathVariable String path,
            @RequestBody ClientKatwarnAlert clientKatwarnAlert) {

        AppVariant appVariant = appService.findAppVariantByApiKey(apiKey);
        if (!appVariant.getApp().getId().equals(KatwarnConstants.APP_ID)) {
            throw new NotAuthorizedException("Wrong app calling: {}", appVariant.getAppVariantIdentifier());
        }

        Set<ConstraintViolation<ClientKatwarnAlert>> violations = validator.validate(clientKatwarnAlert);
        if (!CollectionUtils.isEmpty(violations)) {
            log.warn(LOG_MARKER_KATWARN, "Validation for Katwarn alert {} failed, trying to process it anyway: {}",
                    clientKatwarnAlert.getId(), violations);
        }

        log.debug(LOG_MARKER_KATWARN, "{} received: {}", appVariant.getName(), clientKatwarnAlert);

        notify(KatwarnAlertReceivedEvent.builder()
                .alertId(clientKatwarnAlert.getId())
                .katwarnAppVariant(appVariant)
                .katwarnAlert(clientModelMapper.toKatwarnAlert(clientKatwarnAlert))
                .build());
    }

}