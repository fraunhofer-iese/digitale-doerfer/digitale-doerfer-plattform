/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ClientTestWebFeedScraperRequest;
import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ClientTestWebFeedScraperResponse;
import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.ContentIntegrationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.contentintegration.security.TestWebFeedScraperAction;
import de.fhg.iese.dd.platform.business.contentintegration.services.IWebFeedScraperService;
import de.fhg.iese.dd.platform.business.contentintegration.services.WebFeedItemScrapingResult;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/adminui")
@Api(tags = {"contentintegration.adminui.crawling"})
public class CrawlingAdminUiController extends BaseAdminUiController {

    @Autowired
    private IWebFeedScraperService webFeedScraperService;

    @Autowired
    private ContentIntegrationClientModelMapper contentIntegrationClientModelMapper;

    @ApiOperation(value = "Tests scraping the WebFeedItems for the given url, access type and configuration",
            notes = "The response contains the scraped items together with a log of the scraping. " +
                    "If no items were found for the given request, the list of items will be empty.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {TestWebFeedScraperAction.class}
    )
    @PostMapping("/testWebFeedScraperRequest")
    public ClientTestWebFeedScraperResponse crawlWebFeedItems(
            @Valid @RequestBody
            @ApiParam(required = true)
            ClientTestWebFeedScraperRequest request
    ) {
        final Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(TestWebFeedScraperAction.class, currentPerson);

        final LogSummary logSummary = new LogSummary();
        final List<WebFeedItemScrapingResult> scrapingResults =
                webFeedScraperService.scrapeWebFeed(request.getWebFeedSourceUrl(), request.getAccessType(),
                        contentIntegrationClientModelMapper.toFeedParameters(request.getWebFeedParameters()),
                        logSummary);

        return ClientTestWebFeedScraperResponse.builder()
                .log(logSummary.toString())
                .webFeedItems(contentIntegrationClientModelMapper.toClientWebFeedItems(scrapingResults))
                .build();
    }

}
