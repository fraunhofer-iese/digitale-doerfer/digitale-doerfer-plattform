/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shopping.repos;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

@Transactional(readOnly = true)
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, String> {

    PurchaseOrder findOneByDelivery(Delivery delivery);

    PurchaseOrder findOneByShopOrderId(String shopOrderId);

    long countBySenderAndReceiver(Shop sender, Person receiver);

    long countByReceiver(Person receiver);

    PurchaseOrder findFirstByReceiverOrderByCreatedDesc(Person receiver);

    @EntityGraph(value = "PurchaseOrder.full", type = EntityGraph.EntityGraphType.LOAD)
    List<PurchaseOrder> findAllByReceiverOrderByCreatedDesc(Person receiver);

    @EntityGraph(value = "PurchaseOrder.full", type = EntityGraph.EntityGraphType.LOAD)
    List<PurchaseOrder> findAllBySenderOrderByCreatedDesc(Shop sender);

    //In general in wrong repository because they work with shops, but needs to stay there to avoid dependencies from participants to shopping

    @Query(value = "Select count(distinct p.sender) from PurchaseOrder p where p.receiver = ?1")
    long countDistinctShopsForReceiver(Person receiver);

    @Query(value = "Select p.sender from PurchaseOrder p where p.receiver = ?1 group by p.sender having count(*) > ?2 order by p.sender.created desc")
    List<Shop> findShopsWithPurchaseOrdersByReceiverGreaterThanValueOrderByCreatedDesc(Person receiver, long value);

    @Transactional(readOnly = false)
    void deleteByCommunityId(String id);

    @Transactional(readOnly = false)
    void deleteByDeliveryCommunityId(String id);

}
