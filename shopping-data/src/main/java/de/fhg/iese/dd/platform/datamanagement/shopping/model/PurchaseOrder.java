/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shopping.model;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "PurchaseOrder.reduced",
                attributeNodes = {
                        @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.reduced"),
                        @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.reduced"),
                        @NamedAttributeNode(value = "sender", subgraph = "Shop.reduced"),
                        @NamedAttributeNode(value = "receiver", subgraph = "Person.reduced"),
                        @NamedAttributeNode(value = "items", subgraph = "PurchaseOrderItem.reduced")
                }),
        @NamedEntityGraph(name = "PurchaseOrder.full",
                includeAllAttributes = true)
})
@Table(indexes={
        @Index(columnList="shopOrderId", unique = true),
        })
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class PurchaseOrder extends BaseEntity {

    //unique with index, do not set unique = true to prevent hibernate creating additional constraint
    @Column(nullable = false)
    private String shopOrderId;

    @ManyToOne
    private Tenant community;

    @ManyToOne
    private Shop sender;

    @ManyToOne
    private ParcelAddress pickupAddress;

    @ManyToOne
    private Person receiver;

    @ManyToOne
    private ParcelAddress deliveryAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredDeliveryTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredDeliveryTimeEnd")),
    })
    private TimeSpan desiredDeliveryTime;

    @Column
    private int credits;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval= true)
    @JoinColumn(name = "purchase_order_id")
    private List<PurchaseOrderItem> items;

    @ManyToOne
    private Delivery delivery;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String purchaseOrderNotes;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String transportNotes;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String contentNotes;

    @Override
    public String toString() {
        return "PurchaseOrder [id=" + id +
                ", shopOrderId=" + shopOrderId +
                ", community=" + community +
                ", sender=" + sender +
                ", pickupAddress=" + pickupAddress +
                ", receiver=" + receiver +
                ", deliveryAddress=" + deliveryAddress +
                ", deliveryId=" + (delivery == null ? null : delivery.getId()) +
                "]";
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
