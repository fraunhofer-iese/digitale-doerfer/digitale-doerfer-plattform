/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.framework;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Streams;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.MultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.services.IAchievementService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;

public abstract class BaseAchievementRule<E extends BaseEvent> implements IAchievementRule<E> {

    private static final String ACHIEVEMENT_ICON_DEFAULT_LOCATION = "images/achievements/";

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    protected ITimeService timeService;

    @Autowired
    protected IAchievementService achievementService;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Autowired
    private IMediaItemService mediaItemService;

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize(){
        eventBusAccessor.setOwner(this);
    }

    @Override
    abstract public String getName();

    @Override
    public final List<BaseEvent> checkAchievementAndCreateEvent(E event) {
        try {
            List<AchievementPersonPairing> achievedLevels = checkAchievement(event);
            if (achievedLevels != null) {
                List<AchievementPersonPairing> nonNullAchievedLevels = achievedLevels.stream()
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                if (nonNullAchievedLevels.size() == 1) {
                    return Collections.singletonList(new AchievementAchievedEvent(nonNullAchievedLevels.get(0), false));
                }
                if (nonNullAchievedLevels.size() > 1) {
                    Person achiever = nonNullAchievedLevels.get(0).getPerson();
                    return Streams.concat(
                            nonNullAchievedLevels.stream()
                                    .map(achievementPersonPairing ->
                                            new AchievementAchievedEvent(achievementPersonPairing, true)),
                            Stream.of(new MultiAchievementAchievedEvent(achiever, nonNullAchievedLevels)))
                            .collect(Collectors.toList());
                }
            }
        } catch (Exception e) {
            //never make an achievement fail other events
            log.error("Failed in checking achievements for rule \"" + getName() + "\"", e);
        }
        return null;
    }

    @Override
    abstract public List<AchievementPersonPairing> checkAchievement(E event);

    @Override
    abstract public Collection<AchievementPersonPairing> checkAchievement(Person person);

    @Override
    abstract public Class<E> getRelevantEvent();

    @Override
    abstract public Collection<Achievement> createOrUpdateRelevantAchievements();

    @Override
    abstract public void dropRelevantAchievements();

    /**
     * Create a MediaItem from the available default images. To add available images upload them to the S3 bucket as
     * defined in the configuration
     *
     * @param defaultMediaIdentifier
     *
     * @return created image
     *
     * @throws FileItemUploadException if the file was not available
     */
    protected MediaItem createIconFromDefault(String defaultMediaIdentifier) throws FileItemUploadException {
        MediaItem icon;
        try {
            icon = mediaItemService.createMediaItemFromDefault(
                    ACHIEVEMENT_ICON_DEFAULT_LOCATION + defaultMediaIdentifier, FileOwnership.UNKNOWN);
        } catch (Exception e1) {
            log.error("Failed in getting default image: " + defaultMediaIdentifier + ", using fallback");
            try {
                icon = mediaItemService.createMediaItemFromDefault(
                        ACHIEVEMENT_ICON_DEFAULT_LOCATION + "badge_dummy_grey.png", FileOwnership.UNKNOWN);
            } catch (Exception e2) {
                log.error("Failed in getting fallback image: badge_dummy_grey.png");
                icon = null;
            }
        }
        return icon;
    }

    protected PushCategory findPushCategory(PushCategoryId pushCategoryId){
        return pushCategoryService.findPushCategoryById(pushCategoryId);
    }

}
