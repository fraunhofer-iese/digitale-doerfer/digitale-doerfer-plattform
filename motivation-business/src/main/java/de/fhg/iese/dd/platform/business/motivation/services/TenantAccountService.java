/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Service
class TenantAccountService implements ITenantAccountService {

    @Autowired
    private TenantAccountRepository tenantAccountRepository;

    @Override
    @Transactional
    public TenantAccount createTenantAccountIfNotExisting(AppAccountType appAccountType, Tenant tenant) {
        TenantAccount tenantAccount = tenantAccountRepository.findByOwnerAndAppAccountType(tenant, appAccountType);
        if (tenantAccount != null) {
            return tenantAccount;
        }
        tenantAccount = new TenantAccount();
        tenantAccount.setAppAccountType(appAccountType);
        tenantAccount.setOwner(tenant);
        tenantAccount.setBalance(0);
        return tenantAccountRepository.save(tenantAccount);
    }

    @Override
    public TenantAccount findTenantAccount(AppAccountType appAccountType, Tenant tenant) {
        TenantAccount tenantAccount = tenantAccountRepository.findByOwnerAndAppAccountType(tenant, appAccountType);
        if (tenantAccount == null) {
            throw new AccountNotFoundException("No account of type {} for tenant {}", appAccountType, tenant);
        }
        return tenantAccount;
    }

    @Override
    public TenantAccount store(TenantAccount account) {
        return tenantAccountRepository.save(account);
    }

}
