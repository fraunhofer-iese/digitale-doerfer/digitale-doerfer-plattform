/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreatedInDataInitEvent;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
public class PersonAccountEventProcessor  extends BaseEventProcessor {

    @Autowired
    private IPersonAccountService personAccountService;

    @Autowired
    private IAccountService accountService;

    @EventProcessing
    private void handlePersonCreatedInDataInitEvent(PersonCreatedInDataInitEvent event) {
        createAccountForPerson(event.getPerson());
    }

    @EventProcessing
    private void handlePersonCreatedEvent(PersonCreateConfirmation event) {
        createAccountForPerson(event.getPerson());
    }

    private void createAccountForPerson(Person newPerson) {
        if(newPerson == null){
            log.error("Could not create account, person is null");
            return;
        }
        PersonAccount newAccount = personAccountService.createPersonAccountIfNotExisting(newPerson);
        if(newAccount == null){
            log.error("Could not create account, account is null");
            return;
        }
        accountService.bookInitialValue(newAccount);
        log.info("Created account {} for person {}", newAccount.getId(), newPerson.getId() );
    }

}
