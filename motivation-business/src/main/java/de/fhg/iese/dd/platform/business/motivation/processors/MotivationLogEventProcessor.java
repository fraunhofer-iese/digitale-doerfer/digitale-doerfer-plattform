/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.CheckAchievementAchievedRequest;
import de.fhg.iese.dd.platform.business.motivation.events.MultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.shared.log.processors.BaseLogEventProcessor;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class MotivationLogEventProcessor extends BaseLogEventProcessor {

    @EventProcessing(relevantEvents = {
            AchievementAchievedEvent.class,
            MultiAchievementAchievedEvent.class,
            CheckAchievementAchievedRequest.class})
    protected void logEvent(BaseEvent event, EventProcessingContext context) {
        super.logEvent(event, context);
    }

}
