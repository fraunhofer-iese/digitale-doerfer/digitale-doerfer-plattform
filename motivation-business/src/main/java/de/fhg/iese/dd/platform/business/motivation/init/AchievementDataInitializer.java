/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.init;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.motivation.framework.IAchievementRule;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@Component
public class AchievementDataInitializer extends BaseDataInitializer {

    @Autowired
    private List<IAchievementRule<?>> achievementRules;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("achievement")
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(PushCategory.class)
                .processedEntity(Achievement.class)
                .processedEntity(AchievementLevel.class)
                .build();
    }

    @Override
    public DataInitializerResult createInitialData(LogSummary logSummary) {
        logSummary.setLogger(log);
        List<Throwable> thrownExceptions = new LinkedList<>();
        int numAchievements = 0;
        int numAchievementLevels = 0;
        for(IAchievementRule<?> achievementRule : achievementRules){
            try {
                Collection<Achievement> createdAchievements = achievementRule.createOrUpdateRelevantAchievements();
                int numCurrentAchievements = createdAchievements.size();
                int numCurrentAchievementLevels =
                        createdAchievements.stream().mapToInt(a -> a.getAchievementLevels().size()).sum();
                //create a simple summary to show what achievements were created
                logSummary.info("Creating "+numCurrentAchievements+" Achievements with "+numCurrentAchievementLevels+" Achievement Levels for rule "+achievementRule.getName());
                logSummary.indent();
                for(Achievement createdAchievement : createdAchievements){
                    logSummary.info("Achievement {} ({})", createdAchievement.getName(), createdAchievement.getId());
                    logSummary.indent();
                    if(createdAchievement.getAchievementLevels() == null || createdAchievement.getAchievementLevels().isEmpty()){
                        logSummary.warn("No AchievementLevels");
                    }else{
                        List<AchievementLevel> sortedAchievementLevels = createdAchievement.getAchievementLevels().stream()
                                .sorted(Comparator.comparing(AchievementLevel::getOrderValue))
                                .collect(Collectors.toList());
                        for(AchievementLevel achievementLevel : sortedAchievementLevels){
                            logSummary.info("AchievementLevel '{}' ({})", achievementLevel.getName(), achievementLevel.getId());

                        }
                    }
                    logSummary.outdent();
                }
                logSummary.outdent();
                numAchievements += numCurrentAchievements;
                numAchievementLevels += numCurrentAchievementLevels;
            } catch (Exception e) {
                thrownExceptions.add(e);
                logSummary.error("Failed to create achievements for rule {} : {}", e,
                        achievementRule.getClass().getName(), e.getMessage());
            }
        }
        logSummary.info("Finished creating {} Achievements with {} Achievement Levels", numAchievements,
                numAchievementLevels);
        return new DataInitializerResult(null, thrownExceptions);
    }

}
