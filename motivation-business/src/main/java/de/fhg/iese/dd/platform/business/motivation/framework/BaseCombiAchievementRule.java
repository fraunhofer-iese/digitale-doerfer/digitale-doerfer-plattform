/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.framework;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public abstract class BaseCombiAchievementRule extends BaseAchievementRule<AchievementAchievedEvent> {

    @Override
    public final List<AchievementPersonPairing> checkAchievement(AchievementAchievedEvent event) {
        Set<String> relevantAchievementIds = getAchievementLevelsToCombine();

        AchievementLevel achievedLevel = event.getAchievedLevel().getAchievementLevel();

        if(relevantAchievementIds.contains(achievedLevel.getId())){
            Person achiever = event.getAchievedLevel().getPerson();
            long timestamp = event.getAchievedLevel().getTimestamp();
            if(achievementService.hasAchievedAllLevels(achiever, relevantAchievementIds)){
                return Collections.singletonList(achieveCombinedAchievement(achiever, timestamp));
            }
        }
        return null;
    }

    @Override
    public final Collection<AchievementPersonPairing> checkAchievement(Person person) {
        Set<String> relevantAchievementIds = getAchievementLevelsToCombine();

        if(achievementService.hasAchievedAllLevels(person, relevantAchievementIds)){
            return Collections.singletonList(achieveCombinedAchievement(person, timeService.currentTimeMillisUTC()));
        }
        return Collections.emptyList();
    }

    @Override
    public final Class<AchievementAchievedEvent> getRelevantEvent() {
        return AchievementAchievedEvent.class;
    }

    protected abstract Set<String> getAchievementLevelsToCombine();

    protected abstract AchievementPersonPairing achieveCombinedAchievement(Person person, long timestamp);

}
