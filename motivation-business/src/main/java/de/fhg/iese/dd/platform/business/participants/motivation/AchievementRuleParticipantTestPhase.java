/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.motivation;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.motivation.events.CheckAchievementAchievedRequest;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.config.TestPhaseConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public abstract class AchievementRuleParticipantTestPhase extends BaseAchievementRule<CheckAchievementAchievedRequest> {

    @Autowired
    protected ParticipantsConfig participantsConfig;

    protected TestPhaseConfig getTestPhaseConfig(){
        return participantsConfig.getTestPhase();
    }

    @Override
    public Class<CheckAchievementAchievedRequest> getRelevantEvent() {
        //In fact we do not need any event here, since this is already used by the AchievementEventProcessor.
        //We use that just for convenience to make clear that this is only done manually
        return CheckAchievementAchievedRequest.class;
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(CheckAchievementAchievedRequest event) {
        // the AchievementEventProcessor already triggers the person check, so there is no implementation required
        return null;
    }

    protected Collection<AchievementPersonPairing> checkActiveInBetween(long startTime, long endTime, Person person, String achievementLevelId){
        long personActiveStart = person.getCreated();
        long personActiveEnd = person.getLastLoggedIn();

        if((personActiveEnd > startTime) // active after the start
            &&
           (personActiveStart < endTime)) //active before the end
        {
            return Collections.singleton(achievementService.achieveLevel(achievementLevelId, person, personActiveEnd));
        }
        return Collections.emptyList();
    }

    long convertToUTCTimeStamp(String timestamp) {
        LocalDateTime localDateTime = LocalDateTime.parse(timestamp);
        return timeService.toTimeMillis(
                localDateTime.atZone(timeService.getLocalTimeZone())
                        .withZoneSameInstant(ZoneId.of("UTC")));
    }

}
