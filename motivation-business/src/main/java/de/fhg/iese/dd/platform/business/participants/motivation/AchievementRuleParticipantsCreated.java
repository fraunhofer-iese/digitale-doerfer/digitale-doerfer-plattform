/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.motivation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleParticipantsCreated extends BaseAchievementRule<PersonCreateConfirmation> {

    private static final String ACHIEVEMENT_ID          = "985734fc-4d98-40f6-9787-f758650b75e6";
    /**
     * Created
     */
    private static final String ACHIEVEMENT_LEVEL_ID_00 = "4a82171a-e8a6-4556-b4f2-7be5e5074b8a";

    @Override
    public String getName() {
        return "score.participants.created.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PersonCreateConfirmation event) {
        Person person = event.getPerson();
        if(person == null) {
            return null;
        }
        long timestamp = event.getCreated();

        Collection<String> achievementLevelIds = Collections.singletonList(
                ACHIEVEMENT_LEVEL_ID_00);

        Set<String> achievedLevels = achievementService.getAchievedLevels(person, achievementLevelIds).stream()
                .map(a -> a.getAchievementLevel().getId())
                .collect(Collectors.toSet());

        if(!achievedLevels.contains(ACHIEVEMENT_LEVEL_ID_00)) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_00, person, timestamp));
        }else{
            return null;
        }
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        long timestamp = person.getCreated();

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_00, person, timestamp));

        return achievedLevels;
    }

    @Override
    public Class<PersonCreateConfirmation> getRelevantEvent() {
        return PersonCreateConfirmation.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Participant.Created")
                .description("Melde ich an um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel00 = AchievementLevel.builder()
                .name("Newbie")
                .description("Sie werden so schnell erwachsen - und jetzt Hefte raus: Klassenarbeit!")
                .challengeDescription("Melde dich zum ersten Mal an.")
                .icon(createIconFromDefault("newbie.png"))
                .iconNotAchieved(createIconFromDefault("newbie_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel00.setId(ACHIEVEMENT_LEVEL_ID_00);
        achievementLevel00 = achievementService.save(achievementLevel00);

        achievement.setAchievementLevels(Stream.of(achievementLevel00).collect(Collectors.toSet()));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_00);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
