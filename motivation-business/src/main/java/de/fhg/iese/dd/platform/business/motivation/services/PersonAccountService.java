/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;

@Service
class PersonAccountService extends BaseEntityService<PersonAccount> implements IPersonAccountService {

    @Autowired
    private PersonAccountRepository personAccountRepository;
    @Autowired
    private AccountEntryRepository accountEntryRepository;

    @Override
    @Transactional
    public PersonAccount createPersonAccountIfNotExisting(Person person) {
        PersonAccount account = personAccountRepository.findByOwner(person);
        if (account != null) {
            return account;
        }
        account = new PersonAccount();
        account.setOwner(person);
        account.setBalance(0);
        return personAccountRepository.saveAndFlush(account);
    }

    @Override
    @Transactional(readOnly = true)
    public PersonAccount getAccountForOwner(Person owner) throws AccountNotFoundException {
        PersonAccount account = personAccountRepository.findByOwner(owner);
        if (account == null) {
            throw new AccountNotFoundException("No account for person '" + owner.getId() + "'").withDetail(
                    owner.getId());
        }
        List<AccountEntry> entries = accountEntryRepository.findByAccountOrderByCreatedAsc(account);
        account.setEntries(entries);
        return account;
    }

    @Override
    public PersonAccount getAccountForShop(Shop shop) throws AccountNotFoundException {
        Person shopOwner = shop.getOwner();
        if(shopOwner == null){
            throw new AccountNotFoundException("No owner defined for shop '"+shop.getId()+"'").withDetail(shop.getId());
        }
        return getAccountForOwner(shopOwner);
    }

}
