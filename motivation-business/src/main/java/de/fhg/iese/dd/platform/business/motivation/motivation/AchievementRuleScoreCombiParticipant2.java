/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.motivation;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseCombiAchievementRule;
import de.fhg.iese.dd.platform.business.participants.motivation.AchievementRuleParticipantsRegistered;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleScoreCombiParticipant2 extends BaseCombiAchievementRule {

    private static final String ACHIEVEMENT_ID = "146f0459-468d-4b15-88f3-50b1abb2630d";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "03a01686-758c-4a19-b0cb-fcd0fe556ba3";

    private static final Set<String> achievementLevelsToCombine =
            Set.of(
                    //0 DT
                    AchievementRuleScoreAccountReceived.ACHIEVEMENT_LEVEL_ID_00,
                    //Registered 6 Months
                    AchievementRuleParticipantsRegistered.ACHIEVEMENT_LEVEL_ID_03);

    @Override
    public String getName() {
        return "score.account.combi.participant.2";
    }

    @Override
    protected Set<String> getAchievementLevelsToCombine(){
        return achievementLevelsToCombine;
    }

    @Override
    protected AchievementPersonPairing achieveCombinedAchievement(Person person, long timestamp){
        return achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp);
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Score.Account.Combi.Participant")
                .description("Gebe alle Digitaler aus und sei 6 Monate aktiv")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Dschungel-Camper")
                .description("Oh Oh Oh - Die Leute kennen dich zwar aber du bist pleite! Bitte einchecken im nächsten C-Promi-Airlines-Flug nach Australien!")
                .challengeDescription("Sei sechs Monate angemeldet und habe keine DigiTaler auf dem Konto.")
                .icon(createIconFromDefault("dschungelcamper.png"))
                .iconNotAchieved(createIconFromDefault("dschungelcamper_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        //only needed since the set is not updated automatically by hibernate based on the newly added achievement levels
        achievement.setAchievementLevels(Collections.singleton(achievementLevel01));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
