/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.dataprivacy;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountNotFoundException;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Component
public class AccountDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);
    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Account.class);

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IPersonAccountService personAccountService;

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IMotivationDataDeletionService motivationDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        final PersonAccount account;
        try {
            account = personAccountService.getAccountForOwner(person);
        } catch (AccountNotFoundException ex) {
            //person has no account
            return;
        }
        IDataPrivacyReport.IDataPrivacyReportSection section = dataPrivacyReport.newSection("DigiTaler",
                "Account und Transaktionen");

        //Account
        IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                section.newSubSection("Account", "Informationen zum Account");
        subSection.newContent("Erstellt am", timeService.toLocalTimeHumanReadable(account.getCreated()));
        subSection.newContent("Aktuelles Guthaben", account.getBalance() + " DigiTaler");

        //Transaktionen
        subSection = section.newSubSection("Transaktionen", "DigiTaler-Transaktionen");
        final List<AccountEntry> accountEntries = accountService.getAccountEntriesForAccount(account);
        for (AccountEntry accountEntry : accountEntries) {
            subSection.newContent(buildName(accountEntry), buildContent(accountEntry));
        }
    }

    private String buildName(AccountEntry accountEntry) {
        return timeService.toLocalTimeHumanReadable(accountEntry.getTimestamp()) + ", " + accountEntry.getPostingText();
    }

    private String buildContent(AccountEntry accountEntry) {
        final StringBuilder content = new StringBuilder();
        content.append(Math.abs(accountEntry.getAmount())).append(" DigiTaler");

        final String partnerOrNullIfSystem = accountEntry.getPartner() instanceof PersonAccount ?
                ((PersonAccount) accountEntry.getPartner()).getOwner().getFullName() : null;

        if (accountEntry.getAccountEntryType() == AccountEntryType.PLUS) {
            if (partnerOrNullIfSystem == null) {
                content.append(" erhalten");
            } else {
                content.append(" von ").append(partnerOrNullIfSystem).append(" erhalten");
            }
        } else if (accountEntry.getAccountEntryType() == AccountEntryType.MINUS) {
            if (partnerOrNullIfSystem == null) {
                content.append(" gezahlt");
            } else {
                content.append(" an ").append(partnerOrNullIfSystem).append(" gezahlt");
            }
        } else if (accountEntry.getAccountEntryType() == AccountEntryType.RESERVATION_PLUS) {
            if (partnerOrNullIfSystem == null) {
                content.append(" als 'erhalten' vorgemerkt");
            } else {
                content.append(" als 'erhalten von ").append(partnerOrNullIfSystem).append("' vorgemerkt");
            }
        } else if (accountEntry.getAccountEntryType() == AccountEntryType.RESERVATION_MINUS) {
            if (partnerOrNullIfSystem == null) {
                content.append(" zur Zahlung vorgemerkt");
            } else {
                content.append(" zur Zahlung an ").append(partnerOrNullIfSystem).append(" vorgemerkt");
            }
        } else {
            log.warn("Unknown AccountEntryType " + accountEntry.getAccountEntryType().toString());
            if (accountEntry.getAmount() < 0) {
                content.append(" gezahlt");
            } else {
                content.append(" erhalten");
            }
        }
        content.append(".");
        return content.toString();
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {
        final PersonAccount account;
        try {
            account = personAccountService.getAccountForOwner(person);
        } catch (AccountNotFoundException ex) {
            //person has no account
            return;
        }

        //Account:
        //We do not alter the account, because account.owner will be erased by PersonDataPrivacyHandler. No further
        //action is required.

        //Transactions
        final List<AccountEntry> accountEntriesForAccount = accountService.getAccountEntriesForAccount(account);
        for (AccountEntry accountEntry : accountEntriesForAccount) {
            personDeletionReport.addEntry(accountEntry, motivationDataDeletionService.deleteAccountEntry(accountEntry));
        }
        final List<AccountEntry> accountEntriesForPartner = accountService.getAccountEntriesForPartner(account);
        for (AccountEntry accountEntry : accountEntriesForPartner) {
            personDeletionReport.addEntry(accountEntry, motivationDataDeletionService.deleteAccountEntry(accountEntry));
        }
    }
    
}
