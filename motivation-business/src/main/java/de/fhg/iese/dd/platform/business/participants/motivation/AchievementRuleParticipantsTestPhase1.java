/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.motivation;

import java.util.Collection;
import java.util.Collections;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleParticipantsTestPhase1 extends AchievementRuleParticipantTestPhase {

    private static final String ACHIEVEMENT_ID          = "38f8a105-aa48-41c9-85f4-d1edbc80aa0e";
    private static final String ACHIEVEMENT_LEVEL_ID_00 = "5a0d155e-41e8-4525-923b-6415c2927839";

    @Override
    public String getName() {
        return "score.participants.testPhase.1";
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        return checkActiveInBetween(
            convertToUTCTimeStamp(getTestPhaseConfig().getAlphaStartDate()),
            convertToUTCTimeStamp(getTestPhaseConfig().getAlphaEndDate()),
            person,
            ACHIEVEMENT_LEVEL_ID_00
            );
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Participant.TestPhase1")
                .description("Nehme am Alpha-Test teil um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel00 = AchievementLevel.builder()
                .name("Nerd")
                .description("Sheldon Cooper wäre stolz auf dich!")
                .challengeDescription("Nimm an einem Alpha-Test teil.")
                .icon(createIconFromDefault("nerd.png"))
                .iconNotAchieved(createIconFromDefault("nerd_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel00.setId(ACHIEVEMENT_LEVEL_ID_00);
        achievementLevel00 = achievementService.save(achievementLevel00);

        achievement.setAchievementLevels(Collections.singleton(achievementLevel00));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_00);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
