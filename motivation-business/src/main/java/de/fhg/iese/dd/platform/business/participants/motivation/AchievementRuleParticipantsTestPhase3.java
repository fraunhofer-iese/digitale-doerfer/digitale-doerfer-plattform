/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.motivation;

import java.util.Collection;
import java.util.Collections;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleParticipantsTestPhase3 extends AchievementRuleParticipantTestPhase {

    private static final String ACHIEVEMENT_ID          = "73515cae-999b-42bf-b84d-4e176c16559a";
    private static final String ACHIEVEMENT_LEVEL_ID_00 = "3bdbe0b5-a8a4-48f4-88a7-57a56677a793";

    @Override
    public String getName() {
        return "score.participants.testPhase.3";
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        return checkActiveInBetween(
            convertToUTCTimeStamp(getTestPhaseConfig().getGammaStartDate()),
            convertToUTCTimeStamp(getTestPhaseConfig().getGammaEndDate()),
            person,
            ACHIEVEMENT_LEVEL_ID_00
            );
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Participant.TestPhase3")
                .description("Nehme am Gamma-Test teil um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel00 = AchievementLevel.builder()
                .name("Hacker")
                .description("Schluckst du die rote Kapsel, bleibst du im Wunderland und ich führe dich in die tiefsten Tiefen des Kaninchenbaus.")
                .challengeDescription("Nimm an einem Gamma-Test teil.")
                .icon(createIconFromDefault("hacker.png"))
                .iconNotAchieved(createIconFromDefault("hacker_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel00.setId(ACHIEVEMENT_LEVEL_ID_00);
        achievementLevel00 = achievementService.save(achievementLevel00);

        achievement.setAchievementLevels(Collections.singleton(achievementLevel00));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_00);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
