/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.init;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.ITenantAccountService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class AccountDataInitializer extends BaseDataInitializer {

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private ITenantAccountService tenantAccountService;
    @Autowired
    private IAccountService accountService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("account")
                .useCase(USE_CASE_NEW_TENANT)
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .processedEntity(Account.class)
                .processedEntity(TenantAccount.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        Collection<Tenant> tenants = tenantService.findAllOrderedByNameAsc();
        for (Tenant tenant : tenants) {
            for (AppAccountType appAccountType : AppAccountType.values()) {
                TenantAccount tenantAccount =
                        tenantAccountService.createTenantAccountIfNotExisting(appAccountType, tenant);
                accountService.bookInitialValue(tenantAccount);
                logSummary.info("Created tenant account {} for {}", appAccountType, tenant.toString());
            }
        }
    }

}
