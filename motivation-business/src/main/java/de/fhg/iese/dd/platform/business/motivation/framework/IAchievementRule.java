/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.framework;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.motivation.processors.AchievementEventProcessor;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

/**
 * A rule to check if an {@link AchievementLevel} was achieved by a person.
 * <p/>
 * <strong>Do not implement this on your own, use {@link BaseAchievementRule}</strong>
 *
 * @param <E> the type of event that can trigger the achievement.
 */
public interface IAchievementRule<E extends BaseEvent> {

    /**
     * A human readable identifier for the rule. Only used in logs, should be
     * unique, e.g. "module.my-awesome-achievement.1"
     *
     * @return the name of the rule
     *
     */
    String getName();

    /**
     * Check if a new achievement level was achieved due to the event. This
     * check is automatically triggered when this event occurs.
     * <p/>
     * There is no need to send an event after an achievement level was
     * achieved, this is automatically done by the generic
     * {@link AchievementEventProcessor}.
     * <p/>
     * In general only one achievement level is reached based on an event. If
     * several are reached they should be separated into different rules.
     *
     * @param event
     *            the event of type {@link #getRelevantEvent()} that was seen on
     *            the event bus
     * @return
     *         <li><code>null</code> if no achievement level was achieved</li>
     *         <li>the new achieved level according to the event</li>
     *
     */
    List<AchievementPersonPairing> checkAchievement(E event);

    /**
     * Internal method to register the rule on the event bus. This method can
     * not be overwritten and should not be changed.
     *
     * @param event
     *            the event of type {@link #getRelevantEvent()} that was seen on
     *            the event bus
     *
     */
    List<BaseEvent> checkAchievementAndCreateEvent(E event);

    /**
     * Check if a new achievement level was achieved previously by this person.
     * This check is manually triggered after a new achievement was introduced
     * to check if some persons already have it. Since in that case several
     * levels can be achieved at once a list of achievement levels is returned.
     * <p/>
     * Try to use a realistic time stamp for the achievement levels, but do not
     * spend too much effort in determining it completely correct.
     * <p/>
     * There is no need to send an event after the achievement levels are
     * achieved.
     *
     * @param person
     *
     * @return
     */
    Collection<AchievementPersonPairing> checkAchievement(Person person);

    /**
     * Get the type of event that can trigger the achievement of an achievement
     * level managed by this rule
     *
     * @return the class of event that can be received in
     *         {@link #checkAchievement(BaseEvent)}
     *
     */
    Class<E> getRelevantEvent();

    /**
     * Create all achievements and all achievement levels that are managed by
     * this rule. This is triggered by the admin services on
     * create/updateInitialData using the topic <code>achievement</code>
     *
     * @return the newly created achievements and contained achievement levels
     *
     */
    Collection<Achievement> createOrUpdateRelevantAchievements();

    /**
     * Drop all achievements and all achievement levels that are managed by this
     * rule. This is triggered by the admin services on dropData
     *
     */
    void dropRelevantAchievements();

}
