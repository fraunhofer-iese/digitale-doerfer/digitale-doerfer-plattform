/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AchievementLevelNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IAchievementService extends IService {

    List<AchievementPersonPairing> getAchievedLevels(Person person);

    /**
     * Checks if the person has achieved all specified levels
     *
     * @param person
     *            the person that should have achieved all levels
     * @param achievementLevelIds
     *            ids of the levels that should be checked
     * @return <code>true</code> if the person has
     *         {@link AchievementPersonPairing}s for all
     *         {@link AchievementLevel}s with the given ids
     */
    boolean hasAchievedAllLevels(Person person, Collection<String> achievementLevelIds);
    List<AchievementPersonPairing> getAchievedLevels(Person person, Collection<String> achievementLevelIds);

    List<Achievement> getAchievements();
    List<AchievementLevel> getAchievementLevels(Achievement achievement);
    AchievementLevel getAchievementLevelById(String achievementLevelId) throws AchievementLevelNotFoundException;

    /**
     * Achieve the level for the person at the given time. <strong>Only returns
     * the {@link AchievementPersonPairing} when the achievement was not
     * achieved before</strong>
     *
     * @param achievementLevel
     *            the level to achieve
     * @param person
     *            the person that achieves this level
     * @param timestamp
     *            the timestamp that will be used if the level was not achieved
     *            yet
     * @return
     *         <li>the {@link AchievementPersonPairing} that was newly created
     *         </li>
     *         <li><code>null</code> if the level was already achieved</li>
     */
    AchievementPersonPairing achieveLevel(AchievementLevel achievementLevel, Person person, long timestamp);

    /**
     * Achieve the level for the person at the given time. <strong>Only returns
     * the {@link AchievementPersonPairing} when the achievement was not
     * achieved before</strong>
     *
     * @param achievementLevelId
     *            the id of the level to achieve
     * @param person
     *            the person that achieves this level
     * @param timestamp
     *            the timestamp that will be used if the level was not achieved
     *            yet
     * @return
     *         <li>the {@link AchievementPersonPairing} that was newly created
     *         </li>
     *         <li><code>null</code> if the level was already achieved</li>
     * @throws AchievementLevelNotFoundException
     *             if the level with the achievementLevelId could not be found
     */
    AchievementPersonPairing achieveLevel(String achievementLevelId, Person person, long timestamp) throws AchievementLevelNotFoundException;

    Achievement save(Achievement achievement);
    void removeAchievement(String achievementId);

    AchievementLevel save(AchievementLevel achievementLevel);
    void removeAchievementLevel(String achievementLevelId);
    void removeAchievementLevelAndAchievementPersonPairings(String achievementLevelId);

    AchievementPersonPairing save(AchievementPersonPairing achievementPersonPairing);
    void removeAllAchievementPersonPairingsForAchievementLevel(String achievementLevelId);
    List<AchievementPersonPairing> achieveAllLevels(Person person);
    void unachieveAllLevels(Person person);

}
