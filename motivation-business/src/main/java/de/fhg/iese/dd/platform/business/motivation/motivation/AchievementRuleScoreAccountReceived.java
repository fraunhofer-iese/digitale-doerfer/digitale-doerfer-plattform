/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.motivation;

import de.fhg.iese.dd.platform.business.motivation.events.ScoreChangedEvent;
import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.datamanagement.motivation.config.ScoreConfig;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AchievementRule
public class AchievementRuleScoreAccountReceived extends BaseAchievementRule<ScoreChangedEvent> {

    private static final String ACHIEVEMENT_ID          = "8e15fd2a-e280-47a1-b034-74c5e5447bec";
    /**
     * 0 DT
     */
    public  static final String ACHIEVEMENT_LEVEL_ID_00 = "ff98b0b7-e3f1-4f23-bfdc-e554d08559c0";
    /**
     * 10 DT
     */
    private static final String ACHIEVEMENT_LEVEL_ID_10 = "6dd092b5-e00b-4c81-a1bd-d7ebaf7f5bea";
    /**
     * 50 DT
     */
    public  static final String ACHIEVEMENT_LEVEL_ID_50 = "1782796b-a5a0-4eaf-8299-ea687e4917c9";

    @Autowired
    private IPersonAccountService personAccountService;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ScoreConfig scoreConfig;

    @Override
    public String getName() {
        return "score.account.received.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(ScoreChangedEvent event) {
        Account account = event.getAccount();
        if (!(account instanceof PersonAccount personAccount)) {
            return null;
        }
        Person person = personAccount.getOwner();
        if(person == null) {
            return null;
        }

        long timestamp = event.getCreated();

        if (account.getBalance() == 0) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_00, person, timestamp));
        }

        if (account.getBalance() >= scoreConfig.getInitialAmountPersonAccounts() + 10) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, person, timestamp));
        }

        if (account.getBalance() >= scoreConfig.getInitialAmountPersonAccounts() + 50) {
            return Collections.singletonList(
                    achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_50, person, timestamp));
        }

        return null;
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PersonAccount personAccount = personAccountService.getAccountForOwner(person);

        AccountEntry accountEntry = accountService.getLatestAccountEntryForAccount(personAccount);
        long timestamp = accountEntry == null ? timeService.currentTimeMillisUTC() : accountEntry.getCreated();

        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        if(personAccount.getBalance() == 0) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_00, person, timestamp));
        }

        if(personAccount.getBalance() >= scoreConfig.getInitialAmountPersonAccounts() + 10) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, person, timestamp));
        }

        if(personAccount.getBalance() >= scoreConfig.getInitialAmountPersonAccounts() + 50) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_50, person, timestamp));
        }

        return achievedLevels;
    }

    @Override
    public Class<ScoreChangedEvent> getRelevantEvent() {
        return ScoreChangedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Score.Account.Received")
                .description("Spare Digitaler an um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Bankrott")
                .description("Zeit den Gürtel ein wenig enger zu schnallen!")
                .challengeDescription("Habe keine DigiTaler auf dem Konto.")
                .icon(createIconFromDefault("bankrott.png"))
                .iconNotAchieved(createIconFromDefault("bankrott_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_00);
        achievementLevel01 = achievementService.save(achievementLevel01);

        AchievementLevel achievementLevel10 = AchievementLevel.builder()
                .name("Sparschwein")
                .description("Schon zehn Digitaler angespart - bald kannst du es aber so richtig krachen lassen!")
                .challengeDescription("Spare 10 DigiTaler an.")
                .icon(createIconFromDefault("sparschwein.png"))
                .iconNotAchieved(createIconFromDefault("sparschwein_grey.png"))
                .achievement(achievement)
                .orderValue(2)
                .build();
        achievementLevel10.setId(ACHIEVEMENT_LEVEL_ID_10);
        achievementLevel10 = achievementService.save(achievementLevel10);

        AchievementLevel achievementLevel50 = AchievementLevel.builder()
                .name("Cash Cow")
                .description("Mehr Cash als die Bank - Tut dir mal was Gutes, oder deinen Nachbarn!")
                .challengeDescription("Spare 50 DigiTaler an.")
                .icon(createIconFromDefault("cashcow.png"))
                .iconNotAchieved(createIconFromDefault("cashcow_grey.png"))
                .achievement(achievement)
                .orderValue(3)
                .build();
        achievementLevel50.setId(ACHIEVEMENT_LEVEL_ID_50);
        achievementLevel50 = achievementService.save(achievementLevel50);

        achievement.setAchievementLevels(Stream.of(achievementLevel01, achievementLevel10, achievementLevel50).collect(Collectors.toSet()));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_00);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_10);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_50);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
