/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.dataprivacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.BaseDataDeletionService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementPersonPairingRepository;

@Service
class MotivationDataDeletionService extends BaseDataDeletionService implements IMotivationDataDeletionService {

    @Autowired
    private AchievementPersonPairingRepository achievementPersonPairingRepository;

    @Autowired
    private AccountEntryRepository accountEntryRepository;

    @Override
    public DeleteOperation deleteAchievementPersonPairing(AchievementPersonPairing achievedLevel) {

        achievementPersonPairingRepository.delete(achievedLevel);

        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteAccountEntry(AccountEntry accountEntry) {

        accountEntry.setPostingText(null);
        accountEntryRepository.save(accountEntry);

        return DeleteOperation.ERASED;
    }

}
