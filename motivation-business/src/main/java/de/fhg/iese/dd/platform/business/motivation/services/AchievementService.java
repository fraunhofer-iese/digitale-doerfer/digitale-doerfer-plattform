/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AchievementLevelNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementLevelRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementPersonPairingRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Service
class AchievementService extends BaseService implements IAchievementService {

    @Autowired
    private AchievementRepository achievementRepository;

    @Autowired
    private AchievementLevelRepository achievementLevelRepository;

    @Autowired
    private AchievementPersonPairingRepository achievementPersonPairingRepository;

    @Override
    public List<AchievementPersonPairing> getAchievedLevels(Person person) {
        return achievementPersonPairingRepository.findAllByPersonOrderByTimestampDesc(person);
    }

    @Override
    public boolean hasAchievedAllLevels(Person person, Collection<String> achievementLevelIds) {
        return achievementLevelIds.size() == achievementPersonPairingRepository.countAchievedLevels(person, achievementLevelIds);
    }

    @Override
    public List<AchievementPersonPairing> getAchievedLevels(Person person, Collection<String> achievementLevelIds) {
        return achievementPersonPairingRepository.findAllAchievedLevelsOrderByTimestampDesc(person, achievementLevelIds);
    }

    @Override
    public List<Achievement> getAchievements() {
        return achievementRepository.findAllOrderByCreatedDesc();
    }

    @Override
    public List<AchievementLevel> getAchievementLevels(Achievement achievement) {
        return achievementLevelRepository.findAllByAchievementOrderByCreatedDesc(achievement);
    }

    @Override
    public AchievementLevel getAchievementLevelById(String achievementLevelId) throws AchievementLevelNotFoundException {
        return achievementLevelRepository.findById(achievementLevelId)
                .orElseThrow(() -> new AchievementLevelNotFoundException("Achievement level with id "+achievementLevelId+" not found").withDetail(achievementLevelId));
    }

    @Override
    public AchievementPersonPairing achieveLevel(String achievementLevelId, Person person, long timestamp) throws AchievementLevelNotFoundException  {
        AchievementLevel achievementLevel = getAchievementLevelById(achievementLevelId);
        return achieveLevel(achievementLevel, person, timestamp);
    }

    @Override
    @Transactional
    public AchievementPersonPairing achieveLevel(AchievementLevel achievementLevel, Person person, long timestamp) {
        AchievementPersonPairing existingPairing = achievementPersonPairingRepository.findByPersonAndAchievementLevel(person, achievementLevel);
        if(existingPairing != null){
            //instead of returning the already achieved level we are returning null
            //so that we can identify if the achieved level should be pushed to the user or not
            return null;
        }else{
            AchievementPersonPairing achievementPersonPairing = AchievementPersonPairing.builder()
                    .person(person)
                    .achievementLevel(achievementLevel)
                    .timestamp(timestamp)
                    .build()
                    .withConstantId();
            AchievementPersonPairing savedAchievementPersonPairing = saveAndRetryOnDataIntegrityViolation(
                    () -> achievementPersonPairingRepository.saveAndFlush(achievementPersonPairing),
                    () -> achievementPersonPairingRepository.findByPersonAndAchievementLevel(person, achievementLevel)
            );
            if (Objects.equals(achievementPersonPairing, savedAchievementPersonPairing)) {
                return savedAchievementPersonPairing;
            } else {
                //in this case the one we created could not be saved, so we got the one created concurrently
                //instead of returning the concurrently created we are returning null
                //so that we can identify if the achieved level should be pushed to the user or not
                return null;
            }
        }
    }

    @Override
    public Achievement save(Achievement achievement) {
        return achievementRepository.saveAndFlush(achievement);
    }

    @Override
    public AchievementLevel save(AchievementLevel achievementLevel) {
        return achievementLevelRepository.saveAndFlush(achievementLevel);
    }

    @Override
    public AchievementPersonPairing save(AchievementPersonPairing achievementPersonPairing) {
        return achievementPersonPairingRepository.saveAndFlush(achievementPersonPairing);
    }

    @Override
    @Transactional
    public List<AchievementPersonPairing> achieveAllLevels(Person person) {
        List<AchievementLevel> availableLevels = achievementLevelRepository.findAll();
        List<AchievementPersonPairing> achievedLevels = new ArrayList<>(availableLevels.size());
        long timestamp = timeService.currentTimeMillisUTC();
        for(AchievementLevel availableLevel : availableLevels ){
            AchievementPersonPairing achievedLevel = achieveLevel(availableLevel, person, timestamp);
            if(achievedLevel != null){
                achievedLevels.add(achievedLevel);
            }
        }
        return achievedLevels;
    }

    @Override
    @Transactional
    public void unachieveAllLevels(Person person) {
        achievementPersonPairingRepository.deleteAllByPerson(person);
    }

    @Override
    @Transactional
    public void removeAchievement(String achievementId) {
        achievementRepository.deleteById(achievementId);
    }

    @Override
    @Transactional
    public void removeAchievementLevel(String achievementLevelId) {
        achievementLevelRepository.deleteById(achievementLevelId);
    }

    @Override
    @Transactional
    public void removeAllAchievementPersonPairingsForAchievementLevel(String achievementLevelId) {
        achievementPersonPairingRepository.deleteAllByAchievementLevelId(achievementLevelId);
    }

    @Override
    @Transactional
    public void removeAchievementLevelAndAchievementPersonPairings(String achievementLevelId) {
        removeAllAchievementPersonPairingsForAchievementLevel(achievementLevelId);
        removeAchievementLevel(achievementLevelId);
    }

}
