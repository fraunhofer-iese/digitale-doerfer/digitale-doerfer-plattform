/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.motivation.events.ScoreChangedEvent;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountCreditLimitExceededException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountEntryAlreadyBookedException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountEntryNotFoundException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountNotFoundException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.ReservationsNotMatchingException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.motivation.config.ScoreConfig;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@Service
class AccountService extends BaseService implements IAccountService {

    //can't be renamed, because there exist values in the DB
    private static final String ACCOUNT_ENTRY_MARKER_INITIAL_TENANT = "initial-community";
    private static final String ACCOUNT_ENTRY_MARKER_INITIAL_PERSON = "initial-person";

    @Autowired
    private TenantAccountRepository tenantAccountRepository;

    @Autowired
    private ITenantAccountService tenantAccountService;

    @Autowired
    private PersonAccountRepository personAccountRepository;

    @Autowired
    private AccountEntryRepository accountEntryRepository;

    @Autowired
    private ScoreConfig scoreConfig;

    @Override
    @Transactional
    public Pair<AccountEntry, AccountEntry> reserveScore(Account fromAccount, Account toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory) throws AccountCreditLimitExceededException {
        return internalBookOrReserveScore(fromAccount, toAccount, postingText, amount, subject, subjectMarker, pushCategory, true);
    }

    @Override
    @Transactional
    public Pair<AccountEntry, AccountEntry> bookScore(Account fromAccount, Account toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory) throws AccountCreditLimitExceededException {
        return internalBookOrReserveScore(fromAccount, toAccount, postingText, amount, subject, subjectMarker, pushCategory, false);
    }

    @Override
    @Transactional
    public AccountEntry bookInitialValue(TenantAccount tenantAccount) {
        Tenant tenant = tenantAccount.getOwner();
        if (!existsAccountEntryForSubject(tenantAccount, tenant, ACCOUNT_ENTRY_MARKER_INITIAL_TENANT)) {
            return bookInitialScore(tenantAccount, "Initiale Digitaler", scoreConfig.getInitialAmountTenantAccounts(),
                    tenantAccount.getOwner(), ACCOUNT_ENTRY_MARKER_INITIAL_TENANT, null);
        }
        return null;
    }

    private Pair<AccountEntry, AccountEntry> bookInitialValue(TenantAccount fromTenantAccount,
            PersonAccount toPersonAccount) {
        Person owner = toPersonAccount.getOwner();
        if (!existsAccountEntryForSubject(toPersonAccount, owner, ACCOUNT_ENTRY_MARKER_INITIAL_PERSON)) {
            return bookScore(fromTenantAccount, toPersonAccount, "Startkontingent an DigiTalern für die Testphase",
                    scoreConfig.getInitialAmountPersonAccounts(),
                    owner, ACCOUNT_ENTRY_MARKER_INITIAL_PERSON, null);
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Pair<AccountEntry, AccountEntry> bookInitialValue(PersonAccount toPersonAccount) {
        Person owner = toPersonAccount.getOwner();
        Tenant tenant = owner.getTenant();
        if (tenant == null) {
            log.error("Person {} does not belong to any tenant!", owner.getId());
            return null;
        }
        TenantAccount tenantAccount = tenantAccountService.findTenantAccount(AppAccountType.GENERAL, tenant);
        return bookInitialValue(tenantAccount, toPersonAccount);
    }

    @Override
    @Transactional
    public AccountEntry bookInitialScore(TenantAccount toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory) {

        if (amount == 0) {
            return null;
        }

        AccountEntryType toAccountEntryType = amount < 0 ? AccountEntryType.MINUS : AccountEntryType.PLUS;

        AccountEntry toAccountEntry = new AccountEntry();
        toAccountEntry.setAccount(toAccount);
        toAccountEntry.setPartner(null);
        toAccountEntry.setAccountEntryType(toAccountEntryType);
        toAccountEntry.setAmount(amount);
        toAccountEntry.setPostingText(postingText);
        toAccountEntry.setSubjectId(toSubjectId(subject));
        toAccountEntry.setSubjectName(toSubjectName(subject));
        toAccountEntry.setSubjectMarker(subjectMarker);
        toAccountEntry.setTimestamp(timeService.currentTimeMillisUTC());
        try {
            toAccountEntry = accountEntryRepository.save(toAccountEntry);
        } catch (ObjectRetrievalFailureException | EntityNotFoundException e) {
            throw new AccountNotFoundException("Account was not persisted", e);
        }

        accountEntryRepository.flush();

        toAccount.addToBalance(amount);
        save(toAccount);
        tenantAccountRepository.flush();

        notify(new ScoreChangedEvent(toAccount, toAccountEntry, pushCategory));
        return toAccountEntry;
    }

    @Override
    @Transactional
    public Pair<AccountEntry, AccountEntry> bookReservedScore(AccountEntry fromAccountEntry, AccountEntry toAccountEntry, PushCategory pushCategory) throws AccountCreditLimitExceededException {
        if(fromAccountEntry == null) {
            throw new AccountEntryNotFoundException("The requested account entry could not be found").withDetail(null);
        }
        if(toAccountEntry == null) {
            throw new AccountEntryNotFoundException("The requested account entry could not be found").withDetail(null);
        }

        Account fromAccount = fromAccountEntry.getAccount();
        if(fromAccount == null || fromAccountEntry.getPartner() == null){
            throw new AccountNotFoundException("Account is null");
        }

        Account toAccount = toAccountEntry.getAccount();
        if(toAccount == null || toAccountEntry.getPartner() == null){
            throw new AccountNotFoundException("Account is null");
        }

        int fromAmount = fromAccountEntry.getAmount();
        int toAmount = toAccountEntry.getAmount();

        checkReservationsMatch(fromAccountEntry, toAccountEntry);

        checkIsBookingWithinCreditLimitIgnoreReserved(fromAccount, fromAmount);
        checkIsBookingWithinCreditLimitIgnoreReserved(toAccount, toAmount);

        long timestamp = timeService.currentTimeMillisUTC();

        AccountEntryType fromAccountEntryType = fromAmount < 0 ? AccountEntryType.MINUS : AccountEntryType.PLUS;
        fromAccountEntry.setAccountEntryType(fromAccountEntryType);
        fromAccountEntry.setTimestamp(timestamp);
        try {
            fromAccountEntry = accountEntryRepository.save(fromAccountEntry);
        } catch (ObjectRetrievalFailureException | EntityNotFoundException e) {
            throw new AccountNotFoundException("Account was not persisted", e);
        }

        AccountEntryType toAccountEntryType = toAmount < 0 ? AccountEntryType.MINUS : AccountEntryType.PLUS;
        toAccountEntry.setAccountEntryType(toAccountEntryType);
        toAccountEntry.setTimestamp(timestamp);
        try {
            toAccountEntry = accountEntryRepository.save(toAccountEntry);
        } catch (ObjectRetrievalFailureException | EntityNotFoundException e) {
            throw new AccountNotFoundException("Account was not persisted", e);
        }

        fromAccount.addToBalance(fromAmount);
        toAccount.addToBalance(toAmount);

        save(fromAccount);
        save(toAccount);

        accountEntryRepository.flush();
        personAccountRepository.flush();
        tenantAccountRepository.flush();

        notify(new ScoreChangedEvent(fromAccount, fromAccountEntry, pushCategory));
        notify(new ScoreChangedEvent(toAccount, toAccountEntry, pushCategory));

        return Pair.of(fromAccountEntry, toAccountEntry);
    }

    @Override
    @Transactional
    public void freeReservedScore(AccountEntry fromAccountEntry, AccountEntry toAccountEntry, PushCategory pushCategory) {
        checkAccountEntry(fromAccountEntry);
        checkAccountEntry(toAccountEntry);
        checkReservationsMatch(fromAccountEntry, toAccountEntry);
        accountEntryRepository.delete(fromAccountEntry);
        accountEntryRepository.delete(toAccountEntry);
        accountEntryRepository.flush();
    }

    private void checkAccountEntry(AccountEntry accountEntry){
        if(accountEntry == null) {
            throw new AccountEntryNotFoundException("The requested account entry was null").withDetail(null);
        }
        if(accountEntry.getAccount() == null) {
            throw new AccountNotFoundException("The account entry has no account: {}", accountEntry).withDetail(null);
        }
        if(accountEntry.getPartner() == null) {
            throw new AccountNotFoundException("The account entry has no partner: {}", accountEntry).withDetail(null);
        }
    }

    @Override
    public AccountEntry findFirstAccountEntryForSubject(Account account, BaseEntity subject, String subjectMarker){
        return accountEntryRepository.findFirstByAccountAndSubjectIdAndSubjectNameAndSubjectMarkerOrderByCreatedAsc(account,
            toSubjectId(subject), toSubjectName(subject), subjectMarker);
    }

    @Override
    public boolean existsAccountEntryForSubject(Account account, BaseEntity subject, String subjectMarker) {
        return accountEntryRepository.countByAccountAndSubjectIdAndSubjectNameAndSubjectMarker(account,
            toSubjectId(subject), toSubjectName(subject), subjectMarker) > 0L;
    }

    @Override
    public int getReservedMinusAmountForAccount(Account account) {
        List<AccountEntry> reservedAccountEntries = accountEntryRepository.findByAccountAndAccountEntryTypeOrderByCreatedAsc(account, AccountEntryType.RESERVATION_MINUS);
        if(reservedAccountEntries.isEmpty()) {
            return 0;
        }
        return reservedAccountEntries.stream().mapToInt(AccountEntry::getAmount).sum();
    }

    @Override
    public Account getAccountById(String accountId) {
        if(accountId != null){
            Optional<? extends Account> account = personAccountRepository.findById(accountId);
            if(account.isPresent()) {
                return account.get();
            }
            account = tenantAccountRepository.findById(accountId);
            if(account.isPresent()) {
                return account.get();
            }
        }
        throw new AccountNotFoundException("The requested account could not be found.").withDetail(accountId);
    }

    @Override
    public Account save(Account account) {
        if (account instanceof PersonAccount) {
            return personAccountRepository.save((PersonAccount) account);
        }
        if (account instanceof TenantAccount) {
            return tenantAccountRepository.save((TenantAccount) account);
        }
        return null;
    }

    @Override
    public AccountEntry getLatestAccountEntryForAccount(Account account) {
        return accountEntryRepository.findFirstByAccountOrderByCreatedDesc(account);
    }

    @Override
    public int sumOfAllAccountEntriesCreatedInBetween(AccountEntryType type, Account account, long after, long before) {
        return accountEntryRepository.sumOfAbsAmountForTypeAndAccountCreatedAfterAndCreatedBefore(type, account, after, before).orElse(0L).intValue();
    }

    @Override
    public List<AccountEntry> getAccountEntriesForAccountByType(Account account, AccountEntryType type) {
        return accountEntryRepository.findByAccountAndAccountEntryTypeOrderByCreatedAsc(account, type);
    }

    private Pair<AccountEntry, AccountEntry> internalBookOrReserveScore(Account fromAccount, Account toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory, boolean isReservation) throws AccountCreditLimitExceededException {

        if(fromAccount == null){
            throw new AccountNotFoundException("From account is null");
        }

        if(toAccount == null){
            throw new AccountNotFoundException("To account is null");
        }

        if(amount == 0){
            //we do not book empty transactions
            return Pair.of(null, null);
        }

        if(amount < 0){
            //we do not book negative amounts, we just swap fromAccount and toAccount
            Account swapFromAccount = fromAccount;
            fromAccount = toAccount;
            toAccount = swapFromAccount;
            amount = -amount;
        }

        //we are removing the amount from fromAccount and adding it to toAccount
        checkIsBookingWithinCreditLimit(fromAccount, -amount);

        long timestamp = timeService.currentTimeMillisUTC();

        AccountEntry fromAccountEntry = new AccountEntry();
        fromAccountEntry.setAccount(fromAccount);
        fromAccountEntry.setPartner(toAccount);
        fromAccountEntry.setAccountEntryType(isReservation ? AccountEntryType.RESERVATION_MINUS : AccountEntryType.MINUS);
        fromAccountEntry.setAmount(-amount);
        fromAccountEntry.setPostingText(postingText);
        fromAccountEntry.setSubjectId(toSubjectId(subject));
        fromAccountEntry.setSubjectName(toSubjectName(subject));
        fromAccountEntry.setSubjectMarker(subjectMarker);
        fromAccountEntry.setTimestamp(timestamp);
        try {
            fromAccountEntry = accountEntryRepository.save(fromAccountEntry);
        } catch (ObjectRetrievalFailureException | EntityNotFoundException e) {
            throw new AccountNotFoundException("Account was not persisted", e);
        }

        AccountEntry toAccountEntry = new AccountEntry();
        toAccountEntry.setAccount(toAccount);
        toAccountEntry.setPartner(fromAccount);
        toAccountEntry.setAccountEntryType(isReservation ? AccountEntryType.RESERVATION_PLUS : AccountEntryType.PLUS);
        toAccountEntry.setAmount(amount);
        toAccountEntry.setPostingText(postingText);
        toAccountEntry.setSubjectId(toSubjectId(subject));
        toAccountEntry.setSubjectName(toSubjectName(subject));
        toAccountEntry.setSubjectMarker(subjectMarker);
        toAccountEntry.setTimestamp(timestamp);
        try {
            toAccountEntry = accountEntryRepository.save(toAccountEntry);
        } catch (ObjectRetrievalFailureException | EntityNotFoundException e) {
            throw new AccountNotFoundException("Account was not persisted", e);
        }

        accountEntryRepository.flush();

        if(!isReservation){
            fromAccount.addToBalance(-amount);
            toAccount.addToBalance(amount);
            save(fromAccount);
            save(toAccount);
            personAccountRepository.flush();
            tenantAccountRepository.flush();
        }

        //also notify for reservations, since we need to push this silent to the clients
        notify(new ScoreChangedEvent(fromAccount, fromAccountEntry, pushCategory));
        notify(new ScoreChangedEvent(toAccount, toAccountEntry, pushCategory));

        return Pair.of(fromAccountEntry, toAccountEntry);
    }

    private void checkReservationsMatch(AccountEntry fromAccountEntry, AccountEntry toAccountEntry) {
        Account fromAccount = fromAccountEntry.getAccount();
        Account toAccount = toAccountEntry.getAccount();
        int fromAmount = fromAccountEntry.getAmount();
        int toAmount = toAccountEntry.getAmount();

        if(fromAccountEntry.getAccountEntryType() != AccountEntryType.RESERVATION_PLUS &&
           fromAccountEntry.getAccountEntryType() != AccountEntryType.RESERVATION_MINUS){
            throw new AccountEntryAlreadyBookedException("The account entry "+fromAccountEntry.getId()+" is already booked.").withDetail(fromAccountEntry.getId());
        }
        if(toAccountEntry.getAccountEntryType() != AccountEntryType.RESERVATION_PLUS &&
           toAccountEntry.getAccountEntryType() != AccountEntryType.RESERVATION_MINUS){
            throw new AccountEntryAlreadyBookedException("The account entry "+toAccountEntry.getId()+" is already booked.").withDetail(toAccountEntry.getId());
        }
        if(fromAccountEntry.getAccountEntryType() == toAccountEntry.getAccountEntryType()) {
            throw new ReservationsNotMatchingException("Reservations do not match: fromAccountEntry.partner "+fromAccountEntry.getPartner().getId()+" toAccountEntry.account "+toAccount.getId());
        }
        if(!(Objects.equals(fromAccountEntry.getPartner(), toAccount))){
            throw new ReservationsNotMatchingException("Reservations do not match: fromAccountEntry.partner "+fromAccountEntry.getPartner().getId()+" toAccountEntry.account "+toAccount.getId());
        }
        if(!(Objects.equals(toAccountEntry.getPartner(), fromAccount))){
            throw new ReservationsNotMatchingException("Reservations do not match: toAccountEntry.partner "+toAccountEntry.getPartner().getId()+" fromAccountEntry.account "+fromAccount.getId());
        }
        if((fromAmount + toAmount) != 0){
            throw new ReservationsNotMatchingException("Reservations do not match: fromAccountEntry.amount "+fromAmount+" + toAccountEntry.amount "+toAmount+" != 0");
        }
    }

    private boolean checkIsBookingWithinCreditLimit(Account account, int bookingAmount) throws AccountCreditLimitExceededException {
        int bookingAmountWithReserved = bookingAmount + getReservedMinusAmountForAccount(account);
        return checkIsBookingWithinCreditLimitIgnoreReserved(account, bookingAmountWithReserved);
    }

    private boolean checkIsBookingWithinCreditLimitIgnoreReserved(Account account, int bookingAmount) throws AccountCreditLimitExceededException {
        int newBalance = account.getBalance() + bookingAmount;
        int creditLimit = scoreConfig.getGlobalCreditLimit();
        if(newBalance < creditLimit) {
            throw new AccountCreditLimitExceededException("Not enough points (new balance = "+newBalance+" <  credit limit = "+creditLimit+") for the booking").withDetail(account.getId());
        }
        return true;
    }

    private String toSubjectName(BaseEntity entity){
        return entity == null ? null : entity.getClass().getName();
    }

    private String toSubjectId(BaseEntity entity){
        return entity == null ? null : entity.getId();
    }

    @Override
    @Transactional
    public int recalculateAllAccountBalances(boolean saveChanges) {
        int absDifference = 0;
        int difference = 0;
        int count = 0;
        List<Account> accounts = new ArrayList<>();
        accounts.addAll(personAccountRepository.findAll());
        accounts.addAll(tenantAccountRepository.findAll());
        for(Account account : accounts){
            int diff = recalculateAccountBalance(account, saveChanges);
            if(diff!=0){
                absDifference += Math.abs(diff);
                difference += diff;
                count++;
            }
        }

        if(count > 0){
            log.warn("Balance inconsistencies: absolute {}, overall {}, count {}", absDifference, difference, count);
        }

        return absDifference;
    }

    @Override
    @Transactional
    public int recalculateAllAccountBalances(Tenant tenant, boolean saveChanges) {
        int absDifference = 0;
        int difference = 0;
        int count = 0;
        List<Account> accounts = new ArrayList<>();
        accounts.addAll(personAccountRepository.findAllByOwnerCommunityOrderByCreatedDesc(tenant));
        accounts.addAll(tenantAccountRepository.findAllByOwnerOrderByCreatedDesc(tenant));
        for (Account account : accounts) {
            int diff = recalculateAccountBalance(account, saveChanges);
            if (diff != 0) {
                absDifference += Math.abs(diff);
                difference += diff;
                count++;
            }
        }

        if (count > 0) {
            log.warn("Balance inconsistencies: absolute {}, overall {}, count {}", absDifference, difference, count);
        }

        return absDifference;
    }

    @Override
    @Transactional
    public int recalculateAccountBalance(Account account, boolean saveChanges){
        int difference = 0;
        List<AccountEntry> entries = accountEntryRepository.findByAccountOrderByCreatedAsc(account);
        if(entries.isEmpty() && account.getBalance() != 0){
            difference = Math.abs(account.getBalance());
            log.error("Account {} has a wrong balance: {} without any entries", account.getId(), account.getBalance());
            if(saveChanges){
                account.setBalance(0);
                save(account);
            }
            return difference;
        }

        Optional<Integer> calculatedScore = entries.stream()
                .filter(e -> e.getAccountEntryType().equals(AccountEntryType.PLUS) ||
                        e.getAccountEntryType().equals(AccountEntryType.MINUS))
                .map(AccountEntry::getAmount)
                .reduce(Integer::sum);

        if(calculatedScore.isPresent()){
            if(calculatedScore.get()!=account.getBalance()){
                difference = account.getBalance()-calculatedScore.get();
                log.warn("Account {} has a wrong balance: calculated {} vs stored {} -> difference {}", account.getId(), calculatedScore.get(), account.getBalance(), difference);
                if(saveChanges){
                    account.setBalance(calculatedScore.get());
                    save(account);
                }
                return difference;
            }
        } else {
            log.warn("For account {} the calculation of the balance failed", account.getId());
        }

        return 0;
    }

    @Override
    public AccountEntry save(AccountEntry entry) {
        return accountEntryRepository.save(entry);
    }

    @Override
    public List<AccountEntry> getAccountEntriesForAccount(Account account){
        return accountEntryRepository.findByAccountOrderByCreatedAsc(account);
    }

    @Override
    public List<AccountEntry> getAccountEntriesForPartner(Account partner) {
        return accountEntryRepository.findByPartnerOrderByCreatedAsc(partner);
    }

}
