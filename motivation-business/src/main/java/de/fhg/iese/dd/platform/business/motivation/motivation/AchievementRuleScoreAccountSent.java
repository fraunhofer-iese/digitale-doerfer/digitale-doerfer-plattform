/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.motivation;

import de.fhg.iese.dd.platform.business.motivation.events.ScoreChangedEvent;
import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.*;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@AchievementRule
public class AchievementRuleScoreAccountSent extends BaseAchievementRule<ScoreChangedEvent> {

    private static final String ACHIEVEMENT_ID          = "3640c605-3d26-4623-93ee-de7b4e4fd1a6";
    private static final String ACHIEVEMENT_LEVEL_ID_10 = "8719328d-62d1-4ad7-a856-1d8cbcd74015";

    @Autowired
    private IAccountService accountService;
    @Autowired
    private IPersonAccountService personAccountService;

    @Override
    public String getName() {
        return "score.account.sent.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(ScoreChangedEvent event) {
        Account account = event.getAccount();
        if (!(account instanceof PersonAccount personAccount)) {
            return null;
        }
        AccountEntry accountEntry = event.getAccountEntry();
        if(accountEntry == null || accountEntry.getAccountEntryType() != AccountEntryType.MINUS) {
            return null;
        }

        long timestamp = accountEntry.getCreated();

        long oneWeekBeforeTimeStamp = Instant.ofEpochMilli(timestamp).minus(Duration.ofDays(7L)).toEpochMilli();
        int sum = accountService.sumOfAllAccountEntriesCreatedInBetween(AccountEntryType.MINUS, account, oneWeekBeforeTimeStamp, timestamp);

        if(sum+Math.abs(accountEntry.getAmount()) == 10) {
            return Collections.singletonList(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, personAccount.getOwner(), timestamp));
        }

        return null;
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        PersonAccount personAccount = personAccountService.getAccountForOwner(person);
        List<AccountEntry> accountEntries = accountService.getAccountEntriesForAccountByType(personAccount, AccountEntryType.MINUS);
        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();
        for(AccountEntry accountEntry: accountEntries) {
            long timestamp = accountEntry.getCreated();

            long oneWeekBeforeTimeStamp = Instant.ofEpochMilli(timestamp).minus(Duration.ofDays(7L)).toEpochMilli();
            int sum = accountService.sumOfAllAccountEntriesCreatedInBetween(AccountEntryType.MINUS, personAccount, oneWeekBeforeTimeStamp, timestamp);

            if(sum+Math.abs(accountEntry.getAmount()) == 10) {
                achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, personAccount.getOwner(), timestamp));
                break;
            }
        }
        return achievedLevels;
    }

    @Override
    public Class<ScoreChangedEvent> getRelevantEvent() {
        return ScoreChangedEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Score.Account.Sent")
                .description("Gebe Digitaler aus um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel10 = AchievementLevel.builder()
                .name("Rockefeller")
                .description("Du haust die Digitaler aber ganz schön raus - Gönn dir!")
                .challengeDescription("Gib 10 DigiTaler in einer Woche aus.")
                .icon(createIconFromDefault("rockefeller.png"))
                .iconNotAchieved(createIconFromDefault("rockefeller_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel10.setId(ACHIEVEMENT_LEVEL_ID_10);
        achievementLevel10 = achievementService.save(achievementLevel10);

        achievement.setAchievementLevels(Collections.singleton(achievementLevel10));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_10);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
