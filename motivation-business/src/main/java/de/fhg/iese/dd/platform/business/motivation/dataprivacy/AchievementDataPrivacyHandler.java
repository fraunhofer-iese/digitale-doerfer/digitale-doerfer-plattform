/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.dataprivacy;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.motivation.services.IAchievementService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Component
public class AchievementDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);
    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Achievement.class);

    @Autowired
    private IAchievementService achievementService;

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IMotivationDataDeletionService motivationDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        final List<AchievementPersonPairing> achievedLevels = achievementService.getAchievedLevels(person);
        if (CollectionUtils.isEmpty(achievedLevels)) {
            return;
        }
        Collections.reverse(achievedLevels);

        IDataPrivacyReport.IDataPrivacyReportSection section = dataPrivacyReport.newSection("Auszeichnungen",
                "erhaltene Auszeichnungen");

        for (AchievementPersonPairing achievedLevel : achievedLevels) {
            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Auszeichnung", achievedLevel.getAchievementLevel().getName());
            //achievedLevel.getAchievementLevel().getIcon() does not belong to the person, hence it is not listed here
            subSection.newContent("Erhalten am", timeService.toLocalTimeHumanReadable(achievedLevel.getTimestamp()));
            subSection.newContent("Beschreibung", achievedLevel.getAchievementLevel().getChallengeDescription());
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        final List<AchievementPersonPairing> achievedLevels = achievementService.getAchievedLevels(person);
        for (AchievementPersonPairing achievedLevel : achievedLevels) {
            personDeletionReport.addEntry(achievedLevel,
                    motivationDataDeletionService.deleteAchievementPersonPairing(achievedLevel));
        }
    }
    
}
