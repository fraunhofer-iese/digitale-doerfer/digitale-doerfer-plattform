/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.motivation;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonLoggedInEvent;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleParticipantsRegistered extends BaseAchievementRule<PersonLoggedInEvent> {

    private static final String ACHIEVEMENT_ID          = "be291424-de4b-4cf1-92af-e27e35603131";

    /**
     * Registered 1 Week
     */
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "90923bee-5624-4fb2-b891-90d4785c26cf";
    /**
     * Registered 1 Month
     */
    private static final String ACHIEVEMENT_LEVEL_ID_02 = "13a3eb68-6ea2-491d-a806-e0cacc9b35b3";
    /**
     * Registered 6 Months
     */
    public  static final String ACHIEVEMENT_LEVEL_ID_03 = "b95b588e-1653-4ce1-9daa-928b91440ae2";
    /**
     * Registered 1 Year
     */
    private static final String ACHIEVEMENT_LEVEL_ID_04 = "5988a059-bf0c-48fa-8feb-fa17ba47914a";

    private static final Set<String> achievementLevelIds = Stream.of(
            ACHIEVEMENT_LEVEL_ID_01,
            ACHIEVEMENT_LEVEL_ID_02,
            ACHIEVEMENT_LEVEL_ID_03,
            ACHIEVEMENT_LEVEL_ID_04)
            .collect(Collectors.toSet());

    @Override
    public String getName() {
        return "score.participants.registered.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(PersonLoggedInEvent event) {
        return checkAchievement(event.getPerson());
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(Person person) {

        long lastLoggedIn = person.getLastLoggedIn();

        Set<String> achievedLevels = achievementService.getAchievedLevels(person, achievementLevelIds).stream()
                .map(a -> a.getAchievementLevel().getId())
                .collect(Collectors.toSet());

        List<AchievementPersonPairing> newAchievedLevels = new ArrayList<>();

        ZonedDateTime personCreated = timeService.toLocalTime(person.getCreated());
        ZonedDateTime currentTime = timeService.nowLocal();
        long ageOfUser = currentTime.toEpochSecond() - personCreated.toEpochSecond();

        if (ageOfUser >= daysToSeconds(7) && !achievedLevels.contains(ACHIEVEMENT_LEVEL_ID_01)) {
            newAchievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, lastLoggedIn));
        }
        if (ageOfUser >= daysToSeconds(30) && !achievedLevels.contains(ACHIEVEMENT_LEVEL_ID_02)) {
            newAchievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_02, person, lastLoggedIn));
        }
        if (ageOfUser >= daysToSeconds(30*6) && !achievedLevels.contains(ACHIEVEMENT_LEVEL_ID_03)) {
            newAchievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_03, person, lastLoggedIn));
        }
        if (ageOfUser >= daysToSeconds(365) && !achievedLevels.contains(ACHIEVEMENT_LEVEL_ID_04)) {
            newAchievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_04, person, lastLoggedIn));
        }
        return newAchievedLevels;
    }

    private long daysToSeconds(int days){
        return TimeUnit.SECONDS.convert(days, TimeUnit.DAYS);
    }

    @Override
    public Class<PersonLoggedInEvent> getRelevantEvent() {
        return PersonLoggedInEvent.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Participant.Registered")
                .description("Melde ich an um Awards zu bekommen")
                //we can not define a push category, instead all categories with subject MotivationConstants.PUSH_CATEGORY_SUBJECT are used
                .pushCategory(null)
                .category("Allgemein")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Erste Schritte")
                .description("Ein kleiner Schritt für dich, aber ein großer Schritt hin zum digitalen Dorf.")
                .challengeDescription("Sei seit einer Woche angemeldet.")
                .icon(createIconFromDefault("erste_schritte.png"))
                .iconNotAchieved(createIconFromDefault("erste_schritte_grey.png"))
                .achievement(achievement)
                .orderValue(2)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        AchievementLevel achievementLevel02 = AchievementLevel.builder()
                .name("Entdecker")
                .description("Yay! Einen Monat hier! Schon viel gesehen und es gibt noch viel mehr zu entdecken!")
                .challengeDescription("Sei seit einem Monat angemeldet.")
                .icon(createIconFromDefault("entdecker.png"))
                .iconNotAchieved(createIconFromDefault("entdecker_grey.png"))
                .achievement(achievement)
                .orderValue(3)
                .build();
        achievementLevel02.setId(ACHIEVEMENT_LEVEL_ID_02);
        achievementLevel02 = achievementService.save(achievementLevel02);

        AchievementLevel achievementLevel03 = AchievementLevel.builder()
                .name("Superstar")
                .description("Sechs Monate im Digitalen Dorf - Man kennt dich! Angeblich formiert sich gerade ein Fanclub.")
                .challengeDescription("Sei seit sechs Monaten angemeldet.")
                .icon(createIconFromDefault("superstar.png"))
                .iconNotAchieved(createIconFromDefault("superstar_grey.png"))
                .achievement(achievement)
                .orderValue(4)
                .build();
        achievementLevel03.setId(ACHIEVEMENT_LEVEL_ID_03);
        achievementLevel03 = achievementService.save(achievementLevel03);

        AchievementLevel achievementLevel04 = AchievementLevel.builder()
                .name("Mumie")
                .description("Herzlich Willkommen im Ältestenrat!")
                .challengeDescription("Sei seit einem Jahr angemeldet.")
                .icon(createIconFromDefault("mumie.png"))
                .iconNotAchieved(createIconFromDefault("mumie_grey.png"))
                .achievement(achievement)
                .orderValue(5)
                .build();
        achievementLevel04.setId(ACHIEVEMENT_LEVEL_ID_04);
        achievementLevel04 = achievementService.save(achievementLevel04);

        achievement.setAchievementLevels(
                Stream.of(achievementLevel01, achievementLevel02, achievementLevel03, achievementLevel04)
                        .collect(Collectors.toSet()));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_02);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_03);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_04);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
