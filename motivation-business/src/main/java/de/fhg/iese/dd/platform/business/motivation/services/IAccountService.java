/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.services;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountCreditLimitExceededException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

public interface IAccountService extends IService {

    /**
     * Reserves score from one account to another. A check is done on the
     * account that gets charged if the credit limit would be exceeded.
     *
     * @param fromAccount
     *            start account for the booking
     * @param toAccount
     *            target account for the booking
     * @param postingText
     *            message to be used for the account entries
     * @param amount
     *            <li>Positive: |amount| (unsigned value) gets removed from the
     *            fromAccount and added to the toAccount</li>
     *            <li>Negative: |amount| (unsigned value) gets removed from the
     *            toAccount and added to the fromAccount</li>
     *            <li>0: No booking takes places, returns (null, null)</li>
     * @param subject
     *            entity that is related to the booking that is done
     * @param subjectMarker
     *            marker to differentiate different bookings that were done for
     *            the same subject
     * @param pushCategory
     *            push category that will be used for push notifications
     * @return Pair of AccountEntries with
     *         <li>Left: AccountEntry fromAccount</li>
     *         <li>Right: AccountEntry toAccount</li>
     *         <li>in case of amount = 0: No booking takes places, returns
     *         (null, null)</li>
     * @throws AccountCreditLimitExceededException
     *             If the Account that gets charged has insufficient scores and
     *             the reservation can not be done
     */
    Pair<AccountEntry, AccountEntry> reserveScore(Account fromAccount, Account toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory) throws AccountCreditLimitExceededException;

    /**
     * Actually book the pair of account entries and add or remove the amount
     * from the according accounts. If the two entries do not match the booking
     * is done, but a warning is logged. Matching means that the according
     * partner account and the amount is identical.
     *
     * @param fromAccountEntry
     *            first account entry that has to be in state reservation
     * @param toAccountEntry
     *            second account entry that has to be in state reservation
     * @param pushCategory
     *            push category that will be used for push notifications
     * @return Pair of AccountEntries with
     *         <li>Left: AccountEntry fromAccountEntry</li>
     *         <li>Right: AccountEntry toAccountEntry</li>
     * @throws AccountCreditLimitExceededException
     */
    Pair<AccountEntry, AccountEntry> bookReservedScore(AccountEntry fromAccountEntry, AccountEntry toAccountEntry, PushCategory pushCategory) throws AccountCreditLimitExceededException;

    /**
     * Books score from one account to another. A check is done on the account
     * that gets charged if the credit limit would be exceeded.
     *
     * @param fromAccount
     *            start account for the booking
     * @param toAccount
     *            target account for the booking
     * @param postingText
     *            message to be used for the account entries
     * @param amount
     *            <li>Positive: |amount| (unsigned value) gets removed from the
     *            fromAccount and added to the toAccount</li>
     *            <li>Negative: |amount| (unsigned value) gets removed from the
     *            toAccount and added to the toAccount</li>
     *            <li>0: No booking takes places, returns (null, null)</li>
     * @param subject
     *            entity that is related to the booking that is done
     * @param subjectMarker
     *            marker to differentiate different bookings that were done for
     *            the same subject
     * @param pushCategory
     *            push category that will be used for push notifications
     * @return Pair of AccountEntries with
     *         <li>Left: AccountEntry fromAccount</li>
     *         <li>Right: AccountEntry toAccount</li>
     *         <li>in case of amount = 0: No booking takes places, returns (null, null)</li>
     * @throws AccountCreditLimitExceededException
     *             If the Account that gets charged has insufficient scores and
     *             the booking can not be done
     */
    Pair<AccountEntry, AccountEntry> bookScore(Account fromAccount, Account toAccount, String postingText, int amount,
            BaseEntity subject, String subjectMarker, PushCategory pushCategory) throws AccountCreditLimitExceededException;

    /**
     * Removes the two reservations from the account.
     *
     * @param fromAccountEntry
     *            first account entry that has to be in state reservation
     * @param toAccountEntry
     *            second account entry that has to be in state reservation
     * @param pushCategory
     *            push category that will be used for push notifications
     */
    void freeReservedScore(AccountEntry fromAccountEntry, AccountEntry toAccountEntry, PushCategory pushCategory);

    /**
     * Get the sum of all minus reservations on the account.
     *
     * @param account
     *            the account to be checked
     * @return sum of amount all account entries with type
     *         {@link AccountEntryType#RESERVATION_MINUS}
     */
    int getReservedMinusAmountForAccount(Account account);

    /**
     * Finds an account by id, depending on the concrete type the {@link TenantAccountRepository} or {@link
     * PersonAccountRepository} is used.
     *
     * @param accountId the id of the account to be looked up
     *
     * @return the {@link PersonAccount} or {@link TenantAccount} with that id or null if none exists
     */
    Account getAccountById(String accountId);

    /**
     * Persists an account, depending on the concrete type the
     * {@link TenantAccountRepository} or {@link PersonAccountRepository} is
     * used.
     *
     * @param account
     *            the account to be saved
     * @return the saved account
     */
    Account save(Account account);

    /**
     * Returns the last created account entry for the Account.
     *
     * @param account
     * @return The latest account entry
     */
    AccountEntry getLatestAccountEntryForAccount(Account account);

    /**
     * Returns the absolute sum of all account entries of the specified type,
     * created in between the specified timestamps.
     *
     * @param type
     *            AccountEntryType
     * @param account
     * @param after
     * @param before
     * @return
     */
    int sumOfAllAccountEntriesCreatedInBetween(AccountEntryType type, Account account, long after, long before);

    /**
     * Returns all account entries of the specified type.
     *
     * @param account
     * @param type
     * @return all account entries belonging to the account and of specified
     *         type
     */
    List<AccountEntry> getAccountEntriesForAccountByType(Account account, AccountEntryType type);

    /**
     * Returns the first account entry in that account for the specified
     * subject.
     *
     * @param account
     *            the account to look for the account entry
     * @param subject
     *            entity that is related to the booking that is done
     * @param subjectMarker
     *            marker to differentiate different bookings that were done for
     *            the same subject
     * @return the first account entry in that account for the specified subject
     *
     */
    AccountEntry findFirstAccountEntryForSubject(Account account, BaseEntity subject, String subjectMarker);

    /**
     * Checks if an account entry in that account exists for the specified
     * subject.
     *
     * @param account
     *            the account to look for the account entry
     * @param subject
     *            entity that is related to the booking that is done
     * @param subjectMarker
     *            marker to differentiate different bookings that were done for
     *            the same subject
     * @return true if there is an account entry in that account for the
     *         specified subject.
     */
    boolean existsAccountEntryForSubject(Account account, BaseEntity subject, String subjectMarker);

    /**
     * Book initial score to a TenantAccount. This booking will not have a
     * partner account entry, it is the source of scores.
     *
     * @param toAccount
     *            target account for the booking
     * @param postingText
     *            message to be used for the account entry
     * @param amount
     *            <li>Positive: |amount| (unsigned value) gets added to the
     *            toAccount</li>
     *            <li>Negative: |amount| (unsigned value) gets removed from the
     *            toAccount</li>
     *            <li>0: No booking takes places, returns null</li>
     * @param subject
     *            entity that is related to the booking that is done
     * @param subjectMarker
     *            marker to differentiate different bookings that were done for
     *            the same subject
     * @param pushCategory
     *            push category that will be used for push notifications
     * @return the account entry of the initial booking. It will not have a
     *         partner!
     *         <p/>
     *         <b>or</b> <code>null</code> if the booking was already done
     */
    AccountEntry bookInitialScore(TenantAccount toAccount, String postingText, int amount, BaseEntity subject,
            String subjectMarker, PushCategory pushCategory);

    /**
     * Book the initial scores to a tenant account, according to the score
     * configuration.
     *
     * @param toTenantAccount
     *            target account for the booking
     * @return the account entry of the initial booking. It will not have a
     *         partner!
     *         <p/>
     *         <b>or</b> <code>null</code> if the booking was already done
     */
    AccountEntry bookInitialValue(TenantAccount toTenantAccount);

    /**
     * Book the initial scores from a tenant account to a person account,
     * according to the score configuration.
     *
     * @param toPersonAccount
     *            target account for the booking
     * @return Pair of AccountEntries with
     *         <li>Left: AccountEntry fromAccount</li>
     *         <li>Right: AccountEntry toAccount</li>
     *         <p/>
     *         <b>or</b> <code>null</code> if the booking was already done
     */
    Pair<AccountEntry, AccountEntry> bookInitialValue(PersonAccount toPersonAccount);

    /**
     * Recalculates all account balances based on all account entries
     *
     * @param saveChanges
     *            if false no changes are done, only logging takes place
     * @return
     */
    int recalculateAllAccountBalances(boolean saveChanges);
    int recalculateAccountBalance(Account account, boolean saveChanges);

    int recalculateAllAccountBalances(Tenant tenant, boolean saveChanges);

    AccountEntry save(AccountEntry entry);

    List<AccountEntry> getAccountEntriesForAccount(Account account);

    /**
     * Returns all account entries where the given account is the partner account
     *
     * @param partner
     * @return
     */
    List<AccountEntry> getAccountEntriesForPartner(Account partner);

}
