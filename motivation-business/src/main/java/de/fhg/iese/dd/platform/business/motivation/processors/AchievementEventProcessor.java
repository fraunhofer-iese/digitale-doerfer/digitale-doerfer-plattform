/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.processors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionExceptionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.CheckAchievementAchievedRequest;
import de.fhg.iese.dd.platform.business.motivation.events.MultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.framework.IAchievementRule;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor
public class AchievementEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPersonService personService;

    @Autowired
    private List<IAchievementRule<?>> achievementRules;

    @Override
    protected void registerForEventsManually() {
        achievementRules.forEach(this::registerAchievementRule);
    }

    private <E extends BaseEvent> void registerAchievementRule(IAchievementRule<E> achievementRule){
        registerEventProcessingFunction(achievementRule.getRelevantEvent(),
                achievementRule::checkAchievementAndCreateEvent,
                EventExecutionStrategy.ASYNCHRONOUS_PREFERRED,
                EventExecutionExceptionStrategy.IGNORE_EXCEPTION,
                false);
        log.trace("Registering achievement rule {}", achievementRule.getName());
    }

    @EventProcessing
    private void handleCheckAchievementAchievedRequest(CheckAchievementAchievedRequest event,
            final EventProcessingContext context) {

        Collection<IAchievementRule<?>> achievementRulesToCheck;
        if (event.isCheckAllAchievementRules()) {
            achievementRulesToCheck = achievementRules;
        } else {
            if (CollectionUtils.isEmpty(event.getAchievementRulesToCheck())) {
                return;
            }
            //restrict the rules that should be checked
            achievementRulesToCheck = achievementRules
                    .stream()
                    .filter(ar -> event.getAchievementRulesToCheck().contains(ar.getName()))
                    .collect(Collectors.toList());
        }

        if (event.isCheckAllPersons()) {
            int pageNumber = 0;
            Page<Person> personPage;
            do {
                personPage = personService.findAllNotDeleted(PageRequest.of(pageNumber, 1000));
                for (Person person : personPage) {
                    checkAchievementsForPerson(context, achievementRulesToCheck, person);
                }
            } while (personPage.hasNext());
        } else {
            if (CollectionUtils.isEmpty(event.getPersonsToCheck())) {
                return;
            }
            for (Person person : event.getPersonsToCheck()) {
                checkAchievementsForPerson(context, achievementRulesToCheck, person);
            }
        }
    }

    private void checkAchievementsForPerson(EventProcessingContext context,
            Collection<IAchievementRule<?>> achievementRulesToCheck,
            Person personToCheck) {
        Collection<AchievementPersonPairing> achievedAchievementLevelsPerson = new ArrayList<>();
        for (IAchievementRule<?> achievementRule : achievementRulesToCheck) {
            try {
                Collection<AchievementPersonPairing> achievedAchievementLevelsRule =
                        achievementRule.checkAchievement(personToCheck);
                if (achievedAchievementLevelsRule != null && !achievedAchievementLevelsRule.isEmpty()) {
                    achievedAchievementLevelsPerson.addAll(achievedAchievementLevelsRule.stream()
                            .filter(Objects::nonNull)
                            .collect(Collectors.toList()));
                }
            } catch (Exception e) {
                log.error("Failed to check achievement rule " + achievementRule.getClass() + " for person " +
                        personToCheck.getId(), e);
            }
        }

        boolean isMultiAchievement = achievedAchievementLevelsPerson.size() > 1;

        achievedAchievementLevelsPerson
                .forEach(achievedAchievementLevel -> notify(
                        new AchievementAchievedEvent(achievedAchievementLevel, isMultiAchievement), context));
        //we need to fire achieved events for every newly achieved level
        if (isMultiAchievement) {
            notify(new MultiAchievementAchievedEvent(personToCheck, achievedAchievementLevelsPerson), context);
        }
    }

}
