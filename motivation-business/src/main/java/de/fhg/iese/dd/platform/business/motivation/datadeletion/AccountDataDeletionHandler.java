/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.ITenantAccountService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class AccountDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private IAccountService accountService;
    @Autowired
    private TenantAccountRepository tenantAccountRepository;
    @Autowired
    private AccountEntryRepository accountEntryRepository;
    @Autowired
    private ITenantAccountService tenantAccountService;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        //Person accounts reference persons
                        .referencedEntity(Person.class)
                        .changedOrDeletedEntity(TenantAccount.class)
                        .changedOrDeletedEntity(PersonAccount.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptySet();
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {

        int numAccounts = tenantAccountRepository.findAllByOwnerOrderByCreatedDesc(tenantToBeDeleted).size();
        int numSwapAccounts = tenantAccountRepository.findAllByOwnerOrderByCreatedDesc(tenantToBeUsedInstead).size();
        logSummary.info("{} tenant accounts will be deleted", numAccounts);
        logSummary.info("{} tenant accounts can be used instead, {} will be newly created", numSwapAccounts,
                numAccounts - numSwapAccounts);
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {

        List<TenantAccount> tenantAccountsToBeDeleted =
                tenantAccountRepository.findAllByOwnerOrderByCreatedDesc(tenantToBeDeleted);
        logSummary.info("{} tenant accounts will be deleted", tenantAccountsToBeDeleted.size());
        for (TenantAccount accountToBeDeleted : tenantAccountsToBeDeleted) {
            TenantAccount accountToBeUsedInstead = tenantAccountService.createTenantAccountIfNotExisting(
                    accountToBeDeleted.getAppAccountType(), tenantToBeUsedInstead);
            logSummary.info("{} is used as tenant account instead", accountToBeUsedInstead);
            //these are the account entries on the to be deleted tenant account
            List<AccountEntry> accountEntriesToBeChanged1 =
                    accountService.getAccountEntriesForAccount(accountToBeDeleted);
            logSummary.info("{} entries are mapped to new account", accountEntriesToBeChanged1.size());
            for (AccountEntry accountEntry : accountEntriesToBeChanged1) {
                accountEntry.setAccount(accountToBeUsedInstead);
                accountEntryRepository.save(accountEntry);
            }
            //these are the account entries of persons that got DigiTaler from the to be deleted tenant account
            List<AccountEntry> accountEntriesToBeChanged2 =
                    accountService.getAccountEntriesForPartner(accountToBeDeleted);
            logSummary.info("{} entries are mapped to new partner account", accountEntriesToBeChanged2.size());
            for (AccountEntry accountEntry : accountEntriesToBeChanged2) {
                accountEntry.setPartner(accountToBeUsedInstead);
                accountEntryRepository.save(accountEntry);
            }
            logSummary.info("{} is recalculated", accountToBeUsedInstead);
            accountService.recalculateAccountBalance(accountToBeUsedInstead, true);
            logSummary.info("{} is deleted", accountToBeDeleted);
            tenantAccountRepository.delete(accountToBeDeleted);
        }
    }

}
