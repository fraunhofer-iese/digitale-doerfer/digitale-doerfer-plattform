/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Trade;

public class PostRepositoryIntegrityTest {

    @Test
    public void allExternalPostSubTypesAreListed() throws Exception {
        assertThatSubClassesMatchStringConstant(ExternalPost.class, PostRepository.EXTERNAL_POST_SUBCLASSES);
    }

    @Test
    public void allTradeSubTypesAreListed() throws Exception {
        assertThatSubClassesMatchStringConstant(Trade.class, PostRepository.TRADE_SUBCLASSES);
    }

    private void assertThatSubClassesMatchStringConstant(Class<?> superClass, String subClassesAsString) {
        final Reflections reflections = new Reflections(superClass.getPackage().getName());
        final Set<String> subClassNames = reflections.getSubTypesOf(superClass)
                .stream()
                .map(Class::getSimpleName)
                .collect(Collectors.toSet());

        final Set<String> classNamesInPostRepository = Stream.of(
                subClassesAsString
                        .split(","))
                .map(s -> s.replace("'", "").trim())
                .collect(Collectors.toSet());

        assertEquals(subClassNames, classNamesInPostRepository);
    }

}
