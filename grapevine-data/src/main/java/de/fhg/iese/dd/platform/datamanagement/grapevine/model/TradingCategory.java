/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Adeline Silva Schäfer, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TradingCategory extends BaseEntity {

    @Column
    private String shortName;

    @Column
    private String displayName;

    @Column
    private int orderValue;

    public TradingCategory withOrderValue(int orderValue) {
        setOrderValue(orderValue);
        return this;
    }

    public TradingCategory withId(String id) {
        setId(id);
        return this;
    }

    @Override
    public String toString() {
        return "TradingCategory [" +
                "id='" + id + "', " +
                "shortName='" + shortName + "', " +
                "displayName='" + displayName + "', " +
                "orderValue='" + orderValue + "', " +
                ']';
    }

}
