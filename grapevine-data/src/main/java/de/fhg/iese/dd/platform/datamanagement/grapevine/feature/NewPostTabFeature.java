/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.feature;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonGetter;

import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.Feature;
import lombok.Getter;

@Getter
@SuppressWarnings("unused")
@Feature(featureIdentifier = "de.fhg.iese.dd.dorffunk.new-post-tab")
public class NewPostTabFeature extends BaseFeature {

    private String tabName;
    private String itemName;
    private int orderValue;

    @JsonGetter
    public String getItemName() {
        if (StringUtils.isEmpty(itemName)) {
            return tabName;
        } else {
            return itemName;
        }
    }

}
