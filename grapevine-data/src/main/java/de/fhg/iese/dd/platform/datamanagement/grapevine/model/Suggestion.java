/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Suggestion extends Post {

    @Enumerated(EnumType.STRING)
    private SuggestionStatus suggestionStatus;

    @ManyToOne
    private SuggestionCategory suggestionCategory;

    /**
     * Overrides the deleted status if a person with one of the roles {@link de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder}
     * or {@link de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker} for the tenant of the
     * suggestion requests the suggestion.
     * <p>
     * The following formula describes the visibility of Suggestion s for Person p:
     * <pre>
     * if(deleted) {
     *      if (p has Role SuggestionFirstResponder or SuggestionWorker for tenant s.tenant) {
     *          return overrideDeletedForSuggestionRoles;
     *      } else {
     *          return false
     *      }
     * } else {
     *     return true
     * }
     * </pre>
     * </ul>
     */
    @Column(nullable = false)
    @ColumnDefault("false")
    @Builder.Default
    private boolean overrideDeletedForSuggestionRoles = false;

    /**
     * Read-only list of suggestion workers, sorted by creation date ascending (oldest first).
     * If workers should be added, use the according ISuggestionService.
     */
    @JsonIgnore
    @Transient
    private List<SuggestionWorkerMapping> suggestionWorkerMappings;

    /**
     * Read-only list of suggestion status records, sorted by creation date descending (newest first).
     * If status should be added, use the according ISuggestionService.
     */
    @JsonIgnore
    @Transient
    private List<SuggestionStatusRecord> suggestionStatusRecords;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String withdrawReason;

    @Column
    @ColumnDefault("0")
    private long withdrawn;

    @Column
    @ColumnDefault("0")
    private long lastSuggestionActivity;

    @ManyToOne
    private Chat internalWorkerChat;

    @Column(nullable = false)
    @ColumnDefault("false")
    @Builder.Default
    private boolean internal = false;

    public PostType getPostType() {
        return PostType.Suggestion;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.whenSuggestion(this);
    }

    @Override
    public String toString() {
        return "Suggestion [" +
                "id='" + id + "', " +
                "creator='" + this.getCreator() + "', " +
                "text='" + this.getText() + "', " +
                "suggestionCategory='" + (suggestionCategory != null ? suggestionCategory.getShortName() : "null") + "', " +
                "tenant='" + getTenant() + "', " +
                "geoAreas='" + getGeoAreas() + "'" +
                ']';
    }

}
