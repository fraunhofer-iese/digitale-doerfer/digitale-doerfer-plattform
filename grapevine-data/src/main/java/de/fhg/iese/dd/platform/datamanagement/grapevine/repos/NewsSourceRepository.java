/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Adeline Silva Schäfer, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NewsSourceRepository extends JpaRepository<NewsSource, String> {

    @Query("select ns from NewsSource ns where ns.id = :id")
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD, attributePaths = {"appVariant", "siteLogo"})
    NewsSource findByIdFull(String id);

    Optional<NewsSource> findByIdAndDeletedFalse(String id);

    Page<NewsSource> findAllByDeletedFalse(Pageable pageable);

    Optional<NewsSource> findBySiteUrlAndDeletedFalse(String newsSiteUrl);

    Optional<NewsSource> findBySiteUrl(String newsSiteUrl);

    Optional<NewsSource> findByAppVariant(AppVariant appVariant);

    Optional<NewsSource> findByAppVariantAndDeletedFalse(AppVariant appVariant);

    @Query("select ns.id from NewsSource ns where ns.apiManaged = true")
    List<String> getIdsByApiManagedTrue();

}
