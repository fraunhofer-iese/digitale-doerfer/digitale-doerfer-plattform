/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupIdMetaData;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public interface GroupGeoAreaMappingRepository extends JpaRepository<GroupGeoAreaMapping, String> {

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupIdMetaData( ggam.group.id, ggam.geoArea.id, ggam.excluded, ggam.mainGeoArea ) " +
            "from GroupGeoAreaMapping ggam " +
            "where ggam.group in :groups")
    List<GroupIdMetaData> findAllGeoAreaIdsByGroupIds(Set<Group> groups);

    List<GroupGeoAreaMapping> findAllByGroupOrderByCreatedDesc(Group group);

    long countByGeoArea(GeoArea geoArea);

    @Query("select gA " +
            "from GroupGeoAreaMapping gA " +
            "where gA.geoArea = :geoAreaA " +
            "and not exists " +
            "   (select 1 from GroupGeoAreaMapping gB " +
            "   where gB.group = gA.group " +
            "   and gB.geoArea = :geoAreaB) ")
    Set<GroupGeoAreaMapping> findMappingsToGeoAreaAAndNotToGeoAreaB(GeoArea geoAreaA, GeoArea geoAreaB);

    @Transactional
    @Modifying
    int deleteByGeoArea(GeoArea geoArea);

    @Transactional
    @Modifying
    void deleteByGroup(Group group);

    @Transactional
    @Modifying
    @Query("delete from GroupGeoAreaMapping gA where gA.group = :group and gA.id not in :mappingIdsToKeep")
    void deleteByGroupAndIdNotIn(Group group, Set<String> mappingIdsToKeep);

}
