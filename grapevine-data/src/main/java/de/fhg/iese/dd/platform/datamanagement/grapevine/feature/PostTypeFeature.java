/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.feature;

import com.fasterxml.jackson.annotation.JsonGetter;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

@Getter
@SuppressWarnings("unused")
public abstract class PostTypeFeature<P extends Post> extends BaseFeature {

    private String channelName;
    private String itemName;
    private int maxNumberCharsPerPost;
    private int orderValue;
    private Map<String, Object> additionalCustomAttributes;

    @JsonGetter
    public String getItemName() {
        if (StringUtils.isEmpty(itemName)) {
            return channelName;
        } else {
            return itemName;
        }
    }

    @SuppressWarnings("unchecked")
    public static <P extends Post> Class<? extends PostTypeFeature<P>> getSpecificPostTypeFeature(P post) {

        Class<? extends PostTypeFeature<?>> accept = post.accept(new Post.Visitor<>() {

            @Override
            public Class<? extends PostTypeFeature<?>> whenGossip(Gossip gossip) {

                return GossipPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenSuggestion(Suggestion suggestion) {

                return SuggestionPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenSeeking(Seeking seeking) {

                return SeekingPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenOffer(Offer offer) {

                return SuggestionPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenNewsItem(NewsItem newsItem) {

                return NewsItemPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenHappening(Happening happening) {

                return HappeningPostTypeFeature.class;
            }

            @Override
            public Class<? extends PostTypeFeature<?>> whenSpecialPost(SpecialPost specialPost) {

                return SpecialPostTypeFeature.class;
            }
        });
        return (Class<? extends PostTypeFeature<P>>) accept;
    }

}
