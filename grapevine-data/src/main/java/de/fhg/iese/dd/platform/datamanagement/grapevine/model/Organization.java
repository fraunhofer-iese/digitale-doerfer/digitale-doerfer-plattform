/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Organization extends BaseEntity implements DeletableEntity, NamedEntity {

    private static final ObjectWriter FACT_JSON_WRITER = defaultJsonWriter();
    private static final TypeReference<List<OrganizationFact>> FACT_JSON_TYPEREF =
            new TypeReference<>() {
            };
    private static final ObjectReader FACT_JSON_READER = defaultJsonReader().forType(FACT_JSON_TYPEREF);

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String locationDescription;

    @Column(nullable = false, length = MAXIMAL_TEXT_LENGTH)
    private String description;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem overviewImage;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem logoImage;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn //required to keep the order of images, creates an additional column
    private List<MediaItem> detailImages;

    @Column
    private String url;

    @Column
    private String phone;

    @Column
    private String emailAddress;

    @ManyToOne
    private Address address;

    @Column(length = DESCRIPTION_LENGTH)
    private String factsJson;

    @OneToMany(mappedBy = "organization", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrganizationPerson> organizationPersons;

    @Column(nullable = false)
    @Builder.Default
    private int tag = 0;

    @Column(nullable = false)
    private boolean deleted;

    @Column
    private Long deletionTime;

    public List<OrganizationFact> getFacts() {
        try {
            if (StringUtils.isNotEmpty(factsJson)) {
                return FACT_JSON_READER.readValue(factsJson);
            } else {
                return Collections.emptyList();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    //only required for data init, not for the API
    @JsonProperty
    public void setFacts(List<OrganizationFact> facts) {
        if (!CollectionUtils.isEmpty(facts)) {
            try {
                factsJson = FACT_JSON_WRITER.writeValueAsString(facts);
            } catch (JsonProcessingException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    //only required for data init, not for the API
    @JsonProperty
    public void setTags(Set<OrganizationTag> tags) {
        if (!CollectionUtils.isEmpty(tags)) {
            getTags().setValues(tags);
        }
    }

    /**
     * Tags of the organization. Changes are directly reflected at {@link #tag}, which is the value that is actually
     * stored.
     */
    @Transient
    @Getter(lazy = true)
    @Setter(value = AccessLevel.NONE)
    private final StorableEnumSet<OrganizationTag> tags =
            new StorableEnumSet<>(this::setTag, this::getTag, OrganizationTag.class);

    private static final Set<Organization> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new Organization());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<Organization> toInSafeEntitySet(Set<Organization> organizations) {

        if (CollectionUtils.isEmpty(organizations)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return organizations;
        }
    }

    @Override
    public String toString() {

        return "Organization{" +
                "id='" + id + "', " +
                "name='" + name + '\'' +
                '}';
    }

}
