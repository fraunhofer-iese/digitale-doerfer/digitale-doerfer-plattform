/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public enum OrganizationTag implements StorableEnum {

    EMERGENCY_AID(1 << 1),
    VOLUNTEERING(1 << 2),
    CIVIL_PROTECTION(1 << 3);

    private final int bitMaskValue;

}
