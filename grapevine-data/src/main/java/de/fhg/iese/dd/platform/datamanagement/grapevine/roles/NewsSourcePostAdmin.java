/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.roles;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantTenantContractRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
@SuppressFBWarnings(value="SE_BAD_FIELD", justification="If this role gets serialized it needs new repositories anyway.")
public class NewsSourcePostAdmin extends BaseRole<NewsSource> {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private NewsSourceRepository newsSourceRepository;

    @Autowired
    private AppVariantTenantContractRepository appVariantTenantContractRepository;

    private NewsSourcePostAdmin() {
        super(NewsSource.class);
    }

    @Override
    public String getDisplayName() {
        return "NewsSource-Post-Admin";
    }

    @Override
    public String getDescription() {
        return "Kann externe Posts einer NewsSource verwalten (bspw. löschen)";
    }

    @Override
    public NewsSource getRelatedEntityById(String relatedEntityId) {
        return newsSourceRepository.findByIdFull(relatedEntityId);
    }

    @Override
    public boolean existsRelatedEntity(String relatedEntityId) {
        return newsSourceRepository.existsById(relatedEntityId);
    }

    @Override
    public Tenant getTenantOfRelatedEntity(NewsSource relatedEntity) {

        AppVariant appVariant = relatedEntity.getAppVariant();
        if (appVariant == null) {
            return null;
        }
        Page<Tenant> tenantPage = appVariantTenantContractRepository.findTenantsByAppVariant(
                appVariant,
                Pageable.ofSize(1));

        if (tenantPage.getTotalElements() != 1) {
            log.warn("Related news source {} has not a distinct tenant. " +
                            "Found {} tenants for the NewsSource. Returning first/null.",
                    relatedEntity.getId(),
                    tenantPage.getTotalElements());
        }
        return tenantPage.stream().findFirst().orElse(null);
    }

}
