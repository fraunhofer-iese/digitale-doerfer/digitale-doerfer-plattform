/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionWorkerMapping;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface SuggestionWorkerMappingRepository extends JpaRepository<SuggestionWorkerMapping, String> {

    List<SuggestionWorkerMapping> findBySuggestionOrderByCreatedAsc(Suggestion suggestion);

    List<SuggestionWorkerMapping> findBySuggestionAndInactivatedIsNullOrderByCreatedAsc(Suggestion suggestion);

    List<SuggestionWorkerMapping> findBySuggestionIdInAndInactivatedIsNullOrderByCreatedAsc(Collection<String> suggestionIds);

    SuggestionWorkerMapping findBySuggestionAndWorkerAndInactivatedIsNull(Suggestion suggestion, Person worker);

}
