/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Adeline Silva Schäfer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

/**
 * Status a suggestion can have.
 * Currently the display names are only used in the eMails, the LösBar manages its own list of names.
 */
public enum SuggestionStatus {
    
    OPEN("Offen"),
    IN_PROGRESS("In Bearbeitung"),
    ON_HOLD("Weitergegeben an Extern"),
    DONE("Abgeschlossen");

    private final String displayName;

    SuggestionStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
