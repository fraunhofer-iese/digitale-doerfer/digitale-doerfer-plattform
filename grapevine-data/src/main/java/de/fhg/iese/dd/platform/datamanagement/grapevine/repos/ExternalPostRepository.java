/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ExternalPostRepository extends JpaRepository<ExternalPost, String> {

    Optional<ExternalPost> findByNewsSourceAndExternalId(NewsSource newsSource, String externalId);

    Page<ExternalPost> findAllByOrderByCreated(Pageable pageable);

    @Query("select distinct p from ExternalPost p " +
            "left join p.geoAreas g " +
            "where p.deleted = false " +
            "and p.published = true " +
            "and (g.id in :geoAreaIds) " +
            "and (p.postType in :postTypes) ")
    Page<ExternalPost> findAllByGeoAreaIdInAndPostTypeInAndDeletedFalseAndPublishedTrue(Collection<String> geoAreaIds,
            Collection<PostType> postTypes,
            Pageable pageable);

    Page<ExternalPost> findAllByNewsSourceIdAndPostTypeInAndDeletedFalseAndPublishedTrue(String newsSourceId,
            Collection<PostType> postTypes,
            Pageable pageable);

    @Query("select p from ExternalPost p " +
            "where p.deleted = false " +
            "and p.published = false " +
            "and p.desiredPublishTime <= :timestamp " +
            "and ( p.desiredUnpublishTime is null or p.desiredUnpublishTime >= :timestamp ) " +
            "order by p.created asc ")
    List<ExternalPost> findAllPublishedFalseAndWithinDesiredPublishTime(long timestamp);

    @Query("select p from ExternalPost p " +
            "where p.deleted = false " +
            "and p.published = true " +
            "and ( p.desiredUnpublishTime is not null and p.desiredUnpublishTime <= :timestamp ) " +
            "order by p.created asc ")
    List<ExternalPost> findAllPublishedTrueAndAfterDesiredUnpublishTime(long timestamp);

    @Query("select p from ExternalPost p " +
            "where  p.deleted = false " +
            "and p.published = true " +
            "and p.newsSource.checkUrlOfPosts = true " +
            "and p.lastModified >= :minLastModified " +
            "and (p.lastUrlCheckTime is null or p.lastUrlCheckTime <= :maxLastCheck) " +
            "order by p.created asc ")
    List<ExternalPost> findAllPublishedAndLastModifiedAfterAndLastCheckBefore(long minLastModified, long maxLastCheck, Pageable pageable);

}
