/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Entity
@Table( indexes = {
        @Index(columnList = "DTYPE"),
        @Index(columnList = "DTYPE, deleted, created, group_id"),
        @Index(columnList = "DTYPE, deleted, lastActivity, group_id"),
        @Index(columnList = "deleted"),
        @Index(columnList = "lastActivity"),
        @Index(columnList = "created"),
        // see comments at ExternalPost for explanation about this index
        @Index(name = "UKGrapevinePost_NewsS_Dtype_Exter_azoxknp8ar",
                columnList = "news_source_id, dtype, externalId", unique = true)
})
@NamedEntityGraph(name = "Post_NoTenantGeoAreaGroup",
        attributeNodes = {
                @NamedAttributeNode(value = "creator", subgraph = "subgraph.creator"),
                @NamedAttributeNode(value = "customAddress"),
                //we don't need the full geo areas, but it seems there is no way to fetch just the ids
                @NamedAttributeNode(value = "geoAreas", subgraph = "subgraph.geoAreas"),
                @NamedAttributeNode(value = "images", subgraph = "subgraph.images"),
                @NamedAttributeNode(value = "attachments", subgraph = "subgraph.attachments"),
        },
        subgraphs = {
                @NamedSubgraph(name = "subgraph.creator", attributeNodes = {@NamedAttributeNode("profilePicture")}),
                @NamedSubgraph(name = "subgraph.geoAreas", attributeNodes = {}),
                @NamedSubgraph(name = "subgraph.images", attributeNodes = {}),
                @NamedSubgraph(name = "subgraph.attachments", attributeNodes = {})
        }
)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public abstract class Post extends BaseEntity implements DeletableEntity {

    public interface Visitor<T> {

        T whenGossip(Gossip gossip);

        T whenSuggestion(Suggestion suggestion);

        T whenSeeking(Seeking seeking);

        T whenOffer(Offer offer);

        T whenNewsItem(NewsItem newsItem);

        T whenHappening(Happening happening);

        T whenSpecialPost(SpecialPost specialPost);

    }

    public abstract <T> T accept(Visitor<T> visitor);

    @ManyToOne
    @Nullable
    private Person creator;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "latitude", column = @Column(name = "createdLocationLat")),
            @AttributeOverride(name = "longitude", column = @Column(name = "createdLocationLng")),
    })
    private GPSLocation createdLocation;

    @ManyToOne
    @JoinColumn(name = "community_id")
    private Tenant tenant;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<GeoArea> geoAreas;

    //using semantic "hidden" ToOtherHomeAreas since boolean default value is false, so this attribute is false
    //by default which means that posts are not hidden by default.
    @Column(nullable = false)
    @ColumnDefault("false")
    @Builder.Default
    private boolean hiddenForOtherHomeAreas = false;

    @ManyToOne
    private Group group;

    @ManyToOne
    private Organization organization;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String text;

    /*
     * It should be @OneToMany here, but hibernate creates a unique constraint on images_id in this case.
     * With this constraint it is impossible to reorder images (because hibernate does not update by images_id, but by images_order).
     * This is a bug since 2010, so it will never be fixed:
     * https://stackoverflow.com/questions/4022509/constraint-violation-in-hibernate-unidirectional-onetomany-mapping-with-jointabl
     *
     * The workaround is @ManyToMany, because this does not create the constraint. On the other hand it also does not provide orphan removal.
     * So we need to delete the images that are removed from the list "manually" by calling IMediaItemService.deleteMediaItem(MediaItem).
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn //required to keep the order of images, creates an additional column
    private List<MediaItem> images;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<DocumentItem> attachments;

    @Column
    private long lastModified;

    @Column
    @Builder.Default
    private boolean deleted = false;

    @Column
    private long lastActivity;

    @Column(name = "DTYPE", insertable = false, updatable = false)
    @ReadOnlyProperty
    @Enumerated(EnumType.STRING)
    @Setter(AccessLevel.NONE)
    private PostType postType;

    @Column
    private long commentCount;

    @Column(nullable = false)
    @Builder.Default
    private boolean commentsAllowed = true;

    @Column
    private long likeCount;

    @ManyToOne
    private Address customAddress;

    @Column
    private Long deletionTime;

    @Column
    private Boolean searchIndexFailed;

    @Column(nullable = false)
    @Builder.Default
    private boolean parallelWorld = false;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String customAttributesJson;

    private static final Set<Post> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new Gossip());

    private static final ObjectWriter ORDERED_MAP_ENTRY_JSON_WRITER = defaultJsonWriter()
            .with(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);

    private static final TypeReference<SortedMap<String, Object>> JSON_TYPEREF_NAME_TO_CUSTOM_ATTRIBUTE =
            new TypeReference<>() {

            };

    private static final ObjectReader STRING_MAP_READER =
            defaultJsonReader().forType(JSON_TYPEREF_NAME_TO_CUSTOM_ATTRIBUTE);

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    @SuppressWarnings("unchecked")
    public static <P extends Post> Set<P> toInSafeEntitySet(Set<P> posts) {
        if (CollectionUtils.isEmpty(posts)) {
            return (Set<P>) emptySetToFixHQLBugOnEmptySet;
        } else {
            return posts;
        }
    }

    @JsonIgnore
    public Map<String, Object> getCustomAttributes() {

        try {
            if (StringUtils.isNotEmpty(customAttributesJson)) {
                return STRING_MAP_READER.readValue(customAttributesJson);
            } else {
                return Collections.emptySortedMap();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @JsonIgnore
    public void setCustomAttributes(Map<String, Object> customAttributes) {

        if (!CollectionUtils.isEmpty(customAttributes)) {
            try {
                customAttributesJson = ORDERED_MAP_ENTRY_JSON_WRITER.writeValueAsString(customAttributes);
            } catch (JsonProcessingException e) {
                throw new UncheckedIOException(e);
            }
        } else {
            customAttributesJson = null;
        }
    }

    @Override
    public String toString() {
        return "Post [" +
                "id='" + id + "', " +
                "creator='" + getIdOf(creator) + "', " +
                "postType='" + postType + "', " +
                "deleted='" + deleted + "', " +
                "text='" + text + "', " +
                "tenant='" + tenant + "', " +
                "geoAreas='" + geoAreas + "'" +
                "customAttributes='" + customAttributesJson + "', " +
                ']';
    }

}
