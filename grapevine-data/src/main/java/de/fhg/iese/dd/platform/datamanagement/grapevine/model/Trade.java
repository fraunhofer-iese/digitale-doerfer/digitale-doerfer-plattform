/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public abstract class Trade extends Post {

    @ManyToOne
    protected TradingCategory tradingCategory;

    @Enumerated(EnumType.STRING)
    protected TradingStatus tradingStatus;

    @Column
    protected long date;

    @Override
    public String toString() {
        return "Trade [" +
                "id='" + id + "', " +
                "creator='" + this.getCreator() + "', " +
                "text='" + this.getText() + "', " +
                "tradingCategory='" + (tradingCategory != null ? tradingCategory.getShortName() : "null") + "', " +
                "tenant='" + getTenant() + "', " +
                "geoAreas='" + getGeoAreas() + "'" +
                ']';
    }

}
