/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

// This unique constraint annotation is disliked by hibernated (because of polymorphism)
// (Warning HHH000139: Illegal use of @Table in a subclass of a SINGLE_TABLE hierarchy)
// When defining the constraint as actual unique constraint, it is completely ignored.
// Defining it as index works, but the column list has to mix the raw database column names and the entity field names.
// Defining it here unfortunately causes a warning at startup, to avoid it it's moved to Post
/*
@Table(indexes = {@Index(name = "UKGrapevinePost_NewsS_Dtype_Exter_azoxknp8ar",
        columnList = "news_source_id, dtype, externalId", unique = true)
}
*/
@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SuperBuilder
@NoArgsConstructor
public abstract class ExternalPost extends Post {

    @Column(length = 500)
    private String externalId;

    @Column(length = 500)
    private String url;

    @Column
    private String authorName;

    @Column
    private String categories;

    @ManyToOne
    private NewsSource newsSource;

    @Column
    @Builder.Default
    private Long desiredPublishTime = null;

    @Column
    @Builder.Default
    private Long desiredUnpublishTime = null;

    @Column
    @Builder.Default
    private Long lastUrlCheckTime = null;

    @Column(nullable = false)
    @ColumnDefault("true")
    @Builder.Default
    private boolean published = true;

}
