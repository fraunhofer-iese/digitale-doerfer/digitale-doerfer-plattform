/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.PostCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PostRepository extends JpaRepository<Post, String> {

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.PostCountByGeoAreaId(" +
            "g.id, " +
            "sum( case when p.postType = 'Gossip' then 1 else 0 end), " +
            "sum( case when p.postType = 'Suggestion' then 1 else 0 end), " +
            "sum( case when p.postType = 'Offer' then 1 else 0 end), " +
            "sum( case when p.postType = 'Seeking' then 1 else 0 end), " +
            "sum( case when p.postType = 'Happening' then 1 else 0 end), " +
            "sum( case when p.postType = 'Happening' and p.creator is not null and p.newsSource is null then 1 else 0 end), " +
            "sum( case when p.postType = 'NewsItem' then 1 else 0 end), " +
            "sum( case when p.postType = 'SpecialPost' then 1 else 0 end)," +
            "sum( case when p.group is not null then 1 else 0 end), " +
            "sum( case when size(p.images) > 0 then 1 else 0 end), " +
            "sum( case when size(p.attachments) > 0 then 1 else 0 end)) " +
            "from Post p " +
            "left join p.geoAreas g " +
            "where p.created >= :lastCreated " +
            "group by g.id ")
    List<PostCountByGeoAreaId> countByGeoAreaId(long lastCreated);

    @Query("select distinct p " +
            "from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and ( p.lastModified between :startInterval and :endInterval " +
            "or p.lastActivity between :startInterval and :endInterval ) " +
            "order by p.created asc")
    Page<Post> findAllByDeletedFalseAndLastModifiedBetweenOrderByCreated(long startInterval, long endInterval,
            Pageable pageable);

    @Query("select p " +
            "from Post p " +
            "left join p.geoAreas g " +
            "where p.deleted = false " +
            "and g = :geoArea " +
            "and p.postType = :postType " +
            "and p not in :postsToExclude " +
            "order by p.created asc")
    Page<Post> findAllByGeoAreaAndPostType(GeoArea geoArea, PostType postType,
            Collection<? extends Post> postsToExclude, Pageable pageable);

    Set<Post> findAllByIdIn(Collection<String> postIds);

    Page<Post> findAllBySearchIndexFailedTrueOrderByCreatedDesc(Pageable pageable);

    Optional<Post> findByIdAndDeletedFalse(String postId);

    long countAllByPostTypeAndCreatedBetween(PostType type, long startInterval, long endInterval);

    long countAllByPostTypeAndGroupIsNotNullAndCreatedBetween(
            PostType type, long startInterval, long endInterval);

    long countAllByTenantTagAndPostTypeAndCreatedBetween(
            String tag, PostType type, long startInterval, long endInterval);

    long countAllByTenantAndPostTypeAndCreatedBetween(
            Tenant tenant, PostType type, long startInterval, long endInterval);

    long countAllByTenantTagAndPostTypeAndGroupIsNotNullAndCreatedBetween(
            String tag, PostType type, long startInterval, long endInterval);

    long countAllByTenantAndPostTypeAndGroupIsNotNullAndCreatedBetween(
            Tenant tenant, PostType type, long startInterval, long endInterval);

    long countAllByGroup(Group group);

    @Query("select count(p.id) " +
            "from Post p " +
            "left join p.geoAreas g " +
            "where p.deleted = false " +
            "and (g = :geoArea)")
    long countAllByGeoAreaAndDeletedFalse(GeoArea geoArea);

    @Query("select count(p.id) " +
            "from Post p " +
            "left join p.geoAreas g " +
            "where (g = :geoArea)")
    long countByGeoArea(GeoArea geoArea);

    @Query("select max(p.created) from Post p where p.group = :group")
    Long findNewestPostCreatedTime(Group group);

    @Query("select max(p.lastActivity) from Post p where p.group = :group")
    Long findNewestCommentCreatedTime(Group group);

    List<Post> findAllByGroupAndDeletedFalse(Group group);

    Page<Post> findAllByGeoAreasContainsOrderByCreatedDesc(GeoArea geoArea, Pageable pageable);

    //Here, all subclasses of ExternalPost must be listed. This is tested by PostRepositoryIntegrityTest
    String EXTERNAL_POST_SUBCLASSES = "'NewsItem', 'Happening'";
    String TRADE_SUBCLASSES = "'Offer', 'Seeking'";

    String QUERY_POST_BY_GEOAREA =
            "select distinct p.id from Post p " +
                    "left join p.geoAreas g " +
                    "left join p.organization o " +
                    "where p.deleted = false " +
                    "and ((p.parallelWorld = false) or :parallelWorld = true) " +
                    "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
                    "and " +
                    "   (" +
                    "      (" +
                    "      p.group is null " +
                    "      and (g.id in :geoAreaIds) " +
                    "      and (" +
                    "          p.hiddenForOtherHomeAreas = false " +
                    "          or (p.hiddenForOtherHomeAreas = true and (p.creator = :caller or (:homeArea = g))) " +
                    "          ) " +
                    "      and (type(p) != 'Suggestion' or p.internal = false) " +
                    "      )" +
                    "   or " +
                    "      p.group.id in :memberGroupIds" +
                    "   ) " +
                    "and " +
                    "(:requiredTagBitMask = 0 or (o is not null and (bitand(o.tag, :requiredTagBitMask) != 0)))" +
                    "and " +
                    "(:forbiddenTagBitMask = 0 or (o is null or (bitand(o.tag, :forbiddenTagBitMask) = 0)))";

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "Post_NoTenantGeoAreaGroup")
    List<Post> findAllByIdIn(List<String> ids);

    /**
     * Only for testing, when we need all related entities
     */
    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,
            attributePaths = {"creator", "geoAreas", "tenant", "group", "images", "attachments", "customAddress"})
    @Query("select p from Post p where p.id = :id")
    Post findByIdFull(String id);

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,
            attributePaths = {"creator", "geoAreas", "tenant", "group", "images", "attachments", "customAddress"})
    @Query("select p from Post p order by p.id")
    List<Post> findAllFull();

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query(QUERY_POST_BY_GEOAREA + " and p.created between :startTime and :endTime ")
    Slice<String> findAllByGeoAreaAndCreatedBetween(
            Person caller,
            boolean parallelWorld,
            GeoArea homeArea,
            Set<String> geoAreaIds,
            Set<String> memberGroupIds,
            long startTime, long endTime,
            int requiredTagBitMask, int forbiddenTagBitMask,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group = :group " +
            "and p.created between :startTime and :endTime")
    Page<Post> findAllByGroupAndCreatedBetween(Group group, long startTime, long endTime, Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group.id in :memberGroupIds " +
            "and p.created between :startTime and :endTime")
    Page<Post> findAllByGroupIdsAndCreatedBetween(Set<String> memberGroupIds, long startTime, long endTime,
            Pageable pageable);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query(QUERY_POST_BY_GEOAREA + " and p.created between :startTime and :endTime and p.postType = :type ")
    Slice<String> findAllByGeoAreaAndTypeAndCreatedBetween(
            Person caller,
            boolean parallelWorld,
            GeoArea homeArea,
            Set<String> geoAreaIds,
            Set<String> memberGroupIds,
            PostType type,
            long startTime, long endTime,
            int requiredTagBitMask, int forbiddenTagBitMask,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group = :group " +
            "and p.postType = :type " +
            "and p.created between :startTime and :endTime")
    Page<Post> findAllByGroupAndTypeAndCreatedBetween(Group group, PostType type, long startTime, long endTime,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group.id in :memberGroupIds " +
            "and p.postType = :type " +
            "and p.created between :startTime and :endTime")
    Page<Post> findAllByGroupIdsAndTypeAndCreatedBetween(Set<String> memberGroupIds, PostType type, long startTime,
            long endTime,
            Pageable pageable);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query(QUERY_POST_BY_GEOAREA + " and p.lastActivity between :startTime and :endTime ")
    Slice<String> findAllByGeoAreaAndLastlastActivityBetween(
            Person caller,
            boolean parallelWorld,
            GeoArea homeArea,
            Set<String> geoAreaIds,
            Set<String> memberGroupIds,
            long startTime, long endTime,
            int requiredTagBitMask, int forbiddenTagBitMask,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group = :group " +
            "and p.lastActivity between :startTime and :endTime")
    Page<Post> findAllByGroupAndLastActivityBetween(Group group, long startTime, long endTime, Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group.id in :memberGroupIds " +
            "and p.lastActivity between :startTime and :endTime")
    Page<Post> findAllByGroupIdsAndLastActivityBetween(Set<String> memberGroupIds, long startTime, long endTime,
            Pageable pageable);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query(QUERY_POST_BY_GEOAREA + " and p.lastActivity between :startTime and :endTime and p.postType = :type ")
    Slice<String> findAllByGeoAreaAndTypeAndLastlastActivityBetween(
            Person caller,
            boolean parallelWorld,
            GeoArea homeArea,
            Set<String> geoAreaIds,
            Set<String> memberGroupIds,
            PostType type,
            long startTime, long endTime,
            int requiredTagBitMask, int forbiddenTagBitMask,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group = :group " +
            "and p.postType = :type " +
            "and p.lastActivity between :startTime and :endTime")
    Page<Post> findAllByGroupAndTypeAndLastActivityBetween(Group group, PostType type, long startTime, long endTime,
            Pageable pageable);

    @Query("select p from Post p " +
            "where p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.group.id in :memberGroupIds " +
            "and p.postType = :type " +
            "and p.lastActivity between :startTime and :endTime")
    Page<Post> findAllByGroupIdsAndTypeAndLastActivityBetween(Set<String> memberGroupIds, PostType type, long startTime,
            long endTime,
            Pageable pageable);

    @Query(value = "select distinct p " +
            "from Post p " +
            "where (p.creator.id != :personId or p.creator is null) " +
            "and p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and p.internal = false " +
            "and " +
            "(" +
            "   p.id in " +
            "      ( " +
            "      select distinct c.post.id " +
            "      from Comment c " +
            "      where c.creator.id = :personId " +
            "      and c.deleted = false " +
            "      ) " +
            "   or" +
            "   p.id in " +
            "      ( " +
            "      select hp.happening.id " +
            "      from HappeningParticipant hp " +
            "      where hp.person.id = :personId " +
            "      ) " +
            ") ")
    Page<Post> findAllPostsCommentedAndHappeningsParticipatedByPerson(String personId, Pageable pageRequest);

    @Query("select distinct p from Post p " +
            "left join p.geoAreas g " +
            "where p.id = :postId " +
            "and p.deleted = false " +
            "and (type(p) not in (" + EXTERNAL_POST_SUBCLASSES + ") or p.published = true) " +
            "and (type(p) != 'Suggestion' or p.internal = false) " +
            "and (" +
            "   p.hiddenForOtherHomeAreas = false " +
            "   or (p.hiddenForOtherHomeAreas = true and (p.creator = :caller or (:homeArea = g)))" +
            "   or p.group is not null " +
            //all group posts are returned, because the group content access rights are checked afterwards
            "   )")
    Optional<Post> findByIdAndNotDeletedFilteredByHiddenForOtherHomeAreas(String postId, Person caller,
            GeoArea homeArea);

    @Transactional
    @Modifying
    @Query("update Post p set p.likeCount = :newLikeCount where p = :post")
    void updateLikeCount(Post post, long newLikeCount);

    long countByTenant(Tenant tenant);

    @Transactional
    @Modifying
    @Query("update Post p set p.tenant = :newTenant where p.tenant = :oldTenant")
    int updateTenant(Tenant oldTenant, Tenant newTenant);

    @Query("select ip " +
            "from Post ip " +
            //creator is in parallel world but the parallel world attribute of the post is false
            "where (bitand(ip.creator.status, :parallelWorldBitMask) != 0 and ip.parallelWorld = false )" +
            //creator is not in parallel world but the parallel world attribute of the post is true
            "or (bitand(ip.creator.status, :parallelWorldBitMask) = 0 and ip.parallelWorld = true )" +
            "order by ip.created asc")
    Page<Post> findAllWithIncorrectParallelWorldStatus(int parallelWorldBitMask, Pageable request);

    @Query("select ip " +
            "from Post ip " +
            "where ip.creator = :creator " +
            "and ip.deleted = false " +
            "and ip.internal = false")
    Page<Post> findAllByCreatorAndDeletedFalse(Person creator, Pageable request);

    @Query("select ip " +
            "from Post ip " +
            "where ip.creator = :creator " +
            "and ip.postType = :postType " +
            "and ip.deleted = false " +
            "and ip.internal = false ")
    Page<Post> findAllByCreatorAndPostTypeAndDeletedFalse(Person creator, PostType postType, Pageable request);

    List<Post> findAllByCreatorOrderByCreatedDesc(Person creator);

    @Query("select distinct ip.creator.id " +
            "from Post ip " +
            "where ip.tenant.tag = :tag " +
            "and ip.created between :createdStart and :createdEnd")
    Set<String> distinctCreatorIdsByTenantTag(String tag, long createdStart, long createdEnd);

    @Query("select distinct ip.creator.id " +
            "from Post ip " +
            "where ip.tenant = :tenant " +
            "and ip.created between :createdStart and :createdEnd")
    Set<String> distinctCreatorIdsByTenant(Tenant tenant, long createdStart, long createdEnd);

    @Query("select count(distinct ip.creator.id) " +
            "from Post ip " +
            "where ip.tenant.tag = :tag " +
            "and ip.created between :createdStart and :createdEnd " +
            "group by ip.creator.id " +
            "having count(ip.creator.id) >= 10")
    List<Long> activeUsersInternalPostCount(String tag, long createdStart, long createdEnd);

    @Query("select count(distinct ip.creator.id) " +
            "from Post ip " +
            "where ip.tenant = :tenant " +
            "and ip.created between :createdStart and :createdEnd " +
            "group by ip.creator.id " +
            "having count(ip.creator.id) >= 10")
    List<Long> activeUsersInternalPostCount(Tenant tenant, long createdStart, long createdEnd);

    @Query("select count(ip.creator.id) as postcount " +
            "from Post ip " +
            "where ip.tenant.tag = :tag " +
            "and ip.created between :createdStart and :createdEnd " +
            "group by ip.creator.id " +
            "order by postcount desc")
    List<Long> postCountByAmount(String tag, long createdStart, long createdEnd);

    @Query("select count(ip.creator.id) as postcount " +
            "from Post ip " +
            "where ip.tenant = :tenant " +
            "and ip.created between :createdStart and :createdEnd " +
            "group by ip.creator.id " +
            "order by postcount desc")
    List<Long> postCountByAmount(Tenant tenant, long createdStart, long createdEnd);

    @Query("select p " +
            "from Post p " +
            "where p.created between :start and :end " +
            "and not exists " +
            "   (select 1 from UserGeneratedContentFlag ugcf " +
            "   where ugcf.entityId = p.id " +
            "   and ugcf.status in :forbiddenFlagStatuses) " +
            "order by p.created desc ")
    Page<Post> findAllByUserGeneratedContentFlagStatusNotInAndCreatedBetween(
            Collection<UserGeneratedContentFlagStatus> forbiddenFlagStatuses, long start, long end, Pageable page);

}
