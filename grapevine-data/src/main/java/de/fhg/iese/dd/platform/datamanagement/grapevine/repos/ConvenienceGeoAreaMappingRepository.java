/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public interface ConvenienceGeoAreaMappingRepository extends JpaRepository<ConvenienceGeoAreaMapping, String> {

    @Transactional
    @Modifying
    @Query("delete from ConvenienceGeoAreaMapping gA where gA.convenience = :convenience and (gA.geoArea not in :geoAreas)")
    void deleteAllByConvenienceAndGeoAreaNotIn(Convenience convenience, Collection<GeoArea> geoAreas);

    long countByGeoArea(GeoArea geoArea);

    @Query("select gA " +
            "from ConvenienceGeoAreaMapping gA " +
            "where gA.geoArea = :geoAreaA " +
            "and not exists " +
            "   (select 1 from ConvenienceGeoAreaMapping gB " +
            "   where gB.convenience = gA.convenience " +
            "   and gB.geoArea = :geoAreaB) ")
    Set<ConvenienceGeoAreaMapping> findMappingsToGeoAreaAAndNotToGeoAreaB(GeoArea geoAreaA, GeoArea geoAreaB);

    @Transactional
    @Modifying
    int deleteByGeoArea(GeoArea geoArea);

}
