/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSourceOrganizationMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

public interface NewsSourceOrganizationMappingRepository extends JpaRepository<NewsSourceOrganizationMapping, String> {

    @Transactional
    @Modifying
    @Query("delete from NewsSourceOrganizationMapping n where (n not in :organizationMappingToKeep)")
    int deleteByNotIn(Set<NewsSourceOrganizationMapping> organizationMappingToKeep);

    boolean existsByNewsSourceAndOrganization(NewsSource newsSource, Organization organization);

}
