/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;

public interface SuggestionRepository extends JpaRepository<Suggestion, String> {

    @Query("select distinct s " +
            "from Suggestion s " +
            "left join SuggestionWorkerMapping swm " +
            "on swm.suggestion = s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.created between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and swm.worker.id = :assignedWorkerId " +
            "and swm.inactivated = null " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndCreatedBetweenIncludingOverrideDeletedAndWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            String assignedWorkerId,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.created between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and not exists ( " +
            "       select 1 " +
            "       from SuggestionWorkerMapping swm " +
            "       where swm.suggestion = s " +
            "       and swm.inactivated is null ) " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndCreatedBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.created between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndCreatedBetweenIncludingOverrideDeleted(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    @Query("select distinct s " +
            "from Suggestion s " +
            "left join SuggestionWorkerMapping swm " +
            "on swm.suggestion = s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and swm.worker.id = :assignedWorkerId " +
            "and swm.inactivated = null " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastActivityBetweenIncludingOverrideDeletedAndWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            String assignedWorkerId,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and not exists ( " +
            "       select 1 " +
            "       from SuggestionWorkerMapping swm " +
            "       where swm.suggestion = s " +
            "       and swm.inactivated is null ) " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastActivityBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastActivityBetweenIncludingOverrideDeleted(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    @Query("select distinct s " +
            "from Suggestion s " +
            "left join SuggestionWorkerMapping swm " +
            "on swm.suggestion = s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastSuggestionActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and swm.worker.id = :assignedWorkerId " +
            "and swm.inactivated = null " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeletedAndWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            String assignedWorkerId,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastSuggestionActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and not exists ( " +
            "       select 1 " +
            "       from SuggestionWorkerMapping swm " +
            "       where swm.suggestion = s " +
            "       and swm.inactivated is null ) " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    @Query("select s " +
            "from Suggestion s " +
            "where (s.deleted = false or s.overrideDeletedForSuggestionRoles = true) " +
            "and s.tenant.id in :tenantIds " +
            "and s.lastSuggestionActivity between :startTime and :endTime " +
            "and s.suggestionStatus in :includedStatus " +
            "and s.parallelWorld = false ")
    Page<Suggestion> findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeleted(
            Set<String> tenantIds, long startTime, long endTime, Set<SuggestionStatus> includedStatus,
            Pageable request);

    Optional<Suggestion> findByInternalWorkerChat(Chat chat);

    @Transactional
    @Modifying
    @Query("update Suggestion s set s.suggestionCategory = :newCategory where s.suggestionCategory = :oldCategory")
    void updateSuggestionCategory(SuggestionCategory oldCategory, SuggestionCategory newCategory);

}
