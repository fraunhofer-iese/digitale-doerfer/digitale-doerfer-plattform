/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface OrganizationRepository extends JpaRepository<Organization, String> {

    Optional<Organization> findByIdAndDeletedFalse(String organizationId);

    @Query("select org from Organization org " +
            "where exists " +
            "     (select 1 from OrganizationGeoAreaMapping orgGeoMap" +
            "      where orgGeoMap.organization = org " +
            "      and orgGeoMap.geoArea.id in :geoAreaIds) " +
            "and bitand(org.tag, :requiredTagBitMask) != 0 " +
            "and org.deleted = false " +
            "order by org.name asc ")
    List<Organization> findByGeoAreaInOrderByName(Set<String> geoAreaIds, int requiredTagBitMask);

    @Query("select org from Organization org " +
            "where exists " +
            "     (select 1 from NewsSourceOrganizationMapping newssourceOrgMap" +
            "      where newssourceOrgMap.organization = org " +
            "      and newssourceOrgMap.newsSource = :newsSource) " +
            "and bitand(org.tag, :requiredTagBitMask) != 0 " +
            "and org.deleted = false " +
            "order by org.name asc ")
    List<Organization> findByNewsSourceOrderByName(NewsSource newsSource, int requiredTagBitMask);

    @Query("select org from Organization org " +
            "where org not in :organizations " +
            "and org.deleted = false " +
            "order by org.name asc ")
    List<Organization> findAllNotInOrderByName(Collection<Organization> organizations);

}
