/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.config;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.time.DurationMax;
import org.hibernate.validator.constraints.time.DurationMin;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Duration;

@Validated
@Configuration
@ConfigurationProperties(prefix="dd-platform.grapevine")
@Getter
@Setter
public class GrapevineConfig {

    @NotNull
    @Min(value = 0, message = "Only positive number of images possible")
    private int maxNumberImagesPerPost;

    @NotNull
    @Min(value = 0, message = "Only positive number of images possible")
    private int maxNumberImagesPerComment;

    @NotNull
    @Min(value = 0, message = "Only positive number of attachments possible")
    private int maxNumberAttachmentsPerPost;
    
    @NotNull
    @Min(value = 0, message = "Only positive number of attachments possible")
    private int maxNumberAttachmentsPerComment;

    /**
     * maximal number of chars for {@link Comment#getText()} and for {@link Suggestion#getWithdrawReason()} and for
     * {@link GroupMembership#getMemberIntroductionText()}
     */
    @NotNull
    @Min(value = 0, message = "Only positive number of chars possible")
    private int maxNumberCharsPerComment;

    @NotNull
    @Min(value = 0, message = "Only positive number of chars possible")
    private int maxNumberCharsPerGroupName;

    @NotNull
    @Min(value = 0, message = "Only positive number of chars possible")
    private int maxNumberCharsPerGroupShortName;

    @NotNull
    @Min(value = 0, message = "Only positive number of chars possible")
    private int maxNumberCharsPerGroupDescription;

    private String loesbarBaseUrl;

    private String dorffunkBaseUrl;

    @NotBlank
    private String senderEmailAddressDorffunk;

    @NotBlank
    private String senderEmailAddressLoesbar;

    /**
     * Time span in which posts can be searched. Only posts with lastModified and lastActivity newer than this time can
     * be searched.
     */
    @NotNull
    @DurationMin(days = 7)
    private Duration searchablePostTime;

    private boolean searchActivated = true;

    @NotNull
    private ExternalPostUrlCheckConfig externalPostUrlCheck;

    @Getter
    @Setter
    public static class ExternalPostUrlCheckConfig {

        /**
         * Max age of the post to be checked. The age is determined using the last modified time stamp of the post.
         * Older posts are not checked.
         */
        @NotNull
        @DurationMax(days = 150)
        private Duration postMaxAge;

        /**
         * Minimum interval between a check of the same post. It can be longer, depending on the number of posts that need to be checked.
         */
        @NotNull
        @DurationMin(hours = 1)
        private Duration checkIntervalPerPost;

        /**
         * Maximum number of posts that are checked in one check run, independent of the news source of the post.
         */
        @NotNull
        private int maxNumPostsPerCheckRun;

        /**
         * Maximum number of posts that are checked within one iteration, with all posts belonging to different news sources.
         */
        @NotNull
        private int maxNumPostsPerIteration;

        /**
         * Wait time between the checks in one iteration, with all posts belonging to different news sources.
         */
        @NotNull
        private Duration waitTimeWithinIteration;

        /**
         * Minimum wait time between the check of posts of the same news source.
         */
        @NotNull
        private Duration minWaitTimeBetweenIterations;

        /**
         * Maximum wait time between the check of posts of the same news source.
         */
        @NotNull
        private Duration maxWaitTimeBetweenIterations;

        /**
         * Maximum duration of a check run.
         */
        @NotNull
        private Duration maxCheckRunDuration;

    }

    @NotNull
    private ClassificationServiceConfig classificationService;

    @Getter
    @Setter
    public static class ClassificationServiceConfig {

        @NotBlank
        private String baseUrl;

        @NotBlank
        private String classificationQuery;

        @NotBlank
        private String apiKey;

    }

}
