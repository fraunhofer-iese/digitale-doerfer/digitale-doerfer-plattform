/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface OrganizationGeoAreaMappingRepository extends JpaRepository<OrganizationGeoAreaMapping, String> {

    @Query("select ogam.geoArea.id from OrganizationGeoAreaMapping ogam where ogam.organization = :organization order by 1")
    List<String> findGeoAreaIdsByOrganization(Organization organization);

    @Transactional
    @Modifying
    @Query("delete from OrganizationGeoAreaMapping ogam where ogam.organization = :organization and (ogam.geoArea not in :geoAreas)")
    void deleteAllByOrganizationAndGeoAreaNotIn(Organization organization, Collection<GeoArea> geoAreas);

    long countByGeoArea(GeoArea geoArea);

    @Query("select ogam " +
            "from OrganizationGeoAreaMapping ogam " +
            "where ogam.geoArea = :geoAreaA " +
            "and not exists " +
            "   (select 1 from OrganizationGeoAreaMapping gB " +
            "   where gB.organization = ogam.organization " +
            "   and gB.geoArea = :geoAreaB) ")
    Set<OrganizationGeoAreaMapping> findMappingsToGeoAreaAAndNotToGeoAreaB(GeoArea geoAreaA, GeoArea geoAreaB);

    @Transactional
    @Modifying
    int deleteByGeoArea(GeoArea geoArea);

}
