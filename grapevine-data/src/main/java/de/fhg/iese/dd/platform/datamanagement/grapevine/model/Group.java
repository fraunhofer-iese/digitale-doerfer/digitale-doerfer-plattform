/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import java.util.Collections;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Group extends BaseEntity implements DeletableEntity, NamedEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String shortName;

    @Column(length = BaseEntity.DESCRIPTION_LENGTH)
    private String description;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem logo;

    @ManyToOne(optional = false)
    private Tenant tenant;

    @ManyToOne
    private Person creator;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GroupVisibility visibility;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GroupAccessibility accessibility;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GroupContentVisibility contentVisibility;

    @Column(nullable = false)
    private boolean deleted;

    @Column
    private long memberCount;

    @Column
    private Long deletionTime;

    private static final Set<Group> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new Group());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<Group> toInSafeEntitySet(Set<Group> groups) {
        if (CollectionUtils.isEmpty(groups)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return groups;
        }
    }

    public void generateConstantId() {
        super.generateConstantId(this, tenant);
    }

    public Group withCreated(long created) {
        this.created = created;
        return this;
    }

    @Override
    public String toString() {
        return "Group [" +
                "id='" + id + "', " +
                "name='" + name + "', " +
                "tenant='" + tenant +
                ']';
    }

}
