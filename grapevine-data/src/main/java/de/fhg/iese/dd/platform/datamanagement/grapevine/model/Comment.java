/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Adeline Silva Schäfer, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Comment extends BaseEntity implements DeletableEntity {

    @ManyToOne(optional = false)
    private Post post;

    @Column
    private long lastModified;

    @Builder.Default
    @Column
    private boolean deleted = false;

    @ManyToOne(optional = false)
    private Person creator;

    /*
     * It should be @OneToMany here, but hibernate creates a unique constraint on images_id in this case.
     * With this constraint it is impossible to reorder images (because hibernate does not update by images_id, but by images_order).
     * This is a bug since 2010, so it will never be fixed:
     * https://stackoverflow.com/questions/4022509/constraint-violation-in-hibernate-unidirectional-onetomany-mapping-with-jointabl
     *
     * The workaround is @ManyToMany, because this does not create the constraint. On the other hand it also does not provide orphan removal.
     * So we need to delete the images that are removed from the list "manually" by calling IMediaItemService.deleteMediaItem(MediaItem).
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn //required to keep the order of images, creates an additional column
    @Singular //for lombok.Builder
    private List<MediaItem> images;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<DocumentItem> attachments;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String text;

    @Column
    private long likeCount;

    @ManyToOne
    private Comment replyTo;

    @Column
    private Long deletionTime;
    
    private static final Set<Comment> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new Comment());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<Comment> toInSafeEntitySet(Set<Comment> comments) {
        if (CollectionUtils.isEmpty(comments)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return comments;
        }
    }

    public Comment withCreated(long created) {
        this.created = created;
        return this;
    }

    @Override
    public String toString() {
        return "Comment [" +
                "id='" + id + "', " +
                "creator='" + creator + "', " +
                "text='" + text + "', " +
                "post='" + post + "'" +
                ']';
    }

}
