/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.feature;

import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonVerificationStatusRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.Feature;

/**
 * Used to restrict the endpoints for creating posts and comments to persons with a specific verification status.
 */
@Feature(featureIdentifier = "de.fhg.iese.dd.dorffunk.content.creation-restriction")
public class ContentCreationRestrictionFeature extends PersonVerificationStatusRestrictionFeature {

}
