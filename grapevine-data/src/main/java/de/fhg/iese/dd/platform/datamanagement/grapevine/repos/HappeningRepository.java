/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface HappeningRepository extends JpaRepository<Happening, String> {

    Optional<Happening> findByIdAndDeletedFalse(String happeningId);

    @Transactional(isolation = Isolation.READ_COMMITTED)
    @Query("select distinct p.id from Happening p " +
            "left join p.geoAreas g " +
            "left join p.organization o " +
            "where p.deleted = false " +
            "and p.published = true " +
            "and (" +
            "    ( p.startTime between :startTime and :endTime ) " +
            " or ( p.endTime between :startTime and :endTime ) " +
            " or ( p.startTime <= :startTime and p.endTime >= :endTime ) " +
            ") " +
            "and " +
            "   (" +
            "   (p.group is null " +
            "   and (g.id in :geoAreaIds) " +
            "   and (" +
            "       p.hiddenForOtherHomeAreas = false " +
            "       or (p.hiddenForOtherHomeAreas = true and (p.creator = :caller or (:homeArea = g)))" +
            "       )" +
            "   )" +
            "   or p.group.id in :memberGroupIds ) " +
            "and " +
            "(:requiredTagBitMask = 0 or (o is not null and (bitand(o.tag, :requiredTagBitMask) != 0))) " +
            "and " +
            "(:forbiddenTagBitMask = 0 or (o is null or (bitand(o.tag, :forbiddenTagBitMask) = 0))) ")
    Slice<String> findAllByGeoAreaAndStartTimeBetween(
            Person caller,
            GeoArea homeArea,
            Set<String> geoAreaIds,
            Set<String> memberGroupIds,
            long startTime, long endTime,
            int requiredTagBitMask, int forbiddenTagBitMask,
            Pageable pageable);

    @Query("select distinct p from Happening p " +
            "left join p.geoAreas g " +
            "where p.deleted = false " +
            "and p.published = true " +
            "and (g.id in :geoAreaIds) ")
    Page<Happening> findAllByGeoAreaIdInAndDeletedFalseAndPublishedTrue(Collection<String> geoAreaIds, Pageable pageable);

    Page<Happening> findAllByNewsSourceIdAndDeletedFalseAndPublishedTrue(String newsSourceId, Pageable pageable);
}
