/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"happening_id", "person_id"})}
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HappeningParticipant extends BaseEntity {

    @ManyToOne(optional = false)
    private Happening happening;

    @ManyToOne(optional = false)
    private Person person;

    @Override
    public String toString() {
        return "HappeningParticipant [" +
                "id='" + id + "', " +
                "happening='" + happening + "', " +
                "person='" + person + "', " +
                "]";
    }

}
