/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Benjamin Hassenfratz, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(indexes = {
        @Index(columnList = "app_variant_id", unique = true),
        @Index(columnList = "siteUrl", unique = true)
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class NewsSource extends BaseEntity implements DeletableEntity, NamedEntity {

    @Column(nullable = false)
    private String siteUrl;

    @Column(nullable = false)
    private String siteName;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem siteLogo;

    @Nullable
    @ManyToOne
    private AppVariant appVariant;

    @Column(nullable = false)
    @Builder.Default
    private boolean deleted = false;

    @Column
    private Long deletionTime;

    /**
     * If true, it was created by the api or was created by data-init and later modified via the api.
     */
    @Column(nullable = false)
    @Builder.Default
    private boolean apiManaged = false;

    /**
     * If true, the url of posts created by this news source are regularly checked for validity, otherwise the post is unpublished.
     */
    @Column(nullable = false)
    @Builder.Default
    private boolean checkUrlOfPosts = false;

    @Override
    public String toString() {
        return "NewsSource [" +
                "id='" + id + "', " +
                "siteUrl='" + siteUrl + "', " +
                "siteName='" + siteName + "'" +
                ']';
    }

    @Override
    public String getName() {
        return getSiteName();
    }

    @Override
    public void setName(String name) {
        setSiteName(name);
    }

}
