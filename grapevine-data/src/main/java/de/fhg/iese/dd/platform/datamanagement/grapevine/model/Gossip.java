/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Gossip extends Post {

    public PostType getPostType() {
        return PostType.Gossip;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.whenGossip(this);
    }

    @Override
    public String toString() {
        return "Gossip [" +
                "id='" + id + "', " +
                "deleted='" + isDeleted() + "', " +
                "text='" + getText() + "', " +
                "creator='" + getCreator() + "', " +
                "tenant='" + getTenant() + "', " +
                "geoAreas='" + getGeoAreas() + "', " +
                "group='" + getIdOf(getGroup()) + "'" +
                ']';
    }

}
