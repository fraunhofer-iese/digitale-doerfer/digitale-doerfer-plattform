/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"suggestion_id", "worker_id", "inactivated"})}
)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class SuggestionWorkerMapping extends BaseEntity {

    @ManyToOne(optional = false)
    private Suggestion suggestion;

    @ManyToOne(optional = false)
    private Person worker;

    /**
     * The person that triggered the add of the worker, might be null in case of automatic changes
     */
    @ManyToOne
    private Person initiator;

    /**
     * The time when the mapping became inactive. If it is still active, the timestamp is null.
     * <p>
     * The reason for this implementation is that we can still have a constraint for all the active ones.
     *
     */
    @Column(nullable = true)
    private Long inactivated;

    /**
     * @return true if the current mapping is active, which means {@link #inactivated} is null.
     */
    public boolean isActive(){
        return inactivated == null;
    }

    @Override
    public String toString() {
        return "SuggestionWorkerMapping [" +
                "id='" + id + "', " +
                "created='" + created + "', " +
                "initiator='" + initiator + "', " +
                "worker='" + worker + "'" +
                ']';
    }

}
