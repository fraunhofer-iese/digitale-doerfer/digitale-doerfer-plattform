/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Dominik Schnier, Johannes Eveslage, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostInteraction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.LikeCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.ViewCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PostInteractionRepository extends JpaRepository<PostInteraction, String> {

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.LikeCountByGeoAreaId(" +
            "g.id, " +
            "count(lp)) " +
            "from PostInteraction lp " +
            "left join lp.post.geoAreas g " +
            "where lp.created >= :lastCreated " +
            "and lp.liked = true " +
            "group by g.id ")
    List<LikeCountByGeoAreaId> countLikeByGeoAreaId(long lastCreated);

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    long countAllByPostAndLikedTrue(Post post);

    Optional<PostInteraction> findByPostAndPerson(Post post, Person person);

    Optional<PostInteraction> findByPostAndPersonAndLiked(Post post, Person person, boolean liked);

    boolean existsLikePostByPostAndPersonAndLikedIsTrue(Post post, Person person);

    @EntityGraph(type = EntityGraph.EntityGraphType.LOAD,
            attributePaths = {"person",
                    "post"})
    List<PostInteraction> findAllByPersonOrderByCreatedAsc(Person person);

    @Query("select lp.post.id from PostInteraction lp " +
            "where lp.post in :postsToCheck " +
            "and lp.person = :person " +
            "and lp.liked = true")
    Set<String> findPostIdByPersonAndPostInAndLikedTrue(Person person, Set<Post> postsToCheck);

    void deleteAllByPost(Post post);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.ViewCountByGeoAreaId(" +
            "lp.person.homeArea.id, " +
            "sum(case when lp.viewedOverviewTime is null then 0 else 1 end)," +
            "sum(case when lp.viewedDetailTime is null then 0 else 1 end)) " +
            "from PostInteraction lp " +
            "where lp.post = :post " +
            "group by lp.person.homeArea " +
            "order by count(lp) desc ")
    List<ViewCountByGeoAreaId> viewCountByGeoAreaId(Post post);

    @Query("select pi.viewedOverviewTime " +
            "from PostInteraction pi " +
            "where pi.post = :post " +
            "and pi.viewedOverviewTime is not null " +
            "order by pi.viewedOverviewTime ")
    List<Long> viewedOverviewTimeByPost(Post post);

    @Query("select pi.viewedDetailTime " +
            "from PostInteraction pi " +
            "where pi.post = :post " +
            "and pi.viewedDetailTime is not null " +
            "order by pi.viewedDetailTime ")
    List<Long> viewedDetailTimeByPost(Post post);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.ViewCountByGeoAreaId(" +
            "g.id, " +
            "sum( case when lp.viewedOverviewTime is not null then 1 else 0 end), " +
            "sum( case when lp.viewedDetailTime is not null then 1 else 0 end)) " +
            "from PostInteraction lp " +
            "left join lp.post.geoAreas g " +
            "where lp.created >= :lastCreated " +
            "group by g.id ")
    List<ViewCountByGeoAreaId> countViewsByGeoAreaId(long lastCreated);

}
