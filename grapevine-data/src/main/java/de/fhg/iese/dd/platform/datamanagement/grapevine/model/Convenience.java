/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.DeletableEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Convenience extends BaseEntity implements DeletableEntity, NamedEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false, length = DESCRIPTION_LENGTH)
    private String description;
    @Column
    private String organizationName;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem organizationLogo;
    @Column
    private String url;
    @Column
    private String urlLabel;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem overviewImage;
    /*
     * It should be @OneToMany here, but hibernate creates a unique constraint on images_id in this case.
     * With this constraint it is impossible to reorder images (because hibernate does not update by images_id, but by images_order).
     * This is a bug since 2010 and it seems to not be fixed in our version of hibernate
     * https://stackoverflow.com/questions/4022509/constraint-violation-in-hibernate-unidirectional-onetomany-mapping-with-jointabl
     *
     * The workaround is @ManyToMany, because this does not create the constraint. On the other hand it also does not provide orphan removal.
     * So we need to delete the images that are removed from the list "manually" by calling IMediaItemService.deleteMediaItem(MediaItem).
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn //required to keep the order of images, creates an additional column
    private List<MediaItem> detailImages;
    @ManyToOne(fetch = FetchType.LAZY)
    private AppVariant relatedAppVariant;
    @Column(nullable = false)
    private boolean deleted;
    @Column
    private Long deletionTime;

    @Override
    public void generateConstantId(String... seedIds) {
        super.generateConstantId(seedIds);
    }

    private static final Set<Convenience> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new Convenience());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<Convenience> toInSafeEntitySet(Set<Convenience> conveniences) {
        if (CollectionUtils.isEmpty(conveniences)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return conveniences;
        }
    }

}
