/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.HappeningParticipant;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface HappeningParticipantRepository extends JpaRepository<HappeningParticipant, String> {

    long countAllByHappening(Happening happening);

    long countAllByPerson(Person person);

    Optional<HappeningParticipant> findByHappeningAndPerson(Happening happening, Person person);

    List<HappeningParticipant> findAllByPerson(Person person);

    @Query("select hp.happening.id from HappeningParticipant hp " +
            "where hp.person = :person " +
            "and hp.happening in :happenings")
    Set<String> findAllHappeningIdByPersonAndHappeningIn(Person person, Set<Happening> happenings);

    @Query("select hp.person from HappeningParticipant hp " +
            "left join Person participant " +
            "on hp.person = participant " +
            "where hp.happening = :happening " +
            "and bitand(participant.status, :hiddenStatusBitMask) = 0 ")
    Page<Person> findAllParticipantsByHappening(Happening happening, int hiddenStatusBitMask,
            Pageable pageable);

    boolean existsByHappeningAndPerson(Happening happening, Person person);

    void deleteAllByHappening(Happening happening);

}
