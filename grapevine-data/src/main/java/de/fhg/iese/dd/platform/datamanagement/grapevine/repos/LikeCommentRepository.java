/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.LikeComment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.LikeCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface LikeCommentRepository extends JpaRepository<LikeComment, String> {

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.LikeCountByGeoAreaId(" +
            "g.id, " +
            "count(lc)) " +
            "from LikeComment lc " +
            "left join lc.comment.post.geoAreas g " +
            "where lc.created >= :lastCreated " +
            "and lc.liked = true " +
            "group by g.id ")
    List<LikeCountByGeoAreaId> countLikeByGeoAreaId(long lastCreated);

    long countAllByCommentAndLikedTrue(Comment comment);

    Optional<LikeComment> findByCommentAndPerson(Comment comment, Person person);

    Optional<LikeComment> findByCommentAndPersonAndLiked(Comment comment, Person person, boolean liked);

    boolean existsLikeCommentByCommentAndPersonAndLikedIsTrue(Comment comment, Person person);

    List<LikeComment> findAllByPersonOrderByCreatedAsc(Person person);

    @Query("select lc.comment.id from LikeComment lc " +
            "where lc.comment in :commentsToCheck " +
            "and lc.person = :person " +
            "and lc.liked = true")
    Set<String> findCommentIdByPersonAndPostInAndLikedTrue(Person person, Set<Comment> commentsToCheck);

    void deleteAllByComment(Comment comment);

}
