/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Trade;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;

public interface TradeRepository extends JpaRepository<Trade, String> {

    @Transactional
    @Modifying
    @Query("update Trade p set p.tradingCategory = :newCategory where p.tradingCategory = :oldCategory")
    void updateTradingCategory(TradingCategory oldCategory, TradingCategory newCategory);

}
