/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.util.CollectionUtils;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"news_source_id", "organization_id"})}
)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class NewsSourceOrganizationMapping extends BaseEntity {

    @ManyToOne(optional = false)
    private NewsSource newsSource;
    @ManyToOne(optional = false)
    private Organization organization;

    public NewsSourceOrganizationMapping withConstantId() {
        super.generateConstantId(newsSource, organization);
        return this;
    }

    private static final Set<NewsSourceOrganizationMapping> emptySetToFixHQLBugOnEmptySet = Collections.singleton(new NewsSourceOrganizationMapping());

    /**
     * HQL can not deal with IN conditions and empty sets, it constructs an invalid query. To fix this a fake entity is
     * used that never matches a real entity.
     */
    public static Set<NewsSourceOrganizationMapping> toInSafeEntitySet(Set<NewsSourceOrganizationMapping> mappings) {
        if (CollectionUtils.isEmpty(mappings)) {
            return emptySetToFixHQLBugOnEmptySet;
        } else {
            return mappings;
        }
    }

}
