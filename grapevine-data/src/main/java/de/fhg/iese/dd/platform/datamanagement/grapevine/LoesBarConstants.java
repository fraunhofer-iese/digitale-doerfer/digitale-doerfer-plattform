/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine;

import static de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId.id;

import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;

@SuppressWarnings("unused")//these constants are referenced via reflection by the data init
public interface LoesBarConstants {

    String APP_ID = "57b5a42c-c139-43c7-a04d-dc7d5f003dd4";

    String SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC = "loesbar-internal-chat";
    PushCategoryId PUSH_CATEGORY_ID_INTERNAL_WORKER_CHAT_MESSAGE_RECEIVED = id("1732810e-08ca-417a-935a-aedff768fd73");
    PushCategoryId PUSH_CATEGORY_ID_EMAIL_SUGGESTION_CREATED = id("22e0ff4e-fa06-4764-a65e-58bd468efe09");
    PushCategoryId PUSH_CATEGORY_ID_EMAIL_SUGGESTION_COMMENT_CREATED = id("b367dca7-aed4-4515-a1a1-9f21916282ec");
    PushCategoryId PUSH_CATEGORY_ID_EMAIL_SUGGESTION_STATUS_CHANGED = id("d967f03a-cb26-4a59-ab4f-6be41eba28d8");
    PushCategoryId PUSH_CATEGORY_ID_EMAIL_SUGGESTION_WORKER_ADDED = id("80281d1a-d461-42a1-a5e2-c34c6f52c10a");

}
