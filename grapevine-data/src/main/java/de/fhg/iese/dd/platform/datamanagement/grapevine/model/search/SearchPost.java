/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model.search;

import de.fhg.iese.dd.platform.datamanagement.framework.model.search.BaseSearchEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.util.List;

@Document(indexName = "#{@environmentService.getEnvironmentIdentifier()}.grapevine.post")
//the setting is a json file in the resources that gets referenced here, it is required to create the custom analyzer
@Setting(settingPath = "/search/SearchPostSettings.json")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@ToString
public class SearchPost extends BaseSearchEntity {

    @Field(type = FieldType.Long)
    private long lastModified;
    @Field(type = FieldType.Long)
    private long lastCommented;
    @Field(type = FieldType.Keyword)
    private List<String> geoAreaIds;
    @Field(type = FieldType.Keyword)
    private List<String> homeGeoAreaIds;
    @Field(type = FieldType.Keyword)
    private String groupId;
    @Field(type = FieldType.Keyword)
    private String creatorId;
    @Field(type = FieldType.Keyword)
    private PostType postType;
    @Field(type = FieldType.Text, analyzer = "custom_german")
    private String address;
    @Field(type = FieldType.Text, analyzer = "custom_german")
    private String text;
    @Field(type = FieldType.Text, analyzer = "custom_german")
    private List<String> commentTexts;

}
