/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupMetaData;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

public interface GroupRepository extends JpaRepository<Group, String> {

    Optional<Group> findByIdAndDeletedFalse(String id);

    long countAllByAccessibilityAndCreatedBetween(GroupAccessibility groupAccessibility, long start, long end);

    long countAllByTenantTagAndAccessibilityAndCreatedBetween(String tag, GroupAccessibility groupAccessibility,
            long start, long end);

    long countAllByTenantAndAccessibilityAndCreatedBetween(Tenant tenant, GroupAccessibility groupAccessibility,
            long start, long end);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupCountByGeoAreaId(" +
            "g.creator.homeArea.id, " +
            "count(g)) " +
            "from Group g " +
            "where g.created >= :lastCreated " +
            "group by g.creator.homeArea.id ")
    List<GroupCountByGeoAreaId> countByGeoAreaId(long lastCreated);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupCountByGeoAreaId(" +
            "g.creator.homeArea.id, " +
            "count(g)) " +
            "from Group g " +
            "where g.deletionTime >= :lastDeleted " +
            "and g.deleted = true " +
            "group by g.creator.homeArea.id ")
    List<GroupCountByGeoAreaId> countByGeoAreaIdDeleted(long lastDeleted);

    @Query("select distinct count(gm.member) " +
            "from Group g " +
            "left join GroupMembership gm " +
            "on gm.group = g " +
            "where g.accessibility = :groupAccessibility " +
            "and gm.created between :start and :end " +
            "and gm.status = 'APPROVED'")
    Optional<Long> countGroupMembersByAccessibilityAndJoinedBetween(GroupAccessibility groupAccessibility, long start,
            long end);

    @Query("select distinct count(gm.member) " +
            "from Group g " +
            "left join GroupMembership gm " +
            "on gm.group = g " +
            "where g.accessibility = :groupAccessibility " +
            "and g.tenant.tag = :tag " +
            "and gm.created between :start and :end " +
            "and gm.status = 'APPROVED'")
    Optional<Long> countGroupMembersByTenantTagAndAccessibilityAndJoinedBetween(String tag,
            GroupAccessibility groupAccessibility, long start, long end);

    @Query("select distinct count(gm.member) " +
            "from Group g " +
            "left join GroupMembership gm " +
            "on gm.group = g " +
            "where g.accessibility = :groupAccessibility " +
            "and g.tenant = :tenant " +
            "and gm.created between :start and :end " +
            "and gm.status = 'APPROVED'")
    Optional<Long> countGroupMembersByTenantAndAccessibilityAndJoinedBetween(Tenant tenant,
            GroupAccessibility groupAccessibility, long start, long end);

    Page<Group> findByDeletedFalseOrderByNameAsc(Pageable pageable);

    List<Group> findByCreatorOrderByCreatedDesc(Person person);

    Page<Group> findByTenantIdInAndDeletedFalseOrderByNameAsc(Set<String> tenantIds, Pageable pageable);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupMetaData(ggm.group, ggm.geoArea.id, ggm.excluded) " +
            "from GroupGeoAreaMapping ggm " +
            "where ggm.group.deleted = false " +
            "and exists (" +
            "   select 1 " +
            "   from  GroupGeoAreaMapping ggm2 " +
            "   where ggm2.geoArea.id in :selectedGeoAreaIds " +
            "   and ggm.group = ggm2.group " +
            ")")
    List<GroupMetaData> findAllAvailableGroupsAndMetadata(Set<String> selectedGeoAreaIds);

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupMetaData(ggm.group, ggm.geoArea.id, ggm.excluded) " +
            "from GroupGeoAreaMapping ggm " +
            "where ggm.group.deleted = false " +
            "and ggm.group.id = :groupId ")
    List<GroupMetaData> findGroupAndMetadata(String groupId);

    @Query("select g " +
            "from Group g " +
            "left join GroupMembership gm " +
            "on gm.group = g " +
            "where g.id = :groupId " +
            "and gm.member = :caller " +
            "and g.deleted = false " +
            "and ( " +
            "    gm.status = de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus.PENDING " +
            "    or " +
            "    gm.status = de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus.APPROVED " +
            "    ) ")
    Group findByIdAndMembership(String groupId, Person caller);

    @Query("select g " +
            "from Group g " +
            "left join GroupMembership gm " +
            "on gm.group = g " +
            "where gm.member = :caller " +
            "and g.deleted = false " +
            "and ( " +
            "    gm.status = de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus.PENDING " +
            "    or " +
            "    gm.status = de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus.APPROVED " +
            "    ) ")
    Set<Group> findAllByMembership(Person caller);

    @Query("select g " +
            "from Group g " +
            "where not exists (select 1 from GroupGeoAreaMapping gm where gm.group = g and gm.mainGeoArea = true )")
    Page<Group> findGroupsWithoutMainGeoArea(Pageable pageable);

    boolean existsByTenantAndNameIgnoreCase(Tenant tenant, String name);

    long countByTenant(Tenant tenant);

    @Transactional
    @Modifying
    @Query("update Group g set g.tenant = :newTenant where g.tenant = :oldTenant")
    int updateTenant(Tenant oldTenant, Tenant newTenant);

}
