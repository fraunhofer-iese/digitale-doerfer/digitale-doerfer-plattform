/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ConvenienceRepository extends JpaRepository<Convenience, String> {

    Optional<Convenience> findByIdAndDeletedFalse(String convenienceId);

    @Query("select c from Convenience c " +
            "where exists " +
            "     (select 1 from ConvenienceGeoAreaMapping cgm" +
            "      where cgm.convenience = c " +
            "      and cgm.geoArea.id in :geoAreaIds) " +
            "and c.deleted = false " +
            "order by c.name asc ")
    List<Convenience> findByGeoAreaInOrderByName(Set<String> geoAreaIds);

    @Query("select c from Convenience c " +
            "where c not in :conveniences " +
            "and c.deleted = false " +
            "order by c.name asc ")
    List<Convenience> findAllNotInOrderByName(Collection<Convenience> conveniences);

    Optional<Convenience> findByRelatedAppVariant(AppVariant appVariant);

}
