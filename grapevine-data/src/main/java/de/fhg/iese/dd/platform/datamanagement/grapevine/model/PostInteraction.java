/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"person_id", "post_id"})}
)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class PostInteraction extends BaseEntity {

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Person person;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Post post;

    @Builder.Default
    @Column
    private boolean liked = false;

    @Column
    private Long viewedDetailTime;

    @Column
    private Long viewedOverviewTime;

    @Override
    public String toString() {
        return "LikePost [" +
                "id='" + id + "', " +
                "person='" + person + "', " +
                "post='" + post + "', " +
                "liked='" + liked + "' " +
                "viewedDetailTime='" + viewedDetailTime + "' " +
                "viewedOverviewTime='" + viewedOverviewTime + "'" +
                "]";
    }

}
