/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(
        uniqueConstraints = {@UniqueConstraint(columnNames = {"group_id", "member_id"})}
)
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class GroupMembership extends BaseEntity {

    @ManyToOne(optional = false)
    private Group group;

    @ManyToOne(optional = false)
    private Person member;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private GroupMembershipStatus status;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String memberIntroductionText;

    @Override
    public String toString() {
        return "GroupMembership [" +
                "id='" + id + "', " +
                "group='" + group + "', " +
                "member='" + member + "', " +
                "status='" + status + "', " +
                ']';
    }

}
