/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface GroupMembershipRepository extends JpaRepository<GroupMembership, String> {

    Page<GroupMembership> findAllByGroup(Group group, Pageable pageable);

    List<GroupMembership> findAllByGroupInAndMemberOrderByCreatedDesc(Collection<Group> groups, Person person);

    List<GroupMembership> findAllByMemberOrderByCreatedDesc(Person person);

    Optional<GroupMembership> findByGroupAndMember(Group group, Person person);

    boolean existsByGroupAndMemberAndStatus(Group group, Person person, GroupMembershipStatus status);

    @Query("select distinct gm.group.id " +
            "from GroupMembership gm " +
            "left join Group g " +
            "on gm.group = g " +
            "where gm.member = :person " +
            "and g.deleted = false " +
            "and gm.status = :status ")
    Set<String> findAllMemberGroupIds(Person person, GroupMembershipStatus status);

    long countByGroupAndStatus(Group group, GroupMembershipStatus status);

    @Query("select distinct gm.member " +
            "from GroupMembership gm " +
            "where gm.group = :group " +
            "and gm.status = :membershipStatus " +
            "order by gm.member.id")
    List<Person> findAllMembersByGroupAndMembershipStatus(Group group, GroupMembershipStatus membershipStatus);

    @Query("select distinct gm.member " +
            "from GroupMembership gm " +
            "where gm.group = :group " +
            "and gm.status = :membershipStatus " +
            "order by gm.member.firstName, gm.member.lastName")
    Page<Person> findAllMembersByGroupAndMembershipStatusPaged(Group group, GroupMembershipStatus membershipStatus,
            Pageable pageable);

    @Transactional
    @Modifying
    int deleteByGroup(Group group);

}
