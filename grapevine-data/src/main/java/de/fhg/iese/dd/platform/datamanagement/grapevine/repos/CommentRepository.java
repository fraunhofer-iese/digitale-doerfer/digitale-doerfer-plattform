/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.repos;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.CommentCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CommentRepository extends JpaRepository<Comment, String> {

    @Query("select new de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.CommentCountByGeoAreaId(" +
            "g.id, " +
            "count(c), " +
            "sum(case when size(c.images) > 0 then 1 else 0 end), " +
            "sum(case when size(c.attachments) > 0 then 1 else 0 end)) " +
            "from Comment c " +
            "left join c.post p " +
            "left join p.geoAreas g " +
            "where c.created >= :lastCreated " +
            "group by g ")
    List<CommentCountByGeoAreaId> countByGeoAreaId(long lastCreated);

    Optional<Comment> findByIdAndDeletedFalse(String s);

    List<Comment> findAllByPostOrderByCreatedAsc(Post post);

    @Query("select c " +
            "from Comment c " +
            "where c.post = :post " +
            "and bitand(c.creator.status, :hiddenCreatorStatusBitMask) = 0 " +
            "order by c.created asc")
    List<Comment> findAllByPostAndNotCreatorStatusOrderByCreatedAsc(Post post, int hiddenCreatorStatusBitMask);

    List<Comment> findAllByCreatorOrderByCreatedDesc(Person creator);

    List<Comment> findAllByPostAndCreatorAndDeletedFalseOrderByCreatedDesc(Post post, Person creator);

    long countAllByCreatedBetweenAndDeletedFalse(long createdStart, long createdEnd);

    long countAllByPostTenantTagAndCreatedBetweenAndDeletedFalse(String tag, long createdStart, long createdEnd);

    long countAllByPostTenantAndCreatedBetweenAndDeletedFalse(Tenant tenant, long createdStart,
            long createdEnd);

    @Query("select count(c.id) " +
            "from Comment c " +
            "left join c.post p " +
            "left join p.geoAreas g " +
            "where c.deleted = false " +
            "and  p.deleted = false " +
            "and (g = :geoArea)")
    long countAllByPostGeoAreaAndDeletedFalse(GeoArea geoArea);

    @Query("select c.text " +
            "from Comment c " +
            "where c.post.id = :postId " +
            "and c.deleted = false " +
            "order by c.created desc")
    List<String> findAllCommentTextsByPostId(String postId);

    @Query("select distinct c.creator.id " +
            "from Comment c " +
            "where c.post.tenant.tag = :tag " +
            "and c.created between :createdStart and :createdEnd")
    Set<String> distinctCreatorIdsByTenantTag(String tag, long createdStart, long createdEnd);

    @Query("select distinct c.creator.id " +
            "from Comment c " +
            "where c.post.tenant = :tenant " +
            "and c.created between :createdStart and :createdEnd")
    Set<String> distinctCreatorIdsByTenant(Tenant tenant, long createdStart, long createdEnd);

    @Modifying
    @Query("update Comment c set c.likeCount = :newLikeCount where c = :comment")
    void updateLikeCount(Comment comment, long newLikeCount);

}
