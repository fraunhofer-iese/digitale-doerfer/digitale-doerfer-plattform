/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine;

import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;

import static de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId.id;

@SuppressWarnings("unused")//these constants are referenced via reflection by the data init
public interface DorfFunkConstants {

    String APP_ID = "b57636ce-a573-4742-ad75-55393a47eb38";

    String CHAT_TOPIC = "dorffunk-chat";

    PushCategoryId PUSH_CATEGORY_ID_POST_CREATED = id("e6e0808d-2be7-492c-baf2-7d131fe281e2");
    PushCategoryId PUSH_CATEGORY_ID_GOSSIP_CREATED = id("b396564d-e9d6-4d62-baf6-a29798343d11");
    PushCategoryId PUSH_CATEGORY_ID_NEWSITEM_CREATED = id("6286fe3e-a9e7-477a-ab4d-536e21c270ba");
    PushCategoryId PUSH_CATEGORY_ID_SUGGESTION_CREATED = id("db92329f-8ee5-4abd-b2b6-113c4a6a3789");
    PushCategoryId PUSH_CATEGORY_ID_SEEKING_CREATED = id("cdd931b3-601f-4bb5-9490-650046db0b16");
    PushCategoryId PUSH_CATEGORY_ID_OFFER_CREATED = id("2cbe23b6-cf99-4463-a0a1-40015485e67b");
    PushCategoryId PUSH_CATEGORY_ID_HAPPENING_CREATED = id("6e36fe4c-385f-42c5-9350-1364f1561946");
    PushCategoryId PUSH_CATEGORY_ID_SPECIALPOST_CREATED = id("46ddce29-97b7-4145-b58e-efcd775fb366");
    PushCategoryId PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED = id("839180a0-ce06-4068-b60f-6e44840db096");
    PushCategoryId PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED = id("7c6f99c3-8cb6-4627-aed7-2a222f150764");
    PushCategoryId PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED = id(
            "55afb150-b8ff-46f8-acd6-e68e14cc5992");
    PushCategoryId PUSH_CATEGORY_ID_POST_CHANGED = id("d0216ff0-b796-4d3f-a204-a87d9bf08e42");
    PushCategoryId PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED = id("ab8926cd-6258-4c99-bb38-f30bf4ca27e6");
    PushCategoryId PUSH_CATEGORY_ID_POST_DELETED = id("7e64129b-a652-487e-8728-b318c322ca2e");
    PushCategoryId PUSH_CATEGORY_ID_COMMENT_CREATED = id("390d1bb2-db4a-11e7-9296-cec278b6b50a");
    PushCategoryId PUSH_CATEGORY_ID_COMMENT_CHANGED = id("390d1e14-db4a-11e7-9296-cec278b6b50a");
    PushCategoryId PUSH_CATEGORY_ID_COMMENT_DELETED = id("390d20a8-db4a-11e7-9296-cec278b6b50a");
    PushCategoryId PUSH_CATEGORY_ID_COMMENT_ON_POST = id("0d4129f6-dbf4-11e7-9296-cec278b6b50a");
    PushCategoryId PUSH_CATEGORY_ID_COMMENT_REPLY = id("0d412c8a-dbf4-11e7-9296-cec278b6b50a");
    PushCategoryId PUSH_CATEGORY_ID_MOTIVATION = id("e3a71cdb-2cef-4b85-b81b-aa51c11396a5");
    PushCategoryId PUSH_CATEGORY_ID_OWN_PERSON_CHANGES = id("2850e1d1-889d-4300-ab70-241ab7b631de");
    PushCategoryId PUSH_CATEGORY_ID_EMAIL_VERIFIED = id("750485e9-be30-4499-bb68-cf1c481e2b37");
    PushCategoryId PUSH_CATEGORY_ID_CHAT = id("d0621d58-dfd7-4e46-9e1c-193d44c3dc17");
    PushCategoryId PUSH_CATEGORY_ID_CHAT_CREATED = id("5fb2d8d1-bdab-434c-bc4e-f2447f63100a");
    PushCategoryId PUSH_CATEGORY_ID_CHAT_MESSAGE_RECEIVED = id("8ba71617-d2d3-4dc1-8802-3a78d53d54e3");
    PushCategoryId PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED = id("08b0d55f-1af0-4596-a377-9e7bb5a4d790");

}
