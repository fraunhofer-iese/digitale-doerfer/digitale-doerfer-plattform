/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.grapevine.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
public class Happening extends ExternalPost {

    @Column
    private String organizer;
    @Column
    private long startTime;
    @Column
    private long endTime;
    @Column
    private boolean allDayEvent;
    @Column
    private long participantCount;
    @Transient
    private Boolean participated;

    public PostType getPostType() {
        return PostType.Happening;
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.whenHappening(this);
    }

    @Override
    public String toString() {
        return "Happening [" +
                "id='" + id + "', " +
                "externalId='" + getExternalId() + "'," +
                "deleted='" + isDeleted() + "', " +
                "authorName='" + getAuthorName() + "', " +
                "text='" + getText() + "', " +
                "newsSource='" + getNewsSource() + "', " +
                "tenant='" + getTenant() + "', " +
                "geoAreas='" + getGeoAreas() + "'" +
                ']';
    }

}
