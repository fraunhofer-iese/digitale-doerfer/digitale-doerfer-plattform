/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.util.Objects;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAssignmentCancelRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredOtherPersonRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredPlacementRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredReceivedResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupResponse;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.UnexpectedEventException;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeRejectedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredOtherPersonRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredPlacementRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.exceptions.CancelledByWrongCarrierException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveredByWrongCarrierException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeBundleNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.Signature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/event")
@Api(tags={"logistics.transport.events"})
class TransportEventController extends BaseController {

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private IDeliveryService deliveryService;

    @ApiOperation(value = "Receives a ClientTransportAlternativeSelectRequest")
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_ALTERNATIVE_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ALTERNATIVE_BUNDLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportAlternativeSelectRequest")
    public ClientTransportAlternativeSelectResponse onTransportAlternativeSelectRequest(
            @Valid @RequestBody ClientTransportAlternativeSelectRequest event) {

        Person carrier = getCurrentPersonNotNull();

        TransportAlternative transportAlternative =
                transportAlternativeService.findTransportAlternativeById(event.getTransportAlternativeId());
        TransportAlternativeBundle transportAlternativeBundle = transportAlternative.getTransportAlternativeBundle();
        if (transportAlternativeBundle == null) {
            throw new TransportAlternativeBundleNotFoundException(
                    "for Alternative '" + event.getTransportAlternativeId());
        }

        //idempotence
        //in fact this should _not_ be done in the controller, but it is easier to do it that way, because triggering these events again will cause other events that are not idempotent
        TransportAssignment transportAssignment =
                transportAssignmentService.findAssignedTransportAssignmentForAlternative(transportAlternative, carrier);
        if (transportAssignment != null) {
            return ClientTransportAlternativeSelectResponse.builder()
                    .accepted(true)
                    .transportAlternativeBundleToRemoveId(transportAlternativeBundle.getId())
                    .transportAlternativeId(event.getTransportAlternativeId())
                    .transportAssignmentToLoadId(transportAssignment.getId())
                    .build();
        }

        BaseEvent resultEvent = notifyAndWaitForAnyReply(
                new TransportAlternativeSelectRequest(carrier, transportAlternative),
                TransportAssignmentCreatedEvent.class, TransportAlternativeRejectedEvent.class);

        if (resultEvent instanceof TransportAssignmentCreatedEvent) {
            return ClientTransportAlternativeSelectResponse.builder()
                    .accepted(true)
                    .transportAlternativeBundleToRemoveId(transportAlternativeBundle.getId())
                    .transportAlternativeId(event.getTransportAlternativeId())
                    .transportAssignmentToLoadId(
                            ((TransportAssignmentCreatedEvent) resultEvent).getTransportAssignment().getId())
                    .build();
        } else if (resultEvent instanceof TransportAlternativeRejectedEvent) {
            return ClientTransportAlternativeSelectResponse.builder()
                    .accepted(false)
                    .transportAlternativeBundleToRemoveId(transportAlternativeBundle.getId())
                    .transportAlternativeId(event.getTransportAlternativeId())
                    .build();
        } else {
            throw new UnexpectedEventException("{} occurred unexpectedly", resultEvent);
        }
    }

    @ApiOperation(value = "Cancel a TransportAssignment with a ClientTransportAssignmentCancelRequest")
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.WRONG_CARRIER_CANCELLED,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportAssignmentCancelRequest")
    public ClientTransportAssignmentCancelConfirmation onTransportAssignmentCancelRequest(
            @Valid @RequestBody ClientTransportAssignmentCancelRequest event) {

        Person carrier = getCurrentPersonNotNull();

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        String cancellationReason = event.getCancelationReason();

        if(!Objects.equals(transportAssignment.getCarrier(), carrier))
            throw new CancelledByWrongCarrierException("The TransportAssignment "+transportAssignment.getId()+" can not be cancelled by "+carrier.getId()+", the assigned carrier is "+transportAssignment.getCarrier().getId()+"!");

        TransportAssignmentCancelRequest cancelRequest =
                new TransportAssignmentCancelRequest(
                        carrier,
                        transportAssignment,
                        delivery,
                        cancellationReason);

        TransportAssignmentCancelConfirmation cancelConfirmation =
                notifyAndWaitForReply(TransportAssignmentCancelConfirmation.class, cancelRequest);

        return new ClientTransportAssignmentCancelConfirmation(cancelConfirmation.getTransportAssignment().getId());
    }

    @ApiOperation(value = "Receives a ClientTransportDeliveredRequest")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportDeliveredRequest")
    public ClientTransportDeliveredResponse onTransportDeliveredRequest(
            @Valid @RequestBody ClientTransportDeliveredRequest event) {

        Person carrier = getCurrentPersonNotNull();

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        if(!Objects.equals(transportAssignment.getCarrier(), carrier))
            throw new DeliveredByWrongCarrierException("The Delivery "+delivery.getId()+" was delivered to the receiver by "+carrier.getId()+" instead of the actual carrier "+transportAssignment.getCarrier().getId()+"!");

        TransportDeliveredRequest transportDeliveredRequest = new TransportDeliveredRequest(CreatorType.CARRIER, delivery, event.getHandover(), transportAssignment, event.getHandoverNotes());

        TransportDeliveredConfirmation resultEvent =
                notifyAndWaitForReply(TransportDeliveredConfirmation.class, transportDeliveredRequest);

        return ClientTransportDeliveredResponse.builder()
                .deliveryTrackingCode(event.getDeliveryTrackingCode())
                .transportAssignmentId(resultEvent.getTransportAssignment().getId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientTransportDeliveredOtherPersonRequest")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportDeliveredOtherPersonRequest")
    public ClientTransportDeliveredResponse onTransportDeliveredOtherPersonRequest(
            @Valid @RequestBody ClientTransportDeliveredOtherPersonRequest event) {

        Person carrier = getCurrentPersonNotNull();

        if (StringUtils.isEmpty(event.getSignature().getSignature())) {
            throw new InvalidEventAttributeException("The {} may not be empty/null!", "signature");
        }

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        if (!Objects.equals(transportAssignment.getCarrier(), carrier))
            throw new DeliveredByWrongCarrierException(
                    "The delivery " + delivery.getId() + " was delivered to the receiver by " + carrier.getId() +
                            " instead of the actual carrier " + transportAssignment.getCarrier().getId() + "!");

        TransportDeliveredOtherPersonRequest transportDeliveredOtherPersonRequest =
                TransportDeliveredOtherPersonRequest.builder()
                        .delivery(delivery)
                        .transportAssignment(transportAssignment)
                        .acceptorName(event.getAcceptorName())
                        //FIXME needs to be adjusted when we know how to handle signatures
                        .signature(new Signature(event.getSignature().getSignature()))
                        .build();

        TransportDeliveredConfirmation resultEvent =
                notifyAndWaitForReply(TransportDeliveredConfirmation.class, transportDeliveredOtherPersonRequest);

        return ClientTransportDeliveredResponse.builder()
                .deliveryTrackingCode(event.getDeliveryTrackingCode())
                .transportAssignmentId(resultEvent.getTransportAssignment().getId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientTransportDeliveredPlacementRequest")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportDeliveredPlacementRequest")
    public ClientTransportDeliveredResponse onTransportDeliveredPlacementRequest(
            @Valid @RequestBody ClientTransportDeliveredPlacementRequest event) {

        Person carrier = getCurrentPersonNotNull();

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        if (!Objects.equals(transportAssignment.getCarrier(), carrier))
            throw new DeliveredByWrongCarrierException(
                    "The delivery " + delivery.getId() + " was delivered to the receiver by " + carrier.getId() +
                            " instead of the actual carrier " + transportAssignment.getCarrier().getId() + "!");

        TransportDeliveredPlacementRequest transportDeliveredPlacementRequest =
                TransportDeliveredPlacementRequest.builder()
                        .delivery(delivery)
                        .transportAssignment(transportAssignment)
                        .placementDescription(event.getPlacementDescription())
                        .build();

        TransportDeliveredConfirmation resultEvent =
                notifyAndWaitForReply(TransportDeliveredConfirmation.class, transportDeliveredPlacementRequest);

        return ClientTransportDeliveredResponse.builder()
                .deliveryTrackingCode(event.getDeliveryTrackingCode())
                .transportAssignmentId(resultEvent.getTransportAssignment().getId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientTransportDeliveredReceivedRequest")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportDeliveredReceivedRequest")
    public ClientTransportDeliveredReceivedResponse onTransportDeliveredReceivedRequest(
            @Valid @RequestBody ClientTransportDeliveredReceivedRequest event) {

        Person carrier = getCurrentPersonNotNull();

        if (StringUtils.isEmpty(event.getSignature().getSignature())) {
            throw new InvalidEventAttributeException("The {} may not be empty/null!", "signature");
        }

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        if(!Objects.equals(transportAssignment.getCarrier(), carrier))
            throw new DeliveredByWrongCarrierException("The Delivery "+delivery.getId()+" was delivered to the receiver by "+carrier.getId()+" instead of the actual carrier "+transportAssignment.getCarrier().getId()+"!");

        TransportDeliveredReceivedRequest transportDeliveredReceivedRequest =
                TransportDeliveredReceivedRequest.builder()
                .delivery(delivery)
                .transportAssignment(transportAssignment)
                //FIXME needs to be adjusted when we know how to handle signatures
                .signature(new Signature(event.getSignature().getSignature()))
                .build();

        TransportDeliveredReceivedConfirmation resultEvent =
                notifyAndWaitForReply(TransportDeliveredReceivedConfirmation.class, transportDeliveredReceivedRequest);

        return ClientTransportDeliveredReceivedResponse.builder()
                .deliveryTrackingCode(event.getDeliveryTrackingCode())
                .transportAssignmentId(resultEvent.getTransportAssignment().getId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientTransportPickupRequest")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/transportPickupRequest")
    public ClientTransportPickupResponse onTransportPickupRequest(
            @Valid @RequestBody ClientTransportPickupRequest event) {

        Person carrier = getCurrentPersonNotNull();

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException("for TransportAssignment " + event.getTransportAssignmentId());
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        TransportPickupRequest transportPickupRequest = new TransportPickupRequest(carrier,
                delivery,
                transportAssignment);
        TransportPickupConfirmation resultEvent =
                notifyAndWaitForReply(TransportPickupConfirmation.class, transportPickupRequest);

        return new ClientTransportPickupResponse(resultEvent.getTransportAssignment().getId());
    }

}
