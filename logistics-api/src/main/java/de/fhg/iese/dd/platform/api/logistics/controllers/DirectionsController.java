/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportNotAssignedToCurrentPersonException;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.shared.geo.services.IGeoService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;

@RestController
@RequestMapping("logistics/directions")
@Api(tags = {"logistics.directions"})
@IgnoreArchitectureViolation(
        value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
        reason = "More or less just a cache for the google maps API, so all endpoints return raw objects")
class DirectionsController extends BaseController {

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private IGeoService geoService;

    @Data
    @AllArgsConstructor
    private static class OriginDestination {

        private GPSLocation from;
        private GPSLocation to;
    }

    //Cache for directions with a fixed starting point
    private final LoadingCache<OriginDestination, Object> staticDirectionsCache = buildGeoCache();

    //Cache for directions with the current location as a starting point
    private final LoadingCache<OriginDestination, Object> dynamicDirectionsCache = buildGeoCache();

    private volatile Instant lastStatsReported = Instant.now();

    private LoadingCache<OriginDestination, Object> buildGeoCache() {
        return CacheBuilder.newBuilder()
                //directions should not be cached forever, e.g., to update road closures etc.
                .expireAfterWrite(24, TimeUnit.HOURS)
                //approx. 20k per entry, thus about 100M of RAM.
                //"When eviction is necessary, the cache evicts entries that are less likely to be used again.
                //For example, the cache may evict an entry because it hasn't been used recently or very often."
                //see http://google.github.io/guava/releases/23.0/api/docs/com/google/common/cache/CacheBuilder.html#maximumSize-long-
                .maximumSize(5000)
                .recordStats()
                .build(new CacheLoader<>() {
                    @Override
                    public Object load(OriginDestination key) throws Exception {
                        final Object directions = geoService.getDirections(key.getFrom(), key.getTo());
                        if (directions == null) {
                            //avoid caching null results
                            throw new NullPointerException();
                        }
                        return directions;
                    }
                });
    }

    private Object getDirections(LoadingCache<OriginDestination, Object> cache, GPSLocation from, GPSLocation to) {
        try {
            reportStats();
            return cache.get(new OriginDestination(from, to));
        } catch (ExecutionException ex) {
            return null;
        }
    }

    private Object getStaticDirections(GPSLocation from, GPSLocation to) {
        return getDirections(staticDirectionsCache, from, to);
    }

    private Object getDynamicDirections(GPSLocation from, GPSLocation to) {
        return getDirections(dynamicDirectionsCache, from, to);
    }

    private void reportStats() {
        if (!log.isDebugEnabled()) {
            return;
        }
        //reporting stats at most every 2 minutes
        final Instant now = Instant.now();
        if (now.minus(2, ChronoUnit.MINUTES).isAfter(lastStatsReported)) {
            lastStatsReported = now;
            log.debug("static directions cache statistics: {}", staticDirectionsCache.stats().toString());
            log.debug("dynamic directions cache statistics: {}", dynamicDirectionsCache.stats().toString());
        }
    }

    private static final String NOTES_RETURNED_RESULT = "Call returns Google Directions result, see "
            + "https://developers.google.com/maps/documentation/directions/start for details.";
    private static final String NOTES_AUTHENTICATED = "\n\nUser must be authenticated and be the carrier of the transport assignment.\n\n";

    @ApiOperation(value = "Returns Google Maps directions from the current location to the pickup position",
            notes = DirectionsController.NOTES_RETURNED_RESULT + DirectionsController.NOTES_AUTHENTICATED)
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("toPickupByAssignmentId")
    public Object toPickupByAssignmentId(
            @RequestParam(value = "assignmentId", required = true)
            @ApiParam(value = "id of a TransportAssignment", required = true) String assignmentId,
            @RequestParam(value = "currentLatitude", required = true)
            @ApiParam(value = "latitude value of current position", required = true) double currentLatitude,
            @RequestParam(value = "currentLongitude", required = true)
            @ApiParam(value = "longitude value of current position", required = true) double currentLongitude)
            throws TransportNotAssignedToCurrentPersonException {

        final Person currentPerson = getCurrentPersonNotNull();
        return toLocationByAssignmentId(currentPerson, assignmentId,
                assignment -> assignment.getPickupAddress().getAddress().getGpsLocation(),
                "pickup", currentLatitude, currentLongitude);
    }

    @ApiOperation(value = "Returns Google Maps directions from the current location to the delivery position",
            notes = DirectionsController.NOTES_RETURNED_RESULT + DirectionsController.NOTES_AUTHENTICATED)
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("toDeliveryByAssignmentId")
    public Object toDeliveryByAssignmentId(
            @RequestParam(value = "assignmentId", required = true)
            @ApiParam(value = "id of a TransportAssignment", required = true) String assignmentId,
            @RequestParam(value = "currentLatitude", required = true)
            @ApiParam(value = "latitude value of current position", required = true) double currentLatitude,
            @RequestParam(value = "currentLongitude", required = true)
            @ApiParam(value = "longitude value of current position", required = true) double currentLongitude)
            throws TransportNotAssignedToCurrentPersonException {

        final Person currentPerson = getCurrentPersonNotNull();
        return toLocationByAssignmentId(currentPerson, assignmentId,
                assignment -> assignment.getDeliveryAddress().getAddress().getGpsLocation(),
                "delivery", currentLatitude, currentLongitude);
    }

    @ApiOperation(value = "Returns Google Maps directions from pickup to delivery position",
            notes = DirectionsController.NOTES_RETURNED_RESULT + DirectionsController.NOTES_AUTHENTICATED)
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("pickupToDeliveryByAssignmentId")
    public Object pickupToDeliveryByAssignmentId(
            @RequestParam(value = "assignmentId", required = true)
            @ApiParam(value = "id of a TransportAssignment", required = true) String assignmentId)
            throws TransportNotAssignedToCurrentPersonException {

        final Person currentPerson = getCurrentPersonNotNull();
        final TransportAssignment assignment = transportAssignmentService.findTransportAssignmentById(assignmentId);
        checkCorrectAssignment(currentPerson, assignment);
        final GPSLocation origin = assignment.getPickupAddress().getAddress().getGpsLocation();
        checkNotNull(origin, "pickup");
        final GPSLocation destination = assignment.getDeliveryAddress().getAddress().getGpsLocation();
        checkNotNull(destination, "delivery");

        return getStaticDirections(origin, destination);
    }

    @ApiOperation(value = "Returns Google Maps directions from pickup to delivery position",
            notes = DirectionsController.NOTES_RETURNED_RESULT)
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("pickupToDeliveryByAlternativeId")
    public Object pickupToDeliveryByAlternativeId(
            @RequestParam(value = "alternativeId", required = true)
            @ApiParam(value = "id of a TransportAlternative", required = true) String alternativeId) {

        final TransportAlternative alternative =
                transportAlternativeService.findTransportAlternativeById(alternativeId);
        final GPSLocation origin =
                alternative.getTransportAlternativeBundle().getPickupAddress().getAddress().getGpsLocation();
        checkNotNull(origin, "pickup");
        final GPSLocation destination = alternative.getDeliveryAddress().getAddress().getGpsLocation();
        checkNotNull(destination, "delivery");

        return getStaticDirections(origin, destination);
    }

    private Object toLocationByAssignmentId(Person currentPerson, String assignmentId, Function<TransportAssignment, GPSLocation> destinationProvider,
            String addressName, double currentLatitude, double currentLongitude)
            throws TransportNotAssignedToCurrentPersonException {

        final TransportAssignment assignment = transportAssignmentService.findTransportAssignmentById(assignmentId);
        checkCorrectAssignment(currentPerson, assignment);
        final GPSLocation destination = destinationProvider.apply(assignment);
        checkNotNull(destination, addressName);
        return getDynamicDirections(new GPSLocation(currentLatitude, currentLongitude), destination);
    }

    private void checkCorrectAssignment(Person currentPerson, final TransportAssignment assignment) throws TransportNotAssignedToCurrentPersonException {

        final Person carrier = assignment.getCarrier();
        if (!currentPerson.equals(carrier)) {
            log.debug("Transport is assigned to {} (id={}) which is not the person calling the endpoint.",
                    carrier.getEmail(), carrier.getId());
            throw new TransportNotAssignedToCurrentPersonException();
        }
    }

    private static void checkNotNull(final GPSLocation destination, String addressName) throws IllegalStateException {
        if (destination == null) {
            throw new IllegalStateException(addressName + " address has not been geocoded");
        }
    }

}
