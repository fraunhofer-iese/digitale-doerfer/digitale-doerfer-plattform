/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.clientmodel;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class ClientDimension {

    /**
     * Length of the box in cm, largest number
     * <p>
     * length >= width >= height
     */
    private double length;

    /**
     * Width of the box in cm
     * <p>
     * length >= width >= height
     */
    private double width;

    /**
     * Height of the box in cm, smallest number
     * <p>
     * length >= width >= height
     */
    private double height;

    /**
     * Category of the box according to the size (length, width, height).
     *
     */
    private DimensionCategory category;

    /**
     * Weight of the box in kg
     */
    private double weight;
    
}
