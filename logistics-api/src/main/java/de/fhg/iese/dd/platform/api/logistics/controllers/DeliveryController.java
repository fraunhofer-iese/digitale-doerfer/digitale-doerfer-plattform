/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Steffen Hupp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.AccessedAnothersResourceException;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDelivery;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/delivery")
@Api(tags={"logistics.delivery"})
class DeliveryController extends BaseController {

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private LogisticsClientModelMapper mapper;

    @ApiOperation(value = "Returns all deliveries of the requesting user",
            notes = "Returns all deliveries where the requesting user is receiver within the specified day range and all deliveries that are != RECEIVED")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping
    public List<ClientDelivery> getAllDeliveries(
            @RequestParam(value = "sinceDays", required = false, defaultValue = "20") int sinceDays
            ){

        long startTime = System.nanoTime();

        Person requester = getCurrentPersonNotNull();

        if(sinceDays <= 0) sinceDays = 20;
        long sinceTimestamp = Instant.now().minus(Duration.ofDays(sinceDays)).toEpochMilli();

        List<Delivery> deliveries = deliveryService.findAllDeliveriesByReceiverSinceAndAllNotReceived(requester.getId(), sinceTimestamp);

        List<ClientDelivery> result = deliveries.stream().map(d -> mapper.createClientDelivery(d)).collect(Collectors.toList());

        long elapsedTime = System.nanoTime() - startTime;

        log.debug("Returned {} Delivery in {} ms", deliveries.size(), TimeUnit.NANOSECONDS.toMillis(elapsedTime));

        return result;
    }

    @ApiOperation(value = "Returns a single delivery")
    @ApiExceptions({
            ClientExceptionType.ACCESSED_ANOTHERS_RESOURCES,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("{deliveryId}")
    public ClientDelivery getDelivery(
            @PathVariable String deliveryId
            ){

        Person requester = getCurrentPersonNotNull();
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        Person receiver = delivery.getReceiver().getPerson();
        Shop sender = delivery.getSender().getShop();
        if((receiver != null && Objects.equals(receiver, requester)) ||
                (sender != null && getRoleService().hasRoleForEntity(requester, ShopOwner.class, sender))){

            return mapper.createClientDelivery(delivery);
        }else{
            throw new AccessedAnothersResourceException();
        }
    }

}
