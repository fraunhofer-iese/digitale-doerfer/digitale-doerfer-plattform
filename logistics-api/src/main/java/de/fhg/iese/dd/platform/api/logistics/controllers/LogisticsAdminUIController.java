/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.LogisticsAdminRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.DeliveryAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.PoolingStationBoxAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.PoolingStationBoxAllocationAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAlternativeBundleAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAssignmentAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/adminui")
@Api(tags = {"logistics.adminui"})
@IgnoreArchitectureViolation(
        value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
        reason = "Legacy implementation")
public class LogisticsAdminUIController extends BaseController {

    @Autowired
    private LogisticsAdminRepository logisticsAdminRepository;

    @Autowired
    private ITenantService tenantService;

    @ApiOperation(value = "Admin Overview: Deliveries",
            notes = "All deliveries of the selected tenant in a specific time range")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {LogisticsAdminUiRestrictedAdmin.class, LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping("deliveries")
    public List<DeliveryAdminUIRow> getAllDeliveries(
            @RequestParam String communityId,
            @RequestParam long fromTime,
            @RequestParam long toTime
            ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedCensored =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiRestrictedAdmin.class, tenant);
        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedCensored && !authorizedDetail) {
            throw new NotAuthorizedException();
        }

        List<DeliveryAdminUIRow> result =
                logisticsAdminRepository.findDeliveriesInCommunityCreatedFromToOrderByCreatedDesc(communityId, fromTime,
                        toTime);

        if (!authorizedDetail) {
            result.forEach(LogisticsAdminUIController::censorDeliveryAdminUIRow);
        }

        return result;
    }

    @ApiOperation(value = "Admin Overview: TransportAlternativeBundles",
            notes = "All open TransportAlternativeBundles of the selected tenant in a specific time range")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {LogisticsAdminUiRestrictedAdmin.class, LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping("transportAlternativeBundles")
    public List<TransportAlternativeBundleAdminUIRow> getAllOpenTransportAlternativeBundles(
            @RequestParam String communityId,
            @RequestParam long fromTime,
            @RequestParam long toTime
            ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedCensored =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiRestrictedAdmin.class, tenant);
        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedCensored && !authorizedDetail) {
            throw new NotAuthorizedException();
        }

        List<TransportAlternativeBundleAdminUIRow> result = logisticsAdminRepository
                .findTransportAlternativeBundlesCreatedFromToOrderByCreatedDesc(communityId, fromTime, toTime);

        if (!authorizedDetail) {
            result.forEach(LogisticsAdminUIController::censorTransportAlternativeBundleAdminUIRow);
        }

        return result;

    }

    @ApiOperation(value = "Admin Overview: TransportAssignments",
            notes = "All TransportAssignments of the selected tenant in a specific time range")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {LogisticsAdminUiRestrictedAdmin.class, LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping(value = "transportAssignments")
    public List<TransportAssignmentAdminUIRow> getAllTransportAssignments(
            @RequestParam String communityId,
            @RequestParam long fromTime,
            @RequestParam long toTime
            ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedCensored =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiRestrictedAdmin.class, tenant);
        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedCensored && !authorizedDetail) {
            throw new NotAuthorizedException();
        }

        List<TransportAssignmentAdminUIRow> result = logisticsAdminRepository
                .findTransportAssignmentsCreatedFromToOrderByCreatedDesc(communityId, fromTime, toTime);

        if (!authorizedDetail) {
            result.forEach(LogisticsAdminUIController::censorTransportAssignmentAdminUIRow);
        }

        return result;
    }

    @ApiOperation(value = "Admin Overview: PoolingStationBox",
            notes = "All PoolingStationBoxes of the selected tenant or PoolingStation")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {LogisticsAdminUiRestrictedAdmin.class, LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping("poolingStationBoxes")
    public List<PoolingStationBoxAdminUIRow> getAllPoolingStationBoxes(
            @RequestParam String communityId,
            @RequestParam(required=false) String poolingStationId
            ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedCensored =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiRestrictedAdmin.class, tenant);
        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedCensored && !authorizedDetail) {
            throw new NotAuthorizedException();
        }

        if (poolingStationId == null || poolingStationId.isEmpty()) {
            poolingStationId = "%";
        }

        List<PoolingStationBoxAdminUIRow> result = logisticsAdminRepository
                .findPoolingStationBoxesOrderByPoolingStationNameAndBoxName(communityId, poolingStationId);

        if(!authorizedDetail){
            result.forEach(LogisticsAdminUIController::censorPoolingStationBoxAdminUIRow);
        }

        return result;
    }

    @ApiOperation(value = "Admin Overview: PoolingStationBoxAllocation",
            notes = "All PoolingStationBoxAllocations of the selected tenant or PoolingStation in a specific time range")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping("poolingStationBoxAllocations")
    public List<PoolingStationBoxAllocationAdminUIRow> getAllPoolingStationBoxAllocations(
            @RequestParam String communityId,
            @RequestParam(required=false) String poolingStationId,
            @RequestParam long fromTime,
            @RequestParam long toTime
            ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedDetail) {
            throw new NotAuthorizedException();
        }

        if (StringUtils.isEmpty(poolingStationId)) {
            poolingStationId = "%";
        }

        return logisticsAdminRepository.
                findPoolingStationBoxAllocationsOrderByTimestampDesc(communityId, poolingStationId, fromTime, toTime);
    }

    public static void censorDeliveryAdminUIRow(DeliveryAdminUIRow rowEntry) {
        rowEntry.setPurchaseOrderItems(null);
        rowEntry.setTrackingCode(null);
        rowEntry.setStatusHistory(null);
        rowEntry.setPurchaseOrderId(null);
        rowEntry.setCommunity(null);
    }

    private static void censorTransportAlternativeBundleAdminUIRow(TransportAlternativeBundleAdminUIRow rowEntry){
        rowEntry.setPurchaseOrderItems(null);
        rowEntry.setStatusHistory(null);
        rowEntry.setPurchaseOrderId(null);
        rowEntry.setSenderShopId(null);
        rowEntry.setReceiverId(null);
        rowEntry.setCommunity(null);
    }

    private static void censorTransportAssignmentAdminUIRow(TransportAssignmentAdminUIRow rowEntry){
        rowEntry.setPurchaseOrderItems(null);
        rowEntry.setStatusHistory(null);
        rowEntry.setPurchaseOrderId(null);
        rowEntry.setSenderShopId(null);
        rowEntry.setReceiverId(null);
        rowEntry.setCommunity(null);
        rowEntry.setCarrierId(null);
    }

    private static void censorPoolingStationBoxAdminUIRow(PoolingStationBoxAdminUIRow rowEntry) {
        rowEntry.setPoolingStationBoxId(null);
        rowEntry.setAllocationId(null);
        rowEntry.setTransportAssignmentInId(null);
        rowEntry.setTransportAssignmentOutId(null);
        rowEntry.setCommunity(null);
    }

}
