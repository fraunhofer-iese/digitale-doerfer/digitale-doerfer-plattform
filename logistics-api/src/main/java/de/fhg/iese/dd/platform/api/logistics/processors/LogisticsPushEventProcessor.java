/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.processors;

import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.*;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.*;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.*;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import org.springframework.beans.factory.annotation.Autowired;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class LogisticsPushEventProcessor extends BaseEventProcessor {

    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private LogisticsClientModelMapper mapper;

    @PushEvent(clientEvent = ClientTransportAlternativeBundleForSelectionEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID")
    @EventProcessing
    private void handleTransportAlternativeBundleForSelectionEvent(TransportAlternativeBundleForSelectionEvent event) {
        try {
            ClientTransportAlternativeBundleForSelectionEvent clientEvent =
                    new ClientTransportAlternativeBundleForSelectionEvent(
                            event.getTransportAlternativeBundle().getId());

            //we can push it loud, since the clients are by default set to silent for this push category
            clientPushService.pushToGeoAreaLoud(geoAreaService.getLegacyRootGeoAreaOfTenant(event.getTenant()),
                    clientEvent,
                    "Es sind neue Lieferungen verfügbar!",
                    clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID));
        } catch (Exception e) {
            log.error("Failed to push TransportAlternativeBundleForSelectionEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientTransportAlternativeBundleExpiredEvent.class,
            loud = false,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID")
    @EventProcessing
    private void handleTransportAlternativeBundleExpiredEvent(TransportAlternativeBundleExpiredEvent event) {
        try {
            ClientTransportAlternativeBundleExpiredEvent clientEvent =
                    new ClientTransportAlternativeBundleExpiredEvent(event.getTransportAlternativeBundle().getId());

            clientPushService.pushToGeoAreaSilent(geoAreaService.getLegacyRootGeoAreaOfTenant(event.getTenant()),
                    clientEvent,
                    clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID));
        } catch (Exception e) {
            log.error("Failed to push TransportAlternativeBundleExpiredEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientDeliveryStatusChangedEvent.class,
            loud = false,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID")
    @PushEvent(clientEvent = ClientDeliveryStatusChangedEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID")
    @EventProcessing
    private void handleDeliveryStatusChangedEvent(DeliveryStatusChangedEvent event) {
        try {
            DeliveryStatus newStatus = event.getNewStatus();
            DeliveryStatus oldStatus = event.getOldStatus();

            if (newStatus == DeliveryStatus.IN_DELIVERY_POOLING_STATION && oldStatus == DeliveryStatus.IN_DELIVERY) {
                return;
            }
            if (newStatus == DeliveryStatus.IN_DELIVERY && oldStatus == DeliveryStatus.IN_DELIVERY_POOLING_STATION) {
                return;
            }
            if (newStatus == DeliveryStatus.POOLING_STATION_WAITING_FOR_ASSIGNMENT) {
                return;
            }
            if (newStatus == DeliveryStatus.POOLING_STATION_WAITING_FOR_PICKUP) {
                return;
            }

            String message = null;
            Delivery delivery = event.getDelivery();
            Person receiver = delivery.getReceiver().getPerson();
            if(receiver == null) return;

            TransportConfirmation deliveredConfirmation = event.getDeliveredConfirmation();

            if(newStatus == DeliveryStatus.WAITING_FOR_ASSIGNMENT && oldStatus != DeliveryStatus.WAITING_FOR_PICKUP){
                //the first waiting for assignment
                if(delivery.getSender().getShop() != null){
                    Shop shop = delivery.getSender().getShop();
                    message = "Dein Paket von "+shop.getName()+" ist versandfertig.";
                } else if (delivery.getSender().getPerson() != null){
                    message = "Dein Paket von "+delivery.getSender().getPerson().getFirstName()+" "
                            + delivery.getSender().getPerson().getLastName()+" ist versandfertig.";
                }else{
                    message = "Dein Paket ist versandfertig.";
                }
            }
            if(newStatus == DeliveryStatus.WAITING_FOR_ASSIGNMENT && oldStatus == DeliveryStatus.WAITING_FOR_PICKUP){
                //the second waiting for assignment, after a cancellation of an assignment
                //push the change silent
                message = null;
            }
            if (newStatus == DeliveryStatus.IN_DELIVERY){
                if(delivery.getSender().getShop() != null){
                    Shop shop = delivery.getSender().getShop();
                    message = "Dein Paket von "+shop.getName()+" ist unterwegs zu Dir.";
                }else{
                    message = "Dein Paket ist unterwegs zu Dir.";
                }
            }
            if(newStatus == DeliveryStatus.DELIVERED){
                if(deliveredConfirmation != null &&
                        deliveredConfirmation.getTransportAssignmentDelivered() != null &&
                        deliveredConfirmation.getTransportAssignmentDelivered().getCarrier() != null){
                    Person carrier = deliveredConfirmation.getTransportAssignmentDelivered().getCarrier();
                    message = carrier.getFirstName() + " " + carrier.getLastName() + " hat Dein Paket zugestellt.";
                }else {
                    message = "Dein Paket wurde zugestellt.";
                }
            }

            ClientDeliveryStatusChangedEvent clientEvent =
                    new ClientDeliveryStatusChangedEvent(
                            event.getDelivery().getId(),
                            mapper.createClientDeliveryStatus(newStatus),
                            mapper.createClientDeliveryStatus(oldStatus));

            if(message == null){
                clientPushService.pushToPersonSilent(receiver, clientEvent,
                        clientPushService.findCategory(
                                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID));
            } else {
                clientPushService.pushToPersonLoud(receiver, clientEvent, message,
                        clientPushService.findCategory(
                                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID));
            }
        } catch (Exception e) {
            log.error("Failed to push DeliveryStatusChangedEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientTransportDeliveredEvent.class,
            loud = false,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID")
    @PushEvent(clientEvent = ClientTransportDeliveredEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID")
    @EventProcessing
    private void handleTransportDeliveredConfirmationEvent(TransportDeliveredConfirmation event) {
        try {
            CreatorType deliveredConfirmationCreator = event.getDeliveredConfirmation().getCreator();
            if (deliveredConfirmationCreator != CreatorType.CARRIER) {
                String message = null;
                Delivery delivery = event.getDelivery();
                TransportAssignment transportAssignment = event.getTransportAssignment();
                Person carrier = transportAssignment.getCarrier();
                TransportConfirmation deliveredConfirmation = event.getDeliveredConfirmation();

                if(deliveredConfirmationCreator == CreatorType.RECEIVER){
                    Person receiver = delivery.getReceiver().getPerson();
                    if(receiver != null){
                        message = receiver.getFirstName() + " " + receiver.getLastName() + " hat deine Lieferung als erhalten markiert.";
                    } else if (delivery.getReceiver().getShop() != null){
                        message = delivery.getReceiver().getShop().getName() + " hat deine Lieferung als erhalten markiert.";
                    } else
                        message = "Der Empfänger hat deine Lieferung als erhalten markiert.";
                }
                if(deliveredConfirmationCreator == CreatorType.POOLING_STATION){
                    message = transportAssignment.getDeliveryAddress().getPoolingStation().getName() + " hat deine Lieferung als erhalten markiert.";
                }

                ClientTransportDeliveredEvent clientEvent = new ClientTransportDeliveredEvent(
                        deliveredConfirmation.getHandover(),
                        delivery.getTrackingCode(),
                        transportAssignment.getId(),
                        deliveredConfirmation.getCreator());

                if(message == null){
                    clientPushService.pushToPersonSilent(carrier, clientEvent,
                            clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID));
                } else {
                    clientPushService.pushToPersonLoud(carrier, clientEvent, message,
                            clientPushService.findCategory(
                                    LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID));
                }
            }
        } catch (Exception e) {
            log.error("Failed to push TransportDeliveredConfirmation " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientReceiverPickupAssignedEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID")
    @EventProcessing
    private void handleReceiverPickupAssignedEvent(ReceiverPickupAssignedEvent event) {
        try {
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            if (receiver == null) {
                log.warn("Can not push ReceiverPickupAssignedEvent for delivery {} to shop", delivery.getId());
            }
            PoolingStation poolingStation = event.getReceiverPickupAssignment().getAddress().getPoolingStation();

            ClientReceiverPickupAssignedEvent clientEvent = new ClientReceiverPickupAssignedEvent(delivery.getId());
            String message = "Es ist eine neue Lieferung in " + poolingStation.getName() + " zur Abholung bereit.";

            clientPushService.pushToPersonLoud(receiver, clientEvent, message,
                    clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID));
        } catch (Exception e) {
            log.error("Failed to push ReceiverPickupAssignedEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientMessageRelatedToDeliveryEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID")
    @EventProcessing
    private void handleMessageRelatedToDeliveryEvent(MessageRelatedToDeliveryEvent event) {
        try {
            Delivery delivery = event.getDelivery();
            Person receiver = delivery.getReceiver().getPerson();

            ClientMessageRelatedToDeliveryEvent
                    clientEvent = new ClientMessageRelatedToDeliveryEvent(delivery.getId(), event.getMessage());

            if (receiver == null) {
                log.warn("Can not push MessageRelatedToDeliveryEvent for delivery {} to shop", delivery.getId());
            }

            clientPushService.pushToPersonLoud(receiver, clientEvent, event.getMessage(),
                    clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID));
        } catch (Exception e) {
            log.error("Failed to push MessageRelatedToDeliveryEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientMessageRelatedToTransportAlternativeBundleEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID")
    @EventProcessing
    private void handleMessageRelatedToTransportAlternativeBundleEvent(
            MessageRelatedToTransportAlternativeBundleEvent event) {
        try {
            ClientMessageRelatedToTransportAlternativeBundleEvent clientEvent =
                    new ClientMessageRelatedToTransportAlternativeBundleEvent(
                            event.getTransportAlternativeBundle().getId(), event.getMessage());
            Tenant tenant = event.getTransportAlternativeBundle().getDelivery().getTenant();

            clientPushService.pushToGeoAreaLoud(geoAreaService.getLegacyRootGeoAreaOfTenant(tenant), clientEvent,
                    event.getMessage(),
                    clientPushService.findCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID));
        } catch (Exception e) {
            log.error("Failed to push MessageRelatedToTransportAlternativeBundleEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientMessageRelatedToTransportAssignmentEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID")
    @EventProcessing
    private void handleMessageRelatedToTransportAssignmentEvent(MessageRelatedToTransportAssignmentEvent event) {
        try {
            ClientMessageRelatedToTransportAssignmentEvent clientEvent =
                    new ClientMessageRelatedToTransportAssignmentEvent(event.getTransportAssignment().getId(),
                            event.getMessage());

            clientPushService.pushToPersonLoud(event.getTransportAssignment().getCarrier(), clientEvent,
                    event.getMessage(),
                    clientPushService.findCategory(
                            LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID));
        } catch (Exception e) {
            log.error("Failed to push MessageRelatedToTransportAssignmentEvent " + event.getLogMessage(), e);
        }
    }

    @PushEvent(clientEvent = ClientMessageRelatedToTransportAlternativeBundleEvent.class,
            categoryIdName = "LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_PERIODIC_ID")
    @EventProcessing
    private void handleTransportAlternativeBundlePeriodicNotificationExpiredEvent(
            TransportAlternativeBundlePeriodicNotificationExpiredEvent event) {
        try {
            Tenant tenant = event.getTenant();
            GeoArea rootGeoAreaOfTenant = geoAreaService.getLegacyRootGeoAreaOfTenant(event.getTenant());
            TransportAlternativeBundle newTransportAlternativeBundle = transportAlternativeService
                    .findFirstTransportAlternativeBundleForTenantSince(
                            tenant,
                            event.getPreviousDeadline());
            if (newTransportAlternativeBundle != null) {
                log.info("Checked for new transport alternative bundles for tenant '{}': found at least one",
                        tenant.getName());
                ClientMessageRelatedToTransportAlternativeBundleEvent clientEvent =
                        new ClientMessageRelatedToTransportAlternativeBundleEvent(
                                newTransportAlternativeBundle.getId(),
                                "Es sind neue Lieferungen verfügbar!");

                clientPushService.pushToGeoAreaLoud(rootGeoAreaOfTenant,
                        clientEvent,
                        clientEvent.getMessage(),
                        clientPushService.findCategory(
                                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_PERIODIC_ID));
            }else {
                log.info("Checked for new transport alternative bundles for tenant '{}': did not find any",
                        tenant.getName());
            }
        } catch (Exception e) {
            log.error("Failed to push new transport notifications "+event.getLogMessage(), e);
        }
    }

}
