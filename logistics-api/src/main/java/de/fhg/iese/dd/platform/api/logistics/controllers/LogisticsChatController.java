/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2018 Axel Wickenkamp, Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatMessage;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.AccessedAnothersResourceException;
import de.fhg.iese.dd.platform.api.logistics.exceptions.SenderNotCarrierException;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/logistics")
@Api(tags={"logistics.chat"})
class LogisticsChatController extends BaseController {

    @Autowired
    private IChatService chatService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Send a chat message as carrier")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/transport/{transportAssignmentId}/chat")
    public void sendCarrierMessage(
            @PathVariable String transportAssignmentId,
            @RequestBody ClientChatMessage chatMessage)  {

        Person carrier = getCurrentPersonNotNull();
        TransportAssignment transportAssignment = checkAssignment(carrier, transportAssignmentId);

        Chat chat = chatService.findChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, transportAssignment);
        chatService.addUserTextMessageToChat(chat, carrier, chatMessage.getMessage(), chatMessage.getSentTime());
    }

    @ApiOperation(value = "Send a chat message as receiver")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/delivery/{deliveryId}/chat")
    public void sendReceiverMessage(
            @PathVariable String deliveryId,
            @RequestBody ClientChatMessage chatMessage)  {

        Person receiver = getCurrentPersonNotNull();
        Delivery delivery = checkDelivery(receiver, deliveryId);

        Chat chat = chatService.findChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, delivery);
        chatService.addUserTextMessageToChat(chat, receiver, chatMessage.getMessage(), chatMessage.getSentTime());
    }

    @ApiOperation(value = "Get chat messages for a Delivery since timestamp from")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/delivery/{deliveryId}/chat")
    public List<ClientChatMessage> getChatMessagesDelivery(
            @PathVariable String deliveryId,
            @RequestParam long from)  {

        Person sender = getCurrentPersonNotNull();

        Delivery delivery = checkDelivery(sender, deliveryId);

        Chat chat = chatService.findChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, delivery);
        Page<ChatMessage> chatMessages = chatService.getLatestMessagesByChatAndUpdateReceived(chat, from, sender, 0, Integer.MAX_VALUE);

        return chatMessages.getContent().stream()
                .map(chatMessage -> communicationClientModelMapper.toClientChatMessage(chatMessage))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get chat messages for a TransportAssignment since timestamp from")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/transport/{transportAssignmentId}/chat")
    public List<ClientChatMessage> getChatMessagesTransportAssignment(
            @PathVariable String transportAssignmentId,
            @RequestParam long from)  {

        Person sender = getCurrentPersonNotNull();

        TransportAssignment transportAssignment = checkAssignment(sender, transportAssignmentId);

        Chat chat = chatService.findChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, transportAssignment);

        Page<ChatMessage> chatMessages = chatService.getLatestMessagesByChatAndUpdateReceived(chat, from, sender, 0, Integer.MAX_VALUE);

        return chatMessages.getContent().stream()
                .map(chatMessage -> communicationClientModelMapper.toClientChatMessage(chatMessage))
                .collect(Collectors.toList());
    }

    /**
     * Check that TransportAssignment for id exists and carrier of the assignment equals messageSender
     *
     * @param messageSender
     * @param transportAssignmentId
     * @return TransportAssignment
     */
    private TransportAssignment checkAssignment(Person messageSender, String transportAssignmentId) {

        TransportAssignment ta = transportAssignmentService.findTransportAssignmentById(transportAssignmentId);

        if ( ! ta.getCarrier().getId().equals(messageSender.getId()) )
            throw new SenderNotCarrierException();

        return ta;
    }

    /**
     * Check that Delivery for id exists and receiver of the delivery equals messageSender
     *
     * @param messageSender
     * @param deliveryId
     * @return Delivery
     */
    private Delivery checkDelivery(Person messageSender, String deliveryId) {

        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        if ( ! delivery.getReceiver().getPerson().getId().equals(messageSender.getId()))
            throw new AccessedAnothersResourceException();

        return delivery;
    }

}
