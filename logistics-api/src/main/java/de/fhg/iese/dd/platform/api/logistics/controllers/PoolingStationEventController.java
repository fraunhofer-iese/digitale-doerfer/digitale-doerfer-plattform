/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxOpenedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxReadyForAllocationResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxReadyForDeallocationResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxScannedReadyForAllocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupReceivedResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxScannedReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.ReceivedByWrongReceiverException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthenticatedException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/event")
@Api(tags={"logistics.poolingstation.events"})
class PoolingStationEventController extends BaseController {

    @Autowired
    private IPoolingStationService poolingStationService;

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @ApiOperation(value = "Signals a closed event at a poolingstation box.",
            notes = "Authorized via ApiKey")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHENTICATED
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API key is configured in d-station.api-key")
    @PostMapping("/poolingStationBoxClosed")
    public void onPoolingStationBoxClosed(
            @Valid @RequestBody ClientPoolingStationBoxClosedEvent event) {

        if (!getConfig().getDStation().getApiKey().equals(getApiKeyNotNull())) {
            throw new NotAuthenticatedException();
        }

        PoolingStationBox box = poolingStationService.findPoolingStationBoxById(event.getPoolingStationBoxId());

        PoolingStationBoxClosedEvent boxClosedEvent = new PoolingStationBoxClosedEvent(box);

        notify(boxClosedEvent);
    }

    @ApiOperation(value = "Signals a opened event at a poolingstation box.",
            notes = "Authorized via ApiKey")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHENTICATED
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API key is configured in d-station.api-key")
    @PostMapping("/poolingStationBoxOpened")
    public void onPoolingStationBoxOpened(
            @Valid @RequestBody ClientPoolingStationBoxOpenedEvent event) {

        if (!getConfig().getDStation().getApiKey().equals(getApiKeyNotNull())) {
            throw new NotAuthenticatedException();
        }

        PoolingStationBox box = poolingStationService.findPoolingStationBoxById(event.getPoolingStationBoxId());

        PoolingStationBoxOpenedEvent boxOpenedEvent = new PoolingStationBoxOpenedEvent(box);

        notify(boxOpenedEvent);
    }

    @ApiOperation(value = "Receives a ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest",
            notes = "A receiver wants to get a delivery out of the pooling station, the respective box should be opened.")
    @ApiExceptions({
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.WRONG_RECEIVER_RECEIVED,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/receiverPickupPoolingStationBoxReadyForDeallocationRequest")
    public ClientPoolingStationBoxReadyForDeallocationResponse onReceiverPickupPoolingStationBoxReadyForDeallocationRequest(
            @Valid @RequestBody ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest event) {

        Person receiver = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        ReceiverPickupPoolingStationBoxReadyForDeallocationRequest readyForDeallocationRequest
                = new ReceiverPickupPoolingStationBoxReadyForDeallocationRequest(delivery, poolingStation, receiver);

        return handleReadyForDeallocationRequest(readyForDeallocationRequest);

    }

    @ApiOperation(value = "Receives a ClientReceiverPickupReceivedPoolingStationRequest",
            notes = "A receiver just got a delivery out of the pooling station and closed the box, mark it as received.")
    @ApiExceptions({
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.WRONG_RECEIVER_RECEIVED,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/receiverPickupReceivedPoolingStationRequest")
    public ClientReceiverPickupReceivedResponse onReceiverPickupReceivedPoolingStationRequest(
            @Valid @RequestBody ClientReceiverPickupReceivedPoolingStationRequest event) {

        Person receiver = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        if (!delivery.getTrackingCode().equals(event.getDeliveryTrackingCode())) {
            throw new ReceivedByWrongReceiverException(
                    "Tracking code '" + event.getDeliveryTrackingCode() + "' does not match delivery")
                    .withDetail(event.getDeliveryTrackingCode());
        }

        ReceiverPickupReceivedPoolingStationRequest receivedRequest
                = new ReceiverPickupReceivedPoolingStationRequest(delivery, poolingStation, receiver);
        ReceiverPickupReceivedConfirmation receivedConfirmation
                = notifyAndWaitForReply(ReceiverPickupReceivedConfirmation.class, receivedRequest);

        return new ClientReceiverPickupReceivedResponse(receivedConfirmation.getDelivery().getTrackingCode());
    }

    @ApiOperation(value = "Receives an ClientPoolingStationBoxScannedReadyForAllocationRequest.",
            notes = "A carrier wants to put a parcel in a scanning pooling station, an empty box should be opened.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_ALREADY_IN_POOLING_STATION,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.POOLING_STATION_FULL
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API key is configured in d-station.api-key")
    @PostMapping("/poolingStationBoxScannedReadyForAllocationRequest")
    public ClientPoolingStationBoxReadyForAllocationResponse onPoolingStationBoxScannedReadyForAllocationRequest(
            @Valid @RequestBody ClientPoolingStationBoxScannedReadyForAllocationRequest event) {

        if (!getConfig().getDStation().getApiKey().equals(getApiKeyNotNull())) {
            throw new NotAuthenticatedException();
        }

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        PoolingStationBoxScannedReadyForAllocationRequest readyForAllocationRequest =
                new PoolingStationBoxScannedReadyForAllocationRequest(poolingStation, delivery);

        PoolingStationBoxReadyForAllocationConfirmation allocationConfirmation
                =
                notifyAndWaitForReply(PoolingStationBoxReadyForAllocationConfirmation.class, readyForAllocationRequest);

        return ClientPoolingStationBoxReadyForAllocationResponse.builder()
                .deliveryTrackingCode(allocationConfirmation.getDelivery().getTrackingCode())
                .poolingStationBoxId(allocationConfirmation.getPoolingStationBox().getId())
                .poolingStationBoxName(allocationConfirmation.getPoolingStationBox().getName())
                .poolingStationBoxType(allocationConfirmation.getPoolingStationBoxConfiguration().getBoxType())
                .timeoutForDoorOpen(allocationConfirmation.getPoolingStationBoxConfiguration().getTimeoutForDoorOpen())
                .build();

    }

    @ApiOperation(value = "Receives a ClientReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest",
            notes = "A receiver wants to get a delivery out of a scanning pooling station, the respective box should be opened.")
    @ApiExceptions({
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.WRONG_RECEIVER_RECEIVED,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API key is configured in d-station.api-key")
    @PostMapping("/receiverPickupPoolingStationBoxScannedReadyForDeallocationRequest")
    public ClientPoolingStationBoxReadyForDeallocationResponse onReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest(
            @Valid @RequestBody ClientReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest event) {

        if (!getConfig().getDStation().getApiKey().equals(getApiKeyNotNull())) {
            throw new NotAuthenticatedException();
        }

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        ReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest readyForDeallocationRequest
            = new ReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest(poolingStation, delivery);
        
        return handleReadyForDeallocationRequest(readyForDeallocationRequest);
    }

    @ApiOperation(value = "Receives a ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest",
            notes = "A carrier wants to get a delivery out of the pooling station, the respective box should be opened.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_PICKUP,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_PICKED_UP_AT_WRONG_LOCATION,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/transportPickupPoolingStationBoxReadyForDeallocationRequest")
    public ClientPoolingStationBoxReadyForDeallocationResponse onTransportPickupPoolingStationBoxReadyForDeallocationRequest(
            @Valid @RequestBody ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest event) {

        Person carrier = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException(
                    "Inconsistent data: Transport Assignment " + transportAssignment.getId() + " without delivery");
        }

        TransportPickupPoolingStationBoxReadyForDeallocationRequest readyForDeallocationRequest =
                new TransportPickupPoolingStationBoxReadyForDeallocationRequest(carrier, delivery, poolingStation,
                        transportAssignment);

        return handleReadyForDeallocationRequest(readyForDeallocationRequest);
    }

    @ApiOperation(value = "Receives a ClientTransportPickupPoolingStationRequest",
            notes = "A carrier just got a delivery out of the pooling station and closed the box, mark it as picked up.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_PICKUP,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_PICKED_UP_AT_WRONG_LOCATION,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION,
            ClientExceptionType.POOLING_STATION_BOX_NOT_CLOSED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/transportPickupPoolingStationRequest")
    public ClientTransportPickupResponse onTransportPickupPoolingStationRequest(
            @Valid @RequestBody ClientTransportPickupPoolingStationRequest event) {

        Person carrier = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException(
                    "Inconsistent data: Transport Assignment " + transportAssignment.getId() + " without delivery");
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        TransportPickupPoolingStationRequest pickupRequest
                = new TransportPickupPoolingStationRequest(carrier, delivery, poolingStation, transportAssignment);
        TransportPickupConfirmation pickupConfirmation
                = notifyAndWaitForReply(TransportPickupConfirmation.class, pickupRequest);

        return new ClientTransportPickupResponse(pickupConfirmation.getTransportAssignment().getId());
    }

    @ApiOperation(value = "Receives an ClientTransportPoolingStationBoxReadyForAllocationRequest",
            notes = "A carrier wants to put a parcel in a pooling station, an empty box should be opened.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_ALREADY_IN_POOLING_STATION,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.POOLING_STATION_FULL
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/transportPoolingStationBoxReadyForAllocationRequest")
    public ClientPoolingStationBoxReadyForAllocationResponse onTransportPoolingStationBoxReadyForAllocationRequest(
            @Valid @RequestBody ClientTransportPoolingStationBoxReadyForAllocationRequest event) {

        Person carrier = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException(
                    "Inconsistent data: Transport Assignment " + transportAssignment.getId() + " without delivery");
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        TransportPoolingStationBoxReadyForAllocationRequest readyForAllocationRequest =
                new TransportPoolingStationBoxReadyForAllocationRequest(carrier, delivery, poolingStation,
                        transportAssignment);

        PoolingStationBoxReadyForAllocationConfirmation allocationConfirmation
                =
                notifyAndWaitForReply(PoolingStationBoxReadyForAllocationConfirmation.class, readyForAllocationRequest);

        return ClientPoolingStationBoxReadyForAllocationResponse.builder()
                .deliveryTrackingCode(allocationConfirmation.getDelivery().getTrackingCode())
                .poolingStationBoxId(allocationConfirmation.getPoolingStationBox().getId())
                .poolingStationBoxName(allocationConfirmation.getPoolingStationBox().getName())
                .poolingStationBoxType(allocationConfirmation.getPoolingStationBoxConfiguration().getBoxType())
                .timeoutForDoorOpen(allocationConfirmation.getPoolingStationBoxConfiguration().getTimeoutForDoorOpen())
                .build();

    }

    @ApiOperation(value = "Receives a ClientTransportDeliveredPoolingStationRequest",
            notes = "A carrier just put a parcel in a pooling station and closed the box, mark it as delivered.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_DELIVERED,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.POOLING_STATION_BOX_NOT_CLOSED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/transportDeliveredPoolingStationRequest")
    public ClientTransportDeliveredResponse onTransportDeliveredPoolingStationRequest(
            @Valid @RequestBody ClientTransportDeliveredPoolingStationRequest event) {

        Person carrier = getCurrentPersonNotNull();

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(event.getTransportAssignmentId());

        Delivery delivery = transportAssignment.getDelivery();
        if (delivery == null) {
            throw new DeliveryNotFoundException(
                    "Inconsistent data: Transport Assignment " + transportAssignment.getId() + " without delivery");
        }

        deliveryService.checkTrackingCode(
                "delivery belonging to the TransportAssignment with the id " + event.getTransportAssignmentId(),
                delivery,
                event.getDeliveryTrackingCode());

        TransportDeliveredPoolingStationRequest deliveredPoolingStationRequest =
                new TransportDeliveredPoolingStationRequest(carrier, delivery, poolingStation, transportAssignment);

        TransportDeliveredConfirmation deliveredConfirmation
                = notifyAndWaitForReply(TransportDeliveredConfirmation.class, deliveredPoolingStationRequest);

        return ClientTransportDeliveredResponse.builder()
                .deliveryTrackingCode(deliveredConfirmation.getDelivery().getTrackingCode())
                .transportAssignmentId(transportAssignment.getId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest.",
            notes = "A carrier wants to get a delivery out of the pooling station, the respective box should be opened.")
    @ApiExceptions({
            ClientExceptionType.WRONG_CARRIER_PICKUP,
            ClientExceptionType.POOLING_STATION_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.DELIVERY_PICKED_UP_AT_WRONG_LOCATION,
            ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED,
            ClientExceptionType.DELIVERY_NOT_IN_POOLING_STATION
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API key is configured in d-station.api-key")
    @PostMapping("/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
    public ClientPoolingStationBoxReadyForDeallocationResponse onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
            @Valid @RequestBody ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest event) {

        if (!getConfig().getDStation().getApiKey().equals(getApiKeyNotNull())) {
            throw new NotAuthenticatedException();
        }

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(event.getPoolingStationId());

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        TransportAssignment transportAssignment = transportAssignmentService.getLatestTransportAssignment(delivery);
        if(transportAssignment == null) throw new TransportAssignmentNotFoundException("for delivery "+event.getDeliveryId());

        TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest readyForDeallocationRequest =
                new TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(delivery, poolingStation, transportAssignment);

        return handleReadyForDeallocationRequest(readyForDeallocationRequest);
    }

    private ClientPoolingStationBoxReadyForDeallocationResponse handleReadyForDeallocationRequest(
            BaseEvent readyForDeallocationRequest) {
        PoolingStationBoxReadyForDeallocationConfirmation deallocationConfirmation
                = notifyAndWaitForReply(PoolingStationBoxReadyForDeallocationConfirmation.class,
                readyForDeallocationRequest);

        return ClientPoolingStationBoxReadyForDeallocationResponse.builder()
                .deliveryId(deallocationConfirmation.getDelivery().getId())
                .poolingStationBoxId(deallocationConfirmation.getPoolingStationBox().getId())
                .poolingStationBoxName(deallocationConfirmation.getPoolingStationBox().getName())
                .poolingStationBoxType(deallocationConfirmation.getPoolingStationBoxConfiguration().getBoxType())
                .timeoutForDoorOpen(
                        deallocationConfirmation.getPoolingStationBoxConfiguration().getTimeoutForDoorOpen())
                .build();
    }

}
