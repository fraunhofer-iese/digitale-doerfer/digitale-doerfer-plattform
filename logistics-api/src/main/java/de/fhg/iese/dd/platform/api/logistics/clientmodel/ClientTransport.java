/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.clientmodel;

import java.util.List;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportType;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientTransport extends ClientBaseEntity {

    private ClientTransportStatusRecord currentStatus;
    private ClientLogisticsParticipant sender;
    private ClientParcelAddress pickupAddress;
    private ClientParcelAddress deliveryAddress;
    private ClientLogisticsParticipant receiver;
    private long latestPickupTime;
    private long latestDeliveryTime;
    private List<ClientTransportAlternative> transportAlternatives;
    private ClientTransportType transportType;
    private List<ClientTransportStatusRecord> statusHistory;

    /**
     * {@link Delivery#getTransportNotes()}
     */
    private String transportNotes;
    private String trackingCode;
    private ClientDimension size;
    private PackagingType packagingType;
    private int parcelCount;
    /**
     * {@link Delivery#getContentNotes()}
     */
    private String contentNotes;
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Legacy implementation")
    private TimeSpan desiredPickupTime;
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Legacy implementation")
    private TimeSpan desiredDeliveryTime;
    private boolean parcelAlreadyAvailableForPickup;
    private String deliveryId;
    private int credits;

}
