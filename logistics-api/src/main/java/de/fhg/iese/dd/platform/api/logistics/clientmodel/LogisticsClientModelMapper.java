/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.clientmodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportType;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ShopClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class LogisticsClientModelMapper {

    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private ShopClientModelMapper shopClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    public ClientDimension toClientDimension(Dimension dimension) {
        if (dimension == null) {
            return null;
        }
        return ClientDimension.builder()
                .width(dimension.getWidth())
                .length(dimension.getLength())
                .height(dimension.getHeight())
                .category(dimension.getCategory())
                .weight(dimension.getWeight())
                .build();
    }

    public Dimension fromClientDimension(ClientDimension clientDimension) {
        if (clientDimension == null) {
            return null;
        }
        return Dimension.builder()
                .width(clientDimension.getWidth())
                .length(clientDimension.getLength())
                .height(clientDimension.getHeight())
                .weight(clientDimension.getWeight())
                .build();
    }

    public ClientDelivery createClientDelivery(Delivery delivery) {

        if (delivery == null) {
            return null;
        }

        ClientDelivery cd = new ClientDelivery();
        cd.setId(delivery.getId());

        final Pair<Person, ParcelAddress> carrierAndLocation = transportAssignmentService
                .getCurrentCarrierAndLocation(delivery);

        cd.setCurrentCarrier(personClientModelMapper.createCensoredClientPerson(carrierAndLocation.getLeft()));
        ParcelAddress currentLocation = carrierAndLocation.getRight();
        if (currentLocation != null) {
            cd.setCurrentLocation(createClientParcelAddress(currentLocation));
        }

        List<DeliveryStatusRecord> deliveryStatusRecordsSortedDesc = deliveryStatusRecordRepository.findAllByDeliveryOrderByTimeStampDesc(delivery);
        DeliveryStatusRecord currentDeliveryStatus = deliveryStatusRecordsSortedDesc.isEmpty() ? null : deliveryStatusRecordsSortedDesc.get(0);
        if(currentDeliveryStatus != null){

            cd.setCurrentStatus(createClientDeliveryStatusRecord(currentDeliveryStatus));
            currentLocation = deliveryService.getCurrentLocation(currentDeliveryStatus);
            if (currentLocation != null) {
                cd.setCurrentLocation(createClientParcelAddress(currentLocation));
            }

        } else {
            log.warn("Delivery {} ({}) has no current status", delivery.getId(),
                    timeService.toLocalTimeHumanReadable(delivery.getCreated()));
        }
        cd.setDeliveryAddress(createClientParcelAddress(delivery.getDeliveryAddress()));
        cd.setDesiredDeliveryTime(delivery.getDesiredDeliveryTime());
        cd.setEstimatedDeliveryTime(delivery.getEstimatedDeliveryTime());
        cd.setPackagingType(delivery.getPackagingType());
        cd.setReceiver(createClientLogisticsParticipant(delivery.getReceiver()));
        cd.setSender(createClientLogisticsParticipant(delivery.getSender()));
        cd.setSenderAddress(createClientParcelAddress(delivery.getPickupAddress()));
        cd.setSize(toClientDimension(delivery.getSize()));
        cd.setStatusHistory(createClientDeliveryStatusRecordList(deliveryStatusRecordsSortedDesc));
        cd.setTrackingCode(delivery.getTrackingCode());
        cd.setParcelCount(delivery.getParcelCount());
        return cd;
    }

    private List<ClientDeliveryStatusRecord> createClientDeliveryStatusRecordList(List<DeliveryStatusRecord> deliveryStatusSortedDesc) {
        if(deliveryStatusSortedDesc == null) return null;

        List<ClientDeliveryStatusRecord> clientDeliveryStatusRecords = new ArrayList<>();

        MultiValueMap<DeliveryStatus, DeliveryStatusRecord> deliveryStatusRecordByStatus = new LinkedMultiValueMap<>();

        for(DeliveryStatusRecord deliveryStatusRecord : deliveryStatusSortedDesc){
            deliveryStatusRecordByStatus.add(deliveryStatusRecord.getStatus(), deliveryStatusRecord);
        }

        Pair<DeliveryStatusRecord, DeliveryStatusRecord> firstAndLastWaitingForAssignment = getFirstAndLastStatus(deliveryStatusRecordByStatus, DeliveryStatus.WAITING_FOR_ASSIGNMENT);
        Pair<DeliveryStatusRecord, DeliveryStatusRecord> firstAndLastWaitingForPickup = getFirstAndLastStatus(deliveryStatusRecordByStatus, DeliveryStatus.WAITING_FOR_PICKUP);

        DeliveryStatusRecord firstWaitingForAssignment = firstAndLastWaitingForAssignment.getLeft();
        DeliveryStatusRecord lastWaitingForAssignment = firstAndLastWaitingForAssignment.getRight();

        DeliveryStatusRecord lastWaitingForPickup = firstAndLastWaitingForPickup.getRight();

        //build the list of status records

        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.CANCELLED));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.CREATED));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, firstWaitingForAssignment);

        if(lastWaitingForPickup != null && lastWaitingForAssignment != null){
            if(lastWaitingForPickup.getTimeStamp() > lastWaitingForAssignment.getTimeStamp()){
                //the last waiting for pickup is valid, since we don't have a waiting for assignment after it
                addClientDeliveryStatusRecord(clientDeliveryStatusRecords, lastWaitingForPickup);
            }
        }

        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.IN_DELIVERY));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.IN_DELIVERY_POOLING_STATION));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.DELIVERED));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.DELIVERED_POOLING_STATION));
        addClientDeliveryStatusRecord(clientDeliveryStatusRecords, getFirstStatus(deliveryStatusRecordByStatus, DeliveryStatus.RECEIVED));

        return clientDeliveryStatusRecords;
    }

    private Pair<DeliveryStatusRecord, DeliveryStatusRecord> getFirstAndLastStatus(MultiValueMap<DeliveryStatus, DeliveryStatusRecord> deliveryStatusRecordByStatus, DeliveryStatus status){
        List<DeliveryStatusRecord> statusRecords = deliveryStatusRecordByStatus.get(status);

        if (statusRecords != null && !statusRecords.isEmpty()) {
            DeliveryStatusRecord last = statusRecords.get(0);
            DeliveryStatusRecord first = statusRecords.get(statusRecords.size() - 1);
            return new MutablePair<>(first, last);
        }
        return new MutablePair<>(null, null);
    }

    private DeliveryStatusRecord getFirstStatus(MultiValueMap<DeliveryStatus, DeliveryStatusRecord> deliveryStatusRecordByStatus, DeliveryStatus status){
        List<DeliveryStatusRecord> statusRecords = deliveryStatusRecordByStatus.get(status);

        if (statusRecords != null && !statusRecords.isEmpty()) {
            return statusRecords.get(statusRecords.size() - 1);
        }
        return null;
    }

    private List<ClientDeliveryStatusRecord> addClientDeliveryStatusRecord(List<ClientDeliveryStatusRecord> clientDeliveryStatusRecords, DeliveryStatusRecord deliveryStatusRecord){
        ClientDeliveryStatusRecord clientDeliveryStatusRecord = createClientDeliveryStatusRecord(deliveryStatusRecord);
        if(clientDeliveryStatusRecord != null){
            clientDeliveryStatusRecords.add(clientDeliveryStatusRecord);
        }
        return clientDeliveryStatusRecords;
    }

    private ClientDeliveryStatusRecord createClientDeliveryStatusRecord(DeliveryStatusRecord deliveryStatusRecord){
        if(deliveryStatusRecord == null) return null;
        ClientDeliveryStatusRecord cdsr = new ClientDeliveryStatusRecord();
        cdsr.setId(deliveryStatusRecord.getId());
        cdsr.setStatus(createClientDeliveryStatus(deliveryStatusRecord.getStatus()));
        cdsr.setTimestamp(deliveryStatusRecord.getTimeStamp());
        return cdsr;
    }

    public ClientDeliveryStatus createClientDeliveryStatus(DeliveryStatus status) {
        if(status == null) return null;
        switch (status) {
            case CANCELLED:
                return ClientDeliveryStatus.CANCELLED;
            case CREATED:
                return ClientDeliveryStatus.ORDERED;
            case WAITING_FOR_ASSIGNMENT:
                return ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT;
            case WAITING_FOR_PICKUP:
                return ClientDeliveryStatus.WAITING_FOR_PICKUP;
            case IN_DELIVERY:
                return ClientDeliveryStatus.IN_DELIVERY;
            case IN_DELIVERY_POOLING_STATION:
                return ClientDeliveryStatus.IN_DELIVERY_POOLING_STATION;
            case POOLING_STATION_WAITING_FOR_ASSIGNMENT:
                return ClientDeliveryStatus.IN_DELIVERY_POOLING_STATION;
            case POOLING_STATION_WAITING_FOR_PICKUP:
                return ClientDeliveryStatus.IN_DELIVERY_POOLING_STATION;
            case DELIVERED:
                return ClientDeliveryStatus.DELIVERED;
            case DELIVERED_POOLING_STATION:
                return ClientDeliveryStatus.DELIVERED_POOLING_STATION;
            case RECEIVED:
                return ClientDeliveryStatus.RECEIVED;
            default:
                return null;
        }
    }

    public ClientLogisticsParticipant createClientLogisticsParticipant(LogisticsParticipant logisticsParticipant) {
        if (logisticsParticipant == null) {
            return null;
        }
        ClientLogisticsParticipant clp = new ClientLogisticsParticipant();
        clp.setId(logisticsParticipant.getId());
        clp.setParticipantType(logisticsParticipant.getParticipantType());
        clp.setPerson(personClientModelMapper.createClientPerson(logisticsParticipant.getPerson()));
        clp.setShop(shopClientModelMapper.createClientShopReduced(logisticsParticipant.getShop()));
        return clp;
    }

    private ClientLogisticsParticipant createFullCensoredClientLogisticsParticipant(LogisticsParticipant logisticsParticipant) {
        if (logisticsParticipant == null) {
            return null;
        }
        ClientLogisticsParticipant clp = new ClientLogisticsParticipant();
        clp.setId(logisticsParticipant.getId());
        clp.setParticipantType(logisticsParticipant.getParticipantType());
        clp.setPerson(personClientModelMapper.createFullCensoredClientPerson(logisticsParticipant.getPerson()));
        clp.setShop(shopClientModelMapper.createClientShopReduced(logisticsParticipant.getShop()));
        return clp;
    }

    private ClientParcelAddress createClientParcelAddress(ParcelAddress parcelAddress) {
        if(parcelAddress == null) return null;
        ClientParcelAddress cpa = new ClientParcelAddress();
        cpa.setId(parcelAddress.getId());
        cpa.setAddress(addressClientModelMapper.toClientAddress(parcelAddress.getAddress()));
        cpa.setAddressType(parcelAddress.getAddressType());
        cpa.setNotes(parcelAddress.getNotes());
        cpa.setPerson(personClientModelMapper.createClientPerson(parcelAddress.getPerson()));
        cpa.setPoolingStation(createClientPoolingStationReduced(parcelAddress.getPoolingStation()));
        cpa.setPoolingStationBox(createClientPoolingStationBox(parcelAddress.getPoolingStationBox()));
        cpa.setShop(shopClientModelMapper.createClientShopReduced(parcelAddress.getShop()));

        return cpa;
    }

    private ClientParcelAddress createCensoredClientParcelAddress(ParcelAddress parcelAddress) {
        if(parcelAddress == null) return null;
        ClientParcelAddress cpa = new ClientParcelAddress();
        cpa.setId(parcelAddress.getId());
        Address address = parcelAddress.getAddress();
        address.setName(null);
        cpa.setAddress((addressClientModelMapper.toClientAddress(address)));
        cpa.setAddressType(parcelAddress.getAddressType());
        cpa.setNotes(parcelAddress.getNotes());
        cpa.setPerson(personClientModelMapper.createFullCensoredClientPerson(parcelAddress.getPerson()));
        cpa.setPoolingStation(createClientPoolingStationReduced(parcelAddress.getPoolingStation()));
        cpa.setPoolingStationBox(createClientPoolingStationBox(parcelAddress.getPoolingStationBox()));
        cpa.setShop(shopClientModelMapper.createClientShopReduced(parcelAddress.getShop()));

        return cpa;
    }

    private ClientPoolingStation createClientPoolingStationReduced(PoolingStation poolingStation) {
        if(poolingStation == null) return null;
        ClientPoolingStation cps = new ClientPoolingStation();
        cps.setId(poolingStation.getId());
        cps.setAddress(addressClientModelMapper.toClientAddress(poolingStation.getAddress()));
        cps.setName(poolingStation.getName());
        cps.setProfilePicture(fileClientModelMapper.toClientMediaItem(poolingStation.getProfilePicture()));
        //cps.setOpeningHours(poolingStation.getOpeningHours());
        cps.setStationType(poolingStation.getStationType());
        cps.setVerificationCode(poolingStation.getVerificationCode());
        cps.setSelfScanningStation(poolingStation.isSelfScanningStation());
        return cps;
    }

    public ClientPoolingStation createClientPoolingStation(PoolingStation poolingStation) {
        if(poolingStation == null) return null;
        ClientPoolingStation cps = new ClientPoolingStation();
        cps.setId(poolingStation.getId());
        cps.setAddress(addressClientModelMapper.toClientAddress(poolingStation.getAddress()));
        cps.setName(poolingStation.getName());
        cps.setProfilePicture(fileClientModelMapper.toClientMediaItem(poolingStation.getProfilePicture()));
        cps.setOpeningHours(poolingStation.getOpeningHours());
        cps.setStationType(poolingStation.getStationType());
        cps.setVerificationCode(poolingStation.getVerificationCode());
        cps.setSelfScanningStation(poolingStation.isSelfScanningStation());
        return cps;
    }

    private ClientPoolingStationBox createClientPoolingStationBox(PoolingStationBox poolingStationBox) {
        if(poolingStationBox == null) return null;
        ClientPoolingStationBox cpsb = new ClientPoolingStationBox();
        cpsb.setId(poolingStationBox.getId());
        cpsb.setName(poolingStationBox.getName());
        return cpsb;
    }

    private ClientTransportAlternative createClientTransportAlternative(TransportAlternative transportAlternative){
        if(transportAlternative==null) return null;
        ClientTransportAlternative cta = new ClientTransportAlternative();
        cta.setId(transportAlternative.getId());
        cta.setCredits(transportAlternative.getCredits());
        cta.setDeliveryAddress(createClientParcelAddress(transportAlternative.getDeliveryAddress()));
        cta.setDesiredDeliveryTime(transportAlternative.getDesiredDeliveryTime());
        cta.setDesiredPickupTime(transportAlternative.getDesiredDeliveryTime());
        cta.setKind(transportAlternative.getKind());
        cta.setLatestPickupTime(transportAlternative.getLatestPickupTime());

        return cta;
    }

    private ClientTransportAlternative createCensoredClientTransportAlternative(TransportAlternative transportAlternative){
        if(transportAlternative==null) return null;
        ClientTransportAlternative cta = new ClientTransportAlternative();
        cta.setId(transportAlternative.getId());
        cta.setCredits(transportAlternative.getCredits());
        cta.setDeliveryAddress(createCensoredClientParcelAddress(transportAlternative.getDeliveryAddress()));
        cta.setDesiredDeliveryTime(transportAlternative.getDesiredDeliveryTime());
        cta.setDesiredPickupTime(transportAlternative.getDesiredDeliveryTime());
        cta.setKind(transportAlternative.getKind());
        cta.setLatestPickupTime(transportAlternative.getLatestPickupTime());

        return cta;
    }

      public ClientTransport createClientTransportFromTransportAssignment(TransportAssignment transportAssignment) {
          if (transportAssignment == null) {
              return null;
          }
          Delivery delivery = transportAssignment.getDelivery();
          List<TransportAssignmentStatusRecord> transportAssignmentStatusRecords =
                  transportAssignmentStatusRecordRepository.
                          findAllByTransportAssignmentIdOrderByTimestampDesc(transportAssignment.getId());

          TransportAssignmentStatusRecord currentAssignmentStatus =
                  transportAssignmentStatusRecords.isEmpty() ? null : transportAssignmentStatusRecords.get(0);
          if (currentAssignmentStatus == null) {
              log.warn("TransportAssignment {} ({}) has no current status", transportAssignment.getId(),
                      timeService.toLocalTimeHumanReadable(transportAssignment.getCreated()));
          }

          ClientTransport clientTransport = new ClientTransport();
          clientTransport.setId(transportAssignment.getId());
          clientTransport.setCurrentStatus(createClientTransportStatusRecord(currentAssignmentStatus));
          clientTransport.setDeliveryAddress(createClientParcelAddress(transportAssignment.getDeliveryAddress()));
          clientTransport.setContentNotes(delivery.getContentNotes());
          clientTransport.setDesiredDeliveryTime(transportAssignment.getDesiredDeliveryTime());
          clientTransport.setDesiredPickupTime(transportAssignment.getDesiredPickupTime());
          clientTransport.setLatestDeliveryTime(transportAssignment.getLatestDeliveryTime());
          clientTransport.setLatestPickupTime(transportAssignment.getLatestPickupTime());
          clientTransport.setPackagingType(delivery.getPackagingType());
          clientTransport.setPickupAddress(createClientParcelAddress(transportAssignment.getPickupAddress()));
          clientTransport.setReceiver(createClientLogisticsParticipant(delivery.getReceiver()));
          clientTransport.setSender(createClientLogisticsParticipant(delivery.getSender()));
          clientTransport.setSize(toClientDimension(delivery.getSize()));
          clientTransport.setTrackingCode(delivery.getTrackingCode());
          clientTransport.setTransportNotes(delivery.getTransportNotes());
          clientTransport.setTransportType(ClientTransportType.TRANSPORT_ASSIGNMENT);
          clientTransport.setDeliveryId(delivery.getId());
          clientTransport.setParcelAlreadyAvailableForPickup(currentAssignmentStatus != null && currentAssignmentStatus.getStatus() ==  TransportAssignmentStatus.WAITING_FOR_PICKUP);
          clientTransport.setCredits(transportAssignment.getCredits());
          clientTransport.setParcelCount(delivery.getParcelCount());

          clientTransport.setStatusHistory(createClientTransportStatusRecordList(transportAssignmentStatusRecords));
          return clientTransport;
      }

      private ClientTransportStatusRecord createClientTransportStatusRecord(TransportAssignmentStatusRecord statusRecord) {
          if(statusRecord == null) return null;
          ClientTransportStatusRecord ctsr = new ClientTransportStatusRecord();
          ctsr.setId(statusRecord.getId());
          ctsr.setTimestamp(statusRecord.getTimestamp());
          switch (statusRecord.getStatus()){
            case WAITING_FOR_ASSIGNMENT:
                ctsr.setStatus(ClientTransportStatus.AVAILABLE);
                break;
            case DELIVERED:
            case RECEIVED:
                ctsr.setStatus(ClientTransportStatus.DELIVERED);
                break;
            case IN_DELIVERY:
                ctsr.setStatus(ClientTransportStatus.PICKED_UP);
                break;
            case NOT_THERE_YET:
            case WAITING_FOR_PICKUP:
                ctsr.setStatus(ClientTransportStatus.ACCEPTED);
                break;
            case REJECTED:
            case CANCELLED:
                ctsr.setStatus(ClientTransportStatus.CANCELLED);
                break;
            default:
                return null;
        }
          return ctsr;
    }

    private List<ClientTransportStatusRecord> createClientTransportStatusRecordList(List<TransportAssignmentStatusRecord> transportAssignmentStatusSortedDesc) {
        List<ClientTransportStatusRecord> clientTransportStatusRecords = new ArrayList<>();

        Set<TransportAssignmentStatus> seenStatuses = new HashSet<>();
        for(TransportAssignmentStatusRecord record : transportAssignmentStatusSortedDesc){
            if(!seenStatuses.contains(record.getStatus())){
                ClientTransportStatusRecord ctsr = createClientTransportStatusRecord(record);
                if(ctsr != null){
                    clientTransportStatusRecords.add(ctsr);
                }
                seenStatuses.add(record.getStatus());
            }
        }
        return clientTransportStatusRecords;
    }

    public ClientTransport createClientTransportFromTransportAlternativeBundleForPotentialCarrier(
            TransportAlternativeBundle transportAlternativeBundle, Person potentialCarrier){
        if(transportAlternativeBundle == null) return null;
        Delivery delivery = transportAlternativeBundle.getDelivery();
        List<TransportAlternative> transportAlternatives =  transportAlternativeService.findAllAlternativesForPotentialCarrier(transportAlternativeBundle, potentialCarrier);

        TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus = transportAlternativeService.getCurrentStatusRecord(transportAlternativeBundle);

        if (currentAlternativeBundleStatus == null) {
            log.warn("TransportAssignment {} ({}) has no current status", transportAlternativeBundle.getId(),
                    timeService.toLocalTimeHumanReadable(transportAlternativeBundle.getCreated()));
        }

        ClientTransport clientTransport = new ClientTransport();
        clientTransport.setId(transportAlternativeBundle.getId());

        setClientTransportStatus(clientTransport, currentAlternativeBundleStatus);

        clientTransport.setDeliveryAddress(createClientParcelAddress(delivery.getDeliveryAddress()));
        clientTransport.setContentNotes(delivery.getContentNotes());
        Optional<TransportAlternative> receiverTransportAlternative = transportAlternatives.stream()
                .filter(ta->TransportKind.RECEIVER == ta.getKind()).findFirst();
        if(receiverTransportAlternative.isPresent()){
            clientTransport.setDesiredDeliveryTime(receiverTransportAlternative.get().getDesiredDeliveryTime());
            clientTransport.setDesiredPickupTime(receiverTransportAlternative.get().getDesiredPickupTime());
            clientTransport.setLatestDeliveryTime(receiverTransportAlternative.get().getLatestDeliveryTime());
            clientTransport.setLatestPickupTime(receiverTransportAlternative.get().getLatestPickupTime());
        }
        clientTransport.setPackagingType(delivery.getPackagingType());
        clientTransport.setPickupAddress(createClientParcelAddress(transportAlternativeBundle.getPickupAddress()));
        clientTransport.setReceiver(createClientLogisticsParticipant(delivery.getReceiver()));
        clientTransport.setSender(createClientLogisticsParticipant(delivery.getSender()));
        clientTransport.setSize(toClientDimension(delivery.getSize()));
        clientTransport.setTrackingCode(delivery.getTrackingCode());
        clientTransport.setTransportType(ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE);

        clientTransport.setTransportAlternatives(createClientTransportAlternativeList(transportAlternatives));
        clientTransport.setCredits(clientTransport.getTransportAlternatives().stream().
            flatMapToInt(alternative -> IntStream.of(alternative.getCredits()))
            .max()
            .orElse(0));
        if(currentAlternativeBundleStatus != null){
            clientTransport.setParcelAlreadyAvailableForPickup(currentAlternativeBundleStatus.isParcelAlreadyAvailableForPickup());
        }else{
            clientTransport.setParcelAlreadyAvailableForPickup(true);
        }
        return clientTransport;
    }

    public ClientTransport createCensoredClientTransportFromTransportAlternativeBundle(TransportAlternativeBundle transportAlternativeBundle){
        if(transportAlternativeBundle == null) return null;
        Delivery delivery = transportAlternativeBundle.getDelivery();
        List<TransportAlternative> transportAlternatives =  transportAlternativeService.findTransportAlternativesByBundle(transportAlternativeBundle);

        TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus = transportAlternativeService.getCurrentStatusRecord(transportAlternativeBundle);

        if(currentAlternativeBundleStatus == null) {
            log.warn("TransportAssignment {} ({}) has no current status", transportAlternativeBundle.getId(),
                    timeService.toLocalTimeHumanReadable(transportAlternativeBundle.getCreated()));
        }

        ClientTransport clientTransport = new ClientTransport();
        clientTransport.setId(transportAlternativeBundle.getId());

        setClientTransportStatus(clientTransport, currentAlternativeBundleStatus);

        clientTransport.setDeliveryAddress(createCensoredClientParcelAddress(delivery.getDeliveryAddress()));
        clientTransport.setContentNotes(delivery.getContentNotes());
        Optional<TransportAlternative> receiverTransportAlternative = transportAlternatives.stream()
                .filter(ta->TransportKind.RECEIVER == ta.getKind()).findFirst();
        if(receiverTransportAlternative.isPresent()){
            clientTransport.setDesiredDeliveryTime(receiverTransportAlternative.get().getDesiredDeliveryTime());
            clientTransport.setDesiredPickupTime(receiverTransportAlternative.get().getDesiredPickupTime());
            clientTransport.setLatestDeliveryTime(receiverTransportAlternative.get().getLatestDeliveryTime());
            clientTransport.setLatestPickupTime(receiverTransportAlternative.get().getLatestPickupTime());
        }
        clientTransport.setPackagingType(delivery.getPackagingType());
        clientTransport.setPickupAddress(createClientParcelAddress(transportAlternativeBundle.getPickupAddress()));
        clientTransport.setReceiver(createFullCensoredClientLogisticsParticipant(delivery.getReceiver()));
        clientTransport.setSender(createClientLogisticsParticipant(delivery.getSender()));
        clientTransport.setSize(toClientDimension(delivery.getSize()));
        clientTransport.setTrackingCode(delivery.getTrackingCode());
        clientTransport.setTransportType(ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE);

        clientTransport.setTransportAlternatives(createCensoredClientTransportAlternativeList(transportAlternatives));
        clientTransport.setCredits(clientTransport.getTransportAlternatives().stream()
            .flatMapToInt(alternative -> IntStream.of(alternative.getCredits()))
            .max()
            .orElse(0));
        clientTransport.setParcelCount(delivery.getParcelCount());

        if(currentAlternativeBundleStatus != null){
            clientTransport.setParcelAlreadyAvailableForPickup(currentAlternativeBundleStatus.isParcelAlreadyAvailableForPickup());
        }else{
            clientTransport.setParcelAlreadyAvailableForPickup(true);
        }
        return clientTransport;
    }

    private void setClientTransportStatus(ClientTransport clientTransport, TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus) {

        ClientTransportStatusRecord clientTransportStatusRecord = new ClientTransportStatusRecord();
        clientTransportStatusRecord.setStatus(ClientTransportStatus.AVAILABLE);
        if(currentAlternativeBundleStatus != null){
            clientTransportStatusRecord.setTimestamp(currentAlternativeBundleStatus.getTimestamp());
            clientTransportStatusRecord.setId(currentAlternativeBundleStatus.getId());
        }
        clientTransport.setCurrentStatus(clientTransportStatusRecord);

        List<ClientTransportStatusRecord> clientTransportStatusHistory = new ArrayList<>(1);
        clientTransportStatusHistory.add(clientTransportStatusRecord);
        clientTransport.setStatusHistory(clientTransportStatusHistory);
    }

    private List<ClientTransportAlternative> createClientTransportAlternativeList(List<TransportAlternative> transportAlternatives) {
        if(transportAlternatives == null) {
            return null;
        }
        Stream<ClientTransportAlternative> ctaList = transportAlternatives.stream().map(this::createClientTransportAlternative);
        //we want to have "RECEIVER" alternatives on the top, relevant for tests
        return ctaList.sorted((i1, i2) -> Integer.compare(i2.getKind().ordinal(), i1.getKind().ordinal()))
            .collect(Collectors.toList());
    }

   private List<ClientTransportAlternative> createCensoredClientTransportAlternativeList(List<TransportAlternative> transportAlternatives) {
        if(transportAlternatives == null) {
            return null;
        }
        Stream<ClientTransportAlternative> ctaList = transportAlternatives.stream().map(this::createCensoredClientTransportAlternative);
        //we want to have "RECEIVER" alternatives on the top, relevant for tests
        return ctaList.sorted((i1, i2) -> Integer.compare(i2.getKind().ordinal(), i1.getKind().ordinal()))
            .collect(Collectors.toList());
    }

}
