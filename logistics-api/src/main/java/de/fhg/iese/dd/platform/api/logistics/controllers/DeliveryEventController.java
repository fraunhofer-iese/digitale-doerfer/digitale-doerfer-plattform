/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryReceivedRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryReceivedResponse;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.DuplicateEvent;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.UnexpectedEventException;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.exceptions.WrongTrackingCodeForDeliveryException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/event")
@Api(tags={"logistics.delivery.events"})
class DeliveryEventController extends BaseController {

    @Autowired
    private IDeliveryService deliveryService;

    @Autowired
    private IAuthorizationService authorizationService;

    @ApiOperation(value = "Receives a ClientDeliveryReceivedRequest.")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/deliveryReceivedRequest")
    public ClientDeliveryReceivedResponse onDeliveryReceivedRequest(
            @Valid @RequestBody ClientDeliveryReceivedRequest event) {

        Person receiver = getCurrentPersonNotNull();
        // can not be checked via validation since the ClientDeliveryReceivedRequest is also used in
        // onDeliveryReceivedRequestMail():86 where 'deliveryTrackingCode' is not needed
        checkAttributeNotNullOrEmpty(event.getDeliveryTrackingCode(), "deliveryTrackingCode");

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());

        if (!StringUtils.equals(delivery.getTrackingCode(), event.getDeliveryTrackingCode())) {
            throw new WrongTrackingCodeForDeliveryException(
                    "The deliveryTrackingCode {} does not match the delivery {}!", event.getDeliveryTrackingCode(),
                    event.getDeliveryId());
        }
        DeliveryReceivedRequest deliveryReceivedRequest = new DeliveryReceivedRequest(delivery, receiver);

        return handleDeliveryReceivedRequest(deliveryReceivedRequest);
    }

    @ApiOperation(value = "Receives a ClientDeliveryReceivedRequest that was requested via mail.")
    @ApiExceptions({
            ClientExceptionType.WRONG_TRACKING_CODE,
            ClientExceptionType.DELIVERY_NOT_FOUND,
    })
    @ApiAuthentication(value = ApiAuthenticationType.ONE_TIME_TOKEN, notes = "Authorization is done via mail token")
    @PostMapping("/deliveryReceivedRequest/mail")
    public ClientDeliveryReceivedResponse onDeliveryReceivedRequestMail(
            @RequestParam(required = true) String mailToken,
            @Valid @RequestBody ClientDeliveryReceivedRequest event) {

        Delivery delivery = deliveryService.findDeliveryById(event.getDeliveryId());
        Person receiver = delivery.getReceiver().getPerson();

        DeliveryReceivedRequest deliveryReceivedRequest = new DeliveryReceivedRequest(delivery, receiver);

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, deliveryReceivedRequest);

        return handleDeliveryReceivedRequest(deliveryReceivedRequest);
    }

    private ClientDeliveryReceivedResponse handleDeliveryReceivedRequest(
            DeliveryReceivedRequest deliveryReceivedRequest) {
        BaseEvent resultEvent = notifyAndWaitForAnyReply(deliveryReceivedRequest, DeliveryReceivedConfirmation.class,
                DuplicateEvent.class);
        DeliveryReceivedConfirmation receivedConfirmation;

        if (resultEvent instanceof DeliveryReceivedConfirmation) {
            receivedConfirmation = (DeliveryReceivedConfirmation) resultEvent;
        } else if (resultEvent instanceof DuplicateEvent<?>) {
            receivedConfirmation =
                    ((DuplicateEvent<?>) resultEvent).getDuplicatedEvent(DeliveryReceivedConfirmation.class);
        } else {
            throw new UnexpectedEventException();
        }

        return ClientDeliveryReceivedResponse.builder()
                .deliveryTrackingCode(receivedConfirmation.getDelivery().getTrackingCode())
                .deliveryId(receivedConfirmation.getDelivery().getId())
                .build();
    }

}
