/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransport;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeBundleNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentOrAlternativeNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportNotAssignedToCurrentPersonException;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("logistics/transport")
@Api(tags={"logistics.transport"})
class TransportController extends BaseController {

    @Autowired
    private LogisticsClientModelMapper mapper;

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private IAppService appService;

    @ApiOperation(value = "Returns all available ClientTransports",
            notes = """
                    ClientTransport are combining TransportAssignment or AlternativeBundle.\s

                     Can be called unauthorized, the transports are censored in this case. TransportAssignments are only returned if the user is carrier.\s

                     All AlternativeBundles are returned where the user can bid on to be a carrier, which are all with status Waiting For Assignment in that tenant. Only the AlternativeBundles that were not expired (desiredDeliveryTimeEnd of the corresponding Delivery) alternativeBundleExpiredHours ago are shown. Default value is 3 hours.""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2_OPTIONAL)
    @GetMapping
    public List<ClientTransport> getAllTransports(
            @ApiParam(value = "When not set, the IDs of the tenants/communities of all subscribed geo areas are used. If unauthorized, this parameter must be set")
            @RequestParam(required = false)
                    String communityId,
            @ApiParam(value = "Ignored when requested without authorization, since no assignments are available.")
            @RequestParam(required = false, defaultValue = "20")
                    int transportAssignmentSinceDays,
            @RequestParam(required = false, defaultValue = "3")
                    int alternativeBundleExpiredHours) {

        long startTime = System.nanoTime();

        //if the call was made anonymous the requester is null
        final Person requester = getCurrentPersonOrNull();

        Set<Tenant> tenants = null;

        if(StringUtils.isNotBlank(communityId)){
            tenants = Collections.singleton(tenantService.findTenantById(communityId));
        } else {
            if(requester == null) throw new TenantNotFoundException(communityId);
            final AppVariant appVariant = getAppVariantNotNull();
            tenants = appService.getTenantsOfSelectedGeoAreas(appVariant, requester);
        }

        if(tenants.isEmpty()) throw new TenantNotFoundException(communityId);

        if (alternativeBundleExpiredHours <= 0) {
            alternativeBundleExpiredHours = 3;
        }
        long expiredTimestamp = Instant.now().minus(Duration.ofHours(alternativeBundleExpiredHours)).toEpochMilli();

        if (transportAssignmentSinceDays <= 0) {
            transportAssignmentSinceDays = 20;
        }
        long sinceTimestamp = Instant.now().minus(Duration.ofDays(transportAssignmentSinceDays)).toEpochMilli();

        Collection<TransportAssignment> transportAssignments = Collections.emptyList();

        Collection<TransportAlternativeBundle> transportAlternativeBundles =
                transportAlternativeService.findAllAlternativeBundleWaitingForAssignmentNotExpiredSince(
                        tenants, expiredTimestamp);

        Stream<ClientTransport> transportAssignmentsStream = Stream.empty();
        Stream<ClientTransport> transportAlternativeBundlesStream = Stream.empty();
        if (requester != null) {
            //authorized request, more details and potential assignments
            transportAssignments = transportAssignmentService.findAllNonCancelledByCarrier(requester, sinceTimestamp);
            transportAssignmentsStream =
                    transportAssignments.stream().map(mapper::createClientTransportFromTransportAssignment);
            transportAlternativeBundlesStream = transportAlternativeBundles.stream().map(
                    tab -> mapper.createClientTransportFromTransportAlternativeBundleForPotentialCarrier(tab,
                            requester));
        } else {
            //anonymous request
            transportAlternativeBundlesStream = transportAlternativeBundles.stream().map(
                    mapper::createCensoredClientTransportFromTransportAlternativeBundle);
        }

        List<ClientTransport> result = Stream.concat(transportAssignmentsStream, transportAlternativeBundlesStream)
                .sorted(Comparator.comparingLong(o -> o.getCurrentStatus().getTimestamp())).collect(Collectors.toList());

        long elapsedTime = System.nanoTime() - startTime;

        log.debug("Returned {} TransportAssignment, {} TransportAlternativeBundle in {} ms", transportAssignments.size(), transportAlternativeBundles.size(), TimeUnit.NANOSECONDS.toMillis(elapsedTime));
        return result;
    }

    @ApiOperation(value = "Returns a single ClientTransport",
            notes = """
                    ClientTransport are combining TransportAssignment or AlternativeBundle.\s

                     Be aware that the Id is referring either one of them.""")
    @ApiExceptions({
            ClientExceptionType.TRANSPORT_ALTERNATIVE_BUNDLE_NOT_FOUND,
            ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND,
            ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2_OPTIONAL)
    @GetMapping("/{clientTransportId}")
    public ClientTransport getOneTransport(
            @PathVariable String clientTransportId) {

        //if the call was made anonymous the requester is null
        Person requester = getCurrentPersonOrNull();

        try {
            TransportAlternativeBundle transportAlternativeBundle =
                    transportAlternativeService.findTransportAlternativeBundleById(clientTransportId);
            TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus =
                    transportAlternativeService.getCurrentStatusRecord(transportAlternativeBundle);

            if (currentAlternativeBundleStatus != null &&
                    currentAlternativeBundleStatus.getStatus() == TransportAlternativeBundleStatus.EXPIRED) {
                throw new TransportAlternativeBundleNotFoundException(clientTransportId);
            }
            if(requester != null){
                //authorized request, more details
                return mapper.createClientTransportFromTransportAlternativeBundleForPotentialCarrier(transportAlternativeBundle, requester);
            }else{
                //anonymous request
                return mapper.createCensoredClientTransportFromTransportAlternativeBundle(transportAlternativeBundle);
            }

        } catch (TransportAlternativeBundleNotFoundException e) {
          //ignore it, since it might be an assignment bundle
        }

        if(requester != null){
            try {
                TransportAssignment transportAssignment = transportAssignmentService.findTransportAssignmentById(clientTransportId);
                if (!Objects.equals(transportAssignment.getCarrier(), requester)) {
                    throw new TransportNotAssignedToCurrentPersonException();
                }
                return mapper.createClientTransportFromTransportAssignment(transportAssignment);
            } catch (TransportAssignmentNotFoundException e) {
                //ignore it, since we will throw an exception anyway
            }
        }
        throw new TransportAssignmentOrAlternativeNotFoundException(clientTransportId);
    }

}
