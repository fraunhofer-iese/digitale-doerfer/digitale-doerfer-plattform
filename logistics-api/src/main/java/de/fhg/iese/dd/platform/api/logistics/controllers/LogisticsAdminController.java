/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageReceivedEvent;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientevent.*;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.shared.misc.clientevent.ClientQrCodeLabelCreateRequest;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToDeliveryEvent;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToTransportAlternativeBundleEvent;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToTransportAssignmentEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenRequest;
import de.fhg.iese.dd.platform.business.logistics.services.*;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.*;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/administration/logistics")
@Api(tags = {"admin","logistics.admin"})
class LogisticsAdminController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private IPoolingStationService poolingStationService;
    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IDemoLogisticsService demoLogisticsService;
    @Autowired
    private IQRCodeService qrCodeService;

    @ApiOperation(value = "Set Delivery and associated entities to CANCELLED",
            notes = """
                    Delivery and associated Assignments, AlternativeBundles, Alternatives

                    This does not notify anyone, only the status is set to CANCELLED or EXPIRED.

                    Do NOT use if sender, receiver or carrier do not know that you are going to do that!""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping("cancelDelivery/{deliveryId}")
    public void cancelDelivery(@PathVariable String deliveryId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);
        //the event is only used for the status records
        deliveryService.cancelDeliveryAndAllRelatedEntities(delivery,
                new MessageRelatedToDeliveryEvent(delivery, "manual cancellation"));
    }

    @ApiOperation(value = "Send push notifications related to a delivery",
            notes = "Send out to receiver")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping("event/messagerelatedto/delivery")
    public String sendMessageRelatedToDelivery(
            @RequestParam String deliveryId,
            @RequestParam String message) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);
        notify(new MessageRelatedToDeliveryEvent(delivery, message));
        return "Sent MessageRelatedToDeliveryEvent";
    }

    @ApiOperation(value = "Send push notifications related to a TransportAlternativeBundle",
            notes = "Send out to tenant")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping("event/messagerelatedto/transportAlternativeBundle")
    public String sendMessageRelatedToTransportAlternativeBundle(
            @RequestParam String transportAlternativeBundleId,
            @RequestParam String message) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        TransportAlternativeBundle transportAlternativeBundle = transportAlternativeService
                .findTransportAlternativeBundleById(transportAlternativeBundleId);
        notify(new MessageRelatedToTransportAlternativeBundleEvent(transportAlternativeBundle, message));
        return "Sent MessageRelatedToTransportAlternativeBundleEvent";
    }

    @ApiOperation(value = "Send push notifications related to a TransportAlternative",
            notes = "Send out to tenant")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping("event/messagerelatedto/transportAlternative")
    public String sendMessageRelatedToTransportAlternative(
            @RequestParam String transportAlternativeId,
            @RequestParam String message) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        TransportAlternative transportAlternative = transportAlternativeService.findTransportAlternativeById(transportAlternativeId);
        notify(new MessageRelatedToTransportAlternativeBundleEvent(transportAlternative.getTransportAlternativeBundle(), message));
        return "Sent MessageRelatedToTransportAlternativeBundleEvent";
    }

    @ApiOperation(value = "Send push notifications related to a TransportAssignment",
            notes = "Send out to carrier")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping("event/messagerelatedto/transportAssignment")
    public String sendMessageRelatedToTransportAssignment(
            @RequestParam String transportAssignmentId,
            @RequestParam String message) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        TransportAssignment transportAssignment =
                transportAssignmentService.findTransportAssignmentById(transportAssignmentId);
        notify(new MessageRelatedToTransportAssignmentEvent(transportAssignment, message));
        return "Sent MessageRelatedToTransportAssignmentEvent";
    }

    @ApiOperation(value = "Send test push notification types to a Person",
            notes = "Only sends push notifications for LieferBar categories")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("notification/send/testall/person")
    public void sendTestNotificationsToPerson(
            @RequestParam("personId") String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        Person person = personService.findPersonById(personId);

        PushCategory categoryNewTransports =
                pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID);
        PushCategory categoryNewTransportsPeriodic = pushCategoryService.findPushCategoryById(
                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_PERIODIC_ID);
        PushCategory categoryDeliveryStatusReceiver = pushCategoryService.findPushCategoryById(
                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID);
        PushCategory categoryPickupAssignment = pushCategoryService.findPushCategoryById(
                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID);
        PushCategory categoryTransportStatusCarrier = pushCategoryService.findPushCategoryById(
                LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID);

        Collection<TransportAlternativeBundle> transportAlternativeBundles =
                transportAlternativeService.findAllAlternativeBundles();
        Optional<TransportAlternativeBundle> transportAlternativeBundleForSelection =
                transportAlternativeBundles.stream()
                        .filter(tab -> transportAlternativeService.getCurrentStatusRecord(tab).getStatus()
                                == TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT)
                        .findAny();
        if (transportAlternativeBundleForSelection.isEmpty()) {
            transportAlternativeBundleForSelection = transportAlternativeBundles.stream().findAny();
        }

        Optional<Delivery> delivery = deliveryService.findAllDeliveries().stream()
                .filter(d -> {DeliveryStatus currentStatus = deliveryService.getCurrentStatusRecord(d).getStatus();
                return currentStatus != DeliveryStatus.CREATED &&
                       currentStatus != DeliveryStatus.WAITING_FOR_ASSIGNMENT;}).findAny();

        Optional<TransportAssignment> transportAssignment = transportAssignmentService.findAllNonCancelledByCarrier(person, 0).stream().findAny();
        if (transportAssignment.isEmpty()) {
            transportAssignment = transportAssignmentService.findAllTransportAssignments().stream().findAny();
        }

        if (transportAlternativeBundleForSelection.isPresent()) {
            ClientTransportAlternativeBundleForSelectionEvent clientTransportAlternativeBundleForSelectionEvent =
                    new ClientTransportAlternativeBundleForSelectionEvent(transportAlternativeBundleForSelection.get().getId());
            clientPushService.pushToPersonLoud(person, clientTransportAlternativeBundleForSelectionEvent, "Test: ClientTransportAlternativeBundleForSelectionEvent", categoryNewTransports);
            clientPushService.pushToPersonLoud(person, clientTransportAlternativeBundleForSelectionEvent, "Test: ClientTransportAlternativeBundleForSelectionEvent", categoryNewTransportsPeriodic);
        }
        if (transportAlternativeBundleForSelection.isPresent()) {
            ClientTransportAlternativeBundleExpiredEvent clientTransportAlternativeBundleExpiredEvent =
                    new ClientTransportAlternativeBundleExpiredEvent(transportAlternativeBundleForSelection.get().getId());
            clientPushService.pushToPersonSilent(person, clientTransportAlternativeBundleExpiredEvent, categoryNewTransports);
        }
        if (delivery.isPresent()) {
            ClientDeliveryStatusChangedEvent clientDeliveryStatusChangedEvent =
                    new ClientDeliveryStatusChangedEvent(delivery.get().getId(), ClientDeliveryStatus.DELIVERED, ClientDeliveryStatus.IN_DELIVERY);
            clientPushService.pushToPersonLoud(person, clientDeliveryStatusChangedEvent, "Test: ClientDeliveryStatusChangedEvent", categoryDeliveryStatusReceiver);

            clientDeliveryStatusChangedEvent =
                    new ClientDeliveryStatusChangedEvent(delivery.get().getId(), ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT, ClientDeliveryStatus.ORDERED);
            clientPushService.pushToPersonLoud(person, clientDeliveryStatusChangedEvent, "Test: ClientDeliveryStatusChangedEvent", categoryDeliveryStatusReceiver);

            clientDeliveryStatusChangedEvent =
                    new ClientDeliveryStatusChangedEvent(delivery.get().getId(), ClientDeliveryStatus.IN_DELIVERY, ClientDeliveryStatus.WAITING_FOR_PICKUP);
            clientPushService.pushToPersonLoud(person, clientDeliveryStatusChangedEvent,
                    "Test: ClientDeliveryStatusChangedEvent", categoryDeliveryStatusReceiver);

            ClientReceiverPickupAssignedEvent clientReceiverPickupAssignedEvent =
                    new ClientReceiverPickupAssignedEvent(delivery.get().getId());
            clientPushService.pushToPersonLoud(person, clientReceiverPickupAssignedEvent,
                    "Test: ClientReceiverPickupAssignedEvent", categoryDeliveryStatusReceiver);

            ClientChatMessageReceivedEvent clientChatMessageReceivedEvent = ClientChatMessageReceivedEvent.builder()
                    .chatId(UUID.randomUUID().toString())
                    .messageId(UUID.randomUUID().toString())
                    .build();
            clientPushService.pushToPersonLoud(person, clientChatMessageReceivedEvent,
                    "Test: ClientChatMessageReceivedEvent", categoryPickupAssignment);
        }
        if (transportAssignment.isPresent()) {
            ClientTransportDeliveredEvent clientTransportDeliveredEvent =
                    new ClientTransportDeliveredEvent(HandoverType.UNKNOWN,
                            transportAssignment.get().getDelivery().getTrackingCode(),
                            transportAssignment.get().getId(), CreatorType.RECEIVER);
            clientPushService.pushToPersonLoud(person, clientTransportDeliveredEvent,
                    "Test: ClientTransportDeliveredEvent", categoryTransportStatusCarrier);
        }
    }

    @ApiOperation(value = "Send open door request",
            notes = "Sends an open door request to the pooling station box, the allocation is not changed.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("poolingstation/sendOpenDoorRequest")
    public void sendOpenDoorRequest(
            @RequestParam String poolingStationBoxId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        PoolingStationBox poolingStationBox = poolingStationService.findPoolingStationBoxById(poolingStationBoxId);
        PoolingStationBoxOpenRequest boxOpenRequest = new PoolingStationBoxOpenRequest();
        boxOpenRequest.setPoolingStationBox(poolingStationBox);

        notify(boxOpenRequest);
    }

    @ApiOperation(value = "Allocate known Pooling Station Box",
            notes = "Allocate a known pooling station box with a delivery. "
                    +
                    "This needs to be used carefully because it does not check for existing allocations and thus can cause duplicates.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("poolingstation/allocatePoolingStationBox")
    public String allocatePoolingStationBox(
            @RequestParam String poolingStationBoxId,
            @RequestParam String deliveryId
    ) {
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        PoolingStationBox poolingStationBox = poolingStationService.findPoolingStationBoxById(poolingStationBoxId);
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        poolingStationService.allocatePoolingStationBoxDirectly(poolingStationBox, delivery);

        return "PoolingStationBoxId: '" + poolingStationBox.getId() + "', PoolingStationBoxName: '" + poolingStationBox.getName() + "'";
    }

    @ApiOperation(value = "Allocate Pooling Station",
            notes = "Allocate a pooling station box of a pooling station with a delivery, without opening it")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("poolingstation/allocatePoolingStation")
    public String allocatePoolingStation(
            @RequestParam String poolingStationId,
            @RequestParam String deliveryId
    ) {
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        PoolingStation poolingStation = poolingStationService.findPoolingStationById(poolingStationId);
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        PoolingStationBoxAllocation boxAllocation = poolingStationService.startAllocation(poolingStation, delivery);
        boxAllocation = poolingStationService.finalizeAllocation(poolingStation, delivery);

        PoolingStationBox box = boxAllocation.getPoolingStationBox();
        return "PoolingStationBoxId: '" + box.getId() + "', PoolingStationBoxName: '" + box.getName() + "'";
    }

    @ApiOperation(value = "Deallocate Pooling Station",
            notes = "Deallocate a pooling station box of a pooling station that contains a delivery, without opening it")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("poolingstation/deallocatePoolingStation")
    public String deallocatePoolingStation(
            @RequestParam String poolingStationId,
            @RequestParam String deliveryId
    ) {
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        PoolingStation poolingStation = poolingStationService.findPoolingStationById(poolingStationId);
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);

        PoolingStationBoxAllocation boxAllocation = poolingStationService.startDeallocation(poolingStation, delivery);

        boxAllocation = poolingStationService.finalizeDeallocation(poolingStation, delivery);
        PoolingStationBox box = boxAllocation.getPoolingStationBox();
        return "PoolingStationBoxId: '" + box.getId() + "', PoolingStationBoxName: '" + box.getName() + "'";
    }

    @ApiOperation(value = "Delete logistics data in demo tenant",
            notes = "Deletes all logistics data in the given demo tenant")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping("deleteDemoLogisticsData")
    public void deleteDemoLogisticsData(@RequestParam String tenantId) {
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        demoLogisticsService.deleteDataForDemoTenant(tenantId);
    }

    @ApiOperation(value = "Creates a QR code pdf label. Returns URL to the pdf.",
            notes = "Use it for testing or to regenerate label if label generation failed the first time")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "generateQrCodeLabel")
    public String generateQrCodeLabel(@RequestBody ClientQrCodeLabelCreateRequest request) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        return qrCodeService.generateQRCodePDFURL(request.getReceiverName(), request.getReceiverAddressName(),
                request.getReceiverStreet(), request.getReceiverCity(), request.getSenderName(),
                request.getSenderStreet(), request.getSenderCity(), request.getTransportNotes(),
                request.getContentNotes(), request.getTrackingCode(), request.getQrCodeText(),
                request.getParcelCount());
    }

}
