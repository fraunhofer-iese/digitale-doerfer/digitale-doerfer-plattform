/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientPoolingStation;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("logistics/poolingstation")
@Api(tags={"logistics.poolingstation"})
class PoolingStationController extends BaseController {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private IPoolingStationService poolingStationService;

    @Autowired
    private LogisticsClientModelMapper mapper;

    @ApiOperation(value = "Returns all PoolingStations of the given tenanat")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping
    public ResponseEntity<List<ClientPoolingStation>> getAllPoolingStations(
            @RequestParam String communityId) {

        Tenant tenant = tenantService.findTenantById(communityId);

        List<PoolingStation> poolingStations = poolingStationService.findPoolingStationsInTenant(tenant);
        List<ClientPoolingStation> result = poolingStations.stream()
                .map(ps -> mapper.createClientPoolingStation(ps))
                .collect(Collectors.toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returns a single PoolingStation",
            notes = "Returns the PoolingStation for the given poolingStation id")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/{poolingStationId}")
    public ClientPoolingStation getPoolingStation(
            @PathVariable String poolingStationId){

        PoolingStation poolingStation = poolingStationService.findPoolingStationById(poolingStationId);

        return mapper.createClientPoolingStation(poolingStation);
    }

}
