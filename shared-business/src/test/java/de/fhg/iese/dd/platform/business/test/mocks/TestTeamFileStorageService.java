/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import de.fhg.iese.dd.platform.business.shared.files.services.ITeamFileStorageService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TeamFileNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Service
@Profile("test")
@Log4j2
public class TestTeamFileStorageService implements ITeamFileStorageService {

    private final Map<String, FileAttributes> storedFiles = new HashMap<>();

    private final Set<String> folders = new HashSet<>(Collections.singletonList("/"));

    @Autowired
    private ITimeService timeService;

    @Data
    @AllArgsConstructor
    @Builder
    public static class FileAttributes {
        private String hash;
        private byte[] content;
        private String mimeType;
        private int length;
        private long lastModified;
    }

    public void reset() {
        storedFiles.clear();
        folders.clear();
        folders.add("/");
    }

    @Override
    public void saveFile(byte[] content, String mimeType, String fileName){
        if(content.length == 0 ){
            throw new FileStorageException("content empty");
        }
        if (StringUtils.isEmpty(mimeType)) {
            throw new FileStorageException("Empty mimeType");
        }
        if (StringUtils.isEmpty(fileName)) {
            throw new FileStorageException("Empty fileName");
        }
        //the parent directories are automatically created
        internalCreateDirectory(toFolderName(StringUtils.substringBeforeLast(fileName, "/")));
        storedFiles.put(fileName, new FileAttributes(toHashCode(content), content, mimeType, content.length,
                timeService.currentTimeMillisUTC()));
        log.info("saveFile({}, {}, {})", content.length, mimeType, fileName);
    }

    @Override
    public byte[] getFile(String fileName) throws TeamFileNotFoundException {
        if (!storedFiles.containsKey(fileName)) {
            throw new TeamFileNotFoundException("File {} not found", fileName);
        }
        log.info("getFile({})", fileName);
        return storedFiles.get(fileName).content;
    }

    @Override
    public void createDirectory(String directoryName) throws FileStorageException {
        if (StringUtils.isEmpty(directoryName)) {
            //root does not need to be created
            return;
        }
        internalCreateDirectory(directoryName);
        log.info("createDirectory({})", directoryName);
    }

    private void internalCreateDirectory(String directoryName) {
        //parent folders are also created
        String[] parentDirectories = StringUtils.split(directoryName, "/");
        StringBuilder fullDirectory = new StringBuilder();
        for (String parentDirectory : parentDirectories) {
            fullDirectory.append(parentDirectory);
            fullDirectory.append("/");
            folders.add(toFolderName(fullDirectory.toString()));
        }
    }

    @Override
    public boolean fileExists(String fileName) throws FileStorageException {
        return storedFiles.containsKey(fileName) || folders.contains(toFolderName(fileName));
    }

    @Override
    public void delete(String fileName) throws FileStorageException {
        storedFiles.remove(fileName);
        String folderName = toFolderName(fileName);
        folders.remove(folderName);
        List<String> filesToDelete = storedFiles.keySet().stream()
                .filter(existingFileName -> existingFileName.startsWith(folderName))
                .collect(Collectors.toList());
        filesToDelete.forEach(storedFiles::remove);
        log.info("delete({})", fileName);
    }

    public FileAttributes getFileAttributes(String fileName) {
        return storedFiles.get(fileName);
    }

    public void saveFilesToTemp(String context) throws IOException {

        final List<Map.Entry<String, FileAttributes>> entries = storedFiles.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());
        for (Map.Entry<String, FileAttributes> nameAndFileAttributes : entries) {
            final String fileName = nameAndFileAttributes.getKey();
            final String suffix = "." + StringUtils.substringAfterLast(fileName, ".");
            final String sanitizedFileName = StringUtils.replace(fileName, "/", "_");
            final Path tempFile =
                    Files.createTempFile(context + "__" + sanitizedFileName + "_", suffix);
            log.info("Saving file to {}", tempFile.toUri());
            Files.write(tempFile, nameAndFileAttributes.getValue().getContent());
        }
    }

    private String toFolderName(String directoryName) {
        return StringUtils.appendIfMissing(directoryName, "/");
    }

    private String toHashCode(byte[] content) {
        return DigestUtils.md5DigestAsHex(content);
    }

}
