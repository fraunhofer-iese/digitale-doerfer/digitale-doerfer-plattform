/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.msteams;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.rocket.RocketChatTeamNotificationPublisherProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class MSTeamsPublisherTest {

    private static final String CONNECTION_NAME = "test-team";
    private static final String CONNECTION_WEBHOOK_URL = "https://teams.example.org/hook";
    private static final String CHANNEL_NAME = "dev-notifications";
    private final Tenant tenant;
    private final String message200;
    private final String message500;

    private static final ObjectWriter CONNECTION_CONFIG_WRITER = defaultJsonWriter()
            .forType(MSTeamsTeamNotificationPublisher.MSTeamsConnectionConfig.class);

    private final RestTemplate restTemplate;
    private final MockRestServiceServer server;
    private final TestTimeService testTimeService;

    public MSTeamsPublisherTest() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        String timeZone = "Pacific/Chuuk"; //UTC+10 different to avoid implicit handling of time zone on machine
        applicationConfig.setLocalTimeZone(timeZone);
        testTimeService = new TestTimeService(applicationConfig);
        restTemplate = new RestTemplate();
        server = MockRestServiceServer.bindTo(restTemplate).build();
        tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        message200 = "";
        message500 = "Internal Server Error :-(";
    }

    private MSTeamsTeamNotificationPublisher newMSTeamsPublisherWithMockedRestTemplate()
            throws JsonProcessingException {

        TeamNotificationConnection connection = TeamNotificationConnection.builder()
                .name(CONNECTION_NAME)
                .teamNotificationConnectionType(RocketChatTeamNotificationPublisherProvider.CONNECTION_TYPE)
                .connectionConfig(
                        CONNECTION_CONFIG_WRITER.writeValueAsString(
                                MSTeamsTeamNotificationPublisher.MSTeamsConnectionConfig.builder()
                                        .channelConfig(
                                                MSTeamsTeamNotificationPublisher.MSTeamsConnectionChannelConfig.builder()
                                                        .channelName(CHANNEL_NAME)
                                                        .webhookUrl(CONNECTION_WEBHOOK_URL)
                                                        .build())
                                        .build()))
                .build();

        return new MSTeamsTeamNotificationPublisher(testTimeService, new RestTemplateBuilder(), connection) {
            @Override
            protected RestTemplate getRestTemplate() {
                return MSTeamsPublisherTest.this.restTemplate;
            }
        };
    }

    @Test
    public void message_contains_correct_data() throws Exception {

        final TeamNotificationMessage message = TeamNotificationMessage.builder()
                .title("Aus gegebenem Anlass!")
                .text(UUID.randomUUID() + "ÄÜÖ👍")
                .created(System.currentTimeMillis())
                .build();

        server.expect(ExpectedCount.once(), requestTo(CONNECTION_WEBHOOK_URL))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo(message.getTitle())))
                .andExpect(jsonPath("$.text", equalTo(message.getText())))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final MSTeamsTeamNotificationPublisher chatPublisher = newMSTeamsPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant, message));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void message_sending_retry() throws Exception {

        final TeamNotificationMessage message = TeamNotificationMessage.builder()
                .title("Aus gegebenem Anlass!")
                .text("Erneuter Versuch macht kluch!")
                .created(System.currentTimeMillis())
                .build();

        server.expect(ExpectedCount.once(), requestTo(CONNECTION_WEBHOOK_URL))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo(message.getTitle())))
                .andExpect(jsonPath("$.text", equalTo(message.getText())))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).body(message500)
                        .contentType(MediaType.TEXT_PLAIN));

        server.expect(ExpectedCount.once(), requestTo(CONNECTION_WEBHOOK_URL))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title", equalTo(message.getTitle())))
                .andExpect(jsonPath("$.text", equalTo(message.getText())))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final MSTeamsTeamNotificationPublisher chatPublisher = newMSTeamsPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant, message));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

}
