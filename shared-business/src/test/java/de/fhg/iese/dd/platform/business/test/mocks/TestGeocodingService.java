/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.geo.services.IGeoService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.extern.log4j.Log4j2;

/**
 * Geocoding service that uses a built in cache to answer the requests.
 *
 * <p/>
 *
 * <strong>Only intended for tests</strong>
 *
 */
@Service
@Profile({"test"})
@Log4j2
public class TestGeocodingService implements IGeoService {

    /**
     * NEVER set this to {@code true} when committing! This is only for having a
     * convenient way to fill the cache when having added new addresses in the
     * tests.
     * <p/>
     * When set to true a summary of all cache misses with according GPS
     * location is logged at the end of the test runs. This can just be copied
     * to the initializer.
     */
    private static final boolean LOOKUP_CACHE_MISSES_WITH_GOOGLE = false;

    @Autowired
    ApplicationConfig applicationConfig;

    private final Map<String, GPSLocation> geocodeCache = new HashMap<>();
    private String directionsResponse = null;
    private boolean cacheMiss = false;
    private final StringBuffer cacheMisses = new StringBuffer();

    /**
     * Only used when LOOKUP_CACHE_MISSES_WITH_GOOGLE is {@code true}
     */
    private GeoApiContext geoApiContext;

    @PostConstruct
    protected void initialize() {
        cacheMisses.append("\nThe following cache misses have been recorded, add the address to the cache at ")
                .append(this.getClass().getName())
                .append(":\n\n");
        if (LOOKUP_CACHE_MISSES_WITH_GOOGLE) {
            initializeGoogleLookup();
        }

        addToCache("Pfaffenbärstraße 42", "67663", "Kaiserslautern", 49.4302135, 7.7581613); // Balthasar Weitzel
        addToCache("Fraunhofer-Platz 1", "67663", "Kaiserslautern", 49.4308357, 7.7504267); // Fraunhofer IESE
        addToCache("Gottlieb-Daimler-Straße 49", "67663", "Kaiserslautern", 49.423920, 7.755011); // D-Station TU KL
        addToCache("Königstraße 115", "67655", "Kaiserslautern", 49.437537, 7.756299); // D-Station Lidl
        addToCache("Pariser Straße 196", "67663", "Kaiserslautern", 49.441573, 7.743977); // D-Station ATU
        addToCache("Zollamtstraße 8", "67663", "Kaiserslautern", 49.435450, 7.769099); // D-Station Bahnhof
        addToCache("Erwin-Schrödinger-Straße 1", "67663", "Kaiserslautern", 49.4242194, 7.7529187);

        addToCache("NewStreet", "NewZip", "NewCity", 0.0, 0.0);
        addToCache("NN", "NN", "NN1", 0.0, 0.0);
        addToCache("NN", "NN", "NN2", 0.0, 0.0);
        addToCache("Peterstraße 7", "67307", "Göllheim", 0.0, 0.0);
        addToCache("Zur betrunkenen Traube 1", "12345", "Kaiserslautern", 0.0, 0.0);
        addToCache("Malerstraße 1", "67308", "Zellertal", 0.0, 0.0);
        addToCache("Geldstraße", "67304", "Eisenberg", 0.0, 0.0);
        addToCache("Peterstraße", "47307", "Betzdorf", 0.000000, 0.000000);
        addToCache("Müllerstraße", "47304", "Betzdorf", 0.000000, 0.000000);
        addToCache("Malerstraße", "47308", "Betzdorf", 0.000000, 0.000000);
        addToCache("Geldstraße", "47307", "Betzdorf", 0.000000, 0.000000);

        addToCache("Lauterstraße 51", "67659", "Kaiserslautern", 49.4468995, 7.749766);
        addToCache("Mozartstraße 4", "67308", "Kaiserslautern", 49.4409838, 7.7682621);
        addToCache("Alex-Müller-Straße 160", "67308", "Kaiserslautern", 49.4555101, 7.7753689);
        addToCache("Alex-Müller-Straße 123", "67657", "Kaiserslautern", 49.45508, 7.776825299999999);
        addToCache("Peterstraße 7", "67655", "Kaiserslautern", 49.4426796, 7.7590265);
        addToCache("Schützenstraße 24", "57518", "Alsdorf", 50.7737056, 7.8837382);
        addToCache("Schützenstraße 26", "57518", "Alsdorf", 50.7736959, 7.8850664);
        addToCache("Bahnhofstraße 5", "67308", "Zellertal", 49.6413073, 8.1345504);
        addToCache("Albert-Wilkes-Straße 2", "67304", "Eisenberg", 49.55996, 8.073869999999999);
        addToCache("Viktoriastraße 22", "57518", "Betzdorf", 50.7881688, 7.8719863);
        addToCache("Arleshof 1", "67304", "Kerzenheim", 49.5690256, 8.0343058);
        addToCache("Kurt-Schumacher-Straße 1", "67663", "Kaiserslautern", 49.4230125, 7.748495500000001);
        addToCache("Konrad-Adenauer-Straße 2", "67663", "Kaiserslautern", 49.4269302, 7.7427981);
        addToCache("Pariser Straße 123", "67655", "Kaiserslautern", 49.4424827, 7.7513553);
        addToCache("Kirchstraße 79", "57584", "Scheuerfeld", 50.78958919999999, 7.850966400000001);
        addToCache("Am Krahstück 1", "57518", "Alsdorf", 50.7743, 7.88904);
        addToCache("Lindenstraße 2", "67308", "Zellertal", 49.64195, 8.13355);
        addToCache("Wilhelmstraße 45-47", "57518", "Betzdorf", 50.7914202, 7.8697434);
        addToCache("Bahnhofstraße 3", "67816", "Dreisen", 49.6022332, 8.0138458);
        addToCache("Hauptstraße 42", "57584", "Scheuerfeld", 50.78788, 7.837029999999999);
        addToCache("Schutzbacher Weg 79-81", "57518", "Alsdorf", 50.77274999999999, 7.896369999999999);
        addToCache("Lindenstraße 2", "67308", "Zellertal", 49.64195, 8.13355);
        addToCache("Freiherr-vom-Stein-Straße 20", "67307", "Göllheim", 49.59404, 8.05062);
        addToCache("Jung-Stilling-Straße 2", "67663", "Kaiserslautern", 49.43094439999999, 7.7604038);
        addToCache("Müllerstraße 1", "67304", "Eisenberg", 49.5581168, 8.066914499999999);
        addToCache("Wilhelmstraße 16", "57518", "Betzdorf", 50.7904912, 7.871754699999999);
        addToCache("Stetter Straße 5", "67308", "Albisheim", 49.6511, 8.097199999999999);
        addToCache("Hellerstraße 2", "57518", "Betzdorf", 50.7878176, 7.874085999999999);
        addToCache("Konrad-Adenauer-Platz", "57518", "Betzdorf", 50.7893065, 7.8724088);
        addToCache("Im Grund 1", "57518", "Betzdorf", 50.7936475, 7.85399);
        addToCache("Konrad-Adenauer-Straße 1", "67304", "Eisenberg", 49.5628, 8.08342);
        addToCache("Bahnhofstraße 11", "57518", "Betzdorf", 50.78907, 7.87132);
        addToCache("Fliegerstraße 2", "67657", "Kaiserslautern", 49.45253160000001, 7.777550499999999);
        addToCache("Gutenbergstraße 40", "67304", "Eisenberg", 49.56164, 8.075669999999999);
        addToCache("Hauptstraße 51", "67305", "Ramsen", 49.5369608, 8.0089615);
        addToCache("Viktoriastraße 9-11", "57518", "Betzdorf", 50.7880094, 7.872792199999999);
        addToCache("Kerzenheimer Straße 10", "67304", "Eisenberg", 49.55850419999999, 8.071807);
        addToCache("Hauptstraße 86", "67304", "Eisenberg", 49.5580578, 8.0736115);
        addToCache("Scheuerfelder Straße 43", "57518", "Betzdorf", 50.7903247, 7.8543228);
        addToCache("Hauptstraße 62", "67307", "Göllheim", 49.595163, 8.050680);
        addToCache("Hauptstraße 58", "67307", "Göllheim", 49.595300, 8.050970);
        addToCache("Hauptstraße 119A", "67304", "Eisenberg", 49.558548, 8.075953);
        addToCache("Philipp-Mayer-Straße 7", "67304", "Eisenberg", 49.558860, 8.070256);
        addToCache("Am Marktplatz 3", "67304", "Eisenberg", 49.559774, 8.071385);
        addToCache("Hauptstraße 1", "67308", "Albishem (Pfrimm)", 49.651020, 8.100710);
        addToCache("Kerzenheimerstraße 8", "67304", "Eisenberg", 49.558400, 8.071878);
        addToCache("Pestalozzistraße 4", "67304", "Eisenberg", 49.561948, 8.071557);
        addToCache("Wilhelmstraße 13", "57518", "Betzdorf", 50.790511, 7.872282);
        addToCache("Wilhelmstraße 15", "57518", "Betzdorf", 50.790589, 7.872090);
        addToCache("Bahnhofstraße 18", "57518", "Betzdorf", 50.789050, 7.871840);
        addToCache("Wilhelmstraße 14", "57518", "Betzdorf", 50.790454, 7.872042);
        addToCache("Decizer Straße 3", "57518", "Betzdorf", 50.789213, 7.873022);
        addToCache("Wilhelmstraße 17", "57518", "Betzdorf", 50.790699, 7.872142);
        addToCache("Hauptstraße 2-4", "57584", "Scheuerfeld", 50.790960, 7.835250);
        addToCache("Waldstraße 37a", "57520", "Dickendorf", 50.735430, 7.853000);
        addToCache("Bahnhofstraße 15", "57518", "Betzdorf", 50.788898, 7.871764);
        addToCache("In der Au 7", "57290", "Neunkirchen", 50.783840, 8.017980);
        addToCache("Schützenstraße 1", "57518", "Alsdorf", 50.776034, 7.883543);
        addToCache("Industriestraße 3", "57584", "Scheuerfeld", 50.789852, 7.834198);
        addToCache("Freiherr-vom-Stein-Straße 3", "67307", "Göllheim", 49.593850, 8.053570);
        addToCache("Philipp-Mayer-Straße 1", "67304", "Eisenberg", 49.558980, 8.071000);
        addToCache("Eisenberger Straße 5", "67304", "Kerzenheim", 49.576020, 8.060450);
        addToCache("Maximilian-Kolbe-Straße", "57584", "Scheuerfeld", 50.787380, 7.847722);
        addToCache("Raiffeisenstraße 2", "57518", "Betzdorf", 50.785269, 7.861232);
        addToCache("Rainstraße 26", "57518", "Betzdorf", 50.786225, 7.871569);
        addToCache("Tiergartenstraße 28", "57584", "Wallmenroth", 50.797020, 7.830940);
        addToCache("Jahnstraße 10", "57584", "Wallmenroth", 50.798520, 7.835260);
        addToCache("Schladeweg 15", "57584", "Wallmenroth", 50.798370, 7.839590);
        addToCache("K106", "57584", "Wallmenroth", 50.800975, 7.837565);
        addToCache("Eisenweg 86", "57518", "Betzdorf", 50.799702, 7.884118);
        addToCache("Struthofstraße 35", "57518", "Betzdorf", 50.795139, 7.881461);
        addToCache("Am Kreisel in die Isigny-Allee", "67685", "Weilerbach", 49.475906, 7.637231);
        addToCache("Isigny-Allee Ecke Sonnenstraße", "67685", "Weilerbach", 49.476861, 7.636310);
        addToCache("Sonnenstraße Ecke Schellengerger Straße", "67685", "Weilerbach", 49.476727, 7.630813);
        addToCache("Schellengerger Straße Ecke Sandhübel", "67685", "Weilerbach", 49.478030, 7.629830);
        addToCache("Falltor 1. Verbindung Heinrich Koch Straße", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Falltor 2. Verbindung Heinrich Koch Straße", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Mackenbacher Straße 72", "67685", "Weilerbach", 49.481420, 7.623543);
        addToCache("Mackenbacher Straße 31", "67685", "Weilerbach", 49.480400, 7.627110);
        addToCache("Mörikestraße Ecke von Brentanostraße", "67685", "Weilerbach", 49.480232, 7.624043);
        addToCache("Eichendorfstraße Ecke Übergang Falltor", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Mackenbacherstraße Kreuzung Öbergasse", "67685", "Weilerbach", 49.480727, 7.628751);
        addToCache("Rummelstraße 7", "67685", "Weilerbach", 49.481748, 7.631813);
        addToCache("Hüttengärten 22", "67685", "Weilerbach", 49.483900, 7.632550);
        addToCache("Beethovenstraße Ecke Mozartstraße In der Lehmenkaut", "67685", "Weilerbach", 49.483880, 7.631059);
        addToCache("Hüttengärten 1", "67685", "Weilerbach", 49.482018, 7.630211);
        addToCache("Busenhübel 5", "67685", "Weilerbach", 49.482038, 7.628939);
        addToCache("Busenhübel Kreuzung Lindenstraße", "67685", "Weilerbach", 49.482744, 7.626691);
        addToCache("Am Hochrain Ecke Zum Geißerech drehen", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Ringstraße Kreuzung Talstraße (oben)", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Ringstraße Kreuzung Talstraße (unten)", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Spitzäckerstraße Ecke Am Elpel", "67685", "Weilerbach", 49.482082, 7.622571);
        addToCache("Lindenstraße 13", "67685", "Weilerbach", 49.482554, 7.625292);
        addToCache("Hauptstraße/ Parkplatz/ Barbarossa Bäckerei/ Reisebüro Lowak, Fahrschule Schneider", "67685",
                "Weilerbach", 0.000000, 0.000000);
        addToCache("Hauptstraße/ Apotheke/ Tee- und Bastelstube Rutz", "67685", "Weilerbach", 49.481608, 7.631874);
        addToCache("Hans-Reiner-Straße 7", "67685", "Weilerbach", 49.478371, 7.634145);
        addToCache("Danziger Straße/ Penny/ Paulus Fachmarkt/ Edeka/ Netto", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Danziger Straße/ Dietz/ Aldi", "67685", "Weilerbach", 49.477205, 7.642920);
        addToCache("Danziger Straße/ Zemo", "67685", "Weilerbach", 49.478320, 7.640358);
        addToCache("In der Nasserde Kreuzung Deutschehernstraße", "67685", "Weilerbach", 49.479856, 7.637088);
        addToCache("Friedenstraße 5", "67685", "Weilerbach", 49.481058, 7.638314);
        addToCache("In der Nasserde 32", "67685", "Weilerbach", 49.481401, 7.643449);
        addToCache("Ostpreussenstraße 8", "67685", "Weilerbach", 49.482590, 7.642410);
        addToCache("Am Mühlhebel 8", "67685", "Weilerbach", 49.485138, 7.640563);
        addToCache("In den Sandäckern Kreuzung im Schelmental", "67685", "Weilerbach", 49.485705, 7.638358);
        addToCache("Rummelstraße 52", "67685", "Weilerbach", 49.485108, 7.636941);
        addToCache("Rabenhübel/ Parkplatz Rabennest", "67685", "Weilerbach", 49.484173, 7.636390);
        addToCache("Kirchenstraße/ Friedhof Nord", "67685", "Weilerbach", 48.153467, 11.571584);
        addToCache("Friedhofstraße Kreuzung Turnerstraße", "67685", "Weilerbach", 49.483404, 7.635547);
        addToCache("Rummelstraße 19", "67685", "Weilerbach", 49.482900, 7.632980);
        addToCache("Hauptstraße/ Parkplatz Kreissparkasse/ Barz/ Spar", "67685", "Weilerbach", 0.000000, 0.000000);
        addToCache("Hauptstraße 84", "67304", "Einsenberg", 49.5580318, 8.0712848);
        addToCache("Hellerstraße 2", "57518", "Betzdorf", 50.7878176, 7.8718973);
        addToCache("Fraunhofer-Platz 1", "67663", "Kaiserslautern", 49.4314667, 7.7498114);
        addToCache("Rummelstraße 15", "67685", "Weilerbach", 49.4829, 7.6307913);
        addToCache("Konrad-Adenauer-Strasse 5", "67663", "Kaiserslautern", 49.4266035, 7.7409713);
        addToCache("Pirmasenser Strasse 8", "67655", "Kaiserslautern", 49.441864, 7.7652635);
        addToCache("Gutenbergstrasse 18", "67663", "Kaiserslautern", 49.4312883, 7.7524382);
        addToCache("Kuglerweg 8", "95689", "Fuchsmühl", 49.919821, 12.14195);
        addToCache("Lindenweg 6", "95689", "Fuchsmühl", 49.917783, 12.146507);
        addToCache("Jahnweg 2", "95689", "Fuchsmühl", 49.919674, 12.142742);
        addToCache("Gartenstraße 5", "95689", "Fuchsmühl", 49.920809, 12.149518);
        addToCache("Zur betrunkenen Traube 1 Straße straße straße Straße", "12345", "Kaiserslautern", 49.468526,
                12.126325);
        addToCache("Street", "12345", "City", 49.454526, 12.126123);
        addToCache("Hellerstrasse 30", "57518", "Betzdorf", 50.7866827, 7.8737623);
        addToCache("Stein ung Garten Park", "57818", "Betzdorf", 50.7667246, 7.8525028);
        addToCache("Marktplatz 1", "92681", "Erbendorf", 49.838315, 12.047873);
        addToCache("Kemnather Straße 48", "95505", "Immenreuth", 49.90793, 11.8671013);
        addToCache("Marktplatz 12", "95676", "Wiesau", 49.91007, 12.1822713);
        addToCache("Marktplatz 8", "95685", "Falkenberg", 49.85867, 12.2214913);

        //Person_96eaa4b2-dc80-4eed-8c1b-ea7eaaa7c5d5.json
        addToCache("Frühmeßgasse 13", "92681", "Erbendorf", 49.838312, 12.045979);
        addToCache("Tulpenstraße 2", "95679", "Waldershof", 49.949636, 12.091386);
        addToCache("Im Hopfenthal 8", "95478", "Kemnath", 49.865218, 11.949485);
        addToCache("Am Hügel 2", "95704", "Pullenreuth", 49.931055, 12.000514);
        addToCache("Rathausstraße 1", "95689", "Fuchsmühl", 49.921402, 12.146088);

        addToCache("Bräugasse 6", "92681", "Erbendorf", 49.838218, 12.047551);

        //GrapevineTestHelper
        addToCache("Willy-Brandt-Platz 4-5", "67657", "Kaiserslautern", 49.464438, 7.7671056);
        addToCache("Fraunhofer-Platz 2", "67663", "Kaiserslautern", 49.4311216, 7.7502187);

        //ShopEventControllerTest
        addToCache("Badstraße 11", "67655", "Kaiserslautern", 49.441557, 7.773918);
        addToCache("Badstraße 13", "67655", "Kaiserslautern", 49.441557, 7.773918);

        //HappeningEventControllerTest
        geocodeCache.put(createNameQuery("Veranstaltungsort"), new GPSLocation(48.0, 6.0));

        //Organizations.json
        addToCache("Fraunhofer-Platz 1", "67663", "Digitalbach", 0.0, 0.0);
    }

    private void addToCache(String street, String zip, String city, double lat, double lon) {
        geocodeCache.put(createAddressQuery(street, zip, city), new GPSLocation(lat, lon));
    }

    @Override
    public GPSLocation getGPSLocation(String street, String zip, String city) {

        GPSLocation location = lookupCache(street, zip, city);
        if (location == null) {
            cacheMiss = true;
            if (LOOKUP_CACHE_MISSES_WITH_GOOGLE) {
                location = lookupLocationWithGoogle(street, zip, city);
                String message = String.format(Locale.ROOT,
                        "addToCache(\"%s\", \"%s\", \"%s\",%.8f, %.8f);\n",
                        street, zip, city, location.getLatitude(), location.getLongitude());
                cacheMisses.append(message);
                geocodeCache.put(createAddressQuery(street, zip, city), location);
                return location;
            } else {
                String queryURL;
                queryURL =
                        "https://www.google.com/maps/search/" + URLEncoder.encode(createAddressQuery(street, zip, city),
                                StandardCharsets.UTF_8);
                String message =
                        "Cache miss, lookup GPS coordinates with the link below and add the address to the cache at " +
                                this.getClass().getName() + ":\n\n" +
                                String.format(
                                        "addToCache(\"%s\", \"%s\", \"%s\", 0.0, 0.0);",
                                        street, zip, city) + "\n\n"
                                + queryURL + "\n\n";
                cacheMisses.append(message).append("-----------------------------------------\n");
                return new GPSLocation(0.0, 0.0);
            }
        }
        return location;
    }

    private final Map<String, Address> locationLookupStringToAddressMap = new HashMap<>();

    /**
     * registers Address for {@link TestGeocodingService#resolveLocation(String, String)}
     *
     * @param locationLookupString must not be {@code null}
     * @param address must not be {@code null}
     */
    public void registerAddress(String locationLookupString, Address address) {
        Objects.requireNonNull(locationLookupString);
        Objects.requireNonNull(address);
        log.info("Registered address={} for locationLookupString={}", address, locationLookupString);
        locationLookupStringToAddressMap.put(locationLookupString, address);
    }

    /**
     * registers Address for {@link TestGeocodingService#resolveLocation(String, String)}
     *
     * @param gpsLocation must not be {@code null}
     * @param address must not be {@code null}
     */
    public void registerGpsCoordinates(GPSLocation gpsLocation, Address address) {
        Objects.requireNonNull(gpsLocation);
        Objects.requireNonNull(address);
        log.info("Registered address={} for gpsLocation={}", address, gpsLocation);
        locationLookupStringToAddressMap.put(gpsLocation.toString(), address);
    }

    public void clearLocationToAddressRegistration() {
        log.info("Cleared locationLookupStringToAddressMap");
        locationLookupStringToAddressMap.clear();
    }

    @Override
    public Address resolveLocation(String locationName, String locationLookupString) {
        final Address address = locationLookupStringToAddressMap.get(locationLookupString);
        if (address != null && !Objects.equals(locationName, address.getName())
                && !Objects.equals(locationLookupString, address.getName())) {
            throw new IllegalStateException("Unrealistic test data: Returned address.name=" + address.getName() +
                    " is different from the given locationName=" + locationName + " and locationLookupString="
                    + locationLookupString + ". This will never happen in the real GoogeGeoService.");
        }
        if (address == null) {
            log.info("No address found for locationLookupString={}", locationLookupString);
        } else {
            log.info("Found address={} for locationLookupString={}", address, locationLookupString);
        }
        return address;
    }

    @Override
    public Address resolveLocation(String locationName, GPSLocation gpsLocation) {
        return locationLookupStringToAddressMap.get(gpsLocation.toString());
    }

    private GPSLocation lookupCache(String street, String zip, String city){
        return geocodeCache.get(createAddressQuery(street, zip, city));
    }

    private GPSLocation getGPSLocation(String name) {
        final GPSLocation gpsLocation = geocodeCache.get(createNameQuery(name));
        if (gpsLocation == null) {
            throw new RuntimeException("No location for name " + name + " defined in " + getClass().getSimpleName() +
                    ". Add query to initialize() function!");
        }
        return gpsLocation;
    }

    protected String createAddressQuery(String street, String zip, String city) {
        return street + ", " + zip + " " + city;
    }

    private String createNameQuery(String veranstaltungsort) {
        return "###" + veranstaltungsort;
    }

    @Override
    public GPSLocation getGPSLocation(IAddressService.AddressDefinition address) {
        if (address.getName() != null && address.getStreet() == null && address.getCity() == null && address.getZip() == null) {
            return getGPSLocation(address.getName());
        }
        return getGPSLocation(address.getStreet(), address.getZip(), address.getCity());
    }

    @Override
    public Object getDirections(GPSLocation from, GPSLocation to) {
        if (directionsResponse == null) {
            try {
                directionsResponse = StreamUtils.copyToString(getClass().getResourceAsStream("/google_maps_directions_example_backend.json"),
                        StandardCharsets.UTF_8);
            } catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        }
        return directionsResponse;
    }

    private void initializeGoogleLookup(){
        geoApiContext = new GeoApiContext.Builder()
            .apiKey(applicationConfig.getGoogle().getApiKey())
            .connectTimeout(500, TimeUnit.MILLISECONDS)
            .readTimeout(1500, TimeUnit.MILLISECONDS)
            .retryTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(500, TimeUnit.MILLISECONDS)
            .build();
    }

    private GPSLocation lookupLocationWithGoogle(String street, String zip, String city) {
        try {
            String addressQuery = createAddressQuery(street, zip, city);
            GeocodingApiRequest request = GeocodingApi.newRequest(geoApiContext)
                    .address(addressQuery)
                    .language("de");

            GeocodingResult[] geocodeResults = request.await();

            if (geocodeResults == null || geocodeResults.length == 0) {
                return new GPSLocation(0.0, 0.0);
            }
            LatLng location = geocodeResults[0].geometry.location;
            return new GPSLocation(location.lat, location.lng);

        } catch (Exception e) {
            log.warn("Failed to lookup", e);
            return new GPSLocation(0.0, 0.0);
        }
    }

    @PreDestroy
    protected void preDestroy(){
        if(cacheMiss){
            log.error(cacheMisses.toString());
        }else{
            log.info("\n\nNo cache miss, everything configured well! 👍\n\n");
        }
    }

}
