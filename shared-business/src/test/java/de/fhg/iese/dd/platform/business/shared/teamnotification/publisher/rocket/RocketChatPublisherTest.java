/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.rocket;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Scanner;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class RocketChatPublisherTest {

    private static final String CONNECTION_NAME = "test-team";
    private static final String CONNECTION_URL = "http://test";
    private static final String CONNECTION_USERNAME = "klaus";
    private static final String CONNECTION_PASSWORD = "4§)%(12FG3$ÖÄ";
    private static final String CHANNEL_NAME = "dev-notifications";
    private final Tenant tenant;

    private final String login200;
    private final String login401;
    private final String message200;
    private final String message500;

    private static final ObjectWriter CONNECTION_CONFIG_WRITER = defaultJsonWriter()
            .forType(RocketChatTeamNotificationPublisher.RocketChatConnectionConfig.class);
    private static final ObjectReader LOGIN_RESPONSE_READER = defaultJsonReader().forType(LoginResponse.class);

    private final RestTemplate restTemplate;
    private final MockRestServiceServer server;
    private final TestTimeService testTimeService;

    public RocketChatPublisherTest() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        String timeZone = "Pacific/Chuuk"; //UTC+10 different to avoid implicit handling of time zone on machine
        applicationConfig.setLocalTimeZone(timeZone);
        testTimeService = new TestTimeService(applicationConfig);
        restTemplate = new RestTemplate();
        server = MockRestServiceServer.bindTo(restTemplate).build();
        tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        login200 = loadResource("login_200.json");
        login401 = loadResource("login_401.json");
        message200 = loadResource("message_200.json");
        message500 = "Internal Server Error :-(";
        log.debug("Huhuuhuhu");
    }

    private RocketChatTeamNotificationPublisher newRocketChatPublisherWithMockedRestTemplate()
            throws JsonProcessingException {

        TeamNotificationConnection connection = TeamNotificationConnection.builder()
                .name(CONNECTION_NAME)
                .teamNotificationConnectionType(RocketChatTeamNotificationPublisherProvider.CONNECTION_TYPE)
                .connectionConfig(
                        CONNECTION_CONFIG_WRITER.writeValueAsString(
                                RocketChatTeamNotificationPublisher.RocketChatConnectionConfig.builder()
                                        .url(CONNECTION_URL)
                                        .username(CONNECTION_USERNAME)
                                        .password(CONNECTION_PASSWORD)
                                        .build())
                ).build();

        return new RocketChatTeamNotificationPublisher(testTimeService, new RestTemplateBuilder(), connection) {
            @Override
            protected RestTemplate getRestTemplate() {
                return RocketChatPublisherTest.this.restTemplate;
            }
        };
    }

    @Test
    public void login_not_successful_handled_correctly() throws Exception {

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withStatus(HttpStatus.UNAUTHORIZED).body(login401).contentType(MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        try {
            chatPublisher.login();
            fail("exception expected");
        } catch (Exception ex) {
            assertThat(ex.getMessage()).contains("log in");
        }

        server.verify();
    }

    @Test
    public void login_successful_handled_correctly() throws Exception {

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.login();

        server.verify();
    }

    @Test
    public void login_contains_correct_data() throws Exception {

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username", equalTo(CONNECTION_USERNAME)))
                .andExpect(jsonPath("$.password", equalTo(CONNECTION_PASSWORD)))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.login();

        server.verify();
    }

    @Test
    public void message_contains_correct_data() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);
        final TeamNotificationMessage message = TeamNotificationMessage.builder()
                .text(UUID.randomUUID() + "ÄÜÖ👍")
                .created(System.currentTimeMillis())
                .build();

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andExpect(jsonPath("$.channel", equalTo(CHANNEL_NAME)))
                .andExpect(jsonPath("$.text", equalTo(message.getText())))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant, message));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void message_warn_contains_atAll() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);
        final TeamNotificationMessage message = TeamNotificationMessage.builder()
                .text(UUID.randomUUID() + "ÄÜÖ👍")
                .created(System.currentTimeMillis())
                .priority(TeamNotificationPriority.WARN_APPLICATION_MULTIPLE_USERS)
                .build();
        String textWithAtAll = "@all " + message.getText();

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andExpect(jsonPath("$.channel", equalTo(CHANNEL_NAME)))
                .andExpect(jsonPath("$.text", equalTo(textWithAtAll)))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant, message));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void message_delayed_gets_timestamp() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);

        long created = ZonedDateTime.parse("2019-10-03T20:28:43+10:00[Pacific/Chuuk]").toInstant().toEpochMilli() -
                RocketChatTeamNotificationPublisher.MAX_DELAY_BEFORE_TIMESTAMP_MILLIS;
        final TeamNotificationMessage message = TeamNotificationMessage.builder()
                .text(UUID.randomUUID() + "ÄÜÖ👍")
                .created(created)
                .build();
        String textWithTimestamp = "_03.10. 20:28_\n" + message.getText();

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andExpect(jsonPath("$.channel", equalTo(CHANNEL_NAME)))
                .andExpect(jsonPath("$.text", equalTo(textWithTimestamp)))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant, message));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void login_for_first_message_and_no_login_for_second_message() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);

        // first message
        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        // second message (authentication data already present -> no login)
        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant,
                TeamNotificationMessage.builder()
                        .text("messädsch")
                        .created(System.currentTimeMillis())
                        .build()));
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant,
                TeamNotificationMessage.builder()
                        .text("näkst")
                        .created(System.currentTimeMillis())
                        .build()));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void login_expired_for_second_message() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);

        // first message
        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        // second message: login expired, re-login, re-post of message
        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withStatus(HttpStatus.UNAUTHORIZED).body(login401).contentType(MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant,
                TeamNotificationMessage.builder()
                        .text("messädsch")
                        .created(System.currentTimeMillis())
                        .build()));
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant,
                TeamNotificationMessage.builder()
                        .text("ägäjn")
                        .created(System.currentTimeMillis())
                        .build()));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    @Test
    public void message_sending_retry() throws Exception {

        final LoginResponse response = LOGIN_RESPONSE_READER.readValue(login200);

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/login"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andRespond(withSuccess(login200, MediaType.APPLICATION_JSON));

        // second message: temporary internal server error
        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withStatus(HttpStatus.SERVICE_UNAVAILABLE).body(message500)
                        .contentType(MediaType.TEXT_PLAIN));

        server.expect(ExpectedCount.once(), requestTo("http://test/api/v1/chat.postMessage"))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(header("X-Auth-Token", response.getData().getAuthToken()))
                .andExpect(header("X-User-Id", response.getData().getUserId()))
                .andRespond(withSuccess(message200, MediaType.APPLICATION_JSON));

        final RocketChatTeamNotificationPublisher chatPublisher = newRocketChatPublisherWithMockedRestTemplate();
        chatPublisher.sendTeamNotification(new TeamNotificationSendRequest(CHANNEL_NAME, "topic", tenant,
                TeamNotificationMessage.builder()
                        .text("jetzt ist kaputt, kommt aber trotzdem an")
                        .created(System.currentTimeMillis())
                        .build()));

        //the shutdown waits for the sending of all messages
        chatPublisher.shutdownAndWait();

        server.verify();
    }

    private static String loadResource(String resourceName) {
        // simple method to load an input stream to a String using Scanner,
        // try-with-resources in order to avoid eclipse warning
        try (Scanner s = new Scanner(RocketChatPublisherTest.class.getResourceAsStream(resourceName),
                StandardCharsets.UTF_8)) {
            return s.useDelimiter("\\Z").next();
        }
    }

}
