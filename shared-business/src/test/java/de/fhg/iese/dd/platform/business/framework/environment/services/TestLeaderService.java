/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.services;

import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStartedEvent;
import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStoppedEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * This service is just to not run the leader election at test
 */
@Service
@Profile({"test"})
public class TestLeaderService extends LeaderService {

    @Override
    public void runLeaderCheck() {
        //nothing to do, we do not really elect in the test environment
    }

    public void startLeader() {

        String instanceIdentifier = getInstanceIdentifier();
        if (!instanceIdentifier.equals(lastSeenLeader)) {
            notify(new CurrentNodeLeaderStartedEvent());
        }
        this.lastSeenLeader = instanceIdentifier;
        this.currentLeaderElectionRecord = new LeaderElectionRecord(instanceIdentifier,
                timeService.currentTimeMillisUTC());
    }

    public void stopLeader() {

        String instanceIdentifier = getInstanceIdentifier();
        if (instanceIdentifier.equals(lastSeenLeader)) {
            notify(new CurrentNodeLeaderStoppedEvent());
        }
        this.lastSeenLeader = null;
        this.currentLeaderElectionRecord = null;
    }

}
