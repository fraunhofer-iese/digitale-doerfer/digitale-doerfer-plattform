/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * This service is to prevent the workers from starting at the test of the actual leader service
 */
@Service
@Profile({"LeaderServiceTest"})
public class TestWorkerService extends WorkerService {

    @Override
    public void startWorkerTasks() {
        //no workers should be started
    }

    @Override
    public void stopWorkerTasks() {
        //no workers should be stopped
    }

}
