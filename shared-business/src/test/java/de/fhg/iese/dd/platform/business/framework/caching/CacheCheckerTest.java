/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CacheCheckerTest {

    private static final int NUM_CACHE_CHANGES = 10;
    private static final int NUM_CACHE_ACCESSES = 10000;
    private static final int NUM_KEYS = 20;

    @Log4j2
    static class TestCacheUsage {

        private final boolean useAtOnceCache;
        private Map<String, Integer> atOnceCache;
        private Map<String, Integer> incrementalCache;
        private final AtomicInteger value = new AtomicInteger(0);
        private final AtomicInteger cacheRefreshes = new AtomicInteger(0);
        private final AtomicInteger nullValueAccess = new AtomicInteger(0);
        private final AtomicInteger staleValueAccess = new AtomicInteger(0);

        private final CacheCheckerAtOnceCache cacheCheckerAtOnceCache =
                new CacheCheckerAtOnceCache(System::currentTimeMillis, null, this::checkCache);

        private final CacheCheckerIncrementalCache cacheCheckerIncrementalCache =
                new CacheCheckerIncrementalCache(System::currentTimeMillis, null, this::checkCache);

        TestCacheUsage(boolean useAtOnceCache) {
            cacheCheckerAtOnceCache.setCheckInterval(Duration.ofMillis(1));
            cacheCheckerIncrementalCache.setCheckInterval(Duration.ofMillis(1));
            this.useAtOnceCache = useAtOnceCache;
            if (useAtOnceCache) {
                atOnceCache = new HashMap<>();
            } else {
                incrementalCache = new ConcurrentHashMap<>();
            }
        }

        private int checkCache() {
            return value.get();
        }

        private Integer getCacheValue(String key) {
            if (useAtOnceCache) {
                cacheCheckerAtOnceCache.refreshCacheIfStale(() -> {
                    cacheRefreshes.incrementAndGet();
                    Map<String, Integer> atOnceCacheNew = new HashMap<>();
                    for (int i = 0; i < NUM_KEYS; i++) {
                        String newKey = "key" + i;
                        atOnceCacheNew.put(newKey, computeValue(newKey));
                    }
                    atOnceCache = atOnceCacheNew;
                });
                //this is a wrong implementation for comparison
                /*
                if (cacheCheckerAtOnceCache.clearCacheIfStaleAndFillCacheAtOnce(() -> {
                })) {
                    cacheRefreshes.incrementAndGet();
                    for (int i = 0; i < NUM_KEYS; i++) {
                        String newKey = "key" + i;
                        atOnceCache.put(newKey, computeValue(newKey));
                    }
                }
                 */
                return atOnceCache.get(key);
            } else {
                cacheCheckerIncrementalCache.clearCacheIfStale(this::clearCache);
                return incrementalCache.computeIfAbsent(key,
                        k -> {
                            cacheRefreshes.incrementAndGet();
                            return computeValue(k);
                        });
            }
        }

        private Integer computeValue(String key) {
            try {
                Thread.sleep(new Random().nextInt(2));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            return value.get();
        }

        private int changeCachedValue() {
            return value.incrementAndGet();
        }

        private void clearCache() {
            if (useAtOnceCache) {
                atOnceCache.clear();
            } else {
                incrementalCache.clear();
            }
        }

    }

    @Log4j2
    static class CacheAccessThread extends Thread {

        private final CountDownLatch activeThreadCountDown;
        private final String key;
        private final TestCacheUsage testCacheUsage;

        public CacheAccessThread(String name, CountDownLatch activeThreadCountDown,
                String key, TestCacheUsage testCacheUsage) {
            super(name);
            this.activeThreadCountDown = activeThreadCountDown;
            this.key = key;
            this.testCacheUsage = testCacheUsage;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < NUM_CACHE_ACCESSES; i++) {
                    Integer cacheValue = testCacheUsage.getCacheValue(key);
                    if (cacheValue == null) {
                        log.error("NULL value {} refreshes {} accesses {}",
                                testCacheUsage.value.get(),
                                testCacheUsage.cacheRefreshes.get(),
                                i);
                        testCacheUsage.nullValueAccess.incrementAndGet();
                    } else {
                        if (cacheValue != testCacheUsage.value.get()) {
                            testCacheUsage.staleValueAccess.incrementAndGet();
                        }
                    }
                }
            } finally {
                activeThreadCountDown.countDown();
            }
        }

    }

    @Log4j2
    static class CacheChangeThread extends Thread {

        private final CountDownLatch activeThreadCountDown;
        private final TestCacheUsage testCacheUsage;

        public CacheChangeThread(String name, CountDownLatch activeThreadCountDown,
                TestCacheUsage testCacheUsage) {
            super(name);
            this.activeThreadCountDown = activeThreadCountDown;
            this.testCacheUsage = testCacheUsage;
        }

        @Override
        public void run() {
            try {
                for (int i = 0; i < NUM_CACHE_CHANGES; i++) {
                    try {
                        sleep(new Random().nextInt(2));
                    } catch (InterruptedException e) {
                        interrupt();
                    }
                    testCacheUsage.changeCachedValue();
                }
            } finally {
                activeThreadCountDown.countDown();
            }
        }

    }

    @Test
    public void parallelCacheRefreshTest_PartialCache() throws InterruptedException {

        int cacheFailures = 0;
        for (int i = 0; i < 10; i++) {
            cacheFailures += runTestScenario(false);
        }
        assertThat(cacheFailures)
                .as("Cache should never return invalid values")
                .isZero();
    }

    @Test
    public void parallelCacheRefreshTest_AtOnceCache() throws InterruptedException {

        int cacheFailures = 0;
        for (int i = 0; i < 10; i++) {
            cacheFailures += runTestScenario(true);
        }
        assertThat(cacheFailures)
                .as("Cache should never return invalid values")
                .isZero();
    }

    private int runTestScenario(boolean useAtOnceCache) throws InterruptedException {
        log.info("START scenario");
        TestCacheUsage testCacheUsage = new TestCacheUsage(useAtOnceCache);
        int numAccessThreadsPerKey = 13;
        int numThreads = NUM_KEYS * numAccessThreadsPerKey + 1;
        List<Thread> threads = new ArrayList<>(numThreads);
        CountDownLatch activeThreadCountDown = new CountDownLatch(numThreads);

        for (int k = 0; k < NUM_KEYS; k++) {
            for (int i = 0; i < numAccessThreadsPerKey; i++) {
                threads.add(new CacheAccessThread("access-" + k + "-" + i, activeThreadCountDown,
                        "key" + k, testCacheUsage));
            }
        }
        threads.add(new CacheChangeThread("change", activeThreadCountDown, testCacheUsage));

        threads.parallelStream().forEach(Thread::start);
        activeThreadCountDown.await();
        log.info("Refreshes {}, max intended {}", testCacheUsage.cacheRefreshes.get(),
                useAtOnceCache
                        ? NUM_CACHE_CHANGES + 1
                        : (NUM_KEYS * (NUM_CACHE_CHANGES + 1)));
        log.info("Potential stale accesses {}", testCacheUsage.staleValueAccess.get());
        if (testCacheUsage.nullValueAccess.get() > 0) {
            log.error("Null accesses {}", testCacheUsage.nullValueAccess.get());
        }
        return testCacheUsage.nullValueAccess.get();
    }

}
