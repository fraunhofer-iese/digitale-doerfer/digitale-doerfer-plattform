/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("test")
public class TestEmailSenderService extends EmailSenderService implements IEmailSenderService {

    @Getter
    @Builder
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class TestEmail {

        private final String senderEmailAddress;
        private final String recipientName;
        private final String recipientEmailAddress;
        private final String subject;
        private final String text;
        private final boolean html;
        private final List<File> attachments;

        @Override
        public String toString() {
            return "TestEmail{" +
                    "sender='" + senderEmailAddress + '\'' +
                    ", recipient='" + recipientEmailAddress + '\'' +
                    ", subject='" + subject + '\'' +
                    ", html=" + html +
                    ", attachments=" + formatAttachments() +
                    '}';
        }

        private String formatAttachments() {
            if (attachments == null) {
                return "[]";
            }
            return attachments.stream()
                    .map(File::getName)
                    .collect(Collectors.joining(",", "[", "]"));
        }
    }

    /**
     * Returns the list of sent mails.
     * <p>
     * In all tests extending BaseServiceTest from rest-application, the list is cleared before each test case.
     */
    @Getter
    private final List<TestEmail> emails = new ArrayList<>();

    public void clearMailList() {
        emails.clear();
    }

    @Override
    public void sendEmail(String fromEmailAddress, String recipientName, String recipientEmailAddress, String subject,
            String text, List<File> attachments, boolean isHtml) {
        log.info("E-Mail from {} to {} with subject {} would have been sent when using real email service",
                fromEmailAddress, recipientEmailAddress, subject);
        emails.add(
                TestEmail.builder()
                        .senderEmailAddress(fromEmailAddress)
                        .recipientName(recipientName)
                        .recipientEmailAddress(recipientEmailAddress)
                        .subject(subject)
                        .text(text)
                        .attachments(attachments)
                        .html(isHtml)
                        .build()
        );
    }

    public List<TestEmail> getMailsByFromAndToRecipient(String fromEmailAddress, String toEmailAddress) {
        return emails.stream()
                .filter(m -> m.senderEmailAddress.equals(fromEmailAddress) &&
                        m.recipientEmailAddress.equals(toEmailAddress))
                .collect(Collectors.toList());
    }

}
