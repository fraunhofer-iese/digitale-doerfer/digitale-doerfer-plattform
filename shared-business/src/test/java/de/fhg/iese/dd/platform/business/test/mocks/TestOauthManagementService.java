/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordWrongException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OAuthEMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.LoginHint;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;
import lombok.extern.log4j.Log4j2;

/**
 * Mock implementation. Needed in Unit tests, e.g. for PersonAdminUiControllerTest or testing SuperAdminChecker.
 */
@Service
@Profile({"test & !Auth0OAuthManagementServiceTest"})
@Log4j2
public class TestOauthManagementService implements IOauthManagementService {

    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;

    private final Map<String, String> emailToPassword = new HashMap<>();
    private final Map<String, OauthAccount> oauthAccountById = new HashMap<>();
    private final Map<String, OauthAccount> oauthAccountByEmail = new HashMap<>();

    public void reset() {
        emailToPassword.clear();
        oauthAccountById.clear();
        oauthAccountByEmail.clear();
    }

    @Override
    public String createUser(String email, String password, boolean emailVerified) throws OauthManagementException {
        //log.info("Simulated creation of user with email {}", email);
        if (oauthAccountByEmail.containsKey(email)) {
            throw new OauthManagementException("User with email " + email + " already created");
        }
        emailToPassword.put(email, password);
        String oauthId = emailToOauthId(email);
        final OauthAccount oauthAccount = OauthAccount.builder()
                .oauthId(oauthId)
                .name(email)
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .emailVerified(false)
                .blocked(false)
                .blockReason(null)
                .loginCount(0)
                .build();
        oauthAccountById.put(oauthId, oauthAccount);
        oauthAccountByEmail.put(email, oauthAccount);
        oauthRegistrationRepository.save(OauthRegistration.builder()
                .oauthId(oauthId)
                .email(email)
                .build());
        return oauthId;
    }

    @Override
    public void changeEmail(String oauthId, String newEmail, boolean newEmailVerified)
            throws EMailChangeNotPossibleException, OauthManagementException, OAuthEMailAlreadyUsedException {
        log.info("Simulated change of email address in oauth to '{}' for user '{}'", newEmail, oauthId);
        if (oauthAccountByEmail.containsKey(newEmail)) {
            throw new OAuthEMailAlreadyUsedException(newEmail);
        }
        if (queryUserByOauthId(oauthId).getAuthenticationMethods().stream()
                .noneMatch(authenticationMethod ->
                        authenticationMethod.equals(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))) {
            throw EMailChangeNotPossibleException.forNoUsernamePasswordAccount(newEmail);
        }
        emailToPassword.put(newEmail, emailToPassword.remove(oauthIdToEmail(oauthId)));
        oauthAccountByEmail.put(newEmail, oauthAccountByEmail.remove(oauthIdToEmail(oauthId)));
        OauthRegistration oauthRegistration = oauthRegistrationRepository.findByOauthId(oauthId);
        if (oauthRegistration != null) {
            oauthRegistration.setEmail(newEmail);
            oauthRegistrationRepository.save(oauthRegistration);
        }
    }

    @Override
    public void changePassword(String oauthId, char[] newPassword) throws PasswordChangeNotPossibleException,
            OauthManagementException, OauthAccountNotFoundException {
        if (queryUserByOauthId(oauthId).getAuthenticationMethods().stream()
                .noneMatch(authenticationMethod ->
                        authenticationMethod.equals(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))) {
            throw new PasswordChangeNotPossibleException("The password could not be changed");
        }
        emailToPassword.replace(oauthIdToEmail(oauthId), String.valueOf(newPassword));
    }

    @Override
    public void changePasswordAndVerifyOldPassword(String oauthId, String email, char[] oldPassword, char[] newPassword)
            throws PasswordChangeNotPossibleException, OauthManagementException, OauthAccountNotFoundException,
            PasswordWrongException {
        if (queryUserByOauthId(oauthId).getAuthenticationMethods().stream()
                .noneMatch(authenticationMethod ->
                        authenticationMethod.equals(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))) {
            throw new PasswordChangeNotPossibleException("The password could not be changed");
        }

        String currentPassword = emailToPassword.get(email);
        if (!currentPassword.equals(String.valueOf(oldPassword))) {
            throw new PasswordWrongException(
                    "Could not log in to Auth0, wrong password was provided by person with email '{}'", email);
        }
        // increment login count due to a simulated successful login
        OauthAccount oauthAccount = queryUserByOauthId(oauthId);
        oauthAccount.setLoginCount(oauthAccount.getLoginCount() + 1);
        emailToPassword.replace(oauthAccount.getName(), String.valueOf(newPassword));
    }

    @Override
    public void blockUser(String oauthId) {
        OauthAccount oauthAccount = queryUserByOauthId(oauthId);
        oauthAccount.setBlocked(true);
        oauthAccount.setBlockReason(OauthAccount.BlockReason.MANUALLY_BLOCKED);
    }

    @Override
    public void unblockUser(String oauthId) {
        OauthAccount oauthAccount = queryUserByOauthId(oauthId);
        oauthAccount.setBlocked(false);
        oauthAccount.setBlockReason(null);
    }

    @Override
    public void unblockAutomaticallyBlockedUser(String oauthId) {
        OauthAccount oauthAccount = queryUserByOauthId(oauthId);
        oauthAccount.setBlocked(false);
        oauthAccount.setBlockReason(null);
    }

    @Override
    public void triggerResetPasswordMail(String email) throws OauthManagementException {
        if (!oauthAccountByEmail.containsKey(email)) {
            throw new OauthManagementException("User with email " + email + " has not been created");
        }
    }

    @Override
    public void triggerVerificationMail(String oauthId) throws OauthManagementException {
        queryUserByOauthId(oauthId);
        if (!oauthAccountByEmail.containsKey(oauthIdToEmail(oauthId))) {
            throw new OauthManagementException("User with email " + oauthIdToEmail(oauthId) + " has not been created");
        }
    }

    @Override
    public void setEmailVerified(String oauthId, boolean emailVerified) throws OauthManagementException {
        OauthAccount oauthAccount = queryUserByOauthId(oauthId);
        if (!oauthAccountByEmail.containsKey(oauthIdToEmail(oauthId))) {
            throw new OauthManagementException("User with email " + oauthIdToEmail(oauthId) + " has not been created");
        }
        oauthAccount.setEmailVerified(emailVerified);
    }

    @Override
    public String getOauthIdForUser(String email) throws OauthManagementException {
        // this might need to be adjusted if we also want to find non-custom accounts
        if (oauthAccountByEmail.containsKey(email)) {
            return emailToOauthId(email);
        }
        return null;
    }

    @Override
    public String getOauthIdForCustomUser(String email) throws OauthManagementException {
        if (oauthAccountByEmail.containsKey(email)) {
            return emailToOauthId(email);
        }
        return null;
    }

    @Override
    public void revokeAllRefreshTokens(String oauthId) throws OauthManagementException {
        throw new OauthManagementException("Not implemented.", new UnsupportedOperationException());
    }

    @Override
    public void deleteUser(String oauthId) throws OauthManagementException {
        log.info("Simulated deleting of user with id {}", oauthId);
        emailToPassword.remove(oauthIdToEmail(oauthId));
        OauthAccount removed = oauthAccountById.remove(oauthId);
        if (removed == null) {
            throw OauthAccountNotFoundException.forOauthId(oauthId);
        }
        oauthAccountByEmail.values().remove(removed);

        //if the deletion was successful, the saved entity at the platform needs to be deleted, too
        oauthRegistrationRepository.deleteByOauthId(oauthId);
    }

    public void blockUser(Person person, OauthAccount.BlockReason blockReason) {
        OauthAccount oauthAccount = queryUserByOauthId(person.getOauthId());
        oauthAccount.setBlocked(true);
        oauthAccount.setBlockReason(blockReason);
    }

    public void unblockUser(Person person) {
        OauthAccount oauthAccount = queryUserByOauthId(person.getOauthId());
        oauthAccount.setBlocked(false);
        oauthAccount.setBlockReason(null);
    }

    @Override
    public boolean isBlocked(String oauthId) {
        return queryUserByOauthId(oauthId).isBlocked();
    }

    @Override
    public OauthAccount queryUserByOauthId(String oauthId) throws OauthAccountNotFoundException {

        final OauthAccount oAuthAccount = oauthAccountById.get(oauthId);
        if (oAuthAccount == null) {
            throw OauthAccountNotFoundException.forOauthId(oauthId);
        }
        return filterDuplicatedOauthAccountDevices(oAuthAccount);
    }

    @Override
    public OauthAccount queryUserByEmail(String email) throws OauthAccountNotFoundException, OauthManagementException {

        final OauthAccount oAuthAccount = oauthAccountByEmail.get(email);
        if (oAuthAccount == null) {
            throw OauthAccountNotFoundException.forEmail(email);
        }
        return filterDuplicatedOauthAccountDevices(oAuthAccount);
    }

    @Override
    public List<OauthClient> getConfiguredOauthClients() throws OauthManagementException {
        return Collections.emptyList();
    }

    @Override
    public LoginHint getLoginHint(Person person) throws OauthManagementException {

        final OauthAccount oauthAccount = oauthAccountById.get(person.getOauthId());
        return LoginHint.builder()
                .authenticationMethods(oauthAccount.getAuthenticationMethods())
                .passwordChangeable(isPasswordChangeable(oauthAccount))
                .build();
    }

    // password is only changeable when the user has username/password authenticationMethod
    private boolean isPasswordChangeable(OauthAccount oauthAccount) {
        return oauthAccount.getAuthenticationMethods().contains(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD);
    }

    public Map<String, String> getEmailToPasswordMap() {
        return Collections.unmodifiableMap(emailToPassword);
    }

    public void addOAuthAccount(OauthAccount oAuthAccount, String email) {
        oauthAccountById.put(oAuthAccount.getOauthId(), oAuthAccount);
        oauthAccountByEmail.put(email, oAuthAccount);
    }

    private String emailToOauthId(String email) {
        return "custom|" + email;
    }

    private String oauthIdToEmail(String oauthId) {
        return oauthId.replace("custom|", "");
    }

    public boolean userWithOauthIdExists(String oauthId) {
        return oauthAccountById.containsKey(oauthId);
    }

    private OauthAccount filterDuplicatedOauthAccountDevices(OauthAccount oauthAccount) {

        List<OauthAccount.OauthAccountDevice> oauthAccountDevices = oauthAccount.getDevices();
        if (!CollectionUtils.isEmpty(oauthAccountDevices)) {
            oauthAccount.setDevices(oauthAccountDevices.stream()
                    .distinct()
                    .collect(Collectors.toList()));
        }
        return oauthAccount;
    }

}
