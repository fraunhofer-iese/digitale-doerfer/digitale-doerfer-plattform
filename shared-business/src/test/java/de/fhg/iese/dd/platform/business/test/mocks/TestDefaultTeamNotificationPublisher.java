/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.IDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Component
@Profile("test")
@Log4j2
public class TestDefaultTeamNotificationPublisher implements IDefaultTeamNotificationPublisher {

    @Getter
    private final List<Triple<String, Tenant, TeamNotificationMessage>> sentMessages = new ArrayList<>();

    @Override
    public void sendTeamNotification(String topic, Tenant tenant, TeamNotificationMessage message) {
        sentMessages.add(Triple.of(topic, tenant, message));
    }

    public void reset() {
        sentMessages.clear();
    }

}
