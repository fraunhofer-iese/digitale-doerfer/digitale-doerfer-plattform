/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.test.mocks;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.ITeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.msteams.MSTeamsTeamNotificationPublisherProvider;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.extern.log4j.Log4j2;

@Profile("test | local")//The publisher needs to be available in test to ensure that the configuration check is working
@Log4j2
@Component
public class TestMSTeamNotificationPublisherProvider extends MSTeamsTeamNotificationPublisherProvider {

    @Override
    public ITeamNotificationPublisher newPublisher(TeamNotificationConnection connection) {
        return request -> log.log(request.getMessage().getPriority().getLogLevel(),
                "Team notification MS Teams {}: {}\n{}\n-----------------\n{}\n-----------------",
                request.getMessage().getPriority(),
                request.getTopic(),
                request.getTenantName("-"),
                request.getMessage().getPlainText());
    }

}
