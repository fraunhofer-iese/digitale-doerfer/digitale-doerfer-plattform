/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Jannis von Albedyll
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;

public class DataPrivacyServiceExclusionPatternTest {

    @Test
    public void testExcludedPatterns() {

        List<Pattern> excludedExpressions = new ArrayList<>();
        excludedExpressions.add(Pattern.compile("digdorfdev(.+)\\@gmail\\.com"));
        excludedExpressions.add(Pattern.compile("digdorfdev\\@gmail\\.com"));
        excludedExpressions.add(Pattern.compile("digitaledoerfer(.+)\\@gmail\\.com"));
        excludedExpressions.add(Pattern.compile("digitaledoerfer\\@gmail\\.com"));
        excludedExpressions.add(Pattern.compile("(.+)\\@betzdorf\\.de\\.example"));
        excludedExpressions.add(Pattern.compile("(.+)\\@dbk\\.de\\.example"));
        excludedExpressions.add(Pattern.compile("(.+)\\@digitale-doerfer\\.de"));

        InactivityDataPrivacyService service = new InactivityDataPrivacyService();
        DataPrivacyConfig dataPrivacyConfig = new DataPrivacyConfig();
        dataPrivacyConfig.setUserInactivityExcludes(excludedExpressions);

        service.setConfig(dataPrivacyConfig);

        assertTrue(service.isEmailAddressExcludedByInactivity("digdorfdev@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("support@digitale-doerfer.de"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digitaledoerfer+emma@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digdorfdev+helpdesk-DBK@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digitaledoerfer+buecherei@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digitaledoerfer@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digdorfdev+CeBIT-Shop-2@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digdorfdev+Edith-Kauth-Drogerie/Reformhaus@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("digdorfdev+Edeka-Aktiv-Markt-Franz-Rös-e.K.@gmail.com"));
        assertTrue(service.isEmailAddressExcludedByInactivity("person1@betzdorf.de.example"));
        assertTrue(service.isEmailAddressExcludedByInactivity("person1@dbk.de.example"));

        assertFalse(service.isEmailAddressExcludedByInactivity("digdorfdev@gmail,com"));
        assertFalse(service.isEmailAddressExcludedByInactivity("peter.hase@gmail.com"));
        assertFalse(service.isEmailAddressExcludedByInactivity("digdorfdev@gmx.com"));
    }

}
