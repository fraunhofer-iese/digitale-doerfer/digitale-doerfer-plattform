/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.deletion;

import java.util.Collection;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;

public interface IReferenceDataDeletionHandler {

    /**
     * Id that is used for identification in logs. It is the prefix of the id of the strategies.
     */
    String getId();

    /**
     * All {@link SwapReferencesDeletionStrategy}s the handler registers.
     */
    Collection<SwapReferencesDeletionStrategy<?>> getSwapReferencesDeletionStrategies();

    /**
     * All {@link DeleteDependentEntitiesDeletionStrategy}s the handler registers.
     */
    Collection<DeleteDependentEntitiesDeletionStrategy<?>> getDeleteDependentEntitiesDeletionStrategies();

}
