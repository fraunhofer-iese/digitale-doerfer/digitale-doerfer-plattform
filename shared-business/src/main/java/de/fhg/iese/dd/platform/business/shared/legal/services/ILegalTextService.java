/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.legal.services;

import java.util.List;
import java.util.Map;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shared.legal.LegalTextStatus;
import de.fhg.iese.dd.platform.business.shared.legal.exceptions.LegalTextNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

public interface ILegalTextService extends IEntityService<LegalText> {

    /**
     * Get the current relevant legal texts for the app variant. A legal text for this app variant is relevant if the
     * current time is in its validity time {@link LegalText#getDateActive()} and {@link LegalText#getDateInactive()}
     *
     * @param appVariant the app variant to get the legal texts for
     *
     * @return all current active legal texts, both required and not required ones.
     */
    List<LegalText> getCurrentLegalTexts(AppVariant appVariant);

    /**
     * Get the acceptance status of the given person for the legal texts of the given app variant.
     * <p/>
     * If all legal texts that are required are accepted, {@link LegalTextStatus#isAllRequiredAccepted()} is true.
     * <p/>
     * The texts in {@link LegalTextStatus#getOpenLegalTexts()} are all non accepted texts, both required and not
     * required.
     *
     * @param appVariant the app variant to get the status for
     * @param person     the person to check for legal text acceptance
     *
     * @return all accepted legal texts and their acceptance, all open legal texts and an aggregation if all required
     *         texts have been accepted
     */
    LegalTextStatus getCurrentLegalTextStatus(AppVariant appVariant, Person person);

    /**
     * Get a legal text by its identifier {@link LegalText#getLegalTextIdentifier()}
     *
     * @param legalTextIdentifiers the identifier of the text to look up
     *
     * @return the legal text with this identifier if it exists
     *
     * @throws LegalTextNotFoundException if no legal text with this identifier could be found
     */
    List<LegalText> getLegalTextsByIdentifier(List<String> legalTextIdentifiers) throws LegalTextNotFoundException;

    /**
     * Accept legal texts for that person.
     * <p/>
     * If a legal text was accepted already, the timestamp of the acceptance will not change.
     * <p/>
     * Both optional or required texts can be accepted. There is no check if the text is currently active (if the
     * current time is in its validity time {@link LegalText#getDateActive()} and {@link LegalText#getDateInactive()}).
     * Also inactive texts can be accepted.
     *
     * @param person             the person that read the texts and wants to accept them
     * @param legalTextsToAccept the texts to be accepted by the peson
     *
     * @return the acceptances of the texts and the according texts
     */
    Map<LegalText, LegalTextAcceptance> acceptLegalTexts(Person person, List<LegalText> legalTextsToAccept);

    /**
     * Copy the legal texts from the default file location ({@link IFileStorage#getFileFromDefault(String)} to the
     * static file location.
     * <p/>
     * As folder for the copied static legal texts {@link LegalTextService#LEGAL_PATH} is used.
     *
     * @param defaultFileNameSource     the file name pointing to the file location in the default file location
     * @param legalTextIdentifierTarget the {@link LegalText#getLegalTextIdentifier()} of the legal text that should
     *                                  reference the copied file in {@link LegalText#getUrlText()}
     *
     * @return the external URL ready to be used in  {@link LegalText#getUrlText()}
     */
    String copyLegalTextFromDefault(String defaultFileNameSource, String legalTextIdentifierTarget);

    List<LegalTextAcceptance> findAllAcceptancesByPersonOrderByTimestamp(Person person);

    long deleteLegalTextsAppVariant(AppVariant appVariant);

}
