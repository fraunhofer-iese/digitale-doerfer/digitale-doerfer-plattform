/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.security;

import java.util.Collection;
import java.util.Set;

import de.fhg.iese.dd.platform.business.shared.security.services.Action;
import de.fhg.iese.dd.platform.business.shared.security.services.ActionRoleRelatedEntityIdRestricted;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

/**
 * This action restricts the tenants that can be used in the action. It can either be all, a subset or no tenants.
 *
 * @see Action
 * @see ActionRoleRelatedEntityIdRestricted
 */
public abstract class ActionTenantRestricted extends ActionRoleRelatedEntityIdRestricted<PermissionTenantRestricted> {

    /**
     * If a {@link RoleAssignment} with one of these roles exist, the action is allowed on all tenants.
     */
    protected abstract Collection<Class<? extends BaseRole<?>>> rolesAllTenantsAllowed();

    /**
     * For all {@link RoleAssignment}s with one of these roles, the action is allowed on the tenant id of {@link
     * RoleAssignment#getRelatedEntityId()}.
     */
    protected abstract Collection<Class<? extends BaseRole<?>>> rolesSpecificTenantsAllowed();

    @Override
    protected String entitiesName() {
        return "tenants";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllEntitiesAllowed() {
        return rolesAllTenantsAllowed();
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificEntitiesAllowed() {
        return rolesSpecificTenantsAllowed();
    }

    @Override
    protected PermissionTenantRestricted createPermission(Set<String> allowedEntityIds, boolean allEntitiesAllowed) {
        return new PermissionTenantRestricted(allowedEntityIds, allEntitiesAllowed);
    }

    @Override
    protected PermissionTenantRestricted createAllDeniedPermission() {
        return PermissionTenantRestricted.ALL_DENIED;
    }

}
