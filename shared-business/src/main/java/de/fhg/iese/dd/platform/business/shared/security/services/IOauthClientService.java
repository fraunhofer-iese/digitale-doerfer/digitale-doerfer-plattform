/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthClientNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

import java.util.Optional;

public interface IOauthClientService extends IEntityService<OauthClient>, ICachingComponent {

    OauthClient findByOauthClientIdentifier(String oAuthClientIdentifier) throws OauthClientNotFoundException;

    Optional<OauthClient> findByOauthClientIdentifierOptional(String oAuthClientIdentifier);

}
