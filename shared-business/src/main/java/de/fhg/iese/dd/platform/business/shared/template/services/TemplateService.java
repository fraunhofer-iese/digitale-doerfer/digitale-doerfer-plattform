/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.template.services;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
class TemplateService extends BaseService implements ITemplateService {

    @Override
    public String createTextWithTemplateMapModel(TemplateLocation templateLocation, Map<String, Object> templateModel) throws
            IOException, TemplateException {

        return createTextWithTemplateObjectModel(templateLocation, templateModel);
    }

    @Override
    public String createTextWithTemplateObjectModel(TemplateLocation templateLocation, Object templateModel) throws
            IOException, TemplateException {

        final Configuration freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_23);
        freemarkerConfiguration.setDefaultEncoding("UTF-8");
        freemarkerConfiguration.setOutputEncoding("UTF-8");
        freemarkerConfiguration.setClassForTemplateLoading(templateLocation.getClass(), templateLocation.getFolder());
        final Template template = freemarkerConfiguration.getTemplate(templateLocation.getName());

        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        log.debug("Used template {}", templateLocation);
        return text;
    }

}
