/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.feature.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotEnabledException;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.InvalidFeatureConfigurationException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IFeatureService extends IService, ICachingComponent {

    /**
     * Gets a feature configured for this target.
     *
     * @return the feature in the according configuration
     *
     * @throws FeatureNotFoundException if the feature is not configured
     */
    @NonNull
    <F extends BaseFeature> F getFeature(@NonNull Class<F> featureClass, @NonNull FeatureTarget featureTarget)
            throws FeatureNotFoundException;

    /**
     * Gets a feature configured for this target or null.
     *
     * @return the feature in the according configuration or null if the feature is not configured
     */
    @Nullable
    <F extends BaseFeature> F getFeatureOrNull(@NonNull Class<F> featureClass, @NonNull FeatureTarget featureTarget);

    /**
     * Gets an optional for this feature configured for this target.
     *
     * @return the feature in the according configuration, empty if the feature is not configured
     */
    <F extends BaseFeature> Optional<F> getFeatureOptional(@NonNull Class<F> featureClass,
            @NonNull FeatureTarget featureTarget);

    /**
     * Returns true if the feature is enabled, false if it is not enabled or not configured.
     * <p>
     * It will not throw {@link FeatureNotFoundException} and consider the feature as not enabled in this case.
     */
    boolean isFeatureEnabled(@NonNull Class<? extends BaseFeature> featureClass, @NonNull FeatureTarget featureTarget);

    /**
     * Checks if the feature is enabled and throws {@link FeatureNotEnabledException} if it is not enabled. It will not
     * throw {@link FeatureNotFoundException} and consider the feature as not enabled in this case.
     *
     * @throws FeatureNotEnabledException if the feature is not enabled
     */
    void checkFeatureEnabled(@NonNull Class<? extends BaseFeature> featureClass, @NonNull FeatureTarget featureTarget)
            throws FeatureNotEnabledException;

    /**
     * Gets all configured features for the given target.
     *
     * @param featureTarget the feature configuration target
     * @param onlyEnabled   if true, only enabled features are returned
     *
     * @return all configured features for that target, ordered by {@link BaseFeature#getFeatureIdentifier()}.
     */
    @NonNull
    List<BaseFeature> getAllFeaturesForTarget(FeatureTarget featureTarget, boolean onlyEnabled);

    /**
     * Gets all feature configurations for the given feature class.
     * <p/>
     * <b>This is an uncached method, do not use for processing requests!</b>
     *
     * @param featureClass the feature class
     *
     * @return all configurations of the feature class mapped to the feature config
     */
    @NonNull
    <F extends BaseFeature> Map<FeatureConfig, F> getAllFeatureConfigurations(@NonNull Class<F> featureClass);

    /**
     * Calculates all meta models of the features in the given modules. Very expensive method, only intended for
     * developer documentation.
     *
     * @return meta models (property names) of the features in the given module.
     */
    List<FeatureMetaModel> getAllFeatureMetaModels();

    /**
     * Deletes all feature configurations for the given app variant. The configurations should also be deleted from the
     * data-init files.
     *
     * @param appVariant the app variant that should be removed
     *
     * @return number of deleted configurations.
     */
    long deleteFeatureConfigAppVariant(AppVariant appVariant);

    /**
     * Internal method required for feature data init.
     */
    List<FeatureConfig> getAllFeatureConfigs();

    /**
     * Internal method required for feature data init.
     */
    List<? extends Class<? extends BaseFeature>> getFeatureClasses();

    /**
     * Internal method required for feature data init. Checks if the feature config can be parsed.
     */
    void checkIsFeatureConfigValid(FeatureConfig featureConfig) throws InvalidFeatureConfigurationException;

}
