/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

/**
 * A person deletion report contains information on which person related information has been deleted/erased.
 */
public interface IPersonDeletionReport {

    /**
     * Information on an entity that has been deleted from the database or been made anonymous ("erased").
     */
    interface IDeletedEntity {

        Class<? extends BaseEntity> getEntityClass();

        String getId();

        DeleteOperation getOperation();
    }

    /**
     * Add a deleted/erased entity to the report
     *
     * @param entity should not be {@code null}. If {@code null}, a warning is logged (no hard exception in order
     *               to continue the deletion process)
     * @param operation Use {@link DeleteOperation#DELETED} to indicate that the entity is deleted from the database.
     *                  Use {@link DeleteOperation#ERASED} to indicate that the data of the entity is made anonymous.
     */
    void addEntry(BaseEntity entity, DeleteOperation operation);

    /**
     * Adds all entities to the report by calling {@link IPersonDeletionReport#addEntry(BaseEntity, DeleteOperation)}
     * for each entity with the given operation
     *
     * @param entities
     * @param operation one delete operation for all entities
     */
    void addEntries(Iterable<? extends BaseEntity> entities, DeleteOperation operation);

    /**
     * List of deleted entities sorted by processed timestamp
     *
     * @return never {@code null}
     */
    List<IDeletedEntity> getDeletedEntities();
}
