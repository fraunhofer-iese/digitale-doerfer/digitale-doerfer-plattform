/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.processors;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminRequest;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagRequest;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagStatusChangeConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagStatusChangeRequest;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;

@EventProcessor
class UserGeneratedContentFlagEventProcessor extends BaseEventProcessor {

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @EventProcessing
    private UserGeneratedContentFlagStatusChangeConfirmation changeFlagStatus(
            UserGeneratedContentFlagStatusChangeRequest request) {

        final UserGeneratedContentFlag updatedFlag = userGeneratedContentFlagService.updateStatus(
                request.getFlag(),
                request.getInitiator(),
                request.getNewStatus(),
                request.getComment());

        return new UserGeneratedContentFlagStatusChangeConfirmation(updatedFlag);
    }

    @EventProcessing
    private List<BaseEvent> onFlaggedEntityDeletion(UserGeneratedContentFlagDeleteFlagEntityByAdminRequest request) {

        final DeletedFlagReport deletedFlagReport =
                userGeneratedContentFlagService.deleteFlaggedEntity(request.getFlag());

        final UserGeneratedContentFlag updatedFlag = userGeneratedContentFlagService.updateStatus(
                request.getFlag(),
                request.getInitiator(),
                UserGeneratedContentFlagStatus.ACCEPTED,
                request.getComment());

        return Arrays.asList(
                new UserGeneratedContentFlagStatusChangeConfirmation(updatedFlag),
                UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation.builder()
                        .changedFlag(updatedFlag)
                        .flagReport(deletedFlagReport)
                        .build());
    }

    @EventProcessing(includeSubtypesOfEvent = true)
    private UserGeneratedContentFlagConfirmation createUserGeneratedContentFlag(
            UserGeneratedContentFlagRequest<?> request) {

        final UserGeneratedContentFlag createdFlag;
        if (request.isAllowMultipleFlagsOfSameEntity()) {
            createdFlag = userGeneratedContentFlagService.createOrUpdateStatus(
                    request.getEntity(),
                    request.getEntityAuthor(),
                    request.getTenant(),
                    request.getFlagCreator(),
                    request.getComment());
        } else {
            createdFlag = userGeneratedContentFlagService.create(
                    request.getEntity(),
                    request.getEntityAuthor(),
                    request.getTenant(),
                    request.getFlagCreator(),
                    request.getComment());
        }
        return UserGeneratedContentFlagConfirmation.builder()
                .createdFlag(createdFlag)
                .flaggedEntity(request.getEntity())
                .flaggedEntityAuthor(request.getEntityAuthor())
                .flaggedEntityDescription(request.getEntityDescription())
                .comment(request.getComment())
                .build();
    }

}
