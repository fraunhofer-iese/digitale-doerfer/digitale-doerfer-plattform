/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.security;

import java.util.Collections;
import java.util.Set;

import de.fhg.iese.dd.platform.business.shared.security.services.Permission;
import de.fhg.iese.dd.platform.business.shared.security.services.PermissionEntityIdRestricted;
import de.fhg.iese.dd.platform.business.shared.security.services.PermissionEntityRestricted;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

/**
 * Permission that grants permission per tenant id.
 *
 * @see Permission
 * @see PermissionEntityIdRestricted
 */
public class PermissionTenantRestricted extends PermissionEntityRestricted<Tenant> {

    public static final PermissionTenantRestricted ALL_DENIED =
            new PermissionTenantRestricted(Collections.emptySet(), false);

    protected PermissionTenantRestricted(Set<String> allowedTenantIds, boolean allTenantsAllowed) {
        super(allowedTenantIds, allTenantsAllowed);
    }

    /**
     * @return the ids of allowed tenants
     */
    public Set<String> getAllowedTenantIds() {
        return getAllowedEntityIds();
    }

    /**
     * @return true if all tenants are allowed
     */
    public boolean isAllTenantsAllowed() {
        return isAllEntitiesAllowed();
    }

    /**
     * @param tenantId
     *         the id of the tenant to check
     *
     * @return if all tenants are allowed or the given tenant id is in the set of allowed tenants ids.
     */
    public boolean isTenantAllowed(String tenantId) {
        return isEntityIdAllowed(tenantId);
    }

    /**
     * @param tenant
     *         the tenant to check, can be null
     *
     * @return {@link #isTenantAllowed(String)} with the id of the tenant or false if the tenant is null
     */
    public boolean isTenantAllowed(Tenant tenant) {
        if (tenant == null) {
            return isAllEntitiesAllowed();
        }
        return isEntityIdAllowed(tenant.getId());
    }

    /**
     * Inversion of {@link #isTenantAllowed(String)}
     */
    public boolean isTenantDenied(String tenantId) {
        return isEntityIdDenied(tenantId);
    }

    /**
     * @param tenant the tenant to check, can be null
     *
     * @return {@link #isTenantDenied(String)} with the id of the tenant or true if the tenant is null
     */
    public boolean isTenantDenied(Tenant tenant) {
        return !isTenantAllowed(tenant);
    }

}
