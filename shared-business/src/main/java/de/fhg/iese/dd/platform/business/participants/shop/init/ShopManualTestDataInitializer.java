/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.shop.init;

import java.util.Collection;
import java.util.Collections;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;

@Component
public class ShopManualTestDataInitializer extends ShopBaseDataInitializer implements IManualTestDataInitializer {

    //ids according to json, referenced by other data initializers in code
    public static final String TEST_DEMO_LIDL_UUID     = "a17f6e1a-09c7-4240-b42a-3c03403850ce";
    public static final String TEST_DEMO_KAUFLAND_UUID = "1b0e8952-b3df-4e2c-880e-89551214229c";

    @Override
    public String getTopic() {
        return "shop-manual-test";
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return Collections.emptyList();
    }

}
