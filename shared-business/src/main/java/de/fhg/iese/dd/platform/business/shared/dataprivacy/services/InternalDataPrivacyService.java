/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import de.fhg.iese.dd.platform.business.framework.datadependency.services.IDataDependencyAwareOrderService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.IDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
class InternalDataPrivacyService extends BaseDataPrivacyService implements IInternalDataPrivacyService {

    @Autowired
    private IDataPrivacyReportConverterService converterService;
    @Autowired
    private IPluginService pluginService;
    @Autowired
    private IDataDependencyAwareOrderService dataDependencyAwareOrderService;
    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;

    //Calling dataDependencyAwareOrderService only works here, because lombok's lazy getter moves this into a code
    //block that is not executed before the getter is called for the first time
    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final List<IDataPrivacyHandler> dataPrivacyHandlersRequiredBeforeProcessed =
            dataDependencyAwareOrderService.orderRequiredBeforeProcessed(
                    pluginService.getPluginInstancesRaw(PluginTarget.of(IDataPrivacyHandler.class)));

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final List<IDataPrivacyHandler> dataPrivacyHandlersProcessedBeforeRequired =
            dataDependencyAwareOrderService.orderProcessedBeforeRequired(
                    pluginService.getPluginInstancesRaw(PluginTarget.of(IDataPrivacyHandler.class)));

    @Override
    public void collectInternalUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        log.info("Starting collecting internal data of person {}", person.getId());
        //we want to have them ordered so that all required entities are collected before
        List<IDataPrivacyHandler> handlers = getDataPrivacyHandlersRequiredBeforeProcessed();
        long waitTime = dataPrivacyConfig.getWaitTimeBetweenDataPrivacyHandlers().toMillis();

        for (IDataPrivacyHandler handler : handlers) {
            //every handler is executed, even if some fails with exception
            try {
                log.debug("Calling " + handler.getClass().getName());
                handler.collectUserData(person, dataPrivacyReport);
                //we do not want to call each data privacy handler quickly one after the other to avoid high database loads
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            } catch (Exception e) {
                log.error(handler.getClass().getSimpleName() + " failed to collect user data", e);
            }
        }
        log.info("Finished collecting internal data of person {}", person.getId());
    }

    @Override
    public void deleteInternalUserData(Person personToBeDeleted, IPersonDeletionReport personDeletionReport) {

        log.info("Starting deletion of person with id {}", personToBeDeleted.getId());
        Objects.requireNonNull(personToBeDeleted);

        //first handle the "leaves" in the dependency tree
        List<IDataPrivacyHandler> handlers = getDataPrivacyHandlersProcessedBeforeRequired();
        long waitTime = dataPrivacyConfig.getWaitTimeBetweenDataPrivacyHandlers().toMillis();

        try {
            for (IDataPrivacyHandler handler : handlers) {
                log.debug("Calling {}", handler.getClass().getName());
                try {
                    handler.deleteUserData(personToBeDeleted, personDeletionReport);
                } catch (Exception e) {
                    log.error(handler.getClass().getSimpleName() + " failed to delete user data", e);
                    throw e;
                }
                //we do not want to call each data privacy handler quickly one after the other to avoid high database loads
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        } catch (Exception e) {
            //we want to ensure that we are notified of such a failed deletion
            teamNotificationService.sendTeamNotification(personToBeDeleted.getTenant(),
                    TEAM_NOTIFICATION_TOPIC_USER_DELETION,
                    TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                    "Could not completely delete person ```{}```:```{}``` ", personToBeDeleted, e.getMessage());
            log.error("Could not completely delete person {}. Deleted entities so far:\n{}", personToBeDeleted,
                    converterService.convertToJson(personDeletionReport));
            throw e;
        }
        log.info("Deletion report for person with id {}: \n{}", personToBeDeleted.getId(),
                converterService.convertToJson(personDeletionReport));
    }

}
