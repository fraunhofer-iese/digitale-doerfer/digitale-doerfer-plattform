/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2018 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.init;

import java.util.Collection;
import java.util.Collections;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class PersonDemoDataInitializer extends PersonBaseDataInitializer implements IManualTestDataInitializer {

    public static final String PERSON_ID_DEMO_1 = "a7908099-9830-4c11-ad9a-db25865e3b00";
    public static final String PERSON_ID_DEMO_2 = "89c78459-d686-4f47-b506-5cbe1e733e35";
    public static final String PERSON_ID_DEMO_3 = "26d2280d-0a63-49e1-bad1-fbfcc6d70ce0";
    public static final String PERSON_ID_DEMO_4 = "e595511a-b72f-4d7e-bc78-d8cf9754862b";
    public static final String PERSON_ID_DEMO_5 = "0e659289-d2b8-45d7-9616-f0000be9205f";

    @Override
    public String getTopic() {
        return TOPIC_DEMO;
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return Collections.emptyList();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary){
        logSummary.info("Demo persons are not dropped");
    }

}
