/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ListOauthClientConfigurationAdminTask extends BaseAdminTask {

    @Autowired
    private IAppService appService;
    @Autowired
    private IEnvironmentService environmentService;

    @Override
    public String getName() {
        return "ListOauthClientConfiguration";
    }

    @Override
    public String getDescription() {
        return "Lists the current oauth client configuration per app variant";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {

        List<AppVariant> allAppVariants = appService.findAllAppVariants().stream()
                .sorted(Comparator.comparing(AppVariant::getAppVariantIdentifier))
                .collect(Collectors.toList());

        Map<OauthClient, List<AppVariant>> appVariantsByOauthClient = allAppVariants.stream()
                .flatMap(a -> a.getOauthClients().stream()
                        .map(c -> Pair.of(c, a)))
                .collect(Collectors.groupingBy(Pair::getKey,
                        Collectors.mapping(Pair::getRight, Collectors.toList())));

        logSummary.info("===== Oauth client config for {} =====", environmentService.getEnvironmentFullName());
        logSummary.info("===== App variants by oauth client identifier =====");
        List<Map.Entry<OauthClient, List<AppVariant>>> entries = appVariantsByOauthClient.entrySet().stream()
                .sorted(Comparator.comparing(e -> getOauthClientName(e.getKey())))
                .collect(Collectors.toList());
        for (Map.Entry<OauthClient, List<AppVariant>> oauthClientAndAppVariants : entries) {
            OauthClient oauthClient = oauthClientAndAppVariants.getKey();
            List<AppVariant> appVariants = oauthClientAndAppVariants.getValue();
            printOauthClient(logSummary, oauthClient);
            logSummary.indent();
            if (CollectionUtils.isEmpty(appVariants)) {
                logSummary.info("- no app variants -");
            } else {
                appVariants.stream()
                        .sorted(Comparator.comparing(AppVariant::getAppVariantIdentifier))
                        .forEach(appVariant -> printAppVariant(logSummary, appVariant));
            }
            logSummary.outdent();
        }

        logSummary.info("===== Oauth client identifiers by app variants =====");
        for (AppVariant appVariant : allAppVariants) {
            Set<OauthClient> oauthClients = appVariant.getOauthClients();
            printAppVariant(logSummary, appVariant);
            logSummary.indent();
            if (CollectionUtils.isEmpty(oauthClients)) {
                logSummary.info("- no oauth clients -");
            } else {
                oauthClients.stream()
                        .sorted(Comparator.comparing(ListOauthClientConfigurationAdminTask::getOauthClientName))
                        .forEach(oauthClient -> printOauthClient(logSummary, oauthClient));
            }
            logSummary.outdent();
        }
    }

    private void printOauthClient(LogSummary logSummary, OauthClient oauthClient) {
        logSummary.info("{} ({})",
                oauthClient.getOauthClientIdentifier(),
                getOauthClientName(oauthClient));
    }

    private static String getOauthClientName(OauthClient oauthClient) {

        return Objects.toString(oauthClient.getName(), "-");
    }

    private void printAppVariant(LogSummary logSummary, AppVariant appVariant) {
        logSummary.info("{} ({}) [{}]",
                appVariant.getAppVariantIdentifier(),
                appVariant.getName(),
                appVariant.getApp().getName());
    }

}
