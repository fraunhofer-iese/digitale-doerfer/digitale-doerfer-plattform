/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

public class RoleManagementNotAllowedException extends NotAuthorizedException {

    private static final long serialVersionUID = 2240821289019008759L;

    public RoleManagementNotAllowedException(BaseRole<? extends NamedEntity> role, String relatedEntityId,
            String detail) {
        super("The current user is not allowed to manage role {} for related entity with ID {}: {}", role.getKey(),
                relatedEntityId, detail);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.ROLE_MANAGEMENT_NOT_ALLOWED;
    }

}
