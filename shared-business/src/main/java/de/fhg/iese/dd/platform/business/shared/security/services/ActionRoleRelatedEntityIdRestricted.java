/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

/**
 * This action restricts the entities that can be used in the action. It can either be all, a subset or no entities.
 *
 * @param <P>
 *         the type of permission
 *
 * @see Action
 */
public abstract class ActionRoleRelatedEntityIdRestricted<P extends PermissionEntityIdRestricted> extends Action<P> {

    /**
     * If a {@link RoleAssignment} with one of these roles exist, the action is allowed on all entities.
     */
    protected abstract Collection<Class<? extends BaseRole<?>>> rolesAllEntitiesAllowed();

    /**
     * For all {@link RoleAssignment}s with one of these roles, the action is allowed on the entity id of {@link
     * RoleAssignment#getRelatedEntityId()}.
     */
    protected abstract Collection<Class<? extends BaseRole<?>>> rolesSpecificEntitiesAllowed();

    @Override
    public P decidePermission(Collection<RoleAssignment> roleAssignments) {

        P allEntitiesAllowed = decideAllEntitiesAllowed(roleAssignments);
        if (allEntitiesAllowed != null) {
            return allEntitiesAllowed;
        }
        return decideSpecificEntitiesAllowed(roleAssignments);
    }

    @Override
    public String getPermissionDescription(Function<Class<? extends BaseRole<?>>, BaseRole<?>> roleMapper) {
        StringBuilder permissionDescription = new StringBuilder();
        if (!CollectionUtils.isEmpty(rolesAllEntitiesAllowed())) {
            permissionDescription.append("Callers with role assignments \"");
            permissionDescription.append(rolesAllEntitiesAllowed().stream()
                    .map(roleMapper)
                    .map(BaseRole::getKey)
                    .collect(Collectors.joining(", ")));
            permissionDescription.append("\" are allowed to do this action for all ");
            permissionDescription.append(entitiesName());
            permissionDescription.append(".");
        }
        if (!CollectionUtils.isEmpty(rolesSpecificEntitiesAllowed())) {
            if (permissionDescription.length() > 0) {
                permissionDescription.append("\n");
            }
            permissionDescription.append("Callers with role assignments \"");
            permissionDescription.append(rolesSpecificEntitiesAllowed().stream()
                    .map(roleMapper)
                    .map(BaseRole::getKey)
                    .collect(Collectors.joining(", ")));
            permissionDescription.append("\" are allowed to do this action for the related ");
            permissionDescription.append(entitiesName());
            permissionDescription.append(" of the role assignment.");
        }
        return permissionDescription.toString();
    }

    @Override
    public List<Class<? extends BaseRole<?>>> getPermittedRoles() {
        return Stream.concat(
                rolesAllEntitiesAllowed().stream()
                        .sorted(Comparator.comparing(Class::getSimpleName)),
                rolesSpecificEntitiesAllowed().stream()
                        .sorted(Comparator.comparing(Class::getSimpleName)))
                .distinct()
                .collect(Collectors.toList());
    }

    protected String entitiesName() {
        return "entities";
    }

    private P decideAllEntitiesAllowed(Collection<RoleAssignment> roleAssignments) {
        Collection<Class<? extends BaseRole<?>>> expectedRoles = rolesAllEntitiesAllowed();
        if (roleAssignments.stream()
                .map(RoleAssignment::getRole)
                .anyMatch(expectedRoles::contains)) {
            return createPermission(Collections.emptySet(), true);
        } else {
            return null;
        }
    }

    private P decideSpecificEntitiesAllowed(Collection<RoleAssignment> roleAssignments) {
        Collection<Class<? extends BaseRole<?>>> expectedRoles = rolesSpecificEntitiesAllowed();
        Set<String> relatedEntityIds = roleAssignments.stream()
                .filter(ra -> expectedRoles.contains(ra.getRole()))
                .map(RoleAssignment::getRelatedEntityId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (relatedEntityIds.isEmpty()) {
            return createAllDeniedPermission();
        } else {
            return createPermission(relatedEntityIds, false);
        }
    }

    /**
     * Implement it with a constructor for the specific permission of this action
     */
    protected abstract P createPermission(Set<String> allowedEntityIds, boolean allEntitiesAllowed);

    /**
     * Implement it with a reference to a static constant to an all denied permission of this action
     */
    protected abstract P createAllDeniedPermission();

}
