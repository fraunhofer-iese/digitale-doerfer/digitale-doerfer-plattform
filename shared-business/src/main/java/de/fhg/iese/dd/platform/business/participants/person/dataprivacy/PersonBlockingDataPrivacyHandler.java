/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.dataprivacy;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

@Component
public class PersonBlockingDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(PersonBlocking.class);

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Autowired
    private IParticipantsDataDeletionService participantsDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(final Person person, final IDataPrivacyReport dataPrivacyReport) {
        
        Collection<App> blockingApps = personBlockingService.getBlockingApps(person);

        if (CollectionUtils.isEmpty(blockingApps)) {
            return;
        }
        final IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Von dir blockierte Personen",
                        "Übersicht über die von dir blockierten Personen in den jeweiligen Anwendungen");

        for (final App app : blockingApps) {

            final IDataPrivacyReport.IDataPrivacyReportSubSection appSection =
                    section.newSubSection(app.getName(), "Blockierte Personen in " + app.getName());

            int i = 1;
            for (final Person blockedPerson : personBlockingService.getBlockedPersons(app, person)) {
                appSection.newContent(String.valueOf(i++), blockedPerson.getFirstNameFirstCharLastDot());
            }
        }
    }

    @Override
    public void deleteUserData(final Person person, final IPersonDeletionReport personDeletionReport) {
        // Person blockings where the deleted person is blocked will not be deleted
        // Otherwise content (especially chat messages) from blocked+deleted persons would be displayed
        //   at the blocking person when the blocked person is deleted
        personBlockingService.getBlockingsOfPerson(person)
                .forEach(personBlocking -> personDeletionReport.addEntry(personBlocking,
                        participantsDataDeletionService.deletePersonBlocking(personBlocking)));
    }

}
