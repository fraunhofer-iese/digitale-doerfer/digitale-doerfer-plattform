/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics.worker;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ConfiguredStatisticsReportCreationWorker implements IWorkerTask {

    @Autowired
    private IStatisticsService statisticsService;
    @Autowired
    private ITimeService timeService;

    @Override
    public String getName() {
        return "StatisticsReportCreationWorker";
    }

    @Override
    public Trigger getTaskTrigger() {
        return new CronTrigger("30 28 0 * * *", timeService.getLocalTimeZone());//every day at 00:28:30
    }

    @Override
    public void run() {
        final long start = System.currentTimeMillis();
        log.info("creating custom statistics reports triggered");
        List<StatisticsReportDefinition> createdReports =
                statisticsService.createConfiguredStatisticsReports();
        log.info("creating {} custom statistics reports finished in {} ms", createdReports.size(),
                System.currentTimeMillis() - start);
    }

}
