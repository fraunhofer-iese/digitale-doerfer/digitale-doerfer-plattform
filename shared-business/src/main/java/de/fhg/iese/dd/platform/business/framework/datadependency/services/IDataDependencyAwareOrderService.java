/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.datadependency.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.business.framework.datadependency.exceptions.DataDependencyUnsatisfiableException;

/**
 * Service to sort {@link IDataDependencyAware} according to their dependencies as defined by required and processed
 * entities.
 */
public interface IDataDependencyAwareOrderService {

    /**
     * Orders a collection of data dependency aware so, that all {@link IDataDependencyAware#getRequiredEntities()} are
     * before {@link IDataDependencyAware#getProcessedEntities()}. If there are multiple ways to solve the dependency
     * requirements they are ordered according to {@link IDataDependencyAware#getId()}.
     *
     * @param dataDependencyAwareToSort
     *         the data dependency aware to sort
     * @param <T>
     *         type of the data dependency aware
     *
     * @return sorted list according to the rules described above, containing all given data dependency aware
     *
     * @throws DataDependencyUnsatisfiableException
     *         if the dependency requirements can not be fulfilled
     */
    <T extends IDataDependencyAware> List<T> orderRequiredBeforeProcessed(Collection<T> dataDependencyAwareToSort)
            throws DataDependencyUnsatisfiableException;

    /**
     * Orders a collection of data dependency aware so, that all {@link IDataDependencyAware#getProcessedEntities()} are
     * before {@link IDataDependencyAware#getRequiredEntities()}. If there are multiple ways to solve the dependency
     * requirements they are ordered according to {@link IDataDependencyAware#getId()}.
     *
     * @param dataDependencyAwareToSort
     *         the data dependency aware to sort
     * @param <T>
     *         type of the data dependency aware
     *
     * @return sorted list according to the rules described above, containing all given data dependency aware
     *
     * @throws DataDependencyUnsatisfiableException
     *         if the dependency requirements can not be fulfilled
     */
    <T extends IDataDependencyAware> List<T> orderProcessedBeforeRequired(Collection<T> dataDependencyAwareToSort)
            throws DataDependencyUnsatisfiableException;

}
