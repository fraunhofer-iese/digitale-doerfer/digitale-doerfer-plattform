/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.callback.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ExternalException;

public class ExternalSystemCallFailedException extends ExternalException {

    private static final long serialVersionUID = 2783472607274424414L;

    public ExternalSystemCallFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExternalSystemCallFailedException(String message) {
        super(message);
    }

    public ExternalSystemCallFailedException(String message, Object... arguments) {
        super(message, arguments);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.EXTERNAL_SYSTEM_CALL_FAILED;
    }

}
