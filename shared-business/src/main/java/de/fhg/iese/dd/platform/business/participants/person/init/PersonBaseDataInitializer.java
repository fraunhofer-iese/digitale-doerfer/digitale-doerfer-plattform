/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.init;

import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreatedInDataInitEvent;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommonHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommunityHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public abstract class PersonBaseDataInitializer extends BaseDataInitializer {

    protected static final String DATA_JSON_SECTION_COMMUNITY_HELP_DESK = "help_desk";
    protected static final String DATA_JSON_SECTION_COMMON_HELP_DESK = "common_help_desk";
    protected static final String DATA_JSON_SECTION_COMMUNITY_ROLE_MANAGER = "role_manager";
    public static final String TOPIC_DEMO = "person-demo";

    @Autowired
    protected IAppService appService;

    @Autowired
    protected ITenantService tenantService;

    @Autowired
    protected IGeoAreaService geoAreaService;

    @Autowired
    protected IPersonService personService;

    @Autowired
    protected IRoleService roleService;

    @Autowired
    private IOauthManagementService oauthManagementService;

    @Autowired
    private IRandomService randomService;

    @Autowired
    protected ApplicationConfig appConfig;

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize(){
        eventBusAccessor.setOwner(this);
    }

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .requiredEntity(Tenant.class)
                .processedEntity(Person.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        for (Tenant tenant : tenantService.findAllOrderedByNameAsc()) {
            Map<String, List<Person>> entities = loadEntitiesFromDataJson(tenant, Person.class);
            List<OauthAccount> oauthAccounts = loadEntitiesFromDataJsonDefaultSection(tenant, OauthAccount.class);
            if (entities != null) {
                List<Person> persons = entities.get(DATA_JSON_SECTION_DEFAULT);
                if (persons != null) {
                    for (Person person : persons) {
                        createPerson(tenant, person, oauthAccounts, logSummary);
                    }
                }
                List<Person> helpDeskPersons = entities.get(DATA_JSON_SECTION_COMMUNITY_HELP_DESK);
                if (!CollectionUtils.isEmpty(helpDeskPersons)) {
                    for(Person helpDeskPerson : helpDeskPersons){
                        helpDeskPerson = createPerson(tenant, helpDeskPerson, oauthAccounts, logSummary);
                        roleService.assignRoleToPerson(helpDeskPerson, CommunityHelpDesk.class, tenant);
                        logSummary.info("Assigned {} role COMMUNITY_HELP_DESK for {}", helpDeskPerson.toString(),
                                tenant.toString());
                    }
                }
                List<Person> commonHelpDeskPersons = entities.get(DATA_JSON_SECTION_COMMON_HELP_DESK);
                if(!CollectionUtils.isEmpty(commonHelpDeskPersons)){
                    for(Person commonHelpDeskPerson : commonHelpDeskPersons){
                        commonHelpDeskPerson = createPerson(tenant, commonHelpDeskPerson, oauthAccounts, logSummary);
                        roleService.assignRoleToPerson(commonHelpDeskPerson, CommonHelpDesk.class, (String) null);
                        logSummary.info("Assigned {} role COMMON_HELP_DESK", commonHelpDeskPerson.toString());
                    }
                }
                List<Person> roleManagers = entities.get(DATA_JSON_SECTION_COMMUNITY_ROLE_MANAGER);
                if (!CollectionUtils.isEmpty(roleManagers)) {
                    for(Person roleManager : roleManagers){
                        roleManager = createPerson(tenant, roleManager, oauthAccounts, logSummary);
                        roleService.assignRoleToPerson(roleManager, RoleManager.class, tenant);
                        logSummary.info("Assigned {} role ROLE_MANAGER for {}", roleManager.toString(),
                                tenant.toString());
                    }
                }
                createAdditionalPersons(tenant, entities, oauthAccounts, logSummary);
            }
        }
    }

    protected void createAdditionalPersons(Tenant tenant, Map<String, List<Person>> entities,
            List<OauthAccount> oauthAccounts, LogSummary logSummary) {

    }

    protected Person createPerson(Tenant tenant, Person dataJsonPerson, List<OauthAccount> oauthAccounts,
            LogSummary logSummary) {

        final String email = dataJsonPerson.getEmail();
        final Optional<OauthAccount> oauthAccount = oauthAccounts.stream()
                .filter(account -> email.equals(account.getEmail()))
                .findFirst();
        if (oauthAccount.isPresent()) {
            dataJsonPerson.setOauthId(createOauthAccount(oauthAccount.get(), logSummary));
        } else {
            dataJsonPerson.setOauthId(randomOauthId());
        }

        dataJsonPerson = checkId(dataJsonPerson);
        dataJsonPerson = adjustCreated(dataJsonPerson);
        dataJsonPerson = createImage(Person::getProfilePicture, Person::setProfilePicture, dataJsonPerson,
                FileOwnership.of(appService.getPlatformAppVariant()), logSummary, true);
        dataJsonPerson.setTenant(tenant);
        GeoArea homeArea;
        if (dataJsonPerson.getHomeArea() != null) {
            homeArea = geoAreaService.findGeoAreaById(dataJsonPerson.getHomeArea().getId());
        } else {
            homeArea = geoAreaService.getLegacyRootGeoAreaOfTenant(tenant);
        }
        dataJsonPerson.setHomeArea(homeArea);
        dataJsonPerson = createAddresses(Person::getAddresses, Person::setAddresses, dataJsonPerson, logSummary);

        dataJsonPerson = personService.store(dataJsonPerson);
        logSummary.info("Created person {} ('{}', '{}') in {}",
                dataJsonPerson.getFullName(), dataJsonPerson.getId(), dataJsonPerson.getOauthId(), tenant.toString());

        eventBusAccessor.notifyTrigger(new PersonCreatedInDataInitEvent(dataJsonPerson)); //TODO synchronous call? pass logSummary?

        return dataJsonPerson;
    }

    private String createOauthAccount(final OauthAccount oauthAccount, LogSummary logSummary) {

        final String email = oauthAccount.getEmail();

        try {
            String oauthId = oauthManagementService.getOauthIdForCustomUser(email);
            if (oauthId != null) {
                logSummary.info("Using existing OAuth account:  {}: {}", email, oauthId);
            } else {
                oauthId = oauthManagementService.createUser(oauthAccount.getEmail(),
                        passwordFor(oauthAccount.getPasswordType()), true);
                logSummary.info("Created OAuth account:         {}: {}", email, oauthId);
            }
            return oauthId;
        } catch (OauthManagementException ex) {
            logSummary.error("Person " + email + ": " + ex.getMessage(), ex);
            return randomOauthId();
        }
    }

    protected String randomOauthId() {
        return "random|" + UUID.randomUUID();
    }

    protected String passwordFor(OauthAccount.PasswordType passwordType) {
        switch (passwordType) {
            case DEMO_PERSON: return appConfig.getTestPersons().getDemoPersonDefaultPassword();
            case DEMO_PERSON_EXTENDED: return appConfig.getTestPersons().getDemoPersonExtendedDefaultPassword();
            case SHOP: return appConfig.getTestPersons().getShopDefaultPassword();
            case RANDOM:
            default: return randomService.randomPassword();
        }
    }

}
