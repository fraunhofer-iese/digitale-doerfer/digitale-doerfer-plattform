/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.shop.init;

import java.util.Collection;
import java.util.Collections;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;

@Component
public class ShopDemoDataInitializer extends ShopBaseDataInitializer implements IManualTestDataInitializer {

    //ids according to json, referenced by other data initializers in code
    public static final String DEMO_ARLESHOF_UUID                = "5a04304b-1059-446e-9651-56963890ed3b";
    public static final String DEMO_BÄCKEREI_WS_DREISEN_UUID     = "088eb7cf-5081-47ec-96fe-4d070c8b6e0f";
    public static final String DEMO_HIT_EISENBERG_UUID           = "f8c19bb5-dda4-4e30-8ace-19e0aaa14830";
    public static final String DEMO_BÜCHEREI_EISENBERG_UUID      = "6e71bef7-366c-4ca7-bf34-9d131be0f686";
    public static final String DEMO_RAMOSA_MARKT_UUID            = "1fc8c6c9-272f-4df3-b32f-e43dd8ab11f7";

    @Override
    public String getTopic() {
        return "shop-demo";
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return Collections.emptyList();
    }

}
