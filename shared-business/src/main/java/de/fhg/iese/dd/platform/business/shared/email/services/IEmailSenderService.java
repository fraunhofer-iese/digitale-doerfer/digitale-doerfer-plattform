/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.mail.MessagingException;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;
import de.fhg.iese.dd.platform.business.shared.template.services.ITemplateService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import freemarker.template.TemplateException;

public interface IEmailSenderService extends IService {

    /**
     * Shortcut to {@link ITemplateService#createTextWithTemplateMapModel(TemplateLocation,
     * Map)}
     */
    String createTextWithTemplate(TemplateLocation templateLocation, Map<String, Object> templateModel)
            throws IOException, TemplateException;

    void sendEmail(String fromEmailAddress, String recipientName, String recipientEmailAddress, String subject,
            String text,
            List<File> attachments, boolean isHtml) throws MessagingException;

    void sendHtmlEmailToPersons(String fromEmailAddress, Collection<Person> personsToNotify,
            TemplateLocation templateLocation, Function<Person, Map<String, Object>> templateModelProducer,
            String subject);

    void deleteOldSavedEmails(BiFunction<String, Duration, Boolean> onDelete);

}
