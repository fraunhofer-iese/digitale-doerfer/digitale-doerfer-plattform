/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import lombok.AccessLevel;
import lombok.Getter;

import java.util.concurrent.TimeUnit;

/**
 * Context in which an event is processed. This is only required to pass to {@link
 * EventBusAccessor#notifyWithoutHandlingExceptions(BaseEvent, EventProcessingContext)}. The internal data structures
 * can not be accessed.
 */
@Getter(AccessLevel.PACKAGE)
public class EventProcessingContext {

    private final long start;
    private final boolean waitingForReply;
    private final EventProcessingContext previousContext;
    private final EventBusAccessor.EventChainIdentifier eventChainIdentifier;
    private final BaseEvent consumedEvent;
    private final EventBusAccessor eventConsumer;

    /**
     * Create a starting context
     */
    EventProcessingContext(EventBusAccessor eventConsumer, boolean waitingForReply) {
        this.start = System.nanoTime();
        this.waitingForReply = waitingForReply;
        this.previousContext = null;
        this.eventChainIdentifier = new EventBusAccessor.EventChainIdentifier();
        this.consumedEvent = null;
        this.eventConsumer = eventConsumer;
    }

    /**
     * Create an intermediate context
     */
    EventProcessingContext(EventBusAccessor eventConsumer, EventProcessingContext previousContext,
            BaseEvent consumedEvent) {
        this.previousContext = previousContext;
        this.start = previousContext.start;
        this.waitingForReply = previousContext.waitingForReply;
        this.eventChainIdentifier = previousContext.eventChainIdentifier;
        this.consumedEvent = consumedEvent;
        this.eventConsumer = eventConsumer;
    }

    public long getOverallDuration() {
        return TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
    }

}
