/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.Set;

import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Target for a collection of entities at data init. Within the data init files {@link DataInitKeyRaw} is used for
 * defining this target. Since several data init files can be combined (due to multiple modules or defining the same
 * target in different files) there is a list of file names that are the source of this target.
 */
@Getter
@Setter
@Builder
@EqualsAndHashCode(exclude = "fileNames")
public class DataInitKey {

    @Nullable
    private App app;
    @Nullable
    private AppVariant appVariant;
    @Nullable
    private Set<GeoArea> geoAreasIncluded;
    @Nullable
    private Set<GeoArea> geoAreasExcluded;
    private boolean geoAreaChildrenIncluded;
    @Nullable
    private Tenant tenant;
    @Nullable
    private String fileNames;

    void addFileName(String fileName) {
        if (fileNames == null) {
            fileNames = fileName;
        } else {
            fileNames = fileNames + "; " + fileName;
        }
    }

    @Override
    public String toString() {
        return "DataInitKey[" +
                (app != null ? "app=" + app.getAppIdentifier() + ", " : "") +
                (appVariant != null ? "appVariant=" + appVariant.getAppVariantIdentifier() + ", " : "") +
                (!CollectionUtils.isEmpty(geoAreasIncluded) ? "geoAreasIncluded=" + geoAreasIncluded + ", " : "") +
                (!CollectionUtils.isEmpty(geoAreasExcluded) ? "geoAreasExcluded=" + geoAreasExcluded + ", " : "") +
                "geoAreaChildrenIncluded=" + geoAreaChildrenIncluded + ", " +
                (tenant != null ? "tenant=" + tenant + ", " : "") +
                "fileNames='" + fileNames + "']";
    }

}
