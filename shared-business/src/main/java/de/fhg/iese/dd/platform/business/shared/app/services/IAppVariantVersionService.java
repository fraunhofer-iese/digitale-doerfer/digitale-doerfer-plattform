/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import lombok.*;

import java.util.Collection;
import java.util.List;

public interface IAppVariantVersionService extends IEntityService<AppVariantVersion>, ICachingComponent {

    @Getter
    @Builder
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    class CheckVersionResult {

        private final boolean supported;
        private final String text;
        private final String url;
        @Setter
        private String logMessage;

    }

    CheckVersionResult checkAppVariantVersionSupport(String appVariantIdentifier, String appVariantVersionIdentifier,
            AppPlatform platform, String osversion, String device);

    Collection<AppVariantVersion> findAllAppVariantVersions();

    Collection<AppVariantVersion> findAllAppVariantVersions(AppVariant appVariant);

    List<String> getHumanReadableAppVariantVersions();

}
