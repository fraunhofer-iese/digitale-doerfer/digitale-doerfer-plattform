/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.*;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.IUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSummary;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import java.util.HashSet;
import java.util.Set;

public interface IUserGeneratedContentFlagService extends IEntityService<UserGeneratedContentFlag> {

    @Getter
    @Setter
    @SuperBuilder
    @NoArgsConstructor
    class DeletedFlagReport extends BaseEntity {

        private String appName;
        private String reportedText;
        private String fromEmailAddressUserNotification;

    }

    UserGeneratedContentFlag findById(String id) throws UserGeneratedContentFlagNotFoundException;

    /**
     * Finds a flag by id and includes the status records
     */
    UserGeneratedContentFlag findByIdWithStatusRecords(String id) throws UserGeneratedContentFlagNotFoundException;

    /**
     * Checks if the type of the flagged entity is assignable to a field of expectedType.
     * <p>
     * expectedType is same class or superclass of flag.entity
     * <p>
     * or
     * <p>
     * the type of the the flagged entity is invalid (could not be loaded)
     *
     * @param flag         the flag to check
     * @param expectedType the type to expect as flagged entity
     *
     * @throws UserGeneratedContentFlagHasDifferentTypeException if the condition is not met
     */
    void checkEntityTypeOrSubtype(UserGeneratedContentFlag flag, Class<? extends BaseEntity> expectedType) throws
            UserGeneratedContentFlagHasDifferentTypeException;

    /**
     * Returns the detail path to the flag content detail page in the admin ui
     *
     * @param flag the flag to get the detail page path
     */
    String getAdminUiDetailPagePath(UserGeneratedContentFlag flag);

    /**
     * Creates a new flag and checks if a flag for the same entityToFlag and person exists.
     *
     * @param entityToFlag         entity to be flagged
     * @param flaggedEntityAuthor  author of the entity to be flagged
     * @param tenantOfEntityToFlag tenant of the entity to be flagged
     * @param flagCreator          creator of the flag
     * @param comment              comment to add to the first status
     * @return the saved flag
     * @throws UserGeneratedContentFlagAlreadyExistsException if a flag for the same entityToFlag and person exists
     */
    UserGeneratedContentFlag create(BaseEntity entityToFlag, Person flaggedEntityAuthor, Tenant tenantOfEntityToFlag,
            Person flagCreator, String comment) throws UserGeneratedContentFlagAlreadyExistsException;

    /**
     * Creates a new flag or updates the status of an existing flag with the same entityToFlag and person.
     *
     * @param entityToFlag         entity to be flagged
     * @param flaggedEntityAuthor  author of the entity to be flagged
     * @param tenantOfEntityToFlag tenant of the entity to be flagged
     * @param flagCreator          creator of the flag
     * @param comment              comment to add to the newly created status
     * @return the created or updated flag
     */
    UserGeneratedContentFlag createOrUpdateStatus(BaseEntity entityToFlag, @Nullable Person flaggedEntityAuthor,
                                                  @Nullable Tenant tenantOfEntityToFlag, @Nullable Person flagCreator, String comment);

    UserGeneratedContentFlag updateStatus(UserGeneratedContentFlag userGeneratedContentFlag, Person initiator,
            UserGeneratedContentFlagStatus newStatus, String comment);

    boolean existsForPersonAndEntity(Person flagCreator, String entityId, String entityType);

    Set<UserGeneratedContentFlag> findByFlagCreator(Person flagCreator);

    Set<String> findFlaggedEntityIdsByFlagCreator(Person flagCreator);

    /**
     * returns all flags. If status is not {@code null}, the list is filtered by the given status, otherwise entries
     * with all different status are returned. The response is paged.
     *
     * @param status
     * @param page
     *
     * @return
     */
    Page<UserGeneratedContentFlag> findAllByStatusIn(Set<UserGeneratedContentFlagStatus> status, Pageable page);

    /**
     * returns all flags for a tenant. If status is not {@code null}, the list is filtered by the given status,
     * otherwise entries with all different status are returned. The response is paged.
     *
     * @param tenantIds must not be {@code null}
     * @param status
     * @param page
     *
     * @return
     */
    Page<UserGeneratedContentFlag> findAllByTenantInAndStatusIn(Set<String> tenantIds,
            Set<UserGeneratedContentFlagStatus> status, Pageable page);

    Page<UserGeneratedContentFlagSummary> getFlagSummaryPerFlaggedEntity(HashSet<String> expectedFlaggedEntityTypes, long start, long end, PageRequest pageRequest);

    /**
     * Deletes a flagged entity of a flag. Calls the according {@link IUserGeneratedContentFlagHandler} that can handle
     * the type of entity of the flag. If there is none an exception is thrown.
     * <p>
     * The according handlers can fire events to notify about the deletion of the entity.
     *
     * @param flag the flag with the entity that should be deleted.
     *
     * @return report about the deletion of the flagged entity
     *
     * @throws UserGeneratedContentFlagInvalidException               if the flag references invalid entity types or
     *                                                                multiple handlers are available
     * @throws UserGeneratedContentFlagEntityCanNotBeDeletedException if there is no handler available that can delete
     *                                                                the flagged entity
     * @throws UserGeneratedContentFlagEntityDeletionFailedException  if the handler for that flagged entity threw an
     *                                                                exception or the class of the entity could not be
     *                                                                loaded
     */
    DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) throws
            UserGeneratedContentFlagInvalidException,
            UserGeneratedContentFlagEntityCanNotBeDeletedException,
            UserGeneratedContentFlagEntityDeletionFailedException;

}
