/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;

public interface IAdminTaskService extends IService {

    /**
     * Get a human readable description of the ids {@link BaseAdminTask#getId()} and the descriptions {@link
     * BaseAdminTask#getDescription()} of all admin tasks
     *
     * @return
     */
    String getAllAdminTaskIdsAndDescriptions();

    /**
     * Execute the admin task with the given name and return a human readable summary of what has been done. If the task
     * does not exist no exception is thrown, but an error message can be found in the summary.
     *
     * @param adminTaskName
     * @param adminTaskParameters
     *
     * @return human readable summary of the tasks execution.
     */
    String executeAdminTask(String adminTaskName, BaseAdminTask.AdminTaskParameters adminTaskParameters);

}
