/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public class GeoAreaIsNoLeafException extends BadRequestException {

    private static final long serialVersionUID = 6420904241663117048L;

    private GeoAreaIsNoLeafException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static GeoAreaIsNoLeafException forHomeAreaIsInvalid(GeoArea homeArea) {
        return new GeoAreaIsNoLeafException(
                "The geo area '{}' is not a valid home area because it has children", BaseEntity.getIdOf(homeArea));
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.GEO_AREA_IS_NO_LEAF;
    }

}
