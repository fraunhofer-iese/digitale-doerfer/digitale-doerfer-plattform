/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.template;

import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;

public class ParticipantsTemplate extends TemplateLocation {

    public static final ParticipantsTemplate EMAIL_VERIFICATION_EMAIL =
            new ParticipantsTemplate("emailVerificationEmail.ftl");
    public static final String EMAIL_VERIFICATION_EMAIL_SUBJECT = "Willkommen bei den Digitalen Dörfern %s";

    protected ParticipantsTemplate(String name) {
        super(name);
    }

}
