/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;

public abstract class BaseLegalTextAppDataInitializer extends BaseLegalTextDataInitializer {

    @Autowired
    protected AppVariantRepository appVariantRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("legal")
                .useCase(USE_CASE_NEW_APP_VARIANT)
                .requiredEntity(App.class)
                .requiredEntity(AppVariant.class)
                .processedEntity(LegalText.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {

        Collection<AppVariant> allAppVariants = appVariantRepository.findAllByOrderByAppVariantIdentifier();

        List<LegalText> legalTexts = loadEntitiesFromDataJsonDefaultSection(null, LegalText.class);

        Map<String, List<AppVariantLegalTextMapping>> appVariantLegalTextMappingsByAppVariantIdentifier =
                loadEntitiesFromDataJson(null, AppVariantLegalTextMapping.class);

        if (appVariantLegalTextMappingsByAppVariantIdentifier != null) {
            for (AppVariant appVariant : allAppVariants) {
                // the AppVariantIdentifier is used as key in the data json for the app variant
                List<AppVariantLegalTextMapping> appVariantLegalTextMappings =
                        appVariantLegalTextMappingsByAppVariantIdentifier.get(appVariant.getAppVariantIdentifier());
                if (appVariantLegalTextMappings != null) {
                    List<LegalText> matchingLegalTexts = new ArrayList<>();
                    // these mappings are only used as container for the appVariantIdentifier
                    for (AppVariantLegalTextMapping appVariantLegalTextMapping : appVariantLegalTextMappings) {
                        final Optional<LegalText> legalTextOpt = legalTexts.stream()
                                .filter(lt -> appVariantLegalTextMapping.getLegalTextIdentifier().equals(
                                        lt.getLegalTextIdentifier()))
                                .findFirst();
                        if (legalTextOpt.isEmpty()) {
                            throw new DataInitializationException(
                                    "Legal text '{}' could not be found! Check the AppVariantLegalTextMapping.json!",
                                    appVariantLegalTextMapping.getLegalTextIdentifier());
                        } else {
                            LegalText legalText = createShallowCopy(legalTextOpt.get());
                            // if dateActive or dateInactive are specified in mapping, the initial value is overwritten
                            if (appVariantLegalTextMapping.getDateActive() != null) {
                                legalText.setDateActive(appVariantLegalTextMapping.getDateActive());
                            }
                            if (appVariantLegalTextMapping.getDateInactive() != null) {
                                legalText.setDateActive(appVariantLegalTextMapping.getDateInactive());
                            }
                            matchingLegalTexts.add(legalText);
                        }
                    }
                    createLegalTexts(logSummary, matchingLegalTexts, appVariant);
                }
            }
        }
    }

    private void createLegalTexts(LogSummary logSummary, List<LegalText> legalTextsForAppVariant,
            AppVariant appVariant) {

        logSummary.indent();
        if (CollectionUtils.isEmpty(legalTextsForAppVariant)) {
            logSummary.warn("No legal texts configured for app variant '{}'! " +
                            "This app variant can not be used in production!",
                    appVariant.getAppVariantIdentifier());
        } else {
            for (LegalText legalTextForAppVariant : legalTextsForAppVariant) {
                createLegalTextAppVariant(logSummary, appVariant, legalTextForAppVariant);
            }
        }
        logSummary.outdent();
    }

    private LegalText createShallowCopy(LegalText legalText) {
        return LegalText.builder()
                .appVariant(legalText.getAppVariant())
                .legalTextIdentifier(legalText.getLegalTextIdentifier())
                .name(legalText.getName())
                .legalTextType(legalText.getLegalTextType())
                .orderValue(legalText.getOrderValue())
                .required(legalText.isRequired())
                .dateActive(legalText.getDateActive())
                .dateInactive(legalText.getDateInactive())
                .urlText(legalText.getUrlText())
                .build();
    }

}
