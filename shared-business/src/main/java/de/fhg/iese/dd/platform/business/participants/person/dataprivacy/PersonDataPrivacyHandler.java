/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.dataprivacy;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.OauthUser;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ISharedDataDeletionService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;

@Component
public class PersonDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(MediaItem.class, DocumentItem.class);
    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Person.class);

    @Autowired
    private IPersonService personService;

    @Autowired
    private IParticipantsDataDeletionService participantsDataDeletionService;

    @Autowired
    private ISharedDataDeletionService sharedDataDeletionService;

    @Autowired
    private IRoleService roleService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {
        
        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Person", "Daten zur Person");

        IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                section.newSubSection("Profil", "Profildaten");
        subSection.newContent("ID", "Eindeutige Identifikation", person.getId());
        subSection.newContent("Name", "Vor- und Nachname", person.getFullName());
        subSection.newContent("Nickname", person.getNickName());
        subSection.newContent("Email", person.getEmail());
        if (StringUtils.isNotEmpty(person.getPendingNewEmail())) {
            subSection.newContent("Unbestätigte neue Email", person.getPendingNewEmail());
        }
        subSection.newContent("Telefon", person.getPhoneNumber());
        if (person.getHomeArea() != null) {
            subSection.newContent("Heimatgemeinde", "Name der Heimatgemeinde, in der der Wohnort liegt",
                    person.getHomeArea().getName());
        } else {
            if (person.getTenant() != null) {
                subSection.newContent("Mandant", "Mandant, dem diese Person zugeordnet ist",
                        person.getTenant().getName());
            }
        }
        if (person.getProfilePicture() != null) {
            subSection.newImageContent("Profilbild", person.getProfilePicture());
        }

        subSection.newContent("Registrierung", "Zeitpunkt der Registrierung",
                timeService.toLocalTimeHumanReadable(person.getCreated()));
        subSection.newContent("Anmeldung", "Zeitpunkt der letzten Anmeldung",
                timeService.toLocalTimeHumanReadable(person.getLastLoggedIn()));
        subSection.newContent("Verifikations-Status", "Verifikationen von Daten der Person",
                String.valueOf(person.getVerificationStatuses().getValues()));

        List<AddressListEntry> addresses = personService.getAddressListEntries(person);
        if (!CollectionUtils.isEmpty(addresses)) {
            subSection = section.newSubSection("Adressen", "Adressen, die zu dieser Person angegeben wurden");
            for (AddressListEntry address : addresses) {
                subSection.newContent("Addresse", address.getName(), address.getAddress().toHumanReadableString(true));
            }
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        //get fresh version of the person
        final Person personToBeErased = personService.findPersonById(person.getId());

        //save in memory for later reference
        final List<AddressListEntry> addresses = personService.getAddressListEntries(person);
        final MediaItem profilePicture = personToBeErased.getProfilePicture();

        //erase person
        personDeletionReport.addEntry(person, participantsDataDeletionService.deletePerson(personToBeErased));

        //the profile picture will be removed by MediaItemDataPrivacyHandler
        if (profilePicture != null) {
            personDeletionReport.addEntry(profilePicture, DeleteOperation.DELETED);
        }

        //delete/erase addresses
        for (AddressListEntry addressListEntry : addresses) {
            final Address address = addressListEntry.getAddress();
            personDeletionReport.addEntry(address, sharedDataDeletionService.deleteAddress(address));

            personDeletionReport.addEntry(addressListEntry,
                    participantsDataDeletionService.deleteAddressListEntry(addressListEntry));
        }

        //delete all assigned roles
        final List<RoleAssignment> roleAssignments = roleService.getCurrentExtendedRoleAssignments(personToBeErased);
        for (RoleAssignment roleAssignment : roleAssignments) {
            personDeletionReport.addEntry(roleAssignment,
                    participantsDataDeletionService.deleteRoleAssignment(roleAssignment));
        }

        //delete OAuth user if not blocked in OAuth management
        if(person.getAccountType() == AccountType.OAUTH && StringUtils.isNotEmpty(person.getOauthId())) {
            try {
                if (person.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED)) {
                    log.info("Not deleting OAuth account of user (id={}, oauthId={}) since " +
                                    "this user is blocked and must not recreate an account with " +
                                    "the same email address.",
                            person.getId(), person.getOauthId());
                } else {
                    personDeletionReport.addEntry(OauthUser.forOauthId(person.getOauthId()),
                            participantsDataDeletionService.deleteOauthAccount(personToBeErased));
                }
            } catch (OauthAccountNotFoundException e) {
                log.info("OAuth account of user {id={}, oauthId={}} not deleted since it was not existing anymore",
                        person.getId(), person.getOauthId());
            }
        }
    }

}
