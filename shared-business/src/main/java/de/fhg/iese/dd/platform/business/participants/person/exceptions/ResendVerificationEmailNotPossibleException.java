/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class ResendVerificationEmailNotPossibleException extends BadRequestException {

    private static final long serialVersionUID = 2495797107037349154L;

    private ResendVerificationEmailNotPossibleException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static ResendVerificationEmailNotPossibleException forWaitForResendNotExpired(
            String nextPossibleResendTime) {
        return new ResendVerificationEmailNotPossibleException("Next time to resend verification mail is: '{}'",
                nextPossibleResendTime);
    }

    public static ResendVerificationEmailNotPossibleException forEmailAlreadyVerified() {
        return new ResendVerificationEmailNotPossibleException("The email is already verified");
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.RESEND_VERIFICATION_EMAIL_NOT_POSSIBLE;
    }

}
