/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;

public class UserGeneratedContentFlagStatusChangeInvalidException extends BadRequestException {

    private static final long serialVersionUID = -1580214184891047345L;

    private UserGeneratedContentFlagStatusChangeInvalidException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static UserGeneratedContentFlagStatusChangeInvalidException forInvalidStatusTransition(
            UserGeneratedContentFlag flag, UserGeneratedContentFlagStatus oldStatus,
            UserGeneratedContentFlagStatus newStatus) {
        return new UserGeneratedContentFlagStatusChangeInvalidException(
                "The status of user generated content flag with id {} can not be changed from '{}' to '{}'",
                flag.getId(), oldStatus, newStatus);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.USER_GENERATED_CONTENT_FLAG_STATUS_CHANGE_INVALID;
    }

}
