/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;

public class PushCategoryNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 7902987626215451106L;

    public PushCategoryNotFoundException(String message, Object... arguments) {
        super(message, arguments);
    }

    public PushCategoryNotFoundException(String categoryId) {
        super("Push category {} cannot be found.", categoryId);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.PUSH_CATEGORY_NOT_FOUND;
    }

}
