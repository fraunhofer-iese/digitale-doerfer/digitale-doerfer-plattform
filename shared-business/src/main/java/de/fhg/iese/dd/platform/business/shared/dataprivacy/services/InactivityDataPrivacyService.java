/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonResetLastLoggedInRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyTemplate;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import freemarker.template.TemplateException;

@Service
class InactivityDataPrivacyService extends BaseDataPrivacyService implements IInactivityDataPrivacyService {

    private static final Pattern EMAIL_MASK = Pattern.compile("(\\S{1,4})(\\S+)@(\\S{1,4})(\\S+)\\.(\\S+)");
    private static final Pattern NAME_MASK = Pattern.compile("(\\S{1,3})\\S{1,2}");

    @Autowired
    private ParticipantsConfig participantsConfig;
    @Autowired
    private IPersonService personService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private IAuthorizationService authorizationService;

    @Override
    public List<Person> sendWarningEmailToInactivePersons() {

        if (!dataPrivacyConfig.getUserInactivityDeletionPreparation().isEnabled()) {
            return Collections.emptyList();
        }

        int warningThresholdDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int warningBatchSize = dataPrivacyConfig.getUserInactivityDeletionPreparation().getBatchSize();

        Page<Person> personsToWarn = personService.findInactivePersonsWithoutStatusWithVerificationStatus(
                p -> isEmailAddressExcludedByInactivity(p.getEmail()),
                PersonStatus.PENDING_DELETION,
                PersonVerificationStatus.EMAIL_VERIFIED, //email needs to be verified to reduce the mail bounces
                warningThresholdDays,
                warningBatchSize);

        List<Person> warnedPersons = new ArrayList<>();
        try {
            for (Person person : personsToWarn) {

                Map<String, Object> emailData = new HashMap<>();
                long notLoggedInDays = TimeUnit.MILLISECONDS.toDays(
                        timeService.currentTimeMillisUTC() - person.getLastLoggedIn());

                emailData.put("receiverName", person.getFirstName());
                emailData.put("notLoggedInDays", notLoggedInDays);
                final String mailToken = authorizationService.generateAuthToken(
                        new PersonResetLastLoggedInRequest(person),
                        //the mail token should not be valid when we deleted the user
                        Duration.ofDays(
                                dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionAndWarnedForDays() -
                                        1));
                emailData.put("resetLastLoggedInLink",
                        UriComponentsBuilder
                                .fromHttpUrl(
                                        dataPrivacyConfig.getUserInactivityDeletionPreparation().getResetLastLoggedInBaseUrl())
                                .queryParam("person", person.getId())
                                .queryParam("token", mailToken)
                                .build()
                                .toUriString());
                emailData.put("contactPerson", roleService.getDefaultContactPersonForTenant(person.getTenant()));

                String fromEmailAddress = participantsConfig.getSenderEmailAddressAccount();
                String emailBody = emailSenderService.createTextWithTemplate(
                        DataPrivacyTemplate.USER_DELETION_WARNING_EMAIL, emailData);

                emailSenderService.sendEmail(fromEmailAddress, person.getFullName(), person.getEmail(),
                        "Noch dabei? Wir vermissen dich!", emailBody, Collections.emptyList(), true);

                person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
                person.setLastSentTimePendingDeletionWarning(timeService.currentTimeMillisUTC());
                personService.store(person);

                log.info("User deletion warning email sent to person with id {}, status changed to PENDING_DELETION",
                        person.getId());
                warnedPersons.add(person);
            }
        } catch (IOException | TemplateException | MessagingException e) {
            log.error("Could not send email to warn about pending deletion, skipping remaining batch", e);
        }
        teamNotificationService.sendTeamNotification(
                TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Warning {} / {} (batch size {}) users with verified email that have been not logged in for {} days.\n```{}```",
                warnedPersons.size(),
                personsToWarn.getTotalElements(),
                warningBatchSize,
                warningThresholdDays,
                toPersonSummary(warnedPersons));
        return warnedPersons;
    }

    @Override
    public List<Person> setPendingDeletionOfUnverifiedEmailInactivePersons() {
        if (!dataPrivacyConfig.getUserInactivityDeletionPreparation().isEnabled()) {
            return Collections.emptyList();
        }

        int warningBatchSize = dataPrivacyConfig.getUserInactivityDeletionPreparation().getBatchSize();
        int warningThresholdDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        Page<Person> personsToMarkPendingDeletion =
                personService.findInactivePersonsWithoutStatusWithoutVerificationStatus(
                        p -> isEmailAddressExcludedByInactivity(p.getEmail()),
                        PersonStatus.PENDING_DELETION,
                        PersonVerificationStatus.EMAIL_VERIFIED, //unverified email does not get warnings
                        warningThresholdDays,
                        warningBatchSize);

        for (Person person : personsToMarkPendingDeletion) {
            person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
            personService.store(person);
        }

        teamNotificationService.sendTeamNotification(
                TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Marked {} / {} (batch size {}) users with unverified email for pending deletion that have been not logged in for {} days.\n```{}```",
                personsToMarkPendingDeletion.getNumberOfElements(),
                personsToMarkPendingDeletion.getTotalElements(),
                warningBatchSize,
                warningThresholdDays,
                toPersonSummary(personsToMarkPendingDeletion.getContent()));
        return personsToMarkPendingDeletion.getContent();
    }

    @Override
    public List<Person> deletePendingDeletionWarnedPersons() {

        if (!dataPrivacyConfig.getUserInactivityDeletion().isEnabled()) {
            return Collections.emptyList();
        }

        int deletionThresholdDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionAndWarnedForDays();
        int deletionBatchSize = dataPrivacyConfig.getUserInactivityDeletion().getBatchSize();

        Page<Person> personsToBeDeleted = personService.findPersonsWarnedForDaysAndNotDeleted(
                p -> isEmailAddressExcludedByInactivity(p.getEmail()),
                PersonStatus.PENDING_DELETION,
                deletionThresholdDays,
                deletionBatchSize);

        teamNotificationService.sendTeamNotification(
                TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Deleting {} / {} (batch size {}) users that have been warned and not logged in for {} days.\n```{}```",
                personsToBeDeleted.getNumberOfElements(),
                personsToBeDeleted.getTotalElements(),
                deletionBatchSize,
                deletionThresholdDays,
                toPersonSummary(personsToBeDeleted.getContent()));

        for (Person person : personsToBeDeleted) {
            log.info("Triggering delete request due to inactivity of person with id {}", person.getId());
            notify(PersonDeleteRequest.builder()
                    .person(person)
                    .trigger(DataPrivacyWorkflowTrigger.USER_INACTIVITY)
                    .build());
        }
        return personsToBeDeleted.getContent();
    }

    @Override
    public List<Person> deletePendingDeletionUnverifiedEmailPersons() {

        if (!dataPrivacyConfig.getUserInactivityDeletion().isEnabled()) {
            return Collections.emptyList();
        }

        int deletionThresholdDays =
                //since we do not store the actual timestamp when the pending deletion was set we calculate the latest last login time
                dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays() +
                        dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionUnverifiedEmailForDays();
        int deletionBatchSize = dataPrivacyConfig.getUserInactivityDeletion().getBatchSize();

        Page<Person> personsToBeDeleted = personService.findInactivePersonsWithStatusWithoutVerificationStatus(
                p -> isEmailAddressExcludedByInactivity(p.getEmail()),
                PersonStatus.PENDING_DELETION,
                PersonVerificationStatus.EMAIL_VERIFIED, //unverified email did not get the warning
                deletionThresholdDays,
                deletionBatchSize);

        teamNotificationService.sendTeamNotification(
                TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Deleting {} / {} (batch size {}) users with unverified email and in status pending deletion that have been not logged in for {} days.\n```{}```",
                personsToBeDeleted.getNumberOfElements(),
                personsToBeDeleted.getTotalElements(),
                deletionBatchSize,
                deletionThresholdDays,
                toPersonSummary(personsToBeDeleted.getContent()));

        for (Person person : personsToBeDeleted) {
            log.info("Triggering delete request due to inactivity of person with id {}", person.getId());
            notify(PersonDeleteRequest.builder()
                    .person(person)
                    .trigger(DataPrivacyWorkflowTrigger.USER_INACTIVITY)
                    .build());
        }
        return personsToBeDeleted.getContent();
    }

    private String toPersonSummary(Collection<Person> personsToBeDeleted) {
        return personsToBeDeleted.stream()
                .map(person -> String.format("%-40s %30s %36s created %s last login %19s %s",
                        maskName(person.getFullName()),
                        maskEmail(person.getEmail()),
                        person.getId(),
                        timeService.toLocalTimeHumanReadable(person.getCreated()),
                        timeService.toLocalTimeHumanReadable(person.getLastLoggedIn()),
                        person.getLastSentTimePendingDeletionWarning() != null
                                ? "warned " + timeService.toLocalTimeHumanReadable(
                                person.getLastSentTimePendingDeletionWarning()) :
                                ""))
                .collect(Collectors.joining("\n"));
    }

    private String maskEmail(String email) {
        if (StringUtils.isNotEmpty(email)) {
            //emails look like balt***@iese***.de
            return EMAIL_MASK.matcher(email).replaceAll("$1***@$3***.$5");
        } else {
            return "-";
        }
    }

    private String maskName(String name) {
        if (StringUtils.isNotEmpty(name)) {
            //name look like Bal*asa* Wei*e*
            return NAME_MASK.matcher(name).replaceAll("$1*");
        } else {
            return "-";
        }
    }

    //package visible for testing
    boolean isEmailAddressExcludedByInactivity(String emailAddress) {

        List<Pattern> excludedPatterns = dataPrivacyConfig.getUserInactivityExcludes();
        return excludedPatterns.stream()
                .anyMatch(pattern -> pattern.matcher(emailAddress).matches());
    }

    void setConfig(DataPrivacyConfig dataPrivacyConfig) {
        this.dataPrivacyConfig = dataPrivacyConfig;
    }

}
