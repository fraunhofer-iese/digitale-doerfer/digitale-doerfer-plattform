/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.Collection;

import de.fhg.iese.dd.platform.business.shared.init.services.IDataInitializerService;

/**
 * A special data initializers that creates either manual test data or demo data. These data initializers are not called
 * if {@link IDataInitializerService#SELECT_ALL} is used.
 */
public interface IManualTestDataInitializer extends IDataInitializer {

    /**
     * Identifier of the test scenario that is this manual test data initializers supports. Can be null if multiple
     * scenarios are supported. In this case the data initializer is referenced in {@link #getDependentTopics()} by
     * others.
     *
     * @return the id of the supported scenario or null
     */
    String getScenarioId();

    /**
     * Topics of data initializers that need to be executed before.
     *
     * @return all topics of data initializer this one depends on
     */
    Collection<String> getDependentTopics();

}
