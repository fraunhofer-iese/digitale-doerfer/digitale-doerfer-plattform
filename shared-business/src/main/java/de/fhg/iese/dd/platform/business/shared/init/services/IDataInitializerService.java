/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init.services;

import java.util.List;
import java.util.Set;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer.DataInitializerResult;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IDataInitializerService extends IService {

    String SELECT_ALL = "*";

    /**
     * Get all human readable identifiers of the data initializers by calling
     * {@link IDataInitializer#getId()}.
     *
     */
    List<String> getAllDataInitializerIds();

    List<String> getAllDataInitializerTopics();

    List<String> getAllDataInitializerUseCases();

    /**
     * Get all human readable manual test scenario ids of the data manual test data initializers by calling
     * {@link IManualTestDataInitializer#getScenarioId()}.
     *
     */
    List<String> getAllManualTestScenarioIds();

    DataInitializerResult createInitialDataForUseCase(@NonNull String useCase, @Nullable Person caller);

    /**
     * Create / update the initial data set for the given topics. SELECT_ALL ("*") only selects data initializers that
     * are not {@link IManualTestDataInitializer}
     * <p/>
     * The data initializers need to be constructed in a way that this method can be called multiple times, also on
     * production data.
     *
     * @param commandPrefix
     * @param topics        the topics to use, matching the data initializers
     * @param caller        the person that triggered this data init
     *
     * @return a human readable summary
     */
    DataInitializerResult createInitialData(String commandPrefix, @NonNull Set<String> topics, @Nullable Person caller);

    /**
     * Drop all data for all topics, not only initial data, but all of it. SELECT_ALL ("*") only selects data
     * initializers that are not {@link IManualTestDataInitializer}
     *
     * @param topics the topics to use, matching the data initializers
     * @param caller the person that triggered this data init
     *
     * @return a human readable summary
     */
    DataInitializerResult dropData(@NonNull Set<String> topics, @Nullable Person caller);

    /**
     * Starts a specific manual test scenario by calling all manual test data initializers with that scenario id and all
     * data initializers that are marked as required by them.
     *
     * @param scenarioId
     *         the id of the scenario to execute
     * @param deleteOtherDataInDomain
     *         if true {@link IDataInitializer#dropData(LogSummary)} is called on these data initializers to ensure that
     *         only the data of the manual test scenario is present
     *
     * @return a human readable summary
     */
    DataInitializerResult startManualTestScenario(@NonNull String scenarioId, boolean deleteOtherDataInDomain);

}
