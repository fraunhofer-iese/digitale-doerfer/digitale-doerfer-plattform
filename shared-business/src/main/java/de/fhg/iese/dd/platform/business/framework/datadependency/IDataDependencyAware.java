/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.datadependency;

import java.util.Collection;

import de.fhg.iese.dd.platform.business.framework.datadependency.services.IDataDependencyAwareOrderService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

/**
 * Interface for all components that process data entities and require a certain processing order. This processing order
 * is defined by the relationship between required and processed entity types. A graph is built with the data entity
 * types and the data aware as nodes, the required and processed entity sets as relationships.
 */
public interface IDataDependencyAware {

    /**
     * A unique identifier used in logs and for sorting multiple dependency aware if they are in the same place of the
     * dependency tree.
     *
     * @return unique identifier
     */
    String getId();

    /**
     * All types of entities that are required by this dependency aware to run.
     * <p/>
     * <strong>Only</strong> add the entities
     * that are directly required. The {@link IDataDependencyAwareOrderService} will take care to ensure that the
     * prerequisites for these required entities are met.
     * <p/>
     * Important implementation note: <br/> Return the value of a {@code static final} constant here, because the method
     * is called very frequently.
     *
     * @return classes of entities that are required for this dependency aware to run
     *
     * @see IDataDependencyAwareOrderService
     */
    Collection<Class<? extends BaseEntity>> getRequiredEntities();

    /**
     * All types of entities that are processed by this dependency aware. After they are processed they can be in the
     * {@link #getRequiredEntities()} of another dependency aware.
     * <p/>
     * <strong>Only</strong> add the "major" entities here and no contained
     * ones, like {@link de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem}s.
     * <p/>
     * Important implementation note: <br/> Return the value of a {@code static final} constant here, because the method
     * is called very frequently.
     *
     * @return classes of entities that are processed by this dependency aware
     *
     * @see IDataDependencyAwareOrderService
     */
    Collection<Class<? extends BaseEntity>> getProcessedEntities();

}
