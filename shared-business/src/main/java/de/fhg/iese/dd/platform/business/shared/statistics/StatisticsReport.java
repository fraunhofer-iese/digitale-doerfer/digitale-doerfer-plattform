/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsReport {

    private String title;
    private StatisticsReportDefinition definition;
    private List<StatisticsMetaData> metaData;
    private Collection<GeoAreaStatistics> geoAreaStatistics;
    private long creationTime;

    public List<Pair<StatisticsMetaData, StatisticsTimeRelation>> getStatisticsMetadataAndTimeRelation() {
        return metaData.stream()
                .flatMap(m -> m.getTimeRelations().stream()
                        .map(t -> Pair.of(m, t)))
                .collect(Collectors.toList());
    }

}
