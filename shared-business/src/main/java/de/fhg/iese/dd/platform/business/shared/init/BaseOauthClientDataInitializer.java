/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;

public abstract class BaseOauthClientDataInitializer extends BaseDataInitializer {

    public static final String TOPIC = "oauth";

    @Autowired
    private OauthClientRepository oauthClientRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                .processedEntity(OauthClient.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        createOauthClients(logSummary);
    }

    private void createOauthClients(LogSummary logSummary) {

        List<OauthClient> createdOauthClients = new ArrayList<>();

        DuplicateIdChecker<OauthClient> duplicateIdChecker =
                new DuplicateIdChecker<>(OauthClient::getOauthClientIdentifier);

        List<OauthClient> oauthClients = loadEntitiesFromDataJsonDefaultSection(null, OauthClient.class);
        for (OauthClient oauthClient : oauthClients) {
            oauthClient = oauthClient.withConstantId();
            duplicateIdChecker.checkId(oauthClient);
            oauthClient = adjustCreated(oauthClient);
            createdOauthClients.add(oauthClient);
            logSummary.info("created {}", oauthClient.toString());
        }
        oauthClientRepository.saveAll(createdOauthClients);
        oauthClientRepository.flush();
    }

}
