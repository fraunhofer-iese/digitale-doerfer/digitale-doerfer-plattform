/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.geoarea.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.participants.geoarea.exceptions.GeoAreaNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.locationtech.jts.geom.Geometry;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

public interface IGeoAreaService extends IEntityService<GeoArea>, ICachingComponent {

    enum GeoAreaMatch {
        NONE,
        INSIDE,
        CLOSEST
    }

    Set<GeoArea> findAllRootAreas();

    List<GeoArea> findAll();

    GeoArea findGeoAreaById(String geoAreaId) throws GeoAreaNotFoundException;

    Set<GeoArea> findAllById(Collection<String> ids) throws GeoAreaNotFoundException;

    /**
     * Searches for geo areas having the search string as infix (=continuous substring) in id or name. Search is done in
     * a case-insensitive way.
     */
    Collection<GeoArea> findAllByIdOrNameInfixSearch(String infixSearchString);

    /**
     * Get all geo areas where its tenant is one of the tenants. Parent areas of the geo areas are not included in the
     * returned set.
     */
    Set<GeoArea> getGeoAreasForTenants(Collection<Tenant> tenants);

    /**
     * Get all root geo areas where its tenant is one of the tenants. The returned geo areas are the most top level ones
     * that have these tenants.
     */
    Set<GeoArea> getRootGeoAreasForTenants(Collection<Tenant> tenants);

    /**
     * Get the root areas of the given geo areas by selecting the top most geo areas. Replaces the geo areas with cached
     * if necessary.
     */
    Set<GeoArea> getRootGeoAreas(Collection<GeoArea> geoAreas);

    GeoArea getLegacyRootGeoAreaOfTenant(Tenant tenant);

    boolean isLeaf(GeoArea geoArea);

    Set<GeoArea> getParentGeoAreas(Set<GeoArea> geoAreas);

    /**
     * Get a new set of geo areas that also contains all parent geo areas if they match the predicate
     *
     * @param geoAreas        the geo areas to add the parents
     * @param parentPredicate only parents with parentPredicate(parent) = true are added
     *
     * @return A set of geo areas additionally including all parents, if they match the parentPredicate
     */
    Set<GeoArea> addParentGeoAreas(Set<GeoArea> geoAreas, Predicate<GeoArea> parentPredicate);

    /**
     * Get a new set of geo areas that also contains all parent geo areas if they match the predicate
     *
     * @param geoAreas        the geo areas to add the parents
     * @param parentPredicate only parents with parentPredicate(parent) = true are added
     * @param upToDepth       the depth at which the tree should be cut, with 0 being the overall root areas. The given
     *                        geo areas can also be partially removed, if they are below this depth.
     *
     * @return A set of geo areas additionally including all parents, if they match the parentPredicate and are below
     *         the given depth
     */
    Set<GeoArea> addParentGeoAreas(Set<GeoArea> geoAreas, Predicate<GeoArea> parentPredicate, int upToDepth);

    /**
     * Returns a copy of the set which only include leaf geo areas, relative to the given geo areas.
     * <p>
     * A leaf is a geo area that is not the parent of any other geo area.
     * <p>
     * IMPORTANT! This is only done relative to the given geo areas! If other geo areas reference them but are not
     * included in the set they are not considered!
     */
    Set<GeoArea> removeNonLeafGeoAreas(Set<GeoArea> geoAreas);

    /**
     * Finds nearest geo area within the given availableGeoAreas. The algorithm filters out all geo areas whose center
     * point is further away than {@link ApplicationConfig#getGeoAreaLookupRadiusInKm()}. For the remaining areas, the
     * location is tried to fit inside the boundary of every area. If a matching area is found, it is returned.
     * Otherwise, the area whose center is closest to the location is returned. If no area is found in the configured
     * radius, {@code null} is returned.
     *
     * @param availableGeoAreas the geo areas to test, must not be {@code null}
     * @param currentLocation   must not be {@code null}
     *
     * @return the nearest geo area or {@code null} if none is found within the radius
     */
    Pair<GeoArea, GeoAreaMatch> findNearestGeoArea(Collection<GeoArea> availableGeoAreas, GPSLocation currentLocation);

    /**
     * Find the minimal set of leaf geo areas that are intersected by the given geometry.
     * Minimal means: If all child areas of an area are intersected, this parent area is returned instead all the child areas.
     *
     * @param geometry the geometry to test
     * @param availableGeoAreas the geo areas to test, must not be {@code null}
     * @return minimum set of geo areas that are intersected by the geometry
     */
    @NotNull
    Set<GeoArea> findIntersectedLeafGeoAreas(Geometry geometry, Collection<GeoArea> availableGeoAreas);

    /**
     * Finds the nearest geo areas within the given availableGeoAreas. The algorithm filters out all geo areas whose
     * center point is further away than given radius and the remaining areas are returned. If no geo area is found, an
     * empty set is returned. If {@code onlyLeaves} is true, non leaf geo areas are filtered out.
     *
     * @param availableGeoAreas must not be {@code null}
     * @param currentLocation   must not be {@code null}
     * @param radiusKM          radius in km
     * @param onlyLeaves        filter to return only leaf geo areas
     *
     * @return the nearest geo areas or empty list if none is found within the radius
     */
    Set<GeoArea> findNearestGeoAreas(Collection<GeoArea> availableGeoAreas, GPSLocation currentLocation,
            double radiusKM, boolean onlyLeaves);

    /**
     * Minimize the given geo areas by replacing the child areas with the parent area if all children are included.
     * The parent and the children have to be contained in the availableGeoAreas.
     * In other words: Only the part of the geo area tree that is within the availableGeoAreas is considered.
     *
     * @param geoAreasToMinimize the geo areas to minimize
     * @param availableGeoAreas  all resulting geo areas have to be within these geo areas and only siblings that are in these geo areas are considered
     * @return minimized geo areas
     */
    @NotNull
    Set<GeoArea> minimizeGeoAreas(Collection<GeoArea> geoAreasToMinimize, Collection<GeoArea> availableGeoAreas);

    /**
     * Searches in all geo areas by name and/or zip code. The search tries to deal with typos and normalizes the search
     * (check the implementation for details).
     * <p>
     * Filter to only include areas >= upToDepth, 0 = full tree. If `includeAllChildrenOfMatchingAreas` is true all
     * children of a matching area are added, if they are in allGeoAreas.
     *
     * @param allGeoAreas                       the geo areas to search in
     * @param upToDepth                         filter to only include areas >= upToDepth, 0 = full tree
     * @param includeAllChildrenOfMatchingAreas if true all children of a matching area are added, if they are in
     *                                          allGeoAreas
     * @param search                            the search term to use
     *
     * @return the result of the search, might be empty if none match
     */
    Set<GeoArea> searchInGeoAreas(Collection<GeoArea> allGeoAreas, int upToDepth,
            boolean includeAllChildrenOfMatchingAreas, String search);

    Collection<GeoArea> getDirectChildAreas(GeoArea geoArea);

    Collection<GeoArea> getAllChildAreasIncludingSelf(GeoArea geoArea);

    Collection<GeoArea> getAllChildAreasExcludingSelf(GeoArea geoArea);

    /**
     * Get a new set of geo areas that also contains all child geo areas if they match the predicate.
     *
     * @param rootAreas      the geo areas to add the children
     * @param childPredicate only children with childPredicate(child) = true are added
     * @param depth          the depth at which the tree should be cut, with 0 being the current root areas.
     * @param revealVisible  includes all visible geo areas, relative to the given root areas. Visible = sibling or
     *                       sibling of all parents up to the root areas.
     *
     * @return A set of geo areas additionally including all children, if they match the childPredicate and are above
     *         the given depth. If revealVisible is true also the siblings and the siblings of the parents are
     *         included.
     */
    Set<GeoArea> addChildAreas(Collection<GeoArea> rootAreas, Predicate<GeoArea> childPredicate,
            int depth, boolean revealVisible);

    GeoArea ensureCached(GeoArea geoArea);

    GeoArea forceCached(GeoArea geoArea);

    Set<GeoArea> forceCached(Set<GeoArea> geoAreas);

    /**
     * Checks if the cache for geo areas is stale and returns the point in time when the cache was refreshed for the
     * last time.
     * <p>
     * Required to connect caches that depend on this one.
     */
    Long checkCacheGeoAreas();

}
