/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;

@Component
public class CleanupGeoAreaSelectionsParentsAdminTask extends BaseAdminTask {

    @Autowired
    private IAppService appService;
    @Autowired
    private IGeoAreaService geoAreaService;

    @Override
    public String getName() {
        return "CleanupGeoAreaSelectionsParents";
    }

    @Override
    public String getDescription() {
        return "Cleans up the geo area selections by adding new selections for persons " +
                "that selected geo areas without selecting all of the parents. " +
                "This can happen if new geo areas were added up the hierarchy.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {

        //we cache the available geo areas, so that we do not query them every time
        // they are needed to filter out invalid selections
        Map<AppVariant, Set<GeoArea>> availableGeoAreasByAppVariant = new HashMap<>();

        processPages(logSummary, parameters,
                //find suspicious selections, these are the ones that did not select the parents
                // they might not all be invalid:
                // in the very special case that the parent belongs to another tenant that is not available in the app variant this might be okay
                pageRequest -> appService.findAllUsagesWithSelectionsWithoutParents(pageRequest),
                //and restore the consistency in parallel, according to the parameters
                invalidSelectionPage -> processParallel(
                        (AppVariantUsage appVariantUsage) -> restoreConsistency(
                                appVariantUsage,
                                availableGeoAreasByAppVariant,
                                logSummary,
                                parameters.isDryRun()),
                        invalidSelectionPage.getContent(), logSummary, parameters));
    }

    private void restoreConsistency(AppVariantUsage appVariantUsage,
            Map<AppVariant, Set<GeoArea>> availableGeoAreasByAppVariant, LogSummary logSummary, boolean dryRun) {

        AppVariant appVariant = appVariantUsage.getAppVariant();
        Person person = appVariantUsage.getPerson();
        Set<GeoArea> previousSelection = appService.getSelectedGeoAreas(appVariant, person);
        Set<GeoArea> availableGeoAreas = availableGeoAreasByAppVariant
                .computeIfAbsent(appVariant, appService::getAvailableGeoAreas);

        //invalid selections are those that are not a subset of the available geo areas
        Set<GeoArea> invalidSelectedAreas = new HashSet<>(previousSelection);
        invalidSelectedAreas.removeAll(availableGeoAreas);

        if (dryRun) {
            //we can not update the selection in dry run, so we calculate the potential change here
            Set<GeoArea> validPreviousSelection = new HashSet<>(previousSelection);
            validPreviousSelection.removeAll(invalidSelectedAreas);
            //if it is a common geo area or in the tenants of the app variant
            Set<GeoArea> geoAreasToAdd =
                    geoAreaService.addParentGeoAreas(validPreviousSelection, availableGeoAreas::contains);
            geoAreasToAdd.removeAll(validPreviousSelection);
            if (CollectionUtils.isEmpty(invalidSelectedAreas)) {
                logSummary.info("!Dry run! {}: {} selection of {}, would add {}: {}",
                        person.getId(),
                        getAppVariantIdentifier(appVariant),
                        previousSelection.size(),
                        geoAreasToAdd.size(),
                        toString(geoAreasToAdd));
            } else {
                logSummary.info(
                        "!Dry run! {}: {} selection of {}, would add {} and remove {} invalid: {} / {}",
                        person.getId(),
                        getAppVariantIdentifier(appVariant),
                        previousSelection.size(),
                        geoAreasToAdd.size(),
                        invalidSelectedAreas.size(),
                        toString(geoAreasToAdd),
                        toString(invalidSelectedAreas));
            }
        } else {
            //we need to remove the invalid selections to avoid exceptions when re-selecting them
            previousSelection.removeAll(invalidSelectedAreas);
            // just re-selecting the geo areas is enough, since this goes up the hierarchy
            Pair<AppVariantUsage, Set<GeoArea>> updatedSelection =
                    appService.selectGeoAreas(appVariant, person, previousSelection);
            List<GeoArea> addedGeoAreas = updatedSelection.getRight().stream()
                    .filter(o -> !previousSelection.contains(o))
                    .collect(Collectors.toList());
            if (CollectionUtils.isEmpty(invalidSelectedAreas)) {
                logSummary.info("{}: {} selection of {}, added {}: {}",
                        person.getId(),
                        getAppVariantIdentifier(appVariant),
                        previousSelection.size(),
                        addedGeoAreas.size(),
                        toString(addedGeoAreas));
            } else {
                logSummary.info("{}: {} selection of {}, added {} and removed {} invalid: {} / {}",
                        person.getId(),
                        getAppVariantIdentifier(appVariant),
                        previousSelection.size() + invalidSelectedAreas.size(),
                        addedGeoAreas.size(),
                        invalidSelectedAreas.size(),
                        toString(addedGeoAreas),
                        toString(invalidSelectedAreas));
            }
        }
    }

    private static String getAppVariantIdentifier(AppVariant appVariant) {
        return StringUtils.rightPad(appVariant.getAppVariantIdentifier(), 35);
    }

    private static String toString(Collection<GeoArea> geoAreas) {
        return geoAreas.stream()
                .map(GeoArea::getName)
                .sorted()
                .collect(Collectors.joining(", ", "{", "}"));
    }

    //copied from AppService, only needed for calculation the dry run result

}
