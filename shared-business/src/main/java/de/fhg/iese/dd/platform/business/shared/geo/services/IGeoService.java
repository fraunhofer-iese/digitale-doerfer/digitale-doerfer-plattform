/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Axel Wickenkamp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public interface IGeoService extends IService {

    /**
     * Lookup GPSLocation for an Address
     *
     * @param address the address to lookup
     * @return GPSLocation if successful, otherwise null
     */
    GPSLocation getGPSLocation(IAddressService.AddressDefinition address);

    GPSLocation getGPSLocation(String street, String zip, String city);

    /**
     * Resolves the locationLookupString to an Address. Street may be empty.
     *
     * @param locationName can be {@code null}
     * @param locationLookupString must not be {@code null}
     * @return {@code null} if no Address is found
     */
    Address resolveLocation(String locationName, String locationLookupString);

    /**
     * Resolves the GPSLocation to an Address. Street may be empty.
     *
     * @param locationName can be {@code null}
     * @param gpsLocation must not be {@code null}
     * @return {@code null} if no Address is found
     */
    Address resolveLocation(String locationName, GPSLocation gpsLocation);

    Object getDirections(GPSLocation from, GPSLocation to);

}
