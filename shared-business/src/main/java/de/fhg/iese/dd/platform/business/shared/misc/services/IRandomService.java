/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public interface IRandomService extends IService {

    int DEFAULT_PASSWORD_LENGTH = 20;

    GPSLocation randomGPSLocation();

    TimeSpan randomTimeSpan();

    String randomAlphabetic(String name, int min, int max);

    double randomDouble(double min, double max, int places);

    long randomTimeStamp(long min, long max);

    long randomLong(long min, long max);

    int randomInt(int min, int max);

    String randomNumberString(int length);

    boolean randomBoolean(double probability);

    <T> T randomElement(List<T> elements);

    String randomPassword(int passwordLength);

    String randomPassword();

}
