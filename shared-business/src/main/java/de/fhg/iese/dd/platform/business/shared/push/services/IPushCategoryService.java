/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotConfigurableException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IPushCategoryService extends IService, ICachingComponent {

    PushCategory findPushCategoryById(String categoryId) throws PushCategoryNotFoundException;

    PushCategory findPushCategoryById(PushCategory.PushCategoryId categoryId) throws PushCategoryNotFoundException;

    Set<PushCategory> findAllPushCategoriesForApp(App app);

    /**
     * Explanation for pushCategorySubject:
     * <p/>
     * Machine readable subject that helps to find push categories for a specific subject across multiple apps without
     * the need to explicitly identify it by id.
     * <p/>
     * An example are the "motivation" push categories for achievements and Digitaler. They are present in all apps and
     * need to be found for pushing "general" achievements to all of them.
     *
     * @param pushCategorySubject
     *         the subject that all returned push categories need to have
     *
     * @return all push categories with the given subject
     */
    Collection<PushCategory> findPushCategoriesBySubject(String pushCategorySubject);

    /**
     * Deletes the push category, all according push category user settings and removes its configuration from all
     * endpoints.
     *
     * @param pushCategory
     *         the push category to delete
     *
     * @return a human readable summary of what was deleted
     */
    LogSummary deletePushCategory(PushCategory pushCategory);

    PushCategory storePushCategory(PushCategory pushCategory);

    /**
     * Get the user setting for this category, if it exists, otherwise null.
     *
     * @param pushCategory
     * @param person
     * @param appVariant
     * @return
     */
    PushCategoryUserSetting findPushCategoryUserSetting(PushCategory pushCategory, Person person, AppVariant appVariant);

    /**
     * Change the settings for that user and that category if the category has
     * set {@code configurable == true}
     * <p/>
     * Changes all child categories with {@link PushCategory#getParentCategory()} == pushCategory
     *
     * @param pushCategory
     * @param person
     * @param appVariant
     * @param loudPushEnabled
     * @throws PushCategoryNotConfigurableException
     *             if {@code configurable == false}
     * @return Pair of the changed category and all changed child categories if there are any, otherwise empty list.
     */
    Pair<PushCategoryUserSetting, List<PushCategoryUserSetting>> changePushCategoryUserSetting(
            PushCategory pushCategory, Person person, AppVariant appVariant, boolean loudPushEnabled)
            throws PushCategoryNotConfigurableException;

    /**
     * Get all push settings for the person and the app variant.
     * <p/>
     * Important! the returned entities are partially virtual, if the user did
     * not configure the push category yet. The virtual ones do not have an id.
     *
     * @param person
     * @param appVariant
     *
     * @return
     */
    List<PushCategoryUserSetting> getConfigurableAndVisiblePushCategoryUserSettings(Person person,
            AppVariant appVariant);

    /**
     * Finds all push settings the person configured.
     * <p/>
     * This is only required for data privacy handling! There are more efficient methods available if actual push
     * configuration is done
     *
     * @param person
     *         the person that created the settings
     *
     * @return all settings of the person or an empty list if there are none
     */
    List<PushCategoryUserSetting> findAllPushCategoryUserSettings(Person person);

    /**
     * Divides the given persons into persons with loud and silent push enabled, according to the settings of the
     * persons. If no setting exists, the default setting of the push category is taken.
     * <p>
     * If persons or app variants is empty an empty list is returned.
     *
     * @param pushCategory the category to use for the settings
     * @param persons      the persons to divide
     * @param appVariants  the app variants of which the settings should be used
     *
     * @return a map that splits up the persons according to their settings.
     */
    Map<Boolean, Collection<Person>> filterPersonsWithPushCategoryUserSetting(PushCategory pushCategory,
            Collection<Person> persons, Collection<AppVariant> appVariants);

}
