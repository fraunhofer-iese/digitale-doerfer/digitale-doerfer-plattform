/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public interface IRoutingService extends IService {

    enum  TravelMode {
        WALKING, BICYCLING, DRIVING
    }

    /**
     * Compute (decimal-) minutes necessary to travel from start to end in TravelMode
     * @param start
     * @param end
     * @param type
     * @return time in minutes
     */
    long routeTimeInMinutes(GPSLocation start, GPSLocation end, TravelMode type);

}
