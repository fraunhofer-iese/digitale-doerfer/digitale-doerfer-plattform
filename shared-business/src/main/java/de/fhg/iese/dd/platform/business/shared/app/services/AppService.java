/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Stefan Schweitzer, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheChecker;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerIncrementalCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.exceptions.ReferenceDataDeletionException;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantAlreadyExistsException;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantUsageInvalidException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.legal.services.ILegalTextService;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.SharedConstants;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.*;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByAppVariantUsageAreaSelection;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.results.PersonCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.AppVariantOverrideFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
class AppService extends BaseEntityService<App> implements IAppService {

    private static final String APP_VARIANT_IDENTIFIER_PREFIX = "de.fhg.iese.dd.";

    private Map<AppVariant, Set<Tenant>> availableTenantsByAppVariant;
    private Map<AppVariant, Map<GeoArea, Set<AppVariantGeoAreaMapping>>> availableGeoAreasByAppVariant;
    private Map<AppVariant, Map<GeoArea, Set<AppVariantGeoAreaMapping>>> availableGeoAreasExcludingParentsByAppVariant;
    private Map<AppVariant, Set<GeoArea>> rootGeoAreasByAppVariant;
    private Map<String, App> appById;
    private Map<String, AppVariant> appVariantById;
    private Map<String, AppVariant> appVariantByAppVariantIdentifier;
    private Map<String, List<AppVariant>> appVariantsNotDeletedByOauthClientId;
    private Map<String, List<AppVariant>> appVariantsNotDeletedByApiKey;
    private App platformApp;
    private AppVariant platformAppVariant;

    @Autowired
    private AppRepository appRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private AppVariantTenantContractRepository appVariantTenantContractRepository;
    @Autowired
    private AppVariantGeoAreaMappingRepository appVariantGeoAreaMappingRepository;
    @Autowired
    private AppVariantVersionRepository appVariantVersionRepository;
    @Autowired
    private AppVariantUsageRepository appVariantUsageRepository;
    @Autowired
    private AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private ILegalTextService legalTextService;
    @Autowired
    private IRandomService randomService;
    @Autowired
    private CacheCheckerIncrementalCache cacheCheckerGeoAreasAndTenantsByAppVariant;
    @Autowired
    private CacheCheckerAtOnceCache cacheCheckerAppsAndAppVariants;

    @PostConstruct
    private void initialize() {

        cacheCheckerGeoAreasAndTenantsByAppVariant.addCheckQuery(geoAreaService::checkCacheGeoAreas);
        cacheCheckerGeoAreasAndTenantsByAppVariant.addCheckQuery(tenantService::checkCacheTenants);
        cacheCheckerAppsAndAppVariants.addCheckQuery(geoAreaService::checkCacheGeoAreas);

        //this cache needs to be concurrent, since it is incrementally filled with new values
        availableTenantsByAppVariant = new ConcurrentHashMap<>();
        availableGeoAreasByAppVariant = new ConcurrentHashMap<>();
        availableGeoAreasExcludingParentsByAppVariant = new ConcurrentHashMap<>();
        rootGeoAreasByAppVariant = new ConcurrentHashMap<>();
        appById = Collections.emptyMap();
        //this cache is replaced with a new map after it got cleared
        appVariantByAppVariantIdentifier = Collections.emptyMap();
        appVariantsNotDeletedByOauthClientId = Collections.emptyMap();
        appVariantsNotDeletedByApiKey = Collections.emptyMap();
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(App.class)
                .cachedEntity(AppVariant.class)
                .cachedEntity(AppVariantTenantContract.class)
                .cachedEntity(AppVariantGeoAreaMapping.class)
                .usedCache(IGeoAreaService.class)
                .usedCache(IFeatureService.class)
                .usedCache(ITenantService.class)
                .build();
    }

    @Override
    public App getPlatformApp() {
        if (platformApp == null) {
            platformApp = query(() -> appRepository.findById(SharedConstants.PLATFORM_APP_ID).orElse(null));
            if (platformApp == null) {
                //if it is not there, we create a stub that will be extended with more data during data init
                platformApp = save(() -> appRepository.saveAndFlush(App.builder()
                        .id(SharedConstants.PLATFORM_APP_ID)
                        .appIdentifier(SharedConstants.PLATFORM_APP_IDENTIFIER)
                        .build()));
            }
        }
        return platformApp;
    }

    @Override
    public AppVariant getPlatformAppVariant() {
        if (platformAppVariant == null) {
            platformAppVariant =
                    query(() -> appVariantRepository.findById(SharedConstants.PLATFORM_APP_VARIANT_ID).orElse(null));
            if (platformAppVariant == null) {
                //if it is not there, we create a stub that will be extended with more data during data init
                platformAppVariant = save(() -> appVariantRepository.saveAndFlush(AppVariant.builder()
                        .id(SharedConstants.PLATFORM_APP_VARIANT_ID)
                        .app(getPlatformApp())
                        .appVariantIdentifier(SharedConstants.PLATFORM_APP_VARIANT_IDENTIFIER)
                        .build()));
            }
        }
        return platformAppVariant;
    }

    @Override
    public App findById(String appId) throws AppNotFoundException {
        if (StringUtils.isEmpty(appId)) {
            throw new AppNotFoundException("App not found, appId is empty");
        }
        checkAndRefreshAppAndAppVariantCache();

        final App app = appById.get(appId);
        if (app == null) {
            throw new AppNotFoundException("App with id '{}' not found.", appId);
        }
        return app;
    }

    @Override
    public App findByAppIdentifier(String appIdentifier) throws AppNotFoundException {
        if (StringUtils.isEmpty(appIdentifier)) {
            throw new AppNotFoundException("App not found, appIdentifier is empty");
        }
        checkAndRefreshAppAndAppVariantCache();

        return appById.values().stream()
                .filter(a -> a.getAppIdentifier().equals(appIdentifier))
                .findFirst()
                .orElseThrow(() -> new AppNotFoundException("App with appIdentifier '{}' not found.", appIdentifier));
    }

    @Override
    public AppVariant findAppVariantByAppVariantIdentifierIncludingDeleted(String appVariantIdentifier)
            throws AppVariantNotFoundException {

        if (StringUtils.isEmpty(appVariantIdentifier)) {
            throw new AppVariantNotFoundException("AppVariant not found, appVariantIdentifier is empty").withDetail("");
        }
        checkAndRefreshAppAndAppVariantCache();

        AppVariant appVariant = appVariantByAppVariantIdentifier.get(appVariantIdentifier);
        if (appVariant == null) {
            throw new AppVariantNotFoundException("AppVariant with appVariantIdentifier '{}' not found.",
                    appVariantIdentifier);
        }
        return appVariant;
    }

    @Override
    public AppVariant findAppVariantByAppVariantIdentifier(String appVariantIdentifier)
            throws AppVariantNotFoundException {

        AppVariant appVariant = findAppVariantByAppVariantIdentifierIncludingDeleted(appVariantIdentifier);
        if (appVariant.isDeleted()) {
            throw new AppVariantNotFoundException("AppVariant with appVariantIdentifier '{}' is deleted.",
                    appVariantIdentifier);
        }
        return appVariant;
    }

    @Override
    public AppVariant findAppVariantByIdIncludingDeleted(String appVariantId) throws AppVariantNotFoundException {
        
        if (StringUtils.isEmpty(appVariantId)) {
            throw new AppVariantNotFoundException("AppVariant not found, id is empty").withDetail("");
        }
        checkAndRefreshAppAndAppVariantCache();

        AppVariant appVariant = appVariantById.get(appVariantId);
        if (appVariant == null) {
            throw new AppVariantNotFoundException("AppVariant with id '{}' not found.", appVariantId);
        } else {
            return appVariant;
        }
    }

    @Override
    public AppVariant findAppVariantById(String appVariantId) throws AppVariantNotFoundException {

        AppVariant appVariant = findAppVariantByIdIncludingDeleted(appVariantId);
        if (appVariant.isDeleted()) {
            throw new AppVariantNotFoundException("AppVariant with id '{}' is deleted.", appVariantId);
        } else {
            return appVariant;
        }
    }

    @Override
    public AppVariant findAppVariantByOauthClient(OauthClient oAuthClient, Optional<String> appVariantIdentifierOpt,
            Optional<String> apiKeyOpt) throws AppVariantNotFoundException {

        checkAndRefreshAppAndAppVariantCache();

        List<AppVariant> matchingAppVariants = appVariantsNotDeletedByOauthClientId.get(oAuthClient.getId());

        if (CollectionUtils.isEmpty(matchingAppVariants)) {
            throw new AppVariantNotFoundException("No app variant found for oauth client '{}'",
                    oAuthClient.getOauthClientIdentifier());
        }
        if (matchingAppVariants.size() == 1) {
            // most simple case, one to one mapping of oauth client to app variant
            AppVariant matchingAppVariant = matchingAppVariants.get(0);
            if (appVariantIdentifierOpt.isPresent()) {
                String appVariantIdentifier = appVariantIdentifierOpt.get();
                if (matchingAppVariant.getAppVariantIdentifier().equals(appVariantIdentifier)) {
                    // in this case the app variant identifier was useless, but not harmful
                    // if there was an api key in the request, it has to match
                    checkApiKeyMatches(oAuthClient, apiKeyOpt, matchingAppVariant);
                    return matchingAppVariant;
                } else {
                    //check if we have a feature for that app variant that allows to override the app variant
                    AppVariantOverrideFeature feature =
                            featureService.getFeatureOrNull(AppVariantOverrideFeature.class,
                                    FeatureTarget.of(matchingAppVariant));
                    if (feature != null && feature.isEnabled()) {
                        //we have a feature that allows overriding the app variant
                        if (!feature.getAllowedAppVariantIdentifiers().contains(appVariantIdentifier)) {
                            //the app variant identifier is not in the list of allowed ones
                            throw new AppVariantNotFoundException(
                                    "App variant found for oauth client '{}', app variant override feature enabled, " +
                                            "but app variant identifier '{}' is not allowed",
                                    oAuthClient.getOauthClientIdentifier(), appVariantIdentifier);
                        } else {
                            // now we can lookup the actual app variant that should be used instead
                            AppVariant overridingAppVariant =
                                    appVariantByAppVariantIdentifier.get(appVariantIdentifier);
                            if (overridingAppVariant == null) {
                                //that's a stupid misconfiguration:
                                // An app variant identifier is configured in the feature that is not existing
                                throw new AppVariantNotFoundException(
                                        "App variant found for oauth client '{}', app variant override feature enabled, " +
                                                "but app variant identifier '{}' does not match any app variant",
                                        oAuthClient.getOauthClientIdentifier(), appVariantIdentifier);
                            }
                            if (overridingAppVariant.isDeleted()) {
                                //that's a stupid misconfiguration:
                                // An app variant identifier is configured in the feature that is deleted
                                throw new AppVariantNotFoundException(
                                        "App variant found for oauth client '{}', app variant override feature enabled, " +
                                                "but app variant with app variant identifier '{}' is deleted",
                                        oAuthClient.getOauthClientIdentifier(), appVariantIdentifier);
                            }
                            return overridingAppVariant;
                        }
                    } else {
                        throw new AppVariantNotFoundException(
                                "App variant found for oauth client '{}', but app variant identifier '{}' differs",
                                oAuthClient.getOauthClientIdentifier(), appVariantIdentifier);
                    }
                }
            }
            // if there was an api key in the request, it has to match
            checkApiKeyMatches(oAuthClient, apiKeyOpt, matchingAppVariant);
            return matchingAppVariant;
        }
        //now there are multiple matching ones, we need to find the right one based on the other information we have

        if (appVariantIdentifierOpt.isPresent()) {
            //find all matching app variants based on app variant identifier
            // it can only be one or none due to the unique constraint on the app variant identifier
            String appVariantIdentifier = appVariantIdentifierOpt.get();
            Optional<AppVariant> matchingAppVariantForIdentifier = matchingAppVariants.stream()
                    .filter(appVariant -> StringUtils.equals(appVariant.getAppVariantIdentifier(),
                            appVariantIdentifier))
                    .findFirst();
            if (matchingAppVariantForIdentifier.isEmpty()) {
                throw new AppVariantNotFoundException(
                        "No app variant found for oauth client '{}' and app variant identifier '{}'",
                        oAuthClient.getOauthClientIdentifier(), appVariantIdentifier);
            }
            //there can't be more than one due to the unique constraint
            AppVariant matchingAppVariant = matchingAppVariantForIdentifier.get();
            //if there has been an api key in the request, it has to match
            checkApiKeyMatches(oAuthClient, apiKeyOpt, matchingAppVariant);
            return matchingAppVariant;
        }

        if (apiKeyOpt.isPresent()) {
            // find all matching app variants based on api key
            String apiKey = apiKeyOpt.get();
            checkApiKeyValid(apiKey);
            List<AppVariant> matchingAppVariantsForApiKey = matchingAppVariants.stream()
                    .filter(appVariant ->
                            StringUtils.equalsAny(apiKey, appVariant.getApiKey1(), appVariant.getApiKey2()))
                    .toList();

            if (matchingAppVariantsForApiKey.isEmpty()) {
                throw new AppVariantNotFoundException("No app variant found for oauth client '{}' and api key",
                        oAuthClient.getOauthClientIdentifier());
            }
            if (matchingAppVariantsForApiKey.size() > 1) {
                throw new AppVariantNotFoundException("Multiple app variants found for oauth client '{}' and api key",
                        oAuthClient.getOauthClientIdentifier());
            }
            return matchingAppVariantsForApiKey.get(0);
        }
        //we tried hard, but can't find the matching app variant
        throw new AppVariantNotFoundException("Multiple app variants found for oauth client '{}'",
                oAuthClient.getOauthClientIdentifier());
    }

    private void checkApiKeyMatches(OauthClient oAuthClient,
            @SuppressWarnings("OptionalUsedAsFieldOrParameterType") Optional<String> apiKeyOpt,
            AppVariant matchingAppVariant) {
        if (apiKeyOpt.isPresent()) {
            String apiKey = apiKeyOpt.get();
            checkApiKeyValid(apiKey);
            if (!StringUtils.equalsAny(apiKey, matchingAppVariant.getApiKey1(), matchingAppVariant.getApiKey2())) {
                throw new AppVariantNotFoundException(
                        "App variant found for oauth client '{}', but api key differs",
                        oAuthClient.getOauthClientIdentifier());
            }
        }
    }

    @Override
    public AppVariant findAppVariantByIdentifierOrApiKey(Optional<String> appVariantIdentifierOpt,
            Optional<String> apiKeyOpt) throws AppVariantNotFoundException {

        checkAndRefreshAppAndAppVariantCache();
        if (appVariantIdentifierOpt.isPresent()) {
            String appVariantIdentifier = appVariantIdentifierOpt.get();
            AppVariant appVariant = appVariantByAppVariantIdentifier.get(appVariantIdentifier);
            if (appVariant != null) {
                if (appVariant.isDeleted()) {
                    throw new AppVariantNotFoundException(
                            "App variant found for app variant identifier '{}', but is deleted",
                            appVariantIdentifier);
                }
                if (apiKeyOpt.isPresent()) {
                    String apiKey = apiKeyOpt.get();
                    checkApiKeyValid(apiKey);
                    if (!StringUtils.equalsAny(apiKey, appVariant.getApiKey1(), appVariant.getApiKey2())) {
                        throw new AppVariantNotFoundException(
                                "App variant found for app variant identifier '{}', but api key differs",
                                appVariantIdentifier);
                    }
                }
                return appVariant;
            }
        }
        if (apiKeyOpt.isPresent()) {
            return findAppVariantByApiKeyInternal(apiKeyOpt.get());
        }
        throw new AppVariantNotFoundException(
                "No app variant found, no app variant identifier and no api key provided");
    }

    @Override
    public AppVariant findAppVariantByApiKey(String apiKey) throws AppVariantNotFoundException {

        checkAndRefreshAppAndAppVariantCache();
        return findAppVariantByApiKeyInternal(apiKey);
    }

    private AppVariant findAppVariantByApiKeyInternal(String apiKey) {

        checkApiKeyValid(apiKey);
        List<AppVariant> appVariants = appVariantsNotDeletedByApiKey.get(apiKey);
        if (CollectionUtils.isEmpty(appVariants)) {
            throw new AppVariantNotFoundException("App variant not found for api key.");
        }

        if (appVariants.size() > 1) {
            throw new AppVariantNotFoundException("Multiple app variants found for api key.");
        }
        return appVariants.get(0);
    }

    private void checkApiKeyValid(String apiKey) throws AppVariantNotFoundException {
        if (StringUtils.isBlank(apiKey)) {
            throw new AppVariantNotFoundException("Empty api key, no app variant can be found");
        }
    }

    @Override
    public List<App> findAllApps() {
        checkAndRefreshAppAndAppVariantCache();
        return appById.values().stream()
                .sorted(Comparator.comparing(App::getAppIdentifier))
                .collect(Collectors.toList());
    }

    @Override
    public List<AppVariant> findAllAppVariants() {
        checkAndRefreshAppAndAppVariantCache();
        return appVariantByAppVariantIdentifier.values().stream()
                .filter(AppVariant::isNotDeleted)
                .sorted(Comparator.comparing(AppVariant::getAppVariantIdentifier))
                .collect(Collectors.toList());
    }

    @Override
    public Set<AppVariant> findAllAppVariants(Collection<Tenant> tenants) {
        checkAndRefreshAppAndAppVariantCache();
        return appVariantByAppVariantIdentifier.values().stream()
                .filter(AppVariant::isNotDeleted)
                .filter(appVariant -> !Collections.disjoint(getAvailableTenants(appVariant), tenants))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<AppVariant> findAllAppVariants(App app) {
        checkAndRefreshAppAndAppVariantCache();
        return appVariantByAppVariantIdentifier.values().stream()
                .filter(AppVariant::isNotDeleted)
                .filter(appVariant -> Objects.equals(app, appVariant.getApp()))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<AppVariant> findAllAppVariants(Set<GeoArea> geoAreas, App app) {
        checkAndRefreshAppAndAppVariantCache();
        return appVariantByAppVariantIdentifier.values().stream()
                .filter(AppVariant::isNotDeleted)
                .filter(appVariant -> Objects.equals(app, appVariant.getApp()))
                .filter(appVariant -> CollectionUtils.containsAny(getAvailableGeoAreas(appVariant), geoAreas))
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public AppVariant createAppVariant(AppVariant appVariant, String appVariantIdentifierSuffix) {

        checkAndRefreshAppAndAppVariantCache();

        String appVariantIdentifier = generateAppVariantIdentifier(appVariant.getApp().getAppIdentifier(),
                appVariantIdentifierSuffix);
        //we also need to check the deleted ones
        if (appVariantByAppVariantIdentifier.containsKey(appVariantIdentifier)) {
            throw AppVariantAlreadyExistsException.forAlreadyExists(appVariantIdentifier);
        }

        String apiKey = randomService.randomPassword(64);
        while (!isApiKeyAlreadyUsed(apiKey)) {
            apiKey = randomService.randomPassword(64);
        }

        appVariant.setName(appVariant.getApp().getName() + " " + appVariantIdentifierSuffix);
        appVariant.setAppVariantIdentifier(appVariantIdentifier);
        appVariant.setApiKey1(apiKey);

        return appVariantRepository.saveAndFlush(appVariant);
    }

    private String generateAppVariantIdentifier(String appIdentifier, String appVariantIdentifierSuffix) {
        final String adjustedAppIdentifier = appIdentifier.replace("-", ".");
        return APP_VARIANT_IDENTIFIER_PREFIX + adjustedAppIdentifier + "." + appVariantIdentifierSuffix;
    }

    private boolean isApiKeyAlreadyUsed(String apiKey) {
        checkAndRefreshAppAndAppVariantCache();
        return !appVariantsNotDeletedByApiKey.containsKey(apiKey);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void logHumanReadableAppVariantStatistics(AppVariant appVariant, LogSummary logSummary) {

        logSummary.info("Statistics for {}", appVariant.toString());
        List<AppVariantUsage> usages =
                query(() -> appVariantUsageRepository.findAllByAppVariantOrderByLastUsageDesc(appVariant));
        long personCount = usages.stream()
                .map(AppVariantUsage::getPerson)
                .distinct()
                .count();
        LongSummaryStatistics lastUsageStatistics = usages.stream()
                .collect(Collectors.summarizingLong(AppVariantUsage::getLastUsage));
        logSummary.info("{} usages", usages.size());
        logSummary.info("{} persons", personCount);
        long maxUsage = lastUsageStatistics.getMax() == Long.MIN_VALUE ? 0 : lastUsageStatistics.getMax();
        long minUsage = lastUsageStatistics.getMin() == Long.MAX_VALUE ? 0 : lastUsageStatistics.getMin();
        long avgUsage = Math.round(lastUsageStatistics.getAverage());
        logSummary.info("{} max last usage", timeService.toLocalTimeHumanReadable(maxUsage));
        logSummary.info("{} min last usage", timeService.toLocalTimeHumanReadable(minUsage));
        logSummary.info("{} avg last usage", timeService.toLocalTimeHumanReadable(avgUsage));
    }

    @Override
    @Transactional
    public void deleteAppVariantHard(AppVariant appVariant, LogSummary logSummary) {

        long existingUsages = appVariantUsageRepository.countByAppVariant(appVariant);
        if (existingUsages > APP_VARIANT_DELETION_MAX_USAGES) {
            String errorMessage =
                    logSummary.error("Too many ({}) usages of app variant, deletion is too risky", existingUsages);
            throw new ReferenceDataDeletionException(errorMessage);
        }
        logSummary.info("Deleting {}", appVariant.toString());
        logSummary.info("Deleted {} app variant usage geo area selections",
                appVariantUsageAreaSelectionRepository.deleteAllByAppVariantUsageAppVariant(appVariant));
        logSummary.info("Deleted {} app variant usages",
                appVariantUsageRepository.deleteAllByAppVariant(appVariant));
        logSummary.info("Deleted {} app variant versions",
                appVariantVersionRepository.deleteAllByAppVariant(appVariant));
        logSummary.info("Deleted {} app variant specific feature configs",
                featureService.deleteFeatureConfigAppVariant(appVariant));
        logSummary.info("Deleted {} app variant specific legal texts and acceptances",
                legalTextService.deleteLegalTextsAppVariant(appVariant));
        logSummary.info("Deleted {} app variant geo area mappings",
                appVariantGeoAreaMappingRepository.deleteByAppVariant(appVariant));
        logSummary.info("Deleted {} app variant tenant contracts",
                appVariantTenantContractRepository.deleteByAppVariant(appVariant));
        appVariantRepository.delete(appVariant);
        logSummary.info("Deleted app variant {}", appVariant.toString());
    }

    @Override
    public AppVariant deleteAppVariant(AppVariant appVariant) {

        appVariant.setDeleted(true);
        appVariant.setDeletionTime(timeService.currentTimeMillisUTC());
        return appVariantRepository.saveAndFlush(appVariant);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public AppVariantUsage createOrUpdateAppVariantUsage(AppVariant appVariant, Person person,
            boolean alwaysReturnAppVariantUsage) {

        final long currentTime = timeService.currentTimeMillisUTC();
        Long appVariantLastUsage = appVariantUsageRepository.getAppVariantLastUsage(appVariant, person);

        if (appVariantLastUsage == null) {
            //there is no app variant usage, it needs to be created
            AppVariantUsage newAppVariantUsage = AppVariantUsage.builder()
                    .appVariant(appVariant)
                    .person(person)
                    .lastUsage(currentTime)
                    .build()
                    .withConstantId();
            return saveAndRetryOnDataIntegrityViolation(
                    () -> appVariantUsageRepository.saveAndFlush(newAppVariantUsage),
                    () -> appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person));

        } else {
            //there is an existing app variant usage, check if we need to update it
            if (appVariantLastUsage + LAST_APP_VARIANT_USAGE_PERIOD < currentTime) {
                //we need to update the app variant usage, so we fetch it first
                AppVariantUsage appVariantUsage =
                        appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person);
                appVariantUsage.setLastUsage(currentTime);
                return saveAndRetryOnDataIntegrityViolation(
                        () -> appVariantUsageRepository.saveAndFlush(appVariantUsage),
                        () -> appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person));
            } else {
                if (alwaysReturnAppVariantUsage) {
                    // in this case we need to fetch the unchanged existing one
                    return appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person);
                } else {
                    return null;
                }
            }
        }
    }

    @Override
    public List<AppVariantUsage> getAppVariantUsages(App app, Person person) {
        return query(() -> appVariantUsageRepository.findAllByAppVariantAppAndPersonOrderByCreatedDesc(app, person));
    }

    @Override
    public List<AppVariantUsage> getAppVariantUsagesOrderByLastUsageDesc(App app, Person person) {
        return query(() -> appVariantUsageRepository.findAllByAppVariantAppAndPersonOrderByLastUsageDesc(app, person));
    }

    @Override
    public List<AppVariantUsage> getAppVariantUsages(Person person) {
        return query(() -> appVariantUsageRepository.findAllByPersonOrderByCreatedDesc(person));
    }

    @Override
    public Set<Tenant> getAvailableTenants(AppVariant appVariant) {

        checkAndClearAppVariantGeoAreaCaches();
        return availableTenantsByAppVariant.computeIfAbsent(appVariant,
                this::internalGetAvailableTenants);
    }

    @Override
    public Set<GeoArea> getAvailableGeoAreas(final AppVariant appVariant) {

        return getAvailableGeoAreasWithMappings(appVariant).keySet();
    }

    @Override
    public Set<String> getAvailableGeoAreaIds(final AppVariant appVariant) {

        return getAvailableGeoAreasWithMappings(appVariant).keySet().stream().map(GeoArea::getId)
                .collect(Collectors.toSet());
    }

    @Override
    public Map<GeoArea, Set<AppVariantGeoAreaMapping>> getAvailableGeoAreasWithMappings(final AppVariant appVariant) {

        checkAndClearAppVariantGeoAreaCaches();
        return availableGeoAreasByAppVariant.computeIfAbsent(appVariant,
                this::internalGetAvailableGeoAreasWithMappings);
    }

    private Map<GeoArea, Set<AppVariantGeoAreaMapping>> internalGetAvailableGeoAreasWithMappings(
            AppVariant appVariant) {

        final Map<GeoArea, Set<AppVariantGeoAreaMapping>> geoAreasWithMappings =
                new HashMap<>(getAvailableGeoAreasStrictWithMappings(appVariant));
        final Set<GeoArea> parentGeoAreas = geoAreaService.getParentGeoAreas(geoAreasWithMappings.keySet());
        //the parent areas do not have a mapping, so we add them as empty sets
        parentGeoAreas.forEach(parent -> geoAreasWithMappings.putIfAbsent(parent, Collections.emptySet()));
        return geoAreasWithMappings;
    }

    @Override
    public Set<GeoArea> getAvailableGeoAreasStrict(AppVariant appVariant) {

        return getAvailableGeoAreasStrictWithMappings(appVariant).keySet();
    }

    @Override
    public Map<GeoArea, Set<AppVariantGeoAreaMapping>> getAvailableGeoAreasStrictWithMappings(AppVariant appVariant) {

        checkAndClearAppVariantGeoAreaCaches();
        return availableGeoAreasExcludingParentsByAppVariant.computeIfAbsent(appVariant,
                this::internalGetAvailableGeoAreasExcludingParents);
    }

    @Override
    public Set<GeoArea> getConfiguredAvailableRootGeoAreas(AppVariant appVariant) {

        final Set<AppVariantGeoAreaMapping> mappings =
                appVariantGeoAreaMappingRepository.findAllByAppVariant(appVariant);
        if (mappings.stream().anyMatch(AppVariantGeoAreaMapping::isExcluded)) {
            //in this case some geo areas are excluded, so it is not a set of subtrees anymore
            //we just use all the geo areas in this case, in an optimization it
            return getAvailableGeoAreasStrict(appVariant);
        }
        return mappings.stream()
                .map(AppVariantGeoAreaMapping::getGeoArea)
                .collect(Collectors.toSet());
    }

    private Set<Tenant> internalGetAvailableTenants(AppVariant appVariant) {

        final Set<AppVariantGeoAreaMapping> appVariantGeoAreaMappings =
                query(() -> appVariantGeoAreaMappingRepository.findAllByAppVariant(appVariant));

        return appVariantGeoAreaMappings.stream()
                .map(AppVariantGeoAreaMapping::getContract)
                .filter(Objects::nonNull)
                .map(AppVariantTenantContract::getTenant)
                .map(tenantService::forceCached)
                .collect(Collectors.toSet());
    }

    private Map<GeoArea, Set<AppVariantGeoAreaMapping>> internalGetAvailableGeoAreasExcludingParents(
            AppVariant appVariant) {

        long start = System.currentTimeMillis();
        final Map<GeoArea, Set<AppVariantGeoAreaMapping>> mappingsByGeoArea = new HashMap<>();

        //the exclusion is relative to the contract, so we need to group it by contract
        final Map<AppVariantTenantContract, List<AppVariantGeoAreaMapping>> mappingsByContract =
                query(() -> appVariantGeoAreaMappingRepository.findAllByAppVariant(appVariant)).stream()
                        .collect(Collectors.groupingBy(AppVariantGeoAreaMapping::getContract));

        for (List<AppVariantGeoAreaMapping> mappingsForContract : mappingsByContract.values()) {

            final Map<GeoArea, Set<AppVariantGeoAreaMapping>> includedGeoAreasAndMappings = mappingsForContract.stream()
                    .filter(m -> !m.isExcluded())
                    .peek(m -> m.setGeoArea(geoAreaService.ensureCached(m.getGeoArea())))
                    //we want to know what mapping is responsible for having this geo area available at this app variant
                    .flatMap(m -> geoAreaService.getAllChildAreasIncludingSelf(m.getGeoArea()).stream()
                            .map(geoArea -> Pair.of(m, geoArea)))
                    .collect(Collectors.groupingBy(Pair::getRight,
                            Collectors.mapping(Pair::getLeft, Collectors.toSet())));

            //the excluded get removed
            mappingsForContract.stream()
                    .filter(AppVariantGeoAreaMapping::isExcluded)
                    .flatMap(m -> geoAreaService.getAllChildAreasIncludingSelf(m.getGeoArea()).stream())
                    .forEach(includedGeoAreasAndMappings::remove);

            includedGeoAreasAndMappings.forEach(
                    (key, value) -> mappingsByGeoArea.computeIfAbsent(key, x -> new HashSet<>()).addAll(value));
        }

        log.info("Caching {} geo area mapping for {} took {}ms",
                mappingsByGeoArea.size(), appVariant.getAppVariantIdentifier(), System.currentTimeMillis() - start);
        return mappingsByGeoArea;
    }

    @Override
    public Set<GeoArea> getNearestAvailableGeoAreas(AppVariant appVariant, GPSLocation currentLocation, double radiusKM,
            boolean onlyLeaves) {
        return geoAreaService.findNearestGeoAreas(getAvailableGeoAreas(appVariant), currentLocation, radiusKM,
                onlyLeaves);
    }

    @Override
    public Set<GeoArea> getAvailableRootGeoAreas(final AppVariant appVariant) {

        checkAndClearAppVariantGeoAreaCaches();
        return rootGeoAreasByAppVariant.computeIfAbsent(appVariant,
                // fetch the available root geo areas if they were not fetched already (or the cache was just cleared)
                // get the geo areas from the database
                this::internalGetAvailableRootGeoAreas);
    }

    private Set<GeoArea> internalGetAvailableRootGeoAreas(AppVariant appVariant) {

        Set<GeoArea> availableGeoAreasByAppVariant = getAvailableGeoAreas(appVariant);
        return geoAreaService.findAllRootAreas().stream()
                .filter(availableGeoAreasByAppVariant::contains)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<GeoArea> getAvailableLeafGeoAreas(final AppVariant appVariant) {
        return geoAreaService.removeNonLeafGeoAreas(getAvailableGeoAreas(appVariant));
    }

    @Override
    public Set<GeoArea> searchAvailableGeoAreas(AppVariant appVariant, int upToDepth,
            boolean includeAllChildrenOfMatchingAreas, String search) {

        //cache check is done in called method
        return geoAreaService.searchInGeoAreas(getAvailableGeoAreas(appVariant),
                upToDepth, includeAllChildrenOfMatchingAreas, search);
    }

    @Override
    public Set<GeoArea> getSelectedGeoAreas(final AppVariant appVariant, final Person person) {

        // we do not get the geo areas directly, we only get their id and then use the cached ones
        return getSelectedGeoAreaIds(appVariant, person).stream()
                .map(geoAreaService::findGeoAreaById)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSelectedGeoAreaIds(AppVariant appVariant, Person person) {

        return query(
                () -> appVariantUsageAreaSelectionRepository.findAllSelectedGeoAreaIdsByAppVariant(appVariant, person));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public Set<Tenant> getTenantsOfSelectedGeoAreas(AppVariant appVariant, Person person) {

        final AppVariantUsage appVariantUsage = appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person);

        if (appVariantUsage != null) {
            return appVariantUsageAreaSelectionRepository.findAllTenantsOfSelectedGeoAreasByAppVariantUsage(appVariantUsage);
        } else {
            return Collections.emptySet();
        }
    }

    @Nullable
    private GeoArea getHomeArea(Person person) {
        if (person.getHomeArea() != null) {
            // we get the cached geo area from the service, because we need to walk up the parent hierarchy
            // if we do it on the one from the database it will result in a lot of lazy loading queries
            return geoAreaService.forceCached(person.getHomeArea());
        } else {
            return null;
        }
    }

    @Override
    public Pair<AppVariantUsage, Set<GeoArea>> selectGeoAreas(final AppVariant appVariant, final Person person,
            final Set<GeoArea> selectedGeoAreas) throws AppVariantUsageInvalidException {

        final Set<GeoArea> availableGeoAreas = getAvailableGeoAreas(appVariant);
        if (!availableGeoAreas.containsAll(selectedGeoAreas)) {
            throw new AppVariantUsageInvalidException("The selected geo areas are not a subset of the available geo " +
                    "areas. These geo areas are invalid: {}",
                    selectedGeoAreas
                            .stream()
                            .filter(selectedArea -> !availableGeoAreas.contains(selectedArea))
                            .map(BaseEntity::getId)
                            .collect(Collectors.joining(", ", "[", "]")));
        }
        Set<GeoArea> adjustedSelectedGeoAreas;
        //we want the home area in the selection if it is in the available geo areas
        GeoArea homeArea = getHomeArea(person);
        if (homeArea != null && availableGeoAreas.contains(homeArea)) {
            adjustedSelectedGeoAreas = new HashSet<>(selectedGeoAreas);
            adjustedSelectedGeoAreas.add(homeArea);
        } else {
            adjustedSelectedGeoAreas = selectedGeoAreas;
        }

        //we walk up the path to the root of the tree and select them, too
        final Set<GeoArea> selectedGeoAreasAndParents =
                geoAreaService.addParentGeoAreas(adjustedSelectedGeoAreas, availableGeoAreas::contains);

        final AppVariantUsage appVariantUsage = createOrUpdateAppVariantUsage(appVariant, person, true);

        save(() ->
                executeAndRetryOnDataIntegrityViolation(
                        () -> {
                            internalSelectGeoAreas(appVariantUsage, selectedGeoAreasAndParents);
                            return null;
                        }));

        return Pair.of(appVariantUsage, selectedGeoAreasAndParents);
    }

    private void internalSelectGeoAreas(AppVariantUsage appVariantUsage, Set<GeoArea> allSelectedGeoAreas) {

        //we delete all previous selections that are now not selected anymore
        appVariantUsageAreaSelectionRepository.deleteByAppVariantUsageAndSelectedAreaIsNotIn(appVariantUsage,
                allSelectedGeoAreas);

        //we get all remaining selected geo area ids already existing in the DB
        Set<String> existingSelectedAreas = appVariantUsageAreaSelectionRepository
                .findAllSelectedGeoAreaIdsByAppVariantUsage(appVariantUsage);

        List<AppVariantUsageAreaSelection> selectionsToCreate = allSelectedGeoAreas.stream()
                //filter out already created selections
                .filter(geoArea -> !existingSelectedAreas.contains(geoArea.getId()))
                .map(areaToSelect -> AppVariantUsageAreaSelection.builder()
                        .appVariantUsage(appVariantUsage)
                        .selectedArea(areaToSelect)
                        .build()
                        .withConstantId())
                .collect(Collectors.toList());
        appVariantUsageAreaSelectionRepository.saveAll(selectionsToCreate);
        appVariantUsageAreaSelectionRepository.flush();
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public Map<GeoArea, Map<AppVariant, Long>> getPersonCountBySelectedGeoAreaAndAppVariant() {
        List<PersonCountByAppVariantUsageAreaSelection> countByAppVariantUsageAreaSelections =
                appVariantUsageAreaSelectionRepository.countByAppVariantUsageAreaSelection();

        return countByAppVariantUsageAreaSelections.stream()
                .collect(Collectors.groupingBy(PersonCountByAppVariantUsageAreaSelection::getGeoArea,
                        Collectors.groupingBy(PersonCountByAppVariantUsageAreaSelection::getAppVariant,
                                // the summing is only needed if there would be more than one entity per
                                // geoarea and app variant, which should never be the case
                                Collectors.summingLong(PersonCountByAppVariantUsageAreaSelection::getPersonCount))));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public Map<String, Long> getPersonCountBySelectedGeoAreaId(App app) {

        List<PersonCountByGeoAreaId> countByAppUsageAreaSelections =
                appVariantUsageAreaSelectionRepository.countByAppUsageAreaSelection(app);

        return countByAppUsageAreaSelections.stream()
                .collect(Collectors.toMap(
                        PersonCountByGeoAreaId::getGeoAreaId,
                        PersonCountByGeoAreaId::getPersonCount));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public Collection<AppVariantUsageAreaSelection> findAllSelectionsWithoutChildren() {
        return appVariantUsageAreaSelectionRepository.findAllInvalidSelectionsWithoutChildren();
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public Page<AppVariantUsage> findAllUsagesWithSelectionsWithoutParents(Pageable request) {
        return appVariantUsageAreaSelectionRepository.findAllInvalidUsagesWithoutParentSelections(request);
    }

    @Override
    public Long checkCacheAvailableGeoAreasByAppVariant() {
        checkAndClearAppVariantGeoAreaCaches();
        return cacheCheckerGeoAreasAndTenantsByAppVariant.getLastCacheRefresh();
    }

    @Override
    public Long checkCacheAppsAndAppVariants() {

        checkAndClearAppVariantGeoAreaCaches();
        return cacheCheckerAppsAndAppVariants.getLastCacheRefresh();
    }

    @Override
    public void invalidateCache() {

        cacheCheckerGeoAreasAndTenantsByAppVariant.resetCacheChecks();
        cacheCheckerAppsAndAppVariants.resetCacheChecks();

        log.info("invalidated app service cache");
    }

    private void checkAndClearAppVariantGeoAreaCaches() {

        cacheCheckerGeoAreasAndTenantsByAppVariant.clearCacheIfStale(
                () -> {
                    CacheChecker.clearIfNotNull(availableTenantsByAppVariant);
                    CacheChecker.clearIfNotNull(availableGeoAreasByAppVariant);
                    CacheChecker.clearIfNotNull(availableGeoAreasExcludingParentsByAppVariant);
                    CacheChecker.clearIfNotNull(rootGeoAreasByAppVariant);
                    log.info("Cleared geo area and tenant cache");
                });
    }

    private void checkAndRefreshAppAndAppVariantCache() {
        // if the cache is stale or no fetch ever happened
        // get all apps / app variants fresh from the database
        // additionally the app entities of app variants are replaced by the cached ones
        cacheCheckerAppsAndAppVariants.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();
            appById = query(() -> appRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, App.class))
                    .collect(Collectors.toMap(App::getId, Function.identity())));

            Set<AppVariant> appVariants = query(() -> appVariantRepository.findAllFull().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, AppVariant.class))
                    .peek(this::replaceAppByCachedApp)
                    .collect(Collectors.toSet()));

            appVariantByAppVariantIdentifier = appVariants.stream()
                    .collect(Collectors.toMap(AppVariant::getAppVariantIdentifier, Function.identity()));

            appVariantById = appVariants.stream()
                    .collect(Collectors.toMap(AppVariant::getId, Function.identity()));

            appVariantsNotDeletedByApiKey = appVariants.stream()
                    .filter(AppVariant::isNotDeleted)
                    .flatMap(appVariant -> Stream.of(
                            Pair.of(appVariant.getApiKey1(), appVariant),
                            Pair.of(appVariant.getApiKey2(), appVariant)))
                    .filter(p -> !StringUtils.isBlank(p.getLeft()))
                    .collect(Collectors.groupingBy(Pair::getLeft,
                            Collectors.mapping(Pair::getRight, Collectors.toList())));

            appVariantsNotDeletedByOauthClientId = appVariants.stream()
                    .filter(AppVariant::isNotDeleted)
                    .flatMap(a -> a.getOauthClients().stream()
                            .map(c -> Pair.of(c.getId(), a)))
                    .collect(Collectors.groupingBy(Pair::getKey,
                            Collectors.mapping(Pair::getRight, Collectors.toList())));
            //we want to reload them, if we cached the intermediate stubs before
            platformApp = null;
            platformAppVariant = null;
            log.info("Refreshed app variant cache in {} ms, loading {} apps and {} app variants",
                    System.currentTimeMillis() - start, appById.size(), appVariantById.size());
        });
    }

    private void replaceAppByCachedApp(AppVariant appVariant) {

        App cachedApp = appById.get(appVariant.getApp().getId());
        appVariant.setApp(cachedApp);
    }

}
