/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.model;

import java.nio.charset.StandardCharsets;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Container for a push message that is ready to be sent.
 * <p>
 * <b>IMPORTANT</b>
 * Never instantiate this manually, always use {@code IClientPushService} to create it.
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode //needed by PushSendServiceTest
public class PushMessage {

    private String jsonMessageLoudFcm;
    private String jsonMessageSilentFcm;
    private String jsonMessageLoudApns;
    private String jsonMessageSilentApns;
    private String logMessage;

    public String getMessageFcm(boolean isLoud) {
        if (isLoud) {
            return jsonMessageLoudFcm;
        } else {
            return jsonMessageSilentFcm;
        }
    }

    public String getMessageApns(boolean isLoud) {
        if (isLoud) {
            return jsonMessageLoudApns;
        } else {
            return jsonMessageSilentApns;
        }
    }

    public int getMessageSizeFcm(boolean isLoud) {
        return getMessageSize(getMessageFcm(isLoud));
    }

    public int getMessageSizeApns(boolean isLoud) {
        return getMessageSize(getMessageApns(isLoud));
    }

    private int getMessageSize(String message) {
        if (message != null) {
            return message.getBytes(StandardCharsets.UTF_8).length;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "PushMessage [logMessage='" + logMessage + "']";
    }

}
