/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.slack;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectReader;

import de.fhg.iese.dd.platform.business.shared.teamnotification.BaseHttpTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.Data;

class SlackTeamNotificationPublisher extends BaseHttpTeamNotificationPublisher {

    private final SlackConnectionConfig connectionConfig;

    @Data
    private static class SlackConnectionConfig {

        private String webHookUrl;
        private String username;

    }

    SlackTeamNotificationPublisher(ITimeService timeService, RestTemplateBuilder restTemplateBuilder,
            TeamNotificationConnection connection) {
        super(timeService, restTemplateBuilder, connection);
        connectionConfig = readConnectionConfiguration(connection);
    }

    static SlackConnectionConfig readConnectionConfiguration(TeamNotificationConnection connection) {
        SlackConnectionConfig connectionConfig;
        try {
            ObjectReader connectionConfigReader = defaultJsonReader().forType(SlackConnectionConfig.class);
            connectionConfig = connectionConfigReader.readValue(connection.getConnectionConfig());
        } catch (IOException e) {
            throw new TeamNotificationConnectionConfigInvalidException("Connection config invalid in {}: {}",
                    connection, e.getMessage());
        }
        if (connectionConfig == null) {
            throw new TeamNotificationConnectionConfigInvalidException("No connection config in {}", connection);
        }
        if (StringUtils.isEmpty(connectionConfig.getWebHookUrl())) {
            throw new TeamNotificationConnectionConfigInvalidException("No url in connection config in {}", connection);
        }
        if (StringUtils.isEmpty(connectionConfig.getUsername())) {
            throw new TeamNotificationConnectionConfigInvalidException("No username in connection config in {}",
                    connection);
        }
        return connectionConfig;
    }

    @Override
    protected void processSendRequest(TeamNotificationSendRequest request) throws Exception {
        String body = String.format("{\"username\":\"%s\",\"text\":\"%s\"}",
                connectionConfig.getUsername(), request.getMessage().getPlainText());

        HttpHeaders headers = getJsonHeaders();
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        ResponseEntity<String> response =
                getRestTemplate().postForEntity(connectionConfig.getWebHookUrl(), entity, String.class);

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error("Could not push to slack: slack service returned != OK");
        }
    }
    
}
