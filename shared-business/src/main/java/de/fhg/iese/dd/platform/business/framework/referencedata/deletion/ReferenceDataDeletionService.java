/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.deletion;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.business.framework.datadependency.services.IDataDependencyAwareOrderService;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.exceptions.ReferenceDataDeletionException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@Service
class ReferenceDataDeletionService extends BaseService implements IReferenceDataDeletionService {

    private static final String TEAM_NOTIFICATION_TOPIC = "data-deletion";
    //currently the same as data init, because we want it in the same log
    private static final Marker LOG_MARKER_DATA_DELETION = MarkerManager.getMarker("DATA_INIT");

    @Autowired
    private IPluginService pluginService;
    @Autowired
    private IDataDependencyAwareOrderService dataEntityDependencyAwareOrderService;
    @Autowired
    private ITeamNotificationService teamNotificationService;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;

    @Override
    public <T extends BaseEntity> ReferenceDataChangeResult checkReferenceDataDeletionImpact(
            ReferenceDataCheckDeletionImpactRequest<T> request, Person caller) {
        checkReferenceDataChangeRequestValidity(request);
        LogSummary logSummary = new LogSummary(log, LOG_MARKER_DATA_DELETION);
        //if the dependent entities should be deleted a different strategy is chosen
        if (request.isDeleteDependentEntities()) {
            List<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<T>> strategies =
                    findMatchingDeleteStrategies(request);
            return callDataDeletionHandlers(
                    strategies,
                    s -> s.checkImpact(
                            request.getEntityToBeDeleted(),
                            logSummary),
                    request,
                    logSummary,
                    "impact check (deletion of dependent entities)",
                    caller);
        } else {
            List<DataDeletionStrategy.SwapReferencesDeletionStrategy<T>> strategies =
                    findMatchingSwapStrategies(request);
            return callDataDeletionHandlers(
                    strategies,
                    s -> s.checkImpact(
                            request.getEntityToBeDeleted(),
                            request.getEntityToBeUsedInstead(),
                            logSummary),
                    request,
                    logSummary,
                    "impact check (swap before deletion)",
                    caller);
        }
    }

    @Override
    public <T extends BaseEntity> ReferenceDataChangeResult deleteReferenceData(
            ReferenceDataDeletionRequest<T> request, Person caller) {
        checkReferenceDataChangeRequestValidity(request);
        LogSummary logSummary = new LogSummary(log, LOG_MARKER_DATA_DELETION);
        final ReferenceDataChangeLogEntry referenceDataChangeLock =
                referenceDataChangeService.acquireReferenceDataChangeLock(caller, request.toString(), true);
        try {
            final ReferenceDataChangeResult changeResult;
            //if the dependent entities should be deleted a different strategy is chosen
            if (request.isDeleteDependentEntities()) {
                List<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<T>> strategies =
                        findMatchingDeleteStrategies(request);
                changeResult = callDataDeletionHandlers(
                        strategies,
                        s -> s.deleteData(
                                request.getEntityToBeDeleted(),
                                logSummary),
                        request,
                        logSummary,
                        "deletion of dependent entities",
                        caller);
            } else {
                List<DataDeletionStrategy.SwapReferencesDeletionStrategy<T>> strategies =
                        findMatchingSwapStrategies(request);
                changeResult = callDataDeletionHandlers(
                        strategies,
                        s -> s.swapData(
                                request.getEntityToBeDeleted(),
                                request.getEntityToBeUsedInstead(),
                                logSummary),
                        request,
                        logSummary,
                        "swap before deletion",
                        caller);
            }
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLock, true);
            return changeResult;
        } catch (Exception e) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLock, false);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    private <T extends BaseEntity> List<DataDeletionStrategy.SwapReferencesDeletionStrategy<T>> findMatchingSwapStrategies(
            ReferenceDataChangeRequest<T> request) {
        Class<T> entityToBeDeletedClass = request.getEntityToBeDeletedClass();
        final List<IReferenceDataDeletionHandler> referenceDataDeletionHandlers = pluginService.getPluginInstancesRaw(
                PluginTarget.of(IReferenceDataDeletionHandler.class));
        List<DataDeletionStrategy.SwapReferencesDeletionStrategy<T>> strategies = referenceDataDeletionHandlers.stream()
                .flatMap(h -> h.getSwapReferencesDeletionStrategies().stream())
                .filter(s -> s.getTriggeringEntity().isAssignableFrom(entityToBeDeletedClass))
                //this cast is okay, since this are only filtered strategies
                .map(s -> (DataDeletionStrategy.SwapReferencesDeletionStrategy<T>) s)
                .collect(Collectors.toList());
        //processed before triggering
        return dataEntityDependencyAwareOrderService.orderProcessedBeforeRequired(strategies);
    }

    @SuppressWarnings("unchecked")
    private <T extends BaseEntity> List<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<T>> findMatchingDeleteStrategies(
            ReferenceDataChangeRequest<T> request) {
        Class<T> entityToBeDeletedClass = request.getEntityToBeDeletedClass();
        final List<IReferenceDataDeletionHandler> referenceDataDeletionHandlers = pluginService.getPluginInstancesRaw(
                PluginTarget.of(IReferenceDataDeletionHandler.class));
        List<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<T>> strategies =
                referenceDataDeletionHandlers.stream()
                        .flatMap(h -> h.getDeleteDependentEntitiesDeletionStrategies().stream())
                        .filter(s -> s.getTriggeringEntity().isAssignableFrom(entityToBeDeletedClass))
                        //this cast is okay, since this are only filtered strategies
                        .map(s -> (DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<T>) s)
                        .collect(Collectors.toList());
        //processed before triggering
        return dataEntityDependencyAwareOrderService.orderProcessedBeforeRequired(strategies);
    }

    private <T extends BaseEntity, S extends DataDeletionStrategy<T>> ReferenceDataChangeResult callDataDeletionHandlers(
            List<S> strategiesOrdered,
            Consumer<S> methodToCall,
            ReferenceDataChangeRequest<T> request,
            LogSummary logSummary,
            String logMessage,
            Person caller) {

        List<Exception> thrownExceptions = new LinkedList<>();
        long start = System.currentTimeMillis();

        logSummary.info("start {}: {}", logMessage, request.toStringLineBreak());

        logSummary.info("Intended execution order:\n{}", strategiesOrdered.stream()
                .map(IDataDependencyAware::getId)
                .collect(Collectors.joining("\n")));
        for (S deletionStrategy : strategiesOrdered) {
            String strategyId = deletionStrategy.getId();

            logSummary.info("{}: start", strategyId);
            logSummary.indent();
            //set the logger to the logger of the deletion handler to have a better readable log
            logSummary.setLogger(LogManager.getLogger(deletionStrategy.getDeletionHandler()));
            try {
                methodToCall.accept(deletionStrategy);
            } catch (Exception e) {
                logSummary.setLogger(log);
                thrownExceptions.add(e);
                logSummary.error("Failed with {} : {}", e, strategyId, e.toString());
            }
            logSummary.setLogger(log);
            logSummary.resetIndentation();
            logSummary.info("{}: finished", strategyId);
        }

        logSummary.info("finished {}: {}", logMessage, request.toStringLineBreak());
        if (!thrownExceptions.isEmpty()) {
            log.error("There were {} exceptions!", thrownExceptions.size());
            thrownExceptions.forEach(
                    e -> log.error("Exception while running reference data deletion: " + e.toString(), e));
        }
        ReferenceDataChangeResult deletionResult = ReferenceDataChangeResult.builder()
                .exceptions(thrownExceptions)
                .message(logSummary.toString())
                .build();
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                deletionResult.wasSuccessful()
                        ? TeamNotificationPriority.INFO_TECHNICAL_SYSTEM
                        : TeamNotificationPriority.WARN_TECHNICAL_SYSTEM,
                "Called `{}` by {} `{}` in {} seconds with result {}, {} errors and {} warnings",
                logMessage,
                caller != null ? caller.getFullName() : "anonymous",
                caller != null ? caller.getId() : "-",
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start),
                deletionResult.wasSuccessful() ? "🆗" : "❌",
                logSummary.getErrorCount(), logSummary.getWarningsCount());
        return deletionResult;
    }

    private void checkReferenceDataChangeRequestValidity(ReferenceDataChangeRequest<?> request) {
        if (request.getEntityToBeDeleted() == null) {
            throw new ReferenceDataDeletionException("Request is invalid, entity to be deleted is null");
        }
        if (!request.isDeleteDependentEntities()) {
            if (request.getEntityToBeUsedInstead() == null) {
                throw new ReferenceDataDeletionException("Request is invalid, entity to be used instead is null");
            }
            if (Objects.equals(request.getEntityToBeDeleted(), request.getEntityToBeUsedInstead())) {
                throw new ReferenceDataDeletionException("Request is invalid, entities are equal");
            }
        }
    }

}
