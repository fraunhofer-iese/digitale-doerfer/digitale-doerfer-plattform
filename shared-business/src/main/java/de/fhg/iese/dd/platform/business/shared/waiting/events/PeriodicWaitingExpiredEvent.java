/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.events;

import org.springframework.context.ApplicationContext;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class PeriodicWaitingExpiredEvent extends WaitingExpiredEvent {

    private long previousDeadline;

    /**
     * Calculate the next deadline based on the current expired deadline or
     * relative to now.
     * <p>
     * If the event did not run so far, the {@link #getDeadline()} is null. In
     * this case the next deadline should be relative to now.
     * <p>
     * If the event did already run, use {@link #getDeadline()} as the current
     * expired deadline to calculate the next one.
     * <p>
     * The {@link ApplicationContext} can be used to get access to all Spring
     * beans, e.g. configuration and services.
     */
    @Override
    public abstract WaitingDeadlineUpdate nextDeadline(ApplicationContext context);

}
