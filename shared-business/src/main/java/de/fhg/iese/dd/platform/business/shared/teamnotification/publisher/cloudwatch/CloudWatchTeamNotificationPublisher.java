/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.cloudwatch;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import de.fhg.iese.dd.platform.business.shared.teamnotification.IDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Setter;
import lombok.Value;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.CreateLogGroupRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.CreateLogStreamRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogStreamsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogStreamsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.InputLogEvent;
import software.amazon.awssdk.services.cloudwatchlogs.model.PutLogEventsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.ResourceAlreadyExistsException;
import software.amazon.awssdk.services.cloudwatchlogs.model.ResourceNotFoundException;

@Profile("aws | CloudWatchPublisherTest")
@Log4j2
@Component
public class CloudWatchTeamNotificationPublisher implements IDefaultTeamNotificationPublisher {

    @Autowired
    private AWSConfig awsConfig;

    @Autowired
    private IEnvironmentService environmentService;

    private CloudWatchLogsClient cloudWatchLogsClient;
    private String logGroupName;
    private String logStreamName;
    private ObjectWriter logMessageWriter;
    @Setter(AccessLevel.PACKAGE)
    private boolean enabled;

    /**
     * Log message for cloud watch that gets serialized as json, so that the fields can be interpreted by cloud watch.
     */
    @Value
    @Builder
    private static class TeamNotificationLogMessage {

        String topic;
        String tenantName;
        String tenantId;
        String message;

    }

    @PostConstruct
    private void initialize() {
        enabled = awsConfig.getCloudWatch().getTeamNotification().isEnabled();
        if (!enabled) {
            log.warn("Cloud watch log team notification publisher is deactivated");
            return;
        }
        //the log stream is just the identifier of the current machine, since it is not relevant for analyzing the logs
        logStreamName = environmentService.getNodeIdentifier();
        //the log group name is relevant for any analysis
        //all log entries within a log group can be analyzed within one query, so it should contain all relevant data
        logGroupName = awsConfig.getCloudWatch().getTeamNotification().getLogGroup();
        cloudWatchLogsClient = CloudWatchLogsClient.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();

        logMessageWriter = new ObjectMapper().writerFor(TeamNotificationLogMessage.class);
    }

    @Override
    public synchronized void sendTeamNotification(String topic, Tenant tenant, TeamNotificationMessage message) {
        if (!enabled) {
            return;
        }
        InputLogEvent logEvent = InputLogEvent.builder()
                //the actual log message is send as json, so that it can be parsed by cloud watch
                .message(logMessageToJson(createLogMessage(topic, tenant, message.getText())))
                .timestamp(message.getCreated())
                .build();
        int maxRetries = 5;
        boolean success = false;
        Exception exception = null;
        for (int retry = 1; retry <= maxRetries; retry++) {
            // the sequence token is required to specify the position on where to add the log messages in the stream
            try {
                String nextSequenceToken = getSequenceTokenOrCreateLogStream(logGroupName, logStreamName);
                cloudWatchLogsClient.putLogEvents(PutLogEventsRequest.builder()
                        .logGroupName(logGroupName)
                        .logStreamName(logStreamName)
                        .logEvents(logEvent)
                        .sequenceToken(nextSequenceToken)
                        .build());
                success = true;
                break;
            } catch (Exception e) {
                //we keep the exception to log in case of failure
                exception = e;
            }
        }
        if (!success) {
            log.error("Failed to send message to CloudWatch after " + maxRetries + " retries: " + exception, exception);
        }
    }

    private String logMessageToJson(TeamNotificationLogMessage teamNotificationLogMessage) {
        try {
            return logMessageWriter.writeValueAsString(teamNotificationLogMessage);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize log message", e);
            return "failed to serialize log message: " + e.getMessage();
        }
    }

    private TeamNotificationLogMessage createLogMessage(String topic, Tenant tenant, String message) {
        return TeamNotificationLogMessage.builder()
                .tenantId(tenant != null ? tenant.getId() : null)
                .tenantName(tenant != null ? tenant.getName() : null)
                .topic(topic)
                .message(message)
                .build();
    }

    private String getSequenceTokenOrCreateLogStream(String logGroupName, String logStreamName) {

        //we already restrict the result by the prefix of the log stream
        try {
            DescribeLogStreamsResponse describeLogStreamsResponse = cloudWatchLogsClient.describeLogStreams(
                    DescribeLogStreamsRequest.builder()
                            .logGroupName(logGroupName)
                            .logStreamNamePrefix(logStreamName)
                            .build());

            return describeLogStreamsResponse.logStreams().stream()
                    .filter(logStream -> StringUtils.equals(logStream.logStreamName(), logStreamName))
                    .findAny()
                    .orElseThrow(() -> ResourceNotFoundException.builder()
                            .message("")
                            .build())
                    .uploadSequenceToken();

        } catch (ResourceNotFoundException e) {
            createLogGroupAndStream(logGroupName, logStreamName);
            //in case the log stream was newly created the upload token can be empty
            return null;
        }
    }

    private void createLogGroupAndStream(String logGroupName, String logStreamName) {
        log.debug("Creating log group {} and stream {}", logGroupName, logStreamName);
        try {
            cloudWatchLogsClient.createLogGroup(CreateLogGroupRequest.builder()
                    .logGroupName(logGroupName)
                    .tags(awsConfig.getAWSTags())
                    .build());
        } catch (ResourceAlreadyExistsException e) {
            //exceptions are not handled, since it is okay if the resource already exists
        }
        try {
            cloudWatchLogsClient.createLogStream(CreateLogStreamRequest.builder()
                    .logGroupName(logGroupName)
                    .logStreamName(logStreamName)
                    .build());
        } catch (ResourceAlreadyExistsException e) {
            //exceptions are not handled, since it is okay if the resource already exists
        }
    }

}
