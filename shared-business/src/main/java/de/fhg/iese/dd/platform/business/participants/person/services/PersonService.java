/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonLoggedInEvent;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.GeoAreaIsNoLeafException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonNotFoundException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithEmailNotFoundException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithOauthIdNotFoundException;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.Auth0ManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;

@Service
class PersonService extends BaseEntityService<Person> implements IPersonService {

    private long lastLoggedInUpdateThresholdMillis;

    @Autowired
    private ParticipantsConfig participantsConfig;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private AddressListEntryRepository addressListEntryRepository;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IRandomService randomService;
    @Autowired
    private IOauthManagementService oauthManagementService;

    @PostConstruct
    private void initialize() {
        this.lastLoggedInUpdateThresholdMillis = participantsConfig.getLastLoggedInUpdateThreshold().toMillis();
    }

    @Override
    @Transactional(readOnly = true)
    public Person findPersonById(String personId) throws PersonNotFoundException {
        if (personId == null) {
            throw PersonNotFoundException.noPersonWithId(null);
        }
        return forceHomeAreaCached(personRepository.findById(personId)
                .orElseThrow(() -> PersonNotFoundException.noPersonWithId(personId)));
    }

    @Override
    @Transactional(readOnly = true)
    public Person fetchPersonExtendedAttributes(Person person) {
        return forceHomeAreaCached(person);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void checkPersonExistsById(String personId) throws PersonNotFoundException {
        if (!personRepository.existsById(personId)) {
            throw PersonNotFoundException.noPersonWithId(personId);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> findAllPersonsById(List<String> personIds) throws PersonNotFoundException {
        if (personIds == null) {
            throw PersonNotFoundException.noPersonWithId(null);
        }

        if (CollectionUtils.isEmpty(personIds)) {
            return Collections.emptyList();
        }

        final List<Person> persons = personRepository.findAllById(personIds.stream()
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toList()));
        if (persons.size() != personIds.size()) {
            //at least one of the persons was not found
            throw new PersonNotFoundException("Not all persons could be found, ids are invalid '{}'",
                    getMissingIds(persons, personIds));
        }
        return persons;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public boolean existsPersonByEmail(String email) {
        return personRepository.existsByEmailOrPendingNewEmail(email);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public boolean existsPersonByOauthIdNotDeleted(String oauthId) {
        return personRepository.existsByOauthIdAndDeletedFalse(oauthId);
    }

    @Override
    @Transactional(readOnly=true)
    public Person findPersonByEmail(String email) throws PersonWithEmailNotFoundException {
        Optional<Person> personOpt = personRepository.findByEmail(email);
        return forceHomeAreaCached(
                personOpt.orElseThrow(() -> PersonWithEmailNotFoundException.noPersonWithEmail(email)));
    }

    @Override
    @Transactional
    public Person findCurrentlyLoggedInPerson(String oauthId)
            throws PersonWithOauthIdNotFoundException, NotAuthorizedException {
        Person person = personRepository.findByOauthId(oauthId)
                .orElseThrow(() -> PersonWithOauthIdNotFoundException.noPersonWithOauthId(oauthId));
        person = forceHomeAreaCached(person);
        if (person.isDeleted()) {
            throw new NotAuthorizedException("Person with oAuthId '{}' is deleted", oauthId);
        }
        if (person.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED)) {
            throw new NotAuthorizedException("Person with oAuthId '{}' is blocked", oauthId);
        }
        return updateLastLoginAndFirePersonLoggedIn(person);
    }

    private Person updateLastLoginAndFirePersonLoggedIn(Person person) {
        Pair<Boolean, Person> updatedPersonPair = updateLastLoggedIn(person);
        final Person updatedPerson = updatedPersonPair.getRight();
        if (updatedPersonPair.getLeft()) {
            if (participantsConfig.isDelayPersonLoggedInEvent()) {
                //the delay is required to reduce the risk that the actions triggered by this event are executed
                //in parallel on two machines
                notifyDelayed(() -> new PersonLoggedInEvent(refresh(updatedPerson)),
                        randomService.randomLong(0, 40) * 20, TimeUnit.MILLISECONDS);
            } else {
                notify(new PersonLoggedInEvent(person));
            }
        }
        return updatedPerson;
    }

    private Person forceHomeAreaCached(Person person) {
        if (person == null) {
            return null;
        }
        if (person.getHomeArea() != null) {
            person.setHomeArea(geoAreaService.forceCached(person.getHomeArea()));
        }
        return person;
    }

    @Override
    @Transactional
    public Pair<Boolean, Person> updateLastLoggedIn(final Person person) {
        final long currentTimestamp = timeService.currentTimeMillisUTC();
        if (person.getLastLoggedIn() + lastLoggedInUpdateThresholdMillis < currentTimestamp) {
            //persons that have been warned about pending deletion and log in again are set back to normal
            person.getStatuses().removeValue(PersonStatus.PENDING_DELETION);
            person.setLastLoggedIn(currentTimestamp);
            return Pair.of(true, store(person));
        } else {
            return Pair.of(false, person);
        }
    }

    @Override
    @Transactional
    public Person updateLastSentTimeEmailVerification(final Person person,
            final long lastSentTimeEmailVerification) {
        // This may cause problems. In that case keep in mind de.fhg.iese.dd.platform.datamanagement.participants.repos.PersonRepository#updateOauthId
        person.setLastSentTimeEmailVerification(lastSentTimeEmailVerification);
        return store(person);
    }

    @Override
    @Transactional
    public Pair<Boolean, Person> updateEmailAddressVerificationStatus(final Person person,
            final boolean emailAddressVerified) {

        if (emailAddressVerified) {
            // reset number of email changes
            person.setNumberOfEmailChanges(0);
            if (!person.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED)) {
                person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
                return Pair.of(true, store(person));
            }
        } else {
            person.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        }
        return Pair.of(false, store(person));
    }

    @Override
    public String getFirstAndLastName(int maxLength, Person person) {
        if (person == null) {
            return "";
        }
        String firstName = person.getFirstName();
        String lastName = person.getLastName();
        if (maxLength >= firstName.length() + 1 + lastName.length()) {
            return firstName + " " + lastName;
        }
        if (maxLength >= 3 + lastName.length()) {
            return firstName.charAt(0) + ". " + lastName;
        }
        int maxLastNameLength = Math.max(maxLength - 5, lastName.length() - 1);
        if (maxLength >= 5) {
            return firstName.charAt(0) + ". " + lastName.substring(0, maxLastNameLength) + ".";
        }
        return lastName.substring(0, maxLastNameLength);
    }

    @Override
    //Currently removed as a potential fix for the issue that the PersonCreateConfirmation is processed before this transaction is commited and thus the person is not yet existing
    //@Transactional
    public Person create(Person person) throws EMailAlreadyUsedException {
        if (existsPersonByEmail(person.getEmail())) {
            throw new EMailAlreadyUsedException(person.getEmail());
        }

        //if there is a deleted person with the same oauth id, first set a random oauth id to avoid unique key violation
        if (personRepository.existsByOauthIdAndDeletedTrue(person.getOauthId())) {
            final String randomOauthId = UUID.randomUUID().toString();
            personRepository.updateOauthId(person.getOauthId(), randomOauthId);
        }

        return store(person);
    }

    @Override
    @Transactional
    public Person updateProfilePicture(Person person, MediaItem mediaItem) {
        person.setProfilePicture(mediaItem);
        return store(person);
    }

    @Override
    @Transactional
    public Person deleteProfilePicture(Person person) {
        person.setProfilePicture(null);
        return store(person);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllNotDeleted(Pageable page) {
        return personRepository.findAllByDeletedFalse(page)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AddressListEntry> getAddressListEntries(Person person) {
        return personRepository.findAllAddressListEntriesByPerson(person).stream()
                .sorted(Comparator.comparingLong(AddressListEntry::getCreated).reversed())
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public AddressListEntry getAddressListEntryByName(Person person, String name) {
        List<AddressListEntry> personAddresses = getAddressListEntries(person);
        if (personAddresses != null && !personAddresses.isEmpty()) {
            //get the oldest matching one
            return personAddresses.stream()
                    .filter(addressListEntry -> addressListEntry.getName().equals(name))
                    .max(Comparator.comparingLong(AddressListEntry::getCreated))
                    .orElse(null);
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AddressListEntry getAddressListEntryById(Person person, String id) {
        List<AddressListEntry> personAddresses = getAddressListEntries(person);
        if(personAddresses != null && !personAddresses.isEmpty()){
            return personAddresses.stream()
                    .filter(addressListEntry -> addressListEntry.getId().equals(id))
                    .findFirst()
                    .orElse(null);
        }else{
            return null;
        }
    }

    @Override
    @Transactional
    public AddressListEntry setDefaultAddress(Person person, IAddressService.AddressDefinition defaultAddress) {
        AddressListEntry defaultAddressListEntry = getAddressListEntryByName(person, DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        if (defaultAddressListEntry != null) {
            //there is already a default addressListEntry, so we just update it
            defaultAddressListEntry.setAddress(addressService.findOrCreateAddress(defaultAddress,
                    IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                    IAddressService.GPSResolutionStrategy.LOOKEDUP_PROVIDED_NONE,
                    false));
            defaultAddressListEntry = addressListEntryRepository.saveAndFlush(defaultAddressListEntry);
            return defaultAddressListEntry;
        } else {
            //there is no default addressListEntry, we need to create it
            return addAddressListEntry(
                    person,
                    new AddressListEntry(addressService.findOrCreateAddress(defaultAddress,
                            IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                            IAddressService.GPSResolutionStrategy.LOOKEDUP_PROVIDED_NONE,
                            false),
                            DEFAULT_ADDRESS_LIST_ENTRY_NAME));
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AddressListEntry getDefaultAddressListEntry(Person person) {
        return getAddressListEntryByName(person, DEFAULT_ADDRESS_LIST_ENTRY_NAME);
    }

    @Override
    @Transactional
    public void deleteDefaultAddress(Person person) {
        Set<AddressListEntry> personAddresses = personRepository.findAllAddressListEntriesByPerson(person);
        if (!CollectionUtils.isEmpty(personAddresses)) {
            final Set<AddressListEntry> newAddressListEntries = personAddresses.stream()
                    .filter(addressListEntry -> !addressListEntry.getName().equals(DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                    .collect(Collectors.toSet());
            saveAddressListEntries(person, newAddressListEntries);
        }
    }

    @Override
    @Transactional
    public AddressListEntry addAddressListEntry(Person person, AddressListEntry entry) {
        //ensure that the address is a valid one
        Address checkedAddress = addressService.findOrCreateAddress(
                IAddressService.AddressDefinition.fromAddress(entry.getAddress()),
                IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                IAddressService.GPSResolutionStrategy.LOOKEDUP_PROVIDED_NONE,
                false);
        entry.setAddress(checkedAddress);
        entry = addressListEntryRepository.saveAndFlush(entry);
        final Set<AddressListEntry> addressListEntries = personRepository.findAllAddressListEntriesByPerson(person);
        addressListEntries.add(entry);
        saveAddressListEntries(person, addressListEntries);
        return entry;
    }

    @Override
    @Transactional
    public AddressListEntry changeAddressListEntry(Person person, AddressListEntry oldEntry, AddressListEntry newEntry)
            throws NotFoundException {
        AddressListEntry personEntry = null;
        Set<AddressListEntry> personAddresses = personRepository.findAllAddressListEntriesByPerson(person);
        if (!CollectionUtils.isEmpty(personAddresses)) {
            personEntry = personAddresses.stream()
                    .filter(addressListEntry -> addressListEntry.getId().equals(oldEntry.getId()))
                    .findFirst().orElse(null);
        }
        if (personEntry == null) {
            throw new NotFoundException("No AddressListEntry found with the given id {} for the person.",
                    oldEntry.getId());
        }
        //ensure that the address is a valid one
        Address checkedAddress = addressService.findOrCreateAddress(
                IAddressService.AddressDefinition.fromAddress(newEntry.getAddress()),
                IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE,
                false);
        personEntry.setAddress(checkedAddress);
        personEntry.setName(newEntry.getName());
        personEntry = addressListEntryRepository.saveAndFlush(personEntry);
        saveAddressListEntries(person, personAddresses);
        return personEntry;
    }

    @Override
    @Transactional
    public void deleteAddressListEntryById(Person person, String id) throws NotFoundException {
        List<AddressListEntry> personAddresses = getAddressListEntries(person);
        if (!CollectionUtils.isEmpty(personAddresses)) {

            if (personAddresses.stream().noneMatch(addressListEntry -> addressListEntry.getId().equals(id))) {
                throw new NotFoundException("No AddressListEntry found with the given id {} for the person.", id);
            }

            final Set<AddressListEntry> newAddressListEntries = personAddresses.stream()
                    .filter(addressListEntry -> !addressListEntry.getId().equals(id))
                    .collect(Collectors.toSet());
            saveAddressListEntries(person, newAddressListEntries);
        }
    }

    private void saveAddressListEntries(Person person, Set<AddressListEntry> addressListEntries) {
        person = refresh(person);
        person.setAddresses(addressListEntries);
        personRepository.save(person);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllNotDeletedBelongingToTenantIn(Set<String> tenantIds, Pageable page) {
        if (CollectionUtils.isEmpty(tenantIds)) {
            return Page.empty(page);
        }
        return personRepository.findByTenantOrHomeAreaTenantInAndDeletedFalse(tenantIds, page)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllByCaseInsensitiveInfixSearchInMainFields(String infixSearchString,
            Pageable page) {

        final String searchString = buildInfixSearch(infixSearchString);

        return personRepository.findByIdLikeOrFirstNameLikeOrLastNameLikeOrOauthIdLikeOrEmailLikeCaseInsensitive(
                        searchString,
                        searchString,
                        searchString,
                        searchString,
                        searchString,
                        page)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllByCaseInsensitiveInfixSearchInFirstNameAndLastName(
            String firstNameInfix, String lastNameInfix, Pageable page) {

        return personRepository.findByFirstNameLikeAndLastNameLikeCaseInsensitive(
                        buildInfixSearch(firstNameInfix),
                        buildInfixSearch(lastNameInfix),
                        buildInfixSearch(String.join(" ", firstNameInfix, lastNameInfix)),
                        page)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllByCaseInsensitiveInfixSearchInMainFieldsAndBelongingToTenantIn(String infixSearchString,
            Set<String> tenantIds, Pageable page) {

        final String searchString = buildInfixSearch(infixSearchString);

        return personRepository.findByHomeCommunityOrHomeAreaTenantInAndIdLikeOrFirstNameLikeOrLastNameLikeOrOauthIdLikeOrEmailLikeCaseInsensitive(
                        tenantIds,
                        searchString,
                        searchString,
                        searchString,
                        searchString,
                        searchString,
                        page)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findAllByCaseInsensitiveInfixSearchInFirstNameAndLastNameAndBelongingToTenantIn(
            String firstNameInfix, String lastNameInfix, Set<String> tenantIds, Pageable page) {

        return personRepository.findByHomeCommunityOrHomeAreaTenantInAndFirstNameLikeAndLastNameLikeCaseInsensitive(
                        tenantIds,
                        buildInfixSearch(firstNameInfix),
                        buildInfixSearch(lastNameInfix),
                        buildInfixSearch(String.join(" ", firstNameInfix, lastNameInfix)),
                        page)
                .map(this::forceHomeAreaCached);
    }

    private static String buildInfixSearch(String infixSearchString) {
        return "%" + infixSearchString + "%";
    }

    @Override
    public void checkIsValidHomeArea(GeoArea newHomeArea) throws GeoAreaIsNoLeafException {
        if (!geoAreaService.isLeaf(newHomeArea)) {
            throw GeoAreaIsNoLeafException.forHomeAreaIsInvalid(newHomeArea);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findInactivePersonsWithoutStatusWithVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus forbiddenStatus, PersonVerificationStatus requiredVerificationStatus,
            int lastLoggedInDays, int maxNumPersons) {

        long lastLoginThreshold = timeService.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(lastLoggedInDays);

        return findAndFilter(
                isExcluded,
                pageable -> personRepository.findByLastLoggedInBeforeWithoutStatusWithVerificationStatus(
                        forbiddenStatus.getBitMaskValue(),
                        requiredVerificationStatus.getBitMaskValue(),
                        lastLoginThreshold,
                        pageable),
                maxNumPersons)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findInactivePersonsWithStatusWithoutVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus requiredStatus, PersonVerificationStatus forbiddenVerificationStatus, int lastLoggedInDays,
            int maxNumPersons) {

        long lastLoginThreshold = timeService.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(lastLoggedInDays);

        return findAndFilter(
                isExcluded,
                pageable -> personRepository.findByLastLoggedInBeforeWithStatusWithoutVerificationStatus(
                        requiredStatus.getBitMaskValue(),
                        forbiddenVerificationStatus.getBitMaskValue(),
                        lastLoginThreshold,
                        pageable),
                maxNumPersons)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findInactivePersonsWithoutStatusWithoutVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus forbiddenStatus, PersonVerificationStatus forbiddenVerificationStatus, int lastLoggedInDays,
            int maxNumPersons) {

        long lastLoginThreshold = timeService.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(lastLoggedInDays);

        return findAndFilter(
                isExcluded,
                pageable -> personRepository.findByLastLoggedInBeforeWithoutStatusWithoutVerificationStatus(
                        forbiddenStatus.getBitMaskValue(),
                        forbiddenVerificationStatus.getBitMaskValue(),
                        lastLoginThreshold,
                        pageable),
                maxNumPersons)
                .map(this::forceHomeAreaCached);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Person> findPersonsWarnedForDaysAndNotDeleted(Predicate<Person> isExcluded, PersonStatus requiredStatus,
            int warnedForDays, int maxNumPersons) {

        long warningThreshold = timeService.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(warnedForDays);

        return findAndFilter(
                isExcluded,
                pageable -> personRepository.findAllByStatusAndPendingDeletionWarningBeforeAndNotDeletedOrderByPendingDeletionWarningAsc(
                        requiredStatus.getBitMaskValue(), warningThreshold, pageable),
                maxNumPersons)
                .map(this::forceHomeAreaCached);
    }

    private <T extends BaseEntity> Page<T> findAndFilter(Predicate<T> isExcluded,
            Function<Pageable, Page<T>> repositoryCall, int maxNumEntities) {
        List<T> entities = new ArrayList<>(maxNumEntities);
        long total = 0;
        int excludedEntities = 0;

        //we query multiple queries until we have enough entities that are not excluded
        int maxQueries = 4;
        for (int i = 0; i < maxQueries; i++) {
            Page<T> queryResult =
                    // for subsequent calls we fetch too many, but reducing the page size here might result in
                    // more queries if the isExcluded patterns matches all of the result
                    repositoryCall.apply(PageRequest.of(i, maxNumEntities));
            total = queryResult == null ? 0 : queryResult.getTotalElements();
            if (queryResult == null || queryResult.isEmpty()) {
                return toPage(maxNumEntities, entities, total);
            }
            for (T entity : queryResult) {
                if (isExcluded.test(entity)) {
                    excludedEntities++;
                } else {
                    //the entity is not excluded and can be added
                    entities.add(entity);
                    //we have enough entities
                    if (entities.size() == maxNumEntities) {
                        return toPage(maxNumEntities, entities, total);
                    }
                }
            }
            //we already reached the end of all matching entities, an additional query would not return any entities
            if (queryResult.getNumberOfElements() < maxNumEntities) {
                return toPage(maxNumEntities, entities, total);
            }
        }
        log.error("Too many excluded entities ({}) after {} queries, returning only {} of {} entities",
                excludedEntities, maxQueries, entities.size(), maxNumEntities);
        return toPage(maxNumEntities, entities, total);
    }

    private <T extends BaseEntity> PageImpl<T> toPage(int maxNumEntities, List<T> entities, long total) {
        //relative to the overall request we are always on page 0
        return new PageImpl<>(entities, PageRequest.of(0, maxNumEntities), total);
    }

    @Override
    @Transactional
    public Person changePersonStatuses(Person person, Set<PersonStatus> statusesToAdd,
            Set<PersonStatus> statusesToRemove) {

        if (CollectionUtils.isEmpty(statusesToAdd) && CollectionUtils.isEmpty(statusesToRemove)) {
            return person;
        }
        // add all statusesToAdd
        statusesToAdd.forEach(person.getStatuses()::addValue);
        // remove all statusesToRemove
        statusesToRemove.forEach(person.getStatuses()::removeValue);
        return store(person);
    }

    @Override
    @Transactional
    public Person changeEmailAddress(Person person, String newEmail) {
        final String oldEmail = person.getEmail();
        boolean oldEmailVerified = person.isEmailVerified();

        oauthManagementService.changeEmail(person.getOauthId(), newEmail, false);

        try {
            person.setPendingNewEmail(null);
            person.setEmail(newEmail);
            person = store(person);
            log.debug("Changed email address of person '{}'", person.getId());
            return person;
        } catch (Exception e) {
            try {
                // Saving the new email in backend has failed, so we change it back to oldEmail
                oauthManagementService.changeEmail(person.getOauthId(), oldEmail, oldEmailVerified);
            } catch (Auth0ManagementException ex) {
                throw EMailChangeNotPossibleException.forInconsistentState(oldEmail, newEmail);
            }
            log.error("Unexpected exception while changing email of user " + person.getId(), e);
            throw EMailChangeNotPossibleException.forUnknownReason(newEmail);
        }
    }

    @Override
    public Person store(Person person) {
        return forceHomeAreaCached(personRepository.saveAndFlush(person));
    }

}
