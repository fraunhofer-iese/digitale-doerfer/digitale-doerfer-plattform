/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.web.client.RestTemplate;

import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.AccessLevel;
import lombok.Getter;

public abstract class BaseHttpTeamNotificationPublisher implements ITeamNotificationPublisher {

    private static final long MAX_RETRY_TIME_MILLIS = TimeUnit.HOURS.toMillis(6);
    private static final long MAX_SHUTDOWN_WAIT_TIME_MILLIS = TimeUnit.SECONDS.toMillis(2);
    private static final int HTTP_TIMEOUT = Math.toIntExact(TimeUnit.SECONDS.toMillis(10L));

    protected final Logger log = LogManager.getLogger(this.getClass());

    protected final ITimeService timeService;

    @Getter
    private final String name;

    @Getter(AccessLevel.PACKAGE)
    protected final BlockingDeque<TeamNotificationSendRequest> requestQueue;
    protected final ExecutorService requestExecutor;
    private final AtomicBoolean shuttingDown;
    private boolean sending;

    @Getter(AccessLevel.PROTECTED)
    protected RestTemplate restTemplate;

    protected BaseHttpTeamNotificationPublisher(ITimeService timeService, RestTemplateBuilder restTemplateBuilder,
            TeamNotificationConnection connection) {
        this.name =
                ITeamNotificationPublisher.super.getName() + "-" + (connection == null ? "?" : connection.getName());
        this.timeService = timeService;
        this.shuttingDown = new AtomicBoolean(false);
        this.requestQueue = new LinkedBlockingDeque<>(1000);
        this.requestExecutor = Executors.newSingleThreadExecutor(new CustomizableThreadFactory(getName()));
        this.restTemplate = restTemplateBuilder
                .additionalMessageConverters(new StringHttpMessageConverter(StandardCharsets.UTF_8))
                .requestFactory(this::clientHttpRequestFactory)
                .build();
        sending = false;
        requestExecutor.submit(this::sendingRunnable);
    }

    private void sendingRunnable() {
        mainLoop:
        while (!Thread.currentThread().isInterrupted()) {
            int failCount = 0;
            TeamNotificationSendRequest request;
            try {
                request = requestQueue.takeFirst();
                sending = true;
                while (sending) {
                    try {
                        processSendRequest(request);
                        sending = false;
                    } catch (InterruptedException interruptedException) {
                        break mainLoop;
                    } catch (Exception e) {
                        log.debug("Error while sending {}", e.toString());
                        try {
                            if (!waitUntilRetry(request.getMessage().getCreated(), failCount++)) {
                                log.error("Could not send message created at {} to team, " +
                                                "finally timed out with last exception: {}",
                                        request.getMessage().getCreated(),
                                        e.toString());
                                sending = false;
                            }
                        } catch (InterruptedException interruptedException) {
                            //interrupted while waiting for the next retry
                            break mainLoop;
                        }
                    }
                }
            } catch (InterruptedException e) {
                break;
            }
        }
        sending = false;
    }

    protected boolean waitUntilRetry(long created, int failCount) throws InterruptedException {
        if ((created + MAX_RETRY_TIME_MILLIS) <= timeService.currentTimeMillisUTC()) {
            return false;
        }
        if (failCount < 3) {
            Thread.sleep(10);
        }
        if (failCount < 10) {
            Thread.sleep(failCount * 3L);
        }
        if (failCount < 20) {
            Thread.sleep(failCount * 10L);
        }
        if (failCount < 30) {
            Thread.sleep(failCount * 20L);
        }
        Thread.sleep(TimeUnit.MINUTES.toSeconds(10L));
        return true;
    }

    @Override
    public void sendTeamNotification(TeamNotificationSendRequest request) {
        if (shuttingDown.get()) {
            throw new RuntimeException("Publisher " + getName() + " is shutting down");
        }
        boolean inQueue = requestQueue.offerLast(request);
        if (!inQueue) {
            throw new RuntimeException("Failed to send message via " + getName() + ", waiting queue is full");
        }
    }

    protected abstract void processSendRequest(TeamNotificationSendRequest request) throws Exception;

    private ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(HTTP_TIMEOUT);
        factory.setConnectTimeout(HTTP_TIMEOUT);
        return factory;
    }

    protected HttpHeaders getJsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Override
    public void shutdownAndWait() {
        if (shuttingDown.compareAndSet(false, true)) {
            log.debug("Shutting down {}...", getName());
            int waitInterval = 150;
            for (int totalWaitTime = 0; totalWaitTime < MAX_SHUTDOWN_WAIT_TIME_MILLIS; totalWaitTime += waitInterval) {
                if (getRequestQueue().isEmpty() && !sending) {
                    break;
                }
                try {
                    Thread.sleep(waitInterval);
                } catch (InterruptedException e) {
                    break;
                }
            }
            requestExecutor.shutdown();
            log.debug("Shut down {}", getName());
        }
    }

}
