/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import java.util.function.BiFunction;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.shared.push.exceptions.ExternalPushProviderException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import software.amazon.awssdk.services.pinpoint.PinpointClient;
import software.amazon.awssdk.services.pinpoint.model.NotFoundException;
import software.amazon.awssdk.services.pinpoint.model.PinpointException;
import software.amazon.awssdk.services.pinpoint.model.PinpointRequest;
import software.amazon.awssdk.services.pinpoint.model.PinpointResponse;

abstract class PinpointBaseProvider {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @FunctionalInterface
    protected interface PushLogConsumer<T> {

        void logPushRequest(T t, PinpointException ex, long duration);

    }

    @Autowired
    protected AWSConfig awsConfig;

    protected PinpointClient pinpointClient;

    @PostConstruct
    protected void init() {
        pinpointClient = PinpointClient.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();
    }

    /**
     * Calls {@link PinpointClient} with a given request and returns the results.
     *
     * @throws ExternalPushProviderException if the calls fails.
     */
    protected <R extends PinpointResponse, P extends PinpointRequest> R
    callPinpoint(BiFunction<PinpointClient, P, R> biConsumer, P request) throws ExternalPushProviderException {
        return callPinpoint(biConsumer, request, null);
    }

    /**
     * Calls {@link PinpointClient} with a given request and returns the results. Ignores {@link NotFoundException} and
     * returns null in this case.
     *
     * @throws ExternalPushProviderException if the calls fails.
     */
    protected <R extends PinpointResponse, P extends PinpointRequest> R
    callPinpointIgnoreNotFound(BiFunction<PinpointClient, P, R> biConsumer, P request)
            throws ExternalPushProviderException {
        return callPinpointIgnoreNotFound(biConsumer, request, null);
    }

    /**
     * Calls {@link PinpointClient} with a given request and returns the results. Logs the call with the log consumer.
     * Ignores {@link NotFoundException} and returns null in this case.
     *
     * @throws ExternalPushProviderException if the calls fails.
     */
    protected <R extends PinpointResponse, P extends PinpointRequest> R
    callPinpointIgnoreNotFound(BiFunction<PinpointClient, P, R> biConsumer, P request, PushLogConsumer<R> logConsumer)
            throws ExternalPushProviderException {

        long start = System.currentTimeMillis();
        try {
            return executeRequest(biConsumer, request, logConsumer, start);
        } catch (NotFoundException ex) {
            long duration = System.currentTimeMillis() - start;
            if (logConsumer != null) {
                logConsumer.logPushRequest(null, ex, duration);
            }
            return null;
        } catch (PinpointException ex) {
            long duration = System.currentTimeMillis() - start;
            if (logConsumer != null) {
                logConsumer.logPushRequest(null, ex, duration);
            }
            throw new ExternalPushProviderException("AWS Pinpoint API call failed", ex);
        }
    }

    /**
     * Calls {@link PinpointClient} with a given request and returns the results. Logs the call with the log consumer.
     *
     * @throws ExternalPushProviderException if the calls fails.
     */
    protected <R extends PinpointResponse, P extends PinpointRequest> R
    callPinpoint(BiFunction<PinpointClient, P, R> biConsumer, P request, PushLogConsumer<R> logConsumer)
            throws ExternalPushProviderException {

        long start = System.currentTimeMillis();
        try {
            return executeRequest(biConsumer, request, logConsumer, start);
        } catch (PinpointException ex) {
            long duration = System.currentTimeMillis() - start;
            if (logConsumer != null) {
                logConsumer.logPushRequest(null, ex, duration);
            }
            throw new ExternalPushProviderException("AWS Pinpoint API call failed", ex);
        }
    }

    private <R extends PinpointResponse, P extends PinpointRequest> R
    executeRequest(BiFunction<PinpointClient, P, R> biConsumer, P request, PushLogConsumer<R> logConsumer, long start)
            throws PinpointException {

        final R result = biConsumer.apply(pinpointClient, request);

        long duration = System.currentTimeMillis() - start;
        int statusCode = result.sdkHttpResponse().statusCode();

        if (log.isTraceEnabled()) {
            log.trace("AWS Pinpoint result {} in {} ms: {}", statusCode, duration, result);
        }

        if (logConsumer != null) {
            logConsumer.logPushRequest(result, null, duration);
        }

        return result;
    }

    protected boolean pushConfigurationInvalid(AppVariant appVariant) {
        if (StringUtils.isEmpty(appVariant.getPushAppId())) {
            if (appVariant.isSupportsAndroid() || appVariant.isSupportsIos()) {
                log.warn("AppVariant {} should support Android or Ios, but does not provide push configuration. " +
                                "Check if AppVariantPushConfiguration is provided and data init is executed.",
                        appVariant.getAppVariantIdentifier());
            }
            return true;
        }
        return false;
    }

}
