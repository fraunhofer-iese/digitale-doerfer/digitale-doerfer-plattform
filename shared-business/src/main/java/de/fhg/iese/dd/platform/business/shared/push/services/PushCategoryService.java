/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotConfigurableException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotFoundException;
import de.fhg.iese.dd.platform.business.shared.push.plugin.IPushCategoryCustomization;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class PushCategoryService extends BaseService implements IPushCategoryService {

    private Map<String, PushCategory> pushCategoryById = Collections.emptyMap();

    @Autowired
    private PushCategoryRepository pushCategoryRepository;
    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;
    @Autowired
    private IPluginService pluginService;
    @Autowired
    private CacheCheckerAtOnceCache cacheCheckerPushCategories;

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return ICachingComponent.CacheConfiguration.builder()
                .cachedEntity(PushCategory.class)
                .build();
    }

    @Override
    public PushCategory findPushCategoryById(String categoryId) throws PushCategoryNotFoundException {

        checkAndRefreshPushCategoryCache();

        final PushCategory pushCategory = pushCategoryById.get(categoryId);
        if (pushCategory == null) {
            throw new PushCategoryNotFoundException(categoryId);
        }
        return pushCategory;
    }

    @Override
    public PushCategory findPushCategoryById(PushCategory.PushCategoryId categoryId)
            throws PushCategoryNotFoundException {

        return findPushCategoryById(categoryId.getId());
    }

    @Override
    public Set<PushCategory> findAllPushCategoriesForApp(App app) {

        checkAndRefreshPushCategoryCache();

        return pushCategoryById.values().stream()
                .filter(pc -> pc.getApp().equals(app))
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<PushCategory> findPushCategoriesBySubject(String pushCategorySubject) {
        if (StringUtils.isEmpty(pushCategorySubject)) {
            throw new PushCategoryNotFoundException(pushCategorySubject);
        }
        checkAndRefreshPushCategoryCache();

        return pushCategoryById.values().stream()
                .filter(pc -> !StringUtils.isEmpty(pc.getSubject()) && pc.getSubject().equals(pushCategorySubject))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public LogSummary deletePushCategory(PushCategory pushCategory) {

        LogSummary logSummary = new LogSummary(log);
        logSummary.info("Starting deleting {} and all related entities", pushCategory);
        logSummary.info("Deleting push category user settings");
        logSummary.indent();

        List<PushCategoryUserSetting> userSettings =
                pushCategoryUserSettingRepository.findAllByPushCategoryOrderByCreatedDesc(pushCategory);
        logSummary.info("Deleting {} settings", userSettings.size());
        pushCategoryUserSettingRepository.deleteAll(userSettings);

        logSummary.outdent();
        logSummary.info("Deleting push category itself");
        logSummary.indent();

        pushCategoryRepository.delete(pushCategory);

        logSummary.outdent();
        logSummary.info("Finished deleting {} and all related entities", pushCategory);

        return logSummary;
    }

    @Override
    @Transactional
    public PushCategory storePushCategory(PushCategory pushCategory) {
        return pushCategoryRepository.save(pushCategory);
    }

    @Override
    public PushCategoryUserSetting findPushCategoryUserSetting(PushCategory pushCategory, Person person,
            AppVariant appVariant) {

        return query(
                () -> pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(pushCategory, person,
                        appVariant));
    }

    @Override
    public Pair<PushCategoryUserSetting, List<PushCategoryUserSetting>> changePushCategoryUserSetting(
            PushCategory parentCategory, Person person, AppVariant appVariant, boolean loudPushEnabled)
            throws PushCategoryNotConfigurableException {

        if (!parentCategory.isConfigurable()) {
            throw new PushCategoryNotConfigurableException("Category '{}' is not configurable.",
                    parentCategory.getId());
        }
        checkAndRefreshPushCategoryCache();

        //get the dependent configurable child categories, too
        List<PushCategory> childCategories = pushCategoryById.values().stream()
                .filter(PushCategory::isConfigurable)
                .filter(pc -> pc.getParentCategory() != null && pc.getParentCategory().equals(parentCategory))
                .sorted(Comparator.comparing(PushCategory::getOrderValue))
                .collect(Collectors.toList());
        List<PushCategory> categoriesToAdjust = new ArrayList<>(childCategories.size() + 1);
        categoriesToAdjust.add(parentCategory);
        categoriesToAdjust.addAll(childCategories);
        List<PushCategory> customizedCategoriesToAdjust =
                customizePushCategories(person, appVariant, categoriesToAdjust);
        PushCategory customizedParentCategory =
                customizedCategoriesToAdjust.stream().filter(parentCategory::equals).findFirst()
                        //this can happen if the customization removes the parent category
                        //in this case we use the un-customized category, since the user had seen it and wants to change it
                        .orElse(parentCategory);
        //find all user settings for these categories
        Set<PushCategoryUserSetting> existingSettings =
                query(() -> pushCategoryUserSettingRepository.findAllByPushCategoryInAndPersonAndAppVariant(
                        categoriesToAdjust,
                        person,
                        appVariant));
        //the idea is to prepare the update that needs to be done and write it down in a single step
        Map<PushCategory, PushCategoryUserSetting> existingSettingByCategory = existingSettings.stream()
                //there is always at maximum one setting by category, there is a constraint to prevent multiples
                .collect(Collectors.toMap(PushCategoryUserSetting::getPushCategory, Function.identity()));

        Collection<PushCategoryUserSetting> settingsToSave = new ArrayList<>(categoriesToAdjust.size());
        Collection<PushCategoryUserSetting> settingsToDelete = new ArrayList<>(categoriesToAdjust.size());
        List<PushCategoryUserSetting> changedChildSettingsToReturn = new ArrayList<>(categoriesToAdjust.size());
        for (PushCategory category : customizedCategoriesToAdjust) {
            PushCategoryUserSetting existingSetting = existingSettingByCategory.get(category);
            boolean changed = false;
            if (existingSetting == null) {
                //there are no settings, no need to adjust them
                if (category.isDefaultLoudPushEnabled() != loudPushEnabled) {
                    // the user setting is only necessary if is is not the default setting
                    // a new setting is required, save the setting
                    changed = true;
                    settingsToSave.add(PushCategoryUserSetting.builder()
                            .person(person)
                            .appVariant(appVariant)
                            .pushCategory(category)
                            .loudPushEnabled(loudPushEnabled)
                            .build());
                }
            } else {
                //there are settings, adjust them
                if (category.isDefaultLoudPushEnabled() == loudPushEnabled) {
                    //the user setting is not necessary because it is the default setting, we need to delete it
                    changed = true;
                    settingsToDelete.add(existingSetting);
                } else {
                    //a setting is required, save the setting if it changed
                    if (existingSetting.isLoudPushEnabled() != loudPushEnabled) {
                        //the setting changed, which is actually due to an invalid state, there should not be a setting before
                        changed = true;
                        existingSetting.setLoudPushEnabled(loudPushEnabled);
                        settingsToSave.add(existingSetting);
                    }
                }
            }
            //only return changed child categories that are visible
            if (!parentCategory.equals(category) && changed && category.isVisible()) {
                //this setting is only required as result for the clients
                changedChildSettingsToReturn.add(PushCategoryUserSetting.builder()
                        .id(null)
                        .person(person)
                        .pushCategory(category)
                        .appVariant(appVariant)
                        .loudPushEnabled(loudPushEnabled)
                        .build());
            }
        }
        // write them down to db at once and ignore any exceptions
        // would mean that there was a parallel save, so the changes of this request do not matter
        // the result of the request are synthetic settings anyway
        saveAndIgnoreIntegrityViolation(
                () -> {
                    pushCategoryUserSettingRepository.deleteAll(settingsToDelete);
                    pushCategoryUserSettingRepository.saveAll(settingsToSave);
                    return settingsToSave;
                },
                () -> settingsToSave);
        return Pair.of(
                PushCategoryUserSetting.builder()
                        .id(null)
                        .person(person)
                        .pushCategory(customizedParentCategory)
                        .appVariant(appVariant)
                        .loudPushEnabled(loudPushEnabled)
                        .build(),
                changedChildSettingsToReturn);
    }

    @Override
    public List<PushCategoryUserSetting> getConfigurableAndVisiblePushCategoryUserSettings(Person person,
            AppVariant appVariant) {

        checkAndRefreshPushCategoryCache();

        //get all configurable and visible categories for that app
        final App app = appVariant.getApp();
        List<PushCategory> categories = pushCategoryById.values().stream()
                .filter(PushCategory::isConfigurable)
                .filter(PushCategory::isVisible)
                .filter(pc -> pc.getApp().equals(app))
                .sorted(Comparator.comparing(PushCategory::getOrderValue))
                .collect(Collectors.toList());

        List<PushCategory> customizedPushCategories = customizePushCategories(person, appVariant, categories);

        //get all configurations for that person and app as map pushCategory -> setting
        Map<PushCategory, PushCategoryUserSetting> actualUserSettings = query(() ->
                pushCategoryUserSettingRepository
                        .findAllByPersonAndAppVariantOrderByCreatedDesc(person, appVariant).stream()
                        .collect(Collectors.toMap(PushCategoryUserSetting::getPushCategory, Function.identity())));

        List<PushCategoryUserSetting> actualAndVirtualUserSettings = new ArrayList<>(customizedPushCategories.size());

        for (int i = 0; i < customizedPushCategories.size(); i++) {
            PushCategory category = customizedPushCategories.get(i);
            //try to get the setting for this category
            PushCategoryUserSetting userSetting = actualUserSettings.get(category);
            if (userSetting == null) {
                //we create a virtual user setting that should not be persisted
                //such entity is only needed to present the client a consistent view on the push categories
                userSetting = PushCategoryUserSetting.builder()
                        .id(null)
                        .pushCategory(category)
                        .person(person)
                        .loudPushEnabled(category.isDefaultLoudPushEnabled())
                        .build();
            } else {
                //this is important to keep the customizations of the push categories
                userSetting.setPushCategory(category);
            }
            actualAndVirtualUserSettings.add(i, userSetting);
        }

        return actualAndVirtualUserSettings;
    }

    private List<PushCategory> customizePushCategories(Person person, AppVariant appVariant,
            List<PushCategory> categories) {

        final List<IPushCategoryCustomization> pushCategoryCustomizations = pluginService.getPluginInstancesRaw(
                PluginTarget.of(IPushCategoryCustomization.class));

        List<PushCategory> categoriesCopy = categories.stream()
                .map(PushCategory::createShallowCopy)
                .collect(Collectors.toList());

        //customize push categories, if certain features are turned off
        for (IPushCategoryCustomization pushCategoryCustomization : pushCategoryCustomizations) {
            categoriesCopy = pushCategoryCustomization.customizePushCategories(categoriesCopy, person, appVariant);
        }
        return categoriesCopy;
    }

    @Override
    public List<PushCategoryUserSetting> findAllPushCategoryUserSettings(Person person) {
        return query(() -> pushCategoryUserSettingRepository.findByPersonOrderByCreatedDesc(person));
    }

    @Override
    public Map<Boolean, Collection<Person>> filterPersonsWithPushCategoryUserSetting(PushCategory pushCategory,
            Collection<Person> persons, Collection<AppVariant> appVariants) {

        if (CollectionUtils.isEmpty(persons)) {
            return Collections.emptyMap();
        }
        if (CollectionUtils.isEmpty(appVariants)) {
            return Collections.singletonMap(pushCategory.isDefaultLoudPushEnabled(), persons);
        }
        final List<Pair<String, Boolean>> personIdAndLoudPush =
                query(() -> pushCategoryUserSettingRepository
                        .findLoudPushEnabledAndPersonIdByCategoryAndPersonAndAppVariant(pushCategory, persons,
                                appVariants));

        //the lists are ordered by created timestamp of the setting
        final Map<String, List<Boolean>> loudPushByPersonId = personIdAndLoudPush.stream()
                .collect(Collectors.groupingBy(Pair::getLeft,
                        Collectors.mapping(Pair::getRight, Collectors.toList())));

        return persons.stream()
                .collect(Collectors.groupingBy(
                        p -> getFirstOrDefault(loudPushByPersonId.get(p.getId()),
                                pushCategory.isDefaultLoudPushEnabled()),
                        //since we want a collection as output we have to specify the collector that way
                        Collectors.toCollection(ArrayList::new)));
    }

    private static boolean getFirstOrDefault(List<Boolean> list, Boolean defaultValue) {
        if (CollectionUtils.isEmpty(list)) {
            return defaultValue;
        } else {
            return list.get(0);
        }
    }

    @Override
    public void invalidateCache() {
        cacheCheckerPushCategories.resetCacheChecks();
    }

    private void checkAndRefreshPushCategoryCache() {
        cacheCheckerPushCategories.refreshCacheIfStale(() -> {
            pushCategoryById = pushCategoryRepository.findAll().stream()
                    .collect(Collectors.toMap(PushCategory::getId, Function.identity()));
            // replace parent with cached push category
            pushCategoryById.values().stream()
                    .filter(pc -> pc.getParentCategory() != null)
                    .forEach(pc -> {
                        final PushCategory cachedParent = pushCategoryById.get(pc.getParentCategory().getId());
                        pc.setParentCategory(cachedParent);
                    });
        });
    }

}
