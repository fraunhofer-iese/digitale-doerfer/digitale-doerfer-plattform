/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.reflection.services;

import java.util.Map;

import de.fhg.iese.dd.platform.business.framework.reflection.exceptions.ReflectionException;
import de.fhg.iese.dd.platform.business.framework.services.IService;

public interface IReflectionService extends IService {

    /**
     * Extracts the module name by using "de.fhg.iese.dd.platform.business._module_.init.Clazz"
     *
     * @param clazz the class to extract the module name
     *
     * @return module name of a class that adheres to the given pattern
     */
    String getModuleName(Class<?> clazz);

    /**
     * Extracts the module name by using "de.fhg.iese.dd.platform.business._module_.init.Clazz"
     *
     * @param className the fully qualified class name to extract the module name
     *
     * @return module name of a class that adheres to the given pattern
     */
    String getModuleName(String className);

    /**
     * Lookup the value of a constant by its fully qualified name, e.g. {@code my.package.MyClass.MY_CONSTANT}. The
     * value has to be {@code public static final}. The lookup works across modules.
     *
     * @param <T>                expected type of the value
     * @param fullyQualifiedName the fully qualified name of the constant
     * @param expectedType       expected type of the value
     *
     * @return the value found in the constant
     *
     * @throws ReflectionException if the field did not exist, could not be accessed or the expected type was wrong.
     */
    <T> T findConstantByFullyQualifiedName(String fullyQualifiedName, Class<T> expectedType) throws
            ReflectionException;

    Map<String, Object> reflectMetamodel(Class<?> classToInspect, int depth, String... propertiesToIgnore);

    /**
     * Extract the values of the properties of the given object and exclude the properties with the given name.
     * <li>properties == type BaseEntity: IDs of entities</li>
     * <li>properties != type BaseEntity: value of the property</li>
     *
     * @param object            the object to extract the properties from
     * @param excludeProperties name of the properties to exclude
     *
     * @return sorted map with property name -> property value according to the rules described above
     *
     * @throws ReflectionException if the object could not be analyzed
     */
    Map<String, Object> extractPropertyValues(Object object, String... excludeProperties) throws ReflectionException;

}
