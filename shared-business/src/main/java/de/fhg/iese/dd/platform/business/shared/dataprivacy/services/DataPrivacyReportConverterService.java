/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyProcessingException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormat;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormatVisitor;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyTemplate;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.template.services.ITemplateService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

@Service
class DataPrivacyReportConverterService implements IDataPrivacyReportConverterService {

    @Autowired
    private ITemplateService templateService;

    private ObjectWriter jsonWriter;

    @Override
    public String convert(IDataPrivacyReport privacyReport, DataPrivacyReportFormat format) {
        return format.accept(new DataPrivacyReportFormatVisitor<>() {
            @Override
            public String whenHtml() {
                try {
                    return templateService.createTextWithTemplateObjectModel(
                            DataPrivacyTemplate.DATA_PRIVACY_REPORT_HTML,
                            privacyReport);
                } catch (Exception e) {
                    throw new DataPrivacyProcessingException("Could not use template", e);
                }
            }

            @Override
            public String whenText() {
                try {
                    return templateService.createTextWithTemplateObjectModel(
                            DataPrivacyTemplate.DATA_PRIVACY_REPORT_TEXT,
                            privacyReport);
                } catch (Exception e) {
                    throw new DataPrivacyProcessingException("Could not use template", e);
                }
            }

            @Override
            public String whenJson() {
                try {
                    return getJsonWriter().writeValueAsString(privacyReport);
                } catch (Exception e) {
                    throw new DataPrivacyProcessingException("Could not serialize report to JSON", e);
                }
            }
        });
    }

    @Override
    public String convertToJson(IPersonDeletionReport deletionReport) {
        try {
            return getJsonWriter().writeValueAsString(deletionReport.getDeletedEntities());
        } catch (JsonProcessingException e) {
            throw new DataPrivacyProcessingException("Could not serialize report to JSON", e);
        }
    }

    /**
     * Custom json writer that does not serialize {@link BaseEntity}s, only their id.
     */
    private ObjectWriter getJsonWriter() {
        if (jsonWriter == null) {
            final ObjectMapper objectMapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addSerializer(BaseEntity.class, new BaseEntityIdSerializer(BaseEntity.class));
            objectMapper.registerModule(module);
            jsonWriter = objectMapper.writerWithDefaultPrettyPrinter();
        }
        return jsonWriter;
    }

    /**
     * This serializer only writes the id of the entity, instead of the complete entity. This behavior is very important
     * if entities are included in the data privacy report, because they should never by published completely.
     */
    private static class BaseEntityIdSerializer extends StdSerializer<BaseEntity> {

        private static final long serialVersionUID = -8467185912583910993L;

        public BaseEntityIdSerializer(Class<BaseEntity> t) {
            super(t);
        }

        @Override
        public void serialize(BaseEntity value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeStartObject();
            gen.writeStringField("id", value.getId());
            gen.writeEndObject();
        }

    }

}
