/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Johannes Schneider, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.Signature;
import lombok.AllArgsConstructor;
import lombok.Getter;

public class DataPrivacyReport implements IDataPrivacyReport {

    @Getter
    private final String title;

    @Getter
    private final List<IDataPrivacyReportSection> sections;

    @Getter
    private static class DataPrivacyReportSection implements IDataPrivacyReportSection {

        private final String name;
        private final String description;
        private final List<IDataPrivacyReportSubSection> subSections;

        private DataPrivacyReportSection(String name, String description) {
            super();
            this.name = name;
            this.description = description;
            this.subSections = new LinkedList<>();
        }

        @Override
        public synchronized IDataPrivacyReportSubSection newSubSection(String name, String description) {
            DataPrivacyReportSubSection result = new DataPrivacyReportSubSection(name, description);
            subSections.add(result);
            return result;
        }

        @Override
        public boolean isEmpty() {
            return CollectionUtils.isEmpty(subSections);
        }

    }

    @Getter
    private static class DataPrivacyReportSubSection implements IDataPrivacyReportSubSection {

        private final String name;
        private final String description;
        private final List<IDataPrivacyReportContent<?>> contents;

        private DataPrivacyReportSubSection(String name, String description) {
            super();
            this.name = name;
            this.description = description;
            this.contents = new LinkedList<>();
        }

        @Override
        public IDataPrivacyReportContent<String> newContent(String name, String content) {
            return newContent(name, null, content);
        }

        @Override
        public synchronized IDataPrivacyReportContent<String> newContent(String name, String description, String content) {
            if(StringUtils.isEmpty(content)){
                return null;
            }
            IDataPrivacyReportContent<String> result = new DataPrivacyReportContent<>(name, description, content);
            contents.add(result);
            return result;
        }

        @Override
        public IDataPrivacyReportContent<MediaItem> newImageContent(String name, MediaItem mediaItem) {
            return newImageContent(name, null, mediaItem);
        }

        @Override
        public synchronized IDataPrivacyReportContent<MediaItem> newImageContent(String name, String description,
                MediaItem mediaItem) {
            if (mediaItem == null) {
                return null;
            }
            IDataPrivacyReportContent<MediaItem> result =
                    new DataPrivacyReportContentImage(name, description, mediaItem);
            contents.add(result);
            return result;
        }

        @Override
        public IDataPrivacyReportContent<DocumentItem> newDocumentContent(String name, String description,
                DocumentItem documentItem) {
            if (documentItem == null) {
                return null;
            }
            IDataPrivacyReportContent<DocumentItem> result =
                    new DataPrivacyReportContentDocument(name, description, documentItem);
            contents.add(result);
            return result;
        }

        @Override
        public IDataPrivacyReportContent<GPSLocation> newLocationContent(String name, String description,
                GPSLocation location) {
            if (location == null) {
                return null;
            }
            IDataPrivacyReportContent<GPSLocation> result =
                    new DataPrivacyReportContentLocation(name, description, location);
            contents.add(result);
            return result;
        }

        @Override
        public IDataPrivacyReportContent<Signature> newSignatureContent(String name, Signature signature) {
            return newSignatureContent(name, null, signature);
        }

        @Override
        public IDataPrivacyReportContent<Signature> newSignatureContent(String name, String description,
                Signature signature) {
            if (signature == null || Strings.isNullOrEmpty(signature.getSignature())) {
                return null;
            }
            IDataPrivacyReportContent<Signature> result = new DataPrivacyReportContentSignature(name, null, signature);
            contents.add(result);
            return result;
        }

    }

    @Getter
    @AllArgsConstructor
    private static class DataPrivacyReportContent<T> implements IDataPrivacyReportContent<T> {

        private final String name;
        private final String description;
        private final T content;

        @Override
        public String getContentClass() {
            return content.getClass().getSimpleName();
        }

    }

    @Getter
    @AllArgsConstructor
    private static class DataPrivacyReportContentImage implements IDataPrivacyReportContent<MediaItem> {

        private final String name;
        private final String description;
        private final MediaItem content;

        @Override
        public String getContentClass() {
            return "MediaItem";
        }

    }

    @Getter
    @AllArgsConstructor
    private static class DataPrivacyReportContentDocument implements IDataPrivacyReportContent<DocumentItem> {

        private final String name;
        private final String description;
        private final DocumentItem content;

        @Override
        public String getContentClass() {
            return "DocumentItem";
        }

    }

    @Getter
    @AllArgsConstructor
    private static class DataPrivacyReportContentLocation implements IDataPrivacyReportContent<GPSLocation> {

        private final String name;
        private final String description;
        private final GPSLocation content;

        @Override
        public String getContentClass() {
            return "GPSLocation";
        }

    }

    @Getter
    @AllArgsConstructor
    private static class DataPrivacyReportContentSignature implements IDataPrivacyReportContent<Signature> {

        private final String name;
        private final String description;
        private final Signature content;

        @Override
        public String getContentClass() {
            return "Signature";
        }

    }

    public DataPrivacyReport(String title) {
        super();
        this.title = title;
        this.sections = new LinkedList<>();
    }

    @Override
    public synchronized IDataPrivacyReportSection newSection(String name, String description) {
        DataPrivacyReportSection result = new DataPrivacyReportSection(name, description);
        sections.add(result);
        return result;
    }

    @Override
    public synchronized boolean removeSection(IDataPrivacyReportSection section) {
        return sections.remove(section);
    }

    @Override
    @JsonIgnore
    public List<MediaItem> getMediaItems() {
        return getSections().stream()
                .flatMap(section -> section.getSubSections().stream())
                .flatMap(subSection -> subSection.getContents().stream())
                .filter(content -> content instanceof DataPrivacyReportContentImage)
                .map(content -> (DataPrivacyReportContentImage) content)
                .map(DataPrivacyReportContentImage::getContent)
                //important to not have duplicates in the zip, causing exceptions
                .distinct()
                .sorted(Comparator.comparing(MediaItem::getCreated))
                .collect(Collectors.toList());
    }

    @Override
    @JsonIgnore
    public List<DocumentItem> getDocumentItems() {
        return getSections().stream()
                .flatMap(section -> section.getSubSections().stream())
                .flatMap(subSection -> subSection.getContents().stream())
                .filter(content -> content instanceof DataPrivacyReportContentDocument)
                .map(content -> (DataPrivacyReportContentDocument) content)
                .map(DataPrivacyReportContentDocument::getContent)
                //important to not have duplicates in the zip, causing exceptions
                .distinct()
                .sorted(Comparator.comparing(DocumentItem::getCreated))
                .collect(Collectors.toList());
    }

}
