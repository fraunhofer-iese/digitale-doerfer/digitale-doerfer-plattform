/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class TenantService extends BaseEntityService<Tenant> implements ITenantService {

    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    private Map<String, Tenant> tenantById;

    @PostConstruct
    private void initialize() {
        //this cache is replaced with a new map after it got cleared
        tenantById = Collections.emptyMap();
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return ICachingComponent.CacheConfiguration.builder()
                .cachedEntity(Tenant.class)
                .build();
    }

    @Override
    public Tenant findTenantById(String tenantId) throws TenantNotFoundException {

        checkAndRefreshCache();

        Tenant tenant = tenantById.get(tenantId);
        if (tenant == null) {
            throw new TenantNotFoundException(tenantId);
        }
        return tenant;
    }

    @Override
    public List<Tenant> findAllOrderedByNameAsc() {

        checkAndRefreshCache();

        return tenantById.values().stream()
                .sorted(Comparator.comparing(Tenant::getName))
                .collect(Collectors.toList());
    }

    @Override
    public Page<Tenant> findAll(Pageable page) {

        return toPage(findAllOrderedByNameAsc(), page);
    }

    @Override
    public Page<Tenant> findAllByInfixSearch(String infixSearchString, Pageable page) {

        checkAndRefreshCache();

        List<Tenant> tenants = tenantById.values().stream()
                .filter(tenant -> infixSearchMatches(tenant, infixSearchString))
                .sorted(Comparator.comparing(Tenant::getName))
                .collect(Collectors.toList());
        return toPage(tenants, page);
    }

    @Override
    public Page<Tenant> findAllById(Set<String> tenantIds, Pageable page) {

        checkAndRefreshCache();

        List<Tenant> tenants = tenantById.values().stream()
                .filter(tenant -> tenantIds.contains(tenant.getId()))
                .sorted(Comparator.comparing(Tenant::getName))
                .collect(Collectors.toList());
        return toPage(tenants, page);
    }

    @Override
    public Page<Tenant> findAllByIdInfixSearch(String infixSearchString, Set<String> tenantIds, Pageable page) {

        checkAndRefreshCache();

        List<Tenant> tenants = tenantById.values().stream()
                .filter(tenant -> tenantIds.contains(tenant.getId()))
                .filter(tenant -> infixSearchMatches(tenant, infixSearchString))
                .sorted(Comparator.comparing(Tenant::getName))
                .collect(Collectors.toList());
        return toPage(tenants, page);
    }

    private boolean infixSearchMatches(Tenant tenant, String infixSearchString) {

        return StringUtils.containsIgnoreCase(tenant.getId(), infixSearchString) ||
                StringUtils.containsIgnoreCase(tenant.getName(), infixSearchString) ||
                StringUtils.containsIgnoreCase(tenant.getTag(), infixSearchString);
    }

    @Override
    public List<Tenant> findAllExcluding(Collection<Tenant> tenants) {

        checkAndRefreshCache();

        ArrayList<Tenant> filteredTenants = new ArrayList<>(tenantById.values());
        filteredTenants.removeAll(tenants);
        return filteredTenants;
    }

    @Override
    public void checkTenantByIdExists(String tenantId) throws TenantNotFoundException {

        checkAndRefreshCache();

        if (!tenantById.containsKey(tenantId)) {
            throw new TenantNotFoundException(tenantId);
        }
    }

    @Override
    public void checkTenantsByIdExists(Set<String> tenantIds) throws TenantNotFoundException {

        checkAndRefreshCache();

        if (CollectionUtils.isEmpty(tenantIds)) {
            return;
        }
        for (String tenantId : tenantIds) {
            if (!tenantById.containsKey(tenantId)) {
                //one was not found, we check which ones are not found for a proper exception
                Set<String> invalidIds = tenantIds.stream()
                        .filter(id -> !tenantById.containsKey(id))
                        .collect(Collectors.toSet());
                throw new TenantNotFoundException("Could not find one of the tenants, these ids are invalid: {}",
                        invalidIds);
            }
        }
    }

    @Override
    public List<String> findAllTenantTags() {

        checkAndRefreshCache();

        return tenantById.values().stream()
                .map(Tenant::getTag)
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Tenant forceCached(Tenant tenant) {

        if (tenant == null) {
            return null;
        }
        checkAndRefreshCache();
        return tenantById.get(tenant.getId());
    }

    @Override
    public Long checkCacheTenants() {

        checkAndRefreshCache();
        return cacheChecker.getLastCacheRefresh();
    }

    @Override
    public void invalidateCache() {

        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {
        // if the cache is stale or no fetch ever happened
        // get all trading categories fresh from the database
        cacheChecker.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();

            tenantById = query(() -> tenantRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, Tenant.class))
                    .collect(Collectors.toMap(Tenant::getId, Function.identity())));
            log.info("Refreshed {} cached tenants in {} ms", tenantById.size(),
                    System.currentTimeMillis() - start);
        });
    }

}
