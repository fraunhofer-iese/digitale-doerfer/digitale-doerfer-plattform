/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
class CacheService implements ICacheService {

    @Autowired
    private Set<ICachingComponent> cachingComponents;

    /**
     * Caching configurations are cached :-)
     * They are queried often, and we do not want to create new configuration objects for every query.
     */
    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}),
            value = AccessLevel.PROTECTED)
    private final Map<ICachingComponent, ICachingComponent.CacheConfiguration> configurationByComponent = loadConfigurations();

    private Map<ICachingComponent, ICachingComponent.CacheConfiguration> loadConfigurations() {

        if (CollectionUtils.isEmpty(cachingComponents)) {
            return Collections.emptyMap();
        } else {
            Map<ICachingComponent, ICachingComponent.CacheConfiguration> map = new HashMap<>();
            for (ICachingComponent cachingComponent : cachingComponents) {
                ICachingComponent.CacheConfiguration configuration = cachingComponent.getCacheConfiguration();
                if (configuration == null || configuration.getCachedEntities() == null) {
                    throw new IllegalStateException(
                            "Caching component '" + cachingComponent + "' returns null cache configuration");
                }
                map.put(cachingComponent, configuration);
            }
            return map;
        }
    }

    @Override
    public int invalidateCachesForEntities(Collection<Class<? extends BaseEntity>> entities) {

        Set<ICachingComponent> componentsToInvalidate = cachingComponents.stream()
                .filter(cachingComponent ->
                        CollectionUtils.containsAny(configOf(cachingComponent).getCachedEntities(), entities))
                .collect(Collectors.toSet());
        //these caches depend on the caches we are going to invalidate, so we trigger them too
        //limitation: only one level of dependency is considered!
        Set<ICachingComponent> dependingCaches = cachingComponents.stream()
                .filter(cachingComponent -> dependsOn(cachingComponent, componentsToInvalidate))
                .collect(Collectors.toSet());

        componentsToInvalidate.forEach(ICachingComponent::invalidateCache);
        dependingCaches.forEach(ICachingComponent::invalidateCache);
        return componentsToInvalidate.size() + dependingCaches.size();
    }

    @Override
    public int invalidateAllCaches() {

        cachingComponents.forEach(ICachingComponent::invalidateCache);
        return cachingComponents.size();
    }

    private boolean dependsOn(ICachingComponent componentToCheck, Set<ICachingComponent> componentsToInvalidate) {

        return configOf(componentToCheck).getUsedCaches().stream()
                //we need to get the actual component behind the class of the component
                .flatMap(cachingComponentClass -> cachingComponents.stream()
                        .filter(cachingComponent -> cachingComponentClass
                                .isAssignableFrom(cachingComponent.getClass())))
                .anyMatch(componentsToInvalidate::contains);
    }

    private ICachingComponent.CacheConfiguration configOf(ICachingComponent cachingComponent) {

        return getConfigurationByComponent().get(cachingComponent);
    }

}
