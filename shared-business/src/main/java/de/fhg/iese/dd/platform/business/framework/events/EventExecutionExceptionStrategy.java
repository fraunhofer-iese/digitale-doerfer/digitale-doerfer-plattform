/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

/**
 * Strategy how exceptions occurring during the execution of an {@link EventConsumingFunction} or
 * {@link EventProcessingFunction} should be handled.
 */
public enum EventExecutionExceptionStrategy {

    /**
     * Any exception that occurs is propagated up the event chain.
     */
    PROPAGATE_EXCEPTION,
    /**
     * Any exception that occurs is ignored and just logged.
     */
    IGNORE_EXCEPTION,

    /**
     * Equivalent to {@link #IGNORE_EXCEPTION} if the execution strategy is set to:
     * <ul>
     *     <li>{@link EventExecutionStrategy#ASYNCHRONOUS_PREFERRED}</li>
     *     <li>{@link EventExecutionStrategy#ASYNCHRONOUS_REQUIRED}</li>
     * </ul>
     * Equivalent to {@link #PROPAGATE_EXCEPTION} if the execution strategy is set to:
     * <ul>
     *     <li>{@link EventExecutionStrategy#SYNCHRONOUS_PREFERRED}</li>
     *     <li>{@link EventExecutionStrategy#AUTO}</li>
     * </ul>
     */
    AUTO

}
