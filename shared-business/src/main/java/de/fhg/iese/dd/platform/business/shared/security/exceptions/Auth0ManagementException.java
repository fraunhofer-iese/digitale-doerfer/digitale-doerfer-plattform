/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.exceptions;

import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;

public class Auth0ManagementException extends OauthManagementException {

    private static final long serialVersionUID = -6983304697846252091L;

    public Auth0ManagementException(APIException cause) {
        super(String.format("Error during auth0 management api call, got %d, %s (%s)",
                cause.getStatusCode(), cause.getError(), cause.getDescription()), cause);
    }

    public Auth0ManagementException(Auth0Exception cause) {
        super("Error during auth0 management api call", cause);
    }

}
