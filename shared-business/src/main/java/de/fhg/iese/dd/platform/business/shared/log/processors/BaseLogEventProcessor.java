/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.log.processors;

import static de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor.EVENT_JSON_MARKER;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public abstract class BaseLogEventProcessor extends BaseEventProcessor {

    protected void logEvent(BaseEvent event, EventProcessingContext context) {

        if (event == null) {
            return;
        }
        final Pair<Map<String, Object>, Set<String>> logDetails = event.getLogDetails(Person.class::isAssignableFrom);

        //these are the attributes of the log message, mainly all ids of entities referenced by this event
        Map<String, Object> logData = logDetails.getLeft();
        //we want a log message for every person id we find, so that it's possible to trace the events a person triggered
        Set<String> personIds = logDetails.getRight();

        for (String personId : personIds) {
            logData.put("personId", personId);
            logData.put("duration", context.getOverallDuration());
            log.log(Level.DEBUG, EVENT_JSON_MARKER, logData);
        }
    }

}
