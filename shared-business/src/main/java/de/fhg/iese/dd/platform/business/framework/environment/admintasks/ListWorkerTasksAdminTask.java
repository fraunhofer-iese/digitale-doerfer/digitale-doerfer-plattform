/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.admintasks;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.framework.environment.services.ILeaderService;
import de.fhg.iese.dd.platform.business.framework.environment.services.IWorkerService;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ListWorkerTasksAdminTask extends BaseAdminTask {

    @Autowired
    private ILeaderService leaderService;
    @Autowired
    private IWorkerService workerService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private IReflectionService reflectionService;

    @Override
    public String getName() {
        return "ListWorkerTasks";
    }

    @Override
    public String getDescription() {
        return "Lists all worker tasks that are registered if the node is leader.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        final List<IWorkerTask> workerTasks = workerService.getWorkerTasks();
        final Pair<String, Long> lastSeenLeader = leaderService.getLastSeenLeader();

        final String workerTaskDescriptions = workerTasks.stream()
                .map(this::toWorkerDescription)
                .sorted()
                .collect(Collectors.joining("\n"));

        final String lastSeenLeaderDescription;
        if (lastSeenLeader == null) {
            lastSeenLeaderDescription = "unknown";
        } else {
            lastSeenLeaderDescription = timeService.toLocalTimePreciseHumanReadable(lastSeenLeader.getRight()) + " - " +
                    lastSeenLeader.getLeft();
        }

        logSummary.info("""
                        Last seen leader:
                        {}
                        Registered worker tasks:
                        {}""",
                lastSeenLeaderDescription, workerTaskDescriptions);
    }

    private String toWorkerDescription(IWorkerTask workerTask) {
        return String.format("%-10.10s - %-45.45s - %s", reflectionService.getModuleName(workerTask.getClass()),
                workerTask.getName(),
                workerService.triggerToString(workerTask.getTaskTrigger()));
    }

}
