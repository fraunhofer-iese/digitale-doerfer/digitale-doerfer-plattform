/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.dataprivacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.BaseDataDeletionService;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos.PersonBlockingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.RoleAssignmentRepository;

@Service
class ParticipantsDataDeletionService extends BaseDataDeletionService implements IParticipantsDataDeletionService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RoleAssignmentRepository roleAssignmentRepository;
    @Autowired
    private PersonBlockingRepository personBlockingRepository;
    @Autowired
    private AddressListEntryRepository addressListEntryRepository;
    @Autowired
    private IOauthManagementService oauthManagementService;
    @Autowired
    private ITimeService timeService;

    @Override
    public DeleteOperation deletePerson(Person person) {

        person.setAddresses(null);
        person.setNickName(null);
        person.setPhoneNumber(null);
        person.setProfilePicture(null);
        person.setFirstName(erasedNonNullString());
        person.setLastName(erasedNonNullString());
        person.setEmail(randomUniqueString());
        person.setPendingNewEmail(null);
        person.setVerificationStatus(0);
        person.setLastLoggedIn(timeService.currentTimeMillisUTC());
        person.setDeleted(true);
        person.setDeletionTime(timeService.currentTimeMillisUTC());
        personRepository.saveAndFlush(person);

        return DeleteOperation.ERASED;
    }

    @Override
    public DeleteOperation deleteOauthAccount(Person person) {

        oauthManagementService.deleteUser(person.getOauthId());
        // oauth id is only "deleted" when the deletion at auth0 was successful
        person.setOauthId("deleted|" + randomUniqueString());
        personRepository.saveAndFlush(person);

        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteRoleAssignment(RoleAssignment roleAssignment) {

        roleAssignmentRepository.delete(roleAssignment);

        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deletePersonBlocking(PersonBlocking personBlocking) {

        personBlockingRepository.delete(personBlocking);

        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteAddressListEntry(AddressListEntry addressListEntry) {

        addressListEntryRepository.delete(addressListEntry);

        return DeleteOperation.DELETED;
    }

}
