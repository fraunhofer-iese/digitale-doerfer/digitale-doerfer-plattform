/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Johannes Schneider, Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.personblocking.services;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.personblocking.exceptions.BlockedPersonsLimitExceededException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.config.PersonBlockingConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos.PersonBlockingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

@Service
class PersonBlockingService extends BaseService implements IPersonBlockingService {

    @Autowired
    private PersonBlockingRepository personBlockingRepository;

    @Autowired
    private PersonBlockingConfig personBlockingConfig;

    @Transactional
    @Override
    public void blockPerson(final App app, final Person blockingPerson, final Person blockedPerson) {
        if (Objects.equals(blockingPerson, blockedPerson)) {
            log.warn("{} tries to block themselves in {}", blockingPerson, app);
            return;
        }
        checkBlockLimit(app, blockingPerson);
        if (!personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app, blockingPerson, blockedPerson)) {
            final PersonBlocking personBlocking = PersonBlocking.builder().app(app)
                    .blockingPerson(blockingPerson)
                    .blockedPerson(blockedPerson)
                    .build();
            personBlockingRepository.saveAndFlush(personBlocking);
            log.info("{} blocked {} in {}", blockingPerson, blockedPerson, app);
        } else {
            log.warn("{} already blocked {} in {}", blockingPerson, blockedPerson, app);
        }
    }

    private void checkBlockLimit(final App app, final Person blockingPerson) {
        if (personBlockingRepository.countByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(app, blockingPerson)
                >= personBlockingConfig.getMaxNumberBlockedPersons()) {
            throw new BlockedPersonsLimitExceededException();
        }
    }

    @Transactional
    @Override
    public void unblockPerson(final App app, final Person blockingPerson, final Person blockedPerson) {
        final Optional<PersonBlocking> personBlocking = personBlockingRepository
                .findByAppAndBlockingPersonAndBlockedPerson(app, blockingPerson, blockedPerson);
        if (personBlocking.isPresent()) {
            personBlockingRepository.delete(personBlocking.get());
            log.info("{} unblocked {} in {}", blockingPerson, blockedPerson, app);
        } else {
            log.warn("{} already unblocked {} in {}", blockingPerson, blockedPerson, app);
        }
    }

    @Override
    public Collection<Person> getBlockedPersons(final App app, final Person blockingPerson) {
        return personBlockingRepository.findBlockedPersonsByAppAndBlockingPerson(app, blockingPerson);
    }

    @Override
    public boolean existsBlocking(App app, Person blockingPerson, Person blockedPerson) {
        return personBlockingRepository
                .existsByAppAndBlockingPersonAndBlockedPerson(app, blockingPerson, blockedPerson);
    }

    @Override
    public Collection<Person> getBlockedNotDeletedPersons(final App app, final Person blockingPerson) {
        return personBlockingRepository
                .findBlockedPersonByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(app, blockingPerson);
    }

    @Override
    public Set<String> getBlockedNotDeletedPersonIds(final App app, final Person blockingPerson) {
        return personBlockingRepository
                .findBlockedPersonIdsByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(app, blockingPerson);
    }

    @Override
    public Collection<App> getBlockingApps(final Person blockingPerson) {
        return personBlockingRepository.findAppsByBlockingPerson(blockingPerson);
    }

    @Override
    public Collection<PersonBlocking> getBlockingsOfPerson(final Person blockingPerson) {
        return personBlockingRepository.findAllByBlockingPersonOrderByCreatedDesc(blockingPerson);
    }

}
