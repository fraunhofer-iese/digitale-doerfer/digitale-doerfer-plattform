/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events.processing;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventConsumingFunction;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionExceptionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingFunction;

/**
 * Method within an Event Processor (subtype of {@link BaseEventProcessor} annotated with @{@link EventProcessor}) that
 * actually process events. The method needs to be of type {@link EventConsumingFunction} or a form of {@link
 * EventProcessingFunction}.
 * <p/>
 * See {@link BaseEventProcessor} for more details of supported signatures.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EventProcessing {

    /**
     * If the event processing function should also be called if subtypes of the event are on the event bus.
     */
    boolean includeSubtypesOfEvent() default false;

    /**
     * All event types that the event processing function should be called if they are on the event bus.
     * <p/>
     * Does not need to be set, default is the type of the first parameter of the function.
     */
    Class<? extends BaseEvent>[] relevantEvents() default {};

    /**
     * Select the {@link EventExecutionStrategy} to control how the method is called when the relevant events are
     * present on the event bus.
     * <p/>
     * Use @{@link EventProcessor#executionStrategy()} to set it globally for all event processing methods that have the
     * default value ({@link EventExecutionStrategy#AUTO}) set.
     */
    EventExecutionStrategy executionStrategy() default EventExecutionStrategy.AUTO;

    /**
     * Select the {@link EventExecutionExceptionStrategy} to control how the exceptions occurring during the execution
     * of the method are handled.
     * <p/>
     * Use @{@link EventProcessor#exceptionStrategy()} to set it globally for all event processing methods that have the
     * default value ({@link EventExecutionExceptionStrategy#AUTO}) set.
     */
    EventExecutionExceptionStrategy exceptionStrategy() default EventExecutionExceptionStrategy.AUTO;

}
