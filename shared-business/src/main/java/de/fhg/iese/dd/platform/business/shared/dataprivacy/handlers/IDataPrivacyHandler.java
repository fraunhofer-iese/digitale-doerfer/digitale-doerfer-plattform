/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

/**
 * Common interface for all data privacy handlers for all modules.
 * <p/>
 * This handler will be used to collect user data and to delete it.
 */
public interface IDataPrivacyHandler extends IDataDependencyAware {

    /**
     * Collects the stored data of the given person. The data is added to the report, so that the user can review the
     * data.
     *
     * @param person            data privacy data collection of person
     * @param dataPrivacyReport the data report to extend
     */
    void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport);

    void deleteUserData(Person person, IPersonDeletionReport personDeletionReport);

}
