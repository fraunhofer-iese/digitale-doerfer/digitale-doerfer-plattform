/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Danielle Korth, Ben Bukrhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.files.services.ITeamFileStorageService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.statistics.*;
import de.fhg.iese.dd.platform.business.shared.statistics.exceptions.StatisticsReportNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.config.StatisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.StatisticsCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TeamFileNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Intended to be THE single statistics service, with various report generators as plugins.
 */
@Service
class StatisticsService extends BaseService implements IStatisticsService {

    private static final int DEFAULT_ACTIVE_DAYS_BEFORE = 6;
    private static final Duration ALLOWED_MAX_OFFSET_FROM_STATISTICS_REPORT_END_TIME = Duration.ofHours(1);

    private static final List<Locale> SUPPORTED_LOCALES = List.of(Locale.GERMAN, Locale.ENGLISH);

    @Autowired
    private StatisticsConfig statisticsConfig;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IEnvironmentService environmentService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private IPluginService pluginService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private ITeamFileStorageService teamFileStorageService;
    @Autowired
    private MessageSource messageSource;

    @Override
    public StatisticsReport generateStatisticsReport(StatisticsReportDefinition statisticsDefinition) {

        long creationTime = timeService.currentTimeMillisUTC();
        Collection<GeoArea> geoAreas = getDefinedGeoAreas(statisticsDefinition);
        if (CollectionUtils.isEmpty(geoAreas)) {
            log.error("Trying to create empty report without any geo areas");
            throw new RuntimeException("Trying to create empty report without any geo areas");
        }
        final List<GeoAreaStatistics> geoAreaStatistics = geoAreas.stream()
                .map(GeoAreaStatistics::new)
                .collect(Collectors.toList());

        String reportTitle = "Current statistics for " + environmentService.getEnvironmentFullName() + " at " +
                timeService.toLocalTimeHumanReadable(timeService.currentTimeMillisUTC());
        final Collection<IStatisticsProvider> allStatisticsProviders =
                pluginService.getPluginInstancesRaw(PluginTarget.of(IStatisticsProvider.class));

        for (IStatisticsProvider statisticsProvider : allStatisticsProviders) {
            try {
                //the provider has to decide if it wants to add statistics values or not, based on the definition
                statisticsProvider.calculateGeoAreaStatistics(geoAreaStatistics, statisticsDefinition);
            } catch (Exception e) {
                log.error("Statistics provider {} failed: {}", statisticsProvider.getId(), e);
                throw e;
            }
        }

        final Map<String, StatisticsMetaData> statisticsMetaDataById = allStatisticsProviders.stream()
                .flatMap(sp -> sp.getAllMetaData().stream())
                .collect(Collectors.toMap(StatisticsMetaData::getId, Function.identity()));

        final List<StatisticsMetaData> metaData = statisticsDefinition.getStatisticsIds().stream()
                .map(statisticsMetaDataById::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (metaData.size() != statisticsDefinition.getStatisticsIds().size()) {

            final List<String> foundIds = metaData.stream()
                    .map(StatisticsMetaData::getId)
                    .collect(Collectors.toList());

            final List<String> notFoundIds = statisticsDefinition.getStatisticsIds().stream()
                    .filter(id -> !foundIds.contains(id))
                    .collect(Collectors.toList());

            log.error("Could not find statistics with ids {}", notFoundIds);
        }

        return StatisticsReport.builder()
                .title(reportTitle)
                .creationTime(creationTime)
                .definition(statisticsDefinition)
                .geoAreaStatistics(geoAreaStatistics)
                .metaData(metaData)
                .build();
    }

    @Override
    public String reportToCsv(StatisticsReport report, String separator) {

        List<GeoArea> geoAreas = report.getGeoAreaStatistics().stream()
                .map(GeoAreaStatistics::getGeoArea)
                .collect(Collectors.toList());

        List<GeoArea> rootAreas = new ArrayList<>(geoAreaService.getRootGeoAreas(geoAreas));

        Map<GeoArea, List<GeoArea>> geoAreasByParent = geoAreas.stream()
                .filter(geoArea -> geoArea.getParentArea() != null)
                .collect(Collectors.groupingBy(GeoArea::getParentArea));

        final Map<GeoArea, GeoAreaStatistics> statisticsByGeoArea = report.getGeoAreaStatistics().stream()
                .collect(Collectors.toMap(GeoAreaStatistics::getGeoArea, Function.identity()));

        final List<StatisticsMetaData> sortedMetadata = report.getMetaData();
        final List<Pair<StatisticsMetaData, StatisticsTimeRelation>> sortedStatisticsMetaDataAndTimeRelation =
                report.getStatisticsMetadataAndTimeRelation();
        final List<String> sortedMachineNames = sortedMetadata.stream()
                .flatMap(m -> m.getSpecificMachineNames().stream())
                .collect(Collectors.toList());
        final String valueFormat = sortedStatisticsMetaDataAndTimeRelation
                .stream()
                .map(Pair::getLeft)
                .map(m -> Objects.toString(m.getValueFormat(), "%s"))
                .collect(Collectors.joining(separator));

        StringBuilder csv = new StringBuilder();
        csv.append('\ufeff');//this should help excel to interpret UTF8 correctly by adding a UTF8 BOM
        csv.append("geoarea-id")
                .append(separator)
                .append("geoarea-name")
                .append(separator)
                .append("geoarea-depth")
                .append(separator);

        sortedMachineNames.forEach(m -> csv.append(m).append(separator));
        csv.append("\n");

        appendToCSV(statisticsByGeoArea,
                (geoAreaInfo) -> {
                    Object[] values = geoAreaInfo.getValues(sortedStatisticsMetaDataAndTimeRelation);
                    try {
                        return String.format(valueFormat, values);
                    } catch (IllegalFormatException e) {
                        log.error("Failed with pattern {} on values {}, " +
                                        "statistics and time relations currently are\n{}",
                                valueFormat, values, sortedStatisticsMetaDataAndTimeRelation.stream()
                                        .map(p -> p.getLeft().getId() + "/" + p.getRight().toString())
                                        .collect(Collectors.joining("\n")));
                        throw e;
                    }
                },
                geoAreasByParent, rootAreas, csv, separator);
        return csv.toString();
    }

    private void appendToCSV(Map<GeoArea, GeoAreaStatistics> geoAreaInfos,
            Function<GeoAreaStatistics, String> printer,
            Map<GeoArea, List<GeoArea>> geoAreasByParent,
            List<GeoArea> siblings,
            StringBuilder result,
            String separator) {

        //these are all the siblings on the same level, they should be sorted by name
        siblings.sort(Comparator.comparing(GeoArea::getName));

        for (GeoArea geoArea : siblings) {

            result.append(geoArea.getId())
                    .append(separator)
                    .append(geoArea.getName())
                    .append(separator)
                    .append(geoArea.getDepth())
                    .append(separator);

            result.append(printer.apply(geoAreaInfos.get(geoArea)))
                    .append("\n");

            //recursion to easily construct the tree
            List<GeoArea> children = geoAreasByParent.get(geoArea);
            if (!CollectionUtils.isEmpty(children)) {
                // if there are children, append them
                appendToCSV(geoAreaInfos, printer, geoAreasByParent, children, result, separator);
            }
        }
    }

    @Override
    public String reportStatisticsMetadataToCsv(StatisticsReport report, String separator) {

        final List<Pair<StatisticsMetaData, StatisticsTimeRelation>> statisticsMetadataAndTimeRelation =
                report.getStatisticsMetadataAndTimeRelation();

        StringBuilder csv = new StringBuilder();
        csv.append('\ufeff');//this should help excel to interpret UTF8 correctly by adding a UTF8 BOM
        csv.append("statistics-id")
                .append(separator)
                .append("statistics-machine-name")
                .append(separator);
        for (Locale locale : SUPPORTED_LOCALES) {
            csv.append("statistics-human-name-").append(locale.getLanguage())
                    .append(separator)
                    .append("statistics-description-").append(locale.getLanguage())
                    .append(separator);
        }
        csv.append("statistics-start")
                .append(separator)
                .append("statistics-end")
                .append("\n");
        for (Pair<StatisticsMetaData, StatisticsTimeRelation> metaDataAndTimeRelation : statisticsMetadataAndTimeRelation) {
            final StatisticsMetaData metaData = metaDataAndTimeRelation.getLeft();
            final StatisticsTimeRelation timeRelation = metaDataAndTimeRelation.getRight();
            csv.append(metaData.getId())
                    .append(separator)
                    .append(sanitizeForCSV(metaData.getSpecificMachineName(timeRelation)))
                    .append(separator);
            for (Locale locale : SUPPORTED_LOCALES) {
                csv.append(sanitizeForCSV(getSpecificHumanName(metaData, locale, timeRelation)))
                        .append(separator)
                        .append(sanitizeForCSV(getSpecificDescription(metaData, locale, timeRelation)))
                        .append(separator);
            }
            csv.append(timeService.toLocalTimeHumanReadable(timeRelation.getStartTime(report.getDefinition())))
                    .append(separator)
                    .append(timeService.toLocalTimeHumanReadable(report.getCreationTime()))
                    .append("\n");
        }
        return csv.toString();
    }

    private String sanitizeForCSV(String string) {
        if (StringUtils.containsAny(string, ',', ';', '\n', '\t')) {
            return "\"" + string + "\"";
        } else {
            return string;
        }
    }

    private String getSpecificHumanName(StatisticsMetaData metaData, Locale locale,
            StatisticsTimeRelation timeRelation) {
        return messageSource.getMessage(
                "statistics." + metaData.getId() + ".humanName", null, locale)
                + " (" + getTimeRelationHumanName(locale, timeRelation) + ")";
    }

    private String getSpecificDescription(StatisticsMetaData metaData, Locale locale,
            StatisticsTimeRelation timeRelation) {
        return messageSource.getMessage("statistics." + metaData.getId() + ".description", null, locale)
                + " (" + getTimeRelationDescription(locale, timeRelation) + ")";
    }

    private String getTimeRelationHumanName(Locale locale, StatisticsTimeRelation timeRelation) {
        return messageSource.getMessage("statistics.timeRelation." + timeRelation.name().toLowerCase() + ".humanName",
                null, locale);
    }

    private String getTimeRelationDescription(Locale locale, StatisticsTimeRelation timeRelation) {
        return messageSource.getMessage("statistics.timeRelation." + timeRelation.name().toLowerCase() + ".description",
                null,
                locale);
    }

    @Override
    public String reportToText(StatisticsReport report) {

        List<GeoArea> geoAreas = report.getGeoAreaStatistics().stream()
                .map(GeoAreaStatistics::getGeoArea)
                .collect(Collectors.toList());

        List<GeoArea> rootAreas = new ArrayList<>(geoAreaService.getRootGeoAreas(geoAreas));

        Map<GeoArea, List<GeoArea>> geoAreasByParent = geoAreas.stream()
                .filter(geoArea -> geoArea.getParentArea() != null)
                .collect(Collectors.groupingBy(GeoArea::getParentArea));

        final Map<GeoArea, GeoAreaStatistics> statisticsByGeoArea = report.getGeoAreaStatistics().stream()
                .collect(Collectors.toMap(GeoAreaStatistics::getGeoArea, Function.identity()));

        final List<Pair<StatisticsMetaData, StatisticsTimeRelation>> sortedStatisticsMetaDataAndTimeRelation =
                report.getStatisticsMetadataAndTimeRelation();
        final String valueFormat = sortedStatisticsMetaDataAndTimeRelation
                .stream()
                .map(m -> "%" + m.getLeft().getExpectedWidth() + "s")
                .collect(Collectors.joining(" "));
        final String headerFormat = sortedStatisticsMetaDataAndTimeRelation
                .stream()
                .map(m -> "%-" + m.getLeft().getExpectedWidth() + "." + m.getLeft().getExpectedWidth() + "s")
                .collect(Collectors.joining("│", "", "│"));

        StringBuilder result = new StringBuilder();
        result.append("\n").append(report.getTitle()).append("\n");
        result.append("- Geo area name and hierarchy\n");

        for (Pair<StatisticsMetaData, StatisticsTimeRelation> metaDataAndTimeRelation : sortedStatisticsMetaDataAndTimeRelation) {
            final StatisticsMetaData metaData = metaDataAndTimeRelation.getLeft();
            final StatisticsTimeRelation timeRelation = metaDataAndTimeRelation.getRight();
            result.append("- ")
                    .append(getSpecificDescription(metaData, Locale.ENGLISH, timeRelation))
                    .append(" (")
                    .append(timeService.toLocalTimeHumanReadable(timeRelation.getStartTime(report.getDefinition())))
                    .append(" - ")
                    .append(timeService.toLocalTimeHumanReadable(report.getCreationTime()))
                    .append(")")
                    .append("\n");
        }
        result.append(StringUtils.repeat("═", 130)).append("\n");
        result.append("\n");

        result.append(String.format("%-55.55s│", "Name of geo area"));
        result.append(String.format(headerFormat, sortedStatisticsMetaDataAndTimeRelation.stream()
                .map(m -> getSpecificHumanName(m.getLeft(), Locale.ENGLISH, m.getRight()))
                .toArray()));
        result.append("\n");
        result.append(StringUtils.repeat("─", 130)).append("\n");

        printGeoArea(statisticsByGeoArea,
                (geoAreaInfo) -> String.format(valueFormat,
                        geoAreaInfo.getValues(sortedStatisticsMetaDataAndTimeRelation)),
                geoAreasByParent, rootAreas, " ", result);
        return result.toString();
    }

    private void printGeoArea(Map<GeoArea, GeoAreaStatistics> geoAreaInfos,
            Function<GeoAreaStatistics, String> printer,
            Map<GeoArea, List<GeoArea>> geoAreasByParent,
            List<GeoArea> siblings,
            String prefix,
            StringBuilder result) {

        //these are all the siblings on the same level, they should be sorted by name
        siblings.sort(Comparator.comparing(GeoArea::getName));

        for (int i = 0, geoAreasSize = siblings.size(); i < geoAreasSize; i++) {
            GeoArea geoArea = siblings.get(i);
            boolean last = i == (geoAreasSize - 1);

            //constructs a "graphical" tree
            String indent = prefix + (last ? "  └─" : "  ├─");

            GeoAreaStatistics geoAreaInfo = geoAreaInfos.get(geoArea);

            result.append(String.format("%-55.55s %s %n",
                    indent + geoArea.getName(),
                    printer.apply(geoAreaInfo)));

            //recursion to easily construct the tree
            List<GeoArea> children = geoAreasByParent.get(geoArea);
            if (!CollectionUtils.isEmpty(children)) {
                // if there are children, print them
                printGeoArea(geoAreaInfos, printer, geoAreasByParent, children, prefix + (last ? "    " : "  │ "),
                        result);
            }
        }
    }

    @Override
    public List<StatisticsReportDefinition> getConfiguredReportDefinitions(int maxOffset) {

        List<StatisticsReportDefinition> configuredDefinitions = new ArrayList<>();

        //get features as config for reports
        final Map<FeatureConfig, StatisticsCreationFeature> statisticsFeatures =
                featureService.getAllFeatureConfigurations(StatisticsCreationFeature.class);

        for (Map.Entry<FeatureConfig, StatisticsCreationFeature> e : statisticsFeatures.entrySet()) {
            //the config is required to get the actual target geo areas of the report
            final FeatureConfig featureConfig = e.getKey();
            final StatisticsCreationFeature feature = e.getValue();
            if (!featureConfig.isEnabled()) {
                continue;
            }
            if (CollectionUtils.isEmpty(featureConfig.getGeoAreasIncluded())) {
                log.error("Invalid statistics creation feature config '{}', no geo areas defined",
                        toLogMessage(featureConfig, null));
                continue;
            }
            for (StatisticsCreationFeature.StatisticsCreationConfiguration definition : feature.getCreationConfigurations()) {

                if (StringUtils.isBlank(definition.getTeamFileStoragePath())) {
                    log.error("Invalid statistics creation feature config '{}', no team file storage path defined",
                            toLogMessage(featureConfig, definition));
                    continue;
                }
                if (CollectionUtils.isEmpty(definition.getFrequencies())) {
                    log.error("Invalid statistics creation feature config '{}', no frequencies defined",
                            toLogMessage(featureConfig, definition));
                    continue;
                }
                log.trace("Creating statistics for '{}' with config {}", definition.getTeamFileStoragePath(),
                        toLogMessage(featureConfig, definition));

                final String csvSeparator = StringUtils.defaultIfEmpty(definition.getCsvSeparator(), ";");
                final String reportNamePrefix = StringUtils.defaultIfEmpty(definition.getReportNamePrefix(), "report");

                final String cleanedStoragePath =
                        StringUtils.removeEnd(StringUtils.removeStart(definition.getTeamFileStoragePath(), "/"), "/") +
                                "/";
                for (ChronoUnit frequency : definition.getFrequencies()) {

                    for (int offsetFromCurrentFullUnit = -1;
                         offsetFromCurrentFullUnit >= maxOffset; offsetFromCurrentFullUnit--) {

                        String fileNamePrefix = cleanedStoragePath +
                                getReportName(reportNamePrefix, offsetFromCurrentFullUnit, frequency);

                        StatisticsReportDefinition reportDefinition = StatisticsReportDefinition.builder()
                                .name(fileNamePrefix)
                                .statisticsIds(definition.getStatisticsIds())
                                .includedRootGeoAreaIds(featureConfig.getGeoAreasIncluded().stream()
                                        .map(GeoArea::getId)
                                        .collect(Collectors.toList()))
                                .startTimeNew(getReportStartTimeNew(offsetFromCurrentFullUnit, frequency))
                                .startTimeActive(getReportStartTimeActive(offsetFromCurrentFullUnit, frequency))
                                .endTime(getReportEndTime(frequency))
                                .timeUnit(frequency)
                                .offsetFromCurrentFullTimeUnit(offsetFromCurrentFullUnit)
                                .allGeoAreas(false)
                                .build();

                        if (definition.isTextReport()) {
                            reportDefinition.setFileNameText(fileNamePrefix + ".txt");
                        }
                        if (definition.isCsvReport()) {
                            reportDefinition.setFileNameCsv(fileNamePrefix + ".csv");
                            reportDefinition.setFileNameCsvMetadata(fileNamePrefix + "_readme.csv");
                            reportDefinition.setCsvSeparator(csvSeparator);
                        }
                        configuredDefinitions.add(reportDefinition);
                    }
                }
            }
        }
        configuredDefinitions.sort(Comparator.comparing(StatisticsReportDefinition::getName));
        return configuredDefinitions;
    }

    @Override
    public List<StatisticsReportDefinition> createConfiguredStatisticsReports() {

        List<StatisticsReportDefinition> reportDefinitionsToBeGenerated = getConfiguredReportDefinitions(-1).stream()
                //only report definitions that can be generated now are relevant
                .filter(this::canReportBeGeneratedNow)
                .collect(Collectors.toList());
        //we group the report definitions by time
        Map<StatisticsReportDefinition, List<StatisticsReportDefinition>> combinedDefinitionToDefinitions =
                reportDefinitionsToBeGenerated.stream()
                        .collect(Collectors.groupingBy(definition -> StatisticsReportDefinition.builder()
                                .startTimeNew(definition.getStartTimeNew())
                                .startTimeActive(definition.getStartTimeActive())
                                .endTime(definition.getEndTime())
                                .build()));
        for (Iterator<Map.Entry<StatisticsReportDefinition, List<StatisticsReportDefinition>>> iterator =
             combinedDefinitionToDefinitions.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<StatisticsReportDefinition, List<StatisticsReportDefinition>> entry = iterator.next();
            StatisticsReportDefinition combinedDefinition = entry.getKey();
            List<StatisticsReportDefinition> derivedReportDefinitions = entry.getValue();
            //the combined definition gets all the combined statistics and geo area ids, so that the others can be created by filtering
            List<String> statisticsIds = derivedReportDefinitions.stream()
                    .flatMap(d -> d.getStatisticsIds().stream())
                    .distinct()
                    .collect(Collectors.toList());
            List<String> geoAreaIds = derivedReportDefinitions.stream()
                    .flatMap(d -> d.getIncludedRootGeoAreaIds().stream())
                    .distinct()
                    .collect(Collectors.toList());
            boolean allGeoAreas = derivedReportDefinitions.stream()
                    .anyMatch(StatisticsReportDefinition::isAllGeoAreas);
            combinedDefinition.setStatisticsIds(statisticsIds);
            combinedDefinition.setIncludedRootGeoAreaIds(geoAreaIds);
            combinedDefinition.setAllGeoAreas(allGeoAreas);
            log.debug("Deriving {} statistic reports from one", derivedReportDefinitions.size());
            //we only need to generate the combined one, the derived ones are just filtered
            StatisticsReport combinedReport = generateStatisticsReport(combinedDefinition);
            for (StatisticsReportDefinition derivedReportDefinition : derivedReportDefinitions) {
                StatisticsReport derivedReport = filterStatisticsReport(combinedReport, derivedReportDefinition);
                final boolean reportCreated = saveStatisticsReports(derivedReport);
                if (!reportCreated) {
                    log.trace("Skipping saving of statistics for '{}' because it already exists",
                            StringUtils.defaultString(derivedReport.getDefinition().getFileNameCsv()) + " | " +
                                    StringUtils.defaultString(derivedReport.getDefinition().getFileNameText()));
                }
            }
            if (iterator.hasNext()) {
                //to avoid overloading the DB with a series of subsequent complex queries we wait shortly between report generations
                try {
                    log.trace("Waiting for {} seconds before next report",
                            statisticsConfig.getDelayBetweenReportGeneration().toSeconds());
                    //noinspection BusyWait
                    Thread.sleep(statisticsConfig.getDelayBetweenReportGeneration().toMillis());
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }
            }
        }
        return reportDefinitionsToBeGenerated;
    }

    private StatisticsReport filterStatisticsReport(StatisticsReport combinedReport,
            StatisticsReportDefinition derivedReportDefinition) {

        Set<String> statisticsIds = new HashSet<>(derivedReportDefinition.getStatisticsIds());
        Collection<GeoArea> geoAreas = getDefinedGeoAreas(derivedReportDefinition);
        //the metadata needs to be filtered by the statistics of the derived report
        List<StatisticsMetaData> filteredMetadata = combinedReport.getMetaData().stream()
                .filter(m -> statisticsIds.contains(m.getId()))
                .collect(Collectors.toList());

        //the statistics itself need to be filtered by the geo areas and the statistics of the derived report
        List<GeoAreaStatistics> filteredStatistics = combinedReport.getGeoAreaStatistics().stream()
                .filter(s -> derivedReportDefinition.isAllGeoAreas() || geoAreas.contains(s.getGeoArea()))
                .map(s -> s.cloneAndFilter(statisticsIds))
                .collect(Collectors.toList());

        return StatisticsReport.builder()
                .creationTime(combinedReport.getCreationTime())
                .title(combinedReport.getTitle())
                .definition(derivedReportDefinition)
                .metaData(filteredMetadata)
                .geoAreaStatistics(filteredStatistics)
                .build();
    }

    @Override
    public boolean saveStatisticsReports(StatisticsReport statisticsReport) {

        StatisticsReportDefinition reportDefinition = statisticsReport.getDefinition();

        String teamFileStorageMainFolder =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolder(), "/");

        boolean csvReport = reportDefinition.isCsvReport() &&
                !teamFileStorageService.fileExists(teamFileStorageMainFolder + reportDefinition.getFileNameCsv());
        boolean textReport = reportDefinition.isTextReport() &&
                !teamFileStorageService.fileExists(teamFileStorageMainFolder + reportDefinition.getFileNameText());
        if (!csvReport && !textReport) {
            return false;
        }

        if (csvReport) {
            final String reportCsv = reportToCsv(statisticsReport, reportDefinition.getCsvSeparator());
            final String metadataCsv =
                    reportStatisticsMetadataToCsv(statisticsReport, reportDefinition.getCsvSeparator());
            teamFileStorageService.saveFile(metadataCsv.getBytes(StandardCharsets.UTF_8), "text/csv",
                    teamFileStorageMainFolder + reportDefinition.getFileNameCsvMetadata());
            teamFileStorageService.saveFile(reportCsv.getBytes(StandardCharsets.UTF_8), "text/csv",
                    teamFileStorageMainFolder + reportDefinition.getFileNameCsv());
        }
        if (textReport) {
            final String reportText = reportToText(statisticsReport);
            teamFileStorageService.saveFile(reportText.getBytes(StandardCharsets.UTF_8), "text/plain",
                    teamFileStorageMainFolder + reportDefinition.getFileNameText());
        }
        return true;
    }

    @Override
    public byte[] getStatisticsReport(String fileName) throws StatisticsReportNotFoundException, FileStorageException {

        String teamFileStorageMainFolder =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolder(), "/");
        try {
            return teamFileStorageService.getFile(teamFileStorageMainFolder + fileName);
        } catch (TeamFileNotFoundException e) {
            throw new StatisticsReportNotFoundException("Could not find statistics report {}", fileName);
        }
    }

    @Override
    public long getReportStartTimeNew(int offsetFromCurrentFullUnit, ChronoUnit unit) {

        switch (unit) {
            case DAYS:
                return timeService.getSameDayStart(timeService.nowLocal())
                        .plusDays(offsetFromCurrentFullUnit)
                        .toInstant()
                        .toEpochMilli();
            case WEEKS:
                return timeService.getSameWeekStart(timeService.nowLocal())
                        .plusWeeks(offsetFromCurrentFullUnit)
                        .toInstant()
                        .toEpochMilli();
            case MONTHS:
                return timeService.getSameMonthStart(timeService.nowLocal())
                        .plusMonths(offsetFromCurrentFullUnit)
                        .toInstant()
                        .toEpochMilli();
        }
        throw new IllegalArgumentException("Could not get start time new for: " + unit);
    }

    @Override
    public long getReportStartTimeActive(int offsetFromCurrentFullUnit, ChronoUnit unit) {

        switch (unit) {
            case DAYS:
                return timeService.getSameDayStart(timeService.nowLocal())
                        .plusDays(offsetFromCurrentFullUnit)
                        .minusDays(DEFAULT_ACTIVE_DAYS_BEFORE)
                        .toInstant()
                        .toEpochMilli();
            case WEEKS:
                return timeService.getSameWeekStart(timeService.nowLocal())
                        .plusWeeks(offsetFromCurrentFullUnit)
                        .minusDays(DEFAULT_ACTIVE_DAYS_BEFORE)
                        .toInstant()
                        .toEpochMilli();

            case MONTHS:
                return timeService.getSameMonthStart(timeService.nowLocal())
                        .plusMonths(offsetFromCurrentFullUnit)
                        .toInstant()
                        .toEpochMilli();
        }
        throw new IllegalArgumentException("Could not get start time active for: " + unit);
    }

    @Override
    public long getReportEndTime(ChronoUnit unit) {

        switch (unit) {
            case DAYS:
                return timeService.getPreviousDayEnd(timeService.nowLocal())
                        .toInstant()
                        .toEpochMilli();
            case WEEKS:
                return timeService.getPreviousWeekEnd(timeService.nowLocal())
                        .toInstant()
                        .toEpochMilli();
            case MONTHS:
                return timeService.getPreviousMonthEnd(timeService.nowLocal())
                        .toInstant()
                        .toEpochMilli();
        }
        throw new IllegalArgumentException("Could not get end time for: " + unit);
    }

    @Override
    public String getReportName(String prefix, int offsetFromCurrentFullUnit, ChronoUnit unit) {

        switch (unit) {
            case DAYS:
                ZonedDateTime dayEnd = timeService.getSameDayStart(timeService.nowLocal())
                        .plusDays(offsetFromCurrentFullUnit);
                return String.format("%1s_%2$tY-%2$tm-%2$td", prefix, dayEnd);
            case WEEKS:
                ZonedDateTime weekEnd = timeService.getSameWeekStart(timeService.nowLocal())
                        .plusWeeks(offsetFromCurrentFullUnit);
                return String.format("%1s_%2$04d-KW%3$02d",
                        prefix,
                        //this is not just the year, since around year change this value differs
                        //https://en.wikipedia.org/wiki/ISO_week_date
                        weekEnd.get(IsoFields.WEEK_BASED_YEAR),
                        weekEnd.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR));
            case MONTHS:
                ZonedDateTime monthStart = timeService.getSameMonthStart(timeService.nowLocal())
                        .plusMonths(offsetFromCurrentFullUnit);
                return String.format("%1s_%2$tY-%2$tm", prefix, monthStart);
        }
        throw new IllegalArgumentException("Could not get statistics name for: " + unit);
    }

    private String toLogMessage(FeatureConfig featureConfig,
            StatisticsCreationFeature.StatisticsCreationConfiguration definition) {

        return "CustomStatisticsCreationFeature" +
                (featureConfig.getTenant() != null ? "_" + featureConfig.getTenant().getId() : "") +
                (definition != null ? " (" + definition.getTeamFileStoragePath() + ")" : "");
    }

    private Collection<GeoArea> getDefinedGeoAreas(StatisticsReportDefinition statisticsDefinition) {
        if (statisticsDefinition.isAllGeoAreas()) {
            return geoAreaService.findAll();
        } else {
            final Set<GeoArea> includedRootAreas =
                    geoAreaService.findAllById(statisticsDefinition.getIncludedRootGeoAreaIds());
            return geoAreaService.addChildAreas(includedRootAreas, geoArea -> true, Integer.MAX_VALUE, false);
        }
    }

    private boolean canReportBeGeneratedNow(StatisticsReportDefinition reportDefinition) {
        final long currentTime = timeService.currentTimeMillisUTC();
        final long reportEndTime = reportDefinition.getEndTime();
        return Math.abs(currentTime - reportEndTime) <= ALLOWED_MAX_OFFSET_FROM_STATISTICS_REPORT_END_TIME.toMillis();
    }

}
