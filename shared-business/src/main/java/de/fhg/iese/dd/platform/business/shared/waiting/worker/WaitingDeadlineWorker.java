/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.worker;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.shared.waiting.events.WaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.services.IWaitingService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class WaitingDeadlineWorker implements IWorkerTask {

    @Autowired
    private IWaitingService waitingService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private EventBusAccessor eventBusAccessor;

    @Override
    public Trigger getTaskTrigger() {
        return new CronTrigger("42 * * * * *", timeService.getLocalTimeZone());//every minute at 42 seconds
    }

    @Override
    public void run() {

        //all expired deadlines based on their deadline
        List<WaitingDeadline> expiredDeadlines = waitingService.findAllExpiredDeadlines();

        for (WaitingDeadline expiredDeadline : expiredDeadlines) {
            try {
                WaitingExpiredEvent waitingExpiredEvent = waitingService.toWaitingExpiredEvent(expiredDeadline);
                WaitingExpiredEvent.WaitingDeadlineUpdate waitingDeadlineUpdate =
                        waitingService.getDeadlineUpdate(waitingExpiredEvent);
                eventBusAccessor.notifyTrigger(waitingExpiredEvent);
                waitingService.handleDeadlineExpired(expiredDeadline, waitingDeadlineUpdate);
            } catch (Exception e) {
                log.error("Failed to process expired deadline " + expiredDeadline.getWaitingEventName() + " (" +
                        expiredDeadline.getId() + ")", e);
            }
        }
    }

}
