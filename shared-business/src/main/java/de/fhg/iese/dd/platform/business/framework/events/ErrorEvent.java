/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Internal event used to distribute Errors during event processing.
 *
 * Do not instantiate, can not be received.
 *
 * @param <C>
 */
@Getter
@AllArgsConstructor
class ErrorEvent<C extends BaseEvent> {

    private final C causingEvent;
    private final Exception exception;
    private final EventBusAccessor.ErrorEventRoutingKey errorEventRoutingKey;

}
