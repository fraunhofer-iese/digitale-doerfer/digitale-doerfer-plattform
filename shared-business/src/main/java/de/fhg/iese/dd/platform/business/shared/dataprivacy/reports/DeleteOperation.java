/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

/**
 * Indicates how an entity has been handled during deletion
 */
public enum DeleteOperation {

    /**
     * Entity is deleted from database table
     */
    DELETED,

    /**
     * Entity data has been erased, i.e. columns with person-related data are either set to null or are overwritten with
     * random or empty data. The entity itself still exists in the table (with unchanged id). It is recommended to
     * add a deleted columns to those tables to mark entities as erased.
     */
    ERASED
}
