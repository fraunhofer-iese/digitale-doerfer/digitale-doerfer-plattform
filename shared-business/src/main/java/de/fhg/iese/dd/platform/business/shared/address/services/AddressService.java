/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2018 Axel Wickenkamp, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.address.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.address.exceptions.AddressInvalidException;
import de.fhg.iese.dd.platform.business.shared.geo.services.IGeoService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

@Service
class AddressService extends BaseService implements IAddressService {

    public static final String DEFAULT_UNKNOWN_ADDRESS_ID = "d6ee188a-ec2f-4448-b91d-281fcc20b663";
    private static final String[] KNOWN_UNKNOWN_LOCATION_STRING = {"unbekannt", "online"};

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private AddressListEntryRepository addressListEntryRepository;
    @Autowired
    private IGeoService geoService;

    @Override
    public void checkAddress(AddressDefinition address) throws AddressInvalidException {
        checkAddress(address, "address invalid");
    }

    @Override
    public void checkAddress(AddressDefinition address, String message) throws AddressInvalidException {
        if ( address == null ){
            throw new AddressInvalidException("{}: address is null", message).withDetail("address");
        }
        if (StringUtils.isEmpty(address.getName())) {
            throw new AddressInvalidException("{}: name is empty", message).withDetail("name");
        }
        if (StringUtils.isEmpty(address.getStreet())) {
            throw new AddressInvalidException("{}: street is empty", message).withDetail("street");
        }
        if (StringUtils.isEmpty(address.getZip())) {
            throw new AddressInvalidException("{}: zip is empty", message).withDetail("zip");
        }
        if (StringUtils.isEmpty(address.getCity())) {
            throw new AddressInvalidException("{}: city is empty", message).withDetail("city");
        }
    }

    private static boolean isKnownUnknownLocation(LocationDefinition ld) {
        return StringUtils.equalsAnyIgnoreCase(ld.getLocationName(), KNOWN_UNKNOWN_LOCATION_STRING) ||
                StringUtils.equalsAnyIgnoreCase(ld.getLocationLookupString(), KNOWN_UNKNOWN_LOCATION_STRING);
    }

    private static boolean hasLocationName(LocationDefinition ld) {
        return StringUtils.isNotEmpty(ld.getLocationName());
    }

    private static boolean hasLocationString(LocationDefinition ld) {
        return StringUtils.isNotEmpty(ld.getLocationLookupString());
    }

    private static boolean hasGpsLocation(LocationDefinition ld) {
        return ld.getGpsLocation() != null && ld.getGpsLocation().isValid();
    }

    private static boolean hasGpsLocation(Address address) {
        return address.getGpsLocation() != null && address.getGpsLocation().isValid();
    }

    private static boolean hasAllAddressData(LocationDefinition ld) {
        return StringUtils.isNotEmpty(ld.getAddressCity()) && StringUtils.isNotEmpty(ld.getAddressStreet()) &&
                StringUtils.isNotEmpty(ld.getAddressZip());
    }

    private static boolean hasAllAddressData(Address address) {
        return StringUtils.isNotEmpty(address.getStreet()) && StringUtils.isNotEmpty(address.getZip()) &&
                StringUtils.isNotEmpty(address.getCity());
    }

    private static boolean hasAllFields(Address address) {
        return StringUtils.isNotEmpty(address.getName()) && hasGpsLocation(address) && hasAllAddressData(address);
    }

    private static String createAddressLookupString(Address address) {
        return address.getStreet() + ", " + address.getZip() + " " + address.getCity();
    }

    private static Address createAddressFromNameAndGps(String name, GPSLocation location) {
        return Address.builder()
                .name(name)
                .gpsLocation(location)
                .verified(false)
                .build();
    }

    private static Address createAddressFromNameLocationStringAndGpsPosition(String name, String locationLookupString,
            GPSLocation gpsLocation) {
        return Address.builder()
                .name(name + ", " + locationLookupString)
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();
    }

    private Address findAddressWithGpsLocationForAddressWithoutGpsLocation(Address beautifiedAddress) {
        final List<Address> existingAddresses = addressRepository
                .findByNameAndStreetAndZipAndCityAndVerifiedTrueOrderByCreatedAsc(
                        beautifiedAddress.getName(),
                        beautifiedAddress.getStreet(),
                        beautifiedAddress.getZip(),
                        beautifiedAddress.getCity()
                );

        return existingAddresses.stream()
                .filter(a -> a.getGpsLocation() != null && a.getGpsLocation().isValid())
                .findFirst()
                .orElse(null);
    }

    private Address createAddressInDbIfNotExisting(Address address) {

        final List<Address> addressesInDb = addressRepository
                .findByNameAndStreetAndZipAndCityAndGpsLocationOrderByVerifiedDescCreatedAsc(
                        address.getName(),
                        address.getStreet(),
                        address.getZip(),
                        address.getCity(),
                        address.getGpsLocation()
                );
        if (addressesInDb.isEmpty()) {
            return addressRepository.save(address);
        } else {
            final Address addressFromDb = addressesInDb.get(0);
            //due to sort order, if there is a verified address, addressFromDb is verified
            if (address.isVerified() && !addressFromDb.isVerified()) {
                addressFromDb.setVerified(true);
                return addressRepository.save(addressFromDb);
            }
            return addressFromDb;
        }
    }

    @Override
    public Address findOrCreateAddress(LocationDefinition locationDefinition, boolean fullAddressRequired)
            throws AddressInvalidException {

        if (locationDefinition == null) {
            return null;
        }

        //integrity check
        if (!(hasLocationName(locationDefinition) || hasLocationString(locationDefinition) ||
                hasGpsLocation(locationDefinition) || hasAllAddressData(locationDefinition))) {
            throw new AddressInvalidException("Either the location name, or the location string, or the " +
                    "gps coordinates, or every address field must be non-empty.");
        }

        final Address addressFromLocationBeautified = beautifyAddress(Address.builder()
                .name(locationDefinition.getLocationName())
                .gpsLocation(locationDefinition.getGpsLocation())
                .street(locationDefinition.getAddressStreet())
                .zip(locationDefinition.getAddressZip())
                .city(locationDefinition.getAddressCity())
                .verified(false)
                .build());
        Address addressToReturn = null;
        boolean alreadyLookedUpInDatabase = false;

        //try resolution via address fields first
        if (hasAllAddressData(addressFromLocationBeautified)) {
            if (hasGpsLocation(addressFromLocationBeautified)) {
                addressToReturn = addressFromLocationBeautified;
            } else {
                addressToReturn = findAddressWithGpsLocationForAddressWithoutGpsLocation(addressFromLocationBeautified);
                if (addressToReturn != null) {
                    alreadyLookedUpInDatabase = true;
                } else {
                    addressToReturn = geoService.resolveLocation(addressFromLocationBeautified.getName(),
                            createAddressLookupString(addressFromLocationBeautified));
                    if (addressToReturn == null
                            && (hasLocationName(locationDefinition)
                            || !hasLocationName(locationDefinition) && !hasLocationString(locationDefinition))
                    ) {
                        //saving unverified address without GPS location
                        addressToReturn = addressFromLocationBeautified;
                    }
                }
            }
        }
        //if not resolved yet and not location string but gpsLocation, resolve by gpsLocation
        if (addressToReturn == null && hasGpsLocation(locationDefinition) && !hasLocationString(locationDefinition)) {
            addressToReturn = geoService.resolveLocation(addressFromLocationBeautified.getName(),
                    locationDefinition.getGpsLocation());
            if (addressToReturn == null) {
                addressToReturn = createAddressFromNameAndGps(addressFromLocationBeautified.getName(),
                        locationDefinition.getGpsLocation());
            }
        }
        //if not resolved yet, use name and locationLookupString fields
        if (addressToReturn == null) {
            if (isKnownUnknownLocation(locationDefinition)) {
                return getDefaultUnknownAddress();
            }
            if (hasLocationString(locationDefinition)) {
                if (hasLocationName(locationDefinition)) {
                    addressToReturn = geoService.resolveLocation(locationDefinition.getLocationName(),
                            locationDefinition.getLocationLookupString());
                    if (addressToReturn == null) {
                        addressToReturn = createAddressFromNameLocationStringAndGpsPosition(
                                addressFromLocationBeautified.getName(),
                                locationDefinition.getLocationLookupString(),
                                locationDefinition.getGpsLocation());
                    }
                } else { //no location name
                    addressToReturn = geoService.resolveLocation(null, locationDefinition.getLocationLookupString());
                    if (addressToReturn == null) {
                        addressToReturn = createAddressFromNameAndGps(locationDefinition.getLocationLookupString(),
                                locationDefinition.getGpsLocation());
                    }
                }
            } else {
                if (hasLocationName(locationDefinition)) {
                    //Note that in this case the gpsLocation is null. Otherwise it would have already been handled
                    //in the previous top level if
                    addressToReturn = geoService.resolveLocation(addressFromLocationBeautified.getName(),
                            locationDefinition.getLocationName());
                    if (addressToReturn == null) {
                        addressToReturn = createAddressFromNameAndGps(addressFromLocationBeautified.getName(), null);
                    }
                }
            }
        }

        //given GPS coordinates always override the found ones and makes the address unverified
        if (hasGpsLocation(locationDefinition)) {
            if (addressToReturn == null) {
                //addressToReturn can still be null if locationDefinition only contains GPS location
                addressToReturn = Address.builder().build();
            }
            addressToReturn.setGpsLocation(locationDefinition.getGpsLocation());
            //might be set to true again if existing, verified address is found in createAddressInDbIfNotExisting
            addressToReturn.setVerified(false);
        }

        if (addressToReturn == null) {
            throw new IllegalStateException("Internal error: Case handling in address resolution not exhaustive.");
        }

        if (fullAddressRequired && !hasAllFields(addressToReturn)) {
            throw new AddressInvalidException("Full address required, but could not retrieve all data. Resolved data: "
                            + addressToReturn.toHumanReadableString(true));
        }

        if (alreadyLookedUpInDatabase) {
            return addressToReturn;
        } else {
            return createAddressInDbIfNotExisting(beautifyAddress(addressToReturn));
        }
    }

    @Override
    public Address findOrCreateAddress(AddressDefinition addressDefinition, AddressFindStrategy addressFindStrategy,
            GPSResolutionStrategy gpsResolutionStrategy, boolean fullAddressRequired) {

        if(fullAddressRequired){
            checkAddress(addressDefinition);
        }

        AddressDefinition beautifiedAddressDefinition = beautifyAddressDefinition(addressDefinition);

        Address existingAddress;
        List<Address> existingAddresses;
        if (addressFindStrategy == AddressFindStrategy.NAME_STREET_ZIP_CITY_GPS &&
                beautifiedAddressDefinition.getGpsLocation() != null) {
            existingAddresses = addressRepository.
                    findByNameAndStreetAndZipAndCityAndGpsLocationLatitudeAndGpsLocationLongitudeOrderByCreatedAsc(
                            beautifiedAddressDefinition.getName(),
                            beautifiedAddressDefinition.getStreet(),
                            beautifiedAddressDefinition.getZip(),
                            beautifiedAddressDefinition.getCity(),
                            beautifiedAddressDefinition.getGpsLocation().getLatitude(),
                            beautifiedAddressDefinition.getGpsLocation().getLongitude());
        } else {
            existingAddresses = addressRepository.findByNameAndStreetAndZipAndCityOrderByCreatedAsc(
                    beautifiedAddressDefinition.getName(),
                    beautifiedAddressDefinition.getStreet(),
                    beautifiedAddressDefinition.getZip(),
                    beautifiedAddressDefinition.getCity());
        }

        if (!existingAddresses.isEmpty()) {
            existingAddress = existingAddresses.get(0);
            if (existingAddresses.size() > 1) {
                log.warn("Duplicate addresses ({}) found, taking oldest: '{}'", existingAddresses.size(),
                        existingAddress.toHumanReadableString(true));
            }
            //do not geocode the existing address again if it has no GPS!
            //if we work with an address that can't be geocoded we will fire a lot of geocode requests whenever we get this address from the client
            return existingAddress;
        } else {

            Address newAddress = Address.builder()
                    .name(beautifiedAddressDefinition.getName())
                    .street(beautifiedAddressDefinition.getStreet())
                    .zip(beautifiedAddressDefinition.getZip())
                    .city(beautifiedAddressDefinition.getCity())
                    .build();

            switch (gpsResolutionStrategy) {
                case LOOKEDUP_PROVIDED_NONE:
                    GPSLocation geocodedLocation = geoService.getGPSLocation(beautifiedAddressDefinition);
                    if (geocodedLocation != null) {
                        newAddress.setGpsLocation(geocodedLocation);
                        newAddress.setVerified(true);
                    } else {
                        newAddress.setGpsLocation(beautifiedAddressDefinition.getGpsLocation());
                    }
                    break;
                case LOOKEDUP_NONE:
                    newAddress.setGpsLocation(geoService.getGPSLocation(beautifiedAddressDefinition));
                    if (newAddress.getGpsLocation() != null) {
                        newAddress.setVerified(true);
                    }
                    break;
                case PROVIDED_LOOKEDUP_NONE:
                    if (beautifiedAddressDefinition.getGpsLocation() != null) {
                        newAddress.setGpsLocation(beautifiedAddressDefinition.getGpsLocation());
                    }else{
                        newAddress.setGpsLocation(geoService.getGPSLocation(beautifiedAddressDefinition));
                        if (newAddress.getGpsLocation() != null) {
                            newAddress.setVerified(true);
                        }
                    }
                    break;
            }
            return addressRepository.saveAndFlush(newAddress);
        }
    }

    @Override
    public AddressListEntry storeAddressListEntry(AddressListEntry addressListEntry) {
        return addressListEntryRepository.saveAndFlush(addressListEntry);
    }

    private AddressDefinition beautifyAddressDefinition(AddressDefinition uglyAddressDefinition) {

        GPSLocation gpsLocation;
        if (uglyAddressDefinition.getGpsLocation() != null && !uglyAddressDefinition.getGpsLocation().isValid()) {
            //we do not want to use invalid GPS locations, so we set it to null
            gpsLocation = null;
        } else {
            gpsLocation = uglyAddressDefinition.getGpsLocation();
        }

        return AddressDefinition.builder()
                .name(nice(uglyAddressDefinition.getName()))
                .street(normalizeStreet(uglyAddressDefinition.getStreet()))
                .zip(trim(uglyAddressDefinition.getZip()))
                .city(nice(uglyAddressDefinition.getCity()))
                .gpsLocation(gpsLocation)
                .build();
    }

    private Address beautifyAddress(Address uglyAddress) {
        //do not set the id of the new address, it should _never_ be chosen by the client that provides that address
        return Address.builder()
                .name(nice(uglyAddress.getName()))
                .street(normalizeStreet(uglyAddress.getStreet()))
                .zip(trim(uglyAddress.getZip()))
                .city(nice(uglyAddress.getCity()))
                .verified(uglyAddress.isVerified())
                .gpsLocation(uglyAddress.getGpsLocation())
                .build();
    }

    @Override
    public Multimap<Address, Address> findDuplicateAddresses(){
        List<Address> addresses = addressRepository.findAll();

        Multimap<String, Address> duplicateAddresses = ArrayListMultimap.create();

        for(Address address : addresses){
            duplicateAddresses.put(
                    address.getName()+"|"+address.getStreet()+"|"+address.getZip()+"|"+address.getCity(),
                    address);
        }

        Set<String> keys = new HashSet<>(duplicateAddresses.keySet());

        Multimap<Address, Address> mainToReplaceAddresses = ArrayListMultimap.create();

        for (final String key : keys) {
            Collection<Address> duplicates = duplicateAddresses.get(key);
            if(duplicates.size() < 2) {
                continue;
            }
            Address oldestAddress = Collections.min(duplicates, Comparator.comparingLong(Address::getCreated));

            Collection<Address> addressesToReplace = duplicates.stream()
                    .filter(a -> a != oldestAddress)
                    .collect(Collectors.toList());
            mainToReplaceAddresses.putAll(oldestAddress, addressesToReplace);

            log.warn("Duplicate Addresses: " + duplicates.size() + "\n" + duplicates.stream()
                    .map(Address::toString)
                    .collect(Collectors.joining("\n")));
        }

        return mainToReplaceAddresses;
    }

    private String normalizeStreet(String street) {
        if(street == null) {
            return null;
        }
        String normalizedStreet = nice(street);
        normalizedStreet = normalizedStreet.replace("Strasse", "Straße");
        normalizedStreet = normalizedStreet.replace("strasse", "straße");
        normalizedStreet = normalizedStreet.replace("str.", "straße");
        normalizedStreet = normalizedStreet.replace("Str.", "Straße");
        return normalizedStreet;
    }

    private String normalize(String ugly) {
        return ugly.trim().replaceAll("\\s+", " ");
    }

    private String trim(String ugly) {
        if (ugly == null) {
            return null;
        }
        return normalize(ugly);
    }

    private String nice(String ugly) {
        if (ugly == null) {
            return null;
        }
        return StringUtils.capitalize(normalize(ugly));
    }

    @Override
    public Address getDefaultUnknownAddress() {
        return addressRepository.findById(DEFAULT_UNKNOWN_ADDRESS_ID)
                .orElseThrow(() -> new RuntimeException("Default address not configured"));
    }

}
