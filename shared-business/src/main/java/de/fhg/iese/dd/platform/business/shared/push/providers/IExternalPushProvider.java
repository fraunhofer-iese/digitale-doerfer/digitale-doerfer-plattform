/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

public interface IExternalPushProvider {

    /**
     * Container for a push app.
     */
    @Getter
    @Setter
    @Builder
    class ExternalPushApp {

        private String pushAppId;
        private String pushAppName;

    }

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor
    @ToString
    class ExternalPushEndpointDescription {

        private String appVariantId;
        private String endpointId;
        private String token;
        private String platformType;
        private String creationDate;
        private String userInfo;

    }

    /**
     * Configuration for a push app.
     */
    @Getter
    @Setter
    @Builder
    class ExternalPushAppConfiguration {

        private String fcmApiKey;

        private byte[] apnsCertificate;
        private String apnsCertificatePassword;

        private byte[] apnsSandboxCertificate;
        private String apnsSandboxCertificatePassword;

        boolean isFcmSet() {
            return StringUtils.isNotEmpty(getFcmApiKey());
        }

        boolean isApnsSet() {
            return getApnsCertificate() != null && getApnsCertificate().length > 0;
        }

        boolean isApnsSandboxSet() {
            return getApnsSandboxCertificate() != null && getApnsSandboxCertificate().length > 0;
        }

        public String toLogInfo() {
            final List<String> logInfo = new ArrayList<>(3);
            if (isFcmSet()) {
                logInfo.add("fcm (" + StringUtils.abbreviate(getFcmApiKey(), 16) + ")");
            }
            if (isApnsSet()) {
                logInfo.add("apns");
            }
            if (isApnsSandboxSet()) {
                logInfo.add("apnsSandbox");
            }
            return String.join(", ", logInfo);
        }

        @Override
        public String toString() {
            return "ExternalPushAppConfiguration [" + toLogInfo() + "]";
        }
    }

    /**
     * lists all push apps that are configured in the external push provider. Those push apps are one-to-one mapped onto
     * app variants.
     *
     * @return
     */
    List<ExternalPushApp> listAllPushApps();

    ExternalPushApp createOrUpdatePushApp(AppVariant appVariant,
            ExternalPushAppConfiguration externalPushAppConfiguration);

    String getPushAppInfoForAppVariant(AppVariant appVariant);

    void registerPushEndpoint(
            String registrationToken,
            PushPlatformType platformType,
            AppVariant appVariant,
            Person person);

    long deleteAppVariant(AppVariant appVariant);

    Collection<ExternalPushEndpointDescription> getPushEndpoints(Person person, AppVariant appVariant);

    void deletePushEndpoint(String registrationToken, PushPlatformType platformType, AppVariant appVariant,
            Person person);

    void deletePushEndpoints(Person person, AppVariant appVariant);

    /**
     * Sends the message to all persons that have endpoints registered for the appVariants
     *  @param person
     * @param message
     * @param loudMessage
     */
    void sendMessageToPerson(Set<AppVariant> appVariants, Person person, PushMessage message,
            boolean loudMessage);

    /**
     * Sends a message to a list of persons. One can specify person ids to which the message is sent loud and
     * ids to which the message is sent silent.
     *
     * @param appVariant
     * @param personIdsLoudMessage
     * @param personIdsSilentMessage
     * @param message
     */
    void sendMessageToPersons(AppVariant appVariant, Collection<String> personIdsLoudMessage,
            Collection<String> personIdsSilentMessage, PushMessage message);

}
