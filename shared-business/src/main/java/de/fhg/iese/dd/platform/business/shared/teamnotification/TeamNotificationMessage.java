/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

@Data
@AllArgsConstructor
@Builder
public class TeamNotificationMessage {

    private long created;

    @Builder.Default
    private TeamNotificationPriority priority = TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER;

    private String title;
    private String text;
    @Singular
    private List<Pair<String, String>> links;

    public TeamNotificationPriority getPriority() {
        if (priority == null) {
            return TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER;
        }
        return priority;
    }

    public String getPlainText() {
        boolean titleEmpty = StringUtils.isEmpty(title);
        boolean linksEmpty = CollectionUtils.isEmpty(links);
        if (titleEmpty && linksEmpty) {
            return text;
        }
        StringBuilder plainText = new StringBuilder();
        if (!titleEmpty) {
            plainText.append("*").append(title).append("*").append("\n");
        }
        plainText.append(text);
        if (!linksEmpty) {
            plainText.append("\n");
            links.forEach(l -> plainText.append("[")
                    .append(l.getLeft())
                    .append("](")
                    .append(l.getRight())
                    .append(")")
                    .append("\n"));
        }
        return plainText.toString();
    }

    /**
     * Picked up by lombok as the base for the builder
     */
    @SuppressWarnings("unused")
    public static class TeamNotificationMessageBuilder {

        public TeamNotificationMessageBuilder message(String message, Object... arguments) {
            this.text = new ParameterizedMessage(message, arguments).getFormattedMessage();
            return this;
        }

    }

}
