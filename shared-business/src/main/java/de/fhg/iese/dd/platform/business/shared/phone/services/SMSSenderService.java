/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.phone.services;

import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.phone.exceptions.PhoneNumberInvalidException;
import de.fhg.iese.dd.platform.business.shared.phone.exceptions.SMSSenderException;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.regex.Pattern;

@Service
class SMSSenderService extends BaseService implements ISMSSenderService {

    private static final String TEAM_NOTIFICATION_TOPIC = "sms";
    //at least one of a-z or A-Z
    //1-11 of a-z or A-Z or 0-9 or blank
    private static final Pattern ALPHA_NUMERIC_SENDER_ID = Pattern.compile("^(?=.*[a-zA-Z])[a-zA-Z 0-9]{1,11}$");
    //starting with + followed by a non zero number
    //1-11 of 0-9
    private static final Pattern E164_NUMBER = Pattern.compile("^\\+[1-9][0-9]{1,14}$");

    @Autowired
    private ApplicationConfig config;
    @Autowired
    private ITeamNotificationService teamNotificationService;

    @PostConstruct
    private void initialize() {
        if (config.getTwilioSms().isEnabled()) {
            Twilio.init(config.getTwilioSms().getAccountSid(), config.getTwilioSms().getAuthToken());
        }
    }

    @Override
    public void sendSMS(String senderId, String recipientNumber, String text) throws SMSSenderException {
        if (!config.getTwilioSms().isEnabled()) {
            log.info("SMS is deactivated. Should be sent from '{}' to '{}' with text '{}'",
                    senderId, recipientNumber, text);
            teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER,
                    "SMS is deactivated. Should be sent from '{}' to '{}' with text '{}'",
                    senderId, recipientNumber, text);
            return;
        }
        if (!E164_NUMBER.matcher(recipientNumber).matches()) {
            throw new SMSSenderException("SMS from '{}' to '{}' can not be send: Recipient number is invalid '{}'",
                    senderId, recipientNumber, recipientNumber).withDetail("recipientNumber");
        }
        if (!ALPHA_NUMERIC_SENDER_ID.matcher(senderId).matches()) {
            throw new SMSSenderException("SMS from '{}' to '{}' can not be send: Sender id is invalid '{}'",
                    senderId, recipientNumber, senderId).withDetail("senderId");
        }
        if (text.length() > 140) {
            throw new SMSSenderException("SMS from '{}' to '{}' can not be send: Text too long '{}'",
                    senderId, recipientNumber, text).withDetail("text");
        }
        try {
            Message message = Message
                    .creator(new PhoneNumber(recipientNumber), new PhoneNumber(senderId), text)
                    .create();
            log.info("SMS was sent from '{}' to '{}' and message id '{}'",
                    senderId, recipientNumber, message.getSid());
            teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER,
                    "SMS was sent from '{}' to '{}' and message id '{}'",
                    senderId, recipientNumber, message.getSid());
            //the sid of the message can be used to check the status of the message
            //https://www.twilio.com/docs/sms/api/message-resource#fetch-a-message-resource
        } catch (Exception e) {
            throw new SMSSenderException(String.format(
                    "SMS can not be send: SMS Gateway returned error (%s) while sending SMS from '%s' to '%s' with text '%s'",
                    e.getMessage(), senderId, recipientNumber, text), e);
        }
    }

    @Override
    public PhoneNumberDetails validatePhoneNumber(String number) throws PhoneNumberInvalidException {
        return validateLocalPhoneNumber(number, null);
    }

    @Override
    public PhoneNumberDetails validateLocalPhoneNumber(String number, String countryCode)
            throws PhoneNumberInvalidException {

        if (!config.getTwilioSms().isEnabled()) {
            log.info("SMS is deactivated. Can not validate number '{}' with country code '{}'", number, countryCode);
            return PhoneNumberDetails.builder()
                    .number(number)
                    .localNumber(number)
                    .countryCode("DE")
                    .carrierName("DorfPhone")
                    .type(PhoneNumberDetails.PhoneNumberType.MOBILE)
                    .build();
        }
        String formattedNumber = StringUtils.remove(number, " ");

        String countryCodeOrNull = StringUtils.isBlank(countryCode) ? null : countryCode;
        try {
            com.twilio.rest.lookups.v1.PhoneNumber phoneNumber =
                    com.twilio.rest.lookups.v1.PhoneNumber.fetcher(formattedNumber)
                            .setType("carrier")
                            .setCountryCode(countryCodeOrNull)
                            .fetch();
            log.info("Phone number validation returned: {}", phoneNumber);
            return PhoneNumberDetails.builder()
                    .number(phoneNumber.getPhoneNumber().getEndpoint())
                    .localNumber(phoneNumber.getNationalFormat())
                    .countryCode(phoneNumber.getCountryCode())
                    .carrierName(String.valueOf(phoneNumber.getCarrier().get("name")))
                    .type(EnumUtils.getEnumIgnoreCase(PhoneNumberDetails.PhoneNumberType.class,
                            String.valueOf(phoneNumber.getCarrier().get("type"))))
                    .build();
        } catch (ApiException e) {
            if (e.getStatusCode() == 404) {
                throw new PhoneNumberInvalidException(formattedNumber, countryCodeOrNull);
            }
            throw e;
        }
    }

}
