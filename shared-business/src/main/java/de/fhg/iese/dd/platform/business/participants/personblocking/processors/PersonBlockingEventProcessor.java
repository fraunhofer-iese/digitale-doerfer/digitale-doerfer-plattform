/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.personblocking.processors;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonBlockConfirmation;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonBlockRequest;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonUnBlockConfirmation;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonUnBlockRequest;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import org.springframework.beans.factory.annotation.Autowired;

@EventProcessor
class PersonBlockingEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPersonBlockingService personBlockingService;

    @EventProcessing
    private PersonBlockConfirmation handlePersonBlockRequest(final PersonBlockRequest event) {

        personBlockingService.blockPerson(event.getApp(), event.getBlockingPerson(), event.getBlockedPerson());

        return PersonBlockConfirmation.builder()
                .app(event.getApp())
                .blockingPerson(event.getBlockingPerson())
                .blockedPerson(event.getBlockedPerson())
                .build();
    }

    @EventProcessing
    private PersonUnBlockConfirmation handlePersonUnBlockRequest(final PersonUnBlockRequest event) {

        personBlockingService.unblockPerson(event.getApp(), event.getBlockingPerson(), event.getBlockedPerson());

        return PersonUnBlockConfirmation.builder()
                .app(event.getApp())
                .blockingPerson(event.getBlockingPerson())
                .blockedPerson(event.getBlockedPerson())
                .build();
    }

}
