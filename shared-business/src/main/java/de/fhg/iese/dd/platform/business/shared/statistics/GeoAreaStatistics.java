/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class GeoAreaStatistics {

    @EqualsAndHashCode.Include
    private GeoArea geoArea;
    private Map<String, StatisticsValue<?>> statisticsValues = new HashMap<>();

    public GeoAreaStatistics(GeoArea geoArea) {
        this.geoArea = geoArea;
    }

    public GeoAreaStatistics cloneAndFilter(Set<String> statisticsIds) {
        GeoAreaStatistics cloned = new GeoAreaStatistics(this.geoArea);
        cloned.statisticsValues = statisticsValues.entrySet().stream()
                .filter(e -> statisticsIds.contains(e.getValue().getId()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return cloned;
    }

    public void setStatisticsValue(StatisticsValue<?> value) {
        statisticsValues.put(toValueIdentifier(value.getId(), value.getTimeRelation()), value);
    }

    @SuppressWarnings("unchecked")
    public void setStatisticsValueLong(String id, StatisticsTimeRelation timeRelation, long value) {
        final StatisticsValue<?> statisticsValue = getStatisticsValue(id, timeRelation);
        if (statisticsValue != null) {
            ((StatisticsValue<Long>) statisticsValue).setValue(value);
        } else {
            setStatisticsValue(new StatisticsValue<>(id, timeRelation, value));
        }
    }

    @SuppressWarnings("unchecked")
    public void setStatisticsValueString(String id, StatisticsTimeRelation timeRelation, String value) {
        final StatisticsValue<?> statisticsValue = getStatisticsValue(id, timeRelation);
        if (statisticsValue != null) {
            ((StatisticsValue<String>) statisticsValue).setValue(value);
        } else {
            setStatisticsValue(new StatisticsValue<>(id, timeRelation, value));
        }
    }
    
    @SuppressWarnings("unchecked")
    public void setStatisticsValueDouble(String id, StatisticsTimeRelation timeRelation, double value) {
        final StatisticsValue<?> statisticsValue = getStatisticsValue(id, timeRelation);
        if (statisticsValue != null) {
            ((StatisticsValue<Double>) statisticsValue).setValue(value);
        } else {
            setStatisticsValue(new StatisticsValue<>(id, timeRelation, value));
        }
    }

    public StatisticsValue<?> getStatisticsValue(String id, StatisticsTimeRelation timeRelation) {
        return statisticsValues.get(toValueIdentifier(id, timeRelation));
    }

    public long getStatisticsValueLong(String id, StatisticsTimeRelation timeRelation) {
        final StatisticsValue<?> statisticsValue = getStatisticsValue(id, timeRelation);
        if (statisticsValue != null) {
            return (Long) statisticsValue.getValue();
        } else {
            return 0;
        }
    }

    public Object[] getValues(List<Pair<StatisticsMetaData, StatisticsTimeRelation>> idsAndTimeRelations) {
        return idsAndTimeRelations.stream()
                .map(idAndTimeRelation ->
                        getStatisticsValue(idAndTimeRelation.getKey().getId(), idAndTimeRelation.getValue()))
                .map(v -> v == null ? "" : v.getValue())
                .toArray();
    }

    private String toValueIdentifier(String id, StatisticsTimeRelation timeRelation) {
        return id + timeRelation;
    }

}
