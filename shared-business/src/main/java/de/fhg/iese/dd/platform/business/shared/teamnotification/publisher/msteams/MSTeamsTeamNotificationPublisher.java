/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.msteams;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectReader;

import de.fhg.iese.dd.platform.business.shared.teamnotification.BaseHttpTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

class MSTeamsTeamNotificationPublisher extends BaseHttpTeamNotificationPublisher {

    private final Map<String, String> channelNameToWebhookUrl;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class MSTeamsConnectionConfig {

        @Singular
        private List<MSTeamsConnectionChannelConfig> channelConfigs;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class MSTeamsConnectionChannelConfig {

        private String channelName;
        private String webhookUrl;

    }

    MSTeamsTeamNotificationPublisher(ITimeService timeService, RestTemplateBuilder restTemplateBuilder,
            TeamNotificationConnection connection) {
        super(timeService, restTemplateBuilder, connection);
        MSTeamsConnectionConfig connectionConfig = readConnectionConfiguration(connection);
        channelNameToWebhookUrl = connectionConfig.channelConfigs.stream()
                .collect(Collectors.toMap(MSTeamsConnectionChannelConfig::getChannelName,
                        MSTeamsConnectionChannelConfig::getWebhookUrl));
    }

    static MSTeamsConnectionConfig readConnectionConfiguration(TeamNotificationConnection connection) {
        MSTeamsConnectionConfig connectionConfig;
        try {
            ObjectReader connectionConfigReader = defaultJsonReader().forType(MSTeamsConnectionConfig.class);
            connectionConfig = connectionConfigReader.readValue(connection.getConnectionConfig());
        } catch (IOException e) {
            throw new TeamNotificationConnectionConfigInvalidException("Connection config invalid in {}: {}",
                    connection, e.getMessage());
        }
        if (connectionConfig == null) {
            throw new TeamNotificationConnectionConfigInvalidException("No connection config in {}", connection);
        }
        if (CollectionUtils.isEmpty(connectionConfig.getChannelConfigs())) {
            throw new TeamNotificationConnectionConfigInvalidException("No channel configs in connection config in {}",
                    connection);
        }
        return connectionConfig;
    }

    @Override
    protected void processSendRequest(TeamNotificationSendRequest request) throws Exception {

        String webhookUrl = channelNameToWebhookUrl.get(request.getChannel());
        if (StringUtils.isEmpty(webhookUrl)) {
            throw new RuntimeException("Failed to find webhook for channel '" + request.getChannel() + "'");
        }
        TeamNotificationMessage message = request.getMessage();

        MSTeamsMessageRequest request1 = MSTeamsMessageRequest.builder()
                .title(message.getTitle())
                .text(message.getText())
                .build();

        if (!CollectionUtils.isEmpty(message.getLinks())) {
            request1.setLinkActions(message.getLinks().stream()
                    .map(l -> new MSTeamsMessageRequest.LinkAction(l.getLeft(), l.getRight()))
                    .collect(Collectors.toList()));
        }

        if (message.getPriority().getLogLevel().isMoreSpecificThan(Level.ERROR)) {
            request1.setThemeColor("#de2828");
        } else if (message.getPriority().getLogLevel().isMoreSpecificThan(Level.WARN)) {
            request1.setThemeColor("#edc542");
        } else {
            request1.setThemeColor("#3e9431");
        }

        final HttpHeaders headers = getJsonHeaders();
        HttpEntity<MSTeamsMessageRequest> entity = new HttpEntity<>(request1, headers);
        try {
            ResponseEntity<String> response = getRestTemplate().postForEntity(webhookUrl, entity, String.class);
            if (!response.getStatusCode().is2xxSuccessful()) {
                log.error("Could not push to MS Teams: service returned != OK");
                throw new RuntimeException(
                        "Failed to send message, service returned " + response.getStatusCode() + ": " + response);
            }
        } catch (HttpClientErrorException ex) {
            log.error("Client error, adjust request {}", ex.toString());
            throw ex;
        } catch (
                HttpServerErrorException ex) {
            throw new RuntimeException("Failed to send message, service returned " + ex);
        }

    }

}
