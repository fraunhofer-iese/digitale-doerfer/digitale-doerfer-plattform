/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.dataprivacy;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ISharedDataDeletionService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushRegistrationService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;

@Component
public class PushDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(App.class, AppVariant.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(PushCategoryUserSetting.class);

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Autowired
    protected IAppService appService;

    @Autowired
    protected IPushCategoryService pushCategoryService;

    @Autowired
    protected IPushRegistrationService pushRegistrationService;

    @Autowired
    protected ISharedDataDeletionService sharedDataDeletionService;

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Push", "Push-Einstellungen");

        List<AppVariantUsage> appVariantUsages = appService.getAppVariantUsages(person);
        for (AppVariantUsage appVariantUsage : appVariantUsages) {
            AppVariant appVariant = appVariantUsage.getAppVariant();

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(appVariant.getName(), "Push-Einstellungen");

            List<PushCategoryUserSetting> pushCategoryUserSettings =
                    pushCategoryService.getConfigurableAndVisiblePushCategoryUserSettings(person, appVariant);

            String userSettings = pushCategoryUserSettings.stream()
                    .map(us -> us.getPushCategory().getName() + ": " + (us.isLoudPushEnabled() ? "laut" : "leise"))
                    .collect(Collectors.joining("\n"));

            subSection.newContent("Benachrichtigungen", userSettings);

        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        try {
            pushRegistrationService.deleteAllEndpoints(person);
        } catch (Exception e) {
            log.error("Failed to delete endpoints via PushRegistrationService", e);
        }

        List<PushCategoryUserSetting> userSettings =
                pushCategoryService.findAllPushCategoryUserSettings(person);

        DeleteOperation deleteOperationUserSettings =
                sharedDataDeletionService.deletePushCategoryUserSettings(userSettings);

        personDeletionReport.addEntries(userSettings, deleteOperationUserSettings);

    }

}
