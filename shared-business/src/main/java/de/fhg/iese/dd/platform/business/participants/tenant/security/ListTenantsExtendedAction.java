/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Danielle Korth
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.security;

import java.util.Collection;
import java.util.Collections;

import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalConfigurationAdmin;

public class ListTenantsExtendedAction extends ActionTenantRestricted {

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_ALL_TENANTS_ALLOWED =
            Collections.singletonList(GlobalConfigurationAdmin.class);

    @Override
    public String getActionDescription() {
        return "Lists all configured tenants and their details.\n" +
                "The listed tenants contain all fields of ClientTenantExtended.";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllTenantsAllowed() {
        return ROLES_ALL_TENANTS_ALLOWED;
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificTenantsAllowed() {
        return Collections.emptyList();
    }

}
