/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.callback.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.callback.exceptions.ExternalSystemCallFailedException;
import de.fhg.iese.dd.platform.business.shared.callback.providers.IWebClientProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileDownloadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.unit.DataSize;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
class ExternalSystemCallbackService extends BaseService implements IExternalSystemCallbackService, IFileDownloadService {

    private static final Collection<String> ALLOWED_DOWNLOAD_URL_PROTOCOLS = List.of("http", "https");
    private static final String HEADER_NAME_API_KEY = "apiKey";

    @Autowired
    private IWebClientProvider webClientProvider;

    private WebClient webClient;

    private WebClient getWebClient() {
        if (webClient == null) {
            webClient = webClientProvider.createWebClient();
        }
        return webClient;
    }

    @Override
    public <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, Class<T> bodyClass) throws ExternalSystemCallFailedException {

        return callExternalSystem(appVariant, method, urlSuffix, null, bodyClass);
    }

    @Override
    public <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, Object requestBodyObject, Class<T> bodyClass) throws ExternalSystemCallFailedException {

        String requestBody;
        try {
            requestBody = JsonMapping.defaultJsonWriter().writeValueAsString(requestBodyObject);
        } catch (JsonProcessingException e) {
            throw new ExternalSystemCallFailedException(
                    "Failed to call external callback url of " + appVariant + ": json could not be mapped", e);
        }
        return callExternalSystem(appVariant, method, urlSuffix, requestBody, bodyClass);
    }

    @Override
    public <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, String requestBody, Class<T> bodyClass) throws ExternalSystemCallFailedException {

        String baseUrl = appVariant.getCallBackUrl();
        if (StringUtils.isBlank(baseUrl)) {
            throw new ExternalSystemCallFailedException("{} does not define a callback url", appVariant);
        }
        final URI uri = UriComponentsBuilder.fromUriString(baseUrl).path(urlSuffix).build().toUri();

        return callExternalSystem(appVariant, method, uri, requestBody, bodyClass);
    }

    @Override
    public <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method, URI uri,
                                                            String requestBody, Class<T> bodyClass) throws ExternalSystemCallFailedException {

        WebClient.RequestBodySpec request = getWebClient()
                .method(method)
                .uri(uri);
        ExternalSystemResponse<T> response = ExternalSystemResponse.<T>builder()
                .requestMethod(method)
                .requestURI(uri)
                .build();
        if (appVariant != null) {
            if (StringUtils.isNotBlank(appVariant.getExternalApiKey())) {
                request = request.header(HEADER_NAME_API_KEY, appVariant.getExternalApiKey());
                response.addRequestHeader(HEADER_NAME_API_KEY, appVariant.getExternalApiKey());
            }
            if (StringUtils.isNotBlank(appVariant.getExternalBasicAuth())) {
                final String username = appVariant.getExternalBasicAuthUsername();
                final String password = appVariant.getExternalBasicAuthPassword();
                request.headers(httpHeaders -> httpHeaders.setBasicAuth(username, password));
                response.addRequestHeader("Authorization", "Basic " + appVariant.getExternalBasicAuth());
            }
        }
        try {
            ResponseEntity<T> responseEntity;
            if (StringUtils.isEmpty(requestBody)) {
                responseEntity = request.retrieve()
                        .onStatus(HttpStatus::isError, clientResponse -> Mono.empty())
                        .toEntity(bodyClass)
                        .block();
            } else {
                responseEntity = request.body(Mono.just(requestBody), String.class)
                        .retrieve()
                        .onStatus(HttpStatus::isError, clientResponse -> Mono.empty())
                        .toEntity(bodyClass)
                        .onErrorStop()
                        .block();
            }
            response.setResponse(responseEntity);
            return response;
        } catch (Exception e) {
            throw new ExternalSystemCallFailedException(
                    "Failed to call external system " +
                            (appVariant != null
                                    ? appVariant
                                    : "") + ": URI '" + uri + "'", e);
        }
    }

    @Override
    public byte[] downloadFromExternalSystem(URL url, DataSize minSize, DataSize maxSize,
                                             List<String> allowedMediaTypes) throws FileDownloadException {

        checkFileDownloadable(url);

        // basic auth is extracted separately, so that it can be sent via header
        final String encodedBasicAuth;
        if (StringUtils.isNotEmpty(url.getUserInfo())) {
            String[] userInfo = StringUtils.split(url.getUserInfo(), ":");
            if (userInfo.length == 2) {
                encodedBasicAuth = HttpHeaders.encodeBasicAuth(userInfo[0], userInfo[1], null);
            } else {
                encodedBasicAuth = null;
            }
        } else {
            encodedBasicAuth = null;
        }
        URI uri;
        try {
            //we do not change the given url, we only remove the user info
            try {
                //we assume first that the url is encoded
                uri = UriComponentsBuilder.fromUri(url.toURI())
                        .userInfo(null)
                        .build(true)
                        .toUri();
            } catch (IllegalArgumentException ignored) {
                //it seems the url is not encoded, so we try to encode it
                uri = UriComponentsBuilder.fromUri(url.toURI())
                        .userInfo(null)
                        .build(false)
                        .toUri();
            }
        } catch (URISyntaxException | IllegalArgumentException e) {
            throw FileDownloadException.notDownloadableForException(url, e);
        }

        try {
            //get the headers first, so that we can check first if the file is valid
            HttpHeaders headHeaders = getWebClient().head()
                    .uri(uri)
                    .headers(h -> {
                        if (encodedBasicAuth != null) {
                            h.setBasicAuth(encodedBasicAuth);
                        }
                    })
                    .retrieve()
                    .toBodilessEntity()
                    .map(HttpEntity::getHeaders)
                    .block();
            if (headHeaders == null) {
                throw FileDownloadException.notDownloadableForReason(url, "head returned null");
            }
            checkHeaders(url, headHeaders, minSize, maxSize, allowedMediaTypes);

            //download the actual file
            ResponseEntity<byte[]> mainResponse = getWebClient().get()
                    .uri(uri)
                    .headers(h -> h.setAccept(MediaType.parseMediaTypes(allowedMediaTypes)))
                    .headers(h -> {
                        if (encodedBasicAuth != null) {
                            h.setBasicAuth(encodedBasicAuth);
                        }
                    })
                    .retrieve()
                    .toEntity(byte[].class)
                    .block();
            if (mainResponse == null) {
                throw FileDownloadException.notDownloadableForReason(url, "get returned null");
            }
            checkHeaders(url, mainResponse.getHeaders(), minSize, maxSize, allowedMediaTypes);

            byte[] responseBytes = mainResponse.getBody();

            long contentLength = (responseBytes == null) ? 0 : responseBytes.length;

            if (contentLength < minSize.toBytes()) {
                throw FileDownloadException.notDownloadableForTooSmall(url, contentLength, minSize);
            }
            if (contentLength > maxSize.toBytes()) {
                throw FileDownloadException.notDownloadableForTooBig(url, contentLength, maxSize);
            }
            return responseBytes;
        } catch (WebClientResponseException e) {
            throw FileDownloadException.notDownloadableForException(url, e);
        }
    }

    private void checkHeaders(URL url, HttpHeaders headHeaders, DataSize minSize, DataSize maxSize,
            List<String> allowedMediaTypes) {

        //if we didn't get the content length we ignore it, since it is valid to not deliver it
        // see https://www.rfc-editor.org/rfc/rfc9110.html#name-content-length
        String contentLengthString = headHeaders.getFirst(HttpHeaders.CONTENT_LENGTH);
        Long contentLength = StringUtils.isNumeric(contentLengthString)
                ? Long.parseLong(contentLengthString)
                : null;
        if (contentLength != null) {
            if (contentLength < minSize.toBytes()) {
                throw FileDownloadException.notDownloadableForTooSmall(url, contentLength, minSize);
            }
            if (contentLength > maxSize.toBytes()) {
                throw FileDownloadException.notDownloadableForTooBig(url, contentLength, maxSize);
            }
        }

        MediaType contentType = headHeaders.getContentType();
        String mediaType = Objects.toString(contentType);
        if (!allowedMediaTypes.contains(mediaType)) {
            throw FileDownloadException.notDownloadableForUnsupportedFileType(url, mediaType, allowedMediaTypes);
        }
    }

    @Override
    public URL createUrlAndCheck(String rawURL) throws FileDownloadException {

        URL url;
        URI uri;
        try {
            //we assume first that the url is encoded
            uri = UriComponentsBuilder.fromHttpUrl(rawURL)
                    .build(true)
                    .toUri();
        } catch (IllegalArgumentException ignored) {
            try {
                //it seems the url is not encoded, so we try to encode it
                uri = UriComponentsBuilder.fromHttpUrl(rawURL)
                        .build(false)
                        .toUri();
            } catch (IllegalArgumentException e) {
                throw FileDownloadException.notDownloadableForInvalidUrl(rawURL, e);
            }
        }
        try {
            url = uri.toURL();
        } catch (MalformedURLException | IllegalArgumentException e) {
            throw FileDownloadException.notDownloadableForInvalidUrl(rawURL, e);
        }
        checkFileDownloadable(url);
        return url;
    }

    private void checkFileDownloadable(URL url) throws FileDownloadException {

        String protocol = url.getProtocol();

        if (!ALLOWED_DOWNLOAD_URL_PROTOCOLS.contains(protocol)) {
            throw FileDownloadException.notDownloadableForUnsupportedProtocol(url);
        }
    }

}
