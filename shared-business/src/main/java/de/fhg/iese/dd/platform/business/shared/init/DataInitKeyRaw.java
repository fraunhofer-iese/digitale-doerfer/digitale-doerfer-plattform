/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.List;

import org.springframework.lang.Nullable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Used in data init files to determine where the {@link #values} should be used. When running data init this raw target
 * is combined into a {@link DataInitKey} which references actual entities (app, app variant, geo area).
 *
 * @param <T> the type of entities that are in the data init file. Only one type can be used in a data init file.
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"fileName", "values"})
class DataInitKeyRaw<T> {

    @Nullable
    private String appIdentifier;
    @Nullable
    private String appVariantIdentifier;
    @Nullable
    private String geoAreaId;
    @Nullable
    private List<String> geoAreaIdsIncluded;
    @Nullable
    private List<String> geoAreaIdsExcluded;
    private boolean geoAreaChildrenIncluded;
    /**
     * This should not be set in the data init files, it is automatically set based on the file name
     */
    @Nullable
    private String tenantId;
    /**
     * This is only set internally and can not be set in the data init files
     */
    @Nullable
    private String tenantTag;
    /**
     * This is only set internally and can not be set in the data init files
     */
    @Nullable
    private String environmentIdentifier;
    /**
     * This is only set internally and can not be set in the data init files
     */
    @Nullable
    private String moduleName;
    /**
     * This is only set internally and can not be set in the data init files
     */
    @Nullable
    private String fileName;

    private List<T> values;

}
