/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.processors;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCancelChangeEmailAddressConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCancelChangeEmailAddressRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeHomeAreaConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeHomeAreaRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeHomeTenantConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeHomeTenantRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeStatusConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeStatusRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonExtendedUpdateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonExtendedUpdateRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonProfilePictureChangeConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonProfilePictureChangeRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonProfilePictureDeleteConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonProfilePictureDeleteRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUpdateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUpdateRequest;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonInformationInvalidException;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonResetLastLoggedInConfirmation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonResetLastLoggedInRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.SharedConstants;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@EventProcessor
class PersonEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPersonService personService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private ParticipantsConfig participantsConfig;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IAppService appService;

    @EventProcessing
    private PersonCreateConfirmation handlePersonCreateRequest(PersonCreateRequest request) {

        final GeoArea homeArea = request.getHomeArea();
        final Tenant homeTenant;

        if (homeArea != null) {
            personService.checkIsValidHomeArea(homeArea);
            homeTenant = homeArea.getTenant();
        } else {
            homeTenant = request.getHomeTenant();
        }
        if (homeTenant == null) {
            throw PersonInformationInvalidException.forMissingBothHomeAreaAndHomeTenant();
        }

        Person person = Person.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .nickName(StringUtils.isNotBlank(request.getNickName()) ? request.getNickName() : null)
                .phoneNumber(StringUtils.isNotBlank(request.getPhoneNumber()) ? request.getPhoneNumber() : null)
                .email(request.getEmail())
                .accountType(AccountType.OAUTH)
                .oauthId(request.getOauthId())
                .lastLoggedIn(timeService.currentTimeMillisUTC())
                .community(homeTenant)
                .homeArea(homeArea)
                .build();

        person = personService.create(person);

        if (request.getAddress() != null) {
            personService.setDefaultAddress(person,
                    IAddressService.AddressDefinition.fromAddress(request.getAddress()));
        }

        log.debug("Created person {}", person.getId());
        return new PersonCreateConfirmation(person);
    }

    @EventProcessing
    private PersonUpdateConfirmation handlePersonUpdateRequest(PersonUpdateRequest request) {

        Person person = applyUpdateRequest(request);
        person = personService.store(person);

        log.debug("Updated person {}", person.getId());
        return new PersonUpdateConfirmation(person);
    }

    @EventProcessing
    private List<BaseEvent> handlePersonExtendedUpdateRequest(PersonExtendedUpdateRequest request) {

        List<BaseEvent> confirmations = new ArrayList<>(2);

        Person person = applyUpdateRequest(request);

        GeoArea homeArea = request.getHomeArea();
        if (homeArea != null) {
            personService.checkIsValidHomeArea(homeArea);
            if (!Objects.equals(person.getHomeArea(), homeArea)) {
                //only if there is a new home area the event is triggered
                person.setHomeArea(homeArea);
                person.setTenant(homeArea.getTenant());
                confirmations.add(PersonChangeHomeAreaConfirmation.builder()
                        .person(person)
                        .newHomeArea(homeArea)
                        .build());
            }
        }

        if (request.getProfilePicture() == null) {
            person.setProfilePicture(null);
        } else {
            if (request.getProfilePicture().getTemporaryMediaItem() != null) {
                final AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(
                        SharedConstants.PLATFORM_APP_VARIANT_IDENTIFIER);
                MediaItem newProfilePicture =
                        mediaItemService.useItem(request.getProfilePicture().getTemporaryMediaItem(),
                                FileOwnership.of(person, appVariant));
                person.setProfilePicture(newProfilePicture);
            }
        }

        person = personService.store(person);

        log.debug("Updated person {}", person.getId());
        confirmations.add(new PersonExtendedUpdateConfirmation(person));
        return confirmations;
    }

    private Person applyUpdateRequest(PersonUpdateRequest request) {

        Person person = request.getPerson();

        person.setFirstName(request.getFirstName());
        person.setLastName(request.getLastName());
        if (StringUtils.isNotEmpty(request.getNickName())) {
            person.setNickName(request.getNickName());
        } else {
            person.setNickName(null);
        }
        if (StringUtils.isNotEmpty(request.getPhoneNumber())) {
            person.setPhoneNumber(request.getPhoneNumber());
        } else {
            person.setPhoneNumber(null);
        }
        return person;
    }

    @EventProcessing
    private PersonChangeEmailAddressConfirmation handlePersonChangeEmailAddressRequest(
            PersonChangeEmailAddressRequest request) {

        Person person = request.getPerson();
        final String newEmail = request.getNewEmailAddress();

        // check wait time for changing email address regarding number of email changes
        int numberOfEmailChanges = Optional.ofNullable(person.getNumberOfEmailChanges()).orElse(0);
        if (person.getLastSentTimeEmailVerification() != null) {
            if (numberOfEmailChanges >= 10) {
                checkEmailChangeInterval(person.getLastSentTimeEmailVerification(),
                        participantsConfig.getEmailChangeIntervalLong().toMillis());
            } else if (numberOfEmailChanges >= 3) {
                checkEmailChangeInterval(person.getLastSentTimeEmailVerification(),
                        participantsConfig.getEmailChangeIntervalShort().toMillis());
            }
        }

        // if old email address was already verified, just set pending new email and do not change the current email
        // address yet
        if (person.isEmailVerified()) {
            person.setNumberOfEmailChanges(++numberOfEmailChanges);
            person.setPendingNewEmail(newEmail);
            person = personService.store(person);
        } else {
            person.setNumberOfEmailChanges(++numberOfEmailChanges);
            // if not, the old email address can be replaced by the one immediately
            person = personService.changeEmailAddress(person, newEmail);
        }
        return new PersonChangeEmailAddressConfirmation(person);
    }

    private void checkEmailChangeInterval(long lastSentTimeEmailVerification, long emailChangeInterval) {

        final long nextPossibleTimeStamp = lastSentTimeEmailVerification + emailChangeInterval;
        if (timeService.currentTimeMillisUTC() < nextPossibleTimeStamp) {
            throw EMailChangeNotPossibleException.forTooManyChanges(
                    timeService.toLocalTimeHumanReadable(nextPossibleTimeStamp));
        }
    }

    @EventProcessing
    private PersonChangeEmailAddressConfirmation handlePersonChangeEmailAddressByAdminRequest(
            PersonChangeEmailAddressByAdminRequest request) {

        Person person = request.getPerson();
        final String newEmail = request.getNewEmailAddress();
        boolean newEmailVerified = request.isNewEmailAddressVerified();

        if (newEmailVerified) {
            person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        } else {
            person.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        }

        person = personService.changeEmailAddress(person, newEmail);
        return new PersonChangeEmailAddressConfirmation(person);
    }

    @EventProcessing
    private PersonCancelChangeEmailAddressConfirmation handlePersonCancelChangeEmailAddressRequest(
            PersonCancelChangeEmailAddressRequest request) {

        Person person = request.getPerson();
        person.setPendingNewEmail(null);
        person = personService.store(person);

        return new PersonCancelChangeEmailAddressConfirmation(person);
    }

    @EventProcessing
    private PersonChangeHomeTenantConfirmation handlePersonChangeHomeTenantRequest(
            PersonChangeHomeTenantRequest request) {

        Person person = request.getPerson();
        final Tenant oldHomeTenant = person.getTenant();
        final Tenant newHomeTenant = request.getNewHomeTenant();

        person.setTenant(newHomeTenant);
        person = personService.store(person);

        log.debug("Changed tenant: person '{}', old tenant {}, new tenant {}",
                person.getId(), BaseEntity.getIdOf(oldHomeTenant), BaseEntity.getIdOf(newHomeTenant));
        return PersonChangeHomeTenantConfirmation.builder()
                .person(person)
                .oldHomeTenant(oldHomeTenant)
                .newHomeTenant(newHomeTenant).build();
    }

    @EventProcessing
    private PersonChangeHomeAreaConfirmation handlePersonChangeHomeAreaRequest(
            final PersonChangeHomeAreaRequest request) {

        Person person = request.getPerson();
        final GeoArea oldHomeArea = person.getHomeArea();
        final GeoArea newHomeArea = request.getNewHomeArea();

        personService.checkIsValidHomeArea(newHomeArea);

        person.setTenant(newHomeArea.getTenant());
        person.setHomeArea(newHomeArea);
        person = personService.store(person);

        log.debug("Changed home area: person '{}', old geo area {}, new geo area {}",
                person.getId(), BaseEntity.getIdOf(oldHomeArea), BaseEntity.getIdOf(newHomeArea));
        return PersonChangeHomeAreaConfirmation.builder()
                .person(person)
                .newHomeArea(newHomeArea)
                .build();
    }

    @EventProcessing
    private PersonResetLastLoggedInConfirmation handlePersonResetLastLoggedInRequest(
            final PersonResetLastLoggedInRequest request) {

        Person updatedPerson = personService.updateLastLoggedIn(request.getPerson()).getRight();
        return new PersonResetLastLoggedInConfirmation(updatedPerson);
    }

    /**
     * Handles both {@link PersonChangeEmailAddressVerificationStatusRequest} and
     * {@link PersonChangeEmailAddressVerificationStatusByAdminRequest}. In both cases
     * {@link PersonChangeEmailAddressVerificationStatusConfirmation} is returned.
     */
    @EventProcessing(includeSubtypesOfEvent = true)
    private PersonChangeEmailAddressVerificationStatusConfirmation handlePersonChangeEmailAddressVerificationStatusRequest(
            final PersonChangeEmailAddressVerificationStatusRequest request) {

        boolean emailAddressVerified;
        if (request instanceof PersonChangeEmailAddressVerificationStatusByAdminRequest) {
            emailAddressVerified =
                    ((PersonChangeEmailAddressVerificationStatusByAdminRequest) request).isEmailAddressVerified();
        } else {
            emailAddressVerified = true;
        }

        Person person = request.getPerson();
        // if a pending new email exists, and the verification is for the new pending email
        // --> the current email is changed to the pending new email
        boolean emailAddressChanged = false;
        if (!StringUtils.isEmpty(person.getPendingNewEmail()) &&
                StringUtils.equals(request.getEmailAddress(), person.getPendingNewEmail())) {
            final String newEmail = person.getPendingNewEmail();
            person.setEmail(newEmail);
            person.setPendingNewEmail(null);
            emailAddressChanged = true;
        }

        final Pair<Boolean, Person> personPair =
                personService.updateEmailAddressVerificationStatus(person, emailAddressVerified);
        final Person updatedPerson = personPair.getRight();
        final boolean verificationStatusChanged = personPair.getLeft();

        log.debug("Email address of person '{}' is verified (verificationStatusChanged={})",
                updatedPerson.getId(), verificationStatusChanged);

        return PersonChangeEmailAddressVerificationStatusConfirmation.builder()
                .person(updatedPerson)
                .verificationStatusChanged(verificationStatusChanged)
                .emailAddressChanged(emailAddressChanged)
                .build();
    }

    @EventProcessing
    private PersonChangeStatusConfirmation handlePersonChangeStatusRequest(
            final PersonChangeStatusRequest request) {

        final Set<PersonStatus> statusesToAdd = request.getStatusesToAdd();
        final Set<PersonStatus> statusesToRemove = request.getStatusesToRemove();

        final Person updatedPerson =
                personService.changePersonStatuses(request.getPerson(), statusesToAdd, statusesToRemove);
        return PersonChangeStatusConfirmation.builder()
                .updatedPerson(updatedPerson)
                .statusesAdded(statusesToAdd)
                .statusesRemoved(statusesToRemove)
                .build();
    }

    @EventProcessing
    private PersonProfilePictureChangeConfirmation handlePersonProfilePictureChangeRequest(
            final PersonProfilePictureChangeRequest request) {

        final Person person = request.getPerson();
        final AppVariant appVariant =
                appService.findAppVariantByAppVariantIdentifier(SharedConstants.PLATFORM_APP_VARIANT_IDENTIFIER);

        final MediaItem newProfilePicture =
                mediaItemService.useItem(request.getNewProfilePicture(),
                        FileOwnership.of(person, appVariant));
        final Person updatedPerson =
                personService.updateProfilePicture(person, newProfilePicture);
        return PersonProfilePictureChangeConfirmation.builder()
                .person(updatedPerson)
                .build();
    }

    @EventProcessing
    private PersonProfilePictureDeleteConfirmation handlePersonProfilePictureDeleteRequest(
            final PersonProfilePictureDeleteRequest request) {

        final Person updatedPerson = personService.deleteProfilePicture(request.getPerson());
        return PersonProfilePictureDeleteConfirmation.builder()
                .person(updatedPerson)
                .build();
    }

}
