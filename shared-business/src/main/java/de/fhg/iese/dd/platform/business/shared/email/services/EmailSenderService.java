/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Steffen Hupp, Balthasar Weitzel, Dominik Schnier, Danielle Korth
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;
import de.fhg.iese.dd.platform.business.shared.template.services.ITemplateService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import freemarker.template.TemplateException;

@Service
@Profile({"!test", "EmailSenderServiceTest", "TemporaryEmailStorageCleanUpWorkerTest"})
class EmailSenderService extends BaseService implements IEmailSenderService {

    private static final String TEAM_NOTIFICATION_TOPIC = "email";

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") // implementation injected via Spring
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ITemplateService templateService;

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Autowired
    private IEnvironmentService environmentService;

    @Autowired
    private IFileStorage fileStorage;

    private String emailBaseStoragePath;

    @PostConstruct
    private void initialize() {
        this.emailBaseStoragePath = StringUtils.appendIfMissing(getConfig().getEmail().getEmailStoragePath(), "/");
    }

    @Override
    public String createTextWithTemplate(TemplateLocation templateLocation, Map<String, Object> templateModel)
            throws IOException, TemplateException {
        Map<String, Object> templateModelExtended = new HashMap<>(templateModel);
        templateModelExtended.put("mailImageLocation", getConfig().getEmail().getEmailImageBaseURL());
        return templateService.createTextWithTemplateMapModel(templateLocation, templateModelExtended);
    }

    @Override
    public void sendEmail(String fromEmailAddress, String recipientName, String recipientEmailAddress, String subject,
            String text, List<File> attachments, boolean isHtml) throws MessagingException {

        boolean hasAttachments = !CollectionUtils.isEmpty(attachments);
        if (getConfig().getEmail().isIncludeEnvironmentInfo()) {
            subject = String.format("[%s] %s", environmentService.getEnvironmentFullName(), subject);
        }
        String emailStorageExternalURL = null;
        if (getConfig().getEmail().isEmailStorageEnabled()) {
            emailStorageExternalURL = saveToEmailStorage(recipientEmailAddress, text, isHtml);
        }
        InternetAddress toAddress;
        if (StringUtils.isNotBlank(recipientName)) {
            try {
                toAddress =
                        new InternetAddress(recipientEmailAddress, recipientName, StandardCharsets.UTF_8.toString());
            } catch (Exception e) {
                log.error("Failed to encode email address: {}", e.toString());
                toAddress = new InternetAddress(recipientEmailAddress);
            }
        } else {
            toAddress = new InternetAddress(recipientEmailAddress);
        }
        if (getConfig().getEmail().isEnabled()) {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, hasAttachments);
            helper.setFrom(fromEmailAddress);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(text, isHtml);
            if (hasAttachments) {
                for (File f : attachments) {
                    helper.addAttachment(f.getName(), f);
                }
            }
            mailSender.send(message);
        }

        StringBuilder teamNotification = new StringBuilder();

        teamNotification.append("Email from '").append(fromEmailAddress).append("' ");
        if (hasAttachments) {
            teamNotification.append("with ").append(attachments.size()).append(" attachments ");
        }
        if (!getConfig().getEmail().isEnabled()) {
            teamNotification.append("should have been sent ");
        } else {
            teamNotification.append("was sent ");
        }
        teamNotification.append("to '").append(toAddress).append("' with subject '")
                .append(subject).append("'.");
        if (getConfig().getEmail().isEmailStorageEnabled()) {
            teamNotification.append("\nMail text is saved at ").append(emailStorageExternalURL);
        }

        log.info(teamNotification.toString());
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.DEBUG_APPLICATION_SINGLE_USER,
                teamNotification.toString());
    }

    private String saveToEmailStorage(String recipientEmailAddress, String text, boolean isHtml) {
        String fileEnding;
        String mimeType;
        if (isHtml) {
            fileEnding = "html";
            mimeType = "text/html";
        } else {
            fileEnding = "txt";
            mimeType = "text/plain";
        }
        String sanitizedMailAddress = fileStorage.sanitizeFileName(recipientEmailAddress);
        String internalFileName = String.format("%s%s_%d_%s.%s",
                emailBaseStoragePath,
                sanitizedMailAddress,
                timeService.currentTimeMillisUTC(),
                UUID.randomUUID().toString().replace("-", "").substring(0, 20),
                fileEnding);
        fileStorage.saveFile(text.getBytes(StandardCharsets.UTF_8), mimeType, internalFileName);
        return fileStorage.getExternalUrl(internalFileName);
    }

    @Override
    public void sendHtmlEmailToPersons(String fromEmailAddress, Collection<Person> personsToNotify,
            TemplateLocation templateLocation, Function<Person, Map<String, Object>> templateModelProducer,
            String subject) {

        for (Person person : personsToNotify) {
            if (person.isDeleted()) {
                continue;
            }
            final String mailText;
            try {
                mailText = createTextWithTemplate(templateLocation, templateModelProducer.apply(person));
            } catch (IOException | TemplateException e) {
                log.error("Could not prepare email template " + templateLocation, e);
                continue;
            }
            try {
                sendEmail(fromEmailAddress, person.getFullName(), person.getEmail(), subject, mailText,
                        Collections.emptyList(), true);
            } catch (MessagingException e) {
                log.error("Could not send email to " + person.getEmail(), e);
            }
        }
    }

    @Override
    public void deleteOldSavedEmails(BiFunction<String, Duration, Boolean> onDelete) {
        final Duration maxRetentionTime = Duration.ofDays(getConfig().getEmail().getMaxRetentionEmailStorageInDays());
        fileStorage.deleteOldFiles(emailBaseStoragePath, maxRetentionTime, onDelete);
    }

}
