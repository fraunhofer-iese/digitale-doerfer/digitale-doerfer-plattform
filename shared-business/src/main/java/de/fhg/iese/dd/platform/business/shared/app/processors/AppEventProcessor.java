/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.processors;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantChangeSelectedGeoAreasConfirmation;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantChangeSelectedGeoAreasRequest;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantTenantContractChangeAdditionalNotesConfirmation;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantTenantContractChangeAdditionalNotesRequest;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantTenantContractService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;

@EventProcessor
public class AppEventProcessor extends BaseEventProcessor {

    @Autowired
    private IAppService appService;

    @Autowired
    private IAppVariantTenantContractService appVariantTenantContractService;

    @EventProcessing
    private AppVariantChangeSelectedGeoAreasConfirmation handleAppVariantChangeSelectedGeoAreasRequest(
            final AppVariantChangeSelectedGeoAreasRequest event) {
        //refresh everything to avoid working with stale entities
        //final AppVariant appVariant = appService.refresh(event.getAppVariant());
        //final Person person = personService.refresh(event.getPerson());
        //final Set<GeoArea> selectedGeoAreas = geoAreaService.refresh(event.getNewSelectedGeoAreas());
        //avoid the refresh and see what happens, these entities can't be stale or they are not persisted
        final AppVariant appVariant = event.getAppVariant();
        final Person person = event.getPerson();
        final Set<GeoArea> selectedGeoAreas = event.getNewSelectedGeoAreas();

        Pair<AppVariantUsage, Set<GeoArea>> updatedSelection =
                appService.selectGeoAreas(appVariant, person, selectedGeoAreas);

        return AppVariantChangeSelectedGeoAreasConfirmation.builder()
                .appVariant(updatedSelection.getLeft().getAppVariant())
                .person(updatedSelection.getLeft().getPerson())
                .newSelectedGeoAreas(updatedSelection.getRight())
                .build();
    }

    @EventProcessing
    private AppVariantTenantContractChangeAdditionalNotesConfirmation handleAppVariantTenantContractChangeAdditionalNotesRequest(
            final AppVariantTenantContractChangeAdditionalNotesRequest event) {

        AppVariantTenantContract appVariantTenantContract = event.getAppVariantTenantContract();

        final String changedNotes = event.getChangedNotes();
        if (StringUtils.isBlank(changedNotes)) {
            appVariantTenantContract.setAdditionalNotes(null);
        } else {
            appVariantTenantContract.setAdditionalNotes(changedNotes);
        }

        return AppVariantTenantContractChangeAdditionalNotesConfirmation.builder()
                .appVariantTenantContract(appVariantTenantContractService.store(appVariantTenantContract))
                .build();
    }

}
