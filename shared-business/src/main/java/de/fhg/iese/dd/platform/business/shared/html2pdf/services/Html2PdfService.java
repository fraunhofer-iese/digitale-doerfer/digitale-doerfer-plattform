/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Dominik Schnier, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.html2pdf.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.springframework.stereotype.Service;

import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import de.fhg.iese.dd.platform.business.shared.html2pdf.exceptions.Html2PdfException;

@Service
class Html2PdfService implements IHtml2PdfService {

    @Override
    public byte[] convert(String html, int pageWidthMM, int pageHeightMM) throws Html2PdfException {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            new PdfRendererBuilder()
                    .useDefaultPageSize(pageWidthMM, pageHeightMM, BaseRendererBuilder.PageSizeUnits.MM)
                    .withW3cDocument(new W3CDom().fromJsoup(Jsoup.parse(html)), null)
                    .toStream(os)
                    .run();

            return os.toByteArray();
        } catch (IOException e) {
            throw new Html2PdfException(e);
        }
    }

    @Override
    public byte[] convertA4(String html) throws Html2PdfException {
        return convert(html, 210, 297);
    }

}
