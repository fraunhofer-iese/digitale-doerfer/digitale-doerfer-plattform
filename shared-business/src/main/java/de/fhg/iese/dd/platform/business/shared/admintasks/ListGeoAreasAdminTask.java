/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.statistics.AppStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReport;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class ListGeoAreasAdminTask extends BaseAdminTask {

    @Autowired
    IStatisticsService statisticsService;

    @Override
    public String getName() {
        return "ListGeoAreas";
    }

    @Override
    public String getDescription() {
        return "Lists all GeoAreas in their hierarchy and the app variants that are available there.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {
        final StatisticsReportDefinition reportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(AppStatisticsProvider.STATISTICS_ID_APP_INFO)
                .allGeoAreas(true)
                .build();
        final StatisticsReport report = statisticsService.generateStatisticsReport(reportDefinition);
        String reportPrint = statisticsService.reportToText(report);

        logSummary.info(reportPrint);
    }

}
