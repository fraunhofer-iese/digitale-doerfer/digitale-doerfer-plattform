/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationChannelMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

import java.util.Collection;
import java.util.Set;

public interface ITeamNotificationService extends IService, ICachingComponent {

    void sendTeamNotification(String topic, TeamNotificationMessage message);

    void sendTeamNotification(Tenant tenant, String topic, TeamNotificationMessage message);

    /**
     * Sends a message to the team that is formatted similar to log4j logging.
     * <p/>
     * <code>("New {} created", entity.toString())</code> will result in
     * <code>("New entity created")</code>
     *
     * @param tenant    the tenant that this message is relevant for, or null if it is not specific to a tenant
     * @param topic     a topic that is used to map messages to channels, using {@link TeamNotificationChannelMapping}
     * @param priority  the priority the message should have, controls how it is communicated
     * @param message   message to the team
     * @param arguments arguments that are used to expand the {} in the message
     */
    void sendTeamNotification(Tenant tenant, String topic, TeamNotificationPriority priority,
            String message, Object... arguments);

    /**
     * Sends a message to the team that is formatted similar to log4j logging.
     * <p/>
     * <code>("New {} created", entity.toString())</code> will result in
     * <code>("New entity created")</code>
     *
     * @param topic     a topic that is used to map messages to channels, using {@link TeamNotificationChannelMapping}
     * @param priority  the priority the message should have, controls how it is communicated
     * @param message   message to the team
     * @param arguments arguments that are used to expand the {} in the message
     */
    void sendTeamNotification(String topic, TeamNotificationPriority priority, String message, Object... arguments);

    /**
     * Checks if the according publisher provider is available and the connection config is valid
     *
     * @param teamNotificationConnection
     *         the connection to check
     *
     * @throws TeamNotificationConnectionConfigInvalidException
     *         if no publisher provider can be found for the given type or the connection config is invalid
     */
    void validateConnection(TeamNotificationConnection teamNotificationConnection) throws
            TeamNotificationConnectionConfigInvalidException;

    TeamNotificationConnection store(TeamNotificationConnection teamNotificationConnection);

    TeamNotificationChannelMapping store(TeamNotificationChannelMapping teamNotificationChannelMapping);

    Set<TeamNotificationConnection> getAllConnections();

    Set<TeamNotificationChannelMapping> getAllChannelMappings();

    void deleteConnections(Collection<TeamNotificationConnection> connectionsToDelete);

    void deleteChannelMappings(Collection<TeamNotificationChannelMapping> channelMappingsToDelete);

}
