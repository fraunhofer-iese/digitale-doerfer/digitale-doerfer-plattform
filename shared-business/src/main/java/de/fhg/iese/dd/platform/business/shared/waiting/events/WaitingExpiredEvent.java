/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.events;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;

import org.springframework.context.ApplicationContext;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.services.IWaitingService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Event that is fired after an according deadline expired.
 * <p/>
 * It contains all attributes that are automatically filled based on the values
 * that the event had when it was registered as deadline, using the {@link IWaitingService}.
 * {@link BaseEntity}s as values of the event are fetched from the DB based on
 * their id. If an entity does not exist anymore the attribute is null.
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class WaitingExpiredEvent extends BaseEvent {

    private Long deadline;

    /**
     * Returns the current deadline of the waiting event.
     *
     * @param context
     * @return
     */
    public WaitingDeadlineUpdate nextDeadline(ApplicationContext context){
        return new PeriodicWaitingExpiredEvent.WaitingDeadlineUpdate(false, deadline);
    }

    /**
     * Adds a duration to NOW and sets it as deadline.
     *
     * @param duration
     * @return
     */
    public WaitingExpiredEvent withDeadlineIn(Duration duration){
        deadline = Instant.now().plus(duration).toEpochMilli();
        return this;
    }

    /**
     * Adds a duration to NOW and sets it as deadline.
     *
     * @param deadline
     * @return
     */
    public WaitingExpiredEvent withDeadline(long deadline){
        this.deadline = deadline;
        return this;
    }

    public String getDeadlineReadable(){
        return Instant.ofEpochMilli(deadline).atZone(ZoneOffset.UTC).toString();
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WaitingDeadlineUpdate {

        private boolean periodicDeadline;
        private long nextDeadline;

    }

}
