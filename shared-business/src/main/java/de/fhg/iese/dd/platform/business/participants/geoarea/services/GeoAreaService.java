/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.geoarea.services;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.geoarea.exceptions.GeoAreaNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.geo.services.ISpatialService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.BoundingBox;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.Hibernate;
import org.jetbrains.annotations.NotNull;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
class GeoAreaService extends BaseEntityService<GeoArea> implements IGeoAreaService {

    //consecutive character
    private static final Pattern NORMALIZATION_CONSECUTIVE_PATTERN = Pattern.compile("(.)\\1+");
    //normalization of german characters
    private static final String[] NORMALIZATION_SEARCH = new String[]{"ß", "ae", "oe", "ue",};
    private static final String[] NORMALIZATION_REPLACE = new String[]{"s", "a", "o", "u",};

    private static final Pattern NORMALIZATION_IGNORE_PATTERN = Pattern.compile(
            "(?<=[^a-z])[a-z]\\.|" +//every alpha character that is not preceded by another alpha character
                    ", st\\b|, m\\b|, gkst\\b|\\(vgem\\)|" + //these are useless extensions to the name
                    "[^a-z]+"); //if any non-alpha character is left, it shall be eaten
    //any consecutive numbers, even with spaces inside
    private static final Pattern NUMERIC_PATTERN = Pattern.compile("(\\d\\s*)+");

    private static final Comparator<GeoArea> geoAreaComparator =
            Comparator.comparing(GeoArea::getName).thenComparing(GeoArea::getCreated);

    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private ISpatialService spatialService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private CacheCheckerAtOnceCache cacheCheckerGeoAreaById;

    private List<GeoArea> allGeoAreas;
    private Map<String, GeoArea> geoAreaById;
    private Map<Tenant, Set<GeoArea>> geoAreasByTenant;
    private Map<Tenant, GeoArea> legacyRootGeoAreaByTenant;
    private Set<GeoArea> leafGeoAreas;
    private GeoArea fakeGlobalRootArea;
    private Map<GeoArea, Set<GeoArea>> geoAreasByParentArea;
    private Cache<GeoArea, Geometry> geometryByGeoArea;

    @PostConstruct
    private void initialize() {
        allGeoAreas = Collections.emptyList();
        geoAreaById = Collections.emptyMap();
        geoAreasByTenant = Collections.emptyMap();
        legacyRootGeoAreaByTenant = Collections.emptyMap();
        leafGeoAreas = Collections.emptySet();
        geometryByGeoArea = CacheBuilder.newBuilder()
                .maximumSize(250)
                .build();
        //this is just a trick to have a fake global root to get the real root areas
        fakeGlobalRootArea = new GeoArea();
        cacheCheckerGeoAreaById.addCheckQuery(tenantService::checkCacheTenants);
    }

    @Override
    public CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(GeoArea.class)
                .usedCache(ITenantService.class)
                .build();
    }

    private void checkGeoAreaCache() {
        cacheCheckerGeoAreaById.refreshCacheIfStale(() -> {

            long startLoad = System.currentTimeMillis();
            // if the cache is stale or no fetch ever happened
            // get all geo areas fresh from the database
            // we query it in chunks to not overload the DB
            int pageNumber = 0;
            List<GeoArea> geoAreasFromDB = new ArrayList<>((int) geoAreaRepository.count());
            while (true) {
                PageRequest pageRequest = PageRequest.of(pageNumber, 1000);
                Page<GeoArea> geoAreaPage = query(() -> geoAreaRepository.findAllFull(pageRequest));
                geoAreasFromDB.addAll(geoAreaPage.getContent());
                pageNumber++;
                if (geoAreaPage.isLast()) {
                    break;
                }
            }
            final List<GeoArea> allGeoAreasNew = geoAreasFromDB.stream()
                    .map(proxy -> Hibernate.unproxy(proxy, GeoArea.class))
                    .peek(geoArea -> geoArea.setTenant(tenantService.forceCached(geoArea.getTenant())))
                    .collect(Collectors.toList());
            long loadTime = System.currentTimeMillis() - startLoad;
            long start = System.currentTimeMillis();
            final Set<GeoArea> newLeafGeoAreas = new HashSet<>(allGeoAreasNew);
            final Map<String, GeoArea> geoAreaByIdNew = allGeoAreasNew.parallelStream()
                    .collect(Collectors.toMap(GeoArea::getId, Function.identity()));
            for (GeoArea cachedGeoArea : allGeoAreasNew) {
                //cached is set to true to indicate that the transient fields have been set
                cachedGeoArea.setCached(true);
                cachedGeoArea.setSimilarityCodes(normalizedGeoAreaNames(cachedGeoArea.getName()));
                //uncomment to log the similarity codes of the geo areas
                // log.debug("{} {}", StringUtils.rightPad(cachedGeoArea.getName(), 100),
                //         Arrays.asList(cachedGeoArea.getSimilarityCodes()));
                //this gets set to true later on if the geo area is a leaf, otherwise it would be null
                cachedGeoArea.setLeaf(false);
                cachedGeoArea.setZips(StringUtils.isEmpty(cachedGeoArea.getZip())
                        ? Collections.emptyList()
                        : Arrays.asList(StringUtils.split(cachedGeoArea.getZip(), ",")));
                // we replace every parent with a cached parent, to be sure that we have only one single instance
                // this ensures that no db access is necessary if we climb up the parent hierarchy
                String parentAreaId = BaseEntity.getIdOf(cachedGeoArea.getParentArea());
                GeoArea cachedParent = geoAreaByIdNew.get(parentAreaId);
                cachedGeoArea.setParentArea(cachedParent);
                newLeafGeoAreas.remove(cachedParent);
            }
            allGeoAreasNew.parallelStream()
                    .forEach(cachedGeoArea -> cachedGeoArea
                            .setBoundingBox(spatialService.calculateBoundingBox(cachedGeoArea)));
            newLeafGeoAreas.forEach(leafGeoArea -> leafGeoArea.setLeaf(true));

            Map<GeoArea, Set<GeoArea>> newGeoAreasByParentArea = allGeoAreasNew.stream()
                    .collect(Collectors.groupingBy(
                            geoArea -> geoArea.getParentArea() == null ? fakeGlobalRootArea : geoArea.getParentArea(),
                            Collectors.mapping(Function.identity(), Collectors.toSet())));

            setGeoAreaDepth(fakeGlobalRootArea, newGeoAreasByParentArea, -1);
            log.info("Loaded {} geo areas in {} ms and refreshed cache in {} ms",
                    allGeoAreasNew.size(),
                    loadTime,
                    System.currentTimeMillis() - start);

            Map<Tenant, Set<GeoArea>> geoAreasByTenantNew = allGeoAreasNew.stream()
                    .filter(g -> g.getTenant() != null)
                    .collect(Collectors.groupingBy(GeoArea::getTenant,
                            Collectors.mapping(Function.identity(), Collectors.toSet())));

            Map<Tenant, GeoArea> legacyRootGeoAreaByTenantNew = geoAreasByTenantNew.keySet().stream()
                    .map(tenant -> Pair.of(tenant, geoAreaByIdNew.get(BaseEntity.getIdOf(tenant.getRootArea()))))
                    .filter(tenantGeoAreaPair -> tenantGeoAreaPair.getLeft() != null && tenantGeoAreaPair.getRight() != null)
                    //this is a hack to ensure that the tenant (cache instance from tenant service) also has a cached geo area
                    .peek(tenantGeoAreaPair -> tenantGeoAreaPair.getLeft().setRootArea(tenantGeoAreaPair.getRight()))
                    .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));

            allGeoAreas = allGeoAreasNew;
            geoAreaById = geoAreaByIdNew;
            geoAreasByTenant = geoAreasByTenantNew;
            legacyRootGeoAreaByTenant = legacyRootGeoAreaByTenantNew;
            leafGeoAreas = newLeafGeoAreas;
            geoAreasByParentArea = newGeoAreasByParentArea;
        });
    }

    private void setGeoAreaDepth(GeoArea rootArea, Map<GeoArea, Set<GeoArea>> geoAreasByParentArea, int depth) {
        Set<GeoArea> childAreas = geoAreasByParentArea.getOrDefault(rootArea, Collections.emptySet());
        if (CollectionUtils.isEmpty(childAreas)) {
            rootArea.setCountChildren(0);
            rootArea.setCountDirectChildren(0);
        } else {
            int countChildren = childAreas.size();
            for (GeoArea childArea : childAreas) {
                setGeoAreaDepth(childArea, geoAreasByParentArea, depth + 1);
                countChildren += childArea.getCountChildren();
            }
            rootArea.setCountChildren(countChildren);
            rootArea.setCountDirectChildren(childAreas.size());
        }
        rootArea.setDepth(depth);
    }

    @Override
    public Set<GeoArea> findAllRootAreas() {
        checkGeoAreaCache();
        return geoAreasByParentArea.get(fakeGlobalRootArea);
    }

    @Override
    public List<GeoArea> findAll() {
        checkGeoAreaCache();
        return allGeoAreas.stream()
                .sorted(geoAreaComparator)
                .collect(Collectors.toList());
    }
    
    @Override
    public GeoArea findGeoAreaById(final String geoAreaId) throws GeoAreaNotFoundException {
        checkGeoAreaCache();
        GeoArea geoArea = geoAreaById.get(geoAreaId);
        if (geoArea == null) {
            throw new GeoAreaNotFoundException(geoAreaId);
        }
        return geoArea;
    }

    @Override
    public Set<GeoArea> getGeoAreasForTenants(final Collection<Tenant> tenants) {
        checkGeoAreaCache();
        return tenants.stream()
                .map(geoAreasByTenant::get)
                //there might be nulls if the tenants do not have any geo areas
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<GeoArea> getRootGeoAreasForTenants(Collection<Tenant> tenants) {

        final Set<GeoArea> geoAreasForTenants = getGeoAreasForTenants(tenants);
        return getRootGeoAreas(geoAreasForTenants);
    }

    @Override
    public Set<GeoArea> getRootGeoAreas(Collection<GeoArea> geoAreas) {

        checkGeoAreaCache();

        final Integer minDepth = geoAreas.stream()
                .map(this::internalEnsureCached)
                .map(GeoArea::getDepth)
                .min(Comparator.naturalOrder())
                .orElse(-1);
        return geoAreas.stream()
                .map(this::internalEnsureCached)
                .filter(geoArea -> geoArea.getDepth() == minDepth)
                .collect(Collectors.toSet());
    }

    @Override
    public GeoArea getLegacyRootGeoAreaOfTenant(Tenant tenant) {

        checkGeoAreaCache();
        return legacyRootGeoAreaByTenant.get(tenant);
    }

    @Override
    public boolean isLeaf(final GeoArea geoArea) {
        checkGeoAreaCache();
        return leafGeoAreas.contains(geoArea);
    }

    @Override
    public Set<GeoArea> findAllById(final Collection<String> ids) throws GeoAreaNotFoundException {
        if (CollectionUtils.isEmpty(ids)) {
            return Collections.emptySet();
        }
        checkGeoAreaCache();
        Set<GeoArea> geoAreas = ids.stream()
                .filter(StringUtils::isNotEmpty)
                .map(geoAreaById::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        if (geoAreas.size() != ids.size()) {
            //at least one of the geo areas was not found
            throw new GeoAreaNotFoundException("Not all geo areas could be found, ids are invalid '{}'",
                    getMissingIds(geoAreas, ids));
        }
        return geoAreas;
    }

    @Override
    public Collection<GeoArea> findAllByIdOrNameInfixSearch(String infixSearchString) {

        checkGeoAreaCache();

        final Predicate<GeoArea> predicate = geoArea ->
                StringUtils.containsIgnoreCase(geoArea.getId(), infixSearchString) ||
                        StringUtils.containsIgnoreCase(geoArea.getName(), infixSearchString);
        return allGeoAreas.stream()
                .filter(predicate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<GeoArea> getParentGeoAreas(final Set<GeoArea> geoAreas) {

        final Set<GeoArea> parents = new HashSet<>();

        for (GeoArea geoArea : geoAreas) {
            //we do not need to go up the tree, if the geo area is already in the result
            if (!parents.contains(geoArea)) {
                GeoArea parent = geoArea;
                while (parent != null) {
                    parent = parent.getParentArea();
                    //if the parent area is null we are at the root
                    // or it is already contained
                    // -> we can stop going up the tree
                    if (parent == null || parents.contains(parent)) {
                        break;
                    }
                    parents.add(parent);
                }
            }
        }
        return parents;
    }

    @Override
    public Set<GeoArea> addParentGeoAreas(final Set<GeoArea> geoAreas, Predicate<GeoArea> parentPredicate) {

        final Set<GeoArea> geoAreasWithParents = new HashSet<>();

        for (GeoArea geoArea : geoAreas) {
            //we do not need to go up the tree, if the geo area is already in the result
            if (!geoAreasWithParents.contains(geoArea)) {
                //the seed geo areas are added without the filter
                geoAreasWithParents.add(geoArea);
                while (geoArea != null) {
                    geoArea = geoArea.getParentArea();
                    //if the parent area is null we are at the root
                    // or it is already contained
                    // -> we can stop going up the tree
                    if (geoArea == null || geoAreasWithParents.contains(geoArea)) {
                        break;
                    }
                    if (parentPredicate.test(geoArea)) {
                        geoAreasWithParents.add(geoArea);
                    }
                }
            }
        }
        return geoAreasWithParents;
    }

    @Override
    public Set<GeoArea> addParentGeoAreas(Set<GeoArea> geoAreas, Predicate<GeoArea> parentPredicate, int upToDepth) {

        checkGeoAreaCache();

        final Set<GeoArea> geoAreasWithParents = new HashSet<>();

        for (GeoArea geoArea : geoAreas) {
            //if it is not a cached one, we replace it (since we need to check the depth)
            geoArea = internalEnsureCached(geoArea);
            //we do not need to go up the tree, if the geo area is already in the result
            if (!geoAreasWithParents.contains(geoArea)) {
                //the seed geo areas are added without the filter
                geoAreasWithParents.add(geoArea);
                while (geoArea != null) {
                    geoArea = geoArea.getParentArea();
                    //if the parent area is null we are at the root
                    // or it is already contained
                    // or we are above the final depth
                    // -> we can stop going up the tree
                    if (geoArea == null || geoAreasWithParents.contains(geoArea) || geoArea.getDepth() < upToDepth) {
                        break;
                    }
                    if (parentPredicate.test(geoArea)) {
                        geoAreasWithParents.add(geoArea);
                    }
                }
            }
        }
        return geoAreasWithParents;
    }

    @Override
    public Set<GeoArea> removeNonLeafGeoAreas(Set<GeoArea> geoAreas) {

        checkGeoAreaCache();

        return geoAreas.stream()
                .filter(geoArea -> leafGeoAreas.contains(geoArea))
                .collect(Collectors.toSet());
    }

    @Override
    public Pair<GeoArea, GeoAreaMatch> findNearestGeoArea(Collection<GeoArea> availableGeoAreas,
            GPSLocation currentLocation) {

        Objects.requireNonNull(availableGeoAreas);
        Objects.requireNonNull(currentLocation);

        final List<GeoArea> geoAreasSortedByDistance = availableGeoAreas.stream()
                // filter geo areas which do not have geo json for creating the required geometry
                .filter(geoArea -> StringUtils.isNotEmpty(geoArea.getBoundaryJson()))
                .map(geoArea -> Pair.of(geoArea, spatialService.distanceInKM(geoArea.getCenter(), currentLocation)))
                .filter(pair -> pair.getRight() <= getConfig().getGeoAreaLookupRadiusInKm())
                .sorted(Comparator.comparingDouble(Pair::getRight))
                //for debugging:
                //.peek(pair -> System.out.printf("%s, %.3f km\n", pair.getLeft().getName(), pair.getRight()))
                .map(Pair::getLeft)
                .toList();

        if (geoAreasSortedByDistance.isEmpty()) {
            return Pair.of(null, GeoAreaMatch.NONE);
        }

        for (GeoArea geoArea : geoAreasSortedByDistance) {
            Geometry geometry = getCachedGeometry(geoArea);
            if (spatialService.isWithinBoundary(geometry, currentLocation)) {
                //found geoArea including the currentLocation
                return Pair.of(geoArea, GeoAreaMatch.INSIDE);
            }
        }

        //fallback: using closest geoArea
        return Pair.of(geoAreasSortedByDistance.get(0), GeoAreaMatch.CLOSEST);
    }


    @Override
    public Set<GeoArea> findNearestGeoAreas(Collection<GeoArea> availableGeoAreas, GPSLocation currentLocation,
            double radiusKM, boolean onlyLeaves) {

        Objects.requireNonNull(currentLocation);

        Collection<GeoArea> candidateGeoAreas;
        if (onlyLeaves) {
            candidateGeoAreas = availableGeoAreas.stream()
                    .filter(GeoArea::isLeaf)
                    .collect(Collectors.toSet());
        } else {
            candidateGeoAreas = availableGeoAreas;
        }
        return candidateGeoAreas.stream()
                .filter(geoArea -> spatialService.distanceInKM(geoArea.getCenter(), currentLocation) <= radiusKM)
                .collect(Collectors.toSet());
    }

    @Override
    @NotNull
    public Set<GeoArea> findIntersectedLeafGeoAreas(Geometry geometry, Collection<GeoArea> availableGeoAreas) {

        Set<GeoArea> availableGeoAreasCached = availableGeoAreas.stream()
                .map(this::ensureCached)
                .collect(Collectors.toSet());
        //find all leaf areas that intersect with the bounding box of the geometry
        BoundingBox boundingBox = spatialService.calculateBoundingBox(geometry);
        List<GeoArea> candidateGeoAreas = availableGeoAreasCached.stream()
                .filter(GeoArea::isLeaf)
                .filter(geoArea -> spatialService.intersects(geoArea.getBoundingBox(), boundingBox))
                .toList();
        //filter them more by those that intersect with the actual boundary
        return candidateGeoAreas.stream()
                .filter(geoArea -> {
                    Geometry geoAreaGeometry = getCachedGeometry(geoArea);
                    if (geoAreaGeometry == null || geoAreaGeometry.isEmpty()) {
                        return false;
                    } else {
                        return geoAreaGeometry.intersects(geometry);
                    }
                })
                .collect(Collectors.toSet());
    }

    @Override
    @NotNull
    public Set<GeoArea> minimizeGeoAreas(Collection<GeoArea> geoAreasToMinimize, Collection<GeoArea> availableGeoAreas) {

        Set<GeoArea> availableGeoAreasCached = availableGeoAreas.stream()
                .map(this::ensureCached)
                .collect(Collectors.toSet());
        Set<GeoArea> incrementallyMinimizedGeoAreas = geoAreasToMinimize.stream()
                .map(this::ensureCached)
                .collect(Collectors.toSet());
        while (true) {
            Set<GeoArea> minimizationIterationResult = new HashSet<>(incrementallyMinimizedGeoAreas.size());
            for (GeoArea geoArea : incrementallyMinimizedGeoAreas) {
                // we only get the siblings that are in the available ones
                Set<GeoArea> siblings = getSiblings(geoArea).stream()
                        .filter(availableGeoAreasCached::contains)
                        .collect(Collectors.toSet());
                if (incrementallyMinimizedGeoAreas.containsAll(siblings)) {
                    //all the siblings are contained, so we can replace them with the parent area
                    //it's okay if we add it multiple times, since it is a set
                    GeoArea parentArea = geoArea.getParentArea();
                    if (availableGeoAreasCached.contains(parentArea)) {
                        //we can add the parent area, since it is in the available ones
                        minimizationIterationResult.add(parentArea);
                    } else {
                        //the parent is not in the available ones, so it can not be replaced
                        minimizationIterationResult.add(geoArea);
                    }
                } else {
                    //only a subset of the siblings are contained, we can not replace them
                    minimizationIterationResult.add(geoArea);
                }
            }
            if (minimizationIterationResult.equals(incrementallyMinimizedGeoAreas)) {
                //there was no change in this iteration, so there is no more potential to minimize
                break;
            } else {
                //there was a minimization, so we try it again if we can minimize more
                incrementallyMinimizedGeoAreas = minimizationIterationResult;
            }
        }
        return incrementallyMinimizedGeoAreas;
    }

    private Geometry getCachedGeometry(GeoArea geoArea) {

        try {
            return geometryByGeoArea.get(geoArea, () -> spatialService.createGeometryFromGeoArea(geoArea));
        } catch (ExecutionException e) {
            log.error("Creation of geometry for geo area '" + geoArea.getId() + "' failed: " + e, e);
            return null;
        }
    }

    @Override
    public Set<GeoArea> searchInGeoAreas(Collection<GeoArea> allGeoAreas, int upToDepth,
            boolean includeAllChildrenOfMatchingAreas, String rawSearch) {

        checkGeoAreaCache();
        final String search = StringUtils.strip(rawSearch);
        if (StringUtils.isEmpty(search)) {
            return Collections.emptySet();
        }
        final String zipSearch = extractZipSearch(search);
        //this will also eat the digits
        final String[] similaritySearchCodes = normalizedGeoAreaNames(search);

        final boolean multiSearch = similaritySearchCodes.length > 1;
        final String singleSearchTerm = similaritySearchCodes.length == 1
                ? similaritySearchCodes[0]
                : "";

        if (log.isTraceEnabled()) {
            if (multiSearch) {
                log.trace("geo area multi search '{}'", Arrays.asList(similaritySearchCodes));
            } else {
                log.trace("geo area single search '{}'", singleSearchTerm);
            }
            if (StringUtils.isNotEmpty(zipSearch)) {
                log.trace("geo area zip search '{}'", zipSearch);
            }
        }

        Predicate<GeoArea> searchPredicate;
        if (multiSearch) {
            searchPredicate = geoArea -> namesMatch(geoArea, similaritySearchCodes);
        } else {
            searchPredicate = geoArea -> nameMatches(geoArea, singleSearchTerm);
        }

        if (!StringUtils.isEmpty(zipSearch)) {
            searchPredicate = searchPredicate.and(geoArea -> zipMatches(geoArea, zipSearch));
        }

        final Set<GeoArea> matchingGeoAreas = allGeoAreas.parallelStream()
                //if there is a non-cached geo area replace it with a cached one
                .map(this::internalEnsureCached)
                //first we filter out those areas that do not have the intended depth
                .filter(geoArea -> geoArea.getDepth() >= upToDepth)
                //then we use the search predicate
                .filter(searchPredicate)
                .collect(Collectors.toSet());

        if (includeAllChildrenOfMatchingAreas) {
            //if we need to include all children of matching areas we need to find out which matched but are not leaves
            final List<GeoArea> matchingIntermediateAreas = matchingGeoAreas.stream()
                    .filter(geoArea -> !geoArea.isLeaf())
                    .collect(Collectors.toList());
            //this empty check is important, since an empty set returns all geo areas
            if (!CollectionUtils.isEmpty(matchingIntermediateAreas)) {
                //we add all children down to the leaves
                final Collection<GeoArea> childAreasOfMatchingIntermediates =
                        addChildAreas(matchingIntermediateAreas, allGeoAreas::contains, Integer.MAX_VALUE, false);

                matchingGeoAreas.addAll(childAreasOfMatchingIntermediates);
            }
        }
        //finally we add all parent areas up to the given depths
        return addParentGeoAreas(matchingGeoAreas, g -> true, upToDepth);
    }

    private String extractZipSearch(String search) {
        final Matcher matcher = NUMERIC_PATTERN.matcher(search);
        if (matcher.find()) {
            return StringUtils.deleteWhitespace(matcher.group());
        }
        return "";
    }

    private boolean namesMatch(GeoArea geoArea, String[] similaritySearchCodes) {

        //intentionally not with streams for performance reasons
        for (String similaritySearchCode : similaritySearchCodes) {
            boolean anySimCodeGeoAreaMatches = false;
            for (String simCodeGeoArea : geoArea.getSimilarityCodes()) {
                if (simCodeGeoArea.startsWith(similaritySearchCode)) {
                    anySimCodeGeoAreaMatches = true;
                    break;
                }
            }
            //if for one of the search codes none of the codes of the geo area matches, this geo area does not match
            if (!anySimCodeGeoAreaMatches) {
                return false;
            }
        }
        return true;
    }

    private boolean nameMatches(GeoArea geoArea, String similaritySearchCode) {

        //intentionally not with streams for performance reasons
        for (String simCode : geoArea.getSimilarityCodes()) {
            if (simCode.startsWith(similaritySearchCode)) {
                return true;
            }
        }
        return false;
    }

    private boolean zipMatches(GeoArea geoArea, String searchZip) {

        //intentionally not with streams for performance reasons
        for (String zip : geoArea.getZips()) {
            if (zip.startsWith(searchZip)) {
                return true;
            }
        }
        return false;
    }

    private String[] normalizedGeoAreaNames(String text) {
        if (StringUtils.isBlank(text)) {
            return new String[0];
        }
        //no accents anymore, no umlaut
        final String raw = StringUtils.stripAccents(text);
        //everything as lowercase
        final String lowered = StringUtils.lowerCase(raw);
        //cleaning out left over special characters
        final String cleaned = StringUtils.replaceEach(lowered, NORMALIZATION_SEARCH, NORMALIZATION_REPLACE);
        //removing all noise
        final String scrubbed = NORMALIZATION_IGNORE_PATTERN.matcher(cleaned).replaceAll(" ");
        //consecutive characters get condensed to one
        String singularized = NORMALIZATION_CONSECUTIVE_PATTERN.matcher(scrubbed).replaceAll("$1");
        //split by blank to have single words that can match
        return StringUtils.split(singularized);
    }

    @Override
    public Collection<GeoArea> getDirectChildAreas(GeoArea geoArea) {
        checkGeoAreaCache();
        final Set<GeoArea> children = geoAreasByParentArea.get(geoArea);
        if (CollectionUtils.isEmpty(children)) {
            return Collections.emptySet();
        } else {
            return new HashSet<>(children);
        }
    }

    @Override
    public Collection<GeoArea> getAllChildAreasIncludingSelf(GeoArea geoArea) {
        checkGeoAreaCache();
        final HashSet<GeoArea> result = new HashSet<>();
        result.add(internalEnsureCached(geoArea));
        internalGetAllChildAreas(geoArea, result);
        //nulls can occur if the data init runs while this query was running
        result.removeIf(Objects::isNull);
        return result;
    }

    @Override
    public Collection<GeoArea> getAllChildAreasExcludingSelf(GeoArea geoArea) {
        checkGeoAreaCache();
        final HashSet<GeoArea> result = new HashSet<>();
        internalGetAllChildAreas(geoArea, result);
        //nulls can occur if the data init runs while this query was running
        result.removeIf(Objects::isNull);
        return result;
    }

    private void internalGetAllChildAreas(GeoArea rootArea, Set<GeoArea> result) {
        Set<GeoArea> childAreas = geoAreasByParentArea.getOrDefault(rootArea, Collections.emptySet());
        if (!CollectionUtils.isEmpty(childAreas)) {
            result.addAll(childAreas);
            for (GeoArea childArea : childAreas) {
                internalGetAllChildAreas(childArea, result);
            }
        }
    }

    @Override
    public Set<GeoArea> addChildAreas(Collection<GeoArea> rootAreas, Predicate<GeoArea> childPredicate,
            int depth, boolean revealVisible) {

        if (CollectionUtils.isEmpty(rootAreas)) {
            return Collections.emptySet();
        }

        checkGeoAreaCache();

        Collection<GeoArea> parentAreas;
        parentAreas = rootAreas;
        Set<GeoArea> children = new HashSet<>(parentAreas);

        for (int i = 0; i < depth; i++) {

            Set<GeoArea> directChildren = parentAreas.stream()
                    .map(geoAreasByParentArea::get)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .filter(childPredicate)
                    .collect(Collectors.toSet());
            if (CollectionUtils.isEmpty(directChildren)) {
                //there are no more children, so we do not need to go deeper
                break;
            }
            children.addAll(directChildren);
            parentAreas = directChildren;
        }
        if (revealVisible) {
            children.addAll(revealVisible(rootAreas));
        }
        return children;
    }

    private Set<GeoArea> revealVisible(Collection<GeoArea> startAreas) {

        final Set<GeoArea> visibleGeoAreas = new HashSet<>();

        for (GeoArea geoArea : startAreas) {
            //if it is not a cached one, we replace it (since we need to check the depth)
            geoArea = internalEnsureCached(geoArea);
            //we do not need to go up the tree, if the geo area is already in the result
            if (!visibleGeoAreas.contains(geoArea)) {

                do {
                    //the seed geo areas are added without the filter
                    visibleGeoAreas.add(geoArea);
                    //the siblings should be revealed
                    visibleGeoAreas.addAll(getSiblings(geoArea));
                    geoArea = geoArea.getParentArea();

                    //if the parent area is null we are at the root
                    // or it is already contained
                    // -> we can stop going up the tree
                } while (geoArea != null && !visibleGeoAreas.contains(geoArea));
            }
        }
        return visibleGeoAreas;
    }

    private Set<GeoArea> getSiblings(GeoArea geoArea) {

        GeoArea parentArea = geoArea.getParentArea();
        if (parentArea != null) {
            return geoAreasByParentArea.get(parentArea);
        } else {
            //the siblings of the root areas are all root areas
            return geoAreasByParentArea.get(fakeGlobalRootArea);
        }
    }

    @Override
    public GeoArea ensureCached(GeoArea geoArea) {
        if (geoArea == null) {
            return null;
        }
        checkGeoAreaCache();
        return internalEnsureCached(geoArea);
    }

    private GeoArea internalEnsureCached(GeoArea geoArea) {
        if (geoArea == null) {
            return null;
        } else {
            if (geoArea.isCached()) {
                return geoArea;
            } else {
                return geoAreaById.get(geoArea.getId());
            }
        }
    }

    @Override
    public GeoArea forceCached(GeoArea geoArea) {
        if (geoArea == null) {
            return null;
        }
        checkGeoAreaCache();
        return geoAreaById.get(geoArea.getId());
    }

    @Override
    public Set<GeoArea> forceCached(Set<GeoArea> geoAreas) {

        if (CollectionUtils.isEmpty(geoAreas)) {
            return Collections.emptySet();
        }
        checkGeoAreaCache();
        return geoAreas.stream()
                .map(this::forceCached)
                .collect(Collectors.toSet());
    }

    @Override
    public Long checkCacheGeoAreas() {
        checkGeoAreaCache();
        return cacheCheckerGeoAreaById.getLastCacheRefresh();
    }

    @Override
    public void invalidateCache() {
        cacheCheckerGeoAreaById.resetCacheChecks();
    }
    
}
