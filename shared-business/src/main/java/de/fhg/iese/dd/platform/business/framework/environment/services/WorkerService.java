/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.services;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ScheduledFuture;

@Service
@Profile("!LeaderServiceTest")
class WorkerService extends BaseService implements IWorkerService {

    static class LoggingWorkerTask implements Runnable {

        final private IWorkerTask workerTask;
        final private Logger log;

        public LoggingWorkerTask(IWorkerTask workerTask) {
            this.workerTask = workerTask;
            this.log = LogManager.getLogger(workerTask);
        }

        @Override
        public void run() {
            try {
                workerTask.run();
            } catch (Exception e) {
                log.error("Failed to execute: " + e, e);
            }
        }

    }

    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private List<IWorkerTask> workerTasks;

    private final Map<IWorkerTask, ScheduledFuture<?>> scheduledTasks = new HashMap<>();

    @Override
    public List<IWorkerTask> getWorkerTasks() {
        return Collections.unmodifiableList(workerTasks);
    }

    @Override
    public void startWorkerTasks() {

        for (IWorkerTask workerTask : workerTasks) {
            String taskName = workerTask.getClass().getName();
            try {
                taskName = workerTask.getName();

                if (scheduledTasks.containsKey(workerTask)) {
                    log.warn("Worker task '{}' is already scheduled, ignoring", taskName);
                    continue;
                }

                final Trigger taskTrigger = workerTask.getTaskTrigger();
                final ScheduledFuture<?> future =
                        taskScheduler.schedule(new LoggingWorkerTask(workerTask), taskTrigger);
                scheduledTasks.put(workerTask, future);
                log.info("Scheduled worker task '{}' with trigger '{}'", taskName, triggerToString(taskTrigger));
            } catch (Exception e) {
                log.error("Failed to schedule worker task {}", taskName);
            }
        }
    }

    @Override
    public void stopWorkerTasks() {

        for (Iterator<Map.Entry<IWorkerTask, ScheduledFuture<?>>> iterator =
             scheduledTasks.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<IWorkerTask, ScheduledFuture<?>> taskAndFuture = iterator.next();
            final IWorkerTask workerTask = taskAndFuture.getKey();
            final ScheduledFuture<?> future = taskAndFuture.getValue();
            String taskName = workerTask.getClass().getName();
            try {
                future.cancel(false);
                //this will remove it from the map
                iterator.remove();
                log.info("Unscheduled task '{}'", taskName);
            } catch (Exception e) {
                log.error("Failed to unschedule worker task {}", taskName);
            }
        }
    }

    @Override
    public String triggerToString(Trigger trigger) {
        if (trigger instanceof CronTrigger) {
            return "cron: " + trigger;
        }
        if (trigger instanceof PeriodicTrigger periodicTrigger) {
            return String.format("periodic: initial %d period %d unit %s fixed-rate %s",
                    periodicTrigger.getInitialDelay(), periodicTrigger.getPeriod(), periodicTrigger.getTimeUnit(),
                    periodicTrigger.isFixedRate());
        }
        return "custom: " + trigger.toString();
    }

}
