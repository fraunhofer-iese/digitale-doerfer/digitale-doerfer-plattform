/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

import org.springframework.transaction.PlatformTransactionManager;

import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;

/**
 * Checks caches that are filled incrementally after they are cleared if they are stale
 * <p>
 * It can use multiple query methods to check if database content has changed. The result of the queries are stored and
 * compared. If they change the {@link #clearCacheIfStale(Runnable)} returns true and clears the cache by executing the
 * given cache clearing method. To avoid executing the queries on every check there is a check interval defined. Within
 * this interval no cache check is executed.
 * <p>
 * You have to use write thread safe caches in this case, typically {@link ConcurrentMap}
 * <p>
 * The default autowired cache uses a check interval of one minute and uses {@link
 * IReferenceDataChangeService#getLastChangeTime()} as cache check.
 *
 * @see #clearCacheIfStale(Runnable) code examples
 */
public class CacheCheckerIncrementalCache extends CacheChecker {

    CacheCheckerIncrementalCache(final Supplier<Long> currentTimeSupplier,
            PlatformTransactionManager transactionManager, Supplier<Object> defaultCheckQuery) {
        super(currentTimeSupplier, transactionManager, defaultCheckQuery);
    }

    /**
     * Clears the cache if it is stale using the given method to clear the cache.
     * <p>
     * Stale means that either the cache was never filled or the check interval elapsed and the result of the check
     * queries changed.
     * <p>
     * To create the cache and incrementally fill it typically this code has to be used:
     *
     * <pre> {@code
     * Map<K,Y> cache = new ConcurrentHashMap<>();
     * @Autowired
     * CacheCheckerIncrementalCache cacheChecker;
     *
     * V getCachedValue(K key){
     *      cacheChecker.clearCacheIfStale(cache::clear);
     *      return cache.computeIfAbsent(key, k -> new Value(f(k)));
     * }
     * }</pre>
     *
     * @return true if the cache was stale and cleared
     */
    public boolean clearCacheIfStale(Runnable cacheClearer) {
        return runIfCacheIsStale(cacheClearer);
    }

}
