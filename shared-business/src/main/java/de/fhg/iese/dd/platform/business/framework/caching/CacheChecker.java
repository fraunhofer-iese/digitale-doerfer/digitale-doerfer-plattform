/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import org.springframework.lang.Nullable;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Base class for checking cache, see {@link CacheCheckerAtOnceCache} or {@link CacheCheckerIncrementalCache} for two
 * implementations for different use cases.
 */
public abstract class CacheChecker {

    private static final Duration DEFAULT_CACHE_CHECK_INTERVAL = Duration.ofMinutes(1);

    private final Supplier<Long> currentTimeSupplier;
    private final PlatformTransactionManager transactionManager;
    private long checkInterval;
    private final List<CacheCheck> cacheChecks;
    private Long lastCheck;
    private Long lastCacheRefresh;

    private static class CacheCheck {

        private final PlatformTransactionManager transactionManager;
        private final Supplier<Object> query;
        private Object previousQueryResult;

        CacheCheck(@Nullable PlatformTransactionManager transactionManager, Supplier<Object> query) {
            this.transactionManager = transactionManager;
            this.query = query;
            //it needs to be something that is different from anything returned by the query
            previousQueryResult = this;
        }

        boolean isStale() {
            Object currentQueryResult = runQuery(query);
            boolean stale = !Objects.equals(currentQueryResult, previousQueryResult);
            previousQueryResult = currentQueryResult;
            return stale;
        }

        private <T> T runQuery(Supplier<T> query) {
            if (transactionManager == null) {
                return query.get();
            } else {
                TransactionTemplate transaction = new TransactionTemplate(transactionManager);
                transaction.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);
                transaction.setReadOnly(true);
                return transaction.execute(status -> query.get());
            }
        }

    }

    /**
     * Creates a new cache checker to manage the state of a single cache.
     * <p>
     * NEVER use the same cache checker for multiple caches, even if they share the same query! It will result in not
     * updating caches and hard to reproduce behavior.
     *
     * @param currentTimeSupplier required for checking if the interval is elapsed
     * @param transactionManager  transaction manager for wrapping the queries in read only transactions
     */
    protected CacheChecker(final Supplier<Long> currentTimeSupplier,
            PlatformTransactionManager transactionManager, Supplier<Object> defaultCheckQuery) {
        this.currentTimeSupplier = currentTimeSupplier;
        this.transactionManager = transactionManager;
        this.cacheChecks = new LinkedList<>();
        this.checkInterval = DEFAULT_CACHE_CHECK_INTERVAL.toMillis();
        this.lastCheck = null;
        this.lastCacheRefresh = null;
        addCheckQuery(defaultCheckQuery);
    }

    /**
     * Configures the interval between cache checks
     *
     * @param checkInterval minimum interval between execution of the check queries
     */
    public void setCheckInterval(Duration checkInterval) {
        this.checkInterval = checkInterval.toMillis();
    }

    /**
     * Clears all configured check queries
     */
    public void clearCheckQueries() {
        cacheChecks.clear();
    }

    /**
     * Adds a query to be checked on a cache check
     *
     * @param checkQuery query that is expected to deliver different results if the cached content changed
     */
    public void addCheckQuery(Supplier<Object> checkQuery) {
        cacheChecks.add(new CacheCheck(transactionManager, checkQuery));
    }

    protected boolean runIfCacheIsStale(Runnable runIfStale) {
        if (isCacheCheckRequired()) {
            synchronized (this) {
                if (isCacheCheckRequired()) {
                    boolean stale = false;
                    for (CacheCheck cacheCheck : cacheChecks) {
                        //we check all cache checkers, so that they are always provided with the newest values
                        stale |= cacheCheck.isStale();
                    }
                    if (stale) {
                        runIfStale.run();
                        lastCacheRefresh = currentTimeSupplier.get();
                    }
                    lastCheck = currentTimeSupplier.get();
                    return stale;
                }
            }
        }
        return false;
    }

    private boolean isCacheCheckRequired() {
        //only run the checks if the checkInterval is elapsed or it needs to be initialized
        return lastCheck == null || currentTimeSupplier.get() >= (lastCheck + checkInterval);
    }

    /**
     * Resets the time interval so that the next call to clearCache will cause the check queries to be executed again.
     */
    public void resetCacheChecks() {
        synchronized (this) {
            lastCheck = null;
            for (CacheCheck cacheCheck : cacheChecks) {
                //reset also the cache checks internally
                cacheCheck.previousQueryResult = cacheCheck;
            }
        }
    }

    /**
     * The point in time when the cache was refreshed for the last time. null if the cache was never refreshed.
     */
    public Long getLastCacheRefresh() {
        return lastCacheRefresh;
    }

    /**
     * Clears the map if it is not null
     */
    public static void clearIfNotNull(@Nullable Map<?, ?> map) {
        if (map != null) {
            map.clear();
        }
    }

}
