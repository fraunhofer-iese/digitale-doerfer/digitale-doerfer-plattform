/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.email.exceptions.EmailInvalidException;
import lombok.Getter;

@Service
class EmailAddressVerificationService extends BaseService implements IEmailAddressVerificationService {

    @Getter(lazy = true)
    private final Pattern emailAddressPattern = createEmailAddressPattern();

    private Pattern createEmailAddressPattern() {
        // see https://emailregex.com/ (considered too complex) and
        // https://www.regular-expressions.info/email.html
        return Pattern.compile(
                "\\A(?=[a-z0-9@.!#$%&'*+/=?^_‘{|}~-]{6,254}\\z)" +
                        "(?=[a-z0-9.!#$%&'*+/=?^_‘{|}~-]{1,64}@)" +
                        "[a-z0-9!#$%&'*+/=?^_‘{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_‘{|}~-]+)*" +
                        "@" +
                        "(?:(?=[a-z0-9-]{1,63}\\.)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+" +
                        "(?=[a-z0-9-]{1,63}\\z)[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\z");
    }

    @Override
    public String ensureCorrectEmailSyntaxAndNormalize(String emailAddress) throws EmailInvalidException {
        String normalizedEmail = normalizeEmail(emailAddress);
        if (!isCorrectEmailSyntax(normalizedEmail)) {
            throw new EmailInvalidException(emailAddress);
        }
        return normalizedEmail;
    }

    private String normalizeEmail(String emailAddress) {
        return StringUtils.lowerCase(StringUtils.trim(emailAddress));
    }

    private boolean isCorrectEmailSyntax(String emailAddress) {
        return getEmailAddressPattern().matcher(emailAddress).matches();
    }

}
