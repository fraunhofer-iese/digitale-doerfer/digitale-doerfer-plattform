/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushEndpointNotFoundException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushMessageInvalidException;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessageMinimizable;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Used as a dummy implementation that logs calls. Currently, only
 * {@link TestExternalPushProvider#registerPushEndpoint(String, PushPlatformType, AppVariant, Person)}
 * and {@link TestExternalPushProvider#getPushEndpoints(Person, AppVariant)} work as expected
 */
@Service
@Profile("(test | local) & !PinpointExternalPushProviderTest")
@Log4j2
public class TestExternalPushProvider implements IExternalPushProvider {

    private static final int MAX_SIZE_MESSAGE = 4096;

    private final List<ExternalPushEndpointDescription> testPushRegistrations = new ArrayList<>();

    @Override
    public List<ExternalPushApp> listAllPushApps() {
        log.debug("listAllPushApps()");
        return Collections.emptyList();
    }

    @Override
    public ExternalPushApp createOrUpdatePushApp(AppVariant appVariant,
            ExternalPushAppConfiguration externalPushAppConfiguration) {
        logCall("createOrUpdatePushApp", appVariant, externalPushAppConfiguration);
        return ExternalPushApp.builder()
                .pushAppName("test." + appVariant.getAppVariantIdentifier())
                .pushAppId("testPushAppId:" + StringUtils.abbreviate(appVariant.getAppVariantIdentifier(), 48)).build();
    }

    @Override
    public String getPushAppInfoForAppVariant(AppVariant appVariant) {
        logCall("getPushAppInfoForAppVariant", appVariant);
        return "no push apps available at TestExternalPushProvider";
    }

    @Override
    public void registerPushEndpoint(String registrationToken, PushPlatformType platformType,
            AppVariant appVariant, Person person) {
        logCall("registerPushEndpoint", registrationToken, appVariant, person);

        testPushRegistrations.add(ExternalPushEndpointDescription.builder()
                .appVariantId(appVariant.getId())
                .creationDate(ZonedDateTime.now(Clock.system(ZoneOffset.UTC)).toString())
                .endpointId(person.getId()) //used for filtering the endpoint by person id
                .platformType(platformType.toString())
                .token(registrationToken)
                .userInfo(person.toString())
                .build());
    }

    @Override
    public Collection<ExternalPushEndpointDescription> getPushEndpoints(Person person, AppVariant appVariant) {
        logCall("getPushEndpoints", person, appVariant);

        return testPushRegistrations.stream()
                .filter(description -> description.getEndpointId().equals(person.getId()) &&
                        description.getAppVariantId().equals(appVariant.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public void deletePushEndpoint(String registrationToken, PushPlatformType platformType, AppVariant appVariant,
            Person person) throws PushEndpointNotFoundException {
        logCall("deletePushEndpoint", registrationToken, platformType, appVariant, person);
    }

    @Override
    public void deletePushEndpoints(Person person, AppVariant appVariant) {
        logCall("deletePushEndpoints", person, appVariant);
    }

    @Override
    public void sendMessageToPerson(Set<AppVariant> appVariants, Person person, PushMessage message,
            boolean loudMessage) {
        PushMessage pushMessage = checkMessageSize(message, loudMessage);
        logCall("sendMessageToPerson", appVariants, person, pushMessage.getLogMessage(),
                loudMessage);
    }

    @Override
    public void sendMessageToPersons(AppVariant appVariant, Collection<String> personIdsLoudMessage,
            Collection<String> personIdsSilentMessage, PushMessage message) {
        PushMessage pushMessage = checkMessageSize(message, true);
        logCall("sendMessageToPersons", appVariant, personIdsLoudMessage, personIdsSilentMessage, pushMessage);
    }

    @Override
    public long deleteAppVariant(AppVariant appVariant) {
        logCall("deleteAppVariant", appVariant);
        return 0;
    }

    private void logCall(String methodName, Object... arguments) {
        log.debug(Arrays.stream(arguments)
                .map(arg -> arg == null ? null : "\"" + arg + "\"")
                .collect(Collectors.joining(",", methodName + "(", ")")));
    }

    private PushMessage checkMessageSize(PushMessage message, boolean isLoudMessage) {
        String errorMessage;
        String logPushMessage = message.getLogMessage();
        if (isPushMessageSizeValid(message, isLoudMessage)) {
            return message;
        }
        if (message instanceof PushMessageMinimizable messageMinimizable) {
            PushMessage minimizedMessage = messageMinimizable.toMinimizedPushMessage();
            if (isPushMessageSizeValid(minimizedMessage, isLoudMessage)) {
                return minimizedMessage;
            }
            errorMessage = String.format(
                    "Minimized event %s is too big (APNS: %d, FCM: %d) for sending via push",
                    logPushMessage,
                    minimizedMessage.getMessageSizeApns(isLoudMessage),
                    minimizedMessage.getMessageSizeFcm(isLoudMessage));
        } else {
            errorMessage = String.format(
                    "Event %s is too big (APNS: %d, FCM: %d) for sending via push but no small event is provided",
                    logPushMessage,
                    message.getMessageSizeApns(isLoudMessage),
                    message.getMessageSizeFcm(isLoudMessage));
        }
        log.error(errorMessage);
        throw new PushMessageInvalidException(errorMessage);
    }

    private boolean isPushMessageSizeValid(PushMessage message, boolean isLoudMessage) {
        return message.getMessageSizeApns(isLoudMessage) < MAX_SIZE_MESSAGE &&
                message.getMessageSizeFcm(isLoudMessage) < MAX_SIZE_MESSAGE;
    }

}
