/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shared.misc.exceptions.NamedCounterNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.NamedCounter;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.repos.NamedCounterRepository;

@Service
class NamedCounterService extends BaseEntityService<NamedCounter> implements INamedCounterService {

    @Autowired
    private NamedCounterRepository namedCounterRepository;

    @Override
    @Transactional(readOnly = true)
    public long getCounterValueByName(String name) {
        if(name == null) {
            throw new NamedCounterNotFoundException("null");
        }
        NamedCounter counter = namedCounterRepository.findByName(name);
        if(counter == null) {
            throw new NamedCounterNotFoundException(name);
        }
        return counter.getCounterValue();
    }

    @Override
    @Transactional
    public long setCounter(String name, long value) {
        NamedCounter counter;
        if(namedCounterRepository.existsByName(name)) {
            counter = namedCounterRepository.findByName(name);
            counter.setCounterValue(value);
        } else {
            counter = NamedCounter.builder()
                    .name(name)
                    .counterValue(value)
                    .build();
        }
        counter = store(counter);
        return counter.getCounterValue();
    }

    @Override
    @Transactional
    public long increaseCounter(String name) {
        if(!namedCounterRepository.existsByName(name)){
            return setCounter(name, 1);
        }
        namedCounterRepository.increaseCounterByName(name);
        return getCounterValueByName(name);
    }

    @Override
    @Transactional
    public long decreaseCounter(String name) {
        if(!namedCounterRepository.existsByName(name)){
            return setCounter(name, 0);
        }
        namedCounterRepository.decreaseCounterByName(name);
        return getCounterValueByName(name);
    }

}
