/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Steffen Hupp, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.security.events.CreateRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.security.events.CreateRoleAssignmentRequest;
import de.fhg.iese.dd.platform.business.shared.security.events.RemoveRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.security.events.RemoveRoleAssignmentRequest;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;

@EventProcessor
class RoleAssignmentEventProcessor extends BaseEventProcessor {

    @Autowired
    private IRoleService roleService;

    @EventProcessing
    private CreateRoleAssignmentConfirmation handleCreateRoleAssignmentRequest(CreateRoleAssignmentRequest event) {
        final RoleAssignment roleAssignment =
                roleService.assignRoleToPerson(event.getPerson(), event.getRole(), event.getEntityId());
        roleService.extend(roleAssignment);
        return new CreateRoleAssignmentConfirmation(roleAssignment);
    }

    @EventProcessing
    private RemoveRoleAssignmentConfirmation handleRemoveRoleAssignmentRequest(RemoveRoleAssignmentRequest event) {
        final RoleAssignment roleAssignment = event.getRoleAssignment();
        final Person person = roleAssignment.getPerson();
        roleService.removeRoleAssignment(roleAssignment);
        return new RemoveRoleAssignmentConfirmation(roleAssignment, person);
    }

}
