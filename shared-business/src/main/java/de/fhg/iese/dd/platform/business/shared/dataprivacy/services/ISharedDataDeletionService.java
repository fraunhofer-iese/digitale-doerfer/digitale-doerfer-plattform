/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2019 Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.util.Collection;
import java.util.Map;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;

public interface ISharedDataDeletionService extends IService {

    DeleteOperation deleteAddress(Address address);

    Map<LegalTextAcceptance, DeleteOperation> deleteAllLegalTextAcceptances(Person person);

    DeleteOperation deleteAppVariantUsage(AppVariantUsage appVariantUsage);

    DeleteOperation deletePushCategoryUserSettings(Collection<PushCategoryUserSetting> pushCategoryUserSettings);

}
