/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.PlatformTransactionManager;

import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;

/**
 * Supplier for pre-configured {@link CacheChecker} instances.
 *
 * @see CacheCheckerAtOnceCache
 * @see CacheCheckerIncrementalCache
 */
@Configuration
class CacheCheckerSupplier {

    @Autowired
    private ITimeService timeService;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    CacheCheckerIncrementalCache cacheCheckerIncrementalCache() {
        return new CacheCheckerIncrementalCache(timeService::currentTimeMillisUTC, transactionManager,
                referenceDataChangeService::getLastChangeTime);
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    CacheCheckerAtOnceCache cacheCheckerAtOnceCache() {
        return new CacheCheckerAtOnceCache(timeService::currentTimeMillisUTC, transactionManager,
                referenceDataChangeService::getLastChangeTime);
    }

}
