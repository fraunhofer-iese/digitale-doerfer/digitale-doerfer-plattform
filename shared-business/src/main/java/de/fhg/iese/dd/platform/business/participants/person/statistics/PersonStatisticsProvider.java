/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.results.PersonCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.shared.statistics.repos.results.ResultValueByGeoAreaId;

@Component
public class PersonStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_PERSON_UNDELETED = "participants.person.undeleted";
    public static final String STATISTICS_ID_PERSON_DELETED = "participants.person.deleted";
    public static final String STATISTICS_ID_PERSON_CREATED = "participants.person.created";
    public static final String STATISTICS_ID_PERSON_ACTIVE = "participants.person.active";

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_PERSON_UNDELETED)
                .machineName("person-undeleted")
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(7)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_PERSON_CREATED)
                .machineName("person-created")
                .timeRelation(StatisticsTimeRelation.NEW)
                .expectedWidth(7)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_PERSON_DELETED)
                .machineName("person-deleted")
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(7)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_PERSON_ACTIVE)
                .machineName("person-active")
                .timeRelation(StatisticsTimeRelation.ACTIVE)
                .expectedWidth(7)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        if (!CollectionUtils.containsAny(definition.getStatisticsIds(),
                Set.of(STATISTICS_ID_PERSON_UNDELETED,
                        STATISTICS_ID_PERSON_DELETED,
                        STATISTICS_ID_PERSON_CREATED,
                        STATISTICS_ID_PERSON_ACTIVE))) {
            return;
        }

        long lastLoginThreshold = definition.getStartTimeActive();
        long createdThreshold = definition.getStartTimeNew();

        Map<String, PersonCountByGeoAreaId> personCountByGeoAreaId =
                personRepository.countByHomeGeoAreaId(createdThreshold, lastLoginThreshold).stream()
                        .collect(Collectors.toMap(ResultValueByGeoAreaId::getGeoAreaId, Function.identity()));

        for (GeoAreaStatistics geoAreaInfo : geoAreaInfos) {
            final GeoArea geoArea = geoAreaInfo.getGeoArea();
            final Collection<GeoArea> allChildAreas = geoAreaService.getAllChildAreasIncludingSelf(geoArea);

            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_UNDELETED, StatisticsTimeRelation.TOTAL,
                    sumValues(personCountByGeoAreaId, allChildAreas,
                            PersonCountByGeoAreaId::getPersonCountTotalUndeleted));

            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_CREATED, StatisticsTimeRelation.NEW,
                    sumValues(personCountByGeoAreaId, allChildAreas,
                            PersonCountByGeoAreaId::getPersonCountCreated));

            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_DELETED, StatisticsTimeRelation.TOTAL,
                    sumValues(personCountByGeoAreaId, allChildAreas,
                            PersonCountByGeoAreaId::getPersonCountTotalDeleted));

            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_DELETED, StatisticsTimeRelation.NEW,
                    sumValues(personCountByGeoAreaId, allChildAreas,
                            PersonCountByGeoAreaId::getPersonCountNewDeleted));

            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_ACTIVE, StatisticsTimeRelation.ACTIVE,
                    sumValues(personCountByGeoAreaId, allChildAreas,
                            PersonCountByGeoAreaId::getPersonCountActive));
        }
    }

}
