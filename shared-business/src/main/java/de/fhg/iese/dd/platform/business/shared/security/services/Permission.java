/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;

/**
 * Decision if a {@link Person} is allowed to do a specific
 * {@link Action} based on the {@link RoleAssignment}s.
 */
public class Permission {

    public static final Permission DENIED = new Permission(false);
    public static final Permission ALLOWED = new Permission(true);

    private final boolean allowed;

    public Permission(boolean allowed) {
        this.allowed = allowed;
    }

    /**
     * True if the corresponding action is allowed. In sub classes like {@link PermissionEntityIdRestricted} there are
     * more fine grained permissions for specific entities. If one of them is true this method returns true.
     */
    public boolean isAllowed() {
        return allowed;
    }

    /**
     * Inversion of {@link #isAllowed()}
     */
    public boolean isDenied() {
        return !allowed;
    }

}
