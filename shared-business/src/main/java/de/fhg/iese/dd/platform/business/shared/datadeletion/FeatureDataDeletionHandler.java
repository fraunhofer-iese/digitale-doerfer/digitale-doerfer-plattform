/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.datadeletion;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;

@Component
public class FeatureDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private FeatureConfigRepository featureConfigRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        .changedOrDeletedEntity(FeatureConfig.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptySet();
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        long impactedConfigs = featureConfigRepository.countByTenant(tenantToBeDeleted);
        logSummary.info("{} feature configs for this tenant will be deleted", impactedConfigs);
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {
        long deletedConfigs = featureConfigRepository.deleteAllByTenant(tenantToBeDeleted);
        logSummary.info("{} feature configs for this tenant have been deleted", deletedConfigs);
    }

}
