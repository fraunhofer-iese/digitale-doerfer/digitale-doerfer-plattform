/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RelatedEntityIdMustNotBeNullException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RelatedEntityNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RoleAssignmentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.exceptions.RoleNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

public interface IRoleService {

    Set<BaseRole<? extends NamedEntity>> getAllRoles();

    <E extends NamedEntity> BaseRole<E> getRole(Class<?> roleClass) throws RoleNotFoundException;

    String getKeyForRole(Class<? extends BaseRole<? extends NamedEntity>> role) throws RoleNotFoundException;

    BaseRole<? extends NamedEntity> getRoleForKey(String key) throws RoleNotFoundException;

    /**
     * Assign the role to the person for that entity. A check is done if the assignment already exists, then no changes
     * are done. If it does not exist, it is first checked that the corresponding entity exists in the database.
     *
     * @param <E>           entity type of the roles
     * @param person        a person, must not be {@code null}
     * @param relatedEntity
     * @param role
     *
     * @return
     */
    <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, Class<? extends BaseRole<E>> role,
            E relatedEntity);

    /**
     * Assign the role to the person for that entity. A check is done if the assignment already exists, then no changes
     * are done. If it does not exist, it is first checked that the corresponding entity exists in the database.
     *
     * @param <E>             entity type of the roles
     * @param person          a person, must not be {@code null}
     * @param relatedEntityId
     * @param role
     *
     * @return
     */
    <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, Class<? extends BaseRole<E>> role,
            String relatedEntityId);

    /**
     * Assign the role to the person for that entity. A check is done if the assignment already exists, then no changes
     * are done. If it does not exist, it is first checked that the corresponding entity exists in the database.
     *
     * @param <E>             entity type of the roles
     * @param person          a person, must not be {@code null}
     * @param relatedEntityId
     * @param role
     *
     * @return
     */
    <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, BaseRole<E> role, String relatedEntityId);

    /**
     * Check if the person has the given role for this entity.
     *
     * @param <E>           entity type of the roles
     * @param person        a person, must not be {@code null}
     * @param role          the role to check
     * @param relatedEntity the entity the role needs to be assigned for, must not be {@code null}
     *
     * @return <code>true</code> if the person has a roleAssignment with the given entity and the role.
     */
    <E extends NamedEntity> boolean hasRoleForEntity(Person person, Class<? extends BaseRole<E>> role, E relatedEntity);

    /**
     * Check if the person has at least one of the given roles for this entity.
     *
     * @param <E>           entity type of the roles
     * @param person        a person, must not be {@code null}
     * @param roles         the roles to check
     * @param relatedEntity the entity the roles need to be assigned for, must not be {@code null}
     *
     * @return <code>true</code> if the person has at least one roleAssignment with the given entity and one of the
     *         roles.
     */
    <E extends NamedEntity> boolean hasRoleForEntity(Person person, Collection<Class<? extends BaseRole<E>>> roles,
            E relatedEntity);

    /**
     * Check if the person has at least one of the given roles for any entity.
     *
     * @param person a person, must not be {@code null}
     * @param roles  the roles to check
     *
     * @return <code>true</code> if the person has at least one roleAssignment for one of the roles.
     */
    boolean hasRoleForAnyEntity(Person person, Collection<Class<? extends BaseRole<?>>> roles);

    /**
     * Gets the role assignments of a person. The fields {@link RoleAssignment#getRelatedEntity()} and {@link
     * RoleAssignment#getRelatedEntityTenant()} are populated.
     *
     * @param person a person, must not be {@code null}
     *
     * @return the role assignments of the person
     */
    List<RoleAssignment> getCurrentExtendedRoleAssignments(Person person);
    
    /**
     * Gets the all ids of the related entities of the role assignments of the person with these roles.
     *
     * @param person a person, must not be {@code null}
     * @param roles  the roles that the role assignments should have
     *
     * @return the ids of the related entities of the role assignments
     */
    Set<String> getRelatedEntityIdsOfRoleAssignments(Person person, Collection<Class<? extends BaseRole<?>>> roles);

    /**
     * Gets the role assignments of a person with respect to the given tenant: Only assignments that have no related
     * entity and assignments whose related entity belongs to the given tenant are returned. The fields {@link
     * RoleAssignment#getRelatedEntity()} and {@link RoleAssignment#getRelatedEntityTenant()} are populated.
     *
     * @param person a person, must not be {@code null}
     * @param tenant a tenant, must not be {@code null}
     *
     * @return the role assignments of the person in the tenant
     */
    List<RoleAssignment> getCurrentExtendedRoleAssignments(Person person, Tenant tenant);

    /**
     * Gets the role assignments of a person regarding an entity.
     *
     * @param <E>
     * @param person        a person, must not be {@code null}
     * @param relatedEntity an entity, must not be {@code null}
     *
     * @return the role assignments of the person in the entity
     */
    <E extends NamedEntity> List<RoleAssignment> getCurrentRoleAssignmentsForEntity(Person person, E relatedEntity);

    /**
     * Gets all role assignments regarding an entity.
     *
     * @param <E>
     * @param relatedEntity an entity, must not be {@code null}
     *
     * @return the role assignments of the person in the entity
     */
    <E extends NamedEntity> List<RoleAssignment> getAllRoleAssignmentsForEntity(E relatedEntity);

    /**
     * Gets the role assignment of a person regarding a role and an entity.
     *
     * @param person        a person, must not be {@code null}
     * @param relatedEntity an entity, must not be {@code null}
     * @param role          a role, must not be {@code null}
     *
     * @return the role assignment of the person for the role in the entity
     */
    <E extends NamedEntity> Optional<RoleAssignment> getCurrentRoleAssignmentForRoleAndEntity(Person person,
            Class<? extends BaseRole<E>> role, E relatedEntity);

    /**
     * Checks if a person exists that has the given role.
     *
     * @param role
     *
     * @return true if there is such a person
     */
    boolean existsPersonWithRole(Class<? extends BaseRole<? extends NamedEntity>> role);

    /**
     * Get all persons with the given role that does not relate to a specific entity
     *
     * @param role the role the persons should have
     *
     * @return all persons having the role
     */
    List<Person> getAllPersonsWithRole(Class<? extends BaseRole<? extends NamedEntity>> role);

    Page<Person> findAllByAssignedRole(Class<? extends BaseRole<? extends NamedEntity>> role, Pageable page);

    Page<Person> findAllByAssignedRoleAndBelongingToTenantIn(Class<? extends BaseRole<?>> role, Set<String> tenantIds,
            Pageable page);

    /**
     * Get all persons with the given role for the given entity
     *
     * @param <E>
     * @param role
     * @param relatedEntity
     *
     * @return all persons having the role for the given entity
     */
    <E extends NamedEntity> List<Person> getAllPersonsWithRoleForEntity(Class<? extends BaseRole<E>> role,
            E relatedEntity);

    /**
     * Get all persons with at least one of the given role for this entity.
     *
     * @param <E>
     * @param roles
     * @param relatedEntity
     *
     * @return all persons having at least one roleAssignment with the given entity and one of the roles.
     */
    <E extends NamedEntity> List<Person> getAllPersonsWithRoleForEntity(Collection<Class<? extends BaseRole<E>>> roles,
            E relatedEntity);

    /**
     * Return the default person with the Role.COMMUNITY_HELP_DESK role in the tenant
     *
     * @param tenant
     *
     * @return The list of persons having the role for the given entity
     */
    Person getDefaultContactPersonForTenant(Tenant tenant);

    /**
     * Remove a roleAssignment
     *
     * @param roleAssignment
     */
    void removeRoleAssignment(RoleAssignment roleAssignment);

    /**
     * Gets a roleAssignment by its id
     *
     * @param roleAssignmentId
     *
     * @return the role assignment referenced by the id
     *
     * @throws RoleAssignmentNotFoundException
     */
    RoleAssignment getRoleAssignment(String roleAssignmentId) throws RoleAssignmentNotFoundException;

    /**
     * Checks if the given role requires a non-null relatedEntity and the given relatedEntityId is null
     *
     * @param role
     * @param relatedEntityId
     *
     * @throws RelatedEntityIdMustNotBeNullException if the role requires a non-null related entity
     */
    void checkIfRelatedEntityIdCanBeNull(BaseRole<? extends NamedEntity> role, String relatedEntityId)
            throws RelatedEntityIdMustNotBeNullException;

    /**
     * If the given role assignment has a related entity, the {@link RoleAssignment#getRelatedEntity()} and {@link
     * RoleAssignment#getRelatedEntityTenant()} are set appropriately.
     *
     * @param roleAssignment
     *
     * @throws IllegalStateException if the role assigment concerns a role with a related entity, but the related entity
     *                               id is null or the entity is not existing in the database.
     */
    void extend(RoleAssignment roleAssignment);

    /**
     * Get extended role assignments (i.e. role assignments with relatedEntity and relatedTenant set) of the person
     * filtered by the provided listPersonsPermission. I.e. role managers and user admins for tenant A get only those
     * role assignments listed that somehow belong to tenant A, e.g. shop owner of shop X in tenant A.
     *
     * @param listPersonsPermission
     * @param person
     *
     * @return
     */
    List<RoleAssignment> getExtendedRoleAssignmentsOfPersonForListingPermission(
            PermissionTenantRestricted listPersonsPermission, Person person);

    /**
     * If the person is super admin, it is allowed to manage the super admin role and all other roles for all entities.
     * Moreover, a non-rest-admin person is allowed to manage a role for an entity if the person is role manager in all
     * tenants that are affected by the related entity. E.g. a the person must be role manager for tenant x if the
     * person wants to manage the VG admin role of related entity x. And, more complicated, a person must be role
     * manager for tenant x and y if the person wants to manage the shop owner role for a shop entity of a shop that
     * belongs to both tenant x and y.
     *
     * @param <E>
     * @param person
     * @param role
     * @param relatedEntityId
     *
     * @throws RelatedEntityNotFoundException if the related entity does not exist
     * @throws NotAuthorizedException         if the person is not allowed to manage the role
     */
    <E extends NamedEntity> void checkIfPersonIsAllowedToCreateAssignmentOfRoleForEntity(Person person,
            BaseRole<E> role, String relatedEntityId)
            throws RelatedEntityNotFoundException, NotAuthorizedException;

    /**
     * Checks if a person is allowed to remove a role assignment. See {@link IRoleService#checkIfPersonIsAllowedToCreateAssignmentOfRoleForEntity(Person,
     * BaseRole, String)} for the checks. Additionally, if the role assignment is in some inconsistent state (e.g. the
     * related entity was removed or is null even if it should not be null), the assignment can be deleted, too.
     *
     * @param roleAssignment
     */
    void checkIfPersonIsAllowedToDeleteRoleAssignment(Person person, RoleAssignment roleAssignment);

    /**
     * Decides for the person about the permissions it has for the given action.
     * <p/>
     * Uses {@link Action#decidePermission(Collection)} with the role assignments of the person.
     *
     * @param actionClass the action that should be decided upon
     * @param person      the person that wants to perform the action
     * @param <P>         type of permission that is expected
     *
     * @return the permission of the person for the given action, never null
     */
    <P extends Permission> P decidePermission(Class<? extends Action<P>> actionClass, Person person);

    /**
     * Decides for the person about the permissions it has for the given action.
     * <p/>
     * Uses {@link Action#decidePermission(Collection)} with the role assignments of the person.
     *
     * @param actionClass the action that should be decided upon
     * @param person      the person that wants to perform the action
     * @param <P>         type of permission that is expected
     *
     * @return the permission of the person for the given action, never null
     *
     * @throws NotAuthorizedException if the {@link Permission#isDenied()} is true
     */
    <P extends Permission> P decidePermissionAndThrowNotAuthorized(Class<? extends Action<P>> actionClass,
            Person person) throws NotAuthorizedException;

    /**
     * Get the instance of an action.
     * <p/>
     * This is never required for deciding about permissions, use {@link #decidePermission(Class, Person)} instead!
     *
     * @param actionClass the action to be instantiated
     * @param <A>         type of action
     *
     * @return the instantiated action
     */
    <A extends Action<?>> A getAction(Class<A> actionClass);

    <E extends NamedEntity> void deleteAllRoleAssignmentsForRoleAndEntity(Class<? extends BaseRole<E>> role,
            E relatedEntity);

}
