/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import java.lang.management.ManagementFactory;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
class ApplicationLifecycleTeamNotifier {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    private static final String TEAM_NOTIFICATION_TOPIC = "application-lifecycle";

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private IEnvironmentService environmentService;

    @Autowired
    private ITimeService timeService;

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Value( "#{'${application.name:undefined}' != 'undefined' ? '${application.name:unknown}' : '${spring.application.name:unknown}'}" )
    private String applicationName;

    @EventListener
    public void onApplicationEvent(ApplicationReadyEvent event) {

        String applicationDescription = getApplicationDescription();

        //unfortunately there is no way to get the more exact time that is displayed by spring boot in the log
        long startupTimeMillis = System.currentTimeMillis() - event.getApplicationContext().getStartupDate();

        String startupTime = toMinuteSeconds(startupTimeMillis);

        String javaVersion = StringUtils.defaultIfBlank(System.getProperty("java.version"), "unknown");
        String startTimeJVM = timeService.toLocalTime(ManagementFactory.getRuntimeMXBean().getStartTime())
                .format(DATE_TIME_FORMATTER);

        String message = String.format("🚀 Started %s in `%s`%n"
                        + "on JVM `%s` running since `%s`",
                applicationDescription, startupTime, javaVersion, startTimeJVM);

        log.info(message);
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_TECHNICAL_SYSTEM,
                message);
    }

    @EventListener
    public void onApplicationEvent(ContextClosedEvent event) {

        String applicationDescription = getApplicationDescription();

        long uptimeMillis = System.currentTimeMillis() - event.getApplicationContext().getStartupDate();

        String uptime = toMinuteSeconds(uptimeMillis);

        String startDateTime = timeService.toLocalTime(event.getApplicationContext().getStartupDate())
                .format(DATE_TIME_FORMATTER);

        String message = String.format("⏹️ Stopped %s %n"
                        + "running for `%s` (since `%s`)",
                applicationDescription, uptime, startDateTime);

        log.info(message);
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_TECHNICAL_SYSTEM,
                message);
    }

    private String toMinuteSeconds(long milliseconds){
        long seconds =  TimeUnit.MILLISECONDS.toSeconds(milliseconds) % 60;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
        if(minutes < 200){
            return String.format("%02dm:%02ds", minutes, seconds);
        }else{
            minutes %= 60;
            long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);
            return String.format("%dh:%02dm:%02ds", hours, minutes, seconds);
        }
    }

    private String getApplicationDescription() {
        String nodeIdentifier = environmentService.getNodeIdentifier();
        String environments = environmentService.getActiveProfileInfo(true);
        String version = config.getVersionInfo().getPlatformVersion();

        return String.format("*%s* %n"
                        + "on `%s` version `%s` environment `%s`",
                applicationName, nodeIdentifier, version, environments);
    }

}
