/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IDataInitializer extends IDataDependencyAware {

    String USE_CASE_NEW_TENANT = "new_tenant";
    String USE_CASE_NEW_APP_VARIANT = "new_appVariant";

    /**
     * Simple wrapper for the results produced by an {@link IDataInitializer}.
     */
    record DataInitializerResult(String message, List<Throwable> exceptions) {

        /**
         * @return true if no exception occurred during execution
         */
        public boolean wasSuccessful() {

            return CollectionUtils.isEmpty(exceptions);
        }

    }

    @Getter
    @Builder
    class DataInitializerConfiguration {

        @Singular
        private final Set<String> useCases;
        private final String topic;
        @Singular
        private final Set<Class<? extends BaseEntity>> requiredEntities;
        @Singular
        private final Set<Class<? extends BaseEntity>> processedEntities;

    }

    DataInitializerConfiguration getConfiguration();

    /**
     * Human readable use cases that typically require the data initializer to run
     *
     * @return human use cases for the data initializer
     */
    Collection<String> getUseCases();

    /**
     * A human readable topic for the data initializer
     *
     * @return human readable topic for the data initializer
     */
    String getTopic();

    /**
     * Create / update the initial data set for that topic. The data
     * initializers need to be constructed in a way that this method can be
     * called multiple times, also on production data.
     *
     * @param logSummary
     *
     * @return a human readable summary
     */
    DataInitializerResult createInitialData(LogSummary logSummary);

    /**
     * Drop all data for that topic, not only initial data, but all of it.
     * <p>
     * In most cases this is not implemented by the data initializers.
     *
     * @param logSummary
     *
     * @return a human readable summary
     */
    DataInitializerResult dropData(LogSummary logSummary);

}
