/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.legal.dataprivacy;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ISharedDataDeletionService;
import de.fhg.iese.dd.platform.business.shared.legal.services.ILegalTextService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

@Component
public class LegalDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(LegalTextAcceptance.class);

    @Autowired
    private ILegalTextService legalTextService;

    @Autowired
    private ISharedDataDeletionService sharedDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<LegalTextAcceptance> acceptances = legalTextService.findAllAcceptancesByPersonOrderByTimestamp(person);

        if (CollectionUtils.isEmpty(acceptances)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Bedingungen und Verträge",
                        "Zustimmungen zu Bedingungen und Verträgen für Plattform und Dienste");

        //all acceptances to legals without app variant concern the platform
        List<LegalTextAcceptance> acceptancesPlatform = acceptances.stream()
                .filter(a -> Objects.isNull(a.getLegalText().getAppVariant()))
                .collect(Collectors.toList());

        IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                section.newSubSection("Plattform", "Zustimmungen zur Plattform");

        reportAcceptances(subSection, acceptancesPlatform);

        //all acceptances to legals with app variant concern a specific app variant
        Set<AppVariant> appVariantsWithAcceptance = acceptances.stream()
                .filter(a -> Objects.nonNull(a.getLegalText().getAppVariant()))
                .map(a -> a.getLegalText().getAppVariant())
                .collect(Collectors.toSet());

        for (AppVariant appVariant : appVariantsWithAcceptance) {

            subSection =
                    section.newSubSection(appVariant.getName(), "Zustimmungen zum Dienst");

            List<LegalTextAcceptance> acceptancesAppVariant = acceptances.stream()
                    .filter(a -> appVariant.equals(a.getLegalText().getAppVariant()))
                    .collect(Collectors.toList());

            reportAcceptances(subSection, acceptancesAppVariant);
        }
    }

    private void reportAcceptances(IDataPrivacyReport.IDataPrivacyReportSubSection subSection,
            List<LegalTextAcceptance> acceptances) {
        for (LegalTextAcceptance acceptance : acceptances) {
            subSection.newContent("Zustimmung zu " + acceptance.getLegalText().getName(),
                    timeService.toLocalTimeHumanReadable(acceptance.getTimestamp()));
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        final Map<LegalTextAcceptance, DeleteOperation> deletedAcceptances =
                sharedDataDeletionService.deleteAllLegalTextAcceptances(person);

        deletedAcceptances.forEach(personDeletionReport::addEntry);
    }

}
