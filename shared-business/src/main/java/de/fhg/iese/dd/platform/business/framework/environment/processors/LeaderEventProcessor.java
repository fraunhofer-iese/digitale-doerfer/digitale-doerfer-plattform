/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStartedEvent;
import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStoppedEvent;
import de.fhg.iese.dd.platform.business.framework.environment.services.IWorkerService;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;

@EventProcessor
public class LeaderEventProcessor extends BaseEventProcessor {

    @Autowired
    private IWorkerService workerService;

    @EventProcessing
    private void handleCurrentNodeLeaderStartedEvent(CurrentNodeLeaderStartedEvent event) {

        workerService.startWorkerTasks();
    }

    @EventProcessing
    private void handleCurrentNodeLeaderStoppedEvent(CurrentNodeLeaderStoppedEvent event) {

        workerService.stopWorkerTasks();
    }

}
