/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Danielle Korth
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.geoarea.security;

import java.util.Collection;
import java.util.Set;

import de.fhg.iese.dd.platform.business.participants.tenant.security.ActionTenantRestricted;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalConfigurationAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalGroupAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GroupAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.roles.GlobalUserGeneratedContentAdmin;

public class ListGeoAreasExtendedAction extends ActionTenantRestricted {

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_ALL_TENANTS_ALLOWED =
            Set.of(GlobalConfigurationAdmin.class, GlobalGroupAdmin.class, GlobalUserAdmin.class,
                    GlobalUserGeneratedContentAdmin.class);

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_SPECIFIC_TENANTS_ALLOWED =
            Set.of(GroupAdmin.class, UserAdmin.class);

    @Override
    public String getActionDescription() {
        return "Lists all configured GeoAreas and their tenant id";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllTenantsAllowed() {
        return ROLES_ALL_TENANTS_ALLOWED;
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificTenantsAllowed() {
        return ROLES_SPECIFIC_TENANTS_ALLOWED;
    }

}
