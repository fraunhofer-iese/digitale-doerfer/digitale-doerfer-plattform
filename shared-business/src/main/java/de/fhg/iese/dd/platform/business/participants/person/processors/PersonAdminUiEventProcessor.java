/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.processors;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonBlockLoginByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonBlockLoginByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeNameByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonResetPasswordByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonResetPasswordByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUnblockAutomaticallyBlockedLoginByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUnblockAutomaticallyBlockedLoginByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUnblockLoginByAdminConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonUnblockLoginByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OAuthEMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;

@EventProcessor
public class PersonAdminUiEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPersonService personService;

    @Autowired
    private IOauthManagementService oauthManagementService;

    @Autowired
    private IRandomService randomService;

    @EventProcessing
    private PersonCreateByAdminConfirmation handlePersonCreateByAdminRequest(PersonCreateByAdminRequest request) {

        String email = request.getPersonCreateRequest().getEmail();

        if (oauthManagementService.getOauthIdForUser(email) != null) {
            throw new OAuthEMailAlreadyUsedException(email);
        }

        final String oauthId;
        try {
            oauthId = oauthManagementService.createUser(email, randomService.randomPassword(), true);
        } catch (OauthManagementException ex) {
            throw new OauthManagementException("Could not create user in Auth0.");
        }

        PersonCreateRequest personCreateRequest = request.getPersonCreateRequest();
        personCreateRequest.setOauthId(oauthId);

        final Person createdPerson;
        try {
            @SuppressWarnings("deprecation") //this is an intended usage
                    PersonCreateConfirmation personCreateConfirmation =
                    notifyAndWaitForReply(PersonCreateConfirmation.class, personCreateRequest);
            createdPerson = personCreateConfirmation.getPerson();
        } catch (Exception ex) {
            oauthManagementService.deleteUser(oauthId);
            throw ex;
        }

        oauthManagementService.triggerResetPasswordMail(email);

        return new PersonCreateByAdminConfirmation(createdPerson);
    }

    @EventProcessing
    private PersonChangeNameByAdminConfirmation handleChangeNameByAdminRequest(
            PersonChangeNameByAdminRequest request) {
        Person person = request.getPerson();

        person.setFirstName(request.getFirstName());
        person.setLastName(request.getLastName());

        return new PersonChangeNameByAdminConfirmation(personService.store(person));
    }

    @EventProcessing
    private PersonBlockLoginByAdminConfirmation handleBlockLoginByAdminRequest(
            PersonBlockLoginByAdminRequest request) {

        Person person = request.getPerson();
        oauthManagementService.blockUser(person.getOauthId());

        person.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        person = personService.store(person);

        return new PersonBlockLoginByAdminConfirmation(person);
    }

    @EventProcessing
    private PersonUnblockLoginByAdminConfirmation handleUnblockLoginByAdminRequest(
            PersonUnblockLoginByAdminRequest request) {

        Person person = request.getPerson();
        if (person.isDeleted()) {
            try {
                // Deleting the OAuth-account, so the user can register again
                oauthManagementService.deleteUser(person.getOauthId());
            } catch (OauthAccountNotFoundException e) {
                log.info("OAuth account of user {id={}, oauthId={}} not deleted since it was not existing anymore",
                        person.getId(), person.getOauthId());
            }
            person.setOauthId("deleted|" + UUID.randomUUID());
        } else {
            oauthManagementService.unblockUser(person.getOauthId());
        }
        person.getStatuses().removeValue(PersonStatus.LOGIN_BLOCKED);
        person = personService.store(person);

        return new PersonUnblockLoginByAdminConfirmation(person);
    }

    @EventProcessing
    private PersonUnblockAutomaticallyBlockedLoginByAdminConfirmation handleUnblockAutomaticallyBlockedLoginByAdminRequest(
            PersonUnblockAutomaticallyBlockedLoginByAdminRequest request) {

        final Person person = request.getPerson();
        oauthManagementService.unblockAutomaticallyBlockedUser(person.getOauthId());

        return new PersonUnblockAutomaticallyBlockedLoginByAdminConfirmation(person);
    }

    @EventProcessing
    private PersonResetPasswordByAdminConfirmation handleResetPasswordByAdminRequest(
            PersonResetPasswordByAdminRequest request) {

        final Person person = request.getPerson();

        oauthManagementService.changePassword(person.getOauthId(), randomService.randomPassword().toCharArray());

        oauthManagementService.triggerResetPasswordMail(person.getEmail());

        return new PersonResetPasswordByAdminConfirmation(person);
    }

}
