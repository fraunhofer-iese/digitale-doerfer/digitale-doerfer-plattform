/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class CountEntitiesAdminTask extends BaseAdminTask {

    @Autowired
    private List<JpaRepository<?,?>> repositories;

    @Override
    public String getName() {
        return "CountEntities";
    }

    @Override
    public String getDescription() {
        return "Checks the status of the db by querying the number of entities.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {
        logSummary.info("Counting entities for "+repositories.size()+" repositories");
        //unfortunately we only get proxies here, due to the AOP nature of spring data
        //we can ask them for the interfaces they implement and get the JpaRepository interface
        //since we want to order them, they need to be stored in the map
        Map<Class<?>, JpaRepository<?,?>> repositoriesInterfaces = new HashMap<>();

        for(JpaRepository<?,?> repository : repositories){
            //get all the interfaces of the proxies and
            //filter out the right interface that we are interested in
            Class<?> repositoryInterface = Arrays.stream(repository.getClass().getInterfaces())
                    .filter(JpaRepository.class::isAssignableFrom)
                    .findFirst()
                    .orElse(null);
            repositoriesInterfaces.put(repositoryInterface, repository);
        }

        //sort it according to the domain name
        repositoriesInterfaces.entrySet().stream()
            .sorted(Comparator.comparing(e -> e.getKey().getCanonicalName()))
            .forEach(e -> {
                String entityName = e.getKey().getCanonicalName();
                entityName = StringUtils.replace(entityName, "de.fhg.iese.dd.platform.datamanagement.", "");
                entityName = StringUtils.replace(entityName, ".repos", "");
                entityName = StringUtils.replace(entityName, "Repository", "");
                entityName = StringUtils.replace(entityName, "MySQL", "");
                logSummary.info(entityName+": "+e.getValue().count());
            });

    }

}
