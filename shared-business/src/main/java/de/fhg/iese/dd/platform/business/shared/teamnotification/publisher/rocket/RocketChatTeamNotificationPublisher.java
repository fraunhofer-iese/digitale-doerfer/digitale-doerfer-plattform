/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.rocket;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectReader;

import de.fhg.iese.dd.platform.business.shared.teamnotification.BaseHttpTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

class RocketChatTeamNotificationPublisher extends BaseHttpTeamNotificationPublisher {

    public static final long MAX_DELAY_BEFORE_TIMESTAMP_MILLIS = TimeUnit.SECONDS.toMillis(30);
    private static final DateTimeFormatter FORMATTER_TIMESTAMP = DateTimeFormatter.ofPattern("dd.MM. HH:mm");

    private final RocketChatConnectionConfig connectionConfig;
    private LoginResponse.Data auth;
    private final String postUrl;
    private final String loginUrl;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class RocketChatConnectionConfig {

        private String url;
        private String username;
        private String password;

    }

    RocketChatTeamNotificationPublisher(ITimeService timeService, RestTemplateBuilder restTemplateBuilder,
            TeamNotificationConnection connection) {
        super(timeService, restTemplateBuilder, connection);
        this.connectionConfig = readConnectionConfiguration(connection);
        this.postUrl = connectionConfig.getUrl() + "/api/v1/chat.postMessage";
        this.loginUrl = connectionConfig.getUrl() + "/api/v1/login";
    }

    static RocketChatConnectionConfig readConnectionConfiguration(TeamNotificationConnection connection) {
        RocketChatConnectionConfig connectionConfig;
        try {
            ObjectReader connectionConfigReader = defaultJsonReader().forType(RocketChatConnectionConfig.class);
            connectionConfig = connectionConfigReader.readValue(connection.getConnectionConfig());
        } catch (IOException e) {
            throw new TeamNotificationConnectionConfigInvalidException("Connection config invalid in {}: {}",
                    connection, e.getMessage());
        }
        if (connectionConfig == null) {
            throw new TeamNotificationConnectionConfigInvalidException("No connection config in {}", connection);
        }
        if (StringUtils.isEmpty(connectionConfig.getUrl())) {
            throw new TeamNotificationConnectionConfigInvalidException("No url in connection config in {}", connection);
        }
        if (StringUtils.isEmpty(connectionConfig.getUsername())) {
            throw new TeamNotificationConnectionConfigInvalidException("No username in connection config in {}",
                    connection);
        }
        if (StringUtils.isEmpty(connectionConfig.getPassword())) {
            throw new TeamNotificationConnectionConfigInvalidException("No password in connection config in {}",
                    connection);
        }
        return connectionConfig;
    }

    void login() {
        try {
            final LoginRequest request =
                    new LoginRequest(connectionConfig.getUsername(), connectionConfig.getPassword());
            final HttpHeaders headers = getJsonHeaders();
            HttpEntity<LoginRequest> entity = new HttpEntity<>(request, headers);
            final LoginResponse response = getRestTemplate().postForObject(loginUrl, entity, LoginResponse.class);
            if (response == null) {
                throw new IllegalStateException("Login response is empty");
            }
            auth = response.getData();
        } catch (Exception ex) {
            throw new RuntimeException("Could not log in to rocket chat", ex);
        }
    }

    protected void processSendRequest(TeamNotificationSendRequest request) {

        String channelName = request.getChannel();
        TeamNotificationMessage message = request.getMessage();
        if (auth == null) {
            login();
        }
        final String text;
        if (message.getCreated() < (timeService.currentTimeMillisUTC() - MAX_DELAY_BEFORE_TIMESTAMP_MILLIS)) {
            String timestamp = timeService.toLocalTime(message.getCreated()).format(FORMATTER_TIMESTAMP);
            text = "_" + timestamp + "_\n" + message.getPlainText();
        } else {
            text = message.getPlainText();
        }
        final PostMessageRequest messageRequest;
        if (message.getPriority().getLogLevel().isMoreSpecificThan(Level.WARN)) {
            messageRequest = new PostMessageRequest(channelName, "@all " + text, ":rotating_light:");
        } else {
            messageRequest = new PostMessageRequest(channelName, text, null);
        }

        final HttpHeaders headers = getJsonHeaders();
        headers.add("X-Auth-Token", auth.getAuthToken());
        headers.add("X-User-Id", auth.getUserId());

        HttpEntity<PostMessageRequest> entity = new HttpEntity<>(messageRequest, headers);
        ResponseEntity<String> response;

        try {
            response = getRestTemplate().postForEntity(postUrl, entity, String.class);
        } catch (HttpClientErrorException ex) {
            if (HttpStatus.UNAUTHORIZED.equals(ex.getStatusCode())) {
                login();
                response = getRestTemplate().postForEntity(postUrl, entity, String.class);
            } else {
                log.error("Client error, adjust request {}", ex.toString());
                throw ex;
            }
        } catch (HttpServerErrorException ex) {
            throw new RuntimeException("Failed to send message, service returned " + ex);
        }

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error("Unexpected error, service returned {} : {}", response.getStatusCode(), response);
            throw new RuntimeException(
                    "Failed to send message, service returned " + response.getStatusCode() + ": " + response);
        }
    }

}
