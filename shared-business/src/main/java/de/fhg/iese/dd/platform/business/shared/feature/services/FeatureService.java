/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.feature.services;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.ExpiringResourceReference;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotEnabledException;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.InvalidFeatureConfigurationException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget.FeatureTargetLevel;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.Feature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

@Service
class FeatureService extends BaseService implements IFeatureService {

    private static final String FIELD_FEATURE_IDENTIFIER = "featureIdentifier";
    private static final String FIELD_ENABLED = "enabled";
    private static final String FIELD_FEATURE_VALUES_AS_JSON = "featureValuesAsJson";
    private static final String FIELD_VISIBLE_TO_CLIENTS = "visibleForClient";

    /**
     * Only required to add a json filter to feature, in order to remove redundant values from the feature json. This is
     * added as a "mixin" to the base feature
     */
    @JsonFilter("featureSerialization")
    private static class FeatureFilter {

    }

    private static final String FEATURE_PACKAGE_PREFIX = "de.fhg.iese.dd.platform.datamanagement.";

    // cached maps
    private Map<String, BaseFeature> featuresByFeatureKeyIdentifier;
    private MultiValueMap<String, BaseFeature> featuresByFeatureTargetIdentifier;

    private ExpiringResourceReference<ObjectMapper> objectMapperReference;
    private ExpiringResourceReference<ObjectWriter> filteringObjectWriterReference;
    private ExpiringResourceReference<Reflections> reflectionsReference;

    @Autowired
    private FeatureConfigRepository featureConfigRepository;
    @Autowired
    private IReflectionService reflectionService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;
    @Autowired
    private TaskScheduler taskScheduler;

    @PostConstruct
    private void initialize() {
        objectMapperReference =
                new ExpiringResourceReference<>(ObjectMapper::new, null, Duration.ofSeconds(15), taskScheduler);
        filteringObjectWriterReference =
                new ExpiringResourceReference<>(this::createFilteringObjectWriter, null, Duration.ofSeconds(15),
                        taskScheduler);
        reflectionsReference =
                new ExpiringResourceReference<>(this::createReflections, null, Duration.ofSeconds(15), taskScheduler);
        featuresByFeatureKeyIdentifier = new HashMap<>();
        featuresByFeatureTargetIdentifier = CollectionUtils.toMultiValueMap(new HashMap<>());
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(FeatureConfig.class)
                .usedCache(IGeoAreaService.class)
                .build();
    }

    @Override
    @NonNull
    public <F extends BaseFeature> F getFeature(@NonNull Class<F> featureClass, @NonNull FeatureTarget featureTarget)
            throws FeatureNotFoundException {

        return getFeatureOptional(featureClass, featureTarget)
                .orElseThrow(() -> new FeatureNotFoundException(
                        "No feature defined for '{}'. Make sure that data init was executed.", featureClass.getName()));
    }

    @Override
    @Nullable
    public <F extends BaseFeature> F getFeatureOrNull(@NonNull Class<F> featureClass,
            @NonNull FeatureTarget featureTarget) {

        return getFeatureOptional(featureClass, featureTarget).orElse(null);
    }

    @Override
    public <F extends BaseFeature> Optional<F> getFeatureOptional(@NonNull Class<F> featureClass,
            @NonNull FeatureTarget featureTarget) {

        checkChangedConfigurationAndRefresh();

        return Optional.ofNullable(getFeatureInternalOrNull(featureClass, featureTarget));
    }

    @Override
    public boolean isFeatureEnabled(@NonNull Class<? extends BaseFeature> featureClass,
            @NonNull FeatureTarget featureTarget) {

        checkChangedConfigurationAndRefresh();

        BaseFeature feature = getFeatureInternalOrNull(featureClass, featureTarget);
        if (feature == null) {
            return false;
        } else {
            return feature.isEnabled();
        }
    }

    @Override
    public void checkFeatureEnabled(@NonNull Class<? extends BaseFeature> featureClass,
            @NonNull FeatureTarget featureTarget) throws FeatureNotEnabledException {

        if (!isFeatureEnabled(featureClass, featureTarget)) {
            throw new FeatureNotEnabledException(featureClass);
        }
    }

    @Override
    @NonNull
    public List<BaseFeature> getAllFeaturesForTarget(FeatureTarget featureTarget, boolean onlyEnabled) {

        checkChangedConfigurationAndRefresh();

        Map<Class<? extends BaseFeature>, BaseFeature> result = new HashMap<>();

        // we start with the levels in reverse declaration order:
        // From the most generic one, so that we replace it with a more specific one
        FeatureTargetLevel[] levels = FeatureTargetLevel.values();
        for (int i = levels.length - 1; i >= 0; i--) {
            FeatureTargetLevel level = levels[i];

            List<BaseFeature> features =
                    featuresByFeatureTargetIdentifier.get(featureTarget.getFeatureTargetIdentifier(level));
            if (!CollectionUtils.isEmpty(features)) {
                for (BaseFeature feature : features) {
                    //this will replace the more generic ones for the same feature with a more specific one
                    result.put(feature.getClass(), feature);
                }
            }
        }

        return result.values().stream()
                .filter(feature -> !onlyEnabled || feature.isEnabled())
                .sorted(Comparator.comparing(BaseFeature::getFeatureIdentifier))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public <F extends BaseFeature> Map<FeatureConfig, F> getAllFeatureConfigurations(@NotNull Class<F> featureClass) {

        checkChangedConfigurationAndRefresh();
        final List<FeatureConfig> featureConfigs =
                featureConfigRepository.findAllByFeatureClassOrderById(featureClass.getName());

        Map<FeatureConfig, F> result = new HashMap<>();
        for (FeatureConfig featureConfig : featureConfigs) {
            //actually only one feature target should be sufficient, since the resulting configured feature should be the same
            final List<FeatureTarget> featureTargets = featureConfigToFeatureTargets(featureConfig);
            String featureValuesJson = null;
            for (FeatureTarget featureTarget : featureTargets) {
                final F feature = getFeatureInternalOrNull(featureClass, featureTarget);
                if (feature != null) {
                    if (featureValuesJson == null) {
                        featureValuesJson = feature.getFeatureValuesAsJson();
                    }
                    if (!StringUtils.equals(featureValuesJson, feature.getFeatureValuesAsJson())) {
                        // there are different feature configurations for the same initial feature config
                        // this should not happen, it only can if the feature overwrite rules do not work properly
                        log.error("Multiple configured features for the same feature config {}\n{}\n{}",
                                featureClass.getName(),
                                featureValuesJson,
                                feature.getFeatureValuesAsJson());
                    }
                    result.put(featureConfig, feature);
                }
            }
        }
        return result;
    }

    private <F extends BaseFeature> F getFeatureInternalOrNull(@NonNull Class<F> featureClass,
            @NonNull FeatureTarget featureTarget) {

        final FeatureTargetLevel mostSpecificFeatureTargetLevel = featureTarget.getMostSpecificFeatureTargetLevel();
        //these are the levels that are actually possible, because they are less specific
        EnumSet<FeatureTargetLevel> potentialFeatureTargetLevels =
                EnumSet.range(mostSpecificFeatureTargetLevel, FeatureTargetLevel.COMMON);

        for (FeatureTargetLevel featureTargetLevel : potentialFeatureTargetLevels) {
            //we walk up the hierarchy of feature target levels starting with the most specific level
            // (they are organized from most specific to most generic)
            // and try to get a feature key identifier for that level.
            // This might not be possible, if the feature target has too less information

            //feature key identifier identifies the feature at the level we are looking at
            final String featureKeyIdentifier =
                    featureTarget.getFeatureKeyIdentifier(featureClass, featureTargetLevel);
            //if the feature target has not all information required for that level
            // (e.g. if the level is about geo area and the feature target has only a tenant)
            //we need to skip looking this up
            if (featureKeyIdentifier == null) {
                continue;
            }
            final BaseFeature rawFeature = featuresByFeatureKeyIdentifier.get(featureKeyIdentifier);
            if (rawFeature != null) {
                // we found the feature in the hierarchy
                //no need to check for the types, since this is guaranteed by the way they are put in the cache
                @SuppressWarnings("unchecked")
                F feature = (F) rawFeature;
                return feature;
            }
        }
        //there is no feature configuration available for this feature
        return null;
    }

    @Override
    public List<FeatureConfig> getAllFeatureConfigs() {
        return featureConfigRepository.findAllByOrderByFeatureClass();
    }

    @Override
    public List<? extends Class<? extends BaseFeature>> getFeatureClasses() {

        //get all classes that have @Feature annotations
        Set<Class<?>> featureClassesRaw = reflectionsReference.get().getTypesAnnotatedWith(Feature.class);
        //check if all these classes inherit from BaseFeature
        List<Class<?>> invalidFeatureClasses = featureClassesRaw.stream()
                .filter(featureClassRaw -> !BaseFeature.class.isAssignableFrom(featureClassRaw))
                //sorting helps to keep the logs of the same calls the same, too
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        if (!invalidFeatureClasses.isEmpty()) {
            log.error("Feature classes {} are no subtype of BaseFeature, ignoring them.",
                    invalidFeatureClasses);
        }

        //cast the classes to BaseFeature and get the @Feature annotation
        @SuppressWarnings("unchecked")
        List<? extends Class<? extends BaseFeature>> result = featureClassesRaw.stream()
                .filter(BaseFeature.class::isAssignableFrom)
                .map(featureClassRaw -> (Class<? extends BaseFeature>) featureClassRaw)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public List<FeatureMetaModel> getAllFeatureMetaModels() {

        List<? extends Class<? extends BaseFeature>> featureClasses = getFeatureClasses();
        return featureClasses.stream()
                .map(this::getFeatureMetaModel)
                .collect(Collectors.toList());
    }

    @Override
    public long deleteFeatureConfigAppVariant(AppVariant appVariant) {

        long appVariantConfigs = featureConfigRepository.deleteAllByAppVariant(appVariant);
        log.info("Deleted {} app variant specific feature configurations for {}",
                appVariantConfigs, appVariant);

        //update the internal cache
        refreshCachedFeatureConfig();
        return appVariantConfigs;
    }

    @Override
    public void checkIsFeatureConfigValid(FeatureConfig featureConfig) throws InvalidFeatureConfigurationException {
        //reading the configuration is enough, it will fail if it is invalid
        readFeatureConfig(featureConfig, null, null);
    }

    private FeatureMetaModel getFeatureMetaModel(Class<? extends BaseFeature> featureClass) {

        Map<String, Object> propertyMap =
                reflectionService.reflectMetamodel(featureClass, 5,
                        FIELD_FEATURE_VALUES_AS_JSON, FIELD_FEATURE_IDENTIFIER, FIELD_ENABLED,
                        FIELD_VISIBLE_TO_CLIENTS);
        return FeatureMetaModel.builder()
                .featureName(getFeatureIdentifier(featureClass))
                .featureValueTypes(propertyMap)
                .build();
    }

    private Reflections createReflections() {

        return new Reflections(new ConfigurationBuilder()
                .forPackage(FEATURE_PACKAGE_PREFIX)
                .setScanners(Scanners.SubTypes, Scanners.TypesAnnotated));
    }

    private void checkChangedConfigurationAndRefresh() {

        cacheChecker.refreshCacheIfStale(this::refreshCachedFeatureConfig);
    }

    private void refreshCachedFeatureConfig() {

        log.info("Feature configuration change detected, updating...");

        List<FeatureConfig> featureConfigs = query(() -> featureConfigRepository.findAll());
        Map<String, BaseFeature> featuresByFeatureKeyIdentifierNew = new HashMap<>();
        MultiValueMap<String, BaseFeature> featuresByFeatureTargetIdentifierNew =
                CollectionUtils.toMultiValueMap(new HashMap<>());

        Map<String, List<FeatureConfig>> featureConfigsByFeatureClass = featureConfigs.stream()
                .collect(Collectors.groupingBy(FeatureConfig::getFeatureClass));

        for (Map.Entry<String, List<FeatureConfig>> entry : featureConfigsByFeatureClass.entrySet()) {
            String featureClassName = entry.getKey();
            List<FeatureConfig> configs = entry.getValue();
            Class<? extends BaseFeature> featureClass;
            try {
                //it does not matter which config we take to get the feature class, they are all the same
                featureClass = getFeatureClass(configs.get(0));
            } catch (InvalidFeatureConfigurationException e) {
                // We do not fail the complete feature loading here, since we do not want the platform to fail fully
                // if one feature has an invalid configuration.
                // Since the class can not be loaded, we can only continue with loading other feature classes.
                log.error("Failed to read feature config: " + e, e);
                continue;
            }
            Map<FeatureTargetLevel, List<FeatureConfig>> configsByLevel = configs.stream()
                    .collect(Collectors.groupingBy(this::getMaxSpecificLevel));
            // these are the configs for this feature class that have been read already and that are used to get overwritten
            Map<String, FeatureConfig> featureConfigsForFeatureClassByFeatureTargetIdentifier = new HashMap<>();

            // we start with the levels from the most generic one,
            // so that we can use it for getting overwritten by the more specific config
            FeatureTargetLevel[] levels = FeatureTargetLevel.values();
            for (int i = levels.length - 1; i >= 0; i--) {
                FeatureTargetLevel level = levels[i];
                List<FeatureConfig> configsForLevel = configsByLevel.get(level);
                // there can be multiple configs for this level, because there a multiple geoareas, tenants, apps, appvariants
                if (!CollectionUtils.isEmpty(configsForLevel)) {
                    for (FeatureConfig configForLevel : configsForLevel) {
                        final List<FeatureTarget> featureTargets = featureConfigToFeatureTargets(configForLevel);
                        for (FeatureTarget featureTarget : featureTargets) {
                            try {
                                // load the actual feature from the config and overwrite any more generic one
                                BaseFeature feature = readFeatureConfig(configForLevel, featureTarget,
                                        featureConfigsForFeatureClassByFeatureTargetIdentifier);
                                //the identifiers are always available, since the level was determined based on the values of the config
                                String featureKeyIdentifier =
                                        featureTarget.getFeatureKeyIdentifier(featureClass, level);
                                String featureTargetIdentifier = featureTarget.getFeatureTargetIdentifier(level);
                                if (featuresByFeatureKeyIdentifierNew.containsKey(featureKeyIdentifier)) {
                                    throw new InvalidFeatureConfigurationException(
                                            """
                                                    Feature class {} defines multiple configs for the same configuration target {}.
                                                    Caused by config: {}
                                                    Resulting target: {}""",
                                            featureClassName,
                                            level,
                                            configForLevel.toString(),
                                            featureTarget.toString());
                                }
                                featuresByFeatureKeyIdentifierNew.put(featureKeyIdentifier, feature);
                                featuresByFeatureTargetIdentifierNew.add(featureTargetIdentifier, feature);
                                featureConfigsForFeatureClassByFeatureTargetIdentifier.put(featureTargetIdentifier,
                                        configForLevel);
                            } catch (InvalidFeatureConfigurationException e) {
                                // We do not fail the complete feature loading here, since we do not want the platform to
                                // fail fully if one feature has an invalid configuration.
                                // We can continue with the other levels for this feature
                                log.error("Failed to read feature config: " + e, e);
                            }
                        }
                    }
                }
            }
        }
        log.info("Feature configuration update done, {} configurations for {} features",
                featuresByFeatureKeyIdentifierNew.size(), featureConfigsByFeatureClass.size());
        //swap the cached features with the new ones
        featuresByFeatureKeyIdentifier = featuresByFeatureKeyIdentifierNew;
        featuresByFeatureTargetIdentifier = featuresByFeatureTargetIdentifierNew;
    }

    List<FeatureTarget> featureConfigToFeatureTargets(FeatureConfig featureConfig) {

        if (!CollectionUtils.isEmpty(featureConfig.getGeoAreasIncluded())) {
            //the feature config is for a geo area, so it needs to be applied to all sub geo areas, too
            final Set<GeoArea> includedGeoAreas;
            if (featureConfig.isGeoAreaChildrenIncluded()) {
                includedGeoAreas = featureConfig.getGeoAreasIncluded().stream()
                        .flatMap(g -> geoAreaService.getAllChildAreasIncludingSelf(g).stream())
                        .collect(Collectors.toSet());
            } else {
                includedGeoAreas = featureConfig.getGeoAreasIncluded().stream()
                        .map(geoAreaService::ensureCached)
                        .collect(Collectors.toSet());
            }

            featureConfig.getGeoAreasExcluded().stream()
                    .flatMap(g -> geoAreaService.getAllChildAreasIncludingSelf(g).stream())
                    .forEach(includedGeoAreas::remove);

            return includedGeoAreas.stream()
                    .map(geoArea -> new FeatureTarget(
                            geoArea,
                            featureConfig.getTenant(),
                            featureConfig.getAppVariant(),
                            featureConfig.getApp()))
                    .collect(Collectors.toList());
        } else {
            return Collections.singletonList(new FeatureTarget(
                    null,
                    featureConfig.getTenant(),
                    featureConfig.getAppVariant(),
                    featureConfig.getApp()));
        }
    }

    private FeatureTargetLevel getMaxSpecificLevel(FeatureConfig featureConfig) {
        final FeatureTarget featureTarget =
                new FeatureTarget(
                        //this is just a hack to have any geo area, since the calculation only checks if it's null or not
                        CollectionUtils.isEmpty(featureConfig.getGeoAreasIncluded())
                                ? null
                                : featureConfig.getGeoAreasIncluded().iterator().next(),
                        featureConfig.getTenant(),
                        featureConfig.getAppVariant(),
                        featureConfig.getApp());
        return featureTarget.getMostSpecificFeatureTargetLevel();
    }

    private Class<? extends BaseFeature> getFeatureClass(FeatureConfig featureConfig) {
        //the name of the feature class that should be created is stored in the db
        String featureClassName = featureConfig.getFeatureClass();
        Class<?> featureClassRaw;
        try {
            featureClassRaw = Class.forName(featureClassName);
        } catch (ClassNotFoundException e) {
            throw new InvalidFeatureConfigurationException("Feature class {} could not be found: {}",
                    featureClassName, featureConfig.getId());
        }
        if (!BaseFeature.class.isAssignableFrom(featureClassRaw)) {
            throw new InvalidFeatureConfigurationException(
                    "Feature class {} is not a subclass of BaseFeature: {}",
                    featureClassName, featureConfig.getId());
        }
        @SuppressWarnings("unchecked")
        Class<? extends BaseFeature> featureClass = (Class<? extends BaseFeature>) featureClassRaw;
        return featureClass;
    }

    private BaseFeature readFeatureConfig(FeatureConfig featureConfig, FeatureTarget featureConfigTarget,
            Map<String, FeatureConfig> featureConfigsForFeatureClassByFeatureTargetIdentifier)
            throws InvalidFeatureConfigurationException {

        try {
            //log.debug("configuring {}", featureConfigTarget);
            Class<? extends BaseFeature> featureClass = getFeatureClass(featureConfig);
            BaseFeature feature = BeanUtils.instantiateClass(featureClass);
            // "readerForUpdating" means that only values present in the json will be set, the other ones are taken
            // from the featureToOverwrite.
            // Important: it does not create a new instance of the feature, it updates the existing one
            ObjectReader reader = objectMapperReference.get().readerForUpdating(feature);
            if (featureConfigTarget != null &&
                    !CollectionUtils.isEmpty(featureConfigsForFeatureClassByFeatureTargetIdentifier)) {
                // We pick the more generic feature configs from the featureConfigsForFeatureClassByFeatureTargetIdentifier, e.g.
                // common -> tenant -> tenant and app -> tenant and app variant (see FeatureKeyLevel for the complete list)
                // the left ones are overwritten by the right ones

                // we start with the levels from the most generic one,
                // so that we overwrite it with the more specific config step by step
                FeatureTargetLevel[] levels = FeatureTargetLevel.values();
                for (int i = levels.length - 1; i >= 0; i--) {
                    FeatureTargetLevel level = levels[i];
                    FeatureConfig featureToOverwrite =
                            featureConfigsForFeatureClassByFeatureTargetIdentifier.get(
                                    featureConfigTarget.getFeatureTargetIdentifier(level));
                    if (featureToOverwrite != null) {
                        /*
                        log.debug("  overwriting with {} : {}", featureToOverwrite,
                                featureToOverwrite.getConfigValues().replace("\n", " "));

                         */
                        // Updates the feature with the more specific config that match the target identifier.
                        reader.readValue(featureToOverwrite.getConfigValues());
                    }
                }
            }
            if (StringUtils.isEmpty(featureConfig.getConfigValues())) {
                featureConfig.setConfigValues("{}");
            }
            //update the feature with the values from the configuration
            reader.readValue(featureConfig.getConfigValues());

            //this is a hack to avoid having public setters for these values
            setFieldValue(feature, FIELD_FEATURE_IDENTIFIER, getFeatureIdentifier(featureClass));
            setFieldValue(feature, FIELD_ENABLED, featureConfig.isEnabled());
            setFieldValue(feature, FIELD_VISIBLE_TO_CLIENTS, isVisibleForClients(featureClass));
            //store full JSON with all values, except featureValuesAsJson, featureIdentifier and enabled
            setFieldValue(feature, FIELD_FEATURE_VALUES_AS_JSON,
                    filteringObjectWriterReference.get().writeValueAsString(feature));

            return feature;
        } catch (Exception e) {
            throw new InvalidFeatureConfigurationException(
                    "Feature config for feature class {} at feature config '{}' could not be read: {}",
                    featureConfig.getFeatureClass(),
                    featureConfig.getId(),
                    e.toString());
        }
    }

    private void setFieldValue(Object target, String fieldName, Object value) {

        Field field = ReflectionUtils.findField(target.getClass(), fieldName);
        if (field == null) {
            throw new IllegalStateException(
                    "Field " + fieldName + " could not be found, check implementation of " + target.getClass());
        }
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, target, value);
    }

    private ObjectWriter createFilteringObjectWriter() {

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        //this enables adding annotations to a class without actually annotating it
        objectMapper.addMixIn(BaseFeature.class, FeatureFilter.class);

        return objectMapper.writer()
                .with(SerializationFeature.INDENT_OUTPUT)
                .with(new SimpleFilterProvider().addFilter("featureSerialization",
                        //we don't want to serialize these values, since they are stored in
                        // FeatureConfig or FeatureConfigTenant and ignored in the json
                        SimpleBeanPropertyFilter.serializeAllExcept(FIELD_FEATURE_IDENTIFIER, FIELD_ENABLED,
                                FIELD_VISIBLE_TO_CLIENTS,
                                FIELD_FEATURE_VALUES_AS_JSON)));
    }

    private String getFeatureIdentifier(Class<? extends BaseFeature> featureClass) {

        //if the feature defines a custom identifier in the annotation it is used
        Feature featureAnnotation = featureClass.getAnnotation(Feature.class);
        String featureIdentifier = featureAnnotation.featureIdentifier();
        if (StringUtils.isEmpty(featureIdentifier)) {
            //if none is provided, the class name is used
            featureIdentifier = featureClass.getName();
        }
        return featureIdentifier;
    }

    private boolean isVisibleForClients(Class<? extends BaseFeature> featureClass) {

        //if the feature defines a custom identifier in the annotation it is used
        Feature featureAnnotation = featureClass.getAnnotation(Feature.class);
        return featureAnnotation.visibleForClient();
    }

    public void invalidateCache() {
        cacheChecker.resetCacheChecks();
    }

}
