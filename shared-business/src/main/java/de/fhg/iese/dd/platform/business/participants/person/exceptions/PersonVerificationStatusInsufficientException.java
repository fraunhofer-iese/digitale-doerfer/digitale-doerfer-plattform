/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonVerificationStatusRestrictionFeature;

public class PersonVerificationStatusInsufficientException extends NotAuthorizedException {

    private static final long serialVersionUID = -8361141859866590662L;

    public PersonVerificationStatusInsufficientException(
            PersonVerificationStatusRestrictionFeature restrictionFeature) {
        super("The expected person verification statuses 'any of {}' or 'all of {}' do not match the actual ones.",
                restrictionFeature.getAllowedStatusesAnyOf(), restrictionFeature.getAllowedStatusesAllOf());
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT;
    }

}
