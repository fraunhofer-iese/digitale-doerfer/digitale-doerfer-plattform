/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions;

import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.IUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;

public class UserGeneratedContentFlagEntityDeletionFailedException extends InternalServerErrorException {

    private static final long serialVersionUID = -8327952378309578193L;

    //it is important to use this constructor to include the stack trace
    private UserGeneratedContentFlagEntityDeletionFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public static UserGeneratedContentFlagEntityDeletionFailedException forExceptionDuringDeletion(
            UserGeneratedContentFlag flag, IUserGeneratedContentFlagHandler handler, Exception ex) {
        return new UserGeneratedContentFlagEntityDeletionFailedException(
                String.format("The user generated content flag handler %s for flag %s threw exception : %s",
                        handler.getId(), BaseEntity.getIdOf(flag), ex.toString()), ex);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_DELETION_FAILED;
    }

}
