/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.security;

import java.util.Collection;
import java.util.Set;

import de.fhg.iese.dd.platform.business.participants.tenant.security.ActionTenantRestricted;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

/**
 * Action for listing persons with basic information in tenants.
 */
public class ListPersonsAction extends ActionTenantRestricted {

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_ALL_TENANTS_ALLOWED =
            Set.of(SuperAdmin.class, GlobalUserAdmin.class);
    private static final Collection<Class<? extends BaseRole<?>>> ROLES_SPECIFIC_TENANTS_ALLOWED =
            Set.of(UserAdmin.class, RoleManager.class);

    @Override
    public String getActionDescription() {
        return "List persons for tenant t: `person.tenant == t` or `person.homeArea.tenant == t`.\n" +
                "The listed persons only include the fields id, status, first name, last name, email and profile picture";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllTenantsAllowed() {
        return ROLES_ALL_TENANTS_ALLOWED;
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificTenantsAllowed() {
        return ROLES_SPECIFIC_TENANTS_ALLOWED;
    }

}
