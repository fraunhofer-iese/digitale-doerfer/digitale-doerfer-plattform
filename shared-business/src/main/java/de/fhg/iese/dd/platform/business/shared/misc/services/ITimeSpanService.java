/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public interface ITimeSpanService extends IService {

    enum RoundingType { DOWN, UP, ROUND }

    /**
     * Return a TimeSpan with given minutes subtracted from start/end
     * @param timeSpan
     * @param minutes
     * @return
     */
    TimeSpan subtractMinutes(TimeSpan timeSpan, long minutes );

    /**
     * Compute the overlap of two TimeSpans
     * @param span1
     * @param span2
     * @return overlapping TimeSpan or null, if spans do not overlap
     */
    TimeSpan overlap( TimeSpan span1, TimeSpan span2 );

    /**
     * Round start and end of a TimeSpan to minutes accuracy
     * @param span TimeSpan to round
     * @param minutes accuracy
     * @param type RoundingType
     * @return rounded TimeSpan
     */
    TimeSpan round( TimeSpan span, int minutes, RoundingType type );

    /**
     * Round an UTC Timestamp to Minutes accuracy
     * @param time UTC Timestamp
     * @param minutes Minutes to round to (e.g. 5/10/15)
     * @param type RoundingType (DOWN, UP, ROUND)
     * @return
     */
    long roundTimeToMinutes( long time, int minutes, RoundingType type);

}
