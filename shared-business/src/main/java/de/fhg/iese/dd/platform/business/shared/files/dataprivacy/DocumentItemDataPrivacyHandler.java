/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.dataprivacy;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@Component
public class DocumentItemDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            Collections.emptyList();

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(DocumentItem.class);

    @Autowired
    private IDocumentItemService documentItemService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    @Transactional
    public void collectUserData(final Person person, final IDataPrivacyReport dataPrivacyReport) {

        String commonCategoryName = "Allgemein";
        Map<String, List<DocumentItem>> documentItemsByAppOrAppVariantName =
                documentItemService.findAllItemsOwnedByPerson(person).stream()
                        .collect(groupingBy(documentItem -> {
                            // categorize document items by app/app variant. If both are missing, use the common category
                            if (documentItem.getApp() != null) {
                                if (documentItem.getAppVariant() != null) {
                                    return documentItem.getAppVariant().getName();
                                }
                                return documentItem.getApp().getName();
                            }
                            return commonCategoryName;
                        }));
        if (CollectionUtils.isEmpty(documentItemsByAppOrAppVariantName)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Dokumente", "Von dir hochgeladene Dokumente");

        for (Map.Entry<String, List<DocumentItem>> documentItemsEntry : documentItemsByAppOrAppVariantName.entrySet()) {
            String description = "Hochgeladen in der App " + documentItemsEntry.getKey();
            if (documentItemsEntry.getKey().equals(commonCategoryName)) {
                // this description is only used when the document items could not be assigned to an app or app variant
                description = "Hochgeladene Dokumente, die keiner App zugeordnet sind";
            }

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(documentItemsEntry.getKey(), description);

            documentItemsEntry.getValue()
                    .forEach(documentItem -> subSection.newDocumentContent(documentItem.getId(),
                            "Dokument '" + documentItem.getTitle() + "' " +
                                    "mit Beschreibung '" +
                                    Objects.toString(documentItem.getDescription(), "-") + "' " +
                                    "hochgeladen am " + timeService.toLocalTimeHumanReadable(documentItem.getCreated()),
                            documentItem));
        }
    }

    @Override
    public void deleteUserData(final Person person, final IPersonDeletionReport personDeletionReport) {

        List<DocumentItem> documentItems = documentItemService.findAllItemsOwnedByPerson(person);

        personDeletionReport.addEntries(documentItems, DeleteOperation.DELETED);

        documentItemService.deleteAllItemsByOwner(person);
    }

}
