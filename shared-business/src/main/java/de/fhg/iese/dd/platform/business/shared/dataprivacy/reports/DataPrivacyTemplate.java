/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;

public class DataPrivacyTemplate extends TemplateLocation {

    public static final DataPrivacyTemplate DATA_PRIVACY_REPORT_HTML = new DataPrivacyTemplate("dataPrivacyReportHTML.ftl");
    public static final DataPrivacyTemplate DATA_PRIVACY_REPORT_TEXT = new DataPrivacyTemplate("dataPrivacyReportText.ftl");
    public static final DataPrivacyTemplate USER_DELETED_EMAIL = new DataPrivacyTemplate("userDeletedEmail.ftl");
    public static final DataPrivacyTemplate DATA_PRIVACY_EMAIL_TEXT = new DataPrivacyTemplate("dataPrivacyReportEmail.ftl");
    public static final DataPrivacyTemplate USER_DELETION_WARNING_EMAIL = new DataPrivacyTemplate("userDeletionWarningEmail.ftl");
    public static final DataPrivacyTemplate USER_DELETION_DUE_TO_INACTIVITY_EMAIL = new DataPrivacyTemplate("userDeletionDueToInactivityEmail.ftl");

    private DataPrivacyTemplate(String name) {
        super(name);
    }

}
