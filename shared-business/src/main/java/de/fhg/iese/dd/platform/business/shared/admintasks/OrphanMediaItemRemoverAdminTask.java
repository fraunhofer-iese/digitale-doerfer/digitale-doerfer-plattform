/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Component
public class OrphanMediaItemRemoverAdminTask extends BaseAdminTask {

    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    private IMediaItemService mediaItemService;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private DataSource dataSource;

    @Override
    public String getName() {
        return "RemoveOrphanMediaItems";
    }

    @Override
    public String getDescription() {
        return "Removes orphaned media items that are not referenced by other entities anymore";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        String schema;
        try(Connection connection = dataSource.getConnection()){
            schema = connection.getCatalog();
        }
        if(StringUtils.isEmpty(schema)){
            logSummary.error("Could not determine database schema");
            return;
        }

        //find all tables that reference media items
        List<Object[]> tablesReferencingMediaItems = mediaItemRepository.findTableAndColumnReferencingMediaItems(schema);
        logSummary.info("{} references to media items", tablesReferencingMediaItems.size());

        List<String> queriesToSelectOrphans = new ArrayList<>(tablesReferencingMediaItems.size());

        for (final Object[] tableReferencingMediaItem : tablesReferencingMediaItems) {
            final String tableName = (String) tableReferencingMediaItem[0];
            final String columnName = (String) tableReferencingMediaItem[1];
            queriesToSelectOrphans.add("SELECT " + columnName + " FROM " + tableName + ";");
        }

        //find all referenced media items
        final Set<String> referencedMediaItemIds = new HashSet<>();
        for (final String query : queriesToSelectOrphans) {
            @SuppressWarnings("unchecked")
            final List<String> resultList = entityManager.createNativeQuery(query).getResultList();
            referencedMediaItemIds.addAll(resultList);
        }
        //remove all invalid references
        referencedMediaItemIds.remove(null);
        referencedMediaItemIds.remove("");
        if(referencedMediaItemIds.isEmpty()) {
            //if the list is totally empty the following query "select * from media_item where id not in ($empty list$)" is not working
            referencedMediaItemIds.add("never-existing-id");
        }
        long totalMediaItems = mediaItemRepository.count();
        logSummary.info("{} of {} media items in use, {} will be deleted",
                referencedMediaItemIds.size(), totalMediaItems, totalMediaItems - referencedMediaItemIds.size());

        processPages(logSummary, parameters,
                (pageRequest) -> mediaItemRepository
                    .findAllByIdNotInOrderByCreatedDesc(referencedMediaItemIds, pageRequest),
                (mediaItemsToBeDeletedPage) -> {
                    logSummary.info("Deleting files of {} media items",
                            mediaItemsToBeDeletedPage.getNumberOfElements());

                    // we can not use the hibernate delete listener here, because we could not control the parallelism of file deletion
                    processParallel((MediaItem m) -> {
                        logSummary.info("{}: Deleting files", m.getId());
                        if (!parameters.isDryRun()) {
                            mediaItemService.deleteReferencedFiles(m);
                            m.setUrlsJson(null);
                            //we need to save it first, so that the delete listener is not trying to delete the files again
                            mediaItemRepository.save(m);
                        }
                        logSummary.info("{}: Deleting files finished", m.getId());
                    }, mediaItemsToBeDeletedPage.getContent(), logSummary, parameters);

                    mediaItemRepository.flush();
                    logSummary.info("Deleted files of {} media items", mediaItemsToBeDeletedPage.getNumberOfElements());
                });

        logSummary.info("Finished deleting files, now deleting media items themselves");

        processPages(logSummary, parameters,
                (pageRequest) -> mediaItemRepository
                        .findAllByIdNotInAndUrlsJsonIsNullOrderByCreatedDesc(referencedMediaItemIds, pageRequest),
                (mediaItemsToBeDeletedPage) -> {
                    logSummary.info("Deleting {} media items", mediaItemsToBeDeletedPage.getNumberOfElements());
                    if (!parameters.isDryRun()) {
                        mediaItemRepository.deleteAll(mediaItemsToBeDeletedPage.getContent());
                        mediaItemRepository.flush();
                    }
                    logSummary.info("Deleted {} media items", mediaItemsToBeDeletedPage.getNumberOfElements());
                });
    }

}
