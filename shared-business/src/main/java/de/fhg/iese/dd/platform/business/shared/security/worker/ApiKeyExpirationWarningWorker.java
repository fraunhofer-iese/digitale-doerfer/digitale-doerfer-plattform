/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.worker;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.SecurityConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Log4j2
public class ApiKeyExpirationWarningWorker implements IWorkerTask {

    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private SecurityConfig securityConfig;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Override
    public Trigger getTaskTrigger() {
        return new CronTrigger("0 30 3 * * *", timeService.getLocalTimeZone());//every day at 03:30:00
    }

    @Override
    public void run() {

        long reminderTime = timeService.currentTimeMillisUTC() - securityConfig.getApiKeyReminderTime().toMillis();
        long warningTime = timeService.currentTimeMillisUTC() - securityConfig.getApiKeyWarningTime().toMillis();

        List<AppVariant> warningAppVariants = appVariantRepository.findAllByApiKeyCreatedBetween(0, warningTime);
        List<AppVariant> remindingAppVariants =
                appVariantRepository.findAllByApiKeyCreatedBetween(warningTime, reminderTime);

        final String reminderList = CollectionUtils.isEmpty(remindingAppVariants)
                ? ""
                : toAppVariantSummary(remindingAppVariants, "ℹ") + "\n";
        final String warningList = CollectionUtils.isEmpty(warningAppVariants)
                ? ""
                : toAppVariantSummary(warningAppVariants, "⚠");
        if (!CollectionUtils.isEmpty(remindingAppVariants) || !CollectionUtils.isEmpty(warningAppVariants)) {
            teamNotificationService.sendTeamNotification(
                    "old-api-key-warning",
                    warningAppVariants.isEmpty()
                            ? TeamNotificationPriority.INFO_TECHNICAL_SYSTEM
                            : TeamNotificationPriority.WARN_TECHNICAL_SYSTEM,
                    "The following API keys are older than {}/{} days and should be changed:\n```{}{}```",
                    securityConfig.getApiKeyReminderTime().toDays(),
                    securityConfig.getApiKeyWarningTime().toDays(),
                    reminderList,
                    warningList);
        }
    }

    private String toAppVariantSummary(List<AppVariant> expiredAppVariants, String info) {
        return expiredAppVariants.stream()
                .map(appVariant -> String.format(
                        "%s %-80s apiKey1 age: %3s days apiKey2 age: %3s days externalApiKey age: %3s days",
                        info,
                        appVariant.getAppVariantIdentifier(),
                        getApiKeyAge(appVariant.getApiKey1Created()),
                        getApiKeyAge(appVariant.getApiKey2Created()),
                        getApiKeyAge(appVariant.getExternalApiKeyCreated())))
                .collect(Collectors.joining("\n"));
    }

    private String getApiKeyAge(Long created) {
        if (created == null) {
            return "-";
        } else {
            return String.valueOf(TimeUnit.MILLISECONDS.toDays(timeService.currentTimeMillisUTC() - created));
        }
    }

}
