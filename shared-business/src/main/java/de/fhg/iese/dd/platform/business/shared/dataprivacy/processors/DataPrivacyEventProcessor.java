/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Johannes Schneider, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;

@EventProcessor
public class DataPrivacyEventProcessor extends BaseEventProcessor {

    @Autowired
    private ICommonDataPrivacyService commonDataPrivacyService;

    @EventProcessing
    private void handleDataPrivacyReportRequest(DataPrivacyReportRequest request) {

        commonDataPrivacyService.startDataPrivacyDataCollection(request.getPerson(), request.getTrigger());
    }

    @EventProcessing
    private void handlePersonDeleteRequest(PersonDeleteRequest request) {

        commonDataPrivacyService.startDataPrivacyDataDeletion(request.getPerson(), request.getTrigger());
    }

}
