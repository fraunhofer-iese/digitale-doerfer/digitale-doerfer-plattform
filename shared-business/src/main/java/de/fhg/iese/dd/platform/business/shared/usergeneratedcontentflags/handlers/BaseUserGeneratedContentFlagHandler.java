/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;

public abstract class BaseUserGeneratedContentFlagHandler implements IUserGeneratedContentFlagHandler {

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @Autowired
    protected IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @Autowired
    private IReflectionService reflectionService;

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private final String moduleName = determineModuleName();

    @Override
    public final String getId() {
        return this.getClass().getSimpleName() + " [" + getModuleName() + "]";
    }

    @Override
    public String getAdminUiDetailPagePath(UserGeneratedContentFlag flag) {
        return "";
    }

    @Override
    public String toString() {
        return getId();
    }

    private String determineModuleName() {
        if (reflectionService == null) {
            return this.getClass().getPackage().getName();
        }
        return reflectionService.getModuleName(this.getClass());
    }

    /**
     * Publishes the given event and does not wait for a reply. Exceptions caused by this event will not be received.
     *
     * @param <E>   type of the event
     * @param event The event to be sent out
     */
    final protected <E extends BaseEvent> void notify(E event) {
        eventBusAccessor.notifyTrigger(event);
    }

    @SafeVarargs
    @SuppressWarnings("varargs")
    protected static <T> List<T> unmodifiableList(T... entities) {
        return Collections.unmodifiableList(Arrays.asList(entities));
    }

}
