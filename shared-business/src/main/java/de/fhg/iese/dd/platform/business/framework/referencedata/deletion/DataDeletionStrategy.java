/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.deletion;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

/**
 * A deletion strategy encapsulates the code that is necessary to handle the deletion of an instance D of the {@link
 * #triggeringEntity} by manipulating all instances of the {@link #changedOrDeletedEntities} that reference D.
 * <p>
 * To ensure that they are ordered correctly the strategy can specify which {@link #referencedEntities} need to be
 * treated afterwards.
 */
@Getter
@SuperBuilder
public abstract class DataDeletionStrategy<T extends BaseEntity> implements IDataDependencyAware {

    @NonNull
    private final Class<T> triggeringEntity;
    @NonNull
    @Singular
    private final Set<Class<? extends BaseEntity>> referencedEntities;
    @NonNull
    @Singular
    private final Set<Class<? extends BaseEntity>> changedOrDeletedEntities;
    @Setter(AccessLevel.PACKAGE)
    private IReferenceDataDeletionHandler deletionHandler;
    private String id;
    private Set<Class<? extends BaseEntity>> requiredEntities;

    public String getId() {
        if (id == null) {
            id = deletionHandler.getId() + "/" + changedOrDeletedEntities.stream()
                    .map(Class::getSimpleName)
                    .collect(Collectors.joining(", ", "(", ")"));
        }
        return id;
    }

    @Override
    public String toString() {
        return getId();
    }

    @Override
    public final Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        if (requiredEntities == null) {
            requiredEntities = new HashSet<>(referencedEntities);
            // the thing that triggers us is required and needs to be treated afterwards -> sorting is Processed before Required
            requiredEntities.add(triggeringEntity);
            // things we handle on our own are not required
            requiredEntities.removeAll(changedOrDeletedEntities);
        }
        return requiredEntities;
    }

    @Override
    public final Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return changedOrDeletedEntities;
    }

    /**
     * A deletion strategy encapsulates the code that is necessary to handle the deletion of an instance D of the {@link
     * #triggeringEntity} by deleting all instances of the {@link #changedOrDeletedEntities} that reference D.
     * <p>
     * To ensure that they are ordered correctly the strategy can specify which {@link #referencedEntities} need to be
     * treated afterwards.
     */
    @SuperBuilder
    public static final class DeleteDependentEntitiesDeletionStrategy<T extends BaseEntity> extends DataDeletionStrategy<T> {

        @NonNull
        private final BiConsumer<T, LogSummary> checkImpact;
        @NonNull
        private final BiConsumer<T, LogSummary> deleteData;

        /**
         * New strategy that will support deletion of entities that depend on this trigger entity
         */
        public static <T extends BaseEntity> DeleteDependentEntitiesDeletionStrategyBuilder<T, ?, ?> forTriggeringEntity(
                Class<T> triggeringEntity) {
            return new DeleteDependentEntitiesDeletionStrategyBuilderImpl<T>().triggeringEntity(triggeringEntity);
        }

        void checkImpact(T entityToBeDeleted, LogSummary logSummary) {
            checkImpact.accept(entityToBeDeleted, logSummary);
        }

        void deleteData(T entityToBeDeleted, LogSummary logSummary) {
            deleteData.accept(entityToBeDeleted, logSummary);
        }

    }

    /**
     * A deletion strategy encapsulates the code that is necessary to handle the deletion of an instance D of the {@link
     * #triggeringEntity} by changing the references to D to another entity E of all instances of the {@link
     * #changedOrDeletedEntities} that reference D.
     * <p>
     * To ensure that they are ordered correctly the strategy can specify which {@link #referencedEntities} need to be
     * treated afterwards.
     */
    @SuperBuilder
    public static final class SwapReferencesDeletionStrategy<T extends BaseEntity> extends DataDeletionStrategy<T> {

        @NonNull
        private final BaseReferenceDataDeletionHandler.TriConsumer<T, T, LogSummary> checkImpact;
        @NonNull
        private final BaseReferenceDataDeletionHandler.TriConsumer<T, T, LogSummary> swapData;

        /**
         * New strategy that will support swapping references to the trigger entity to another entity
         */
        public static <T extends BaseEntity> SwapReferencesDeletionStrategyBuilder<T, ?, ?> forTriggeringEntity(
                Class<T> triggeringEntity) {
            return new SwapReferencesDeletionStrategyBuilderImpl<T>().triggeringEntity(triggeringEntity);
        }

        void checkImpact(T entityToBeDeleted, T entityToBeUsedInstead, LogSummary logSummary) {
            checkImpact.accept(entityToBeDeleted, entityToBeUsedInstead, logSummary);
        }

        void swapData(T entityToBeDeleted, T entityToBeUsedInstead, LogSummary logSummary) {
            swapData.accept(entityToBeDeleted, entityToBeUsedInstead, logSummary);
        }

    }

}
