/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.change;

import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.business.framework.caching.CacheChecker;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IReferenceDataChangeService extends IService {

    /**
     * Acquires a change lock that enables to change reference data.
     * <p>
     * Before any change in reference data such a lock needs to be acquired. This ensures that only one change at the
     * same time can be done. After the change lock is released, all caches that use {@link CacheChecker} are cleared if
     * they use the default settings.
     *
     * @param initiator     the person triggering the change of reference data, can be null if this is triggered by
     *                      another machine
     * @param changeCommand the command that is used to change the reference data
     *
     * @param coolDownRequired  if true, there is a cool down required after the previous data change
     * @return a log entry that describes the lock. This is not intended to be changed outside this service.
     */
    ReferenceDataChangeLogEntry acquireReferenceDataChangeLock(@Nullable Person initiator, String changeCommand,
            boolean coolDownRequired);

    /**
     * Sets the version info for an ongoing change. This info describes what the change is about. In most cases it will
     * be the commit message of the reference data init files.
     *
     * @param referenceDataChangeLogEntry the log entry that was acquired before
     * @param versionInfo                 info about the changes of the reference data
     *
     * @return the updated log entry
     */
    ReferenceDataChangeLogEntry setDataChangeVersionInfo(ReferenceDataChangeLogEntry referenceDataChangeLogEntry,
            String versionInfo);

    /**
     * Releases a change log that was acquired before. This finalizes the ongoing change and notifies all {@link
     * CacheChecker} if they use the default settings. The flag that the change was successful is not influencing the
     * refresh of the caches.
     *
     * @param referenceDataChangeLogEntry the log entry that was acquired before
     * @param successful                  true if the change was successful, this is just for documentation
     */
    void releaseReferenceDataChangeLock(ReferenceDataChangeLogEntry referenceDataChangeLogEntry,
            boolean successful);

    /**
     * End time of the last change, no matter if it was successful or not.
     * <p>
     * This is mainly required for the{@link CacheChecker} to find out that the reference data changed.
     *
     * @return the most recent end time of a change.
     */
    long getLastChangeTime();

}
