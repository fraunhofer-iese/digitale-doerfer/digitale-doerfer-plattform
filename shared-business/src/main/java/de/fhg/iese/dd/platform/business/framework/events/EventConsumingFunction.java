/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

/**
 * Consumes an event with the possibility to send out events during the consumption using the {@link EventBusAccessor}
 *
 * @param <E>
 *         Type of event to consume
 */
@FunctionalInterface
public interface EventConsumingFunction<E extends BaseEvent> {

    /**
     * Consume an event with the possibility to send out events during the consumption
     *
     * @param event
     *         event that should be consumed
     * @param context
     *         required to send out new events using the {@link EventBusAccessor}
     *
     * @throws Exception
     *         if something fails while consuming the event
     */
    void consumeEvent(E event, EventProcessingContext context) throws Exception;

}
