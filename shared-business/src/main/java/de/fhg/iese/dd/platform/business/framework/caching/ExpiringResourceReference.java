/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.springframework.lang.Nullable;
import org.springframework.scheduling.TaskScheduler;

import lombok.extern.log4j.Log4j2;

/**
 * Reference to an (expensive) resource that gets automatically evicted after some time if it is unused. It manages the
 * thread safe creation and shutdown of the resource. To ensure that the tracking of the resource usage works always use
 * {@link #get()} and do not keep references to the resource.
 * <p/>
 * Typical use cases are resources that are only required during startup or on rare calls.
 */
@Log4j2
public class ExpiringResourceReference<R> {

    private final AtomicReference<R> resourceReference;
    private final Consumer<R> onExpiration;
    private final Supplier<R> resourceSupplier;
    private long lastAccess;
    private final Duration expirationDuration;
    private volatile ScheduledFuture<?> expirationFuture;
    private final TaskScheduler taskScheduler;

    /**
     * Creates a new expiring resource reference
     *
     * @param resourceSupplier   creates the resource when it is requested via {@link #get()} for the first time, can be
     *                           null.
     * @param onExpiration       called when the resource is expired, can be null.
     * @param expirationDuration time span that needs to elapse after the last access until the resource gets evicted
     * @param taskScheduler      Spring task scheduler, required for the automatic expiration
     */
    public ExpiringResourceReference(@Nullable Supplier<R> resourceSupplier, @Nullable Consumer<R> onExpiration,
            Duration expirationDuration, TaskScheduler taskScheduler) {
        this.expirationDuration = expirationDuration;
        this.taskScheduler = taskScheduler;
        this.onExpiration = onExpiration;
        this.resourceSupplier = resourceSupplier;
        this.resourceReference = new AtomicReference<>();
    }

    /**
     * Sets the referenced resource. Not required if a resourceSupplier was set at construction since the resource is
     * automatically created in that case.
     * <p/>
     * The timer until the resource gets evicted is automatically reset.
     *
     * @param resource the resource
     *
     * @return the same resource
     */
    public R set(R resource) {
        this.lastAccess = System.currentTimeMillis();
        this.resourceReference.set(resource);
        ScheduledFuture<?> expirationFuture = this.expirationFuture;
        if (expirationFuture == null) {
            synchronized (this) {
                expirationFuture = this.expirationFuture;
                if (expirationFuture == null) {
                    this.expirationFuture = taskScheduler.scheduleAtFixedRate(this::checkExpiration,
                            Instant.now().plus(expirationDuration).plusMillis(50),
                            expirationDuration.plusMillis(50));
                }
            }
        }
        return resource;
    }

    /**
     * Gets the referenced resource. If a resourceSupplier was set at construction the resource is automatically
     * created.
     * <p/>
     * The timer until the resource gets evicted is automatically reset.
     *
     * @return the resource or null if none is set and no resourceSupplier is available
     */
    public R get() {
        this.lastAccess = System.currentTimeMillis();
        R resource = resourceReference.get();
        if (resource == null && resourceSupplier != null) {
            synchronized (this) {
                resource = this.resourceReference.get();
                if (resource == null) {
                    return this.set(resourceSupplier.get());
                }
            }
        }
        return resource;
    }

    /**
     * Manually expires the resource. If the resource is not null the expiration handler is called.
     */
    public void expire() {
        final R resource = resourceReference.get();
        if (resource != null && log.isTraceEnabled()) {
            log.trace("Expiring resource {}", resource.getClass().getName());
        }
        if (expirationFuture != null) {
            expirationFuture.cancel(false);
            expirationFuture = null;
        }
        if (resource != null && onExpiration != null) {
            onExpiration.accept(resource);
        }
        this.resourceReference.set(null);
    }

    private void checkExpiration() {
        if (lastAccess < (System.currentTimeMillis() - expirationDuration.toMillis())) {
            expire();
        }
    }

}
