/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import java.util.EnumMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

/**
 * Computes traveling time based on (air-) distance and TravelMode
 */
@Service
class PrimitiveRoutingService extends BaseService implements IRoutingService {

    @Autowired
    private ISpatialService spatialService;

    private static final EnumMap<TravelMode, Integer> minutesPerKilometer = new EnumMap<>(TravelMode.class);

    static {
        minutesPerKilometer.put(TravelMode.WALKING, 15);
        minutesPerKilometer.put(TravelMode.BICYCLING, 10);
        minutesPerKilometer.put(TravelMode.DRIVING, 5);
    }

    @Override
    public long routeTimeInMinutes(GPSLocation start, GPSLocation end, TravelMode type) {
        double distKM = spatialService.distanceInKM(start, end);
        double minutes = minutesPerKilometer.get(type) * distKM;
        return Math.round( minutes );
    }

}
