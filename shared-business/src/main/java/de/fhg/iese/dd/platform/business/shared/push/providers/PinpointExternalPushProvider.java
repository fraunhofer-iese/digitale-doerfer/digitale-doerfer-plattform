/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import com.google.common.collect.Lists;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.ExternalPushProviderException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushMessageInvalidException;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessageMinimizable;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import lombok.AccessLevel;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import software.amazon.awssdk.services.pinpoint.PinpointClient;
import software.amazon.awssdk.services.pinpoint.model.*;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Profile("aws | PinpointExternalPushProviderTest")
@Component
class PinpointExternalPushProvider extends PinpointBaseProvider implements IExternalPushProvider {

    // Marker for json log messages
    private static final Marker PUSH_JSON_MARKER = MarkerManager.getMarker("PUSH_JSON");

    // https://docs.aws.amazon.com/pinpoint/latest/developerguide/limits.html
    // Endpoints with the same user ID
    private static final int MAX_NUM_ENDPOINTS_PER_USER = 10;
    // Mobile Push Limits (Should be 4KB, but we stay a bit below, so that we do not run into "message too big" errors)
    private static final int MAX_SIZE_MESSAGE = 4000;
    // Number of messages per batch, this is a limit from AWS pinpoint and can not be changed
    private static final int BATCH_SIZE = 100;

    // only required for testing in order to wait for the completion of background tasks
    @Setter(AccessLevel.PROTECTED)
    private Consumer<Future<?>> backgroundTaskFutureConsumer = f -> {
    };

    @Autowired
    private PinpointExternalPushConfigProvider configProvider;
    @Autowired
    private IParallelismService parallelismService;

    @Override
    public List<ExternalPushApp> listAllPushApps() {
        return configProvider.listAllPushApps();
    }

    @Override
    public ExternalPushApp createOrUpdatePushApp(AppVariant appVariant,
            ExternalPushAppConfiguration externalPushAppConfiguration) throws ExternalPushProviderException {
        return configProvider.createOrUpdatePushApp(appVariant, externalPushAppConfiguration);
    }

    @Override
    public long deleteAppVariant(AppVariant appVariant) {
        return configProvider.deleteAppVariant(appVariant);
    }

    @Override
    public String getPushAppInfoForAppVariant(AppVariant appVariant) {
        return configProvider.getPushAppInfoForAppVariant(appVariant);
    }

    @Override
    public void registerPushEndpoint(String registrationToken, PushPlatformType platformType,
            AppVariant appVariant, Person person) {

        if (pushConfigurationInvalid(appVariant)) {
            return;
        }

        //Under iOS, tokens are sent with spaces, but AWS Pinpoint expects them to be trimmed, since, otherwise,
        //it complains with "Device token is invalid in hex form"
        final String normalizedToken = registrationToken.replace(" ", "");
        final ChannelType channelType = toChannelType(platformType);
        if (log.isTraceEnabled()) {
            log.trace("Registering or updating push endpoint for person with id {}, token '{}' and app variant {}",
                    person.getId(), normalizedToken, appVariant.getAppVariantIdentifier());
        }

        final List<EndpointResponse> endpointsOldestFirst =
                findEndpointsForAppVariantOldestFirst(appVariant, person.getId());
        //we delete all duplicates of the user, since we already have all endpoints available
        deleteDuplicates(appVariant, person.getId(), endpointsOldestFirst);

        boolean endpointWithSameTokenExists = endpointsOldestFirst.stream()
                .anyMatch(ep -> StringUtils.equals(ep.address(), normalizedToken) && ep.channelType() == channelType);
        if (endpointWithSameTokenExists) {
            //there is already an endpoint with the same token
            //updating it would not change anything, the creation date is not changed
            return;
        } else {
            //we need to add another endpoint, remove those that would be over the limit
            deleteOldestEndpointsOverLimit(appVariant, endpointsOldestFirst, person);
        }

        String endpointIdToCreate = UUID.randomUUID().toString();
        callPinpoint(PinpointClient::updateEndpoint, UpdateEndpointRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .endpointId(endpointIdToCreate)
                        .endpointRequest(EndpointRequest.builder()
                                .address(normalizedToken)
                                .channelType(channelType)
                                //.withOptOut("NONE") //default value
                                .user(EndpointUser.builder()
                                        .userId(person.getId())
                                        .build())
                                .build())
                        .build(),
                (r, ex, d) -> logUpdateEndpoint(appVariant, person.getId(), normalizedToken, endpointIdToCreate,
                        r, ex, d));
    }

    private void checkAllEndpointsForPersonAndDeleteDuplicates(AppVariant appVariant, String personId) {

        final List<EndpointResponse> endpointsOldestFirst = findEndpointsForAppVariantOldestFirst(appVariant, personId);

        deleteDuplicates(appVariant, personId, endpointsOldestFirst);
    }

    private void deleteDuplicates(AppVariant appVariant, String personId, List<EndpointResponse> endpointsOldestFirst) {

        //this groups all endpoints by an artificial key of channel type + address, so that duplicates can be identified
        Map<String, List<EndpointResponse>> endpointsByChannelAndAddress = endpointsOldestFirst.stream()
                .collect(Collectors.groupingBy(e -> e.channelType() + e.address()));

        for (Map.Entry<String, List<EndpointResponse>> endpointByChannelAndAddress : endpointsByChannelAndAddress.entrySet()) {
            List<EndpointResponse> endpointsWithSameToken = endpointByChannelAndAddress.getValue();
            if (endpointsWithSameToken.size() > 1) {
                //there are duplicates that need to be deleted
                //leave the first one, which is the oldest, delete the other ones
                endpointsWithSameToken.subList(1, endpointsWithSameToken.size())
                        .forEach(endpointResponse -> deletePushEndpoint(appVariant, personId, endpointResponse,
                                "duplicate"));
            }
        }
    }

    private void deleteOldestEndpointsOverLimit(AppVariant appVariant, List<EndpointResponse> endpointsOldestFirst,
            Person person) {
        int numEndpoints = endpointsOldestFirst.size();
        int endpointsToBeDeleted = (numEndpoints + 1) - MAX_NUM_ENDPOINTS_PER_USER;
        if (endpointsToBeDeleted > 0) {
            //we actually need to delete endpoints because we could not add another one
            endpointsOldestFirst.subList(0, endpointsToBeDeleted).stream()
                    .peek(endpointToBeDeleted -> log.debug(
                            "Person with id {} wants to register a new endpoint, but already has {} of {} " +
                                    "allowed endpoints. Deleting oldest endpoint {}",
                            person.getId(),
                            numEndpoints,
                            MAX_NUM_ENDPOINTS_PER_USER,
                            endpointToBeDeleted))
                    .forEach(endpointResponse -> deletePushEndpoint(appVariant, person.getId(), endpointResponse,
                            "limit-exceeded"));
        }
    }

    private ChannelType toChannelType(PushPlatformType pushPlatformType) {
        switch (pushPlatformType) {
            case GCM:
            case FCM:
                return ChannelType.GCM;
            case APNS_DEV:
            case APNS_DEV_ENTERPRISE:
                return ChannelType.APNS_SANDBOX;
            case APNS_PROD:
            case APNS_PROD_ENTERPRISE:
                return ChannelType.APNS;
            default:
                throw new IllegalStateException("Unexpected enum value PushPlatformType." + pushPlatformType);
        }
    }

    @Override
    public Collection<ExternalPushEndpointDescription> getPushEndpoints(Person person, AppVariant appVariant) {

        if (log.isTraceEnabled()) {
            log.trace("Finding push endpoints for person with id {} and app variant {}", person.getId(),
                    appVariant.getAppVariantIdentifier());
        }

        return findEndpointsForAppVariantOldestFirst(appVariant, person.getId())
                .stream()
                .map(endpoint -> ExternalPushEndpointDescription.builder()
                        .appVariantId(appVariant.getId())
                        .endpointId(endpoint.id())
                        .platformType(endpoint.channelType().toString())
                        .token(endpoint.address())
                        .creationDate(endpoint.creationDate())
                        .userInfo(endpoint.user().toString())
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public void deletePushEndpoint(String registrationToken, PushPlatformType platformType, AppVariant appVariant,
            Person person) {

        if (log.isTraceEnabled()) {
            log.trace("Deleting push endpoint for person with id {}, app variant {} and platform {}", person.getId(),
                    appVariant.getAppVariantIdentifier(), platformType.name());
        }

        List<EndpointResponse> endpoints = findEndpointsForToken(registrationToken, platformType, appVariant, person);
        for (EndpointResponse endpoint : endpoints) {
            deletePushEndpoint(appVariant, person.getId(), endpoint, "unregister");
        }
    }

    private void deletePushEndpoint(AppVariant appVariant, String personId, EndpointResponse endpoint, String trigger) {
        deletePushEndpoint(appVariant, personId, endpoint.id(), endpoint.address(), trigger);
    }

    private void deletePushEndpoint(AppVariant appVariant, String personId, String endpointId, String endpointAddress,
            String trigger) {

        callPinpointIgnoreNotFound(PinpointClient::deleteEndpoint, DeleteEndpointRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .endpointId(endpointId)
                        .build(),
                (r, ex, d) ->
                        logDeletePushEndpoint(appVariant, personId, endpointAddress, endpointId, r, ex, d, trigger));
    }

    private void deletePushUser(AppVariant appVariant, String personId, String trigger) {

        if (pushConfigurationInvalid(appVariant)) {
            return;
        }

        callPinpointIgnoreNotFound(PinpointClient::deleteUserEndpoints, DeleteUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(personId)
                        .build(),
                (r, ex, d) ->
                        logDeletePushUser(appVariant, personId, r, ex, d, trigger));
    }

    private List<EndpointResponse> findEndpointsForAppVariantOldestFirst(
            AppVariant appVariant, String personId) {

        if (pushConfigurationInvalid(appVariant)) {
            return Collections.emptyList();
        }

        GetUserEndpointsResponse response = callPinpointIgnoreNotFound(PinpointClient::getUserEndpoints,
                GetUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(personId)
                        .build());
        if (response != null) {
            return response.endpointsResponse().item()
                    .stream()
                    //creationDate examples: "2019-04-23T07:12:38.612Z","2019-03-26T10:38:35.024Z","2019-04-02T12:06:04.873Z","2019-04-03T15:45:47.321Z"
                    //sort by date ascending = natural string order
                    .sorted(Comparator.comparing(
                            EndpointResponse::creationDate))
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    private List<EndpointResponse> findEndpointsForToken(
            String registrationToken, PushPlatformType platformType,
            AppVariant appVariant, Person person) {

        return findEndpointsForAppVariantOldestFirst(appVariant, person.getId())
                .stream()
                .filter(ep -> ep.address().equals(registrationToken) &&
                        ep.channelType().equals(toChannelType(platformType)))
                .collect(Collectors.toList());
    }

    @Override
    public void deletePushEndpoints(Person person, AppVariant appVariant) {

        if (log.isTraceEnabled()) {
            log.trace("Deleting all push endpoints for person with id {} and app variant {}", person.getId(),
                    appVariant.getAppVariantIdentifier());
        }

        deletePushUser(appVariant, person.getId(), "delete-all");
    }

    @Override
    public void sendMessageToPerson(Set<AppVariant> appVariants, Person person, PushMessage message,
            boolean loudMessage) {

        if (log.isTraceEnabled()) {
            log.trace("Sending {} message {} to person with id {} and app variant(s) {}", loudOrSilent(loudMessage),
                    message.getLogMessage(), person.getId(), toLogString(appVariants));
        }

        final Map<String, EndpointSendConfiguration> map = new HashMap<>();
        map.put(person.getId(), EndpointSendConfiguration.builder().build());

        for (AppVariant appVariant : appVariants) {
            PushMessage messageToSend =
                    getMessageWithValidSizeOrThrowException(appVariant, Collections.singletonList(person.getId()),
                            message, loudMessage);
            sendUsersMessageRequest(appVariant, map, messageToSend, loudMessage, 0L);
        }
    }

    @Override
    public void sendMessageToPersons(AppVariant appVariant, Collection<String> personIdsLoudMessage,
            Collection<String> personIdsSilentMessage, PushMessage message) {

        Objects.requireNonNull(appVariant);

        final Collection<String> loudIds = personIdsLoudMessage == null ? Collections.emptySet() : personIdsLoudMessage;
        final Collection<String> silentIds =
                personIdsLoudMessage == null ? Collections.emptySet() : personIdsSilentMessage;

        if (log.isTraceEnabled()) {
            log.debug("Sending message {} to {} person loud and {} persons silent in app variant {}",
                    message.getLogMessage(), loudIds.size(), silentIds.size(), appVariant.getAppVariantIdentifier());
        }

        PushMessage messageToSendLoud =
                getMessageWithValidSizeOrThrowException(appVariant, loudIds, message, true);
        PushMessage messageToSendSilent =
                getMessageWithValidSizeOrThrowException(appVariant, silentIds, message, false);

        batchSendToUsers(appVariant, loudIds, messageToSendLoud, true);
        batchSendToUsers(appVariant, silentIds, messageToSendSilent, false);
    }

    private PushMessage getMessageWithValidSizeOrThrowException(AppVariant appVariant, Collection<String> personIds,
            PushMessage message, boolean isLoudMessage) {

        String errorMessage;
        String logPushMessage = message.getLogMessage();
        if (isPushMessageSizeValid(message, isLoudMessage)) {
            return message;
        }
        if (message instanceof PushMessageMinimizable messageMinimizable) {
            PushMessage minimizedMessage = messageMinimizable.toMinimizedPushMessage();
            if (isPushMessageSizeValid(minimizedMessage, isLoudMessage)) {
                return minimizedMessage;
            }
            errorMessage = String.format(
                    "Minimized event %s is too big (APNS: %d, FCM: %d) for sending via push",
                    logPushMessage,
                    minimizedMessage.getMessageSizeApns(isLoudMessage),
                    minimizedMessage.getMessageSizeFcm(isLoudMessage));
        } else {
            errorMessage = String.format(
                    "Event %s is too big (APNS: %d, FCM: %d) for sending via push but no small event is provided",
                    logPushMessage,
                    message.getMessageSizeApns(isLoudMessage),
                    message.getMessageSizeFcm(isLoudMessage));
        }
        log.error(errorMessage);
        //we want to be sure to have a detailed log message, even when we would not push to a person
        if (CollectionUtils.isEmpty(personIds)) {
            personIds = Collections.singletonList("");
        }
        for (String personId : personIds) {
            Map<String, String> logMap = new TreeMap<>();
            logMap.put("method", "SENDING");
            logMap.put("requestPattern", "SENDING " + logPushMessage);
            logMap.put("pushMessage", logPushMessage);
            logMap.put("loudMessage", loudOrSilent(isLoudMessage));
            logMap.put("direction", "PUSH");
            logMap.put("personId", personId);
            logMap.put("appVariantIdentifier", appVariant.getAppVariantIdentifier());
            logMap.put("duration", "0");
            logMap.put("exceptionType", "MESSAGE_TOO_BIG");
            logMap.put("exception", errorMessage);
            logMap.put("status", "400");
            log.log(Level.ERROR, PUSH_JSON_MARKER, logMap);
        }
        throw new PushMessageInvalidException(errorMessage);
    }

    private boolean isPushMessageSizeValid(PushMessage message, boolean isLoudMessage) {
        return message.getMessageSizeApns(isLoudMessage) < MAX_SIZE_MESSAGE &&
                message.getMessageSizeFcm(isLoudMessage) < MAX_SIZE_MESSAGE;
    }

    private void batchSendToUsers(AppVariant appVariant, Collection<String> userIds, PushMessage message,
            boolean isLoudMessage) {

        //actual sorting does not matter, we just need at least some kind of sorting if we want to partition
        final List<String> sortedUserIds = userIds.stream()
                .sorted()
                .collect(Collectors.toList());
        //creates the actual batches
        final List<List<String>> partitions = Lists.partition(sortedUserIds, BATCH_SIZE);

        //currently there is no special send config for each user
        final EndpointSendConfiguration sendConfig =
                EndpointSendConfiguration.builder().build();
        //the request for pinpoint wants the user ids mapped to the (same) send config
        final List<Map<String, EndpointSendConfiguration>>
                partitionsConfigured = partitions.stream()
                .map(partition -> partition.stream()
                        .collect(Collectors.toMap(Function.identity(), id -> sendConfig)))
                .collect(Collectors.toList());

        //in order to log how long the delay was we sum it up, so that we know how long every batch waited
        long start = System.currentTimeMillis();
        long delayBetweenMessageBatches = getDelayBetweenMessageBatchesMilliseconds(userIds.size());
        for (Map<String, EndpointSendConfiguration> partition : partitionsConfigured) {
            final long totalDelay = System.currentTimeMillis() - start;
            sendUsersMessageRequest(appVariant, partition, message, isLoudMessage, totalDelay);
            try {
                Thread.sleep(delayBetweenMessageBatches);
            } catch (InterruptedException e) {
                log.warn("Interrupted while waiting for the next batch to send");
                Thread.currentThread().interrupt();
            }
        }
    }

    private long getDelayBetweenMessageBatchesMilliseconds(int numUsers) {

        Duration spreadTime;
        if (numUsers <= 500) {
            //almost no delay
            spreadTime = Duration.ofSeconds(1);
        } else {
            if (numUsers <= 1000) {
                //500 ms delay at batch size 100
                spreadTime = Duration.ofSeconds(5);
            } else {
                if (numUsers <= 2000) {
                    //750 ms delay at batch size 100
                    spreadTime = Duration.ofSeconds(15);
                } else {
                    if (numUsers <= 3000) {
                        //1000 ms delay at batch size 100
                        spreadTime = Duration.ofSeconds(30);
                    } else {
                        if (numUsers <= 5000) {
                            //4800 ms delay at batch size 100 or 1250 users per minute
                            spreadTime = Duration.ofMinutes(4);
                        } else {
                            if (numUsers <= 10000) {
                                //5400 ms delay at batch size 100 or 1111 users per minute
                                spreadTime = Duration.ofMinutes(9);
                            } else {
                                //more than 10000 users, so we slow down
                                //1000 users per minute at batch size 100
                                return Duration.ofSeconds(6).toMillis();
                            }
                        }
                    }
                }
            }
        }
        return spreadTime.toMillis() / Math.max(numUsers / BATCH_SIZE, 1);
    }

    private void sendUsersMessageRequest(AppVariant appVariant,
            Map<String, EndpointSendConfiguration> personIdToSendConfig,
            PushMessage message, boolean isLoudMessage,
            long totalDelay) {

        if (pushConfigurationInvalid(appVariant)) {
            return;
        }

        SendUsersMessagesRequest sendUsersMessagesRequest = SendUsersMessagesRequest.builder()
                .applicationId(appVariant.getPushAppId())
                .sendUsersMessageRequest(SendUsersMessageRequest.builder()
                        .messageConfiguration(DirectMessageConfiguration.builder()
                                .apnsMessage(APNSMessage.builder()
                                        .rawContent(message.getMessageApns(isLoudMessage))
                                        .priority(highOrNormal(isLoudMessage))
                                        .apnsPushType(alertOrBackground(isLoudMessage))
                                        .build())
                                .gcmMessage(GCMMessage.builder()
                                        .rawContent(message.getMessageFcm(isLoudMessage))
                                        .build())
                                .build())
                        .users(personIdToSendConfig)
                        .build())
                .build();

        if (log.isTraceEnabled()) {
            log.trace("Sending {} message to {} users with the following ids: {}", loudOrSilent(isLoudMessage),
                    personIdToSendConfig.size(), personIdToSendConfig.keySet());
        }

        long start = System.currentTimeMillis();
        final SendUsersMessagesResponse sendUsersMessagesResponse =
                callPinpoint(PinpointClient::sendUsersMessages, sendUsersMessagesRequest,
                        (r, ex, d) -> logSendingPushMessages(appVariant,
                                message.getLogMessage(),
                                isLoudMessage, personIdToSendConfig.keySet(), r, ex, d));
        long duration = System.currentTimeMillis() - start;

        if (sendUsersMessagesResponse != null && sendUsersMessagesResponse.sendUsersMessageResponse() != null &&
                sendUsersMessagesResponse.sendUsersMessageResponse().result() != null) {

            //The object lists user IDs and, for each user ID, provides the endpoint IDs that the message was sent to.
            // For each endpoint ID, it provides an EndpointMessageResult object
            // personId -> endpointId -> EndpointMessageResult
            final Map<String, Map<String, EndpointMessageResult>> personIdToEndpointIdToMessageResult =
                    sendUsersMessagesResponse.sendUsersMessageResponse().result();

            for (Map.Entry<String, Map<String, EndpointMessageResult>> personIdToEndpointIdToMessageResultEntry : personIdToEndpointIdToMessageResult.entrySet()) {
                String personId = personIdToEndpointIdToMessageResultEntry.getKey();
                for (Map.Entry<String, EndpointMessageResult> endpointIdToMessageResultEntry : personIdToEndpointIdToMessageResultEntry.getValue()
                        .entrySet()) {
                    String endpointId = endpointIdToMessageResultEntry.getKey();
                    EndpointMessageResult endpointMessageResult =
                            endpointIdToMessageResultEntry.getValue();
                    logSentPushMessage(
                            appVariant,
                            personId,
                            endpointId,
                            message,
                            isLoudMessage,
                            endpointMessageResult,
                            duration,
                            totalDelay);
                    //check if it is a failure that requires us to delete the endpoint
                    //see https://docs.aws.amazon.com/pinpoint/latest/apireference/apps-application-id-users-messages.html#apps-application-id-users-messages-response-body-sendusersmessageresponse-example
                    //error: expired
                    if (StringUtils.isNotEmpty(endpointId) && isExpiredToken(endpointMessageResult)) {
                        if (log.isTraceEnabled()) {
                            log.trace(
                                    "AppVariant {} person {} endpoint with expired token: {}. Deleting Endpoint!",
                                    appVariant.getAppVariantIdentifier(),
                                    personId,
                                    endpointMessageResult);
                        }
                        //run it async to not delay the delivery of other messages
                        backgroundTaskFutureConsumer.accept(parallelismService.getBlockableExecutor().submit(
                                () -> deletePushEndpoint(appVariant, personId, endpointId,
                                        endpointMessageResult.address(), "expired")));
                    }
                    //error: duplicate
                    //in this case no endpoint id is provided, we have to check all endpoints of that person for duplicates
                    if (DeliveryStatus.DUPLICATE.equals(endpointMessageResult.deliveryStatus())) {
                        if (log.isTraceEnabled()) {
                            log.trace(
                                    "AppVariant {} person {} endpoint with duplicate endpoint: {}. Checking endpoints for duplicates!",
                                    appVariant.getAppVariantIdentifier(),
                                    personId,
                                    endpointMessageResult);
                        }
                        //run it async to not delay the delivery of other messages
                        backgroundTaskFutureConsumer.accept(parallelismService.getBlockableExecutor().submit(
                                () -> checkAllEndpointsForPersonAndDeleteDuplicates(appVariant, personId)));
                    }
                    // deleting the user when there are no endpoints is useless!
                    // pinpoint returns the same error ("No endpoints found for userId.") for a random user id
                    // the only solution is to not push to users that do not have endpoints
                }
            }
        }
    }

    private boolean isExpiredToken(
            EndpointMessageResult endpointMessageResult) {
        return DeliveryStatus.PERMANENT_FAILURE.equals(
                endpointMessageResult.deliveryStatus()) &&
                //410 (gone) is used by APNS and FCM to indicate that the endpoint no longer exists
                endpointMessageResult.statusCode() == 410;
    }

    private boolean isPushMessageInvalid(
            EndpointMessageResult endpointMessageResult) {
        return StringUtils.contains(endpointMessageResult.statusMessage(), "Invalid notification");
    }

    private boolean isNoEndpointsForUser(
            EndpointMessageResult endpointMessageResult) {
        return "No endpoints found for userId.".equals(endpointMessageResult.statusMessage());
    }

    private void logUpdateEndpoint(AppVariant appVariant, String personId, String endpointAddress,
            String endpointId, UpdateEndpointResponse response, PinpointException ex, long duration) {

        Map<String, String> logMap = new TreeMap<>();
        logMap.put("method", "REGISTER");
        logMap.put("requestPattern", "REGISTER {endpoint}");
        logMap.put("endpointAddress", endpointAddress);
        logMap.put("endpointId", endpointId);
        if (response != null) {
            logMap.put("status", String.valueOf(response.sdkHttpResponse().statusCode()));
        }
        logPushOperation(appVariant, personId, ex, duration, logMap);
    }

    private void logSendingPushMessages(AppVariant appVariant, String logPushMessage, boolean isLoudMessage,
            Collection<String> personIds, @SuppressWarnings("unused") SendUsersMessagesResponse response,
            PinpointException ex, long duration) {

        //since this request does not contain more information than the actual response we only log it if it threw an
        // exception
        if (ex != null) {
            for (String personId : personIds) {
                logSendingPushMessage(appVariant, personId, logPushMessage, isLoudMessage, ex, duration);
            }
        }
    }

    private void logSendingPushMessage(AppVariant appVariant, String personId, String logPushMessage,
            boolean isLoudMessage, PinpointException ex, long duration) {

        Map<String, String> logMap = new TreeMap<>();
        logMap.put("method", "SENDING");
        logMap.put("requestPattern", "SENDING " + logPushMessage);
        logMap.put("pushMessage", logPushMessage);
        logMap.put("loudMessage", loudOrSilent(isLoudMessage));
        logPushOperation(appVariant, personId, ex, duration, logMap);
    }

    private void logSentPushMessage(AppVariant appVariant, String personId, String endpointId,
            PushMessage pushMessage, boolean isLoudMessage,
            EndpointMessageResult endpointMessageResult,
            long duration, long totalDelay) {

        String pushMessageLogMessage = pushMessage.getLogMessage();
        Map<String, String> logMap = new TreeMap<>();
        logMap.put("method", "SEND");
        logMap.put("requestPattern", "SEND " + pushMessageLogMessage);
        logMap.put("pushMessage", pushMessageLogMessage);
        logMap.put("pushMessage.apns.size", Integer.toString(pushMessage.getMessageSizeApns(isLoudMessage)));
        logMap.put("pushMessage.fcm.size", Integer.toString(pushMessage.getMessageSizeFcm(isLoudMessage)));
        logMap.put("loudMessage", loudOrSilent(isLoudMessage));
        logMap.put("endpointAddress", endpointMessageResult.address());
        logMap.put("endpointId", endpointId);
        logMap.put("deliveryStatus", endpointMessageResult.deliveryStatus().toString());
        if (!DeliveryStatus.SUCCESSFUL.equals(
                endpointMessageResult.deliveryStatus())) {
            if (isNoEndpointsForUser(endpointMessageResult)) {
                //this is equivalent to "user does not exist at pinpoint"
                logMap.put("exceptionType", endpointMessageResult.deliveryStatus() + " (NO ENDPOINTS)");
            } else if (isExpiredToken(endpointMessageResult)) {
                logMap.put("exceptionType", endpointMessageResult.deliveryStatus() + " (ENDPOINT EXPIRED)");
            } else if (isPushMessageInvalid(endpointMessageResult)) {
                logMap.put("exceptionType", endpointMessageResult.deliveryStatus() + " (MESSAGE INVALID)");
            } else {
                logMap.put("exceptionType", endpointMessageResult.deliveryStatus().toString());
            }
        }
        if (endpointMessageResult.statusCode() != null) {
            logMap.put("status", endpointMessageResult.statusCode().toString());
        }
        logMap.put("statusMessage", endpointMessageResult.statusMessage());
        logMap.put("messageId", endpointMessageResult.messageId());
        logMap.put("delay", Long.toString(totalDelay));
        logPushOperation(appVariant, personId, null, duration, logMap);
    }

    private void logDeletePushUser(AppVariant appVariant, String personId, DeleteUserEndpointsResponse response,
            PinpointException ex, long duration, String trigger) {

        Map<String, String> logMap = new TreeMap<>();
        logMap.put("method", "DELETE");
        logMap.put("requestPattern", "DELETE {person} " + trigger);
        logMap.put("trigger", trigger);
        if (response != null) {
            logMap.put("status", String.valueOf(response.sdkHttpResponse().statusCode()));
        }
        logPushOperation(appVariant, personId, ex, duration, logMap);
    }

    private void logDeletePushEndpoint(AppVariant appVariant, String personId, String endpointAddress,
            String endpointId, DeleteEndpointResponse response, PinpointException ex, long duration, String trigger) {

        Map<String, String> logMap = new TreeMap<>();
        logMap.put("method", "DELETE");
        logMap.put("requestPattern", "DELETE {endpoint} " + trigger);
        logMap.put("endpointAddress", endpointAddress);
        logMap.put("endpointId", endpointId);
        logMap.put("trigger", trigger);
        if (response != null) {
            logMap.put("status", String.valueOf(response.sdkHttpResponse().statusCode()));
        }
        logPushOperation(appVariant, personId, ex, duration, logMap);
    }

    private void logPushOperation(AppVariant appVariant, String personId, PinpointException ex,
            long duration, Map<String, String> logMap) {

        //direction PUSH appears weird here, but is consistent to IN and OUT used in TraceInterceptor
        logMap.put("direction", "PUSH");
        logMap.put("personId", personId);
        logMap.put("appVariantIdentifier", appVariant.getAppVariantIdentifier());
        logMap.put("duration", Long.toString(duration));
        if (ex != null) {
            logMap.put("exceptionType", ex.getClass().getSimpleName());
            logMap.put("exception", ex.getMessage());
            logMap.put("status", ex.awsErrorDetails().errorCode());
            log.log(Level.ERROR, PUSH_JSON_MARKER, logMap);
        } else {
            log.log(Level.DEBUG, PUSH_JSON_MARKER, logMap);
        }
    }

    private String loudOrSilent(boolean loud) {
        return loud ? "loud" : "silent";
    }

    private String highOrNormal(boolean loud) {
        return loud ? "high" : "normal";
    }

    private String alertOrBackground(boolean loud) {
        return loud ? "alert" : "background";
    }

    private String toLogString(Set<AppVariant> appVariants) {
        final String logString = appVariants.stream()
                .map(AppVariant::getAppVariantIdentifier)
                .collect(Collectors.joining(", "));
        return logString.isEmpty() ? "<none>" : logString;
    }

}
