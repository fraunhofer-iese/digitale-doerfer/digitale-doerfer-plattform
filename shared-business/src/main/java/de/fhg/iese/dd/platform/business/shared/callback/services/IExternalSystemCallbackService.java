/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.callback.services;

import de.fhg.iese.dd.platform.business.shared.callback.exceptions.ExternalSystemCallFailedException;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.http.HttpMethod;

import javax.annotation.Nullable;
import java.net.URI;

public interface IExternalSystemCallbackService {

    <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, Class<T> bodyClass)
            throws ExternalSystemCallFailedException;

    <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, Object requestBodyObject, Class<T> bodyClass)
            throws ExternalSystemCallFailedException;

    <T> ExternalSystemResponse<T> callExternalSystem(AppVariant appVariant, HttpMethod method,
            String urlSuffix, String requestBody, Class<T> bodyClass)
            throws ExternalSystemCallFailedException;

    <T> ExternalSystemResponse<T> callExternalSystem(@Nullable AppVariant appVariant, HttpMethod method,
                                                     URI uri, String requestBody, Class<T> bodyClass)
            throws ExternalSystemCallFailedException;
}
