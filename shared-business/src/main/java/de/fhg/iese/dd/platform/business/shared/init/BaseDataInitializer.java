/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.OptBoolean;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import de.fhg.iese.dd.platform.business.framework.reflection.exceptions.ReflectionException;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IOpeningHoursService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.DefaultMediaItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.init.IDataInitializerFileService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.jsonTypeFactory;

public abstract class BaseDataInitializer implements IDataInitializer {

    private static final long LATEST_ALLOWED_CREATED =
            ZonedDateTime.of(2015, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toInstant().toEpochMilli();

    private static final Pattern UUID_PATTERN =
            Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", Pattern.CASE_INSENSITIVE);
    //x_UUID.x
    private static final Pattern TENANT_ID_PATTERN =
            Pattern.compile("[a-zA-Z0-9]_([a-zA-Z0-9-]+)\\.[a-zA-Z0-9]",
                    Pattern.CASE_INSENSITIVE);
    //x__tag.x
    private static final Pattern TENANT_TAG_PATTERN =
            Pattern.compile("[a-zA-Z0-9]__([a-zA-Z0-9-]+)\\.[a-zA-Z0-9]",
                    Pattern.CASE_INSENSITIVE);
    //x/x/env/x.x
    private static final Pattern ENVIRONMENT_PATTERN =
            Pattern.compile(
                    "[a-zA-Z0-9_-]+[/\\\\][a-zA-Z0-9_-]+[/\\\\]([a-zA-Z0-9_-]+)[/\\\\][a-zA-Z0-9_-]+\\.[a-zA-Z0-9]",
                    Pattern.CASE_INSENSITIVE);
    private static final Pattern MODULE_NAME_PATTERN =
            Pattern.compile(
                    "init-([a-zA-Z0-9_-]+)[/\\\\]",
                    Pattern.CASE_INSENSITIVE);
    /**
     * Parses dates in the format YYYYMMDD to UTC time
     */
    private static final DateTimeFormatter DATE_TIME_FORMATTER_YYYYMMDD = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.YEAR, 4)
            .appendValue(ChronoField.MONTH_OF_YEAR, 2)
            .appendValue(ChronoField.DAY_OF_MONTH, 2)
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
            .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
            .parseDefaulting(ChronoField.OFFSET_SECONDS, 0)
            .toFormatter();

    /**
     * Parses dates in the format YYYYMMDDhhmm to UTC time
     */
    private static final DateTimeFormatter DATE_TIME_FORMATTER_YYYYMMDDHHMM = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(ChronoField.YEAR, 4)
            .appendValue(ChronoField.MONTH_OF_YEAR, 2)
            .appendValue(ChronoField.DAY_OF_MONTH, 2)
            .appendValue(ChronoField.HOUR_OF_DAY, 2)
            .appendValue(ChronoField.MINUTE_OF_HOUR, 2)
            .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
            .parseDefaulting(ChronoField.MILLI_OF_SECOND, 0)
            .parseDefaulting(ChronoField.OFFSET_SECONDS, 0)
            .toFormatter();

    private static ObjectReader initObjectReader() {
        final ObjectMapper objectMapper = new ObjectMapper();
        //forces Jackson to use no args constructor when deserializing. In this constructor, default values can be applied.
        objectMapper.setVisibility(
                objectMapper.getVisibilityChecker().withCreatorVisibility(JsonAutoDetect.Visibility.PUBLIC_ONLY));
        objectMapper.addMixIn(BaseEntity.class, BaseEntityMixin.class);
        return objectMapper.reader()
                //trick part 2) tell jackson to inject null as defaultId
                .with(new InjectableValues.Std().addValue("defaultId", null))
                .with(JsonParser.Feature.ALLOW_COMMENTS)
                .with(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    protected final ObjectReader PRECONFIGURED_READER = initObjectReader();

    private static class BaseEntityMixin {

        //this trick is to ensure that the random uuid that is used as default in the BaseEntity, is overwritten with null
        //trick part 1) Tells jackson to use the injectable value with the identifier "defaultId" if not defined in the json
        @JacksonInject(value = "defaultId", useInput = OptBoolean.TRUE)
        protected String id;

    }

    /**
     * Default section to be used in the map in data json files when no specific differentiation is needed.
     */
    protected static final String DATA_JSON_SECTION_DEFAULT = "default";

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}),
            value = AccessLevel.PROTECTED)
    private final DataInitializerConfiguration cachedConfiguration = getConfiguration();

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private final String moduleName = determineModuleName();

    @Autowired
    protected IEnvironmentService environmentService;

    @Autowired
    protected IReflectionService reflectionService;

    @Autowired
    protected ITimeService timeService;

    @Autowired
    protected IMediaItemService mediaItemService;

    @Autowired
    protected IAddressService addressService;

    @Autowired
    protected IOpeningHoursService openingHoursService;

    @Autowired
    private IDataInitializerFileService dataInitializerFileService;

    @Autowired
    private AppRepository appRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private TenantRepository tenantRepository;

    @SafeVarargs
    @SuppressWarnings("varargs")
    protected static <T> List<T> unmodifiableList(T... entities) {
        return Collections.unmodifiableList(Arrays.asList(entities));
    }

    public static final class DuplicateIdChecker<T extends BaseEntity> {

        private final Map<Function<T, String>, Set<String>> idExtractorToExistingIds;

        public DuplicateIdChecker() {
            idExtractorToExistingIds = Collections.singletonMap(BaseEntity::getId, new HashSet<>());
        }

        public DuplicateIdChecker(Function<T, String> additionalIdToCheck) {
            idExtractorToExistingIds = new HashMap<>(2);
            idExtractorToExistingIds.put(additionalIdToCheck, new HashSet<>());
            idExtractorToExistingIds.put(BaseEntity::getId, new HashSet<>());
        }

        public void checkId(T entity) {
            for (Map.Entry<Function<T, String>, Set<String>> idExtractorAndExistingIds : idExtractorToExistingIds.entrySet()) {
                String idToCheck = idExtractorAndExistingIds.getKey().apply(entity);
                Set<String> existingIdSet = idExtractorAndExistingIds.getValue();
                if (existingIdSet.contains(idToCheck)) {
                    throw new DataInitializationException("Duplicate id '{}': {}", idToCheck, entity);
                }
                existingIdSet.add(idToCheck);
            }
        }

    }

    /**
     * Checks if the jsonString is valid JSON. Empty string and null is also valid
     *
     * @param jsonString the json to check
     * @param fieldName  name of the field, used in error message.
     *
     * @throws DataInitializationException if the string cannot be parsed as JSON
     */
    public static void checkValidJson(String jsonString, String fieldName) throws DataInitializationException {
        if (StringUtils.isEmpty(jsonString)) {
            return;
        }
        try {
            JsonMapping.defaultJsonReader().readTree(jsonString);
        } catch (JsonProcessingException e) {
            throw new DataInitializationException("Invalid JSON content in field " + fieldName, e);
        }
    }

    @Override
    public final String getId() {
        return this.getClass().getSimpleName()
                + " (" + getTopic() + ") "
                + " [" + getModuleName() + "]";
    }

    @Override
    public final Collection<String> getUseCases() {
        return this.getCachedConfiguration().getUseCases();
    }

    @Override
    public String getTopic() {
        return this.getCachedConfiguration().getTopic();
    }

    @Override
    public final Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return this.getCachedConfiguration().getRequiredEntities();
    }

    @Override
    public final Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return this.getCachedConfiguration().getProcessedEntities();
    }

    @Override
    public DataInitializerResult createInitialData(LogSummary logSummary) {
        logSummary.setLogger(log);
        createInitialDataSimple(logSummary);
        return new DataInitializerResult(null, Collections.emptyList());
    }

    protected void createInitialDataSimple(LogSummary logSummary) {
        throw new NotImplementedException("create initial data is not implemented for this data initializer");
    }

    @Override
    public DataInitializerResult dropData(LogSummary logSummary) {
        logSummary.setLogger(log);
        dropDataSimple(logSummary);
        return new DataInitializerResult(null, Collections.emptyList());
    }

    protected void dropDataSimple(LogSummary logSummary) {
        logSummary.info("No drop implemented");
    }

    /**
     * Checks if the field is null or, for Strings also if it is empty and throws a {@link DataInitializationException}
     * if this is true.
     */
    protected <T, F> void checkFieldNotNull(T entity, Function<T, F> accessor, String fieldName) {
        F fieldValue = accessor.apply(entity);
        if (fieldValue == null) {
            throw new DataInitializationException("Field '{}' of entity {} is null", fieldName, entity.toString());
        }
        if (fieldValue instanceof String && StringUtils.isEmpty((String) fieldValue)) {
            throw new DataInitializationException("Field '{}' of entity {} is empty", fieldName, entity.toString());
        }
    }

    /**
     * Checks if the id of the given entity is a valid UUID, if not it tries to look it up using
     * {@link IReflectionService#findConstantByFullyQualifiedName(String, Class)} and checks if the looked up value is a
     * valid UUID again.
     * <p/>
     * Sets the value in {@link BaseEntity#setId(String)} and returns the entity.
     *
     * @param entity the entity to check
     *
     * @throws DataInitializationException if the id is invalid
     */
    protected <E extends BaseEntity> E checkId(E entity) throws DataInitializationException {
        try {
            String id = checkOrLookupId(entity.getId());
            entity.setId(id);
            return entity;
        } catch (DataInitializationException e) {
            throw new DataInitializationException("Id of entity '" + entity + "' is invalid", e);
        }
    }

    /**
     * Checks if the given id is a valid UUID, if not it tries to look it up using
     * {@link IReflectionService#findConstantByFullyQualifiedName(String, Class)} and checks the looked up value if it
     * is a valid UUID again.
     *
     * @param idOrFullyQualifiedName the id or reference to an id
     *
     * @return the id
     *
     * @throws DataInitializationException if the id is not a valid UUID and could not be looked up
     */
    protected String checkOrLookupId(String idOrFullyQualifiedName) throws DataInitializationException {
        if (StringUtils.isEmpty(idOrFullyQualifiedName)) {
            throw new DataInitializationException("Id is empty");
        }
        if (UUID_PATTERN.matcher(idOrFullyQualifiedName).matches()) {
            return idOrFullyQualifiedName;
        } else {
            String constantId = findConstantByFullyQualifiedName(idOrFullyQualifiedName, String.class);
            if (UUID_PATTERN.matcher(constantId).matches()) {
                return constantId;
            } else {
                throw new DataInitializationException("Looked up id '{}' is invalid according to UUID standard",
                        idOrFullyQualifiedName);
            }
        }
    }

    /**
     * @see IReflectionService#findConstantByFullyQualifiedName(String, Class)
     */
    protected <T> T findConstantByFullyQualifiedName(String fullyQualifiedName, Class<T> expectedType)
            throws DataInitializationException {
        try {
            return reflectionService.findConstantByFullyQualifiedName(fullyQualifiedName, expectedType);
        } catch (ReflectionException e) {
            throw new DataInitializationException(e);
        }
    }

    /**
     * Checks if the created timestamp is newer than 01.01.2015 (no entity should be older than the system itself) and
     * later than now. If the timestamp is out of the range it is set to now.
     *
     * @param <T>    type of the entity
     * @param entity the entity
     *
     * @return the entity with the adjusted created timestamp
     */
    protected <T extends BaseEntity> T adjustCreated(T entity) {
        if (entity.getCreated() <= LATEST_ALLOWED_CREATED || entity.getCreated() > timeService.currentTimeMillisUTC()) {
            entity.setCreated(timeService.currentTimeMillisUTC());
        }
        return entity;
    }

    /**
     * Creates a new image for an entity loaded from a data json file and therefore, the owner of that image is always
     * an app or app variant. Does not fail if there is no image defined.
     * <p/>
     * Uses the id attribute of the {@link MediaItem} entity that was found in the entity to create a new one by calling
     * {@link IMediaItemService#createMediaItemFromDefault(String, FileOwnership)}.
     *
     * @param accessor         reference to the getter of the image
     * @param consumer         reference to the setter of the image
     * @param entity           the entity to add the image
     * @param fileOwnership    the owner of a media item
     * @param logSummary the log summary for the data init
     * @param warnOnEmptyImage if true a warning is logged if no image is defined
     * @param <T>              type of the entity
     *
     * @return the entity with the image set
     */
    protected <T extends BaseEntity> T createImage(Function<T, MediaItem> accessor, BiConsumer<T, MediaItem> consumer,
            T entity, FileOwnership fileOwnership, LogSummary logSummary, boolean warnOnEmptyImage) {
        logSummary.indent();
        //we use the media item that we load from the json just to get the location
        MediaItem locationImage = accessor.apply(entity);
        if (locationImage != null) {
            MediaItem image = createMediaItem(entity, locationImage, fileOwnership, logSummary);
            //we need to ensure that the image is also set if it is null, to avoid having the json image stored
            consumer.accept(entity, image);
        } else {
            if (warnOnEmptyImage) {
                logSummary.warn("No image is defined for {} '{}'", entity.getClass().getSimpleName(), entity.getId());
            }
        }
        logSummary.outdent();
        return entity;
    }

    /**
     * Creates all the images for an entity loaded from a data json file. Does not fail if there is no image defined.
     * <p/>
     * Uses the id attribute of the {@link MediaItem} entity that was found in the entity to create a new one by calling
     * {@link IMediaItemService#createMediaItemFromDefault(String, FileOwnership)}.
     *
     * @param <T>        type of the entity
     * @param accessor   reference to the getter of the list of images
     * @param consumer   reference to the setter of the list of images
     * @param entity     the entity to add the images
     * @param logSummary the log summary for the data init
     *
     * @return the entity with the images set
     */
    protected <T extends BaseEntity> T createImages(Function<T, List<MediaItem>> accessor,
            BiConsumer<T, List<MediaItem>> consumer, T entity, FileOwnership fileOwnership, LogSummary logSummary,
            boolean warnOnEmptyImage) {
        logSummary.indent();
        List<MediaItem> locationImages = accessor.apply(entity);
        if (!CollectionUtils.isEmpty(locationImages)) {
            List<MediaItem> newImages = new ArrayList<>(locationImages.size());
            for (MediaItem locationImage : locationImages) {
                MediaItem createdImage = createMediaItem(entity, locationImage, fileOwnership, logSummary);
                if (createdImage != null) {
                    newImages.add(createdImage);
                }
            }
            consumer.accept(entity, newImages);
        } else {
            if (warnOnEmptyImage) {
                logSummary.warn("No images are defined for {} '{}'", entity.getClass().getSimpleName(), entity.getId());
            }
        }
        logSummary.outdent();
        return entity;
    }

    protected MediaItem createMediaItem(BaseEntity entity, MediaItem locationImage, FileOwnership fileOwnership,
            LogSummary logSummary) {
        return createMediaItemFromLocation(entity, locationImage.getId(), fileOwnership, logSummary);
    }

    protected MediaItem createMediaItemFromLocation(BaseEntity entity, String location, FileOwnership fileOwnership,
            LogSummary logSummary) {
        try {
            return mediaItemService.createMediaItemFromDefault(location, fileOwnership);
        } catch (DefaultMediaItemNotFoundException e) {
            logSummary.warn("Default image '{}' for {} '{}' not existing, image is set to null", location,
                    entity.getClass().getSimpleName(), entity.getId());
        } catch (FileItemUploadException e) {
            logSummary.error("Failed to get image '{}' for {} '{}'", e, location, entity.getClass().getSimpleName(),
                    entity.getId());
        }
        return null;
    }

    /**
     * Creates an Address for an entity loaded from a data json file. Does not fail if there are no  addresses defined.
     * <p/>
     * The addresses are looked up by {@link IAddressService#findOrCreateAddress(IAddressService.AddressDefinition,
     * IAddressService.AddressFindStrategy, IAddressService.GPSResolutionStrategy, boolean)}, the created Address
     * get a constant id.
     *
     * @param <T>
     *         type of the entity
     * @param accessor
     *         reference to the getter of the Address
     * @param consumer
     *         reference to the setter of the Address
     * @param entity
     * @param logSummary
     *
     * @return
     */
    protected <T extends BaseEntity> T createAddress(Function<T, Address> accessor,
            BiConsumer<T, Address> consumer, T entity, LogSummary logSummary) {
        logSummary.indent();
        Address address = accessor.apply(entity);
        if (address != null) {
            Address existingAddress = addressService.findOrCreateAddress(
                    IAddressService.AddressDefinition.fromAddress(address),
                    IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                    IAddressService.GPSResolutionStrategy.PROVIDED_LOOKEDUP_NONE,
                    false);
            consumer.accept(entity, existingAddress);
        } else {
            logSummary.warn("No address defined for entity '{}'", entity.getId());
        }
        logSummary.outdent();
        return entity;
    }

    /**
     * Creates all AddressListEntries for an entity loaded from a data json file. Does not fail if there are no
     * addresses defined.
     * <p/>
     * The addresses are looked up by {@link IAddressService#findOrCreateAddress(IAddressService.AddressDefinition,
     * IAddressService.AddressFindStrategy, IAddressService.GPSResolutionStrategy, boolean)}, the created
     * AddressListEntries get a constant id.
     *
     * @param <T>
     *         type of the entity
     * @param accessor
     *         reference to the getter of the AddressListEntries
     * @param consumer
     *         reference to the setter of the AddressListEntries
     * @param entity
     * @param logSummary
     *
     * @return
     */
    protected <T extends BaseEntity> T createAddresses(Function<T, Set<AddressListEntry>> accessor,
            BiConsumer<T, Set<AddressListEntry>> consumer, T entity, LogSummary logSummary) {
        logSummary.indent();
        Set<AddressListEntry> addressListEntries = accessor.apply(entity);
        if (addressListEntries != null) {
            Set<AddressListEntry> newAddressListEntries = new HashSet<>(addressListEntries.size());
            for (AddressListEntry addressListEntry : addressListEntries) {
                Address existingAddress = addressService.findOrCreateAddress(
                        IAddressService.AddressDefinition.fromAddress(addressListEntry.getAddress()),
                        IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                        IAddressService.GPSResolutionStrategy.PROVIDED_LOOKEDUP_NONE,
                        false);
                newAddressListEntries.add(addressService.storeAddressListEntry(
                        new AddressListEntry(existingAddress, addressListEntry.getName())
                                .withConstantId(entity)));
            }
            consumer.accept(entity, newAddressListEntries);
        } else {
            logSummary.warn("No addresses are defined for entity '{}'", entity.getId());
        }
        logSummary.outdent();
        return entity;
    }

    /**
     * Creates OpeningHours for an entity loaded from a data json
     * file. Does not fail if there are no opening hours defined.
     * <p/>
     * The created opening hours and entries get a constant id.
     *
     * @param accessor
     *            reference to the getter of the opening hours
     * @param consumer
     *            reference to the setter of the opening hours
     * @param entity
     * @param logSummary
     * @return
     */
    protected <T extends BaseEntity> T createOpeningHours(Function<T, OpeningHours> accessor,
            BiConsumer<T, OpeningHours> consumer, T entity, LogSummary logSummary) {
        logSummary.indent();
        OpeningHours openingHours = accessor.apply(entity);
        if (openingHours != null) {
            for (OpeningHoursEntry entry : openingHours.getEntries()) {
                entry.setFromTime(toMillis(entry.getFromTime()));
                entry.setToTime(toMillis(entry.getToTime()));
                openingHoursService.store(entry.withConstantId(openingHours));
            }
            openingHours = openingHoursService.store(openingHours.withConstantId(entity));
            consumer.accept(entity, openingHours);
        } else {
            logSummary.warn("No opening hours are defined for entity '{}'", entity.getId());
        }
        logSummary.outdent();
        return entity;
    }

    /**
     * Converts a human readable timestamp in the form hhmm to milliseconds
     *
     * @param hoursMinutes
     *
     * @return
     */
    private long toMillis(long hoursMinutes) {
        long fromHours = hoursMinutes / 100L;
        long fromMinutes = hoursMinutes - (fromHours * 100L);
        return TimeUnit.HOURS.toMillis(fromHours) + TimeUnit.MINUTES.toMillis(fromMinutes);
    }

    /**
     * Loads all data init entities for the given topic and entity type. All files from all modules get loaded.
     * <p/>
     * The data init files need to follow one of these patterns to be loaded:
     * <pre>
     * init-* /{environment}/{topic}/{entityType}.json
     * init-* /{environment}/{topic}/{entityType}_{tenantId}.json
     * init-* /{environment}/{topic}/{entityType}__{tenantTag}.json
     * init-* /{topic}/{entityType}.json
     * init-* /{topic}/{entityType}_{tenantId}.json
     * init-* /{topic}/{entityType}__{tenantTag}.json
     * </pre>
     *
     * @param entityType The type of the expected entities
     *
     * @return all entities, regardless of the keys
     */
    protected <T> List<T> loadAllEntities(Class<T> entityType) {
        return loadAllDataInitKeysAndEntities(entityType).values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * Loads all data init entities for the given topic and entity type and their respective keys. All files from all
     * modules get loaded.
     * <p/>
     * The data init files need to follow one of these patterns to be loaded:
     * <pre>
     * init-* /{environment}/{topic}/{entityType}.json
     * init-* /{environment}/{topic}/{entityType}_{tenantId}.json
     * init-* /{environment}/{topic}/{entityType}__{tenantTag}.json
     * init-* /{topic}/{entityType}.json
     * init-* /{topic}/{entityType}_{tenantId}.json
     * init-* /{topic}/{entityType}__{tenantTag}.json
     * </pre>
     *
     * @param entityType The type of the expected entities
     *
     * @return all entities and according keys
     */
    protected <T> Map<DataInitKey, List<T>> loadAllDataInitKeysAndEntities(Class<T> entityType) {
        return loadAllDataInitKeysAndEntities(entityType.getSimpleName(), entityType);
    }

    /**
     * Loads all data init entities for the given topic and entity type and their respective keys. All files from all
     * modules get loaded.
     * <p/>
     * The data init files need to follow one of these patterns to be loaded:
     * <pre>
     * init-* /{environment}/{topic}/{fileNameWithoutExtension}.json
     * init-* /{environment}/{topic}/{fileNameWithoutExtension}_{tenantId}.json
     * init-* /{environment}/{topic}/{fileNameWithoutExtension}__{tenantTag}.json
     * init-* /{topic}/{fileNameWithoutExtension}.json
     * init-* /{topic}/{fileNameWithoutExtension}_{tenantId}.json
     * init-* /{topic}/{fileNameWithoutExtension}__{tenantTag}.json
     * </pre>
     *
     * @param entityType The type of the expected entities
     *
     * @return all entities and according keys
     */
    protected <T> Map<DataInitKey, List<T>> loadAllDataInitKeysAndEntities(String fileNameWithoutExtension,
            Class<T> entityType) {

        //create pattern for all relevant types, these are all with the same topic and entity
        final Pattern filePattern = createDataInitFilePatternForTopicAndEntity(fileNameWithoutExtension);
        //get all relevant files by pattern
        final Collection<String> dataInitFileNames = dataInitializerFileService.findDataInitFiles(filePattern);
        //load all files and initially combine the values of the keys
        MultiValueMap<DataInitKeyRaw<T>, T> combinedDataInitKeysRaw = new LinkedMultiValueMap<>();
        for (String dataInitFileName : dataInitFileNames) {
            final List<DataInitKeyRaw<T>> dataInitFileContent = loadDataInitFile(dataInitFileName, entityType);
            dataInitFileContent.forEach(c -> c.setFileName(dataInitFileName));

            //setting all key details based on the file name
            final String tenantId = extractFromPattern(dataInitFileName, TENANT_ID_PATTERN);
            if (StringUtils.isNotEmpty(tenantId)) {
                //we do not just overwrite the tenant id, since it is possible to set it in the json itself
                dataInitFileContent.forEach(c -> c.setTenantId(tenantId));
            }
            final String tenantTag = extractFromPattern(dataInitFileName, TENANT_TAG_PATTERN);
            dataInitFileContent.forEach(c -> c.setTenantTag(tenantTag));

            final String environmentIdentifier = extractFromPattern(dataInitFileName, ENVIRONMENT_PATTERN);
            dataInitFileContent.forEach(c -> c.setEnvironmentIdentifier(environmentIdentifier));

            final String moduleName = extractFromPattern(dataInitFileName, MODULE_NAME_PATTERN);
            dataInitFileContent.forEach(c -> c.setModuleName(moduleName));

            //all file contents get combined in a single map, since the same key could be specified in one file
            dataInitFileContent.forEach(c -> combinedDataInitKeysRaw.addAll(c,
                    c.getValues() == null ? Collections.emptyList() : c.getValues()));
        }

        //find out what gets overwritten (environment specific overwrites common)
        Set<DataInitKeyRaw<T>> overwrittenKeys = new HashSet<>();
        for (DataInitKeyRaw<T> keyOuter : combinedDataInitKeysRaw.keySet()) {
            for (DataInitKeyRaw<T> keyInner : combinedDataInitKeysRaw.keySet()) {
                if (isLeftOverwritingRight(keyOuter, keyInner)) {
                    overwrittenKeys.add(keyInner);
                }
            }
        }
        overwrittenKeys.forEach(combinedDataInitKeysRaw::remove);

        MultiValueMap<DataInitKey, T> combinedDataInitKeys = new LinkedMultiValueMap<>();

        for (Map.Entry<DataInitKeyRaw<T>, List<T>> entryRaw : combinedDataInitKeysRaw.entrySet()) {
            //expand DataInitKeyRaw to 1-n DataInitKey
            final List<DataInitKey> keys = findReferencedEntities(entryRaw.getKey());
            for (DataInitKey newKey : keys) {

                //check if one of the keys already exists in the result combined keys
                //it is important that the equals for the key does _not_ include the file names
                final Optional<DataInitKey> existingKeyOpt = combinedDataInitKeys.keySet().stream()
                        .filter(k -> k.equals(newKey))
                        .findFirst();
                if (existingKeyOpt.isPresent()) {
                    //there is already the same key, so we add the file name to the existing ones
                    final DataInitKey existingKey = existingKeyOpt.get();
                    existingKey.addFileName(newKey.getFileNames());
                    if (log.isTraceEnabled()) {
                        log.trace("Combining contents into one key: '{}'", existingKey.getFileNames());
                    }
                }
                //adding all the values to a new or the existing key
                combinedDataInitKeys.addAll(newKey, entryRaw.getValue());
            }
        }
        return combinedDataInitKeys;
    }

    private Pattern createDataInitFilePatternForTopicAndEntity(String fileNameWithoutExtension) {
        //we only want init files for the current environments
        final String allowedEnvironments = environmentService.getActiveProfiles(true).stream()
                .map(Pattern::quote)
                .collect(Collectors.joining("|", "(", ")"));
        //module/topic(/(env1|env2))/entity(_id or tag).json
        return Pattern.compile(
                "[a-zA-Z0-9_-]+[/\\\\]" +
                        Pattern.quote(getTopic()) + "([/\\\\]" +
                        allowedEnvironments + ")?[/\\\\]" +
                        fileNameWithoutExtension + "(_[a-zA-Z0-9_-]+)?\\.json");
    }

    private boolean isLeftOverwritingRight(DataInitKeyRaw<?> left, DataInitKeyRaw<?> right) {
        //overwriting only works in the same module
        if (StringUtils.equals(left.getModuleName(), right.getModuleName())) {
            //left is specific for an environment, right is not
            if (StringUtils.isNotEmpty(left.getEnvironmentIdentifier()) &&
                    StringUtils.isEmpty(right.getEnvironmentIdentifier())) {
                //if left and right have the same tenant (or both none), the more specific left overwrites the less specific right
                return StringUtils.equals(left.getTenantId(), right.getTenantId());
            }
        }
        return false;
    }

    private <T> List<DataInitKeyRaw<T>> loadDataInitFile(String location, Class<T> entityType) {

        try (final InputStream inputStream = dataInitializerFileService.loadInitDataFile(location)) {

            //this constructs List<DataInitKeyRaw<EntityType>>
            final JavaType keyType = jsonTypeFactory().constructParametricType(DataInitKeyRaw.class, entityType);
            final CollectionType keyListType = jsonTypeFactory().constructCollectionType(List.class, keyType);

            final List<DataInitKeyRaw<T>> dataInitKeyRaws = PRECONFIGURED_READER
                    .forType(keyListType)
                    .readValue(inputStream);
            //if there is no content at all we need to fake some content, so that the overwrite rules can be evaluated
            if (CollectionUtils.isEmpty(dataInitKeyRaws)) {
                return Collections.singletonList(new DataInitKeyRaw<>());
            } else {
                return dataInitKeyRaws;
            }

        } catch (IOException e) {
            throw new DataInitializationException("Could not open data json at '" + location + "': " + e.getMessage(),
                    e);
        }
    }

    private String extractFromPattern(String dataInitFileName, Pattern pattern) {
        final Matcher matcher = pattern.matcher(dataInitFileName);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    @SuppressFBWarnings(value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "Spotbugs seems to not understand that the getters of DataInitKeyRaw will " +
                    "always return the same value.")
    private List<DataInitKey> findReferencedEntities(DataInitKeyRaw<?> initKeyRaw) {

        DataInitKey.DataInitKeyBuilder dataInitKeyBuilder = DataInitKey.builder()
                .geoAreaChildrenIncluded(initKeyRaw.isGeoAreaChildrenIncluded())
                .fileNames(initKeyRaw.getFileName());
        if (StringUtils.isNotBlank(initKeyRaw.getAppIdentifier())) {
            final App app = appRepository.findByAppIdentifier(initKeyRaw.getAppIdentifier())
                    .orElseThrow(() -> new DataInitializationException(
                            "Could not find App with AppIdentifier '{}', check file {}",
                            initKeyRaw.getAppIdentifier(),
                            initKeyRaw.getFileName()));
            dataInitKeyBuilder = dataInitKeyBuilder.app(app);
        }
        if (StringUtils.isNotBlank(initKeyRaw.getAppVariantIdentifier())) {
            final AppVariant appVariant =
                    appVariantRepository.findByAppVariantIdentifier(initKeyRaw.getAppVariantIdentifier())
                            .orElseThrow(() -> new DataInitializationException(
                                    "Could not find AppVariant with AppVariantIdentifier '{}', check file {}",
                                    initKeyRaw.getAppVariantIdentifier(),
                                    initKeyRaw.getFileName()));
            dataInitKeyBuilder = dataInitKeyBuilder.appVariant(appVariant);
        }
        if (StringUtils.isNotBlank(initKeyRaw.getGeoAreaId())) {
            //this is just a shortcut for only having one included geo area
            if (initKeyRaw.getGeoAreaIdsIncluded() == null) {
                initKeyRaw.setGeoAreaIdsIncluded(new ArrayList<>());
            }
            initKeyRaw.getGeoAreaIdsIncluded().add(initKeyRaw.getGeoAreaId());
        }
        if (!CollectionUtils.isEmpty(initKeyRaw.getGeoAreaIdsIncluded())) {
            final Set<GeoArea> geoAreasIncluded =
                    geoAreaRepository.findAllByIdIn(initKeyRaw.getGeoAreaIdsIncluded());
            if (geoAreasIncluded.size() != initKeyRaw.getGeoAreaIdsIncluded().size()) {
                throw new DataInitializationException(
                        "Could not find included GeoAreas with ids '{}', check file {}",
                        BaseEntityService.getMissingIds(geoAreasIncluded, initKeyRaw.getGeoAreaIdsIncluded()),
                        initKeyRaw.getFileName());
            }
            dataInitKeyBuilder = dataInitKeyBuilder.geoAreasIncluded(geoAreasIncluded);
        }
        if (!CollectionUtils.isEmpty(initKeyRaw.getGeoAreaIdsExcluded())) {
            final Set<GeoArea> geoAreasExcluded =
                    geoAreaRepository.findAllByIdIn(initKeyRaw.getGeoAreaIdsExcluded());
            if (geoAreasExcluded.size() != initKeyRaw.getGeoAreaIdsExcluded().size()) {
                throw new DataInitializationException(
                        "Could not find excluded GeoAreas with ids '{}', check file {}",
                        BaseEntityService.getMissingIds(geoAreasExcluded, initKeyRaw.getGeoAreaIdsExcluded()),
                        initKeyRaw.getFileName());
            }
            dataInitKeyBuilder = dataInitKeyBuilder.geoAreasExcluded(geoAreasExcluded);
        }
        if (StringUtils.isNotBlank(initKeyRaw.getTenantId())) {
            final Tenant tenant = tenantRepository.findById(initKeyRaw.getTenantId())
                    .orElseThrow(() -> new DataInitializationException(
                            "Could not find Tenant with id '{}', check file {}",
                            initKeyRaw.getTenantId(),
                            initKeyRaw.getFileName()));
            dataInitKeyBuilder = dataInitKeyBuilder.tenant(tenant);
        }
        if (StringUtils.isNotBlank(initKeyRaw.getTenantTag())) {
            final List<Tenant> tenants = tenantRepository.findAllByTagOrderByNameDesc(initKeyRaw.getTenantTag());
            if (CollectionUtils.isEmpty(tenants)) {
                throw new DataInitializationException("Could not find any Tenant with tag '{}', check file {}",
                        initKeyRaw.getTenantTag(),
                        initKeyRaw.getFileName());
            }
            //in case of a tenant tag we have multiple tenants, so we have multiple keys
            DataInitKey.DataInitKeyBuilder finalDataInitKeyBuilder = dataInitKeyBuilder;
            return tenants.stream()
                    .map(t -> finalDataInitKeyBuilder.tenant(t).build())
                    .collect(Collectors.toList());
        }
        return Collections.singletonList(dataInitKeyBuilder.build());
    }

    /**
     * Uses {@link #loadEntitiesFromDataJsonSection(Tenant, Class, String)} to load all entities from the default
     * section.
     * <p/>
     * Never returns <code>null</code>, returns an empty list instead.
     *
     * @param <T>
     * @param tenant
     * @param entityType
     *
     * @return the list of entities found in the default section or an empty list
     *
     * @throws DataInitializationException
     */
    protected <T> List<T> loadEntitiesFromDataJsonDefaultSection(Tenant tenant, Class<T> entityType)
            throws DataInitializationException {
        return loadEntitiesFromDataJsonSection(tenant, entityType, DATA_JSON_SECTION_DEFAULT);
    }

    /**
     * Uses {@link #loadEntitiesFromDataJsonSectionTenantSpecific(Tenant, Class, String)} to load all
     * entities from the default section.
     * <p/>
     * Never returns <code>null</code>, returns an empty list instead.
     *
     * @param <T>
     * @param tenant
     * @param entityType
     * @return the list of entities found in the default section or an empty
     *         list
     * @throws DataInitializationException
     */
    protected <T> List<T> loadEntitiesFromDataJsonDefaultSectionTenantSpecific(Tenant tenant, Class<T> entityType)
            throws DataInitializationException {
        return loadEntitiesFromDataJsonSectionTenantSpecific(tenant, entityType, DATA_JSON_SECTION_DEFAULT);
    }

    /**
     * Uses {@link #loadEntitiesFromDataJson(Tenant, Class)} to load all entities from the section with the given
     * name.
     * <p/>
     * Never returns <code>null</code>, returns an empty list instead.
     *
     * @param tenant
     * @param entityType
     * @param sectionName
     * @param <T>
     *
     * @return the list of entities found in the section with the given name or an empty list
     *
     * @throws DataInitializationException
     */
    protected <T> List<T> loadEntitiesFromDataJsonSection(Tenant tenant, Class<T> entityType,
            String sectionName) throws DataInitializationException {
        Map<String, List<T>> entityMap = loadEntitiesFromDataJson(tenant, entityType);
        if (entityMap != null) {
            return entityMap.getOrDefault(sectionName, Collections.emptyList());
        }
        return Collections.emptyList();
    }

    /**
     * Uses {@link #loadEntitiesFromDataJsonTenantSpecific(Tenant, Class)} to load all entities from the section with the given
     * name.
     * <p/>
     * Never returns <code>null</code>, returns an empty list instead.
     *
     * @param tenant
     * @param entityType
     * @param sectionName
     * @param <T>
     *
     * @return the list of entities found in the section with the given name or an empty list
     *
     * @throws DataInitializationException
     */
    private <T> List<T> loadEntitiesFromDataJsonSectionTenantSpecific(Tenant tenant, Class<T> entityType,
            String sectionName) throws DataInitializationException {
        Map<String, List<T>> entityMap = loadEntitiesFromDataJsonTenantSpecific(tenant, entityType);
        if (entityMap != null) {
            return entityMap.getOrDefault(sectionName, Collections.emptyList());
        }
        return Collections.emptyList();
    }

    /**
     * Loads the entities specified in an init data json that is stored at one of the following locations, trying them
     * out in the given order:
     * </p>
     *
     * <pre>
     * init-{module}/{environment}/{topic}_{entityType}_{tenantId}.json
     * init-{module}/{environment}/{topic}_{entityType}__{tenantTag}.json
     * init-{module}/{topic}_{entityType}_{tenantId}.json
     * init-{module}/{topic}_{entityType}__{tenantTag}.json
     * init-{module}/{environment}/{topic}_{entityType}.json
     * init-{module}/{topic}_{entityType}.json
     * </pre>
     *
     * @param tenant
     *         The tenant the configuration should be specific for. Can be null if the data is common for all tenants
     * @param entityType
     *         The type of entities that are expected in the configuration
     *
     * @return map of entities or {@code null} if no data json could be found.
     *
     * @throws DataInitializationException
     *         if the entities found in the data json are different than the expected entity type
     */
    protected <T> Map<String, List<T>> loadEntitiesFromDataJson(Tenant tenant, Class<T> entityType)
            throws DataInitializationException {

        return loadEntitiesFromDataJson(tenant, false, entityType.getSimpleName(), entityType);
    }

    /**
     * Loads the entities specified in an init data json that is stored at one of the following locations, trying them
     * out in the given order:
     * </p>
     *
     * <pre>
     * init-{module}/{environment}/{topic}_{entityType}_{tenantId}.json
     * init-{module}/{environment}/{topic}_{entityType}__{tenantTag}.json
     * init-{module}/{topic}_{entityType}_{tenantId}.json
     * init-{module}/{topic}_{entityType}__{tenantTag}.json
     * </pre>
     *
     * @param tenant
     *         The tenant the configuration should be specific for. Can be null if the data is common for all tenants
     * @param entityType
     *         The type of entities that are expected in the configuration
     *
     * @return map of entities or {@code null} if no data json could be found.
     *
     * @throws DataInitializationException
     *         if the entities found in the data json are different than the expected entity type
     */
    protected <T> Map<String, List<T>> loadEntitiesFromDataJsonTenantSpecific(Tenant tenant, Class<T> entityType)
            throws DataInitializationException {

        return loadEntitiesFromDataJson(tenant, true, entityType.getSimpleName(), entityType);
    }

    /**
     * Loads the entities specified in an init data json that is stored at one of the following locations, trying them
     * out in the given order:
     * </p>
     *
     * <pre>
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}_{tenantId}.json
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * init-{module}/{topic}_{fileNameWithoutExtension}_{tenantId}.json
     * init-{module}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * if (tenantSpecific == false) {
     *       init-{module}/{environment}/{topic}_{fileNameWithoutExtension}.json
     *       init-{module}/{topic}_{fileNameWithoutExtension}.json
     * }
     * </pre>
     *
     * @param tenant
     *         The tenant the configuration should be specific for. Can be null if the data is common for all tenants
     * @param tenantSpecific
     *         if true, only tenant specific configuration is returned
     * @param fileNameWithoutExtension
     *         The filename of the configuration file to load
     * @param entityType
     *         The type of entities that are expected in the configuration
     * @param <T>
     *         type of entities to load
     *
     * @return map of entities or {@code null} if no data json could be found.
     *
     * @throws DataInitializationException
     *         if the entities found in the data json are different than the expected entity type
     */
    @SuppressFBWarnings(value = {"RCN_REDUNDANT_NULLCHECK_OF_NULL_VALUE", "NP_LOAD_OF_KNOWN_NULL_VALUE"}, justification = "unexplainable false positive")
    private <T> Map<String, List<T>> loadEntitiesFromDataJson(@Nullable Tenant tenant, boolean tenantSpecific,
            String fileNameWithoutExtension, Class<T> entityType) throws DataInitializationException {

        try (InputStream inputStream = tenantSpecific ?
                findTenantSpecificConfigurationFile(environmentService.getActiveProfiles(true), tenant,
                        fileNameWithoutExtension, "json") :
                findConfigurationFile(environmentService.getActiveProfiles(true), tenant,
                        fileNameWithoutExtension, "json")) {
            if (inputStream == null) {
                return null;
            } else {
                CollectionType entityListType = jsonTypeFactory().constructCollectionType(List.class, entityType);
                JavaType keyType = jsonTypeFactory().constructType(String.class);
                MapType mapType = jsonTypeFactory().constructMapType(Map.class, keyType, entityListType);

                return PRECONFIGURED_READER
                        .forType(mapType)
                        .readValue(inputStream);
            }
        } catch (IOException e) {
            throw new DataInitializationException("Could not open data json: " + e.getMessage(), e);
        }
    }

    /**
     * Loads the content of a file located in the init data that is stored at one of the following locations, trying
     * them out in the given order:
     * </p>
     *
     * <pre>
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * init-{module}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * </pre>
     *
     * @param fileName The name of the file to be loaded
     *
     * @return content of the file or {@code null} if no file could be found in the init data.
     *
     * @throws DataInitializationException if the file was found but could not be opened
     */
    protected byte[] loadConfigurationFileContent(String fileName)
            throws DataInitializationException {
        return loadConfigurationFileContent(null, fileName);
    }

    /**
     * Loads the content of a file located in the init data that is stored at one of the following locations, trying them
     * out in the given order:
     * </p>
     *
     * <pre>
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}_{tenantId}.{fileExtension}
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * init-{module}/{topic}_{fileNameWithoutExtension}_{tenantId}.{fileExtension}
     * init-{module}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * init-{module}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * </pre>
     *
     * @param tenant
     *         The tenant the configuration should be specific for. Can be null if the data is common for all
     *         tenants
     * @param fileName
     *         The name of the file to be loaded
     *
     * @return content of the file or {@code null} if no file could be found in the init data.
     *
     * @throws DataInitializationException
     *         if the file was found but could not be opened
     */
    @SuppressFBWarnings(value = {"RCN_REDUNDANT_NULLCHECK_OF_NULL_VALUE", "NP_LOAD_OF_KNOWN_NULL_VALUE"}, justification = "unexplainable false positive")
    protected byte[] loadConfigurationFileContent(@SuppressWarnings("SameParameterValue") @Nullable Tenant tenant,
            String fileName) throws DataInitializationException {

        String fileExtension = FilenameUtils.getExtension(fileName);
        String fileNameWithoutExtension = FilenameUtils.removeExtension(fileName);

        try (InputStream inputStream = findConfigurationFile(environmentService.getActiveProfiles(true), tenant,
                fileNameWithoutExtension, fileExtension)) {
            if (inputStream == null) {
                return null;
            } else {
                return IOUtils.toByteArray(inputStream);
            }
        } catch (IOException e) {
            throw new DataInitializationException("Could not open configuration file: " + e.getMessage(), e);
        }
    }

    private @Nullable InputStream findConfigurationFile(Set<String> activeProfiles, @Nullable Tenant tenant,
            String fileNameWithoutExtension, String fileExtension) {

        //try to find the most specific file: tenant
        if (tenant != null) {
            InputStream tenantConfig =
                    findTenantSpecificConfigurationFile(activeProfiles, tenant, fileNameWithoutExtension,
                            fileExtension);
            if (tenantConfig != null) {
                return tenantConfig;
            }
        }
        //try to find the data json file: activeProfile
        for (String activeProfile : activeProfiles) {
            InputStream activeProfileConfig = dataInitializerFileService.loadInitDataFile(
                    getSpecificFileName(activeProfile, null, false, fileNameWithoutExtension, fileExtension));
            if (activeProfileConfig != null) {
                return activeProfileConfig;
            }
        }
        //try to find the general data json file
        return dataInitializerFileService.loadInitDataFile(
                getSpecificFileName(null, null, false, fileNameWithoutExtension, fileExtension));
    }

    private InputStream findTenantSpecificConfigurationFile(Set<String> activeProfiles, Tenant tenant,
            String fileNameWithoutExtension, String fileExtension) {

        if (tenant == null) {
            return null;
        }
        InputStream inputStream;
        //try to find the data json file specific for: tenant & activeProfile
        for (String activeProfile : activeProfiles) {
            inputStream = dataInitializerFileService.loadInitDataFile(
                    getSpecificFileName(activeProfile, tenant, false, fileNameWithoutExtension, fileExtension));
            if (inputStream != null) {
                return inputStream;
            }
        }
        //try to find the data json file specific for: tenant tag & activeProfile
        for (String activeProfile : activeProfiles) {
            inputStream = dataInitializerFileService.loadInitDataFile(
                    getSpecificFileName(activeProfile, tenant, true, fileNameWithoutExtension, fileExtension));
            if (inputStream != null) {
                return inputStream;
            }
        }
        //try to find the data json file specific for: tenant
        inputStream = dataInitializerFileService.loadInitDataFile(
                getSpecificFileName(null, tenant, false, fileNameWithoutExtension, fileExtension));
        if (inputStream != null) {
            return inputStream;
        }
        //try to find the data json file specific for: tenant tag
        inputStream = dataInitializerFileService.loadInitDataFile(
                getSpecificFileName(null, tenant, true, fileNameWithoutExtension, fileExtension));
        return inputStream;
    }

    private String determineModuleName() {
        return reflectionService.getModuleName(this.getClass());
    }

    /**
     * Get the specific file name to locate a file for profile and tenant
     * <p>
     * Tries to create these names, depending on the parameters
     * <pre>
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}_{tenantId}.{fileExtension}
     * (useTenantTag == true)   init-{module}/{environment}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * init-{module}/{topic}_{fileNameWithoutExtension}_{tenantId}.{fileExtension}
     * (useTenantTag == true)   init-{module}/{topic}_{fileNameWithoutExtension}__{tenantTag}.json
     * init-{module}/{environment}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * init-{module}/{topic}_{fileNameWithoutExtension}.{fileExtension}
     * </pre>
     */
    private String getSpecificFileName(String activeProfile, Tenant tenant, boolean useTenantTag,
            String fileNameWithoutExtension, String fileExtension) {

        StringBuilder fileName = new StringBuilder();
        fileName.append("init-");
        fileName.append(getModuleName());
        fileName.append("/");
        fileName.append(getTopic());
        fileName.append("/");
        if (!StringUtils.isEmpty(activeProfile)) {
            fileName.append(activeProfile);
            fileName.append("/");
        }
        fileName.append(fileNameWithoutExtension);
        if (Objects.nonNull(tenant)) {
            if (useTenantTag && StringUtils.isNotEmpty(tenant.getTag())) {
                fileName.append("__");
                fileName.append(tenant.getTag());
            } else {
                fileName.append("_");
                fileName.append(tenant.getId());
            }
        }
        fileName.append(".");
        fileName.append(fileExtension);
        return fileName.toString();
    }

    protected long timestampYYYYMMDDtoMillisUTC(long yyyymmdd) {
        String timestampString = Long.toString(yyyymmdd);

        try {
            return Instant.from(DATE_TIME_FORMATTER_YYYYMMDD.parse(timestampString)).toEpochMilli();
        } catch (DateTimeParseException e) {
            throw new DataInitializationException("Timestamp has the wrong format, expected YYYYMMDD: " + e.getMessage());
        }
    }

    protected long timestampYYYYMMDDhhmmtoMillisUTC(long yyyymmddhhmm) {
        String timestampString = Long.toString(yyyymmddhhmm);

        try {
            return Instant.from(DATE_TIME_FORMATTER_YYYYMMDDHHMM.parse(timestampString)).toEpochMilli();
        } catch (DateTimeParseException e) {
            throw new DataInitializationException("Timestamp has the wrong format, expected YYYYMMDDhhmm: " + e.getMessage());
        }
    }

    /**
     * <b>NOT intended for production!</b> Convenience method for writing
     * existing entities in an initial data json.
     *
     * <p>
     * Copy the following annotations to {@link BaseEntity} to enable better readable json files!
     */
    //@JsonPropertyOrder(value = { "id", "created", "name" ,"firstName" , "lastName" , "nickName", "email", "phone", "smsNotificationNumber", "webShopURL" , "street", "zip", "city" }, alphabetic=true)
    //@JsonFilter("dataInit")
    @Deprecated
    protected <T extends BaseEntity> void writeEntitiesToJson(String resourceFolderLocation,
            String environment, Tenant tenant, Class<T> entity, Map<String, List<T>> entities)
    throws IOException {

        //it is ok to construct this mapper for every method call, since this method is only used manually in certain situations
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(Include.NON_NULL); //because of this, we cannot use JsonMapping.defaultJsonWriter

        final ObjectWriter prettyPrintJsonWriter = objectMapper
                .writer()
                .with(SerializationFeature.INDENT_OUTPUT)
                .with(new SimpleFilterProvider().addFilter("dataInit", SimpleBeanPropertyFilter.serializeAllExcept(
                        "created", "externalLocated", "lastLoggedIn", "tenants", "oauthId", "accountType")))
                .with(new DefaultPrettyPrinter().withArrayIndenter(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE));

        String initJson = prettyPrintJsonWriter.writeValueAsString(entities);

        FileUtils.write(new File(resourceFolderLocation +
                        getSpecificFileName(environment, tenant, false, entity.getSimpleName(), "json")),
                initJson, "UTF-8");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "(" + getTopic() + ")";
    }

}
