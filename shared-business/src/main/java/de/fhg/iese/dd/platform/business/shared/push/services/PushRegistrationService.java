/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;

@Service
class PushRegistrationService extends BaseService implements IPushRegistrationService {

    @Autowired
    private IExternalPushProvider externalPushProvider;
    @Autowired
    private IAppService appService;

    @Override
    public void registerEndpoint(String registrationToken, PushPlatformType platformType, AppVariant appVariant,
            Person person) {

        externalPushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);
    }

    @Override
    public void unregisterEndpoint(String registrationToken, PushPlatformType platformType,
            AppVariant appVariant, Person person) {

        externalPushProvider.deletePushEndpoint(registrationToken, platformType, appVariant, person);
    }

    @Override
    public void deleteAllEndpoints(Person person) {

        for (AppVariantUsage appVariantUsage : appService.getAppVariantUsages(person)) {
            externalPushProvider.deletePushEndpoints(person, appVariantUsage.getAppVariant());
        }
    }

    @Override
    public Collection<IExternalPushProvider.ExternalPushEndpointDescription> getPushEndpoints(Person person) {

        return appService.getAppVariantUsages(person)
                .stream()
                .flatMap(avu -> getPushEndpoints(person, avu.getAppVariant()).stream())
                .collect(Collectors.toList());
    }

    @Override
    public Collection<IExternalPushProvider.ExternalPushEndpointDescription> getPushEndpoints(Person person,
            AppVariant appVariant) {

        return externalPushProvider.getPushEndpoints(person, appVariant);
    }

    @Override
    public void logHumanReadablePushStatisticsForAppVariant(AppVariant appVariant, LogSummary logSummary) {
        logSummary.info(externalPushProvider.getPushAppInfoForAppVariant(appVariant));
    }

    @Override
    public long deletePushForAppVariant(AppVariant appVariant, LogSummary logSummary) {
        long deletedPushApps = externalPushProvider.deleteAppVariant(appVariant);
        logSummary.info("Deleted {} push apps for {}", deletedPushApps, appVariant);
        return deletedPushApps;
    }

}
