/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init.services;

import de.fhg.iese.dd.platform.business.framework.caching.services.ICacheService;
import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.business.framework.datadependency.services.IDataDependencyAwareOrderService;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer.DataInitializerResult;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.init.IDataInitializerFileService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
class DataInitializerService extends BaseService implements IDataInitializerService {

    private static final String TEAM_NOTIFICATION_TOPIC = "data-init";
    private static final Marker LOG_MARKER_DATA_INIT = MarkerManager.getMarker("DATA_INIT");

    @Autowired
    private List<IDataInitializer> dataInitializers;
    @Autowired
    private List<IManualTestDataInitializer> manualTestDataInitializers;
    @Autowired
    private IDataDependencyAwareOrderService dataEntityDependencyAwareOrderService;
    @Autowired
    private IDataInitializerFileService dataInitializerFileService;
    @Autowired
    private ITeamNotificationService teamNotificationService;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;
    @Autowired
    private ICacheService cacheService;

    @Override
    public List<String> getAllDataInitializerIds() {
        return dataInitializers.stream()
                .map(IDataInitializer::getId)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllDataInitializerTopics() {
        return dataInitializers.stream()
                .map(IDataInitializer::getTopic)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllDataInitializerUseCases() {
        return dataInitializers.stream()
                .flatMap(dataInitializer -> CollectionUtils.isEmpty(dataInitializer.getUseCases())
                        ? Stream.empty()
                        : dataInitializer.getUseCases().stream())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllManualTestScenarioIds() {
        return manualTestDataInitializers.stream()
                .map(IManualTestDataInitializer::getScenarioId)
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public DataInitializerResult createInitialDataForUseCase(@NonNull String useCase, @Nullable Person caller) {

        Set<String> topicsForUseCases = dataInitializers.stream()
                .filter(dataInitializer ->
                        !CollectionUtils.isEmpty(dataInitializer.getUseCases()) &&
                                dataInitializer.getUseCases().contains(useCase))
                .map(IDataInitializer::getTopic)
                .collect(Collectors.toSet());

        return createInitialData("use case '" + useCase + "' = ", topicsForUseCases, caller);
    }

    @Override
    public DataInitializerResult createInitialData(String commandPrefix, @NonNull final Set<String> topics,
            @Nullable Person caller) {

        final ReferenceDataChangeLogEntry referenceDataChangeLock =
                referenceDataChangeService.acquireReferenceDataChangeLock(caller,
                        "init, " + commandPrefix + "topics " + topics, true);
        try {
            DataInitializerResult dataInitializerResult =
                    internalCreateInitialData(commandPrefix, topics, caller, referenceDataChangeLock);
            referenceDataChangeService
                    .releaseReferenceDataChangeLock(referenceDataChangeLock, dataInitializerResult.wasSuccessful());
            return dataInitializerResult;
        } catch (Exception e) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLock, false);
            throw e;
        }
    }

    @NonNull
    private DataInitializerResult internalCreateInitialData(String commandPrefix, Set<String> topics, Person caller,
            ReferenceDataChangeLogEntry referenceDataChangeLock) {
        LogSummary logSummary = new LogSummary(log, LOG_MARKER_DATA_INIT);
        final String versionInfo = dataInitializerFileService.checkoutInitData(logSummary);
        referenceDataChangeService.setDataChangeVersionInfo(referenceDataChangeLock, versionInfo);
        DataInitializerResult dataInitializerResult = callDataInitializers(logSummary,
                caller,
                "create initial data " + commandPrefix + "topics " + topics,
                versionInfo,
                topicMatcher(topics),
                IDataInitializer::createInitialData,
                true);
        dataInitializerFileService.cleanup(logSummary);
        return dataInitializerResult;
    }

    @Override
    public DataInitializerResult dropData(@NonNull Set<String> topics, @Nullable Person caller) {

        final ReferenceDataChangeLogEntry referenceDataChangeLock =
                referenceDataChangeService.acquireReferenceDataChangeLock(caller, "drop, topics " + topics, true);
        try {
            final DataInitializerResult dataInitializerResult = internalDropData(topics, caller);
            referenceDataChangeService
                    .releaseReferenceDataChangeLock(referenceDataChangeLock, dataInitializerResult.wasSuccessful());
            return dataInitializerResult;
        } catch (Exception e) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLock, false);
            throw e;
        }
    }

    @NonNull
    private DataInitializerResult internalDropData(Set<String> topics, Person caller) {
        LogSummary logSummary = new LogSummary(log, LOG_MARKER_DATA_INIT);
        return callDataInitializers(logSummary,
                caller,
                topics.stream()
                        .collect(Collectors.joining(", ", "drop data [", "]")),
                "no version",
                topicMatcher(topics),
                IDataInitializer::dropData,
                false);
    }

    private static Predicate<IDataInitializer> topicMatcher(Set<String> topics) {
        return dataInitializer ->
                //select all only works for non manual test data initializers
                (topics.contains(SELECT_ALL) &&
                        !(dataInitializer instanceof IManualTestDataInitializer))
                        //if the data initializer topic is explicitly mentioned, we can call it
                        || topics.contains(dataInitializer.getTopic());
    }

    private DataInitializerResult callDataInitializers(
            LogSummary logSummary,
            Person caller,
            String message,
            String versionMessage,
            Predicate<IDataInitializer> matcher,
            BiFunction<IDataInitializer, LogSummary, DataInitializerResult> method,
            boolean requiredBeforeProcessed) {

        List<Throwable> thrownExceptions = new LinkedList<>();
        long start = System.currentTimeMillis();

        logSummary.info("global: {} start", message);
        List<IDataInitializer> dataInitializersOrdered;
        if (requiredBeforeProcessed) {
            dataInitializersOrdered = dataEntityDependencyAwareOrderService
                    .orderRequiredBeforeProcessed(dataInitializers);
        } else {
            dataInitializersOrdered = dataEntityDependencyAwareOrderService
                    .orderProcessedBeforeRequired(dataInitializers);
        }
        log.info("Executing data init in this order: {}", dataInitializersOrdered.stream()
                .map(IDataDependencyAware::getId)
                .collect(Collectors.joining(", ")));
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_TECHNICAL_SYSTEM,
                "⌛ Starting `{}` by {} `{}`\n```{}```",
                message,
                caller != null ? caller.getFullName() : "anonymous",
                caller != null ? caller.getId() : "-",
                versionMessage);
        for (IDataInitializer dataInitializer : dataInitializersOrdered) {
            String dataInitializerId = dataInitializer.getId();
            if (matcher.test(dataInitializer)) {
                logSummary.info("{}: {} start", dataInitializerId, message);
                logSummary.indent();
                //set the logger of this class to avoid that some data initializer forgets setting the own log and write to the previous logger
                logSummary.setLogger(log);
                try {
                    DataInitializerResult resultDataInitializer = method.apply(dataInitializer, logSummary);
                    thrownExceptions.addAll(resultDataInitializer.exceptions());
                    // we also want to catch errors here, so that we are able to properly finalize the data init
                } catch (Throwable e) {
                    logSummary.setLogger(log);
                    thrownExceptions.add(e);
                    logSummary.error("Failed {} with {} : {}", e, message, dataInitializerId, e.getMessage());
                }
                logSummary.setLogger(log);
                logSummary.resetIndentation();
                logSummary.info("{}: {} finished", dataInitializerId, message);
                //to ensure that the next data initializers are able to use caching services the caches of processed entities get invalidated
                int invalidatedCaches = cacheService
                        .invalidateCachesForEntities(dataInitializer.getProcessedEntities());
                if (invalidatedCaches > 0) {
                    logSummary.info("Invalidated {} cache{}", invalidatedCaches,
                            invalidatedCaches > 1
                                    ? "s"
                                    : "");
                }
            }
        }

        logSummary.info("global: {} finished", message);
        if (!thrownExceptions.isEmpty()) {
            log.error("There were exceptions!");
            thrownExceptions.forEach(e -> log.error("Exception while running data init: " + e.getMessage(), e));
        }
        DataInitializerResult dataInitResult = new DataInitializerResult(logSummary.toString(), thrownExceptions);
        boolean success = dataInitResult.wasSuccessful() && logSummary.getErrorCount() == 0;
        String warningsAndErrors = extractWarningsAndErrors(logSummary);
        teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_TECHNICAL_SYSTEM,
                "{} Called `{}` by {} `{}` in {} seconds with {} errors and {} warnings {}",
                success ? "🆗" : "❌",
                message,
                caller != null ? caller.getFullName() : "anonymous",
                caller != null ? caller.getId() : "-",
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start),
                logSummary.getErrorCount(),
                logSummary.getWarningsCount(),
                warningsAndErrors);
        return dataInitResult;
    }

    @NonNull
    private static String extractWarningsAndErrors(LogSummary logSummary) {

        StringBuilder warningsAndErrors = new StringBuilder();
        if (logSummary.getErrorCount() > 0 || logSummary.getWarningsCount() > 0) {
            warningsAndErrors.append("\n```");
            if (logSummary.getErrorCount() > 0) {
                warningsAndErrors.append("========== ERRORS ==========\n");
                warningsAndErrors.append(logSummary.getErrorLog());
            }
            if (logSummary.getWarningsCount() > 0) {
                warningsAndErrors.append("========== WARNINGS ==========\n");
                warningsAndErrors.append(logSummary.getWarningLog());
            }
            warningsAndErrors.append("```");
        }
        return warningsAndErrors.toString();
    }

    @Override
    public DataInitializerResult startManualTestScenario(@NonNull String scenarioId, boolean deleteOtherDataInDomain) {

        StringBuilder completeLogMessage = new StringBuilder();
        List<Throwable> exceptions = new ArrayList<>();

        LogSummary logSummary = new LogSummary(log, LOG_MARKER_DATA_INIT);
        logSummary.info("manual test scenario: {}, delete other data {} start", scenarioId, deleteOtherDataInDomain);

        //find all matching test data initializers with that scenario id
        List<IManualTestDataInitializer> matchingManualTestDataInitializers = manualTestDataInitializers.stream()
                .filter(mtdi -> StringUtils.equals(scenarioId, mtdi.getScenarioId()))
                .collect(Collectors.toList());

        //find all dependent topics by getting them from the matching initializers
        Set<String> allTopics = matchingManualTestDataInitializers.stream()
                .map(IManualTestDataInitializer::getDependentTopics)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        //add the topics of the matching manual test data initializers themselves, so that they are called, too
        allTopics.addAll(matchingManualTestDataInitializers.stream()
                .map(IDataInitializer::getTopic)
                .collect(Collectors.toList()));

        completeLogMessage.append(logSummary);
        final ReferenceDataChangeLogEntry referenceDataChangeLock =
                referenceDataChangeService.acquireReferenceDataChangeLock(null, "manual-test, scenario " + scenarioId,
                        true);
        try {
            if (deleteOtherDataInDomain) {
                DataInitializerResult dropResult = internalDropData(allTopics, null);
                completeLogMessage.append(dropResult.message()).append("\n");
                exceptions.addAll(dropResult.exceptions());
            }
            DataInitializerResult createResult =
                    internalCreateInitialData("", allTopics, null, referenceDataChangeLock);
            completeLogMessage.append(createResult.message());
            exceptions.addAll(createResult.exceptions());
            referenceDataChangeService
                    .releaseReferenceDataChangeLock(referenceDataChangeLock, createResult.wasSuccessful());
        } catch (Exception e) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLock, false);
            throw e;
        }

        return new DataInitializerResult(completeLogMessage.toString(), exceptions);
    }

}
