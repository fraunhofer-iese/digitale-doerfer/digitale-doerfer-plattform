/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;

public class UserGeneratedContentFlagEntityCanNotBeDeletedException extends BadRequestException {

    private static final long serialVersionUID = -79526243355111204L;

    private UserGeneratedContentFlagEntityCanNotBeDeletedException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static UserGeneratedContentFlagEntityCanNotBeDeletedException forNotDeletableEntity(
            UserGeneratedContentFlag flag) {
        return new UserGeneratedContentFlagEntityCanNotBeDeletedException(
                "The entity of flag {} is not deletable", BaseEntity.getIdOf(flag));
    }

    public static UserGeneratedContentFlagEntityCanNotBeDeletedException forNoHandlerFound(
            UserGeneratedContentFlag flag) {
        return new UserGeneratedContentFlagEntityCanNotBeDeletedException(
                "No user generated content flag handler for flag {}", BaseEntity.getIdOf(flag));
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_CAN_NOT_BE_DELETED;
    }

}
