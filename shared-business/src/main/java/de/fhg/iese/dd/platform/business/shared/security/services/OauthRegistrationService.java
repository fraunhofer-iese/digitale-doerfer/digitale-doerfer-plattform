/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import com.google.common.collect.Lists;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
class OauthRegistrationService extends BaseEntityService<OauthRegistration> implements IOauthRegistrationService {

    private static final Pattern EMAIL_MASK = Pattern.compile("(\\S{1,4})(\\S+)@(\\S{1,4})(\\S+)\\.(\\S+)");
    private static final String TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY = "inactivity";
    private static final int BATCH_SIZE = 30;

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Autowired
    private OauthConfig oauthConfig;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;

    @Autowired
    private IOauthManagementService oauthManagementService;

    @Override
    public boolean registrationWithOauthIdExists(String oauthId) {
        return oauthRegistrationRepository.existsByOauthId(oauthId);
    }

    @Override
    public void checkAndDeleteUnusedRegistrations() {
        long thresholdCreatedMillis =
                timeService.currentTimeMillisUTC() - oauthConfig.getDurationUntilDeleteUnused().toMillis();

        //these are only those registrations where no person exists with that oauthid
        List<OauthRegistration> oldUnusedRegistrations =
                oauthRegistrationRepository.findAllUnusedAndNoError(thresholdCreatedMillis);

        //split it up into batches, so that we have not such a long running task
        List<List<OauthRegistration>> oldRegistrationsPartitioned = Lists.partition(oldUnusedRegistrations, BATCH_SIZE);

        int numBatches = oldRegistrationsPartitioned.size();
        int total = oldUnusedRegistrations.size();
        for (int currentBatch = 0; currentBatch < numBatches; currentBatch++) {
            if (currentBatch > 0) {
                //this is not the first batch, so we need to pause a bit
                try {
                    //we can use this simple way of sleeping here, since this is called from a worker, so it does not impact performance
                    Thread.sleep(TimeUnit.SECONDS.toMillis(15));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            List<OauthRegistration> oldRegistrationsBatch = oldRegistrationsPartitioned.get(currentBatch);
            List<OauthRegistration> deletedRegistrationsBatch = new ArrayList<>(oldRegistrationsBatch.size());

            for (OauthRegistration oldRegistration : oldRegistrationsBatch) {
                //if no person with this oauth id exists
                if (!personRepository.existsByOauthId(oldRegistration.getOauthId())) {
                    //delete the account and the registration entity we keep
                    deletedRegistrationsBatch.add(deleteOauthUser(oldRegistration));
                }
            }
            notifyTeam(deletedRegistrationsBatch, total, currentBatch + 1, numBatches);
        }
    }

    private void notifyTeam(List<OauthRegistration> deletedRegistrationsBatch, int total, int currentBatch,
            int numBatches) {
        teamNotificationService.sendTeamNotification(
                TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Deleted {} / {} (batch {} / {} with size {}) unused OAuth accounts after {} hours of creation without completing registration.\n```{}```",
                deletedRegistrationsBatch.size(),
                total,
                currentBatch,
                numBatches,
                BATCH_SIZE,
                oauthConfig.getDurationUntilDeleteUnused().toHours(),
                deletedRegistrationsBatch.stream()
                        .map(reg -> String.format("%50s %30s created %s %s",
                                reg.getOauthId(),
                                maskEmail(reg.getEmail()),
                                timeService.toLocalTimeHumanReadable(reg.getCreated()),
                                Objects.toString(reg.getErrorMessage(), "")))
                        .collect(Collectors.joining("\n")));
    }

    private OauthRegistration deleteOauthUser(OauthRegistration oauthRegistration) {
        try {
            oauthManagementService.deleteUser(oauthRegistration.getOauthId());
            oauthRegistrationRepository.delete(oauthRegistration);
        } catch (OauthManagementException e) {
            String errorMessage = "Error while deleting Oauth user: " + e;
            log.error(errorMessage, e);
            oauthRegistration.setErrorMessage(errorMessage);
            return store(oauthRegistration);
        }
        return oauthRegistration;
    }

    private String maskEmail(String email) {
        if (StringUtils.isNotEmpty(email)) {
            //emails look like balt***@iese***.de
            return EMAIL_MASK.matcher(email).replaceAll("$1***@$3***.$5");
        } else {
            return "-";
        }
    }

}
