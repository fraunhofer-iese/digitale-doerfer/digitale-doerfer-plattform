/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.deletion;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public abstract class ReferenceDataChangeRequest<T extends BaseEntity> {

    /**
     * If true all entities referencing the {@link #entityToBeDeleted} are deleted, too. This does not work for all
     * entities.
     * <p>
     * If false all entities referencing the {@link #entityToBeDeleted} are referencing {@link #entityToBeUsedInstead}
     * afterwards.
     */
    private final boolean deleteDependentEntities;
    private final T entityToBeUsedInstead;
    private final T entityToBeDeleted;

    @SuppressWarnings("unchecked")
    public Class<T> getEntityToBeDeletedClass() {
        return (Class<T>) entityToBeDeleted.getClass();
    }

    protected String fieldsToString(boolean linebreak) {
        if (linebreak) {
            return "\n" +
                    "deleteDependentEntities='" + isDeleteDependentEntities() + "'\n" +
                    "entityToBeDeleted='" + getEntityToBeDeleted() + "'\n" +
                    (!isDeleteDependentEntities()
                            ? ("entityToBeUsedInstead='" + getEntityToBeUsedInstead() + "'")
                            : "");
        } else {
            return "[" +
                    "deleteDependentEntities='" + isDeleteDependentEntities() + "', " +
                    "entityToBeDeleted='" + getEntityToBeDeleted() + "'" +
                    (!isDeleteDependentEntities()
                            ? (", entityToBeUsedInstead='" + getEntityToBeUsedInstead() + "']")
                            : "]");
        }
    }

    public abstract String toStringLineBreak();

}
