/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.time.Duration;
import java.util.function.BiFunction;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflow;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

public interface ICommonDataPrivacyService extends IService {

    String PRIVACY_REPORT_FOLDER = "privacy-report/";

    /**
     * Initiates a data privacy data collection to collect all personal data of the given person and send a report based
     * on this data. This workflow includes:
     * <ul>
     *     <li>Calling all external systems {@link AppVariant#hasExternalSystem()} that the person used</li>
     *     <li>Waiting for all responses from those systems</li>
     *     <li>Collecting all data inside the platform</li>
     *     <li>Creating a report based on the responses and the internal data</li>
     *     <li>Depending on the trigger: Sending this report to the user</li>
     * </ul>
     *
     * @param trigger initiator of the workflow, controls if the report should be send to the user
     * @param person  the owner of the data
     *
     * @return the newly started data collection
     */
    DataPrivacyDataCollection startDataPrivacyDataCollection(Person person, DataPrivacyWorkflowTrigger trigger);

    /**
     * Initiates a data privacy data deletion to delete all personal data of the given person and create an internal
     * report based on this data. This workflow includes:
     * <ul>
     *     <li>Calling all external systems {@link AppVariant#hasExternalSystem()} that the person used</li>
     *     <li>Waiting for all responses from those systems</li>
     *     <li>Deleting all data inside the platform</li>
     *     <li>Creating a report based on the responses and the internal data deletion</li>
     *     <li>Logging the full report of the user</li>
     * </ul>
     *
     * @param trigger initiator of the deletion workflow
     * @param person  the owner of the data
     */
    void startDataPrivacyDataDeletion(Person person, DataPrivacyWorkflowTrigger trigger);

    /**
     * Processes this data privacy workflow:
     * <ul>
     *     <li>completes it if all responses are there or the timeout is reached</li>
     *     <li>calls the external systems again that could not be called at the first time</li>
     * </ul>
     *
     * @param dataPrivacyWorkflow data privacy workflow
     */
    void processDataPrivacyWorkflow(DataPrivacyWorkflow dataPrivacyWorkflow);

    /**
     * Processes all data privacy workflows that are not yet finished by calling {@link
     * #processDataPrivacyWorkflow(DataPrivacyWorkflow)} for each of them.
     * <p>
     * Called regularly by a worker.
     *
     * @return the number of processed data privacy workflows
     */
    int processAllUnfinishedDataPrivacyWorkflows();

    /**
     * Deletes all old data privacy reports and data privacy workflow entities that are older than {@link
     * DataPrivacyConfig#getMaxRetentionDataPrivacyReportsInDays()}.
     * <p>
     * Uses {@link IFileStorage#deleteOldFiles(String, Duration, BiFunction)} to delete the reports.
     */
    void deleteOldDataPrivacyWorkflows();

    DataPrivacyDataCollection findDataCollectionById(String dataCollectionId);

    DataPrivacyDataDeletion findDataDeletionById(String dataDeletionId);

    DataPrivacyWorkflowAppVariant findDataPrivacyWorkflowAppVariant(
            DataPrivacyDataCollection dataPrivacyDataCollection, AppVariant appVariant);

    DataPrivacyWorkflowAppVariant findDataPrivacyWorkflowAppVariant(
            DataPrivacyDataDeletion dataPrivacyDataDeletion, AppVariant appVariant);

}
