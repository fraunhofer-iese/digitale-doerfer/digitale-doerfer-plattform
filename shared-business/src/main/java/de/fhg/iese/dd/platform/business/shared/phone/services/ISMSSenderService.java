/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.phone.services;

import java.util.Locale;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.phone.exceptions.PhoneNumberInvalidException;
import de.fhg.iese.dd.platform.business.shared.phone.exceptions.SMSSenderException;
import lombok.Builder;
import lombok.Getter;

public interface ISMSSenderService extends IService {

    @Getter
    @Builder
    class PhoneNumberDetails {

        enum PhoneNumberType {
            MOBILE, LANDLINE, VOIP
        }

        private final String number;
        private final String localNumber;
        private final String countryCode;
        private final String carrierName;
        private final PhoneNumberType type;

    }

    /**
     * Sends a sms message to a recipient. The number has to be in the international E.164 format. Use {@link
     * #validatePhoneNumber(String)} or {@link #validateLocalPhoneNumber(String, String)} to convert and validate
     * numbers.
     * <p>
     * IMPORTANT
     * <p>
     * Be sure that the recipient confirmed that we are allowed to send a sms! If we send sms to wrong numbers we will
     * be in legal trouble :-(
     *
     * @param senderId        the alphanumeric sender id that will be used for sending the sms, up to 11 characters
     * @param recipientNumber the number of the recipient in E.164 format
     * @param text            the text to send, has to be less than 140 characters
     *
     * @throws SMSSenderException if senderId or recipientNumber are invalid or the text is too long or the sms gateway
     *                            failed
     * @see <a href="https://en.wikipedia.org/wiki/E.164">E.164</a>
     * @see <a href="https://support.twilio.com/hc/en-us/articles/223133767-International-support-for-Alphanumeric-Sender-ID">twilio
     *         docu on suppurted countries</a>
     * @see <a href="https://support.twilio.com/hc/en-us/articles/223181348-Getting-Started-with-Alphanumeric-Sender-ID-for-Twilio-Programmable-SMS">twilio
     *         docu on alphanumeric sender ids</a>
     */
    void sendSMS(String senderId, String recipientNumber, String text) throws SMSSenderException;

    /**
     * Validates a phone number that is in the E.164 format.
     *
     * @see #validateLocalPhoneNumber(String, String)
     */
    PhoneNumberDetails validatePhoneNumber(String number) throws PhoneNumberInvalidException;

    /**
     * Validates a phone number to ensure that the number exists and gets meta information about the number.
     * <p>
     * IMPORTANT
     * <p>
     * Every call costs a tiny bit of money, currently $0.005 per request.
     *
     * @param number      the number to validate in the format of the country or in E.164 format
     * @param countryCode the country code in ISO 3166-1 alpha-2 format to specify the formatting of the number. If null
     *                    the number has to be in the E.164 format.
     *
     * @return details of the phone number, if it exists
     *
     * @throws PhoneNumberInvalidException if the number does not exist
     * @see <a href="https://en.wikipedia.org/wiki/E.164">E.164</a>
     * @see <a href="https://www.twilio.com/docs/lookup/tutorials/validation-and-formatting">twilio docu on lookup
     *         service</a>
     * @see <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">country codes</a>
     * @see Locale#getCountry()
     * @see Locale#GERMANY
     */
    PhoneNumberDetails validateLocalPhoneNumber(String number, String countryCode) throws PhoneNumberInvalidException;

}
