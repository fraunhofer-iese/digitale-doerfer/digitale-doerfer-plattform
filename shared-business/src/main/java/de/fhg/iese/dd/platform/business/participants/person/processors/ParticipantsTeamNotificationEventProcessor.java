/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportFinalizedEvent;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteConfirmation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class ParticipantsTeamNotificationEventProcessor extends BaseEventProcessor {

    private static final int MAX_LENGTH_REPORT = 3000;

    private static final String TEAM_NOTIFICATION_TOPIC_PARTICIPANTS = "participants";
    private static final String TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY = "data-privacy";

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @EventProcessing
    private void handlePersonCreatedEvent(PersonCreateConfirmation event) {
        Person newPerson = event.getPerson();
        teamNotificationService.sendTeamNotification(
                newPerson.getTenant(),
                TEAM_NOTIFICATION_TOPIC_PARTICIPANTS,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "🆕 '{}' ({}) in {}",
                newPerson.getFullName(),
                newPerson.getEmail(),
                tenantAndGeoArea(newPerson));
    }

    @EventProcessing
    private void handlePersonDeleteEvent(PersonDeleteConfirmation event) {
        teamNotificationService.sendTeamNotification(
                event.getTenant(),
                TEAM_NOTIFICATION_TOPIC_PARTICIPANTS,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "User '{}' ({}) created at {} in `{}` last logged in {} with id {} has been deleted due to {}.\n```{}```",
                event.getFullName(),
                event.getEmail(),
                timeService.toLocalTimeHumanReadable(event.getPersonCreated()),
                event.getTenant().getName(),
                timeService.toLocalTimeHumanReadable(event.getPersonLastLoggedIn()),
                event.getPersonId(),
                event.getTrigger(),
                truncate(event.getPersonDeletionReport()));
    }

    @EventProcessing
    private void handleDataPrivacyReportRequest(DataPrivacyReportRequest event) {
        teamNotificationService.sendTeamNotification(
                event.getPerson().getTenant(),
                TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "User '{}' created at {} has requested a data privacy report",
                event.getPerson().getId(),
                timeService.toLocalTimeHumanReadable(event.getPerson().getCreated()));
    }

    @EventProcessing
    private void handleDataPrivacyReportResponse(DataPrivacyReportFinalizedEvent event) {
        if (event.getTrigger() == DataPrivacyWorkflowTrigger.ADMIN_TRIGGERED_REPORT_ONLY) {
            teamNotificationService.sendTeamNotification(
                    event.getDataCollection().getPerson().getTenant(),
                    TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                    "A data privacy report has been generated for user '{}'. The report can be found at '{}'",
                    event.getDataCollection().getPerson().getId(), event.getZipFileUrl());
        } else {
            teamNotificationService.sendTeamNotification(
                    event.getDataCollection().getPerson().getTenant(),
                    TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                    "A data privacy report has been generated for user '{}'",
                    event.getDataCollection().getPerson().getId());
        }
    }

    private String truncate(IPersonDeletionReport report) {
        if (report == null) {
            return "Error: report was null";
        }
        String reportText = report.toString();
        return reportText.length() > MAX_LENGTH_REPORT
                ? reportText.substring(0, MAX_LENGTH_REPORT) +
                "... (truncated " + (reportText.length() - MAX_LENGTH_REPORT) + " chars)"
                : reportText;
    }

    private String tenantAndGeoArea(Person person){
        return String.format("`%s` `[%s]`",
                person.getHomeArea() == null ? "unknown" : person.getHomeArea().getName(),
                person.getTenant().getName());
    }

}
