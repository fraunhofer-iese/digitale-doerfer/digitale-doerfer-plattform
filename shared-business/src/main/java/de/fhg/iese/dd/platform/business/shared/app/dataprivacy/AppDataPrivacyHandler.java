/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.dataprivacy;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ISharedDataDeletionService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;

@Component
public class AppDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(App.class, AppVariant.class, AppVariantUsage.class);

    @Autowired
    private IAppService appService;

    @Autowired
    private ISharedDataDeletionService sharedDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("App-Benutzung", "Benutzung von Apps");

        List<AppVariantUsage> appVariantUsages = appService.getAppVariantUsages(person);

        for (AppVariantUsage appVariantUsage : appVariantUsages) {

            final AppVariant appVariant = appVariantUsage.getAppVariant();

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(appVariant.getName(), "Benutzung der App " + appVariant.getName());
            subSection.newContent("Installationszeitpunkt der App", "Zeitpunkt der ersten Benutzung",
                    timeService.toLocalTimeHumanReadable(appVariantUsage.getCreated()));
            subSection.newContent("Letzte Benutzung der App", "Zeitpunkt der letzten Benutzung",
                    timeService.toLocalTimeHumanReadable(appVariantUsage.getLastUsage()));

            Set<GeoArea> selectedGeoAreas = appService.getSelectedGeoAreas(appVariant, person);
            if (!CollectionUtils.isEmpty(selectedGeoAreas)) {
                final String selectedGeoAreasString = selectedGeoAreas.stream()
                        .map(GeoArea::getName)
                        .collect(Collectors.joining(", "));
                subSection.newContent("Ausgewählte Gemeinden", "Gemeinden und Städte, die in der App angezeigt werden",
                        selectedGeoAreasString);
            }
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {
        appService.getAppVariantUsages(person)
                .forEach(appVariantUsage ->
                    personDeletionReport.addEntry(appVariantUsage,
                            sharedDataDeletionService.deleteAppVariantUsage(appVariantUsage))
                );
    }

}
