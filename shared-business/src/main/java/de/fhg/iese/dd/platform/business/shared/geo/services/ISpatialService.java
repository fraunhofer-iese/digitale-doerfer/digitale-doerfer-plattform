/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Axel Wickenkamp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.BoundingBox;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.locationtech.jts.geom.Geometry;

public interface ISpatialService extends IService {

    /**
     * Computes the distance in Kilometer between two GPSLocations.
     *
     * @param loc1
     * @param loc2
     * @return The distance in KM
     */
    double distanceInKM(GPSLocation loc1, GPSLocation loc2);

    /**
     * Checks whether the location is inside the polygons defined by the boundary points
     *
     * @param geometry must not be {@code null} and should at least contain one polygon. All polygons need to contain at
     *                 least three points. If it does not contain any polygon, the result is false.
     * @param location must not be {@code null}
     *
     * @return
     */
    boolean isWithinBoundary(Geometry geometry, GPSLocation location);

    /**
     * Calculates the bounding box of the geo area
     *
     * @param geoArea the geo area
     * @return Min and Max of the bounding box
     */
    BoundingBox calculateBoundingBox(GeoArea geoArea);

    BoundingBox calculateBoundingBox(Geometry geometry);

    /**
     * True if the bounding boxes have an intersection, the order of the bounding boxes does not matter
     *
     * @param boundingBoxA first bounding box
     * @param boundingBoxB second bounding box
     * @return true if there is an intersection
     */
    boolean intersects(BoundingBox boundingBoxA, BoundingBox boundingBoxB);

    /**
     * Answers if a GPSLocation loc is contained within a circle that is defined by GPSLocations start and end, which
     * are on the circle and on a diameter
     *
     * @param start
     * @param end
     * @param loc
     *
     * @return
     */
    boolean isWithinPerimeter(GPSLocation start, GPSLocation end, GPSLocation loc);

    /**
     * Creates geometry from geo json of geo area
     *
     * @param geoArea
     *
     * @return
     */
    Geometry createGeometryFromGeoArea(GeoArea geoArea);

    Geometry createGeometryFromGeoJson(String geoJson);

    /**
     * Checks if the geo json of a geo area represents a valid geometry
     *
     * @param geoArea
     */
    void checkGeometry(GeoArea geoArea);

}
