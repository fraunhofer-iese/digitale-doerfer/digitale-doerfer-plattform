/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthClientNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class OauthClientService extends BaseEntityService<OauthClient> implements IOauthClientService {

    private Map<String, OauthClient> oauthClientByOauthClientIdentifier = Collections.emptyMap();

    @Autowired
    private OauthClientRepository oauthClientRepository;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return ICachingComponent.CacheConfiguration.builder()
                .cachedEntity(OauthClient.class)
                .build();
    }

    @Override
    public OauthClient findByOauthClientIdentifier(String oAuthClientIdentifier) throws OauthClientNotFoundException {

        checkAndRefreshCache();
        OauthClient oauthClient = oauthClientByOauthClientIdentifier.get(oAuthClientIdentifier);
        if (oauthClient == null) {
            throw new OauthClientNotFoundException("Unknown oauth client identifier {}", oAuthClientIdentifier);
        }
        return oauthClient;
    }

    @Override
    public Optional<OauthClient> findByOauthClientIdentifierOptional(String oAuthClientIdentifier) {

        checkAndRefreshCache();
        return Optional.ofNullable(oauthClientByOauthClientIdentifier.get(oAuthClientIdentifier));
    }

    @Override
    public void invalidateCache() {
        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {

        cacheChecker.refreshCacheIfStale(
                () -> oauthClientByOauthClientIdentifier = oauthClientRepository.findAll().stream()
                        .collect(Collectors.toMap(OauthClient::getOauthClientIdentifier, Function.identity())));
    }

}
