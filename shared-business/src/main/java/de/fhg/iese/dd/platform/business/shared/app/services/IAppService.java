/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantUsageInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.concurrent.TimeUnit;

public interface IAppService extends IEntityService<App>, ICachingComponent {

    long LAST_APP_VARIANT_USAGE_PERIOD = TimeUnit.HOURS.toMillis(2L);

    int APP_VARIANT_DELETION_MAX_USAGES = 100;

    App getPlatformApp();

    AppVariant getPlatformAppVariant();

    App findById(String appId) throws AppNotFoundException;

    App findByAppIdentifier(String appIdentifier) throws AppNotFoundException;

    AppVariant findAppVariantByAppVariantIdentifierIncludingDeleted(String appVariantIdentifier)
            throws AppVariantNotFoundException;

    AppVariant findAppVariantByAppVariantIdentifier(String appVariantIdentifier) throws AppVariantNotFoundException;

    AppVariant findAppVariantByIdIncludingDeleted(String appVariantId) throws AppVariantNotFoundException;

    AppVariant findAppVariantById(String appVariantId) throws AppVariantNotFoundException;

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    AppVariant findAppVariantByOauthClient(OauthClient oAuthClient,
            Optional<String> appVariantIdentifier,
            Optional<String> apiKey) throws AppVariantNotFoundException;

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    AppVariant findAppVariantByIdentifierOrApiKey(
            Optional<String> appVariantIdentifier,
            Optional<String> apiKey) throws AppVariantNotFoundException;

    AppVariant findAppVariantByApiKey(String apiKey) throws AppVariantNotFoundException;

    List<App> findAllApps();

    List<AppVariant> findAllAppVariants();

    Set<AppVariant> findAllAppVariants(Collection<Tenant> tenants);

    Set<AppVariant> findAllAppVariants(App app);

    Set<AppVariant> findAllAppVariants(Set<GeoArea> geoAreas, App app);

    /**
     * Creates a new app variant. This is required for creating news sources by admin ui without using the data initializer.
     */
    AppVariant createAppVariant(AppVariant appVariant, String appVariantIdentifierSuffix);

    /**
     * Updates an existing app variant usage with the new usage time or creates a new one.
     *
     * @param appVariant                  the app variant the person is using
     * @param person                      the user of the app variant
     * @param alwaysReturnAppVariantUsage if true the app variant usage is always returned. If false it is only returned
     *                                    if it was updated or created.
     *
     * @return the created or updated app variant usage or null if alwaysReturnAppVariantUsage is false and it did not
     *         need to be created or updated.
     */
    AppVariantUsage createOrUpdateAppVariantUsage(AppVariant appVariant, Person person,
            boolean alwaysReturnAppVariantUsage);

    void logHumanReadableAppVariantStatistics(AppVariant appVariant, LogSummary logSummary);

    /**
     * Tries to hard delete an app variant.
     *
     * @param appVariant the app variant to delete
     * @param logSummary log summary for feedback to the admin
     */
    void deleteAppVariantHard(AppVariant appVariant, LogSummary logSummary);

    /**
     * Soft deletes an app variant.
     *
     * @param appVariant the app variant to delete
     */
    AppVariant deleteAppVariant(AppVariant appVariant);

    /**
     * Get all app variant usages of that person for that app.
     *
     * @param app
     * @param person
     * @return
     */
    List<AppVariantUsage> getAppVariantUsages(App app, Person person);

    List<AppVariantUsage> getAppVariantUsagesOrderByLastUsageDesc(App app, Person person);

    /**
     * Get all app variant usages of that person.
     *
     * @param person
     *
     * @return
     */
    List<AppVariantUsage> getAppVariantUsages(Person person);

    /**
     * Get all tenants for which the app variant is available.
     *
     * @param appVariant the app variant to get the tenants for
     *
     * @return all tenants for which the app variant is available
     */
    Set<Tenant> getAvailableTenants(AppVariant appVariant);

    /**
     * Get all geo areas for which the app variant is available and all parents of these areas.
     *
     * @param appVariant the app variant to get the geo areas for
     *
     * @return all geo areas for which the app variant is available, including their parents
     */
    Set<GeoArea> getAvailableGeoAreas(AppVariant appVariant);

    Set<String> getAvailableGeoAreaIds(AppVariant appVariant);

    Map<GeoArea, Set<AppVariantGeoAreaMapping>> getAvailableGeoAreasWithMappings(AppVariant appVariant);

    /**
     * Get all geo areas for which the app variant is available.
     *
     * @param appVariant the app variant to get the geo areas for
     *
     * @return all geo areas for which the app variant is available, excluding the parents, but including the children.
     */
    Set<GeoArea> getAvailableGeoAreasStrict(AppVariant appVariant);

    Map<GeoArea, Set<AppVariantGeoAreaMapping>> getAvailableGeoAreasStrictWithMappings(AppVariant appVariant);

    /**
     * Get the configured root geo areas for which the app variant should be available. All geo areas under them are
     * available in the app variant.
     * <p>
     * If there is an exclusion all available geo areas are returned, see {@link #getAvailableGeoAreasStrict(AppVariant)}.
     * <p>
     * <strong>This method is not cached!</strong>
     *
     * @param appVariant the app variant to get the geo areas for
     *
     * @return the roots of the available geo areas or all available geo areas
     */
    Set<GeoArea> getConfiguredAvailableRootGeoAreas(AppVariant appVariant);

    /**
     * Get all available geo areas for an AppVariant based on {@link #getAvailableGeoAreas(AppVariant)} whose center
     * point is located within a given radius. If {@code onlyLeaves} is true, non leaf geo areas are filtered out.
     *
     * @param appVariant
     * @param currentLocation
     * @param radiusKM
     * @param onlyLeaves
     *
     * @return
     */
    Set<GeoArea> getNearestAvailableGeoAreas(AppVariant appVariant, GPSLocation currentLocation, double radiusKM,
            boolean onlyLeaves);

    Set<GeoArea> getAvailableRootGeoAreas(AppVariant appVariant);

    Set<GeoArea> getAvailableLeafGeoAreas(AppVariant appVariant);

    Set<GeoArea> searchAvailableGeoAreas(AppVariant appVariant, int upToDepth,
            boolean includeAllChildrenOfMatchingAreas, String search);

    /**
     * Get all selected geo areas
     * <p/>
     * DO NOT USE THIS METHOD! Whenever possible, try to use {@link #getSelectedGeoAreaIds(AppVariant, Person)} for a
     * better performance.
     *
     * @param appVariant
     * @param person
     *
     * @return
     */
    Set<GeoArea> getSelectedGeoAreas(AppVariant appVariant, Person person);

    /**
     * Get ids of all selected geo areas
     *
     * @param appVariant
     * @param person
     *
     * @return
     */
    Set<String> getSelectedGeoAreaIds(AppVariant appVariant, Person person);

    /**
     * Get tenants of all selected geo areas
     *
     * @param appVariant
     * @param person
     *
     * @return
     */
    Set<Tenant> getTenantsOfSelectedGeoAreas(AppVariant appVariant, Person person);

    /**
     * Selects the geo ares for that person for this app variant.
     * <p/>
     * Internally checks if there is already an app variant usage and updates it.
     *
     * @param appVariant
     * @param person
     * @param selectedGeoAreas must be a subset of the {@link #getAvailableGeoAreas(AppVariant)}
     *
     * @return the app variant usage for that person and app variant and the actually selected geo areas. This can
     *         differ from the selectedGeoAreas since the parents are also added.
     *
     * @throws AppVariantUsageInvalidException if the selectedGeoAreas are not a subset of the {@link
     *                                         #getAvailableGeoAreas(AppVariant)}
     */
    Pair<AppVariantUsage, Set<GeoArea>> selectGeoAreas(AppVariant appVariant, Person person,
            Set<GeoArea> selectedGeoAreas) throws AppVariantUsageInvalidException;

    /**
     * For every geo area get a map of app variants to number of persons that selected this geo area in this app
     * variant.
     * <p/>
     * Only those geo areas that have at least one selection area returned.
     *
     * @return A map with geo area selection for all app variants
     */
    Map<GeoArea, Map<AppVariant, Long>> getPersonCountBySelectedGeoAreaAndAppVariant();

    /**
     * For every geo area that has an {@link AppVariantUsageAreaSelection} for an app variant of {@code app} get the
     * number of persons that selected this geo area
     *
     * @param app the app to get the statistics for
     *
     * @return A map with geo area id and number of persons who selected this geo area in an app variant of the app
     */
    Map<String, Long> getPersonCountBySelectedGeoAreaId(App app);

    Collection<AppVariantUsageAreaSelection> findAllSelectionsWithoutChildren();

    Page<AppVariantUsage> findAllUsagesWithSelectionsWithoutParents(Pageable request);

    /**
     * Checks if the cache for available geo areas is stale and returns the point in time when the cache was refreshed
     * for the last time.
     * <p>
     * Required to connect caches that depend on this one.
     */
    Long checkCacheAvailableGeoAreasByAppVariant();

    /**
     * Checks if the cache for apps and app variants is stale and returns the point in time when the cache was refreshed
     * for the last time.
     * <p>
     * Required to connect caches that depend on this one.
     */
    Long checkCacheAppsAndAppVariants();

}
