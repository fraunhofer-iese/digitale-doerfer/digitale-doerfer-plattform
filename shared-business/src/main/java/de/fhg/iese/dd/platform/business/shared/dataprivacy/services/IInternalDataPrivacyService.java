/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.IDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IInternalDataPrivacyService extends IService {

    /**
     * Collects all data of the given person stored at the platform.
     * <p/>
     * Internally calls all {@link IDataPrivacyHandler} to collect the data from the different modules.
     *
     * @param person            to collect the data for
     * @param dataPrivacyReport report that will be extended with the data of the given person
     */
    void collectInternalUserData(Person person, IDataPrivacyReport dataPrivacyReport);

    /**
     * Deletes or erases all data of the given person stored at the platform. </p/> Internally calls all {@link
     * IDataPrivacyHandler} to delete the data from the different modules.
     *
     * @param person               the person to be deleted
     * @param personDeletionReport report that will be extended with the deletion information of the data of the given
     *                             person
     */
    void deleteInternalUserData(Person person, IPersonDeletionReport personDeletionReport);

}
