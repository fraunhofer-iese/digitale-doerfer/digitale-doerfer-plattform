/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.events;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AppVariantTenantContractChangeAdditionalNotesConfirmation  extends BaseEvent {

    private AppVariantTenantContract appVariantTenantContract;
    
}
