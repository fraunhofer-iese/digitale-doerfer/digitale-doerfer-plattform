/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.InvalidFeatureConfigurationException;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;

@Component
public class FeatureDataInitializer extends BaseDataInitializer {

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private static final ObjectWriter filteringPrettyObjectWriter = initializeFilteringPrettyObjectWriter();

    @Autowired
    protected IFeatureService featureService;
    @Autowired
    protected FeatureConfigRepository featureConfigRepository;
    @Autowired
    protected AppRepository appRepository;
    @Autowired
    protected AppVariantRepository appVariantRepository;
    @Autowired
    protected ITenantService tenantService;

    /**
     * Only required to add a json filter to feature, in order to remove redundant values from the feature json. This is
     * added as a "mixin" to the base feature
     */
    @JsonFilter("featureSerializationToDB")
    private static class FeatureFilter {

    }

    private static ObjectWriter initializeFilteringPrettyObjectWriter() {

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        //this enables adding annotations to a class without actually annotating it
        objectMapper.addMixIn(BaseFeature.class, FeatureFilter.class);

        return objectMapper.writer()
                .with(SerializationFeature.INDENT_OUTPUT)
                .with(new SimpleFilterProvider().addFilter("featureSerializationToDB",
                        //we don't want to serialize these values, since they are stored in FeatureConfig
                        SimpleBeanPropertyFilter.serializeAllExcept("featureIdentifier", "enabled",
                                "featureValuesAsJson")))
                .with(new DefaultPrettyPrinter().withArrayIndenter(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE));
    }

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("feature")
                .useCase(USE_CASE_NEW_TENANT)
                .useCase(USE_CASE_NEW_APP_VARIANT)
                .requiredEntity(Tenant.class)
                .requiredEntity(GeoArea.class)
                .requiredEntity(App.class)
                .requiredEntity(AppVariant.class)
                .processedEntity(FeatureConfig.class)
                .processedEntity(AppVariantVersion.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        //get all old entities to remove the orphans later
        List<FeatureConfig> existingFeatureConfigs = featureService.getAllFeatureConfigs();

        //get all defined feature classes and their @Feature annotation
        List<? extends Class<? extends BaseFeature>> featureClasses = featureService.getFeatureClasses();

        logSummary.info("Found {} features to initialize: {}",
                featureClasses.size(),
                featureClasses.stream()
                        .map(this::featureClassToName)
                        .collect(Collectors.joining(", ", "[", "]")));

        Set<FeatureConfig> createdFeatureConfigs = new HashSet<>();
        for (Class<? extends BaseFeature> featureClass : featureClasses) {
            createdFeatureConfigs.addAll(loadFeatureConfigs(featureClass, logSummary));
        }

        //we remove all old configurations, this is possible since they get constant ids based on the feature target
        existingFeatureConfigs.removeAll(createdFeatureConfigs);
        if (!existingFeatureConfigs.isEmpty()) {
            logSummary.info("Deleting {} previous feature configurations", existingFeatureConfigs.size());
            featureConfigRepository.deleteAll(existingFeatureConfigs);
        }
        logSummary.info("Saving {} feature configurations", createdFeatureConfigs.size());
        // we save them at once to avoid triggering a cache refresh with only partial config
        featureConfigRepository.saveAll(createdFeatureConfigs);
        featureConfigRepository.flush();
    }

    private String featureClassToName(Class<? extends BaseFeature> featureClass) {
        return featureClass.getSimpleName();
    }

    private Set<FeatureConfig> loadFeatureConfigs(Class<? extends BaseFeature> featureClass, LogSummary logSummary) {

        /*
        Since we only want to store the actual configuration and NOT the default values that were
        automatically set by java (e.g. int = 0, boolean = false, ...) we loaded the feature also as a Json Node.
        Such a Json Node can hold any valid Json, in our case exactly the configuration that was done
        in the data init for this tenant and feature.
        */
        final Map<DataInitKey, List<JsonNode>> keysAndEntitiesAsRawJson =
                loadAllDataInitKeysAndEntities(featureClass.getSimpleName(), JsonNode.class);

        //if there is no configuration, skip
        if (CollectionUtils.isEmpty(keysAndEntitiesAsRawJson)) {
            return Collections.emptySet();
        }
        Set<FeatureConfig> loadedFeatures = new HashSet<>();
        for (Map.Entry<DataInitKey, List<JsonNode>> keyAndEntities : keysAndEntitiesAsRawJson.entrySet()) {
            final DataInitKey key = keyAndEntities.getKey();
            final List<JsonNode> featureConfigJsons = keyAndEntities.getValue();
            //multiple definitions for the same feature key do not make sense
            if (featureConfigJsons.size() > 1) {
                throw new InvalidFeatureConfigurationException(
                        "Feature {} defines multiple configurations. Check the configurations defined at '{}'",
                        featureClass, key.getFileNames());
            }

            final JsonNode featureConfigRawJson = keysAndEntitiesAsRawJson.get(key).get(0);
            final BaseFeature featureConfigJson;
            // We check the feature configuration that it is valid by having it de-serialized as the according class
            try {
                featureConfigJson = PRECONFIGURED_READER.forType(featureClass).readValue(featureConfigRawJson);
            } catch (IOException e) {
                throw new InvalidFeatureConfigurationException(
                        "Feature configuration {} is invalid. Check the configurations defined at '{}': {}",
                        featureClass, key.getFileNames(), e.toString());
            }
            if (key.getApp() != null && key.getAppVariant() != null) {
                throw new InvalidFeatureConfigurationException(
                        "Feature configuration {} defines config for both app and app variant. " +
                                "Split it into two configurations. Check the configurations defined at '{}': {}",
                        featureClass, key.getFileNames());
            }
            if (CollectionUtils.isEmpty(key.getGeoAreasIncluded()) &&
                    !CollectionUtils.isEmpty(key.getGeoAreasExcluded())) {
                throw new InvalidFeatureConfigurationException(
                        "Feature configuration {} defines excluded geo areas but no included. " +
                                "Check the configurations defined at '{}': {}",
                        featureClass, key.getFileNames());
            }
            FeatureConfig featureConfig = FeatureConfig.builder()
                    .featureClass(featureClass.getName())
                    //enabled is saved separately, NOT in the json
                    .enabled(featureConfigJson.isEnabled())
                    //the json is filtered, so that the identifier and the enabled flag are not stored there to avoid redundancy
                    .configValues(toFilteredPrettyJson(featureConfigRawJson))
                    .geoAreasIncluded(key.getGeoAreasIncluded())
                    .geoAreasExcluded(key.getGeoAreasExcluded())
                    .geoAreaChildrenIncluded(key.isGeoAreaChildrenIncluded())
                    .tenant(key.getTenant())
                    .appVariant(key.getAppVariant())
                    .app(key.getApp())
                    .build()
                    //the constant id is based on the featureClass, tenant, appVariant, app, geoArea
                    .withConstantId();
            //checks if the feature can be loaded, so that we fail instead of saving it
            featureService.checkIsFeatureConfigValid(featureConfig);
            loadedFeatures.add(featureConfig);
            logSummary.info("'{}' with enabled {} for {}",
                    featureClassToName(featureClass), featureConfig.isEnabled(), featureKeyToString(key));
        }
        return loadedFeatures;
    }

    public String featureKeyToString(DataInitKey key) {
        StringBuilder s = new StringBuilder();
        final App app = key.getApp();
        if (app != null) {
            s.append("app=").append(app.getAppIdentifier()).append(", ");
        }
        final AppVariant appVariant = key.getAppVariant();
        if (appVariant != null) {
            s.append("appVariant=").append(appVariant.getAppVariantIdentifier()).append(", ");
        }
        if (!CollectionUtils.isEmpty(key.getGeoAreasIncluded())) {
            s.append("geoAreasIncluded=").append(key.getGeoAreasIncluded()).append(", ");
        }
        s.append("geoAreaChildrenIncluded=").append(key.isGeoAreaChildrenIncluded()).append(", ");
        if (!CollectionUtils.isEmpty(key.getGeoAreasExcluded())) {
            s.append("geoAreasExcluded=").append(key.getGeoAreasExcluded()).append(", ");
        }
        if (key.getTenant() != null) {
            s.append("tenant=").append(key.getTenant()).append(", ");
        }
        return s.toString();
    }

    private String toFilteredPrettyJson(Object feature) throws InvalidFeatureConfigurationException {

        try {
            return FeatureDataInitializer.getFilteringPrettyObjectWriter().writeValueAsString(feature);
        } catch (JsonProcessingException e) {
            throw new InvalidFeatureConfigurationException("Feature {} can not be serialized: {}", feature.getClass(),
                    e.getMessage());
        }
    }

}
