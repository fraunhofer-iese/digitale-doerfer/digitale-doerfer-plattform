/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Danielle Korth, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TeamFileNotFoundException;

public interface ITeamFileStorageService extends IService {

    /**
     * Saves a file and creates the parent directories, if they are not existing
     */
    void saveFile(byte[] content, String mimeType, String fileName) throws FileStorageException;

    /**
     * Returns a file, if existing
     */
    byte[] getFile(String fileName) throws FileStorageException, TeamFileNotFoundException;

    /**
     * Creates a directory, including all parent directories
     */
    void createDirectory(String directoryName) throws FileStorageException;

    /**
     * Checks if a file or directory exists
     */
    boolean fileExists(String fileName) throws FileStorageException;

    /**
     * Deletes a file or directory. If it is not existing, no exception is thrown.
     */
    void delete(String fileName) throws FileStorageException;

}
