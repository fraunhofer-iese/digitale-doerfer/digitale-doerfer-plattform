/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ITenantService extends IEntityService<Tenant>, ICachingComponent {

    Tenant findTenantById(String tenantId) throws TenantNotFoundException;

    List<Tenant> findAllOrderedByNameAsc();

    Page<Tenant> findAll(Pageable page);

    /**
     * Searches for tenants having the search string as infix (=continuous substring) in id, name or tag. Search is done
     * in a case-insensitive way.
     */
    Page<Tenant> findAllByInfixSearch(String infixSearchString, Pageable page);

    Page<Tenant> findAllById(Set<String> tenantIds, Pageable page);

    Page<Tenant> findAllByIdInfixSearch(String infixSearchString, Set<String> tenantIds, Pageable page);

    List<Tenant> findAllExcluding(Collection<Tenant> tenants);

    void checkTenantByIdExists(String tenantId) throws TenantNotFoundException;

    void checkTenantsByIdExists(Set<String> tenantIds) throws TenantNotFoundException;

    /**
     * All tags that have been given to the tenants, including null
     */
    List<String> findAllTenantTags();

    Tenant forceCached(Tenant tenant);

    /**
     * Checks if the cache for tenants is stale and returns the point in time when the cache was refreshed for the
     * last time.
     * <p>
     * Required to connect caches that depend on this one.
     */
    Long checkCacheTenants();

}
