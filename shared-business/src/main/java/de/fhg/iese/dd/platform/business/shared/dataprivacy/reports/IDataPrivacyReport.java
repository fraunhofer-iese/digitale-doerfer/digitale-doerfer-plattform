/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Johannes Schneider, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.Signature;

/**
 * Simple tree like data report, following the structure
 * <p/>
 * section -> sub section -> content
 * <p/>
 * Every element has a name and an optional description.
 */
public interface IDataPrivacyReport {

    String getTitle();

    IDataPrivacyReportSection newSection(String name, String description);

    boolean removeSection(IDataPrivacyReportSection section);

    List<IDataPrivacyReportSection> getSections();

    List<MediaItem> getMediaItems();

    List<DocumentItem> getDocumentItems();

    interface IDataPrivacyReportSection {

        String getName();

        String getDescription();

        IDataPrivacyReportSubSection newSubSection(String name, String description);

        List<IDataPrivacyReportSubSection> getSubSections();

        @JsonIgnore
        boolean isEmpty();

    }

    interface IDataPrivacyReportSubSection {

        String getName();

        String getDescription();

        /**
         * Creates a new content element in the report without description.
         * <p/>
         * If the content is empty, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param content
         *         the actual content
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<String> newContent(String name, String content);

        /**
         * Creates a new content element in the report with description.
         * <p/>
         * If the content is empty, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param description
         *         of the data attribute in the content, if the name is not enough
         * @param content
         *         the actual content
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<String> newContent(String name, String description, String content);

        /**
         * Creates a new content element for a media item in the report without description.
         * <p/>
         * If the content is null, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param content
         *         the actual media item
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<MediaItem> newImageContent(String name, MediaItem content);

        /**
         * Creates a new content element for a media item in the report with description.
         * <p/>
         * If the content is null, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param description
         *         of the data attribute in the content, if the name is not enough
         * @param content
         *         the actual media item
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<MediaItem> newImageContent(String name, String description, MediaItem content);

        /**
         * Creates a new content element for a document item in the report with description.
         * <p/>
         * If the content is null, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name        of the data attribute in the content
         * @param description of the data attribute in the content, if the name is not enough
         * @param content     the actual document item
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<DocumentItem> newDocumentContent(String name, String description,
                DocumentItem content);

        /**
         * Creates a new content element for a signature in the report without description.
         * <p/>
         * If the content is null, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param signature
         *         the actual signature
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<Signature> newSignatureContent(String name, Signature signature);

        /**
         * Creates a new content element for a signature in the report with description.
         * <p/>
         * If the content is null, no element is added. Thus there is no need to check if the content is there or not.
         *
         * @param name
         *         of the data attribute in the content
         * @param description
         *         of the data attribute in the content, if the name is not enough
         * @param signature
         *         the actual signature
         *
         * @return the newly created content element
         */
        IDataPrivacyReportContent<Signature> newSignatureContent(String name, String description, Signature signature);

        /**
         * Creates a new content element for a location in the report. If the content is null, no element is added.
         *
         * @param name
         *         of the data attribute in the content
         * @param description
                  of the data attribute in the content, if the name is not enough
         * @param content
         *         the actual gps location
         * @return the newly created content element
         */
        IDataPrivacyReportContent<GPSLocation> newLocationContent(String name, String description, GPSLocation content);

        List<IDataPrivacyReportContent<?>> getContents();

    }

    /**
     * Leaf element of the report, containing the actual data.
     *
     * @param <T>
     *         type of the data, currently {@link String}, {@link MediaItem} and {@link GPSLocation} are supported.
     *         If more should be supported, the templates at {@link DataPrivacyTemplate} need to be adjusted.
     */
    interface IDataPrivacyReportContent<T> {

        String getName();

        String getDescription();

        T getContent();

        String getContentClass();

    }

}
