/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import java.util.function.Function;

public enum DataPrivacyReportFormat {

    HTML(DataPrivacyReportFormatVisitor::whenHtml),
    TEXT(DataPrivacyReportFormatVisitor::whenText),
    JSON(DataPrivacyReportFormatVisitor::whenJson);

    //safe to declare it transient since an enum is serialized by it constant name (String) only
    // cf. section 1.12 in https://docs.oracle.com/javase/1.5.0/docs/guide/serialization/spec/serial-arch.html#enum
    private final transient Function<DataPrivacyReportFormatVisitor<?>, Object> visitor;

    DataPrivacyReportFormat(Function<DataPrivacyReportFormatVisitor<?>, Object> visitor) {
        this.visitor = visitor;
    }

    @SuppressWarnings("unchecked") //the visitor returns type T, there is just no option to pass <T> to the Function
    public <T> T accept(DataPrivacyReportFormatVisitor<T> v) {
        return (T) visitor.apply(v);
    }

}
