/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

@Component
public class OrphanImageFilesRemoverAdminTask extends BaseAdminTask {

    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    private MediaConfig mediaConfig;

    @Autowired
    private IFileStorage fileStorage;

    @Override
    public String getName() {
        return "RemoveOrphanImageFiles";
    }

    @Override
    public String getDescription() {
        return "Removes orphaned files on the file storage at '" + mediaConfig.getInternalStoragePrefix() +
                "' that are not referenced by media items";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        String imagePath = mediaConfig.getInternalStoragePrefix();
        logSummary.info("Retrieving all file names in file storage at '{}'", imagePath);
        Set<String> existingFileNames = new HashSet<>(fileStorage.findAll(imagePath));
        logSummary.info("Found {} files", existingFileNames.size());

        logSummary.info("Retrieving all referenced file names in database");
        Set<String> referencedFileNames = Collections.synchronizedSet(new HashSet<>());

        //we need to use other parameters here, since we need to ensure that we get all media items
        AdminTaskParameters databaseAccessParameters = AdminTaskParameters.builder()
                .pageFrom(0)
                .pageTo(Integer.MAX_VALUE)
                .pageSize(500)
                .pageTimeoutSeconds(3)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(parameters.getParallelismPerPage())
                .build();

        processPages(logSummary, databaseAccessParameters,
                (pageRequest) -> mediaItemRepository
                        .findAll(pageRequest),
                (existingMediaItemsPage) -> processParallel((MediaItem m) -> m.getUrls().values().stream()
                        .map(fileStorage::getInternalFileName)
                        .forEach(f -> {
                            if (!existingFileNames.contains(f)) {
                                logSummary.warn("Media item {} references file '{}' that is not existing",
                                        m.getId(), f);
                            }
                            referencedFileNames.add(f);
                        }), existingMediaItemsPage.getContent(), logSummary, databaseAccessParameters));

        logSummary.info("Found {} referenced file names for {} media items in database ", referencedFileNames.size(),
                mediaItemRepository.count());

        List<String> nonReferencedFiles = existingFileNames.stream()
                .filter(e -> !referencedFileNames.contains(e))
                .sorted()
                .collect(Collectors.toList());

        logSummary.info("Trashing {} orphan files in file storage at '{}'", nonReferencedFiles.size(),
                imagePath);

        processPages(logSummary, parameters,
                pageRequest -> {
                    int start = Math.toIntExact(pageRequest.getOffset());
                    int end = Math.min(start + pageRequest.getPageSize(), nonReferencedFiles.size());
                    return new PageImpl<>(nonReferencedFiles.subList(start, end), pageRequest,
                            nonReferencedFiles.size());
                },
                filesToDeletePage -> {
                    logSummary.info("Trashing {} orphan files", filesToDeletePage.getNumberOfElements());
                    processParallel((String fileName) -> {
                        if (!parameters.isDryRun()) {
                            fileStorage.moveToTrash(fileName);
                        }
                    }, filesToDeletePage.getContent(), logSummary, parameters);
                    logSummary.info("Trashed {} orphan files", filesToDeletePage.getNumberOfElements());
                });

    }

}
