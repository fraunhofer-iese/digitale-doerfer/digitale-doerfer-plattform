/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsMetaData {

    private String id;
    private String machineName;
    private int expectedWidth;
    private String valueFormat;
    @Singular
    private List<StatisticsTimeRelation> timeRelations;

    public List<String> getSpecificMachineNames() {
        if (CollectionUtils.isEmpty(timeRelations)) {
            return Collections.singletonList(machineName);
        } else {
            return timeRelations.stream()
                    .map(this::getSpecificMachineName)
                    .collect(Collectors.toList());
        }
    }

    public String getSpecificMachineName(StatisticsTimeRelation timeRelation) {
        return machineName + "." + timeRelation.name().toLowerCase();
    }

}
