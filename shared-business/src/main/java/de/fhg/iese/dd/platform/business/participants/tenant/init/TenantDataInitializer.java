/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.init;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;

@Component
public class TenantDataInitializer extends BaseDataInitializer {

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private IAppService appService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("tenant")
                .useCase(USE_CASE_NEW_TENANT)
                .processedEntity(Tenant.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        Map<String, List<Tenant>> entities = loadEntitiesFromDataJson(null, Tenant.class);
        if (entities == null) {
            logSummary.error("No data json found, skipping creation!");
            return;
        }
        final AppVariant platformAppVariant = appService.getPlatformAppVariant();
        DuplicateIdChecker<Tenant> idChecker = new DuplicateIdChecker<>();
        Collection<Tenant> createdTenants = new LinkedList<>();
        // we expect the sections of the data init to have the tags as names
        for (Map.Entry<String, List<Tenant>> tenantsByTag : entities.entrySet()) {
            String tag = tenantsByTag.getKey();
            if (StringUtils.isBlank(tag)) {
                tag = null;
            }
            List<Tenant> tenants = tenantsByTag.getValue();
            for (Tenant tenant : tenants) {
                checkId(tenant);
                idChecker.checkId(tenant);
                tenant = adjustCreated(tenant);
                tenant = createImage(Tenant::getProfilePicture, Tenant::setProfilePicture, tenant,
                        FileOwnership.of(platformAppVariant), logSummary, false);
                tenant.setTag(tag);
                createdTenants.add(tenant);
                logSummary.info("created {}", tenant.toString());
            }
        }
        //we save them all at once to be more efficient and reduce time for the data init
        createdTenants = tenantRepository.saveAll(createdTenants);
        tenantRepository.flush();

        //check for orphans
        List<Tenant> orphans = tenantService.findAllExcluding(createdTenants);
        if (!CollectionUtils.isEmpty(orphans)) {
            logSummary.warn("There are orphaned tenants that are not in the data init json files:\n{}",
                    orphans.stream()
                            .map(Tenant::toString)
                            .collect(Collectors.joining("\n")));
        }
    }

}
