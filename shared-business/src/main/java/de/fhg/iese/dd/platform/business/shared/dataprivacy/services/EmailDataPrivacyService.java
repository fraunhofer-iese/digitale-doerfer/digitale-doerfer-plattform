/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyTemplate;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import freemarker.template.TemplateException;

@Service
class EmailDataPrivacyService extends BaseDataPrivacyService implements IEmailDataPrivacyService {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @Autowired
    private ITeamNotificationService teamNotificationService;
    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private ParticipantsConfig participantsConfig;

    @Override
    public void sendDataPrivacyReport(DataPrivacyDataCollection dataCollection, String zipFileUrl)
            throws IOException, TemplateException, MessagingException {

        if (dataCollection.getWorkflowTrigger() == DataPrivacyWorkflowTrigger.USER_TRIGGERED ||
                dataCollection.getWorkflowTrigger() == DataPrivacyWorkflowTrigger.ADMIN_TRIGGERED_MAIL_TO_USER) {
            try {
                Map<String, Object> templateModel = new HashMap<>();
                final Person person = dataCollection.getPerson();
                templateModel.put("userName", person.getFirstName());
                templateModel.put("zipFile", zipFileUrl);
                templateModel.put("deadline", getDataPrivacyReportDeletionDeadline());

                log.info("Sending email to the person with id {}", person.getId());

                String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
                String emailText = emailSenderService
                        .createTextWithTemplate(DataPrivacyTemplate.DATA_PRIVACY_EMAIL_TEXT, templateModel);
                emailSenderService.sendEmail(fromEmailAddress, person.getFullName(), person.getEmail(),
                        "Deine angeforderten Daten", emailText, Collections.emptyList(),
                        true);
            } catch (Exception e) {
                Person person = dataCollection.getPerson();
                log.error("Could not send email to " + person.getId() +
                        " with data privacy report for person with id " + person.getId(), e);
                teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                        TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                        "Could not send email to '{}' with data privacy report for person with id '{}'",
                        person.getEmail(),
                        person.getId());
                throw e;
            }
        }
    }

    private String getDataPrivacyReportDeletionDeadline() {
        return timeService.nowLocal()
                .plus(dataPrivacyConfig.getMaxRetentionDataPrivacyReportsInDays(), ChronoUnit.DAYS)
                .format(DATE_FORMATTER);
    }

    @Override
    public void sendPersonDeletionConfirmation(DataPrivacyDataDeletion dataDeletion)
            throws IOException, TemplateException, MessagingException {

        TemplateLocation templateLocation;
        switch (dataDeletion.getWorkflowTrigger()) {
            case USER_TRIGGERED:
                templateLocation = DataPrivacyTemplate.USER_DELETED_EMAIL;
                break;
            case ADMIN_TRIGGERED:
                //Don't send a mail when an admin has triggered the deletion
                //In future: We could send a mail with another template...
                return;
            case USER_INACTIVITY:
                templateLocation = DataPrivacyTemplate.USER_DELETION_DUE_TO_INACTIVITY_EMAIL;
                break;
            default:
                log.error("Unexpected value: {}", dataDeletion.getWorkflowTrigger());
                return;
        }

        final Person deletedPerson = dataDeletion.getPerson();
        if (deletedPerson.isDeleted()) {
            log.warn("Deletion of already deleted person, can not send email");
            return;
        }
        try {

            String fromEmailAddress = participantsConfig.getSenderEmailAddressAccount();
            String mailText = emailSenderService.createTextWithTemplate(templateLocation,
                    Map.of("receiverName", deletedPerson.getFirstName()));

            emailSenderService.sendEmail(fromEmailAddress, deletedPerson.getFullName(), deletedPerson.getEmail(),
                    "Dein Konto wurde gelöscht", mailText, Collections.emptyList(), true);
        } catch (Exception e) {
            log.error("Could not send email to " + deletedPerson.getEmail() + " about deletion person with id "
                    + deletedPerson.getId(), e);
            teamNotificationService.sendTeamNotification(TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                    "Could not send email to '{}' about deletion person with id '{}'",
                    deletedPerson.getEmail(),
                    deletedPerson.getId());
            throw e;
        }
    }

}
