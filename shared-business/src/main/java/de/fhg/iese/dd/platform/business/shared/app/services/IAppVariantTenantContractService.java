/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantTenantContractNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;

public interface IAppVariantTenantContractService extends IEntityService<AppVariantTenantContract> {

    AppVariantTenantContract findAppVariantTenantContractById(String appVariantTenantContractId) throws
            AppVariantTenantContractNotFoundException;

    Page<Tenant> getTenantsByContractOrAppVariant(Set<String> contractIds, Set<String> appVariantIds, String searchTerm,
            Pageable pageable);

    Page<AppVariant> getAppVariantsByTenantOrContract(Set<String> tenantIds, Set<String> contractIds,
            String searchTerm, Pageable pageable);

    Page<AppVariantTenantContract> getContractsByTenantOrAppVariant(Set<String> tenantIds, Set<String> appVariantIds,
            String searchTerm, Pageable pageable);

}
