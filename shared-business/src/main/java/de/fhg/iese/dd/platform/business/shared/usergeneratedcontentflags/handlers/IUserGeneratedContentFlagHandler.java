/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers;

import java.util.Collection;

import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.UserGeneratedContentFlagEntityCanNotBeDeletedException;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;

/**
 * A user generated content flag handler is able to deal with deletion requests of entities that were flagged. They are
 * injected in the {@link IUserGeneratedContentFlagService} and called when a request to delete a flagged entity comes
 * in.
 * <p>
 * Do not implement this interface directly, use {@link BaseUserGeneratedContentFlagHandler} instead.
 */
public interface IUserGeneratedContentFlagHandler {

    /**
     * A unique identifier used in logs.
     *
     * @return unique identifier
     */
    String getId();

    /**
     * All types of entities that are processed by this flag handler.
     * <p/>
     * <strong>Only</strong> add the "major" entities here and no contained
     * ones, like {@link de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem}s.
     * <p/>
     * Important implementation note: <br/> Return the value of a {@code static final} constant here, because the method
     * is called very frequently.
     *
     * @return classes of entities that are processed by this flag handler.
     */
    Collection<Class<? extends BaseEntity>> getProcessedEntities();

    /**
     * Deletes the entity that is referenced by the {@link UserGeneratedContentFlag#getEntityId()}. This method is only
     * called when the handler is capable of deleting the entity, as indicated by {@link #getProcessedEntities()}.
     *
     * @param flag the flag of the entity that should be deleted
     *
     * @return information about deleted flagged entity
     *
     * @throws UserGeneratedContentFlagEntityCanNotBeDeletedException if the entity of the flag is not intended to be
     *                                                                deleted
     */
    DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag)
            throws UserGeneratedContentFlagEntityCanNotBeDeletedException;

    /**
     * Returns the detail path to the flag content detail page in the admin ui
     *
     * @param flag the flag to get the detail page path
     */
    String getAdminUiDetailPagePath(UserGeneratedContentFlag flag);

}
