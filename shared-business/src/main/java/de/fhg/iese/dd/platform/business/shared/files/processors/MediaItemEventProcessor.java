/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.processors;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.files.events.MediaItemDeleteConfirmation;
import de.fhg.iese.dd.platform.business.shared.files.events.MediaItemDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.files.events.TemporaryMediaItemUseConfirmation;
import de.fhg.iese.dd.platform.business.shared.files.events.TemporaryMediaItemUseRequest;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@EventProcessor
public class MediaItemEventProcessor extends BaseEventProcessor {

    @Autowired
    private IMediaItemService mediaItemService;

    @EventProcessing
    private TemporaryMediaItemUseConfirmation handleTemporaryMediaItemUseRequest(TemporaryMediaItemUseRequest event) {

        List<MediaItem> mediaItems =
                mediaItemService.useAllItems(event.getTemporaryMediaItems(), event.getNewFileOwnership());
        return TemporaryMediaItemUseConfirmation.builder()
                .mediaItems(mediaItems)
                .build();
    }

    @EventProcessing
    private MediaItemDeleteConfirmation handleMediaItemDeleteRequest(MediaItemDeleteRequest request) {

        List<MediaItem> mediaItemsToDelete = request.getMediaItemsToDelete();
        mediaItemService.deleteAllItems(mediaItemsToDelete);
        return new MediaItemDeleteConfirmation();
    }

}
