/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Stefan Schweitzer, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.geoarea.init;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.geo.services.ISpatialService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class GeoAreaDataInitializer extends BaseDataInitializer {

    private static final String DATA_JSON_SECTION_COMMON_GEO_AREAS = "common";

    private static final String DATA_JSON_SECTION_ROOT_GEO_AREA = "rootArea";

    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAppService appService;
    @Autowired
    private ISpatialService spatialService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("geoarea")
                .useCase(USE_CASE_NEW_TENANT)
                .requiredEntity(Tenant.class)
                .processedEntity(GeoArea.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(final LogSummary logSummary) {
        createGeoAreasAndUpdateRootArea(logSummary);
    }

    private void createGeoAreasAndUpdateRootArea(final LogSummary logSummary) {

        DuplicateIdChecker<GeoArea> geoAreaDuplicateIdChecker = new DuplicateIdChecker<>();

        //we collect all created geo areas first, before we store them,
        // so that we can store the parents before the children
        Map<String, GeoArea> geoAreasById = new HashMap<>();
        //first create all geo areas that do not have a tenant
        //they need to be in the "common" part, otherwise the tenant-specific lookup of the geo area json file
        // would always try to create them again, if the tenant does not define own ones
        List<GeoArea> jsonGeoAreasNoTenant;
        Map<String, List<GeoArea>> jsonGeoAreasNoTenantMap = loadEntitiesFromDataJson(null, GeoArea.class);
        if (!CollectionUtils.isEmpty(jsonGeoAreasNoTenantMap)) {
            jsonGeoAreasNoTenant =
                    jsonGeoAreasNoTenantMap.getOrDefault(DATA_JSON_SECTION_COMMON_GEO_AREAS, Collections.emptyList());
        } else {
            jsonGeoAreasNoTenant = Collections.emptyList();
        }
        jsonGeoAreasNoTenant.stream()
                .map(jsonGeoAreaNoTenant -> createGeoArea(jsonGeoAreaNoTenant, null, logSummary))
                .peek(geoAreaDuplicateIdChecker::checkId)
                .peek(geoArea -> checkValidJson(geoArea.getCustomAttributesJson(), "customAttributesJson"))
                .forEach(geoArea -> geoAreasById.put(geoArea.getId(), geoArea));
        logSummary.info("Created {} geo areas without tenant", jsonGeoAreasNoTenant.size());

        //now create all geo areas for the tenants
        for (final Tenant tenant : tenantService.findAllOrderedByNameAsc()) {
            final List<GeoArea> jsonGeoAreasForTenant =
                    loadEntitiesFromDataJsonDefaultSectionTenantSpecific(tenant, GeoArea.class);

            if (!CollectionUtils.isEmpty(jsonGeoAreasForTenant)) {
                logSummary.info("Creating geo areas for {}", tenant.toString());
                logSummary.indent();
                try {
                    jsonGeoAreasForTenant.stream()
                            .map(jsonGeoAreaForTenant -> createGeoArea(jsonGeoAreaForTenant, tenant, logSummary))
                            .peek(geoAreaDuplicateIdChecker::checkId)
                            .peek(geoArea -> checkValidJson(geoArea.getCustomAttributesJson(), "customAttributesJson"))
                            .forEach(geoArea -> geoAreasById.put(geoArea.getId(), geoArea));
                } finally {
                    logSummary.outdent();
                }
                logSummary.info("Created {} geo areas for {}", jsonGeoAreasForTenant.size(), tenant.toString());
            }
        }

        //set parent area to a real entity
        for (GeoArea geoArea : geoAreasById.values()) {
            if (geoArea.getParentArea() != null) {
                GeoArea parentArea = geoAreasById.get(geoArea.getParentArea().getId());
                if (parentArea == null) {
                    throw new DataInitializationException("Invalid parent reference to '{}' by {}",
                            geoArea.getParentArea().getId(), geoArea.toString());
                }
                geoArea.setParentArea(parentArea);
            }
        }

        //save geo areas
        storeGeoAreas(geoAreasById.values(), logSummary);

        //set root area of tenants
        for (final Tenant tenant : tenantService.findAllOrderedByNameAsc()) {
            final List<GeoArea> jsonRootAreasForTenant =
                    loadEntitiesFromDataJsonSection(tenant, GeoArea.class, DATA_JSON_SECTION_ROOT_GEO_AREA);
            if (!CollectionUtils.isEmpty(jsonRootAreasForTenant)) {
                if (jsonRootAreasForTenant.size() > 1) {
                    logSummary.error("More than one root area defined for {}",
                            tenant.toString());
                    throw new DataInitializationException("Multiple root references in geo area json");
                } else {
                    final GeoArea jsonRootAreaForTenant = jsonRootAreasForTenant.get(0);
                    //we can look it up in the db here, because we saved all of them before
                    GeoArea rootArea = geoAreaRepository.findById(jsonRootAreaForTenant.getId())
                            .orElseThrow(
                                    () -> new DataInitializationException("Invalid root area reference to '{}' by {}",
                                            jsonRootAreaForTenant.getId(), tenant.toString()));
                    tenant.setRootArea(rootArea);
                    Tenant updatedTenant = tenantService.store(tenant);
                    logSummary.info("Setting root area for {} to {}", updatedTenant.toString(), rootArea.toString());
                }
            } else {
                logSummary.info("No root area defined for {}", tenant.toString());
            }
        }

        //check for orphans
        List<GeoArea> orphans = geoAreaRepository.findAllNotIn(new HashSet<>(geoAreasById.values()));
        if (!CollectionUtils.isEmpty(orphans)) {
            logSummary.warn("There are orphaned geo areas that are not in the data init json files:\n {}",
                    orphans.stream()
                            .map(GeoArea::toString)
                            .collect(Collectors.joining("\n")));
        }
    }

    private GeoArea createGeoArea(GeoArea geoArea, Tenant tenant, final LogSummary logSummary) {

        checkId(geoArea);
        final AppVariant platformAppVariant = appService.getPlatformAppVariant();
        geoArea = createImage(GeoArea::getLogo, GeoArea::setLogo, geoArea, FileOwnership.of(platformAppVariant),
                logSummary, false);
        geoArea.setTenant(tenant);

        if (StringUtils.isEmpty(geoArea.getBoundaryJson())) {
            logSummary.warn("No boundary defined for {}", geoArea.toString());
        } else {
            spatialService.checkGeometry(geoArea);
        }
        return geoArea;
    }

    private void storeGeoAreas(Collection<GeoArea> geoAreas, final LogSummary logSummary) {

        //this is just a trick to have a fake global root to get the real root areas
        GeoArea fakeGlobalRootArea = new GeoArea();

        Map<GeoArea, Set<GeoArea>> geoAreasByParenArea = geoAreas.stream()
                .collect(Collectors.groupingBy(
                        geoArea -> geoArea.getParentArea() == null ? fakeGlobalRootArea : geoArea.getParentArea(),
                        Collectors.mapping(Function.identity(), Collectors.toSet())));

        Map<Integer, Set<GeoArea>> geoAreasByDepth = new HashMap<>();
        splitGeoAreasByDepth(geoAreasByParenArea.get(fakeGlobalRootArea), geoAreasByParenArea, geoAreasByDepth, 0);

        storeGeoAreasByDepth(geoAreasByDepth, logSummary);

        logSummary.info("Stored {} geo areas", geoAreas.size());
    }

    private void splitGeoAreasByDepth(Set<GeoArea> rootAreas, Map<GeoArea, Set<GeoArea>> geoAreasByParentArea,
            Map<Integer, Set<GeoArea>> geoAreasByDepth,
            int depth) {
        geoAreasByDepth.computeIfAbsent(depth, k -> new HashSet<>()).addAll(rootAreas);
        for (GeoArea rootArea : rootAreas) {
            Set<GeoArea> childAreas = geoAreasByParentArea.getOrDefault(rootArea, Collections.emptySet());
            if (!CollectionUtils.isEmpty(childAreas)) {
                splitGeoAreasByDepth(childAreas, geoAreasByParentArea, geoAreasByDepth, depth + 1);
            }
        }
    }

    private void storeGeoAreasByDepth(Map<Integer, Set<GeoArea>> geoAreasByDepth, final LogSummary logSummary) {
        geoAreasByDepth.entrySet()
                .stream()
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .forEach(geoAreas -> {
                    logSummary.info("Storing level {}: {} geo areas", geoAreas.getKey(), geoAreas.getValue().size());
                    geoAreaRepository.saveAll(geoAreas.getValue());
                    geoAreaRepository.flush();
                    // It seems likely that new geo areas are not persisted fast enough in a mirrored database.
                    // If a just persisted geo area is queried immediately it can result in a "not found" exception.
                    // A small waiting time (delay) is required after each group of geo areas is stored to give the db
                    // time enough to distribute the new data to all replicas.
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });
    }

}
