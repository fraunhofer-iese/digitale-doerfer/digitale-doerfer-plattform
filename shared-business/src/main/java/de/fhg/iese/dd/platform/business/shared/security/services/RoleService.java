/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RelatedEntityIdMustNotBeNullException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RelatedEntityNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RoleAssignmentNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.RoleManagementNotAllowedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.exceptions.RoleNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.RoleAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommonHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommunityHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RelatedEntityIsNull;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

@Service
class RoleService extends BaseService implements IRoleService {

    @Autowired
    private RoleAssignmentRepository roleAssignmentRepository;

    @Autowired
    private Set<BaseRole<? extends NamedEntity>> roles;

    private final Map<Class<? extends Action<?>>, Action<?>> actionInstances = new HashMap<>();

    @Override
    public Set<BaseRole<? extends NamedEntity>> getAllRoles() {
        return Collections.unmodifiableSet(roles);
    }

    //this should rather be getRole(Class<? extends BaseRole<E>> roleClass), but the compiler cannot infer the nested type
    @SuppressWarnings("unchecked")
    @Override
    public <E extends NamedEntity> BaseRole<E> getRole(Class<?> roleClass) {
        return (BaseRole<E>) getRoleOptional(roleClass)
                .orElseThrow(() -> new RoleNotFoundException(roleClass));
    }

    @SuppressWarnings("unchecked")
    private <E extends NamedEntity> Optional<BaseRole<E>> getRoleOptional(Class<?> roleClass) {
        return roles.stream()
                .filter(r -> r.getClass().equals(roleClass))
                //we have to cast the role here, see comment on getRole above
                .map(role -> (BaseRole<E>) role)
                .findAny();
    }

    @Override
    public String getKeyForRole(Class<? extends BaseRole<? extends NamedEntity>> role) {
        return roles.stream()
                .filter(r -> r.getClass().equals(role))
                .map(BaseRole::getKey)
                .findAny()
                .orElseThrow(() -> new RoleNotFoundException(role));
    }

    @Override
    public BaseRole<? extends NamedEntity> getRoleForKey(String key) throws RoleNotFoundException {
        return roles.stream()
                .filter(r -> r.getKey().equals(key))
                .findFirst()
                .orElseThrow(() -> new RoleNotFoundException(key));
    }

    public List<RoleAssignment> getCurrentRoleAssignmentsUnchecked(Person person) {
        return roleAssignmentRepository.findByPersonOrderByCreatedDesc(person);
    }

    @Override
    public List<RoleAssignment> getCurrentExtendedRoleAssignments(Person person) {
        return filterAndExtend(getCurrentRoleAssignmentsUnchecked(person), Optional.empty());
    }

    @Override
    public Set<String> getRelatedEntityIdsOfRoleAssignments(Person person,
            Collection<Class<? extends BaseRole<?>>> roles) {
        return roleAssignmentRepository.findRelatedEntityIdByPersonAndRoleIn(person, roles);
    }

    @Override
    public List<RoleAssignment> getCurrentExtendedRoleAssignments(Person person, Tenant tenant) {
        return filterAndExtend(getCurrentRoleAssignmentsUnchecked(person), Optional.ofNullable(tenant));
    }

    @Override
    public <E extends NamedEntity> Optional<RoleAssignment> getCurrentRoleAssignmentForRoleAndEntity(Person person,
            Class<? extends BaseRole<E>> role, E relatedEntity) {
        Objects.requireNonNull(person);
        Objects.requireNonNull(role);
        Objects.requireNonNull(relatedEntity);
        return roleAssignmentRepository.findByPersonAndRoleAndRelatedEntityId(person, role, relatedEntity.getId());
    }

    @Override
    public <E extends NamedEntity> List<RoleAssignment> getCurrentRoleAssignmentsForEntity(Person person,
            E relatedEntity) {
        Objects.requireNonNull(person);
        Objects.requireNonNull(relatedEntity);
        return roleAssignmentRepository.findByPersonAndRelatedEntityIdOrderByCreatedDesc(person, relatedEntity.getId());
    }

    @Override
    public <E extends NamedEntity> List<RoleAssignment> getAllRoleAssignmentsForEntity(E relatedEntity) {
        Objects.requireNonNull(relatedEntity);
        return roleAssignmentRepository.findByRelatedEntityIdOrderByCreatedDesc(relatedEntity.getId());
    }

    @Override
    public <E extends NamedEntity> boolean hasRoleForEntity(Person person, Class<? extends BaseRole<E>> role,
            E relatedEntity) {
        return hasRoleForEntity(person, Collections.singleton(role), relatedEntity);
    }

    @Override
    public <E extends NamedEntity> boolean hasRoleForEntity(Person person,
            Collection<Class<? extends BaseRole<E>>> roles, E relatedEntity) {
        Objects.requireNonNull(person);
        Objects.requireNonNull(relatedEntity);
        return roleAssignmentRepository.existsByPersonAndRelatedEntityIdAndRoleIn(person, relatedEntity.getId(), roles);
    }

    @Override
    public boolean hasRoleForAnyEntity(Person person, Collection<Class<? extends BaseRole<?>>> roles) {
        Objects.requireNonNull(person);
        return roleAssignmentRepository.existsByPersonAndRoleIn(person, roles);
    }

    @Override
    public <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, Class<? extends BaseRole<E>> role,
            String relatedEntityId) {
        final BaseRole<E> roleInstance = getRole(role);
        if (roleInstance.hasNullEntity()) {
            return createRoleAssignmentIfNotExisting(person, null, role);
        }
        if (!roleInstance.existsRelatedEntity(relatedEntityId)) {
            throw new RelatedEntityNotFoundException(roleInstance.getRelatedEntityClass(), relatedEntityId);
        }
        return createRoleAssignmentIfNotExisting(person, relatedEntityId, role);
    }

    @Override
    public <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, BaseRole<E> roleInstance,
            String relatedEntityId) {
        @SuppressWarnings("unchecked") final Class<? extends BaseRole<E>> role =
                (Class<? extends BaseRole<E>>) roleInstance.getClass();
        return assignRoleToPerson(person, role, relatedEntityId);
    }

    @Override
    public <E extends NamedEntity> RoleAssignment assignRoleToPerson(Person person, Class<? extends BaseRole<E>> role,
            E relatedEntity) {
        return assignRoleToPerson(person, role, relatedEntity == null ? null : relatedEntity.getId());
    }

    private RoleAssignment createRoleAssignmentIfNotExisting(Person person, final String relatedEntityId,
            Class<? extends BaseRole<? extends NamedEntity>> role) {
        List<RoleAssignment> existingAssignments = roleAssignmentRepository
                .findByPersonAndRelatedEntityIdOrderByCreatedDesc(person, relatedEntityId);
        Optional<RoleAssignment> existingAssignment = getRoleForEntity(existingAssignments, relatedEntityId, role);
        if (existingAssignment.isEmpty()) {
            RoleAssignment newRoleAssignment = RoleAssignment.builder()
                    .person(person)
                    .role(role)
                    .relatedEntityId(relatedEntityId)
                    .build();
            return roleAssignmentRepository.saveAndFlush(newRoleAssignment);
        } else {
            return existingAssignment.get();
        }
    }

    private static Optional<RoleAssignment> getRoleForEntity(List<RoleAssignment> roleAssignments,
            String relatedEntityId, Class<? extends BaseRole<? extends NamedEntity>> role) {
        return roleAssignments.stream()
                .filter(r -> r.getRole().equals(role) &&
                        StringUtils.equals(r.getRelatedEntityId(), relatedEntityId))
                .min(Comparator.comparingLong(BaseEntity::getCreated));
    }

    @Override
    public void removeRoleAssignment(RoleAssignment roleAssignment) {
        roleAssignmentRepository.delete(roleAssignment);
    }

    @Override
    public RoleAssignment getRoleAssignment(String roleAssignmentId) throws RoleAssignmentNotFoundException {
        return roleAssignmentRepository.findById(roleAssignmentId)
                .orElseThrow(RoleAssignmentNotFoundException::new);
    }

    @Override
    public boolean existsPersonWithRole(Class<? extends BaseRole<? extends NamedEntity>> role) {
        // if such a roleAssignment exists there must be a person with that role
        return roleAssignmentRepository.existsByRole(role);
    }

    @Override
    public List<Person> getAllPersonsWithRole(Class<? extends BaseRole<? extends NamedEntity>> role) {
        return roleAssignmentRepository.findByRoleOrderByCreatedDesc(role);
    }

    @Override
    public Page<Person> findAllByAssignedRole(Class<? extends BaseRole<? extends NamedEntity>> role,
            Pageable page) {
        return roleAssignmentRepository.findAllByAssignedRole(role, page);
    }

    @Override
    public Page<Person> findAllByAssignedRoleAndBelongingToTenantIn(Class<? extends BaseRole<?>> role,
            Set<String> tenantIds, Pageable page) {
        return roleAssignmentRepository.findByHomeCommunityOrHomeAreaTenantIn(role, tenantIds, page);
    }

    @Override
    public <E extends NamedEntity> List<Person> getAllPersonsWithRoleForEntity(Class<? extends BaseRole<E>> role,
            E relatedEntity) {
        return roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(role, relatedEntity.getId());
    }

    @Override
    public <E extends NamedEntity> List<Person> getAllPersonsWithRoleForEntity(
            Collection<Class<? extends BaseRole<E>>> roles, E relatedEntity) {
        return roleAssignmentRepository.findByRoleInAndRelatedEntityIdOrderByCreatedDesc(roles, relatedEntity.getId());
    }

    @Override
    public Person getDefaultContactPersonForTenant(Tenant tenant) {
        RoleAssignment roleAssignment = roleAssignmentRepository.
                findFirstByRoleAndRelatedEntityIdOrderByCreatedDesc(CommunityHelpDesk.class, tenant.getId());
        if (roleAssignment != null) {
            return roleAssignment.getPerson();
        } else {
            roleAssignment = roleAssignmentRepository.findFirstByRoleOrderByCreatedDesc(CommonHelpDesk.class);
            if (roleAssignment != null) {
                return roleAssignment.getPerson();
            } else {
                return null;
            }
        }
    }

    @Override
    public void checkIfRelatedEntityIdCanBeNull(@NonNull @lombok.NonNull BaseRole<? extends NamedEntity> role,
            String relatedEntityId) throws RelatedEntityIdMustNotBeNullException {

        if (!RelatedEntityIsNull.class.equals(role.getRelatedEntityClass()) && relatedEntityId == null) {
            throw new RelatedEntityIdMustNotBeNullException(role);
        }
    }

    @Override
    public List<RoleAssignment> getExtendedRoleAssignmentsOfPersonForListingPermission(
            final PermissionTenantRestricted listPersonsPermission, Person person) {
        return getCurrentExtendedRoleAssignments(person)
                .stream()
                .filter(roleAssignment -> {
                    if (listPersonsPermission.isAllTenantsAllowed()) {
                        return true;
                    }
                    if (roleAssignment.getRelatedEntityId() == null ||
                            roleAssignment.getRelatedEntityTenant() == null) {
                        return false;
                    }
                    //the tenant must be one of the allowed tenants
                    return listPersonsPermission.isTenantAllowed(roleAssignment.getRelatedEntityTenant());
                })
                .collect(Collectors.toList());
    }

    @Override
    public <E extends NamedEntity> void checkIfPersonIsAllowedToCreateAssignmentOfRoleForEntity(Person person,
            BaseRole<E> role, String relatedEntityId)
            throws RelatedEntityNotFoundException, RoleManagementNotAllowedException {

        Objects.requireNonNull(role, "role parameter must not be null");

        final List<RoleAssignment> currentRoleAssignments = getCurrentExtendedRoleAssignments(person);
        final boolean personIsSuperAdmin = currentRoleAssignments.stream()
                .anyMatch(roleAssignment -> roleAssignment.getRole().equals(SuperAdmin.class));
        final boolean personIsGlobalUserAdmin = currentRoleAssignments.stream()
                .anyMatch(roleAssignment -> roleAssignment.getRole().equals(GlobalUserAdmin.class));

        //special case super admin role
        if (SuperAdmin.class.equals(role.getClass())) {
            if (!personIsSuperAdmin) {
                throw new RoleManagementNotAllowedException(role, relatedEntityId,
                        "Only super admins are allowed to manage the super admin role.");
            }
            return;
        }

        //special case global user admin
        if (GlobalUserAdmin.class.equals(role.getClass())) {
            if (!personIsGlobalUserAdmin && !personIsSuperAdmin) {
                throw new RoleManagementNotAllowedException(role, relatedEntityId,
                        "Only global user admins and super admins are allowed to manage " +
                                "the global user admin role.");
            }
            return;
        }

        //other cases where there is no related entity
        if (role.getRelatedEntityClass().equals(RelatedEntityIsNull.class)) {
            if (!personIsGlobalUserAdmin && !personIsSuperAdmin) {
                throw new RoleManagementNotAllowedException(role, relatedEntityId,
                        "Only global user admins and super admins are allowed to manage " +
                                "roles with no related entity");
            }
            return;
        }

        //Since the above check assures that !role.getRelatedEntityClass().equals(RelatedEntityIsNull.class)
        //the relatedEntityId should not be null. We check this in order to fail with a meaningful exception
        if (relatedEntityId == null) {
            throw new RelatedEntityIdMustNotBeNullException(role);
        }

        //get tenants for related entity
        final E relatedEntity = role.getRelatedEntityById(relatedEntityId);
        if (relatedEntity == null) {
            throw new RelatedEntityNotFoundException(role.getRelatedEntityClass(), relatedEntityId);
        }
        final Tenant tenant = role.getTenantOfRelatedEntity(relatedEntity);

        //super admins and global user admins are allowed to manage all roles for every entity
        if (personIsSuperAdmin || personIsGlobalUserAdmin) {
            return;
        }

        //check that the person is role manager for the related tenant
        if (currentRoleAssignments.stream()
                .noneMatch(ra -> (ra.getRole().equals(RoleManager.class) || ra.getRole().equals(UserAdmin.class)) &&
                        tenant != null && tenant.getId().equals(ra.getRelatedEntityId()))) {
            throw new RoleManagementNotAllowedException(role, relatedEntityId,
                    "the person is not role manager or user admin for tenant " + tenant);
        }
    }

    @Override
    public void checkIfPersonIsAllowedToDeleteRoleAssignment(Person person, RoleAssignment roleAssignment) {

        final Optional<BaseRole<NamedEntity>> optionalRole = getRoleOptional(roleAssignment.getRole());
        if (optionalRole.isEmpty()) {
            //if the role has been deleted, it is ok to delete the assignment independent of the person's permission
            log.warn("Role {} cannot be found. Allowing deletion of role assignment.",
                    roleAssignment.getRole().getName());
            return;
        }

        final BaseRole<NamedEntity> role = optionalRole.get();
        final String relatedEntityId = roleAssignment.getRelatedEntityId();

        if (!RelatedEntityIsNull.class.equals(role.getRelatedEntityClass()) &&
                (relatedEntityId == null || !role.existsRelatedEntity(relatedEntityId))
        ) {
            //role assignment is in inconsistent state -> allow deletion
            log.warn("{} expects related entity, but points to null or non-existing related entity. " +
                    "Allowing deletion this role assignment.", roleAssignment);
            return;
        }

        //otherwise, the same rules apply as for role assignment creation
        checkIfPersonIsAllowedToCreateAssignmentOfRoleForEntity(person, role, relatedEntityId);
    }

    @Override
    public <P extends Permission> P decidePermission(Class<? extends Action<P>> actionClass, Person person) {
        Action<P> action = getCachedOrCreateAction(actionClass);
        List<RoleAssignment> roleAssignments = getCurrentRoleAssignmentsUnchecked(person);
        return action.decidePermission(roleAssignments);
    }

    @Override
    public <P extends Permission> P decidePermissionAndThrowNotAuthorized(Class<? extends Action<P>> actionClass,
            Person person) {
        P permission = decidePermission(actionClass, person);
        if (permission.isDenied()) {
            throw new NotAuthorizedException("Action {} is denied for {}", actionClass.getSimpleName(), person.getId());
        }
        return permission;
    }

    @Override
    public <A extends Action<?>> A getAction(Class<A> actionClass) {
        return getCachedOrCreateAction(actionClass);
    }

    /**
     * Gets the cached action in {@link #actionInstances} or creates a new one via reflection.
     */
    private <A extends Action<?>> A getCachedOrCreateAction(Class<A> actionClass) {
        @SuppressWarnings("unchecked")
        A action = (A) actionInstances.get(actionClass);
        if (action == null) {
            try {
                Constructor<A> constructor = actionClass.getConstructor();
                action = constructor.newInstance();
                actionInstances.put(actionClass, action);
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException("No default constructor for action " + actionClass + " available");
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                throw new IllegalStateException(
                        "Default constructor for action " + actionClass + " threw exception: " + e.getMessage(), e);
            }
        }
        return action;
    }

    /**
     * Filters out invalid assignments and sets the relatedEntity and relatedEntityTenant attributes.
     *
     * @param roleAssignments
     * @param tenant          if present, only assignments that have no related entity and assignments whose related
     *                        entity belongs to this tenant are returned
     *
     * @return
     */
    private List<RoleAssignment> filterAndExtend(List<RoleAssignment> roleAssignments, Optional<Tenant> tenant) {
        final List<RoleAssignment> roleAssignmentsToReturn = new ArrayList<>(roleAssignments.size());
        for (RoleAssignment roleAssignment : roleAssignments) {
            filterAndExtend(roleAssignment, tenant, roleAssignmentsToReturn::add,
                    warning -> log.warn(warning + " Entity is filtered out."));
        }
        return roleAssignmentsToReturn;
    }

    @Override
    public void extend(RoleAssignment roleAssignment) {
        filterAndExtend(roleAssignment,
                Optional.empty(), //no tenant filter
                ra -> { //no particular extra handling - just have the the role assigment be extended
                },
                errorString -> {
                    throw new IllegalStateException(errorString);
                });
    }

    /**
     * Extends role assignments by setting relatedEntity and relatedEntityTenant attributes. For every valid role
     * assignment, the validRoleAssignmentConsumer is called. For every invalid role assigment (e.g. related entitiy not
     * existing), errorStringHandler with a descriptive error is invoked.
     *
     * @param roleAssignment
     * @param tenant                      if present, validRoleAssignmentConsumer is only called if the role assignment
     *                                    has no related entity or if the related entity belongs to this tenant
     * @param validRoleAssignmentConsumer
     * @param errorStringHandler
     */
    private void filterAndExtend(RoleAssignment roleAssignment,
            Optional<Tenant> tenant,
            Consumer<RoleAssignment> validRoleAssignmentConsumer,
            Consumer<String> errorStringHandler) {

        //filter out inconsistent role assignments
        final Optional<BaseRole<NamedEntity>> optionalRole = getRoleOptional(roleAssignment.getRole());
        if (optionalRole.isEmpty()) {
            errorStringHandler.accept(roleAssignment + " is invalid: Role does not exist.");
            return;
        }
        final BaseRole<NamedEntity> role = optionalRole.get();
        final boolean roleHasRelatedEntity = !role.getRelatedEntityClass().equals(RelatedEntityIsNull.class);
        if (!roleHasRelatedEntity) {
            //role has no related entity
            validRoleAssignmentConsumer.accept(roleAssignment);
            return;
        }
        final String relatedEntityId = roleAssignment.getRelatedEntityId();
        if (relatedEntityId == null) {
            errorStringHandler.accept(roleAssignment + " is invalid: Expecting non-null related entity ID.");
            return;
        }
        final NamedEntity relatedEntity = role.getRelatedEntityById(relatedEntityId);
        if (relatedEntity == null) {
            errorStringHandler.accept(
                    roleAssignment + " is invalid: Related entity is not existing.");
            return;
        }
        roleAssignment.setRelatedEntity(relatedEntity);

        final Tenant tenantOfRelatedEntity = role.getTenantOfRelatedEntity(relatedEntity);
        roleAssignment.setRelatedEntityTenant(tenantOfRelatedEntity);

        //optionally apply tenant filter
        if (tenant.map(t -> t.equals(tenantOfRelatedEntity)).orElse(true)) {
            validRoleAssignmentConsumer.accept(roleAssignment);
        }
    }

    @Override
    public <E extends NamedEntity> void deleteAllRoleAssignmentsForRoleAndEntity(
            Class<? extends BaseRole<E>> role, E relatedEntity) {
        roleAssignmentRepository.deleteByRoleAndRelatedEntityId(role, relatedEntity.getId());
    }

}
