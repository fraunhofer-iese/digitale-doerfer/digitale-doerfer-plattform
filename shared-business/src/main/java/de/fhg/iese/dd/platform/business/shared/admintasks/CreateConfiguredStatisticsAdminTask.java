/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.StatisticsCreationFeature;

@Component
public class CreateConfiguredStatisticsAdminTask extends BaseAdminTask {

    @Autowired
    private IStatisticsService statisticsService;

    @Override
    public String getName() {
        return "CreateConfiguredStatistics";
    }

    @Override
    public String getDescription() {
        return "Creates the configured statistics as defined via feature " +
                StatisticsCreationFeature.class.getSimpleName() + ".";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {
        logSummary.info("Creating configured statistics...");
        List<StatisticsReportDefinition> createdReports =
                statisticsService.createConfiguredStatisticsReports();
        logSummary.info("Finished creating {} configured statistics", createdReports.size());
    }

}
