/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics.services;

import java.time.temporal.ChronoUnit;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReport;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.exceptions.StatisticsReportNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;

public interface IStatisticsService extends IService {

    StatisticsReport generateStatisticsReport(StatisticsReportDefinition statisticsDefinition);

    String reportToCsv(StatisticsReport report, String separator);

    String reportStatisticsMetadataToCsv(StatisticsReport report, String separator);

    String reportToText(StatisticsReport report);

    List<StatisticsReportDefinition> getConfiguredReportDefinitions(int maxOffset);

    List<StatisticsReportDefinition> createConfiguredStatisticsReports();

    boolean saveStatisticsReports(StatisticsReport statisticsReport);

    byte[] getStatisticsReport(String fileName) throws StatisticsReportNotFoundException, FileStorageException;

    long getReportStartTimeNew(int offsetFromCurrentFullUnit, ChronoUnit unit);

    long getReportStartTimeActive(int offsetFromCurrentFullUnit, ChronoUnit unit);

    long getReportEndTime(ChronoUnit unit);

    String getReportName(String prefix, int offsetFromCurrentFullUnit, ChronoUnit unit);

}
