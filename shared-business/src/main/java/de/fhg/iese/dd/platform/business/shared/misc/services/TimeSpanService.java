/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

@Service
class TimeSpanService extends BaseService implements ITimeSpanService {

    @Override
    public TimeSpan subtractMinutes(TimeSpan timeSpan, long minutes) {

        long milli = TimeUnit.MINUTES.toMillis(minutes);
        return new TimeSpan(timeSpan.getStartTime() - milli, timeSpan.getEndTime() - milli);

    }

    @Override
    public TimeSpan overlap(TimeSpan span1, TimeSpan span2) {

        final long start1 = span1.getStartTime();
        final long end1 = span1.getEndTime();
        final long start2 = span2.getStartTime();
        final long end2 = span2.getEndTime();

        // ensure non-0 spans
        if (start1 == end1) {
            return null;
        }
        if (start2 == end2) {
            return null;
        }

        if ((start1 < end2) && (start2 < end1)) {
            final long start = Math.max(start1, start2);
            final long end = Math.min(end1, end2);
            return new TimeSpan(start, end);
        } else {
            return null;
        }
    }

    @Override
    public TimeSpan round(TimeSpan span, int minutes, RoundingType type) {
        TimeSpan result = new TimeSpan();

        result.setStartTime(roundTimeToMinutes(span.getStartTime(), minutes, type));
        result.setEndTime(roundTimeToMinutes(span.getEndTime(), minutes, type));

        return result;
    }

    @Override
    public long roundTimeToMinutes( long time, int minutes, ITimeSpanService.RoundingType type) {

        // minutes to millis
        long roundMilli = TimeUnit.MINUTES.toMillis(minutes);

        long rest = time % roundMilli;

        // round down
        long down = time - rest;

        // round up
        long up = time - rest + roundMilli;

        // round
        double fraction = ( rest / (double) roundMilli );
        long round = ( fraction < 0.5d ) ? down : up;

        switch ( type ) {
            case ROUND:
                return round;
            case DOWN:
                return down;
            case UP:
                return up;
            default:
                return time;
        }

    }

}
