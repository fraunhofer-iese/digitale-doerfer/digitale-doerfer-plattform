/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;

@Component
public class AddressDataInitializer extends BaseDataInitializer {

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("address")
                .processedEntity(Address.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        List<Address> entities = loadAllEntities(Address.class);

        if (CollectionUtils.isEmpty(entities)) {
            logSummary.error("No data json found, skipping creation!");
            return;
        }

        for (Address address : entities) {
            checkId(address);
            // created
            address = adjustCreated(address);
            // save
            address = addressRepository.save(address);
            logSummary.info("created address '{}' : '{}'", address.getId(), address.getName());
        }
    }

}
