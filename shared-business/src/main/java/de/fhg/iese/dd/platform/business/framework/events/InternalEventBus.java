/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;

import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
class InternalEventBus {

    /**
     * Only set to true when debugging internal event handling
     */
    private static final boolean LOG_INTERNAL_DEBUG_MESSAGES = false;

    /**
     * Consumers of events on the bus
     * <p>
     * registered event tye | consumer
     */
    private final Multimap<Class<? extends BaseEvent>, EventConsumer> eventConsumers = ArrayListMultimap.create();

    /**
     * "Temporary" consumers of events that are waiting for a reply of a specific type in a specific event chain
     * <p>
     * eventChainIdentifier | expectedEventType | consumer
     */
    private final Table<EventBusAccessor.EventChainIdentifier, Class<? extends BaseEvent>, Consumer<BaseEvent>>
            replyConsumers = HashBasedTable.create();

    /**
     * Simple thread-safe counter for number of events that are currently being processed. This includes also the event
     * distribution, not only the bare processing in the event processors.
     */
    private final AtomicInteger currentEventsInProcessing = new AtomicInteger(0);

    /**
     * Shared scheduled executor service for sending events with delay, used by {@link EventBusAccessor}.
     */
    private ScheduledExecutorService scheduledExecutorService;

    private ExecutorService asyncExecutorService;

    /**
     * Internal data structure (more or less just a typed pair) to keep the mapping of event consumer and according
     * execution strategy.
     */
    @AllArgsConstructor
    @Getter
    private static class EventConsumer {

        Consumer<BaseEvent> consumer;
        EventExecutionStrategy executionStrategy;

    }

    /**
     * Register an event consumer for a specific event type.
     *
     * @param expectedEventType
     *         type of the event to consume
     * @param consumer
     *         consumer that will be triggered if the event of that type occurs
     * @param executionStrategy
     *         execution strategy to be applied when the consumer is processed
     * @param <E>
     *         type of the event
     */
    <E extends BaseEvent> void registerEventTypeConsumer(final Class<E> expectedEventType, Consumer<E> consumer,
            final EventExecutionStrategy executionStrategy) {
        @SuppressWarnings("unchecked")
        EventConsumer eventConsumer = new EventConsumer((Consumer<BaseEvent>) consumer,
                //AUTO should not be used here, but to avoid any errors we translate it to SYNCHRONOUS_PREFERRED
                //AUTO is only used for sorting out the annotation values if they get overridden
                executionStrategy == EventExecutionStrategy.AUTO ?
                        EventExecutionStrategy.SYNCHRONOUS_PREFERRED :
                        executionStrategy);
        eventConsumers.put(expectedEventType, eventConsumer);
    }

    /**
     * Register a temporary event consumer that should only be triggered if the event of that type occurs in the given
     * event chain. If there already was a registration for the same event type in the event chain an {@link
     * IllegalStateException} is thrown.
     *
     * @param eventChainIdentifier
     *         identifier of the event chain that should contain the event
     * @param expectedEventType
     *         type of event to consume
     * @param consumer
     *         consumer that will be triggered if the event of that type occurs in the given event chain
     */
    void registerReplyConsumer(EventBusAccessor.EventChainIdentifier eventChainIdentifier,
            Class<? extends BaseEvent> expectedEventType,
            Consumer<BaseEvent> consumer) {
        Consumer<BaseEvent> previousConsumer = replyConsumers.put(eventChainIdentifier, expectedEventType, consumer);
        if (previousConsumer != null) {
            //this can not really happen, in this case a second wait would be initiated within the same event chain
            throw new IllegalStateException("Unexpected double registration of events: " + expectedEventType.getName());
        }
    }

    /**
     * Unregister a temporary event consumer. If it was not registered, no exception is thrown.
     *
     * @param eventChainIdentifier
     *         identifier of the event chain the consumer was registered for
     * @param expectedEvent
     *         type of event the consumer was registered for
     */
    void unregisterReplyConsumer(EventBusAccessor.EventChainIdentifier eventChainIdentifier,
            Class<? extends BaseEvent> expectedEvent) {
        replyConsumers.remove(eventChainIdentifier, expectedEvent);
    }

    /**
     * Send out an event to all registered consumers, both permanent for that type or temporary for that event chain.
     * The consumers are only executed synchronous if synchronousAllowed is true. In that case each consumers execution
     * strategy is checked to decide if it is possible to execute one of the consumers in a synchronous way.
     *
     * @param event              event to distribute
     * @param synchronousAllowed if true one of the consumers can be executed synchronous, depending on its execution
     *                           strategy and the number of consumers
     *
     * @return number of consumers
     */
    int notify(final BaseEvent event, boolean synchronousAllowed) {
        signalEventProcessingStart();
        try {
            int numConsumers = 0;
            final Class<? extends BaseEvent> eventType = event.getClass();
            //check if there is a temporary consumer waiting for that reply
            Consumer<BaseEvent> replyConsumer =
                    replyConsumers.get(event.getEventRoutingKey().getEventChainIdentifier(), eventType);
            //the reply consumers get notified first, so that they can finish what they were waiting for
            if (replyConsumer != null) {
                numConsumers++;
                if (LOG_INTERNAL_DEBUG_MESSAGES) {
                    log.trace("async reply " + event.getClass().getName());
                }
                //execute the reply asynchronous
                getAsyncExecutorService().submit(() -> replyConsumer.accept(event));
                //when there is a reply consumer we want the current thread to work on handling the reply as soon as possible
                synchronousAllowed = false;
            }
            //get all consumers that permanently registered for that event type
            Collection<EventConsumer> eventTypeConsumers = eventConsumers.get(eventType);
            numConsumers += eventTypeConsumers.size();
            //distribute the event to all these consumers
            executeConsumers(eventTypeConsumers, event, synchronousAllowed);
            return numConsumers;
        } finally {
            signalEventProcessingFinish();
        }
    }

    /**
     * Execute the consumers for the given event. The consumers are only executed synchronous if synchronousAllowed is
     * true. In that case each consumers execution strategy is checked to decide if it is possible to execute one of the
     * consumers in a synchronous way.
     *
     * @param consumers
     *         consumers to execute
     * @param event
     *         event to distribute
     * @param synchronousAllowed
     *         if true one of the consumers can be executed synchronous, depending on its execution strategy and the
     *         number of consumers
     */
    private void executeConsumers(final Collection<EventConsumer> consumers, final BaseEvent event,
            boolean synchronousAllowed) {

        if (CollectionUtils.isEmpty(consumers)) {
            return;
        }
        if (synchronousAllowed) {
            //split up the consumers by execution strategy
            Map<EventExecutionStrategy, List<EventConsumer>> consumersByExecutionStrategy = consumers.stream()
                    .collect(Collectors.groupingBy(EventConsumer::getExecutionStrategy));
            List<EventConsumer> consumersAsynchronousRequired =
                    consumersByExecutionStrategy.getOrDefault(EventExecutionStrategy.ASYNCHRONOUS_REQUIRED,
                            Collections.emptyList());
            List<EventConsumer> consumersAsynchronousPreferred =
                    consumersByExecutionStrategy.getOrDefault(EventExecutionStrategy.ASYNCHRONOUS_PREFERRED,
                            Collections.emptyList());
            List<EventConsumer> consumersSynchronousPreferred =
                    consumersByExecutionStrategy.getOrDefault(EventExecutionStrategy.SYNCHRONOUS_PREFERRED,
                            Collections.emptyList());

            //the asynchronous required are always executed asynchronous
            executeAllConsumerAsynchronous(consumersAsynchronousRequired, event);
            if (consumersSynchronousPreferred.size() == 1) {
                //we only have one synchronous consumer, we can execute it synchronous
                executeFirstConsumerSynchronous(consumersSynchronousPreferred, event);
                //all others are executed asynchronous
                executeAllConsumerAsynchronous(consumersAsynchronousPreferred, event);
            } else if (consumersSynchronousPreferred.size() == 0 && consumersAsynchronousPreferred.size() == 1) {
                //we don't have a synchronous consumer, but only one asynchronous preferred that can be executed synchronous
                executeFirstConsumerSynchronous(consumersAsynchronousPreferred, event);
            } else {
                //there are too many consumers, we have to execute them all asynchronous
                executeAllConsumerAsynchronous(consumersSynchronousPreferred, event);
                executeAllConsumerAsynchronous(consumersAsynchronousPreferred, event);
            }
        } else {
            //no decision anyway, all of them have to be executed asynchronous
            executeAllConsumerAsynchronous(consumers, event);
        }
    }

    private void executeFirstConsumerSynchronous(final Collection<EventConsumer> eventConsumers,
            final BaseEvent event) {
        if (CollectionUtils.isEmpty(eventConsumers)) {
            return;
        }
        try {
            if (LOG_INTERNAL_DEBUG_MESSAGES) {
                log.trace("+++sync proc " + event.getClass().getName());
            }
            eventConsumers.iterator().next().getConsumer().accept(event);
        } catch (Exception e) {
            //this exception should never be caused, because the consumer catches every exception
            log.error("Exception while consuming event " + event.getClass() + " synchronous", e);
        }
    }

    @SuppressFBWarnings(value = "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE",
            justification = "The futures of these event consumers are not needed")
    private void executeAllConsumerAsynchronous(final Collection<EventConsumer> eventConsumers, final BaseEvent event) {
        if (CollectionUtils.isEmpty(eventConsumers)) {
            return;
        }
        if (LOG_INTERNAL_DEBUG_MESSAGES) {
            log.trace("---async proc " + eventConsumers.size() + " " + event.getClass().getName());
        }
        //execute them in parallel
        final ExecutorService asyncExecutorService = getAsyncExecutorService();
        for (EventConsumer c : eventConsumers) {
            asyncExecutorService.submit(() -> c.getConsumer().accept(event), asyncExecutorService);
        }
    }

    void signalEventProcessingStart() {
        currentEventsInProcessing.incrementAndGet();
    }

    void signalEventProcessingFinish() {
        currentEventsInProcessing.decrementAndGet();
    }

    boolean areEventsInProcessing() {
        return currentEventsInProcessing.get() > 0;
    }

    private ExecutorService getAsyncExecutorService() {
        if (asyncExecutorService == null) {
            final int availableCores = Runtime.getRuntime().availableProcessors();
            //this is just a guess about a useful number of threads
            int numThreads = availableCores * 4;
            //expected workload: smaller number of potentially blocking tasks
            asyncExecutorService =
                    Executors.newFixedThreadPool(numThreads, new CustomizableThreadFactory("event-async-"));
            log.debug("Using {} threads on {} cores in async event processing pool", numThreads, availableCores);
        }
        return asyncExecutorService;
    }

    ScheduledExecutorService getScheduledExecutorService() {
        if (scheduledExecutorService == null) {
            scheduledExecutorService =
                    new ScheduledThreadPoolExecutor(1, new CustomizableThreadFactory("event-scheduled-"));
        }
        return scheduledExecutorService;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @PreDestroy
    private void shutdownExecutor() {
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
        if (asyncExecutorService != null) {
            asyncExecutorService.shutdown();
        }
        if (scheduledExecutorService != null) {
            try {
                scheduledExecutorService.awaitTermination(20, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            scheduledExecutorService.shutdownNow();
        }
        if (asyncExecutorService != null) {
            try {
                asyncExecutorService.awaitTermination(20, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            asyncExecutorService.shutdownNow();
        }
    }

}
