/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.datadeletion;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class PersonDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private PersonRepository personRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        .changedOrDeletedEntity(Person.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        .changedOrDeletedEntity(Person.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        long personsImpacted = personRepository.countByHomeArea(geoAreaToBeDeleted);
        Tenant tenant = geoAreaToBeUsedInstead.getTenant();
        if (tenant == null) {
            logSummary.info("{} persons get new home area", personsImpacted);
        } else {
            logSummary.info("{} persons get new home area and new tenant {}", personsImpacted, tenant);
        }
        if (personsImpacted > 0 && tenant == null) {
            logSummary.warn("The geo area to be used instead has no tenant! The persons will keep their old tenant.");
        }
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        logSummary.info("Updating persons with new home area and new tenant");
        Tenant tenant = geoAreaToBeUsedInstead.getTenant();
        int personsUpdated;
        if (tenant == null) {
            logSummary.warn("The geo area to be used instead has no tenant! The persons will keep their old tenant.");
            personsUpdated = personRepository.updateHomeArea(geoAreaToBeDeleted, geoAreaToBeUsedInstead);
            logSummary.info("{} persons got a new home area", personsUpdated);
        } else {
            personsUpdated = personRepository.updateHomeAreaAndTenant(geoAreaToBeDeleted, geoAreaToBeUsedInstead,
                    tenant);
            logSummary.info("{} persons got a new home area and new tenant {}", personsUpdated, tenant);
        }
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        long personsImpacted = personRepository.countByCommunity(tenantToBeDeleted);
        logSummary.info("{} persons get new tenant", personsImpacted);
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {

        logSummary.info("Updating persons with new tenant");
        int personsImpacted = personRepository.updateTenant(tenantToBeDeleted, tenantToBeUsedInstead);
        logSummary.info("{} persons got new tenant", personsImpacted);

    }

}
