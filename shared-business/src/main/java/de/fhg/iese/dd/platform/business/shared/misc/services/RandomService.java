/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2021 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.Getter;

@Service
@SuppressFBWarnings(value = "DMI_RANDOM_USED_ONLY_ONCE",
        justification = "Caching of the Random instance is not detected")
class RandomService extends BaseService implements IRandomService {

    private static final char[] PASSWORD_LETTERS =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-()[]".toCharArray();

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final Random random = new Random();

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final SecureRandom secureRandom = new SecureRandom();

    @Override
    public GPSLocation randomGPSLocation() {
        GPSLocation location = new GPSLocation();
        location.setLatitude(randomDouble(49.558068, 49.564638, 6));
        location.setLongitude(randomDouble(8.055468, 8.080659, 6));
        return location;
    }

    @Override
    public TimeSpan randomTimeSpan(){
        int start = getRandom().nextInt(48);
        return new TimeSpan(timeStamp(start), timeStamp(start + getRandom().nextInt(3)));
    }

    @Override
    public String randomAlphabetic(String name, int min, int max) {
        int pre = name.length() + 1;
        if (pre >= max) {
            return name.substring(0, max);
        } else {
            min = Math.max(min - pre, 0);
            max = Math.max(max - pre, 0);
            return name + "." + RandomStringUtils.randomAlphabetic((randomInt(min, max)));
        }
    }

    @Override
    public double randomDouble(double min, double max, int places){
        //noinspection OptionalGetWithoutIsPresent
        double randomDouble = getRandom().doubles(min, max + 1d).findFirst().getAsDouble();
        return round(randomDouble, places);
    }

    @Override
    public long randomTimeStamp(long min, long max){
        return randomLong(min, max);
    }

    @Override
    public long randomLong(long min, long max){
        //noinspection OptionalGetWithoutIsPresent
        return getRandom().longs(min, max + 1L).findFirst().getAsLong();
    }

    @Override
    public int randomInt(int min, int max){
        //noinspection OptionalGetWithoutIsPresent
        return getRandom().ints(min, max + 1).findFirst().getAsInt();
    }

    @Override
    public String randomNumberString(int length){
        StringBuilder randomResult = new StringBuilder();
        while(randomResult.length() < length){
            randomResult.append(getRandom().nextInt(10));
        }
        return randomResult.toString();
    }

    private long timeStamp(int futureHours){
        return timeService.currentTimeMillisUTC() + TimeUnit.HOURS.toMillis(futureHours);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public boolean randomBoolean(double probability){
        double r = getRandom().nextDouble();
        return r > 1 - probability;
    }

    @Override
    public <T> T randomElement(List<T> elements){
        if(!elements.isEmpty()){
            return elements.get(randomInt(0, elements.size()-1));
        }
        return null;
    }

    @Override
    public String randomPassword(int passwordLength) {
        final StringBuilder pw = new StringBuilder();
        for (int i = 0; i < passwordLength; i++) {
            pw.append(PASSWORD_LETTERS[getSecureRandom().nextInt(PASSWORD_LETTERS.length)]);
        }
        return pw.toString();
    }

    @Override
    public String randomPassword() {
        return randomPassword(DEFAULT_PASSWORD_LENGTH);
    }

}
