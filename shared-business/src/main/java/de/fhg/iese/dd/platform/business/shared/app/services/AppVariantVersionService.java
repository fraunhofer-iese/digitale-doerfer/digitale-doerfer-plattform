/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheChecker;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerIncrementalCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
class AppVariantVersionService extends BaseEntityService<AppVariantVersion> implements IAppVariantVersionService {

    private static final String GENERIC_UPDATE_URL = "https://www.digitale-doerfer.de/";

    private Map<String, List<AppVariantVersion>> appVariantVersionByAppVariantIdentifier;

    @Autowired
    private AppVariantVersionRepository appVariantVersionRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private CacheCheckerIncrementalCache cacheCheckerAppVariantVersionByAppVariantIdentifier;

    @PostConstruct
    private void initialize() {
        //this cache needs to be concurrent, since it is incrementally filled with new values
        appVariantVersionByAppVariantIdentifier = new ConcurrentHashMap<>();
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return ICachingComponent.CacheConfiguration.builder()
                .cachedEntity(AppVariantVersion.class)
                .build();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<AppVariantVersion> findAllAppVariantVersions(AppVariant appVariant) {
        return appVariantVersionRepository.findAllByAppVariantOrderByCreatedDesc(appVariant);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<AppVariantVersion> findAllAppVariantVersions() {
        return appVariantVersionRepository.findAll();
    }

    @Override
    public List<String> getHumanReadableAppVariantVersions() {

        Collection<AppVariantVersion> appVariantVersions = findAllAppVariantVersions();

        List<AppVariant> appVariantsWithoutVersion =
                appVariantRepository.findAllWithoutAppVariantVersionOrderByCreatedDesc();

        Comparator<AppVariantVersion> appVersionComparator = Comparator
                .comparing(AppVariantVersion::getAppVariant, Comparator.comparing(a -> a.getApp().getAppIdentifier()))
                .thenComparing(AppVariantVersion::getAppVariant,
                        Comparator.comparing(AppVariant::getAppVariantIdentifier))
                .thenComparing(AppVariantVersion::getVersionIdentifier);

        return Stream.concat(
                appVariantsWithoutVersion.stream()
                        .sorted(Comparator.comparing(AppVariant::getAppVariantIdentifier))
                        .map(av -> String.format("!No versions for: '%s' '%s' '%s'",
                                av.getApp().getAppIdentifier(),
                                av.getAppVariantIdentifier(),
                                av.getName())),
                appVariantVersions.stream()
                        .sorted(appVersionComparator)
                        .filter(AppVariantVersion::isSupported)
                        .map(av -> String.format("'%s' '%s' '%s' on %s in version [%s.*]",
                                av.getAppVariant().getApp().getAppIdentifier(),
                                av.getAppVariant().getAppVariantIdentifier(),
                                av.getAppVariant().getName(),
                                av.getPlatform().toString(),
                                av.getVersionIdentifier())))
                .collect(Collectors.toList());
    }

    @Override
    public CheckVersionResult checkAppVariantVersionSupport(String appVariantIdentifier,
            String appVariantVersionIdentifier, AppPlatform platform, String osversion, String device) {

        CheckVersionResult result;
        List<AppVariantVersion> potentialAppVersions =
                getAppVariantVersionsByAppVariantIdentifier(appVariantIdentifier);

        if (!CollectionUtils.isEmpty(potentialAppVersions)) {
            //app and platform are known
            List<AppVariantVersion> matchingVersions = potentialAppVersions.stream()
                    .filter(av -> av.getPlatform().equals(platform))
                    .filter(av -> av.matchesMajorMinor(appVariantVersionIdentifier))
                    .collect(Collectors.toList());
            if (!matchingVersions.isEmpty()) {
                //app, platform, major, minor match
                if (matchingVersions.size() > 1) {
                    //instead of just taking the first one we could also use the bugfix version number, if it exists
                    log.warn("Found more than one AppVariantVersion ({}), taking first one. Check configuration!",
                            matchingVersions.toString());
                }
                AppVariantVersion appVariantVersion = matchingVersions.get(0);
                if (appVariantVersion.isSupported()) {
                    //app, platform, major, minor match and we support it
                    result = createSuccessResult();
                } else {
                    //app, platform, major, minor match and we explicitly not support it
                    result = createFailedResultUpdate(appVariantVersion.getAppVariant(), platform);
                }
            } else {
                //app, platform match, but we can not find any version
                //we can take any of the potential versions to get the app variant, since they all have the same appVariantIdentifier
                result = createFailedResultUpdate(potentialAppVersions.get(0).getAppVariant(), platform);
            }
        } else {
            //unknown app
            result = createFailedResultUnknown(appVariantIdentifier);
        }
        String paramLogMessage = String.format("Checking %s:%s on %s(%s,%s)",
                appVariantIdentifier, appVariantVersionIdentifier, platform.name(), osversion, device);

        if (result.isSupported()) {
            result.setLogMessage(paramLogMessage + ": supported");
        } else {
            result.setLogMessage(paramLogMessage + ": not supported: " + result.getText());
        }
        return result;
    }

    private CheckVersionResult createSuccessResult() {
        return CheckVersionResult.builder()
                .supported(true)
                .text("App variant version is supported by this platform version (" +
                        getConfig().getVersionInfo().getPlatformVersion() + ")!")
                .url(null)
                .build();
    }

    private CheckVersionResult createFailedResultUnknown(final String appVariantIdentifier) {
        return CheckVersionResult.builder()
                .supported(false)
                .text("App variant '" + appVariantIdentifier + "' is not known by this platform version (" +
                        getConfig().getVersionInfo().getPlatformVersion() + ")!")
                .url(null)
                .build();
    }

    private String getUpdateURL(final AppVariant appVariant, final AppPlatform platform) {
        switch (platform) {
            case ANDROID:
                return appVariant.getUpdateUrlAndroid();
            case IOS:
                return appVariant.getUpdateUrlIOS();
            case WEB:
                return "";
        }
        return null;
    }

    private CheckVersionResult createFailedResultUpdate(final AppVariant appVariant, final AppPlatform platform) {

        final String updateURL = Optional.of(appVariant)
                .map(variant -> getUpdateURL(variant, platform))
                .orElse(GENERIC_UPDATE_URL);

        return CheckVersionResult.builder()
                .supported(false)
                .text("Es ist eine neue Version der App verfügbar! "
                        + "Die aktuelle Version der App kann leider nicht mehr verwendet werden.")
                .url(updateURL)
                .build();
    }

    private List<AppVariantVersion> getAppVariantVersionsByAppVariantIdentifier(String appVariantIdentifier) {

        cacheCheckerAppVariantVersionByAppVariantIdentifier
                .clearCacheIfStale(() -> CacheChecker.clearIfNotNull(appVariantVersionByAppVariantIdentifier));

        return appVariantVersionByAppVariantIdentifier.computeIfAbsent(appVariantIdentifier,
                //if it is not in the cache, get it from db
                query(() -> appVariantVersionRepository::findAllByAppVariantAppVariantIdentifier));
    }

    @Override
    public void invalidateCache() {
        cacheCheckerAppVariantVersionByAppVariantIdentifier.resetCacheChecks();
    }

}
