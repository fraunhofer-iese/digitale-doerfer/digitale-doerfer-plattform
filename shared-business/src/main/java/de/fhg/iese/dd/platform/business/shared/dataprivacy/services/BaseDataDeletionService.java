/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public abstract class BaseDataDeletionService {

    protected final Logger log = LogManager.getLogger(this.getClass());

    public static String randomUniqueString() {
        return UUID.randomUUID().toString();
    }

    public static String erasedNonNullString() {
        return " ";
    }

    public static long erasedLongValue() {
        return -1L;
    }

    public static GPSLocation erasedGPSLocation() {
        return new GPSLocation(0, 0);
    }

    public static Double erasedDoubleValue() {
        return -1D;
    }

}
