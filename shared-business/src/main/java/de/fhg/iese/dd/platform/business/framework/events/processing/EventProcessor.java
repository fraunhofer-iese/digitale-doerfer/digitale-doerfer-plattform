/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events.processing;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionExceptionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;

/**
 * Indicates that an annotated class is an "Event Processor", consuming and producing events. Needs to be a subclass of
 * {@link BaseEventProcessor}. Methods that actually process events need to be annotated with @{@link EventProcessing}
 *
 * @see Component
 * @see BaseEventProcessor
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface EventProcessor {

    /**
     * Select the {@link EventExecutionStrategy} to control how the methods annotated with @{@link EventProcessing} are
     * called when the according events are present on the event bus.
     * <p/>
     * Use @{@link EventProcessing#executionStrategy()} to override this strategy for a specific method.
     */
    EventExecutionStrategy executionStrategy() default EventExecutionStrategy.AUTO;

    /**
     * Select the {@link EventExecutionExceptionStrategy} to control how the exceptions occurring during the execution
     * of methods annotated with @{@link EventProcessing} are propagated.
     * <p/>
     * Use @{@link EventProcessing#exceptionStrategy()} to override this strategy for a specific method.
     */
    EventExecutionExceptionStrategy exceptionStrategy() default EventExecutionExceptionStrategy.AUTO;

}
