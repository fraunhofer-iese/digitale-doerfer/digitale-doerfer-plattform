/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.processors;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shared.template.SharedTemplate;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import freemarker.template.TemplateException;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class UserGeneratedContentFlagsEmailEventProcessor extends BaseEventProcessor {

    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private ApplicationConfig applicationConfig;

    @EventProcessing
    void onUserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation(
            UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation userGeneratedContentFlagDeleteFlagEntityByAdminConfirmation) {

        final UserGeneratedContentFlag changedFlag =
                userGeneratedContentFlagDeleteFlagEntityByAdminConfirmation.getChangedFlag();
        final Person entityAuthor = changedFlag.getEntityAuthor();
        if (entityAuthor == null) {
            log.warn(
                    "entityAuthor is null, no e-mail can be sent to the entityAuthor about the deletion of the entity {} ({})",
                    changedFlag.getEntityId(), changedFlag.getEntityType());
            return;
        }
        final DeletedFlagReport flagReport =
                userGeneratedContentFlagDeleteFlagEntityByAdminConfirmation.getFlagReport();

        try {
            String fromEmailAddress = StringUtils.isNotEmpty(flagReport.getFromEmailAddressUserNotification()) ?
                    flagReport.getFromEmailAddressUserNotification() : applicationConfig.getEmail().getAddress();
            String emailText =
                    emailSenderService.createTextWithTemplate(SharedTemplate.FLAGGED_POST_DELETED_EMAIL,
                            buildTemplateModel(entityAuthor, flagReport));
            emailSenderService.sendEmail(fromEmailAddress, entityAuthor.getFullName(), entityAuthor.getEmail(),
                    SharedTemplate.FLAGGED_POST_DELETED_EMAIL_SUBJECT, emailText, Collections.emptyList(), true);
        } catch (IOException | TemplateException | MessagingException e) {
            log.error("Failed to send information email about deleted entity: " + e, e);
        }
    }

    private Map<String, Object> buildTemplateModel(Person receiver, DeletedFlagReport flagReport) {
        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("appName", flagReport.getAppName());
        templateModel.put("receiverName", receiver.getFullName());
        templateModel.put("deletedFlaggedPost", flagReport.getReportedText());
        templateModel.put("contactPerson", roleService.getDefaultContactPersonForTenant(receiver.getTenant()));
        return templateModel;
    }

}
