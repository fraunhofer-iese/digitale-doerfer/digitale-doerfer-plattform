/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import java.time.temporal.ChronoUnit;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsReportDefinition {

    @Singular
    private List<String> statisticsIds;
    private boolean allGeoAreas;
    private List<String> includedRootGeoAreaIds;
    private long startTimeNew;
    private long startTimeActive;
    private long endTime;

    private ChronoUnit timeUnit;
    private int offsetFromCurrentFullTimeUnit;

    private String name;
    private String fileNameCsv;
    private String fileNameCsvMetadata;
    private String csvSeparator;
    private String fileNameText;

    public boolean isTextReport() {
        return StringUtils.isNotEmpty(fileNameText);
    }

    public boolean isCsvReport() {
        return StringUtils.isNotEmpty(fileNameCsv) && StringUtils.isNotEmpty(fileNameCsvMetadata) &&
                StringUtils.isNotEmpty(csvSeparator);
    }

}
