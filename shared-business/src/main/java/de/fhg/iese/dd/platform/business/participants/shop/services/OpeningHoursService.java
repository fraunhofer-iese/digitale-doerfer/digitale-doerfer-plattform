/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.shop.services;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursEntryRepository;

@Service
class OpeningHoursService extends BaseEntityService<OpeningHours> implements IOpeningHoursService {

    @Autowired
    private ITimeService timeService;

    @Autowired
    private OpeningHoursEntryRepository openingHoursEntryRepository;

    private static ZonedDateTime getLocalizedMidnight(final ZonedDateTime localNow) {
        return localNow.toLocalDate().atStartOfDay(localNow.getZone());
    }

    @Override
    public long getLastFutureClosingTimeOnSameDay(OpeningHours openingHours, long timestamp){
        final ZonedDateTime current = timeService.toLocalTime(timestamp);
        Optional<OpeningHoursEntry> lastOpeningHoursEntryOnSameDay = openingHours.getEntries()
                .stream()
                .filter(ohe -> ohe.getWeekday() == current.getDayOfWeek())
                .min((ohe1, ohe2) -> Long.compare(ohe2.getToTime(), ohe1.getToTime()));

        if (lastOpeningHoursEntryOnSameDay.isPresent()) {
            long closingTime = timeService.toTimeMillis(getLocalizedMidnight(current)) +
                    lastOpeningHoursEntryOnSameDay.get().getToTime();
            if (closingTime > timeService.currentTimeMillisUTC()) {
                return closingTime;
            }
        }

        return -1L;
    }

    @Override
    public long getLastFutureClosingTimeOnSameOrFutureDay(OpeningHours openingHours, long timestamp){
        long currentClosingTime = -1L;
        int counter = 0;
        while (currentClosingTime < 0L && counter < 7){
            currentClosingTime = getLastFutureClosingTimeOnSameDay(openingHours, timestamp + counter * TimeUnit.DAYS.toMillis(1L));
            counter++;
        }

        return currentClosingTime;
    }

    @Override
    public long getFirstFutureOpeningTimeOnSameOrFutureDay(OpeningHours openingHours, long timestamp) {
        final ZonedDateTime current = timeService.toLocalTime(timestamp);
        Optional<OpeningHoursEntry> firstOpeningHoursEntryOnSameDay = openingHours.getEntries()
                .stream()
                .filter(ohe -> ohe.getWeekday() == current.getDayOfWeek())
                .min(Comparator.comparingLong(OpeningHoursEntry::getFromTime));

        if (firstOpeningHoursEntryOnSameDay.isPresent()) {
            long openingTime = timeService.toTimeMillis(getLocalizedMidnight(current)) +
                    firstOpeningHoursEntryOnSameDay.get().getFromTime();
            if (openingTime > timeService.currentTimeMillisUTC()) {
                return openingTime;
            }
        }

        return -1L;
    }

    @Override
    public long getFirstFutureOpeningTimeOnSameDay(OpeningHours openingHours, long timestamp){
        long currentOpeningTime = -1L;
        int counter = 0;
        while (currentOpeningTime < 0L && counter < 7){
            currentOpeningTime = getFirstFutureOpeningTimeOnSameDay(openingHours, timestamp + counter * TimeUnit.DAYS.toMillis(1L));
            counter++;
        }

        return currentOpeningTime;
    }

    @Override
    public OpeningHoursEntry store(OpeningHoursEntry openingHoursEntry) {
        return openingHoursEntryRepository.saveAndFlush(openingHoursEntry);
    }

}
