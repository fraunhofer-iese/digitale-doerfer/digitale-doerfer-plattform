/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.services;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public abstract class BaseService implements IService {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private ApplicationConfig config;
    @Autowired
    protected ITimeService timeService;
    @Autowired
    private EventBusAccessor eventBusAccessor;
    @Autowired
    private IPersistenceSupportService persistenceSupportService;

    @PostConstruct
    private void initialize() {
        eventBusAccessor.setOwner(this);
    }

    /**
     * ApplicationConfig filled with values from application.yml
     *
     * @return current {@link ApplicationConfig} for that profile.
     */
    protected ApplicationConfig getConfig(){
        return config;
    }

    /**
     * Fire this event asynchronous and do not wait for replies or exceptions.
     *
     * @param trigger event that gets fired
     */
    protected <E extends BaseEvent> void notify(E trigger) {
        eventBusAccessor.notifyTrigger(trigger);
    }

    /**
     * Fire this event asynchronous with some delay and do not wait for replies or exceptions.
     *
     * @param triggerSupplier supplier for event that gets fired
     */
    protected <E extends BaseEvent> void notifyDelayed(Supplier<E> triggerSupplier, long delay, TimeUnit unit) {
        eventBusAccessor.notifyTriggerDelayed(triggerSupplier, delay, unit);
    }

    /**
     * Publishes the given event asynchronous with some delay and wait for replies or exceptions.
     *
     * @param event           event that gets fired
     * @param delay           the time from now to delay sending the event
     * @param unit            the time unit of the delay parameter
     * @param eventsToWaitFor The types of event that should occur
     * @param <E>             Type of event that gets fired
     * @return A future for the first event of the specific type that was waited for
     */
    @SafeVarargs
    @SuppressWarnings("varargs")
    final protected <E extends BaseEvent> ScheduledFuture<BaseEvent> notifyTriggerAndWaitForAnyReplyDelayed(
            E event, long delay, TimeUnit unit, Class<? extends BaseEvent>... eventsToWaitFor) {

        List<Class<? extends BaseEvent>> eventsToWaitForList = Arrays.asList(eventsToWaitFor);
        return eventBusAccessor.notifyTriggerAndWaitForAnyReplyDelayed(event, eventsToWaitForList, delay, unit);
    }


    /**
     * @see IPersistenceSupportService#saveAndIgnoreIntegrityViolation(Supplier, Supplier)
     */
    protected <T> T saveAndIgnoreIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier) {
        return persistenceSupportService.saveAndIgnoreIntegrityViolation(saveSupplier, onExceptionSupplier);
    }

    /**
     * @see IPersistenceSupportService#saveAndRetryOnDataIntegrityViolation(Supplier, Supplier)
     */
    protected  <T extends BaseEntity> T saveAndRetryOnDataIntegrityViolation(
            Supplier<T> saveSupplier,
            Supplier<T> onExceptionSupplier) {
        return persistenceSupportService.saveAndRetryOnDataIntegrityViolation(saveSupplier, onExceptionSupplier);
    }

    /**
     * @see IPersistenceSupportService#executeAndRetryOnDataIntegrityViolation(Supplier)
     */
    protected <T extends BaseEntity> T executeAndRetryOnDataIntegrityViolation(Supplier<T> saveSupplier) {
        return persistenceSupportService.executeAndRetryOnDataIntegrityViolation(saveSupplier);
    }

    /**
     * @see IPersistenceSupportService#query(Supplier)
     */
    protected <T> T query(Supplier<T> query) {
        return persistenceSupportService.query(query);
    }

    /**
     * @see IPersistenceSupportService#save(Supplier)
     */
    protected <T> T save(Supplier<T> query) {
        return persistenceSupportService.save(query);
    }

}
