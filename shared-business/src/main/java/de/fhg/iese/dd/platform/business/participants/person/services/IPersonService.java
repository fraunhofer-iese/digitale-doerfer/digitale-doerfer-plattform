/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.services;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.GeoAreaIsNoLeafException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonNotFoundException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithEmailNotFoundException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithOauthIdNotFoundException;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;

public interface IPersonService extends IEntityService<Person> {

    String DEFAULT_ADDRESS_LIST_ENTRY_NAME = "default";

    Page<Person> findAllNotDeleted(Pageable page);

    List<Person> findAllPersonsById(List<String> personIds) throws PersonNotFoundException;

    /**
     * returns all persons having tenant.id or homeArea.tenant.id in the given tenant ids. If tenantIds is null or
     * empty, no persons are returned.
     *
     * @param tenantIds
     * @param page
     *
     * @return
     */
    Page<Person> findAllNotDeletedBelongingToTenantIn(Set<String> tenantIds, Pageable page);

    /**
     * Searches for persons having the search string as infix (=continuous substring) in at least one of the
     * following fields:
     * <ul>
     *     <li>id</li>
     *     <li>firstName</li>
     *     <li>lastName</li>
     *     <li>oauthId</li>
     *     <li>email</li>
     * </ul>
     * Search is done case-insensitive. E.g. the person {id='abcd123', firstName='Franziska', lastName='Franzen',
     * oauthId='1234', email='hallo@example.org'} is found when searching for 'a', '123', 'franz', 'FranZen' and
     * 'Example'.
     *
     * @param infixSearchString
     * @param page
     * @return
     */
    Page<Person> findAllByCaseInsensitiveInfixSearchInMainFields(String infixSearchString, Pageable page);

    /**
     * Searches for persons having the respective search strings as infix (=continuous substring) in first or
     * last name. Search is done in a case-insensitive way.
     *
     * @param firstNameInfix
     * @param lastNameInfix
     * @param page
     * @return
     */
    Page<Person> findAllByCaseInsensitiveInfixSearchInFirstNameAndLastName(String firstNameInfix, String lastNameInfix,
            Pageable page);

    /**
     * Like {@link IPersonService#findAllNotDeletedBelongingToTenantIn(Set, Pageable)}, but also returns deleted
     * persons and additionally filters to persons having the search string as infix (=continuous substring)
     * in at least one of the following fields:
     * <ul>
     *     <li>id</li>
     *     <li>firstName</li>
     *     <li>lastName</li>
     *     <li>oauthId</li>
     *     <li>email</li>
     * </ul>
     * Search is done case-insensitive. E.g. the person {id='abcd123', firstName='Franziska', lastName='Franzen',
     * oauthId='1234', email='hallo@example.org'} is found when searching for 'a', '123', 'franz', 'FranZen' and
     * 'Example'.
     *
     * @param infixSearchString
     * @param tenantIds
     * @param page
     * @return
     */
    Page<Person> findAllByCaseInsensitiveInfixSearchInMainFieldsAndBelongingToTenantIn(String infixSearchString,
            Set<String> tenantIds, Pageable page);

    /**
     * Like {@link IPersonService#findAllNotDeletedBelongingToTenantIn(Set, Pageable)}, but also returns deleted
     * persons and additionally filters to persons having the respective search strings as infix (=continuous
     * substring) in first or last name. Search is done in a case-insensitive way.
     *
     * @param firstNameInfix
     * @param lastNameInfix
     * @param tenantIds
     * @param page
     * @return
     */
    Page<Person> findAllByCaseInsensitiveInfixSearchInFirstNameAndLastNameAndBelongingToTenantIn(String firstNameInfix,
            String lastNameInfix, Set<String> tenantIds, Pageable page);

    Person findPersonById(String personId) throws PersonNotFoundException;

    Person fetchPersonExtendedAttributes(Person person);

    void checkPersonExistsById(String personId) throws PersonNotFoundException;

    /**
     * Find the currently logged in person by oAuthId and update the last logged in timestamp.
     *
     * @param oauthId the oauthId provided in the JWT token of the request
     *
     * @return the currently logged in person
     *
     * @throws PersonWithOauthIdNotFoundException if there is no person with the given oAuthId
     * @throws NotAuthorizedException             if the current {@link Person#isDeleted()} = true
     */
    Person findCurrentlyLoggedInPerson(String oauthId)
            throws PersonWithOauthIdNotFoundException, NotAuthorizedException;

    Person findPersonByEmail(String email) throws PersonWithEmailNotFoundException;

    /**
     * Checks if a person already exists with that email or pending new email address
     */
    boolean existsPersonByEmail(String email);

    boolean existsPersonByOauthIdNotDeleted(String oauthId);

    String getFirstAndLastName(int length, Person person);

    Person create(Person person) throws EMailAlreadyUsedException;

    Person updateProfilePicture(Person person, MediaItem mediaItem);

    Person deleteProfilePicture(Person person);

    List<AddressListEntry> getAddressListEntries(Person person);

    /**
     * Gets the default address list entry of a person. If more than one default entry exists the oldest one is chosen.
     *
     * @param person
     *
     * @return
     */
    AddressListEntry getDefaultAddressListEntry(Person person);

    /**
     * Sets the default address of a person. If such an address list entry does
     * not exist, it creates one. If it exists, it updates it.
     * </p>
     * The address is first checked by {@link IAddressService#findOrCreateAddress(IAddressService.AddressDefinition,
     * IAddressService.AddressFindStrategy, IAddressService.GPSResolutionStrategy, boolean)} before it is
     * used, so it can be a <code><strong>new</strong> Address(...)</code>
     *
     * @param person
     * @param defaultAddress
     * @return
     */
    AddressListEntry setDefaultAddress(Person person, IAddressService.AddressDefinition defaultAddress);

    /**
     * Deletes the default address of a person. The address itself is not deleted, but the {@link AddressListEntry}
     *
     * @param person
     */
    void deleteDefaultAddress(Person person);

    /**
     * Gets the first {@link AddressListEntry} with the given name or null if there is none.
     * If there are multiple with the same name the oldest one is chosen.
     *
     * @param person
     * @param addressListEntryName
     * @return
     */
    AddressListEntry getAddressListEntryByName(Person person, String addressListEntryName);

    /**
     * Gets the {@link AddressListEntry} with the given id or null if there is none for that person.
     *
     * @param person
     * @param id
     * @return
     */
    AddressListEntry getAddressListEntryById(Person person, String id);

    /**
     * Adds a new {@link AddressListEntry}. <strong>Currently there is no check for
     * the name of the address list entry if the default address is
     * duplicated</strong>
     * </p>
     * The address is first checked by {@link IAddressService} before it is
     * used, so it can be a <code><strong>new</strong> Address(...)</code>
     *
     * @param person
     * @param entry
     * @return
     */
    AddressListEntry addAddressListEntry(Person person, AddressListEntry entry);

    /**
     * Changes an existing {@link AddressListEntry}. If the oldEntry is not yet
     * in the address list of the person a {@link NotFoundException} is thrown.
     * <br/>
     * <strong>Currently there is no check for the name of the address list
     * entry is {@link IPersonService#DEFAULT_ADDRESS_LIST_ENTRY_NAME} </strong>
     * </p>
     * The address in newEntry is first checked by {@link IAddressService} before it is
     * used, so it can be a <code><strong>new</strong> Address(...)</code> or
     * directly from the client.
     *
     * @param person
     * @param oldEntry
     * @param newEntry
     * @return
     * @throws NotFoundException
     *             if the person does not have the oldEntry in the address list.
     *             Use
     *             {@link IPersonService#addAddressListEntry(Person, AddressListEntry)}
     *             in this case.
     */
    AddressListEntry changeAddressListEntry(Person person, AddressListEntry oldEntry, AddressListEntry newEntry)
            throws NotFoundException;

    /**
     * Deletes the address list entry with the given id. <strong>Currently there is no check if the default address is
     * removed</strong>
     *
     * @param person
     * @param id
     *
     * @throws NotFoundException if the person does not have the given address in the address list.
     */
    void deleteAddressListEntryById(Person person, String id) throws NotFoundException;

    /**
     * Checks if the geo area can be used as home area
     *
     * @param newHomeArea the new home area to check
     *
     * @throws GeoAreaIsNoLeafException if the geo area can not be used as home area
     */
    void checkIsValidHomeArea(GeoArea newHomeArea) throws GeoAreaIsNoLeafException;

    /**
     * Changes the {@link Person#getLastLoggedIn()} of a person if it is within the {@link
     * ParticipantsConfig#getLastLoggedInUpdateThreshold()}.
     * <p>
     * If the person had the status {@link PersonStatus#PENDING_DELETION} this status is removed.
     *
     * @param person the person to change the last logged in
     *
     * @return The updated person and true if there was an update of the last logged in.
     */
    Pair<Boolean, Person> updateLastLoggedIn(Person person);

    /**
     * Changes the {@link Person#getLastSentTimeEmailVerification()} of a person if the verification mail was sent.
     *
     * @param person
     * @param lastSentTimeEmailVerification timestamp when the last verification mail was sent
     *
     * @return The updated person.
     */
    Person updateLastSentTimeEmailVerification(Person person, long lastSentTimeEmailVerification);

    /**
     * Changes the {@link Person#getVerificationStatuses()} of the email address of a person.
     *
     * @param person
     * @param emailAddressVerified indicator if PersonVerificationStatus.EMAIL_VERIFIED should be added or removed
     *
     * @return The updated person and true if the email address verification status was added.
     */
    Pair<Boolean, Person> updateEmailAddressVerificationStatus(Person person, boolean emailAddressVerified);

    Page<Person> findInactivePersonsWithoutStatusWithVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus forbiddenStatus, PersonVerificationStatus requiredVerificationStatus,
            int lastLoggedInDays, int maxNumPersons);

    Page<Person> findInactivePersonsWithStatusWithoutVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus requiredStatus, PersonVerificationStatus forbiddenVerificationStatus,
            int lastLoggedInDays, int maxNumPersons);

    Page<Person> findInactivePersonsWithoutStatusWithoutVerificationStatus(Predicate<Person> isExcluded,
            PersonStatus forbiddenStatus, PersonVerificationStatus forbiddenVerificationStatus,
            int lastLoggedInDays, int maxNumPersons);

    Page<Person> findPersonsWarnedForDaysAndNotDeleted(Predicate<Person> isExcluded, PersonStatus requiredStatus,
            int warnedForDays, int maxNumPersons);

    Person changePersonStatuses(Person person, Set<PersonStatus> statusesToAdd, Set<PersonStatus> statusesToRemove);

    Person changeEmailAddress(Person person, String newEmail) throws EMailChangeNotPossibleException;

}
