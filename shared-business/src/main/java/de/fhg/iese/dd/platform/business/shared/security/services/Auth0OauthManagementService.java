/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import static de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount.AuthenticationMethod.APPLE;
import static de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount.AuthenticationMethod.FACEBOOK;
import static de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount.AuthenticationMethod.GOOGLE;
import static de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount.AuthenticationMethod.USERNAME_PASSWORD;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.client.mgmt.filter.ClientFilter;
import com.auth0.client.mgmt.filter.DeviceCredentialsFilter;
import com.auth0.client.mgmt.filter.FieldsFilter;
import com.auth0.client.mgmt.filter.UserFilter;
import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.auth0.exception.RateLimitException;
import com.auth0.json.mgmt.Page;
import com.auth0.json.mgmt.client.Client;
import com.auth0.json.mgmt.devicecredentials.DeviceCredentials;
import com.auth0.json.mgmt.userblocks.UserBlocks;
import com.auth0.json.mgmt.users.Identity;
import com.auth0.json.mgmt.users.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordWrongException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.Auth0ManagementException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OAuthEMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthManagementConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.LoginHint;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
@Profile({"!test | Auth0OAuthManagementServiceTest"})
class Auth0OauthManagementService implements IOauthManagementService {

    private static final String AUTH0_USER_DB_CONNECTION = "Username-Password-Authentication";
    private static final String GOOGLE_IDENTITY_PROVIDER = "google-oauth2";
    private static final String FACEBOOK_IDENTITY_PROVIDER = "facebook";
    private static final String APPLE_IDENTITY_PROVIDER = "apple";
    private static final String AUTH0_IDENTITY_PROVIDER = "auth0";
    private static final String NECESSARY_SCOPES =
            "read:users delete:users create:users update:users read:grants delete:grants read:clients read:device_credentials";

    @Autowired
    private OauthManagementConfig config;
    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;

    private ManagementAPI managementApi;
    private AuthAPI authApi;
    private String accessToken = null;

    @FunctionalInterface
    private interface ThrowingSupplier<T> {

        T get() throws Auth0Exception;

    }

    @FunctionalInterface
    private interface PagedQuery<R> {

        Page<R> query(int page, int size) throws Auth0Exception;

    }

    @PostConstruct
    protected void init() {
        authApi = AuthAPI.newBuilder(StringUtils.removeEnd(config.getDomain(), "/"), config.getClientId(),
                config.getClientSecret()).build();
    }

    private ManagementAPI managementApi() {

        if (!isAccessTokenValid(accessToken)) {
            accessToken = getAccessToken();
            managementApi = ManagementAPI.newBuilder(config.getDomain(), getAccessToken()).build();
            log.debug("refreshed token for auth0 management api");
        }
        return managementApi;
    }

    private String getAccessToken() {
        try {
            return authApi
                    .requestToken(StringUtils.appendIfMissing(config.getDomain(), "/") + "api/v2/")
                    .setScope(NECESSARY_SCOPES)
                    .execute()
                    .getBody()
                    .getAccessToken();
        } catch (Auth0Exception ex) {
            throw new IllegalStateException("Configuration error, could not get accessToken for auth0 management api",
                    ex);
        }
    }

    private boolean isAccessTokenValid(String accessToken) {
        if (accessToken == null) {
            return false;
        }
        try {
            final DecodedJWT jwt = JWT.decode(accessToken);
            //no need to use time service here, since this is just dependent on Auth0
            return jwt.getExpiresAt() != null && jwt.getExpiresAt().after(new Date(System.currentTimeMillis() + 5000));
        } catch (JWTDecodeException exception) {
            return false;
        }
    }

    @Override
    public String createUser(String email, String password, boolean emailVerified) throws Auth0ManagementException {

        User newUser = new User(AUTH0_USER_DB_CONNECTION);
        newUser.setName(email);
        newUser.setEmail(email);
        // see comment at "changePassword"
        newUser.setPassword(password.toCharArray());
        newUser.setEmailVerified(emailVerified);
        try {
            String oauthId = executeQueryAndRetryOnceOnRateLimit(() ->
                    managementApi().users()
                            .create(newUser)
                            .execute()
                            .getBody()
                            .getId());

            oauthRegistrationRepository.save(OauthRegistration.builder()
                    .oauthId(oauthId)
                    .email(email)
                    .build());
            return oauthId;
        } catch (Auth0Exception ex) {
            throw new Auth0ManagementException(ex);
        }
    }

    @Override
    public void changeEmail(String oauthId, String newEmail, boolean newEmailVerified)
            throws EMailChangeNotPossibleException, Auth0ManagementException, OAuthEMailAlreadyUsedException {

        // Check if a user with the new email is already present
        if (getOauthIdForUser(newEmail) != null) {
            throw new OAuthEMailAlreadyUsedException(newEmail);
        }
        // Check if the user has a username/password authenticationMethod
        if (queryUserByOauthId(oauthId).getAuthenticationMethods().stream()
                .noneMatch(authenticationMethod ->
                        authenticationMethod.equals(USERNAME_PASSWORD))) {
            throw EMailChangeNotPossibleException.forNoUsernamePasswordAccount(newEmail);
        }

        // managementApi uses the User object as a JSON-Builder with fields to update
        // Specific connection is required when updating email and emailVerified fields
        User updateUser = new User(AUTH0_USER_DB_CONNECTION);
        updateUser.setName(newEmail);
        updateUser.setEmail(newEmail);
        updateUser.setEmailVerified(newEmailVerified);
        executeUserQueryAndHandleExceptions(oauthId, () ->
                managementApi().users()
                        .update(oauthId, updateUser).execute());
        OauthRegistration oauthRegistration = oauthRegistrationRepository.findByOauthId(oauthId);
        if (oauthRegistration != null) {
            oauthRegistration.setEmail(newEmail);
            oauthRegistrationRepository.save(oauthRegistration);
        }
    }

    @Override
    public void changePassword(String oauthId, char[] newPassword)
            throws PasswordChangeNotPossibleException, Auth0ManagementException, OauthAccountNotFoundException {

        checkAuthenticationMethod(oauthId);
        updatePassword(oauthId, newPassword);
    }

    @Override
    public void changePasswordAndVerifyOldPassword(String oauthId, String email, char[] oldPassword, char[] newPassword)
            throws PasswordChangeNotPossibleException, Auth0ManagementException, OauthAccountNotFoundException,
            PasswordWrongException {

        checkAuthenticationMethod(oauthId);
        // check if user can log in with email and current password
        try {
            authApi.login(email, oldPassword).execute();
        } catch (Auth0Exception ex) {
            throw new PasswordWrongException(
                    "Could not log in to Auth0, wrong password was provided by person with email '{}'", email);
        }
        updatePassword(oauthId, newPassword);
    }

    private void checkAuthenticationMethod(String oauthId) {
        // check if the user has a username/password authenticationMethod
        if (queryUserByOauthId(oauthId).getAuthenticationMethods().stream()
                .noneMatch(authenticationMethod ->
                        authenticationMethod.equals(USERNAME_PASSWORD))) {
            throw new PasswordChangeNotPossibleException("The password could not be changed");
        }
    }

    private void updatePassword(String oauthId, char[] newPassword) {
        User updateUser = new User(AUTH0_USER_DB_CONNECTION);
        updateUser.setPassword(newPassword);
        executeUserQueryAndHandleExceptions(oauthId, () ->
                managementApi().users()
                        //this returns 404 if the user does not exist
                        .update(oauthId, updateUser).execute());
    }

    @Override
    public void blockUser(String oauthId) throws Auth0ManagementException, OauthAccountNotFoundException {

        User updateUser = new User();
        updateUser.setBlocked(true);
        executeUserQueryAndHandleExceptions(oauthId, () ->
                managementApi().users()
                        //this returns 404 if the user does not exist
                        .update(oauthId, updateUser).execute());
        revokeAllRefreshTokens(oauthId);
    }

    @Override
    public void unblockUser(String oauthId) throws Auth0ManagementException, OauthAccountNotFoundException {

        User updateUser = new User();
        updateUser.setBlocked(false);
        executeUserQueryAndHandleExceptions(oauthId, () ->
                // Unblock manual admin block
                managementApi().users()
                        //this returns 404 if the user does not exist
                        .update(oauthId, updateUser).execute());
    }

    @Override
    public void unblockAutomaticallyBlockedUser(String oauthId) {

        executeUserQueryAndHandleExceptions(oauthId, () ->
                // Unblock block through e.g. excessive amount of incorrect login attempts
                managementApi().userBlocks()
                        //this returns 404 if the user does not exist
                        .delete(oauthId).execute());
    }

    @Override
    public void triggerResetPasswordMail(String email) throws Auth0ManagementException {
        try {
            authApi.resetPassword(email, AUTH0_USER_DB_CONNECTION).execute();
        } catch (Auth0Exception ex) {
            throw new Auth0ManagementException(ex);
        }
    }

    @Override
    public void triggerVerificationMail(String oauthId) throws Auth0ManagementException, OauthAccountNotFoundException {

        //this is necessary since the next operation does not return 404 if the user does not exist
        ensureUserExists(oauthId);

        executeUserQueryAndHandleExceptions(oauthId, () ->
                managementApi().jobs()
                        .sendVerificationEmail(oauthId, null).execute());
    }

    @Override
    public void setEmailVerified(String oauthId, boolean emailVerified)
            throws Auth0ManagementException, OauthAccountNotFoundException {

        User updateUser = new User(AUTH0_USER_DB_CONNECTION);
        updateUser.setEmailVerified(emailVerified);

        executeUserQueryAndHandleExceptions(oauthId, () ->
                managementApi().users()
                        //this returns 404 if the user does not exist
                        .update(oauthId, updateUser).execute());
    }

    @Override
    public String getOauthIdForUser(String email) throws OauthManagementException {

        return getOauthIdForUser(email, user -> true);
    }

    @Override
    public String getOauthIdForCustomUser(String email) throws Auth0ManagementException {

        return getOauthIdForUser(email, user -> user.getIdentities() != null
                && user.getIdentities().stream()
                .anyMatch(identity -> identity != null && AUTH0_USER_DB_CONNECTION.equals(identity.getConnection())));
    }

    private String getOauthIdForUser(String email, Predicate<User> additionalFilter) {

        try {
            final List<User> users = executeQueryAndRetryOnceOnRateLimit(() ->
                    managementApi()
                            .users()
                            //it seems that auth0 converts email addresses to lower case
                            .listByEmail(email.toLowerCase(Locale.ROOT),
                                    new FieldsFilter().withFields("user_id,identities", true))
                            .execute()
                            .getBody());
            if (CollectionUtils.isEmpty(users)) {
                return null;
            }
            //example response:
            //{
            //    "user_id": "google-oauth2|106849812505744138953",
            //    "identities": [
            //        {
            //            "provider": "google-oauth2",
            //            "access_token": "ya29.Gl0...",
            //            "expires_in": 3599,
            //            "user_id": "106849812505744138953",
            //            "connection": "google-oauth2",
            //            "isSocial": true
            //        }
            //    ]
            //},
            //{
            //    "user_id": "auth0|59afe4812a2926289b6995a5",
            //    "identities": [
            //        {
            //            "user_id": "59afe4812a2926289b6995a5",
            //            "provider": "auth0",
            //            "connection": "Username-Password-Authentication",
            //            "isSocial": false
            //        }
            //    ]
            //}
            //the following filters for the first user for which identities.connection == "Username-Password-Authentication"
            //and returns the user's id if a user is found
            return users.stream()
                    .filter(additionalFilter)
                    .findFirst()
                    .map(User::getId)
                    .orElse(null);
        } catch (Auth0Exception ex) {
            throw new Auth0ManagementException(ex);
        }
    }

    @Override
    public void revokeAllRefreshTokens(String oauthId) throws OauthAccountNotFoundException, OauthManagementException {

        executeUserQueryAndHandleExceptions(oauthId,
                () -> managementApi().grants()
                        //also works with not existing accounts
                        .deleteAll(oauthId).execute());
    }

    public void deleteUser(String oauthId) throws Auth0ManagementException {

        executeUserQueryAndHandleExceptions(oauthId,
                () -> {
                    try {
                        return managementApi().users()
                                //also works with not existing accounts, but not with invalid oauthIds
                                .delete(oauthId).execute();
                    } catch (APIException e) {
                        //this happens if the oauthId is invalid, in this case the user is already deleted
                        if ("invalid_uri".equals(e.getError())) {
                            return true;
                        } else {
                            throw e;
                        }
                    }
                });

        //if the deletion at Auth0 was successful, the saved entity at the platform needs to be deleted, too
        oauthRegistrationRepository.deleteByOauthId(oauthId);
    }

    @Override
    public boolean isBlocked(String oauthId) throws OauthAccountNotFoundException, OauthManagementException {

        final User user = executeUserQueryAndHandleExceptions(oauthId,
                () -> managementApi().users()
                        //this returns 404 if the user does not exist
                        .get(oauthId, new UserFilter().withFields("blocked", true))
                        .execute()
                        .getBody());
        return Boolean.TRUE.equals(user.isBlocked());
    }

    @Override
    public OauthAccount queryUserByOauthId(String oauthId)
            throws OauthAccountNotFoundException, OauthManagementException {

        final User user = executeUserQueryAndHandleExceptions(oauthId,
                () -> managementApi().users()
                        .get(oauthId, null)
                        .execute()
                        .getBody());

        return executeUserQueryAndHandleExceptions(oauthId,
                () -> userToOauthAccount(user));
    }

    private void ensureUserExists(String oauthId) throws OauthAccountNotFoundException {
        executeUserQueryAndHandleExceptions(oauthId,
                () -> managementApi().users()
                        .get(oauthId, null)
                        .execute());
    }

    @Override
    public OauthAccount queryUserByEmail(String email) throws OauthAccountNotFoundException, OauthManagementException {

        if (StringUtils.isEmpty(email)) {
            throw new IllegalArgumentException("email must not be null or empty");
        }
        try {
            final List<User> users = executeQueryAndRetryOnceOnRateLimit(() -> managementApi().users()
                    .listByEmail(email, null)
                    .execute()
                    .getBody());
            if (users.isEmpty()) {
                throw OauthAccountNotFoundException.forEmail(email);
            }
            if (users.size() > 1) {
                log.warn("Found more than one user with email {}, using the first result", email);
            }

            return userToOauthAccount(users.get(0));
        } catch (APIException ex) {
            log.error("Failed to query user by email ({}): {}", ex.getError(), ex.getMessage());
            throw new Auth0ManagementException(ex);
        } catch (Auth0Exception ex) {
            throw new Auth0ManagementException(ex);
        }
    }

    @Override
    public List<OauthClient> getConfiguredOauthClients() {

        List<Client> clients = executePagedQuery((page, size) -> managementApi().clients()
                .list(new ClientFilter()
                        .withFields("client_id,name", true)
                        .withPage(page, size)
                        .withTotals(true))
                .execute()
                .getBody(), 200);

        return clients.stream()
                .map(client -> OauthClient.builder()
                        .oauthClientIdentifier(client.getClientId())
                        .name(client.getName())
                        .build())
                .sorted(Comparator.comparing(oauthClient -> StringUtils.defaultString(oauthClient.getName())))
                .collect(Collectors.toList());
    }

    @Override
    public LoginHint getLoginHint(Person person) throws OauthManagementException {

        final OauthAccount oauthAccount = queryUserByOauthId(person.getOauthId());
        return LoginHint.builder()
                .authenticationMethods(oauthAccount.getAuthenticationMethods())
                .passwordChangeable(isPasswordChangeable(oauthAccount))
                .build();
    }

    // password is only changeable when the user has username/password authenticationMethod
    private boolean isPasswordChangeable(OauthAccount oauthAccount) {
        return oauthAccount.getAuthenticationMethods().contains(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD);
    }

    private OauthAccount userToOauthAccount(User user) throws Auth0Exception {

        final Set<OauthAccount.AuthenticationMethod> authenticationMethods = user.getIdentities().stream()
                .map(Auth0OauthManagementService::identityToAuthenticationMethod)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        final boolean manuallyBlocked = Boolean.TRUE.equals(user.isBlocked());
        final OauthAccount oauthAccount = OauthAccount.builder()
                .oauthId(user.getId())
                .name(user.getName())
                .emailVerified(Boolean.TRUE.equals(user.isEmailVerified()))
                .authenticationMethods(authenticationMethods)
                .loginCount(user.getLoginsCount())
                .blocked(manuallyBlocked)
                .blockReason(manuallyBlocked ? OauthAccount.BlockReason.MANUALLY_BLOCKED : null)
                .build();

        //if user is not manually blocked, check if user is blocked because of too many wrong login attempts
        if (!manuallyBlocked) {
            final UserBlocks userBlocks = managementApi().userBlocks()
                    .get(user.getId())
                    .execute()
                    .getBody();
            if (!CollectionUtils.isEmpty(userBlocks.getBlockedFor())) {
                oauthAccount.setBlocked(true);
                oauthAccount.setBlockReason(OauthAccount.BlockReason.TOO_MANY_LOGIN_ATTEMPTS);
            }
        }
        final List<DeviceCredentials> deviceCredentials = managementApi().deviceCredentials()
                .list(new DeviceCredentialsFilter()
                        .withUserId(user.getId())
                        .withFields("client_id,device_name", true))
                .execute()
                .getBody();
        if (!CollectionUtils.isEmpty(deviceCredentials)) {
            oauthAccount.setDevices(deviceCredentials.stream()
                    .map(d -> OauthAccount.OauthAccountDevice.builder()
                            .clientIdentifier(d.getClientId())
                            .deviceName(d.getDeviceName())
                            .build())
                    .sorted(Comparator.comparing(OauthAccount.OauthAccountDevice::getDeviceName))
                    .distinct()
                    .collect(Collectors.toList()));
        }
        return oauthAccount;
    }

    private static OauthAccount.AuthenticationMethod identityToAuthenticationMethod(Identity identity) {
        final String provider = StringUtils.lowerCase(identity.getProvider());

        switch (provider) {
            case AUTH0_IDENTITY_PROVIDER:
                return USERNAME_PASSWORD;
            case GOOGLE_IDENTITY_PROVIDER:
                return GOOGLE;
            case FACEBOOK_IDENTITY_PROVIDER:
                return FACEBOOK;
            case APPLE_IDENTITY_PROVIDER:
                return APPLE;
            default:
                log.warn("Unknown identity provider: " + provider);
                return null;
        }
    }

    private <T> List<T> executePagedQuery(PagedQuery<T> query, int maxItems) throws Auth0ManagementException {

        try {
            List<T> result = new ArrayList<>();
            Page<T> page = null;
            int pageNumber = 0;
            do {
                try {
                    page = query.query(pageNumber, 100); // 100 is the current maximum page size
                    result.addAll(page.getItems());
                    pageNumber++;
                } catch (RateLimitException ex) {
                    handleRateLimitException(ex);
                }
            } while (result.size() < maxItems //not enough items returned
                    &&
                    (page == null // no request could have been done, due to the rate limit
                            ||
                            result.size() < page.getTotal())); //there are still enough items available
            return result;

        } catch (Auth0Exception e) {
            throw new Auth0ManagementException(e);
        }
    }

    private <T> T executeUserQueryAndHandleExceptions(String oauthId, ThrowingSupplier<T> query)
            throws OauthAccountNotFoundException, Auth0ManagementException {

        if (StringUtils.isEmpty(oauthId)) {
            throw OauthAccountNotFoundException.forOauthId(oauthId);
        }
        try {
            return executeQueryAndRetryOnceOnRateLimit(query);
        } catch (APIException ex) {
            if (ex.getStatusCode() == 404 || "inexistent_user".equalsIgnoreCase(ex.getError())) {
                throw OauthAccountNotFoundException.forOauthId(oauthId);
            }
            throw new Auth0ManagementException(ex);
        } catch (Auth0Exception ex) {
            throw new Auth0ManagementException(ex);
        }
    }

    private <T> T executeQueryAndRetryOnceOnRateLimit(ThrowingSupplier<T> query) throws Auth0Exception {
        try {
            return query.get();
        } catch (RateLimitException ex) {
            handleRateLimitException(ex);
            //we retry it once
            return query.get();
        }
    }

    private void handleRateLimitException(RateLimitException ex) {
        long secondsToWait = ex.getReset() - (System.currentTimeMillis() / 1000);
        log.warn("Rate limit of calls to Auth0 exceeded! Waiting for {} seconds", secondsToWait);
        try {
            Thread.sleep(secondsToWait * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
