/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import lombok.AllArgsConstructor;

/**
 * Event that is fired to encapsulate the response to idempotent events.
 * <p>
 * The reason to encapsulate these events is that it is often required to send
 * these events back to the clients, but it should be avoided to trigger all
 * related event processors (e.g. push)
 *
 * @param <D>
 */
@AllArgsConstructor
public final class DuplicateEvent<D extends BaseEvent> extends BaseEvent {

    private final D duplicatedEvent;

    public <E extends BaseEvent> E getDuplicatedEvent(Class<E> expectedEventClass){
        if(duplicatedEvent != null && expectedEventClass!= null &&
                expectedEventClass.isAssignableFrom(duplicatedEvent.getClass())){
            @SuppressWarnings("unchecked")
            E result = (E) duplicatedEvent;
            return result;
        }
        return null;
    }

}
