/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.services;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectReader;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.waiting.events.PeriodicWaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.events.WaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.events.WaitingExpiredEvent.WaitingDeadlineUpdate;
import de.fhg.iese.dd.platform.business.shared.waiting.exceptions.WaitingExpiredEventInitializationException;
import de.fhg.iese.dd.platform.business.shared.waiting.exceptions.WaitingExpiredEventSerializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.repos.WaitingDeadlineRepository;

@Service
class WaitingService extends BaseService implements IWaitingService {

    private static final ObjectReader MAP_JSON_READER = defaultJsonReader().forType(
            new TypeReference<Map<String, Object>>() {
            });

    @Autowired
    private ApplicationContext context;

    @Autowired
    private IPersistenceSupportService genericRepositoryService;

    @Autowired
    private WaitingDeadlineRepository waitingDeadlineRepository;

    @Override
    public WaitingDeadline registerDeadline(WaitingExpiredEvent waitingExpiredEvent) {
        try {
            WaitingDeadline deadline = toWaitingDeadline(waitingExpiredEvent);
            return waitingDeadlineRepository.saveAndFlush(deadline);
        } catch (WaitingExpiredEventSerializationException e) {
            log.error("Failed to register deadline for expired event " + waitingExpiredEvent.getLogMessage(), e);
        }
        return null;
    }

    @Override
    public WaitingDeadline registerDeadline(WaitingExpiredEvent waitingExpiredEvent, String deadlineId) {
        try {
            WaitingDeadline deadline = toWaitingDeadline(waitingExpiredEvent);
            if (StringUtils.isNotEmpty(deadlineId)) {
                deadline.setId(deadlineId);
            }
            return waitingDeadlineRepository.saveAndFlush(deadline);
        } catch (WaitingExpiredEventSerializationException e) {
            log.error("Failed to register deadline for expired event " + waitingExpiredEvent.getLogMessage(), e);
        }
        return null;
    }

    @Override
    public WaitingDeadline toWaitingDeadline(WaitingExpiredEvent waitingExpiredEvent)
            throws WaitingExpiredEventSerializationException {
        try {
            //get the next deadline
            WaitingExpiredEvent.WaitingDeadlineUpdate deadlineUpdate = getDeadlineUpdate(waitingExpiredEvent);
            WaitingDeadline waitingDeadline =
                    new WaitingDeadline(waitingExpiredEvent.getClass().getName(), deadlineUpdate.getNextDeadline());
            //set "now" as the previous deadline
            waitingDeadline.setPreviousDeadline(waitingExpiredEvent.getCreated());
            Map<String, Object> waitingAttributes = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
            Field[] fields = waitingExpiredEvent.getClass().getDeclaredFields();
            for (Field field : fields) {
                //we do not store static fields in the deadline
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                field.setAccessible(true);
                Object value = field.get(waitingExpiredEvent);
                if (value != null) {
                    if (value instanceof BaseEntity) {
                        //if it is a base entity we only but the id in the deadline
                        String id = ((BaseEntity) value).getId();
                        waitingAttributes.put(field.getName(), id);
                    } else {
                        //if it is of any type we put the whole value in the deadline
                        //(in practice this should only be a primitive value)
                        waitingAttributes.put(field.getName(), value);
                    }
                }
            }
            waitingDeadline.setEventAttributes(defaultJsonWriter().writeValueAsString(waitingAttributes));
            return waitingDeadline;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | JsonProcessingException e) {
            throw new WaitingExpiredEventSerializationException(
                    "Failed to create waiting deadline based on waiting expired event: " +
                            waitingExpiredEvent.getLogMessage(), e);
        }
    }

    @Override
    public WaitingExpiredEvent toWaitingExpiredEvent(WaitingDeadline expiredDeadline)
            throws WaitingExpiredEventInitializationException {

        try {
            //find the name of the expired event
            String classNameExpiredEvent = expiredDeadline.getWaitingEventName();

            @SuppressWarnings("unchecked")
            Class<WaitingExpiredEvent> classExpiredEvent =
                    (Class<WaitingExpiredEvent>) Class.forName(classNameExpiredEvent);

            Constructor<WaitingExpiredEvent> constructorExpiredEvent = classExpiredEvent.getConstructor();

            //instantiate the expired event
            WaitingExpiredEvent waitingExpiredEvent = constructorExpiredEvent.newInstance();

            waitingExpiredEvent.setDeadline(expiredDeadline.getDeadline());
            if (waitingExpiredEvent instanceof PeriodicWaitingExpiredEvent) {
                ((PeriodicWaitingExpiredEvent) waitingExpiredEvent).setPreviousDeadline(
                        expiredDeadline.getPreviousDeadline());
            }

            Map<String, Object> attributeValues = MAP_JSON_READER.readValue(expiredDeadline.getEventAttributes());
            setAttributeValues(attributeValues, waitingExpiredEvent);

            return waitingExpiredEvent;
        } catch (Exception e) {
            throw new WaitingExpiredEventInitializationException(
                    "Failed to create waiting expired event based on deadline: " + expiredDeadline.toString(), e);
        }
    }

    @Override
    public WaitingExpiredEvent.WaitingDeadlineUpdate getDeadlineUpdate(WaitingExpiredEvent waitingExpiredEvent) {
        return waitingExpiredEvent.nextDeadline(context);
    }

    @Override
    public void handleDeadlineExpired(WaitingDeadline expiredDeadline, WaitingDeadlineUpdate deadlineUpdate) {
        if (deadlineUpdate != null && deadlineUpdate.isPeriodicDeadline()) {
            expiredDeadline.setPreviousDeadline(expiredDeadline.getDeadline());
            expiredDeadline.setDeadline(deadlineUpdate.getNextDeadline());
            waitingDeadlineRepository.saveAndFlush(expiredDeadline);
        } else {
            //delete it from the db so that we do not signal it again
            waitingDeadlineRepository.delete(expiredDeadline);
        }
    }

    @Override
    public List<WaitingDeadline> findAllExpiredDeadlines() {
        return waitingDeadlineRepository.findAllExpiredDeadlinesOrderByDeadlineAsc(Instant.now().toEpochMilli());
    }

    private void setAttributeValues(Map<String, Object> attributeValues, Object newInstance)
            throws IllegalAccessException {

        Field[] fields = newInstance.getClass().getDeclaredFields();
        for (Field field : fields) {
            //static fields are not set
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            field.setAccessible(true);
            Class<?> fieldType = field.getType();
            //if it is a base entity we get it out of the db based on the id
            if (BaseEntity.class.isAssignableFrom(fieldType)) {
                @SuppressWarnings("unchecked")
                Class<? extends BaseEntity> castedFieldType = (Class<? extends BaseEntity>) fieldType;
                //the ids of the entities are stored in the deadline
                String id = (String) attributeValues.get(field.getName());
                if (id != null) {
                    //might be null if the entity is not in the db anymore, so the attribute is not set
                    field.set(newInstance, genericRepositoryService.findById(castedFieldType, id));
                }
            } else {
                //this might not work if json de-serialization uses wrong type information
                field.set(newInstance, attributeValues.get(field.getName()));
            }
        }
    }

}
