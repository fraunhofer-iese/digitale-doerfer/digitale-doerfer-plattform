/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Only used at data init, the tenant is mapped via the data init tenant specific file mechanism
 */
@Getter
@Setter
@NoArgsConstructor
public class AppVariantGeoAreaInitMapping extends BaseEntity {

    private List<String> appVariantIdentifiers;
    private List<String> geoAreaIdsIncluded;
    private List<String> geoAreaIdsExcluded;
    private String notes;

    @Override
    public String toString() {
        return "AppVariantGeoAreaInitMapping[" +
                "appVariantIdentifiers='" + appVariantIdentifiers + "'" +
                ", geoAreaIdsIncluded='" + geoAreaIdsIncluded + "'" +
                ", geoAreaIdsExcluded='" + geoAreaIdsExcluded + "'" +
                ']';
    }

}
