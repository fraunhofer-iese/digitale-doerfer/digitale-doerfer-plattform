/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.template;

import lombok.Getter;

/**
 * Location for templates, e.g. email templates, within a jar file. This class must be extended by a class
 * located in the same jar file as the template resources.
 *
 */
public abstract class TemplateLocation {

    protected TemplateLocation(String name) {
        this.name = name;
    }

    @Getter
    private final String name;

    /**
     * Returns folder where the templates are located.
     * <br>Override to use a custom folder. Folder name must start with /.
     *
     * @return
     */
    public String getFolder() {
        return "/templates";
    }

    @Override
    public String toString() {
        return "TemplateLocation [class=" + getClass().getSimpleName() + ", name=" + name + "]";
    }

}
