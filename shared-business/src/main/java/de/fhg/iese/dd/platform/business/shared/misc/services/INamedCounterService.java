/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.misc.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.NamedCounter;

public interface INamedCounterService extends IEntityService<NamedCounter> {

    long getCounterValueByName(String name);
    long setCounter(String name, long value);
    long increaseCounter(String name);
    long decreaseCounter(String name);

}
