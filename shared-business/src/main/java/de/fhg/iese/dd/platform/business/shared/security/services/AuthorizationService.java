/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.exceptions.EncryptionException;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.services.IEncryptionService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Service
class AuthorizationService extends BaseService implements IAuthorizationService {

    @Autowired
    private IEncryptionService encryptionService;

    @Autowired
    private IReflectionService reflectionService;

    /**
     * Internal data structure for the auth token, with extra short identifiers
     */
    @Getter
    @Builder
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private static class AuthToken {

        @JsonProperty("n")
        private String eventName;
        @JsonProperty("x")
        private long expirationTimeUTC;
        @JsonProperty("f")
        private Map<String, Object> eventFields;

        @JsonIgnore
        public Map<String, Object> getEventFields() {
            return Objects.requireNonNullElse(eventFields, Collections.emptyMap());
        }

    }

    @Override
    public String generateAuthToken(BaseEvent authorizedEvent, Duration validityDuration) throws EncryptionException {

        String encryptedAuthToken = encryptionService.encryptObjectForExternalUse(
                createAuthToken(authorizedEvent, timeService.currentTimeMillisUTC() + validityDuration.toMillis()));
        if (encryptedAuthToken.length() > 1024) {
            log.warn("Auth token for {} has length {}", authorizedEvent.getClass().getName(),
                    encryptedAuthToken.length());
        }
        return encryptedAuthToken;
    }

    @Override
    public void checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(String token, BaseEvent authorizedEvent)
            throws NotAuthorizedException {

        final TokenAuthorizationStatus tokenAuthorizationStatus =
                checkAuthTokenAuthorizedForEvent(token, authorizedEvent);

        switch (tokenAuthorizationStatus) {
            case TOKEN_VALID:
                return;
            case TOKEN_EXPIRED:
                throw new NotAuthorizedException("Token is expired");
            case TOKEN_DOES_NOT_MATCH_ENDPOINT:
                throw new NotAuthorizedException("Token does not match for endpoint");
            case TOKEN_VALUES_DO_NOT_MATCH:
                throw new NotAuthorizedException("Token values do not match");
            case TOKEN_INVALID:
                throw new NotAuthorizedException("Token is invalid");
            default:
                log.warn("Unexpected token authorization status was found '{}'", tokenAuthorizationStatus);
                throw new NotAuthorizedException("Unknown token authorization status '{}'", tokenAuthorizationStatus);
        }
    }

    @Override
    public TokenAuthorizationStatus checkAuthTokenAuthorizedForEvent(String token, BaseEvent authorizedEvent) {

        try {
            final AuthToken actualToken = encryptionService.decryptObjectFromExternalUse(token, AuthToken.class);

            if (timeService.currentTimeMillisUTC() > actualToken.getExpirationTimeUTC()) {
                //log.debug("Expired since '{}'", actualToken.getExpirationTimeUTC());
                return TokenAuthorizationStatus.TOKEN_EXPIRED;
            }

            final AuthToken expectedToken = createAuthToken(authorizedEvent, 0);

            if (!StringUtils.equals(actualToken.getEventName(), expectedToken.getEventName())) {
                //log.debug("Mismatch for event name: expected '{}' actual '{}'", actualToken.getEventName(), expectedToken.getEventName());
                return TokenAuthorizationStatus.TOKEN_DOES_NOT_MATCH_ENDPOINT;
            }

            for (Map.Entry<String, Object> e : expectedToken.getEventFields().entrySet()) {
                Object expectedValue = e.getValue();
                Object actualValue = actualToken.getEventFields().get(e.getKey());
                if (!Objects.equals(expectedValue, actualValue)) {
                    //log.debug("Mismatch for field '{}': expected '{}' actual '{}'", e.getKey(), expectedValue, actualValue);
                    return TokenAuthorizationStatus.TOKEN_VALUES_DO_NOT_MATCH;
                }
            }
            return TokenAuthorizationStatus.TOKEN_VALID;
        } catch (EncryptionException e) {
            return TokenAuthorizationStatus.TOKEN_INVALID;
        }
    }

    @Override
    public boolean isApiKeyUsable(String apiKey) {
        return StringUtils.length(apiKey) >= MINIMUM_API_KEY_LENGTH
                && !StringUtils.containsWhitespace(apiKey);
    }

    private AuthToken createAuthToken(BaseEvent authorizedEvent, long expirationTimeUTC) {
        Map<String, Object> fields =
                reflectionService.extractPropertyValues(authorizedEvent, "class", "created", "logMessage",
                        "eventRoutingKey");

        return AuthToken.builder()
                .eventName(reflectionService.getModuleName(authorizedEvent.getClass()) + "." +
                        authorizedEvent.getClass().getSimpleName())
                .expirationTimeUTC(expirationTimeUTC)
                .eventFields(fields)
                .build();
    }

}
