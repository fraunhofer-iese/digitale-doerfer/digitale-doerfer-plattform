/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.deletion;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.Getter;

/**
 * Extend this class to register one or more {@link DataDeletionStrategy}s.
 */
public abstract class BaseReferenceDataDeletionHandler implements IReferenceDataDeletionHandler {

    @Autowired
    protected IReflectionService reflectionService;

    @Autowired
    protected ITimeService timeService;

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}),
            value = AccessLevel.PRIVATE)
    private final Collection<SwapReferencesDeletionStrategy<?>>
            cachedSwapEntityDeletionStrategies = registerSwapReferencesDeletionStrategies().stream()
            .peek(s -> s.setDeletionHandler(this))
            .collect(Collectors.toList());

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}),
            value = AccessLevel.PRIVATE)
    private final Collection<DeleteDependentEntitiesDeletionStrategy<?>>
            cachedDeleteEntityDeletionStrategies = registerDeleteDependentEntitiesDeletionStrategies().stream()
            .peek(s -> s.setDeletionHandler(this))
            .collect(Collectors.toList());

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}),
            value = AccessLevel.PRIVATE)
    private final String moduleName = reflectionService.getModuleName(this.getClass());

    /**
     * Implement this method to register strategies of type {@link SwapReferencesDeletionStrategy}
     */
    protected abstract Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies();

    /**
     * Implement this method to register strategies of type {@link DeleteDependentEntitiesDeletionStrategy}
     */
    protected abstract Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies();

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + " [" + getModuleName() + "]";
    }

    @Override
    public Collection<SwapReferencesDeletionStrategy<?>> getSwapReferencesDeletionStrategies() {
        return getCachedSwapEntityDeletionStrategies();
    }

    @Override
    public Collection<DeleteDependentEntitiesDeletionStrategy<?>> getDeleteDependentEntitiesDeletionStrategies() {
        return getCachedDeleteEntityDeletionStrategies();
    }

    /**
     * Functional reference to a method with three parameters and no result.
     */
    @FunctionalInterface
    protected interface TriConsumer<T, U, V> {

        /**
         * Performs this operation on the given arguments.
         *
         * @param t the first input argument
         * @param u the second input argument
         * @param v the third input argument
         */
        void accept(T t, U u, V v);

    }

    @SafeVarargs
    @SuppressWarnings("varargs")
    protected static <T> List<T> unmodifiableList(T... entities) {
        return Collections.unmodifiableList(Arrays.asList(entities));
    }

}
