/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2018 Axel Wickenkamp, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.address.services;

import com.google.common.collect.Multimap;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.address.exceptions.AddressInvalidException;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.Builder;
import lombok.Value;

public interface IAddressService extends IService {

    /**
     * Data container for a location definition as provided by a user input. All combinations of empty and set fields
     * are possible. It is the responsibility of the services processing this data to handle the different cases of
     * empty/non-empty fields.
     */
    @Value
    @Builder
    class LocationDefinition {

        String locationName;
        String locationLookupString;
        GPSLocation gpsLocation;
        String addressStreet;
        String addressZip;
        String addressCity;

    }

    /**
     * Data container for an address definition as provided by a user input. All fields should be present.
     */
    @Value
    @Builder
    class AddressDefinition {

        String name;
        String street;
        String zip;
        String city;
        GPSLocation gpsLocation;

        /**
         * Only required for legacy conversion where address is used as input for address definition
         */
        public static AddressDefinition fromAddress(Address address) {
            return AddressDefinition.builder()
                    .name(address.getName())
                    .street(address.getStreet())
                    .zip(address.getZip())
                    .city(address.getCity())
                    .gpsLocation(address.getGpsLocation())
                    .build();
        }

    }

    enum GPSResolutionStrategy{

        /**
         * Try to lookup with external service, if it fails use the provided, if there is no provided use none
         */
        LOOKEDUP_PROVIDED_NONE,

        /**
         * Try to lookup with external service, if it fails use none
         */
        LOOKEDUP_NONE,

        /**
         * If there is a provided use it, otherwise try to lookup with external service, if it fails use none
         */
        PROVIDED_LOOKEDUP_NONE

    }

    enum AddressFindStrategy {

        /**
         * Find by name, street, zip, city
         */
        NAME_STREET_ZIP_CITY,

        /**
         * Find by name, street, zip, city, GPS
         */
        NAME_STREET_ZIP_CITY_GPS,

    }

    /**
     * Find or create an address as defined by the location definition. If the given location definition is
     * {@code null}, {@code null} is returned. Otherwise, it is required that either the location name,
     * or the location string, or the gps coordinates, or every address field is not empty.
     * If fullAddressRequired = true the result has to be an Address with all fields (including name and GPS location)
     * set.
     *
     * @param locationDefinition
     * @param fullAddressRequired
     *
     * @return
     * @throws AddressInvalidException if fullAddressRequired is true, but the service cannot find information
     *                                 for all fields
     */
    Address findOrCreateAddress(LocationDefinition locationDefinition, boolean fullAddressRequired)
            throws AddressInvalidException;

    /**
     * Find or create an Address. Use the strategies to find the address and resolve the address to a GPS location.
     *
     * @param addressDefinition
     * @param addressFindStrategy
     * @param gpsResolutionStrategy
     * @param fullAddressRequired
     * @return never null
     *
     * @throws AddressInvalidException
     *         if fullAddressRequired is true, but not all fields are provided
     */
    Address findOrCreateAddress(AddressDefinition addressDefinition, AddressFindStrategy addressFindStrategy,
            GPSResolutionStrategy gpsResolutionStrategy, boolean fullAddressRequired) throws AddressInvalidException;

    /**
     * Finds duplicate addresses based on exact string matches.
     *
     * @return A mapping between the oldest address and all duplicates of the
     *         address
     */
    Multimap<Address, Address> findDuplicateAddresses();
    /**
     * Check if the address is valid and throw AddressInvalidException if not
     *
     * @param address
     *
     * @throws AddressInvalidException
     */
    void checkAddress(AddressDefinition address) throws AddressInvalidException;

    /**
     * Check if the address is valid and throw AddressInvalidException if not
     *
     * @param address
     * @param message
     *            the message that should be used in the
     *            {@link AddressInvalidException} that will be thrown if the
     *            address is invalid
     *
     * @throws AddressInvalidException
     */
    void checkAddress(AddressDefinition address, String message) throws AddressInvalidException;

    AddressListEntry storeAddressListEntry(AddressListEntry addressListEntry);

    Address getDefaultUnknownAddress();

}
