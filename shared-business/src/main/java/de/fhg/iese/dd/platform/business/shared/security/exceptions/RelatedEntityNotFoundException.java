/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;

public class RelatedEntityNotFoundException extends NotFoundException {

    private static final long serialVersionUID = -8846821564588032615L;

    public RelatedEntityNotFoundException(Class<? extends NamedEntity> type, String relatedEntityId) {
        super("Could not find related entity of type '" + type.getSimpleName() + "' with id '{}'.", relatedEntityId);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.RELATED_ENTITY_NOT_FOUND;
    }

}
