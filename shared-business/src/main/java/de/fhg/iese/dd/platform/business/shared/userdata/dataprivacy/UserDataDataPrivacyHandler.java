/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.userdata.dataprivacy;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.userdata.services.IUserDataService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;

@Component
public class UserDataDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(UserDataCategory.class, UserDataKeyValueEntry.class);

    @Autowired
    private IUserDataService userDataService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<UserDataKeyValueEntry> userDataKeyValueEntries = userDataService.getAllKeyValueEntries(person);

        if (CollectionUtils.isEmpty(userDataKeyValueEntries)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Nutzereinstellungen", "Einstellungen des Nutzers, App-übergreifend");

        List<UserDataCategory> categories = userDataKeyValueEntries.stream()
                .map(UserDataKeyValueEntry::getCategory)
                .collect(Collectors.toList());

        for (UserDataCategory category : categories) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(category.getName(),
                            "Letzte Änderung " + timeService.toLocalTimeHumanReadable(category.getLastChange()));

            List<UserDataKeyValueEntry> userDataEntries = userDataKeyValueEntries.stream()
                    .filter(u -> u.getCategory().equals(category))
                    .collect(Collectors.toList());

            for (UserDataKeyValueEntry userDataEntry : userDataEntries) {
                subSection.newContent(userDataEntry.getUniqueKey(),
                        "Letzte Änderung " + timeService.toLocalTimeHumanReadable(userDataEntry.getLastChange()),
                        userDataEntry.getValueContent());
            }
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        final List<UserDataCategory> categories = userDataService.getAllCategories(person);
        for (UserDataCategory category : categories) {
            final List<UserDataKeyValueEntry> keyValueEntries =
                    userDataService.getKeyValueEntries(person, category.getName());
            userDataService.removeCategory(person, category.getName());
            for (UserDataKeyValueEntry keyValueEntry : keyValueEntries) {
                personDeletionReport.addEntry(keyValueEntry, DeleteOperation.DELETED);
            }
            personDeletionReport.addEntry(category, DeleteOperation.DELETED);
        }
    }

}
