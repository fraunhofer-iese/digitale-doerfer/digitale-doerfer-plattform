/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

/**
 * Compensate any action that was done when this happened:
 *
 * <ul>
 *
 * <li>consumedEvent:
 *          we consumed this event ...
 * <li>sentEvent:
 *          then we sent out this event ...
 * <li>exception:
 *          and this exception occurred somewhere down the event chain.
 * <li>context:
 *          required to track the event chain if we are going to send out events to compensate what we did.
 * <li>Exception:
 *          if we fail to compensate what we did we can just throw an exception, too.
 *</ul>
 * @param <C> Type of consumed event
 * @param <S> Type of sent event
 */
@FunctionalInterface
public interface CompensateEventFunction<C extends BaseEvent, S extends BaseEvent> {

    /**
     * Compensate any action that was done when this happened:
     *
     * @param consumedEvent
     *          we consumed this event ...
     * @param sentEvent
     *          then we sent out this event ...
     * @param exception
     *          and this exception occurred somewhere down the event chain.
     * @param context
     *          required to track the event chain if we are going to send out events to compensate what we did.
     * @throws Exception
     *          if we fail to compensate what we did we can just throw an exception, too.
     */
    void compensateEvent(C consumedEvent, S sentEvent, Exception exception, EventProcessingContext context) throws Exception;

}
