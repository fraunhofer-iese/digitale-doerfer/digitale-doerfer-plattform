/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.userdata.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;

public interface IUserDataService extends IService {

    //categories
    List<UserDataCategory> getAllCategories(Person owner);
    List<UserDataCategory> getAllCategoriesSince(Person owner, long timeStamp);

    UserDataCategory createCategory(Person owner, String category);
    void removeCategory(Person owner, String category);

    //uniqueKey valueSet
    List<UserDataKeyValueEntry> getAllKeyValueEntries(Person owner);
    List<UserDataKeyValueEntry> getKeyValueEntries(Person owner, String category);
    List<UserDataKeyValueEntry> getKeyValueEntriesSince(Person owner, String category, long timeStamp);

    UserDataKeyValueEntry getValueOfKey(Person owner, String category, String key);

    void setValueOfKey(Person owner, String category, String key, String value);
    void removeKeyValue(Person owner, String category, String key);

}
