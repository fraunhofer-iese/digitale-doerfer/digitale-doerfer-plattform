/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import de.fhg.iese.dd.platform.business.shared.legal.services.ILegalTextService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseLegalTextDataInitializer extends BaseDataInitializer {

    private static final long LEGAL_TEXT_DATE_INACTIVE_NEVER = 253402297200000L; //01.01.10000

    @Autowired
    protected ILegalTextService legalTextService;

    protected LegalText createLegalTextPlatform(LogSummary logSummary, LegalText jsonLegalText) {

        return createLegalText(logSummary, null, jsonLegalText, jsonLegalText.getLegalTextIdentifier());
    }

    protected LegalText createLegalTextAppVariant(LogSummary logSummary, AppVariant appVariant,
            LegalText jsonLegalText) {

        return createLegalText(logSummary, appVariant, jsonLegalText,
                appVariant.getAppVariantIdentifier() + "." + jsonLegalText.getLegalTextIdentifier());
    }

    protected LegalText createLegalText(LogSummary logSummary, AppVariant appVariant, LegalText jsonLegalText,
            String legalTextIdentifier) {
        long dateActive = timestampYYYYMMDDtoMillisUTC(jsonLegalText.getDateActive());
        long dateInactive;
        if (jsonLegalText.getDateInactive() <= 0) {
            //if it is not set, take "forever"
            dateInactive = LEGAL_TEXT_DATE_INACTIVE_NEVER;
        } else {
            dateInactive = timestampYYYYMMDDtoMillisUTC(jsonLegalText.getDateInactive());
        }
        String urlText = legalTextService.copyLegalTextFromDefault(jsonLegalText.getUrlText(), legalTextIdentifier);
        LegalText legalText = LegalText.builder()
                .appVariant(appVariant)
                .legalTextIdentifier(legalTextIdentifier)
                .name(jsonLegalText.getName())
                .legalTextType(jsonLegalText.getLegalTextType())
                .orderValue(jsonLegalText.getOrderValue())
                .required(jsonLegalText.isRequired())
                .dateActive(dateActive)
                .dateInactive(dateInactive)
                .urlText(urlText)
                .build()
                .withConstantId();
        if (appVariant == null) {
            logSummary.info("Created legal text {}({}) for platform", legalText.getName(),
                    legalText.getLegalTextIdentifier());
        } else {
            logSummary.info("Created legal text {}({}) for app variant {}", legalText.getName(),
                    legalText.getLegalTextIdentifier(), appVariant.getAppVariantIdentifier());
        }
        return legalTextService.store(legalText);
    }

}
