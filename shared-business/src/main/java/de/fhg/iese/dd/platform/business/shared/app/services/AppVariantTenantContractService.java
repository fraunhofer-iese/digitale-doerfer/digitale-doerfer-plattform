/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantTenantContractNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantTenantContractRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.stream.Collectors;

@Service
class AppVariantTenantContractService extends BaseEntityService<AppVariantTenantContract> implements IAppVariantTenantContractService {

    @Autowired
    private AppVariantTenantContractRepository appVariantTenantContractRepository;
    @Autowired
    private AppVariantGeoAreaMappingRepository appVariantGeoAreaMappingRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private ITenantService tenantService;

    @Override
    public AppVariantTenantContract findAppVariantTenantContractById(String appVariantTenantContractId)
            throws AppVariantTenantContractNotFoundException {

        return appVariantTenantContractRepository.findById(appVariantTenantContractId)
                .orElseThrow(() -> new AppVariantTenantContractNotFoundException(
                        "AppVariantTenantContract with id {} not found.", appVariantTenantContractId));
    }

    @Override
    public Page<Tenant> getTenantsByContractOrAppVariant(Set<String> contractIds, Set<String> appVariantIds,
            String searchTerm, Pageable pageable) {

        if (CollectionUtils.isEmpty(contractIds) && CollectionUtils.isEmpty(appVariantIds)) {
            //just a tenant search, independent of the contracts
            if (StringUtils.isBlank(searchTerm)) {
                return tenantService.findAll(pageable);
            } else {
                return tenantService.findAllByInfixSearch(searchTerm, pageable);
            }
        } else {
            //these are only tenants that have a contract
            //the sort needs to be adjusted, since the tenant is only a referenced entity
            final Sort adjustedSort = Sort.by(pageable.getSort().stream()
                    .map(o -> new Sort.Order(o.getDirection(), "tenant." + o.getProperty()))
                    .collect(Collectors.toList()));

            if (StringUtils.isBlank(searchTerm)) {
                return appVariantTenantContractRepository.findTenantsByContractOrAppVariant(
                        BaseEntity.toInSafeIdSet(contractIds),
                        BaseEntity.toInSafeIdSet(appVariantIds),
                        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), adjustedSort));
            } else {
                return appVariantTenantContractRepository.findTenantsByContractOrAppVariantAndSearchTerm(
                        BaseEntity.toInSafeIdSet(contractIds),
                        BaseEntity.toInSafeIdSet(appVariantIds),
                        buildInfixSearch(searchTerm),
                        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), adjustedSort));
            }
        }
    }

    @Override
    public Page<AppVariant> getAppVariantsByTenantOrContract(Set<String> tenantIds, Set<String> contractIds,
            String searchTerm, Pageable pageable) {

        if (CollectionUtils.isEmpty(tenantIds) && CollectionUtils.isEmpty(contractIds)) {
            //just an app variant search, independent of the contracts
            if (StringUtils.isBlank(searchTerm)) {
                return appVariantRepository.findAll(pageable);
            } else {
                final String search = buildInfixSearch(searchTerm);
                return appVariantRepository.findAllByNameOrAppVariantIdentifierLike(search, search, pageable);
            }
        } else {
            //these are only app variants that have a contract
            //the sort needs to be adjusted, since the app variant is only a referenced entity
            final Sort adjustedSort = Sort.by(pageable.getSort().stream()
                    .map(o -> new Sort.Order(o.getDirection(), "appVariant." + o.getProperty()))
                    .collect(Collectors.toList()));

            if (StringUtils.isBlank(searchTerm)) {
                return appVariantTenantContractRepository.findAppVariantsByTenantOrContract(
                        BaseEntity.toInSafeIdSet(tenantIds),
                        BaseEntity.toInSafeIdSet(contractIds),
                        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), adjustedSort));
            } else {
                return appVariantTenantContractRepository.findAppVariantsByTenantOrContractAndSearchTerm(
                        BaseEntity.toInSafeIdSet(tenantIds),
                        BaseEntity.toInSafeIdSet(contractIds),
                        buildInfixSearch(searchTerm),
                        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), adjustedSort));
            }
        }
    }

    @Override
    public Page<AppVariantTenantContract> getContractsByTenantOrAppVariant(Set<String> tenantIds,
            Set<String> appVariantIds, String searchTerm, Pageable pageable) {

        if (CollectionUtils.isEmpty(tenantIds) && CollectionUtils.isEmpty(appVariantIds)) {
            //just a contract search without conditions
            if (StringUtils.isBlank(searchTerm)) {
                return appVariantTenantContractRepository.findAll(pageable)
                        .map(c -> c.withMappings(appVariantGeoAreaMappingRepository.findAllByContract(c)));
            } else {
                final String search = buildInfixSearch(searchTerm);
                return appVariantTenantContractRepository.findAllByNotesLike(search, pageable)
                        .map(c -> c.withMappings(appVariantGeoAreaMappingRepository.findAllByContract(c)));
            }
        } else {
            //conditional search
            if (StringUtils.isBlank(searchTerm)) {
                return appVariantTenantContractRepository.findAllByTenantOrAppVariant(
                                BaseEntity.toInSafeIdSet(tenantIds),
                                BaseEntity.toInSafeIdSet(appVariantIds),
                                pageable)
                        .map(c -> c.withMappings(appVariantGeoAreaMappingRepository.findAllByContract(c)));
            } else {
                return appVariantTenantContractRepository.findAllByTenantOrAppVariantAndSearchTerm(
                                BaseEntity.toInSafeIdSet(tenantIds),
                                BaseEntity.toInSafeIdSet(appVariantIds),
                                buildInfixSearch(searchTerm),
                                pageable)
                        .map(c -> c.withMappings(appVariantGeoAreaMappingRepository.findAllByContract(c)));
            }
        }
    }
    
    private static String buildInfixSearch(String infixSearchString) {
        return "%" + infixSearchString + "%";
    }

}
