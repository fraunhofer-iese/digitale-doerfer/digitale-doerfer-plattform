/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsageAreaSelection;

@Component
public class CleanupGeoAreaSelectionsChildrenAdminTask extends BaseAdminTask {

    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IAppService appService;

    @Override
    public String getName() {
        return "CleanupGeoAreaSelectionsChildren";
    }

    @Override
    public String getDescription() {
        return "Cleans up the geo area selections by adding new selections for persons " +
                "that selected geo areas without selecting any of their children. " +
                "This can happen if new geo areas were added down the hierarchy.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {

        //find all invalid selections, these are selections that only selected an intermediate geo area and no children
        Collection<AppVariantUsageAreaSelection> allInvalidSelections = appService.findAllSelectionsWithoutChildren();
        long numberPersons = allInvalidSelections.stream()
                .map(AppVariantUsageAreaSelection::getAppVariantUsage)
                .map(AppVariantUsage::getPerson)
                .distinct()
                .count();

        logSummary.info("Invalid child selections: {}, concerning {} persons", allInvalidSelections.size(),
                numberPersons);

        //to avoid having multiple changes to the same app variant usage we cluster by app variant usage
        Map<AppVariantUsage, List<AppVariantUsageAreaSelection>> invalidSelectionsByAppVariantUsage =
                allInvalidSelections.stream()
                        .collect(Collectors.groupingBy(AppVariantUsageAreaSelection::getAppVariantUsage));

        Map<AppVariant, Set<GeoArea>> availableGeoAreasByAppVariant = new HashMap<>();

        for (Map.Entry<AppVariantUsage, List<AppVariantUsageAreaSelection>> invalidUsage : invalidSelectionsByAppVariantUsage.entrySet()) {

            AppVariant appVariant = invalidUsage.getKey().getAppVariant();

            //we cache the available geo areas, so that we do not query them every time
            Set<GeoArea> availableGeoAreas = availableGeoAreasByAppVariant
                    .computeIfAbsent(appVariant, appService::getAvailableGeoAreas);
            Person person = invalidUsage.getKey().getPerson();

            logSummary.info("Selection for {} by {}", appVariant.getAppVariantIdentifier(), person.toString());
            logSummary.indent();

            Set<GeoArea> geoAreasToAdd = invalidUsage.getValue().stream()
                    //log the invalid root selection
                    .peek(invalidSelection -> logSummary.info("{}: {}",
                            invalidSelection.getId(), invalidSelection.getSelectedArea().toString()))
                    //map it to the root area
                    .map(AppVariantUsageAreaSelection::getSelectedArea)
                    //get the child areas to subscribe
                    .flatMap(geoArea -> geoAreaService.getDirectChildAreas(geoArea).stream())
                    .collect(Collectors.toSet());

            //we just add the missing geo areas to the subscribed ones
            Set<GeoArea> selectedGeoAreas = appService.getSelectedGeoAreas(appVariant, person);

            selectedGeoAreas.addAll(geoAreasToAdd);

            //since it might happen, that either the person has an invalid selection,
            // or we accidentally added some geo areas that are not available in that app variant
            // we remove these invalid selected areas
            if (! availableGeoAreas.containsAll(selectedGeoAreas)){
                Set<GeoArea> invalidSelectedAreas = new HashSet<>(selectedGeoAreas);
                invalidSelectedAreas.removeAll(availableGeoAreas);
                logSummary.warn("Invalid selection, removing {} unavailable geo areas: {}",
                        invalidSelectedAreas.size(), toString(invalidSelectedAreas));
                selectedGeoAreas.removeAll(invalidSelectedAreas);
            }

            logSummary.info("Additionally selecting {} sub geo areas: {}",
                    geoAreasToAdd.size(), toString(geoAreasToAdd));
            if (parameters.isDryRun()) {
                logSummary.info("Nothing changed, only dry run");
            } else {
                appService.selectGeoAreas(appVariant, person, selectedGeoAreas);
                logSummary.info("Successfully changed selected geo areas");
            }
            logSummary.outdent();
        }
    }

    private String toString(Collection<GeoArea> geoAreas) {
        return geoAreas.stream().map(GeoArea::getName).collect(
                Collectors.joining(", ", "{", "}"));
    }

}
