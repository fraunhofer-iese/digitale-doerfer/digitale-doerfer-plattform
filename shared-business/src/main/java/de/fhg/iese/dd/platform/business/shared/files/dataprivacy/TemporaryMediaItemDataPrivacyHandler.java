/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.dataprivacy;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Component
public class TemporaryMediaItemDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(MediaItem.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(TemporaryMediaItem.class);

    @Autowired
    private IMediaItemService mediaItemService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(final Person person, final IDataPrivacyReport dataPrivacyReport) {

        List<TemporaryMediaItem> temporaryMediaItems = mediaItemService.findAllTemporaryItemsByOwner(person);
        if (CollectionUtils.isEmpty(temporaryMediaItems)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Temporäre Bilder",
                        "Von dir temporär hochgeladene Bilder");

        IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                section.newSubSection("Temporäre Bilder", "");

        temporaryMediaItems
                .forEach(tmi -> subSection.newImageContent(tmi.getId(),
                        "Bild hochgeladen am " + timeService.toLocalTimeHumanReadable(tmi.getCreated()),
                        tmi.getMediaItem()));
    }

    @Override
    public void deleteUserData(final Person person, final IPersonDeletionReport personDeletionReport) {

        List<TemporaryMediaItem> temporaryMediaItems = mediaItemService.findAllTemporaryItemsByOwner(person);

        personDeletionReport.addEntries(temporaryMediaItems, DeleteOperation.DELETED);

        mediaItemService.deleteAllTemporaryItemsByOwner(person);
    }

}
