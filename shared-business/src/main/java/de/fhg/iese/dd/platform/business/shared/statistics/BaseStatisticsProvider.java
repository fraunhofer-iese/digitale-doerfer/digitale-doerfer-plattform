/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.statistics.repos.results.ResultValueByGeoAreaId;

public abstract class BaseStatisticsProvider implements IStatisticsProvider {

    @Autowired
    protected IGeoAreaService geoAreaService;

    protected <T extends ResultValueByGeoAreaId> void querySummingStatistics(
            Function<Long, List<T>> query,
            Map<String, Function<T, Long>> valueExtractorByStatisticsId,
            Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        if (!CollectionUtils.containsAny(definition.getStatisticsIds(), valueExtractorByStatisticsId.keySet())) {
            return;
        }
        Map<String, T> countByGeoAreaIdTotal = query.apply(0L).stream()
                .collect(Collectors.toMap(ResultValueByGeoAreaId::getGeoAreaId, Function.identity()));

        Map<String, T> countByGeoAreaIdNew = query.apply(definition.getStartTimeNew()).stream()
                .collect(Collectors.toMap(ResultValueByGeoAreaId::getGeoAreaId, Function.identity()));
        for (GeoAreaStatistics geoAreaInfo : geoAreaInfos) {

            Collection<GeoArea> childAreas = geoAreaService.getAllChildAreasIncludingSelf(geoAreaInfo.getGeoArea());

            for (Map.Entry<String, Function<T, Long>> statisticsIdAndValueExtractor : valueExtractorByStatisticsId.entrySet()) {
                extractSummingStatistics(statisticsIdAndValueExtractor.getKey(),
                        statisticsIdAndValueExtractor.getValue(),
                        countByGeoAreaIdTotal, countByGeoAreaIdNew, childAreas, geoAreaInfo);
            }
        }
    }

    protected static <T> void extractSummingStatistics(
            String statisticsId,
            Function<T, Long> valueExtractor,
            Map<String, T> countByGeoAreaIdTotal,
            Map<String, T> countByGeoAreaIdNew,
            Collection<GeoArea> childAreas,
            GeoAreaStatistics geoAreaInfo) {

        geoAreaInfo.setStatisticsValueLong(statisticsId, StatisticsTimeRelation.TOTAL,
                sumValues(countByGeoAreaIdTotal, childAreas, valueExtractor));
        geoAreaInfo.setStatisticsValueLong(statisticsId, StatisticsTimeRelation.NEW,
                sumValues(countByGeoAreaIdNew, childAreas, valueExtractor));
    }

    protected static <T> long sumValues(Map<String, T> countByGeoAreaId,
            Collection<GeoArea> childAreas, Function<T, Long> valueExtractor) {
        return childAreas.stream()
                .mapToLong(child -> {
                    T valueContainer = countByGeoAreaId.get(child.getId());
                    return valueContainer == null ? 0L : valueExtractor.apply(valueContainer);
                })
                .sum();
    }

}
