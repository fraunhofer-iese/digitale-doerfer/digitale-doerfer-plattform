/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class UserGeneratedContentFlagAlreadyExistsException extends BadRequestException {

    private static final long serialVersionUID = 7091180113549030488L;

    public UserGeneratedContentFlagAlreadyExistsException(String entityId, String type) {
        super("User generated content flag for entity {} of type {} already existing", entityId, type);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS;
    }

}
