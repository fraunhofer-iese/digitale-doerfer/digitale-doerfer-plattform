/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.springframework.lang.Nullable;

import java.util.Collection;
import java.util.Set;

public interface IPushSendService {

    /**
     * Push the message to all endpoints of the person for the app variants of the app of that category.
     * <p/>
     * If the category or the according user setting is loud the message is pushed loud, otherwise it is pushed silent.
     * <pre>
     * relevantAppVariants = AppVariantUsage[person = a.person = Person, category.app = a.appVariant.app].appVariant
     * loudAppVariants = relevantAppVariants[isCategoryLoud(category,a)]
     * silentAppVariants = relevantAppVariants[isCategorySilent(category,a)]
     *
     * for all loudAppVariants :
     *   pushLoud( Endpoints[loudAppVariant][e.person = person] )
     * for all silentAppVariants :
     *   pushSilent( Endpoints[silentAppVariant][e.person = person] )
     * </pre>
     *
     * @param person
     * @param message
     * @param category
     */
    void pushMessageToPersonLoud(Person person, PushMessage message, PushCategory category);

    /**
     * Push the message to all endpoints of the person for the app variants of the app of that category.
     * <p/>
     * If the category or the according user setting is loud the message is pushed loud, otherwise it is pushed silent.
     * <pre>
     * relevantAppVariants = AppVariantUsage[person = a.person = Person, category.app = a.appVariant.app].appVariant
     * loudAppVariants = relevantAppVariants[isCategoryLoud(category,a)]
     * silentAppVariants = relevantAppVariants[isCategorySilent(category,a)]
     *
     * for all loudAppVariants :
     *   pushLoud( Endpoints[loudAppVariant][e.person = person] )
     * for all silentAppVariants :
     *   pushSilent( Endpoints[silentAppVariant][e.person = person] )
     * </pre>
     *
     * @param author the initiator of the push message, must not be {@code null}. No loud push messages are sent to
     *               persons that have the author on the list of blocked persons (they receive a silent message).
     * @param receiver
     * @param message
     * @param category
     */
    void pushMessageToPersonLoud(Person author, Person receiver, PushMessage message, PushCategory category);

    /**
     * Push the message loud to all endpoints of the person of the last recently used app variant. If the user
     * configured the categories silent for this app variant the message is pushed silently.
     * <p/>
     * Push it silent to all other endpoints for the other app variants of the person.
     * <p/>
     * Example usage: Push a general achievement (e.g. registration for x month) to any app that shows achievements.
     * <pre>
     * relevantAppVariantUsages = AppVariantUsage[person = a.person = Person, a.appVariant.app IN categories.app]
     *
     * lastRecentlyUsedAppVariant = relevantAppVariantUsages[max r.lastUsed]
     * categoryForLastRecentlyUsedAppVariant = category of lastRecentlyUsedAppVariant
     *
     * if [isCategoryLoud(categoryForLastRecentlyUsedAppVariant, lastRecentlyUsedAppVariant]
     *   pushLoud(lastRecentlyUsedAppVariant, message, person)
     * else
     *   pushSilent(lastRecentlyUsedAppVariant, message, person)
     *
     * for all other silentAppVariants :
     *   pushSilent( Endpoints[silentAppVariant][e.person = person], categories[c.app = loudAppVariant.app] )
     * </pre>
     *
     * @param person
     * @param message
     * @param categories
     */
    void pushMessageToPersonInLastUsedAppVariantLoud(Person person, PushMessage message, Collection<PushCategory> categories);

    /**
     * Push the message silent to all endpoints of the person for the app of that categories.
     * <pre>
     * relevantAppVariants = AppVariantUsage[person = a.person = Person, category.app = a.appVariant.app].appVariant
     *
     * for all relevantAppVariants :
     *   pushSilent( Endpoints[relevantAppVariant][e.person = person] )
     * </pre>
     *
     * @param person
     * @param message
     * @param categories
     */
    void pushMessageToPersonSilent(Person person, PushMessage message, Collection<PushCategory> categories);

    /**
     * Sends the message loud to all persons (except for the author) that have endpoints registered for app variants
     * that are configured for the tenants and selected one of these geo areas and have the category configured to loud,
     * otherwise they get them silent.
     * <p/>
     * Example usage: Push a notification about new content to all persons except the author
     * <pre>
     * relevantAppVariants = all app variants where one of the geo areas is available
     *
     * for all relevantAppVariants :
     *   pushLoud( Endpoints[relevantAppVariant][category IN e.loudCategory, one of the geoAreas IN e.selectedGeoArea, e.person != author] )
     *   pushSilent( Endpoints[relevantAppVariant][category IN e.silentCategory, one of the geoAreas IN e.selectedGeoArea] )
     * </pre>
     *
     * @param author              the initiator of the push message. No loud push messages are sent to persons that have
     *                            the author on the list of blocked persons (they receive a silent message).
     * @param geoAreas
     * @param onlyForSameHomeArea if true only persons with homeArea == geoArea get this push
     * @param message
     * @param category
     */
    void pushMessageToGeoAreasLoud(@Nullable Person author, Set<GeoArea> geoAreas, boolean onlyForSameHomeArea,
                                   PushMessage message,
                                   PushCategory category);

    /**
     * Sends the message silent to all persons that have endpoints registered for app variants that are configured for
     * the tenants and selected one of these geo areas. It does not matter if the persons have the category configured as loud or
     * silent, the message is sent silently.
     * <pre>
     * relevantAppVariants = all app variants where one of the geo areas is available
     *
     * for all relevantAppVariants :
     *   pushSilent( Endpoints[relevantAppVariant][one of the geoAreas in e.selectedGeoArea] )
     * </pre>
     *
     * @param geoAreas
     * @param onlyForSameHomeArea if true only persons with homeArea in geoArea get this push
     * @param message
     * @param category
     */
    void pushMessageToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea,
                                     PushMessage message,
                                     PushCategory category);

}
