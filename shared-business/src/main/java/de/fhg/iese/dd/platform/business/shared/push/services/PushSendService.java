/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageAreaSelectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.push.config.PushConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
class PushSendService extends BaseService implements IPushSendService {

    @Autowired
    private PushConfig pushConfig;
    @Autowired
    private IExternalPushProvider externalPushProvider;
    @Autowired
    private IAppService appService;
    @Autowired
    private AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IPersonBlockingService personBlockingService;

    @Override
    public void pushMessageToPersonLoud(Person person, PushMessage message, PushCategory category) {

        final List<AppVariant> relevantAppVariants =
                appService.getAppVariantUsages(category.getApp(), person)
                        .stream()
                        .map(AppVariantUsage::getAppVariant)
                        .collect(Collectors.toList());

        final Set<AppVariant> loudAppVariants = new HashSet<>();
        final Set<AppVariant> silentAppVariants = new HashSet<>();
        for (AppVariant appVariant : relevantAppVariants) {
            if (isCategoryLoud(category, person, appVariant)) {
                loudAppVariants.add(appVariant);
            } else {
                silentAppVariants.add(appVariant);
            }
        }

        externalPushProvider.sendMessageToPerson(loudAppVariants, person,
                message, true);

        externalPushProvider.sendMessageToPerson(silentAppVariants, person,
                message, false);
    }

    @Override
    public void pushMessageToPersonLoud(Person author, Person receiver, PushMessage message, PushCategory category) {

        Objects.requireNonNull(author);

        if (personBlockingService.existsBlocking(category.getApp(), receiver, author)) {
            pushMessageToPersonSilent(receiver, message, Collections.singleton(category));
        } else {
            pushMessageToPersonLoud(receiver, message, category);
        }
    }

    @Override
    public void pushMessageToPersonInLastUsedAppVariantLoud(Person person, PushMessage message,
            Collection<PushCategory> categories) {

        for (PushCategory category : categories) {
            pushMessageToPersonInLastUsedAppVariantLoud(person, message, category);
        }
    }

    private void pushMessageToPersonInLastUsedAppVariantLoud(Person person, PushMessage message,
            PushCategory category) {

        final List<AppVariantUsage> appVariantUsages =
                appService.getAppVariantUsagesOrderByLastUsageDesc(category.getApp(), person);

        if (appVariantUsages.isEmpty()) {
            return;
        }

        final Optional<AppVariant> mostRecentlyUsedAppVariantWithLoudPushConfigured = appVariantUsages.stream()
                .map(AppVariantUsage::getAppVariant)
                .filter(appVariant -> isCategoryLoud(category, person, appVariant))
                .findFirst();

        final Set<AppVariant> silentAppVariants;
        if (mostRecentlyUsedAppVariantWithLoudPushConfigured.isPresent()) {

            externalPushProvider.sendMessageToPerson(
                    Collections.singleton(mostRecentlyUsedAppVariantWithLoudPushConfigured.get()),
                    person, message, true);
            silentAppVariants = appVariantUsages
                    .stream()
                    .map(AppVariantUsage::getAppVariant)
                    .filter(appVariant -> mostRecentlyUsedAppVariantWithLoudPushConfigured.get() != appVariant)
                    .collect(Collectors.toSet());
        } else {
            silentAppVariants = appVariantUsages
                    .stream()
                    .map(AppVariantUsage::getAppVariant)
                    .collect(Collectors.toSet());
        }

        externalPushProvider.sendMessageToPerson(silentAppVariants, person, message, false);
    }

    @Override
    public void pushMessageToPersonSilent(Person person, PushMessage message, Collection<PushCategory> categories) {

        final List<App> relevantApps = categories.stream().map(PushCategory::getApp).collect(Collectors.toList());

        final Set<AppVariant> relevantAppVariants = appService.getAppVariantUsages(person)
                .stream()
                .map(AppVariantUsage::getAppVariant)
                .filter(appVariant -> relevantApps.contains(appVariant.getApp()))
                .collect(Collectors.toSet());

        externalPushProvider.sendMessageToPerson(relevantAppVariants, person, message, false);
    }

    @Override
    public void pushMessageToGeoAreasLoud(Person author, Set<GeoArea> geoAreas, boolean onlyForSameHomeArea,
            PushMessage message, PushCategory category) {

        if (isGeoAreasEmptyOrCategoryNull(geoAreas, category)) {
            return;
        }
        Set<AppVariant> relevantAppVariants = getRelevantAppVariants(geoAreas, category);

        for (AppVariant appVariant : relevantAppVariants) {
            /* person ids are included in the loud push if:
            (
                push category setting is loud and person has not configured it as silent
                OR
                push category setting is silent push and person has configured it as loud
            )
            AND
            person does not block the author
            AND
            person is not the author
             */
            Collection<String> personIdsLoudPush;
            if (onlyForSameHomeArea) {
                /*
                AND
                person has the same home area
                 */
                personIdsLoudPush = appVariantUsageAreaSelectionRepository.getLoudPersonIdsForLoudPushToSameHomeArea(
                        appVariant, getLatestLastUsageForPush(), geoAreas, category, author);
            } else {
                personIdsLoudPush = appVariantUsageAreaSelectionRepository.getLoudPersonIdsForLoudPushToGeoArea(
                        appVariant, getLatestLastUsageForPush(), geoAreas, category, author);
            }

            /* person ids are included in the silent push if:
            (
                push category setting is silent and person has not configured it as loud
                OR
                push category setting is loud push and person has configured it as silent
            )
            AND
            person blocks the author
            AND
            person is the author
             */
            Collection<String> personIdsSilentPush;
            if (onlyForSameHomeArea) {
                /*
                AND
                person has the same home area
                 */
                personIdsSilentPush =
                        appVariantUsageAreaSelectionRepository.getSilentPersonIdsForLoudPushToSameHomeArea(
                                appVariant, getLatestLastUsageForPush(), geoAreas, category, author);
            } else {
                personIdsSilentPush = appVariantUsageAreaSelectionRepository.getSilentPersonIdsForLoudPushToGeoArea(
                        appVariant, getLatestLastUsageForPush(), geoAreas, category, author);
            }
            externalPushProvider.sendMessageToPersons(appVariant, personIdsLoudPush, personIdsSilentPush, message);
        }
    }

    @Override
    public void pushMessageToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushMessage message,
            PushCategory category) {

        if (isGeoAreasEmptyOrCategoryNull(geoAreas, category)) {
            return;
        }

        Set<AppVariant> relevantAppVariants = getRelevantAppVariants(geoAreas, category);

        for (AppVariant appVariant : relevantAppVariants) {
            final Collection<String> personIds;
            if (onlyForSameHomeArea) {
                personIds = appVariantUsageAreaSelectionRepository.getSilentPersonIdsForSilentPushToSameHomeArea(
                        appVariant, geoAreas);
            } else {
                personIds = appVariantUsageAreaSelectionRepository.getSilentPersonIdsForSilentPushToGeoArea(
                        appVariant, geoAreas);
            }
            externalPushProvider.sendMessageToPersons(appVariant, Collections.emptySet(), personIds, message);
        }
    }

    private long getLatestLastUsageForPush() {
        return timeService.currentTimeMillisUTC() - pushConfig.getLastAppVariantUsageTime().toMillis();
    }

    private Set<AppVariant> getRelevantAppVariants(Set<GeoArea> geoArea, PushCategory category) {

        //since we know the geo areas, we only take the app variants that are available for these geo areas
        return appService.findAllAppVariants(geoArea, category.getApp());
    }

    private boolean isGeoAreasEmptyOrCategoryNull(Set<GeoArea> geoAreas, PushCategory pushCategory) {
        if (pushCategory == null || pushCategory.getApp() == null) {
            log.error("Push to null category or category without app {} for geo areas {}. " +
                            "Skipping push. Check configuration and data init!",
                    pushCategory,
                    geoAreas);
            return true;
        }
        if (CollectionUtils.isEmpty(geoAreas)) {
            log.error("Push to null or empty geo areas in category {}. " +
                            "Skipping push. Check configuration and data init!",
                    pushCategory);
            return true;
        }
        return false;
    }

    private boolean isCategoryLoud(PushCategory category, Person person, AppVariant appVariant) {

        final PushCategoryUserSetting userSetting =
                pushCategoryService.findPushCategoryUserSetting(category, person, appVariant);
        return userSetting != null ? userSetting.isLoudPushEnabled() : category.isDefaultLoudPushEnabled();
    }

}
