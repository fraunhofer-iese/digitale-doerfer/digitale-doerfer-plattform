/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;

public class PushException extends InternalServerErrorException {

    private static final long serialVersionUID = -8906323037245027096L;

    public PushException(String message, Throwable cause) {
        super(message, cause);
    }

    public PushException(String message) {
        super(message);
     }

    public PushException(String message, Object... arguments) {
        super(message, arguments);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.PUSH_FAILED;
    }

}
