/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import java.util.function.Supplier;

import org.springframework.transaction.PlatformTransactionManager;

import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;

/**
 * Checks caches that are filled at once if they are stale or not.
 * <p>
 * It can use multiple query methods to check if database content has changed. The result of the queries are stored and
 * compared. If they change the {@link #refreshCacheIfStale(Runnable)} returns true and executes the given method to
 * refresh the cache. To avoid executing the queries on every check there is a check interval defined. Within this
 * interval no cache check is executed.
 * <p>
 * NEVER use the same cache checker for multiple caches, even if they share the same query! It will result in not
 * updating caches and hard to reproduce behavior.
 * <p>
 * You can use a non write thread safe cache in this case, since the cache should just be swapped with a new version when
 * it is stale.
 * <p>
 * The default autowired cache uses a check interval of one minute and uses {@link
 * IReferenceDataChangeService#getLastChangeTime()} as cache check.
 *
 * @see #refreshCacheIfStale(Runnable) code examples
 */
public class CacheCheckerAtOnceCache extends CacheChecker {

    CacheCheckerAtOnceCache(final Supplier<Long> currentTimeSupplier,
            PlatformTransactionManager transactionManager, Supplier<Object> defaultCheckQuery) {
        super(currentTimeSupplier, transactionManager, defaultCheckQuery);
    }

    /**
     * Refreshes the cache if it is stale.
     * <p>
     * Stale means that either the cache was never filled or the check interval elapsed and the result of the check
     * queries changed.
     * <p>
     * The cache filler is run only once by a single thread, so it can access non thread write safe caches. A typical
     * usage looks like this:
     *
     * <pre> {@code
     * Map<K,Y> cache = Collections.emptyMap();
     * @Autowired
     * CacheCheckerIncrementalCache cacheChecker;
     *
     * V getCachedValue(K key){
     *      cacheChecker.runRefreshIfCacheIsStale(this::refreshCache);
     *      return cache.get(key);
     * }
     *
     * void refreshCache(){
     *      Map<K,Y> cacheNew = new HashMap();
     *      cacheNew.putAll(...);
     *      //swap the cache to the newest version
     *      cache = cacheNew;
     * }
     * }</pre>
     *
     * @param cacheRefresher code that gets executed if the cache was stale
     *
     * @return true if the cache was stale and got refreshed
     */
    public boolean refreshCacheIfStale(Runnable cacheRefresher) {
        return super.runIfCacheIsStale(cacheRefresher);
    }

}
