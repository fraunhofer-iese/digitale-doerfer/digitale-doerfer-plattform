/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.Collection;

import org.apache.logging.log4j.Level;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity that is only used in data init files and never persisted. It is required to configure team notification
 * channel filters.
 */
@Getter
@Setter
@NoArgsConstructor
public class TeamNotificationChannelFilter extends BaseEntity {

    @Data
    static class NotificationPriorityFilter {

        private Level logLevel;
        private TeamNotificationPriority.Scope scope;
        private TeamNotificationPriority.Impact impact;

    }

    private String topic;
    private String channel;
    private Collection<NotificationPriorityFilter> allowedPriorities;

}
