/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks.services;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import de.fhg.iese.dd.platform.business.shared.admintasks.exceptions.InvalidAdminTaskParametersException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * An admin task is a small code snippet that does some administrative task and
 * is intended to be executed manually in a specific environment.
 * </p>
 * These tasks should replace the need to create admin endpoints for everything that needs to be run only rarely.
 *
 */
public abstract class BaseAdminTask {

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AdminTaskParameters {

        public static final int MIN_PAGE_SIZE = 5;
        public static final int MAX_PAGE_SIZE = 1000;

        public static final int MIN_PARALLELISM_PER_PAGE = 1;
        public static final int MAX_PARALLELISM_PER_PAGE = 15;

        public static final long MIN_DELAY_BETWEEN_PAGES_MILLISECONDS = 200L;
        // --> Not converted with TimeUnit to be usable in annotations
        public static final long MAX_DELAY_BETWEEN_PAGES_MILLISECONDS = 20L * 1000L;

        public static final long MIN_PAGE_TIMEOUT_SECONDS = 1;
        // 7 Minutes --> Not converted with TimeUnit to be usable in annotations
        public static final long MAX_PAGE_TIMEOUT_SECONDS = 7L * 60L;

        @Builder.Default
        private boolean dryRun = true;

        @Builder.Default
        private int pageFrom = 0;

        @Builder.Default
        private int pageTo = 1;

        @Builder.Default
        private int pageSize = 50;

        @Builder.Default
        private int parallelismPerPage = 2;

        @Builder.Default
        private long pageTimeoutSeconds = TimeUnit.SECONDS.toMillis(1L);

        @Builder.Default
        private long delayBetweenPagesMilliseconds = TimeUnit.SECONDS.toMillis(5L);

        public void check() {
            if (getPageFrom() > getPageTo()) {
                throw new InvalidAdminTaskParametersException("PageFrom ({}) must be <= PageTo ({})",
                        getPageFrom(), getPageTo());
            }
            if (getPageSize() < MIN_PAGE_SIZE || getPageSize() > MAX_PAGE_SIZE) {
                throw new InvalidAdminTaskParametersException("PageSize ({}) must be in [{}, {}]",
                        getPageSize(), MIN_PAGE_SIZE, MAX_PAGE_SIZE);
            }
            if (getParallelismPerPage() < MIN_PARALLELISM_PER_PAGE || getParallelismPerPage() > MAX_PARALLELISM_PER_PAGE) {
                throw new InvalidAdminTaskParametersException("ParallelismPerPage ({}) must be in [{}, {}]",
                        getParallelismPerPage(), MIN_PARALLELISM_PER_PAGE, MAX_PARALLELISM_PER_PAGE);
            }
            if (getDelayBetweenPagesMilliseconds() < MIN_DELAY_BETWEEN_PAGES_MILLISECONDS ||
                    getDelayBetweenPagesMilliseconds() > MAX_DELAY_BETWEEN_PAGES_MILLISECONDS) {
                throw new InvalidAdminTaskParametersException("DelayBetweenPagesMilliseconds ({}) must be in [{}, {}]",
                        getDelayBetweenPagesMilliseconds(), MIN_DELAY_BETWEEN_PAGES_MILLISECONDS,
                        MAX_DELAY_BETWEEN_PAGES_MILLISECONDS);
            }
            if (getPageTimeoutSeconds() < MIN_PAGE_TIMEOUT_SECONDS ||
                    getPageTimeoutSeconds() > MAX_PAGE_TIMEOUT_SECONDS) {
                throw new InvalidAdminTaskParametersException("PageTimeoutSeconds ({}) must be in [{}, {}]",
                        getPageTimeoutSeconds(), MIN_PAGE_TIMEOUT_SECONDS,
                        MAX_PAGE_TIMEOUT_SECONDS);
            }
        }
    }

    private AdminTaskService adminTaskService;

    void init(AdminTaskService executor){
        this.adminTaskService = executor;
    }

    /**
     * Combination of name and class name of the admin task in order to identify it.
     *
     * @return
     */
    public final String getId(){
        return getName()+" ("+this.getClass().getCanonicalName()+")";
    }

    /**
     * Name of the admin task that is used to call it. Should be unique.
     *
     * @return
     */
    public abstract String getName();

    /**
     * Human readable description of the admin task.
     *
     * @return
     */
    public abstract String getDescription();

    /**
     * Actual code snippet to be executed.
     *
     * @param logSummary
     *            logger for a brief human readable summary of what has been
     *            done.
     * @param parameters
     * @throws Exception
     *             exception that got thrown in the code snippet. Automatically
     *             gets logged in the summary.
     *
     */
    public abstract void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception;

    protected ExecutorService getParallelExecutor(int parallelism){
        return adminTaskService.getParallelExecutor(parallelism);
    }

    protected <T> List<Future<Void>> processParallel(final Consumer<T> action, final Collection<T> items,
            final LogSummary logSummary, final AdminTaskParameters parameters) throws InterruptedException {
        return processParallelWithResult((T t) -> {
            action.accept(t);
            return null;
        }, items, logSummary, parameters);
    }

    protected <T, R> List<Future<R>> processParallelWithResult(final Function<T, R> action, final Collection<T> items,
        final LogSummary logSummary, final AdminTaskParameters parameters) throws InterruptedException {
        try {

            ExecutorService executorService = getParallelExecutor(parameters.getParallelismPerPage());

            List<Callable<R>> callables = items.stream()
                    .<Callable<R>>map(t -> () -> {
                        try {
                            return action.apply(t);
                        } catch (Exception e) {
                            logSummary.error("Action failed: {} ", e,  e.getMessage());
                            throw e;
                        }
                    })
                    .collect(Collectors.toList());
            List<Future<R>> futures = executorService.invokeAll(callables, parameters.getPageTimeoutSeconds(), TimeUnit.SECONDS);

            long cancelledFutures = futures.stream()
                    .filter(Future::isCancelled)
                    .count();
            if(cancelledFutures > 0){
                logSummary.error("Not all actions on this page could be processed within {} seconds, {} were cancelled",
                        parameters.getPageTimeoutSeconds(), cancelledFutures);
            }
            return futures;
        } catch (InterruptedException e) {
            logSummary.error("Waiting interrupted, canceling task");
            Thread.currentThread().interrupt();
            throw e;
        }
    }

    @FunctionalInterface
    protected interface ThrowingConsumer<T, E extends Throwable> {
        void accept(T t) throws E;
    }

    protected <T, E extends Throwable> void processPages(LogSummary logSummary, AdminTaskParameters parameters,
            Function<PageRequest, Page<T>> pageSupplier, ThrowingConsumer<Page<T>, E> pageConsumer) throws E, InterruptedException {
        int totalPages = Integer.MAX_VALUE;
        boolean first = true;
        for (int i = parameters.getPageFrom(); i <= parameters.getPageTo() &&  i < totalPages; i++) {
            if(!first){
                waitAfterPageProcessing(logSummary, parameters);
            }
            PageRequest pageRequest = PageRequest.of(i, parameters.getPageSize());

            Page<T> page = pageSupplier.apply(pageRequest);
            totalPages = page.getTotalPages();
            logCurrentPage(logSummary, page, parameters);

            pageConsumer.accept(page);
            first = false;
        }
    }

    protected void logCurrentPage(LogSummary logSummary, Page<?> page, AdminTaskParameters parameters){
        logSummary.info("Found total {} items to be processed, " +
                        "now processing page {} out of [{}, {} (max {})] with {} items on page",
                page.getTotalElements(),
                page.getNumber(),
                parameters.getPageFrom(),
                page.getTotalPages()-1,
                parameters.getPageTo(),
                page.getNumberOfElements());
    }

    protected void waitAfterPageProcessing(LogSummary logSummary, AdminTaskParameters parameters) throws InterruptedException {
        try {
            logSummary.info("Waiting for {} ms before processing next page...", parameters.getDelayBetweenPagesMilliseconds());
            TimeUnit.MILLISECONDS.sleep(parameters.getDelayBetweenPagesMilliseconds());
            logSummary.info("...processing next page");
        } catch (InterruptedException e) {
            logSummary.error("Waiting interrupted, canceling task");
            Thread.currentThread().interrupt();
            throw e;
        }
    }

}
