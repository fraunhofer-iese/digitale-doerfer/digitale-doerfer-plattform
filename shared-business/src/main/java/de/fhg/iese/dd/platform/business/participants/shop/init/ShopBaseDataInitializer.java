/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.shop.init;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;

public abstract class ShopBaseDataInitializer extends BaseDataInitializer {

    @Autowired
    protected ITenantService tenantService;
    @Autowired
    protected IPersonService personService;
    @Autowired
    private IShopService shopService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IAppService appService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .processedEntity(Shop.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        final AppVariant platformAppVariant = appService.getPlatformAppVariant();
        for (Tenant tenant : tenantService.findAllOrderedByNameAsc()) {
            List<Shop> shops = loadEntitiesFromDataJsonDefaultSection(tenant, Shop.class);
            for (Shop shop : shops) {
                shop = checkId(shop);
                shop = adjustCreated(shop);
                shop.setTenant(tenant);
                //image
                shop = createImage(Shop::getProfilePicture, Shop::setProfilePicture, shop,
                        FileOwnership.of(platformAppVariant), logSummary, true);
                //addresses
                shop = createAddresses(Shop::getAddresses, Shop::setAddresses, shop, logSummary);
                //opening hours
                shop = createOpeningHours(Shop::getOpeningHours, Shop::setOpeningHours, shop, logSummary);
                boolean useExistingOwner = false;
                if (shop.getOwner() != null) {
                    String ownerId = checkOrLookupId(shop.getOwner().getId());
                    //lookup owner defined in data json
                    Person owner = personService.findPersonById(ownerId);
                    shop.setOwner(owner);
                } else {
                    //lookup existing owner defined in database
                    Shop existingShop = shopService.findShopByIdOrNull(shop.getId());
                    if (existingShop != null) {
                        shop.setOwner(existingShop.getOwner());
                        useExistingOwner = true;
                    }
                }
                createShop(shop, tenant, useExistingOwner, logSummary);
            }
        }
    }

    protected Shop createShop(Shop shop, Tenant tenant, boolean useExistingOwner, LogSummary logSummary){
        Shop savedShop = shopService.store(shop);
        logSummary.info("Created {} in {}", shop.toString(), tenant.toString());
        Person shopOwner = shop.getOwner();
        //give the person the right role, so that we can give special rights to the person
        logSummary.indent();
        try{
            if(useExistingOwner){
                logSummary.warn("No owner defined in data json, using already in database defined owner {}", shopOwner.toString());
            }
            if(shopOwner != null){
                roleService.assignRoleToPerson(shopOwner, ShopOwner.class, shop);
                logSummary.info("Assigned {} as owner of {}", shopOwner.toString(), shop.toString());
            }else{
                logSummary.warn("No owner for {}" ,shop.toString());
            }
        }finally{
            logSummary.outdent();
        }
        return savedShop;
    }

}
