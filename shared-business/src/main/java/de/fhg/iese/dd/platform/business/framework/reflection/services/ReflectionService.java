/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.reflection.services;

import de.fhg.iese.dd.platform.business.framework.reflection.exceptions.ReflectionException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
class ReflectionService extends BaseService implements IReflectionService {

    @Override
    public String getModuleName(Class<?> clazz) {
        return getModuleName(clazz.getName());
    }

    @Override
    public String getModuleName(String className) {

        if (!StringUtils.startsWith(className, "de.fhg.iese.dd.platform")) {
            return "external";
        }
        //find out module name by using "de.fhg.iese.dd.platform.business._shared_.init.XXXDataInitializer
        String[] packageNames = StringUtils.split(className, '.');
        if (packageNames.length < 7) {
            return "unknown";
        }
        return packageNames[6];
    }

    @Override
    public <T> T findConstantByFullyQualifiedName(String fullyQualifiedName, Class<T> expectedType) throws
            ReflectionException {
        //we split up the fqn into two parts, the actual classname and the name of the constant
        String className = StringUtils.substringBeforeLast(fullyQualifiedName, ".");
        String constantName = StringUtils.substringAfterLast(fullyQualifiedName, ".");
        if (StringUtils.isEmpty(className) || StringUtils.isEmpty(constantName)) {
            throw new ReflectionException(
                    "Constant definition '{}' invalid: Could not extract class and constant name", fullyQualifiedName);
        }
        try {
            //lookup the class
            Class<?> definingClass = Class.forName(className);
            //lookup the field
            Field constantField = definingClass.getField(constantName);
            constantField.setAccessible(true);
            //check if the field type is the expected one
            Class<?> constantFieldType = constantField.getType();
            if (!expectedType.isAssignableFrom(constantFieldType)) {
                throw new ReflectionException(
                        "Constant definition '{}' did not match expected type {}, but was {}", fullyQualifiedName,
                        expectedType, constantFieldType);
            }
            //we always assume that the field is static, so the parameter is null
            Object constantValue = constantField.get(null);
            //no need to type check here, because the type of the field was already checked
            @SuppressWarnings("unchecked")
            T constantValueTyped = (T) constantValue;
            return constantValueTyped;
        } catch (ClassNotFoundException e) {
            throw new ReflectionException("Constant definition '{}' invalid: Could not find class {}",
                    fullyQualifiedName, className);
        } catch (NoSuchFieldException e) {
            throw new ReflectionException("Constant definition '{}' invalid: Could not find constant {}",
                    fullyQualifiedName, constantName);
        } catch (SecurityException e) {
            throw new ReflectionException("Constant definition '{}' could not be used, invalid access",
                    fullyQualifiedName);
        } catch (IllegalArgumentException e) {
            throw new ReflectionException("Constant definition '{}' could not be used, needs to be static",
                    fullyQualifiedName);
        } catch (IllegalAccessException e) {
            throw new ReflectionException("Constant definition '{}' could not be used, needs to be public",
                    fullyQualifiedName);
        }
    }

    @Override
    public Map<String, Object> reflectMetamodel(Class<?> classToInspect, int depth, String... propertiesToIgnore) {

        //the depth is just a simple mechanism to avoid stack overflows on cyclic class dependencies
        if (depth < 0) {
            return Collections.singletonMap("<!>", "...omitted...");
        }
        //that's the main magic, the properties and their types are extracted by spring and the java runtime
        PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(classToInspect);
        if (propertyDescriptors.length == 0) {
            return Collections.emptyMap();
        }

        Map<String, Object> propertyMap = new HashMap<>();
        for (PropertyDescriptor property : propertyDescriptors) {
            //ignore selected properties and the "class" property
            if (StringUtils.equalsAny(property.getName(), ArrayUtils.add(propertiesToIgnore, "class"))) {
                continue;
            }
            if (BeanUtils.isSimpleValueType(property.getPropertyType())) {
                //if it is a simple type we can just finish, this needs no further processing
                propertyMap.put(property.getName(), toTypeName(property.getPropertyType()));
            } else {
                //this can be either a collection like property or a complex type
                //unfortunately the type of the property is just a class, so we need to go for the read method
                Type genericPropertyType = property.getReadMethod().getGenericReturnType();
                if (genericPropertyType instanceof ParameterizedType genericParameterizedType) {
                    Type[] typeArguments = genericParameterizedType.getActualTypeArguments();
                    if (Collection.class.isAssignableFrom(property.getPropertyType()) && typeArguments.length == 1) {
                        //if it is a collection like property and has just one type argument, we treat it as an "array"
                        if (typeArguments[0] instanceof Class) {
                            if (BeanUtils.isSimpleValueType((Class<?>) typeArguments[0])) {
                                //a simple collection of simple values
                                propertyMap.put(property.getName(), "[" + toTypeName(typeArguments[0]) + "]");
                            } else {
                                //a collection of complex types, we go into recursion here, to extract these properties
                                propertyMap.put(property.getName() + "[]",
                                        reflectMetamodel((Class<?>) typeArguments[0], depth - 1, propertiesToIgnore
                                        ));
                            }
                        } else {
                            //this is a collection of parameterized types, too complicated ;-)
                            //we just add it as a string, could be extended if needed
                            propertyMap.put(property.getName() + "[]", typeArguments[0].getTypeName());
                        }
                    } else {
                        //this is a parameterized type, which seems to be too complicated for now
                        //just add it as string, too
                        propertyMap.put(property.getName(), genericPropertyType.getTypeName());
                    }
                } else {
                    //non parameterized, complex type, we go into recursion to extract the properties
                    propertyMap.put(property.getName(),
                            reflectMetamodel(property.getPropertyType(), depth - 1, propertiesToIgnore));
                }
            }
        }
        return propertyMap;
    }

    public Map<String, Object> extractPropertyValues(Object object, String... excludeProperties)
            throws ReflectionException {

        Set<String> excludedProperties = Stream.of(excludeProperties).collect(Collectors.toSet());
        PropertyDescriptor[] propertyDescriptors = BeanUtils.getPropertyDescriptors(object.getClass());
        if (propertyDescriptors.length == 0) {
            return Collections.emptyMap();
        }

        Map<String, Object> extractedProperties = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            if (excludedProperties.contains(propertyDescriptor.getName())) {
                continue;
            }
            try {
                Method readMethod = propertyDescriptor.getReadMethod();
                //this is required for inner/nested classes
                readMethod.setAccessible(true);
                Object value = readMethod.invoke(object);
                if (value instanceof BaseEntity) {
                    extractedProperties.put(propertyDescriptor.getName(), ((BaseEntity) value).getId());
                } else {
                    extractedProperties.put(propertyDescriptor.getName(), value);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new ReflectionException("Failed to get property " + propertyDescriptor.getDisplayName(), e);
            }
        }
        return extractedProperties;
    }

    private String toTypeName(Type type) {
        if (type instanceof Class && ((Class<?>) type).isEnum()) {
            //if it is an enum, we add all enum values in an array style
            return Arrays.stream(((Class<?>) type).getEnumConstants())
                    .map(Object::toString)
                    .collect(Collectors.joining(", ", "[", "]"));
        }
        //lazy way to replace the most common wrapper classes with primitives
        return StringUtils.replace(
                StringUtils.replace(type.getTypeName(),
                        "java.lang.String", "string"),
                "java.lang.Integer", "int");
    }

}
