/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.*;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.IUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSummary;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
class UserGeneratedContentFlagService extends BaseEntityService<UserGeneratedContentFlag>
        implements IUserGeneratedContentFlagService {

    private final static Multimap<UserGeneratedContentFlagStatus, UserGeneratedContentFlagStatus>
            ALLOWED_FLAG_STATUS_TRANSITIONS =
            new ImmutableMultimap.Builder<UserGeneratedContentFlagStatus, UserGeneratedContentFlagStatus>()
                    .putAll(UserGeneratedContentFlagStatus.OPEN, UserGeneratedContentFlagStatus.OPEN,
                            UserGeneratedContentFlagStatus.IN_PROGRESS,
                            UserGeneratedContentFlagStatus.ACCEPTED, UserGeneratedContentFlagStatus.REJECTED)
                    .putAll(UserGeneratedContentFlagStatus.IN_PROGRESS, UserGeneratedContentFlagStatus.IN_PROGRESS,
                            UserGeneratedContentFlagStatus.OPEN,
                            UserGeneratedContentFlagStatus.ACCEPTED, UserGeneratedContentFlagStatus.REJECTED)
                    .build();

    @Autowired
    private UserGeneratedContentFlagRepository flagRepository;
    @Autowired
    private UserGeneratedContentFlagStatusRecordRepository flagStatusRecordRepository;
    @Autowired
    private IPluginService pluginService;

    @Override
    @Transactional(readOnly = true)
    public UserGeneratedContentFlag findById(String id) throws UserGeneratedContentFlagNotFoundException {
        return flagRepository.findById(id)
                .orElseThrow(() -> new UserGeneratedContentFlagNotFoundException(id));
    }

    @Override
    @Transactional(readOnly = true)
    public UserGeneratedContentFlag findByIdWithStatusRecords(String id)
            throws UserGeneratedContentFlagNotFoundException {
        return fetchUserGeneratedContentFlagStatusRecord(findById(id));
    }

    @Override
    public void checkEntityTypeOrSubtype(UserGeneratedContentFlag flag, Class<? extends BaseEntity> expectedType) {

        if (StringUtils.isEmpty(flag.getEntityType())) {
            log.error("No flag entity class set for flag '{}'", flag.getId());
            throw UserGeneratedContentFlagHasDifferentTypeException.forInvalidType(flag.getId(), flag.getEntityType());
        }

        final Class<?> entityClass;
        try {
            entityClass = Class.forName(flag.getEntityType());
        } catch (ClassNotFoundException e) {
            log.error("Could not load flag entity class '{}' of flag '{}'", flag.getEntityType(), flag.getId());
            throw UserGeneratedContentFlagHasDifferentTypeException.forInvalidType(flag.getId(), flag.getEntityType());
        }

        if (!expectedType.isAssignableFrom(entityClass)) {
            throw UserGeneratedContentFlagHasDifferentTypeException.forTypeMismatch(flag.getId(), flag.getEntityType(),
                    toEntityType(expectedType));
        }
    }

    @Override
    public String getAdminUiDetailPagePath(UserGeneratedContentFlag flag) {

        Optional<IUserGeneratedContentFlagHandler> flagHandlerOpt = findSuitableUserGeneratedContentFlagHandler(flag);
        String detailPagePath = null;
        if (flagHandlerOpt.isPresent()) {
            IUserGeneratedContentFlagHandler flagHandler = flagHandlerOpt.get();
            detailPagePath = flagHandler.getAdminUiDetailPagePath(flag);
        }
        if (StringUtils.isEmpty(detailPagePath)) {
            String typeRef = StringUtils.lowerCase(StringUtils.substringAfterLast(flag.getEntityType(), "."));
            detailPagePath = "flagged-contents/details/" + typeRef + "/" + flag.getId();
        }
        return detailPagePath;
    }

    @Override
    @Transactional
    public UserGeneratedContentFlag create(BaseEntity entityToFlag, Person flaggedEntityAuthor,
            Tenant tenantOfEntityToFlag, Person flagCreator, String comment)
            throws UserGeneratedContentFlagAlreadyExistsException {

        Objects.requireNonNull(entityToFlag);
        Objects.requireNonNull(flagCreator);

        if (flagRepository.existsByFlagCreatorAndEntityIdAndEntityType(flagCreator,
                entityToFlag.getId(), toEntityType(entityToFlag))) {
            throw new UserGeneratedContentFlagAlreadyExistsException(entityToFlag.getId(),
                    toEntityType(entityToFlag));
        }
        return createNewFlag(
                entityToFlag,
                flaggedEntityAuthor,
                tenantOfEntityToFlag,
                flagCreator,
                comment);
    }

    @Override
    @Transactional
    public UserGeneratedContentFlag createOrUpdateStatus(BaseEntity entityToFlag, @Nullable Person flaggedEntityAuthor, @Nullable Tenant tenantOfEntityToFlag,
                                                         @Nullable Person flagCreator, String comment) {

        Objects.requireNonNull(entityToFlag);

        Optional<UserGeneratedContentFlag> optionalExistingFlag =
                flagRepository.findByFlagCreatorAndEntityIdAndEntityType(flagCreator,
                        entityToFlag.getId(), toEntityType(entityToFlag));

        //noinspection OptionalIsPresent
        if (optionalExistingFlag.isPresent()) {
            return saveFlagAndAddStatus(
                    optionalExistingFlag.get(),
                    flagCreator,
                    comment,
                    UserGeneratedContentFlagStatus.OPEN,
                    timeService.currentTimeMillisUTC());
        } else {
            return createNewFlag(
                    entityToFlag,
                    flaggedEntityAuthor,
                    tenantOfEntityToFlag,
                    flagCreator,
                    comment);
        }
    }

    private UserGeneratedContentFlag createNewFlag(BaseEntity entityToFlag, Person flaggedEntityAuthor,
            Tenant tenantOfEntityToFlag, Person flagCreator, String comment) {

        long timeStamp = timeService.currentTimeMillisUTC();
        return saveFlagAndAddStatus(
                UserGeneratedContentFlag.builder()
                        .entityId(entityToFlag.getId())
                        .entityType(toEntityType(entityToFlag))
                        .entityAuthor(flaggedEntityAuthor)
                        .flagCreator(flagCreator)
                        .tenant(tenantOfEntityToFlag)
                        .build()
                        .withCreated(timeStamp),
                flagCreator,
                comment,
                UserGeneratedContentFlagStatus.OPEN,
                timeStamp);
    }

    @Override
    @Transactional
    public UserGeneratedContentFlag updateStatus(UserGeneratedContentFlag userGeneratedContentFlag, Person initiator,
            UserGeneratedContentFlagStatus newStatus, String comment) throws
            UserGeneratedContentFlagNotFoundException {

        final UserGeneratedContentFlagStatus oldStatus = userGeneratedContentFlag.getStatus();
        if (newStatus != null && !ALLOWED_FLAG_STATUS_TRANSITIONS.containsEntry(oldStatus, newStatus)) {
            throw UserGeneratedContentFlagStatusChangeInvalidException.forInvalidStatusTransition(
                    userGeneratedContentFlag, oldStatus, newStatus);
        }

        UserGeneratedContentFlag changedFlag = saveFlagAndAddStatus(
                userGeneratedContentFlag,
                initiator,
                comment,
                newStatus,
                timeService.currentTimeMillisUTC());
        return fetchUserGeneratedContentFlagStatusRecord(changedFlag);
    }

    private UserGeneratedContentFlag saveFlagAndAddStatus(UserGeneratedContentFlag userGeneratedContentFlag,
            Person initiator, String comment, UserGeneratedContentFlagStatus status, long timeStamp) {

        userGeneratedContentFlag.setStatus(status);
        userGeneratedContentFlag.setLastStatusUpdate(timeStamp);
        UserGeneratedContentFlag savedFlag = flagRepository.save(userGeneratedContentFlag);

        flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                .created(timeStamp)
                .userGeneratedContentFlag(savedFlag)
                .initiator(initiator)
                .status(status)
                .comment(comment)
                .build());

        return savedFlag;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean existsForPersonAndEntity(Person flagCreator, String entityId, String entityType) {
        return flagRepository.existsByFlagCreatorAndEntityIdAndEntityType(flagCreator, entityId, entityType);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<UserGeneratedContentFlag> findByFlagCreator(Person flagCreator) {
        return flagRepository.findByFlagCreator(flagCreator);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<String> findFlaggedEntityIdsByFlagCreator(Person flagCreator) {
        return flagRepository.findEntityIdsByFlagCreator(flagCreator);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserGeneratedContentFlag> findAllByStatusIn(Set<UserGeneratedContentFlagStatus> status,
            Pageable page) {
        Page<UserGeneratedContentFlag> flags;
        if (CollectionUtils.isEmpty(status)) {
            flags = flagRepository.findAll(page);
        } else {
            flags = flagRepository.findAllByStatusIn(status, page);
        }
        fetchUserGeneratedContentFlagStatusRecord(flags.getContent());
        return flags;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserGeneratedContentFlag> findAllByTenantInAndStatusIn(Set<String> tenantIds,
            Set<UserGeneratedContentFlagStatus> status, Pageable page) {
        Page<UserGeneratedContentFlag> flags;
        if (CollectionUtils.isEmpty(status)) {
            flags = flagRepository.findAllByTenantIn(tenantIds, page);
        } else {
            flags = flagRepository.findAllByTenantInAndStatusIn(tenantIds, status, page);
        }
        fetchUserGeneratedContentFlagStatusRecord(flags.getContent());
        return flags;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserGeneratedContentFlagSummary> getFlagSummaryPerFlaggedEntity(HashSet<String> expectedFlaggedEntityTypes, long start, long end, PageRequest pageRequest) {

        return flagRepository.countByStatusFilteredByEntityType(expectedFlaggedEntityTypes, start, end, pageRequest);
    }

    @Override
    @Transactional
    public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) throws
            UserGeneratedContentFlagInvalidException,
            UserGeneratedContentFlagEntityCanNotBeDeletedException,
            UserGeneratedContentFlagEntityDeletionFailedException {

        Optional<IUserGeneratedContentFlagHandler> suitableFlagHandler =
                findSuitableUserGeneratedContentFlagHandler(flag);
        if (suitableFlagHandler.isEmpty()) {
            throw UserGeneratedContentFlagEntityCanNotBeDeletedException.forNoHandlerFound(flag);
        }

        final IUserGeneratedContentFlagHandler flagHandler = suitableFlagHandler.get();
        try {
            return flagHandler.deleteFlaggedEntity(flag);
        } catch (Exception e) {
            throw UserGeneratedContentFlagEntityDeletionFailedException
                    .forExceptionDuringDeletion(flag, flagHandler, e);
        }
    }

    private Optional<IUserGeneratedContentFlagHandler> findSuitableUserGeneratedContentFlagHandler(
            UserGeneratedContentFlag flag) {

        final List<IUserGeneratedContentFlagHandler> userGeneratedContentFlagHandlers =
                pluginService.getPluginInstancesRaw(PluginTarget.of(IUserGeneratedContentFlagHandler.class));

        //this should never happen, since we should always have at least one
        if (CollectionUtils.isEmpty(userGeneratedContentFlagHandlers)) {
            return Optional.empty();
        }
        // get the class of the flagged entity
        if (StringUtils.isEmpty(flag.getEntityType())) {
            log.error("No flag entity class set for flag '{}'", flag.getId());
            throw UserGeneratedContentFlagInvalidException.forInvalidType(flag);
        }
        Class<?> entityClass;
        try {
            entityClass = Class.forName(flag.getEntityType());
        } catch (ClassNotFoundException e) {
            log.error("Could not load flag entity class '{}' of flag '{}'", flag.getEntityType(), flag.getId());
            throw UserGeneratedContentFlagInvalidException.forInvalidType(flag);
        }

        //find matching flag handlers respecting the type hierarchy of the processed entities
        List<IUserGeneratedContentFlagHandler> flagHandlers = userGeneratedContentFlagHandlers.stream()
                .filter(flagHandler -> flagHandler.getProcessedEntities().stream()
                        .anyMatch(pe -> pe.isAssignableFrom(entityClass)))
                .sorted(Comparator.comparing(IUserGeneratedContentFlagHandler::getId))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(flagHandlers)) {
            return Optional.empty();
        }

        //this should not happen, it means that someone implemented more than one handler for the same entity
        if (flagHandlers.size() > 1) {
            log.error("Multiple flag handlers exist for flag {} of type {}", flag.getId(), flag.getEntityType());
            throw UserGeneratedContentFlagInvalidException.forMultipleHandlersFound(flag);
        }
        return Optional.of(flagHandlers.get(0));
    }

    private UserGeneratedContentFlag fetchUserGeneratedContentFlagStatusRecord(
            UserGeneratedContentFlag userGeneratedContentFlag) {
        userGeneratedContentFlag.setUserGeneratedContentFlagStatusRecords(
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(
                        userGeneratedContentFlag));
        return userGeneratedContentFlag;
    }

    private void fetchUserGeneratedContentFlagStatusRecord(
            Collection<UserGeneratedContentFlag> userGeneratedContentFlags) {

        List<UserGeneratedContentFlagStatusRecord> allStatusRecords = flagStatusRecordRepository
                .findAllByUserGeneratedContentFlagIdInOrderByCreatedDesc(
                        BaseEntity.toInSafeIdSet(userGeneratedContentFlags.stream()
                                .map(BaseEntity::getId)
                                .collect(Collectors.toSet())));

        Map<UserGeneratedContentFlag, List<UserGeneratedContentFlagStatusRecord>> flagsToStatus =
                allStatusRecords.stream()
                        .collect(Collectors.groupingBy(
                                UserGeneratedContentFlagStatusRecord::getUserGeneratedContentFlag));

        userGeneratedContentFlags.forEach(flag -> {
            List<UserGeneratedContentFlagStatusRecord> statusRecords = flagsToStatus.get(flag);
            if (!CollectionUtils.isEmpty(statusRecords)) {
                flag.setUserGeneratedContentFlagStatusRecords(statusRecords);
            }
        });
    }

    private String toEntityType(BaseEntity entityToFlag) {
        return toEntityType(entityToFlag.getClass());
    }

    private String toEntityType(Class<? extends BaseEntity> entityClassToFlag) {
        return entityClassToFlag.getName();
    }

}
