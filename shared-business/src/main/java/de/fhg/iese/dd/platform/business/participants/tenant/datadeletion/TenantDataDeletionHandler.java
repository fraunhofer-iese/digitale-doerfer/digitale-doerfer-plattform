/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.tenant.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;

@Component
public class TenantDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private TenantRepository tenantRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        .changedOrDeletedEntity(Tenant.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        .changedOrDeletedEntity(Tenant.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        Tenant tenant = geoAreaToBeDeleted.getTenant();
        if (tenant != null && Objects.equals(tenant.getRootArea(), geoAreaToBeDeleted)) {
            logSummary.info("Root geo area of {} will be changed to {}", tenant, geoAreaToBeUsedInstead);
        } else {
            logSummary.info("No impact on tenants");
        }
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        Tenant tenant = geoAreaToBeDeleted.getTenant();
        if (tenant != null && Objects.equals(tenant.getRootArea(), geoAreaToBeDeleted)) {
            logSummary.info("Root geo area of {} is changed to {}", tenant, geoAreaToBeUsedInstead);
            tenant.setRootArea(geoAreaToBeUsedInstead);
            tenantService.store(tenant);
        } else {
            logSummary.info("No changes of tenants");
        }
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        logSummary.info("Tenant will be deleted");
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {
        logSummary.info("Deleting tenant");
        tenantRepository.delete(tenantToBeDeleted);
    }

}
