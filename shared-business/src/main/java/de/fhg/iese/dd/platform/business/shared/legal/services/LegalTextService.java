/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.legal.services;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.shared.legal.LegalTextStatus;
import de.fhg.iese.dd.platform.business.shared.legal.exceptions.LegalTextNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextAcceptanceRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextRepository;

@Service
class LegalTextService extends BaseEntityService<LegalText> implements ILegalTextService {

    public static final String LEGAL_PATH = "legal/";

    @Autowired
    private LegalTextRepository legalTextRepository;
    @Autowired
    private LegalTextAcceptanceRepository legalTextAcceptanceRepository;
    @Autowired
    public IFileStorage fileStorage;

    @Override
    public List<LegalText> getLegalTextsByIdentifier(List<String> legalTextIdentifiers) {

        //if there are no ids there is no need to look something up
        if (CollectionUtils.isEmpty(legalTextIdentifiers)) {
            return Collections.emptyList();
        }

        List<LegalText> foundLegalTexts =
                query(() -> legalTextRepository.findAllByLegalTextIdentifierInOrderByOrderValue(legalTextIdentifiers));

        //since the ids are unique this check is enough to find out if we got all
        if (foundLegalTexts.size() != legalTextIdentifiers.size()) {

            Set<String> foundIds = foundLegalTexts.stream()
                    .map(LegalText::getLegalTextIdentifier)
                    .collect(Collectors.toSet());

            List<String> notFoundIds = legalTextIdentifiers.stream()
                    .filter(i -> !foundIds.contains(i))
                    .collect(Collectors.toList());

            throw new LegalTextNotFoundException(notFoundIds);
        }
        return foundLegalTexts;
    }

    @Override
    public List<LegalText> getCurrentLegalTexts(AppVariant appVariant) {

        //we only return those legal texts with active < now && inactive > now
        return query(() -> legalTextRepository.findAllByAppVariantOrAppVariantNullAndCurrentlyActiveOrderByOrderValue(
                appVariant, timeService.currentTimeMillisUTC()));
    }

    @Override
    public LegalTextStatus getCurrentLegalTextStatus(AppVariant appVariant, Person person) {

        //what is currently relevant in the app variant?
        List<LegalText> currentLegalTexts = getCurrentLegalTexts(appVariant);

        //what of the relevant legal texts have been accepted
        List<LegalTextAcceptance> acceptances =
                query(() -> legalTextAcceptanceRepository.findAllByPersonAndLegalTextInOrderByTimestampDesc(person,
                        currentLegalTexts));

        //map it to a map
        Map<LegalText, LegalTextAcceptance> acceptedLegalTexts = acceptances.stream()
                .collect(Collectors.toMap(LegalTextAcceptance::getLegalText, Function.identity()));

        //what legal texts have not been accepted?
        List<LegalText> openLegalTexts = currentLegalTexts.stream()
                .filter(l -> !acceptedLegalTexts.containsKey(l))
                .collect(Collectors.toList());

        //are all open legal texts accepted?
        boolean allRequiredAccepted = openLegalTexts.stream()
                .filter(LegalText::isRequired)
                .allMatch(acceptedLegalTexts::containsKey);

        return LegalTextStatus.builder()
                .appVariant(appVariant)
                .person(person)
                .acceptedLegalTexts(acceptedLegalTexts)
                .openLegalTexts(openLegalTexts)
                .allRequiredAccepted(allRequiredAccepted)
                .build();
    }

    @Override
    public String copyLegalTextFromDefault(String defaultFileNameSource, String legalTextIdentifierTarget) {
        String targetFileName = String.format(LEGAL_PATH + "%s.%s", legalTextIdentifierTarget,
                FilenameUtils.getExtension(defaultFileNameSource));
        fileStorage.copyFileFromDefault(defaultFileNameSource, targetFileName);
        return fileStorage.getExternalUrl(targetFileName);
    }

    @Override
    public Map<LegalText, LegalTextAcceptance> acceptLegalTexts(Person person, List<LegalText> legalTextsToAccept) {

        long timestamp = timeService.currentTimeMillisUTC();

        //accept all legal texts at the same timestamp and map it to a map
        return legalTextsToAccept.stream()
                //the save is important to open a transaction
                .map(l -> save(() -> acceptLegalText(person, l, timestamp)))
                .collect(Collectors.toMap(LegalTextAcceptance::getLegalText, Function.identity()));
    }

    private LegalTextAcceptance acceptLegalText(Person person, LegalText legalTextToAccept, long timestamp) {
        //get the current acceptance, if there is one
        LegalTextAcceptance existingLegalTextAcceptance =
                legalTextAcceptanceRepository.findByPersonAndLegalText(person, legalTextToAccept);
        //if it is there, return it
        if (existingLegalTextAcceptance != null) {
            return existingLegalTextAcceptance;
        } else {
            //if none, create it
            return createNewLegalTextAcceptance(person, legalTextToAccept, timestamp);
        }
    }

    protected LegalTextAcceptance createNewLegalTextAcceptance(Person person, LegalText legalTextToAccept,
            long timestamp) {
        LegalTextAcceptance newLegalTextAcceptance = LegalTextAcceptance.builder()
                .person(person)
                .legalText(legalTextToAccept)
                .timestamp(timestamp)
                .build();
        return saveAndRetryOnDataIntegrityViolation(
                () -> legalTextAcceptanceRepository.saveAndFlush(newLegalTextAcceptance),
                () -> legalTextAcceptanceRepository.findByPersonAndLegalText(person, legalTextToAccept));

    }

    @Override
    public List<LegalTextAcceptance> findAllAcceptancesByPersonOrderByTimestamp(Person person) {
        return query(() -> legalTextAcceptanceRepository.findAllByPersonOrderByTimestampDesc(person));
    }

    @Override
    @Transactional
    public long deleteLegalTextsAppVariant(AppVariant appVariant) {
        long deletedAcceptances = legalTextAcceptanceRepository.deleteAllByLegalTextAppVariant(appVariant);
        log.info("Deleted {} legal text acceptances for {}", deletedAcceptances, appVariant);
        long deletedLegalTexts = legalTextRepository.deleteAllByAppVariant(appVariant);
        log.info("Deleted {} legal texts for {}", deletedLegalTexts, appVariant);
        return deletedAcceptances + deletedLegalTexts;
    }

}
