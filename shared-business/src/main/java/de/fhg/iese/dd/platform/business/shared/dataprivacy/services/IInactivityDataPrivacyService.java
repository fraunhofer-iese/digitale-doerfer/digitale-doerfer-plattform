/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig.UserInactivityDeletionConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig.UserInactivityDeletionPreparationConfig;

public interface IInactivityDataPrivacyService extends IService {

    /**
     * If enabled at {@link UserInactivityDeletionPreparationConfig#isEnabled()} sends warning emails to at maximum
     * {@link UserInactivityDeletionPreparationConfig#getBatchSize()} inactive users, who have not logged in for at
     * least {@link UserInactivityDeletionPreparationConfig#getInactiveForDays()} and have their email verified.
     * <p>
     * The warned persons get the status {@link PersonStatus#PENDING_DELETION} set.
     * <p>
     * This status is removed when the person logs in.
     */
    List<Person> sendWarningEmailToInactivePersons();

    /**
     * If enabled at {@link UserInactivityDeletionPreparationConfig#isEnabled()} at maximum {@link
     * UserInactivityDeletionConfig#getBatchSize()}inactive users, who have not logged in for at least {@link
     * UserInactivityDeletionPreparationConfig#getInactiveForDays()} and have not verified their email.
     * <p>
     * The warned persons get the status {@link PersonStatus#PENDING_DELETION} set.
     * <p>
     * This status is removed when the person logs in.
     */
    List<Person> setPendingDeletionOfUnverifiedEmailInactivePersons();

    /**
     * If enabled at {@link UserInactivityDeletionConfig#isEnabled()} deletes at maximum {@link
     * UserInactivityDeletionConfig#getBatchSize()} inactive users, who have been warned for {@link
     * UserInactivityDeletionConfig#getPendingDeletionAndWarnedForDays()} and not logged in after this warning, as
     * determined by having the status {@link PersonStatus#PENDING_DELETION} set.
     * <p>
     * This status would have been removed if the person had logged in.
     */
    List<Person> deletePendingDeletionWarnedPersons();

    /**
     * If enabled at {@link UserInactivityDeletionConfig#isEnabled()} deletes at maximum {@link
     * UserInactivityDeletionConfig#getBatchSize()} inactive users, who have not verified their mail and have been
     * marked for pending deletion for {@link UserInactivityDeletionConfig#getPendingDeletionUnverifiedEmailForDays()}
     * and not logged in afterwards, as determined by having the status {@link PersonStatus#PENDING_DELETION} set.
     * <p>
     * This status would have been removed if the person had logged in.
     */
    List<Person> deletePendingDeletionUnverifiedEmailPersons();

}
