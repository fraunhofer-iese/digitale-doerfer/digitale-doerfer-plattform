/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.statistics;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.IStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@Component
public class AppStatisticsProvider implements IStatisticsProvider {

    public static final String STATISTICS_ID_APP_INFO = "shared.app.info";
    private static final String ABBREVIATION_WHITE_LABEL = "wl";
    private static final String IOS_MANUAL_TEST = "ios-manual-test";
    private static final String APP_VARIANT_IDENTIFIER_START = "de.fhg.iese.dd.";

    @Autowired
    private IAppService appService;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_APP_INFO)
                .machineName("app-info")
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(120)
                .valueFormat("%s")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        if (definition.getStatisticsIds().contains(STATISTICS_ID_APP_INFO)) {

            Collection<AppVariant> appVariants = appService.findAllAppVariants();
            Map<GeoArea, Map<AppVariant, Long>> personCountBySelectedGeoAreaAndAppVariant =
                    appService.getPersonCountBySelectedGeoAreaAndAppVariant();

            final Map<GeoArea, Map<AppVariant, Set<AppVariantGeoAreaMapping>>> appVariantsByGeoArea =
                    appVariants.stream()
                            //from app variants to a stream of (geo area, app variant, app variant mapping)
                            .flatMap(av -> appService.getAvailableGeoAreasWithMappings(av).entrySet().stream()
                                    .flatMap(e -> e.getValue().stream()
                                            .map(m -> Triple.of(e.getKey(), av, m))))
                            //grouping it by geo area
                            .collect(Collectors.groupingBy(Triple::getLeft,
                                    //then grouping by app variant
                                    Collectors.groupingBy(Triple::getMiddle,
                                            //then collecting the mappings
                                            Collectors.mapping(Triple::getRight, Collectors.toSet()))));

            for (GeoAreaStatistics geoAreaInfo : geoAreaInfos) {
                final GeoArea geoArea = geoAreaInfo.getGeoArea();

                final Map<AppVariant, Long> personCountPerAppVariant = personCountBySelectedGeoAreaAndAppVariant
                        .getOrDefault(geoArea, Collections.emptyMap());
                final Map<AppVariant, Set<AppVariantGeoAreaMapping>> appVariantsAndMappings =
                        appVariantsByGeoArea.getOrDefault(geoArea, Collections.emptyMap());

                final Map<App, Map<AppVariant, Set<AppVariantGeoAreaMapping>>> appVariantsByApp =
                        appVariantsAndMappings.entrySet().stream()
                                .map(e -> Triple.of(e.getKey().getApp(), e.getKey(), e.getValue()))
                                //grouping it by app
                                .collect(Collectors.groupingBy(Triple::getLeft,
                                        //then re-creating the map with the mappings that we had before
                                        Collectors.toMap(Triple::getMiddle, Triple::getRight)));

                geoAreaInfo.setStatisticsValueString(STATISTICS_ID_APP_INFO, StatisticsTimeRelation.TOTAL,
                        toGeoAreaAppInfo(personCountPerAppVariant, appVariantsByApp));
            }
        }
    }

    @NonNull
    @SuppressFBWarnings(value = "NP_NONNULL_RETURN_VIOLATION",
            justification = "False positive, impossible to return null")
    private String toGeoAreaAppInfo(Map<AppVariant, Long> personCountPerAppVariant,
            Map<App, Map<AppVariant, Set<AppVariantGeoAreaMapping>>> appVariantsByApp) {

        return appVariantsByApp.entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey().getName()))
                .map(kv -> toAppInfo(personCountPerAppVariant, kv.getKey(), kv.getValue()))
                .collect(Collectors.joining(", "));
    }

    private String toAppInfo(Map<AppVariant, Long> personCountPerAppVariant,
            App app, Map<AppVariant, Set<AppVariantGeoAreaMapping>> appVariants) {

        //these are all the mapping infos, we calculate them upfront to check if they are different
        final Map<AppVariant, String> appVariantsToAppVariantInfo = appVariants.entrySet().stream()
                .filter(e -> !StringUtils.endsWith(e.getKey().getAppVariantIdentifier(), IOS_MANUAL_TEST))
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream()
                        .map(this::toMappingInfo)
                        .sorted()
                        .collect(Collectors.joining(", ", "[", "]"))));

        final long differentInfos = appVariantsToAppVariantInfo.values().stream().distinct().count();
        if (differentInfos == 1) {
            //in this case the infos do not differ, so we can add them at the end
            return app.getName() +
                    appVariants.entrySet().stream()
                            .sorted(Comparator.comparing(e -> e.getKey().getAppVariantIdentifier()))
                            //the ios-manual-tests are filtered out, since they just duplicate all app variants
                            .filter(e -> !StringUtils.endsWith(e.getKey().getAppVariantIdentifier(), IOS_MANUAL_TEST))
                            .map(e -> toAppVariantShortName(e.getKey()) +
                                    toAppVariantSelectionCountInfo(personCountPerAppVariant.get(e.getKey())))
                            .collect(Collectors.joining(", ", "{", "}")) +
                    appVariantsToAppVariantInfo.values().iterator().next();
        } else {
            //the infos differ, so we add them after every app variant
            return app.getName() +
                    appVariants.entrySet().stream()
                            .sorted(Comparator.comparing(e -> e.getKey().getAppVariantIdentifier()))
                            //the ios-manual-tests are filtered out, since they just duplicate all app variants
                            .filter(e -> !StringUtils.endsWith(e.getKey().getAppVariantIdentifier(), IOS_MANUAL_TEST))
                            .map(e -> toAppVariantShortName(e.getKey()) +
                                    appVariantsToAppVariantInfo.get(e.getKey()) +
                                    toAppVariantSelectionCountInfo(personCountPerAppVariant.get(e.getKey())))
                            .collect(Collectors.joining(", ", "{", "}"));
        }
    }

    private String toAppVariantShortName(AppVariant appVariant) {
        //we want to cut here: |->
        // de.fhg.iese.dd.dorffunk.|->demo
        String shortName = StringUtils.substringAfter(StringUtils.removeStart(appVariant.getAppVariantIdentifier(),
                APP_VARIANT_IDENTIFIER_START), ".");
        if (shortName.isEmpty()) {
            //if there is no long name, it must be the white label
            return ABBREVIATION_WHITE_LABEL;
        } else {
            return shortName;
        }
    }

    private String toMappingInfo(AppVariantGeoAreaMapping mapping) {
        final String notes = mapping.getContract().getNotes();
        if (StringUtils.isEmpty(notes) || notes.startsWith("autogenerated")) {
            return mapping.getContract().getTenant().getName();
        } else {
            return mapping.getContract().getTenant().getName() + "(" + notes + ")";
        }
    }

    private String toAppVariantSelectionCountInfo(Long personCount) {
        if (personCount != null && personCount > 0) {
            return String.format("(%4s)", personCount);
        } else {
            return "";
        }
    }

}
