/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

/**
 * Team notification publisher that does not require any configuration.
 * Automatically added as a publisher, gets all team notifications.
 */
public interface IDefaultTeamNotificationPublisher {

    /**
     * Sends a message to the team for the given topic
     *
     * @param topic
     *         the topic of the message
     * @param tenant
     *         the tenant that message is related to
     * @param message
     *         actual message
     */
    void sendTeamNotification(String topic, Tenant tenant, TeamNotificationMessage message);

}
