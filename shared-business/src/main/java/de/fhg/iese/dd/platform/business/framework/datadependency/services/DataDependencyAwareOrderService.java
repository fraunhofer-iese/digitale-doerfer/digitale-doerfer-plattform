/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.datadependency.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.datadependency.IDataDependencyAware;
import de.fhg.iese.dd.platform.business.framework.datadependency.exceptions.DataDependencyUnsatisfiableException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

@Service
class DataDependencyAwareOrderService extends BaseService implements IDataDependencyAwareOrderService {

    @Override
    public <T extends IDataDependencyAware> List<T> orderRequiredBeforeProcessed(
            Collection<T> dataDependencyAwareToSort) {

        LinkedList<T> orderedDataProcessors = new LinkedList<>();
        //we sort the data processors according to the id, so that we always get the same result if there are
        //multiple ways to satisfy the dependency graph.
        //We need to reverse the order here, since we add them from the top of this list
        List<T> dataProcessorsToAdd = dataDependencyAwareToSort.stream()
                .sorted(Comparator.comparing(IDataDependencyAware::getId).reversed())
                .collect(Collectors.toList());
        Set<Class<? extends BaseEntity>> alreadyAvailableData = new HashSet<>();

        boolean dataInitializerAdded = true;
        //as long as we find data initializers that can be added we proceed
        while (dataInitializerAdded) {
            dataInitializerAdded = false;
            //iterate over all not yet added initializers and try to add them
            for (Iterator<T> iterator = dataProcessorsToAdd.iterator(); iterator.hasNext(); ) {
                T dataProcessorToAdd = iterator.next();

                /*
                 * Add it if we have all the required data available
                 * &&
                 * If no other data provider is missing that can create any of the required entities.
                 * The reason why we want to have _all_ data providers that provided a certain entity executed
                 * before we execute a dependent one is that we only check on entity type
                 * level, so we need to wait until all of the entities of that type are created.
                 * Example:
                 * If we require PushCategory we need to wait until _all_ of them are created, because we do not know
                 * which specific one is actually required by the data initializer.
                 */
                if (alreadyAvailableData.containsAll(dataProcessorToAdd.getRequiredEntities())
                        &&
                        dataProcessorsToAdd.stream()
                                .allMatch(d -> Collections.disjoint(d.getProcessedEntities(),
                                        dataProcessorToAdd.getRequiredEntities()))) {

                    //since we can provide all data we can add it at the end
                    orderedDataProcessors.addLast(dataProcessorToAdd);
                    //now we can also provide the data that the just added initializers provides
                    alreadyAvailableData.addAll(dataProcessorToAdd.getProcessedEntities());
                    //we remove it to avoid adding it multiple times
                    iterator.remove();
                    //the iteration brought some change, so we can go into the next round and try with the remaining ones
                    dataInitializerAdded = true;
                }
            }
        }
        if (!dataProcessorsToAdd.isEmpty()) {
            throw new DataDependencyUnsatisfiableException(
                    "Could not find a way to satisfy constraints for data dependency aware: {} of these {} have unsatisfiable dependencies.",
                    dataProcessorsToAdd.toString(),
                    dataDependencyAwareToSort.toString());
        }

        return orderedDataProcessors;
    }

    @Override
    public <T extends IDataDependencyAware> List<T> orderProcessedBeforeRequired(
            Collection<T> dataDependencyAwareToSort) {
        List<T> requiredBeforeProcessed = orderRequiredBeforeProcessed(dataDependencyAwareToSort);
        Collections.reverse(requiredBeforeProcessed);
        return requiredBeforeProcessed;
    }

}
