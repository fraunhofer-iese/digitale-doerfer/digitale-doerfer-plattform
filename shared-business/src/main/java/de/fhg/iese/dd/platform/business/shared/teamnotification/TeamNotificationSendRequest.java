/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Encapsulates the request of sending a specific message to the team.
 */
@Data
@AllArgsConstructor
@Builder
public class TeamNotificationSendRequest {

    private final String channel;
    private final String topic;
    private final Tenant tenant;
    private final TeamNotificationMessage message;

    public String getTenantName(String defaultName) {
        if (tenant != null) {
            return tenant.getName();
        } else {
            return defaultName;
        }
    }

}
