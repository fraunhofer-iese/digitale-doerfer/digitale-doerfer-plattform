/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordChangeNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordWrongException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OAuthEMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.LoginHint;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

/**
 * Service for managing the OAuth users that are stored in the (external!) OAuth authorization service (e.g. auth0)
 */
public interface IOauthManagementService {

    /**
     * creates a user in oauth.
     *
     * @param email         the email to be used for the new user
     * @param password      the password to be used by the new user
     * @param emailVerified the email verification status of the new email
     *
     * @return the oauth id of the user
     *
     * @throws OauthManagementException if the creation fails, e.g. if the user already exists
     */
    String createUser(String email, String password, boolean emailVerified) throws OauthManagementException;

    /**
     * changes the email for a user in oauth and backend.
     *
     * @param oauthId          the user's OAuth id, must not be {@code null} or empty
     * @param newEmail         the new email to be used for this user
     * @param newEmailVerified the email verification status of the new email
     *
     * @throws OauthManagementException        if the update of the user fails
     * @throws EMailChangeNotPossibleException if person has no username/password connection
     * @throws OAuthEMailAlreadyUsedException  if the email is already registered in Auth0
     */
    void changeEmail(String oauthId, String newEmail, boolean newEmailVerified)
            throws EMailChangeNotPossibleException, OauthManagementException, OAuthEMailAlreadyUsedException;

    /**
     * changes the password for a user in oauth.
     *
     * @param oauthId     the user's OAuth id, must not be {@code null} or empty
     * @param newPassword the new password
     *
     * @throws OauthManagementException           if the update of the user fails
     * @throws PasswordChangeNotPossibleException if person has no username/password connection
     * @throws OauthAccountNotFoundException      if the user does not exist
     */
    void changePassword(String oauthId, char[] newPassword)
            throws PasswordChangeNotPossibleException, OauthManagementException, OauthAccountNotFoundException;

    /**
     * changes the password for a user in oauth after a successful login with old password.
     *
     * @param oauthId     the user's OAuth id, must not be {@code null} or empty
     * @param email       the email adress of the user
     * @param oldPassword the old password
     * @param newPassword the new password
     *
     * @throws OauthManagementException           if the update of the user fails
     * @throws PasswordChangeNotPossibleException if person has no username/password connection
     * @throws OauthAccountNotFoundException      if the user does not exist
     * @throws PasswordWrongException             if the login of the user fails
     */
    void changePasswordAndVerifyOldPassword(String oauthId, String email, char[] oldPassword, char[] newPassword)
            throws PasswordChangeNotPossibleException, OauthManagementException, OauthAccountNotFoundException,
            PasswordWrongException;

    /**
     * blocks a user's oauth account to prevent the user from login in. Additionally revokes all refresh tokens.
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthManagementException      if the update of the user fails
     * @throws OauthAccountNotFoundException if the user does not exist
     */
    void blockUser(String oauthId) throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * unblocks an oauth user from manual blocking by an admin
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthManagementException      if the update of the user fails
     * @throws OauthAccountNotFoundException if the user does not exist
     */
    void unblockUser(String oauthId) throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * unblocks an oauth user after an automatically block event, which happens after too many login attempts
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthManagementException      if the update of the user fails
     * @throws OauthAccountNotFoundException if the user does not exist
     */
    void unblockAutomaticallyBlockedUser(String oauthId) throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * triggers the password reset mail for a user. Equivalent to using "forgot password" on the login screen.
     *
     * @param email the user's email, must not be {@code null} or empty
     *
     * @throws OauthManagementException if the operation fails
     */
    void triggerResetPasswordMail(final String email) throws OauthManagementException;

    /**
     * triggers verification mail for a user.
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthManagementException      if the update of the user fails
     * @throws OauthAccountNotFoundException if the user does not exist
     */
    void triggerVerificationMail(final String oauthId) throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * Sets a user's mail verification status
     *
     * @param oauthId  the user's OAuth id, must not be {@code null} or empty
     * @param verified true if the email address of the user is verified
     *
     * @throws OauthManagementException      if the operation fails
     * @throws OauthAccountNotFoundException if the account does not exist
     */
    void setEmailVerified(String oauthId, boolean verified)
            throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * get oauth id for a user by email
     *
     * @param email the user's email, must not be {@code null} or empty
     *
     * @return oauth id of the user with the given email or null if user is not found
     *
     * @throws OauthManagementException if the query fails
     */
    String getOauthIdForUser(String email) throws OauthManagementException;

    /**
     * get oauth id for a custom user (Username-Password-Authentication, NOT a user with a social account)
     *
     * @param email the user's email, must not be {@code null} or empty
     *
     * @return oauth id of the user with the given email or null if user is not found
     *
     * @throws OauthManagementException if the query fails
     */
    String getOauthIdForCustomUser(String email) throws OauthManagementException;

    /**
     * Revokes all refresh tokens of an OAuth user. If the user does not exist, no exception is thrown.
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthManagementException if the operation fails
     */
    void revokeAllRefreshTokens(String oauthId) throws OauthManagementException;

    /**
     * Deletes an OAuth user. If the user does not exist, no exception is thrown.
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @throws OauthAccountNotFoundException if the account does not exist
     * @throws OauthManagementException      if the operation fails
     */
    void deleteUser(String oauthId) throws OauthAccountNotFoundException, OauthManagementException;

    /**
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @return true if the account is blocked for any reason
     *
     * @throws OauthManagementException      if the query fails
     * @throws OauthAccountNotFoundException if the account does not exist
     */
    boolean isBlocked(String oauthId) throws OauthAccountNotFoundException, OauthManagementException;

    /**
     * Queries information about the OAuth account by the oauth id
     *
     * @param oauthId the user's OAuth id, must not be {@code null} or empty
     *
     * @return the account information with this oauth id
     *
     * @throws OauthManagementException      if the query fails
     * @throws OauthAccountNotFoundException if the account does not exist
     */
    OauthAccount queryUserByOauthId(String oauthId) throws OauthManagementException, OauthAccountNotFoundException;

    /**
     * Queries information about the OAuth account by the email address
     *
     * @param email the user's email, must not be {@code null} or empty
     *
     * @return the account information with this email
     *
     * @throws OauthManagementException      if the query fails
     * @throws OauthAccountNotFoundException if the account does not exist
     */
    OauthAccount queryUserByEmail(String email) throws OauthAccountNotFoundException, OauthManagementException;

    /**
     * Queries for all configured oauth clients
     *
     * @return all configured oauth clients
     *
     * @throws OauthManagementException if the query fails
     */
    List<OauthClient> getConfiguredOauthClients() throws OauthManagementException;

    /**
     * Queries information about all used authentication methods (logins) of OAuth account
     *
     * @return login hint containing all authentication methods and if password is changeable
     *
     * @throws OauthManagementException if the query fails
     */
    LoginHint getLoginHint(Person person) throws OauthManagementException;

}
