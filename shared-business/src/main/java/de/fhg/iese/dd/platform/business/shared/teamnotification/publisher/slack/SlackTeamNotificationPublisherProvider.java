/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.slack;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.ITeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.ITeamNotificationPublisherProvider;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;

@Profile("!test & !local")
@Component
public class SlackTeamNotificationPublisherProvider implements ITeamNotificationPublisherProvider {

    @Autowired
    private ITimeService timeService;
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public String getProvidedConnectionType() {
        return "SLACK";
    }

    @Override
    public ITeamNotificationPublisher newPublisher(TeamNotificationConnection connection) {
        return new SlackTeamNotificationPublisher(timeService, restTemplateBuilder, connection);
    }

    @Override
    public void validateConnection(TeamNotificationConnection connection)
            throws TeamNotificationConnectionConfigInvalidException {
        SlackTeamNotificationPublisher.readConnectionConfiguration(connection);
    }

}
