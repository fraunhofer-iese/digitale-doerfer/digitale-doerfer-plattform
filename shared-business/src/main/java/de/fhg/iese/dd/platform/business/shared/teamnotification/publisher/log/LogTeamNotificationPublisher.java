/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.log;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.IDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
class LogTeamNotificationPublisher implements IDefaultTeamNotificationPublisher {

    @Override
    public void sendTeamNotification(String topic, Tenant tenant, TeamNotificationMessage message) {
        final TeamNotificationPriority priority = message.getPriority();
        if (priority.getScope() == TeamNotificationPriority.Scope.APPLICATION) {
            //if the scope is on application level this should just be a debug note in the logs,
            // since the people handling the application issues do not read the logs
            log.debug("Team notification {}: {}\n{}\n-----------------\n{}\n-----------------",
                    priority, topic, tenant != null ? tenant.getName() : "-", message.getPlainText());
        } else {
            log.log(priority.getLogLevel(),
                    "Team notification {}: {}\n{}\n-----------------\n{}\n-----------------",
                    priority, topic, tenant != null ? tenant.getName() : "-", message.getPlainText());
        }
    }

}
