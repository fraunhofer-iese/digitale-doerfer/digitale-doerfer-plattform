/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonBlockConfirmation;
import de.fhg.iese.dd.platform.business.participants.personblocking.events.PersonUnBlockConfirmation;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.config.UserGeneratedContentFlagConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class SharedTeamNotificationEventProcessor extends BaseEventProcessor {

    private static final String TEAM_NOTIFICATION_TOPIC_FLAG = "flag";
    private static final String TEAM_NOTIFICATION_TOPIC_BLOCK = "block";

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Autowired
    private UserGeneratedContentFlagConfig userGeneratedContentFlagConfig;

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @EventProcessing
    private void handleUserGeneratedContentFlagResponse(UserGeneratedContentFlagConfirmation event) {

        BaseEntity flaggedEntity = event.getFlaggedEntity();
        UserGeneratedContentFlag contentFlag = event.getCreatedFlag();
        String separator = StringUtils.repeat('-', 40);
        String separatorBig = StringUtils.repeat('=', 40);
        Person flagCreator = contentFlag.getFlagCreator();
        teamNotificationService.sendTeamNotification(
                contentFlag.getTenant(),
                TEAM_NOTIFICATION_TOPIC_FLAG,
                TeamNotificationMessage.builder()
                        .priority(TeamNotificationPriority.WARN_APPLICATION_MULTIPLE_USERS)
                        .message(
                                //For some reason the two text blocks need to be directly together
                                """
                                        New flag by '{}' `{}` `{}` in '{}' about content created by '{}' `{}` `{}`
                                        ```Reason for flag (provided by user)
                                        {}
                                        {}
                                        {}
                                        Flagged content (short)
                                        {}
                                        {}
                                        {}
                                        Flagged content (detail)
                                        {}
                                        {}
                                        {}
                                        ```""",
                                flagCreator != null
                                        ? flagCreator.getFullName()
                                        : "-no creator-",
                                flagCreator != null
                                        ? flagCreator.getEmail()
                                        : "-no creator-",
                                flagCreator != null
                                        ? flagCreator.getId()
                                        : "-no creator-",
                                contentFlag.getTenant() != null
                                        ? contentFlag.getTenant().getName()
                                        : "-no tenant-",
                                contentFlag.getEntityAuthor() != null
                                        ? contentFlag.getEntityAuthor().getFullName()
                                        : "-no author-",
                                contentFlag.getEntityAuthor() != null
                                        ? contentFlag.getEntityAuthor().getEmail()
                                        : "-no author-",
                                contentFlag.getEntityAuthor() != null
                                        ? contentFlag.getEntityAuthor().getId()
                                        : "-no author-",
                                separator,
                                event.getComment(),
                                separatorBig,
                                separator,
                                event.getFlaggedEntityDescription(),
                                separatorBig,
                                separator,
                                flaggedEntity.toString(),
                                separatorBig)
                        .link(Pair.of("AdministrierBar for details about flag",
                                UriComponentsBuilder.fromHttpUrl(
                                                userGeneratedContentFlagConfig.getAdministrierbarBaseUrl())
                                        .path(userGeneratedContentFlagService.getAdminUiDetailPagePath(contentFlag))
                                        .toUriString()))
                        .build());
    }

    @EventProcessing
    private void handlePersonBlockResponse(PersonBlockConfirmation event) {

        Person blockingPerson = event.getBlockingPerson();
        teamNotificationService.sendTeamNotification(
                blockingPerson.getTenant(),
                TEAM_NOTIFICATION_TOPIC_BLOCK,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "User '{}' blocked '{}' in '{}' for {}",
                blockingPerson.getFullName(),
                event.getBlockedPerson().getFullName(),
                blockingPerson.getTenant().getName(),
                event.getApp().getName());
    }

    @EventProcessing
    private void handlePersonUnBlockResponse(PersonUnBlockConfirmation event) {

        Person blockingPerson = event.getBlockingPerson();
        teamNotificationService.sendTeamNotification(
                blockingPerson.getTenant(),
                TEAM_NOTIFICATION_TOPIC_BLOCK,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "User '{}' unblocked '{}' in '{}' for {}",
                blockingPerson.getFullName(),
                event.getBlockedPerson().getFullName(),
                blockingPerson.getTenant().getName(),
                event.getApp().getName());
    }

}
