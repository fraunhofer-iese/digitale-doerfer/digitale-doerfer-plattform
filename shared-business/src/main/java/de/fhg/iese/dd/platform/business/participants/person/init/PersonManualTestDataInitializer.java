/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.init;

import java.util.Collection;
import java.util.Collections;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
@Profile({"!prod"})
public class PersonManualTestDataInitializer extends PersonBaseDataInitializer implements IManualTestDataInitializer {

    public static final String PERSON_ID_MANUAL_TEST_1 = "f910e447-fb59-4d0f-9996-0f3f7242cb98";
    public static final String PERSON_ID_MANUAL_TEST_2 = "ab5a19c3-1e50-4ab4-aa0e-9ccdc9de86cd";
    public static final String PERSON_ID_MANUAL_TEST_3 = "c1cf600d-2942-4f84-8664-ad39a33c429d";
    public static final String PERSON_ID_MANUAL_TEST_4 = "1671d6f3-01e4-478d-a895-048f3a542316";
    public static final String PERSON_ID_MANUAL_TEST_5 = "077b5bc0-05f3-44d2-ba11-d6e45ed3d06b";

    @Override
    public String getTopic() {
        return "person-manual-test";
    }

    @Override
    public String getScenarioId() {
        //contributes to several scenarios
        return null;
    }

    @Override
    public Collection<String> getDependentTopics() {
        return Collections.emptyList();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary){
        logSummary.info("Manual test persons are not dropped");
    }

}
