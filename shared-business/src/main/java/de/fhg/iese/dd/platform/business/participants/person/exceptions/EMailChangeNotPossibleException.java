/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.AuthorizationException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class EMailChangeNotPossibleException extends AuthorizationException {

    private static final long serialVersionUID = -3444861403271042287L;

    private EMailChangeNotPossibleException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static EMailChangeNotPossibleException forUnknownReason(String email) {
        return new EMailChangeNotPossibleException("The email address could not be changed to '{}'", email);
    }

    public static EMailChangeNotPossibleException forNoUsernamePasswordAccount(String email) {
        return new EMailChangeNotPossibleException(
                "The oAuth account with email '{}' is not a username password account", email);
    }

    public static EMailChangeNotPossibleException forTooManyChanges(String nextPossibleChangeTime) {
        return new EMailChangeNotPossibleException("Next time to change email is: '{}'", nextPossibleChangeTime);
    }

    public static EMailChangeNotPossibleException forInconsistentState(String backendEmail, String auth0Email) {
        return new EMailChangeNotPossibleException("Inconsistent state of email addresses! Backend: '{}'; Auth0: '{}'",
                backendEmail, auth0Email);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE;
    }

}
