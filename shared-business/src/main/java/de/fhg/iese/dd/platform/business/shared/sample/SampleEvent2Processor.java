/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.sample;

import org.springframework.context.annotation.Profile;

import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleResultAEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleResultBEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleResultCEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleTrigger2Event;

@Profile({"!prod"})
@EventProcessor
public class SampleEvent2Processor extends BaseEventProcessor {

    private String getName(){
        return this.getClass().getSimpleName();
    }

    @EventProcessing
    private void handleSampleResultAEvent(SampleResultAEvent event, final EventProcessingContext context) {
        if(event.payload != null && event.payload.contains("ex2")){
            throw new SampleException("Exception in " + getName());
        }
//        notify(event, new SampleResultBEvent(event.payload + "|" + getName()), this::CompensateSampleResultBEvent, context);
//        notify(event, new SampleResultCEvent(event.payload + "|" + getName()), this::CompensateSampleResultCEvent, context);
        notify(new SampleResultBEvent(event.payload + "|" + getName()), context);
        notify(new SampleResultCEvent(event.payload + "|" + getName()), context);
    }

    @EventProcessing
    private void handleSampleTrigger2Event(SampleTrigger2Event event, final EventProcessingContext context) {
        if(event.payload != null && event.payload.contains("ex2")){
            throw new SampleException("Exception in " + getName());
        }
//        notify(event, new SampleResultBEvent(event.payload + "|" + getName()), this::CompensateSampleResultBEvent, context);
//        notify(event, new SampleResultCEvent(event.payload + "|" + getName()), this::CompensateSampleResultCEvent, context);
        notify(new SampleResultBEvent(event.payload + "|" + getName()), context);
        notify(new SampleResultCEvent(event.payload + "|" + getName()), context);
    }

    @SuppressWarnings("unused")
    private void compensateSampleResultBEvent(SampleResultAEvent consumedEvent, SampleResultBEvent sentEvent, Exception ex, Object context ){
        throw new SampleException("CompensateSampleResultBEvent",ex);
    }

    @SuppressWarnings("unused")
    private void compensateSampleResultCEvent(SampleResultAEvent consumedEvent, SampleResultCEvent sentEvent, Exception ex, Object context ){

    }

}
