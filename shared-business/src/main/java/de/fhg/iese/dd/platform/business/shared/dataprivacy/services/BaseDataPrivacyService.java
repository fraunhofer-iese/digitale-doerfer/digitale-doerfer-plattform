/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataDeletionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

public abstract class BaseDataPrivacyService extends BaseService {

    protected static final String TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY = "data-privacy";
    protected static final String TEAM_NOTIFICATION_TOPIC_USER_DELETION = "user-deletion";
    protected static final String TEAM_NOTIFICATION_TOPIC_USER_INACTIVITY = "inactivity";

    @Autowired
    protected ITeamNotificationService teamNotificationService;
    @Autowired
    protected IFileStorage fileStorage;
    @Autowired
    protected DataPrivacyConfig dataPrivacyConfig;
    @Autowired
    protected DataPrivacyWorkflowRepository dataPrivacyWorkflowRepository;
    @Autowired
    protected DataPrivacyDataCollectionRepository dataCollectionRepository;
    @Autowired
    protected DataPrivacyDataCollectionAppVariantResponseRepository
            dataPrivacyDataCollectionAppVariantResponseRepository;
    @Autowired
    protected DataPrivacyDataDeletionRepository dataDeletionRepository;
    @Autowired
    protected DataPrivacyWorkflowAppVariantRepository dataPrivacyWorkflowAppVariantRepository;
    @Autowired
    protected DataPrivacyWorkflowAppVariantStatusRecordRepository dataPrivacyWorkflowAppVariantStatusRecordRepository;
    @Autowired
    protected DataPrivacyWorkflowAppVariantResponseRepository dataPrivacyWorkflowAppVariantResponseRepository;

    protected void setDataPrivacyWorkflowAppVariantStatus(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus newStatus) {
        setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, newStatus, null);
    }

    protected void setDataPrivacyWorkflowAppVariantStatus(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus newStatus, String statusMessage) {

        final Optional<DataPrivacyWorkflowAppVariantStatusRecord> lastStatusRecord =
                dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataPrivacyWorkflowAppVariant);

        final DataPrivacyWorkflowAppVariantStatusRecord statusRecord =
                DataPrivacyWorkflowAppVariantStatusRecord.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyWorkflowAppVariant)
                        .status(newStatus)
                        .build();
        timeService.setCreatedToNow(statusRecord);
        //we need to check if both would get the same timestamp
        if (statusRecord.getCreated() == lastStatusRecord.map(BaseEntity::getCreated).orElse(0L)) {
            //if this happens, we move the new one a ms into the future
            statusRecord.setCreated(statusRecord.getCreated() + 1);
        }
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAndFlush(statusRecord);
        log.debug("Status of data privacy workflow app variant '{}' changed from '{}' to '{}'{}",
                dataPrivacyWorkflowAppVariant.getId(),
                lastStatusRecord
                        .map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus)
                        .map(Object::toString)
                        .orElse("-"),
                newStatus,
                StringUtils.isNotBlank(statusMessage) ? " with status message '" + statusMessage + "'" : "");
    }

}
