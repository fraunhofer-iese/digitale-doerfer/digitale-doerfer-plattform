/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import de.fhg.iese.dd.platform.business.shared.callback.services.IExternalSystemCallbackService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyWorkflowAlreadyFinishedException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyWorkflowResponseRejectedException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

@Service
class ExternalDataPrivacyService extends BaseDataPrivacyService implements IExternalDataPrivacyService {

    private static final String EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION = "/dataprivacy/report";
    private static final String EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION = "/dataprivacy/delete";
    private static final EnumSet<DataPrivacyWorkflowStatus> DATA_PRIVACY_WORKFLOW_STATUSES_RESPONSES_ALLOWED =
            EnumSet.of(DataPrivacyWorkflowStatus.OPEN, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
    private static final EnumSet<DataPrivacyWorkflowAppVariantStatus>
            DATA_PRIVACY_WORKFLOW_APP_VARIANT_STATUSES_UNCHANGED =
            EnumSet.of(DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS,
                    DataPrivacyWorkflowAppVariantStatus.FINISHED,
                    DataPrivacyWorkflowAppVariantStatus.FAILED,
                    DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND);

    @Autowired
    private IExternalSystemCallbackService externalSystemCallbackService;
    @Autowired
    private IParallelismService parallelismService;

    @Override
    public DataPrivacyWorkflowStatus processExternalDataPrivacyWorkflowAppVariant(
            DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant) {

        final Optional<DataPrivacyWorkflowAppVariantStatusRecord> lastStatusRecord =
                dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataPrivacyWorkflowAppVariant);
        DataPrivacyWorkflowAppVariantStatus status =
                lastStatusRecord.map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus)
                        .orElse(DataPrivacyWorkflowAppVariantStatus.OPEN);
        long lastStatusTimeAppVariant =
                lastStatusRecord.map(DataPrivacyWorkflowAppVariantStatusRecord::getCreated)
                        .orElse(dataPrivacyWorkflowAppVariant.getCreated());
        final boolean waitAfterCallsElapsed = timeService.currentTimeMillisUTC() - lastStatusTimeAppVariant >
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getWaitTimeAfterFailedCall().toMillis();

        switch (status) {
            case OPEN:
                //we did not call it so far
                parallelismService.getBlockableExecutor().submit(
                        () -> callExternalSystemForDataPrivacyWorkflow(dataPrivacyWorkflowAppVariant));
                return DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS;
            case CALL_FAILED:
                //we tried to called, but did not reach the system, or it returned !=2xx
                //the first call to the external system failed, so we try again, if enough time is elapsed
                if (waitAfterCallsElapsed) {
                    parallelismService.getBlockableExecutor().submit(
                            () -> callExternalSystemForDataPrivacyWorkflow(dataPrivacyWorkflowAppVariant));
                }
                return DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS;
            case CALL_SUCCESSFUL:
            case IN_PROGRESS:
                //we called successfully, but the system did not yet respond with "FINISHED"
                return DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS;
            case FINISHED:
            case PERSON_NOT_FOUND:
            case FAILED:
                //nothing to do, it's either done or failed, so we don't call the external system anymore
                return DataPrivacyWorkflowStatus.EXTERNALS_FINISHED;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }
    }

    @Override
    public void cancelExternalDataPrivacyWorkflowAppVariant(
            DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant) {

        DataPrivacyWorkflow dataPrivacyWorkflow = dataPrivacyWorkflowAppVariant.getDataPrivacyWorkflow();

        DataPrivacyWorkflowAppVariantStatus status = getCurrentStatus(dataPrivacyWorkflowAppVariant);

        switch (status) {
            case OPEN:
                //it failed because we did not call it
                setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant,
                        DataPrivacyWorkflowAppVariantStatus.FAILED);
                teamNotificationService.sendTeamNotification(
                        dataPrivacyWorkflow.getPerson().getTenant(),
                        TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                        TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                        "Data privacy workflow '{}' of person '{}' failed due to: " +
                                "AppVariant '{}' did not get called within time",
                        dataPrivacyWorkflow.getId(),
                        dataPrivacyWorkflow.getPerson().getId(),
                        dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier());
                return;
            case CALL_FAILED:
                //we tried to called, but did not reach the system, or it returned !=2xx
                final int retryNumber = dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .countStatusRecordsHavingStatus(dataPrivacyWorkflowAppVariant,
                                Collections.singleton(DataPrivacyWorkflowAppVariantStatus.CALL_FAILED));
                setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant,
                        DataPrivacyWorkflowAppVariantStatus.FAILED);
                teamNotificationService.sendTeamNotification(
                        dataPrivacyWorkflow.getPerson().getTenant(),
                        TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                        TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                        "Data privacy workflow '{}' of person '{}' failed due to: " +
                                "AppVariant '{}' could not be called successfully within time, tried {} times",
                        dataPrivacyWorkflow.getId(),
                        dataPrivacyWorkflow.getPerson().getId(),
                        dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier(),
                        retryNumber);
                return;
            case CALL_SUCCESSFUL:
            case IN_PROGRESS:
                //we called successfully, but the system did not yet respond with "FINISHED"
                setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant,
                        DataPrivacyWorkflowAppVariantStatus.FAILED);
                teamNotificationService.sendTeamNotification(
                        dataPrivacyWorkflow.getPerson().getTenant(),
                        TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                        TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                        "Data privacy workflow '{}' of person '{}' failed due to: " +
                                "AppVariant '{}' did not reply within time",
                        dataPrivacyWorkflow.getId(),
                        dataPrivacyWorkflow.getPerson().getId(),
                        dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier());
                return;
            case FINISHED:
            case PERSON_NOT_FOUND:
            case FAILED:
                //nothing to do, it's either done or failed, so we don't call the external system anymore
                return;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }
    }

    @NonNull
    @SuppressFBWarnings(value = "NP_NONNULL_RETURN_VIOLATION",
            justification = "False positive, impossible to return null")
    private DataPrivacyWorkflowAppVariantStatus getCurrentStatus(
            DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant) {
        final Optional<DataPrivacyWorkflowAppVariantStatusRecord> lastStatusRecord =
                dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataPrivacyWorkflowAppVariant);
        return lastStatusRecord.map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus)
                .orElse(DataPrivacyWorkflowAppVariantStatus.OPEN);
    }

    public void callExternalSystemForDataPrivacyWorkflow(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant) {

        final DataPrivacyWorkflow dataPrivacyWorkflow = dataPrivacyWorkflowAppVariant.getDataPrivacyWorkflow();
        final AppVariant appVariant = dataPrivacyWorkflowAppVariant.getAppVariant();
        final int retryNumber = dataPrivacyWorkflowAppVariantStatusRecordRepository
                .countStatusRecordsHavingStatus(dataPrivacyWorkflowAppVariant,
                        Collections.singleton(DataPrivacyWorkflowAppVariantStatus.CALL_FAILED));
        try {
            log.debug("Calling {} for data privacy workflow {} {}, retry {}",
                    appVariant.getAppVariantIdentifier(),
                    dataPrivacyWorkflow.getWorkflowType(),
                    dataPrivacyWorkflow.getId(),
                    retryNumber);
            final ResponseEntity<String> response;
            long start = timeService.currentTimeMillisUTC();
            switch (dataPrivacyWorkflow.getWorkflowType()) {
                case DATA_COLLECTION:
                    response = externalSystemCallbackService.callExternalSystem(
                            appVariant,
                            HttpMethod.POST,
                            EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                            ExternalDataPrivacyReportRequest.builder()
                                    .personId(dataPrivacyWorkflow.getPerson().getId())
                                    .dataCollectionId(dataPrivacyWorkflow.getId())
                                    .retryNumber(retryNumber)
                                    .build(),
                            String.class)
                            .getResponse();
                    break;
                case DATA_DELETION:
                    response = externalSystemCallbackService.callExternalSystem(
                            appVariant,
                            HttpMethod.POST,
                            EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                            ExternalPersonDeleteRequest.builder()
                                    .personId(dataPrivacyWorkflow.getPerson().getId())
                                    .dataDeletionId(dataPrivacyWorkflow.getId())
                                    .retryNumber(retryNumber)
                                    .build(),
                            String.class)
                            .getResponse();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + dataPrivacyWorkflow.getWorkflowType());
            }
            long duration = timeService.currentTimeMillisUTC() - start;
            if (response.getStatusCode().is2xxSuccessful()) {
                handleExternalSystemTriggerCallResponse(dataPrivacyWorkflowAppVariant,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);
                log.debug("External system call to '{}' responded with {} in {} ms: {}",
                        appVariant.getAppVariantIdentifier(),
                        response.getStatusCode().value(),
                        duration,
                        StringUtils.abbreviate(response.getBody(), 255));
            } else {
                handleExternalSystemTriggerCallResponse(dataPrivacyWorkflowAppVariant,
                        DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
                log.warn("External system call to '{}' failed with code {} in {} ms: {}",
                        appVariant.getAppVariantIdentifier(),
                        response.getStatusCode().value(),
                        duration,
                        StringUtils.abbreviate(response.getBody(), 2000));
            }
        } catch (Exception e) {
            handleExternalSystemTriggerCallResponse(dataPrivacyWorkflowAppVariant,
                    DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
            log.warn("External system call to '{}' failed: {}",
                    appVariant.getAppVariantIdentifier(),
                    e.toString());
        }
    }

    private void handleExternalSystemTriggerCallResponse(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus status) {

        final DataPrivacyWorkflowAppVariantStatus currentStatus = getCurrentStatus(dataPrivacyWorkflowAppVariant);
        if (DATA_PRIVACY_WORKFLOW_APP_VARIANT_STATUSES_UNCHANGED.contains(currentStatus)) {
            //in this case the current status of the workflow should not be overwritten, so we put it on the end again
            setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, status, "no status change");
            setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, currentStatus, "no status change");
        } else {
            setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, status);
        }
    }

    @Override
    public void addDataCollectionExternalSystemResponse(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus status, @Nullable String statusMessage, @Nullable MultipartFile file,
            @Nullable String description) {

        final DataPrivacyWorkflow dataPrivacyWorkflow = dataPrivacyWorkflowAppVariant.getDataPrivacyWorkflow();
        if (!(dataPrivacyWorkflow instanceof DataPrivacyDataCollection dataCollection)) {
            throw new InternalServerErrorException(
                    "Called addDataCollectionExternalSystemResponse with data deletion response");
        }
        if (!DATA_PRIVACY_WORKFLOW_STATUSES_RESPONSES_ALLOWED.contains(dataCollection.getStatus())) {
            throw new DataPrivacyWorkflowAlreadyFinishedException(
                    "Data privacy data collection '{}' is already finished.", dataCollection.getId());
        }

        final DataPrivacyDataCollectionAppVariantResponse dataCollectionAppVariantResponse =
                DataPrivacyDataCollectionAppVariantResponse.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyWorkflowAppVariant)
                        .description(StringUtils.isBlank(description) ? null : description)
                        .build();

        statusMessage = StringUtils.defaultString(statusMessage);
        if (file != null && !file.isEmpty()) {
            final AppVariant appVariant = dataPrivacyWorkflowAppVariant.getAppVariant();

            try {
                final byte[] fileBytes = file.getBytes();
                //we have to determine media type and file extension to do the check, so we can get it here
                IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                        checkDataPrivacyDataCollectionResponseFile(
                                "File from " + appVariant.getAppVariantIdentifier() + " for data collection " +
                                        dataCollection.getId(),
                                file.getOriginalFilename(), fileBytes);
                //this is the extension based on the filename, or null if no filename was provided
                String rawExtension = FilenameUtils.getExtension(file.getOriginalFilename());
                String rawFileName =
                        StringUtils.truncate(
                                FilenameUtils.removeExtension(
                                        StringUtils.defaultIfBlank(file.getOriginalFilename(), "file")), 100);

                String mediaType = mediaTypeAndFileExtension.getMediaType();
                String extension;
                if (StringUtils.isEmpty(rawExtension)) {
                    extension = mediaTypeAndFileExtension.getFileExtension();
                } else {
                    extension = rawExtension;
                }

                int fileNumber = dataPrivacyWorkflowAppVariantResponseRepository
                        .countAllByDataPrivacyDataCollectionAppVariant(dataPrivacyWorkflowAppVariant) + 1;
                String fileName = fileStorage.sanitizeFileName(
                        String.format("%s_%d_%s.%s", appVariant.getName(), fileNumber, rawFileName, extension));

                fileStorage.saveFile(fileBytes, mediaType,
                        fileStorage.appendFileName(dataCollection.getInternalFolderPath(), fileName));
                dataCollectionAppVariantResponse.setFileName(fileName);
            } catch (DataPrivacyWorkflowResponseRejectedException e) {
                //we checked the file and it was rejected
                throw e;
            } catch (Exception e) {
                log.error("Failed to process file from " + appVariant + " for data collection " +
                        dataCollection.getId() + ": " + e, e);
                status = DataPrivacyWorkflowAppVariantStatus.FAILED;
                statusMessage += " internal exception while processing file: " + e;
            }
        }

        dataPrivacyWorkflowAppVariantResponseRepository.saveAndFlush(dataCollectionAppVariantResponse);
        if (status == DataPrivacyWorkflowAppVariantStatus.FAILED) {
            teamNotificationService.sendTeamNotification(
                    dataPrivacyWorkflow.getPerson().getTenant(),
                    TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                    "Data collection '{}' of person '{}' failed due to: " +
                            "AppVariant '{}' replied with 'failed' and status message `{}`",
                    dataPrivacyWorkflow.getId(),
                    dataPrivacyWorkflow.getPerson().getId(),
                    dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier(),
                    statusMessage);
        }
        if (status == DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND) {
            teamNotificationService.sendTeamNotification(
                    dataPrivacyWorkflow.getPerson().getTenant(),
                    TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.INFO_TECHNICAL_SINGLE_USER,
                    "Data collection '{}' of person '{}': " +
                            "AppVariant '{}' replied with 'person not found' and status message `{}`",
                    dataPrivacyWorkflow.getId(),
                    dataPrivacyWorkflow.getPerson().getId(),
                    dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier(),
                    statusMessage);
        }
        // update status record of data collection app variant after response is stored
        setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, status, statusMessage);
    }

    private IFileStorage.MediaTypeAndFileExtension checkDataPrivacyDataCollectionResponseFile(String errorMessageStart,
            String originalFileName, byte[] fileContent) throws DataPrivacyWorkflowResponseRejectedException {

        if (StringUtils.isNotEmpty(originalFileName)) {
            //no file name at all would be okay, then we generate it
            String rawExtension = FilenameUtils.getExtension(originalFileName);
            if (!dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getDataCollection()
                    .getAllowedFileExtensions().contains(rawExtension)) {
                throw new DataPrivacyWorkflowResponseRejectedException(
                        "{} has file extension '{}' which is not an allowed extension.",
                        errorMessageStart,
                        rawExtension);
            }
        }
        try {
            IFileStorage.MediaTypeAndFileExtension mediaTypeAndFileExtension =
                    fileStorage.determineMediaTypeAndFileExtension(fileContent, originalFileName);
            String mediaType = mediaTypeAndFileExtension.getMediaType();
            boolean isAllowedMediaType = false;
            for (String allowedMediaType : dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow()
                    .getDataCollection().getAllowedMediaTypes()) {
                //it's possible to specify patterns like image/*
                String mediaTypePrefix = StringUtils.removeEnd(allowedMediaType, "*");
                if (StringUtils.startsWith(mediaType, mediaTypePrefix)) {
                    isAllowedMediaType = true;
                    break;
                }
            }
            if (!isAllowedMediaType) {
                throw new DataPrivacyWorkflowResponseRejectedException(
                        "{} has media type '{}' which is not an allowed media type.",
                        errorMessageStart,
                        mediaType);
            }
            return mediaTypeAndFileExtension;
        } catch (Exception e) {
            throw new DataPrivacyWorkflowResponseRejectedException(
                    "{} could not be read: {}",
                    errorMessageStart,
                    e.toString());
        }
    }

    @Override
    public void addDataDeletionExternalSystemResponse(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus status, @Nullable String statusMessage) {

        final DataPrivacyWorkflow dataPrivacyWorkflow = dataPrivacyWorkflowAppVariant.getDataPrivacyWorkflow();
        if (!(dataPrivacyWorkflow instanceof DataPrivacyDataDeletion)) {
            throw new InternalServerErrorException(
                    "Called addDataDeletionExternalSystemResponse with data collection response");
        }
        if (!DATA_PRIVACY_WORKFLOW_STATUSES_RESPONSES_ALLOWED.contains(dataPrivacyWorkflow.getStatus())) {
            throw new DataPrivacyWorkflowAlreadyFinishedException(
                    "Data privacy data deletion '{}' is already finished.", dataPrivacyWorkflow.getId());
        }

        final DataPrivacyDataDeletionAppVariantResponse dataDeletionAppVariantResponse =
                DataPrivacyDataDeletionAppVariantResponse.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyWorkflowAppVariant)
                        .build();
        dataPrivacyWorkflowAppVariantResponseRepository.saveAndFlush(dataDeletionAppVariantResponse);
        // send team notification when external system replies with status 'failed'
        if (status.equals(DataPrivacyWorkflowAppVariantStatus.FAILED)) {
            teamNotificationService.sendTeamNotification(
                    dataPrivacyWorkflow.getPerson().getTenant(),
                    TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                    TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                    "Data deletion '{}' of person '{}' failed due to: " +
                            "AppVariant '{}' replied with 'failed' and status message `{}`",
                    dataPrivacyWorkflow.getId(),
                    dataPrivacyWorkflow.getPerson().getId(),
                    dataPrivacyWorkflowAppVariant.getAppVariant().getAppVariantIdentifier(),
                    StringUtils.defaultString(statusMessage));
        }
        // update status record of data deletion app variant after response is stored
        setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant, status, statusMessage);
    }

    @Override
    public void addExternalDataCollectionResponsesToReport(DataPrivacyDataCollection dataCollection,
            IDataPrivacyReport dataPrivacyReport) {

        List<DataPrivacyWorkflowAppVariant> dataCollectionAppVariants =
                dataPrivacyWorkflowAppVariantRepository
                        .findAllByDataPrivacyWorkflowOrderByCreatedDesc(dataCollection);
        if (CollectionUtils.isEmpty(dataCollectionAppVariants)) {
            return;
        }
        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("App-Daten", "Daten von Apps");
        for (DataPrivacyWorkflowAppVariant dataCollectionAppVariant : dataCollectionAppVariants) {

            final AppVariant appVariant = dataCollectionAppVariant.getAppVariant();

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(appVariant.getName(), "Benutzung der App " + appVariant.getName());

            // collect all filenames received by external system
            List<DataPrivacyDataCollectionAppVariantResponse> responses =
                    dataPrivacyDataCollectionAppVariantResponseRepository
                            .findAllByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataCollectionAppVariant);
            for (DataPrivacyDataCollectionAppVariantResponse response : responses) {

                if (StringUtils.isNotBlank(response.getFileName())) {
                    if (StringUtils.isNotBlank(response.getDescription())) {
                        //there is a file with a description
                        subSection.newContent("Daten von " + appVariant.getName(), response.getDescription(),
                                response.getFileName());
                    } else {
                        //there is a file, but no description
                        subSection.newContent("Daten von " + appVariant.getName(), "-", response.getFileName());
                    }
                } else {
                    if (StringUtils.isNotBlank(response.getDescription())) {
                        //there is no file, but a description
                        subSection.newContent("Daten von " + appVariant.getName(), "-", response.getDescription());
                    } else {
                        //there no file and no description
                        subSection.newContent("Daten von " + appVariant.getName(), "-", "Keine gespeicherten Daten");
                    }
                }
            }
        }
    }

    @Override
    public void addExternalDataDeletionResponsesToReport(DataPrivacyDataDeletion dataDeletion,
            IPersonDeletionReport personDeletionReport) {

        List<DataPrivacyWorkflowAppVariant> dataDeletionAppVariants =
                dataPrivacyWorkflowAppVariantRepository
                        .findAllByDataPrivacyWorkflowOrderByCreatedDesc(dataDeletion);
        for (DataPrivacyWorkflowAppVariant dataDeletionAppVariant : dataDeletionAppVariants) {
            final Optional<DataPrivacyWorkflowAppVariantStatusRecord> lastStatusRecord =
                    dataPrivacyWorkflowAppVariantStatusRecordRepository
                            .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataDeletionAppVariant);

            final DataPrivacyWorkflowAppVariantStatus status =
                    lastStatusRecord.map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus)
                            .orElse(DataPrivacyWorkflowAppVariantStatus.FAILED);
            if (status == DataPrivacyWorkflowAppVariantStatus.FINISHED) {
                //this is to document that the data stored at this external app variant was deleted
                personDeletionReport.addEntry(dataDeletionAppVariant.getAppVariant(), DeleteOperation.ERASED);
            }
        }
    }

}
