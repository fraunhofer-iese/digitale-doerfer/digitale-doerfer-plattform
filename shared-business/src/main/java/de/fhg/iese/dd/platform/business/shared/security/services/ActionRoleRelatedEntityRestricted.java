/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

public abstract class ActionRoleRelatedEntityRestricted<E extends BaseEntity>
        extends ActionRoleRelatedEntityIdRestricted<PermissionEntityRestricted<E>> {

    @Override
    protected PermissionEntityRestricted<E> createPermission(Set<String> allowedEntityIds, boolean allEntitiesAllowed) {
        return new PermissionEntityRestricted<>(allowedEntityIds, allEntitiesAllowed);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected PermissionEntityRestricted<E> createAllDeniedPermission() {
        return (PermissionEntityRestricted<E>) PermissionEntityRestricted.ALL_DENIED;
    }

}
