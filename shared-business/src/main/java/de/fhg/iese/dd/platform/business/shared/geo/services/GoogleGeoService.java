/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Axel Wickenkamp, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Profile({"!test"})
class GoogleGeoService extends BaseService implements IGeoService {

    @Autowired
    protected ISpatialService spatialService;

    /**
     * If an address is resolved by GPSLocation, it must not be further away than this distance in order to be taken
     * as the returned address.
     */
    private static final double MAX_DISTANCE_FOR_GPS_LOCATION_RESOLUTION_IN_KM = 0.050; //50 meters

    private GeoApiContext geoApiContext;

    @PostConstruct
    protected void initialize(){
        geoApiContext = new GeoApiContext.Builder()
                .apiKey(getConfig().getGoogle().getApiKey())
                .connectTimeout(500, TimeUnit.MILLISECONDS)
                .readTimeout(1500, TimeUnit.MILLISECONDS)
                .retryTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(500, TimeUnit.MILLISECONDS)
                .build();
    }

    @Override
    public GPSLocation getGPSLocation(IAddressService.AddressDefinition address) {
        return getGPSLocation(address.getStreet(), address.getZip(), address.getCity());
    }

    @Override
    public GPSLocation getGPSLocation(String street, String zip, String city) {

        if (StringUtils.isEmpty(street) && StringUtils.isEmpty(city)) {
            return null;
        }

        try {
            String addressQuery = createAddressQuery(street, zip, city);

            GeocodingResult[] geocodeResults = queryGeocoding(addressQuery);

            if (geocodeResults == null || geocodeResults.length == 0) {
                log.debug("Could not geocode address '{}', '{}', '{}'", street, zip, city);
                return null;
            }
            LatLng location = geocodeResults[0].geometry.location;
            return new GPSLocation(location.lat, location.lng);

        } catch (IOException e) {
            log.error("Failed to connect to google when geocoding address '{}', '{}', '{}': {}", street, zip, city, e.toString());
        } catch (ApiException e) {
            log.error("Failed to call google API when geocoding address '{}', '{}', '{}': {}", street, zip, city, e.toString());
        } catch (InterruptedException e) {
            log.error("Interrupted while geocoding address '{}', '{}', '{}': {}", street, zip, city, e.toString());
            Thread.currentThread().interrupt();
        }
        return null;
    }

    @Override
    public Address resolveLocation(String locationName, String locationLookupString) {

        try {
            GeocodingResult[] geocodeResults = queryGeocoding(locationLookupString);

            if (geocodeResults == null || geocodeResults.length == 0) {
                log.debug("Could not geocode address '{}'", locationLookupString);
                return null;
            }
            GeocodingResult geocodeResult = geocodeResults[0];
            LatLng location = geocodeResult.geometry.location;
            GPSLocation gpsLocation = new GPSLocation(location.lat, location.lng);

           return buildAddressFromResult(locationName, gpsLocation, geocodeResult).getLeft();

        } catch (IOException e) {
            log.error("Failed to connect to google when geocoding address '{}': {}", locationLookupString,
                    e.toString());
        } catch (ApiException e) {
            log.error("Failed to call google API when geocoding address '{}': {}", locationLookupString, e.toString());
        } catch (InterruptedException e) {
            log.error("Interrupted while geocoding address '{}': {}", locationLookupString, e.toString());
            Thread.currentThread().interrupt();
        }
        return null;
    }

    private GeocodingResult[] queryGeocoding(String queryString)
            throws ApiException, InterruptedException, IOException {

        GeocodingApiRequest request = GeocodingApi.newRequest(geoApiContext)
                .address(queryString)
                .language("de");

        if (log.isTraceEnabled()) {
            log.trace("Google geocode request '{}'", queryString);
        }

        GeocodingResult[] geocodeResults = request.await();

        if (log.isTraceEnabled()) {
            log.trace("Google geocode result '{}'", (Object) geocodeResults);
        }
        return geocodeResults;
    }

    @Override
    public Address resolveLocation(String locationName, GPSLocation gpsLocation) {

        try {
            GeocodingApiRequest request = GeocodingApi.newRequest(geoApiContext)
                    .latlng(new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude()))
                    .language("de");

            if (log.isTraceEnabled()) {
                log.trace("Google geocode request '{}'", gpsLocation);
            }

            GeocodingResult[] geocodeResults = request.await();

            if (log.isTraceEnabled()) {
                log.trace("Google geocode result '{}'", (Object) geocodeResults);
            }

            if (geocodeResults == null || geocodeResults.length == 0) {
                log.warn("Could not geocode '{}'", gpsLocation);
                return null;
            }
            GeocodingResult geocodeResult = geocodeResults[0];

            final Pair<Address, Boolean> addressAndHasStreetNumber =
                    buildAddressFromResult(locationName, gpsLocation, geocodeResult);
            final Address address = addressAndHasStreetNumber.getLeft();
            final boolean hasStreetNumber = addressAndHasStreetNumber.getRight();

            //Google Maps sometimes returns results that are far away from the actual coordinates
            //In this case, we only keep zip and city, unless the result is only a street without a number
            final GPSLocation foundLocation = new GPSLocation(geocodeResult.geometry.location.lat,
                    geocodeResult.geometry.location.lng);
            final double distance = spatialService.distanceInKM(gpsLocation, foundLocation);
            if (log.isTraceEnabled()) {
                log.trace("Distance of found location to given location in km: {}", distance);
            }
            if (hasStreetNumber && distance > MAX_DISTANCE_FOR_GPS_LOCATION_RESOLUTION_IN_KM) {
                address.setStreet(null);
            }
            address.setGpsLocation(gpsLocation);
            address.setVerified(false);

            return address;
        } catch (IOException e) {
            log.error("Failed to connect to google when geocoding '{}': {}", gpsLocation, e.toString());
        } catch (ApiException e) {
            log.error("Failed to call google API when geocoding '{}': {}", gpsLocation, e.toString());
        } catch (InterruptedException e) {
            log.error("Interrupted while geocoding '{}': {}", gpsLocation, e.toString());
            Thread.currentThread().interrupt();
        }
        return null;
    }

    /**
     * builds an address from google geocode result. The boolean part of the returned pair indicates if the address
     * has a street number
     * @param locationName
     * @param gpsLocation
     * @param geocodeResult
     * @return never {@code null}
     */
    private Pair<Address, Boolean> buildAddressFromResult(String locationName, GPSLocation gpsLocation,
            GeocodingResult geocodeResult) {

        final Map<AddressComponentType, String> addressComponentTypeToName = new HashMap<>();
        for (AddressComponent addressComponent : geocodeResult.addressComponents) {
            for (AddressComponentType type : addressComponent.types) {
                addressComponentTypeToName.put(type, addressComponent.longName);
            }
        }

        final String streetName = addressComponentTypeToName.get(AddressComponentType.ROUTE);
        final String streetNumber = addressComponentTypeToName.get(AddressComponentType.STREET_NUMBER);

        String street;
        boolean addressHasStreetNumber = false;

        if (StringUtils.isNotEmpty(streetName)) {
            //Using geocodeResult.partialMatch to sort out partial addresses seems promising.
            //However, in all my tests, it was false, even for "unnamed roads". So we only test for unnamed road.
            if ("unnamed road".equalsIgnoreCase(streetName)) {
                street = null;
            } else if (StringUtils.isEmpty(streetNumber)) {
                street = streetName;
            } else {
                //street number is not empty
                street = streetName + " " + streetNumber;
                addressHasStreetNumber = true;
            }
        } else {
            street = null;
        }
        String city = addressComponentTypeToName.get(AddressComponentType.LOCALITY);
        String zip = addressComponentTypeToName.get(AddressComponentType.POSTAL_CODE);
        return Pair.of(Address.builder()
                        .name(locationName)
                        .street(street)
                        .zip(zip)
                        .city(city)
                        .gpsLocation(gpsLocation)
                        .verified(true)
                        .build(),
                addressHasStreetNumber);
    }

    private String createAddressQuery(String street, String zip, String city) {
        return street + ", " + zip + " " + city;
    }

    @Override
    public Object getDirections(GPSLocation from, GPSLocation to) {
        try {
            return DirectionsApi
                    .newRequest(geoApiContext)
                    .origin(toLatLng(from))
                    .destination(toLatLng(to))
                    .units(Unit.METRIC)
                    .language("de")
                    .await();
        } catch (Exception ex) {
            log.error("Failed to find directions from " + from + " to " + to, ex);
            return null;
        }
    }

    private static LatLng toLatLng(GPSLocation loc) {
        return new LatLng(loc.getLatitude(), loc.getLongitude());
    }

}
