/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RequiredArgsConstructor
@Log4j2
public class PersonDeletionReport implements IPersonDeletionReport {

    private final List<IDeletedEntity> entries = new ArrayList<>();

    @Getter
    @AllArgsConstructor
    private static class Entry implements IDeletedEntity {

        private final Class<? extends BaseEntity> entityClass;

        private final String id;

        private final DeleteOperation operation;

    }

    public void addEntry(BaseEntity entity, DeleteOperation operation) {
        if (entity == null) {
            log.warn("addEntry called with null entity");
            return;
        }
        final Entry entry = new Entry(entity.getClass(), entity.getId(), operation);
        synchronized (entries) {
            entries.add(entry);
        }
    }

    @Override
    public void addEntries(Iterable<? extends BaseEntity> entities, DeleteOperation operation) {
        entities.forEach(entity -> addEntry(entity, operation));
    }

    @Override
    public List<IDeletedEntity> getDeletedEntities() {
        return new ArrayList<>(entries);
    }

    @Override
    public String toString() {
        return "PersonDeletionReport:\n" + entries.stream()
                .map(this::toHumanReadableEntry)
                .collect(Collectors.joining(",\n"));
    }

    private String toHumanReadableEntry(IDeletedEntity entry) {
        return StringUtils.removeStart(StringUtils.removeStart(entry.getEntityClass().getName(),
                "de.fhg.iese.dd.platform.business.shared.dataprivacy.reports."),
                "de.fhg.iese.dd.platform.datamanagement.") + ":" + entry.getId() + ":" +
                entry.getOperation();
    }

}
