/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Marker;
import org.springframework.beans.BeanUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.reflect.TypeUtils.isAssignable;

@Getter
@Setter
public abstract class BaseEvent {

    private static final ParameterizedType TYPE_COLLECTION_BASE_ENTITY =
            TypeUtils.parameterize(Collection.class,
                    TypeUtils.wildcardType().withUpperBounds(BaseEntity.class).build());

    private final long created;

    private String logMessage = null;

    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    private EventBusAccessor.EventRoutingKey eventRoutingKey;
    @Nullable
    private Marker logMarker;

    public BaseEvent() {

        created = System.currentTimeMillis();
    }

    //de-lomboked to not have the internal fields in the builder
    public static abstract class BaseEventBuilder<C extends BaseEvent, B extends BaseEventBuilder<C, B>> {

        private Marker logMarker;

        protected abstract B self();

        public abstract C build();

        public String toString() {

            return "BaseEvent.BaseEventBuilder()";
        }

        public B logMarker(Marker logMarker) {

            this.logMarker = logMarker;
            return self();
        }

    }

    protected BaseEvent(BaseEventBuilder<?, ?> b) {

        this();
        this.logMarker = b.logMarker;
    }

    /**
     * Log message for an event in human readable format
     *
     * @return log message for the event in format:
     *         <p><code>EventName [AttributeName: "AttributeValue"]</code></p>
     */
    public String getLogMessage() {
        //since events do not get modified it is sufficient to create the log message just once and cache it
        if (logMessage == null) {
            try {
                Map<String, Object> logEntries = getLogDetails();
                logMessage = logEntries.entrySet().stream()
                        .map(e -> e.getKey() + ": \"" + e.getValue() + "\"")
                        .collect(Collectors.joining(", ", "[", "]"));
            } catch (Exception ex) {
                logMessage = "Failed to create log message for event '" + this.getClass() + "': " + ex;
            }
        }
        return logMessage;
    }

    /**
     * Extracts the log details of the event into a modifiable map.
     *
     * @return the log details of the event.
     */
    public Map<String, Object> getLogDetails() {

        return getLogDetails(null).getLeft();
    }

    public Pair<Map<String, Object>, Set<String>> getLogDetails(Predicate<Class<?>> idExtractionPredicate) {

        Set<String> specialIds = idExtractionPredicate == null
                ? Collections.emptySet()
                : new TreeSet<>();
        //these are the attributes of the log message, mainly all ids of entities referenced by this event
        Map<String, Object> logData = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        logData.put("event", getEventName());

        //we get all properties of the event, going up the inheritance hierarchy
        try {
            PropertyDescriptor[] properties = BeanUtils.getPropertyDescriptors(this.getClass());
            for (PropertyDescriptor property : properties) {
                //only some properties are interesting, the others would be too sensitive
                String value = null;
                final Class<?> propertyType = property.getPropertyType();
                final Type genericPropertyType = property.getReadMethod().getGenericReturnType();
                //enum values are logged
                if (propertyType.isEnum()) {
                    value = String.valueOf(getPropertyValue(property));
                }
                //entities are logged by id
                if (BaseEntity.class.isAssignableFrom(propertyType)) {
                    //this is either the id of the referenced entity or ""
                    value = StringUtils.defaultString(BaseEntity.getIdOf((BaseEntity) getPropertyValue(property)));
                    //if the property should be extracted, we save it as a special id
                    if (idExtractionPredicate != null && !StringUtils.isEmpty(value) &&
                            idExtractionPredicate.test(propertyType)) {
                        specialIds.add(value);
                    }
                }
                //collection of entities are logged by ids
                if (genericPropertyType instanceof ParameterizedType parameterizedType) {
                    if (isAssignable(parameterizedType, TYPE_COLLECTION_BASE_ENTITY)) {
                        @SuppressWarnings("unchecked")
                        Collection<BaseEntity> entities = (Collection<BaseEntity>) getPropertyValue(property);
                        if (!CollectionUtils.isEmpty(entities)) {
                            value = entities.stream()
                                    .map(BaseEntity::getIdOf)
                                    .collect(Collectors.joining(", "));
                        }
                    }
                }
                if (value != null) {
                    String name = "event." + property.getName();
                    logData.put(name, value);
                }
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            logData.put("error", "Failed to create log message for event '" + this.getClass() + "': " + ex);
        }
        return Pair.of(logData, specialIds);
    }

    private Object getPropertyValue(PropertyDescriptor property)
            throws InvocationTargetException, IllegalAccessException {

        final Method readMethod = property.getReadMethod();
        if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
            readMethod.setAccessible(true);
        }
        return readMethod.invoke(this);
    }

    private String getEventName() {

        //find out module name by using "de.fhg.iese.dd.platform.business._shared_.init.XXXDataInitializer
        String[] packageNames = StringUtils.split(this.getClass().getName(), '.');
        final String moduleName;
        if (packageNames.length >= 7) {
            moduleName = packageNames[6];
        } else {
            moduleName = "unknown";
        }
        return moduleName + "." + this.getClass().getSimpleName();
    }

}
