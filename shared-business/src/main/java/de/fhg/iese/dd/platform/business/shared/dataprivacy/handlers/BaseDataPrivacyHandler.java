/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;

public abstract class BaseDataPrivacyHandler implements IDataPrivacyHandler {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    protected ITimeService timeService;

    @Autowired
    private IReflectionService reflectionService;

    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private final String moduleName = determineModuleName();

    @Override
    public String getId() {
        return this.getClass().getSimpleName() + " [" + getModuleName() + "]";
    }

    @Override
    public String toString() {
        return getId();
    }

    private String determineModuleName() {
        if (reflectionService == null) {
            return this.getClass().getPackage().getName();
        }
        return reflectionService.getModuleName(this.getClass());
    }

    @SafeVarargs
    @SuppressWarnings("varargs")
    protected static <T> List<T> unmodifiableList(T... entities) {
        return Collections.unmodifiableList(Arrays.asList(entities));
    }

}
