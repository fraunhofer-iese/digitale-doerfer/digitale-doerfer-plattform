/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

@Service
class AuthorizationDocumentationService implements IAuthorizationDocumentationService {

    @Autowired
    private IRoleService roleService;

    @Override
    public Collection<Class<? extends BaseRole<?>>> getRolesUnrestricted(Class<? extends Action<?>> actionClass) {

        if (ActionRoleRestricted.class.isAssignableFrom(actionClass)) {
            ActionRoleRestricted action = (ActionRoleRestricted) roleService.getAction(actionClass);
            return action.rolesAllowed();
        } else if (ActionRoleRelatedEntityIdRestricted.class.isAssignableFrom(actionClass)) {
            ActionRoleRelatedEntityIdRestricted<?> action =
                    (ActionRoleRelatedEntityIdRestricted<?>) roleService.getAction(actionClass);
            return action.rolesAllEntitiesAllowed();
        } else {
            return Collections.emptySet();
        }
    }

    @Override
    public Collection<Class<? extends BaseRole<?>>> getRolesEntityRestricted(Class<? extends Action<?>> actionClass) {

        if (ActionRoleRelatedEntityIdRestricted.class.isAssignableFrom(actionClass)) {
            ActionRoleRelatedEntityIdRestricted<?> action =
                    (ActionRoleRelatedEntityIdRestricted<?>) roleService.getAction(actionClass);
            return action.rolesSpecificEntitiesAllowed();
        } else {
            return Collections.emptySet();
        }
    }
    
}
