/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.*;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationChannelMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.repos.TeamNotificationChannelMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.repos.TeamNotificationConnectionRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.stream.Collectors;

@Service
class TeamNotificationService extends BaseService implements ITeamNotificationService {

    /**
     * If this is used as topic for a channel mapping, all topics match
     */
    private static final String ANY_TOPIC = "*";

    /**
     * If this is used as topic for a channel mapping, it only matches if not other channel mappings match
     */
    private static final String FALLBACK_TOPIC = "[*]";

    @Autowired
    private IPluginService pluginService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private TeamNotificationConnectionRepository teamNotificationConnectionRepository;
    @Autowired
    private TeamNotificationChannelMappingRepository teamNotificationChannelMappingRepository;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;
    private Map<TeamNotificationConnection, ITeamNotificationPublisher> connectionToPublisher;
    private Collection<TeamNotificationChannelMapping> channelMappings;

    @PostConstruct
    private void initialize() {
        log.info("Team notification initialization starting...");
        checkChangedConfigurationAndRefresh();
        log.info("Team notification initialization done");
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return ICachingComponent.CacheConfiguration.builder()
                .cachedEntity(TeamNotificationConnection.class)
                .cachedEntity(TeamNotificationChannelMapping.class)
                .build();
    }

    @PreDestroy
    private void shutdown() {
        if (!CollectionUtils.isEmpty(connectionToPublisher)) {
            connectionToPublisher.values().forEach(op -> parallelismService.getBlockableExecutor()
                    .submit(op::shutdownAndWait));
        }
    }

    private void checkChangedConfigurationAndRefresh() {

        cacheChecker.refreshCacheIfStale(this::refreshTeamNotificationsConnections);
    }

    private void refreshTeamNotificationsConnections() {
        log.info("Team notification configuration change detected, updating...");

        //get all TeamNotificationConnections
        List<TeamNotificationConnection> connections = teamNotificationConnectionRepository.findAll();

        Map<TeamNotificationConnection, ITeamNotificationPublisher> connectionToPublisherNew =
                new HashMap<>(connections.size());

        for (TeamNotificationConnection connection : connections) {
            //find a publisher provider for the connection type
            Optional<ITeamNotificationPublisherProvider> publisherProviderOptional =
                    findPublisherProvider(connection.getTeamNotificationConnectionType());
            if (publisherProviderOptional.isPresent()) {
                //instantiate a new publisher for that connection
                ITeamNotificationPublisherProvider publisherProvider = publisherProviderOptional.get();
                try {
                    ITeamNotificationPublisher publisher = publisherProvider.newPublisher(connection);
                    connectionToPublisherNew.put(connection, publisher);
                    log.info("Team notification publisher '{}' created for '{}'",
                            publisher.getName(), connection.getName());
                } catch (Exception e) {
                    log.error("Could not create new team notification publisher for " + connection, e);
                }
            } else {
                log.warn("No team notification publisher provider available for {}", connection);
            }
        }
        //get all mappings so that we can look them up later
        channelMappings = teamNotificationChannelMappingRepository.findAll();
        Collection<ITeamNotificationPublisher> oldPublishers;
        if (CollectionUtils.isEmpty(connectionToPublisher)) {
            oldPublishers = Collections.emptyList();
        } else {
            oldPublishers = connectionToPublisher.values();
        }
        //swap the cached connections
        connectionToPublisher = connectionToPublisherNew;
        //we need to shut down the old executors
        oldPublishers.forEach(op -> parallelismService.getBlockableExecutor().submit(op::shutdownAndWait));
        log.info("Team notification configuration update done");
    }

    private Optional<ITeamNotificationPublisherProvider> findPublisherProvider(String connectionType) {
        final List<ITeamNotificationPublisherProvider> publisherProviders = pluginService.getPluginInstancesRaw(
                PluginTarget.of(ITeamNotificationPublisherProvider.class));
        return publisherProviders.stream()
                .filter(pp -> StringUtils.equals(pp.getProvidedConnectionType(), connectionType))
                .findAny();
    }

    @Override
    public void sendTeamNotification(String topic, TeamNotificationMessage message) {
        sendTeamNotification(null, topic, message);
    }

    @Override
    public void sendTeamNotification(Tenant tenant, String topic, TeamNotificationMessage message) {
        message.setCreated(timeService.currentTimeMillisUTC());
        sendTeamNotificationInternal(tenant, topic, message);
    }

    @Override
    public void sendTeamNotification(Tenant tenant, String topic, TeamNotificationPriority priority, String message,
            Object... arguments) {
        sendTeamNotification(tenant, topic, TeamNotificationMessage.builder()
                .message(message, arguments)
                .priority(priority)
                .build());
    }

    @Override
    public void sendTeamNotification(String topic, TeamNotificationPriority priority, String message,
            Object... arguments) {
        sendTeamNotification(null, topic, priority, message, arguments);
    }

    private void sendTeamNotificationInternal(Tenant tenant, String topic, TeamNotificationMessage message) {

        final List<IDefaultTeamNotificationPublisher> defaultPublishers =
                pluginService.getPluginInstancesRaw(PluginTarget.of(IDefaultTeamNotificationPublisher.class));

        //all default publishers get called
        for (IDefaultTeamNotificationPublisher defaultPublisher : defaultPublishers) {
            try {
                defaultPublisher.sendTeamNotification(topic, tenant, message);
            } catch (Exception e) {
                log.error("Could not send message '{}' to team: {}", message, e.getMessage());
            }
        }

        //find all matching channel mappings
        List<TeamNotificationChannelMapping> channelMappings = findChannelMapping(tenant, topic, message.getPriority());
        if (!CollectionUtils.isEmpty(channelMappings)) {
            for (TeamNotificationChannelMapping channelMapping : channelMappings) {
                //get the according connection and channel for that mapping
                TeamNotificationConnection connection = channelMapping.getTeamNotificationConnection();
                String channel = channelMapping.getChannel();
                //find a suitable publisher for the connection
                ITeamNotificationPublisher publisher = connectionToPublisher.get(connection);
                if (publisher != null) {
                    try {
                        publisher.sendTeamNotification(
                                new TeamNotificationSendRequest(channel, topic, tenant, message));
                    } catch (Exception e) {
                        log.error("Publisher " + publisher.getName() + " threw unexpected exception", e);
                    }
                } else {
                    log.warn("No team notification publisher available for {}", connection);
                }
            }
        } else {
            if (defaultPublishers.isEmpty()) {
                log.warn("No team notification channel mapping available for '{}' / {}",
                        tenant == null ? "common" : tenant.getName(), topic);
            }
        }
    }

    private List<TeamNotificationChannelMapping> findChannelMapping(Tenant tenant, String topic,
            TeamNotificationPriority priority) {

        //before using the cache check if there was a configuration update
        checkChangedConfigurationAndRefresh();

        List<TeamNotificationChannelMapping> mappings = channelMappings.stream()
                //a mapping has to have the same topic or it is a wildcard mapping
                .filter(cm -> StringUtils.equals(topic, cm.getTopic()) || ANY_TOPIC.equals(cm.getTopic()))
                //the tenant is either null or explicitly has the same tenant specified
                .filter(cm -> cm.getTenant() == null || Objects.equals(cm.getTenant(), tenant))
                //the priority is in the allowed priorities
                .filter(cm -> cm.getAllowedPriorities().hasValue(priority))
                //we sort them to always have them in the same order, it actually does not matter
                .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                .collect(Collectors.toList());
        if (mappings.isEmpty()) {
            //check if we have fallback mappings and return them
            return channelMappings.stream()
                    //a fallback mapping has to have the fallback topic
                    .filter(cm -> FALLBACK_TOPIC.equals(cm.getTopic()))
                    //the tenant is either null or explicitly has the same tenant specified
                    .filter(cm -> cm.getTenant() == null || Objects.equals(cm.getTenant(), tenant))
                    //the priority is in the allowed priorities
                    .filter(cm -> cm.getAllowedPriorities().hasValue(priority))
                    //we sort them to always have them in the same order, it actually does not matter
                    .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                    .collect(Collectors.toList());
        }
        return mappings;
    }

    @Override
    public void validateConnection(TeamNotificationConnection connection)
            throws TeamNotificationConnectionConfigInvalidException {
        Optional<ITeamNotificationPublisherProvider> publisherProviderOptional =
                findPublisherProvider(connection.getTeamNotificationConnectionType());
        if (publisherProviderOptional.isPresent()) {
            //instantiate a new publisher for that connection
            ITeamNotificationPublisherProvider publisherProvider = publisherProviderOptional.get();
            publisherProvider.validateConnection(connection);
        } else {
            throw new TeamNotificationConnectionConfigInvalidException(
                    "No team notification publisher available for {}", connection.getTeamNotificationConnectionType());
        }
    }

    @Override
    public Set<TeamNotificationConnection> getAllConnections() {
        return new HashSet<>(teamNotificationConnectionRepository.findAll());
    }

    @Override
    public Set<TeamNotificationChannelMapping> getAllChannelMappings() {
        return new HashSet<>(teamNotificationChannelMappingRepository.findAll());
    }

    @Override
    public TeamNotificationConnection store(TeamNotificationConnection teamNotificationConnection) {
        return teamNotificationConnectionRepository.saveAndFlush(teamNotificationConnection);
    }

    @Override
    public TeamNotificationChannelMapping store(TeamNotificationChannelMapping teamNotificationChannelMapping) {
        return teamNotificationChannelMappingRepository.saveAndFlush(teamNotificationChannelMapping);
    }

    @Override
    public void deleteConnections(Collection<TeamNotificationConnection> connectionsToDelete) {
        teamNotificationConnectionRepository.deleteAll(connectionsToDelete);
    }

    @Override
    public void deleteChannelMappings(Collection<TeamNotificationChannelMapping> channelMappingsToDelete) {
        teamNotificationChannelMappingRepository.deleteAll(channelMappingsToDelete);
    }

    public void invalidateCache() {
        cacheChecker.resetCacheChecks();
    }

}
