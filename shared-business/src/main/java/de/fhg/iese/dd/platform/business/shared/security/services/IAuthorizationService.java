/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.time.Duration;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.exceptions.EncryptionException;

public interface IAuthorizationService extends IService {

    int MINIMUM_API_KEY_LENGTH = 20;

    enum TokenAuthorizationStatus {
        TOKEN_VALID,
        TOKEN_EXPIRED,
        TOKEN_DOES_NOT_MATCH_ENDPOINT,
        TOKEN_VALUES_DO_NOT_MATCH,
        TOKEN_INVALID
    }

    /**
     * Get an auth token that can be used for authorization of specific events. For the same event and expiration time
     * the same token is returned.
     * <p>
     * The token can only be verified via {@link #checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(String,
     * BaseEvent)}.
     * <p>
     * The generated token contains
     * <li>name of the event</li>
     * <li>expiration time</li>
     * <li>properties == type BaseEntity of the event: IDs of entities</li>
     * <li>properties != type BaseEntity of the event: value of the property</li>
     *
     * @param authorizedEvent  the event to authorize
     * @param validityDuration the duration how long the token should be valid, starting now
     *
     * @return the auth token
     *
     * @throws EncryptionException if the mail token could not be generated
     */
    String generateAuthToken(BaseEvent authorizedEvent, Duration validityDuration) throws EncryptionException;

    /**
     * Calls {@link #checkAuthTokenAuthorizedForEvent(String, BaseEvent)} in order to check the auth token and processes
     * the return. If the validation was not successful, an exception is thrown.
     *
     * @throws NotAuthorizedException if the token is expired or does not match or is invalid
     */
    void checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(String token, BaseEvent authorizedEvent)
            throws NotAuthorizedException;

    /**
     * Checks an auth token that was generated using {@link #generateAuthToken(BaseEvent, Duration)}.
     * <p>
     * A token is valid if
     * <li>names of the events match</li>
     * <li>expiration time is not expired</li>
     * <li>properties == type BaseEntity of the event: IDs of entities match</li>
     * <li>properties != type BaseEntity of the event: value of the property match (using equals)</li>
     *
     * @param token           the token to verify
     * @param authorizedEvent the event that should be authorized with this event
     *
     * @return AuthorizationStatus indicator if the event could be authorized
     */
    TokenAuthorizationStatus checkAuthTokenAuthorizedForEvent(String token, BaseEvent authorizedEvent);

    /**
     * Checks if an api key fulfills the minimum security standards and can be used as key. It does not check if it
     * matches any api key!
     * <p>
     * An api key is meeting the standard if
     * <li>has at least 20 characters</li>
     * <li>does not contain white spaces</li>
     *
     * @param apiKey the key to check
     *
     * @return true if the api key meets the minimum security standards
     */
    boolean isApiKeyUsable(String apiKey);

}
