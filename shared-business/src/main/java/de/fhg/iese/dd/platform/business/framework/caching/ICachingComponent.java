/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.caching;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.Set;

/**
 * Interface to allow controlling the internal cache of a component.
 */
public interface ICachingComponent {

    @Getter
    @Builder
    class CacheConfiguration {

        @Singular
        private final Set<Class<? extends BaseEntity>> cachedEntities;
        @Singular("usedCache")
        private final Set<Class<? extends ICachingComponent>> usedCaches;

    }

    /**
     * The configuration of the internal cache(s).
     *
     * @return configuration of all internal caches, never null
     */
    CacheConfiguration getCacheConfiguration();

    /**
     * Causes all internal caches of the component to be invalidated.
     */
    void invalidateCache();

}
