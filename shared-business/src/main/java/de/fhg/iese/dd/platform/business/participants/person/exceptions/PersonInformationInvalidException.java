/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class PersonInformationInvalidException extends BadRequestException {

    private static final long serialVersionUID = -3030219163612566829L;

    private PersonInformationInvalidException(String message, Object... arguments) {
        super(message, arguments);
    }

    private PersonInformationInvalidException(String message) {
        super(message);
    }

    public static PersonInformationInvalidException forMissingField(String field) {
        return new PersonInformationInvalidException(
                "The person could not be processed, the value of the field '{}' is missing or invalid.", field);
    }

    public static PersonInformationInvalidException forMissingBothHomeAreaAndHomeTenant() {
        return new PersonInformationInvalidException(
                "The person could not be processed, both homeAreaId and homeTenantId is missing");
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.PERSON_INFORMATION_INVALID;
    }

}
