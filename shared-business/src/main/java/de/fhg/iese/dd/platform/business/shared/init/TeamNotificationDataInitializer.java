/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationChannelMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@Component
public class TeamNotificationDataInitializer extends BaseDataInitializer {

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Autowired
    private ITenantService tenantService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("team-notification")
                .requiredEntity(Tenant.class)
                .processedEntity(TeamNotificationConnection.class)
                .processedEntity(TeamNotificationChannelMapping.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        //get all old connections and mappings to delete all that are not configured in this run
        Set<TeamNotificationConnection> existingConnections = teamNotificationService.getAllConnections();
        Set<TeamNotificationChannelMapping> existingChannelMappings = teamNotificationService.getAllChannelMappings();

        //create the connections
        Map<String, TeamNotificationConnection> connectionsByName = createConnections(logSummary);

        //create the common channel mappings that are not specific to a tenant
        Set<TeamNotificationChannelMapping> commonChannelMappings =
                createCommonChannelMappings(connectionsByName, logSummary);
        //create the tenant specific mappings
        Set<TeamNotificationChannelMapping> specificChannelMappings =
                createTenantSpecificChannelMappings(connectionsByName, logSummary);

        //find out what was newly created or updated
        Set<TeamNotificationConnection> newOrUpdatedConnections =
                new HashSet<>(connectionsByName.values());
        Set<TeamNotificationChannelMapping> newOrUpdatedChannelMappings = new HashSet<>();
        newOrUpdatedChannelMappings.addAll(commonChannelMappings);
        newOrUpdatedChannelMappings.addAll(specificChannelMappings);

        //remove the newly created or updated from the existing to get the orphans
        existingConnections.removeAll(newOrUpdatedConnections);
        existingChannelMappings.removeAll(newOrUpdatedChannelMappings);
        //delete the orphans
        deleteOrphanedConnectionsAndChannelMappings(existingConnections, existingChannelMappings, logSummary);
    }

    private Map<String, TeamNotificationConnection> createConnections(LogSummary logSummary) {

        logSummary.info("Creating team notification connections");
        logSummary.indent();
        Map<String, TeamNotificationConnection> teamNotificationConnectionsByName;
        try {
            List<TeamNotificationConfiguration> teamNotificationConnectionJsons =
                    loadAllDataInitKeysAndEntities(TeamNotificationConfiguration.class).values().stream()
                            .flatMap(Collection::stream)
                            .collect(Collectors.toList());

            teamNotificationConnectionsByName = teamNotificationConnectionJsons.stream()
                    .map(tncjson -> createConnection(tncjson, logSummary))
                    .collect(Collectors.toMap(TeamNotificationConnection::getName, Function.identity()));
        } finally {
            logSummary.outdent();
        }
        return teamNotificationConnectionsByName;
    }

    private TeamNotificationConnection createConnection(TeamNotificationConfiguration connectionJson,
            LogSummary logSummary) {

        //the id is actually used, so we need to check it
        checkId(connectionJson);
        TeamNotificationConnection teamNotificationConnection = TeamNotificationConnection.builder()
                .id(connectionJson.getId())
                .name(connectionJson.getName())
                .connectionConfig(connectionJson.getConnectionConfig())
                .teamNotificationConnectionType(connectionJson.getTeamNotificationConnectionType())
                .build();
        //check if the connection is valid to avoid having exceptions at a later point in time, when it is used
        teamNotificationService.validateConnection(teamNotificationConnection);
        TeamNotificationConnection connection = teamNotificationService.store(teamNotificationConnection);
        logSummary.info("Created {}", connection);
        return connection;
    }

    private Set<TeamNotificationChannelMapping> createCommonChannelMappings(
            Map<String, TeamNotificationConnection> connectionsByName, LogSummary logSummary) {

        logSummary.info("Creating common team notification channel mappings");
        logSummary.indent();
        try {
            //get the common channel filters that do not have a tenant restriction
            Map<String, List<TeamNotificationChannelFilter>> channelFiltersByConnectionName =
                    loadEntitiesFromDataJson(null, TeamNotificationChannelFilter.class);
            return createChannelMappings(null, channelFiltersByConnectionName, connectionsByName, logSummary);
        } finally {
            logSummary.outdent();
        }
    }

    private Set<TeamNotificationChannelMapping> createTenantSpecificChannelMappings(
            Map<String, TeamNotificationConnection> connectionsByName, LogSummary logSummary) {

        Collection<Tenant> tenants = tenantService.findAllOrderedByNameAsc();
        Set<TeamNotificationChannelMapping> specificChannelMappings = new HashSet<>();
        for (Tenant tenant : tenants) {
            //get the channel filters for the tenant
            Map<String, List<TeamNotificationChannelFilter>> channelFiltersByConnectionName =
                    loadEntitiesFromDataJsonTenantSpecific(tenant, TeamNotificationChannelFilter.class);

            if (!CollectionUtils.isEmpty(channelFiltersByConnectionName)) {
                logSummary.info("Creating team notification channel mappings for {}", tenant);
                logSummary.indent();
                try {
                    specificChannelMappings.addAll(
                            createChannelMappings(tenant, channelFiltersByConnectionName, connectionsByName,
                                    logSummary));
                } finally {
                    logSummary.outdent();
                }
            }
        }
        return specificChannelMappings;
    }

    private Set<TeamNotificationChannelMapping> createChannelMappings(Tenant tenant,
            Map<String, List<TeamNotificationChannelFilter>> channelFiltersByConnectionName,
            Map<String, TeamNotificationConnection> connectionsByName,
            LogSummary logSummary) {
        Set<TeamNotificationChannelMapping> channelMappings = new HashSet<>();
        if (CollectionUtils.isEmpty(channelFiltersByConnectionName)) {
            return Collections.emptySet();
        }
        for (Map.Entry<String, List<TeamNotificationChannelFilter>> connectionNameToChannelFilters : channelFiltersByConnectionName.entrySet()) {
            String connectionName = connectionNameToChannelFilters.getKey();
            for (TeamNotificationChannelFilter channelFilter : connectionNameToChannelFilters.getValue()) {
                TeamNotificationChannelMapping channelMapping =
                        createChannelMapping(connectionName, channelFilter, connectionsByName, tenant, logSummary);
                if (channelMapping != null) {
                    channelMappings.add(channelMapping);
                }
            }
        }
        return channelMappings;
    }

    private TeamNotificationChannelMapping createChannelMapping(String connectionName,
            TeamNotificationChannelFilter channelFilter, Map<String, TeamNotificationConnection> connectionsByName,
            Tenant tenant, LogSummary logSummary) {

        //we look up the connection by name from the just created / updated ones
        TeamNotificationConnection teamNotificationConnection = connectionsByName.get(connectionName);
        if (teamNotificationConnection == null) {
            logSummary.error(
                    "Invalid reference '{}' to non existing team notification connection, skipping channel mapping {}/{} in {}",
                    connectionName,
                    channelFilter.getTopic(),
                    channelFilter.getChannel(),
                    tenant == null ? "common" : tenant.toString());
            return null;
        }
        TeamNotificationChannelMapping channelMapping =
                TeamNotificationChannelMapping.builder()
                        .channel(channelFilter.getChannel())
                        .tenant(tenant)
                        .teamNotificationConnection(teamNotificationConnection)
                        .topic(channelFilter.getTopic())
                        .build()
                        //the id of the mapping is generated, based on teamNotificationConnection, tenant, topic, channel
                        .withConstantId();
        channelMapping.getAllowedPriorities().setValues(expandFilters(channelFilter.getAllowedPriorities()));
        logSummary.info("Created {}", teamNotificationService.store(channelMapping));
        return channelMapping;
    }

    private Collection<TeamNotificationPriority> expandFilters(
            Collection<TeamNotificationChannelFilter.NotificationPriorityFilter> filters) {

        //if no filter is specified it is considered as wildcard
        if (CollectionUtils.isEmpty(filters)) {
            return EnumSet.allOf(TeamNotificationPriority.class);
        }
        EnumSet<TeamNotificationPriority> allowedPriorities = EnumSet.noneOf(TeamNotificationPriority.class);
        //the filters are adding, meaning more filters = more messages
        for (TeamNotificationChannelFilter.NotificationPriorityFilter filter : filters) {
            //we just check all priorities if this filter allows it
            for (TeamNotificationPriority priority : TeamNotificationPriority.values()) {
                //only exactly the same scope should match
                if (filter.getScope() != null && priority.getScope() != filter.getScope()) {
                    continue;
                }
                //the level should include all levels above or all if undefined
                if (filter.getLogLevel() != null && !priority.getLogLevel().isMoreSpecificThan(filter.getLogLevel())) {
                    continue;
                }
                //the impact should also include all impacts above or all if undefined
                if (filter.getImpact() != null && !priority.getImpact().hasMoreImpactThan(filter.getImpact())) {
                    continue;
                }
                allowedPriorities.add(priority);
            }
        }
        return allowedPriorities;
    }

    private void deleteOrphanedConnectionsAndChannelMappings(Set<TeamNotificationConnection> orphanedConnections,
            Set<TeamNotificationChannelMapping> orphanedChannelMappings, LogSummary logSummary) {

        if (!orphanedConnections.isEmpty() || !orphanedChannelMappings.isEmpty()) {
            logSummary.info("Deleting previous team notification connections and channel mappings");
            logSummary.indent();
            try {
                orphanedChannelMappings.forEach(ecm -> logSummary.info("Deleting {}", ecm));
                teamNotificationService.deleteChannelMappings(orphanedChannelMappings);
                orphanedConnections.forEach(ec -> logSummary.info("Deleting {}", ec));
                teamNotificationService.deleteConnections(orphanedConnections);
            } finally {
                logSummary.outdent();
            }
        }
    }

}
