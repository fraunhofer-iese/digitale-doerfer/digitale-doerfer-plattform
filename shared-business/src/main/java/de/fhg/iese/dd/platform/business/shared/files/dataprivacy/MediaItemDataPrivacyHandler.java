/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.dataprivacy;

import static java.util.stream.Collectors.groupingBy;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Component
public class MediaItemDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            Collections.emptyList();

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(MediaItem.class);

    @Autowired
    private IMediaItemService mediaItemService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    @Transactional
    public void collectUserData(final Person person, final IDataPrivacyReport dataPrivacyReport) {

        String commonCategoryName = "Allgemein";
        Map<String, List<MediaItem>> mediaItemsByAppOrAppVariantName =
                mediaItemService.findAllItemsOwnedByPerson(person).stream()
                        .collect(groupingBy(mediaItem -> {
                            // categorize media items by app/app variant. If both are missing, use the common category
                            if (mediaItem.getApp() != null) {
                                if (mediaItem.getAppVariant() != null) {
                                    return mediaItem.getAppVariant().getName();
                                }
                                return mediaItem.getApp().getName();
                            }
                            return commonCategoryName;
                        }));
        if (CollectionUtils.isEmpty(mediaItemsByAppOrAppVariantName)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Bilder", "Von dir hochgeladene Bilder");

        for (Map.Entry<String, List<MediaItem>> mediaItemsEntry : mediaItemsByAppOrAppVariantName.entrySet()) {
            String description = "Hochgeladen in der App " + mediaItemsEntry.getKey();
            if (mediaItemsEntry.getKey().equals(commonCategoryName)) {
                // this description is only used when the media items could not be assigned to an app or app variant
                description = "Hochgeladene Bilder, die keiner App zugeordnet sind";
            }

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection(mediaItemsEntry.getKey(), description);

            mediaItemsEntry.getValue()
                    .forEach(mediaItem -> subSection.newImageContent(mediaItem.getId(),
                            "Bild hochgeladen am " + timeService.toLocalTimeHumanReadable(mediaItem.getCreated()),
                            mediaItem));
        }
    }

    @Override
    public void deleteUserData(final Person person, final IPersonDeletionReport personDeletionReport) {

        List<MediaItem> mediaItems = mediaItemService.findAllItemsOwnedByPerson(person);

        personDeletionReport.addEntries(mediaItems, DeleteOperation.DELETED);

        mediaItemService.deleteAllItemsByOwner(person);
    }

}
