/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2021 Axel Wickenkamp, Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.log.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportFinalizedEvent;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.security.events.CreateRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.security.events.RemoveRoleAssignmentConfirmation;
import de.fhg.iese.dd.platform.business.shared.waiting.events.WaitingExpiredEvent;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class SharedLogEventProcessor extends BaseLogEventProcessor {

    @EventProcessing(relevantEvents = {
            CreateRoleAssignmentConfirmation.class,
            RemoveRoleAssignmentConfirmation.class,
            DataPrivacyReportFinalizedEvent.class,
            DataPrivacyReportRequest.class,
            WaitingExpiredEvent.class},
            includeSubtypesOfEvent = true)
    protected void logEvent(BaseEvent event, EventProcessingContext context) {
        super.logEvent(event, context);
    }

}
