/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.DefaultMediaItemRepository;

@Component
public class DefaultMediaItemRefreshAdminTask extends BaseAdminTask {

    @Autowired
    private DefaultMediaItemRepository defaultMediaItemRepository;

    @Override
    public String getName() {
        return "RefreshDefaultMediaItems";
    }

    @Override
    public String getDescription() {
        return "Removes all cached default media items so that they get freshly created.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        logSummary.info("Found {} cached default media items", defaultMediaItemRepository.count());
        if (!parameters.isDryRun()) {
            defaultMediaItemRepository.deleteAll();
            logSummary.info("Deleted all cached default media items");
        }
    }

}
