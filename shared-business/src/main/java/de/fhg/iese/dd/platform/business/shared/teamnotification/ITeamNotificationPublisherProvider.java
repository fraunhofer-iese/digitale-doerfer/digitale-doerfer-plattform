/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.exceptions.TeamNotificationConnectionConfigInvalidException;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;

public interface ITeamNotificationPublisherProvider extends IService {

    /**
     * @return the type of connections the publisher supports
     */
    String getProvidedConnectionType();

    /**
     * Create a new publisher for the given connection. The type of the connection hast to match {@link
     * #getProvidedConnectionType()}
     *
     * @param connection
     *         the connection for the new publisher
     *
     * @return a new publisher that works on the given connection
     */
    ITeamNotificationPublisher newPublisher(TeamNotificationConnection connection);

    /**
     * Checks if the connection config is valid
     *
     * @param connection
     *         the connection to check
     *
     * @throws TeamNotificationConnectionConfigInvalidException
     *         if the connection config is invalid
     */
    void validateConnection(TeamNotificationConnection connection) throws
            TeamNotificationConnectionConfigInvalidException;

}
