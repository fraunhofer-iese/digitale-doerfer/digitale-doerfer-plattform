/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity that is only used in data init files and never persisted. It is required to map app variants to legal texts.
 */
@Getter
@Setter
@NoArgsConstructor
public class AppVariantLegalTextMapping extends BaseEntity {

    private String legalTextIdentifier;
    private Long dateActive;
    private Long dateInactive;

    @Override
    public String toString() {
        return "AppVariantLegalTextMapping [legalTextIdentifier=" + legalTextIdentifier + "]";
    }

}
