/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.dataprivacy;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Component
public class TemporaryDocumentItemDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(DocumentItem.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(TemporaryDocumentItem.class);

    @Autowired
    private IDocumentItemService documentItemService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(final Person person, final IDataPrivacyReport dataPrivacyReport) {

        List<TemporaryDocumentItem> temporaryDocumentItems = documentItemService.findAllTemporaryItemsByOwner(person);
        if (CollectionUtils.isEmpty(temporaryDocumentItems)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Temporäre Dokumente",
                        "Von dir temporär hochgeladene Dokumente");

        IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                section.newSubSection("Temporäre Dokumente", "");

        temporaryDocumentItems
                .forEach(tempItem -> subSection.newDocumentContent(tempItem.getId(),
                        "Dokument '" + tempItem.getDocumentItem().getTitle() + "' " +
                                "mit Beschreibung '" +
                                Objects.toString(tempItem.getDocumentItem().getDescription(), "-") + "' " +
                                "hochgeladen am " + timeService.toLocalTimeHumanReadable(tempItem.getCreated()),
                        tempItem.getDocumentItem()));
    }

    @Override
    public void deleteUserData(final Person person, final IPersonDeletionReport personDeletionReport) {

        List<TemporaryDocumentItem> temporaryDocumentItems = documentItemService.findAllTemporaryItemsByOwner(person);

        personDeletionReport.addEntries(temporaryDocumentItems, DeleteOperation.DELETED);

        documentItemService.deleteAllTemporaryItemsByOwner(person);
    }

}
