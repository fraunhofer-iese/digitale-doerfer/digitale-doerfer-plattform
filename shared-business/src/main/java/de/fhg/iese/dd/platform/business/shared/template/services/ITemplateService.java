/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.template.services;

import java.io.IOException;
import java.util.Map;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;
import freemarker.template.TemplateException;

public interface ITemplateService extends IService {

    /**
     * Fills out the specified template with the values provided in the template model.
     *
     * @param templateLocation
     *         a template location
     * @param templateModel
     *         Attributes that should be available in the template
     *
     * @return the result of applying the template on the given model
     *
     * @throws IOException
     *         if the template can not be accessed
     * @throws TemplateException
     *         if the template was invalid for the given model
     */
    String createTextWithTemplateMapModel(TemplateLocation templateLocation, Map<String, Object> templateModel) throws
            IOException, TemplateException;

    /**
     * Fills out the specified template with the values provided in the template model.
     *
     * @param templateLocation
     *         a template location
     * @param templateModel
     *         Attributes that should be available in the template
     *
     * @return the result of applying the template on the given model
     *
     * @throws IOException
     *         if the template can not be accessed
     * @throws TemplateException
     *         if the template was invalid for the given model
     */
    String createTextWithTemplateObjectModel(TemplateLocation templateLocation, Object templateModel) throws
            IOException, TemplateException;

}
