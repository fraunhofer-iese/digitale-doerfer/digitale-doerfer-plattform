/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collections;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import lombok.AccessLevel;
import lombok.Getter;

/**
 * Permission that grants permission per entity id.
 *
 * @see Permission
 */
@Getter(AccessLevel.PROTECTED)
public abstract class PermissionEntityIdRestricted extends Permission {

    private final Set<String> allowedEntityIds;

    private final boolean allEntitiesAllowed;

    protected PermissionEntityIdRestricted(Set<String> allowedEntityIds, boolean allEntitiesAllowed) {
        //the permission is given if either all entities are allowed or at least one
        super(allEntitiesAllowed || !CollectionUtils.isEmpty(allowedEntityIds));
        this.allowedEntityIds = allowedEntityIds != null ? allowedEntityIds : Collections.emptySet();
        this.allEntitiesAllowed = allEntitiesAllowed;
    }

    /**
     * @param entityId the entity id to check
     *
     * @return if all entities are allowed or the given entity id is in the set of allowed entity ids.
     */
    protected boolean isEntityIdAllowed(String entityId) {
        return isAllEntitiesAllowed() || getAllowedEntityIds().contains(entityId);
    }

    /**
     * Inversion of {@link #isEntityIdAllowed(String)}
     */
    protected boolean isEntityIdDenied(String entityId) {
        return !isEntityIdAllowed(entityId);
    }

}
