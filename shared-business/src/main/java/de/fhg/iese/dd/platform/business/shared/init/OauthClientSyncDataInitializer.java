/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;

/**
 * Creates all oauth clients that are configured at Auth0 and that have not been created with the other data
 * initializers
 */
@Component
public class OauthClientSyncDataInitializer extends BaseDataInitializer {

    public static final String TOPIC = "oauth";

    @Autowired
    private OauthClientRepository oauthClientRepository;
    @Autowired
    private IOauthManagementService oauthManagementService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                //should be run *after* all other OauthClient data inits
                //even if it creates OauthClients they should not be marked as processed, since they should never be referenced from other data inits
                .requiredEntity(OauthClient.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        createOauthClients(logSummary);
    }

    private void createOauthClients(LogSummary logSummary) {

        List<OauthClient> storedOauthClients = oauthClientRepository.findAll();

        //we want to get all the configured ones at auth0, so that we see if something is missing
        Map<String, OauthClient> configuredOauthClientsByIdentifier =
                oauthManagementService.getConfiguredOauthClients().stream()
                        .collect(Collectors.toMap(OauthClient::getOauthClientIdentifier, Function.identity()));

        //we remove the ones that are already stored, so we only add the missing ones
        storedOauthClients.forEach(
                oauthClient -> configuredOauthClientsByIdentifier.remove(oauthClient.getOauthClientIdentifier()));

        configuredOauthClientsByIdentifier.values().stream()
                //we want a constant id, not a random one
                .peek(OauthClient::withConstantId)
                .forEach(oauthClient -> logSummary.info("created configured {}", oauthClient.toString()));

        oauthClientRepository.saveAll(configuredOauthClientsByIdentifier.values());
        oauthClientRepository.flush();
    }

}
