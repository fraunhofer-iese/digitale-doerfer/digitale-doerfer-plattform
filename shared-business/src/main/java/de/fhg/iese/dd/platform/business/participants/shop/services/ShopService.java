/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2019 Steffen Hupp, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.shop.services;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.shop.exceptions.ShopNotFoundException;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.ShopRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Service
class ShopService extends BaseEntityService<Shop> implements IShopService {

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IRandomService randomUtil;

    @Override
    public Shop findShopById(String shopId) throws ShopNotFoundException {
        if (StringUtils.isEmpty(shopId)) {
            throw new ShopNotFoundException("Empty shopId");
        }
        return shopRepository.findById(shopId).orElseThrow(
                () -> new ShopNotFoundException("Could not find shop \"" + shopId + "\""));
    }

    @Override
    public Shop findShopByIdOrNull(String shopId)  {
        if(StringUtils.isEmpty(shopId)) return null;
        return shopRepository.findById(shopId).orElse(null);
    }

    @Override
    public Shop findAnyByTenant(Tenant tenant) throws ShopNotFoundException {
        List<Shop> shops = shopRepository.findAllByCommunityOrderByCreatedAsc(tenant);

        Shop shop = randomUtil.randomElement(shops);

        if (shop == null) {
            throw new ShopNotFoundException(
                    "Tenant " + tenant.getId() + " has no shops, call update initial data first");
        }
        return shop;
    }

    @Override
    public List<Person> findAllShopOwners(Shop shop) throws ShopNotFoundException {

        return roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);
    }

    @Override
    @Transactional
    public void deleteShop(Shop shop) {
        shopRepository.delete(shop);
    }

}
