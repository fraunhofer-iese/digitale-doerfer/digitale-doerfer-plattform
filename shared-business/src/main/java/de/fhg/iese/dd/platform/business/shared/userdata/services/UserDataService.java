/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.userdata.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.repos.UserDataCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.repos.UserDataKeyValueEntryRepository;

@Service
class UserDataService extends BaseService implements IUserDataService {

    @Autowired
    private UserDataCategoryRepository userDataCategoryRepository;
    @Autowired
    private UserDataKeyValueEntryRepository userDataKeyValueEntryRepository;

    @Override
    public List<UserDataCategory> getAllCategories(Person user) {
        return userDataCategoryRepository.findByOwnerOrderByCreatedDesc(user);
    }

    @Override
    public List<UserDataCategory> getAllCategoriesSince(Person user, long timeStamp) {
        return userDataCategoryRepository.findByOwnerAndLastChangeAfterOrderByCreatedDesc(user, timeStamp);
    }

    @Override
    @Transactional
    public UserDataCategory createCategory(Person user, String category) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(category, user);
        if(userDataCategory != null){
            return userDataCategory;
        }else{
            userDataCategory = new UserDataCategory();
            userDataCategory.setName(category);
            userDataCategory.setOwner(user);
            userDataCategory.setLastChange(timeService.currentTimeMillisUTC());
            return userDataCategoryRepository.saveAndFlush(userDataCategory);
        }
    }

    @Override
    @Transactional
    public void removeCategory(Person user, String category) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(category, user);
        if(userDataCategory == null) {
            return;
        }
        userDataKeyValueEntryRepository.deleteByCategory(userDataCategory);
        userDataKeyValueEntryRepository.flush();
        userDataCategoryRepository.delete(userDataCategory);
        userDataCategoryRepository.flush();
    }

    @Override
    @Transactional
    public void setValueOfKey(Person user, String category, String key, String value) {
        long timestamp = timeService.currentTimeMillisUTC();
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(category, user);

        if(userDataCategory == null) {
            userDataCategory = new UserDataCategory();
            userDataCategory.setName(category);
            userDataCategory.setOwner(user);
        }
        userDataCategory.setLastChange(timestamp);
        userDataCategory = userDataCategoryRepository.saveAndFlush(userDataCategory);

        UserDataKeyValueEntry entry = userDataKeyValueEntryRepository.findByUniqueKeyAndCategory(key, userDataCategory);
        if(entry == null) {
            entry = new UserDataKeyValueEntry();
            entry.setCategory(userDataCategory);
            entry.setUniqueKey(key);
            entry.setValueContent(value);
        } else {
            entry.setValueContent(value);
        }
        entry.setLastChange(timestamp);
        userDataKeyValueEntryRepository.saveAndFlush(entry);
    }

    @Override
    @Transactional
    public void removeKeyValue(Person owner, String categoryName, String key) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(categoryName, owner);
        if(userDataCategory == null) {
            return;
        }
        userDataKeyValueEntryRepository.deleteByUniqueKeyAndCategory(key, userDataCategory);
        userDataKeyValueEntryRepository.flush();
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDataKeyValueEntry> getKeyValueEntries(Person owner, String categoryName) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(categoryName, owner);
        if(userDataCategory == null) {
            return Collections.emptyList();
        }
        return userDataKeyValueEntryRepository.findByCategoryOrderByCreatedDesc(userDataCategory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDataKeyValueEntry> getKeyValueEntriesSince(Person owner, String categoryName, long timeStamp) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(categoryName, owner);
        if(userDataCategory == null) {
            return Collections.emptyList();
        }
        return userDataKeyValueEntryRepository.findByCategoryAndLastChangeAfterOrderByCreatedDesc(userDataCategory, timeStamp);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDataKeyValueEntry getValueOfKey(Person owner, String categoryName, String keyName) {
        UserDataCategory userDataCategory = userDataCategoryRepository.findByNameAndOwner(categoryName, owner);
        if(userDataCategory == null) {
            return null;
        }
        return userDataKeyValueEntryRepository.findByUniqueKeyAndCategory(keyName, userDataCategory);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDataKeyValueEntry> getAllKeyValueEntries(Person owner) {
        return userDataKeyValueEntryRepository.findAllByCategoryOwnerOrderByCreatedDesc(owner);
    }

}
