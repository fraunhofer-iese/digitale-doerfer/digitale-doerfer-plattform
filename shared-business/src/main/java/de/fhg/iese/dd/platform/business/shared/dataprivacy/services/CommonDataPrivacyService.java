/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Benjamin Hassenfratz, Balthasar Weitzel, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportFinalizedEvent;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteConfirmation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyWorkflowNotFoundException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormat;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflow;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@Service
class CommonDataPrivacyService extends BaseDataPrivacyService implements ICommonDataPrivacyService {

    private static final String ZIP_FILE_NAME = "Datenschutzbericht.zip";
    private static final String REPORT_HTML_FILE_NAME = "Datenschutzbericht.html";
    private static final String REPORT_JSON_FILE_NAME = "Datenschutzbericht.json";
    private static final long DELAY_MARK_PROCESSING_ACTIVE = TimeUnit.SECONDS.toMillis(5);
    private static final long DELAY_CONTINUE_PROCESSING = TimeUnit.SECONDS.toMillis(30);

    @Autowired
    private IDataPrivacyReportConverterService converterService;
    @Autowired
    private ITeamNotificationService teamNotificationService;
    @Autowired
    private IFileStorage fileStorage;
    @Autowired
    private IExternalDataPrivacyService externalDataPrivacyService;
    @Autowired
    private IInternalDataPrivacyService internalDataPrivacyService;
    @Autowired
    private IEmailDataPrivacyService emailDataPrivacyService;
    @Autowired
    private AppVariantUsageRepository appVariantUsageRepository;

    @Override
    public DataPrivacyDataCollection startDataPrivacyDataCollection(Person person, DataPrivacyWorkflowTrigger trigger) {

        final String internalFolderPath =
                StringUtils.appendIfMissing(PRIVACY_REPORT_FOLDER, "/") + UUID.randomUUID() + "/";
        final DataPrivacyDataCollection dataCollection =
                dataCollectionRepository.saveAndFlush(DataPrivacyDataCollection.builder()
                        .person(person)
                        .workflowTrigger(trigger)
                        .status(DataPrivacyWorkflowStatus.OPEN)
                        .lastStatusChanged(timeService.currentTimeMillisUTC())
                        .lastProcessed(timeService.currentTimeMillisUTC())
                        .internalFolderPath(internalFolderPath)
                        .build());

        //for all app variant usages of app variants with an external system, we create a data collection
        createDataPrivacyWorkflowAppVariants(dataCollection);

        //we immediately start the data collection
        processDataPrivacyWorkflow(dataCollection);

        return dataCollection;
    }

    @Override
    public void startDataPrivacyDataDeletion(Person person, DataPrivacyWorkflowTrigger trigger) {

        final DataPrivacyDataDeletion dataDeletion =
                dataDeletionRepository.saveAndFlush(DataPrivacyDataDeletion.builder()
                        .person(person)
                        .workflowTrigger(trigger)
                        .status(DataPrivacyWorkflowStatus.OPEN)
                        .lastStatusChanged(timeService.currentTimeMillisUTC())
                        .lastProcessed(timeService.currentTimeMillisUTC())
                        .build());

        //for all app variant usages of app variants with an external system, we create a data deletion
        createDataPrivacyWorkflowAppVariants(dataDeletion);

        //we immediately start the data deletion
        processDataPrivacyWorkflow(dataDeletion);
    }

    private void createDataPrivacyWorkflowAppVariants(DataPrivacyWorkflow dataPrivacyWorkflow) {

        List<AppVariantUsage> externalAppVariantUsages =
                appVariantUsageRepository.findAllByPersonOrderByCreatedDesc(dataPrivacyWorkflow.getPerson()).stream()
                        .filter(avu -> avu.getAppVariant().hasExternalSystem())
                        .collect(Collectors.toList());

        List<DataPrivacyWorkflowAppVariant> dataPrivacyWorkflowAppVariants =
                new ArrayList<>(externalAppVariantUsages.size());
        List<DataPrivacyWorkflowAppVariantStatusRecord> statusRecords =
                new ArrayList<>(externalAppVariantUsages.size());
        for (AppVariantUsage appVariantUsage : externalAppVariantUsages) {

            final AppVariant appVariant = appVariantUsage.getAppVariant();
            final DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant =
                    DataPrivacyWorkflowAppVariant.builder()
                            .dataPrivacyWorkflow(dataPrivacyWorkflow)
                            .appVariant(appVariant)
                            .build();
            dataPrivacyWorkflowAppVariants.add(dataPrivacyWorkflowAppVariant);

            // create new status record for that data privacy workflow app variant
            statusRecords.add(DataPrivacyWorkflowAppVariantStatusRecord.builder()
                    .dataPrivacyWorkflowAppVariant(dataPrivacyWorkflowAppVariant)
                    .status(DataPrivacyWorkflowAppVariantStatus.OPEN)
                    .build());
        }
        // we store them all at once to have a consistent state
        dataPrivacyWorkflowAppVariantRepository.saveAll(dataPrivacyWorkflowAppVariants);
        dataPrivacyWorkflowAppVariantRepository.flush();
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAll(statusRecords);
        dataPrivacyWorkflowAppVariantStatusRecordRepository.flush();
    }

    @Override
    public void processDataPrivacyWorkflow(DataPrivacyWorkflow dataPrivacyWorkflow) {

        DataPrivacyWorkflowStatus status = dataPrivacyWorkflow.getStatus();
        if (status == DataPrivacyWorkflowStatus.OPEN ||
                status == DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS) {
            //mark this workflow as being processed to avoid the worker to grab it
            markDataPrivacyWorkflowProcessingActive(dataPrivacyWorkflow);
            // not all external systems have responded yet, we call them again if necessary
            DataPrivacyWorkflowStatus newStatus = processDataPrivacyWorkflowAppVariants(dataPrivacyWorkflow);
            changeDataPrivacyWorkflowStatus(dataPrivacyWorkflow, newStatus);
            //it can happen that we find out that we are already done, so we update the status
            status = newStatus;
        }

        if (status == DataPrivacyWorkflowStatus.EXTERNALS_FINISHED ||
                status == DataPrivacyWorkflowStatus.INTERNAL_WORKFLOW_FINISHED) {
            //mark this workflow as being processed to avoid the worker to grab it
            markDataPrivacyWorkflowProcessingActive(dataPrivacyWorkflow);
            String notificationMessage = "";
            try {
                // all external systems are finished or failed
                switch (dataPrivacyWorkflow.getWorkflowType()) {
                    case DATA_COLLECTION:
                        notificationMessage = "Creation of data privacy report";

                        DataPrivacyDataCollection dataCollection = (DataPrivacyDataCollection) dataPrivacyWorkflow;
                        final IDataPrivacyReport privacyReport =
                                new DataPrivacyReport(
                                        "Gespeicherte Daten von " + dataCollection.getPerson().getFullName() +
                                                ", Stand " +
                                                timeService.toLocalTimeHumanReadable(
                                                        timeService.currentTimeMillisUTC()));

                        internalDataPrivacyService.collectInternalUserData(dataCollection.getPerson(), privacyReport);
                        changeDataPrivacyWorkflowStatus(dataCollection,
                                DataPrivacyWorkflowStatus.INTERNAL_WORKFLOW_FINISHED);
                        externalDataPrivacyService.addExternalDataCollectionResponsesToReport(dataCollection,
                                privacyReport);
                        final String zipFileUrl = createDataPrivacyReportZipFile(privacyReport, dataCollection);
                        emailDataPrivacyService.sendDataPrivacyReport(dataCollection, zipFileUrl);
                        notify(DataPrivacyReportFinalizedEvent.builder()
                                .dataCollection(dataCollection)
                                .trigger(dataCollection.getWorkflowTrigger())
                                .zipFileUrl(zipFileUrl)
                                .build());
                        changeDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
                        break;
                    case DATA_DELETION:
                        notificationMessage = "Deletion of person data";

                        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflow;
                        IPersonDeletionReport personDeletionReport = new PersonDeletionReport();
                        externalDataPrivacyService.addExternalDataDeletionResponsesToReport(dataDeletion,
                                personDeletionReport);
                        final Person personToBeDeleted = dataDeletion.getPerson();
                        internalDataPrivacyService.deleteInternalUserData(personToBeDeleted, personDeletionReport);
                        //mark this workflow as being processed to avoid the worker to grab it
                        markDataPrivacyWorkflowProcessingActive(dataPrivacyWorkflow);
                        emailDataPrivacyService.sendPersonDeletionConfirmation(dataDeletion);
                        notify(PersonDeleteConfirmation.builder()
                                .personId(personToBeDeleted.getId())
                                .email(personToBeDeleted.getEmail())
                                .fullName(personToBeDeleted.getFullName())
                                .firstName(personToBeDeleted.getFirstName())
                                .tenant(personToBeDeleted.getTenant())
                                .personCreated(personToBeDeleted.getCreated())
                                .personLastLoggedIn(personToBeDeleted.getLastLoggedIn())
                                .trigger(dataDeletion.getWorkflowTrigger())
                                .personDeletionReport(personDeletionReport)
                                .build());
                        changeDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
                        break;
                    default:
                        notificationMessage = "Type of data privacy workflow has unexpected value " +
                                dataPrivacyWorkflow.getWorkflowType();
                        throw new IllegalStateException("Unexpected value: " + dataPrivacyWorkflow.getWorkflowType());
                }
            } catch (Exception e) {
                // send team notification when the process of data privacy workflow failed
                teamNotificationService.sendTeamNotification(
                        dataPrivacyWorkflow.getPerson().getTenant(),
                        TEAM_NOTIFICATION_TOPIC_DATA_PRIVACY,
                        TeamNotificationPriority.ERROR_TECHNICAL_SINGLE_USER,
                        notificationMessage + " [workflow id '{}', person id '{}'] failed",
                        dataPrivacyWorkflow.getId(),
                        dataPrivacyWorkflow.getPerson().getId());
                log.error("Failed to process data privacy workflow '" + dataPrivacyWorkflow.getId() +
                        "': " + e, e);
                changeDataPrivacyWorkflowStatus(dataPrivacyWorkflow, DataPrivacyWorkflowStatus.FAILED);
            }
        }
    }

    private DataPrivacyWorkflowStatus processDataPrivacyWorkflowAppVariants(DataPrivacyWorkflow dataPrivacyWorkflow) {

        List<DataPrivacyWorkflowAppVariant> dataPrivacyWorkflowAppVariants =
                dataPrivacyWorkflowAppVariantRepository.findAllByDataPrivacyWorkflowOrderByCreatedDesc(
                        dataPrivacyWorkflow);
        long lastStatusTimeDataCollection = dataPrivacyWorkflow.getLastStatusChanged();
        //general timeout for this data collection, all external calls are now either failed or successful
        boolean workflowTimeoutElapsed = timeService.currentTimeMillisUTC() - lastStatusTimeDataCollection >
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout().toMillis();
        boolean waitingForExternals = false;

        for (DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant : dataPrivacyWorkflowAppVariants) {

            //mark this workflow as being processed to avoid the worker to grab it
            markDataPrivacyWorkflowProcessingActive(dataPrivacyWorkflow);
            // get last status record of data collection app variant
            final Optional<DataPrivacyWorkflowAppVariantStatusRecord> lastStatusRecord =
                    dataPrivacyWorkflowAppVariantStatusRecordRepository
                            .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataPrivacyWorkflowAppVariant);
            DataPrivacyWorkflowAppVariantStatus status =
                    lastStatusRecord.map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus)
                            .orElse(DataPrivacyWorkflowAppVariantStatus.OPEN);

            if (dataPrivacyWorkflowAppVariant.getAppVariant().hasExternalSystem()) {
                if (!dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().isEnabled()) {
                    // if the external data privacy workflow is disabled, the call is not done and already marked as finished
                    setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant,
                            DataPrivacyWorkflowAppVariantStatus.FINISHED);
                } else {
                    if (workflowTimeoutElapsed) {
                        externalDataPrivacyService.cancelExternalDataPrivacyWorkflowAppVariant(
                                dataPrivacyWorkflowAppVariant);
                    } else {
                        final DataPrivacyWorkflowStatus externalStatus =
                                externalDataPrivacyService.processExternalDataPrivacyWorkflowAppVariant(
                                        dataPrivacyWorkflowAppVariant);
                        if (externalStatus == DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS) {
                            waitingForExternals = true;
                        }
                    }
                }
            } else {
                //it's an internal system, so we do not need to call it,
                // the data gets collected internally when finalizing the report
                if (status != DataPrivacyWorkflowAppVariantStatus.FINISHED) {
                    setDataPrivacyWorkflowAppVariantStatus(dataPrivacyWorkflowAppVariant,
                            DataPrivacyWorkflowAppVariantStatus.FINISHED);
                }
            }
        }
        if (waitingForExternals) {
            return DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS;
        } else {
            return DataPrivacyWorkflowStatus.EXTERNALS_FINISHED;
        }
    }

    @Override
    public int processAllUnfinishedDataPrivacyWorkflows() {

        List<DataPrivacyWorkflow> openDataPrivacyWorkflows =
                dataPrivacyWorkflowRepository.findAllByStatusIsNotInAndLastProcessedBeforeOrderByLastStatusChanged(
                        EnumSet.of(DataPrivacyWorkflowStatus.FINISHED, DataPrivacyWorkflowStatus.FAILED),
                        //we wait a bit so that we do not grab a workflow that is currently being processed by another machine
                        timeService.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING
                );
        openDataPrivacyWorkflows.forEach(this::processDataPrivacyWorkflow);
        return openDataPrivacyWorkflows.size();
    }

    private String createDataPrivacyReportZipFile(IDataPrivacyReport privacyReport,
            DataPrivacyDataCollection dataCollection) {

        try {
            // add the mediaItems to the list of files
            List<String> fileNames = privacyReport.getMediaItems().stream()
                    .map(mi -> fileStorage.getInternalFileName(mi.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName())))
                    .collect(Collectors.toCollection(ArrayList::new));

            // add the files from the external systems to the list of files
            final String internalFolderPath = dataCollection.getInternalFolderPath();
            // collect the filenames from all data collection app variant responses of the given data collection
            fileNames.addAll(dataPrivacyWorkflowAppVariantResponseRepository
                    .findAllByDataCollectionAppVariantAndFileNameIsNotNull(dataCollection).stream()
                    .map(avr -> internalFolderPath + avr.getFileName())
                    .collect(Collectors.toList()));

            //add the documentItems to the list of files
            fileNames.addAll(privacyReport.getDocumentItems().stream()
                    .map(mi -> fileStorage.getInternalFileName(mi.getUrl()))
                    .collect(Collectors.toList()));

            // save the report
            final String reportHTMLFileName =
                    fileStorage.appendFileName(internalFolderPath, REPORT_HTML_FILE_NAME);
            final String reportJSONFileName =
                    fileStorage.appendFileName(internalFolderPath, REPORT_JSON_FILE_NAME);
            fileStorage.saveFile(converterService.convert(privacyReport, DataPrivacyReportFormat.HTML)
                            .getBytes(StandardCharsets.UTF_8),
                    MediaType.TEXT_HTML_VALUE, reportHTMLFileName);
            fileStorage.saveFile(converterService.convert(privacyReport, DataPrivacyReportFormat.JSON)
                            .getBytes(StandardCharsets.UTF_8),
                    MediaType.APPLICATION_JSON_VALUE, reportJSONFileName);
            fileNames.add(reportHTMLFileName);
            fileNames.add(reportJSONFileName);

            final String zipFileName =
                    fileStorage.appendFileName(internalFolderPath, ZIP_FILE_NAME);
            final String zipFileLocation = fileStorage.createZipFile(zipFileName, fileNames,
                    Collections.singletonList(internalFolderPath));
            log.debug("Data privacy report for data collection {} created", dataCollection.getId());
            return zipFileLocation;
        } catch (Exception e) {
            log.error("Failed to create data privacy report for data collection " + dataCollection.getId() + ": " +
                    e, e);
            throw e;
        }
    }

    @Override
    public void deleteOldDataPrivacyWorkflows() {
        final Duration maxRetentionTime = Duration.ofDays(dataPrivacyConfig.getMaxRetentionDataPrivacyReportsInDays());
        fileStorage.deleteOldFiles(PRIVACY_REPORT_FOLDER, maxRetentionTime, (fileName, fileAge) -> {
            log.info("Privacy report {} is {} old and will be deleted", fileName, fileAge);
            return true;
        });
        List<DataPrivacyWorkflow> finishedOldDataPrivacyWorkflows =
                dataPrivacyWorkflowRepository.findAllByStatusAndLastStatusChangedBeforeOrderByLastStatusChanged(
                        DataPrivacyWorkflowStatus.FINISHED,
                        timeService.currentTimeMillisUTC() - maxRetentionTime.toMillis());

        for (DataPrivacyWorkflow dataPrivacyWorkflow : finishedOldDataPrivacyWorkflows) {
            int responses = dataPrivacyWorkflowAppVariantResponseRepository
                    .deleteAllByDataPrivacyWorkflowAppVariant_DataPrivacyWorkflow(dataPrivacyWorkflow);
            int statusRecords = dataPrivacyWorkflowAppVariantStatusRecordRepository
                    .deleteAllByDataPrivacyWorkflowAppVariant_DataPrivacyWorkflow(dataPrivacyWorkflow);
            int appVariants = dataPrivacyWorkflowAppVariantRepository
                    .deleteAllByDataPrivacyWorkflow(dataPrivacyWorkflow);
            dataPrivacyWorkflowRepository.delete(dataPrivacyWorkflow);
            log.info("Deleted data privacy workflow with {} responses {} app variants and {} status records " +
                            "and age {}", responses, appVariants, statusRecords,
                    Duration.of(timeService.currentTimeMillisUTC() - dataPrivacyWorkflow.getLastStatusChanged(),
                            ChronoUnit.MILLIS));
        }
    }

    @Override
    public DataPrivacyDataCollection findDataCollectionById(String dataCollectionId) {
        return dataCollectionRepository.findById(dataCollectionId)
                .orElseThrow(() -> new DataPrivacyWorkflowNotFoundException(
                        "Could not find data privacy data collection with id '{}'", dataCollectionId));
    }

    @Override
    public DataPrivacyDataDeletion findDataDeletionById(String dataDeletionId) {
        return dataDeletionRepository.findById(dataDeletionId)
                .orElseThrow(() -> new DataPrivacyWorkflowNotFoundException(
                        "Could not find data privacy data deletion with id '{}'", dataDeletionId));
    }

    @Override
    public DataPrivacyWorkflowAppVariant findDataPrivacyWorkflowAppVariant(
            DataPrivacyDataCollection dataPrivacyDataCollection, AppVariant appVariant) {

        return dataPrivacyWorkflowAppVariantRepository
                .findByDataPrivacyWorkflowAndAppVariant(dataPrivacyDataCollection, appVariant)
                .orElseThrow(() -> new DataPrivacyWorkflowNotFoundException(
                        "App variant '{}' is not part of data privacy data collection with id '{}'",
                        appVariant.getAppVariantIdentifier(), dataPrivacyDataCollection.getId()));
    }

    @Override
    public DataPrivacyWorkflowAppVariant findDataPrivacyWorkflowAppVariant(
            DataPrivacyDataDeletion dataPrivacyDataDeletion, AppVariant appVariant) {

        return dataPrivacyWorkflowAppVariantRepository
                .findByDataPrivacyWorkflowAndAppVariant(dataPrivacyDataDeletion, appVariant)
                .orElseThrow(() -> new DataPrivacyWorkflowNotFoundException(
                        "App variant '{}' is not part of data privacy data deletion with id '{}'",
                        appVariant.getAppVariantIdentifier(), dataPrivacyDataDeletion.getId()));
    }

    private void changeDataPrivacyWorkflowStatus(DataPrivacyWorkflow dataPrivacyWorkflow,
            DataPrivacyWorkflowStatus newStatus) {

        final DataPrivacyWorkflowStatus oldStatus = dataPrivacyWorkflow.getStatus();
        if (oldStatus != newStatus) {
            dataPrivacyWorkflow.setStatus(newStatus);
            dataPrivacyWorkflow.setLastStatusChanged(timeService.currentTimeMillisUTC());
            dataPrivacyWorkflowRepository.saveAndFlush(dataPrivacyWorkflow);
            log.debug("Status of data privacy workflow '{}' changed from '{}' to '{}'",
                    dataPrivacyWorkflow.getId(), oldStatus, newStatus);
        }
    }

    private void markDataPrivacyWorkflowProcessingActive(DataPrivacyWorkflow dataPrivacyWorkflow) {

        final long now = timeService.currentTimeMillisUTC();
        final long elapsedTimeSinceLastProcessing = now - dataPrivacyWorkflow.getLastProcessed();
        if (elapsedTimeSinceLastProcessing > DELAY_MARK_PROCESSING_ACTIVE) {
            dataPrivacyWorkflow.setLastProcessed(now);
            dataPrivacyWorkflowRepository.saveAndFlush(dataPrivacyWorkflow);
            log.debug("Data privacy workflow '{}' marked as active processing", dataPrivacyWorkflow.getId());
        }
    }

}
