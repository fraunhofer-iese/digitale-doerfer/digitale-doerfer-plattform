/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.geoarea.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class GeoAreaDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private IGeoAreaService geoAreaService;

    @Autowired
    private GeoAreaRepository geoAreaRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        .changedOrDeletedEntity(GeoArea.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        .changedOrDeletedEntity(GeoArea.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build()
        );
    }

    @Override
    protected Collection<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        Collection<GeoArea> childAreas = geoAreaService.getDirectChildAreas(geoAreaToBeDeleted);

        logSummary.info("Found {} child areas of geo area to be deleted that will get a new parent area{}",
                childAreas.size(), toSummaryList(childAreas));
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        Collection<GeoArea> childAreas = geoAreaService.getDirectChildAreas(geoAreaToBeDeleted);
        logSummary.info("Found {} child areas of geo area to be deleted that get a new parent area",
                childAreas.size());
        for (GeoArea childArea : childAreas) {
            childArea.setParentArea(geoAreaToBeUsedInstead);
            logSummary.info("Set parent of {} to geo area to be used instead", childArea.toString());
            geoAreaService.store(childArea);
        }
        logSummary.info("Deleting {}", geoAreaToBeDeleted.toString());
        geoAreaRepository.delete(geoAreaToBeDeleted);
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        Set<GeoArea> geoAreasOfTenantToBeDeleted =
                geoAreaService.getGeoAreasForTenants(Collections.singleton(tenantToBeDeleted));
        logSummary.info("Found {} geo areas of tenant to be deleted that will get a new tenant{}",
                geoAreasOfTenantToBeDeleted.size(), toSummaryList(geoAreasOfTenantToBeDeleted));
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {
        Set<GeoArea> geoAreasOfTenantToBeDeleted =
                geoAreaService.getGeoAreasForTenants(Collections.singleton(tenantToBeDeleted));
        logSummary.info("Found {} geo areas of tenant to be deleted that will get a new tenant",
                geoAreasOfTenantToBeDeleted.size());
        for (GeoArea geoArea : geoAreasOfTenantToBeDeleted) {
            geoArea.setTenant(tenantToBeUsedInstead);
            logSummary.info("Set tenant of {} to tenant to be used instead", geoArea.toString());
            geoAreaService.store(geoArea);
        }
    }

    private String toSummaryList(Collection<GeoArea> geoAreas) {
        return CollectionUtils.isEmpty(geoAreas)
                ? ""
                : geoAreas.stream()
                .map(GeoArea::toString)
                .collect(Collectors.joining("\n", ":\n", ""));
    }

}
