/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class UserGeneratedContentFlagHasDifferentTypeException extends BadRequestException {

    private static final long serialVersionUID = 5749045623676097892L;

    private UserGeneratedContentFlagHasDifferentTypeException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static UserGeneratedContentFlagHasDifferentTypeException forTypeMismatch(String id, String actualType,
            String expectedType) {
        return new UserGeneratedContentFlagHasDifferentTypeException(
                "The user generated content flag with id {} is of type '{}', the expected type for this endpoint is '{}'",
                id, actualType, expectedType);
    }

    public static UserGeneratedContentFlagHasDifferentTypeException forInvalidType(String id, String invalidType) {
        return new UserGeneratedContentFlagHasDifferentTypeException(
                "The user generated content flag with id {} has invalid type '{}' that can not be loaded",
                id, invalidType);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE;
    }

}
