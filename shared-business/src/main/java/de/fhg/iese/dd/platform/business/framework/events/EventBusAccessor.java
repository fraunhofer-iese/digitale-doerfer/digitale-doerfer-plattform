/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingTimeoutException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BaseRuntimeException;
import de.fhg.iese.dd.platform.datamanagement.shared.config.EventProcessingConfig;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Internal class encapsulating the communication protocol used on the event bus.
 *
 * <li> Sending events </li>
 * <li> Waiting for events </li>
 * <li> Sending replies </li>
 * <li> Retrieving exceptions in the event chain </li>
 * <p/>
 * This is NOT a singleton, it needs to get instantiated for every participant of the event communication. To do so, the
 * {@link Scope} annotation with configuration SCOPE_PROTOTYPE is used.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EventBusAccessor {

    // Marker for event log messages
    public static final Marker EVENT_JSON_MARKER = MarkerManager.getMarker("EVENT_JSON");

    private final InternalEventBus internalEventBus;
    private final EventReflections eventReflections;
    private final Duration waitForEventTimeOut;
    private final long loggingThresholdMilliseconds;

    /**
     * incoming event type | outgoing event type | compensate function
     */
    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private final Table<Class<? extends BaseEvent>, Class<? extends BaseEvent>, CompensateEventFunction<? extends BaseEvent, ? extends BaseEvent>>
            compensateFunctions = HashBasedTable.create();
    /**
     * EventChainIdentifier | expected event type | future
     */
    @Getter(lazy = true, onMethod = @__({@SuppressWarnings("unchecked"), @SuppressFBWarnings}))
    private final Table<EventChainIdentifier, Class<? extends BaseEvent>, CompletableFuture<? extends BaseEvent>>
            repliesToWaitFor = HashBasedTable.create();
    private Logger log;
    private String ownerName;

    /**
     * Internal helper interface to unify {@link EventConsumingFunction} and {@link EventProcessingFunction}. Such
     * functions can never be registered externally, they are only internal.
     *
     * @param <E>
     *         the type of event to be processed
     */
    @FunctionalInterface
    private interface InternalEventProcessingFunction<E extends BaseEvent> {

        List<BaseEvent> processEvent(E event, EventProcessingContext context) throws Exception;

    }

    /**
     * Internal data structure used to route events on the event bus and keep track of the event chain
     */
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    static class EventRoutingKey {

        @Getter(AccessLevel.PACKAGE)
        private final EventProcessingContext senderContext;

        EventChainIdentifier getEventChainIdentifier() {
            return senderContext.getEventChainIdentifier();
        }

        @Override
        public String toString() {
            return "EventRoutingKey:" + formatEventChainIdentifier(getEventChainIdentifier());
        }

    }

    /**
     * Internal data structure used to route error events back in the event chain
     */
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    static class ErrorEventRoutingKey {

        @Getter(AccessLevel.PACKAGE)
        private final EventProcessingContext previousContext;

        EventChainIdentifier getEventChainIdentifier() {
            return previousContext.getEventChainIdentifier();
        }

        EventBusAccessor getPreviousEventConsumer() {
            return previousContext.getEventConsumer();
        }

        @Override
        public String toString() {
            return "ErrorEventRoutingKey:" + formatEventChainIdentifier(getEventChainIdentifier()) +
                    " Error:" + String.format("%08x", getPreviousEventConsumer().hashCode());
        }

    }

    /**
     * Internal data structure used to uniquely identify an event chain. For every event chain such an object is
     * created
     */
    static class EventChainIdentifier {

    }

    public EventBusAccessor(InternalEventBus internalEventBus, EventReflections eventReflections,
            EventProcessingConfig config) {
        log = LogManager.getLogger(this.getClass());
        ownerName = "unknown";
        this.internalEventBus = internalEventBus;
        this.eventReflections = eventReflections;
        this.waitForEventTimeOut = config.getWaitForEventTimeout();
        this.loggingThresholdMilliseconds = config.getLoggingThreshold().toMillis();
    }

    private static String getEventChainIdentifier(EventProcessingContext context) {
        return formatEventChainIdentifier(context.getEventChainIdentifier());
    }

    private static String formatEventChainIdentifier(EventChainIdentifier eventChainIdentifier) {
        if (eventChainIdentifier == null) {
            return "unknown";
        } else {
            return String.format("%08x", eventChainIdentifier.hashCode());
        }
    }

    public void setOwner(Object object) {
        final Class<?> ownerClass = object.getClass();
        log = LogManager.getLogger(ownerClass);
        ownerName = ownerClass.getSimpleName();
    }

    /**
     * Register this function to be executed when any event of that type occurs.
     *
     * @param <E>
     *         Type of event we want to register for
     * @param eventClass
     *         class of event we want to register for
     * @param consumer
     *         event consuming function
     */
    public <E extends BaseEvent> void registerEventProcessingFunction(Class<E> eventClass,
            EventProcessingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEvent(eventClass, toInternalEventProcessingFunction(consumer), executionStrategy, exceptionStrategy);
    }

    /**
     * Register this function to be executed when any event of that type occurs.
     *
     * @param <E>
     *         Type of event we want to register for
     * @param eventClass
     *         class of event we want to register for
     * @param consumer
     *         event consuming function
     */
    public <E extends BaseEvent> void registerEventConsumingFunction(Class<E> eventClass,
            EventConsumingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEvent(eventClass, toInternalEventProcessingFunction(consumer), executionStrategy, exceptionStrategy);
    }

    private <E extends BaseEvent> void registerForEvent(Class<E> eventClass,
            InternalEventProcessingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        if (log.isTraceEnabled()) {
            log.trace("Registering for {} ({})", toShortEventName(eventClass), executionStrategy);
        }
        checkStrategies(executionStrategy, exceptionStrategy);
        internalEventBus.registerEventTypeConsumer(eventClass,
                (event) -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy);
    }

    /**
     * Register this function to be executed when any event of that collection of types occurs.
     *
     * @param eventClasses classes of events we want to register for
     * @param consumer     event consuming function
     */
    public void registerEventProcessingFunction(Collection<Class<? extends BaseEvent>> eventClasses,
            EventProcessingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEvents(eventClasses, toInternalEventProcessingFunction(consumer), executionStrategy,
                exceptionStrategy);
    }

    /**
     * Register this function to be executed when any event of that collection of types occurs.
     *
     * @param eventClasses classes of events we want to register for
     * @param consumer     event consuming function
     */
    public void registerEventConsumingFunction(Collection<Class<? extends BaseEvent>> eventClasses,
            EventConsumingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEvents(eventClasses, toInternalEventProcessingFunction(consumer), executionStrategy,
                exceptionStrategy);
    }

    private void registerForEvents(Collection<Class<? extends BaseEvent>> eventClasses,
            InternalEventProcessingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        if (log.isTraceEnabled()) {
            log.trace("Registering for {} ({})", eventClasses.stream()
                    .map(this::toShortEventName)
                    .collect(Collectors.joining(", ")), executionStrategy);
        }
        checkStrategies(executionStrategy, exceptionStrategy);
        eventClasses.forEach(eventClass -> internalEventBus.registerEventTypeConsumer(eventClass,
                (event) -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy));
    }

    /**
     * Register this function to be executed when any event of that type or subtype occurs. All events have to be in a
     * sub-package of de.fhg.iese.dd.platform.business.
     *
     * @param <E>
     *         Type of event we want to register for
     * @param eventSuperClass
     *         superclass of events we want to register for
     * @param consumer
     *         event consuming function
     */
    public <E extends BaseEvent> void registerEventConsumingFunctionIncludingSubtypes(Class<E> eventSuperClass,
            EventConsumingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEventAndAllSubtypes(eventSuperClass, toInternalEventProcessingFunction(consumer), executionStrategy,
                exceptionStrategy);
    }

    public void registerEventConsumingFunctionIncludingSubtypes(
            Collection<Class<? extends BaseEvent>> eventSuperClasses,
            EventConsumingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEventsAndAllSubtypes(eventSuperClasses, toInternalEventProcessingFunction(consumer),
                executionStrategy, exceptionStrategy);
    }

    /**
     * Register this function to be executed when any event of that type or subtype occurs. All events have to be in a
     * sub-package of de.fhg.iese.dd.platform.business.
     *
     * @param <E>             Type of event we want to register for
     * @param eventSuperClass superclass of events we want to register for
     * @param consumer        event consuming function
     */
    public <E extends BaseEvent> void registerEventProcessingFunctionIncludingSubtypes(Class<E> eventSuperClass,
            EventProcessingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEventAndAllSubtypes(eventSuperClass, toInternalEventProcessingFunction(consumer), executionStrategy,
                exceptionStrategy);
    }

    public void registerEventProcessingFunctionIncludingSubtypes(
            Collection<Class<? extends BaseEvent>> eventSuperClasses,
            EventProcessingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        registerForEventsAndAllSubtypes(eventSuperClasses, toInternalEventProcessingFunction(consumer),
                executionStrategy, exceptionStrategy);
    }

    /**
     * Register this function to be executed when any event of that type or subtype occurs.
     * <p>
     * For better performance the subtypes are determined on registration.
     *
     * @param eventSuperClass   superclass of events we want to register for
     * @param consumer          event consuming function
     * @param executionStrategy execution strategy to be applied for this consumer
     * @param <E>               Type of event we want to register for
     */
    private <E extends BaseEvent> void registerForEventAndAllSubtypes(Class<E> eventSuperClass,
            InternalEventProcessingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        Set<Class<? extends E>> subtypes = eventReflections.allSubtypes(eventSuperClass);
        if (log.isTraceEnabled()) {
            log.trace("Registering for subtype of {}, interfered these additional subtypes {} ({})",
                    toShortEventName(eventSuperClass),
                    subtypes.stream()
                            .map(this::toShortEventName)
                            .collect(Collectors.joining(", ")), executionStrategy);
        }
        checkStrategies(executionStrategy, exceptionStrategy);
        internalEventBus.registerEventTypeConsumer(eventSuperClass,
                (E event) -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy);
        subtypes.forEach(eventClass -> internalEventBus.registerEventTypeConsumer(eventClass,
                event -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy));
    }

    /**
     * Register this function to be executed when any event of that type or subtype occurs.
     * <p>
     * For better performance the subtypes are determined on registration.
     *
     * @param eventClasses      Types of events we want to register for
     * @param consumer          event consuming function
     * @param executionStrategy execution strategy to be applied for this consumer
     */
    private void registerForEventsAndAllSubtypes(Collection<Class<? extends BaseEvent>> eventClasses,
            InternalEventProcessingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        Set<Class<? extends BaseEvent>> subtypes = eventReflections.allSubtypes(eventClasses);
        if (log.isTraceEnabled()) {
            log.trace("Registering for subtypes of {}, interfered these additional subtypes {} ({})",
                    eventClasses.stream()
                            .map(this::toShortEventName)
                            .collect(Collectors.joining(", ")),
                    subtypes.stream()
                            .filter(e -> !eventClasses.contains(e))
                            .map(this::toShortEventName)
                            .collect(Collectors.joining(", ")), executionStrategy);
        }
        checkStrategies(executionStrategy, exceptionStrategy);
        eventClasses.forEach(eventClass -> internalEventBus.registerEventTypeConsumer(eventClass,
                event -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy));
        subtypes.forEach(eventClass -> internalEventBus.registerEventTypeConsumer(eventClass,
                event -> onConsumeRegisteredEvent(event, consumer, executionStrategy, exceptionStrategy),
                executionStrategy));
    }

    private void checkStrategies(EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {
        if (executionStrategy == EventExecutionStrategy.AUTO) {
            throw new IllegalArgumentException(
                    "AUTO is not allowed as execution strategy for registering an event consumer manually");
        }
        if (exceptionStrategy == EventExecutionExceptionStrategy.AUTO) {
            throw new IllegalArgumentException(
                    "AUTO is not allowed as exception strategy for registering an event consumer manually");
        }
    }

    private String toShortEventName(Class<?> eventClass) {
        return eventClass.getName().replace("de.fhg.iese.dd.platform.business.", "");
    }

    /**
     * Fire this event asynchronous and do not wait for replies or exceptions.
     *
     * @param <E>
     *         Type of event that gets fired
     * @param trigger
     *         event that gets fired
     */
    public <E extends BaseEvent> void notifyTrigger(E trigger) {
        //new event chain
        EventProcessingContext initialContext = new EventProcessingContext(this, false);
        //this needs to be done asynchronous
        notifyWithoutHandlingExceptions(trigger, "trigger", initialContext, false);
    }

    /**
     * Fire this event asynchronous with some delay and do not wait for replies or exceptions.
     *
     * @param triggerSupplier
     *         supplier for event that gets fired
     * @param delay
     *         the time from now to delay sending the event
     * @param unit
     *         the time unit of the delay parameter
     * @param <E>
     *         Type of event that gets fired
     */
    public <E extends BaseEvent> void notifyTriggerDelayed(Supplier<E> triggerSupplier, long delay, TimeUnit unit) {
        internalEventBus.getScheduledExecutorService().schedule(() -> {
            try {
                E trigger = triggerSupplier.get();
                notifyTrigger(trigger);
            } catch (Exception e) {
                log.error("Failed to notify event delayed", e);
            }
        }, delay, unit);
    }

    /**
     * Fire this event asynchronous with some delay and wait for replies or exceptions.
     *
     * @param event           event that gets fired
     * @param eventsToWaitFor The types of event that should occur
     * @param delay           the time from now to delay sending the event
     * @param unit            the time unit of the delay parameter
     * @param <E>             Type of event that gets fired
     * @return A future for the first event of the specific type that was waited for
     */
    public <E extends BaseEvent> ScheduledFuture<BaseEvent> notifyTriggerAndWaitForAnyReplyDelayed(E event, Collection<Class<? extends BaseEvent>> eventsToWaitFor,
                                                                                                   long delay, TimeUnit unit) {

        return internalEventBus.getScheduledExecutorService().schedule(
                () -> notifyTriggerAndWaitForAnyReply(Collections.singleton(event), eventsToWaitFor),
                delay, unit);
    }

    /**
     * Fire this event asynchronous and do not handle the exceptions caused by this event. They will be forwarded to the
     * one that send the previous event in the event chain, meaning going backwards in the event chain.
     *
     * @param <E>
     *         Type of event that gets fired
     * @param reply
     *         event that gets fired as reply
     * @param context
     *         context of the existing event chain
     */
    public <E extends BaseEvent> void notifyWithoutHandlingExceptions(E reply, final EventProcessingContext context) {
        notifyWithoutHandlingExceptions(reply, "reply", context, false);
    }

    /**
     * Fire this event and do not handle the exceptions caused by this event. They will be forwarded to the one that
     * send the previous event in the event chain, meaning going backwards in the event chain.
     *
     * @param event
     *         event that gets fired
     * @param logMessage
     *         message part used in the log entry if trace is enabled
     * @param context
     *         context of the existing event chain
     * @param synchronousAllowed
     *         if true the consumers of the event can be executed synchronous
     * @param <E>
     *         Type of event that gets fired
     */
    private <E extends BaseEvent> void notifyWithoutHandlingExceptions(E event, String logMessage,
            final EventProcessingContext context, boolean synchronousAllowed) {

        if (log.isTraceEnabled()) {
            log.trace("Eventchain '{}': {} sent {}", getEventChainIdentifier(context), logMessage,
                    event.getLogMessage());
        }
        event.setEventRoutingKey(new EventRoutingKey(context));
        int numConsumers = internalEventBus.notify(event, synchronousAllowed);
        if (numConsumers == 0) {
            final Map<String, Object> logDetails = event.getLogDetails();
            logDetails.put("processor", ownerName);
            logDetails.put("eventChain", getEventChainIdentifier(context));
            logDetails.put("hint", "no consumers found");
            log.log(Level.DEBUG, EVENT_JSON_MARKER, logDetails);
        }
    }

    /**
     * Fire this event and register this compensate function to be executed when an exception occurs in the upcoming
     * event chain.
     * <h2>It is only possible to register <i>the same</i> compensate function
     * for the combination <code>consumedEvent.getClass()</code> and
     * <code>reply.getClass()</code></h2> <br/>
     * <strong>Registering two different functions will result in overwriting
     * the first one and thus in unwanted behavior</strong> <br/> <br/>
     *
     * @param <C>
     *         Type of consumed event
     * @param <S>
     *         Type of reply
     * @param consumedEvent
     *         the event we just got (actual value is unused, only needed for type checking of compensate function)
     * @param reply
     *         the reply we want to send out
     * @param compensateFunction
     *         a compensation function if something goes wrong with our reply
     * @param context
     *         required to track the event chain
     */
    public <C extends BaseEvent, S extends BaseEvent> void notifyAndRegisterCompensate(
            @SuppressWarnings("unused") C consumedEvent, S reply, CompensateEventFunction<C, S> compensateFunction,
            final EventProcessingContext context) {

        //we register the compensate function for the combination consumed/replied event type
        registerCompensateFunction(context.getConsumedEvent().getClass(), reply.getClass(), compensateFunction);

        notifyWithoutHandlingExceptions(reply, "reply (compensate function)", context, false);
    }

    /**
     * Publishes the given events and waits for any reply of the specified types to occur.
     *
     * @param eventsToWaitFor
     *         The types of event that should occur
     * @param eventsToNotify
     *         The events that are published and should trigger one of the eventsToWaitFor
     *
     * @return The first event of the specific type that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         If no event of the given types occurred as a reply within a predefined time span
     * @throws EventProcessingException
     *         If an exception occurred in one of the triggered event processors
     */
    public BaseEvent notifyTriggerAndWaitForAnyReply(Iterable<? extends BaseEvent> eventsToNotify,
            Collection<Class<? extends BaseEvent>> eventsToWaitFor)
            throws EventProcessingTimeoutException, EventProcessingException {

        try {
            internalEventBus.signalEventProcessingStart();

            //we are the first one in the event chain, so we create the first context
            EventProcessingContext initialContext = new EventProcessingContext(this, true);

            //create futures that are used to wait for the event to occur
            final CompletableFuture<?>[] futureForSingleEvents = eventsToWaitFor.stream()
                    .map(eventToWaitFor -> registerWaitingForReply(eventToWaitFor,
                            initialContext.getEventChainIdentifier()))
                    .toArray(CompletableFuture[]::new);

            //create a global, waiting for any future
            CompletableFuture<?> futureForAnyEvent = CompletableFuture.anyOf(futureForSingleEvents);

            for (BaseEvent eventToNotify : eventsToNotify) {
                //fire the events without registering a compensate function
                //since we just throw any exception in the get of the future and thus in this method
                notifyWithoutHandlingExceptions(eventToNotify, "trigger", initialContext, true);
            }

            boolean interrupted = false;
            try {
                BaseEvent reply = (BaseEvent) futureForAnyEvent.get(waitForEventTimeOut.toMillis(), TimeUnit.MILLISECONDS);
                if (log.isTraceEnabled()) {
                    log.trace(getLogMarker(eventsToNotify), "Eventchain '{}': Reply received {}",
                            getEventChainIdentifier(initialContext),
                            reply.getLogMessage());
                }
                return reply;
            } catch (InterruptedException e) {
                log.warn(getLogMarker(eventsToNotify), "Eventchain '{}': Waiting for event was interrupted",
                        getEventChainIdentifier(initialContext));
                //it seems to be important to set the interrupted state of the thread back
                //see https://www.ibm.com/developerworks/java/library/j-jtp05236/index.html?ca=drs-#2.1
                interrupted = true;
                throw new EventProcessingTimeoutException("Waiting for event was interrupted", e);
            } catch (TimeoutException e) {
                log.error(getLogMarker(eventsToNotify), "Eventchain '{}': Waiting for event timed out",
                        getEventChainIdentifier(initialContext));
                throw new EventProcessingTimeoutException("Waiting for event timed out", e);
            } catch (ExecutionException e) {
                Throwable cause = e.getCause();
                if (cause instanceof EventProcessingException) {
                    throw (EventProcessingException) cause;
                } else {
                    log.warn(getLogMarker(eventsToNotify),
                            "Eventchain '{}': An unknown exception occurred while waiting",
                            getEventChainIdentifier(initialContext), e);
                    throw new EventProcessingException("An unknown exception occurred while waiting", e);
                }
            } finally {
                eventsToWaitFor.forEach(eventToWaitFor -> unregisterWaitingForReply(eventToWaitFor,
                        initialContext.getEventChainIdentifier()));
                if (interrupted) {
                    //see comment above
                    Thread.currentThread().interrupt();
                }
            }
        } finally {
            internalEventBus.signalEventProcessingFinish();
        }
    }

    private Marker getLogMarker(Iterable<? extends BaseEvent> eventsToNotify) {

        for (BaseEvent baseEvent : eventsToNotify) {
            Marker logMarker = baseEvent.getLogMarker();
            if (logMarker != null) {
                return logMarker;
            }
        }
        return null;
    }

    private <E extends BaseEvent> void onConsumeRegisteredEvent(E eventToConsume,
            InternalEventProcessingFunction<E> eventProcessingFunction, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy) {

        EventProcessingContext senderContext = eventToConsume.getEventRoutingKey().getSenderContext();
        Marker logMarker = eventToConsume.getLogMarker();
        //chain the current context to the previous one and use our errorReplyMarker so that we get the error events later
        EventProcessingContext currentContext = new EventProcessingContext(this, senderContext, eventToConsume);

        long start = System.nanoTime();
        try {
            internalEventBus.signalEventProcessingStart();
            if (log.isTraceEnabled()) {
                log.trace(logMarker, "Eventchain '{}': Processing started {}", getEventChainIdentifier(currentContext),
                        eventToConsume.getLogMessage());
            }
            //call the registered consume function with the current context
            List<BaseEvent> producedEvents = eventProcessingFunction.processEvent(eventToConsume, currentContext);
            long processingDuration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
            if (processingDuration > loggingThresholdMilliseconds) {
                log.log(Level.DEBUG, EVENT_JSON_MARKER,
                        createLogDetails(eventToConsume, executionStrategy, currentContext, processingDuration));
            }
            if (log.isTraceEnabled()) {
                log.trace(logMarker, "Eventchain '{}': Processing finished in {} ms {}",
                        getEventChainIdentifier(currentContext), processingDuration, eventToConsume.getLogMessage());
            }
            if (!CollectionUtils.isEmpty(producedEvents)) {
                //in this case the event processing is completely finished and we can use this thread to deal with the event processing of the produced events
                for (BaseEvent e : producedEvents) {
                    //we need to check for null since EventProcessingFunction is allowed to return null
                    if (e != null) {
                        notifyWithoutHandlingExceptions(e, "reply", currentContext, true);
                    }
                }
            }
        } catch (Exception consumeException) {
            if (!senderContext.isWaitingForReply()) {
                //the sender is not waiting for reply, so the exception would otherwise be lost
                log.warn(logMarker, "Exception while processing ({}) \"{}\" : {}",
                        exceptionStrategy == EventExecutionExceptionStrategy.PROPAGATE_EXCEPTION
                                ? "propagating to trigger"
                                : "ignored",
                        eventToConsume, consumeException.toString());
                long processingDuration = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
                Map<String, Object> logDetails =
                        createLogDetails(eventToConsume, executionStrategy, currentContext, processingDuration);
                if (consumeException instanceof BaseRuntimeException castedException) {
                    logDetails.put("exceptionType", castedException.getClientExceptionType());
                    logDetails.put("exceptionMessage", castedException.getMessage());
                } else {
                    logDetails.put("exceptionType", consumeException.getClass().getName());
                    logDetails.put("exceptionMessage", consumeException.toString());
                }
                log.log(Level.DEBUG, EVENT_JSON_MARKER, logDetails);
            }
            if (log.isTraceEnabled()) {
                log.trace(logMarker, "Exception details:", consumeException);
            }
            //depending on the exception strategy the exception gets propagated backwards or ignored
            if (exceptionStrategy == EventExecutionExceptionStrategy.PROPAGATE_EXCEPTION) {
                //create an error event that wraps the exception and the cause
                ErrorEvent<E> errorEvent =
                        new ErrorEvent<>(eventToConsume, consumeException, new ErrorEventRoutingKey(senderContext));
                //call the previous event consumer to handle the error event
                errorEvent.getErrorEventRoutingKey().getPreviousEventConsumer().onConsumeErrorEvent(errorEvent);
            }
        } finally {
            internalEventBus.signalEventProcessingFinish();
        }
    }

    private <E extends BaseEvent> Map<String, Object> createLogDetails(E eventToConsume,
            EventExecutionStrategy executionStrategy,
            EventProcessingContext currentContext, long processingDuration) {
        final Map<String, Object> logDetails = eventToConsume.getLogDetails();
        logDetails.put("processingDuration", Long.toString(processingDuration));
        logDetails.put("processor", ownerName);
        logDetails.put("executionStrategy", executionStrategy);
        logDetails.put("eventChain", getEventChainIdentifier(currentContext));
        return logDetails;
    }

    private <E extends BaseEvent> void onConsumeReplyEvent(E eventToConsume) {

        EventChainIdentifier eventChainIdentifier = eventToConsume.getEventRoutingKey().getEventChainIdentifier();
        //find the future that was waiting for that reply
        @SuppressWarnings("unchecked")
        CompletableFuture<E> waitingForReplyFuture =
                (CompletableFuture<E>) lookupWaitingForReply(eventToConsume.getClass(), eventChainIdentifier);

        if (waitingForReplyFuture != null) {
            waitingForReplyFuture.complete(eventToConsume);
        } else {
            log.error(eventToConsume.getLogMarker(), "Eventchain '{}': Triggered for reply, but no future found: {}",
                    formatEventChainIdentifier(eventChainIdentifier), eventToConsume.getLogMessage());
        }
    }

    private <C extends BaseEvent, S extends BaseEvent> void onConsumeErrorEvent(ErrorEvent<S> errorEvent) {

        ErrorEventRoutingKey routingKey = errorEvent.getErrorEventRoutingKey();

        //the context we were using when consuming the event that caused us to send the event out that caused the exception
        EventProcessingContext contextDuringConsumption = routingKey.getPreviousContext();

        //we consumed this event, this is null if we are at the top of the event chain
        @SuppressWarnings("unchecked")
        C consumedEvent = (C) contextDuringConsumption.getConsumedEvent();

        //then we sent out this event
        S sentEvent = errorEvent.getCausingEvent();

        //and this exception occurred
        Exception causedException = errorEvent.getException();
        try {
            internalEventBus.signalEventProcessingStart();

            if (contextDuringConsumption.getPreviousContext() == null) {
                //we are at the origin of the event chain, we can not push the exception to the previous one
                //wrap the exception so that it is understandable what caused it
                EventProcessingException processingException = new EventProcessingException(sentEvent, causedException);

                //now we try to check if there is a future waiting for replies

                //find the future that was waiting for that reply
                Collection<CompletableFuture<? extends BaseEvent>> waitingForReplyFutures =
                        lookupWaitingForReply(routingKey.getEventChainIdentifier());

                if (waitingForReplyFutures.isEmpty()) {
                    //the error event would get lost, so we at least log it
                    log.debug(sentEvent.getLogMarker(), "Exception caused by sending \"{}\": {}",
                            sentEvent, causedException);
                } else {
                    for (CompletableFuture<? extends BaseEvent> waitingForReplyFuture : waitingForReplyFutures) {
                        waitingForReplyFuture.completeExceptionally(processingException);
                    }
                }
            } else {
                //we are in between and can check if there is a compensate function available
                @SuppressWarnings("unchecked")
                CompensateEventFunction<C, S> compensateFunction =
                        (CompensateEventFunction<C, S>) lookupCompensateFunction(consumedEvent.getClass(),
                                sentEvent.getClass());
                if (compensateFunction == null) {
                    //we have to re-throw the exception to the previous sender

                    log.debug(consumedEvent.getLogMarker(),
                            "Default handling of Exception \"{}\"  for consumed \"{}\" and sent \"{}\"",
                            causedException, consumedEvent, sentEvent);

                    //send the exception to the previous sender and attach the event it was sending
                    ErrorEvent<C> newErrorEvent = new ErrorEvent<>(consumedEvent, causedException,
                            new ErrorEventRoutingKey(contextDuringConsumption.getPreviousContext()));

                    //call the previous event consumer to handle the error event
                    contextDuringConsumption.getPreviousContext().getEventConsumer().onConsumeErrorEvent(newErrorEvent);

                } else {
                    //we have a dedicated compensate function
                    try {
                        //now we can compensate the event we sent out, based on the information in the consumed event
                        compensateFunction.compensateEvent(consumedEvent, sentEvent, causedException,
                                contextDuringConsumption);
                    } catch (Exception compensateException) {
                        log.debug(consumedEvent.getLogMarker(),
                                "Exception while compensating \"{}\" : \"{}\"",
                                errorEvent, compensateException.getMessage());
                        //now an exception occurred while compensating an exception that occurred when we sent out an event
                        //this consumedEvent seems to be the source of all trouble since the event we sent out based on it caused exceptions
                        //create an error event that wraps the exception and the cause
                        ErrorEvent<C> newErrorEvent = new ErrorEvent<>(consumedEvent, compensateException,
                                new ErrorEventRoutingKey(contextDuringConsumption.getPreviousContext()));

                        //we send it to the previous error handler and attach the event we initially consumed
                        contextDuringConsumption.getPreviousContext().getEventConsumer().onConsumeErrorEvent(
                                newErrorEvent);
                    }
                }
            }
        } finally {
            internalEventBus.signalEventProcessingFinish();
        }
    }

    private synchronized <C extends BaseEvent, S extends BaseEvent> CompensateEventFunction<? extends BaseEvent, ? extends BaseEvent> lookupCompensateFunction(
            Class<C> consumedEventType, Class<S> sentEventType) {
        return getCompensateFunctions().get(consumedEventType, sentEventType);
    }

    private synchronized void registerCompensateFunction(Class<? extends BaseEvent> consumedEventType,
            Class<? extends BaseEvent> sentEventType,
            CompensateEventFunction<? extends BaseEvent, ? extends BaseEvent> compensateFunction) {
        getCompensateFunctions().put(consumedEventType, sentEventType, compensateFunction);
    }

    private synchronized <E extends BaseEvent> void unregisterWaitingForReply(Class<E> eventToWaitFor,
            EventChainIdentifier eventChainIdentifier) {

        CompletableFuture<? extends BaseEvent> waitingForReplyFuture =
                getRepliesToWaitFor().remove(eventChainIdentifier, eventToWaitFor);
        try {
            if (waitingForReplyFuture != null) {
                waitingForReplyFuture.cancel(true);
            }
        } catch (Exception e) {
            //does not really matter
            log.error("Exception while canceling futures", e);
        }
        internalEventBus.unregisterReplyConsumer(eventChainIdentifier, eventToWaitFor);
    }

    private synchronized <E extends BaseEvent> CompletableFuture<E> registerWaitingForReply(Class<E> eventToWaitFor,
            EventChainIdentifier eventChainIdentifier) {
        CompletableFuture<E> waitingForReplyFuture = new CompletableFuture<>();
        getRepliesToWaitFor().put(eventChainIdentifier, eventToWaitFor, waitingForReplyFuture);
        internalEventBus.registerReplyConsumer(eventChainIdentifier, eventToWaitFor, this::onConsumeReplyEvent);
        return waitingForReplyFuture;
    }

    private synchronized <E extends BaseEvent> CompletableFuture<? extends BaseEvent> lookupWaitingForReply(
            Class<E> eventType, EventChainIdentifier eventChainIdentifier) {
        return getRepliesToWaitFor().get(eventChainIdentifier, eventType);
    }

    private synchronized Collection<CompletableFuture<? extends BaseEvent>> lookupWaitingForReply(
            EventChainIdentifier eventChainIdentifier) {
        //since the lookup gives out the internal data structure of the table we have to create a copy to avoid concurrent modifications
        return new ArrayList<>(getRepliesToWaitFor().row(eventChainIdentifier).values());
    }

    /**
     * Check if there are events being processed. This includes also the event distribution, not only the bare
     * processing in the event processors.
     *
     * @return true if there are events being processed
     */
    public boolean areEventsInProcessing() {
        return internalEventBus.areEventsInProcessing();
    }

    private <E extends BaseEvent> InternalEventProcessingFunction<E> toInternalEventProcessingFunction(
            EventConsumingFunction<E> consumeEventFunction) {
        return (E e, EventProcessingContext c) -> {
            consumeEventFunction.consumeEvent(e, c);
            return Collections.emptyList();
        };
    }

    private <E extends BaseEvent> InternalEventProcessingFunction<E> toInternalEventProcessingFunction(
            EventProcessingFunction<E> eventProcessingFunction) {
        return (E e, EventProcessingContext c) -> eventProcessingFunction.processEvent(e);
    }

}
