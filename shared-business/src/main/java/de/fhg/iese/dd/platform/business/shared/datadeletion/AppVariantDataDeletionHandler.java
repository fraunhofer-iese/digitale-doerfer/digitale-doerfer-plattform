/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.datadeletion;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantTenantContractRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageAreaSelectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

import static de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import static de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;

@Component
public class AppVariantDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private IAppService appService;
    @Autowired
    private AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;
    @Autowired
    private AppVariantUsageRepository appVariantUsageRepository;
    @Autowired
    private AppVariantGeoAreaMappingRepository appVariantGeoAreaMappingRepository;
    @Autowired
    private AppVariantTenantContractRepository appVariantTenantContractRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        // AppVariantUsage references persons
                        .referencedEntity(Person.class)
                        .changedOrDeletedEntity(AppVariant.class)
                        .changedOrDeletedEntity(AppVariantUsage.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        // AppVariantUsage references persons
                        .referencedEntity(Person.class)
                        // AppVariant is indirectly changed
                        .changedOrDeletedEntity(AppVariant.class)
                        .changedOrDeletedEntity(AppVariantUsageAreaSelection.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return unmodifiableList(
                DeleteDependentEntitiesDeletionStrategy.forTriggeringEntity(AppVariant.class)
                        // no need to reference anything here, since this strategy already deals with everything
                        .changedOrDeletedEntity(AppVariant.class)
                        .changedOrDeletedEntity(AppVariantVersion.class)
                        .changedOrDeletedEntity(LegalText.class)
                        .changedOrDeletedEntity(LegalTextAcceptance.class)
                        .changedOrDeletedEntity(AppVariantUsage.class)
                        .changedOrDeletedEntity(AppVariantUsageAreaSelection.class)
                        .checkImpact(appService::logHumanReadableAppVariantStatistics)
                        .deleteData(appService::deleteAppVariantHard)
                        .build()
        );
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        for (AppVariant appVariant : appService.findAllAppVariants()) {
            long countSelections =
                    appVariantUsageAreaSelectionRepository.countByAppVariantUsage_AppVariantAndSelectedArea(
                            appVariant, geoAreaToBeDeleted);
            if (countSelections > 0) {
                logSummary.info("{} geo area selections to be moved to new geo area in {}", countSelections,
                        appVariant.toString());
            }
        }
        final Set<AppVariantGeoAreaMapping> mappings =
                appVariantGeoAreaMappingRepository.findAllByGeoArea(geoAreaToBeDeleted);
        for (AppVariantGeoAreaMapping mapping : mappings) {
            logSummary.info("Mapping with contract for tenant '{}' and notes '{}' to be moved to new geo area in {}\n" +
                            "!IMPORTANT! Also adjust this in the data init, otherwise this mapping gets deleted on next data init !IMPORTANT!",
                    mapping.getContract().getTenant(), mapping.getContract().getNotes(),
                    mapping.getContract().getAppVariant().toString());
        }
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        Set<AppVariantUsageAreaSelection> selectionsToBeChanged =
                appVariantUsageAreaSelectionRepository.findSelectionsOfGeoAreaAAndNotGeoAreaB(geoAreaToBeDeleted,
                        geoAreaToBeUsedInstead);

        for (AppVariantUsageAreaSelection selection : selectionsToBeChanged) {
            selection.setSelectedArea(geoAreaToBeUsedInstead);
        }
        appVariantUsageAreaSelectionRepository.saveAll(selectionsToBeChanged);
        int deletedSelections = appVariantUsageAreaSelectionRepository.deleteBySelectedArea(geoAreaToBeDeleted);
        logSummary.info("{} selections moved to new geo area, {} selections were already done",
                selectionsToBeChanged.size(), deletedSelections);

        final Set<AppVariantGeoAreaMapping> mappings =
                appVariantGeoAreaMappingRepository.findAllByGeoArea(geoAreaToBeDeleted);
        for (AppVariantGeoAreaMapping mapping : mappings) {
            mapping.setGeoArea(geoAreaToBeUsedInstead);
            logSummary.info("Mapping with  contract for tenant '{}' and notes '{}' moved to new geo area in {}\n" +
                            "!IMPORTANT! Also adjust this in the data init, otherwise this mapping gets deleted on next data init !IMPORTANT!",
                    mapping.getContract().getTenant(), mapping.getContract().getNotes(),
                    mapping.getContract().getAppVariant().toString());
            appVariantGeoAreaMappingRepository.save(mapping);
        }
        appVariantGeoAreaMappingRepository.flush();
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {

        final Set<AppVariantTenantContract> contracts =
                appVariantTenantContractRepository.findAllByTenant(tenantToBeDeleted);
        for (AppVariantTenantContract contract : contracts) {
            final AppVariant impactedAppVariant = contract.getAppVariant();
            logSummary.info("App variant tenant contract for {} with notes '{}' to be moved to new tenant\n" +
                            "!IMPORTANT! Also adjust this in the data init, otherwise this mapping gets overwritten on next data init !IMPORTANT!",
                    impactedAppVariant.toString(), contract.getNotes());

            long appVariantUsagesOfPersonsInTenant =
                    appVariantUsageRepository.countByAppVariantAndPerson_Community(impactedAppVariant,
                            tenantToBeDeleted);
            logSummary.info("{} is used by {} persons living in tenant to be deleted",
                    impactedAppVariant.toString(), appVariantUsagesOfPersonsInTenant);
        }
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {

        final Set<AppVariantTenantContract> contractsToBeDeleted =
                appVariantTenantContractRepository.findAllByTenant(tenantToBeDeleted);
        for (AppVariantTenantContract contractToBeDeleted : contractsToBeDeleted) {
            final Set<AppVariantTenantContract> contractsToBeUsedInstead =
                    appVariantTenantContractRepository.findAllByTenantAndAppVariant(tenantToBeDeleted,
                            contractToBeDeleted.getAppVariant());
            AppVariantTenantContract contract = null;
            if (contractsToBeUsedInstead.size() > 1) {
                logSummary.warn("More than one app variant tenant contract found for {}, taking first one.",
                        contractToBeDeleted.getAppVariant().toString());
                //noinspection OptionalGetWithoutIsPresent
                contract = contractsToBeUsedInstead.stream()
                        .min(Comparator.comparing(AppVariantTenantContract::getCreated))
                        .get();
            }
            if (contractsToBeUsedInstead.size() == 1) {
                contract = contractsToBeUsedInstead.stream().findFirst().get();
            }
            if (CollectionUtils.isEmpty(contractsToBeUsedInstead)) {
                //there is no contract, we need a new one
                contract = AppVariantTenantContract.builder()
                        .appVariant(contractToBeDeleted.getAppVariant())
                        .tenant(tenantToBeUsedInstead)
                        .notes("Moved from tenant '" + tenantToBeDeleted.getName() + "' to this one on " +
                                timeService.toLocalTimeHumanReadable(timeService.currentTimeMillisUTC()) +
                                " (" + contractToBeDeleted.getNotes() + ")")
                        .build()
                        .withAutogeneratedId();
            }
            if (contract == null) {
                logSummary.error("Failed to get contract for app variant tenant contract for {}.",
                        contractToBeDeleted.getAppVariant().toString());
                return;
            }
            contract.setTenant(tenantToBeUsedInstead);
            logSummary.info("App variant tenant contract for {} with notes '{}' to be moved to new tenant\n" +
                            "!IMPORTANT! Also adjust this in the data init, otherwise a new contract is created on next data init !IMPORTANT!",
                    contract.getAppVariant().toString(), contract.getNotes());
            appVariantTenantContractRepository.save(contract);
        }
        appVariantTenantContractRepository.flush();
    }

}
