/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

public abstract class ActionRoleRestricted extends Action<Permission> {

    /**
     * If a {@link RoleAssignment} with one of these roles exist, the action is allowed.
     */
    protected abstract Collection<Class<? extends BaseRole<?>>> rolesAllowed();

    @Override
    public Permission decidePermission(Collection<RoleAssignment> roleAssignments) {

        Collection<Class<? extends BaseRole<?>>> expectedRoles = rolesAllowed();
        if (roleAssignments.stream()
                .map(RoleAssignment::getRole)
                .anyMatch(expectedRoles::contains)) {
            return Permission.ALLOWED;
        } else {
            return Permission.DENIED;
        }
    }

    @Override
    public String getPermissionDescription(Function<Class<? extends BaseRole<?>>, BaseRole<?>> roleMapper) {
        return "Callers with role assignments \"" +
                rolesAllowed().stream()
                        .map(roleMapper)
                        .map(BaseRole::getKey)
                        .collect(Collectors.joining(", ")) +
                "\" are allowed to do this action.";
    }

    @Override
    public List<Class<? extends BaseRole<?>>> getPermittedRoles() {
        return rolesAllowed().stream()
                .sorted(Comparator.comparing(Class::getSimpleName))
                .distinct()
                .collect(Collectors.toList());
    }

}
