/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import java.util.List;

/**
 * Consumes an event and produces new ones
 *
 * @param <E>
 *         Type of event to consume
 */
@FunctionalInterface
public interface EventProcessingFunction<E extends BaseEvent> {

    /**
     * Consume an event and produce new ones
     *
     * @param event event that should be processed
     *
     * @return ordered list of events to be emitted on the event bus, can be null if no events are emitted, null can be
     *         included in the list, too
     *
     * @throws Exception if something fails while processing the event
     */
    List<BaseEvent> processEvent(E event) throws Exception;

}
