/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Component
public class MediaItemSizeUpdaterAdminTask extends BaseAdminTask {

    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    private IMediaItemService mediaItemService;

    @Override
    public String getName() {
        return "UpdateMediaItemSize";
    }

    @Override
    public String getDescription() {
        int currentSizeVersion = mediaItemService.getCurrentSizeVersion();
        return "Convert MediaItems with older size version ( <"+currentSizeVersion+" ) to newer size version. " +
                "Uses current size version "+currentSizeVersion+" for the converted MediaItems. "+
                "Be aware that this might take longer if a lot of image sizes need to be created.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        int currentSizeVersion = mediaItemService.getCurrentSizeVersion();
        processPages(logSummary, parameters,
                (pageRequest) -> mediaItemRepository
                    .findAllBySizeVersionIsLessThanOrderByCreatedDesc(currentSizeVersion, pageRequest),
                (mediaItemsToBeUpdated) -> {
                    processParallel(
                            (MediaItem m) -> {
                                mediaItemService.updateMediaItemToCurrentSizeVersion(m, logSummary,
                                        parameters.isDryRun());
                                logSummary.info("{}: Update finished", m.getId());
                            },
                            mediaItemsToBeUpdated.getContent(), logSummary, parameters);
                    mediaItemRepository.flush();
                });
    }

}
