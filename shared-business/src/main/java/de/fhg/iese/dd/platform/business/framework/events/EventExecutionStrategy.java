/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;

/**
 * Strategy how {@link EventConsumingFunction} and {@link EventProcessingFunction} should be executed by the {@link
 * EventBusAccessor}.
 */
public enum EventExecutionStrategy {
    /**
     * The event processing needs to be done asynchronous, e.g. because it is time consuming.
     */
    ASYNCHRONOUS_REQUIRED,
    /**
     * The event processing should be done asynchronous, but it might also be executed synchronous.
     */
    ASYNCHRONOUS_PREFERRED,
    /**
     * The event processing should be done synchronous, but it might also be executed asynchronous.
     */
    SYNCHRONOUS_PREFERRED,

    /**
     * Alias for SYNCHRONOUS_PREFERRED, but can be overwritten by a top level annotation: <br/> If used on @{@link
     * EventProcessing} the execution strategy of the @{@link
     * EventProcessor} annotation is used.
     * <p/>
     * Only used as default in annotations to express that no explicit selection was done.
     */
    AUTO

}
