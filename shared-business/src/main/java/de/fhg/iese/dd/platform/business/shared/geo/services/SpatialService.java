/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Axel Wickenkamp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.BoundingBox;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.geojson.GeoJsonReader;
import org.locationtech.spatial4j.context.jts.JtsSpatialContext;
import org.locationtech.spatial4j.distance.DistanceUtils;
import org.locationtech.spatial4j.shape.Circle;
import org.locationtech.spatial4j.shape.Point;
import org.locationtech.spatial4j.shape.SpatialRelation;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
class SpatialService extends BaseService implements ISpatialService {

    private static final JtsSpatialContext jtsContext = JtsSpatialContext.GEO;
    private static final GeoJsonReader geoJsonReader =
            new GeoJsonReader(jtsContext.getShapeFactory().getGeometryFactory());

    @Override
    public double distanceInKM(GPSLocation loc1, GPSLocation loc2) {
        Point p1 = gpsLocationToPoint(loc1);
        Point p2 = gpsLocationToPoint(loc2);
        return jtsContext.calcDistance(p1, p2) * DistanceUtils.DEG_TO_KM;
    }

    @Override
    public boolean isWithinBoundary(Geometry geometry, GPSLocation location) {

        Objects.requireNonNull(location);

        if (geometry == null || geometry.isEmpty()) {
            return false;
        }

        Geometry point = jtsContext.getShapeFactory().getGeometryFactory().createPoint(
                new Coordinate(location.getLongitude(), location.getLatitude()));

        return geometry.contains(point);
    }

    @Override
    public Geometry createGeometryFromGeoArea(GeoArea geoArea) {

        try {
            return createGeometryFromGeoJson(geoArea.getBoundaryJson());
        } catch (Exception e) {
            log.error("Failed to build boundary geometry of {} : {}", geoArea, e.toString());
            throw new RuntimeException(e);
        }
    }

    @Override
    @SneakyThrows
    public Geometry createGeometryFromGeoJson(String geoJson) {

        if (StringUtils.isEmpty(geoJson)) {
            // empty geometry
            return jtsContext.getShapeFactory().getGeometryFactory().createGeometryCollection();
        } else {
            return geoJsonReader.read(geoJson);
        }
    }

    @Override
    public void checkGeometry(GeoArea geoArea) {

        Geometry geometry = createGeometryFromGeoArea(geoArea);
        if (!geometry.isValid()) {
            log.error("Geometry of {} is invalid!", geoArea);
            throw new IllegalStateException("Geometry of " + geoArea + " is invalid");
        }
    }

    @Override
    public BoundingBox calculateBoundingBox(GeoArea geoArea) {

        return calculateBoundingBox(createGeometryFromGeoArea(geoArea));
    }

    @Override
    public BoundingBox calculateBoundingBox(Geometry geometry) {

        Envelope envelope = geometry.getEnvelopeInternal();
        return BoundingBox.builder()
                .min(new GPSLocation(envelope.getMinY(), envelope.getMinX()))
                .max(new GPSLocation(envelope.getMaxY(), envelope.getMaxX()))
                .build();
    }

    @Override
    public boolean intersects(BoundingBox boundingBoxA, BoundingBox boundingBoxB) {

        double minXA = boundingBoxA.min().getLongitude();
        double minYA = boundingBoxA.min().getLatitude();
        double maxXA = boundingBoxA.max().getLongitude();
        double maxYA = boundingBoxA.max().getLatitude();
        double minXB = boundingBoxB.min().getLongitude();
        double minYB = boundingBoxB.min().getLatitude();
        double maxXB = boundingBoxB.max().getLongitude();
        double maxYB = boundingBoxB.max().getLatitude();

        return !(minXA > maxXB || maxXA < minXB ||
                minYA > maxYB || maxYA < minYB);
    }

    @Override
    public boolean isWithinPerimeter(GPSLocation start, GPSLocation end, GPSLocation loc) {

        double newX = start.getLongitude() + (end.getLongitude() - start.getLongitude()) / 2.0;
        double newY = start.getLatitude() + (end.getLatitude() - start.getLatitude()) / 2.0;

        Point endPoint = gpsLocationToPoint(end);
        Point middle = jtsContext.getShapeFactory().pointXY(newX, newY);

        Circle perimeter = jtsContext.getShapeFactory().circle(middle, jtsContext.calcDistance(middle, endPoint));

        Point pLoc = gpsLocationToPoint(loc);

        return perimeter.relate(pLoc) == SpatialRelation.CONTAINS;
    }

    private Point gpsLocationToPoint( GPSLocation loc ) {
        return jtsContext.getShapeFactory().pointXY( loc.getLongitude(), loc.getLatitude() );
    }

}
