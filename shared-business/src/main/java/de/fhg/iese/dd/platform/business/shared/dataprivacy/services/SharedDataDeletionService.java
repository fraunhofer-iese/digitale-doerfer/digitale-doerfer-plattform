/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageAreaSelectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextAcceptanceRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;

@Service
class SharedDataDeletionService extends BaseDataDeletionService implements ISharedDataDeletionService {

    @Autowired
    private ITimeService timeService;

    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private AppVariantUsageRepository appVariantUsageRepository;
    @Autowired
    private AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;
    @Autowired
    private LegalTextAcceptanceRepository legalTextAcceptanceRepository;
    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;
    @Autowired
    private IPersistenceSupportService genericRepositoryService;

    @Override
    public DeleteOperation deleteAddress(Address address) {
        //it can happen that the address is stale, so we reload them to be sure we are working with a fresh entity
        Address freshAddress = genericRepositoryService.refreshProxy(address);
        if (freshAddress == null) {
            //the address is not existing, so it is actually deleted
            return DeleteOperation.DELETED;
        }
        freshAddress.setName(erasedNonNullString());
        freshAddress.setCity(erasedNonNullString());
        freshAddress.setStreet(erasedNonNullString());
        freshAddress.setZip("00000");
        freshAddress.setGpsLocation(new GPSLocation(0, 0));
        freshAddress.setDeleted(true);
        address.setDeletionTime(timeService.currentTimeMillisUTC());
        addressRepository.saveAndFlush(freshAddress);

        return DeleteOperation.ERASED;
    }

    @Override
    public Map<LegalTextAcceptance, DeleteOperation> deleteAllLegalTextAcceptances(Person person) {

        final List<LegalTextAcceptance> allAcceptances =
                legalTextAcceptanceRepository.findAllByPersonOrderByTimestampDesc(person);
        legalTextAcceptanceRepository.deleteAllByPerson(person);
        legalTextAcceptanceRepository.flush();
        return allAcceptances.stream()
                .collect(Collectors.toMap(Function.identity(), e -> DeleteOperation.DELETED));
    }

    @Override
    public DeleteOperation deleteAppVariantUsage(AppVariantUsage appVariantUsage) {

        appVariantUsageAreaSelectionRepository.deleteByAppVariantUsage(appVariantUsage);

        appVariantUsageRepository.delete(appVariantUsage);

        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deletePushCategoryUserSettings(Collection<PushCategoryUserSetting> pushCategoryUserSettings) {

        pushCategoryUserSettingRepository.deleteAll(pushCategoryUserSettings);

        return DeleteOperation.DELETED;
    }

}
