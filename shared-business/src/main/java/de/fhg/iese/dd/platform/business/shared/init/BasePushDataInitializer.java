/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;

public abstract class BasePushDataInitializer extends BaseDataInitializer {

    public static final String TOPIC = "push";

    @Autowired
    private AppRepository appRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private PushCategoryRepository pushCategoryRepository;
    @Autowired
    private IExternalPushProvider externalPushProvider;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                .useCase(USE_CASE_NEW_APP_VARIANT)
                .requiredEntity(Tenant.class)
                .requiredEntity(App.class)
                .requiredEntity(AppVariant.class)
                .processedEntity(PushCategory.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        //create all push apps for all app variants
        createPushApps(logSummary);

        //create all push categories for all apps
        createPushCategories(logSummary);
    }

    private void createPushApps(LogSummary logSummary) {
        logSummary.info("Creating and configuring push apps (if not existing)");

        Map<String, List<AppVariantPushConfiguration>> appVariantIdToAppVariantPushConfig =
                loadEntitiesFromDataJson(null, AppVariantPushConfiguration.class);

        if (CollectionUtils.isEmpty(appVariantIdToAppVariantPushConfig)) {
            logSummary.warn("No push configuration defined for all app variants in '{}', check if " +
                    "AppVariantPushConfiguration.json is provided", getModuleName());
            return;
        }

        for (AppVariant appVariant : appVariantRepository.findAllByOrderByAppVariantIdentifier()) {
            List<AppVariantPushConfiguration> appVariantPushConfigs = appVariantIdToAppVariantPushConfig
                    .get(appVariant.getAppVariantIdentifier());
            if (!CollectionUtils.isEmpty(appVariantPushConfigs)) {
                logSummary.info("Configuring push for {}", appVariant);
                logSummary.indent();
                try {
                    if (appVariantPushConfigs.size() > 1) {
                        logSummary.warn("Multiple push configurations defined for {}, check " +
                                "AppVariantPushConfiguration.json in {}", appVariant, getModuleName());
                    }
                    AppVariantPushConfiguration appVariantPushConfig = appVariantPushConfigs.get(0);

                    byte[] apnsCertificate =
                            loadConfigurationFileContent(appVariantPushConfig.getApnsCertificatePKCS12FileName());
                    byte[] apnsSandboxCertificate = loadConfigurationFileContent(
                            appVariantPushConfig.getApnsSandboxCertificatePKCS12FileName());

                    IExternalPushProvider.ExternalPushAppConfiguration pushAppConfig =
                            IExternalPushProvider.ExternalPushAppConfiguration.builder()
                                    .fcmApiKey(appVariantPushConfig.getFcmApiKey())
                                    .apnsCertificate(apnsCertificate)
                                    .apnsCertificatePassword(appVariantPushConfig.getApnsCertificatePassword())
                                    .apnsSandboxCertificate(apnsSandboxCertificate)
                                    .apnsSandboxCertificatePassword(
                                            appVariantPushConfig.getApnsSandboxCertificatePassword())
                                    .build();
                    if (appVariant.isSupportsAndroid() && StringUtils.isEmpty(pushAppConfig.getFcmApiKey())) {
                        logSummary.warn("{} should support Android but does not define push config for FCM",
                                appVariant);
                    }
                    if (appVariant.isSupportsIos() &&
                            (ArrayUtils.isEmpty(pushAppConfig.getApnsCertificate()) ||
                                    ArrayUtils.isEmpty(pushAppConfig.getApnsSandboxCertificate()))) {
                        logSummary.warn("{} should support IOS but does not define push config for either " +
                                "APNS or APNS-Sandbox", appVariant);
                    }
                    logSummary.info("Creating/Updating push app with configuration {} ", pushAppConfig.toLogInfo());
                    IExternalPushProvider.ExternalPushApp updatedPushApp =
                            externalPushProvider.createOrUpdatePushApp(appVariant, pushAppConfig);
                    updatePushAppIdIfChanged(updatedPushApp.getPushAppId(), appVariant, logSummary);
                } finally {
                    logSummary.outdent();
                }
            }
        }
    }

    private void updatePushAppIdIfChanged(String pushAppId, AppVariant appVariant, LogSummary logSummary) {
        Objects.requireNonNull(pushAppId);

        if (!StringUtils.equals(pushAppId, appVariant.getPushAppId())) {
            //pushAppId has changed
            if (StringUtils.isNotEmpty(appVariant.getPushAppId())) {
                //push app id is present, but different from the id that is used in the external push provider
                logSummary.warn("Inconsistency detected: pushAppId {} in database and pushAppId {} in external " +
                                "push provider differ. Will overwrite pushAppId in database with external push id.",
                        appVariant.getPushAppId(), pushAppId);
            }
            appVariant.setPushAppId(pushAppId);
            appVariantRepository.saveAndFlush(appVariant);
            logSummary.info("Updated to new pushAppId {} in database", pushAppId);
        }
    }

    private void createPushCategories(LogSummary logSummary) {
        Map<String, List<PushCategory>> pushCategoriesPerApp = loadEntitiesFromDataJson(null, PushCategory.class);

        if (pushCategoriesPerApp == null || pushCategoriesPerApp.isEmpty()) {
            logSummary.warn("No push categories defined for all apps in '{}', check if PushCategory.json is provided",
                    getModuleName());
            return;
        }

        Collection<App> apps = appRepository.findAllByOrderByAppIdentifier();

        for (App app : apps) {
            //use the app identifier as key
            List<PushCategory> pushCategoriesToCreate = pushCategoriesPerApp.get(app.getAppIdentifier());
            if (pushCategoriesToCreate != null && !pushCategoriesToCreate.isEmpty()) {
                List<PushCategory> createdPushCategoriesForApp = new ArrayList<>(pushCategoriesToCreate.size());
                for (int i = 0; i < pushCategoriesToCreate.size(); i++) {
                    PushCategory pushCategory = pushCategoriesToCreate.get(i);
                    //we do not want to have a bare uuid stored in the data jsons, but the qualifier of an actual push category id
                    String pushCategoryIdReference = pushCategory.getId();
                    PushCategoryId pushCategoryId =
                            findConstantByFullyQualifiedName(pushCategoryIdReference, PushCategoryId.class);
                    pushCategory = pushCategory
                            .withId(pushCategoryId)
                            //we want the same order value as they appear in the json file
                            .withOrderValue(i + 1);
                    pushCategory.setApp(app);
                    //ensure to not have trailing or leading whitespace for the subject
                    pushCategory.setSubject(StringUtils.trim(pushCategory.getSubject()));
                    if (StringUtils.isEmpty(pushCategory.getSubject())) {
                        //ensure that there is null instead of empty strings
                        pushCategory.setSubject(null);
                    }
                    if (pushCategory.getParentCategory() != null &&
                            StringUtils.isNotEmpty(pushCategory.getParentCategory().getId())) {
                        String parentCategoryIdReference = pushCategory.getParentCategory().getId();
                        PushCategoryId parentCategoryId =
                                findConstantByFullyQualifiedName(parentCategoryIdReference, PushCategoryId.class);
                        PushCategory parentCategory = pushCategoryRepository.findById(parentCategoryId.getId())
                                .orElseThrow(() -> new DataInitializationException(
                                        "Push category with id '{}' references parent '{}' that can not be found.",
                                        pushCategoryIdReference, parentCategoryIdReference));
                        if (parentCategory.getParentCategory() != null) {
                            throw new DataInitializationException(
                                    "Push category with id '{}' references parent '{}' that is already a child. " +
                                            "Only hierarchies with one level are supported.",
                                    pushCategoryIdReference, parentCategoryIdReference);
                        }
                        pushCategory.setParentCategory(parentCategory);
                    }
                    pushCategory = pushCategoryRepository.saveAndFlush(pushCategory);
                    logSummary.info("created {}", pushCategory.toString());

                    createdPushCategoriesForApp.add(pushCategory);
                }

                Set<PushCategory> orphanedPushCategoriesForApp = pushCategoryRepository.findAllByApp(app);
                //if an existing push category was not updated it was removed from the data json and needs to be manually deleted from the db
                createdPushCategoriesForApp.forEach(orphanedPushCategoriesForApp::remove);

                orphanedPushCategoriesForApp
                        .forEach(o -> logSummary.warn("Orphaned push category {} for {}, " +
                                "consider deleting it with admin services", o, app));
            }
        }
    }

}
