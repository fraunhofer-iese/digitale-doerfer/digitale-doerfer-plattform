/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events.processing;

import de.fhg.iese.dd.platform.business.framework.events.*;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingTimeoutException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Component that is capable of processing events. It needs to have the annotation @{@link EventProcessor} present.
 * <p>
 * Use the annotation @{@link EventProcessing} on the methods that should actually process events. There are four
 * different signatures possible:
 *
 * <li>void myFunction(? extends {@link BaseEvent})
 * <br/> This function just consumes events.
 * </li>
 *
 * <li>? extends {@link BaseEvent} myFunction(? extends {@link BaseEvent})
 * <br/> This function consumes an event and produces one. The produced event is sent on the event bus afterwards. It can also return null.
 * </li>
 *
 * <li>List<? extends {@link BaseEvent}> myFunction(? extends {@link BaseEvent})
 * <br/> This function consumes an event and produces multiple ones. The produced events are sent on the event bus
 * afterwards. It can also return null or a list containing null.
 * </li>
 *
 * <li>void myFunction(? extends {@link BaseEvent}, {@link EventProcessingContext})
 * <br/> This function can use {@link #notify(BaseEvent, EventProcessingContext)} internally to send events on the event
 * bus asynchronously and still continue processing the event. This flexibility comes with the disadvantage of slightly
 * worse performance. If one of the above alternatives is used, the sending of events can be done synchronous, which can
 * increase performance.
 * </li>
 * <p/>
 * Use {@link EventExecutionStrategy} to control how the methods are called when the according events are present on the
 * event bus.
 */
public abstract class BaseEventProcessor {

    protected final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    protected ITimeService timeService;
    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize() {
        eventBusAccessor.setOwner(this);
        registerForEventsManually();
        registerForEventsViaAnnotation();
    }

    private void registerForEventsViaAnnotation() {

        //the annotation on the event processor itself is used to check for overrides of the execution strategy
        EventProcessor eventProcessorAnnotation =
                AnnotatedElementUtils.findMergedAnnotation(this.getClass(), EventProcessor.class);
        if (eventProcessorAnnotation == null) {
            throw new IllegalArgumentException(
                    "Annotation " + EventProcessor.class.getName() + " needs to be present on all subtypes of  '" +
                            BaseEventProcessor.class.getName() + "': " +
                            this.getClass());
        }
        Method[] declaredMethods = ReflectionUtils.getUniqueDeclaredMethods(this.getClass());
        for (Method method : declaredMethods) {
            EventProcessing eventProcessingAnnotation = method.getAnnotation(EventProcessing.class);
            if (eventProcessingAnnotation == null) {
                //not all methods in an event processor are event processing methods
                continue;
            }
            Parameter[] methodParameters = method.getParameters();
            if (methodParameters.length == 0 || methodParameters.length > 2) {
                throw handleInvalidMethodAnnotation(method,
                        "Number of parameters wrong: 2 for EventConsumingFunction, 1 for EventProcessingFunction");
            }
            if (!BaseEvent.class.isAssignableFrom(methodParameters[0].getType())) {
                throw handleInvalidMethodAnnotation(method, "First parameter not subtype of BaseEvent");
            }
            //implicit event type is defined by the first parameter of the method
            @SuppressWarnings("unchecked")
            Class<? extends BaseEvent> implicitEventTypeToRegister =
                    (Class<? extends BaseEvent>) methodParameters[0].getType();
            //explicit event types are the ones defined in the annotation
            //it is checked by getEventTypesToRegisterFromAnnotation if they fit with the method signature
            Collection<Class<? extends BaseEvent>> explicitEventTypesToRegister =
                    getEventTypesToRegisterFromAnnotation(method, eventProcessingAnnotation,
                            implicitEventTypeToRegister);
            Pair<EventExecutionStrategy, EventExecutionExceptionStrategy> strategies =
                    getStrategies(method, eventProcessorAnnotation, eventProcessingAnnotation);
            EventExecutionStrategy eventExecutionStrategy = strategies.getLeft();
            EventExecutionExceptionStrategy eventExecutionExceptionStrategy = strategies.getRight();
            if (methodParameters.length == 2) {
                //event consuming function that sends events via notify
                if (!EventProcessingContext.class.equals(methodParameters[1].getType())) {
                    throw handleInvalidMethodAnnotation(method,
                            "EventConsumingFunction: Second parameter must be EventProcessingContext");
                }
                if (!void.class.equals(method.getReturnType())) {
                    throw handleInvalidMethodAnnotation(method, "EventConsumingFunction: Return type must be void");
                }
                ReflectionUtils.makeAccessible(method);
                if (explicitEventTypesToRegister == null) {
                    registerEventConsumingFunction(implicitEventTypeToRegister,
                            (e, c) -> ReflectionUtils.invokeMethod(method, this, e, c),
                            eventExecutionStrategy,
                            eventExecutionExceptionStrategy,
                            eventProcessingAnnotation.includeSubtypesOfEvent());
                } else {
                    registerEventConsumingFunctionForAll(explicitEventTypesToRegister,
                            (e, c) -> ReflectionUtils.invokeMethod(method, this, e, c),
                            eventExecutionStrategy,
                            eventExecutionExceptionStrategy,
                            eventProcessingAnnotation.includeSubtypesOfEvent());
                }
            } else {
                //event processing function that returns either ...
                // a list of events
                // a single event
                // no event
                // ... to send. The actual sending of events is done by the event bus accessor
                Class<?> returnType = method.getReturnType();
                if (List.class.isAssignableFrom(returnType)) {
                    //a list will be returned
                    //first check if the right type of list is returned
                    Type genericListReturnType = method.getGenericReturnType();
                    if (!(genericListReturnType instanceof ParameterizedType listReturnType)) {
                        throw handleInvalidMethodAnnotation(method,
                                "EventProcessingFunction: Return type is of type List, but not List<BaseEvent>");
                    }
                    // that means <? extends BaseEvent>
                    WildcardType expectedWildcard = TypeUtils.wildcardType().withUpperBounds(BaseEvent.class).build();
                    ParameterizedType expectedType = TypeUtils.parameterize(List.class, expectedWildcard);
                    if (!TypeUtils.isAssignable(listReturnType, expectedType)) {
                        throw handleInvalidMethodAnnotation(method,
                                "EventProcessingFunction: Return type is not List<? extends BaseEvent>");
                    }
                    ReflectionUtils.makeAccessible(method);

                    if (explicitEventTypesToRegister == null) {
                        registerEventProcessingFunction(implicitEventTypeToRegister, (e) -> {
                                    @SuppressWarnings("unchecked")
                                    List<BaseEvent> result =
                                            (List<BaseEvent>) ReflectionUtils.invokeMethod(method, this, e);
                                    return result;
                                },
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    } else {
                        registerEventProcessingFunctionForAll(explicitEventTypesToRegister, (e) -> {
                                    @SuppressWarnings("unchecked")
                                    List<BaseEvent> result =
                                            (List<BaseEvent>) ReflectionUtils.invokeMethod(method, this, e);
                                    return result;
                                },
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    }
                } else if (BaseEvent.class.isAssignableFrom(returnType)) {
                    //a single event will be returned
                    ReflectionUtils.makeAccessible(method);

                    if (explicitEventTypesToRegister == null) {
                        registerEventProcessingFunction(implicitEventTypeToRegister,
                                (e) -> Collections.singletonList(
                                        (BaseEvent) ReflectionUtils.invokeMethod(method, this, e)),
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    } else {
                        registerEventProcessingFunctionForAll(explicitEventTypesToRegister,
                                (e) -> Collections.singletonList(
                                        (BaseEvent) ReflectionUtils.invokeMethod(method, this, e)),
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    }
                } else if (void.class.equals(returnType)) {
                    //no event will be returned
                    ReflectionUtils.makeAccessible(method);

                    if (explicitEventTypesToRegister == null) {
                        registerEventProcessingFunction(implicitEventTypeToRegister, (e) -> {
                                    ReflectionUtils.invokeMethod(method, this, e);
                                    return Collections.emptyList();
                                },
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    } else {
                        registerEventProcessingFunctionForAll(explicitEventTypesToRegister, (e) -> {
                                    ReflectionUtils.invokeMethod(method, this, e);
                                    return Collections.emptyList();
                                },
                                eventExecutionStrategy,
                                eventExecutionExceptionStrategy,
                                eventProcessingAnnotation.includeSubtypesOfEvent());
                    }
                } else {
                    throw handleInvalidMethodAnnotation(method, "EventProcessingFunction: Return type wrong");
                }
            }
        }
    }

    private Collection<Class<? extends BaseEvent>> getEventTypesToRegisterFromAnnotation(Method method,
            EventProcessing eventProcessingAnnotation, Class<? extends BaseEvent> consumedEventType) {

        if (eventProcessingAnnotation.relevantEvents().length == 0) {
            return null;
        }
        //there is an explicit list of relevant events
        List<Class<? extends BaseEvent>> relevantEvents = Arrays.asList(eventProcessingAnnotation.relevantEvents());
        if (!relevantEvents.stream().allMatch(consumedEventType::isAssignableFrom)) {
            throw handleInvalidMethodAnnotation(method,
                    "Not all @EventProcessing.relevantEvents are subtypes of first parameter");
        }
        return relevantEvents;
    }

    private Pair<EventExecutionStrategy, EventExecutionExceptionStrategy> getStrategies(
            Method method,
            EventProcessor eventProcessorAnnotation,
            EventProcessing eventProcessingAnnotation) {

        EventExecutionStrategy processorExecutionStrategy = eventProcessorAnnotation.executionStrategy();
        EventExecutionStrategy methodExecutionStrategy = eventProcessingAnnotation.executionStrategy();
        final EventExecutionStrategy executionStrategy;
        if (processorExecutionStrategy == EventExecutionStrategy.AUTO) {
            //no specific strategy on the event processor, we can just use the one on the method
            if (methodExecutionStrategy == EventExecutionStrategy.AUTO) {
                executionStrategy = EventExecutionStrategy.SYNCHRONOUS_PREFERRED;
            } else {
                executionStrategy = methodExecutionStrategy;
            }
        } else {
            //strategy on the event processor, check if it is overridden by the method
            if (methodExecutionStrategy == EventExecutionStrategy.AUTO) {
                //the method does not define any strategy, use the one on the processor, which can't be AUTO
                executionStrategy = processorExecutionStrategy;
            } else {
                //the method strategy overrides the one on the processor
                executionStrategy = methodExecutionStrategy;
            }
        }
        EventExecutionExceptionStrategy processorExceptionStrategy = eventProcessorAnnotation.exceptionStrategy();
        EventExecutionExceptionStrategy methodExceptionStrategy = eventProcessingAnnotation.exceptionStrategy();
        final EventExecutionExceptionStrategy definedExceptionStrategy;
        if (processorExceptionStrategy == EventExecutionExceptionStrategy.AUTO) {
            //no specific strategy on the event processor, we can just use the one on the method
            definedExceptionStrategy = methodExceptionStrategy;
        } else {
            //strategy on the event processor, check if it is overridden by the method
            if (methodExceptionStrategy == EventExecutionExceptionStrategy.AUTO) {
                //the method does not define any strategy, use the one on the processor, which can't be AUTO
                definedExceptionStrategy = processorExceptionStrategy;
            } else {
                //the method strategy overrides the one on the processor
                definedExceptionStrategy = methodExceptionStrategy;
            }
        }
        if (executionStrategy == EventExecutionStrategy.ASYNCHRONOUS_REQUIRED ||
                executionStrategy == EventExecutionStrategy.ASYNCHRONOUS_PREFERRED) {
            if (definedExceptionStrategy == EventExecutionExceptionStrategy.AUTO) {
                return Pair.of(executionStrategy, EventExecutionExceptionStrategy.IGNORE_EXCEPTION);
            } else {
                if (definedExceptionStrategy == EventExecutionExceptionStrategy.PROPAGATE_EXCEPTION) {
                    throw handleMisconfiguredMethodAnnotation(method,
                            "Execution exception strategy PROPAGATE_EXCEPTION is not possible in combination " +
                                    "with execution strategy ASYNCHRONOUS_REQUIRED or ASYNCHRONOUS_PREFERRED");
                }
                return Pair.of(executionStrategy, EventExecutionExceptionStrategy.IGNORE_EXCEPTION);
            }
        }
        if (executionStrategy == EventExecutionStrategy.SYNCHRONOUS_PREFERRED) {
            if (definedExceptionStrategy == EventExecutionExceptionStrategy.AUTO) {
                return Pair.of(executionStrategy, EventExecutionExceptionStrategy.PROPAGATE_EXCEPTION);
            } else {
                //in the synchronous case all exception strategies are allowed
                return Pair.of(executionStrategy, definedExceptionStrategy);
            }
        }
        throw handleMisconfiguredMethodAnnotation(method, "Execution and exception strategy could not be determined");
    }

    private IllegalArgumentException handleInvalidMethodAnnotation(Method method, String message) {
        String methodName = method.getDeclaringClass().getName() + "::" + method.getName();
        return new IllegalArgumentException(
                "Annotation " + EventProcessing.class.getName() + " is set on invalid method '" + methodName + "': " +
                        message);
    }

    private IllegalArgumentException handleMisconfiguredMethodAnnotation(Method method, String message) {
        String methodName = method.getDeclaringClass().getName() + "::" + method.getName();
        return new IllegalArgumentException(
                "Annotation " + EventProcessing.class.getName() + " is not configured correctly on method '" +
                        methodName + "': " +
                        message);
    }

    /**
     * Override this method to manually register for events. Should not be used, @{@link EventProcessing} is powerful
     * enough for most registrations.
     */
    protected void registerForEventsManually() {
    }

    /**
     * Register this function to be executed when any event of that type occurs.
     *
     * @param eventClass
     *         type of events we want to receive
     * @param consumer
     *         function to be called when an event of that type comes in
     * @param includeSubtypes
     *         if true the function is triggered also by subtypes of the event class
     * @param <E>
     *         the type of event
     */
    protected <E extends BaseEvent> void registerEventProcessingFunction(Class<E> eventClass,
            EventProcessingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy, boolean includeSubtypes) {
        if (includeSubtypes) {
            eventBusAccessor.registerEventProcessingFunctionIncludingSubtypes(eventClass, consumer, executionStrategy,
                    exceptionStrategy);
        } else {
            eventBusAccessor.registerEventProcessingFunction(eventClass, consumer, executionStrategy,
                    exceptionStrategy);
        }
    }

    /**
     * Register this function to be executed when any event of that type occurs.
     *
     * @param eventClass
     *         type of events we want to receive
     * @param consumer
     *         function to be called when an event of that type comes in
     * @param includeSubtypes
     *         if true the function is triggered also by subtypes of the event class
     * @param <E>
     *         the type of event
     */
    private <E extends BaseEvent> void registerEventConsumingFunction(Class<E> eventClass,
            EventConsumingFunction<E> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy, boolean includeSubtypes) {
        if (includeSubtypes) {
            eventBusAccessor.registerEventConsumingFunctionIncludingSubtypes(eventClass, consumer, executionStrategy,
                    exceptionStrategy);
        } else {
            eventBusAccessor.registerEventConsumingFunction(eventClass, consumer, executionStrategy, exceptionStrategy);
        }
    }

    /**
     * Register this function to be executed when any event of that collection of types occurs.
     *
     * @param eventClasses types of events we want to receive
     * @param consumer     function to be called when an event of that type comes in
     */
    private void registerEventProcessingFunctionForAll(Collection<Class<? extends BaseEvent>> eventClasses,
            EventProcessingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy, boolean includeSubtypes) {
        if (includeSubtypes) {
            eventBusAccessor.registerEventProcessingFunctionIncludingSubtypes(eventClasses, consumer,
                    executionStrategy, exceptionStrategy);
        } else {
            eventBusAccessor.registerEventProcessingFunction(eventClasses, consumer, executionStrategy,
                    exceptionStrategy);
        }
    }

    /**
     * Register this function to be executed when any event of that collection of types occurs.
     *
     * @param eventClasses types of events we want to receive
     * @param consumer     function to be called when an event of that type comes in
     */
    private void registerEventConsumingFunctionForAll(Collection<Class<? extends BaseEvent>> eventClasses,
            EventConsumingFunction<BaseEvent> consumer, EventExecutionStrategy executionStrategy,
            EventExecutionExceptionStrategy exceptionStrategy, boolean includeSubtypes) {
        if (includeSubtypes) {
            eventBusAccessor.registerEventConsumingFunctionIncludingSubtypes(eventClasses, consumer, executionStrategy,
                    exceptionStrategy);
        } else {
            eventBusAccessor.registerEventConsumingFunction(eventClasses, consumer, executionStrategy,
                    exceptionStrategy);
        }
    }

    /**
     * Fire this event and do not handle the exceptions caused by this event. They will be forwarded to the one that
     * triggered us, meaning going backwards in the event chain.
     *
     * @param reply
     *         the reply we want to send out
     * @param context
     *         required to track the event chain
     */
    protected <E extends BaseEvent> void notify(E reply, final EventProcessingContext context) {
        eventBusAccessor.notifyWithoutHandlingExceptions(reply, context);
    }

    /**
     * Fire this event and register this compensate function to be executed when an exception occurs in the upcoming
     * event chain.
     * <h2>
     * It is only possible to register <i>the same</i> compensate function for the combination
     * <code>consumedEvent.getClass()</code> and
     * <code>reply.getClass()</code>
     * </h2> <br/>
     * <strong>Registering two different functions will result in overwriting the first
     * one and thus in unwanted behavior</strong> <br/><br/>
     *
     * @param consumedEvent
     *         the event we just got
     * @param reply
     *         the reply we want to send out
     * @param compensateFunction
     *         a compensation function if something goes wrong with our reply
     * @param context
     *         required to track the event chain
     */
    protected <C extends BaseEvent, S extends BaseEvent> void notify(C consumedEvent, S reply,
            CompensateEventFunction<C, S> compensateFunction, final EventProcessingContext context) {
        eventBusAccessor.notifyAndRegisterCompensate(consumedEvent, reply, compensateFunction, context);
    }

    /**
     * Publishes the given event and waits for any reply of the specified types to occur.
     *
     * @param eventToNotify
     *         The event that is published and should trigger one of the eventsToWaitFor
     * @param eventsToWaitFor
     *         The types of event that should occur
     *
     * @return The first event of the specific type that was waited for
     *
     * @deprecated Marked as deprecated as it should be used with great care. Usually, event processing is asynchronous.
     */
    @Deprecated
    @SafeVarargs
    @SuppressWarnings("varargs")
    final protected BaseEvent notifyAndWaitForAnyReply(BaseEvent eventToNotify,
            Class<? extends BaseEvent>... eventsToWaitFor) throws EventProcessingException {
        return eventBusAccessor.notifyTriggerAndWaitForAnyReply(Collections.singletonList(eventToNotify),
                Arrays.asList(eventsToWaitFor));
    }

    /**
     * Publishes the given events and waits for a reply of a specific type to occur.
     *
     * @param eventToWaitFor The type of event that should occur
     * @param eventsToNotify The events that are published and should trigger the eventToWaitFor
     * @param <E>            type of the event
     *
     * @return The event that was waited for
     *
     * @throws EventProcessingTimeoutException if a timeout occurred while waiting for the reply
     * @throws EventProcessingException        if any involved event processor threw an exception
     * @deprecated Marked as deprecated as it should be used with great care. Usually, event processing is asynchronous.
     */
    @Deprecated
    @SafeVarargs
    @SuppressWarnings("varargs")
    final protected <E extends BaseEvent> E notifyAndWaitForReply(Class<E> eventToWaitFor, BaseEvent... eventsToNotify)
            throws EventProcessingTimeoutException, EventProcessingException {
        @SuppressWarnings("unchecked")
        E reply = (E) eventBusAccessor.notifyTriggerAndWaitForAnyReply(Arrays.asList(eventsToNotify),
                Collections.singletonList(eventToWaitFor));
        return reply;
    }

}
