/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.events;

import java.time.Duration;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.caching.ExpiringResourceReference;

@Component
class EventReflections {

    private final ExpiringResourceReference<Reflections> reflectionsReference;

    public EventReflections(TaskScheduler taskScheduler) {
        reflectionsReference =
                new ExpiringResourceReference<>(this::createReflections, null, Duration.ofSeconds(15), taskScheduler);
    }

    /**
     * Find all subtypes of the events and the types of the events itself by using {@link Reflections}.
     *
     * @param eventSuperClasses types of event to check
     *
     * @return all subtypes of the events and the type of the events itself.
     */
    public Set<Class<? extends BaseEvent>> allSubtypes(
            Collection<Class<? extends BaseEvent>> eventSuperClasses) {

        return eventSuperClasses.stream()
                .map(eventSuperClass -> reflectionsReference.get().getSubTypesOf(eventSuperClass))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    public <E extends BaseEvent> Set<Class<? extends E>> allSubtypes(Class<E> eventSuperClass) {

        return reflectionsReference.get().getSubTypesOf(eventSuperClass);
    }

    /**
     * Initialize {@link Reflections} for looking up event type hierarchies.
     *
     * @return new instance that can check event hierarchies (and nothing else)
     */
    private Reflections createReflections() {
        return new Reflections(new ConfigurationBuilder()
                .forPackage("de.fhg.iese.dd.platform.business")
                .setScanners(Scanners.SubTypes));
    }

}
