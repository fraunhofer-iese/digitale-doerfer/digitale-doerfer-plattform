/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.referencedata.change;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ConcurrentRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.repos.ReferenceDataChangeLogEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DataInitConfig;

@Service
class ReferenceDataChangeService extends BaseService implements IReferenceDataChangeService {

    private static final int MAX_LENGTH_CHANGE_COMMAND = 250;
    private static final int MAX_LENGTH_VERSION_INFO = 1015;

    @Autowired
    private DataInitConfig dataInitConfig;
    @Autowired
    private IEnvironmentService environmentService;
    @Autowired
    private ReferenceDataChangeLogEntryRepository referenceDataChangeLogEntryRepository;

    @Override
    public ReferenceDataChangeLogEntry acquireReferenceDataChangeLock(@Nullable Person initiator,
            String changeCommand, boolean coolDownRequired) {

        final ReferenceDataChangeLogEntry lastDataChange =
                referenceDataChangeLogEntryRepository.findFirstByOrderByStartTimeDesc();
        if (lastDataChange != null) {
            if (lastDataChange.getEndTime() == null) {
                //not finished because end of the most recent one is null
                throw new ConcurrentRequestException(
                        "Reference data change started at {} still in progress by {} ({}), no parallelism possible. (log entry found)",
                        timeService.toLocalTimeHumanReadable(lastDataChange.getStartTime()),
                        lastDataChange.getInitiatorName(),
                        lastDataChange.getInitiatorId());
            }
            //we need to wait for some cool down time, so that the risk of changing the data while caches are refreshing is reduced
            final long earliestPossibleNextDataChange =
                    lastDataChange.getEndTime() + dataInitConfig.getCoolDownTime().toMillis();
            if (coolDownRequired && earliestPossibleNextDataChange > timeService.currentTimeMillisUTC()) {
                throw new ConcurrentRequestException(
                        "Reference data change started at {} did not cool down for at least {}s, next change possible at {} (log entry found)",
                        timeService.toLocalTimeHumanReadable(lastDataChange.getStartTime()),
                        dataInitConfig.getCoolDownTime().getSeconds(),
                        timeService.toLocalTimeHumanReadable(earliestPossibleNextDataChange),
                        lastDataChange.getInitiatorName(),
                        lastDataChange.getInitiatorId());
            }
        }
        try {
            String trimmedChangeCommand = changeCommand;
            String versionInfo = null;
            if (StringUtils.length(changeCommand) > MAX_LENGTH_CHANGE_COMMAND) {
                //in case the change command needs to be trimmed we put it into the version info, too
                versionInfo = StringUtils.abbreviate(changeCommand, "...", MAX_LENGTH_VERSION_INFO);
                trimmedChangeCommand = StringUtils.abbreviate(changeCommand, "...", MAX_LENGTH_CHANGE_COMMAND);
            }

            final ReferenceDataChangeLogEntry newDataChange =
                    referenceDataChangeLogEntryRepository.saveAndFlush(ReferenceDataChangeLogEntry.builder()
                            .instanceIdentifier(environmentService.getInstanceIdentifier())
                            .initiatorId(initiator != null ? initiator.getId() : "")
                            .initiatorName(initiator != null ? initiator.getFullName() : "unknown")
                            .command(trimmedChangeCommand)
                            .versionInfo(versionInfo)
                            .startTime(timeService.currentTimeMillisUTC())
                            .successful(null)
                            .build());
            final ReferenceDataChangeLogEntry newDataInitRunDB =
                    referenceDataChangeLogEntryRepository.findFirstByOrderByStartTimeDesc();
            if (!newDataChange.equals(newDataInitRunDB)) {
                //there was a parallel data init run in between
                // we delete the one we tried to start
                referenceDataChangeLogEntryRepository.delete(newDataChange);
                throw new ConcurrentRequestException(
                        "Reference data change started at {} still in progress by {} ({}), no parallelism possible. (parallel log entry created)",
                        timeService.toLocalTimeHumanReadable(newDataInitRunDB.getStartTime()),
                        newDataInitRunDB.getInitiatorName(),
                        newDataInitRunDB.getInitiatorId());
            }
            log.info("Data change log acquired by {} ({}) for command {}",
                    newDataChange.getInitiatorName(),
                    newDataChange.getInitiatorId(),
                    newDataChange.getCommand());
            return newDataChange;
        } catch (DataIntegrityViolationException e) {
            //this happens if the new log entry has the same timestamp as one that that is in the db
            final ReferenceDataChangeLogEntry runningDataChange =
                    referenceDataChangeLogEntryRepository.findFirstByOrderByStartTimeDesc();
            throw new ConcurrentRequestException(
                    "Reference data change started at {} still in progress by {} ({}), no parallelism possible. (same time log entry creation failed)",
                    timeService.toLocalTimeHumanReadable(runningDataChange.getStartTime()),
                    runningDataChange.getInitiatorName(),
                    runningDataChange.getInitiatorId());
        }
    }

    @Override
    public ReferenceDataChangeLogEntry setDataChangeVersionInfo(ReferenceDataChangeLogEntry referenceDataChangeLogEntry,
            String versionInfo) {

        if (referenceDataChangeLogEntry.getEndTime() != null) {
            throw new IllegalStateException("The change log entry is already finished");
        }
        final String existingVersionInfo = referenceDataChangeLogEntry.getVersionInfo();
        if (StringUtils.isNotEmpty(existingVersionInfo)) {
            referenceDataChangeLogEntry.setVersionInfo(existingVersionInfo + "\n" + versionInfo);
        } else {
            referenceDataChangeLogEntry.setVersionInfo(versionInfo);
        }
        if (StringUtils.length(referenceDataChangeLogEntry.getVersionInfo()) > MAX_LENGTH_VERSION_INFO) {
            referenceDataChangeLogEntry.setVersionInfo(
                    StringUtils.abbreviate(referenceDataChangeLogEntry.getVersionInfo(), "...",
                            MAX_LENGTH_VERSION_INFO));
        }
        return referenceDataChangeLogEntryRepository.saveAndFlush(referenceDataChangeLogEntry);
    }

    @Override
    public void releaseReferenceDataChangeLock(ReferenceDataChangeLogEntry referenceDataChangeLogEntry,
            boolean successful) {

        if (referenceDataChangeLogEntry.getEndTime() != null) {
            throw new IllegalStateException("The change log entry is already finished");
        }
        referenceDataChangeLogEntry.setEndTime(timeService.currentTimeMillisUTC());
        referenceDataChangeLogEntry.setSuccessful(successful);
        referenceDataChangeLogEntryRepository.saveAndFlush(referenceDataChangeLogEntry);
        log.info("Data change log released by {} ({}) for command {} with result {}",
                referenceDataChangeLogEntry.getInitiatorName(),
                referenceDataChangeLogEntry.getInitiatorId(),
                referenceDataChangeLogEntry.getCommand(),
                successful ? "successful" : "fail");
    }

    @Override
    public long getLastChangeTime() {

        return Objects.requireNonNullElse(referenceDataChangeLogEntryRepository.getLastEnd(), 0L);
    }

}
