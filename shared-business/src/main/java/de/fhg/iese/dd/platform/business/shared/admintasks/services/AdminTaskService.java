/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2022 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks.services;

import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.framework.caching.ExpiringResourceReference;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Service
class AdminTaskService extends BaseService implements IAdminTaskService {

    @Autowired
    private IPluginService pluginService;
    @Autowired
    private TaskScheduler taskScheduler;

    private boolean taskRunning = false;
    private ExpiringResourceReference<ForkJoinPool> parallelExecutorReference;

    @PostConstruct
    private void initialize() {

        parallelExecutorReference =
                new ExpiringResourceReference<>(null, this::shutDownExecutor, Duration.ofMinutes(1), taskScheduler);
    }

    @Override
    public String getAllAdminTaskIdsAndDescriptions() {
        final List<BaseAdminTask> baseAdminTasks =
                pluginService.getPluginInstancesRaw(PluginTarget.of(BaseAdminTask.class));
        if (!CollectionUtils.isEmpty(baseAdminTasks)) {
            return baseAdminTasks.stream()
                    .sorted(Comparator.comparing(BaseAdminTask::getName))
                    .map(t -> t.getId() + "\n- " + t.getDescription())
                    .collect(Collectors.joining("\n\n\n"));
        } else {
            return "No tasks defined.";
        }
    }

    ExecutorService getParallelExecutor(int parallelism) {
        final ForkJoinPool forkJoinPool = parallelExecutorReference.get();
        if (forkJoinPool == null) {
            return parallelExecutorReference.set(new ForkJoinPool(parallelism));
        } else {
            if (forkJoinPool.getParallelism() != parallelism) {
                if (forkJoinPool.isQuiescent()) {
                    forkJoinPool.shutdownNow();
                    return parallelExecutorReference.set(new ForkJoinPool(parallelism));
                } else {
                    throw new IllegalStateException(
                            "There are still other tasks running, can not change parallelism of pool");
                }
            }
        }
        return forkJoinPool;
    }

    @Override
    public String executeAdminTask(String adminTaskName, BaseAdminTask.AdminTaskParameters adminTaskParameters) {
        if (taskRunning) {
            throw new IllegalStateException("There are other tasks running, wait for their completion");
        }
        try {
            taskRunning = true;
            adminTaskName = StringUtils.trim(adminTaskName);
            adminTaskParameters.check();
            LogSummary logSummary = new LogSummary(log, true);

            String dryRunMsg = adminTaskParameters.isDryRun() ? "!Dry run! " : "";

            logSummary.info("{}starting admin task: {}", dryRunMsg, adminTaskName);

            BaseAdminTask baseAdminTask = findAdminTaskByName(adminTaskName);
            if(baseAdminTask == null){
                logSummary.warn("can not find admin task: {}", adminTaskName);
            }else{
                baseAdminTask.init(this);
                logSummary.setLogger(LogManager.getLogger(baseAdminTask.getClass()));
                try {
                    baseAdminTask.execute(logSummary, adminTaskParameters);
                } catch (Exception e) {
                    logSummary.error("failed to execute admin task {}: {}", e, adminTaskName, e);
                }
                logSummary.setLogger(log);
                logSummary.info("{}finished admin task: {}", dryRunMsg, adminTaskName);
            }
            return logSummary.toString();
        } finally {
            taskRunning = false;
        }
    }

    /**
     * Find an admin task by name
     *
     * @param adminTaskName
     *
     * @return the admin task with {@link BaseAdminTask#getName()} equals the given name or null
     */
    private BaseAdminTask findAdminTaskByName(String adminTaskName) {
        final List<BaseAdminTask> baseAdminTasks =
                pluginService.getPluginInstancesRaw(PluginTarget.of(BaseAdminTask.class));
        if (!CollectionUtils.isEmpty(baseAdminTasks)) {
            return baseAdminTasks.stream()
                    .filter(a -> StringUtils.equals(adminTaskName, a.getName()))
                    .findFirst()
                    .orElse(null);
        } else {
            return null;
        }
    }

    private void shutDownExecutor(ForkJoinPool forkJoinPool) {

        if (forkJoinPool != null) {
            forkJoinPool.shutdown();
            try {
                if (!forkJoinPool.awaitTermination(20, TimeUnit.MILLISECONDS)) {
                    log.debug("Thread pool could not be terminated");
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            forkJoinPool.shutdownNow();
        }
    }

    @PreDestroy
    private void shutdown() {
        parallelExecutorReference.expire();
    }

}
