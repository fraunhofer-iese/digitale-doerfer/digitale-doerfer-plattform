/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.msteams;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
class MSTeamsMessageRequest {

    private String title;
    private String text;
    private String themeColor;
    @JsonProperty("potentialAction")
    private List<LinkAction> linkActions;

    @Data
    @AllArgsConstructor
    static class LinkAction {

        /**
         * This is required by the MS Teams webhook
         */
        @JsonProperty("@type")
        private final String syntacticSugar = "ViewAction";

        private String name;
        @JsonIgnore
        private String link;

        public String[] getTarget() {
            return new String[]{link};
        }

    }

}
