/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.shared.waiting.events.PeriodicWaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.events.WaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.exceptions.WaitingExpiredEventInitializationException;
import de.fhg.iese.dd.platform.business.shared.waiting.exceptions.WaitingExpiredEventSerializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;

public interface IWaitingService {

    /**
     * Serializes the event to a {@link WaitingDeadline} using
     * {@link #toWaitingDeadline(WaitingExpiredEvent)} and saves it so that it can be
     * sent as a {@link WaitingExpiredEvent} using
     * {@link #toWaitingExpiredEvent(WaitingDeadline)} if the deadline is
     * expired.
     *
     * @param waitingExpiredEvent
     * @return
     */
    WaitingDeadline registerDeadline(WaitingExpiredEvent waitingExpiredEvent);

    /**
     * Serializes the event to a {@link WaitingDeadline} using
     * {@link #toWaitingDeadline(WaitingExpiredEvent)} and saves it so that it can be
     * sent as a {@link WaitingExpiredEvent} using
     * {@link #toWaitingExpiredEvent(WaitingDeadline)} if the deadline is
     * expired.
     * <p>
     * The stored deadline will get the deadlineId as id.
     *
     * @param waitingExpiredEvent
     * @param deadlineId
     * @return
     */
    WaitingDeadline registerDeadline(WaitingExpiredEvent waitingExpiredEvent, String deadlineId);

    /**
     * Serializes a {@link WaitingExpiredEvent} event into a {@link WaitingDeadline} that can be stored in
     * DB to wait for the expiration of the deadline.
     * <p>
     * All {@link BaseEntity}s as attributes of the event are stored in the WaitingDeadline by their
     * IDs. If the entity is not available anymore in DB if the deadline expires
     * these attributes are <code>null</code>.
     *
     * @param waitingExpiredEvent
     * @return
     * @throws WaitingExpiredEventSerializationException
     */
    WaitingDeadline toWaitingDeadline(WaitingExpiredEvent waitingExpiredEvent) throws WaitingExpiredEventSerializationException;

    /**
     * Initialize the attributes of the {@link WaitingExpiredEvent} with the
     * values and entities that are given in the {@link WaitingDeadline}. The
     * {@link BaseEntity}s are retrieved based on their id. If the entity is not
     * available anymore in DB these attributes are <code>null</code>.
     * <p>
     * The according repository for attributes containing BaseEntitys are found
     * by the type of the attribute.
     *
     * @param expiredDeadline
     * @return
     * @throws WaitingExpiredEventInitializationException
     */
    WaitingExpiredEvent toWaitingExpiredEvent(WaitingDeadline expiredDeadline) throws WaitingExpiredEventInitializationException;

    /**
     * Calculate the next deadline for the waiting event, if there is one.
     * <p>
     * For non-periodic event this will always be (false, currentDeadline).
     * <p>
     * For periodic events this is queried from the event using
     * {@link PeriodicWaitingExpiredEvent#nextDeadline(org.springframework.context.ApplicationContext)}
     *
     * @param waitingExpiredEvent
     * @return
     */
    WaitingExpiredEvent.WaitingDeadlineUpdate getDeadlineUpdate(WaitingExpiredEvent waitingExpiredEvent);

    /**
     * Handle the expiration of the deadline and either update the expired
     * deadline or delete it. This depends on the given deadlineUpdate
     *
     * @param expiredDeadline
     * @param deadlineUpdate
     */
    void handleDeadlineExpired(WaitingDeadline expiredDeadline, WaitingExpiredEvent.WaitingDeadlineUpdate deadlineUpdate);

    /**
     * Find all expired deadlines relative to now in UTC
     *
     * @return
     */
    List<WaitingDeadline> findAllExpiredDeadlines();

}
