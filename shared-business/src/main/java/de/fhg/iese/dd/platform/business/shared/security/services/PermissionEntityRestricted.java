/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collections;
import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;

public class PermissionEntityRestricted<E extends BaseEntity> extends PermissionEntityIdRestricted {

    @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
    public static final PermissionEntityRestricted<?> ALL_DENIED =
            new PermissionEntityRestricted(Collections.emptySet(), false);

    protected PermissionEntityRestricted(Set<String> allowedEntityIds, boolean allEntitiesAllowed) {
        super(allowedEntityIds, allEntitiesAllowed);
    }

    /**
     * @param entity the entity to check, can be null
     *
     * @return if all entities are allowed or the given entity id is in the set of allowed entity ids.
     */
    public boolean isEntityAllowed(E entity) {
        if (entity == null) {
            return isAllEntitiesAllowed();
        }
        return isEntityIdAllowed(entity.getId());
    }

    /**
     * Inversion of {@link #isEntityAllowed(BaseEntity)}
     */
    public boolean isEntityDenied(E entity) {
        return !isEntityAllowed(entity);
    }

}
