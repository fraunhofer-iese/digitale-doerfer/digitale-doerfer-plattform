/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.email.exceptions.EmailInvalidException;

public interface IEmailAddressVerificationService extends IService {

    /**
     * Checks if the email address follows correct syntax and normalizes it.
     *
     * @param emailAddress the email address to check
     *
     * @return the normalized email address
     *
     * @throws EmailInvalidException if the email address did not follow the correct syntax
     */
    String ensureCorrectEmailSyntaxAndNormalize(String emailAddress) throws EmailInvalidException;

}
