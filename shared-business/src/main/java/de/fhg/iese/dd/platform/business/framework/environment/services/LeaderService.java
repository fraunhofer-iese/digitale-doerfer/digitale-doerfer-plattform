/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment.services;

import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStartedEvent;
import de.fhg.iese.dd.platform.business.framework.environment.events.CurrentNodeLeaderStoppedEvent;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Profile({"!test", "LeaderServiceTest"})
class LeaderService extends BaseService implements ILeaderService {

    record LeaderElectionRecord(String leaderId, long lastSeenAlive) {

    }

    private static final long EXPIRATION_DURATION_MS = TimeUnit.SECONDS.toMillis(90);
    private static final long RENEWAL_DURATION_MS = TimeUnit.SECONDS.toMillis(60);
    private static final long CHECK_INITIAL_DELAY_MS = 2000L; //2 seconds
    private static final long CHECK_INTERVAL_DURATION_MS = 14000L; //14 seconds
    private static final int TRANSACTION_TIMEOUT_SECONDS = 5;

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    protected IEnvironmentService environmentService;

    protected LeaderElectionRecord currentLeaderElectionRecord;
    protected String lastSeenLeader = null;
    protected boolean shuttingDown = false;

    @Override
    public Pair<String, Long> getLastSeenLeader() {
        if (lastSeenLeader != null && currentLeaderElectionRecord != null) {
            return Pair.of(lastSeenLeader, currentLeaderElectionRecord.lastSeenAlive());
        } else {
            return null;
        }
    }

    @Scheduled(initialDelay = CHECK_INITIAL_DELAY_MS, fixedRate = CHECK_INTERVAL_DURATION_MS)
    public void runLeaderCheck() {

        if (shuttingDown) {
            //we are shutting down, so no need to become leader
            return;
        }
        if (log.isTraceEnabled()) {
            log.trace("Checking leader");
        }
        String instanceIdentifier = getInstanceIdentifier();

        currentLeaderElectionRecord = readLeaderElectionRecord();
        if (currentLeaderElectionRecord == null) {
            //initial leader record, this should only happen once per DB
            if (log.isTraceEnabled()) {
                log.trace("Writing initial leader record");
            }
            insertLeaderElectionRecord(instanceIdentifier, timeService.currentTimeMillisUTC());
            //read it directly to check if we won
            currentLeaderElectionRecord = readLeaderElectionRecord();
        }
        //check if the current leader election record expired
        if (currentLeaderElectionRecord != null
                && isOlderThan(currentLeaderElectionRecord, EXPIRATION_DURATION_MS)) {
            //the current leader election record expired, try becoming leader
            if (log.isTraceEnabled()) {
                log.trace("Current leader election record expired, trying to be leader");
            }
            updateLeaderElectionRecordQuery(currentLeaderElectionRecord.lastSeenAlive(), instanceIdentifier,
                    timeService.currentTimeMillisUTC());
            //read it directly to check if we won
            currentLeaderElectionRecord = readLeaderElectionRecord();
        }
        //check if we can decide who is leader
        if (currentLeaderElectionRecord != null
                && !isOlderThan(currentLeaderElectionRecord, EXPIRATION_DURATION_MS)) {
            //the leader election record is valid, so we can check who is leader
            if (instanceIdentifier.equals(currentLeaderElectionRecord.leaderId())) {
                //we are leader
                if (log.isTraceEnabled()) {
                    log.trace("Node is leader");
                }
                if (!instanceIdentifier.equals(lastSeenLeader)) {
                    //we just got leader
                    log.info("Node {} is leader now", instanceIdentifier);
                    notify(new CurrentNodeLeaderStartedEvent());
                }
                lastSeenLeader = instanceIdentifier;
                if (isOlderThan(currentLeaderElectionRecord, RENEWAL_DURATION_MS)) {
                    //the current leader election record needs renewal
                    if (log.isTraceEnabled()) {
                        log.trace("Renewing leader election record");
                    }
                    updateLeaderElectionRecordQuery(currentLeaderElectionRecord.lastSeenAlive(), instanceIdentifier,
                            timeService.currentTimeMillisUTC());
                }
            } else {
                //we are follower
                if (log.isTraceEnabled()) {
                    log.trace("Node is follower");
                }
                if (instanceIdentifier.equals(lastSeenLeader)) {
                    //we just got follower
                    log.warn("Node {} is follower now (other node overruled)", instanceIdentifier);
                    notify(new CurrentNodeLeaderStoppedEvent());
                }
                lastSeenLeader = currentLeaderElectionRecord.leaderId();
            }
        } else {
            //the leader election record is not there or invalid
            if (log.isTraceEnabled()) {
                log.trace("Unable to determine leader");
            }
            if (instanceIdentifier.equals(lastSeenLeader)) {
                //we just got follower
                log.warn("Node {} is follower now", instanceIdentifier);
                notify(new CurrentNodeLeaderStoppedEvent());
            }
            lastSeenLeader = null;
        }
    }

    @EventListener(classes = {ContextClosedEvent.class})
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public void onShutdown() {

        shuttingDown = true;
        if (currentLeaderElectionRecord == null) {
            //no leader record available, we are not leader
            return;
        }
        if (isOlderThan(currentLeaderElectionRecord, EXPIRATION_DURATION_MS)) {
            //expired, we are not leader
            return;
        }
        String instanceIdentifier = getInstanceIdentifier();
        if (!instanceIdentifier.equals(currentLeaderElectionRecord.leaderId())) {
            //we are not leader
            return;
        }

        log.info("Expiring own leader election record");

        //we are leader and need to let the others know that we are going to die
        updateLeaderElectionRecordQuery(currentLeaderElectionRecord.lastSeenAlive(), instanceIdentifier, 0);
    }

    protected String getInstanceIdentifier() {

        return environmentService.getInstanceIdentifier();
    }

    private boolean isOlderThan(LeaderElectionRecord leaderElectionRecord, long durationMilliseconds) {

        return timeService.currentTimeMillisUTC() - leaderElectionRecord.lastSeenAlive() > durationMilliseconds;
    }

    private void insertLeaderElectionRecord(String leaderId, long lastSeenAlive) {

        try {
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            transactionTemplate.setTimeout(TRANSACTION_TIMEOUT_SECONDS);
            transactionTemplate.executeWithoutResult(
                    (ts) -> insertLeaderElectionRecordInternal(leaderId, lastSeenAlive)
            );
        } catch (Exception e) {
            log.error("Failed to insert leader election record", e);
        }
    }

    public void insertLeaderElectionRecordInternal(String leaderId, long lastSeenAlive) {

        Query query = entityManager.createNativeQuery("insert ignore into shared_leader_election " +
                "(id, leader_id, last_seen_alive) values (1,?,?)")
                .setParameter(1, leaderId)
                .setParameter(2, lastSeenAlive);
        query.executeUpdate();
    }

    private LeaderElectionRecord readLeaderElectionRecord() {

        try {
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            transactionTemplate.setTimeout(TRANSACTION_TIMEOUT_SECONDS);
            return transactionTemplate.execute(
                    (ts) -> readLeaderElectionRecordInternal()
            );
        } catch (Exception e) {
            log.error("Failed to read leader election record", e);
            return null;
        }
    }

    public LeaderElectionRecord readLeaderElectionRecordInternal() {

        Query query = entityManager.createNativeQuery("select leader_id, last_seen_alive " +
                "from shared_leader_election " +
                "where id = 1");
        final List<?> resultList = query.getResultList();
        if (resultList == null || resultList.isEmpty()) {
            return null;
        } else {
            final Object[] values = (Object[]) resultList.get(0);
            if (values != null && values.length == 2) {
                return new LeaderElectionRecord((String) values[0], ((BigInteger) values[1]).longValue());
            } else {
                return null;
            }
        }
    }

    public void updateLeaderElectionRecordQuery(long currentLastSeenAlive, String newLeaderId, long newLastSeenAlive) {

        try {
            TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
            transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            transactionTemplate.setTimeout(TRANSACTION_TIMEOUT_SECONDS);
            transactionTemplate.executeWithoutResult(
                    (ts) -> updateLeaderElectionRecordQueryInternal(currentLastSeenAlive, newLeaderId, newLastSeenAlive)
            );
        } catch (Exception e) {
            log.error("Failed to update leader election record", e);
        }
    }

    public void updateLeaderElectionRecordQueryInternal(long currentLastSeenAlive, String newLeaderId,
            long newLastSeenAlive) {

        Query query = entityManager.createNativeQuery("update " +
                "shared_leader_election " +
                "set leader_id = ?,  last_seen_alive = ? " +
                "where id = 1 " +
                "and last_seen_alive = ?")
                .setParameter(1, newLeaderId)
                .setParameter(2, newLastSeenAlive)
                .setParameter(3, currentLastSeenAlive);
        query.executeUpdate();
    }

}
