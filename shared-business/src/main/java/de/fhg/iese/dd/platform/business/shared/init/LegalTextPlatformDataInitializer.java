/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import java.util.List;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;

@Component
public class LegalTextPlatformDataInitializer extends BaseLegalTextDataInitializer {

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("legal")
                .processedEntity(LegalText.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {

        //get the legal texts for the platform
        List<LegalText> legalTexts = loadEntitiesFromDataJsonSection(null, LegalText.class, "platform");

        logSummary.info("Creating legal texts for the platform");
        logSummary.indent();
        try {
            legalTexts.forEach(l -> createLegalTextPlatform(logSummary, l));
        } finally {
            logSummary.outdent();
        }
    }

}
