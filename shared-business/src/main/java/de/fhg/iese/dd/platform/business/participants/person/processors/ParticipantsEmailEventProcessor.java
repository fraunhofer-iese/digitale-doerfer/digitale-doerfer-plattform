/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.processors;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonResendVerificationEmailByAdminRequest;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonResendVerificationEmailConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonResendVerificationEmailRequest;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.ResendVerificationEmailNotPossibleException;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.template.ParticipantsTemplate;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import freemarker.template.TemplateException;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class ParticipantsEmailEventProcessor extends BaseEventProcessor {

    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IAuthorizationService authorizationService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private IPersonService personService;
    @Autowired
    private ParticipantsConfig participantsConfig;

    @EventProcessing
    void onPersonCreateConfirmation(PersonCreateConfirmation personCreateConfirmation) {

        final Person person = personCreateConfirmation.getPerson();

        if (!featureService.isFeatureEnabled(PersonEmailVerificationFeature.class, FeatureTarget.of(person))) {
            return;
        }
        sendVerificationEmail(person);
    }

    @EventProcessing
    private void onPersonChangeEmailAddressConfirmation(
            PersonChangeEmailAddressConfirmation personChangeEmailAddressConfirmation) {

        final Person person = personChangeEmailAddressConfirmation.getPerson();

        if (person.isEmailVerified() && StringUtils.isEmpty(person.getPendingNewEmail())) {
            return;
        }

        if (!featureService.isFeatureEnabled(PersonEmailVerificationFeature.class, FeatureTarget.of(person))) {
            return;
        }
        sendVerificationEmail(person);
    }

    /**
     * Handles both {@link PersonResendVerificationEmailRequest} and
     * {@link PersonResendVerificationEmailByAdminRequest}. In both cases
     * {@link PersonResendVerificationEmailConfirmation} is returned
     */
    @EventProcessing(executionStrategy = EventExecutionStrategy.SYNCHRONOUS_PREFERRED, includeSubtypesOfEvent = true)
    private PersonResendVerificationEmailConfirmation onPersonResendVerificationEmailRequest(
            PersonResendVerificationEmailRequest personResendVerificationEmailRequest) {

        Person person = personResendVerificationEmailRequest.getPerson();
        final long emailResendInterval = participantsConfig.getEmailResendInterval().toMillis();

        // When requested by admin time restriction for resending the verification mail can be ignored
        if (!(personResendVerificationEmailRequest instanceof PersonResendVerificationEmailByAdminRequest)) {
            if (person.isEmailVerified() && StringUtils.isEmpty(person.getPendingNewEmail())) {
                throw ResendVerificationEmailNotPossibleException.forEmailAlreadyVerified();
            }

            if (person.getLastSentTimeEmailVerification() != null &&
                    (timeService.currentTimeMillisUTC() - person.getLastSentTimeEmailVerification()) <
                            emailResendInterval) {
                throw ResendVerificationEmailNotPossibleException.forWaitForResendNotExpired(
                        timeService.toLocalTimeHumanReadable(
                                person.getLastSentTimeEmailVerification() + emailResendInterval));
            }
        }
        sendVerificationEmail(person);

        return new PersonResendVerificationEmailConfirmation(person);
    }

    private void sendVerificationEmail(Person person) {
        try {
            String fromEmailAddress = participantsConfig.getSenderEmailAddressAccount();
            String toEmailAddress =
                    StringUtils.isEmpty(person.getPendingNewEmail()) ? person.getEmail() : person.getPendingNewEmail();
            String subject = String.format(ParticipantsTemplate.EMAIL_VERIFICATION_EMAIL_SUBJECT, person.getFullName());
            String emailText =
                    emailSenderService.createTextWithTemplate(ParticipantsTemplate.EMAIL_VERIFICATION_EMAIL,
                            buildTemplateModel(person, toEmailAddress));

            emailSenderService.sendEmail(fromEmailAddress, person.getFullName(), toEmailAddress, subject, emailText,
                    Collections.emptyList(), true);
            personService.updateLastSentTimeEmailVerification(person, timeService.currentTimeMillisUTC());
        } catch (IOException | TemplateException | MessagingException e) {
            log.error("Failed to send verification email: " + e.getMessage(), e);
        }
    }

    private Map<String, Object> buildTemplateModel(Person receiver, String emailAddress) {
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(receiver)
                        .emailAddress(emailAddress)
                        .build();

        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiverName", receiver.getFullName());
        templateModel.put("verifyEmailLink", UriComponentsBuilder
                .fromHttpUrl(participantsConfig.getEmailVerificationWebAppUrl())
                .queryParam("person", receiver.getId())
                .queryParam("token",
                        authorizationService.generateAuthToken(personChangeEmailAddressVerificationStatusRequest,
                                participantsConfig.getEmailVerificationExpirationTime()))
                .build());
        templateModel.put("contactPerson", roleService.getDefaultContactPersonForTenant(receiver.getTenant()));
        return templateModel;
    }

}
