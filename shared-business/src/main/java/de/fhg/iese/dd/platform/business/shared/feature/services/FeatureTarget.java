/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.feature.services;

import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import lombok.Getter;

@Getter
public class FeatureTarget {

    /**
     * The order of the levels determines the lookup order! Changing the order of declaration has direct impact on the
     * lookup order at the FeatureService. If the feature target is related to an app or app variant
     * <pre>
     *      GEOAREA_APPVARIANT
     *          TENANT_APPVARIANT
     *              GEOAREA_APP
     *                  TENANT_APP
     *                      GEOAREA
     *                          TENANT
     *                              APPVARIANT
     *                                  APP
     *                                      COMMON
     * </pre>
     * If the feature target is independent of an app or app variant
     * <pre>
     *      GEOAREA
     *          TENANT
     *              COMMON
     * </pre>
     * Only those features that match with the same target identifier are used to get overwritten. That means that only
     * the same app / app variant / tenant are used.
     */
    enum FeatureTargetLevel {
        GEOAREA_APPVARIANT,
        TENANT_APPVARIANT,
        GEOAREA_APP,
        TENANT_APP,
        GEOAREA,
        TENANT,
        APPVARIANT,
        APP,
        COMMON
    }

    @Nullable
    private final String geoAreaId;
    @Nullable
    private final String tenantId;
    @Nullable
    private final String appVariantId;
    @Nullable
    private final String appId;

    FeatureTarget(
            @Nullable GeoArea geoArea,
            @Nullable Tenant tenant,
            @Nullable AppVariant appVariant,
            @Nullable App app) {
        this.appVariantId = BaseEntity.getIdOf(appVariant);
        this.tenantId = BaseEntity.getIdOf(tenant);
        this.geoAreaId = BaseEntity.getIdOf(geoArea);

        // we want the appId also set if we have an app variant
        // creating a FeatureTarget with both app and app variant is not possible
        if (appVariant != null) {
            this.appId = BaseEntity.getIdOf(appVariant.getApp());
        } else {
            this.appId = BaseEntity.getIdOf(app);
        }
    }

    public static FeatureTarget of(
            @Nullable Tenant tenant,
            @Nullable AppVariant appVariant) {
        return new FeatureTarget(null, tenant, appVariant, null);
    }

    public static FeatureTarget of(
            @Nullable GeoArea geoArea,
            @Nullable AppVariant appVariant) {
        return new FeatureTarget(geoArea, null, appVariant, null);
    }

    public static FeatureTarget of(
            @Nullable GeoArea geoArea,
            @Nullable Tenant tenant,
            @Nullable AppVariant appVariant) {
        return new FeatureTarget(geoArea, tenant, appVariant, null);
    }

    public static FeatureTarget of(
            @Nullable GeoArea geoArea,
            @Nullable Tenant tenant,
            @Nullable App app) {
        return new FeatureTarget(geoArea, tenant, null, app);
    }

    public static FeatureTarget of(
            @Nullable GeoArea geoArea,
            @Nullable Tenant tenant) {
        return new FeatureTarget(geoArea, tenant, null, null);
    }

    /**
     * Currently the same as {@link #of(GeoArea, Tenant, AppVariant)}
     */
    public static FeatureTarget of(
            @Nullable Person person,
            @Nullable AppVariant appVariant) {
        return new FeatureTarget(person != null ? person.getHomeArea() : null,
                person != null ? person.getTenant() : null, appVariant, null);
    }

    public static FeatureTarget of(
            @Nullable Tenant tenant,
            @Nullable App app) {
        return new FeatureTarget(null, tenant, null, app);
    }

    public static FeatureTarget of(
            @Nullable Tenant tenant) {
        return new FeatureTarget(null, tenant, null, null);
    }

    public static FeatureTarget of(
            @Nullable GeoArea geoArea) {
        return new FeatureTarget(geoArea, null, null, null);
    }

    /**
     * Currently the same as {@link #of(GeoArea, Tenant)}
     */
    public static FeatureTarget of(
            @Nullable Person person) {
        return new FeatureTarget(person != null ? person.getHomeArea() : null,
                person != null ? person.getTenant() : null, null, null);
    }

    public static FeatureTarget of(
            @Nullable AppVariant appVariant) {
        return new FeatureTarget(null, null, appVariant, null);
    }

    public static FeatureTarget of(
            @Nullable App app) {
        return new FeatureTarget(null, null, null, app);
    }

    /**
     * Most common feature configuration.
     */
    public static FeatureTarget common() {
        return new FeatureTarget(null, null, null, null);
    }

    /**
     * Feature name + feature target identifier
     */
    String getFeatureKeyIdentifier(Class<? extends BaseFeature> featureClass, FeatureTargetLevel level) {
        String featureTargetIdentifier = getFeatureTargetIdentifier(level);
        if (featureTargetIdentifier != null) {
            return featureClass.getName() + featureTargetIdentifier;
        } else {
            return null;
        }
    }

    String getFeatureTargetIdentifier(FeatureTargetLevel level) {
        switch (level) {
            case GEOAREA_APPVARIANT:
                if (appVariantId == null || geoAreaId == null) {
                    return null;
                }
                return "av" + appVariantId +
                        "g" + geoAreaId;
            case TENANT_APPVARIANT:
                if (appVariantId == null || tenantId == null) {
                    return null;
                }
                return "av" + appVariantId +
                        "t" + tenantId;
            case GEOAREA_APP:
                if (appId == null || geoAreaId == null) {
                    return null;
                }
                return "a" + appId +
                        "g" + geoAreaId;
            case TENANT_APP:
                if (appId == null || tenantId == null) {
                    return null;
                }
                return "a" + appId +
                        "t" + tenantId;
            case GEOAREA:
                if (geoAreaId == null) {
                    return null;
                }
                return "g" + geoAreaId;
            case TENANT:
                if (tenantId == null) {
                    return null;
                }
                return "t" + tenantId;
            case APPVARIANT:
                if (appVariantId == null) {
                    return null;
                }
                return "av" + appVariantId;
            case APP:
                if (appId == null) {
                    return null;
                }
                return "a" + appId;
            default:
                return "c";
        }
    }

    FeatureTargetLevel getMostSpecificFeatureTargetLevel() {
        if (geoAreaId == null) {
            if (tenantId == null) {
                if (appVariantId == null) {
                    if (appId == null) {
                        return FeatureTargetLevel.COMMON;
                    } else {
                        return FeatureTargetLevel.APP;
                    }
                } else {
                    return FeatureTargetLevel.APPVARIANT;
                }
            } else {
                if (appVariantId == null) {
                    if (appId == null) {
                        return FeatureTargetLevel.TENANT;
                    } else {
                        return FeatureTargetLevel.TENANT_APP;
                    }
                } else {
                    return FeatureTargetLevel.TENANT_APPVARIANT;
                }
            }
        } else {
            //geo areas is set, so we do not care about the tenant at all
            if (appVariantId == null) {
                if (appId == null) {
                    return FeatureTargetLevel.GEOAREA;
                } else {
                    return FeatureTargetLevel.GEOAREA_APP;
                }
            } else {
                return FeatureTargetLevel.GEOAREA_APPVARIANT;
            }
        }
    }

    @Override
    public String toString() {
        return "FeatureTarget[" +
                " geoAreaId='" + geoAreaId + "'" +
                ", tenantId='" + tenantId + "'" +
                ", appVariantId='" + appVariantId + "'" +
                ", appId='" + appId + "'" +
                ']';
    }

}
