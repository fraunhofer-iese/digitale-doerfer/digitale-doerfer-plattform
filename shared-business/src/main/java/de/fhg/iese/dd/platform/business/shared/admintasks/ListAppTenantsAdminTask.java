/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

@Component
public class ListAppTenantsAdminTask extends BaseAdminTask {

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAppService appService;

    @Override
    public String getName() {
        return "ListAppTenants";
    }

    @Override
    public String getDescription() {
        return "Lists the available app variants per tenant";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) {

        Collection<AppVariant> appVariants = appService.findAllAppVariants();

        Multimap<Tenant, AppVariant> appVariantsByTenant = ArrayListMultimap.create();
        for (AppVariant appVariant : appVariants) {
            final Set<Tenant> availableTenants = appService.getAvailableTenants(appVariant);
            for (Tenant tenant : availableTenants) {
                appVariantsByTenant.put(tenant, appVariant);
            }
        }

        List<Tenant> tenants = tenantService.findAllOrderedByNameAsc();

        for(Tenant tenant : tenants) {

            logSummary.info("{} ({})", tenant.getName(), tenant.getId());
            logSummary.indent();
            
            appVariantsByTenant.get(tenant).stream()
                    .map(AppVariant::getAppVariantIdentifier)
                    .sorted()
                    .forEach(logSummary::info);

            logSummary.outdent();
        }
    }

}
