/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

/**
 * An {@link Action} encapsulates the decision logic if a person is permitted to do some interaction. The input of such
 * a decision are the {@link RoleAssignment}s of a person, the output is a {@link Permission}. The resulting permission
 * can also restrict how the action can be done, e.g. by specifying the entities that can be used in the action.
 *
 * @param <P>
 *         the type of permission
 */
public abstract class Action<P extends Permission> {

    /**
     * Decide if the action can be done by a person with the given role assignments or not and how the permission is
     * restricted.
     *
     * @param roleAssignments
     *         the role assignments of a person
     *
     * @return a permission if the action can be done or how the action needs to be restricted.
     */
    public abstract P decidePermission(Collection<RoleAssignment> roleAssignments);

    /**
     * A simple description what this action does, used in the swagger documentation.
     */
    public abstract String getActionDescription();

    /**
     * A more detailed description to whom the permission is granted.
     *
     * @param roleMapper
     *         can be used to get the actual {@link BaseRole} instance, so that {@link BaseRole#getKey()} can be used.
     */
    public abstract String getPermissionDescription(Function<Class<? extends BaseRole<?>>, BaseRole<?>> roleMapper);

    /**
     * All roles that lead to {@link Permission#isAllowed()} == true.
     */
    public abstract List<Class<? extends BaseRole<?>>> getPermittedRoles();

}
