/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.push.exceptions.ExternalPushProviderException;
import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider.ExternalPushApp;
import de.fhg.iese.dd.platform.business.shared.push.providers.IExternalPushProvider.ExternalPushAppConfiguration;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.Builder;
import lombok.Getter;
import software.amazon.awssdk.services.pinpoint.PinpointClient;
import software.amazon.awssdk.services.pinpoint.model.APNSChannelRequest;
import software.amazon.awssdk.services.pinpoint.model.APNSSandboxChannelRequest;
import software.amazon.awssdk.services.pinpoint.model.ApplicationResponse;
import software.amazon.awssdk.services.pinpoint.model.CreateAppRequest;
import software.amazon.awssdk.services.pinpoint.model.CreateAppResponse;
import software.amazon.awssdk.services.pinpoint.model.CreateApplicationRequest;
import software.amazon.awssdk.services.pinpoint.model.DeleteAppRequest;
import software.amazon.awssdk.services.pinpoint.model.DeleteAppResponse;
import software.amazon.awssdk.services.pinpoint.model.GCMChannelRequest;
import software.amazon.awssdk.services.pinpoint.model.GetAppRequest;
import software.amazon.awssdk.services.pinpoint.model.GetAppResponse;
import software.amazon.awssdk.services.pinpoint.model.GetAppsRequest;
import software.amazon.awssdk.services.pinpoint.model.GetAppsResponse;
import software.amazon.awssdk.services.pinpoint.model.TagResourceRequest;
import software.amazon.awssdk.services.pinpoint.model.TagsModel;
import software.amazon.awssdk.services.pinpoint.model.UpdateApnsChannelRequest;
import software.amazon.awssdk.services.pinpoint.model.UpdateApnsSandboxChannelRequest;
import software.amazon.awssdk.services.pinpoint.model.UpdateGcmChannelRequest;

/**
 * The main purpose of this class is to split up the push sending and configuration, so that {@link
 * PinpointExternalPushProvider} does not get too unreadable.
 */
@Profile("aws | PinpointExternalPushProviderTest")
@Component
class PinpointExternalPushConfigProvider extends PinpointBaseProvider {

    @Getter
    @Builder
    private static class CertificateWithPrivateKey {

        private final String certX509;
        private final String privateKeyPkcs8;

    }

    private String appPrefix;

    @PostConstruct
    protected void init() {
        super.init();
        if (awsConfig.getPinpoint() == null || StringUtils.isEmpty(awsConfig.getPinpoint().getAppPrefix())) {
            throw new IllegalStateException("Missing aws.pinpoint.app-prefix in application configuration.");
        }
        final String appPrefix = awsConfig.getPinpoint().getAppPrefix();
        if (!appPrefix.matches("[a-z]+")) {
            throw new IllegalStateException(
                    "Configuration property aws.pinpoint.app-prefix may only consist of lower-case letters.");
        }
        this.appPrefix = appPrefix + ".";
    }

    public List<ExternalPushApp> listAllPushApps() {

        log.debug("listing all push apps");

        final List<ExternalPushApp> list = new ArrayList<>();

        String nextToken = null;
        do {
            final String currentToken = nextToken;
            GetAppsResponse appsResponse = callPinpoint(PinpointClient::getApps, GetAppsRequest.builder()
                    .pageSize("100")
                    .token(currentToken)
                    .build());
            for (ApplicationResponse app : appsResponse.applicationsResponse().item()) {
                list.add(ExternalPushApp.builder()
                        .pushAppId(app.id())
                        .pushAppName(app.name())
                        .build());
            }
            nextToken = appsResponse.applicationsResponse().nextToken();
        } while (nextToken != null);

        return list;
    }

    public ExternalPushApp createOrUpdatePushApp(AppVariant appVariant,
            ExternalPushAppConfiguration externalPushAppConfiguration) throws ExternalPushProviderException {

        log.debug("Create or update push app for {}", appVariant.getAppVariantIdentifier());

        ExternalPushApp pushApp = findExistingPushApp(appVariant);

        if (pushApp == null) {
            pushApp = createPushApp(appVariant);
        } else {
            log.debug("Found existing push app {} ({}) for {}", pushApp.getPushAppName(), pushApp.getPushAppId(),
                    appVariant);
            updatePushApp(pushApp);
        }

        final String pushAppId = pushApp.getPushAppId();

        checkNotEmpty(pushAppId, "pushAppId");

        if (externalPushAppConfiguration.isFcmSet()) {
            try {
                log.debug("Configuring fcm for push app {}", pushAppId);
                updateFcmChannel(pushAppId, externalPushAppConfiguration.getFcmApiKey());
            } catch (Exception e) {
                throw new ExternalPushProviderException(
                        "Failed to configure FCM push channel for " + appVariant.getAppVariantIdentifier(), e);
            }
        }
        if (externalPushAppConfiguration.isApnsSet()) {
            try {
                log.debug("Configuring apns for push app {}", pushAppId);
                CertificateWithPrivateKey apnsCertificate =
                        createCertificateWithPrivateKey(externalPushAppConfiguration.getApnsCertificate(),
                                externalPushAppConfiguration.getApnsCertificatePassword());
                updateApnsChannel(pushAppId, apnsCertificate);
            } catch (Exception e) {
                throw new ExternalPushProviderException(
                        "Failed to configure APNS push channel for " + appVariant.getAppVariantIdentifier(), e);
            }
        }
        if (externalPushAppConfiguration.isApnsSandboxSet()) {
            try {
                log.debug("Configuring apns sandbox for push app {}", pushAppId);
                CertificateWithPrivateKey apnsSandboxCertificate =
                        createCertificateWithPrivateKey(externalPushAppConfiguration.getApnsSandboxCertificate(),
                                externalPushAppConfiguration.getApnsSandboxCertificatePassword());
                updateApnsSandboxChannel(pushAppId, apnsSandboxCertificate);
            } catch (Exception e) {
                throw new ExternalPushProviderException(
                        "Failed to configure APNS Sandbox push channel for " + appVariant.getAppVariantIdentifier(), e);
            }
        }

        return pushApp;
    }

    private CertificateWithPrivateKey createCertificateWithPrivateKey(byte[] certificatePKCS12,
            String certificatePassword)
            throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException,
            UnrecoverableKeyException {

        final KeyStore keystore = KeyStore.getInstance("pkcs12");
        try (InputStream is = new ByteArrayInputStream(certificatePKCS12)) {
            keystore.load(is, certificatePassword.toCharArray());
        }

        //get first alias
        final String alias = keystore.aliases().nextElement();
        final Certificate certificate = keystore.getCertificate(alias);
        final PrivateKey privateKey = (PrivateKey) keystore.getKey(alias, certificatePassword.toCharArray());

        final String encodedCert = encodeCertificate(certificate.getEncoded(), "CERTIFICATE");
        final String encodedPrivateKey = encodeCertificate(privateKey.getEncoded(), "PRIVATE KEY");

        return CertificateWithPrivateKey.builder().certX509(encodedCert).privateKeyPkcs8(encodedPrivateKey).build();
    }

    private String encodeCertificate(byte[] bytes, String markerText) {

        StringBuilder sb = new StringBuilder();
        sb.append("-----BEGIN ").append(markerText).append("-----\n");
        final String encoded = new String(Base64.getEncoder().encode(bytes), StandardCharsets.US_ASCII);
        sb.append(encoded.replaceAll("(.{64})", "$1\n")); //break after 64 chars
        sb.append("\n-----END ").append(markerText).append("-----");

        return sb.toString();
    }

    private void updateFcmChannel(String pushAppId, String fcmApiKey) {
        GCMChannelRequest gcmChannelRequest;
        if (StringUtils.isNotEmpty(fcmApiKey)) {
            gcmChannelRequest = GCMChannelRequest.builder()
                    .apiKey(fcmApiKey)
                    .enabled(true)
                    .build();
        } else {
            gcmChannelRequest = GCMChannelRequest.builder()
                    .enabled(false)
                    .build();
        }
        callPinpoint(PinpointClient::updateGcmChannel,
                UpdateGcmChannelRequest.builder()
                        .applicationId(pushAppId)
                        .gcmChannelRequest(gcmChannelRequest)
                        .build());
    }

    @SuppressWarnings("Duplicates")
    private void updateApnsChannel(String pushAppId, CertificateWithPrivateKey certificateWithPrivateKey) {

        APNSChannelRequest apnsChannelRequest;
        //IntelliJ complains about duplicate code, but this can not be fixed in a type safe way since
        //APNSChannelRequest and APNSSandboxChannelRequest are not implementing a common interface
        if (certificateWithPrivateKey != null) {
            apnsChannelRequest = APNSChannelRequest.builder()
                    .certificate(certificateWithPrivateKey.getCertX509())
                    .privateKey(certificateWithPrivateKey.getPrivateKeyPkcs8())
                    .enabled(true)
                    .build();
        } else {
            apnsChannelRequest = APNSChannelRequest.builder()
                    .enabled(false)
                    .build();
        }

        callPinpoint(PinpointClient::updateApnsChannel, UpdateApnsChannelRequest.builder()
                .applicationId(pushAppId)
                .apnsChannelRequest(apnsChannelRequest)
                .build());
    }

    @SuppressWarnings("Duplicates")
    private void updateApnsSandboxChannel(String pushAppId, CertificateWithPrivateKey certificateWithPrivateKey) {

        APNSSandboxChannelRequest apnsSandboxChannelRequest;

        if (certificateWithPrivateKey != null) {
            apnsSandboxChannelRequest = APNSSandboxChannelRequest.builder()
                    .certificate(certificateWithPrivateKey.getCertX509())
                    .privateKey(certificateWithPrivateKey.getPrivateKeyPkcs8())
                    .enabled(true)
                    .build();
        } else {
            apnsSandboxChannelRequest = APNSSandboxChannelRequest.builder()
                    .enabled(false)
                    .build();
        }

        callPinpoint(PinpointClient::updateApnsSandboxChannel,
                UpdateApnsSandboxChannelRequest.builder()
                        .applicationId(pushAppId)
                        .apnsSandboxChannelRequest(apnsSandboxChannelRequest)
                        .build());
    }

    private ExternalPushApp createPushApp(AppVariant appVariant) {

        String appVariantIdentifier = appVariant.getAppVariantIdentifier();
        checkNotEmpty(appVariantIdentifier, "appVariantIdentifier");

        String appName = toPushAppName(appVariantIdentifier);

        log.info("Creating push app for {} with name {}", appVariantIdentifier, appName);

        CreateAppResponse response = callPinpoint(PinpointClient::createApp, CreateAppRequest.builder()
                .createApplicationRequest(
                        CreateApplicationRequest.builder()
                                .name(appName)
                                .tags(awsConfig.getAWSTags())
                                .build())
                .build()
        );

        return ExternalPushApp.builder()
                .pushAppId(response
                        .applicationResponse()
                        .id())
                .pushAppName(appName)
                .build();
    }

    // adds tags from AWSConfig to an existing push app
    private void updatePushApp(ExternalPushApp pushApp) {
        GetAppResponse response = pinpointClient.getApp(GetAppRequest.builder()
                .applicationId(pushApp.getPushAppId())
                .build());
        pinpointClient.tagResource(TagResourceRequest.builder()
                .resourceArn(response.applicationResponse().arn())
                .tagsModel(TagsModel.builder()
                        .tags(awsConfig.getAWSTags())
                        .build())
                .build());
    }

    public String getPushAppInfoForAppVariant(AppVariant appVariant) {
        ExternalPushApp pushApp = findExistingPushApp(appVariant);
        if (pushApp == null) {
            return "No push app found";
        }
        GetAppResponse response = pinpointClient.getApp(GetAppRequest.builder()
                .applicationId(pushApp.getPushAppId())
                .build());
        return "Found push app at AWS:\n" + response.applicationResponse();
    }

    public long deleteAppVariant(AppVariant appVariant) {
        if (pushConfigurationInvalid(appVariant)) {
            log.info("Deleted no push apps (none were defined) for {}", appVariant);
            return 0;
        }
        DeleteAppResponse response = callPinpointIgnoreNotFound(PinpointClient::deleteApp, DeleteAppRequest.builder()
                .applicationId(appVariant.getPushAppId())
                .build());
        if (response != null) {
            log.info("Deletion of push app for {} returned {}", appVariant,
                    response.applicationResponse().toString());
        } else {
            log.info("Deletion of push app {} for {} returned not found, assuming it is already deleted",
                    appVariant.getPushAppId(), appVariant);
        }
        return 1L;
    }

    private String toPushAppName(String appVariantIdentifier) {
        Objects.requireNonNull(appVariantIdentifier);
        return appPrefix + appVariantIdentifier;
    }

    private ExternalPushApp findExistingPushApp(AppVariant appVariant) {
        List<ExternalPushApp> existingPushApps = listAllPushApps();
        return existingPushApps.stream()
                .filter(pa -> StringUtils.equals(toPushAppName(appVariant.getAppVariantIdentifier()),
                        pa.getPushAppName()))
                .findFirst().orElse(null);
    }

    private void checkNotEmpty(String value, String valueName) {
        if (StringUtils.isEmpty(value)) {
            throw new IllegalArgumentException(valueName + " must not be empty");
        }
    }

}
