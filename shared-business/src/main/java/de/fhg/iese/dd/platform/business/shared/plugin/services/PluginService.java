/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.plugin.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.shared.plugin.PluginInstance;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;

@Service
class PluginService implements IPluginService {

    private Map<PluginTarget<?>, List<? extends PluginInstance<?>>> pluginInstancesByPluginTarget;
    private Map<PluginTarget<?>, List<?>> pluginsRawByPluginTarget;

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    private void initialize() {
        pluginInstancesByPluginTarget = new ConcurrentHashMap<>();
        pluginsRawByPluginTarget = new ConcurrentHashMap<>();
    }

    @Override
    public <P> List<PluginInstance<P>> getPluginInstances(PluginTarget<P> pluginTarget) {

        @SuppressWarnings("unchecked")
        List<PluginInstance<P>> pluginInstances =
                (List<PluginInstance<P>>) pluginInstancesByPluginTarget.computeIfAbsent(pluginTarget,
                        missingPluginTarget -> getPluginsFromContext(missingPluginTarget.getPluginClass()).stream()
                                .map(PluginInstanceImpl::new)
                                .collect(Collectors.toList()));
        return pluginInstances;
    }

    @Override
    public <P> List<P> getPluginInstancesRaw(PluginTarget<P> pluginTarget) {

        @SuppressWarnings("unchecked")
        List<P> pluginsRaw = (List<P>) pluginsRawByPluginTarget.computeIfAbsent(pluginTarget,
                missingPluginTarget -> getPluginInstances(missingPluginTarget).stream()
                        .map(PluginInstance::getInstance)
                        .collect(Collectors.toList()));
        return pluginsRaw;
    }

    private <P> List<? extends P> getPluginsFromContext(Class<P> pluginClass) {

        Map<String, ? extends P> matchingPlugins = context.getBeansOfType(pluginClass, true, true);

        return matchingPlugins.values().stream()
                .sorted(AnnotationAwareOrderComparator.INSTANCE)
                .collect(Collectors.toList());
    }

}
