/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.init;

import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class BaseAppDataInitializer extends BaseDataInitializer {

    @Autowired
    protected TenantRepository tenantRepository;
    @Autowired
    protected GeoAreaRepository geoAreaRepository;
    @Autowired
    protected AppRepository appRepository;
    @Autowired
    protected AppVariantRepository appVariantRepository;
    @Autowired
    protected AppVariantVersionRepository appVariantVersionRepository;
    @Autowired
    protected AppVariantGeoAreaMappingRepository appVariantGeoAreaMappingRepository;
    @Autowired
    protected AppVariantTenantContractRepository appVariantTenantContractRepository;
    @Autowired
    protected OauthClientRepository oauthClientRepository;
    @Autowired
    protected IAuthorizationService authorizationService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("app")
                .useCase(USE_CASE_NEW_TENANT)
                .useCase(USE_CASE_NEW_APP_VARIANT)
                .requiredEntity(Tenant.class)
                .requiredEntity(OauthClient.class)
                .processedEntity(App.class)
                .processedEntity(AppVariant.class)
                .processedEntity(AppVariantVersion.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        // load all the apps, which are independent of tenants
        createApps(logSummary);

        // load all the app variants and assign them to the apps. They are mapped to geo areas later
        Map<String, AppVariant> createdAppVariantsByAppVariantIdentifier = createAppVariants(logSummary);

        // load all the app variant versions and assign them to the app variants
        createAppVariantVersions(logSummary);

        // we store them all at once to have a consistent state
        appVariantRepository.saveAll(createdAppVariantsByAppVariantIdentifier.values());
        appVariantRepository.flush();
        // map the oauth clients to app variants
        mapOauthClientsToAppVariants(logSummary);
        // map the geo areas to app variants
        final Pair<Set<AppVariantTenantContract>, Set<AppVariantGeoAreaMapping>> contractsAndMappingsNew =
                mapGeoAreasToAppVariants(logSummary);

        //the contracts need to be stored first, since they are referenced by the mappings
        logSummary.info("stored {} app variant tenant contracts", contractsAndMappingsNew.getFirst().size());
        appVariantTenantContractRepository.saveAll(contractsAndMappingsNew.getFirst());
        appVariantTenantContractRepository.flush();

        storeAppVariantGeoAreaMappings(contractsAndMappingsNew.getFirst(), contractsAndMappingsNew.getSecond(),
                logSummary);
    }

    private void createApps(LogSummary logSummary) {

        List<App> createdApps = new ArrayList<>();
        DuplicateIdChecker<App> appDuplicateIdChecker = new DuplicateIdChecker<>(App::getAppIdentifier);
        List<App> apps = loadAllEntities(App.class);
        for (App app : apps) {
            app = checkId(app);
            appDuplicateIdChecker.checkId(app);
            app = adjustCreated(app);
            createdApps.add(app);
            logSummary.info("created {}", app.toString());
        }
        createdApps = appRepository.saveAll(createdApps);
        // images for the apps are created after they are already stored since the app is owner
        // of the created media item. Otherwise, the reference to the app is missing and an exception is thrown
        createdApps
                .forEach(app -> createImage(App::getLogo, App::setLogo, app, FileOwnership.of(app), logSummary, false));
        // apps must be saved again due to created media item (logo)
        appRepository.saveAll(createdApps);
        appRepository.flush();
    }

    private Map<String, AppVariant> createAppVariants(LogSummary logSummary) {

        Map<String, AppVariant> createdAppVariantsByAppVariantIdentifier = new HashMap<>();

        DuplicateIdChecker<AppVariant> appVariantDuplicateIdChecker =
                new DuplicateIdChecker<>(AppVariant::getAppVariantIdentifier);

        final Map<DataInitKey, List<AppVariant>> appVariantsJsonRaw = loadAllDataInitKeysAndEntities(AppVariant.class);
        for (Map.Entry<DataInitKey, List<AppVariant>> keyAndAppVariants : appVariantsJsonRaw.entrySet()) {

            final DataInitKey key = keyAndAppVariants.getKey();
            final List<AppVariant> appVariantsJson = keyAndAppVariants.getValue();
            if (key.getApp() == null) {
                logSummary.error("No app defined in {}:\n{}", key.toString(), appVariantsJson.toString());
                continue;
            }

            logSummary.info("Creating app variants for {}", key.getApp());
            logSummary.indent();
            for (AppVariant appVariantJson : appVariantsJson) {
                if (appVariantJson.getOauthClients() != null) {
                    throw new DataInitializationException(
                            "App variant '{}' already has oauth clients defined, mapping is only allowed in AppVariantOauthClientMapping.json",
                            appVariantJson.getAppVariantIdentifier());
                }
                appVariantJson = checkId(appVariantJson);
                appVariantDuplicateIdChecker.checkId(appVariantJson);
                appVariantJson.setApp(key.getApp());
                appVariantJson = setExistingAppVariantValues(appVariantJson, logSummary);
                if (StringUtils.endsWith(appVariantJson.getCallBackUrl(), "/")) {
                    String newCallBackUrl = StringUtils.removeEnd(appVariantJson.getCallBackUrl(), "/");
                    logSummary.warn(
                            "App variant callback url '{}' for '{}' ends with '/', truncating it to '{}'. " +
                                    "Adjust data init accordingly!",
                            appVariantJson.getCallBackUrl(), appVariantJson.getAppVariantIdentifier(),
                            newCallBackUrl);
                    appVariantJson.setCallBackUrl(newCallBackUrl);
                }
                createdAppVariantsByAppVariantIdentifier.put(appVariantJson.getAppVariantIdentifier(),
                        appVariantJson);
            }
            logSummary.outdent();
        }
        //we store them all at once to have a consistent state
        //these app variants have the same mapping to available tenants, they get adjusted later and then finally stored again
        final Collection<AppVariant> createdAppVariants = createdAppVariantsByAppVariantIdentifier.values();
        appVariantRepository.saveAll(createdAppVariants);

        checkDuplicateApiKeys(createdAppVariants, logSummary);

        // images for the app variants are created after they are already stored since the app variant is owner
        // of the created media item. Otherwise, the reference to the app variant is missing and an exception is thrown
        createdAppVariants
                .forEach(av -> createImage(AppVariant::getLogo, AppVariant::setLogo, av, FileOwnership.of(av),
                        logSummary, false));
        // app variants must be saved again due to created media item (logo)
        appVariantRepository.saveAll(createdAppVariants);
        appVariantRepository.flush();
        return createdAppVariantsByAppVariantIdentifier;
    }

    protected void checkDuplicateApiKeys(Collection<AppVariant> createdAppVariants, LogSummary logSummary) {
        //after storing them all we check for duplicate keys, so that we can use queries in DB
        createdAppVariants.forEach(appVariant -> checkDuplicateApiKey(appVariant, logSummary));
    }

    private void checkDuplicateApiKey(AppVariant appVariant, LogSummary logSummary) {
        if (!StringUtils.isBlank(appVariant.getApiKey1())) {
            final List<AppVariant> duplicatesApiKey1 =
                    appVariantRepository.findAllOtherAppVariantsByApiKeyOrderByCreatedDesc(appVariant.getApiKey1(),
                            appVariant);
            duplicatesApiKey1.forEach(
                    appVariantDuplicate -> logSummary.error(
                            "Duplicate api key 1 defined for {}: same key defined at {}",
                            appVariant.getAppVariantIdentifier(),
                            appVariantDuplicate.getAppVariantIdentifier()
                    ));
        }
        if (!StringUtils.isBlank(appVariant.getApiKey2())) {
            final List<AppVariant> duplicatesApiKey2 =
                    appVariantRepository.findAllOtherAppVariantsByApiKeyOrderByCreatedDesc(appVariant.getApiKey2(),
                            appVariant);
            duplicatesApiKey2.forEach(
                    appVariantDuplicate -> logSummary.error(
                            "Duplicate api key 2 defined for {}: same key defined at {}",
                            appVariant.getAppVariantIdentifier(),
                            appVariantDuplicate.getAppVariantIdentifier()
                    ));
        }
    }

    protected AppVariant setExistingAppVariantValues(AppVariant appVariant, LogSummary logSummary) {
        // direct usage of repo, not of the service, because it would return the cached variant
        Optional<AppVariant> existingAppVariantOpt = appVariantRepository.findById(appVariant.getId());
        if (existingAppVariantOpt.isPresent()) {
            //app variant already exists
            AppVariant existingAppVariant = existingAppVariantOpt.get();
            //keep the external push app ID
            appVariant.setPushAppId(existingAppVariant.getPushAppId());
            //set the created timestamp of the api key accordingly
            try {
                checkApiKeyAndSetCreated(
                        appVariant::getApiKey1,
                        appVariant::setApiKey1,
                        appVariant::setApiKey1Created,
                        existingAppVariant::getApiKey1,
                        existingAppVariant::getApiKey1Created);
                checkApiKeyAndSetCreated(
                        appVariant::getApiKey2,
                        appVariant::setApiKey2,
                        appVariant::setApiKey2Created,
                        existingAppVariant::getApiKey2,
                        existingAppVariant::getApiKey2Created);
                checkApiKeyAndSetCreated(
                        appVariant::getExternalApiKey,
                        appVariant::setExternalApiKey,
                        appVariant::setExternalApiKeyCreated,
                        existingAppVariant::getExternalApiKey,
                        existingAppVariant::getExternalApiKeyCreated);
            } catch (DataInitializationException e) {
                throw new DataInitializationException("Failed to create " + appVariant + ": " + e.getMessage(), e);
            }
            logSummary.info("updated {}", appVariant);
        } else {
            logSummary.info("created {}", appVariant);
        }
        return appVariant;
    }

    private void checkApiKeyAndSetCreated(Supplier<String> newApiKey, Consumer<String> newApiKeySetter,
            Consumer<Long> newApiKeyCreatedSetter, Supplier<String> oldApiKey, Supplier<Long> oldApiKeyCreated)
            throws DataInitializationException {
        if (StringUtils.isBlank(newApiKey.get())) {
            //there is no api key, so we want to be sure it is null instead of an empty string
            newApiKeySetter.accept(null);
            newApiKeyCreatedSetter.accept(null);
        } else {
            if (!authorizationService.isApiKeyUsable(newApiKey.get())) {
                throw new DataInitializationException(
                        "The given api key '{}' does not meet the minimum security requirements", newApiKey.get());
            }
            if (!StringUtils.equals(newApiKey.get(), oldApiKey.get())) {
                //the new api key differs, so we set its created time to now
                newApiKeyCreatedSetter.accept(timeService.currentTimeMillisUTC());
            } else {
                //the api key did not change, but there was no created timestamp, so we set it to now
                if (oldApiKeyCreated.get() == null) {
                    newApiKeyCreatedSetter.accept(timeService.currentTimeMillisUTC());
                }
            }
        }
    }

    protected void createAppVariantVersions(LogSummary logSummary) {

        final Map<DataInitKey, List<AppVariantVersion>> appVariantVersionsJsonRaw =
                loadAllDataInitKeysAndEntities(AppVariantVersion.class);
        List<AppVariantVersion> createdAppVariantVersions = new ArrayList<>();
        for (Map.Entry<DataInitKey, List<AppVariantVersion>> keyAndAppVariantVersions : appVariantVersionsJsonRaw.entrySet()) {

            final DataInitKey key = keyAndAppVariantVersions.getKey();
            final List<AppVariantVersion> appVariantVersionsJson = keyAndAppVariantVersions.getValue();

            final App app = key.getApp();
            if (app != null) {
                //the versions for the app are used for all app variants
                List<AppVariant> appVariants = appVariantRepository.findAllByAppOrderByAppVariantIdentifier(app);
                for (AppVariant appVariant : appVariants) {
                    logSummary.info("Creating app variant versions for '{}' using versions defined for app",
                            appVariant.getAppVariantIdentifier());
                    for (AppVariantVersion appVariantVersionJson : appVariantVersionsJson) {
                        AppVariantVersion appVariantVersion = AppVariantVersion.builder()
                                .appVariant(appVariant)
                                .versionIdentifier(appVariantVersionJson.getVersionIdentifier())
                                .platform(appVariantVersionJson.getPlatform())
                                .supported(appVariantVersionJson.isSupported())
                                .build()
                                .withConstantId();
                        createdAppVariantVersions.add(appVariantVersion);
                    }
                }
            }
            final AppVariant appVariant = key.getAppVariant();
            if (appVariant != null) {
                logSummary.info(
                        "Creating app variant versions for '{}' using versions explicitly defined for the app variant",
                        appVariant.getAppVariantIdentifier());
                for (AppVariantVersion appVariantVersionJson : appVariantVersionsJson) {
                    AppVariantVersion appVariantVersion = AppVariantVersion.builder()
                            .appVariant(appVariant)
                            .versionIdentifier(appVariantVersionJson.getVersionIdentifier())
                            .platform(appVariantVersionJson.getPlatform())
                            .supported(appVariantVersionJson.isSupported())
                            .build()
                            .withConstantId();
                    createdAppVariantVersions.add(appVariantVersion);
                }
            }
        }

        final List<AppVariantVersion> existingVersions = appVariantVersionRepository.findAll();
        existingVersions.removeAll(createdAppVariantVersions);
        logSummary.info("Deleting {} unused versions", existingVersions.size());
        appVariantVersionRepository.deleteAll(existingVersions);
        logSummary.info("Saving {} changed or new versions", createdAppVariantVersions.size());
        appVariantVersionRepository.saveAll(createdAppVariantVersions);
        appVariantVersionRepository.flush();
    }

    protected Pair<Set<AppVariantTenantContract>, Set<AppVariantGeoAreaMapping>> mapGeoAreasToAppVariants(
            LogSummary logSummary) {

        Set<AppVariantGeoAreaMapping> newMappings = new HashSet<>();
        Set<AppVariantTenantContract> newContracts = new HashSet<>();

        final Map<DataInitKey, List<AppVariantGeoAreaInitMapping>> jsonMappingsRaw =
                loadAllDataInitKeysAndEntities(AppVariantGeoAreaInitMapping.class);

        for (Map.Entry<DataInitKey, List<AppVariantGeoAreaInitMapping>> keyAndMappings : jsonMappingsRaw.entrySet()) {

            final DataInitKey key = keyAndMappings.getKey();
            final List<AppVariantGeoAreaInitMapping> jsonMappings = keyAndMappings.getValue();

            for (AppVariantGeoAreaInitMapping jsonMapping : jsonMappings) {

                if (key.getTenant() == null) {
                    logSummary.error("No tenant defined in {}:\n{}", key.toString(), jsonMapping.toString());
                    continue;
                }
                if (CollectionUtils.isEmpty(jsonMapping.getAppVariantIdentifiers())) {
                    logSummary.error("No app variant defined in {}:\n{}", key.toString(), jsonMapping.toString());
                    continue;
                }
                if (CollectionUtils.isEmpty(jsonMapping.getGeoAreaIdsIncluded()) &&
                        CollectionUtils.isEmpty(jsonMapping.getGeoAreaIdsExcluded())) {
                    logSummary.error("No geo area defined in {}:\n{}", key.toString(), jsonMapping.toString());
                    continue;
                }

                for (String appVariantIdentifier : jsonMapping.getAppVariantIdentifiers()) {
                    final Optional<AppVariant> appVariantOpt =
                            appVariantRepository.findByAppVariantIdentifier(appVariantIdentifier);
                    if (appVariantOpt.isEmpty()) {
                        logSummary.error("Could not find app variant '{}' in {}:\n{}",
                                appVariantIdentifier, key.toString(), jsonMapping.toString());
                        continue;
                    }
                    final AppVariant appVariant = appVariantOpt.get();
                    final AppVariantTenantContract contract = AppVariantTenantContract.builder()
                            .appVariant(appVariant)
                            .tenant(key.getTenant())
                            .notes(jsonMapping.getNotes())
                            .build()
                            //since there is only one id in the mapping in the data init we need to generate an id based on the app variants
                            .withConstantId(checkOrLookupId(jsonMapping.getId()));
                    newContracts.add(contract);

                    final List<Pair<String, Boolean>> geoAreaIdAndExcludedList = Stream.concat(
                                    Optional.ofNullable(jsonMapping.getGeoAreaIdsIncluded())
                                            .stream()
                                            .flatMap(Collection::stream)
                                            .map(includedId -> Pair.of(includedId, false)),
                                    Optional.ofNullable(jsonMapping.getGeoAreaIdsExcluded())
                                            .stream()
                                            .flatMap(Collection::stream)
                                            .map(excludedId -> Pair.of(excludedId, true)))
                            .collect(Collectors.toList());
                    for (Pair<String, Boolean> geoAreaIdAndExcluded : geoAreaIdAndExcludedList) {
                        final Optional<GeoArea> geoAreaOpt =
                                geoAreaRepository.findById(geoAreaIdAndExcluded.getFirst());
                        if (geoAreaOpt.isEmpty()) {
                            logSummary.error("Could not find geo area '{}' in {}:\n{}",
                                    geoAreaIdAndExcluded.getFirst(), key.toString(), jsonMapping.toString());
                            continue;
                        }
                        final AppVariantGeoAreaMapping newMapping = AppVariantGeoAreaMapping.builder()
                                .geoArea(geoAreaOpt.get())
                                .excluded(geoAreaIdAndExcluded.getSecond())
                                .contract(contract)
                                .build()
                                .withConstantId();
                        //we can do this check if it is already contained because they are checked for equality by id
                        // and the id is generated based on all three values
                        if (!newMappings.add(newMapping)) {
                            logSummary.error("Duplicate mapping for geo area and app variant in {}:\n{}",
                                    key.toString(), jsonMapping.toString());
                        }
                    }
                }
            }
        }

        //use the existing contracts to save the created date
        final Map<String, AppVariantTenantContract> existingContractsById =
                appVariantTenantContractRepository.findAllById(
                                newContracts.stream()
                                        .map(AppVariantTenantContract::getId)
                                        .collect(Collectors.toSet())).stream()
                        .collect(Collectors.toMap(AppVariantTenantContract::getId, c -> c));
        logSummary.info("Found {} existing contracts, {} contracts are newly created",
                existingContractsById.size(), newContracts.size() - existingContractsById.size());
        for (AppVariantTenantContract newContract : newContracts) {
            final AppVariantTenantContract existingContract = existingContractsById.get(newContract.getId());
            if (existingContract != null) {
                //at this point also other attributes could be saved
                newContract.setAdditionalNotes(existingContract.getAdditionalNotes());
                newContract.setCreated(existingContract.getCreated());
            }
        }

        final Map<AppVariant, List<AppVariantGeoAreaMapping>> mappingsByAppVariant = newMappings.stream()
                .collect(Collectors.groupingBy(m -> m.getContract().getAppVariant()));
        for (Map.Entry<AppVariant, List<AppVariantGeoAreaMapping>> appVariantAndMapping : mappingsByAppVariant.entrySet()) {

            final List<AppVariantGeoAreaMapping> mappings = appVariantAndMapping.getValue();
            final String geoAreaMessage = mappings.stream()
                    .sorted(Comparator.comparing(AppVariantGeoAreaMapping::isExcluded)
                            .thenComparing(mapping -> mapping.getGeoArea().getName()))
                    .map(mapping -> (mapping.isExcluded() ? "- " : "+ ") + mapping.getGeoArea().toString())
                    .collect(Collectors.joining("\n"));
            logSummary.info("mapping {} geo areas to app variant '{}':\n{}",
                    mappings.size(),
                    appVariantAndMapping.getKey(),
                    geoAreaMessage);
        }
        return Pair.of(newContracts, newMappings);
    }

    protected void storeAppVariantGeoAreaMappings(Set<AppVariantTenantContract> contracts,
            Set<AppVariantGeoAreaMapping> mappings, LogSummary logSummary) {

        appVariantGeoAreaMappingRepository.saveAll(mappings);
        //we need to delete the old mappings for the contracts we just created / updated
        int deletedOldMappings = appVariantGeoAreaMappingRepository.deleteByContractInAndIdNotIn(
                contracts,
                BaseEntity.toInSafeIdSet(
                        mappings.stream()
                                .map(AppVariantGeoAreaMapping::getId)
                                .collect(Collectors.toSet())));
        appVariantGeoAreaMappingRepository.flush();
        logSummary.info("stored {} app variant geo area mappings and deleted {} old ones",
                mappings.size(), deletedOldMappings);
    }

    private void mapOauthClientsToAppVariants(LogSummary logSummary) {

        MultiValueMap<AppVariant, OauthClient> appVariantsToOauthClients =
                CollectionUtils.toMultiValueMap(new HashMap<>());
        final Map<DataInitKey, List<AppVariantOauthClientMapping>> mappingsRaw =
                loadAllDataInitKeysAndEntities(AppVariantOauthClientMapping.class);
        final Map<String, OauthClient> oauthClientByIdentifier = oauthClientRepository.findAll().stream()
                .collect(Collectors.toMap(OauthClient::getOauthClientIdentifier, Function.identity()));

        for (Map.Entry<DataInitKey, List<AppVariantOauthClientMapping>> keyAndMapping : mappingsRaw.entrySet()) {
            final DataInitKey key = keyAndMapping.getKey();
            final List<AppVariantOauthClientMapping> mappings = keyAndMapping.getValue();
            if (CollectionUtils.isEmpty(mappings)) {
                continue;
            }
            if (key.getAppVariant() != null) {
                final AppVariant appVariant = key.getAppVariant();
                for (AppVariantOauthClientMapping mapping : mappings) {
                    String oauthClientIdentifier = mapping.getOauthClientIdentifier();
                    OauthClient oauthClient = oauthClientByIdentifier.get(oauthClientIdentifier);
                    if (oauthClient == null) {
                        throw new DataInitializationException(
                                "Could not find oauth client '{}', check {}",
                                oauthClientIdentifier, key.getFileNames());
                    }
                    appVariantsToOauthClients.add(appVariant, oauthClient);
                }
            }
        }

        //store the mappings at the app variant
        for (Map.Entry<AppVariant, List<OauthClient>> appVariantToOauthClients : appVariantsToOauthClients.entrySet()) {
            final AppVariant appVariant = appVariantToOauthClients.getKey();
            Collection<OauthClient> oauthClients = appVariantToOauthClients.getValue();
            //no need to keep the ones that are already set (in DB), the ones that are configured via data init are the "real" ones
            appVariant.setOauthClients(new HashSet<>(oauthClients));
            appVariantRepository.save(appVariant);
            logSummary.info("added {} oauth clients to app variant '{}':\n{}",
                    oauthClients.size(),
                    appVariant.getAppVariantIdentifier(),
                    oauthClients.stream()
                            .map(OauthClient::toString)
                            .collect(Collectors.joining("\n")));
        }
        appVariantRepository.flush();
    }

}
