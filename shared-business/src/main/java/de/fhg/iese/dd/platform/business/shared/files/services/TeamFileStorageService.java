/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Danielle Korth, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.github.sardine.Sardine;
import com.github.sardine.impl.SardineException;
import com.github.sardine.impl.SardineImpl;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.TeamFileStorageConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TeamFileNotFoundException;

@Service
@Profile({"!test", "TeamFileStorageServiceTest"})
class TeamFileStorageService extends BaseService implements ITeamFileStorageService {

    /**
     * The number of retries before we give up, so overall we try it RETRY_COUNT + 1
     */
    private static final int RETRY_COUNT = 2;
    private static final long RETRY_SLEEP_MS = 100L;

    @Autowired
    private TeamFileStorageConfig config;
    private String basePath;
    //required for injecting the mock in tests
    Supplier<Sardine> sardineSupplier = SardineImpl::new;
    //required for resetting the mock in tests
    Sardine sardine = null;

    @FunctionalInterface
    protected interface SardineAction {

        void run(Sardine sardine) throws IOException;

    }

    //unfortunately we can not use the lombok mechanism for lazy getter, since this can not be reset easily in tests
    private Sardine getSardine() {
        if (sardine == null) {
            sardine = sardineSupplier.get();
            sardine.setCredentials(config.getUsername(), config.getPassword());
            sardine.enableCompression();
        }
        return sardine;
    }

    @PostConstruct
    private void initialize() {
        this.basePath = StringUtils.appendIfMissing(config.getWebDavUrl(), "/");
    }

    @Override
    public void saveFile(byte[] content, String mimeType, String fileName) throws FileStorageException {
        String url = toURL(fileName);
        try {
            try {
                retryOnInternalErrors(sardine -> sardine.put(url, content, mimeType));
                log.debug("Saved file {}", fileName);
            } catch (SardineException e) {
                if (e.getStatusCode() == 409) { //status code 409 seems to be the reply of owncloud in this case
                    //the filename contains a parent directory that is not yet existing
                    String parentPath = StringUtils.substringBeforeLast(fileName, "/");
                    createDirectory(parentPath);
                    retryOnInternalErrors(sardine -> sardine.put(url, content, mimeType));
                    log.info("Saved file {} after creating parent directory", fileName);
                } else {
                    throw e;
                }
            }
        } catch (IOException e) {
            throw newLoggedException(() -> new FileStorageException(
                    "Could not save file '" + fileName + "' (" + url + "): " + e.getMessage(), e));
        }
    }

    @Override
    public byte[] getFile(String fileName) throws FileStorageException, TeamFileNotFoundException {
        String url = toURL(fileName);
        try {
            try (InputStream in = getSardine().get(url)) {
                return IOUtils.toByteArray(in);
            } catch (SardineException e) {
                if (e.getStatusCode() == 404) {
                    throw new TeamFileNotFoundException("Could not find file '{}' ({})", fileName, url);
                } else {
                    throw e;
                }
            }
        } catch (IOException e) {
            throw new FileStorageException(
                    "Could not get file '" + fileName + "' (" + url + "): " + e.getMessage(), e);
        }
    }

    @Override
    public void createDirectory(String directoryNameRaw) throws FileStorageException {
        //not necessary for webDav, just for consistency
        String directoryName = StringUtils.removeEnd(directoryNameRaw, "/");
        if (StringUtils.isEmpty(directoryName)) {
            //root directory does not need to be created
            return;
        }
        String url = toURL(directoryName);
        try {
            try {
                retryOnInternalErrors(sardine -> sardine.createDirectory(url));
                log.info("Created directory {}", directoryName);
            } catch (SardineException e) {
                switch (e.getStatusCode()) {
                    case 405:
                        //the directory already exists, nothing to do
                        log.info("Created directory {} (already existing)", directoryName);
                        break;
                    case 409:
                        //we tried to create a hierarchy but some parent directories were not existing
                        //we recursively create the tree
                        String parentPath = StringUtils.substringBeforeLast(directoryName, "/");
                        createDirectory(parentPath);
                        //now we can create the actual directory
                        retryOnInternalErrors(sardine -> sardine.createDirectory(url));
                        log.info("Created directory {}", directoryName);
                        break;
                    default:
                        throw e;
                }
            }
        } catch (IOException e) {
            throw newLoggedException(() -> new FileStorageException(
                    "Could not create directory '" + directoryName + "' (" + url + "): " + e.getMessage(), e));
        }
    }

    @Override
    public boolean fileExists(String fileName) throws FileStorageException {
        String url = toURL(fileName);
        try {
            //this has no retry since it seems to not cause too many issues and it would be too much effort ;-)
            return getSardine().exists(url);
        } catch (IOException e) {
            throw newLoggedException(() -> new FileStorageException(
                    "Could not check existence of file '" + fileName + "' (" + url + "): " + e.getMessage(), e));
        }
    }

    @Override
    public void delete(String fileName) throws FileStorageException {
        String url = toURL(fileName);
        try {
            try {
                retryOnInternalErrors(sardine -> sardine.delete(url));
                log.info("Deleted file {}", fileName);
            } catch (SardineException e) {
                if (e.getStatusCode() == 404) {
                    log.info("Deleted file {} (not existing)", fileName);
                } else {
                    throw e;
                }
            }
        } catch (IOException e) {
            throw newLoggedException(() -> new FileStorageException(
                    "Could not delete file '" + fileName + "' (" + url + "): " + e.getMessage(), e));
        }
    }

    private void retryOnInternalErrors(SardineAction action) throws IOException {

        //we retry the action whenever an error occurs that might be due to internal errors of the connected storage
        for (int i = 1; i <= RETRY_COUNT; i++) {
            try {
                action.run(getSardine());
                return;
            } catch (SardineException e) {
                switch (e.getStatusCode()) {
                    case 500: //classic internal error
                    case 403: //it seems to be that owncloud randomly locks files or folders (option 1)
                    case 423: //it seems to be that owncloud randomly locks files or folders (option 2)
                        log.debug("Action failed {}/{} times with status {}, waiting for {} ms before retry",
                                i, RETRY_COUNT + 1, e.getStatusCode(), RETRY_SLEEP_MS);
                        try {
                            Thread.sleep(RETRY_SLEEP_MS);
                        } catch (InterruptedException e2) {
                            Thread.currentThread().interrupt();
                        }
                        break;
                    default:
                        throw e;
                }
            }
        }
        //this is the last try, where we do not catch anything and do not wait afterwards
        action.run(getSardine());
    }

    private FileStorageException newLoggedException(Supplier<FileStorageException> exceptionSupplier)
            throws FileStorageException {
        final FileStorageException exception = exceptionSupplier.get();
        log.error(exception.getMessage());
        return exception;
    }

    private String toURL(String fileName) {
        return basePath + StringUtils.removeStart(fileName, "/");
    }

}
