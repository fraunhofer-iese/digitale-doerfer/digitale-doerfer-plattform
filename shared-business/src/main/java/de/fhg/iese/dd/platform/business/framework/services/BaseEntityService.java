/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.services;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BaseEntityService<E extends BaseEntity> extends BaseService implements IEntityService<E> {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JpaRepository<E, String> entityRepository;

    @Override
    public E store(E entity) {
        return entityRepository.saveAndFlush(entity);
    }

    @Override
    public E refresh(E entity) {
        if (entity == null) {
            return null;
        }
        return entityRepository.findById(entity.getId()).orElse(null);
    }

    @Override
    public E refreshProxy(E entity) {
        if (entity instanceof HibernateProxy) {
            return entityRepository.findById(entity.getId()).orElse(null);
        } else {
            return entity;
        }
    }

    /**
     * Find out what ids are not contained in the ids of the given entities.
     *
     * @param entities the entities to check
     * @param ids      the ids to check
     *
     * @return all ids that are not contained in the ids of the given entities
     */
    public static <T extends BaseEntity> Collection<String> getMissingIds(final Collection<T> entities,
            final Collection<String> ids) {
        final Set<String> foundEntityIds = entities.stream()
                .map(BaseEntity::getId)
                .collect(Collectors.toSet());
        return ids.stream()
                .filter(id -> !foundEntityIds.contains(id))
                .collect(Collectors.toList());
    }

    protected static <E> Page<E> toPage(List<E> elements, Pageable pageable) {

        int fromIndex = Math.min((int) pageable.getOffset(), elements.size());
        int toIndex = Math.min(fromIndex + pageable.getPageSize(), elements.size());
        List<E> pagedElements = elements.subList(fromIndex, toIndex);
        return new PageImpl<>(pagedElements, pageable, elements.size());
    }

}
