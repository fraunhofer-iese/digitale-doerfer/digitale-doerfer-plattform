/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.services;

import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyWorkflowAlreadyFinishedException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.exceptions.DataPrivacyWorkflowResponseRejectedException;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;

public interface IExternalDataPrivacyService extends IService {

    /**
     * Adds a response from an external system to a data collection. The response is checked for validity first. The
     * data privacy data collection has to be in the right state and the mime type and ending of the file needs to be
     * allowed.
     *
     * @param dataPrivacyWorkflowAppVariant the data privacy workflow app variant that send the response
     * @param status                        the new status of the data collection app variant
     * @param statusMessage                 an internal status message that is used for logging and monitoring
     * @param file                          the file that is part of the response, can be null
     * @param description                   the description of the response, can be null
     *
     * @throws DataPrivacyWorkflowAlreadyFinishedException  if the data privacy data collection is not in the state for
     *                                                      getting new responses
     * @throws DataPrivacyWorkflowResponseRejectedException if the response is not valid (because mime type and file
     *                                                      ending are invalid)
     */
    void addDataCollectionExternalSystemResponse(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus status, String statusMessage, @Nullable MultipartFile file,
            @Nullable String description)
            throws DataPrivacyWorkflowAlreadyFinishedException, DataPrivacyWorkflowResponseRejectedException;

    /**
     * Adds a response from an external system to a data deletion. The data privacy data deletion has to be in the right
     * state.
     *
     * @param dataPrivacyWorkflowAppVariant the data privacy workflow app variant that send the response
     * @param status                        the new status of the data collection app variant
     * @param statusMessage                 an internal status message that is used for logging and monitoring
     *
     * @throws DataPrivacyWorkflowAlreadyFinishedException if the data privacy data collection is not in the state for
     *                                                     getting new responses
     */
    void addDataDeletionExternalSystemResponse(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant,
            DataPrivacyWorkflowAppVariantStatus status, String statusMessage);

    /**
     * Adds the responses from external systems to the report.
     *
     * @param dataCollection    the data collection whose responses should be added
     * @param dataPrivacyReport the report to be extended
     */
    void addExternalDataCollectionResponsesToReport(DataPrivacyDataCollection dataCollection,
            IDataPrivacyReport dataPrivacyReport);

    /**
     * Adds the responses from external systems to the report.
     *
     * @param dataDeletion         the data deletion whose responses should be added
     * @param personDeletionReport the report to be extended
     */
    void addExternalDataDeletionResponsesToReport(DataPrivacyDataDeletion dataDeletion,
            IPersonDeletionReport personDeletionReport);

    /**
     * Process the data privacy workflow of the external app variant.
     *
     * @param dataPrivacyWorkflowAppVariant the data privacy app variant to be processed
     *
     * @return {@link DataPrivacyWorkflowStatus#WAITING_FOR_EXTERNALS} if this data privacy workflow of the external app
     *         variant is still waiting for responses
     *         <p/>
     *         {@link DataPrivacyWorkflowStatus#EXTERNALS_FINISHED} if this data privacy workflow of the external app
     *         variant is finished
     */
    DataPrivacyWorkflowStatus processExternalDataPrivacyWorkflowAppVariant(
            DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant);

    /**
     * Cancel the data privacy workflow of the external app variant.
     *
     * @param dataPrivacyWorkflowAppVariant the data privacy app variant to be canceled
     */
    void cancelExternalDataPrivacyWorkflowAppVariant(DataPrivacyWorkflowAppVariant dataPrivacyWorkflowAppVariant);

}
