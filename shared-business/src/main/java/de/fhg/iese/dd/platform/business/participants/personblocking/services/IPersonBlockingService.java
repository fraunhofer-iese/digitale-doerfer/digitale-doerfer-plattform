/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Johannes Schneider, Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.personblocking.services;

import java.util.Collection;
import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

public interface IPersonBlockingService {

    /**
     * Blocks a person in an app
     *
     * @param app            App in which the person is to be blocked
     * @param blockingPerson Person who wants to block the other person
     * @param blockedPerson  Person to be blocked
     */
    void blockPerson(App app, Person blockingPerson, Person blockedPerson);

    /**
     * Unblocks a person in an app
     *
     * @param app            App in which the person is to be unblocked
     * @param blockingPerson Person who is blocking the person
     * @param blockedPerson  Person which is blocked
     */
    void unblockPerson(App app, Person blockingPerson, Person blockedPerson);

    /**
     * Get a list of blocked persons for a person in an app
     *
     * @param app            App for which the blocked persons are to be returned
     * @param blockingPerson Person for whom the blocked persons are to be returned
     * @return All blocked persons by the blocking person
     */
    Collection<Person> getBlockedPersons(App app, Person blockingPerson);

    /**
     * Queries if blockingPerson blocks blockedPerson
     *
     * @param app            App for which the blocked persons are to be returned
     * @param blockingPerson
     * @param blockedPerson
     * @return
     */
    boolean existsBlocking(App app, Person blockingPerson, Person blockedPerson);

    /**
     * Get a list of blocked persons (only not deleted accounts) for a person in an app
     *
     * @param app            App for which the blocked persons are to be returned
     * @param blockingPerson Person for whom the blocked persons are to be returned
     * @return Blocked persons by the blocking person
     */
    Collection<Person> getBlockedNotDeletedPersons(App app, Person blockingPerson);

    /**
     * Get a list of the ids of blocked persons (only not deleted accounts) for a person in an app
     *
     * @param app            App for which the blocked persons are to be returned
     * @param blockingPerson Person for whom the blocked persons are to be returned
     * @return
     */
    Set<String> getBlockedNotDeletedPersonIds(App app, Person blockingPerson);

    /**
     * Get a list of all apps where the blocking person has blocked other persons
     *
     * @param blockingPerson Person for whom the apps are to be returned
     * @return All apps where persons are blocked
     */
    Collection<App> getBlockingApps(Person blockingPerson);

    /**
     * Get all blockings where the blocking person has blocked other persons
     *
     * @param blockingPerson Person for whom the blockings are to be returned
     *
     * @return All blockings where the person blocks
     */
    Collection<PersonBlocking> getBlockingsOfPerson(Person blockingPerson);

}
