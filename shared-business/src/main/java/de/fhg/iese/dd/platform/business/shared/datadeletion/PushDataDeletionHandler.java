/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.datadeletion;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushRegistrationService;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

@Component
public class PushDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private IPushRegistrationService pushRegistrationService;

    @Override
    protected Collection<DataDeletionStrategy.SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return Collections.emptyList();
    }

    @Override
    protected Collection<DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return unmodifiableList(
                DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy.forTriggeringEntity(AppVariant.class)
                        .referencedEntity(AppVariant.class)
                        .checkImpact(pushRegistrationService::logHumanReadablePushStatisticsForAppVariant)
                        .deleteData(pushRegistrationService::deletePushForAppVariant)
                        .build()
        );
    }

}
