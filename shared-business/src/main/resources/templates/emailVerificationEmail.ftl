<#include "/mailHeaderShared.ftl">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
       style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                   style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:100%; min-width:100%;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding: 0px 18px 9px; line-height: 125%;">
                        <h1 class="null" style="text-align: center;">
                            <span style="font-family:lato,helvetica neue,helvetica,arial,sans-serif">
                            <span style="color:#696969; font-size:12px">E-Mail-Verifizierung</span>
                                <br/>
                            <strong>
                                <span style="color:#DE3B5D">Bestätige deine E-Mail-Adresse</span>
                            </strong>
                            </span>
                        </h1>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
       style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 27px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
                   width="100%"
                   style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--
                            <td class="mcnDividerBlockInner" style="padding: 18px;">
                            <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
            -->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
       style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                   style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:100%; min-width:100%;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <h2 class="null" style="text-align: left;">Willkommen an Bord, ${receiverName}!</h2>

                        <p style="text-align: left;">Vielen Dank für deine Anmeldung. Wir möchten sicherstellen, dass wichtige Informationen zu deinem Konto bei dir ankommen.</p>
                        <p style="text-align: left;">Bitte bestätige deine E-Mail-Adresse, indem du auf den folgenden Link klickst:</p>

                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
       style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 0px 18px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
                   width="100%" style="min-width:100%;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--
                            <td class="mcnDividerBlockInner" style="padding: 18px;">
                            <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
            -->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock"
       style="min-width:100%;">
    <tbody class="mcnButtonBlockOuter">
    <tr>
        <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;"
            valign="top" align="left" class="mcnButtonBlockInner">
            <table border="0" cellpadding="0" cellspacing="0"
                   class="mcnButtonContentContainer"
                   style="border-collapse: separate !important;border-top-left-radius: 3px;border-top-right-radius: 3px;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;background-color: #DE3B5D;">
                <tbody>
                <tr>
                    <td align="center" valign="middle" class="mcnButtonContent"
                        style="font-family: Lato, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; padding: 15px;">
                        <a class="mcnButton " title="E-Mail-Adresse bestätigen"
                           href="${verifyEmailLink}"
                           target="_blank"
                           style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">E-Mail-Adresse bestätigen</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                <!--[if mso]>
<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
<tr>
<![endif]-->

                <!--[if mso]>
<td valign="top" width="600" style="width:600px;">
<![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tbody>
                        <tr>

                            <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                <p style="text-align: left;">
                                    Viele Grüße<br>Dein Digitale-Dörfer-Team </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--[if mso]>
</td>
<![endif]-->

                <!--[if mso]>
</tr>
</table>
<![endif]-->
            </td>
        </tr>
    </tbody>
</table>
<#include "/mailFooterShared.ftl">