<#ftl output_format="HTML">
<#-- @ftlvariable name="" type="de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport" -->
<#-- @ftlvariable name="content.content" type="de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem" -->
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>${title}</title>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            margin-right: 0px;
        }
        h1 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }
        h2 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }
        h3 {
            color: black;
            margin-top: 10px;
            margin-bottom: 10px;
        }
        h4 {
            color: black;
            margin-top: 10px;
            margin-bottom: 0px;
        }
        p {
            color: #222;
            margin-top: 0;
            margin-bottom: 5px;
        }
        a {
            color: #dd3c5d;
        }
        .level-top {
            margin-left: 0px;
        }
        .level-sub-1 {
            margin-left: 8px;
        }
        .level-sub-2 {
            margin-left: 16px;
        }
        .level-sub-3 {
            margin-left: 32px;
        }
        .level-sub-4 {
            margin-left: 48px;
        }
        i.icon {
            vertical-align: middle;
            margin-right: 5px;
        }
        i.icon-location::before {
            content: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 49 72' width='14' height='20' style=''%3E%3Cg class='currentLayer' style=''%3E%3Ctitle%3ELayer 1%3C/title%3E%3Cpath fill='%23000000' fill-opacity='1' stroke='%23000000' stroke-opacity='1' stroke-width='2' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' d='M0.998870849609375,23.99999237060547 C0.998870849609375,11.292808532714844 11.51544189453125,0.9999923706054688 24.498870849609375,0.9999923706054688 C37.4822998046875,0.9999923706054688 47.998870849609375,11.292808532714844 47.998870849609375,23.99999237060547 C47.998870849609375,36.707176208496094 37.4822998046875,46.99999237060547 24.498870849609375,46.99999237060547 C11.51544189453125,46.99999237060547 0.998870849609375,36.707176208496094 0.998870849609375,23.99999237060547 z' id='svg_1' class=''/%3E%3Cpath fill='%23000000' fill-opacity='1' stroke='%23000000' stroke-opacity='1' style='color: rgb(0, 0, 0);' stroke-width='2' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' id='svg_4' d='M169.17788696289062,129.9573516845703 ' class=''/%3E%3Cpath fill='%23000000' fill-opacity='1' stroke='%23000000' stroke-opacity='1' style='color: rgb(0, 0, 0);' stroke-width='2' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' id='svg_6' d='M168.70394897460938,124.74407958984375 ' class=''/%3E%3Cpath fill='%23000000' fill-opacity='1' stroke='%23000000' stroke-opacity='1' style='color: rgb(0, 0, 0);' stroke-width='2' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' id='svg_8' d='M149.27267456054688,216.6872100830078 ' class=''/%3E%3Cpath fill='%23000000' fill-opacity='1' stroke='%23000000' stroke-opacity='1' style='color: rgb(0, 0, 0);' stroke-width='1' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' id='svg_2' d='M3.5538482666015625,71.13178253173828 L24.484664916992188,35.338417053222656 L45.415496826171875,71.13178253173828 z' class='' transform='rotate(-180, 24.4854, 53.2347)'/%3E%3Cpath fill='%23ffffff' fill-opacity='1' stroke='%23ffffff' stroke-opacity='1' stroke-width='1' stroke-dasharray='none' stroke-linejoin='round' stroke-linecap='butt' stroke-dashoffset='' fill-rule='nonzero' opacity='1' marker-start='' marker-mid='' marker-end='' d='M12.041015625,24.14154815673828 C12.041015625,17.184890747070312 17.6759033203125,11.549995422363281 24.632568359375,11.549995422363281 C31.5892333984375,11.549995422363281 37.22410583496094,17.184890747070312 37.22410583496094,24.14154815673828 C37.22410583496094,31.09819793701172 31.5892333984375,36.73308563232422 24.632568359375,36.73308563232422 C17.6759033203125,36.73308563232422 12.041015625,31.09819793701172 12.041015625,24.14154815673828 z' id='svg_13' class=''/%3E%3C/g%3E%3C/svg%3E");
        }
    </style>
</head>
<body>
<h1 class="level-top">${title}</h1>
<#list sections as section>
    <h2 class="level-sub-1">${section.name}: ${section.description}</h2>
    <#if section.subSections?size == 0>
        <p class="level-sub-2">In dieser Kategorie sind keine Daten über dich gespeichert.</p>
    </#if>
    <#list section.subSections as subSection>
        <h3 class="level-sub-2">${subSection.name!"-"}: ${subSection.description}</h3>
        <#list subSection.contents as content>
            <h4 class="level-sub-3">${content.name}<#if content.description?? >: ${content.description}</#if></h4>
            <#if content.contentClass = "String" >
                <#list content.content?split('\\r\\n|\\n|\\r','r') as contentLine>
                    <p class="level-sub-4">${contentLine}</p>
                </#list>
            <#elseif content.contentClass = "MediaItem" >
                <#assign urls = content.content.urls />
                <p class="level-sub-4">
                    <a href="${urls.original}"><img src="${urls.thumbnail!urls.original}"><br/>${content.content.id}</a>
                </p>
            <#elseif content.contentClass = "DocumentItem" >
                <#assign documentItem = content.content />
                <p class="level-sub-4">
                    <a href="${documentItem.url}">${documentItem.title}<br/>
                        ${documentItem.mediaType}<br/>
                        ${content.content.id}</a>
                </p>
            <#elseif content.contentClass = "GPSLocation" >
                <#assign location = content.content />
                <p class="level-sub-4">
                <p class="level-sub-4"><i class="icon icon-location"></i> ${location.latitude?string["0.######"]}
                    , ${location.longitude?string["0.######"]}</p>
                </p>
            <#elseif content.contentClass = "Signature" >
                <#assign signature = content.content />
                <p class="level-sub-4">
                <p class="level-sub-4">
                    <#noautoesc>${signature.signature}</#noautoesc>
                </p>
                </p>
            </#if>
        </#list>
    </#list>
</#list>
</body>
</html>