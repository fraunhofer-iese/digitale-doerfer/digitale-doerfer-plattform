<#include "/mailHeaderShared.ftl">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
       style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                   style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:100%; min-width:100%;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding: 0px 18px 9px; line-height: 125%;">

                        <h1 class="null" style="text-align: center;"><span
                                style="font-family:lato,helvetica neue,helvetica,arial,sans-serif"><span
                                style="color:#696969"><span style="font-size:12px">Hinweis von den Digitalen Dörfern</span></span><br>
<strong><span style="color:#DE3B5D">Dein Konto wurde gelöscht</span></strong></span></h1>

                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
       style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 27px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
                   width="100%"
                   style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
       style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%"
                   style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
                   style="max-width:100%; min-width:100%;" width="100%"
                   class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <h2 class="null" style="text-align: left;">Hallo ${receiverName}
                            !</h2>
                        <p style="text-align: left;">
                            Wir haben deine Anfrage bearbeitet und dein Konto gelöscht.</p>
                        <p style="text-align: left;">
                            Schade, dass du uns verlässt. Solltest du es dir irgendwann
                            anders überlegen, heißen wir dich jederzeit wieder herzlich
                            willkommen!</p>
                        <p style="text-align: left;">
                            Viele Grüße<br>Dein Digitale-Dörfer-Team </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<#include "/mailFooterShared.ftl">