<#include "/mailHeaderShared.ftl">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;">

                        <h1 class="null" style="text-align: center;">
                            <span style="font-family:lato,helvetica neue,helvetica,arial,sans-serif">
                                <span style="color:#696969">
                                    <span style="font-size:12px">Wichtiger Hinweis aus ${appName}</span>
                                </span>
                                <br>
                                <strong>
                                    <span style="color:#DE3B5D">
                                        Ein Beitrag von dir wurde entfernt
                                    </span>
                                </strong>
                            </span>
                        </h1>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 27px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <h2 class="null" style="margin-bottom: 25px; text-align: left;">Hallo ${receiverName}!</h2>
                        <p style="text-align: left;">Ein Beitrag von dir wurde uns als unangemessen gemeldet.</p>
                        <p style="text-align: left; margin-bottom: 0px">
                            Wir haben den Inhalt überprüft und einen Verstoß gegen unsere Richtlinien festgestellt.
                            Deshalb haben wir den folgenden Beitrag entfernt:
                        </p>
                        <table align="center" border="0" cellpadding="10%" cellspacing="0"
                               style="max-width:75%; min-width:75%; margin: 10px 0 0 0;" width="100%"
                               class="mcnTextContentContainer">
                            <tr>
                                <td style="vertical-align: top; font-size: 60px; padding-top: 24px; color: #66BFAC; padding: 25px 0; width: 5%;">
                                    “
                                </td>
                                <td style="font-style:italic; vertical-align: text-top; padding: 12px 10px 10px 5px; color: #696969;">
                                    ${deletedFlaggedPost}
                                </td>
                            </tr>
                        </table>
                        <p style="text-align: left; margin-bottom: 0px">
                            Mit ${appName} möchten wir dich mit deiner Gemeinde näher zusammenbringen. Das klappt nur,
                            wenn jeder ein wenig Rücksicht nimmt und auf Spam, unangemessene und beleidigende Inhalte
                            verzichtet.
                        </p>
                        <p style="text-align: left; margin-bottom: 0px">
                            Bitte hilf uns dabei und beachte in Zukunft <a
                                    href="https://www.digitale-doerfer.de/nutzungsbedingungen-dorffunk/#richtlinien">unsere
                                Richtlinien</a>.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <p style="text-align: left;">
                            Viele Grüße<br>Dein Digitale-Dörfer-Team </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<#include "/mailFooterShared.ftl">
