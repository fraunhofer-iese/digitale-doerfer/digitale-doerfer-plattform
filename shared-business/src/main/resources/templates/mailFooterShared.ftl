<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%"
                       style="min-width:100%;">
                    <tbody>
                    <tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--
                                <td class="mcnDividerBlockInner" style="padding: 18px;">
                                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                -->
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <p><strong>Fragen oder Probleme? Wir helfen dir gerne!</strong><br>Schreib an&nbsp;<a
                                href="mailto:support@digitale-doerfer.de"
                                target="_blank">support@digitale-doerfer.de</a>&nbsp;oder benutze unser <a
                                href="https://digitale-doerfer.de/support/">Kontaktformular</a>.</p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--
                            <td class="mcnDividerBlockInner" style="padding: 18px;">
                            <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
            -->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width:100%;">
    <tbody class="mcnFollowBlockOuter">
    <tr>
        <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer"
                   style="min-width:100%;">
                <tbody>
                <tr>
                    <td align="center" style="padding-left:9px;padding-right:9px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;"
                               class="mcnFollowContent">
                            <tbody>
                            <tr>
                                <td align="center" valign="top"
                                    style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                        <tr>
                                            <td align="center" valign="top">
                                                <!--[if mso]>
                                                <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                       style="display:inline;">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding-right:10px; padding-bottom:9px;"
                                                            class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%"
                                                                   class="mcnFollowContentItem">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle"
                                                                        style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                        <table align="left" border="0" cellpadding="0"
                                                                               cellspacing="0" width="">
                                                                            <tbody>
                                                                            <tr>

                                                                                <td align="center" valign="middle"
                                                                                    width="24"
                                                                                    class="mcnFollowIconContent">
                                                                                    <a href="mailto:support@digitale-doerfer.de"
                                                                                       target="_blank"><img
                                                                                                src="${mailImageLocation}icon-mail.png"
                                                                                                style="display:block;"
                                                                                                height="24" width="24"
                                                                                                class=""></a>
                                                                                </td>


                                                                                <td align="left" valign="middle"
                                                                                    class="mcnFollowTextContent"
                                                                                    style="padding-left:5px;">
                                                                                    <a href="mailto:support@digitale-doerfer.de"
                                                                                       target=""
                                                                                       style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Email</a>
                                                                                </td>

                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                       style="display:inline;">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding-right:10px; padding-bottom:9px;"
                                                            class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%"
                                                                   class="mcnFollowContentItem">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle"
                                                                        style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                        <table align="left" border="0" cellpadding="0"
                                                                               cellspacing="0" width="">
                                                                            <tbody>
                                                                            <tr>

                                                                                <td align="center" valign="middle"
                                                                                    width="24"
                                                                                    class="mcnFollowIconContent">
                                                                                    <a href="https://www.digitale-doerfer.de"
                                                                                       target="_blank"><img
                                                                                                src="${mailImageLocation}icon-link.png"
                                                                                                style="display:block;"
                                                                                                height="24"
                                                                                                width="24" class=""></a>
                                                                                </td>


                                                                                <td align="left" valign="middle"
                                                                                    class="mcnFollowTextContent"
                                                                                    style="padding-left:5px;">
                                                                                    <a href="https://www.digitale-doerfer.de"
                                                                                       target=""
                                                                                       style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Website</a>
                                                                                </td>

                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                       style="display:inline;">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding-right:10px; padding-bottom:9px;"
                                                            class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%"
                                                                   class="mcnFollowContentItem">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle"
                                                                        style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                        <table align="left" border="0" cellpadding="0"
                                                                               cellspacing="0" width="">
                                                                            <tbody>
                                                                            <tr>

                                                                                <td align="center" valign="middle"
                                                                                    width="24"
                                                                                    class="mcnFollowIconContent">
                                                                                    <a href="https://facebook.com/DigitaleDoerfer/"
                                                                                       target="_blank"><img
                                                                                                src="${mailImageLocation}icon-fb.png"
                                                                                                style="display:block;"
                                                                                                height="24"
                                                                                                width="24" class=""></a>
                                                                                </td>


                                                                                <td align="left" valign="middle"
                                                                                    class="mcnFollowTextContent"
                                                                                    style="padding-left:5px;">
                                                                                    <a href="https://facebook.com/DigitaleDoerfer/"
                                                                                       target=""
                                                                                       style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Facebook</a>
                                                                                </td>

                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                <td align="center" valign="top">
                                                <![endif]-->


                                                <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                       style="display:inline;">
                                                    <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding-right:0; padding-bottom:9px;"
                                                            class="mcnFollowContentItemContainer">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%"
                                                                   class="mcnFollowContentItem">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" valign="middle"
                                                                        style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                        <table align="left" border="0" cellpadding="0"
                                                                               cellspacing="0" width="">
                                                                            <tbody>
                                                                            <tr>

                                                                                <td align="center" valign="middle"
                                                                                    width="24"
                                                                                    class="mcnFollowIconContent">
                                                                                    <a href="https://twitter.com/digitaledoerfer"
                                                                                       target="_blank"><img
                                                                                                src="${mailImageLocation}icon-tw.png"
                                                                                                style="display:block;"
                                                                                                height="24"
                                                                                                width="24" class=""></a>
                                                                                </td>


                                                                                <td align="left" valign="middle"
                                                                                    class="mcnFollowTextContent"
                                                                                    style="padding-left:5px;">
                                                                                    <a href="https://twitter.com/digitaledoerfer"
                                                                                       target=""
                                                                                       style="font-family: Helvetica;font-size: 12px;text-decoration: none;color: #656565;">Twitter</a>
                                                                                </td>

                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                                <!--[if mso]>
                                                </td>
                                                <![endif]-->

                                                <!--[if mso]>
                                                </tr>
                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock" style="min-width:100%;">
    <tbody class="mcnImageBlockOuter">
    <tr>
        <td valign="top" style="padding:0px" class="mcnImageBlockInner">
            <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0" class="mcnImageContentContainer"
                   style="min-width:100%;">
                <tbody>
                <tr>
                    <td class="mcnImageContent" valign="top"
                        style="padding-right: 0px; padding-left: 0px; padding-top: 0; padding-bottom: 0; text-align:center;">


                        <img align="center" alt="" src="${mailImageLocation}footer-large.png" width="600"
                             style="max-width:1070px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"
                             class="mcnImage">


                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table></td>
</tr>
<tr>
    <td valign="top" id="templateFooter">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
            <tbody class="mcnTextBlockOuter">
            <tr>
                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                    <!--[if mso]>
                    <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                        <tr>
                    <![endif]-->

                    <!--[if mso]>
                    <td valign="top" width="600" style="width:600px;">
                    <![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                           style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                        <tbody>
                        <tr>

                            <td valign="top" class="mcnTextContent"
                                style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
                                <span style="font-size:14px"><strong>Über uns</strong></span><br>
                                Fraunhofer-Institut für Experimentelles Software Engineering<br>
                                Fraunhofer-Platz 1,&nbsp;67663&nbsp;Kaiserslautern&nbsp;<br>
                                <a href="mailto:support@digitale-doerfer.de"
                                   target="_blank">support@digitale-doerfer.de</a><br>
                                <br>
                                <a href="https://www.digitale-doerfer.de/datenschutzinformation/" target="_blank">Datenschutzinformation</a><br>
                                <a href="https://www.digitale-doerfer.de/impressum/" target="_blank">Impressum</a><br>
                                <br>
                                <br>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!--[if mso]>
                    </td>
                    <![endif]-->

                    <!--[if mso]>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
</table>
<!--[if gte mso 9]>
</td>
</tr>
</table>
<![endif]-->
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
