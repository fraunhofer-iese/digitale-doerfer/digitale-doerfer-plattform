<#ftl output_format="plainText">
<#-- @ftlvariable name="" type="de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport" -->
<#-- @ftlvariable name="content.content" type="de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem" -->
### ${title} ###
<#list sections as section>
- ${section.name}: ${section.description}
    <#if section.subSections?size == 0>
    + In dieser Kategorie sind keine Daten über dich gespeichert.
    </#if>
    <#list section.subSections as subSection>
        + ${subSection.name!"-"}: ${subSection.description}
        <#list subSection.contents as content>
        ${content.name}<#if content.description?? >: ${content.description}</#if>
        <#if content.contentClass = "String" >
            <#list content.content?split('\\r\\n|\\n|\\r','r') as contentLine>
                ${contentLine}
            </#list>
        <#elseif content.contentClass = "MediaItem" >
            <#assign urls = content.content.urls />
            ${content.content.id} :  ${urls.original}
        <#elseif content.contentClass = "DocumentItem" >
            ${content.content.id} :  ${content.content.url}
        <#elseif content.contentClass = "GPSLocation" >
            <#assign location = content.content />
            📍 ${location.latitude}, ${location.longitude}
        </#if>
        </#list>
    </#list>
</#list>