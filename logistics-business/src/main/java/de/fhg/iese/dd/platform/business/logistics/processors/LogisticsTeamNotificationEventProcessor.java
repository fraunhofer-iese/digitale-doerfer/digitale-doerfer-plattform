/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2018 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class LogisticsTeamNotificationEventProcessor  extends BaseEventProcessor {

    private static final String TEAM_NOTIFICATION_TOPIC = "logistics";

    @Autowired
    private ITeamNotificationService teamNotificationService;

    /**
     * Parcel ready for pickup
     */
    @EventProcessing
    private void handleDeliveryReadyForTransportConfirmation(DeliveryReadyForTransportConfirmation event) {
        try {
            Delivery delivery = event.getDelivery();
    
            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFullName();
            String pickupAddressName = delivery.getPickupAddress().getAddress().getName();
    
            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "Parcel for '{}' is ready for pick up at '{}'.",
                    receiverName,
                    pickupAddressName);
    
        }catch(Exception ex){
            log.error("Failed to notify to team: handleDeliveryReadyForTransportConfirmation", ex);
        }
    }

    /**
     * Carrier opted in for delivery
     */
    @EventProcessing
    private void handleTransportAssignmentCreatedEvent(TransportAssignmentCreatedEvent event) {
        try{
            TransportAssignment transportAssignment = event.getTransportAssignment();
            Person carrier = transportAssignment.getCarrier();
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String carrierName = carrier.getFirstName() + " " + carrier.getLastName();
            String pickupAddressName = transportAssignment.getPickupAddress().getAddress().getName();
            String deliveryAddressName = transportAssignment.getDeliveryAddress().getAddress().getName();

            String deliveryDetails;
            if(receiverName.equals(deliveryAddressName)){
                deliveryDetails = "from " + pickupAddressName + " to " + receiverName;
            }else{
                deliveryDetails = "for " + receiverName + " from " + pickupAddressName + " to " + deliveryAddressName;
            }

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' volunteered to deliver a parcel {}.",
                    carrierName,
                    deliveryDetails);
            
        }catch(Exception ex){
            log.error("Failed to notify to team: handleTransportAssignmentCreatedEvent", ex);
        }
    }

    /**
     * Carrier opted out for delivery
     */
    @EventProcessing
    private void handleTransportAssignmentCancelConfirmation(TransportAssignmentCancelConfirmation event) {
        try{
            TransportAssignment transportAssignment = event.getTransportAssignment();
            Person carrier = transportAssignment.getCarrier();
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String carrierName = carrier.getFirstName() + " " + carrier.getLastName();
            String pickupAddressName = transportAssignment.getPickupAddress().getAddress().getName();

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' no longer wants to deliver a parcel for '{}' from '{}'.",
                    carrierName,
                    receiverName,
                    pickupAddressName);
        }catch(Exception ex){
            log.error("Failed to notify to team: handleTransportAssignmentCancelConfirmation", ex);
        }
    }

    /**
     * Someone picked up a parcel
     */
    @EventProcessing
    private void handleTransportPickupConfirmation(TransportPickupConfirmation event) {
        try{
            TransportAssignment transportAssignment = event.getTransportAssignment();
            Person carrier = event.getCarrier();
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String carrierName = carrier.getFirstName() + " " + carrier.getLastName();
            String pickupAddressName = transportAssignment.getPickupAddress().getAddress().getName();

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' picked up a parcel for '{}' from '{}'.",
                    carrierName,
                    receiverName,
                    pickupAddressName);
        }catch(Exception ex){
            log.error("Failed to notify to team: handleTransportPickupConfirmation", ex);
        }
    }

    /**
     * Parcel delivered to receiver
     */
    @EventProcessing
    private void handleTransportDeliveredConfirmation(TransportDeliveredConfirmation event) {
        try{
            TransportAssignment transportAssignment = event.getTransportAssignment();
            Person carrier = transportAssignment.getCarrier();
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String carrierName = carrier.getFirstName() + " " + carrier.getLastName();
            String deliveryAddressName = transportAssignment.getDeliveryAddress().getAddress().getName();

            String deliveryDetails;
            if(receiverName.equals(deliveryAddressName)){
                deliveryDetails = "to " + deliveryAddressName;
            }else{
                deliveryDetails = "for " + receiverName + " to " + deliveryAddressName;
            }

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' delivered a parcel '{}'.",
                    carrierName,
                    deliveryDetails);
        }catch(Exception ex){
            log.error("Failed to notify to team: handleTransportDeliveredConfirmation", ex);
        }
    }

    /**
     * Receiver received parcel
     */
    @EventProcessing
    private void handleDeliveryReceivedConfirmation(DeliveryReceivedConfirmation event) {
        try{
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String pickupAddressName = delivery.getPickupAddress().getAddress().getName();

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' marked parcel from '{}' as received.",
                    receiverName,
                    pickupAddressName);
        }catch(Exception ex){
            log.error("Failed to notify to team: handleDeliveryReceivedConfirmation", ex);
        }
    }

    /**
     * Receiver took parcel out of poolingstation
     */
    @EventProcessing
    private void handleReceiverPickupReceivedConfirmation(ReceiverPickupReceivedConfirmation event) {
        try{
            Delivery delivery = event.getDelivery();

            Person receiver = delivery.getReceiver().getPerson();
            String receiverName = receiver.getFirstName() + " " + receiver.getLastName();
            String shopName = delivery.getPickupAddress().getShop().getName();
            String poolingStationName = event.getReceiverPickupAssignment().getAddress().getPoolingStation().getName();

            teamNotificationService.sendTeamNotification(
                    delivery.getTenant(),
                    TEAM_NOTIFICATION_TOPIC,
                    TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                    "'{}' took parcel from '{}' out of poolingstation '{}'.",
                    receiverName,
                    shopName,
                    poolingStationName);
        }catch(Exception ex){
            log.error("Failed to notify to team: handleReceiverPickupReceivedConfirmation", ex);
        }
    }

}
