/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Steffen Hupp, Balthasar Weitzel, Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeBundleNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public interface ITransportAlternativeService extends IService {

    TransportAlternativeBundle findTransportAlternativeBundleById(String transportAlternativeBundleId) throws TransportAlternativeBundleNotFoundException;
    List<TransportAlternative> findTransportAlternativesByBundle(TransportAlternativeBundle transportAlternativeBundle);

    TransportAlternativeBundle createTransportAlternativeBundleForDelivery(BaseEvent event, Delivery delivery, ParcelAddress pickupAddress);
    boolean decideTransportAlternative(BaseEvent event, Person potentialCarrier, TransportAlternative selectedTransportAlternative);
    List<TransportAlternative> setOtherTransportAlternativesRejected(BaseEvent event, TransportAlternative selectedTransportAlternative);
    void setTransportAlternativeBundleWaitingForAssignment(TransportAssignmentCancelConfirmation event, TransportAlternativeBundle bundle);

    TransportAlternative findTransportAlternativeById(String transportAlternativeId) throws TransportAlternativeNotFoundException;
    Collection<TransportAlternativeBundle> findAllAlternativeBundles();

    /**
     * Returns all transport alternative bundles that are not yet assigned (TransportAlternativeBundleStatus =
     * WAITING_FOR_ASSIGNMENT) and not expired
     *
     * @param tenants          which the transport alternative bundles belong to
     * @param expiredTimestamp timestamp taken to decide which transport alternative bundles are expired and thus not
     *                         returned. This is compared with the end of the desiredDeliveryTime of the corresponding
     *                         Delivery.
     *
     * @return
     */
    List<TransportAlternativeBundle> findAllAlternativeBundleWaitingForAssignmentNotExpiredSince(Set<Tenant> tenants,
            long expiredTimestamp);

    List<TransportAlternative> findAllAlternativesForPotentialCarrier(
            TransportAlternativeBundle transportAlternativeBundle, Person potentialCarrier);

    TransportAlternativeBundle findFirstTransportAlternativeBundleForTenantSince(Tenant tenant, long since);

    TimeSpan computeShopReceiverPickupTime(Delivery delivery, ParcelAddress pickupAddress);

    TransportAlternativeBundleStatusRecord getCurrentStatusRecord(
            TransportAlternativeBundle transportAlternativeBundle);

    TransportAlternativeStatusRecord getCurrentStatusRecord(TransportAlternative transportAlternative);

    TransportAlternativeBundleStatusRecord setCurrentStatus(TransportAlternativeBundle transportAlternativeBundle,
            TransportAlternativeBundleStatus newStatus, boolean parcelAlreadyAvailableForPickup, BaseEvent event);

    TransportAlternativeStatusRecord setCurrentStatus(TransportAlternative transportAlternative,
            TransportAlternativeStatus newStatus, BaseEvent event);
}
