/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel, Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.misc.services.IRandomService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ParcelAddressRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

@Service
class LogisticsDataInitService extends BaseService implements ILogisticsDataInitService {

    public static final long A_PACKAGING_DURATION = TimeUnit.MINUTES.toMillis(60);
    public static final long B_CARRIER_SEEK_DURATION = TimeUnit.MINUTES.toMillis(30);
    public static final long C_CARRIER_PICKUP_DURATION = TimeUnit.MINUTES.toMillis(30);
    public static final long D_DELIVERY_DURATION = TimeUnit.MINUTES.toMillis(90);
    public static final long E_RECEIVER_RECEIVE_DURATION = TimeUnit.MINUTES.toMillis(10);

    @Autowired
    private IRandomService randomService;
    @Autowired
    private IPersonService personService;

    @Autowired
    private ParcelAddressRepository parcelAddressRepository;

    public long getYesterday(int hour) {

        long yesterday = 0;

        ZoneId zoneId = ZoneId.of(getConfig().getLocalTimeZone());
        yesterday = LocalDateTime.now()
                .minus(1, ChronoUnit.DAYS)
                .withHour(hour).withMinute(0).withSecond(0).withNano(0)
                .atZone(zoneId)
                .toInstant().toEpochMilli();

        return yesterday;

    }

    public long getToday(int hour){

        long today = 0;

        ZoneId zoneId = ZoneId.of(getConfig().getLocalTimeZone());
        today = LocalDateTime.now()
                .withHour(hour).withMinute(0).withSecond(0).withNano(0)
                .atZone(zoneId)
                .toInstant().toEpochMilli();

        return today;

    }

    public long getTomorrow(int hour){

        long tomorrow = 0;

        ZoneId zoneId = ZoneId.of(getConfig().getLocalTimeZone());
        tomorrow = LocalDateTime.now()
                .withHour(hour).withMinute(0).withSecond(0).withNano(0)
                .atZone(zoneId)
                .toInstant().toEpochMilli();

        return tomorrow;

    }

    @Override
    @SuppressWarnings("fallthrough")
    public long createRandomTimeStamp(DeliveryStatus finalStatus, DeliveryStatus currentStatus){

        //Explanation of time ranges for every duration (to be considered for the ifs of each case-switch)
        //Shop durations: A, B and C. Time range (getYesterday(16 - getYesterday(23) and (getToday(07 - getToday(16) (Created is also included here)
        //Delivery durations: D and E. Time range (getToday(07 and getToday(21)

        //TODO: there is no randomization for the calculated Timestamps. Use the final resultingTimeStamp - offset as upper limit
        //and the corresponding time ranges to calculate a random timestamp and pass the deliveryId to know the previous StatusRecord timestamp.
        long resultingTimeStamp = timeService.currentTimeMillisUTC();
        if (getToday(7) > resultingTimeStamp - D_DELIVERY_DURATION - E_RECEIVER_RECEIVE_DURATION){
            resultingTimeStamp = getToday(7) + D_DELIVERY_DURATION + E_RECEIVER_RECEIVE_DURATION;
        }
        long offset = 0;
        switch(finalStatus){
            case RECEIVED: {
                //timestamp cannot be before 7, because it is checked in initializeLogisticsDemoTimes()
                //check create timestamp is not after 21, if so move it before
                if(resultingTimeStamp - offset > getToday(21)){
                    resultingTimeStamp = getToday(21);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }

                if(currentStatus == DeliveryStatus.RECEIVED){
                    break;
                }

                offset += E_RECEIVER_RECEIVE_DURATION;
            }
            case DELIVERED: {
                //TODO: Use the desiredDeliveryTime for the Delivered and Received Timestamp

                //timestamp cannot be before 7, because it is checked in initializeLogisticsDemoTimes()
                //check create timestamp is not after 21, if so move it before
                if(resultingTimeStamp - offset > getToday(21)){
                    resultingTimeStamp = getToday(21);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }

                if(currentStatus == DeliveryStatus.DELIVERED){
                    break;
                }

                offset += D_DELIVERY_DURATION;
            }
            case IN_DELIVERY: {
                //check create timestamp is not before 7, if so move it to yesterday
                //check create timestamp is not after 16, if so move it before
                if(resultingTimeStamp > getYesterday(23) && resultingTimeStamp - offset < getToday(7)){
                    resultingTimeStamp = getYesterday(23);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                } else if(resultingTimeStamp - offset > getToday(16)){
                    resultingTimeStamp = getToday(16);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }

                if(currentStatus == DeliveryStatus.IN_DELIVERY){
                    break;
                }

                offset += C_CARRIER_PICKUP_DURATION;
            }
            case WAITING_FOR_PICKUP: {
                //check create timestamp is not before 7, if so move it to yesterday
                //check create timestamp is not after 16, if so move it before
                if(resultingTimeStamp > getYesterday(23) && resultingTimeStamp - offset < getToday(7)){
                    resultingTimeStamp = getYesterday(23);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                } else if(resultingTimeStamp - offset > getToday(16)){
                    resultingTimeStamp = getToday(16);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }

                if(currentStatus == DeliveryStatus.WAITING_FOR_PICKUP){
                    break;
                }

                offset += B_CARRIER_SEEK_DURATION;
            }
            case WAITING_FOR_ASSIGNMENT:{
                //check create timestamp is not before 7, if so move it to yesterday
                //check create timestamp is not after 16, if so move it before
                if(resultingTimeStamp > getYesterday(23) && resultingTimeStamp - offset < getToday(7)){
                    resultingTimeStamp = getYesterday(23);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                } else if(resultingTimeStamp - offset > getToday(16)){
                    resultingTimeStamp = getToday(16);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }

                if(currentStatus == DeliveryStatus.WAITING_FOR_ASSIGNMENT){
                    break;
                }

                offset += A_PACKAGING_DURATION;
            }
            case CREATED:{

                //check create timestamp is not before 7, if so move it to yesterday
                //check create timestamp is not after 16, if so move it before
                if(resultingTimeStamp > getYesterday(23) && resultingTimeStamp - offset < getToday(7)){
                    resultingTimeStamp = getYesterday(23);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                } else if(resultingTimeStamp - offset > getToday(16)){
                    resultingTimeStamp = getToday(16);
                    //Reset the offset since resultingTimeStamp is moved to the upper value of one of the ranges
                    offset = 0;
                }
            }
            break;
            default:
                break;
        }
        return resultingTimeStamp - offset;
    }

    @Override
    public TimeSpan createDesiredDeliveryTime(long createdTimeStamp){

        TimeSpan desiredDeliveryTime = new TimeSpan();

        long deliverDay = 0;
        if (createdTimeStamp + A_PACKAGING_DURATION + B_CARRIER_SEEK_DURATION + C_CARRIER_PICKUP_DURATION > getToday(16)){
            deliverDay = getTomorrow(0);
        } else {
            deliverDay = getToday(0);
        }

        //The delivery time range is (7, 21). For the start of the TimeSpan, 1 hour is left as margin (7, 20)
        int timeSpanStartHour = randomService.randomInt(7, 20);
        long timeSpanStart = deliverDay + TimeUnit.HOURS.toMillis(timeSpanStartHour);
        timeSpanStart +=
                (1 == randomService.randomInt(0, 1)) ? TimeUnit.MINUTES.toMillis(30) : TimeUnit.MINUTES.toMillis(0);
        desiredDeliveryTime.setStartTime(timeSpanStart);

        int timeSpanEndHour = randomService.randomInt(timeSpanStartHour + 1, 21);
        int differenceOfHours = timeSpanEndHour - timeSpanStartHour;
        long timeSpanEnd = desiredDeliveryTime.getStartTime() + TimeUnit.HOURS.toMillis(differenceOfHours);
        if (timeSpanEnd != 21){
            timeSpanEnd +=
                    (1 == randomService.randomInt(0, 1)) ? TimeUnit.MINUTES.toMillis(30) : TimeUnit.MINUTES.toMillis(0);
        }
        desiredDeliveryTime.setEndTime(timeSpanEnd);

        return desiredDeliveryTime;
    }

    @Override
    public ParcelAddress newParcelAddress(Shop shop){
        return parcelAddressRepository.save(new ParcelAddress(shop.getAddresses().iterator().next().getAddress(),shop).withConstantId());
    }

    @Override
    public ParcelAddress newParcelAddress(Person person) {
        return parcelAddressRepository.save(
                new ParcelAddress(personService.getDefaultAddressListEntry(person).getAddress(),
                        person).withConstantId());
    }

}
