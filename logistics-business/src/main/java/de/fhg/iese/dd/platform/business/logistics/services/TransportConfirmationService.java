/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportConfirmationNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;

@Service
class TransportConfirmationService extends BaseEntityService<TransportConfirmation> implements ITransportConfirmationService {

    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;

    @Override
    public TransportConfirmation findTransportConfirmationById(String transportConfirmationId)
            throws TransportConfirmationNotFoundException {
        if (transportConfirmationId == null) {
            throw new TransportConfirmationNotFoundException("");
        }
        return transportConfirmationRepository.findById(transportConfirmationId)
                .orElseThrow(() -> new TransportConfirmationNotFoundException(transportConfirmationId));
    }

    @Override
    public List<TransportConfirmation> findTransportConfirmationPickedUp(TransportAssignment transportAssignment) {
        return transportConfirmationRepository
                .findByTransportAssignmentPickedUpOrderByCreatedDesc(transportAssignment);
    }

    @Override
    public List<TransportConfirmation> findTransportConfirmationDelivered(TransportAssignment transportAssignment) {
        return transportConfirmationRepository
                .findByTransportAssignmentDeliveredOrderByCreatedDesc(transportAssignment);
    }

    @Override
    public List<TransportConfirmation> findTransportConfirmationReceived(TransportAssignment transportAssignment) {
        return transportConfirmationRepository
                .findByTransportAssignmentReceivedOrderByCreatedDesc(transportAssignment);
    }

}
