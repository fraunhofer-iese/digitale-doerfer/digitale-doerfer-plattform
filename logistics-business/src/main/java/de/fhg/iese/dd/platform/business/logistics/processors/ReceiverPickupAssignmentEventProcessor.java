/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForReceiverPickupEvent;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToDeliveryEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupAssignedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupWaitingExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PickedUpAtWrongLocationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.ReceivedByWrongReceiverException;
import de.fhg.iese.dd.platform.business.logistics.services.IReceiverPickupAssignmentService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IOpeningHoursService;
import de.fhg.iese.dd.platform.business.shared.waiting.services.IWaitingService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ReceiverPickupAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ReceiverPickupAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor
class ReceiverPickupAssignmentEventProcessor extends BaseEventProcessor {

    @Autowired
    private ReceiverPickupAssignmentRepository receiverPickupAssignmentRepository;
    @Autowired
    private IReceiverPickupAssignmentService receiverPickupAssignmentService;
    @Autowired
    private IOpeningHoursService openingHoursService;
    @Autowired
    private IWaitingService waitingService;

    @EventProcessing
    private void handleDeliveryReadyForReceiverPickupEvent(DeliveryReadyForReceiverPickupEvent event, final EventProcessingContext context) {
        Delivery delivery = event.getDelivery();
        ParcelAddress pickupAddress = event.getPickupAddress();
        String pickupNotes = event.getPickupNotes();
        ReceiverPickupAssignment pickupAssignment =
                new ReceiverPickupAssignment(pickupAddress, pickupNotes, ReceiverPickupAssignmentStatus.WAITING_FOR_PICKUP, delivery);
        pickupAssignment = receiverPickupAssignmentRepository.saveAndFlush(pickupAssignment);

        ReceiverPickupAssignedEvent assignedEvent = new ReceiverPickupAssignedEvent(delivery, pickupAssignment);
        notify(assignedEvent, context);

        //Reminder Pickup: 2h before closing
        if (pickupAddress.getAddressType() != ParcelAddressType.POOLING_STATION
                || pickupAddress.getPoolingStation() == null
                || pickupAddress.getPoolingStation().getOpeningHours() == null) {
            return;
        }
        long closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                pickupAddress.getPoolingStation().getOpeningHours(), timeService.currentTimeMillisUTC());
        long timeReminderClosing2h = closingTime - TimeUnit.HOURS.toMillis(2L);
        if (timeReminderClosing2h > timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new ReceiverPickupWaitingExpiredEvent(
                    delivery,
                    pickupAssignment,
                    false,
                    pickupAddress.getAddress().getName() + " schließt in 2 Stunden. Hole bitte deine Bestellung dort ab.")
                .withDeadline(timeReminderClosing2h));
        }

        long openingTime = openingHoursService.getFirstFutureOpeningTimeOnSameOrFutureDay(
                pickupAddress.getPoolingStation().getOpeningHours(), timeService.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(1L));
        if (openingTime > timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new ReceiverPickupWaitingExpiredEvent(
                    delivery,
                    pickupAssignment,
                    true,
                    pickupAddress.getAddress().getName() + " hat gerade wieder geöffnet. Hole bitte deine Bestellung dort ab.")
                .withDeadline(openingTime));
        }
    }

    @EventProcessing
    private PoolingStationBoxReadyForDeallocationRequest handleReceiverPickupPoolingStationBoxReadyForDeallocationRequest(
            ReceiverPickupPoolingStationBoxReadyForDeallocationRequest event) {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();
        Person receiver = event.getReceiver();

        if( delivery.getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || ! delivery.getReceiver().getPerson().getId().equals(receiver.getId())){
            throw new ReceivedByWrongReceiverException(delivery.getId(), receiver.getId());
        }

        ReceiverPickupAssignment currentAssignment = receiverPickupAssignmentService.findWaitingForPickupAssignment(delivery.getId());
        if(currentAssignment.getAddress().getAddressType() != ParcelAddressType.POOLING_STATION
                || ! poolingStation.getId().equals(currentAssignment.getAddress().getPoolingStation().getId())){
            throw new PickedUpAtWrongLocationException("The delivery '"+delivery.getId()+"' can not be picked up at pooling station '"+poolingStation.getId()+"' because it is not the one in the pickup assignment'");
        }

        return new PoolingStationBoxReadyForDeallocationRequest(delivery, poolingStation, currentAssignment);
    }

    @EventProcessing
    private PoolingStationBoxDeallocationRequest handleReceiverPickupReceivedPoolingStationRequest(ReceiverPickupReceivedPoolingStationRequest event) {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();
        Person receiver = event.getReceiver();

        if( delivery.getReceiver().getParticipantType() != LogisticsParticipantType.PRIVATE
                || ! delivery.getReceiver().getPerson().getId().equals(receiver.getId())){
            throw new ReceivedByWrongReceiverException(delivery.getId(), receiver.getId());
        }

        ReceiverPickupAssignment currentAssignment = receiverPickupAssignmentService.findWaitingForPickupAssignment(delivery.getId());
        if(currentAssignment.getAddress().getAddressType() != ParcelAddressType.POOLING_STATION
                || ! poolingStation.getId().equals(currentAssignment.getAddress().getPoolingStation().getId())){
            throw new PickedUpAtWrongLocationException("The delivery '"+delivery.getId()+"' can not be picked up at pooling station '"+poolingStation.getId()+"' because it is not the one in the pickup assignment'");
        }

        return new PoolingStationBoxDeallocationRequest(delivery, poolingStation, currentAssignment);
    }

    @EventProcessing
    private ReceiverPickupReceivedConfirmation handlePoolingStationBoxDeallocationConfirmation(PoolingStationBoxDeallocationConfirmation event) {
        ReceiverPickupAssignment pickupAssignment = event.getReceiverPickupAssignment();
        if (pickupAssignment == null) {
            return null;
        }
        TransportConfirmation pickupConfirmation = receiverPickupAssignmentService.createPickupConfirmation(pickupAssignment,
                CreatorType.POOLING_STATION, HandoverType.PERSONALLY, event.getPoolingStationBox().getId());

        return new ReceiverPickupReceivedConfirmation(pickupAssignment.getDelivery(), pickupAssignment,
                pickupConfirmation);
    }

    @EventProcessing
    private PoolingStationBoxReadyForDeallocationRequest handleReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest(
            ReceiverPickupPoolingStationBoxScannedReadyForDeallocationRequest event) {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        ReceiverPickupAssignment currentAssignment = receiverPickupAssignmentService.findWaitingForPickupAssignment(delivery.getId());
        if(currentAssignment.getAddress().getAddressType() != ParcelAddressType.POOLING_STATION
                || ! poolingStation.getId().equals(currentAssignment.getAddress().getPoolingStation().getId())){
            throw new PickedUpAtWrongLocationException("The delivery '"+delivery.getId()+"' can not be picked up at pooling station '"+poolingStation.getId()+"' because it is not the one in the pickup assignment'");
        }

        return new PoolingStationBoxReadyForDeallocationRequest(delivery, poolingStation, currentAssignment);
    }

    @EventProcessing
    private void handleReceiverPickupWaitingExpiredEvent(ReceiverPickupWaitingExpiredEvent event, final EventProcessingContext context) {
        if (event.getReceiverPickupAssignment().getStatus() != ReceiverPickupAssignmentStatus.WAITING_FOR_PICKUP) {
            return;
        }
        notify(new MessageRelatedToDeliveryEvent(event.getDelivery(), event.getMessage()), context);

        if (!event.isRecurringAtOpeningTime()) {
            return;
        }
        long closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                event.getDelivery().getPickupAddress().getPoolingStation().getOpeningHours(), timeService.currentTimeMillisUTC());
        long timeReminderClosing2h = closingTime - TimeUnit.HOURS.toMillis(2L);
        if (timeReminderClosing2h > timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new ReceiverPickupWaitingExpiredEvent(
                    event.getDelivery(),
                    event.getReceiverPickupAssignment(),
                    false,
                    event.getDelivery().getPickupAddress().getAddress().getName() + " schließt in 2 Stunden. Hole bitte deine Bestellung dort ab.")
                .withDeadline(timeReminderClosing2h));
        }

        long openingTime = openingHoursService.getFirstFutureOpeningTimeOnSameOrFutureDay(
                event.getDelivery().getPickupAddress().getPoolingStation().getOpeningHours(), timeService.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(1L));
        if (openingTime > timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new ReceiverPickupWaitingExpiredEvent(
                    event.getDelivery(),
                    event.getReceiverPickupAssignment(),
                    true,
                    event.getDelivery().getPickupAddress().getAddress().getName() + " hat gerade wieder geöffnet. Hole bitte deine Bestellung dort ab.")
                .withDeadline(openingTime));
        }
    }

}
