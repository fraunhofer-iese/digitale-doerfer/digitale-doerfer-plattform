/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

public abstract class BaseTransportDataInitializer extends BaseDataInitializer {

    @Autowired
    protected IPersonService personService;
    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    protected TransportAlternativeRepository transportAlternativeRepository;
    @Autowired
    protected TransportAlternativeBundleRepository transportAlternativeBundleRepository;
    @Autowired
    protected TransportAlternativeBundleStatusRecordRepository transportAlternativeBundleStatusRecordRepository;
    @Autowired
    protected TransportAlternativeStatusRecordRepository transportAlternativeStatusRecordRepository;
    @Autowired
    private TransportAssignmentRepository transportAssignmentRepository;
    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;
    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;
    @Autowired
    private IChatService chatService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private ILogisticsDataInitService logisticsDataInitService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(Delivery.class)
                .requiredEntity(PushCategory.class)
                .processedEntity(TransportAlternativeBundle.class)
                .processedEntity(TransportAlternative.class)
                .processedEntity(TransportAssignment.class)
                .build();
    }

    protected TransportAlternative createTransportAlternative(LogSummary logSummary, String deliveryId, DeliveryStatus finalStatus) {
        Delivery delivery = deliveryService.findDeliveryById(deliveryId);
        TransportAlternativeBundle bundle = new TransportAlternativeBundle();
        bundle.setDelivery(delivery);
        bundle.setPickupAddress(delivery.getPickupAddress());
        bundle = transportAlternativeBundleRepository.save(bundle.withConstantId());

        long waitingForAssingmentTimeStamp = logisticsDataInitService.createRandomTimeStamp(finalStatus, DeliveryStatus.WAITING_FOR_ASSIGNMENT);
        transportAlternativeBundleStatusRecordRepository.deleteAllByTransportAlternativeBundleId(bundle.getId());
        transportAlternativeBundleStatusRecordRepository.save(TransportAlternativeBundleStatusRecord.builder()
                .transportAlternativeBundle(bundle)
                .incomingEvent("Demo, no event!")
                .status(TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT)
                .parcelAlreadyAvailableForPickup(true)
                .timestamp(waitingForAssingmentTimeStamp)
                .build());

        TransportAlternative alternative = new TransportAlternative();
        alternative.setTransportAlternativeBundle(bundle);
        alternative.setKind(TransportKind.RECEIVER);
        alternative.setDeliveryAddress(delivery.getDeliveryAddress());

        alternative.setDesiredDeliveryTime(delivery.getDesiredDeliveryTime());
        alternative.setDesiredPickupTime(
                transportAlternativeService.computeShopReceiverPickupTime(delivery, delivery.getPickupAddress()));

        alternative.setLatestPickupTime(alternative.getDesiredPickupTime().getEndTime());
        alternative.setLatestDeliveryTime(delivery.getDesiredDeliveryTime().getEndTime());
        alternative.setCredits(8);

        alternative = transportAlternativeRepository.save(alternative.withConstantId());

        transportAlternativeStatusRecordRepository.deleteAllByTransportAlternativeId(alternative.getId());
        transportAlternativeStatusRecordRepository.save(TransportAlternativeStatusRecord.builder()
                .transportAlternative(alternative)
                .incomingEvent("Demo, no event!")
                .status(TransportAlternativeStatus.WAITING_FOR_ASSIGNMENT)
                .timestamp(waitingForAssingmentTimeStamp)
            .build());

        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
            .delivery(delivery)
            .incomingEvent("Demo, no event!")
            .status(DeliveryStatus.WAITING_FOR_ASSIGNMENT)
            .timeStamp(waitingForAssingmentTimeStamp)
            .build());

        logSummary.info("Created transport alternative bundle "+bundle.getId()+" for delivery "+delivery.getId());
        return alternative;
    }

    protected TransportAssignment createTransportAssignment(LogSummary logSummary, TransportAlternative transportAlternative, Person carrier, DeliveryStatus finalStatus) {
        TransportAlternativeBundle bundle = transportAlternative.getTransportAlternativeBundle();
        TransportAssignment assignment = new TransportAssignment();
        assignment.setDelivery(bundle.getDelivery());
        assignment.setCarrier(carrier);
        assignment.setCredits(transportAlternative.getCredits());
        assignment.setDeliveryAddress(transportAlternative.getDeliveryAddress());
        assignment.setDesiredDeliveryTime(transportAlternative.getDesiredDeliveryTime());
        assignment.setDesiredPickupTime(transportAlternative.getDesiredPickupTime());
        assignment.setLatestDeliveryTime(transportAlternative.getLatestDeliveryTime());
        assignment.setLatestPickupTime(transportAlternative.getLatestPickupTime());
        assignment.setPickupAddress(bundle.getPickupAddress());
        assignment.setKind(transportAlternative.getKind());
        assignment.setTransportAlternative(transportAlternative);

        assignment = transportAssignmentRepository.save(assignment.withConstantId());
        transportAssignmentStatusRecordRepository.deleteAllByTransportAssignment(assignment);
        long waitingForPickupTimeStamp =
                logisticsDataInitService.createRandomTimeStamp(finalStatus, DeliveryStatus.WAITING_FOR_PICKUP);
        transportAssignmentStatusRecordRepository.save(TransportAssignmentStatusRecord.builder()
                .transportAssignment(assignment)
                .incomingEvent("Demo, no event!")
                .status(TransportAssignmentStatus.WAITING_FOR_PICKUP)
                .timestamp(waitingForPickupTimeStamp)
                .build());

        List<TransportAlternative> otherTransportAlternatives =
                transportAlternativeRepository.findAllByTransportAlternativeBundleOrderByCreatedDesc(bundle);

        otherTransportAlternatives.stream()
                .filter(ta -> !ta.getId().equals(transportAlternative.getId()))
                .forEach(ta -> transportAlternativeStatusRecordRepository
                        .save(TransportAlternativeStatusRecord.builder()
                                .transportAlternative(ta)
                                .incomingEvent("Demo, no event!")
                                .status(TransportAlternativeStatus.REJECTED)
                                .timestamp(waitingForPickupTimeStamp)
                                .build()));

        transportAlternativeBundleStatusRecordRepository.save(TransportAlternativeBundleStatusRecord.builder()
                .transportAlternativeBundle(bundle)
                .incomingEvent("Demo, no event!")
                .status(TransportAlternativeBundleStatus.ASSIGNED)
                .parcelAlreadyAvailableForPickup(true)
                .timestamp(waitingForPickupTimeStamp)
                .build());

        transportAlternativeStatusRecordRepository.save(TransportAlternativeStatusRecord.builder()
            .transportAlternative(transportAlternative)
            .incomingEvent("Demo, no event!")
            .status(TransportAlternativeStatus.ASSIGNED)
            .timestamp(waitingForPickupTimeStamp)
            .build());

        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
            .delivery(bundle.getDelivery())
            .incomingEvent("Demo, no event!")
            .status(DeliveryStatus.WAITING_FOR_PICKUP)
            .timeStamp(waitingForPickupTimeStamp)
            .build());

        Chat chat = chatService.findChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, bundle.getDelivery());
        chat = chatService.addParticipantsToChat(chat, carrier);
        chatService.addSubjectsToChat(getPushCategoryChat(), chat, assignment);

        logSummary.info("Created transport assignment "+assignment.getId()+" for delivery "+bundle.getDelivery().getId());
        return assignment;
    }

    protected void pickupDelivery(LogSummary logSummary, TransportAssignment assignment, DeliveryStatus finalStatus) {

        long inDeliveryTimeStamp = logisticsDataInitService.createRandomTimeStamp(finalStatus, DeliveryStatus.IN_DELIVERY);

        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(CreatorType.CARRIER)
            .handover(HandoverType.PERSONALLY)
            .notes("")
            .timestamp(inDeliveryTimeStamp)
            .transportAssignmentPickedUp(assignment)
            .build();
        transportConfirmationRepository.saveAndFlush(transportConfirmation.withConstantId());

        transportAssignmentStatusRecordRepository.save(TransportAssignmentStatusRecord.builder()
            .transportAssignment(assignment)
            .incomingEvent("Demo, no event!")
            .status(TransportAssignmentStatus.IN_DELIVERY)
            .timestamp(inDeliveryTimeStamp)
            .build());

        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
            .delivery(assignment.getDelivery())
            .incomingEvent("Demo, no event!")
            .status(DeliveryStatus.IN_DELIVERY)
            .timeStamp(inDeliveryTimeStamp)
            .build());

        logSummary.info("Picked up delivery "+assignment.getDelivery().getId());
    }

    protected void deliverDelivery(LogSummary logSummary, TransportAssignment assignment, DeliveryStatus finalStatus) {

        long deliveredTimeStamp = logisticsDataInitService.createRandomTimeStamp(finalStatus, DeliveryStatus.DELIVERED);
        TransportConfirmation deliveryConfirmation = TransportConfirmation.builder()
            .creator(CreatorType.CARRIER)
            .handover(HandoverType.PERSONALLY)
            .notes("")
            .timestamp(deliveredTimeStamp)
            .transportAssignmentDelivered(assignment)
            .build();
        transportConfirmationRepository.saveAndFlush(deliveryConfirmation.withConstantId());

        transportAssignmentStatusRecordRepository.save(TransportAssignmentStatusRecord.builder()
            .transportAssignment(assignment)
            .incomingEvent("Demo, no event!")
            .status(TransportAssignmentStatus.DELIVERED)
            .timestamp(deliveredTimeStamp)
            .build());

        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
            .delivery(assignment.getDelivery())
            .incomingEvent("Demo, no event!")
            .status(DeliveryStatus.DELIVERED)
            .timeStamp(deliveredTimeStamp)
            .build());

        long receivedTimeStamp = logisticsDataInitService.createRandomTimeStamp(finalStatus, DeliveryStatus.RECEIVED);

        assignment.getDelivery().setActualDeliveryTime(timeService.currentTimeMillisUTC());
        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
            .delivery(assignment.getDelivery())
            .incomingEvent("Demo, no event!")
            .status(DeliveryStatus.RECEIVED)
            .timeStamp(receivedTimeStamp)
            .build());

        deliveryService.store(assignment.getDelivery());

        logSummary.info("Delivered and received delivery "+assignment.getDelivery().getId());
    }

    private PushCategory getPushCategoryChat() {
        return pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_CHAT_ID);
    }

}
