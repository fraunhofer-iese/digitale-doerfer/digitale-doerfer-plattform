/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationBoxCannotBeOpenedException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationFullException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.ReceiverPickupAssignmentNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.logistics.services.IReceiverPickupAssignmentService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxConfiguration;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ReceiverPickupAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.BoxAllocationStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationBoxType;

@EventProcessor
class PoolingStationEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPoolingStationService poolingStationService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private IReceiverPickupAssignmentService receiverPickupAssignmentService;

    //Allocation
    @EventProcessing
    private void handlePoolingStationBoxReadyForAllocationRequest(PoolingStationBoxReadyForAllocationRequest event, final EventProcessingContext context) throws PoolingStationFullException {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        PoolingStationBoxAllocation boxAllocation = poolingStationService.startAllocation(poolingStation, delivery);
        PoolingStationBox box = boxAllocation.getPoolingStationBox();
        PoolingStationBoxConfiguration boxConfig = box.getConfiguration();

        if(boxConfig.getBoxType() == PoolingStationBoxType.AUTOMATIC){
            //Send a request to open the box if it is automatic
            PoolingStationBoxOpenRequest openRequest = new PoolingStationBoxOpenRequest(box);
            notify(event, openRequest, this::compensateOpenRequestForAllocation, context);
        }

        notify(new PoolingStationBoxReadyForAllocationConfirmation(delivery, box, boxConfig), context);
    }

    private void compensateOpenRequestForAllocation(PoolingStationBoxReadyForAllocationRequest allocationRequest, PoolingStationBoxOpenRequest sentEvent, Exception ex, final EventProcessingContext context) {
        if(ex instanceof PoolingStationBoxCannotBeOpenedException){
            throw (PoolingStationBoxCannotBeOpenedException)ex;
        }
        PoolingStation poolingStation = allocationRequest.getPoolingStation();
        PoolingStationBox poolingStationBox = sentEvent.getPoolingStationBox();

        throw new PoolingStationBoxCannotBeOpenedException(poolingStation.getName(), poolingStation.getId(), poolingStationBox.getId(), ex);
    }

    @EventProcessing
    private PoolingStationBoxAllocationConfirmation handlePoolingStationBoxAllocationRequest(PoolingStationBoxAllocationRequest event) {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        PoolingStationBoxAllocation boxAllocation = poolingStationService.finalizeAllocation(poolingStation, delivery);
        PoolingStationBox box = boxAllocation.getPoolingStationBox();

        return new PoolingStationBoxAllocationConfirmation(delivery, event.getTransportAssignment(), box);
    }

    //Deallocation
    @EventProcessing
    private void handlePoolingStationBoxReadyForDeallocationRequest(PoolingStationBoxReadyForDeallocationRequest event, final EventProcessingContext context) throws DeliveryNotInPoolingStationException {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        PoolingStationBoxAllocation boxAllocation = poolingStationService.startDeallocation(poolingStation, delivery);
        PoolingStationBox box = boxAllocation.getPoolingStationBox();
        PoolingStationBoxConfiguration boxConfig = box.getConfiguration();

        if (boxConfig.getBoxType() == PoolingStationBoxType.AUTOMATIC) {
            PoolingStationBoxOpenRequest openRequest = new PoolingStationBoxOpenRequest(box);
            notify(event, openRequest, this::compensateOpenRequestForDeallocation, context);
        }

        PoolingStationBoxReadyForDeallocationConfirmation confirmation = new PoolingStationBoxReadyForDeallocationConfirmation(delivery, box, boxConfig, event.getTransportAssignment());

        notify(confirmation, context);
    }

    private void compensateOpenRequestForDeallocation(PoolingStationBoxReadyForDeallocationRequest deallocationRequest, PoolingStationBoxOpenRequest sentEvent, Exception ex, final EventProcessingContext context) {
        if(ex instanceof PoolingStationBoxCannotBeOpenedException){
            throw (PoolingStationBoxCannotBeOpenedException)ex;
        }
        PoolingStation poolingStation = deallocationRequest.getPoolingStation();
        PoolingStationBox poolingStationBox = sentEvent.getPoolingStationBox();

        throw new PoolingStationBoxCannotBeOpenedException(poolingStation.getName(), poolingStation.getId(), poolingStationBox.getId(), ex);
    }

    @EventProcessing
    private PoolingStationBoxDeallocationConfirmation handlePoolingStationBoxDeallocationRequest(PoolingStationBoxDeallocationRequest event) {
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        PoolingStationBoxAllocation boxAllocation = poolingStationService.finalizeDeallocation(poolingStation, delivery);
        PoolingStationBox box = boxAllocation.getPoolingStationBox();

        return new PoolingStationBoxDeallocationConfirmation(delivery, box, event.getReceiverPickupAssignment(), event.getTransportAssignment());
    }

    @EventProcessing
    private void handlePoolingStationBoxOpenedEvent(PoolingStationBoxOpenedEvent event) {
        //change the status of the box to open
        poolingStationService.doorOpened(event.getPoolingStationBox());
    }

    @EventProcessing
    private BaseEvent handlePoolingStationBoxClosedEvent(PoolingStationBoxClosedEvent event) {
        PoolingStationBox box = event.getPoolingStationBox();
        PoolingStation poolingStation = box.getPoolingStation();

        if (!poolingStation.isSelfScanningStation()) {
            //change the status of the box to closed
            poolingStationService.doorClosed(event.getPoolingStationBox());
            return null;
        } else {
            PoolingStationBoxAllocation currentBoxAllocation = poolingStationService.getCurrentAllocation(box);
            if(currentBoxAllocation == null) {
                poolingStationService.doorClosed(event.getPoolingStationBox());
                return null;
            }
            Delivery delivery = currentBoxAllocation.getDelivery();
            if(delivery == null) {
                poolingStationService.doorClosed(event.getPoolingStationBox());
                return null;
            }
            BoxAllocationStatus currentBoxAllocationStatus = currentBoxAllocation.getStatus();

            if (currentBoxAllocationStatus == BoxAllocationStatus.PENDING_IN) {
                Optional<TransportAssignment> assignment = transportAssignmentService.findAllByDeliveryOrderByCreatedDesc(delivery)
                        .stream()
                        .filter(ta -> Objects.equals(ta.getDeliveryAddress().getPoolingStation(), poolingStation))
                        .findFirst();
                if (assignment.isPresent()) {
                    poolingStationService.doorClosed(box);
                    return new PoolingStationBoxAllocationRequest(delivery, assignment.get(), poolingStation);
                } else {
                    //something strange happened here :-)
                    poolingStationService.doorClosed(box);
                    return null;
                }
            }
            if (currentBoxAllocationStatus == BoxAllocationStatus.PENDING_OUT) {
                try {
                    //try receiver pickup
                    ReceiverPickupAssignment pickupAssignment = receiverPickupAssignmentService.findWaitingForPickupAssignment(delivery.getId());

                    poolingStationService.doorClosed(box);

                    return new PoolingStationBoxDeallocationRequest(delivery, poolingStation, pickupAssignment);

                } catch (ReceiverPickupAssignmentNotFoundException e) {
                    //else do carrier pickup
                    Optional<TransportAssignment> assignment = transportAssignmentService.findAllByDeliveryOrderByCreatedDesc(delivery)
                                    .stream()
                                    .filter(ta -> Objects.equals(ta.getPickupAddress().getPoolingStation(), poolingStation))
                                    .findFirst();

                    if (assignment.isPresent()) {

                        poolingStationService.doorClosed(box);
                        return new PoolingStationBoxDeallocationRequest(delivery, poolingStation, assignment.get());
                    } else {
                        //TODO find out what happens here
                        poolingStationService.doorClosed(box);
                        return null;
                    }
                }
            }
            return null;
        }
    }

}
