/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

/**
 * An Internal Business Service that connects logistics and score.
 *
 * Some actions in logistics need scores to be booked or calculated. All
 * business logic that is related to this interconnection between score and
 * logistics is encapsulated with this service.
 *
 */
public interface ILogisticsScoreService extends IService {

    /**
     * Reserve the shipping charges a receiver has to pay for a delivery
     *
     * @param delivery
     */
    void reserveShippingCharges(Delivery delivery);

    /**
     * Book the previously reserved shipping charges a receiver has to pay for a
     * delivery
     *
     * @param delivery
     */
    void bookShippingCharges(Delivery delivery);

    /**
     * Book the transport reward a carrier gets for a successful transport
     *
     * @param transportAssignment
     */
    void bookTransportReward(TransportAssignment transportAssignment);

    /**
     * Calculate the current transport award a potential carrier would get after
     * doing this transport.
     *
     * @param transportAlternative
     * @param potentialCarrier
     * @return
     */
    int calculateTransportReward(TransportAlternative transportAlternative, Person potentialCarrier);

    /**
     * Calculate the maximum transport award a carrier can potentially get after
     * doing this transport.
     *
     * @param transportAlternative
     * @return
     */
    int calculateMaxTransportReward(TransportAlternative transportAlternative);

    /**
     * Calculate the shipping charges a receiver has to pay for this delivery.
     *
     * @param delivery
     * @return
     */
    int calculateShippingCharges(Delivery delivery);

}
