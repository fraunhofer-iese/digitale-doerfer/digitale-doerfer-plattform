/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel, Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeBundleNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAlternativeNotFoundException;
import de.fhg.iese.dd.platform.business.participants.shop.services.IOpeningHoursService;
import de.fhg.iese.dd.platform.business.shared.geo.services.IRoutingService;
import de.fhg.iese.dd.platform.business.shared.geo.services.IRoutingService.TravelMode;
import de.fhg.iese.dd.platform.business.shared.misc.services.ITimeSpanService;
import de.fhg.iese.dd.platform.business.shared.misc.services.ITimeSpanService.RoundingType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ParcelAddressRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

@Service
class TransportAlternativeService extends BaseService implements ITransportAlternativeService  {

    // Search for PoolingStations near to a Shop max distance
    private static final double MAX_DISTANCE_SHOP_TO_NEARBY_POOLING_STATION_KM = 0.2;

    // Search for PoolingStations near to receiver
    private static final double MAX_DISTANCE_RECEIVER_TO_NEARBY_POOLING_STATION_KM = 1.0;

    // intermed PoolingStation/Shop handling time (find someone to pickup, fetch parcel)
    private static final long INTERMED_HANDLING_TIME_MINUTES = 60L;
    private static final long INTERMED_HANDLING_TIME_MILLI = TimeUnit.MINUTES.toMillis(INTERMED_HANDLING_TIME_MINUTES);

    // default transportTimeMinutes
    private static final int DEFAULT_TRANSPORT_TIME_MINUTES = 30;

    // round times to minute accuracy
    private static final int ROUND_MINUTES = 5;

    @Autowired
    private TransportAlternativeRepository transportAlternativeRepository;
    @Autowired
    private TransportAlternativeBundleRepository transportAlternativeBundleRepository;
    @Autowired
    private TransportAlternativeBundleStatusRecordRepository transportAlternativeBundleStatusRecordRepository;
    @Autowired
    private TransportAlternativeStatusRecordRepository transportAlternativeStatusRecordRepository;
    @Autowired
    private ParcelAddressRepository parcelAddressRepository;
    @Autowired
    private IPoolingStationService poolingStationService;
    @Autowired
    private IRoutingService routingService;
    @Autowired
    private ITimeSpanService timeSpanService;
    @Autowired
    private IOpeningHoursService openingHoursService;
    @Autowired
    private ILogisticsScoreService scoreService;

    @Override
    public TransportAlternativeBundle createTransportAlternativeBundleForDelivery(BaseEvent event, Delivery delivery, ParcelAddress pickupAddress) {

        TransportAlternativeBundle bundle = new TransportAlternativeBundle();
        bundle.setDelivery(delivery);
        bundle.setPickupAddress( pickupAddress );
        bundle = transportAlternativeBundleRepository.saveAndFlush(bundle);

        setCurrentStatus(bundle, TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT, true, event);

        // TransportKind.RECEIVER
        createReceiverTransportAlternative(event, bundle, delivery, pickupAddress);

        // avoid picking PoolingStations twice
        List<String> usedPoolingStationIds = new ArrayList<>();
        if ( delivery.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION ) {
            usedPoolingStationIds.add( delivery.getDeliveryAddress().getPoolingStation().getId() );
        }
        List<TransportAlternativeBundle> allPastBundles = transportAlternativeBundleRepository.findAllByDeliveryOrderByCreatedDesc(delivery);
        allPastBundles.stream().filter(tab -> tab.getPickupAddress().getAddressType() == ParcelAddressType.POOLING_STATION)
                .map(tab -> tab.getPickupAddress().getPoolingStation().getId()).forEach(usedPoolingStationIds::add);

        // Optional PoolingStation near Shop ( within MAX_DISTANCE_SHOP_TO_NEARBY_POOLING_STATIONKM)
        GPSLocation pickupLocation = pickupAddress.getAddress().getGpsLocation();

        if ( pickupLocation != null ) {

            Tenant tenant = delivery.getTenant();
            PoolingStation nearestPoolingStationPickup =
                    poolingStationService.findNearestPoolingStation(tenant, pickupAddress,
                            MAX_DISTANCE_SHOP_TO_NEARBY_POOLING_STATION_KM);

            if (nearestPoolingStationPickup != null
                    && !usedPoolingStationIds.contains(nearestPoolingStationPickup.getId())
                    && nearestPoolingStationPickup.isIntermediateStation()
                    && poolingStationService.isAvailableForPutParcel(nearestPoolingStationPickup, delivery)) {

                usedPoolingStationIds.add(nearestPoolingStationPickup.getId());
                createPoolingStationTransportAlternative(event, bundle, delivery, nearestPoolingStationPickup,
                        pickupAddress);

            }
        }

        // additional alternatives require GPSLocations
        GPSLocation deliveryLocation = delivery.getDeliveryAddress().getAddress().getGpsLocation();
        if ( deliveryLocation == null  ) {
            return bundle;
        }

        // deliveryLocation defined
        // Optional PoolingStation near receiver
        ParcelAddress deliveryAddress = delivery.getDeliveryAddress();
        Tenant tenant = delivery.getTenant();

        PoolingStation nearestPoolingStationDelivery =
                poolingStationService.findNearestPoolingStation(tenant, deliveryAddress,
                        MAX_DISTANCE_RECEIVER_TO_NEARBY_POOLING_STATION_KM);

        if ( nearestPoolingStationDelivery != null &&
                poolingStationService.isAvailableForPutParcel(nearestPoolingStationDelivery, delivery) &&
                !usedPoolingStationIds.contains( nearestPoolingStationDelivery.getId() ) &&
                nearestPoolingStationDelivery.isIntermediateStation()) {

            usedPoolingStationIds.add( nearestPoolingStationDelivery.getId() );
            createPoolingStationTransportAlternative(event, bundle, delivery, nearestPoolingStationDelivery, pickupAddress);

        }

        final TransportAlternativeBundle finalBundle = bundle;
        // all PoolingStation MainHubs
        List<PoolingStation> mainHubs =
                poolingStationService.findMainHubsBetween(delivery.getTenant(), pickupLocation, deliveryLocation);
        mainHubs.stream().filter(mainHub -> !usedPoolingStationIds.contains(mainHub.getId()) && poolingStationService.isAvailableForPutParcel(mainHub, delivery))
                .forEach(mainHub -> {
                    usedPoolingStationIds.add(mainHub.getId());
                    createPoolingStationTransportAlternative(event, finalBundle, delivery, mainHub, pickupAddress);
                });

        transportAlternativeBundleRepository.flush();
        transportAlternativeBundleStatusRecordRepository.flush();
        transportAlternativeRepository.flush();
        transportAlternativeStatusRecordRepository.flush();
        return bundle;
    }

    private TransportAlternative createReceiverTransportAlternative(BaseEvent event, TransportAlternativeBundle bundle, Delivery delivery, ParcelAddress pickupAddress) {

        TransportAlternative ta = new TransportAlternative();
        ta.setTransportAlternativeBundle(bundle);
        ta.setKind(TransportKind.RECEIVER);
        ta.setDeliveryAddress(delivery.getDeliveryAddress());

        ta.setDesiredDeliveryTime(delivery.getDesiredDeliveryTime());
        ta.setDesiredPickupTime(computeShopReceiverPickupTime(delivery, pickupAddress));

        ta.setLatestPickupTime(ta.getDesiredPickupTime().getEndTime());
        ta.setLatestDeliveryTime(delivery.getDesiredDeliveryTime().getEndTime());
        ta.setCredits(scoreService.calculateMaxTransportReward(ta));

        ta = transportAlternativeRepository.saveAndFlush(ta);

        setCurrentStatus(ta, TransportAlternativeStatus.WAITING_FOR_ASSIGNMENT, event);
        return ta;
    }

    private TransportAlternative createPoolingStationTransportAlternative(BaseEvent event, TransportAlternativeBundle bundle, Delivery delivery, PoolingStation ps, ParcelAddress pickupAddress) {
        TransportAlternative ta;
        ParcelAddress poolingStationParcelAddress = new ParcelAddress(ps.getAddress(), ps);
        poolingStationParcelAddress = parcelAddressRepository.saveAndFlush(poolingStationParcelAddress);

        ta = new TransportAlternative();
        ta.setTransportAlternativeBundle(bundle);
        ta.setKind(TransportKind.INTERMEDIATE);
        ta.setDeliveryAddress(poolingStationParcelAddress);
        ta.setDesiredDeliveryTime(computeTransportAlternativeDesiredDeliveryTime(delivery, ps));
        ta.setDesiredPickupTime(computeTransportAlternativeDesiredPickupTime(delivery, ps, pickupAddress));

        ta.setLatestPickupTime(ta.getDesiredPickupTime().getEndTime());
        ta.setLatestDeliveryTime(ta.getDesiredDeliveryTime().getEndTime());
        ta.setCredits(scoreService.calculateMaxTransportReward(ta));

        ta = transportAlternativeRepository.saveAndFlush(ta);

        setCurrentStatus(ta, TransportAlternativeStatus.WAITING_FOR_ASSIGNMENT, event);

        return ta;
    }

    @Override
    public boolean decideTransportAlternative(BaseEvent event, Person potentialCarrier, TransportAlternative selectedTransportAlternative) {

        TransportAlternativeBundle transportAlternativeBundle = selectedTransportAlternative.getTransportAlternativeBundle();

        TransportAlternativeBundleStatusRecord oldCurrentStatus = getCurrentStatusRecord(transportAlternativeBundle);

        if(oldCurrentStatus == null || oldCurrentStatus.getStatus() != TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT){
            return false;
        }

        //add the new status, since we assume we can get it
        TransportAlternativeBundleStatusRecord bundleAssignedStatus =
                setCurrentStatus(transportAlternativeBundle, TransportAlternativeBundleStatus.ASSIGNED,
                        oldCurrentStatus.isParcelAlreadyAvailableForPickup(), event);

        //now check if we were really the only one
        List<TransportAlternativeBundleStatusRecord> newBundleStatusRecords = transportAlternativeBundleStatusRecordRepository.
                findAllByTransportAlternativeBundleIdOrderByTimestampDesc(transportAlternativeBundle.getId());

        TransportAlternativeBundleStatusRecord newCurrentStatus = newBundleStatusRecords.get(0);

        if(!newCurrentStatus.getId().equals(bundleAssignedStatus.getId())){
            log.warn("Concurrent request detected and rolled back " + event.getLogMessage());
            //another request also wrote a new status record, this writer wins, we lost
            transportAlternativeBundleStatusRecordRepository.delete(bundleAssignedStatus);
            return false;
        }

        //we win, continue
        setCurrentStatus(selectedTransportAlternative, TransportAlternativeStatus.ASSIGNED, event);

        return true;
    }

    @Override
    public List<TransportAlternative> setOtherTransportAlternativesRejected(BaseEvent event, TransportAlternative selectedTransportAlternative){
        TransportAlternativeBundle transportAlternativeBundle = selectedTransportAlternative.getTransportAlternativeBundle();
        List<TransportAlternative> otherTransportAlternatives = findTransportAlternativesByBundle(transportAlternativeBundle);

        return otherTransportAlternatives.stream()
                .filter(ta -> !ta.equals(selectedTransportAlternative))
                .peek(transportAlternative -> setCurrentStatus(transportAlternative, TransportAlternativeStatus.REJECTED, event))
                .collect(Collectors.toList());
    }

    @Override
    public void setTransportAlternativeBundleWaitingForAssignment(TransportAssignmentCancelConfirmation event, TransportAlternativeBundle transportAlternativeBundle) {

        List<TransportAlternative> transportAlternatives = findTransportAlternativesByBundle(transportAlternativeBundle);
        transportAlternatives.forEach(transportAlternative -> setCurrentStatus(transportAlternative,
                TransportAlternativeStatus.WAITING_FOR_ASSIGNMENT, event));
        TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus = getCurrentStatusRecord(
                transportAlternativeBundle);

        setCurrentStatus(transportAlternativeBundle, TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT,
                currentAlternativeBundleStatus != null &&
                        currentAlternativeBundleStatus.isParcelAlreadyAvailableForPickup(), event);
    }

    @Override
    public List<TransportAlternative> findTransportAlternativesByBundle(TransportAlternativeBundle transportAlternativeBundle) {
        return transportAlternativeRepository.findAllByTransportAlternativeBundleOrderByCreatedDesc(transportAlternativeBundle);
    }

    @Override
    public TransportAlternative findTransportAlternativeById(String transportAlternativeId)    throws TransportAlternativeNotFoundException {
        if(transportAlternativeId == null) throw new TransportAlternativeNotFoundException("");
        return transportAlternativeRepository.findById(transportAlternativeId)
                .orElseThrow(() -> new TransportAlternativeNotFoundException(transportAlternativeId));
    }

    @Override
    public Collection<TransportAlternativeBundle> findAllAlternativeBundles() {
        return transportAlternativeBundleRepository.findAll(Sort.by(Sort.Direction.DESC, "created"));
    }

    @Override
    public TransportAlternativeBundle findTransportAlternativeBundleById(String transportAlternativeBundleId) throws TransportAlternativeBundleNotFoundException {
        if(transportAlternativeBundleId == null) throw new TransportAlternativeBundleNotFoundException("");
        return transportAlternativeBundleRepository.findById(transportAlternativeBundleId)
                .orElseThrow(() -> new TransportAlternativeBundleNotFoundException(transportAlternativeBundleId));
    }

    /**
     * Compute Pickup TimeSpan for direct shop-to-receiver deliveries.
     * Considers shop opening hours and/or transport time
     *
     * @param delivery The delivery to be picked up
     * @param pickupAddress The address where the delivery can be picked up
     * @return The pickup Timespan
     */
    @Override
    public TimeSpan computeShopReceiverPickupTime(Delivery delivery, ParcelAddress pickupAddress) {
        long utcNow = timeService.currentTimeMillisUTC();
        long roundedNow = timeSpanService.roundTimeToMinutes( utcNow, ROUND_MINUTES, RoundingType.UP );

        OpeningHours pickupOpeningHours = null;
        if(pickupAddress.getAddressType() == ParcelAddressType.SHOP ){
            pickupOpeningHours = pickupAddress.getShop().getOpeningHours();
        }
        if(pickupAddress.getAddressType() == ParcelAddressType.POOLING_STATION ){
            pickupOpeningHours = pickupAddress.getPoolingStation().getOpeningHours();
        }

        if (pickupOpeningHours == null) {
            return getTightPickupTime(delivery, pickupAddress);
        }

        // find a current or future OpeningHours TimeSpan
        TimeSpan pickupTimeSpan = null;

        long closingTime = openingHoursService.getLastFutureClosingTimeOnSameDay(pickupOpeningHours,
                delivery.getDesiredDeliveryTime().getStartTime());
        if (closingTime > delivery.getDesiredDeliveryTime().getStartTime()) {
            for (long i = 1; i < 7; i++) {
                closingTime = openingHoursService.getLastFutureClosingTimeOnSameDay(pickupOpeningHours,
                        delivery.getDesiredDeliveryTime().getStartTime() - i * TimeUnit.DAYS.toMillis(1L));
                if (closingTime < delivery.getDesiredDeliveryTime().getStartTime() && closingTime != -1) {
                    pickupTimeSpan = new TimeSpan(timeService.currentTimeMillisUTC(), closingTime);
                    break;
                }

            }
        }

        /*
        for (TimeSpan timeSpan : openingHoursService.getEmergingOpeningHours(pickupOpeningHours)) {
            pickupTimeSpan = timeSpan;

            // Suitable current OpeningHour?
            if (timeSpanService.minutesRemaining(pickupTimeSpan) > intermedHandlingTimeMinutes)
                break;

            // future OpeningHour
            if (pickupTimeSpan.getStart() > utcNow)
                break;
        }
        */

        if ( pickupTimeSpan != null ) {
            pickupTimeSpan.setStartTime(roundedNow);
            return pickupTimeSpan;
        } else {
            // for some reason we can't find an OpeningHours-based TimeSpan --> fallback to modified old implementation
            return getTightPickupTime(delivery, pickupAddress);
        }

    }

    /**
     * Compute pickupTimeSpan based on transportTime and desiredDeliveryTime
     * pickupTimeSpan.start = round( NOW )
     * pickupTimeSpan.end = desiredDeliveryTime.end - transportTime
     * If pickupTimeSpan.end - NOW < intermedHandlingTimeMinutes
     *   extend pickupTimeSpan duration to intermedHandlingTimeMinutes
     *
     * @param delivery
     * @return Pickup-TimeSpan
     */
    private TimeSpan getTightPickupTime(Delivery delivery, ParcelAddress pickupAddress) {

        TimeSpan desiredDeliveryTime = delivery.getDesiredDeliveryTime();

        // can we compute a transport time? Assume defaultTransportTimeMinutes Minutes if not
        GPSLocation from = pickupAddress.getAddress().getGpsLocation();
        GPSLocation to = delivery.getDeliveryAddress().getAddress().getGpsLocation();
        long transportTimeMinutes = DEFAULT_TRANSPORT_TIME_MINUTES;
        if (from != null && to != null) {
            transportTimeMinutes = routingService.routeTimeInMinutes(from, to, TravelMode.BICYCLING);
        }

        TimeSpan idealPickup = timeSpanService.subtractMinutes(desiredDeliveryTime, transportTimeMinutes);
        idealPickup = timeSpanService.round(idealPickup, ROUND_MINUTES, RoundingType.UP);

        long roundedNow =
                timeSpanService.roundTimeToMinutes(timeService.currentTimeMillisUTC(), ROUND_MINUTES, RoundingType.UP);

        // already too late for in-time-delivery?
        if (idealPickup.getStartTime() < timeService.currentTimeMillisUTC()) {
            idealPickup.setStartTime(roundedNow);
        }

        // pickupTimeSpan too short?
        if (idealPickup.getEndTime() - timeService.currentTimeMillisUTC() < INTERMED_HANDLING_TIME_MILLI) {
            idealPickup.setEndTime(roundedNow + INTERMED_HANDLING_TIME_MILLI);
        }

        return idealPickup;
    }

    private TimeSpan computeTransportAlternativeDesiredPickupTime(Delivery delivery, PoolingStation ps, ParcelAddress pickupAddress) {
        TimeSpan taDesiredPickupTime = new TimeSpan();
        // pickup from now on
        taDesiredPickupTime.setStartTime(timeService.currentTimeMillisUTC());

        // shop location is defined
        GPSLocation pickupLocation = pickupAddress.getAddress().getGpsLocation();
        // ParcelAddress is PoolingStation -> pa-gps is defined
        GPSLocation psLocation = ps.getAddress().getGpsLocation();

        long timeToPoolingStationMinutes = DEFAULT_TRANSPORT_TIME_MINUTES;
        if(pickupLocation != null && psLocation != null){
            timeToPoolingStationMinutes = routingService.routeTimeInMinutes(pickupLocation, psLocation, TravelMode.BICYCLING );
        }

        // Delivery Location could be undefined
        GPSLocation deliveryLocation = delivery.getDeliveryAddress().getAddress().getGpsLocation();

        long timePoolingStationToDeliveryMinutes = DEFAULT_TRANSPORT_TIME_MINUTES;
        if ( deliveryLocation != null && psLocation != null) {
            timePoolingStationToDeliveryMinutes = routingService.routeTimeInMinutes(psLocation, deliveryLocation, TravelMode.BICYCLING );
        }

        // latest time to pickup
        // NOW + timeToPoolingStationMinutes + handlingTime + timePoolingStationToDeliveryMinutes < desiredDeliveryTime.end
        long totalHandlingMinutes = timeToPoolingStationMinutes + INTERMED_HANDLING_TIME_MINUTES + timePoolingStationToDeliveryMinutes;
        long totalHandlingMilli = TimeUnit.MINUTES.toMillis(totalHandlingMinutes);

        long latestPickupTime = delivery.getDesiredDeliveryTime().getEndTime() - totalHandlingMilli;

        taDesiredPickupTime.setEndTime(latestPickupTime);

        return taDesiredPickupTime;
    }

    /**
     * Ensure that there is a chance to deliver in time PoolingStation ->
     * deliveryAddress
     *
     * @param delivery
     * @param ps
     * @return
     */
    private TimeSpan computeTransportAlternativeDesiredDeliveryTime(Delivery delivery, PoolingStation ps) {
        // TODO consider OpeningHours of Shop and PoolingStation
        TimeSpan taDesiredDeliveryTime = new TimeSpan();

        // would be best if it was there right now
        taDesiredDeliveryTime.setStartTime(timeService.currentTimeMillisUTC());

        // latest PS delivery time
        // consider transport time and desiredDeliveryTime
        GPSLocation psLocation = ps.getAddress().getGpsLocation();

        // Delivery Location could be undefined
        GPSLocation deliveryLocation = delivery.getDeliveryAddress().getAddress().getGpsLocation();
        long timePoolingStationToDeliveryMinutes = INTERMED_HANDLING_TIME_MINUTES;
        if ( deliveryLocation != null  && psLocation != null) {
            timePoolingStationToDeliveryMinutes = routingService.routeTimeInMinutes(psLocation, deliveryLocation, TravelMode.BICYCLING );
        }

        // latest time to deliver to PoolingStation
        // NOW + timeToPoolingStationMinutes + handlingTime + timePoolingStationToDeliveryMinutes < desiredDeliveryTime.end
        long totalHandlingMinutes = INTERMED_HANDLING_TIME_MINUTES + timePoolingStationToDeliveryMinutes;
        long totalHandlingMilli = TimeUnit.MINUTES.toMillis(totalHandlingMinutes);

        long latestDeliveryTime = delivery.getDesiredDeliveryTime().getEndTime() - totalHandlingMilli;

        taDesiredDeliveryTime.setEndTime(latestDeliveryTime);

        return taDesiredDeliveryTime;
    }

    @Override
    public TransportAlternativeBundle findFirstTransportAlternativeBundleForTenantSince(Tenant tenant, long since) {
        return transportAlternativeBundleRepository.findFirstWaitingForAssignmentByCommunityIdAndTimestampGreaterThan(
                tenant.getId(), since);
    }

    @Override
    public List<TransportAlternativeBundle> findAllAlternativeBundleWaitingForAssignmentNotExpiredSince(
            Set<Tenant> tenants, long expiredTimestamp) {
        return transportAlternativeBundleRepository
                .findAllWaitingForAssignmentByCommunityIdsNotExpiredSinceOrderByCreatedDesc(
                        tenants.stream().map(BaseEntity::getId).collect(Collectors.toList()),
                        expiredTimestamp);
    }

    @Override
    public List<TransportAlternative> findAllAlternativesForPotentialCarrier(TransportAlternativeBundle transportAlternativeBundle, Person potentialCarrier) {
        List<TransportAlternative> alternatives = transportAlternativeRepository.findAllByTransportAlternativeBundleOrderByCreatedDesc(transportAlternativeBundle);
        alternatives.forEach(alternative -> alternative.setCredits(scoreService.calculateTransportReward(alternative, potentialCarrier)));
        return alternatives;
    }

    @Override
    public TransportAlternativeBundleStatusRecord getCurrentStatusRecord(
            TransportAlternativeBundle transportAlternativeBundle) {
        return transportAlternativeBundleStatusRecordRepository.findFirstByTransportAlternativeBundleOrderByTimestampDesc(transportAlternativeBundle);
    }

    @Override
    public TransportAlternativeStatusRecord getCurrentStatusRecord(
            TransportAlternative transportAlternative) {
        return transportAlternativeStatusRecordRepository.findFirstByTransportAlternativeOrderByTimestampDesc(transportAlternative);
    }

    @Override
    public TransportAlternativeBundleStatusRecord setCurrentStatus(
            TransportAlternativeBundle transportAlternativeBundle, TransportAlternativeBundleStatus newStatus,
            boolean parcelAlreadyAvailableForPickup, BaseEvent event){

        //we need to check that the new status record does not get the same timestamp
        // since this would cause confusion when ordering them
        TransportAlternativeBundleStatusRecord currentStatusRecord = getCurrentStatusRecord(transportAlternativeBundle);
        long timestamp = timeService.currentTimeMillisUTC();
        if(currentStatusRecord!= null && timestamp <= currentStatusRecord.getTimestamp()){
            timestamp++;
            if(timestamp <= currentStatusRecord.getTimestamp()){
                log.warn("Events from the past for transport alternative bundle {}, " +
                                "last status record timestamp is {}, current time is {}, taking last timestamp + 1",
                        transportAlternativeBundle.getId(),
                        currentStatusRecord.getTimestamp(), timestamp);
                timestamp = currentStatusRecord.getTimestamp() + 1;
            }
        }

        return transportAlternativeBundleStatusRecordRepository.saveAndFlush(TransportAlternativeBundleStatusRecord.builder()
                .transportAlternativeBundle(transportAlternativeBundle)
                .incomingEvent(event.getClass().getSimpleName())
                .status(newStatus)
                .parcelAlreadyAvailableForPickup(parcelAlreadyAvailableForPickup)
                .timestamp(timestamp)
                .build());
    }

    @Override
    public TransportAlternativeStatusRecord setCurrentStatus(TransportAlternative transportAlternative,
            TransportAlternativeStatus newStatus, BaseEvent event){

        //we need to check that the new status record does not get the same timestamp
        // since this would cause confusion when ordering them
        TransportAlternativeStatusRecord currentStatusRecord = getCurrentStatusRecord(transportAlternative);
        long timestamp = timeService.currentTimeMillisUTC();
        if(currentStatusRecord!= null && timestamp <= currentStatusRecord.getTimestamp()){
            timestamp++;
            if(timestamp <= currentStatusRecord.getTimestamp()){
                log.warn("Events from the past for transport alternative {}, " +
                                "last status record timestamp is {}, current time is {}, taking last timestamp + 1",
                        transportAlternative.getId(),
                        currentStatusRecord.getTimestamp(), timestamp);
                timestamp = currentStatusRecord.getTimestamp() + 1;
            }
        }

        return transportAlternativeStatusRecordRepository.saveAndFlush(TransportAlternativeStatusRecord.builder()
                .transportAlternative(transportAlternative)
                .incomingEvent(event.getClass().getSimpleName())
                .status(newStatus)
                .timestamp(timestamp)
                .build());
    }

}
