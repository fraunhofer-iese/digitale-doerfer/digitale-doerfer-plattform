/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxConfiguration;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationBoxType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxConfigurationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

@Component
public class PoolingStationDataInitializer extends BaseDataInitializer {

    public static final String TEST_AUTOMATIC_1_UUID = "ccbc8b8c-8065-484f-9212-9f6284779df7";
    public static final String TEST_AUTOMATIC_1_BOX1_UUID = "2d886bff-9ee9-4bb6-bab8-5e8603be0619";
    public static final String TEST_AUTOMATIC_1_BOX2_UUID = "e3e197b5-ab56-4c11-8e11-e1ca9d94c673";
    public static final String TEST_AUTOMATIC_1_BOX3_UUID = "2cc7aa2f-98c0-49a7-beda-58811684731d";
    public static final String TEST_AUTOMATIC_1_BOX4_UUID = "b411d8da-1c76-48cd-8f9f-3271bd33c9f6";
    public static final String TEST_AUTOMATIC_1_BOX5_UUID = "61a8af9c-5953-4308-ad64-ed040b2dd8fc";
    public static final String TEST_AUTOMATIC_1_BOX6_UUID = "8abf2028-5d84-4a2b-b614-9a369d0e2842";
    public static final String TEST_AUTOMATIC_1_BOX7_UUID = "824b3042-a83e-42cf-bca5-d7ea2881861d";
    public static final String TEST_AUTOMATIC_1_BOX8_UUID = "c47a18c2-0a5d-4a3d-b918-c28f7d722bfe";
    public static final String TEST_AUTOMATIC_1_VERIFICATION_CODE = "123456";

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private PoolingStationRepository poolingStationRepository;

    @Autowired
    private PoolingStationBoxRepository poolingStationBoxRepository;

    @Autowired
    private PoolingStationBoxConfigurationRepository poolingStationBoxConfigurationRepository;

    @Autowired
    private OpeningHoursEntryRepository openingHoursEntryRepository;

    @Autowired
    private OpeningHoursRepository openingHoursRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("poolingstation")
                .requiredEntity(Tenant.class)
                .processedEntity(PoolingStation.class)
                .processedEntity(PoolingStationBox.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        Tenant demo = tenantService.findTenantById(LogisticsConstants.TENANT_ID_DEMO);

        createPSTest(logSummary, demo);

        poolingStationRepository.flush();
        poolingStationBoxConfigurationRepository.flush();
        poolingStationBoxRepository.flush();
        openingHoursEntryRepository.flush();
        openingHoursRepository.flush();
    }

    private void createPSTest(LogSummary logSummary, Tenant tenant) {
        OpeningHours openingHours = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(8).withToHours(22),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(8).withToHours(22),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(8).withToHours(22),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(8).withToHours(22),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(8).withToHours(22),
                new OpeningHoursEntry(DayOfWeek.SATURDAY).withFromHours(8).withToHours(22));
        PoolingStation poolingStation = poolingStationRepository.saveAndFlush(new PoolingStation(
                TEST_AUTOMATIC_1_UUID,
                addressService.findOrCreateAddress(IAddressService.AddressDefinition.builder()
                                .name("D-Station Test (HIT-Markt)")
                                .street("Konrad-Adenauer-Straße 1")
                                .zip("67304")
                                .city("Eisenberg")
                                .gpsLocation(new GPSLocation(49.562779, 8.083432))
                                .build(),
                        IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                        IAddressService.GPSResolutionStrategy.PROVIDED_LOOKEDUP_NONE,
                        false), // address
                "D-Station Test (HIT-Markt)", // name
                TEST_AUTOMATIC_1_VERIFICATION_CODE, // verificationCode
                null, // profilePicture
                tenant, // community
                openingHours, // openingHours
                PoolingStationType.AUTOMATIC, // stationType
                true, // isFinalStation
                false, // isIntermediateStation
                false, // isMainHub
                // commandQueueUrl
                true // isSelfScanningStation
                // isSpeakingStation
        ));
        logPoolingStationCreation(logSummary, poolingStation);
        PoolingStationBoxConfiguration poolingStationBoxConfiguration = poolingStationBoxConfigurationRepository.saveAndFlush(
                PoolingStationBoxConfiguration.builder()
                .firesDoorOpenEvent(true)
                .firesDoorClosedEvent(true)
                .doorOpensInstantly(false)
                .timeoutForDoorOpen(10)
                .boxType(PoolingStationBoxType.AUTOMATIC)
                .inactive(false)
                .build()
        );
        createPoolingStationBoxes(poolingStation, poolingStationBoxConfiguration, 1, 8,
            TEST_AUTOMATIC_1_BOX1_UUID ,
            TEST_AUTOMATIC_1_BOX2_UUID ,
            TEST_AUTOMATIC_1_BOX3_UUID ,
            TEST_AUTOMATIC_1_BOX4_UUID ,
            TEST_AUTOMATIC_1_BOX5_UUID ,
            TEST_AUTOMATIC_1_BOX6_UUID ,
            TEST_AUTOMATIC_1_BOX7_UUID ,
            TEST_AUTOMATIC_1_BOX8_UUID );
    }

    private void logPoolingStationCreation(LogSummary logSummary, PoolingStation poolingStation){
        logSummary.info("Created poolingstation '{}' ({}) type={}",
            poolingStation.getName(), poolingStation.getId(), poolingStation.getStationType().toString());
    }

    private OpeningHours createOpeningHours(OpeningHoursEntry... openingHoursEntries){
        return openingHoursRepository.save(new OpeningHours(new HashSet<>(openingHoursEntryRepository.saveAll(Arrays.asList(openingHoursEntries)))));
    }

    private PoolingStationBox createPoolingStationBox(String poolingStationBoxId, PoolingStationBoxConfiguration boxConfiguration, PoolingStation poolingStation, int boxNumber){
        return  poolingStationBoxRepository.save(new PoolingStationBox(
            poolingStationBoxId, //id
            "Fach "+boxNumber, //name
            boxConfiguration, //configuration
            poolingStation, //poolingStation
            boxNumber //number
            ));
    }

    private List<PoolingStationBox> createPoolingStationBoxes(
            PoolingStation poolingStation,
            PoolingStationBoxConfiguration boxConfiguration,
            int fromNumber,
            int toNumber,
            String... poolingStationBoxIds){
        if(fromNumber > toNumber){
            throw new RuntimeException("from < to");
        }
        if((toNumber-fromNumber+1) != poolingStationBoxIds.length){
            throw new RuntimeException("to-from+1 != ids.length");
        }
        List<PoolingStationBox> createdBoxes = new ArrayList<>(poolingStationBoxIds.length);
        for( int i = fromNumber; i <= toNumber; i++){
            createdBoxes.add(createPoolingStationBox(poolingStationBoxIds[i-fromNumber], boxConfiguration, poolingStation, i));
        }
        return createdBoxes;
    }

}
