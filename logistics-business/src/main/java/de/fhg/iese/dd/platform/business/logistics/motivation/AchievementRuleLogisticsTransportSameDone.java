/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.motivation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleLogisticsTransportSameDone extends BaseAchievementRule<TransportDeliveredConfirmation> {

    private static final String ACHIEVEMENT_ID =          "a68a3649-aa3a-4aec-be57-2f95c7b018b6";
    private static final String ACHIEVEMENT_LEVEL_ID_05 = "96a62d8e-83d0-4217-86cd-445cf6d0307c";

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Override
    public String getName() {
        return "logistics.score.transport.done.3";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(TransportDeliveredConfirmation event) {
        TransportAssignment transportAssignment = event.getTransportAssignment();
        if(transportAssignment == null || transportAssignment.getDeliveryAddress() == null
                || transportAssignment.getDeliveryAddress().getAddressType() != ParcelAddressType.PRIVATE) {
            return null;
        }
        Person carrier = transportAssignment.getCarrier();
        Person receiver = transportAssignment.getDeliveryAddress().getPerson();
        long timestamp = event.getCreated();

        int numberOfTransportsToSamePersonsDone = transportAssignmentService.getNumberOfCompletedTransportAssignmentsForPersonAndReceiverPerson(carrier, receiver);

        if (numberOfTransportsToSamePersonsDone == 5) {
            return Collections.singletonList(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, carrier, timestamp));
        } else {
            return null;
        }
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        TransportAssignment lastCompletedAssignment = transportAssignmentService.getLatestCompletedTransportAssignment(person);
        long timestamp = lastCompletedAssignment == null ? timeService.currentTimeMillisUTC() : lastCompletedAssignment.getCreated();

        long maxNumberOfTransportsToDistinctPersonsDone = transportAssignmentService.getMaxNumberOfCompletedTransportAssignmentsToSameReceiverPerson(person);

        if (maxNumberOfTransportsToDistinctPersonsDone >= 5) {
           return Collections.singletonList(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, person, timestamp));
        }

        return Collections.emptyList();
    }

    @Override
    public Class<TransportDeliveredConfirmation> getRelevantEvent() {
        return TransportDeliveredConfirmation.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Logistic.Transport.Same")
                .description("Beliefere die gleiche Person mehrmals um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("LieferBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel5 = AchievementLevel.builder()
                .name("Best Friends Forever")
                .description("So langsam kennt ihr euch aber richtig gut - macht doch mal was zusammen und werdet echte Freunde!")
                .challengeDescription("Beliefere fünf mal die gleiche Person.")
                .icon(createIconFromDefault("bff.png"))
                .iconNotAchieved(createIconFromDefault("bff_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel5.setId(ACHIEVEMENT_LEVEL_ID_05);
        achievementLevel5 = achievementService.save(achievementLevel5);

        achievement.setAchievementLevels(Collections.singleton(achievementLevel5));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_05);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
