/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.dataprivacy;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

@Component
public class TransportDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class, App.class, Delivery.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(TransportAssignment.class);

    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;
    @Autowired
    private ILogisticsDataDeletionService logisticsDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    private String getStatusRecordsContent(final TransportAssignment assignment) {
        final List<TransportAssignmentStatusRecord> statusRecords = transportAssignmentStatusRecordRepository
                .findAllByTransportAssignmentIdOrderByTimestampDesc(assignment.getId());
        return statusRecords
                .stream()
                .sorted(Comparator.comparing(TransportAssignmentStatusRecord::getTimestamp))
                .map(assignmentStatusRecord ->
                        timeService.toLocalTimeHumanReadable(assignmentStatusRecord.getTimestamp()) + ": "
                                + assignmentStatusRecord.getStatus().getFriendlyHumanReadableString())
                .collect(Collectors.joining("\n"));
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<TransportAssignment> ownAssignments = transportAssignmentService.findAllByCarrier(person);

        if (CollectionUtils.isEmpty(ownAssignments)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("LieferBar", "Übernommene Lieferungen aus LieferBar");

        for (TransportAssignment assignment : ownAssignments) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Lieferung",
                            "Übernommen am " + timeService.toLocalTimeHumanReadable(assignment.getCreated()));

            subSection.newContent("Abholadresse",
                    assignment.getPickupAddress().getAddress().toHumanReadableString(true));
            subSection.newContent("Lieferadresse",
                    assignment.getDeliveryAddress().getAddress().toHumanReadableString(true));

            // is this specified by the carrier?
            subSection.newContent("Gewünschter Abholzeitpunkt", assignment.getDesiredPickupTime().toHumanReadableString());
            subSection.newContent("Gewünschter Lieferzeitpunkt", assignment.getDesiredDeliveryTime().toHumanReadableString());

            subSection.newContent("Auslieferungsstatus-Verlauf", getStatusRecordsContent(assignment));

            subSection.newContent("DigiTaler", "Mit dieser Lieferung verdiente DigiTaler",
                    String.valueOf(assignment.getCredits()));
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {
        logisticsDataDeletionService.deleteTransportData(person, personDeletionReport);
    }

}
