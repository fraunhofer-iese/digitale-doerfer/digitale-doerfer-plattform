/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 ???, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.DuplicateEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForReceiverPickupEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryStatusChangedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleCreateConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveredToWrongLocationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.InvalidDeliveryStatusException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.ReceivedByWrongReceiverException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor
class DeliveryEventProcessor extends BaseEventProcessor {

    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private DeliveryRepository deliveryRepository;

    @EventProcessing
    private DeliveryCreatedEvent handleDeliveryCreationRequest(DeliveryCreationRequest event) {

        Delivery delivery = deliveryService.createDelivery(Delivery.builder()
            .community(event.getTenant())
            .sender(event.getSender())
            .receiver(event.getReceiver())
            .pickupAddress(event.getPickupAddress())
            .deliveryAddress(event.getDeliveryAddress())
            .desiredDeliveryTime(event.getDesiredDeliveryTime())
            .transportNotes(event.getTransportNotes())
            .contentNotes(event.getContentNotes())
            .customReferenceNumber(event.getCustomReferenceNumber())
            .build());

        newStatusRecord(DeliveryStatus.CREATED, event, delivery);
        return new DeliveryCreatedEvent( delivery, event.getOriginId(), event.getOriginType() );
    }

    @EventProcessing
    private List<BaseEvent> handleTransportAlternativeBundleCreateConfirmation(TransportAlternativeBundleCreateConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportAssignmentCreatedEvent(TransportAssignmentCreatedEvent event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportPickupConfirmationEvent(TransportPickupConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportUndoPickupConfirmationEvent(TransportUndoPickupConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportDeliveredConfirmation(TransportDeliveredConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportDeliveredReceivedConfirmation(TransportDeliveredReceivedConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleDeliveryReceivedRequest(DeliveryReceivedRequest event) {
        Person eventReceiver = event.getReceiver();
        Delivery delivery = event.getDelivery();
        Person deliveryReceiver = delivery.getReceiver().getPerson();

        if(!StringUtils.equals(eventReceiver.getId(),deliveryReceiver.getId()))
            throw new ReceivedByWrongReceiverException(delivery.getId(), eventReceiver.getId());

        return dispatchEvent(delivery, event);
    }

    @EventProcessing
    private List<BaseEvent> handleReceiverPickupReceivedConfirmation(ReceiverPickupReceivedConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    @EventProcessing
    private List<BaseEvent> handleTransportAssignmentCancelConfirmation(TransportAssignmentCancelConfirmation event) {
        return dispatchEvent(event.getDelivery(), event);
    }

    private List<BaseEvent> dispatchEvent(Delivery delivery, BaseEvent event) {
        DeliveryStatus currentStatus = deliveryService.getCurrentStatusRecord(delivery).getStatus();
        DeliveryStatusRecord newStatus;
        switch (currentStatus) {
        case CREATED:
            if(event instanceof TransportAlternativeBundleCreateConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleFirstWaitingForAssignment(delivery, currentStatus, newStatus);
            }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case WAITING_FOR_ASSIGNMENT:
            if(event instanceof TransportAssignmentCreatedEvent){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_PICKUP, event, delivery);
                return handleWaitingForPickup(delivery, currentStatus, newStatus);
            }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case WAITING_FOR_PICKUP:
            if(event instanceof TransportPickupConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY, event, delivery);
                return handleFirstInDelivery(delivery, currentStatus, newStatus);
            }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            if(event instanceof DeliveryReceivedRequest){
                //we also want to have in_delivery and delivered in the status history, so they happen at the same time now
                newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY, event, delivery);
                newStatus = newStatusRecord(DeliveryStatus.DELIVERED, event, delivery);
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedBeforePickedUp(delivery, (DeliveryReceivedRequest)event, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case IN_DELIVERY:
            if(event instanceof TransportPickupConfirmation){
                //idempotent
                newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY, event, delivery);
                //do nothing
                return Collections.emptyList();
            }
            if(event instanceof TransportUndoPickupConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.WAITING_FOR_PICKUP, event, delivery);
                return handleUndoPickup(delivery,  currentStatus, newStatus);
            }
            if(event instanceof TransportDeliveredConfirmation){
                TransportDeliveredConfirmation deliveredConfirmation = (TransportDeliveredConfirmation)event;
                boolean isInPoolingStation = deliveredConfirmation.isInPoolingStation();
                boolean isFinal = deliveredConfirmation.isFinal();
                if(isFinal) {
                    if(isInPoolingStation) {
                        newStatus = newStatusRecord(DeliveryStatus.DELIVERED_POOLING_STATION, event, delivery);
                        return handleDeliveredPoolingStation(delivery, deliveredConfirmation, currentStatus, newStatus);
                    } else {
                        newStatus = newStatusRecord(DeliveryStatus.DELIVERED, event, delivery);
                        return handleDelivered(delivery, deliveredConfirmation, currentStatus, newStatus);
                    }
                } else {
                    if(isInPoolingStation) {
                        newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY_POOLING_STATION, event, delivery);
                        return handleIntermediateDeliveredPoolingStation(delivery, deliveredConfirmation, currentStatus, newStatus);
                    } else {
                        //strange case, should not happen
                        newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY, event, delivery);
                        return handleIntermediateDelivered(delivery, deliveredConfirmation, currentStatus, newStatus);
                    }
                }
            }
            if(event instanceof DeliveryReceivedRequest){
                //we also want to have delivered in the status history, so they happen at the same time now
                newStatus = newStatusRecord(DeliveryStatus.DELIVERED, event, delivery);
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedWhileInDelivery(delivery, (DeliveryReceivedRequest)event, currentStatus, newStatus);
            }
            if(event instanceof TransportDeliveredReceivedConfirmation){
                //we also want to have delivered in the status history, so they happen at the same time now
                newStatus = newStatusRecord(DeliveryStatus.DELIVERED, event, delivery);
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedWhileInDelivery(delivery, (TransportDeliveredReceivedConfirmation)event, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case IN_DELIVERY_POOLING_STATION:
            if(event instanceof TransportDeliveredConfirmation){
                //idempotent
                TransportDeliveredConfirmation deliveredConfirmation = (TransportDeliveredConfirmation)event;
                boolean isInPoolingStation = deliveredConfirmation.isInPoolingStation();
                boolean isFinal = deliveredConfirmation.isFinal();
                if(! isFinal &&   isInPoolingStation) {
                    newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY_POOLING_STATION, event, delivery);
                    //do nothing
                    return Collections.emptyList();
                }
            }
            if(event instanceof TransportAlternativeBundleCreateConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleIntermediateWaitingForAssignment(delivery, currentStatus, newStatus);
            }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY_POOLING_STATION, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case POOLING_STATION_WAITING_FOR_ASSIGNMENT:
             if(event instanceof TransportDeliveredConfirmation){
                //idempotent
                 TransportDeliveredConfirmation deliveredConfirmation = (TransportDeliveredConfirmation)event;
                 boolean isInPoolingStation = deliveredConfirmation.isInPoolingStation();
                 boolean isFinal = deliveredConfirmation.isFinal();
                 if(! isFinal &&   isInPoolingStation) {
                    newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_ASSIGNMENT, event, delivery);
                    //do nothing
                    return Collections.emptyList();
                }
            }
            if(event instanceof TransportAssignmentCreatedEvent){
                 newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_PICKUP, event, delivery);
                 return handleIntermediateWaitingForPickup(delivery, currentStatus, newStatus);
             }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case POOLING_STATION_WAITING_FOR_PICKUP:
            if(event instanceof TransportAssignmentCreatedEvent){
                //idempotent
                newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_PICKUP, event, delivery);
                //do nothing
                return Collections.emptyList();
            }
            if(event instanceof TransportPickupConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.IN_DELIVERY, event, delivery);
                return handleIntermediateInDelivery(delivery, currentStatus, newStatus);
            }
            if(event instanceof TransportAssignmentCancelConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.POOLING_STATION_WAITING_FOR_ASSIGNMENT, event, delivery);
                return handleTransportAssignmentCanceled(delivery, currentStatus, newStatus);
            }
            break;
        case DELIVERED:
            if(event instanceof TransportDeliveredConfirmation){
                //idempotent
                newStatus = newStatusRecord(DeliveryStatus.DELIVERED, event, delivery);
                //do nothing
                return Collections.emptyList();
            }
            if(event instanceof DeliveryReceivedRequest){
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedAfterDelivered(delivery, (DeliveryReceivedRequest) event, currentStatus, newStatus);
            }
            if(event instanceof TransportDeliveredReceivedConfirmation){
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedAfterDelivered(delivery, (TransportDeliveredReceivedConfirmation) event, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case DELIVERED_POOLING_STATION:
            if(event instanceof TransportDeliveredConfirmation){
                //idempotent
                TransportDeliveredConfirmation deliveredConfirmation = (TransportDeliveredConfirmation)event;
                boolean isInPoolingStation = deliveredConfirmation.isInPoolingStation();
                boolean isFinal = deliveredConfirmation.isFinal();
                if(  isFinal &&   isInPoolingStation) {
                    newStatus = newStatusRecord(DeliveryStatus.DELIVERED_POOLING_STATION, event, delivery);
                    //do nothing
                    return Collections.emptyList();
                }
            }
            if(event instanceof DeliveryReceivedRequest){
                // in this case the delivery is in the pooling station but was marked as received
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handleReceivedAfterDeliveredPoolingStation(delivery, (DeliveryReceivedRequest) event, currentStatus, newStatus);
            }
            if(event instanceof ReceiverPickupReceivedConfirmation){
                // the receiver just got the parcel out of the pooling station
                newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);
                return handlePickedUpAfterDeliveredPoolingStation(delivery, (ReceiverPickupReceivedConfirmation) event, currentStatus, newStatus);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case RECEIVED:
            if(event instanceof DeliveryReceivedRequest){
                //idempotent
                //newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);//SHTODO: avoid duplicate status records
                newStatus = null;
                return handleReceivedAfterReceived(delivery, (DeliveryReceivedRequest) event, currentStatus, null);
            }
            if(event instanceof TransportDeliveredConfirmation){
                //newStatus = newStatusRecord(DeliveryStatus.RECEIVED, event, delivery);//avoid duplicate status records
                newStatus = null;
                return handleDeliveredAfterReceived(delivery, (TransportDeliveredConfirmation) event, currentStatus, null);
            }
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        case CANCELLED:
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        default:
            throw handleInvalidStatusTransition(delivery, event, currentStatus);
        }
        throw handleInvalidStatusTransition(delivery, event, currentStatus);
    }

    private DeliveryStatusRecord newStatusRecord(DeliveryStatus newStatus, BaseEvent event, Delivery delivery){
        return deliveryService.setCurrentStatus(delivery, newStatus, event);
    }

    private List<BaseEvent> handleFirstWaitingForAssignment(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {

        if(StringUtils.isEmpty(delivery.getTrackingCodeLabelURL())) {
            delivery = deliveryService.createTrackingCodeLabel(delivery);
        }

        DeliveryReadyForTransportConfirmation readyForTransportConfirmation = new DeliveryReadyForTransportConfirmation(delivery);
        return Arrays.asList(readyForTransportConfirmation, new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleWaitingForPickup(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        //nothing to do currently
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleUndoPickup(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleTransportAssignmentCanceled(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        //nothing to do currently, we only set the status back
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleFirstInDelivery(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    /**
     * Strange case, should not happen
     *
     */
    private List<BaseEvent> handleIntermediateDelivered(Delivery delivery, TransportDeliveredConfirmation deliveredConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        throw new DeliveredToWrongLocationException("Intermediate delivery currently not supported").withDetail(delivery.getId());
    }

    private List<BaseEvent> handleIntermediateDeliveredPoolingStation(Delivery delivery, TransportDeliveredConfirmation deliveredConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {

        ParcelAddress newPickupAddress = deliveredConfirmation.getTransportAssignment().getDeliveryAddress();

        return Arrays.asList(new DeliveryReadyForTransportRequest(delivery, newPickupAddress),
                DeliveryStatusChangedEvent.builder()
                        .delivery(delivery)
                        .oldStatus(currentStatus)
                        .newStatus(newStatus.getStatus())
                        .deliveredConfirmation(deliveredConfirmation.getDeliveredConfirmation()).build());
    }

    private List<BaseEvent> handleIntermediateWaitingForAssignment(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleIntermediateWaitingForPickup(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleIntermediateInDelivery(Delivery delivery, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        return Collections.singletonList(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleDeliveredPoolingStation(Delivery delivery, TransportDeliveredConfirmation deliveredConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel

        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReadyForReceiverPickupEvent(delivery,
                        deliveredConfirmation.getDeliveredConfirmation().getNotes(), delivery.getDeliveryAddress()),
                DeliveryStatusChangedEvent.builder()
                        .delivery(delivery)
                        .oldStatus(currentStatus)
                        .newStatus(newStatus.getStatus())
                        .deliveredConfirmation(deliveredConfirmation.getDeliveredConfirmation()).build());
    }

    private List<BaseEvent> handleDelivered(Delivery delivery, TransportDeliveredConfirmation deliveredConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {

        return Collections.singletonList(DeliveryStatusChangedEvent.builder()
            .delivery(delivery)
            .oldStatus(currentStatus)
            .newStatus(newStatus.getStatus())
            .deliveredConfirmation(deliveredConfirmation.getDeliveredConfirmation()).build());

    }

    private List<BaseEvent> handleReceivedBeforePickedUp(Delivery delivery, DeliveryReceivedRequest receivedRequest, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, receivedRequest.getReceiver()),
                new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleReceivedWhileInDelivery(Delivery delivery, DeliveryReceivedRequest receivedRequest, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, receivedRequest.getReceiver()),
                new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleReceivedWhileInDelivery(Delivery delivery, TransportDeliveredReceivedConfirmation receivedConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, delivery.getReceiver().getPerson()),
                DeliveryStatusChangedEvent.builder()
                        .delivery(delivery)
                        .oldStatus(currentStatus)
                        .newStatus(newStatus.getStatus())
                        .receivedConfirmation(receivedConfirmation.getReceivedConfirmation()).build());
    }

    private List<BaseEvent> handleReceivedAfterDelivered(Delivery delivery, DeliveryReceivedRequest receivedRequest, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, receivedRequest.getReceiver()),
                new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleReceivedAfterDelivered(Delivery delivery, TransportDeliveredReceivedConfirmation deliveredReceivedConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        //this is an unusual case, the delivery was delivered, but now the event that delivers and receives it is triggered
        //we just set it to received, we might have two confirmations in this case
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, delivery.getReceiver().getPerson()),
                DeliveryStatusChangedEvent.builder()
                        .delivery(delivery)
                        .oldStatus(currentStatus)
                        .newStatus(newStatus.getStatus())
                        .receivedConfirmation(deliveredReceivedConfirmation.getReceivedConfirmation()).build());
    }

    private List<BaseEvent> handleDeliveredAfterReceived(Delivery delivery, TransportDeliveredConfirmation deliveredConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        //do not send a status change in this case, since the receiver already marked it as received
        return Collections.emptyList();
    }

    private List<BaseEvent> handleReceivedAfterDeliveredPoolingStation(Delivery delivery, DeliveryReceivedRequest receivedRequest, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel
        log.warn("Delivery {} was marked as received in status {}, so there is a pending allocation of a pooling station!", delivery.getId(), currentStatus);
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        return Arrays.asList(new DeliveryReceivedConfirmation(delivery, receivedRequest.getReceiver()),
                new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
    }

    private List<BaseEvent> handleReceivedAfterReceived(Delivery delivery, DeliveryReceivedRequest receivedRequest, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) {
        //send this event out as a duplication, so that we do not trigger any other events, but signal it to the controller
        return Collections.singletonList(new DuplicateEvent<>(new DeliveryReceivedConfirmation(delivery, receivedRequest.getReceiver())));
    }

    private List<BaseEvent> handlePickedUpAfterDeliveredPoolingStation(Delivery delivery, ReceiverPickupReceivedConfirmation receivedConfirmation, DeliveryStatus currentStatus, DeliveryStatusRecord newStatus) { // NOPMD by weitzel
        delivery.setActualDeliveryTime(timeService.currentTimeMillisUTC());
        delivery = deliveryRepository.saveAndFlush(delivery);
        List<BaseEvent> result = new ArrayList<>(2);
        if(delivery.getReceiver().getParticipantType() == LogisticsParticipantType.PRIVATE){
            Person receiver = delivery.getReceiver().getPerson();
            result.add(new DeliveryReceivedConfirmation(delivery, receiver));
        }
        result.add(new DeliveryStatusChangedEvent(delivery, newStatus.getStatus(), currentStatus));
        return result;
    }

    private InvalidDeliveryStatusException handleInvalidStatusTransition(Delivery delivery, BaseEvent event, DeliveryStatus currentStatus){
        return new InvalidDeliveryStatusException("The current status of delivery "+delivery.getId()+" is "
                    +currentStatus.toString()+", the event "+event.getClass().getSimpleName()+" is invalid in this status.");
    }

}
