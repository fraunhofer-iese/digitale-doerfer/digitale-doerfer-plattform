/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryAlreadyInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryStillInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationBoxNotClosedException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationBoxNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationFullException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.BoxAllocationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public interface IPoolingStationService extends IEntityService<PoolingStation> {

    PoolingStation findPoolingStationById(String poolingStationId) throws PoolingStationNotFoundException;

    PoolingStationBox findPoolingStationBoxById(String poolingStationBoxId) throws PoolingStationBoxNotFoundException;

    /**
     * Find PoolingStation near to ParcelAddress within radius If parcelAddress is a PoolingStation -> return null
     *
     * @param tenant
     * @param pickupAddress
     * @param radiusKM      in Kilometer
     *
     * @return PoolingStation nearest to parcelAddress within radius If type of ParcelAddress is PoolingStation, the
     *         related PoolingStation will not be returned Returns null if no PoolingStation can be found
     */
    PoolingStation findNearestPoolingStation(Tenant tenant, ParcelAddress pickupAddress, double radiusKM);

    PoolingStation findNearestPoolingStation( List<PoolingStation> stations, GPSLocation location, double radiusKM );

    List<PoolingStation> findMainHubsBetween(Tenant tenant, GPSLocation from, GPSLocation to);

    List<PoolingStation> findPoolingStationsInTenant(Tenant tenant);

    boolean isAvailableForPutParcel( PoolingStation ps, Delivery d);

    /**
     * Find a reserved allocation for that delivery in the pooling station and
     * set it to {@link BoxAllocationStatus#PENDING_IN}.
     * <p/>
     * If there is no pending allocation find a free box in that pooling station
     * and allocate it with {@link BoxAllocationStatus#PENDING_IN}.
     *
     * @param poolingStation
     * @param delivery
     * @return The new or updated allocation for that delivery in that pooling
     *         station
     * @throws PoolingStationFullException
     * @throws DeliveryAlreadyInPoolingStationException
     */
    PoolingStationBoxAllocation startAllocation(PoolingStation poolingStation, Delivery delivery) throws PoolingStationFullException, DeliveryAlreadyInPoolingStationException;

    /**
     * Find a {@link BoxAllocationStatus#PENDING_IN} allocation for that
     * delivery in the pooling station and set it to
     * {@link BoxAllocationStatus#ACTIVE}.
     *
     * @param poolingStation
     * @param delivery
     * @return
     * @throws DeliveryNotInPoolingStationException
     *             if there is no pending allocation
     * @throws PoolingStationBoxNotClosedException
     */
    PoolingStationBoxAllocation finalizeAllocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException, PoolingStationBoxNotClosedException;

    /**
     * Find a {@link BoxAllocationStatus#ACTIVE} allocation for that delivery in
     * the pooling station and set it to {@link BoxAllocationStatus#PENDING_OUT}.
     *
     * @param poolingStation
     * @param delivery
     * @return
     * @throws DeliveryNotInPoolingStationException
     *             if there is no active allocation
     *
     */
    PoolingStationBoxAllocation startDeallocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException;

    /**
     * Find a {@link BoxAllocationStatus#PENDING_OUT} allocation for that
     * delivery in the pooling station and set it to
     * {@link BoxAllocationStatus#PAST}.
     *
     * @param poolingStation
     * @param delivery
     * @return
     * @throws DeliveryNotInPoolingStationException
     *             if there is no active allocation
     * @throws PoolingStationBoxNotClosedException
     * @throws DeliveryStillInPoolingStationException
     */
    PoolingStationBoxAllocation finalizeDeallocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException, DeliveryStillInPoolingStationException, PoolingStationBoxNotClosedException ;

    void doorOpened(PoolingStationBox poolingStationBox);

    void doorClosed(PoolingStationBox poolingStationBox);

    PoolingStationBox findAvailablePoolingStationBox(PoolingStation poolingStation, Delivery delivery) throws PoolingStationFullException;

    /**
     * Allocate a box directly without checking for any other allocation of that
     * delivery in the pooling station. This can cause duplicate allocations of
     * the same delivery in the pooling station. <strong>Only intended to be
     * used by administrators in rare cases</strong>
     *
     * @param poolingStationBox
     * @param delivery
     * @return
     * @throws PoolingStationFullException if the box is already occupied
     */
    PoolingStationBoxAllocation allocatePoolingStationBoxDirectly(PoolingStationBox poolingStationBox, Delivery delivery) throws PoolingStationFullException;

    PoolingStationBoxAllocation getCurrentAllocation(PoolingStationBox poolingStationBox);

}
