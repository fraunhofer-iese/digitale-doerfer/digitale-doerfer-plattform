/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.person.init.PersonManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Component
public class TransportManualTestDataInitializer extends BaseTransportDataInitializer implements IManualTestDataInitializer {

    @Override
    public String getTopic() {
        return "logistics-manual-test";
    }

    @Override
    public String getScenarioId() {
        return "logistics-manual-test-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("shopping-manual-test","person-manual-test","shop-manual-test");
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        final Person personTestDemoBoris = personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_1);
        //final Person personTestDemoSusi  = personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_2);
        //final Person personTestDemoKurt  = personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_3);
        //final Person personTestDemoBernd  = personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_4);
        final Person personTestDemoThomas  = personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_5);

        createTransportAlternative(logSummary, DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_01, DeliveryManualTestDataInitializer.DELIVERY_01_STATUS);
        createTransportAlternative(logSummary, DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_02, DeliveryManualTestDataInitializer.DELIVERY_02_STATUS);
        TransportAlternative alternative03 =
        createTransportAlternative(logSummary, DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_03, DeliveryManualTestDataInitializer.DELIVERY_03_STATUS);
        TransportAlternative alternative04 =
        createTransportAlternative(logSummary, DeliveryManualTestDataInitializer.DELIVERY_TEST_DEMO_ID_04, DeliveryManualTestDataInitializer.DELIVERY_04_STATUS);

      //TransportAssignment assignment01 = createTransportAssignment(logSummary, alternative01, carrier, DeliveryManualTestDataInitializer.DELIVERY_01_STATUS);
      //TransportAssignment assignment02 = createTransportAssignment(logSummary, alternative02, carrier, DeliveryManualTestDataInitializer.DELIVERY_02_STATUS);

        createTransportAssignment(logSummary, alternative03, personTestDemoThomas, DeliveryManualTestDataInitializer.DELIVERY_03_STATUS);
        TransportAssignment assignment04 =
        createTransportAssignment(logSummary, alternative04, personTestDemoBoris, DeliveryManualTestDataInitializer.DELIVERY_04_STATUS);

      //pickupDelivery(logSummary, assignment01, DeliveryManualTestDataInitializer.DELIVERY_01_STATUS);
      //pickupDelivery(logSummary, assignment02, DeliveryManualTestDataInitializer.DELIVERY_02_STATUS);
      //pickupDelivery(logSummary, assignment03, DeliveryManualTestDataInitializer.DELIVERY_03_STATUS);
        pickupDelivery(logSummary, assignment04, DeliveryManualTestDataInitializer.DELIVERY_04_STATUS);

      //deliveryDelivery(logSummary, assignment01, DeliveryManualTestDataInitializer.DELIVERY_01_STATUS);
      //deliveryDelivery(logSummary, assignment02, DeliveryManualTestDataInitializer.DELIVERY_02_STATUS);
      //deliveryDelivery(logSummary, assignment03, DeliveryManualTestDataInitializer.DELIVERY_03_STATUS);
      //deliveryDelivery(logSummary, assignment04, DeliveryManualTestDataInitializer.DELIVERY_04_STATUS);

        transportAlternativeBundleRepository.flush();
        transportAlternativeBundleStatusRecordRepository.flush();
        transportAlternativeRepository.flush();
        transportAlternativeStatusRecordRepository.flush();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        logSummary.info("Nothing to do, drop is done in DeliveryManualTestDataInitializer");
    }

}
