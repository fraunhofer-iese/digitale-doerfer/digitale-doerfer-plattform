/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.person.init.PersonManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.participants.shop.init.ShopManualTestDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

@Component
public class DeliveryManualTestDataInitializer extends BaseDeliveryDataInitializer implements IManualTestDataInitializer {

    public static final String DELIVERY_TEST_DEMO_ID_01 = "13f7438f-7684-4d01-bceb-abe2386db00c";
    public static final String DELIVERY_TEST_DEMO_ID_02 = "66aebc19-93f7-4fd2-8be8-841bb28b51cc";
    public static final String DELIVERY_TEST_DEMO_ID_03 = "721dfa5f-e922-4ffe-a79b-fdafa30fcaf3";
    public static final String DELIVERY_TEST_DEMO_ID_04 = "983dec15-eecb-45e6-989f-656bfeb38354";

    public static final DeliveryStatus DELIVERY_01_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;
    public static final DeliveryStatus DELIVERY_02_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;
    public static final DeliveryStatus DELIVERY_03_STATUS = DeliveryStatus.WAITING_FOR_PICKUP;
    public static final DeliveryStatus DELIVERY_04_STATUS = DeliveryStatus.IN_DELIVERY;

    @Override
    public String getTopic() {
        return "logistics-manual-test";
    }

    @Override
    public String getScenarioId() {
        return "logistics-manual-test-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("shopping-manual-test","person-manual-test","shop-manual-test");
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        createDelivery(logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopManualTestDataInitializer.TEST_DEMO_LIDL_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_2)),
                new TimeSpan(logisticsDataInitService.getToday(0) + TimeUnit.HOURS.toMillis(19), logisticsDataInitService.getToday(0) + TimeUnit.HOURS.toMillis(23)),
                DELIVERY_TEST_DEMO_ID_01,
                logisticsDataInitService.createRandomTimeStamp(DeliveryManualTestDataInitializer.DELIVERY_01_STATUS, DeliveryStatus.CREATED));
        createDelivery(logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopManualTestDataInitializer.TEST_DEMO_KAUFLAND_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_3)),
                new TimeSpan(logisticsDataInitService.getTomorrow(0) + TimeUnit.HOURS.toMillis(6), logisticsDataInitService.getTomorrow(0) + TimeUnit.HOURS.toMillis(9)),
                DELIVERY_TEST_DEMO_ID_02,
                logisticsDataInitService.createRandomTimeStamp(DeliveryManualTestDataInitializer.DELIVERY_02_STATUS, DeliveryStatus.CREATED));
        createDelivery(logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopManualTestDataInitializer.TEST_DEMO_KAUFLAND_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_4)),
                new TimeSpan(logisticsDataInitService.getToday(0) + TimeUnit.HOURS.toMillis(9), logisticsDataInitService.getToday(0) + TimeUnit.HOURS.toMillis(18)),
                DELIVERY_TEST_DEMO_ID_03,
                logisticsDataInitService.createRandomTimeStamp(DeliveryManualTestDataInitializer.DELIVERY_03_STATUS, DeliveryStatus.CREATED));
        createDelivery(logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopManualTestDataInitializer.TEST_DEMO_LIDL_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonManualTestDataInitializer.PERSON_ID_MANUAL_TEST_3)),
                new TimeSpan(logisticsDataInitService.getTomorrow(0) + TimeUnit.HOURS.toMillis(6), logisticsDataInitService.getTomorrow(0) + TimeUnit.HOURS.toMillis(9)),
                DELIVERY_TEST_DEMO_ID_04,
                logisticsDataInitService.createRandomTimeStamp(DeliveryManualTestDataInitializer.DELIVERY_04_STATUS, DeliveryStatus.CREATED));

        deliveryStatusRecordRepository.flush();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        demoLogisticsService.deleteDataForDemoTenant(LogisticsConstants.TENANT_ID_DEMO);
        logSummary.info("dropped all logistics data for test demo tenant");
    }

}
