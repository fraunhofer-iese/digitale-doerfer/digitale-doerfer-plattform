/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.dataprivacy;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportConfirmationService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.Signature;

@Component
public class DeliveryDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class, App.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Delivery.class);

    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private ITransportConfirmationService transportConfirmationService;
    @Autowired
    private ILogisticsDataDeletionService logisticsDataDeletionService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    private String getDeliveryStatusContent(final Delivery delivery) {
        return deliveryStatusRecordRepository
                .findAllByDeliveryOrderByTimeStampDesc(delivery)
                .stream()
                .sorted(Comparator.comparing(DeliveryStatusRecord::getTimeStamp))
                .map(deliveryStatus ->
                        timeService.toLocalTimeHumanReadable(deliveryStatus.getTimeStamp()) + ": "
                                + deliveryStatus.getStatus().getFriendlyHumanReadableString())
                .collect(Collectors.joining("\n"));
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<Delivery> ownDeliveries = deliveryService.findAllDeliveriesByReceiver(person);

        if (CollectionUtils.isEmpty(ownDeliveries)) {
            return;
        }

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("LieferBar", "Eigene Pakete aus LieferBar");

        for (Delivery delivery : ownDeliveries) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Paket",
                            "Erstellt am " + timeService.toLocalTimeHumanReadable(delivery.getCreated()));

            if (delivery.getDeliveryAddress() != null && delivery.getDeliveryAddress().getAddress() != null) {
                subSection.newContent("Lieferadresse",
                        delivery.getDeliveryAddress().getAddress().toHumanReadableString(true));
            }

            subSection.newContent("Tracking-Code", delivery.getTrackingCode());
            subSection.newContent("Notizen an Lieferanten", delivery.getTransportNotes());

            final TransportAssignment transportAssignment =
                    transportAssignmentService.getFinalTransportAssignment(delivery);
            if (transportAssignment != null) {
                final List<TransportConfirmation> confirmations =
                        transportConfirmationService.findTransportConfirmationReceived(transportAssignment);
                if (!CollectionUtils.isEmpty(confirmations)) {
                    for (TransportConfirmation confirmation : confirmations) {
                        if (confirmation.getCreator() == CreatorType.CARRIER
                                && confirmation.getHandover() == HandoverType.PERSONALLY) {
                            final Signature signature = new Signature(confirmation.getNotes());
                            subSection.newSignatureContent("Unterschrift", "Unterschrift des Empfängers", signature);
                        }
                    }
                }
            }

            if(delivery.getDesiredDeliveryTime() != null) {
                subSection.newContent("Gewünschter Lieferzeitraum",
                        timeService.toLocalTimeHumanReadable(delivery.getDesiredDeliveryTime().getStartTime())
                                + " - " +
                                timeService.toLocalTimeHumanReadable(delivery.getDesiredDeliveryTime().getEndTime()));
            }

            subSection.newContent("Lieferstatus-Verlauf", getDeliveryStatusContent(delivery));

            subSection.newContent("Empfangsdatum",
                    timeService.toLocalTimeHumanReadable(delivery.getActualDeliveryTime()));
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {
        logisticsDataDeletionService.deleteDeliveryData(person, personDeletionReport);
    }

}
