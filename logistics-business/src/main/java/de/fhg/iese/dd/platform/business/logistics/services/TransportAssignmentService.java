/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Service
class TransportAssignmentService extends BaseEntityService<TransportAssignment> implements ITransportAssignmentService{

    @Autowired
    private TransportAssignmentRepository transportAssignmentRepository;
    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;
    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;
    @Autowired
    private ILogisticsScoreService scoreService;

    @Override
    public TransportAssignment findTransportAssignmentById(String transportAssignmentId) throws TransportAssignmentNotFoundException {
        if(StringUtils.isEmpty(transportAssignmentId)) throw new TransportAssignmentNotFoundException("");
        return transportAssignmentRepository.findById(transportAssignmentId)
                .orElseThrow(() -> new TransportAssignmentNotFoundException(transportAssignmentId));
    }

    @Override
    public Collection<TransportAssignment> findAllTransportAssignments() {
        return transportAssignmentRepository.findAll();
    }

    @Override
    public TransportAssignment findAssignedTransportAssignmentForAlternative(TransportAlternative transportAlternative, Person carrier) {
        List<TransportAssignment> alreadyAssignedAssignments = transportAssignmentRepository.findAllByTransportAlternativeIdAndCarrierIdOrderByCreatedDesc(transportAlternative.getId(), carrier.getId());
        if(alreadyAssignedAssignments == null || alreadyAssignedAssignments.isEmpty()){
            return null;
        }else{
            //check if the TransportAssignment is not already cancelled or rejected
            return alreadyAssignedAssignments.stream()
                    .filter(a -> {
                        TransportAssignmentStatus currentStatus = getCurrentStatusRecord(a).getStatus();
                        return currentStatus != TransportAssignmentStatus.CANCELLED &&
                                currentStatus != TransportAssignmentStatus.REJECTED;
                    }).findFirst().orElse(null);
        }
    }

    @Override
    public Collection<TransportAssignment> findAllByDelivery(Delivery delivery) {
        if(delivery == null) {
            throw new DeliveryNotFoundException("");
        }
        return transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());
    }

    @Override
    public Collection<TransportAssignment> findAllByDeliveryOrderByCreatedDesc(Delivery delivery) {
        if(delivery == null) {
            throw new DeliveryNotFoundException("");
        }
        return transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());
    }

    @Override
    public Collection<TransportAssignment> findAllNonCancelledByCarrier(Person carrier, long sinceTimestamp) {
        return transportAssignmentRepository.findAllNonCancelledByCarrierIdOrderByCreatedDesc(carrier.getId(), sinceTimestamp);
    }

    @Override
    public List<TransportAssignment> findAllByCarrier(Person carrier) {
        return transportAssignmentRepository.findAllByCarrierOrderByCreatedDesc(carrier);
    }

    @Override
    public TransportAssignment createAssignmentFromAlternative(Person carrier, TransportAlternative transportAlternative) {
        TransportAlternativeBundle bundle = transportAlternative.getTransportAlternativeBundle();
        TransportAssignment assignment = new TransportAssignment();
        assignment.setDelivery(bundle.getDelivery());
        assignment.setCarrier(carrier);
        //we can not give the max score in the alternative, but have to calculate it based on the carrier,
        //since every carrier can get different scores, depending if he already transported the delivery.
        assignment.setCredits(scoreService.calculateTransportReward(transportAlternative, carrier));
        assignment.setDeliveryAddress(transportAlternative.getDeliveryAddress());
        assignment.setDesiredDeliveryTime(transportAlternative.getDesiredDeliveryTime());
        assignment.setDesiredPickupTime(transportAlternative.getDesiredPickupTime());
        assignment.setLatestDeliveryTime(transportAlternative.getLatestDeliveryTime());
        assignment.setLatestPickupTime(transportAlternative.getLatestPickupTime());
        assignment.setPickupAddress(bundle.getPickupAddress());
        assignment.setKind(transportAlternative.getKind());
        assignment.setTransportAlternative(transportAlternative);

        return transportAssignmentRepository.saveAndFlush(assignment);
    }

    @Override
    public TransportConfirmation createPickedUpConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes){
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(creatorType)
            .handover(handoverType)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .transportAssignmentPickedUp(transportAssignment).build();
        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

    @Override
    public TransportConfirmation createDeliveredConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes){
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(creatorType)
            .handover(handoverType)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .transportAssignmentDelivered(transportAssignment).build();
        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

    @Override
    public TransportConfirmation createDeliveredPlacementConfirmation(TransportAssignment transportAssignment, String notes){
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(CreatorType.CARRIER)
            .handover(HandoverType.PLACEMENT)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .transportAssignmentDelivered(transportAssignment).build();
        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

    @Override
    public TransportConfirmation createDeliveredOtherPersonConfirmation(TransportAssignment transportAssignment, String acceptorName, String notes){
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(CreatorType.CARRIER)
            .handover(HandoverType.OTHER_PERSON)
            .acceptorName(acceptorName)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .transportAssignmentDelivered(transportAssignment).build();
        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

    @Override
    public TransportConfirmation createReceivedConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes){
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(creatorType)
            .handover(handoverType)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .transportAssignmentReceived(transportAssignment).build();
        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

    @Override
    public TransportAssignment getFinalTransportAssignment(Delivery delivery){
        List<TransportAssignment> assignments = transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());
        if(assignments.isEmpty()) return null;
        return assignments.stream().filter(ta -> ta.getKind() == TransportKind.RECEIVER).findFirst().orElse(null);
    }

    @Override
    public TransportAssignment getLatestTransportAssignment(Delivery delivery){
        return transportAssignmentRepository.findFirstByDeliveryIdOrderByCreatedDesc(delivery.getId());
    }

    @Override
    public TransportAssignment getLatestCompletedTransportAssignment(Person person) {
        return transportAssignmentRepository.findFirstByCarrierIdOrderByCreatedDesc(person.getId());
    }
    
    @Override
    public int getNumberOfCompletedTransportAssignmentsForPerson(Person person) {
        return transportAssignmentRepository.getNumberOfCompletedTransportAssignmentsForPerson(person.getId());
    }

    @Override
    public int getNumberOfTransportAssignmentsCompletedToDistinctPersons(Person person) {
        return transportAssignmentRepository.getNumberOfDistinctDeliveryAddressesOFCompletedTransportAssignmentsForPerson(person.getId());
    }

    @Override
    public int getNumberOfCompletedTransportAssignmentsForPersonAndReceiverPerson(Person person, Person receiver) {
        return transportAssignmentRepository.getNumberOfCompletedTransportAssignmentsForPersonAndReceiverPerson(person.getId(), receiver.getId());
    }

    @Override
    public long getMaxNumberOfCompletedTransportAssignmentsToSameReceiverPerson(Person person) {
        return transportAssignmentRepository.getMaxNumberOfCompletedTransportAssignmentsToSameReceiverPerson(person.getId()).orElse(BigInteger.ZERO).longValueExact();
    }

    @Override
    public Set<TransportAssignment> findAllByDeliveryIds(Set<String> deliveryIds) {
        return transportAssignmentRepository.findAllByDeliveryIdIn(deliveryIds);
    }

    @Override
    public Pair<Person, ParcelAddress> getCurrentCarrierAndLocation(final Delivery delivery) {
        Person carrier = null;
        ParcelAddress location = null;

        Collection<TransportAssignment> transportAssignments = findAllByDelivery(delivery);
        if(transportAssignments != null && !transportAssignments.isEmpty()){

            MultiValueMap<TransportAssignmentStatus, TransportAssignment> transportAssignmentsByStatus = new LinkedMultiValueMap<>();
            Map<TransportAssignment, TransportAssignmentStatusRecord> statusByTransportAssignment = new HashMap<>();

            for(TransportAssignment transportAssignment : transportAssignments){
                TransportAssignmentStatusRecord currentTransportAssignmentStatus = getCurrentStatusRecord(transportAssignment);
                transportAssignmentsByStatus.add(currentTransportAssignmentStatus.getStatus(), transportAssignment);
                statusByTransportAssignment.put(transportAssignment, currentTransportAssignmentStatus);
            }

            TransportAssignment openTransportAssignment = transportAssignmentsByStatus.getFirst(TransportAssignmentStatus.IN_DELIVERY);
            if(openTransportAssignment == null){
                openTransportAssignment = transportAssignmentsByStatus.getFirst(TransportAssignmentStatus.WAITING_FOR_PICKUP);
            }
            if(openTransportAssignment != null){
                carrier = openTransportAssignment.getCarrier();
            }

            List<TransportAssignment> deliveredTransportAssignments = transportAssignmentsByStatus.get(TransportAssignmentStatus.DELIVERED);
            if(deliveredTransportAssignments != null){

                Optional<TransportAssignment> lastDeliveredTransportAssignment = deliveredTransportAssignments.stream()
                        .min(Comparator.comparingLong(ta -> statusByTransportAssignment.get(ta).getTimestamp()));

                if(lastDeliveredTransportAssignment.isPresent()){
                    carrier = lastDeliveredTransportAssignment.get().getCarrier();
                    location = lastDeliveredTransportAssignment.get().getDeliveryAddress();
                }
            }
        }
        return Pair.of(carrier, location);
    }

    @Override
    public TransportAssignmentStatusRecord getCurrentStatusRecord(TransportAssignment transportAssignment) {
        return transportAssignmentStatusRecordRepository.findFirstByTransportAssignmentOrderByTimestampDesc(transportAssignment);
    }

    @Override
    public TransportAssignmentStatusRecord setCurrentStatus(TransportAssignment transportAssignment,
            TransportAssignmentStatus newStatus, BaseEvent event){

        //we need to check that the new status record does not get the same timestamp
        // since this would cause confusion when ordering them
        TransportAssignmentStatusRecord currentStatusRecord = getCurrentStatusRecord(transportAssignment);
        long timestamp = timeService.currentTimeMillisUTC();
        if(currentStatusRecord!= null && timestamp <= currentStatusRecord.getTimestamp()){
            timestamp++;
            if(timestamp <= currentStatusRecord.getTimestamp()){
                log.warn("Events from the past for transport assignment {}, " +
                                "last status record timestamp is {}, current time is {}, taking last timestamp + 1",
                        transportAssignment.getId(),
                        currentStatusRecord.getTimestamp(), timestamp);
                timestamp = currentStatusRecord.getTimestamp() + 1;
            }
        }

        return transportAssignmentStatusRecordRepository.saveAndFlush(TransportAssignmentStatusRecord.builder()
                .transportAssignment(transportAssignment)
                .incomingEvent(event.getClass().getSimpleName())
                .status(newStatus)
                .timestamp(timestamp)
                .build());
    }

}
