/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface ITransportAssignmentService extends IEntityService<TransportAssignment> {

    TransportAssignment createAssignmentFromAlternative(Person carrier, TransportAlternative transportAlternative);

    TransportConfirmation createPickedUpConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes);

    TransportConfirmation createDeliveredConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes);
    TransportConfirmation createDeliveredPlacementConfirmation(TransportAssignment transportAssignment, String notes);
    TransportConfirmation createDeliveredOtherPersonConfirmation(TransportAssignment transportAssignment, String acceptorName, String notes);

    TransportConfirmation createReceivedConfirmation(TransportAssignment transportAssignment, CreatorType creatorType, HandoverType handoverType, String notes);

    TransportAssignment getFinalTransportAssignment(Delivery delivery);

    TransportAssignment getLatestTransportAssignment(Delivery delivery);

    TransportAssignment getLatestCompletedTransportAssignment(Person person);

    int getNumberOfCompletedTransportAssignmentsForPerson(Person person);

    int getNumberOfTransportAssignmentsCompletedToDistinctPersons(Person person);

    int getNumberOfCompletedTransportAssignmentsForPersonAndReceiverPerson(Person person, Person receiver);

    long getMaxNumberOfCompletedTransportAssignmentsToSameReceiverPerson(Person person);

    TransportAssignment findTransportAssignmentById(String transportAssignmentId) throws TransportAssignmentNotFoundException;

    Collection<TransportAssignment> findAllNonCancelledByCarrier(Person carrier, long sinceTimestamp);

    List<TransportAssignment> findAllByCarrier(Person carrier);

    Collection<TransportAssignment> findAllTransportAssignments();

    /**
     * Find the non-cancelled TransportAssignment for the carrier for the TransportAlternative
     *
     * @param transportAlternative
     * @param carrier
     * @return
     */
    TransportAssignment findAssignedTransportAssignmentForAlternative(TransportAlternative transportAlternative, Person carrier);

    Collection<TransportAssignment> findAllByDelivery(Delivery delivery);

    Collection<TransportAssignment> findAllByDeliveryOrderByCreatedDesc(Delivery delivery);

    Set<TransportAssignment> findAllByDeliveryIds(Set<String> deliveryIds);

    Pair<Person, ParcelAddress> getCurrentCarrierAndLocation(Delivery delivery);

    TransportAssignmentStatusRecord getCurrentStatusRecord(TransportAssignment transportAssignment);

    TransportAssignmentStatusRecord setCurrentStatus(TransportAssignment transportAssignment,
            TransportAssignmentStatus newStatus, BaseEvent event);
}
