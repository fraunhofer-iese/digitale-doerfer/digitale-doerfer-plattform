/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.motivation.exceptions.ReservationNotExistingException;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.ITenantAccountService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@Service
class LogisticsScoreService extends BaseService implements ILogisticsScoreService {

    private static final String ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE = "shipping-charge";
    private static final String ACCOUNT_ENTRY_MARKER_TRANSPORT_REWARD = "transport-reward";

    @Autowired
    private TransportAssignmentRepository transportAssignmentRepository;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private ITenantAccountService tenantAccountService;
    @Autowired
    private IPersonAccountService personAccountService;
    @Autowired
    private IPushCategoryService pushCategoryService;

    @Override
    public void reserveShippingCharges(Delivery delivery) {

        Account receiverAccount = getReceiverAccount(delivery);
        Account logisticsAccount = getLogisticsAccount(delivery);

        AccountEntry shippingChargesAccountEntryReceiver =  accountService.findFirstAccountEntryForSubject(receiverAccount,
            delivery, ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE);
        AccountEntry shippingChargesAccountEntryLogistics =  accountService.findFirstAccountEntryForSubject(logisticsAccount,
            delivery, ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE);

        if(shippingChargesAccountEntryReceiver != null || shippingChargesAccountEntryLogistics != null){
            //if there are already account entries, do not reserve them again
            log.debug("Shipping Charges for Delivery {} was already booked/reserved", delivery.getId());
            return;
        }

        LogisticsParticipant sender = delivery.getSender();
        String postingText = "Versandgebühr für Lieferung von ";
        if(sender.getParticipantType() == LogisticsParticipantType.PRIVATE){
            Person senderPerson = sender.getPerson();
            postingText += senderPerson.getFirstName()+" "+senderPerson.getLastName();
        }
        if(sender.getParticipantType() == LogisticsParticipantType.SHOP){
            Shop senderShop = sender.getShop();
            postingText += senderShop.getName();
        }

        int shippingCharges = calculateShippingCharges(delivery);

        accountService.reserveScore(receiverAccount, logisticsAccount, postingText, shippingCharges,
            delivery, ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE, getPushCategory());
    }

    @Override
    public void bookShippingCharges(Delivery delivery) {

        Account receiverAccount = getReceiverAccount(delivery);
        Account logisticsAccount = getLogisticsAccount(delivery);

        AccountEntry shippingChargesAccountEntryReceiver =  accountService.findFirstAccountEntryForSubject(receiverAccount,
            delivery, ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE);
        AccountEntry shippingChargesAccountEntryLogistics =  accountService.findFirstAccountEntryForSubject(logisticsAccount,
            delivery, ACCOUNT_ENTRY_MARKER_SHIPPING_CHARGE);

        if(shippingChargesAccountEntryReceiver == null || shippingChargesAccountEntryLogistics == null){
            //might happen that we still run into this issue if we are too fast
            throw new ReservationNotExistingException("No shipping charge reservation was done for delivery "+delivery.getId()).withDetail(delivery.getId());
        }

        if(!shippingChargesAccountEntryReceiver.isReservation() && !shippingChargesAccountEntryLogistics.isReservation()){
            //avoid booking the same account entry twice
            log.debug("Shipping Charges for Delivery {} was already booked/reserved", delivery.getId());
            return;
        }

        accountService.bookReservedScore(shippingChargesAccountEntryReceiver, shippingChargesAccountEntryLogistics, getPushCategory());
    }

    @Override
    public void bookTransportReward(TransportAssignment transportAssignment) {

        Account carrierAccount = getCarrierAccount(transportAssignment);
        Account logisticAccount = getLogisticsAccount(transportAssignment);

        if(!accountService.existsAccountEntryForSubject(carrierAccount, transportAssignment, ACCOUNT_ENTRY_MARKER_TRANSPORT_REWARD)){
            //the carrier has no booking for that transport reward of this transport assignment

            int transportReward = transportAssignment.getCredits();
            String postingText = "Lieferung von " +
                transportAssignment.getPickupAddress().getAddress().toHumanReadableString(true) +
                " nach "+
                transportAssignment.getDeliveryAddress().getAddress().toHumanReadableString(true);

            accountService.bookScore(logisticAccount, carrierAccount, postingText, transportReward,
                transportAssignment, ACCOUNT_ENTRY_MARKER_TRANSPORT_REWARD, getPushCategory());
        }else{
            log.debug("Transport Reward for Transport Assignment {} was already booked", transportAssignment.getId());
        }
    }

    private Account getLogisticsAccount(Delivery delivery) {
        return tenantAccountService.findTenantAccount(AppAccountType.LOGISTICS, delivery.getTenant());
    }

    private Account getLogisticsAccount(TransportAssignment transportAssignment) {
        return tenantAccountService.findTenantAccount(AppAccountType.LOGISTICS,
                transportAssignment.getDelivery().getTenant());
    }

    private Account getCarrierAccount(TransportAssignment transportAssignment) {
        return personAccountService.getAccountForOwner(transportAssignment.getCarrier());
    }

    private Account getReceiverAccount(Delivery delivery) {
        LogisticsParticipant receiver = delivery.getReceiver();

        Account receiverAccount = null;
        if(receiver.getParticipantType() == LogisticsParticipantType.PRIVATE){
            receiverAccount = personAccountService.getAccountForOwner(receiver.getPerson());
        }
        if(receiver.getParticipantType() == LogisticsParticipantType.SHOP){
            receiverAccount = personAccountService.getAccountForShop(receiver.getShop());
        }
        return receiverAccount;
    }

    @Override
    public int calculateTransportReward(TransportAlternative transportAlternative, Person potentialCarrier) {
        Delivery delivery = transportAlternative.getTransportAlternativeBundle().getDelivery();
        if (hasNonCancelledTransportAssignments(delivery, potentialCarrier)) {
            return 0;
        } else {
            return transportAlternative.getCredits();
        }
    }

    @Override
    public int calculateShippingCharges(Delivery delivery) {
        return 10;
    }

    @Override
    public int calculateMaxTransportReward(TransportAlternative transportAlternative) {
        switch (transportAlternative.getKind()) {
            case INTERMEDIATE:
                return 6;
            case RECEIVER:
                return 8;
            default:
                return 0;
        }
    }

    private PushCategory getPushCategory() {
        return pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID);
    }

    public boolean hasNonCancelledTransportAssignments(Delivery delivery, Person potentialCarrier) {
        return transportAssignmentRepository.getNumberOfNonCancelledTransportAssignmentsForPersonAndDelivery(
                potentialCarrier.getId(), delivery.getId()) > 0;
    }

}
