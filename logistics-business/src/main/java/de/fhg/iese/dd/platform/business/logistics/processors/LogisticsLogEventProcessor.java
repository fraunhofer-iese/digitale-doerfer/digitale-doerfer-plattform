/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2021 Axel Wickenkamp, Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForReceiverPickupEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportAssignmentEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryStatusChangedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxOpenedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupAssignedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.ReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeAcceptedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleCreateConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleForSelectionEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeRejectedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupRequest;
import de.fhg.iese.dd.platform.business.shared.log.processors.BaseLogEventProcessor;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class LogisticsLogEventProcessor extends BaseLogEventProcessor {

    @EventProcessing(relevantEvents = {
            DeliveryReadyForReceiverPickupEvent.class,
            DeliveryReadyForTransportAssignmentEvent.class,
            DeliveryReadyForTransportConfirmation.class,
            DeliveryReceivedConfirmation.class,
            DeliveryReceivedRequest.class,
            DeliveryStatusChangedEvent.class,
            PoolingStationBoxAllocationConfirmation.class,
            PoolingStationBoxAllocationRequest.class,
            PoolingStationBoxClosedEvent.class,
            PoolingStationBoxDeallocationConfirmation.class,
            PoolingStationBoxDeallocationRequest.class,
            PoolingStationBoxOpenedEvent.class,
            PoolingStationBoxOpenRequest.class,
            PoolingStationBoxReadyForAllocationConfirmation.class,
            PoolingStationBoxReadyForAllocationRequest.class,
            PoolingStationBoxReadyForDeallocationConfirmation.class,
            PoolingStationBoxReadyForDeallocationRequest.class,
            ReceiverPickupAssignedEvent.class,
            ReceiverPickupPoolingStationBoxReadyForDeallocationRequest.class,
            ReceiverPickupReceivedConfirmation.class,
            ReceiverPickupReceivedPoolingStationRequest.class,
            TransportAlternativeAcceptedEvent.class,
            TransportAlternativeBundleCreateConfirmation.class,
            TransportAlternativeBundleExpiredEvent.class,
            TransportAlternativeBundleForSelectionEvent.class,
            TransportAlternativeRejectedEvent.class,
            TransportAlternativeSelectRequest.class,
            TransportAssignmentCreatedEvent.class,
            TransportDeliveredRequest.class,
            TransportDeliveredPoolingStationRequest.class,
            TransportDeliveredConfirmation.class,
            TransportDeliveredReceivedRequest.class,
            TransportDeliveredReceivedConfirmation.class,
            TransportPickupConfirmation.class,
            TransportUndoPickupConfirmation.class,
            TransportPickupPoolingStationBoxReadyForDeallocationRequest.class,
            TransportPickupPoolingStationRequest.class,
            TransportPickupRequest.class,
            TransportUndoPickupRequest.class})
    protected void logEvent(BaseEvent event, EventProcessingContext context) {
        super.logEvent(event, context);
    }

}
