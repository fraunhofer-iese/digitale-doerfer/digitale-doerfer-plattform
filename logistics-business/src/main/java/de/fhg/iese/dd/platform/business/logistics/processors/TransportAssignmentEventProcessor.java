/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryWaitingForReceivedConfirmationExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToDeliveryEvent;
import de.fhg.iese.dd.platform.business.logistics.events.MessageRelatedToTransportAssignmentEvent;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.PoolingStationBoxScannedReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeAcceptedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentStatusChangedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredOtherPersonRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredPlacementRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupPoolingStationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportPoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportUndoPickupRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportWaitingForDeliveryExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportWaitingForPickupExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.exceptions.CancelledByWrongCarrierException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveredByWrongCarrierException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveredToWrongLocationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.InvalidTransportAssignmentStatusException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PickedUpAtWrongLocationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PickedUpByWrongCarrierException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.TransportAssignmentNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IOpeningHoursService;
import de.fhg.iese.dd.platform.business.shared.waiting.services.IWaitingService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.Signature;

@EventProcessor
class TransportAssignmentEventProcessor extends BaseEventProcessor {

    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private TransportAssignmentStatusRecordRepository statusRecordRepository;
    @Autowired
    private IOpeningHoursService openingHoursService;
    @Autowired
    private IWaitingService waitingService;
    @Autowired
    private IDeliveryService deliveryService;

    @EventProcessing
    void handleTransportAlternativeAcceptedEvent(TransportAlternativeAcceptedEvent event, final EventProcessingContext context) {
        Person carrier = event.getCarrier();
        TransportAlternative transportAlternative = event.getTransportAlternative();
        Delivery delivery = event.getDelivery();
        TransportAssignment assignment = transportAssignmentService.createAssignmentFromAlternative(carrier, transportAlternative);

        //we are adding this status here to make it easier to generate the status history as it is needed by the clients
        newStatusRecord(TransportAssignmentStatus.WAITING_FOR_ASSIGNMENT, event, assignment);

        TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus =
                transportAlternativeService.getCurrentStatusRecord(
                        transportAlternative.getTransportAlternativeBundle());

        if (currentAlternativeBundleStatus.isParcelAlreadyAvailableForPickup()) {
            newStatusRecord(TransportAssignmentStatus.WAITING_FOR_PICKUP, event, assignment);
        } else {
            newStatusRecord(TransportAssignmentStatus.NOT_THERE_YET, event, assignment);
        }

        notify(TransportAssignmentCreatedEvent.builder()
                .delivery(delivery)
                .transportAssignment(assignment)
                .build(), context);

        //Reminder Pickup: 2h after opt in if after desiredPickupTime.start
        long timeReminderOptIn2h =
                Math.max(timeService.currentTimeMillisUTC(), assignment.getDesiredPickupTime().getStartTime()) +
                        TimeUnit.HOURS.toMillis(2L);
        waitingService.registerDeadline(
                new TransportWaitingForPickupExpiredEvent(
                        carrier,
                        assignment,
                        delivery,
                        "Es liegt noch eine Lieferung zur Abholung bei " +
                                assignment.getPickupAddress().getAddress().getName() + " bereit.")
                        .withDeadline(timeReminderOptIn2h));

        //Reminder Pickup: 2/1/0.5h before closing of shop/poolingstation
        long closingTime = -1;
        if(assignment.getPickupAddress().getAddressType()==ParcelAddressType.SHOP && assignment.getPickupAddress().getShop()!=null && assignment.getPickupAddress().getShop().getOpeningHours()!=null) {
            closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                    assignment.getPickupAddress().getShop().getOpeningHours(),
                    assignment.getDesiredPickupTime().getEndTime());
        } else if (assignment.getPickupAddress().getAddressType()==ParcelAddressType.POOLING_STATION && assignment.getPickupAddress().getPoolingStation()!=null && assignment.getPickupAddress().getPoolingStation().getOpeningHours()!=null) {
            closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                    assignment.getPickupAddress().getPoolingStation().getOpeningHours(),
                    assignment.getDesiredPickupTime().getEndTime());
        }
        if(closingTime > timeService.currentTimeMillisUTC()){
            long timeReminderClosing2h = closingTime-TimeUnit.HOURS.toMillis(2L);
            long timeReminderClosing1h = closingTime-TimeUnit.HOURS.toMillis(1L);
            long timeReminderClosing30m = closingTime-TimeUnit.MINUTES.toMillis(30L);

            if(timeReminderClosing2h > timeService.currentTimeMillisUTC()){
                waitingService.registerDeadline(
                    new TransportWaitingForPickupExpiredEvent(
                        carrier,
                        assignment,
                        delivery,
                        assignment.getPickupAddress().getAddress().getName()+" schließt in 2 Stunden. Bitte hole deine Lieferung dort ab.")
                    .withDeadline(timeReminderClosing2h));
            }
            if(timeReminderClosing1h > timeService.currentTimeMillisUTC()){
                waitingService.registerDeadline(
                    new TransportWaitingForPickupExpiredEvent(
                        carrier,
                        assignment,
                        delivery,
                        assignment.getPickupAddress().getAddress().getName()+" schließt in 1 Stunde. Bitte hole deine Lieferung dort ab.")
                    .withDeadline(timeReminderClosing1h));
            }
            if(timeReminderClosing30m > timeService.currentTimeMillisUTC()){
                waitingService.registerDeadline(
                    new TransportWaitingForPickupExpiredEvent(
                        carrier,
                        assignment,
                        delivery,
                        assignment.getPickupAddress().getAddress().getName()+" schließt in 30 Minuten. Bitte hole deine Lieferung dort ab.")
                      .withDeadline(timeReminderClosing30m));
            }
        }
    }

    @EventProcessing
    MessageRelatedToTransportAssignmentEvent handleTransportWaitingForPickupExpiredEvent(TransportWaitingForPickupExpiredEvent event) {
        TransportAssignmentStatus currentStatus =
                transportAssignmentService.getCurrentStatusRecord(event.getTransportAssignment()).getStatus();
        if (currentStatus == TransportAssignmentStatus.WAITING_FOR_PICKUP) {
            return new MessageRelatedToTransportAssignmentEvent(event.getTransportAssignment(), event.getMessage());
        } else {
            return null;
        }
    }

    @EventProcessing
    void handleTransportDeliveredRequest(TransportDeliveredRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();

        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportDeliveredReceivedRequest(TransportDeliveredReceivedRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();

        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportDeliveredOtherPersonRequest(TransportDeliveredOtherPersonRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();

        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportDeliveredPlacementRequest(TransportDeliveredPlacementRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();

        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportPickupRequest(TransportPickupRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        Person actualCarrier = event.getCarrier();
        Person assignedCarrier = assignment.getCarrier();
        Delivery delivery = event.getDelivery();

        if(! assignedCarrier.getId().equals(actualCarrier.getId())){

            throw new PickedUpByWrongCarrierException("The delivery " + delivery.getId() + " was picked up by " + actualCarrier.getId() + " instead of the actual carrier " + assignedCarrier.getId());
        }
        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportUndoPickupRequest(TransportUndoPickupRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleDeliveryReceivedConfirmation(DeliveryReceivedConfirmation event, final EventProcessingContext context) {
        Delivery delivery  = event.getDelivery();
        //we use the last transport assignment that goes directly to the receiver, since the receiver marked the delivery as received, so this is most likely the right one (even if the last one is cancelled)
        TransportAssignment finalAssignment = transportAssignmentService.getFinalTransportAssignment(delivery);
        if(finalAssignment==null) throw new TransportAssignmentNotFoundException("final assignment for delivery "+event.getDelivery().getId());

        dispatchEvent(finalAssignment, event, context);
    }

    @EventProcessing
    void handleTransportPoolingStationBoxReadyForAllocationRequest(TransportPoolingStationBoxReadyForAllocationRequest event, final EventProcessingContext context) {
        //check if the assignment fits with the pooling station
        //if yes, send out a request to start allocating the pooling stations
        TransportAssignment assignment = event.getTransportAssignment();

        Person actualCarrier = event.getCarrier();
        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        //check if the right carrier is trying to use the pooling station
        if(! assignment.getCarrier().getId().equals(actualCarrier.getId())){
            throw new DeliveredByWrongCarrierException("The delivery "+delivery.getId()+" is not intended to be delivered by this carrier "+actualCarrier.getId()+", the actual carrier is "+assignment.getCarrier().getId());
        }

        //check if this parcel should be delivered in this pooling station
        ParcelAddress deliveryAddress = assignment.getDeliveryAddress();
        if(deliveryAddress.getAddressType() != ParcelAddressType.POOLING_STATION ||
                ! deliveryAddress.getPoolingStation().getId().equals(poolingStation.getId())){
            throw new DeliveredToWrongLocationException("The delivery "+delivery.getId()+" is trying to be delivered to "+ poolingStation +" instead to "+ deliveryAddress);
        }

        dispatchEvent(assignment, event, context);
     }

    @EventProcessing
    void handleTransportDeliveredPoolingStationRequest(TransportDeliveredPoolingStationRequest event, final EventProcessingContext context) {
        //check if the assignment fits with the pooling station
        //if yes, send out a request to finalize allocating the pooling stations
        TransportAssignment assignment = event.getTransportAssignment();

        Person actualCarrier = event.getCarrier();
        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        //check if the right carrier is trying to use the pooling station
        if(! assignment.getCarrier().getId().equals(actualCarrier.getId())){
            throw new DeliveredByWrongCarrierException("The delivery "+delivery.getId()+" is not intended to be delivered by this carrier "+actualCarrier.getId()+", the actual carrier is "+assignment.getCarrier().getId());
        }

        //check if this parcel should be delivered in this pooling station
        ParcelAddress deliveryAddress = assignment.getDeliveryAddress();
        if(deliveryAddress.getAddressType() != ParcelAddressType.POOLING_STATION ||
                ! deliveryAddress.getPoolingStation().getId().equals(poolingStation.getId())){
            throw new DeliveredToWrongLocationException("The delivery "+delivery.getId()+" is trying to be delivered to "+ poolingStation +" instead to "+ deliveryAddress);
        }

        dispatchEvent(assignment, event, context);
    }

     @EventProcessing
     void handlePoolingStationBoxAllocationConfirmation(PoolingStationBoxAllocationConfirmation event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        PoolingStation poolingStation = event.getPoolingStationBox().getPoolingStation();

        if(assignment != null){
            //check if this parcel should be delivered in this pooling station
            ParcelAddress deliveryAddress = assignment.getDeliveryAddress();
            if(deliveryAddress.getAddressType() == ParcelAddressType.POOLING_STATION &&
                    deliveryAddress.getPoolingStation().getId().equals(poolingStation.getId())){
                dispatchEvent(assignment, event, context);
            }
            //do not throw exception here, since we get this event also without an assignment
        }
    }

    @EventProcessing
    void handleTransportPickupPoolingStationBoxReadyForDeallocationRequest(TransportPickupPoolingStationBoxReadyForDeallocationRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        Person actualCarrier = event.getCarrier();
        Person assignedCarrier = assignment.getCarrier();
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        //check if the right carrier is trying to use the pooling station
        if(! assignedCarrier.getId().equals(actualCarrier.getId())){

            throw new PickedUpByWrongCarrierException("The delivery " + delivery.getId() + " was picked up by " + actualCarrier.getId() + " instead of the actual carrier " + assignedCarrier.getId());
        }

        //check if this parcel should be picked up at this pooling station
        ParcelAddress pickupAddress = assignment.getPickupAddress();
        if(pickupAddress.getAddressType() != ParcelAddressType.POOLING_STATION ||
                ! pickupAddress.getPoolingStation().getId().equals(poolingStation.getId())){
            throw new PickedUpAtWrongLocationException("The delivery "+delivery.getId()+" is trying to be picked up at "+ poolingStation +", but the actual pickup location is "+ pickupAddress);
        }
        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handleTransportPickupPoolingStationRequest(TransportPickupPoolingStationRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        Person actualCarrier = event.getCarrier();
        Person assignedCarrier = assignment.getCarrier();
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        //check if the right carrier is trying to use the pooling station
        if(! assignedCarrier.getId().equals(actualCarrier.getId())){

            throw new PickedUpByWrongCarrierException("The delivery " + delivery.getId() + " was picked up by " + actualCarrier.getId() + " instead of the actual carrier " + assignedCarrier.getId());
        }

        //check if this parcel should be picked up at this pooling station
        ParcelAddress pickupAddress = assignment.getPickupAddress();
        if(pickupAddress.getAddressType() != ParcelAddressType.POOLING_STATION ||
                ! pickupAddress.getPoolingStation().getId().equals(poolingStation.getId())){
            throw new PickedUpAtWrongLocationException("The delivery "+delivery.getId()+" is trying to be picked up at "+ poolingStation +", but the actual pickup location is "+ pickupAddress);
        }
        dispatchEvent(assignment, event, context);
    }

    @EventProcessing
    void handlePoolingStationBoxDeallocationConfirmation(PoolingStationBoxDeallocationConfirmation event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        PoolingStation poolingStation = event.getPoolingStationBox().getPoolingStation();

        if(assignment != null){
            //check if this parcel should be picked up from this pooling station
            ParcelAddress pickupAddress = assignment.getPickupAddress();
            if(pickupAddress.getAddressType() == ParcelAddressType.POOLING_STATION &&
                    pickupAddress.getPoolingStation().getId().equals(poolingStation.getId())){
                dispatchEvent(assignment, event, context);
            }
            //do not throw exception here, since we get this event also without an assignment
        }
    }

    @EventProcessing
    void handleTransportAssignmentCancelRequest(TransportAssignmentCancelRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        Person actualCarrier = event.getCarrier();
        Person assignedCarrier = assignment.getCarrier();

        if(!actualCarrier.getId().equals(assignedCarrier.getId())){
            throw new CancelledByWrongCarrierException("The TransportAssignment "+assignment.getId()+" can not be cancelled by "+actualCarrier.getId()+", the assigned carrier is "+assignedCarrier.getId()+"!");
        }

        dispatchEvent(assignment, event, context);
    }

    private void dispatchEvent(TransportAssignment assignment, BaseEvent event, final EventProcessingContext context) {
        TransportAssignmentStatus currentStatus = transportAssignmentService.getCurrentStatusRecord(assignment).getStatus();
        TransportAssignmentStatusRecord newStatus;
        switch (currentStatus) {
        case NOT_THERE_YET:
            if(event instanceof TransportPickupRequest){
                if(assignment.getPickupAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new PickedUpAtWrongLocationException("The pickup address is a pooling station, can not mark assignment "+assignment.getId()+" as picked up.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.IN_DELIVERY, event, assignment);
                handlePickup(assignment, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportAssignmentCancelRequest){
                newStatus = newStatusRecord(TransportAssignmentStatus.CANCELLED, event, assignment);
                handleCancellation(assignment, (TransportAssignmentCancelRequest)event, context, currentStatus, newStatus);
                return;
            }
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case WAITING_FOR_PICKUP:
            if(event instanceof TransportPickupRequest){
                if(assignment.getPickupAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new PickedUpAtWrongLocationException("The pickup address is a pooling station, can not mark assignment "+assignment.getId()+" as picked up.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.IN_DELIVERY, event, assignment);
                handlePickup(assignment, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportPickupPoolingStationBoxReadyForDeallocationRequest){
                handlePickupPoolingStationReadyForDeallocationRequest(assignment, (TransportPickupPoolingStationBoxReadyForDeallocationRequest)event, context);
                return;
            }
            if(event instanceof TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest){
                handlePoolingStationBoxScannedReadyForDeallocationRequest(assignment, (TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest)event, context);
                return;
            }
            if(event instanceof TransportPickupPoolingStationRequest){
                handlePickupPoolingStationRequest(assignment, (TransportPickupPoolingStationRequest)event, context);
                return;
            }
            if(event instanceof PoolingStationBoxDeallocationConfirmation){
                newStatus = newStatusRecord(TransportAssignmentStatus.IN_DELIVERY, event, assignment);
                handlePoolingStationBoxDeallocationConfirmation(assignment, (PoolingStationBoxDeallocationConfirmation)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportAssignmentCancelRequest){
                newStatus = newStatusRecord(TransportAssignmentStatus.CANCELLED, event, assignment);
                handleCancellation(assignment, (TransportAssignmentCancelRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof DeliveryReceivedConfirmation){
                //mark it is in delivery since this seemed to have happened before
                newStatus = newStatusRecord(TransportAssignmentStatus.IN_DELIVERY, event, assignment);
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                handleReceivedBeforePickedUp(assignment, context, currentStatus, newStatus);
                return;
            }
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case IN_DELIVERY:
            if(event instanceof TransportPickupRequest){
                //idempotent
                if(assignment.getPickupAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new PickedUpAtWrongLocationException("The pickup address is a pooling station, can not mark assignment "+assignment.getId()+" as picked up.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.IN_DELIVERY, event, assignment);
                handlePickupWhileInDelivery(assignment, context);
                return;
            }
            if(event instanceof TransportUndoPickupRequest){
                newStatus = newStatusRecord(TransportAssignmentStatus.WAITING_FOR_PICKUP, event, assignment);
                handleUndoPickup(assignment, context, currentStatus, newStatus, ((TransportUndoPickupRequest) event).getReason());
                return;
            }
            if(event instanceof TransportDeliveredRequest){
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                handleDelivered(assignment, (TransportDeliveredRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportDeliveredPlacementRequest){
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                handleDeliveredPlacement(assignment, (TransportDeliveredPlacementRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportDeliveredOtherPersonRequest){
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                handleDeliveredOtherPerson(assignment, (TransportDeliveredOtherPersonRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportDeliveredReceivedRequest){
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                // the delivery is delivered to the receiver, so it is directly received
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                newStatus = newStatusRecord(TransportAssignmentStatus.RECEIVED, event, assignment);
                handleDeliveredReceived(assignment, (TransportDeliveredReceivedRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof DeliveryReceivedConfirmation){
                newStatus = newStatusRecord(TransportAssignmentStatus.RECEIVED, event, assignment);
                handleReceivedWhileInDelivery(assignment, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof TransportPoolingStationBoxReadyForAllocationRequest){
                handlePoolingStationBoxReadyForAllocationRequest((TransportPoolingStationBoxReadyForAllocationRequest)event, context);
                return;
            }
            if(event instanceof PoolingStationBoxScannedReadyForAllocationRequest){
                handlePoolingStationBoxScannedReadyForAllocationRequest(assignment, (PoolingStationBoxScannedReadyForAllocationRequest)event, context);
                return;
            }
            if(event instanceof TransportDeliveredPoolingStationRequest){
                handlePoolingStationBoxDeliveredRequest(assignment, (TransportDeliveredPoolingStationRequest)event, context);
                return;
            }
            if(event instanceof PoolingStationBoxAllocationConfirmation){
                newStatus = newStatusRecord(TransportAssignmentStatus.DELIVERED, event, assignment);
                handlePoolingStationBoxAllocationConfirmation(assignment, (PoolingStationBoxAllocationConfirmation)event, context, currentStatus, newStatus);
                return;
            }
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case DELIVERED:
            if(event instanceof TransportDeliveredRequest){
                //idempotent
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                handleDeliveredWhileDelivered(assignment, (TransportDeliveredRequest)event, context);
                return;
            }
            if(event instanceof TransportDeliveredReceivedRequest){
                newStatus = newStatusRecord(TransportAssignmentStatus.RECEIVED, event, assignment);
                if(assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION){
                    throw new DeliveredToWrongLocationException("The delivery address is a pooling station, can not mark assignment "+assignment.getId()+" as delivered.");
                }
                handleReceivedWhileDelivered(assignment, (TransportDeliveredReceivedRequest)event, context, currentStatus, newStatus);
                return;
            }
            if(event instanceof DeliveryReceivedConfirmation){
                newStatus = newStatusRecord(TransportAssignmentStatus.RECEIVED, event, assignment);
                handleReceivedWhileDelivered(assignment, context, currentStatus, newStatus);
                return;
            }
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case RECEIVED:
            if(event instanceof DeliveryReceivedConfirmation){
                newStatus = newStatusRecord(TransportAssignmentStatus.RECEIVED, event, assignment);
                handleReceivedWhileReceived(assignment, context, currentStatus, newStatus);
                return;
            }
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case CANCELLED:
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        case REJECTED:
            handleInvalidStatusTransition(assignment, event, currentStatus);
            break;
        default:
            handleInvalidStatusTransition(assignment, event, currentStatus);
        }
        handleInvalidStatusTransition(assignment, event, currentStatus);
    }

    private void handlePickup(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation pickupConfirmation = transportAssignmentService.createPickedUpConfirmation(assignment, CreatorType.CARRIER, HandoverType.PERSONALLY, "");
        notify(new TransportPickupConfirmation(assignment.getCarrier(), assignment.getDelivery(), assignment, pickupConfirmation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

        //Reminder Delivery: 1h before closing time of pooling station
        if(assignment.getDeliveryAddress().getAddressType()==ParcelAddressType.POOLING_STATION && assignment.getDeliveryAddress().getPoolingStation()!=null && assignment.getDeliveryAddress().getPoolingStation().getOpeningHours()!=null) {
            long closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                    assignment.getDeliveryAddress().getPoolingStation().getOpeningHours(),
                    assignment.getDesiredDeliveryTime().getEndTime());
            long timeReminderClosing1h = closingTime - TimeUnit.HOURS.toMillis(1L);
            if (timeReminderClosing1h > timeService.currentTimeMillisUTC()) {
                waitingService.registerDeadline(
                        new TransportWaitingForDeliveryExpiredEvent(
                                assignment.getCarrier(),
                                assignment,
                                assignment.getDelivery(),
                                assignment.getDeliveryAddress().getAddress().getName() +
                                        " schließt in 1 Stunde. Bitte bringe deine Lieferung dort hin.")
                                .withDeadline(timeReminderClosing1h));
            }
        }
        //Reminder Delivery: 1h before desiredDeliveryTime.end
        long timeReminderDelivery1h = assignment.getDesiredDeliveryTime().getEndTime() - TimeUnit.HOURS.toMillis(1L);
        if(timeReminderDelivery1h>timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new TransportWaitingForDeliveryExpiredEvent(
                    assignment.getCarrier(),
                    assignment,
                    assignment.getDelivery(),
                    "Du hast noch eine in einer Stunde fällige Lieferung. Bitte bringe diese zu "+assignment.getDeliveryAddress().getAddress().getName()+".")
                .withDeadline(timeReminderDelivery1h));
        }
    }

    private void handleUndoPickup(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus, String reason) {

        notify(new TransportUndoPickupConfirmation(assignment.getDelivery(), assignment, reason), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handlePickupWhileInDelivery(TransportAssignment assignment, final EventProcessingContext context) {

        TransportConfirmation transportConfirmation = transportAssignmentService.createPickedUpConfirmation(assignment, CreatorType.CARRIER, HandoverType.PERSONALLY, "");
        notify(new TransportPickupConfirmation(assignment.getCarrier(), assignment.getDelivery(), assignment, transportConfirmation), context);
    }

    private void handleDelivered(TransportAssignment assignment, TransportDeliveredRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService
                .createDeliveredConfirmation(
                    assignment,
                    event.getCreator(),
                    event.getHandover(),
                    event.getHandoverNotes());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

        String senderName = assignment.getDelivery().getSender().getFullName();

        waitingService.registerDeadline(
            new DeliveryWaitingForReceivedConfirmationExpiredEvent(
                assignment.getDelivery(),
                "Hast du deine Lieferung von "+senderName+" bekommen? Bitte bestätige noch den Erhalt.")
            .withDeadline(timeService.currentTimeMillisUTC()+TimeUnit.HOURS.toMillis(6)));
    }

    private void handleDeliveredPlacement(TransportAssignment assignment, TransportDeliveredPlacementRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService
                .createDeliveredPlacementConfirmation(
                    assignment,
                    event.getPlacementDescription());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

        String senderName = assignment.getDelivery().getSender().getFullName();

        waitingService.registerDeadline(
            new DeliveryWaitingForReceivedConfirmationExpiredEvent(
                assignment.getDelivery(),
                "Hast du deine Lieferung von "+senderName+" am Ablageort abgeholt? Bitte bestätige noch den Erhalt.")
            .withDeadline(timeService.currentTimeMillisUTC()+TimeUnit.HOURS.toMillis(6)));
    }

    private void handleDeliveredOtherPerson(TransportAssignment assignment, TransportDeliveredOtherPersonRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        //TODO handle Signature
        Signature signature = event.getSignature();
        TransportConfirmation deliveryConfirmation = transportAssignmentService
                .createDeliveredOtherPersonConfirmation(
                    assignment,
                    event.getAcceptorName(),
                    signature.getSignature());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

        String senderName = assignment.getDelivery().getSender().getFullName();

        waitingService.registerDeadline(
            new DeliveryWaitingForReceivedConfirmationExpiredEvent(
                assignment.getDelivery(),
                "Hast du deine Lieferung von "+senderName+" von "+event.getAcceptorName()+" abgeholt? Bitte bestätige noch den Erhalt.")
            .withDeadline(timeService.currentTimeMillisUTC()+TimeUnit.HOURS.toMillis(6)));
    }

    private void handleDeliveredReceived(TransportAssignment assignment, TransportDeliveredReceivedRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        //TODO handle Signature
        Signature signature = event.getSignature();
        TransportConfirmation receivedConfirmation = transportAssignmentService
                .createReceivedConfirmation(
                    assignment,
                    CreatorType.CARRIER,
                    HandoverType.PERSONALLY,
                    signature.getSignature());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(TransportDeliveredReceivedConfirmation.builder()
            .transportAssignment(assignment)
            .delivery(assignment.getDelivery())
            .receivedConfirmation(receivedConfirmation)
            .isFinal(isFinal)
            .isInPoolingStation(isInPoolingStation)
            .build(), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

    }

    private void handleDeliveredWhileDelivered(TransportAssignment assignment, TransportDeliveredRequest event, final EventProcessingContext context) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService.createDeliveredConfirmation(assignment, event.getCreator(), event.getHandover(), event.getHandoverNotes());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
    }

    private void handleReceivedWhileDelivered(TransportAssignment assignment, TransportDeliveredReceivedRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {
        //this is an unusual case, the transport was delivered, but now the event that delivers and receives it is triggered
        //we just set it to received, we might have two confirmations in this case
        //TODO handle Signature
        Signature signature = event.getSignature();
        TransportConfirmation receivedConfirmation = transportAssignmentService
                .createReceivedConfirmation(
                    assignment,
                    CreatorType.CARRIER,
                    HandoverType.PERSONALLY,
                    signature.getSignature());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(TransportDeliveredReceivedConfirmation.builder()
            .transportAssignment(assignment)
            .delivery(assignment.getDelivery())
            .receivedConfirmation(receivedConfirmation)
            .isFinal(isFinal)
            .isInPoolingStation(isInPoolingStation)
            .build(), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handleReceivedBeforePickedUp(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        //we create a pickup confirmation manually, but do not send it out
        transportAssignmentService.createPickedUpConfirmation(assignment, CreatorType.RECEIVER, HandoverType.UNKNOWN, "");

        TransportConfirmation deliveryConfirmation = transportAssignmentService.createDeliveredConfirmation(assignment, CreatorType.RECEIVER, HandoverType.UNKNOWN, "");
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        //this is especially relevant for the carrier
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handleReceivedWhileInDelivery(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService.createDeliveredConfirmation(assignment, CreatorType.RECEIVER, HandoverType.UNKNOWN, "");
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        //this is especially relevant for the carrier
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handlePoolingStationBoxReadyForAllocationRequest(TransportPoolingStationBoxReadyForAllocationRequest event, final EventProcessingContext context) {

        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxReadyForAllocationRequest boxReadyForAllocationRequest = new PoolingStationBoxReadyForAllocationRequest(delivery, poolingStation);
        notify(boxReadyForAllocationRequest, context);
    }

    private void handlePoolingStationBoxDeliveredRequest(TransportAssignment assignment, TransportDeliveredPoolingStationRequest event, final EventProcessingContext context) {

        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxAllocationRequest allocationRequest = new PoolingStationBoxAllocationRequest(delivery, assignment, poolingStation);
        notify(allocationRequest, context);
    }

    private void handlePoolingStationBoxAllocationConfirmation(TransportAssignment assignment, PoolingStationBoxAllocationConfirmation event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService.createDeliveredConfirmation(assignment, CreatorType.POOLING_STATION, HandoverType.PERSONALLY, "Delivered to pooling station box: " + event.getPoolingStationBox().getId());
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handlePickupPoolingStationReadyForDeallocationRequest(TransportAssignment assignment, TransportPickupPoolingStationBoxReadyForDeallocationRequest event, final EventProcessingContext context) {

        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxReadyForDeallocationRequest deallocationRequest = new PoolingStationBoxReadyForDeallocationRequest(delivery, poolingStation, assignment);
        notify(deallocationRequest, context);
    }

    private void handlePickupPoolingStationRequest(TransportAssignment assignment, TransportPickupPoolingStationRequest event, final EventProcessingContext context) {

        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxDeallocationRequest deallocationRequest = new PoolingStationBoxDeallocationRequest(delivery, poolingStation, assignment);
        notify(deallocationRequest, context);
    }

    private void handlePoolingStationBoxDeallocationConfirmation(TransportAssignment assignment, PoolingStationBoxDeallocationConfirmation event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation pickupConfirmation = transportAssignmentService.createPickedUpConfirmation(assignment, CreatorType.POOLING_STATION, HandoverType.PERSONALLY, "Picked up at pooling station: " +event.getPoolingStationBox().getPoolingStation().getId());
        notify(new TransportPickupConfirmation(assignment.getCarrier(), assignment.getDelivery(), assignment, pickupConfirmation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

        //Reminder Delivery: 1h before closing time of pooling station
        if(assignment.getDeliveryAddress().getAddressType()==ParcelAddressType.POOLING_STATION && assignment.getDeliveryAddress().getPoolingStation()!=null && assignment.getDeliveryAddress().getPoolingStation().getOpeningHours()!=null) {
            long closingTime = openingHoursService.getLastFutureClosingTimeOnSameOrFutureDay(
                    assignment.getDeliveryAddress().getPoolingStation().getOpeningHours(),
                    assignment.getDesiredDeliveryTime().getEndTime());
            long timeReminderClosing1h = closingTime - TimeUnit.HOURS.toMillis(1L);
            if (timeReminderClosing1h > timeService.currentTimeMillisUTC()) {
                waitingService.registerDeadline(
                        new TransportWaitingForDeliveryExpiredEvent(
                                assignment.getCarrier(),
                                assignment,
                                assignment.getDelivery(),
                                assignment.getDeliveryAddress().getAddress().getName() +
                                        " schließt in 1 Stunde. Bitte bringe deine Lieferung dort hin.")
                                .withDeadline(timeReminderClosing1h));
            }
        }
        //Reminder Delivery: 1h before desiredDeliveryTime.end
        long timeReminderDelivery1h = assignment.getDesiredDeliveryTime().getEndTime() - TimeUnit.HOURS.toMillis(1L);
        if(timeReminderDelivery1h>timeService.currentTimeMillisUTC()){
            waitingService.registerDeadline(
                new TransportWaitingForDeliveryExpiredEvent(
                    assignment.getCarrier(),
                    assignment,
                    assignment.getDelivery(),
                    "Du hast noch eine in einer Stunde fällige Lieferung. Bitte bringe diese zu "+assignment.getDeliveryAddress().getAddress().getName()+".")
                .withDeadline(timeReminderDelivery1h));
        }

    }

    private void handleReceivedWhileDelivered(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        TransportConfirmation deliveryConfirmation = transportAssignmentService.createDeliveredConfirmation(assignment, CreatorType.RECEIVER, HandoverType.UNKNOWN, "");
        boolean isFinal = assignment.getKind() == TransportKind.RECEIVER;
        boolean isInPoolingStation = assignment.getDeliveryAddress().getAddressType() == ParcelAddressType.POOLING_STATION;
        notify(new TransportDeliveredConfirmation(assignment.getDelivery(), assignment, deliveryConfirmation, isFinal, isInPoolingStation), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);
    }

    private void handleReceivedWhileReceived(TransportAssignment assignment, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

    }

    private TransportAssignmentStatusRecord newStatusRecord(TransportAssignmentStatus newStatus, BaseEvent event, TransportAssignment assignment){
        return transportAssignmentService.setCurrentStatus(assignment, newStatus, event);
    }

    private static void handleInvalidStatusTransition(TransportAssignment assignment, BaseEvent event,
            TransportAssignmentStatus currentStatus){
        throw new InvalidTransportAssignmentStatusException("The current status of transportassignment "+assignment.getId()+" is "
                    +currentStatus.toString()+", the event "+event.getClass().getSimpleName()+" is invalid in this status.").withDetail(assignment.getId());
    }

    @EventProcessing
    void handlePoolingStationBoxScannedReadyForAllocationRequest(PoolingStationBoxScannedReadyForAllocationRequest event, final EventProcessingContext context) {
        //check if the assignment fits with the pooling station
        //if yes, send out a request to start allocating the pooling stations
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        Optional<TransportAssignment> assignmentOptional =
                transportAssignmentService.findAllByDelivery(delivery)
                        .stream()
                        .filter(ta -> Objects.equals(ta.getDeliveryAddress().getPoolingStation(), poolingStation))
                        .min((ta1, ta2) -> Long.compare(ta2.getCreated(), ta1.getCreated()));

        if (assignmentOptional.isEmpty()) {
            throw new TransportAssignmentNotFoundException(
                    "for delivery " + delivery.getId() + " with delivery pooling station " + poolingStation.getId());
        }

        dispatchEvent(assignmentOptional.get(), event, context);
    }

    private void handlePoolingStationBoxScannedReadyForAllocationRequest(TransportAssignment assignment,
            PoolingStationBoxScannedReadyForAllocationRequest event, final EventProcessingContext context) {
        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxReadyForAllocationRequest boxReadyForAllocationRequest = new PoolingStationBoxReadyForAllocationRequest(delivery, poolingStation);
        notify(boxReadyForAllocationRequest, context);

    }

    private void handlePoolingStationBoxScannedReadyForDeallocationRequest(TransportAssignment assignment,
            TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest event, final EventProcessingContext context) {
        PoolingStation poolingStation = event.getPoolingStation();
        Delivery delivery = event.getDelivery();

        PoolingStationBoxReadyForDeallocationRequest boxReadyForDeallocationRequest = new PoolingStationBoxReadyForDeallocationRequest(delivery, poolingStation, assignment);
        notify(boxReadyForDeallocationRequest, context);

    }

    @EventProcessing
    void handleTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest event, final EventProcessingContext context) {
        TransportAssignment assignment = event.getTransportAssignment();
        Delivery delivery = event.getDelivery();
        PoolingStation poolingStation = event.getPoolingStation();

        //check if this parcel should be picked up at this pooling station
        ParcelAddress pickupAddress = assignment.getPickupAddress();
        if(pickupAddress.getAddressType() != ParcelAddressType.POOLING_STATION ||
                ! pickupAddress.getPoolingStation().getId().equals(poolingStation.getId())){
            throw new PickedUpAtWrongLocationException("The delivery "+delivery.getId()+" is trying to be picked up at "+ poolingStation +", but the actual pickup location is "+ pickupAddress);
        }
        dispatchEvent(assignment, event, context);
    }

    private void handleCancellation(TransportAssignment assignment, TransportAssignmentCancelRequest event, final EventProcessingContext context, TransportAssignmentStatus currentStatus, TransportAssignmentStatusRecord newStatus) {

        notify(new TransportAssignmentCancelConfirmation(assignment.getCarrier(), assignment, assignment.getDelivery(), event.getCancelationReason()), context);
        notify(new TransportAssignmentStatusChangedEvent(assignment, newStatus.getStatus(), currentStatus), context);

    }

    @EventProcessing
    MessageRelatedToTransportAssignmentEvent handleTransportWaitingForDeliveryExpiredEvent(TransportWaitingForDeliveryExpiredEvent event) {
        TransportAssignmentStatus currentStatus =
                transportAssignmentService.getCurrentStatusRecord(event.getTransportAssignment()).getStatus();
        if (currentStatus == TransportAssignmentStatus.IN_DELIVERY) {
            return new MessageRelatedToTransportAssignmentEvent(event.getTransportAssignment(), event.getMessage());
        }else{
            return null;
        }
    }

    @EventProcessing
    MessageRelatedToDeliveryEvent handleDeliveryWaitingForReceivedConfirmationExpiredEvent(DeliveryWaitingForReceivedConfirmationExpiredEvent event) {
        Delivery delivery = event.getDelivery();
        DeliveryStatus currentStatus = deliveryService.getCurrentStatusRecord(delivery).getStatus();
        if (currentStatus == DeliveryStatus.DELIVERED) {
            return new MessageRelatedToDeliveryEvent(delivery, event.getMessage());
        }else{
            return null;
        }
    }

}
