/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.motivation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleLogisticsTransportDifferentDone extends BaseAchievementRule<TransportDeliveredConfirmation> {

    private static final String ACHIEVEMENT_ID =          "5bafaee3-d292-449d-9754-712d8d7a3f63";
    private static final String ACHIEVEMENT_LEVEL_ID_10 = "8a30497b-0547-4d3f-a21e-32f26334e7be";

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Override
    public String getName() {
        return "logistics.score.transport.done.2";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(TransportDeliveredConfirmation event) {
        TransportAssignment transportAssignment = event.getTransportAssignment();
        if(transportAssignment == null || transportAssignment.getDeliveryAddress() == null
                || transportAssignment.getDeliveryAddress().getAddressType() != ParcelAddressType.PRIVATE) {
            return null;
        }
        Person carrier = transportAssignment.getCarrier();
        long timestamp = event.getCreated();

        int numberOfTransportsToDistinctPersonsDone = transportAssignmentService.getNumberOfTransportAssignmentsCompletedToDistinctPersons(carrier);

        if (numberOfTransportsToDistinctPersonsDone == 10) {
            return Collections.singletonList(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, carrier, timestamp));
        } else {
            return null;
        }
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        TransportAssignment lastCompletedAssignment = transportAssignmentService.getLatestCompletedTransportAssignment(person);
        long timestamp = lastCompletedAssignment == null ? timeService.currentTimeMillisUTC() : lastCompletedAssignment.getCreated();

        int numberOfTransportsToDistinctPersonsDone = transportAssignmentService.getNumberOfTransportAssignmentsCompletedToDistinctPersons(person);

        if (numberOfTransportsToDistinctPersonsDone >= 10) {
           return Collections.singletonList(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, person, timestamp));
        }

        return Collections.emptyList();
    }

    @Override
    public Class<TransportDeliveredConfirmation> getRelevantEvent() {
        return TransportDeliveredConfirmation.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Logistic.Transport.Distinct")
                .description("Beliefere unterschiedliche Personen um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("LieferBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel10 = AchievementLevel.builder()
                .name("Octopus")
                .description("Hast du 'nen heimlichen Zwilling? Oder irgendwo noch ein paar zusätzliche Arme versteckt?")
                .challengeDescription("Beliefere zehn verschiedene Personen.")
                .icon(createIconFromDefault("octopus.png"))
                .iconNotAchieved(createIconFromDefault("octopus_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel10.setId(ACHIEVEMENT_LEVEL_ID_10);
        achievementLevel10 = achievementService.save(achievementLevel10);

        achievement.setAchievementLevels(Collections.singleton(achievementLevel10));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_10);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
