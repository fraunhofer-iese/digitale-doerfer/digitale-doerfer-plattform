/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.AuthorizationException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class TransportNotAssignedToCurrentPersonException extends AuthorizationException {

    private static final long serialVersionUID = -6679059616993218200L;

    public TransportNotAssignedToCurrentPersonException() {
        super(ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON.getExplanation());
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON;
    }

}
