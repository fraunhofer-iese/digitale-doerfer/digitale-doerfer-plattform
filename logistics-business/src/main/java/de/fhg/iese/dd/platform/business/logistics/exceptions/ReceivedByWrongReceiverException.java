/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.AuthorizationException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class ReceivedByWrongReceiverException extends AuthorizationException {

    private static final long serialVersionUID = -5100521819216916517L;

    public ReceivedByWrongReceiverException(String message) {
        super(message);
    }

    public ReceivedByWrongReceiverException(String deliveryId, String wrongReceiverId) {
        super("The delivery "+deliveryId+" cannot be marked as received by "+wrongReceiverId+" because it is the wrong receiver");
        setDetail(deliveryId);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.WRONG_RECEIVER_RECEIVED;
    }

}
