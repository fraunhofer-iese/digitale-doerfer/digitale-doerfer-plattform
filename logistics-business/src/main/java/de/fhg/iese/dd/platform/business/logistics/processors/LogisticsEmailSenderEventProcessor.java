/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.time.Duration;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.LogisticsEmailTemplate;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryStatusChangedEvent;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.config.LogisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.logistics.feature.LogisticsShopFeature;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class LogisticsEmailSenderEventProcessor extends BaseEventProcessor {

    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private ITransportAssignmentService transportAssignmentService;
    @Autowired
    private LogisticsConfig logisticsConfig;
    @Autowired
    private IAuthorizationService authorizationService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IAppService appService;

    @EventProcessing
    private void handleDeliveryStatusChangedEvent(DeliveryStatusChangedEvent event) {
        Set<DeliveryStatus> relevantStatus = EnumSet.of(
            DeliveryStatus.DELIVERED,
            DeliveryStatus.DELIVERED_POOLING_STATION,
            DeliveryStatus.RECEIVED);
        if(! relevantStatus.contains(event.getNewStatus())){
            return;
        }
        try {
            Delivery delivery = event.getDelivery();
            Person receiverPerson = null;
            Shop receiverShop = null;
            String receiverEmail = null;
            String receiverName = null;

            if(delivery.getReceiver().getParticipantType() == LogisticsParticipantType.PRIVATE){
                receiverPerson = delivery.getReceiver().getPerson();
                receiverEmail = receiverPerson.getEmail();
                receiverName = receiverPerson.getFullName();
            }
            if(delivery.getReceiver().getParticipantType() == LogisticsParticipantType.SHOP){
                receiverShop = delivery.getReceiver().getShop();
                receiverEmail = receiverShop.getEmail();
                receiverName = receiverShop.getName();
            }
            Tenant tenant = delivery.getTenant();
            Person contactPerson = roleService.getDefaultContactPersonForTenant(tenant);
            Person senderPerson = null;
            Shop senderShop = null;
            if(delivery.getSender().getParticipantType() == LogisticsParticipantType.PRIVATE){
                senderPerson = delivery.getSender().getPerson();
            }
            if(delivery.getSender().getParticipantType() == LogisticsParticipantType.SHOP){
                senderShop = delivery.getSender().getShop();
            }
            TransportAssignment finalTransportAssignment = transportAssignmentService.getFinalTransportAssignment(delivery);

            String fromEmailAddress = logisticsConfig.getSenderEmailAddressShop();
            Map<String, Object> templateModel = new HashMap<>();
            templateModel.put("acknowledgeUrl", buildAcknowledgeUrl(delivery, event.getDeliveredConfirmation(),
                    logisticsConfig.getDeliveryReceivedMailUrl(),
                    authorizationService.generateAuthToken(
                            new DeliveryReceivedRequest(delivery, receiverPerson),
                            Duration.ofDays(30)),
                    featureService.getFeature(LogisticsShopFeature.class,
                            FeatureTarget.of(delivery.getTenant(),
                                    appService.findById(LogisticsConstants.LIEFERBAR_APP_ID)))));
            templateModel.put("senderShop", senderShop);
            templateModel.put("senderPerson", senderPerson);
            templateModel.put("receiverShop", receiverShop);
            templateModel.put("receiverPerson", receiverPerson);
            templateModel.put("delivery", delivery);
            templateModel.put("transportAssignment", finalTransportAssignment);
            templateModel.put("receivedConfirmation", event.getReceivedConfirmation());
            templateModel.put("deliveredConfirmation", event.getDeliveredConfirmation());
            templateModel.put("contactPerson", contactPerson);

            switch (event.getNewStatus()) {
                case DELIVERED: {
                    String text = null;
                    String subject = null;
                    TransportConfirmation deliveredConfirmation = event.getDeliveredConfirmation();
                    if (deliveredConfirmation != null) {
                        switch (deliveredConfirmation.getHandover()) {
                            case OTHER_PERSON:
                                text = emailSenderService.createTextWithTemplate(
                                        LogisticsEmailTemplate.DELIVERY_DELIVERED_OTHER_PERSON, templateModel);
                                subject = "Deine Bestellung ist bei einem Nachbar abgegeben worden!";
                                break;
                            case PERSONALLY:
                                text = emailSenderService.createTextWithTemplate(
                                        LogisticsEmailTemplate.DELIVERY_DELIVERED_RECEIVER, templateModel);
                                subject = "Deine Bestellung ist bei Dir angekommen!";
                                break;
                            case PLACEMENT:
                                text = emailSenderService.createTextWithTemplate(
                                        LogisticsEmailTemplate.DELIVERY_DELIVERED_PLACEMENT, templateModel);
                                subject = "Deine Bestellung ist abgelegt worden!";
                                break;
                            default:
                                break;
                        }
                    }
                    if (text == null) {
                        text = emailSenderService.createTextWithTemplate(
                                LogisticsEmailTemplate.DELIVERY_DELIVERED_UNKNOWN, templateModel);
                        subject = "Deine Bestellung ist abgegeben worden!";
                    }
                    emailSenderService.sendEmail(fromEmailAddress, receiverName, receiverEmail, subject, text,
                            Collections.emptyList(), true);
                    break;
                }
                case DELIVERED_POOLING_STATION: {
                    String text = emailSenderService.createTextWithTemplate(
                            LogisticsEmailTemplate.DELIVERY_DELIVERED_POOLING_STATION_RECEIVER, templateModel);
                    String subject = "Deine Bestellung ist abgegeben worden!";
                    emailSenderService.sendEmail(fromEmailAddress, receiverName, receiverEmail, subject, text,
                            Collections.emptyList(), true);
                    break;
                }
                case RECEIVED: {
                    String text = emailSenderService.createTextWithTemplate(
                            LogisticsEmailTemplate.DELIVERY_RECEIVED_RECEIVER, templateModel);
                    String subject = "Deine Bestellung ist bei Dir angekommen!";
                    emailSenderService.sendEmail(fromEmailAddress, receiverName, receiverEmail, subject, text,
                            Collections.emptyList(), true);
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {
            log.error("Failed to send Email for DeliveryStatusChangedEvent "+event.getLogMessage(), e);
        }
    }

    static String buildAcknowledgeUrl(Delivery delivery, TransportConfirmation deliveredConfirmation,
            String deliveryReceivedMailUrl, String mailToken, LogisticsShopFeature feature) {

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(feature.getShopUrl())
                .path(deliveryReceivedMailUrl)
                .queryParam("deliveryId", delivery.getId())
                .queryParam("mailToken", mailToken);
        if (deliveredConfirmation != null){
            if (deliveredConfirmation.getHandover() == HandoverType.OTHER_PERSON) {
                uriBuilder.queryParam("deliveryType", HandoverType.OTHER_PERSON.name());
                uriBuilder.queryParam("deliveryInfo", deliveredConfirmation.getAcceptorName());
            }
            if (deliveredConfirmation.getHandover() == HandoverType.PLACEMENT) {
                uriBuilder.queryParam("deliveryType", HandoverType.PLACEMENT.name());
                uriBuilder.queryParam("deliveryInfo", deliveredConfirmation.getNotes());
            }
            if (deliveredConfirmation.getHandover() == HandoverType.PERSONALLY) {
                //do not add any parameters in this case
            }
        }else{
            //do not add any parameters in this case
        }
        return uriBuilder.build().encode().toUriString();
    }

}
