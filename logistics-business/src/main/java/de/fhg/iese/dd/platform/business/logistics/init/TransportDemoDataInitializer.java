/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.person.init.PersonDataInitializer;
import de.fhg.iese.dd.platform.business.participants.person.init.PersonDemoDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Component
public class TransportDemoDataInitializer extends BaseTransportDataInitializer implements IManualTestDataInitializer {

    @Override
    public String getTopic() {
        return "logistics-demo";
    }

    @Override
    public String getScenarioId() {
        return "logistics-demo-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("shopping-demo","shop-demo", PersonDataInitializer.TOPIC_DEMO);
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {
        final Person personDemoBoris = personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_1);
        final Person personDemoAnne  = personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2);
        final Person personDemoEmil  = personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_3);
        final Person personDemoAndi  = personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_4);
        final Person personDemoSusi  = personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_5);

      //TransportAlternative alternative01 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_01, DeliveryDemoDataInitializer.DELIVERY_01_STATUS);
                                             createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_02, DeliveryDemoDataInitializer.DELIVERY_02_STATUS);
        TransportAlternative alternative03 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_03, DeliveryDemoDataInitializer.DELIVERY_03_STATUS);
        TransportAlternative alternative04 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_04, DeliveryDemoDataInitializer.DELIVERY_04_STATUS);
      //TransportAlternative alternative05 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_05, DeliveryDemoDataInitializer.DELIVERY_05_STATUS);
                                             createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_06, DeliveryDemoDataInitializer.DELIVERY_06_STATUS);
      //TransportAlternative alternative07 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_07, DeliveryDemoDataInitializer.DELIVERY_07_STATUS);
        TransportAlternative alternative08 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_08, DeliveryDemoDataInitializer.DELIVERY_08_STATUS);
                                             createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_09, DeliveryDemoDataInitializer.DELIVERY_09_STATUS);
        TransportAlternative alternative10 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_10, DeliveryDemoDataInitializer.DELIVERY_10_STATUS);
        TransportAlternative alternative11 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_11, DeliveryDemoDataInitializer.DELIVERY_11_STATUS);
        TransportAlternative alternative12 = createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_12, DeliveryDemoDataInitializer.DELIVERY_12_STATUS);
                                             createTransportAlternative(logSummary, DeliveryDemoDataInitializer.DELIVERY_DEMO_ID_13, DeliveryDemoDataInitializer.DELIVERY_13_STATUS);

      //TransportAssignment assignment01 = createTransportAssignment(logSummary, alternative01, carrier, DeliveryDemoDataInitializer.DELIVERY_01_STATUS);
      //TransportAssignment assignment02 = createTransportAssignment(logSummary, alternative02, carrier, DeliveryDemoDataInitializer.DELIVERY_02_STATUS);
        TransportAssignment assignment03 = createTransportAssignment(logSummary, alternative03, personDemoBoris, DeliveryDemoDataInitializer.DELIVERY_03_STATUS);
        TransportAssignment assignment04 = createTransportAssignment(logSummary, alternative04, personDemoBoris, DeliveryDemoDataInitializer.DELIVERY_04_STATUS);
      //TransportAssignment assignment05 = createTransportAssignment(logSummary, alternative05, carrier, DeliveryDemoDataInitializer.DELIVERY_05_STATUS);
      //TransportAssignment assignment06 = createTransportAssignment(logSummary, alternative06, carrier, DeliveryDemoDataInitializer.DELIVERY_06_STATUS);
      //TransportAssignment assignment07 = createTransportAssignment(logSummary, alternative07, carrier, DeliveryDemoDataInitializer.DELIVERY_07_STATUS);
        TransportAssignment assignment08 = createTransportAssignment(logSummary, alternative08, personDemoSusi, DeliveryDemoDataInitializer.DELIVERY_08_STATUS);
      //TransportAssignment assignment09 = createTransportAssignment(logSummary, alternative09, carrier, DeliveryDemoDataInitializer.DELIVERY_09_STATUS);
        TransportAssignment assignment10 = createTransportAssignment(logSummary, alternative10, personDemoAnne, DeliveryDemoDataInitializer.DELIVERY_10_STATUS);
        TransportAssignment assignment11 = createTransportAssignment(logSummary, alternative11, personDemoAndi, DeliveryDemoDataInitializer.DELIVERY_11_STATUS);
        TransportAssignment assignment12 = createTransportAssignment(logSummary, alternative12, personDemoEmil, DeliveryDemoDataInitializer.DELIVERY_12_STATUS);
      //TransportAssignment assignment13 = createTransportAssignment(logSummary, alternative13, carrier, DeliveryDemoDataInitializer.DELIVERY_13_STATUS);

      //pickupDelivery(logSummary, assignment01, DeliveryDemoDataInitializer.DELIVERY_01_STATUS);
      //pickupDelivery(logSummary, assignment02, DeliveryDemoDataInitializer.DELIVERY_02_STATUS);
        pickupDelivery(logSummary, assignment03, DeliveryDemoDataInitializer.DELIVERY_03_STATUS);
        pickupDelivery(logSummary, assignment04, DeliveryDemoDataInitializer.DELIVERY_04_STATUS);
      //pickupDelivery(logSummary, assignment05, DeliveryDemoDataInitializer.DELIVERY_05_STATUS);
      //pickupDelivery(logSummary, assignment06, DeliveryDemoDataInitializer.DELIVERY_06_STATUS);
      //pickupDelivery(logSummary, assignment07, DeliveryDemoDataInitializer.DELIVERY_07_STATUS);
        pickupDelivery(logSummary, assignment08, DeliveryDemoDataInitializer.DELIVERY_08_STATUS);
      //pickupDelivery(logSummary, assignment09, DeliveryDemoDataInitializer.DELIVERY_09_STATUS);
        pickupDelivery(logSummary, assignment10, DeliveryDemoDataInitializer.DELIVERY_10_STATUS);
        pickupDelivery(logSummary, assignment11, DeliveryDemoDataInitializer.DELIVERY_11_STATUS);
        pickupDelivery(logSummary, assignment12, DeliveryDemoDataInitializer.DELIVERY_12_STATUS);
      //pickupDelivery(logSummary, assignment13, DeliveryDemoDataInitializer.DELIVERY_13_STATUS);

      //deliveryDelivery(logSummary, assignment01, DeliveryDemoDataInitializer.DELIVERY_01_STATUS);
      //deliveryDelivery(logSummary, assignment02, DeliveryDemoDataInitializer.DELIVERY_02_STATUS);
      //deliveryDelivery(logSummary, assignment03, DeliveryDemoDataInitializer.DELIVERY_03_STATUS);
        deliverDelivery(logSummary, assignment04, DeliveryDemoDataInitializer.DELIVERY_04_STATUS);
      //deliveryDelivery(logSummary, assignment05, DeliveryDemoDataInitializer.DELIVERY_05_STATUS);
      //deliveryDelivery(logSummary, assignment06, DeliveryDemoDataInitializer.DELIVERY_06_STATUS);
      //deliveryDelivery(logSummary, assignment07, DeliveryDemoDataInitializer.DELIVERY_07_STATUS);
      //deliveryDelivery(logSummary, assignment08, DeliveryDemoDataInitializer.DELIVERY_08_STATUS);
      //deliveryDelivery(logSummary, assignment09, DeliveryDemoDataInitializer.DELIVERY_09_STATUS);
      //deliveryDelivery(logSummary, assignment10, DeliveryDemoDataInitializer.DELIVERY_00_STATUS);
      //deliveryDelivery(logSummary, assignment11, DeliveryDemoDataInitializer.DELIVERY_11_STATUS);
      //deliveryDelivery(logSummary, assignment12, DeliveryDemoDataInitializer.DELIVERY_12_STATUS);
      //deliveryDelivery(logSummary, assignment13, DeliveryDemoDataInitializer.DELIVERY_13_STATUS);

        transportAlternativeBundleRepository.flush();
        transportAlternativeBundleStatusRecordRepository.flush();
        transportAlternativeRepository.flush();
        transportAlternativeStatusRecordRepository.flush();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        logSummary.info("Nothing to do, drop is done in DeliveryDemoDataInitializer");
    }

}
