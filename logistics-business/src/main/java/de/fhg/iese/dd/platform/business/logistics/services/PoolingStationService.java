/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryAlreadyInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryStillInPoolingStationException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationBoxNotClosedException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationBoxNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationFullException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.PoolingStationNotFoundException;
import de.fhg.iese.dd.platform.business.shared.geo.services.ISpatialService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.BoxAllocationStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationBoxType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxAllocationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

@Service
class PoolingStationService extends BaseEntityService<PoolingStation> implements IPoolingStationService {

    @Autowired
    private PoolingStationRepository poolingStationRepository;

    @Autowired
    private PoolingStationBoxRepository poolingStationBoxRepository;

    @Autowired
    private PoolingStationBoxAllocationRepository boxAllocationRepository;

    @Autowired
    private ISpatialService spatialService;

    @Override
    public PoolingStation findPoolingStationById(String poolingStationId) throws PoolingStationNotFoundException {
        if(poolingStationId == null) throw new PoolingStationNotFoundException("");
        return poolingStationRepository.findById( poolingStationId )
                .orElseThrow(() -> new PoolingStationNotFoundException( poolingStationId ));
    }

    @Override
    public PoolingStationBox findPoolingStationBoxById(String poolingStationBoxId) throws PoolingStationBoxNotFoundException {
        if(poolingStationBoxId == null) throw new PoolingStationBoxNotFoundException("");
        return poolingStationBoxRepository.findById( poolingStationBoxId )
                .orElseThrow(() -> new PoolingStationBoxNotFoundException( poolingStationBoxId ));
    }

    @Override
    public PoolingStation findNearestPoolingStation(Tenant tenant, ParcelAddress parcelAddress, double radiusKM) {

        List<PoolingStation> stations = poolingStationRepository.findAllByCommunityOrderByCreatedAsc(tenant);

        stations = stations.stream().
                filter(ps -> !(parcelAddress.getAddressType() == ParcelAddressType.POOLING_STATION &&
                        ps.getId().equals(parcelAddress.getPoolingStation().getId()))).collect(Collectors.toList());

        GPSLocation parcelLocation = parcelAddress.getAddress().getGpsLocation();

        if (parcelLocation == null || stations.isEmpty()) {
            return null;
        }
        return findNearestPoolingStation(stations, parcelLocation, radiusKM);
    }

    @Override
    public PoolingStation findNearestPoolingStation(List<PoolingStation> stations, GPSLocation location, double radiusKM) {
        double minDistance = radiusKM;
        PoolingStation poolingStation = null;

        for ( PoolingStation ps : stations ) {
            GPSLocation psLocation = ps.getAddress().getGpsLocation();
            if(psLocation != null){
                double d = spatialService.distanceInKM( location, psLocation );
                if ( d < minDistance) {
                    minDistance = d;
                    poolingStation = ps;
                }
            }
        }

        return poolingStation;
    }

    @Override
    public List<PoolingStation> findMainHubsBetween(Tenant tenant, GPSLocation from, GPSLocation to) {

        List<PoolingStation> stations = poolingStationRepository.findAllByCommunityOrderByCreatedAsc(tenant);
        List<PoolingStation> result = new ArrayList<>();

        for (PoolingStation ps : stations) {
            GPSLocation psLocation = ps.getAddress().getGpsLocation();
            if (ps.isMainHub() && from != null && to != null && psLocation != null) {
                if (spatialService.isWithinPerimeter(from, to, psLocation)) {
                    result.add(ps);
                }
            }
        }

        return result;
    }

    @Override
    public List<PoolingStation> findPoolingStationsInTenant(Tenant tenant) {
        return poolingStationRepository.findAllByCommunityOrderByCreatedAsc(tenant);
    }

    @Override
    public boolean isAvailableForPutParcel(PoolingStation ps, Delivery d) {
        return ps.isMainHub() || ps.isFinalStation() || ps.isIntermediateStation();
    }

    @Override
    public PoolingStationBox findAvailablePoolingStationBox(PoolingStation poolingStation, Delivery delivery) throws PoolingStationFullException{

        PoolingStationBox availableBox = poolingStationBoxRepository.findAvailableByPoolingStationId(poolingStation.getId());

        if(availableBox == null){
            throw new PoolingStationFullException("PoolingStation '{}' has no more empty boxes left.", poolingStation.getId());
        }

        return availableBox;
    }

    @Override
    public PoolingStationBoxAllocation allocatePoolingStationBoxDirectly(PoolingStationBox poolingStationBox, Delivery delivery) throws PoolingStationFullException{
        PoolingStationBoxAllocation boxAllocation = getCurrentAllocation(poolingStationBox);
        if (boxAllocation == null) {
            boxAllocation = PoolingStationBoxAllocation.builder()
                    .poolingStationBox(poolingStationBox)
                    .delivery(delivery)
                    .status(BoxAllocationStatus.RESERVATION)
                    .timestamp(timeService.currentTimeMillisUTC())
                    .build();
        }

        if (boxAllocation.getStatus() == BoxAllocationStatus.ACTIVE) {
            throw new PoolingStationFullException("The box '{}' is already occcupied with an active box allocation, cannot override it.", poolingStationBox.getId());
        }

        boxAllocation
            .setInTime(timeService.currentTimeMillisUTC())
            .setDelivery(delivery)
            .setStatus(BoxAllocationStatus.ACTIVE)
            .setTimestamp(timeService.currentTimeMillisUTC());

        return boxAllocationRepository.saveAndFlush(boxAllocation);
    }

    @Override
    public PoolingStationBoxAllocation getCurrentAllocation(PoolingStationBox poolingStationBox) {
        return boxAllocationRepository.findFirstByPoolingStationBoxOrderByTimestampDesc(poolingStationBox);
    }

    @Override
    public PoolingStationBoxAllocation startAllocation(PoolingStation poolingStation, Delivery delivery) throws PoolingStationFullException, DeliveryAlreadyInPoolingStationException{

        List<PoolingStationBoxAllocation> currentAllocations = boxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(poolingStation.getId(), delivery.getId());
        PoolingStationBoxAllocation pendingAllocation = null;

        for(PoolingStationBoxAllocation currentAllocation : currentAllocations){
            switch(currentAllocation.getStatus()){
              case RESERVATION:
                  pendingAllocation = currentAllocation;
                  break;
              case ACTIVE:
                  throw new DeliveryAlreadyInPoolingStationException(poolingStation.getId(), delivery.getId());
              case PAST:
                  break;
              case PENDING_IN:
                  //this is acceptable since we call this method again if someone did not open the door in time
                  pendingAllocation = currentAllocation;
                  break;
              case PENDING_OUT:
                  //this is a strange status in this situation, but not so bad that we should throw an exception
                  pendingAllocation = currentAllocation;
                  break;
              default:
                  break;
            }
        }

        if(pendingAllocation == null){
            //we did not have a reservation, so we create one
            PoolingStationBox availableBox = findAvailablePoolingStationBox(poolingStation, delivery);

            pendingAllocation = PoolingStationBoxAllocation.builder()
                    .poolingStationBox(availableBox)
                    .delivery(delivery)
                    .status(BoxAllocationStatus.RESERVATION)
                    .timestamp(timeService.currentTimeMillisUTC())
                    .build();
        }

        pendingAllocation
            .setStatus(BoxAllocationStatus.PENDING_IN)
            .setTimestamp(timeService.currentTimeMillisUTC());

        return boxAllocationRepository.saveAndFlush(pendingAllocation);
    }

    @Override
    public PoolingStationBoxAllocation finalizeAllocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException, PoolingStationBoxNotClosedException {

        List<PoolingStationBoxAllocation> currentAllocations = boxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(poolingStation.getId(), delivery.getId());
        PoolingStationBoxAllocation pendingAllocation = null;

        for(PoolingStationBoxAllocation currentAllocation : currentAllocations){
            switch(currentAllocation.getStatus()){
              case RESERVATION:
                  throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
              case ACTIVE:
                  //we can be idempotent here, since there is only one active allocation per delivery
                  return currentAllocation;
              case PAST:
                  break;
              case PENDING_IN:
                  pendingAllocation = currentAllocation;
                  break;
              case PENDING_OUT:
                  throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
              default:
                  break;
            }
        }

        if(pendingAllocation == null){
            throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
        }

        PoolingStationBox box = pendingAllocation.getPoolingStationBox();

        if(box.getConfiguration().getBoxType() == PoolingStationBoxType.AUTOMATIC){
            //Check if the door is closed
            if(pendingAllocation.isDoorOpen()){
                throw new PoolingStationBoxNotClosedException(box.getId());
            }
        }

        pendingAllocation
            .setStatus(BoxAllocationStatus.ACTIVE)
            .setInTime(timeService.currentTimeMillisUTC())
            .setTimestamp(timeService.currentTimeMillisUTC());

        return boxAllocationRepository.saveAndFlush(pendingAllocation);
    }

    @Override
    public PoolingStationBoxAllocation startDeallocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException {

        List<PoolingStationBoxAllocation> currentAllocations = boxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(poolingStation.getId(), delivery.getId());
        PoolingStationBoxAllocation activeAllocation = null;

        for(PoolingStationBoxAllocation currentAllocation : currentAllocations){
            switch(currentAllocation.getStatus()){
              case RESERVATION:
                  break;
              case ACTIVE:
                  activeAllocation = currentAllocation;
                  break;
              case PAST:
                  break;
              case PENDING_IN:
                  break;
              case PENDING_OUT:
                  //we can be idempotent here, since there is only one pending out allocation per delivery
                  return currentAllocation;
              default:
                  break;
            }
        }

        if(activeAllocation == null){
            throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
        }

        activeAllocation
            .setStatus(BoxAllocationStatus.PENDING_OUT)
            .setTimestamp(timeService.currentTimeMillisUTC());

        return boxAllocationRepository.saveAndFlush(activeAllocation);
    }

    @Override
    public PoolingStationBoxAllocation finalizeDeallocation(PoolingStation poolingStation, Delivery delivery) throws DeliveryNotInPoolingStationException, DeliveryStillInPoolingStationException, PoolingStationBoxNotClosedException {

        List<PoolingStationBoxAllocation> currentAllocations = boxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(poolingStation.getId(), delivery.getId());
        PoolingStationBoxAllocation pendingAllocation = null;
        PoolingStationBoxAllocation pastAllocation = null;

        for(PoolingStationBoxAllocation currentAllocation : currentAllocations){
            switch(currentAllocation.getStatus()){
              case RESERVATION:
                  //strange case, it seems to be that it never was in the pooling station
                  throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
              case ACTIVE:
                  //deallocation was not started properly
                  throw new DeliveryStillInPoolingStationException(poolingStation.getId(), delivery.getId());
              case PAST:
                  //idempotent
                  //get the latest past allocation, in case we have more than one
                  if(pastAllocation == null){
                      pastAllocation = currentAllocation;
                  }
                  break;
              case PENDING_IN:
                  //strange case, it seems to be that it never was in the pooling station
                  throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
              case PENDING_OUT:
                  pendingAllocation = currentAllocation;
                  break;
              default:
                  break;
            }
        }

        if(pendingAllocation == null){
            if(pastAllocation != null){
                //idempotent
                //we assume that the past allocation is the one that should be finalized here
                return pastAllocation;
            }
            //delivery was never in pooling station
            throw new DeliveryNotInPoolingStationException(poolingStation.getId(), delivery.getId());
        }

        PoolingStationBox box = pendingAllocation.getPoolingStationBox();

        if(box.getConfiguration().getBoxType() == PoolingStationBoxType.AUTOMATIC){
            //Check if the door is closed
            if(pendingAllocation.isDoorOpen()){
                throw new PoolingStationBoxNotClosedException(box.getId());
            }
        }

        pendingAllocation
            .setStatus(BoxAllocationStatus.PAST)
            .setOutTime(timeService.currentTimeMillisUTC())
            .setTimestamp(timeService.currentTimeMillisUTC());

        return boxAllocationRepository.saveAndFlush(pendingAllocation);
    }

    @Override
    public void doorOpened(PoolingStationBox poolingStationBox) {
        PoolingStationBoxAllocation boxAllocation = getCurrentAllocation(poolingStationBox);
        if (boxAllocation == null) {
            log.warn("{} is opened, but there is no allocation", poolingStationBox.toString());
            return;
        }
        boxAllocation
                .setDoorOpen(true)
                .setTimestamp(timeService.currentTimeMillisUTC());
        boxAllocationRepository.saveAndFlush(boxAllocation);
    }

    @Override
    public void doorClosed(PoolingStationBox poolingStationBox) {
        PoolingStationBoxAllocation boxAllocation = getCurrentAllocation(poolingStationBox);
        if(boxAllocation != null){
            boxAllocation
                .setDoorOpen(false)
                .setTimestamp(timeService.currentTimeMillisUTC());
            boxAllocationRepository.saveAndFlush(boxAllocation);
        }else{
            log.warn(poolingStationBox.toString()+" is closed, but there is no allocation");
        }
    }

}
