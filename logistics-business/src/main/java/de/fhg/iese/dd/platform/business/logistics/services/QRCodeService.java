/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.html2pdf.exceptions.Html2PdfException;
import de.fhg.iese.dd.platform.business.shared.html2pdf.services.IHtml2PdfService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.logistics.config.LogisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
class QRCodeService extends BaseService implements IQRCodeService {

    private static final int QR_CODE_SIZE = 250;

    @Autowired
    private IFileStorage fileStorage;

    @Autowired
    private LogisticsConfig logisticsConfig;

    @Autowired
    private IHtml2PdfService html2PdfService;

    private String generateQRCodeBase64Encoded(String text) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        hints.put(EncodeHintType.MARGIN, 0);

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            BitMatrix byteMatrix = qrCodeWriter.encode(text,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE, hints);

            MatrixToImageWriter.writeToStream(byteMatrix, "PNG", out);
        } catch (WriterException | IOException e) {
            log.error("Could not generate qr code", e);
            return "";
        }

        return new String(Base64.getEncoder().encode(out.toByteArray()), StandardCharsets.US_ASCII);
    }

    @Override
    public String generateQRCodeHTML(String receiverName, String receiverAddressName,
            String receiverStreet, String receiverCity,
            String senderName, String senderStreet, String senderCity,
            String transportNotes, String contentNotes,
            String trackingCode, String qrCodeText, int parcelCount) {

        //TODO: check why necessary in the original code
        //Here it is necessary because it is also a variable in the template, and
        //if left 0, it will count back.
        if (parcelCount < 1) {
            parcelCount = 1;
        }

        //receiverAddressName is the second address line. This value is optional.
        if (receiverAddressName == null) {
            receiverAddressName = ""; //prevents InvalidReferenceException in freemarker template processing
        }

        final Map<String, Object> templateModel = new HashMap<>();
        //templateModel.put("multiPage", multiPage);
        templateModel.put("receiverName", receiverName);
        templateModel.put("receiverAddressName", receiverAddressName);
        templateModel.put("receiverStreet", receiverStreet);
        templateModel.put("receiverCity", receiverCity);
        templateModel.put("senderName", senderName);
        templateModel.put("senderStreet", senderStreet);
        templateModel.put("senderCity", senderCity);
        templateModel.put("trackingCode", trackingCode);

        //QR-Code
        //No need for temporary file
        final String encodedBase64 = generateQRCodeBase64Encoded(qrCodeText);
        //Prepare for the HTML img tag (pay attention to the hard-coded quotation marks
        final String encodedImage = "\"" + "data:image/png;base64," + encodedBase64 + "\"";
        templateModel.put("encodedImage", encodedImage);

        //paket number
        templateModel.put("parcelCount", parcelCount);
        //No need for parcelNumber
        //templateModel.put("parcelNumber", parcelNumber);

        if (transportNotes!=null && !transportNotes.isEmpty()) {
            templateModel.put("transportNotes", transportNotes);
        }
        if (contentNotes!=null && !contentNotes.isEmpty()) {
            templateModel.put("contentNotes", contentNotes);
        }

        final Configuration freemarkerConfiguration = new Configuration(Configuration.VERSION_2_3_23);
        freemarkerConfiguration.setDefaultEncoding("UTF-8");
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");

        try {
            final Template template = freemarkerConfiguration.getTemplate("qr-code/QRCodeTemplate.ftl");
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        } catch (IOException | TemplateException ex) {
            throw new IllegalStateException("QR code label creation via freemarker is not configured properly", ex);
        }
    }

    @Override
    public String generateQRCodePDFURL(String receiverName, String receiverAddressName,
            String receiverStreet, String receiverCity,
            String senderName, String senderStreet, String senderCity, String transportNotes,
            String contentNotes, String trackingCode, String qrCodeText, int parcelCount) {

        final String html = generateQRCodeHTML(receiverName, receiverAddressName, receiverStreet, receiverCity,
                senderName, senderStreet, senderCity, transportNotes, contentNotes, trackingCode, qrCodeText, parcelCount);
        final String fileName = "qr_" + trackingCode + "_" + timeService.currentTimeMillisUTC() + ".pdf";

        try {
            byte[] pdfBytes = html2PdfService.convert(html, 100, 62);
            fileStorage.saveFile(pdfBytes, "application/pdf",
                    logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);

            return fileStorage.getExternalUrl(logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);
        } catch (Html2PdfException ex) {
            log.error("Could not convert qr code html to pdf", ex);
        } catch (FileStorageException ex) {
            log.error("Could not save qr code pdf file to file storage", ex);
        }
        return null;
    }

    @Override
    public String generateQRCodePDFA4URL(String receiverName, String receiverAddressName,
            String receiverStreet, String receiverCity,
            String senderName, String senderStreet, String senderCity, String transportNotes,
            String contentNotes, String trackingCode, String qrCodeText, int parcelCount) {

        final String html = generateQRCodeHTML(receiverName, receiverAddressName, receiverStreet, receiverCity,
                senderName, senderStreet, senderCity, transportNotes, contentNotes, trackingCode, qrCodeText, parcelCount);
        final String fileName = "qr_a4_" + trackingCode + "_" + timeService.currentTimeMillisUTC() + ".pdf";

        try {
            byte[] pdfBytes = html2PdfService.convertA4(html);
            fileStorage.saveFile(pdfBytes, "application/pdf",
                    logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);

            return fileStorage.getExternalUrl(logisticsConfig.getParcelLabelInternalStoragePrefix() + fileName);
        } catch (Html2PdfException ex) {
            log.error("Could not convert qr code html to pdf", ex);
        } catch (FileStorageException ex) {
            log.error("Could not save qr code pdf file to file storage", ex);
        }
        return null;
    }

}
