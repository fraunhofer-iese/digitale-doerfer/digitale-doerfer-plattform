/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ReceiverPickupAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxAllocationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Service
class DemoLogisticsService extends BaseService implements IDemoLogisticsService {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;

    @Autowired
    private PoolingStationBoxAllocationRepository poolingStationBoxAllocationRepository;

    @Autowired
    private ReceiverPickupAssignmentRepository receiverPickupAssignmentRepository;

    @Autowired
    private TransportAlternativeRepository transportAlternativeRepository;

    @Autowired
    private TransportAlternativeStatusRecordRepository transportAlternativeStatusRecordRepository;

    @Autowired
    private TransportAlternativeBundleRepository transportAlternativeBundleRepository;

    @Autowired
    private TransportAlternativeBundleStatusRecordRepository transportAlternativeBundleStatusRecordRepository;

    @Autowired
    private TransportAssignmentRepository transportAssignmentRepository;

    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;

    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;

    @Autowired
    private AccountEntryRepository accountEntryRepository;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IChatService chatService;

    @Transactional
    @Override
    public void deleteDataForDemoTenant(String tenantId) {
        List<TransportAlternativeBundle> transportAlternativeBundles;
        List<TransportAlternative> transportAlternatives;
        List<TransportAssignment> transportAssignments;
        List<ReceiverPickupAssignment> receiverPickupAssignments;

        Tenant demoTenant = tenantService.findTenantById(tenantId);

        //Deliveries
        List<Delivery> deliveries = deliveryRepository.findAllByCommunityId(demoTenant.getId());
        for (Delivery delivery : deliveries) {
            //DeliveryStatusRecords
            deliveryStatusRecordRepository.deleteAllByDeliveryId(delivery.getId());

            //TransportAssignment
            transportAssignments = transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());
            for(TransportAssignment transportAssignment : transportAssignments){
                //TransportAssignmentStatusRecords
                transportAssignmentStatusRecordRepository.deleteAllByTransportAssignment(transportAssignment);

                //TransportConfirmation
                transportConfirmationRepository.deleteAllByTransportAssignmentPickedUpNotNullAndTransportAssignmentPickedUp(
                        transportAssignment);
                transportConfirmationRepository.deleteAllByTransportAssignmentDeliveredNotNullAndTransportAssignmentDelivered(
                        transportAssignment);
                transportConfirmationRepository.deleteAllByTransportAssignmentReceivedNotNullAndTransportAssignmentReceived(
                        transportAssignment);

                //AccountEntry
                accountEntryRepository.deleteAllBySubjectIdAndSubjectName(transportAssignment.getId(),
                        transportAssignment.getClass().getName());
            }
            transportAssignmentRepository.deleteAll(transportAssignments);

            //TransportAlternativeBundles
            transportAlternativeBundles = transportAlternativeBundleRepository.findAllByDeliveryOrderByCreatedDesc(delivery);
            for(TransportAlternativeBundle transportAlternativeBundle : transportAlternativeBundles){
                //TransportAlternativeBundleStatusRecords
                transportAlternativeBundleStatusRecordRepository.deleteAllByTransportAlternativeBundleId(transportAlternativeBundle.getId());

                //TransportAlternatives
                transportAlternatives = transportAlternativeRepository.findAllByTransportAlternativeBundleOrderByCreatedDesc(transportAlternativeBundle);
                for(TransportAlternative transportAlternative : transportAlternatives){
                    //TransportAlternativeStatusRecords
                    transportAlternativeStatusRecordRepository.deleteAllByTransportAlternativeId(transportAlternative.getId());
                }
                transportAlternativeRepository.deleteAll(transportAlternatives);

            }
            transportAlternativeBundleRepository.deleteAll(transportAlternativeBundles);

            //ReceiverPickupAssignment
            receiverPickupAssignments = receiverPickupAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());
            for(ReceiverPickupAssignment receiverPickupAssignment : receiverPickupAssignments){
                //TransportConfirmation
                transportConfirmationRepository.deleteAllByReceiverPickupAssignmentPickedUpNotNullAndReceiverPickupAssignmentPickedUp(
                        receiverPickupAssignment);
            }
            receiverPickupAssignmentRepository.deleteAll(receiverPickupAssignments);

            //PoolingStationBoxAllocation
            poolingStationBoxAllocationRepository.deleteAllByDeliveryId(delivery.getId());

            //AccountEntry
            accountEntryRepository.deleteAllBySubjectIdAndSubjectName(delivery.getId(), delivery.getClass().getName());

            //Chat
            chatService.deleteChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, delivery);
        }
        deliveryRepository.deleteAll(deliveries);

        accountService.recalculateAllAccountBalances(demoTenant, true);
    }

}
