/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.events;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationContext;

import de.fhg.iese.dd.platform.business.shared.waiting.events.PeriodicWaitingExpiredEvent;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportAlternativeBundlePeriodicNotificationExpiredEvent extends PeriodicWaitingExpiredEvent {

    private Tenant tenant;
    private List<String> schedule;

    @Override
    public WaitingDeadlineUpdate nextDeadline(ApplicationContext context) {

        ApplicationConfig config = context.getBean(ApplicationConfig.class);

        //last deadline in current time zone
        LocalTime currentDeadline;
        if(getDeadline() == null){
            //the event did not run so far
            currentDeadline = LocalTime.now(config.getLocalZoneId());
        }else{
            currentDeadline = LocalTime.from(Instant.ofEpochMilli(getDeadline()).atZone(config.getLocalZoneId()));
        }

        List<LocalTime> scheduleTimes = schedule.stream()
                .map(LocalTime::parse)
                //sort so that the next one is the first
                .sorted()
                .collect(Collectors.toList());

        Optional<LocalTime> nextDeadlineTimeToday = scheduleTimes.stream()
                //the next ones for the day
                .filter(availableDeadlines -> availableDeadlines.isAfter(currentDeadline))
                //get the first one, which is the next one today
                .findFirst();

        ZonedDateTime nextDeadline;
        //today is the next deadline
        //next day, the first deadline
        nextDeadline = nextDeadlineTimeToday
                .map(localTime -> Instant.now()
                        .atZone(config.getLocalZoneId())
                        .with(localTime))
                .orElseGet(() -> Instant.now()
                        .atZone(config.getLocalZoneId())
                        .plusDays(1)
                        .with(scheduleTimes.get(0)));

        return new WaitingDeadlineUpdate(true, TimeUnit.SECONDS.toMillis(nextDeadline.toEpochSecond()));
    }

}
