/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.misc.services.INamedCounterService;
import de.fhg.iese.dd.platform.datamanagement.logistics.config.LogisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;

@Service
class TrackingCodeService extends BaseService implements ITrackingCodeService {

    private static final String PARCEL_CODE_COUNTER_NAME = "parcelCodeCounter";

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private LogisticsConfig logisticsConfig;

    @Autowired
    private INamedCounterService counterService;

    @Override
    public String getTrackingCode() {
        long since = timeService.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(60L);
        int retries = 0;

        Set<String> currentTrackingCodes = new HashSet<>(deliveryRepository.findAllTrackingCodesSince(since));
        String trackingCode;

        do {
            trackingCode = RandomStringUtils.randomNumeric(6);
            retries++;
        } while ( currentTrackingCodes.contains(trackingCode) );

        log.debug("Created new tracking code \""+trackingCode+"\" retries: "+retries);

        return trackingCode;
    }

    @Override
    public String getRollingParcelCode(){
        long parcelCode = (counterService.increaseCounter(PARCEL_CODE_COUNTER_NAME) % logisticsConfig.getRollingParcelCodeUpperBound()) + 1;
        log.debug("Created new parcel code '{}'", parcelCode);
        return String.valueOf(parcelCode);
    }

    @Override
    public boolean isRollingParcelCode(String parcelCodeString){
        try {
            long parcelCode = Long.parseLong(parcelCodeString);
            return parcelCode > 0 && parcelCode <= logisticsConfig.getRollingParcelCodeUpperBound();
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
