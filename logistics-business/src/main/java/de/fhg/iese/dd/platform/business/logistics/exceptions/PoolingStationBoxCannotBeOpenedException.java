/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ExternalException;

public class PoolingStationBoxCannotBeOpenedException extends ExternalException {

    private static final long serialVersionUID = -9057482941062871472L;

    public PoolingStationBoxCannotBeOpenedException(String poolingStationName, String poolingStationId, String poolingStationBoxId, Exception e) {
        super("PoolingStationBox '"+poolingStationBoxId+"' of '"+poolingStationName+"' ("+poolingStationId+") can not be opened.", e);
    }

    public PoolingStationBoxCannotBeOpenedException(String poolingStationName, String poolingStationId, String poolingStationBoxId, String message) {
        super("PoolingStationBox '"+poolingStationBoxId+"' of '"+poolingStationName+"' ("+poolingStationId+") can not be opened:" + message);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.POOLING_STATION_BOX_CANNOT_BE_OPENED;
    }

}
