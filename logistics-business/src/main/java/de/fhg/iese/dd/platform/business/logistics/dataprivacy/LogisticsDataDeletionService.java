/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.dataprivacy;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportConfirmationService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.BaseDataDeletionService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ReceiverPickupAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxAllocationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

@Service
class LogisticsDataDeletionService extends BaseDataDeletionService implements ILogisticsDataDeletionService {

    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    private ITransportConfirmationService transportConfirmationService;
    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;
    @Autowired
    private TransportAlternativeBundleRepository transportAlternativeBundleRepository;
    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private TransportAlternativeRepository transportAlternativeRepository;
    @Autowired
    private TransportAssignmentRepository transportAssignmentRepository;
    @Autowired
    private TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;
    @Autowired
    private ReceiverPickupAssignmentRepository receiverPickupAssignmentRepository;
    @Autowired
    private PoolingStationBoxAllocationRepository poolingStationBoxAllocationRepository;
    @Autowired
    private DeliveryRepository deliveryRepository;
    @Autowired
    private IFileStorage fileStorageService;

    @Override
    public DeleteOperation deleteDeliveryData(Person person, IPersonDeletionReport personDeletionReport) {

        List<Delivery> allDeliveries = deliveryService.findAllDeliveriesByReceiver(person);

        if (!CollectionUtils.isEmpty(allDeliveries)) {
            log.debug("Starting delivery data deletion of {} deliveries for receiver {}",
                    allDeliveries.size(), person.getId());

            for (Delivery delivery : allDeliveries) {
                List<TransportAlternativeBundle> transportAlternativeBundles =
                        transportAlternativeBundleRepository.findAllByDeliveryOrderByCreatedDesc(delivery);

                Collection<TransportAssignment> transportAssignments =
                        transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(delivery.getId());

                // deleting transport alternative bundle and corresponding alternatives
                for (TransportAlternativeBundle bundle : transportAlternativeBundles) {
                    bundle.setPickupAddress(null);

                    List<TransportAlternative> alternatives =
                            transportAlternativeService.findTransportAlternativesByBundle(bundle);

                    for (TransportAlternative alternative : alternatives) {
                        alternative.setDeliveryAddress(null);
                        alternative.setDesiredDeliveryTime(null);
                        alternative.setDesiredPickupTime(null);
                        alternative.setLatestDeliveryTime(erasedLongValue());
                        alternative.setLatestPickupTime(erasedLongValue());
                        transportAlternativeRepository.save(alternative);
                        personDeletionReport.addEntry(alternative, DeleteOperation.ERASED);
                    }

                    transportAlternativeBundleRepository.save(bundle);
                    personDeletionReport.addEntry(bundle, DeleteOperation.ERASED);
                }

                // deleting transport assignments and corresponding transport confirmations and receiver pickup assignments
                for (TransportAssignment assignment : transportAssignments) {
                    assignment.setDeliveryAddress(null);
                    assignment.setPickupAddress(null);
                    assignment.setDesiredDeliveryTime(null);
                    assignment.setDesiredPickupTime(null);
                    assignment.setLatestDeliveryTime(erasedLongValue());
                    assignment.setLatestPickupTime(erasedLongValue());

                    eraseTransportConfirmations(
                            transportConfirmationService.findTransportConfirmationDelivered(assignment),
                            personDeletionReport
                    );

                    eraseTransportConfirmations(
                            transportConfirmationService.findTransportConfirmationPickedUp(assignment),
                            personDeletionReport
                    );

                    eraseTransportConfirmations(
                            transportConfirmationService.findTransportConfirmationReceived(assignment),
                            personDeletionReport
                    );

                    transportAssignmentRepository.save(assignment);
                    personDeletionReport.addEntry(assignment, DeleteOperation.ERASED);
                }

                poolingStationBoxAllocationRepository.deleteAllByDeliveryId(delivery.getId());
                deliveryStatusRecordRepository.deleteAllByDeliveryId(delivery.getId());

                delivery.setPickupAddress(null);
                delivery.setDeliveryAddress(null);
                delivery.setContentNotes(erasedNonNullString());
                delivery.setTransportNotes(erasedNonNullString());
                delivery.setDesiredDeliveryTime(null);
                delivery.setEstimatedDeliveryTime(null);
                delivery.setActualDeliveryTime(erasedLongValue());
                delivery.setCustomReferenceNumber(erasedNonNullString());

                delivery.setSize(Dimension.builder()
                        .height(erasedDoubleValue())
                        .length(erasedDoubleValue())
                        .width(erasedDoubleValue())
                        .weight(erasedDoubleValue())
                        .build());

                try {
                    if (delivery.getTrackingCodeLabelURL() != null) {
                        String internalFileName = fileStorageService.getInternalFileName(delivery.getTrackingCodeLabelURL());
                        fileStorageService.deleteFile(internalFileName);
                        delivery.setTrackingCodeLabelURL(erasedNonNullString());
                    }

                    if (delivery.getTrackingCodeLabelA4URL() != null) {
                        String internalFileName = fileStorageService.getInternalFileName(delivery.getTrackingCodeLabelA4URL());
                        fileStorageService.deleteFile(internalFileName);
                        delivery.setTrackingCodeLabelA4URL(erasedNonNullString());
                    }
                } catch (FileStorageException e) {
                    log.error(e.getDetail());
                }

                deliveryRepository.save(delivery);
                personDeletionReport.addEntry(delivery, DeleteOperation.ERASED);
            }

            log.debug("Finished delivery data deletion for receiver {}", person.getId());
        }

        return DeleteOperation.ERASED;
    }

    @Override
    public DeleteOperation deleteTransportData(Person person, IPersonDeletionReport deletionReport) {

        List<TransportAssignment> ownAssignments =
                transportAssignmentRepository.findAllByCarrierOrderByCreatedDesc(person);

        if (ownAssignments.size() != 0) {
            log.debug("Starting transport data deletion for carrier {}", person.getId());

            for (TransportAssignment assignment : ownAssignments) {

                transportAssignmentStatusRecordRepository.deleteAllByTransportAssignment(assignment);

                transportConfirmationRepository
                        .deleteAllByTransportAssignmentPickedUpNotNullAndTransportAssignmentPickedUp(assignment);
                transportConfirmationRepository
                        .deleteAllByTransportAssignmentDeliveredNotNullAndTransportAssignmentDelivered(assignment);
                transportConfirmationRepository
                        .deleteAllByTransportAssignmentReceivedNotNullAndTransportAssignmentReceived(assignment);

                transportAssignmentRepository.delete(assignment);
                deletionReport.addEntry(assignment, DeleteOperation.DELETED);
            }

            log.debug("Finished deleting transport data for carrier {}", person.getId());
        }

        return DeleteOperation.DELETED;
    }

    private void eraseTransportConfirmations(List<TransportConfirmation> confirmations,
            IPersonDeletionReport personDeletionReport) {
        if (CollectionUtils.isEmpty(confirmations)) {
            return;
        }
        for (TransportConfirmation confirmation : confirmations) {
            confirmation.setNotes(erasedNonNullString());
            if (confirmation.getReceiverPickupAssignmentPickedUp() != null) {
                ReceiverPickupAssignment receiverPickupAssignment =
                        confirmation.getReceiverPickupAssignmentPickedUp();
                receiverPickupAssignmentRepository.delete(receiverPickupAssignment);
                confirmation.setReceiverPickupAssignmentPickedUp(null);
                personDeletionReport.addEntry(receiverPickupAssignment, DeleteOperation.DELETED);
            }
            transportConfirmationRepository.save(confirmation);
            personDeletionReport.addEntry(confirmation, DeleteOperation.ERASED);
        }
    }

}
