/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import de.fhg.iese.dd.platform.business.framework.services.IService;

public interface IQRCodeService extends IService {

    String generateQRCodePDFURL(String receiverName, String receiverAddressName, String receiverStreet,
            String receiverCity, String senderName, String senderStreet, String senderCity, String transportNotes,
            String contentNotes, String trackingCode, String qrCodeText, int parcelCount);

    String generateQRCodePDFA4URL(String receiverName, String receiverAddressName, String receiverStreet,
            String receiverCity, String senderName, String senderStreet, String senderCity, String transportNotes,
            String contentNotes, String trackingCode, String qrCodeText, int parcelCount);

    String generateQRCodeHTML(String receiverName, String receiverAddressName, String receiverStreet,
            String receiverCity, String senderName, String senderStreet, String senderCity, String transportNotes,
            String contentNotes, String trackingCode, String qrCodeText, int parcelCount);

}
