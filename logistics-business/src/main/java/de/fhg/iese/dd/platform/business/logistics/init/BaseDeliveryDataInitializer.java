/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.IDemoLogisticsService;
import de.fhg.iese.dd.platform.business.logistics.services.ILogisticsParticipantService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.shop.services.IShopService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

public abstract class BaseDeliveryDataInitializer extends BaseDataInitializer {

    private static final boolean CREATE_LABELS = false;

    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    protected IDemoLogisticsService demoLogisticsService;
    @Autowired
    protected DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    private IChatService chatService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    protected ILogisticsParticipantService participantService;
    @Autowired
    protected IPersonService personService;
    @Autowired
    protected IShopService shopService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    protected LogisticsDataInitService logisticsDataInitService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(Shop.class)
                .requiredEntity(PoolingStation.class)
                .requiredEntity(PushCategory.class)
                .processedEntity(Delivery.class)
                .build();
    }

    protected Delivery createDelivery(LogSummary logSummary, LogisticsParticipant sender, LogisticsParticipant receiver, TimeSpan desiredDeliveryTime, String deliveryId, long createdTimeStamp) {

        Delivery delivery = Delivery.builder()
                .trackingCode("123456")
                .community(tenantService.findTenantById(LogisticsConstants.TENANT_ID_DEMO))
                .sender(sender)
                .pickupAddress(logisticsDataInitService.newParcelAddress(sender.getShop()))
                .receiver(receiver)
                .deliveryAddress(logisticsDataInitService.newParcelAddress(receiver.getPerson()))
                .desiredDeliveryTime(desiredDeliveryTime)
                .transportNotes("Bitte beim Nachbarn klingeln!")
                .contentNotes("Trocken halten")
                .size(Dimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .weight(0.7)
                        .build())
                .packagingType(PackagingType.PARCEL)
                .credits(10)
                .parcelCount(1)
                .build();
        delivery.setId(deliveryId);

        delivery = deliveryService.store(delivery);

        logSummary.info("Created delivery "+delivery.getId());
        if(CREATE_LABELS){
            delivery = deliveryService.createTrackingCodeLabel(delivery);
            logSummary.info("Created label for "+delivery.getId()+" at "+delivery.getTrackingCodeLabelURL());
        }

        deliveryStatusRecordRepository.deleteAllByDeliveryId(deliveryId);
        deliveryStatusRecordRepository.save(DeliveryStatusRecord.builder()
                .delivery(delivery)
                .incomingEvent("Demo, no event!")
                .status(DeliveryStatus.CREATED)
                .timeStamp(createdTimeStamp)
                .build());
        createChat(delivery);
        return delivery;
    }

    private void createChat(Delivery delivery) {
        Person receiver = delivery.getReceiver().getPerson();

        chatService.deleteChatBySubjectAndTopic(LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID, delivery);
        chatService.createChatWithSubjectsAndParticipants(
                LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID,
                getPushCategoryChat(),
                Collections.singletonList(delivery),
                Collections.singletonList(receiver));
    }

    private PushCategory getPushCategoryChat() {
        return pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_CHAT_ID);
    }

}
