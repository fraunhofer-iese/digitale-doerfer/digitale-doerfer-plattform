/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Balthasar Weitzel, Axel Wickenkamp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.WrongTrackingCodeForDeliveryException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IDeliveryService extends IEntityService<Delivery> { Delivery store(Delivery entity);

    Delivery findDeliveryById(String deliveryId) throws DeliveryNotFoundException;

    List<Delivery> findAllDeliveriesByReceiverSinceAndAllNotReceived(String receiverId, long sinceTimestamp);

    List<Delivery> findAllDeliveriesByReceiver(Person receiver);

    /**
     * Fetch the newest entity from DB, needed only if a stale version is around
     *
     * @param delivery the delivery to refresh
     * @return same delivery as it is currently in DB
     */
    Delivery refresh(Delivery delivery);

    Collection<Delivery> findAllDeliveries();

    /**
     * Set Delivery and associated Assignments, AlternativeBundles, Alternatives to CANCELLED
     *
     * This does not notify anyone, only the status is set to CANCELLED or EXPIRED.
     * <p/>
     * <b>Do NOT use if sender, receiver or carrier do not know that you are going to do that!</b>
     * No notifications will be sent out.
     *
     * @param delivery the delivery to cancel
     * @param incomingEvent event that caused the cancellation
     */
    void cancelDeliveryAndAllRelatedEntities(Delivery delivery, BaseEvent incomingEvent);

    boolean checkTrackingCode(String message, Delivery delivery, String trackingCode) throws WrongTrackingCodeForDeliveryException, DeliveryNotFoundException;

    Delivery createTrackingCodeLabel(Delivery delivery);

    Delivery createDelivery(Delivery delivery);

    DeliveryStatusRecord setCurrentStatus(Delivery delivery, DeliveryStatus newStatus, BaseEvent event);

    ParcelAddress getCurrentLocation(DeliveryStatusRecord currentDeliveryStatus);

    List<Delivery> findAllDeliveriesByCustomReferenceNumber(String referenceNumber);

    DeliveryStatusRecord getCurrentStatusRecord(Delivery delivery);

}
