/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeAcceptedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleCreateConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundleForSelectionEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeExpiredEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeRejectedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCancelConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Transactional(readOnly = true)
@EventProcessor
class TransportAlternativeEventProcessor extends BaseEventProcessor {

    @Autowired
    private ITransportAlternativeService transportAlternativeService;
    @Autowired
    private IDeliveryService deliveryService;

    //method needs to be public so that the @Transactional annotation is used
    @Transactional(propagation = Propagation.REQUIRED)
    @EventProcessing
    public void handleDeliveryReadyForTransportRequest(DeliveryReadyForTransportRequest event,
            final EventProcessingContext context) {

        Delivery delivery = event.getDelivery();
        ParcelAddress pickupAddress = event.getPickupAddress();

        TransportAlternativeBundle bundle =
                transportAlternativeService.createTransportAlternativeBundleForDelivery(event, delivery, pickupAddress);

        TransportAlternativeBundleCreateConfirmation createConfirmation =
                new TransportAlternativeBundleCreateConfirmation(delivery, bundle);
        notify(createConfirmation, context);

        Tenant tenant = delivery.getTenant();
        TransportAlternativeBundleForSelectionEvent bundleForSelectionEvent =
                new TransportAlternativeBundleForSelectionEvent(tenant, bundle);
        notify(bundleForSelectionEvent, context);
    }

    //method needs to be public so that the @Transactional annotation is used
    @Transactional(propagation = Propagation.REQUIRED)
    @EventProcessing
    public void handleTransportAlternativeSelectRequest(TransportAlternativeSelectRequest event, final EventProcessingContext context) {

        TransportAlternative alternative = event.getTransportAlternative();
        TransportAlternativeBundle bundle = alternative.getTransportAlternativeBundle();
        TransportAlternativeBundleStatusRecord currentAlternativeBundleStatus =
                transportAlternativeService.getCurrentStatusRecord(bundle);
        Delivery delivery = deliveryService.refreshProxy(bundle.getDelivery());
        Person carrier = event.getCarrier();

        if (currentAlternativeBundleStatus.getStatus() != TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT) {
            notify(new TransportAlternativeRejectedEvent(delivery, alternative), context);
        } else {
            if (transportAlternativeService.decideTransportAlternative(event, carrier, alternative)) {
                List<TransportAlternative> rejectedAlternatives =
                        transportAlternativeService.setOtherTransportAlternativesRejected(event, alternative);

                notify(new TransportAlternativeAcceptedEvent(delivery, carrier, alternative), context);
                for (TransportAlternative rejectedAlternative : rejectedAlternatives) {
                    notify(new TransportAlternativeExpiredEvent(rejectedAlternative), context);
                }
                notify(new TransportAlternativeBundleExpiredEvent(bundle, delivery.getTenant()), context);
            } else {
                notify(new TransportAlternativeRejectedEvent(delivery, event.getTransportAlternative()), context);
            }
        }
    }

    //method needs to be public so that the @Transactional annotation is used
    @Transactional(propagation = Propagation.REQUIRED)
    @EventProcessing
    public TransportAlternativeBundleForSelectionEvent handleTransportAssignmentCancelConfirmation(TransportAssignmentCancelConfirmation event) {

        TransportAssignment assignment = event.getTransportAssignment();
        TransportAlternative alternative = assignment.getTransportAlternative();
        TransportAlternativeBundle bundle = alternative.getTransportAlternativeBundle();
        Delivery delivery = event.getDelivery();

        transportAlternativeService.setTransportAlternativeBundleWaitingForAssignment(event, bundle);

        Tenant tenant = delivery.getTenant();
        return new TransportAlternativeBundleForSelectionEvent(tenant, bundle);
    }

}
