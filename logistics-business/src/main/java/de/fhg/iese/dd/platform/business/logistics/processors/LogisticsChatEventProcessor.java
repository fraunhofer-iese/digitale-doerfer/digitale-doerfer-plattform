/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.TransportAssignmentCreatedEvent;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class LogisticsChatEventProcessor extends BaseEventProcessor {

    @Autowired
    private IChatService chatService;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @EventProcessing
    private void handleDeliveryCreatedEvent(DeliveryCreatedEvent event) {

        Delivery delivery = event.getDelivery();
        Person receiver = delivery.getReceiver().getPerson();
        if(receiver == null){
            log.error("Receiver person is null, could not create chat");
            return;
        }

        PushCategory chatPushCategory = getPushCategory();

        Collection<Chat> existingChats = chatService.findChatsBySubjectAndTopic(
                LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID,
                delivery);
        if (existingChats.isEmpty()) {
            chatService.createChatWithSubjectsAndParticipants(
                    LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID,
                    chatPushCategory,
                    Collections.singletonList(delivery),
                    Collections.singletonList(receiver));
        } else {
            //we have chat(s) connected to the delivery already, currently we just take the first one
            if (existingChats.size() > 1) {
                log.warn("More than one chat existing for delivery '{}', taking first one", delivery.getId());
            }
            Chat chat = existingChats.iterator().next();
            chatService.addParticipantsToChat(chat, receiver);
        }
    }

    @EventProcessing
    private void handleTransportAssignmentCreatedEvent( TransportAssignmentCreatedEvent event) {

        Delivery delivery = event.getDelivery();
        TransportAssignment transportAssignment = event.getTransportAssignment();
        Person carrier = transportAssignment.getCarrier();
        Person receiver = delivery.getReceiver().getPerson();
        if(receiver == null){
            log.error("Receiver person is null, could not create chat");
            return;
        }

        PushCategory chatPushCategory = getPushCategory();

        Collection<Chat> existingChats = chatService.findChatsBySubjectAndTopic(
            LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID,
            delivery);
        if(existingChats.isEmpty()){
            log.error("No chat existing for delivery '{}', creating new one, can lead to duplicates!", delivery.getId());
            chatService.createChatWithSubjectsAndParticipants(
                LogisticsConstants.LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID,
                chatPushCategory,
                Arrays.asList(delivery, transportAssignment),
                Arrays.asList(receiver, carrier));
        }else{
            //we have chat(s) connected to the delivery already, currently we just take the first one
            if(existingChats.size() > 1){
                log.warn("More than one chat existing for delivery '{}', taking first one", delivery.getId());
            }
            Chat chat = existingChats.iterator().next();
            chat = chatService.addSubjectsToChat(chatPushCategory, chat, transportAssignment);
            chatService.addParticipantsToChat(chat, carrier);
        }
    }

    private PushCategory getPushCategory(){
        return pushCategoryService.findPushCategoryById(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_CHAT_ID);
    }

}
