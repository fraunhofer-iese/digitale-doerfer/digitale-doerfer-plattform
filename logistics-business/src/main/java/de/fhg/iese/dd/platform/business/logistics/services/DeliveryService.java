/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Steffen Hupp, Balthasar Weitzel, Axel Wickenkamp, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.DeliveryNotFoundException;
import de.fhg.iese.dd.platform.business.logistics.exceptions.WrongTrackingCodeForDeliveryException;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;

@Service
class DeliveryService extends BaseEntityService<Delivery> implements IDeliveryService {

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private DeliveryStatusRecordRepository deliveryStatusRecordRepository;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private TransportAlternativeBundleRepository transportAlternativeBundleRepository;

    @Autowired
    private TransportAlternativeRepository transportAlternativeRepository;

    @Autowired
    private TrackingCodeService trackingCodeService;

    @Autowired
    private IQRCodeService qrCodeService;

    @Autowired
    private ILogisticsScoreService scoreService;

    @Override
    public Delivery createDelivery(Delivery delivery) {

        String trackingCode;
        if (!StringUtils.equals(delivery.getTenant().getId(), LogisticsConstants.TENANT_ID_DEMO)) {
            trackingCode = trackingCodeService.getTrackingCode();
        } else {
            trackingCode = "123456";
        }

        delivery.setTrackingCode(trackingCode);
        delivery.setCredits(scoreService.calculateShippingCharges(delivery));

        return deliveryRepository.saveAndFlush(delivery);
    }

    @Override
    public Delivery createTrackingCodeLabel(Delivery delivery) {

        String receiverNameFirstLine;
        String receiverNameSecondLine = null;
        ParcelAddress deliveryAddress = delivery.getDeliveryAddress();
        LogisticsParticipant receiver = delivery.getReceiver();

        if (deliveryAddress.getAddressType() == ParcelAddressType.POOLING_STATION) {
            if(receiver.getParticipantType() == LogisticsParticipantType.PRIVATE && receiver.getPerson() != null) {
                receiverNameFirstLine = receiver.getPerson().getFirstName() + " " + receiver.getPerson().getLastName();
            } else {
                if (receiver.getParticipantType() == LogisticsParticipantType.SHOP && receiver.getShop() != null) {
                    receiverNameFirstLine = receiver.getShop().getName();
                } else {
                    receiverNameFirstLine = deliveryAddress.getAddress().getName();
                }
            }
            receiverNameSecondLine =  deliveryAddress.getPoolingStation().getName();
        } else  {
            receiverNameFirstLine = deliveryAddress.getAddress().getName();
        }

        String deliveryStreet = deliveryAddress.getAddress().getStreet();
        String deliveryCity = deliveryAddress.getAddress().getZip()+ " " + deliveryAddress.getAddress().getCity();

        String transportNotes = delivery.getTransportNotes();
        String contentNotes = delivery.getContentNotes();
        String trackingCode = delivery.getTrackingCode();

        Address pickupAddress = delivery.getPickupAddress().getAddress();

        String trackingCodeLabelURL = qrCodeService.generateQRCodePDFURL(
            receiverNameFirstLine,
            receiverNameSecondLine,
            deliveryStreet,
            deliveryCity,
            pickupAddress.getName(),
            pickupAddress.getStreet(),
            pickupAddress.getZip() + " " + pickupAddress.getCity(),
            transportNotes,
            contentNotes,
            trackingCode,
            delivery.getId(),
            delivery.getParcelCount());

        String trackingCodeLabelA4URL = qrCodeService.generateQRCodePDFA4URL(
                receiverNameFirstLine,
                receiverNameSecondLine,
                deliveryStreet,
                deliveryCity,
                pickupAddress.getName(),
                pickupAddress.getStreet(),
                pickupAddress.getZip() + " " + pickupAddress.getCity(),
                transportNotes,
                contentNotes,
                trackingCode,
                delivery.getId(),
                delivery.getParcelCount());

        delivery.setTrackingCodeLabelURL(trackingCodeLabelURL);
        delivery.setTrackingCodeLabelA4URL(trackingCodeLabelA4URL);
        return deliveryRepository.saveAndFlush(delivery);
    }

    @Override
    public Delivery findDeliveryById(String deliveryId) throws DeliveryNotFoundException {
        if(StringUtils.isEmpty(deliveryId)) throw new DeliveryNotFoundException(deliveryId);
        return deliveryRepository.findById(deliveryId)
                .orElseThrow(() -> new DeliveryNotFoundException(deliveryId));
    }

    @Override
    public Collection<Delivery> findAllDeliveries() {
        return deliveryRepository.findAll();
    }

    @Override
    public List<Delivery> findAllDeliveriesByReceiver(Person receiver) {
        return deliveryRepository.findAllByReceiverPersonOrderByCreatedDesc(receiver);
    }

    @Override
    public void cancelDeliveryAndAllRelatedEntities(Delivery delivery, BaseEvent incomingEvent){

        setCurrentStatus(delivery, DeliveryStatus.CANCELLED, incomingEvent);

        transportAssignmentService.findAllByDelivery(delivery).forEach(transportAssignment ->
                transportAssignmentService.setCurrentStatus(transportAssignment, TransportAssignmentStatus.CANCELLED,
                        incomingEvent));

        List<TransportAlternativeBundle> transportAlternativeBundles = transportAlternativeBundleRepository.findAllByDeliveryOrderByCreatedDesc(delivery);
        for(TransportAlternativeBundle transportAlternativeBundle : transportAlternativeBundles){
            transportAlternativeService.setCurrentStatus(transportAlternativeBundle,
                    TransportAlternativeBundleStatus.EXPIRED, false,
                    incomingEvent);

            transportAlternativeRepository.findAllByTransportAlternativeBundleOrderByCreatedDesc(
                    transportAlternativeBundle).forEach(
                    transportAlternative -> transportAlternativeService.setCurrentStatus(transportAlternative,
                            TransportAlternativeStatus.EXPIRED,
                            incomingEvent));

        }
    }

    @Override
    public boolean checkTrackingCode(String message, Delivery delivery, String trackingCode) throws WrongTrackingCodeForDeliveryException, DeliveryNotFoundException {
        if(delivery == null) throw new DeliveryNotFoundException("No delivery found ({})", message);
        if(!StringUtils.equals(delivery.getTrackingCode(), trackingCode))
            throw new WrongTrackingCodeForDeliveryException("DeliveryTrackingCode {} does not match the delivery ({}).", trackingCode, message);
        return true;
    }

    @Override
    public List<Delivery> findAllDeliveriesByReceiverSinceAndAllNotReceived(String receiverId, long sinceTimestamp) {
        return deliveryRepository.findAllDeliveriesByReceiverSinceAndAllNotReceivedOrderByCreatedDesc(receiverId, sinceTimestamp);
    }

    @Override
    public Delivery refresh(Delivery delivery) {
        return deliveryRepository.findById(delivery.getId()).orElse(null);
    }

    @Override
    public List<Delivery> findAllDeliveriesByCustomReferenceNumber(String referenceNumber){
        return deliveryRepository.findAllByCustomReferenceNumber(referenceNumber);
    }

    @Override
    public DeliveryStatusRecord getCurrentStatusRecord(Delivery delivery) {
        return deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery);
    }

    @Override
    public DeliveryStatusRecord setCurrentStatus(Delivery delivery, DeliveryStatus newStatus, BaseEvent event){

        //we need to check that the new status record does not get the same timestamp
        // since this would cause confusion when ordering them
        DeliveryStatusRecord currentStatusRecord = getCurrentStatusRecord(delivery);
        long timestamp = timeService.currentTimeMillisUTC();
        if(currentStatusRecord!= null && timestamp <= currentStatusRecord.getTimeStamp()){
            timestamp++;
            if(timestamp <= currentStatusRecord.getTimeStamp()){
                log.warn("Events from the past for delivery {}, " +
                                "last status record timestamp is {}, current time is {}, taking last timestamp + 1",
                        delivery.getId(),
                        currentStatusRecord.getTimeStamp(), timestamp);
                timestamp = currentStatusRecord.getTimeStamp() + 1;
            }
        }

        return deliveryStatusRecordRepository.saveAndFlush(DeliveryStatusRecord.builder()
                .delivery(delivery)
                .incomingEvent(event.getClass().getSimpleName())
                .status(newStatus)
                .timeStamp(timestamp)
                .build());
    }

    @Override
    public ParcelAddress getCurrentLocation(final DeliveryStatusRecord currentDeliveryStatus) {
        final Delivery delivery = currentDeliveryStatus.getDelivery();
        switch(currentDeliveryStatus.getStatus()){
            case RECEIVED:
                return delivery.getDeliveryAddress();
            case DELIVERED:
            case DELIVERED_POOLING_STATION:
                return delivery.getDeliveryAddress();
            case CREATED:
            case WAITING_FOR_ASSIGNMENT:
            case WAITING_FOR_PICKUP:
                return delivery.getPickupAddress();
            case IN_DELIVERY:
            case CANCELLED:
            case IN_DELIVERY_POOLING_STATION:
            case POOLING_STATION_WAITING_FOR_ASSIGNMENT:
            case POOLING_STATION_WAITING_FOR_PICKUP:
            default:
                return null;
        }
    }

}
