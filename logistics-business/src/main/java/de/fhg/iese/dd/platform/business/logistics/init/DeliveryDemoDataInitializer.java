/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.participants.person.init.PersonDataInitializer;
import de.fhg.iese.dd.platform.business.participants.person.init.PersonDemoDataInitializer;
import de.fhg.iese.dd.platform.business.participants.shop.init.ShopDemoDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;

@Component
public class DeliveryDemoDataInitializer extends BaseDeliveryDataInitializer implements IManualTestDataInitializer {

    public static final String DELIVERY_DEMO_ID_01 = "4272e3f2-07de-4956-9deb-fb79377d176f";
    public static final String DELIVERY_DEMO_ID_02 = "502f6920-f10c-468c-9a46-33360b0d02ba";
    public static final String DELIVERY_DEMO_ID_03 = "c3b2e02c-9484-480f-ab14-86fc386ccd41";
    public static final String DELIVERY_DEMO_ID_04 = "16f98718-7847-4efd-a043-e8d9bd102b91";
    public static final String DELIVERY_DEMO_ID_05 = "8b66d960-a22f-4f0e-9e27-d34f106f01d0";
    public static final String DELIVERY_DEMO_ID_06 = "8945480d-75a6-4deb-b606-ee2c64aebac8";
    public static final String DELIVERY_DEMO_ID_07 = "3b536489-b0be-4bee-87fe-60b940c03c91";
    public static final String DELIVERY_DEMO_ID_08 = "51916cbd-7670-4a4a-9ac2-7b4aafc77c94";
    public static final String DELIVERY_DEMO_ID_09 = "1b0b721a-3fda-4dd8-91c6-3f6b42cb6281";
    public static final String DELIVERY_DEMO_ID_10 = "28cf1df7-d102-4f7c-b764-312432f419e3";
    public static final String DELIVERY_DEMO_ID_11 = "ac508840-fbe0-46c4-a5f3-8adbbf6459c5";
    public static final String DELIVERY_DEMO_ID_12 = "eb6c4413-f1d2-4c1c-9e7d-09c510cec40b";
    public static final String DELIVERY_DEMO_ID_13 = "63d3499d-155f-4e57-8278-2a7db470aed1";

    public static final DeliveryStatus DELIVERY_01_STATUS = DeliveryStatus.CREATED;
    public static final DeliveryStatus DELIVERY_02_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;
    public static final DeliveryStatus DELIVERY_03_STATUS = DeliveryStatus.IN_DELIVERY;
    public static final DeliveryStatus DELIVERY_04_STATUS = DeliveryStatus.RECEIVED;
    public static final DeliveryStatus DELIVERY_05_STATUS = DeliveryStatus.CREATED;
    public static final DeliveryStatus DELIVERY_06_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;
    public static final DeliveryStatus DELIVERY_07_STATUS = DeliveryStatus.CREATED;
    public static final DeliveryStatus DELIVERY_08_STATUS = DeliveryStatus.IN_DELIVERY;
    public static final DeliveryStatus DELIVERY_09_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;
    public static final DeliveryStatus DELIVERY_10_STATUS = DeliveryStatus.IN_DELIVERY;
    public static final DeliveryStatus DELIVERY_11_STATUS = DeliveryStatus.IN_DELIVERY;
    public static final DeliveryStatus DELIVERY_12_STATUS = DeliveryStatus.IN_DELIVERY;
    public static final DeliveryStatus DELIVERY_13_STATUS = DeliveryStatus.WAITING_FOR_ASSIGNMENT;

    @Override
    public String getTopic() {
        return "logistics-demo";
    }

    @Override
    public String getScenarioId() {
        return "logistics-demo-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList("shopping-demo","shop-demo", PersonDataInitializer.TOPIC_DEMO);
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        long timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_01_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÄCKEREI_WS_DREISEN_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_01,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_02_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_RAMOSA_MARKT_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_02,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_03_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_ARLESHOF_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_03,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_04_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_HIT_EISENBERG_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_04,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_05_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÜCHEREI_EISENBERG_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_1)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_05,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_06_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÄCKEREI_WS_DREISEN_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_3)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_06,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_07_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_RAMOSA_MARKT_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_4)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_07,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_08_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_ARLESHOF_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_4)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_08,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_09_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_HIT_EISENBERG_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_5)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_09,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_10_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÄCKEREI_WS_DREISEN_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_5)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_10,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_11_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÜCHEREI_EISENBERG_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_3)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_11,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_12_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_BÄCKEREI_WS_DREISEN_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_1)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_12,
                timeStamp);

        timeStamp = logisticsDataInitService.createRandomTimeStamp(DeliveryDemoDataInitializer.DELIVERY_13_STATUS, DeliveryStatus.CREATED);
        createDelivery(
                logSummary,
                participantService.findOrCreate(shopService.findShopById(ShopDemoDataInitializer.DEMO_RAMOSA_MARKT_UUID)),
                participantService.findOrCreate(personService.findPersonById(PersonDemoDataInitializer.PERSON_ID_DEMO_2)),
                logisticsDataInitService.createDesiredDeliveryTime(timeStamp),
                DELIVERY_DEMO_ID_13,
                timeStamp);

        deliveryStatusRecordRepository.flush();
    }

    @Override
    public void dropDataSimple(LogSummary logSummary) {
        demoLogisticsService.deleteDataForDemoTenant(LogisticsConstants.TENANT_ID_DEMO);
        logSummary.info("dropped all logistics data for demo tenant");
    }

}
