/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.logistics.exceptions.ReceiverPickupAssignmentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ReceiverPickupAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ReceiverPickupAssignmentStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;

@Service
class ReceiverPickupAssignmentService extends BaseEntityService<ReceiverPickupAssignment> implements IReceiverPickupAssignmentService {

    @Autowired
    private ReceiverPickupAssignmentRepository receiverPickupAssignmentRepository;

    @Autowired
    private TransportConfirmationRepository transportConfirmationRepository;

    @Override
    public ReceiverPickupAssignment findReceiverPickupAssignmentById(String receiverPickupAssignmentId) throws ReceiverPickupAssignmentNotFoundException {
        if(receiverPickupAssignmentId == null) throw new ReceiverPickupAssignmentNotFoundException("");

        return receiverPickupAssignmentRepository.findById(receiverPickupAssignmentId)
                .orElseThrow(() -> new ReceiverPickupAssignmentNotFoundException(receiverPickupAssignmentId));
    }

    @Override
    public ReceiverPickupAssignment findWaitingForPickupAssignment(String deliveryId) throws ReceiverPickupAssignmentNotFoundException {
        List<ReceiverPickupAssignment> assignmentsForDelivery = receiverPickupAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(deliveryId);
        for(ReceiverPickupAssignment assignmentForDelivery : assignmentsForDelivery){
            if(assignmentForDelivery.getStatus() == ReceiverPickupAssignmentStatus.WAITING_FOR_PICKUP){
                return assignmentForDelivery;
            }
        }
        throw new ReceiverPickupAssignmentNotFoundException(deliveryId);
    }

    @Override
    public TransportConfirmation createPickupConfirmation(ReceiverPickupAssignment pickupAssignment, CreatorType creatorType, HandoverType handoverType, String notes){ // NOPMD by weitzel
        pickupAssignment.setStatus(ReceiverPickupAssignmentStatus.PICKED_UP);
        pickupAssignment = receiverPickupAssignmentRepository.saveAndFlush(pickupAssignment);
        TransportConfirmation transportConfirmation = TransportConfirmation.builder()
            .creator(creatorType)
            .handover(handoverType)
            .notes(notes)
            .timestamp(timeService.currentTimeMillisUTC())
            .receiverPickupAssignmentPickedUp(pickupAssignment).build();

        return transportConfirmationRepository.saveAndFlush(transportConfirmation);
    }

}
