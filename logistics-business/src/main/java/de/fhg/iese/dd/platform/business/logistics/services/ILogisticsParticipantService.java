/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;

public interface ILogisticsParticipantService extends IEntityService<LogisticsParticipant> {

    /**
     * Find existing LogisticsParticipant or create and store a new
     * LogisticsParticipant for the given person
     *
     * @param person
     * @return
     */
    LogisticsParticipant findOrCreate(Person person);

    /**
     * Find existing LogisticsParticipant or create and store a new
     * LogisticsParticipant for the given shop
     *
     * @param shop
     * @return
     */
    LogisticsParticipant findOrCreate(Shop shop);

}
