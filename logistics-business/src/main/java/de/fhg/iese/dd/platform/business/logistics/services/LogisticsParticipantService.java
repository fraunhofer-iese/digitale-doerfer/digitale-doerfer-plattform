/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Axel Wickenkamp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.LogisticsParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;

@Service
class LogisticsParticipantService extends BaseEntityService<LogisticsParticipant> implements ILogisticsParticipantService {

    @Autowired
    private LogisticsParticipantRepository logisticsParticipantRepository;

    @Override
    public LogisticsParticipant findOrCreate(Person person) {
        List<LogisticsParticipant> partList = logisticsParticipantRepository.findAllByPersonOrderByCreatedDesc( person );
        if (!partList.isEmpty()) {
            return partList.get(0);
        } else {
            LogisticsParticipant newParticipant = new LogisticsParticipant(person);
            newParticipant = logisticsParticipantRepository.saveAndFlush(newParticipant);
            return newParticipant;
        }
    }

    @Override
    public LogisticsParticipant findOrCreate(Shop shop) {
        List<LogisticsParticipant> partList = logisticsParticipantRepository.findAllByShopOrderByCreatedDesc( shop );
        if (!partList.isEmpty()) {
            return partList.get(0);
        } else {
            LogisticsParticipant newParticipant = new LogisticsParticipant(shop);
            newParticipant = logisticsParticipantRepository.saveAndFlush(newParticipant);
            return newParticipant;
        }
    }

}
