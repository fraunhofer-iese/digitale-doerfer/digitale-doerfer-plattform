/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics;

import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;

public class LogisticsEmailTemplate extends TemplateLocation {

    public static final LogisticsEmailTemplate DELIVERY_DELIVERED_OTHER_PERSON = new LogisticsEmailTemplate("deliveryDeliveredOtherPerson.ftl");
    public static final LogisticsEmailTemplate DELIVERY_DELIVERED_PLACEMENT = new LogisticsEmailTemplate("deliveryDeliveredPlacement.ftl");
    public static final LogisticsEmailTemplate DELIVERY_DELIVERED_POOLING_STATION_RECEIVER = new LogisticsEmailTemplate("deliveryDeliveredPoolingStationReceiver.ftl");
    public static final LogisticsEmailTemplate DELIVERY_DELIVERED_RECEIVER = new LogisticsEmailTemplate("deliveryDeliveredReceiver.ftl");
    public static final LogisticsEmailTemplate DELIVERY_DELIVERED_UNKNOWN = new LogisticsEmailTemplate("deliveryDeliveredUnknown.ftl");
    public static final LogisticsEmailTemplate DELIVERY_RECEIVED_RECEIVER = new LogisticsEmailTemplate("deliveryReceivedReceiver.ftl");

    private LogisticsEmailTemplate(String name) {
        super(name);
    }

}
