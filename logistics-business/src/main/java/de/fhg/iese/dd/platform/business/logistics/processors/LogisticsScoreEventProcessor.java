/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryCreatedEvent;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredReceivedConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.ILogisticsScoreService;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class LogisticsScoreEventProcessor extends BaseEventProcessor {

    @Autowired
    private ILogisticsScoreService logisticsScoreService;

    @EventProcessing
    private void handleDeliveryCreatedEvent(DeliveryCreatedEvent event) {
        try {
            // Reserve shipping charges receiver -> dd-logistics
            logisticsScoreService.reserveShippingCharges(event.getDelivery());
        } catch (Exception e) {
            log.error("Failed to book logistics score in DeliveryCreatedEvent", e);
        }
    }

    @EventProcessing
    private void handleDeliveryReadyForTransportConfirmation(DeliveryReadyForTransportConfirmation event) {
        try {
            // Book the reserved shipping charges receiver -> dd-logistics
            logisticsScoreService.bookShippingCharges(event.getDelivery());
        } catch (Exception e) {
            log.error("Failed to book logistics score in DeliveryReadyForTransportConfirmation", e);
        }
    }

    @EventProcessing
    private void handleTransportDeliveredConfirmation(TransportDeliveredConfirmation event) {
        try {
            // Book the transport reward dd-logistics -> carrier
            logisticsScoreService.bookTransportReward(event.getTransportAssignment());
        } catch (Exception e) {
            log.error("Failed to book logistics score in TransportDeliveredConfirmation", e);
        }
    }

    @EventProcessing
    private void handleTransportDeliveredReceivedConfirmation(TransportDeliveredReceivedConfirmation event) {
        try {
            // Book the transport reward dd-logistics -> carrier
            logisticsScoreService.bookTransportReward(event.getTransportAssignment());
        } catch (Exception e) {
            log.error("Failed to book logistics score in TransportDeliveredReceivedConfirmation", e);
        }
    }

}
