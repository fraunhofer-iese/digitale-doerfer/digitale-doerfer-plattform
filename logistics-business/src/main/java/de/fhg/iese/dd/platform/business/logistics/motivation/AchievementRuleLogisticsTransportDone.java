/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Gerbershagen
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.motivation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.logistics.events.TransportDeliveredConfirmation;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.motivation.framework.AchievementRule;
import de.fhg.iese.dd.platform.business.motivation.framework.BaseAchievementRule;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@AchievementRule
public class AchievementRuleLogisticsTransportDone extends BaseAchievementRule<TransportDeliveredConfirmation> {

    private static final String ACHIEVEMENT_ID =          "d8348e42-b4e1-4c94-aa73-abea115192c4";
    private static final String ACHIEVEMENT_LEVEL_ID_01 = "5b4869a9-5b5c-4dd5-b5ed-8a0ca47dc66b";
    private static final String ACHIEVEMENT_LEVEL_ID_05 = "53aeac55-da2a-4aa1-b881-13aed8d6860e";
    private static final String ACHIEVEMENT_LEVEL_ID_10 = "dea2f7ff-b892-4d8c-bf5f-d7ac6e03c171";
    private static final String ACHIEVEMENT_LEVEL_ID_25 = "4b309c84-aa8f-4fa6-8f47-7a7e856a0f52";

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    @Override
    public String getName() {
        return "logistics.score.transport.done.1";
    }

    @Override
    public List<AchievementPersonPairing> checkAchievement(TransportDeliveredConfirmation event) {
        TransportAssignment transportAssignment = event.getTransportAssignment();
        if(transportAssignment == null) {
            return null;
        }
        Person carrier = transportAssignment.getCarrier();
        long timestamp = event.getCreated();

        int numberOfTransportsDone = transportAssignmentService.getNumberOfCompletedTransportAssignmentsForPerson(carrier);

        switch (numberOfTransportsDone) {
            case 1:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, carrier, timestamp));
            case 5:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, carrier, timestamp));
            case 10:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, carrier, timestamp));
            case 25:
                return Collections.singletonList(
                        achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_25, carrier, timestamp));
            default:
                return null;
        }
    }

    @Override
    public Collection<AchievementPersonPairing> checkAchievement(Person person) {
        TransportAssignment lastCompletedAssignment = transportAssignmentService.getLatestCompletedTransportAssignment(person);
        long timestamp = lastCompletedAssignment == null ? timeService.currentTimeMillisUTC() : lastCompletedAssignment.getCreated();

        int numberOfTransportsDone = transportAssignmentService.getNumberOfCompletedTransportAssignmentsForPerson(person);
        Collection<AchievementPersonPairing> achievedLevels = new ArrayList<>();

        if (numberOfTransportsDone >= 1) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_01, person, timestamp));
        }
        if (numberOfTransportsDone >= 5) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_05, person, timestamp));
        }
        if (numberOfTransportsDone >= 10) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_10, person, timestamp));
        }
        if (numberOfTransportsDone >= 25) {
            achievedLevels.add(achievementService.achieveLevel(ACHIEVEMENT_LEVEL_ID_25, person, timestamp));
        }

        return achievedLevels;
    }

    @Override
    public Class<TransportDeliveredConfirmation> getRelevantEvent() {
        return TransportDeliveredConfirmation.class;
    }

    @Override
    public Collection<Achievement> createOrUpdateRelevantAchievements() {
        Achievement achievement = Achievement.builder()
                .name("Logistic.Transport")
                .description("Führe Lieferungen durch um Awards zu bekommen")
                .pushCategory(findPushCategory(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID))
                .category("LieferBar")
                .build();
        achievement.setId(ACHIEVEMENT_ID);
        achievement = achievementService.save(achievement);

        AchievementLevel achievementLevel01 = AchievementLevel.builder()
                .name("Brieftaube")
                .description("Erste Lieferung geschafft - und wieder von ganz alleine zurückgefunden!")
                .challengeDescription("Führe deine erste Lieferung durch.")
                .icon(createIconFromDefault("brieftaube.png"))
                .iconNotAchieved(createIconFromDefault("brieftaube_grey.png"))
                .achievement(achievement)
                .orderValue(1)
                .build();
        achievementLevel01.setId(ACHIEVEMENT_LEVEL_ID_01);
        achievementLevel01 = achievementService.save(achievementLevel01);

        AchievementLevel achievementLevel05 = AchievementLevel.builder()
                .name("Briefträger")
                .description("Fünf Lieferungen! Dann hoffen wir mal, du musstest noch nicht vor Hunden davonlaufen!")
                .challengeDescription("Führe fünf Lieferungen durch.")
                .icon(createIconFromDefault("brieftraeger.png"))
                .iconNotAchieved(createIconFromDefault("brieftraeger_grey.png"))
                .achievement(achievement)
                .orderValue(2)
                .build();
        achievementLevel05.setId(ACHIEVEMENT_LEVEL_ID_05);
        achievementLevel05 = achievementService.save(achievementLevel05);

        AchievementLevel achievementLevel10 = AchievementLevel.builder()
                .name("Liefer-Truck")
                .description("Langsam bekommt der arme, gestresste Mann vom Paketdienst aber so richtig Konkurrenz von dir.")
                .challengeDescription("Führe zehn Lieferungen durch.")
                .icon(createIconFromDefault("liefertruck.png"))
                .iconNotAchieved(createIconFromDefault("liefertruck_grey.png"))
                .achievement(achievement)
                .orderValue(3)
                .build();
        achievementLevel10.setId(ACHIEVEMENT_LEVEL_ID_10);
        achievementLevel10 = achievementService.save(achievementLevel10);

        AchievementLevel achievementLevel25 = AchievementLevel.builder()
                .name("Lieferheld")
                .description("25 Lieferungen! Besorg dir Strumpfhosen, einen Umhang und wähle deinen Superheldennamen!")
                .challengeDescription("Führe 25 Lieferungen durch.")
                .icon(createIconFromDefault("lieferheld.png"))
                .iconNotAchieved(createIconFromDefault("lieferheld_grey.png"))
                .achievement(achievement)
                .orderValue(4)
                .build();
        achievementLevel25.setId(ACHIEVEMENT_LEVEL_ID_25);
        achievementLevel25 = achievementService.save(achievementLevel25);

        achievement.setAchievementLevels(Stream.of(achievementLevel01, achievementLevel05, achievementLevel10, achievementLevel25).collect(Collectors.toSet()));

        return Collections.singletonList(achievement);
    }

    @Override
    public void dropRelevantAchievements() {
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_01);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_05);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_10);
        achievementService.removeAchievementLevelAndAchievementPersonPairings(ACHIEVEMENT_LEVEL_ID_25);
        achievementService.removeAchievement(ACHIEVEMENT_ID);
    }

}
