/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.init;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.logistics.events.TransportAlternativeBundlePeriodicNotificationExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.waiting.services.IWaitingService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;

@Component
public class LogisticsPeriodicWaitingEventsDataInitializer extends BaseDataInitializer {

    @Autowired
    private IWaitingService waitingService;
    @Autowired
    private IAppService appService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("waitingevent")
                .requiredEntity(App.class)
                .processedEntity(AppVariant.class)
                .processedEntity(WaitingDeadline.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        App lieferBarApp = appService.findById(LogisticsConstants.LIEFERBAR_APP_ID);
        Set<AppVariant> lieferBarAppVariants = appService.findAllAppVariants(lieferBarApp);

        Set<Tenant> tenantsWithLieferBar = lieferBarAppVariants.stream()
                .flatMap(l -> appService.getAvailableTenants(l).stream())
                .collect(Collectors.toSet());

        List<String> standardSchedule = Arrays.asList(
                "09:00",
                "10:00",
                "11:30",
                "13:00",
                "14:00",
                "15:00",
                "16:00",
                "17:00",
                "18:00",
                "19:00");

        tenantsWithLieferBar.forEach(t -> registerTransportNotification(logSummary, t, standardSchedule, t.getId()));
    }

    private void registerTransportNotification(LogSummary logSummary, Tenant tenant, List<String> schedule, String id) {
        TransportAlternativeBundlePeriodicNotificationExpiredEvent transportNotification =
                new TransportAlternativeBundlePeriodicNotificationExpiredEvent(tenant, schedule);
        waitingService.registerDeadline(transportNotification, id);
        logSummary.info("Registered transport notification for tenant '{}': {}", tenant.getName(),
                transportNotification.getSchedule());
    }

}
