/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics.processors;

import static de.fhg.iese.dd.platform.business.logistics.processors.LogisticsEmailSenderEventProcessor.buildAcknowledgeUrl;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.logistics.feature.LogisticsShopFeature;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

/**
 * Testing acknowledgeUrl generation.<br>
 * Also note the slightly different shop URLs.
 *
 */
public class LogisticsEmailSenderEventProcessorTest {

    @Test
    public void acknowledgeUrlForPersonalHandoverIsCorrect() {
        final Tenant tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        final LogisticsShopFeature logisticsShopFeature = LogisticsShopFeature.builder()
                .shopUrl("http://this-shop-is-awesome.de")
                .build();
        final Delivery delivery = Delivery.builder().community(tenant).build();
        delivery.setId("d-id");

        final TransportConfirmation confirmation = TransportConfirmation.builder()
                .handover(HandoverType.PERSONALLY)
                .build();

        final String generatedUrl = buildAcknowledgeUrl(delivery, confirmation, "/mailurl", "obfuscatedToken",
                logisticsShopFeature);
        assertEquals("http://this-shop-is-awesome.de/mailurl?deliveryId=d-id&mailToken=obfuscatedToken", generatedUrl);
    }

    @Test
    public void acknowledgeUrlEncodesSpecialCharacters() {
        final Tenant tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        final LogisticsShopFeature logisticsShopFeature = LogisticsShopFeature.builder()
                .shopUrl("http://this-shop-is-awesome.de")
                .build();
        final Delivery delivery = Delivery.builder().community(tenant).build();
        delivery.setId("d-id");

        final TransportConfirmation confirmation = TransportConfirmation.builder()
                .handover(HandoverType.PERSONALLY)
                .build();

        final String generatedUrl = buildAcknowledgeUrl(delivery, confirmation, "/mailurl", "obfuscated=Token",
                logisticsShopFeature);
        assertEquals("http://this-shop-is-awesome.de/mailurl?deliveryId=d-id&mailToken=obfuscated%3DToken",
                generatedUrl);
    }

    @Test
    public void acknowledgeUrlDoesNotFailOnMissingConfirmation() {
        final Tenant tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        final LogisticsShopFeature logisticsShopFeature = LogisticsShopFeature.builder()
                .shopUrl("http://this-shop-is-awesome.de")
                .build();
        final Delivery delivery = Delivery.builder().community(tenant).build();
        delivery.setId("d-id");

        final String generatedUrl = buildAcknowledgeUrl(delivery, null, "/mailurl", "toggenburger",
                logisticsShopFeature);
        assertEquals("http://this-shop-is-awesome.de/mailurl?deliveryId=d-id&mailToken=toggenburger", generatedUrl);
    }

    @Test
    public void acknowledgeUrlForOtherPersonHandoverIsCorrect() {
        final Tenant tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        final LogisticsShopFeature logisticsShopFeature = LogisticsShopFeature.builder()
                .shopUrl("https://this-shop-is-awesome.de")
                .build();
        final Delivery delivery = Delivery.builder().community(tenant).build();
        delivery.setId("d-id");

        final TransportConfirmation confirmation = TransportConfirmation.builder()
                .handover(HandoverType.OTHER_PERSON)
                .acceptorName("Mister Fantastic")
                .build();

        final String generatedUrl = buildAcknowledgeUrl(delivery, confirmation, "/mailurl", "obfuscatedToken",
                logisticsShopFeature);
        assertEquals("https://this-shop-is-awesome.de/mailurl?deliveryId=d-id&mailToken=obfuscatedToken"
                + "&deliveryType=OTHER_PERSON&deliveryInfo=Mister%20Fantastic", generatedUrl);
    }

    @Test
    public void acknowledgeUrlForPlacementHandoverIsCorrect() {
        final Tenant tenant = Tenant.builder()
                .id("098e9db4-de2a-4e03-94fc-f962528c6b0p")
                .name("Testhausen")
                .build();
        final LogisticsShopFeature logisticsShopFeature = LogisticsShopFeature.builder()
                .shopUrl("http://www.this-shop-is-awesome.de")
                .build();
        final Delivery delivery = Delivery.builder().community(tenant).build();
        delivery.setId("d-id");

        final TransportConfirmation confirmation = TransportConfirmation.builder()
                .handover(HandoverType.PLACEMENT)
                .notes("Look\nLeft\nRight")
                .build();

        final String generatedUrl = buildAcknowledgeUrl(delivery, confirmation, "/mailurl", "obfuscatedToken",
                logisticsShopFeature);
        assertEquals("http://www.this-shop-is-awesome.de/mailurl?deliveryId=d-id&mailToken=obfuscatedToken"
                + "&deliveryType=PLACEMENT&deliveryInfo=Look%0ALeft%0ARight", generatedUrl);
    }

}
