/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.processors;

import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;

public interface IChatPushMessageCustomization {

    /**
     * Topic to identify which chats are getting their push messages customized.
     * <p>
     * If {@link Chat#getTopic()} equals this topic,{@link #generatePushMessageText(ChatMessage)} is used to define the
     * push message sent to the user.
     *
     * @return
     */
    String getTopic();

    /**
     * Actual customization method that computes the push message that is sent to the other participants in the chat if
     * the given chat message was added to the chat.
     *
     * @param chatMessage
     *
     * @return
     */
    String generatePushMessageText(ChatMessage chatMessage);

}
