/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientUserGeneratedContentFlagChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.ListUserGeneratedContentFlagsAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("communication/adminui/flag")
@Api(tags = {"communication.adminui.flag"})
public class CommunicationUserGeneratedContentFlagAdminUIController extends BaseAdminUiController {

    @Autowired
    private IChatService chatService;

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Returns details of a flagged chat", notes = "The result can contain deleted entities")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND,
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE,
            ClientExceptionType.CHAT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping("chat/{flagId}")
    public ClientUserGeneratedContentFlagChat getChatFlagById(@PathVariable String flagId) {

        final Person currentPerson = getCurrentPersonNotNull();
        PermissionTenantRestricted getFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        final UserGeneratedContentFlag userGeneratedContentFlag =
                userGeneratedContentFlagService.findByIdWithStatusRecords(flagId);

        if (getFlagsPermission.isTenantDenied(userGeneratedContentFlag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list flag {}", flagId);
        }

        //this ensures that the type of the flag is actually a chat (or subtype of it)
        userGeneratedContentFlagService.checkEntityTypeOrSubtype(userGeneratedContentFlag, Chat.class);

        final Chat flaggedChat =
                chatService.findChatById(userGeneratedContentFlag.getEntityId());
        final long numberOfChatMessages = chatService.getNumberOfMessagesByChat(flaggedChat);

        return communicationClientModelMapper.toClientUserGeneratedContentFlagChat(userGeneratedContentFlag,
                flaggedChat, numberOfChatMessages);
    }

}
