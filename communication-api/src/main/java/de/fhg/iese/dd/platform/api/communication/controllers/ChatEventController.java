/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2019 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.controllers;

import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatFlagRequest;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageSendRequest;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageSentConfirmation;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatReadRequest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.business.communication.events.ChatFlagRequest;
import de.fhg.iese.dd.platform.business.communication.events.ChatMessageSendConfirmation;
import de.fhg.iese.dd.platform.business.communication.events.ChatMessageSendRequest;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;
import de.fhg.iese.dd.platform.datamanagement.communication.config.CommunicationConfig;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("communication/chat/event")
@Api(tags={"communication.chat.events"})
public class ChatEventController extends BaseController {

    @Autowired
    private CommunicationConfig communicationConfig;
    @Autowired
    private IChatService chatService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    private void checkMaxTextLength(final String text) {
        final int maxChars = communicationConfig.getMaxNumberCharsPerMessage();
        if ((text != null) && (text.length() > maxChars)) {
            throw new InvalidEventAttributeException("Message text cannot exceed {} chars", maxChars)
                    .withDetail("message");
        }
    }

    @ApiOperation(value = "Send a chat message to a chat",
            notes = "Only a participant of the chat can send messages. The temporaryMediaItemId is optional")
    @ApiExceptions({
            ClientExceptionType.CHAT_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT,
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @PostMapping("chatMessageSendRequest")
    public ClientChatMessageSentConfirmation sendMessage(
            @Valid @RequestBody ClientChatMessageSendRequest clientRequest) {

        Person sender = getCurrentPersonNotNull();

        final String chatId = clientRequest.getChatId();
        final Chat chat = chatService.findChatByIdAndCheckAccess(chatId, sender);

        final String message = clientRequest.getMessage();
        checkMaxTextLength(message);

        final String temporaryMediaItemId = clientRequest.getTemporaryMediaItemId();
        final TemporaryMediaItem temporaryMediaItem;

        if (StringUtils.isEmpty(temporaryMediaItemId)){
            checkAttributeNotNullOrEmpty(message, "message");
            temporaryMediaItem = null;
        } else {
            temporaryMediaItem = mediaItemService.findTemporaryItemById(sender, temporaryMediaItemId);
        }

        ChatMessageSendRequest chatMessageSendRequest = ChatMessageSendRequest.builder()
                .sender(sender)
                .chat(chat)
                .message(message)
                .temporaryMediaItem(temporaryMediaItem)
                .sentTime(clientRequest.getSentTime())
                .build();

        ChatMessageSendConfirmation chatMessageSendConfirmation = notifyAndWaitForReply(ChatMessageSendConfirmation.class, chatMessageSendRequest);
        return new ClientChatMessageSentConfirmation(communicationClientModelMapper.toClientChatMessage(
                chatMessageSendConfirmation.getChatMessage()));

    }

    @ApiOperation(value = "Set the read timestamp for a chat",
            notes = "Only a participant of the chat can set the read timestamp")
    @ApiExceptions({
            ClientExceptionType.CHAT_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @PostMapping("chatReadRequest")
    public void setRead(
            @Valid @RequestBody ClientChatReadRequest clientRequest) {

        final Person sender = getCurrentPersonNotNull();

        final long readTime = clientRequest.getReadTime();
        final String chatId = clientRequest.getChatId();
        final Chat chat = chatService.findChatByIdAndCheckAccess(chatId, sender);

        chatService.setChatReadTime(chat, sender, readTime);
    }

    @ApiOperation(value = "Flags a chat as inappropriate",
            notes = "caller must be participant of the chat")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT,
            ClientExceptionType.CHAT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @PostMapping(value = "chatFlagRequest")
    public ClientUserGeneratedContentFlagResponse onChatFlagRequest(
            @Valid @RequestBody ClientChatFlagRequest request) {

        final Person flagCreator = getCurrentPersonNotNull();
        final Chat chat = chatService.findChatByIdAndCheckAccess(request.getChatId(), flagCreator);
        //we just choose one other participant out of the chat, if it is a multi person chat, this might be wrong
        //if we want to flag multi person chats, we should flag a single message, not a complete chat
        Person flaggedChatParticipant = chat.getParticipants().stream()
                .map(ChatParticipant::getPerson)
                .filter(person -> !flagCreator.equals(person))
                .findAny()
                .orElse(flagCreator);

        final ChatFlagRequest flagRequest = ChatFlagRequest.builder()
                .entity(chat)
                .entityAuthor(flaggedChatParticipant)
                .entityDescription(String.format("Chat '%s' with participants %n%s", chat.getTopic(),
                        chat.getParticipants().stream()
                                .map(ChatParticipant::getPerson)
                                .map(Objects::toString)
                                .collect(Collectors.joining("\n"))
                ))
                .flagCreator(flagCreator)
                .tenant(flagCreator.getTenant())
                .comment(request.getComment())
                .allowMultipleFlagsOfSameEntity(true)
                .build();

        final UserGeneratedContentFlagConfirmation confirmation =
                notifyAndWaitForReply(UserGeneratedContentFlagConfirmation.class, flagRequest);
        return new ClientUserGeneratedContentFlagResponse(confirmation.getCreatedFlag());
    }

}
