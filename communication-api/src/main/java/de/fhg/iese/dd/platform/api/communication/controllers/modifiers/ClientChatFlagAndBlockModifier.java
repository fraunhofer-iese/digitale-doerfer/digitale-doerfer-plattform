/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.controllers.modifiers;

import java.util.Collections;
import java.util.Set;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;

@Component
public class ClientChatFlagAndBlockModifier implements IOutgoingClientEntityModifier<ClientChat> {

    static final Set<String> CHAT_CONTROLLER_PATHS = Collections.singleton("/communication/chat/**");

    @Override
    public Class<ClientChat> getType() {
        return ClientChat.class;
    }

    @Override
    public Set<String> getPathPatterns() {
        return CHAT_CONTROLLER_PATHS;
    }

    @Override
    public boolean modify(ClientChat clientChat, IContentFilterSettings contentFilterSettings) {
        if (contentFilterSettings.getBlockedPersonIds()
                .stream()
                .anyMatch(id -> clientChat.getChatParticipants()
                        .stream()
                        .map(participant -> participant.getPerson().getId())
                        .anyMatch(id::equals))) {
            //blocked precedes over filtered
            clientChat.setFilterStatus(ClientFilterStatus.BLOCKED);
        } else if (contentFilterSettings.getFlaggedEntityIds().contains(clientChat.getId())) {
            clientChat.setFilterStatus(ClientFilterStatus.FLAGGED);
        }
        return true;
    }

}
