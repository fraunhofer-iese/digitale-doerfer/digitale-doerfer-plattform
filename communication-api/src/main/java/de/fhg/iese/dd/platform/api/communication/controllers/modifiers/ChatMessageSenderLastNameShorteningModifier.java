/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.controllers.modifiers;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;

import java.util.Set;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatMessage;
import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;

@Component
public class ChatMessageSenderLastNameShorteningModifier implements IOutgoingClientEntityModifier<ClientChatMessage> {

    @Override
    public Class<ClientChatMessage> getType() {
        return ClientChatMessage.class;
    }

    @Override
    public Set<String> getPathPatterns() {
        return ClientChatFlagAndBlockModifier.CHAT_CONTROLLER_PATHS;
    }

    @Override
    public boolean modify(ClientChatMessage clientChatMessage, IContentFilterSettings contentFilterSettings) {
        clientChatMessage.setSenderLastName(shorten(clientChatMessage.getSenderLastName()));
        return true;
    }

}
