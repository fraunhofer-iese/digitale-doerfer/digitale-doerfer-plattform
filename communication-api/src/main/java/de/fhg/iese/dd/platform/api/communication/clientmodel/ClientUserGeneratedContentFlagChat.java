/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.clientmodel;

import java.util.List;

import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientUserGeneratedContentFlagChat extends ClientUserGeneratedContentFlagDetail {

    @ApiModelProperty(required = true, notes = "list of chat participating persons")
    private List<ClientPersonReference> chatParticipants;

    @ApiModelProperty(required = true, notes = "machine readable topic of the chat")
    private String topic;

    private long createdTimestamp;

    private long lastMessageTimestamp;

    private long numberOfChatMessages;

}
