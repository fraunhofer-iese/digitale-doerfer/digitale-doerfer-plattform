/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Stefan Schweitzer, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.processors;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageReceivedEvent;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatReadEvent;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatReceivedEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.communication.events.ChatMessageReceivedEvent;
import de.fhg.iese.dd.platform.business.communication.events.ChatReadEvent;
import de.fhg.iese.dd.platform.business.communication.events.ChatReceivedEvent;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.plugin.PluginTarget;
import de.fhg.iese.dd.platform.business.shared.plugin.services.IPluginService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushException;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class ChatPushEventProcessor extends BaseEventProcessor {

    @Autowired
    private IChatService chatService;
    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private IPluginService pluginService;

    private IChatPushMessageCustomization chatPushMessageCustomizationFallback;

    private Map<String, IChatPushMessageCustomization> topicToChatPushMessageCustomization = null;

    @PushEvent(clientEvent = ClientChatReadEvent.class,
            categorySubject = "chat",
            loud = false,
            notes = "Sent to all chat participants if a chat message is marked read by one of the participants")
    @EventProcessing
    private void handleChatReadEvent(final ChatReadEvent readEvent) {
        if (isChatInTwoWorlds(readEvent.getChat())) {
            return;
        }
        final ClientChatReadEvent chatReadEvent = new ClientChatReadEvent(
                chatService.getParticipantByPerson(readEvent.getChat(), readEvent.getPerson()).getId(),
                readEvent.getReadTime());
        notifyChatParticipantsSilent(readEvent.getChat(), chatReadEvent, readEvent.getPerson());
    }

    @PushEvent(clientEvent = ClientChatReceivedEvent.class,
            categorySubject = "chat",
            loud = false,
            notes = "Sent to all chat participants if a chat message is marked received by one of the participants")
    @EventProcessing
    private void handleChatReceivedEvent(final ChatReceivedEvent receivedEvent) {
        if (isChatInTwoWorlds(receivedEvent.getChat())) {
            return;
        }
        final ClientChatReceivedEvent chatReceivedEvent = new ClientChatReceivedEvent(
                chatService.getParticipantByPerson(receivedEvent.getChat(), receivedEvent.getPerson()).getId(),
                receivedEvent.getReceivedTime());
        notifyChatParticipantsSilent(receivedEvent.getChat(), chatReceivedEvent, receivedEvent.getPerson());
    }

    @PushEvent(clientEvent = ClientChatMessageReceivedEvent.class,
            categorySubject = "chat",
            notes = "Sent to all chat participants except the sender if a chat message is added to the chat")
    @PushEvent(clientEvent = ClientChatMessageReceivedEvent.class,
            categorySubject = "chat",
            loud = false,
            notes = "Sent to the sender of a chat message if a chat message is added to the chat")
    @EventProcessing
    private void handleChatMessageReceivedEvent(final ChatMessageReceivedEvent event) {
        final Chat chat = event.getChat();
        if (isChatInTwoWorlds(chat)) {
            return;
        }
        final ChatMessage chatMessage = event.getChatMessage();
        final ChatParticipant chatParticipant = event.getSender();
        final String senderPersonId;
        final String senderChatParticipantId;

        if (chatParticipant != null && chatParticipant.getPerson() != null) {
            senderPersonId = chatParticipant.getPerson().getId();
            senderChatParticipantId = chatParticipant.getId();
        } else {
            senderPersonId = null;
            senderChatParticipantId = null;
        }
        IChatPushMessageCustomization pushMessageCustomization = getChatPushMessageCustomization(chat);

        final ClientChatMessageReceivedEvent clientChatMessageReceivedEvent = ClientChatMessageReceivedEvent.builder()
                .chatId(chat.getId())
                .messageId(chatMessage.getId())
                .senderParticipantId(senderChatParticipantId)
                .senderPersonId(senderPersonId)
                .build();
        final Set<PushCategory> pushCategories = chatService.getPushCategoriesForChat(chat);
        for (final ChatParticipant participant : chat.getParticipants()) {
            Person pushMessageReceiver = participant.getPerson();
            if (!chatMessage.isAutoGenerated() && !Objects.equals(pushMessageReceiver, chatMessage.getSender())) {
                for (PushCategory category : pushCategories) {
                    try {
                        clientPushService.pushToPersonLoud(
                                chatMessage.getSender(),
                                pushMessageReceiver,
                                IClientPushService.PushSendRequest.builder()
                                        .event(clientChatMessageReceivedEvent)
                                        //depending on the chat topic a different message is generated
                                        .message(pushMessageCustomization.generatePushMessageText(chatMessage))
                                        .category(category)
                                        .badgeCount(1)
                                        .build());
                    } catch (PushException e) {
                        log.error("Failed to push chat message to " + participant.getId(), e);
                    }
                }
            } else {
                //the sender of the message gets a silent push, so that devices are kept in sync
                clientPushService.pushToPersonSilent(pushMessageReceiver,
                        IClientPushService.PushSendRequestMultipleCategories.builder()
                                .event(clientChatMessageReceivedEvent)
                                .categories(pushCategories)
                                .build());
            }
        }
    }

    private boolean isChatInTwoWorlds(Chat chat) {
        long countParallelWorldInhabitants = chat.getParticipants().stream()
                .map(ChatParticipant::getPerson)
                .map(Person::getStatuses)
                .filter(s -> s.hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT))
                .count();
        return !(countParallelWorldInhabitants == 0 || countParallelWorldInhabitants == chat.getParticipants().size());
    }

    private void notifyChatParticipantsSilent(final Chat chat, final ClientBaseEvent clientEvent,
            final Person excludedPerson) {
        final Collection<PushCategory> pushCategories = chatService.getPushCategoriesForChat(chat);
        for (final ChatParticipant participant : chat.getParticipants()) {
            if (!Objects.equals(participant.getPerson(), excludedPerson)) {
                try {
                    clientPushService.pushToPersonSilent(participant.getPerson(), clientEvent, pushCategories);
                } catch (PushException e) {
                    log.error("Failed to push chat message to " + participant.getId(), e);
                }
            }
        }
    }

    private IChatPushMessageCustomization getChatPushMessageCustomization(Chat chat) {
        if (topicToChatPushMessageCustomization == null) {
            initializePlugins();
        }
        return topicToChatPushMessageCustomization.getOrDefault(
                chat.getTopic(),
                chatPushMessageCustomizationFallback);
    }

    private void initializePlugins() {
        final List<IChatPushMessageCustomization> chatPushMessageCustomizations =
                pluginService.getPluginInstancesRaw(PluginTarget.of(IChatPushMessageCustomization.class));
        if (!CollectionUtils.isEmpty(chatPushMessageCustomizations)) {
            topicToChatPushMessageCustomization = chatPushMessageCustomizations.stream()
                    .collect(Collectors.toMap(
                            IChatPushMessageCustomization::getTopic,
                            Function.identity()));
        } else {
            topicToChatPushMessageCustomization = Collections.emptyMap();
        }

        chatPushMessageCustomizationFallback = new IChatPushMessageCustomization() {

            @Override
            public String getTopic() {
                //this topic is never used, since it is the fallback
                return "*";
            }

            @Override
            public String generatePushMessageText(ChatMessage chatMessage) {
                Person messageSender = chatMessage.getSender();
                return String.format("Neue Nachricht von %s", messageSender.getFirstName());
            }
        };
    }

}
