/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2019 Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatMessage;
import de.fhg.iese.dd.platform.api.communication.clientmodel.CommunicationClientModelMapper;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags={"communication.chat"})
@RequestMapping("communication/chat")
public class ChatController extends BaseController {

    @Autowired
    private IChatService chatService;

    @Autowired
    private CommunicationClientModelMapper communicationClientModelMapper;

    @ApiOperation(value = "Get chat messages of a chat since timestamp from",
            notes = "The received timestamp for the requesting participant is updated.")
    @ApiExceptions({
        ClientExceptionType.CHAT_NOT_FOUND,
        ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @GetMapping(value = "{chatId}/message")
    public List<ClientChatMessage> getChatMessages(
            @PathVariable String chatId,
            @RequestParam long from)  {

        final Person user = getCurrentPersonNotNull();
        final Chat chat = chatService.findChatByIdAndCheckAccess(chatId, user);

        final Page<ChatMessage> chatMessages = chatService.getLatestMessagesByChatAndUpdateReceived(chat, from, user, 0, Integer.MAX_VALUE);

        return chatMessages.getContent().stream()
                .map(chatMessage -> communicationClientModelMapper.toClientChatMessage(chatMessage))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get chat messages of a chat since timestamp from with pagination",
            notes = "The received timestamp for the requesting participant is updated.")
    @ApiExceptions({
            ClientExceptionType.CHAT_NOT_FOUND,
            ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @GetMapping(value = "{chatId}/message/paged")
    public Page<ClientChatMessage> getChatMessagesPaged(
            @PathVariable String chatId,
            @RequestParam(value = "from", required = false, defaultValue = "0")
            @ApiParam(value = "time from which on the chat messages should be returned",
                    defaultValue = "0") long from,
            @RequestParam(value = "page", required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of chat messages",
                    defaultValue = "0") int page,
            @RequestParam(value = "count", required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of chat messages returned in a page, if 0 no pagination is applied",
                    defaultValue = "10") int count)  {

        if (count == 0) {
            count = Integer.MAX_VALUE;
            page = 0;
        }

        checkPageAndCountValues(page, count);

        final Person user = getCurrentPersonNotNull();
        final Chat chat = chatService.findChatByIdAndCheckAccess(chatId, user);

        final Page<ChatMessage> chatMessages = chatService.getLatestMessagesByChatAndUpdateReceived(chat, from, user, page, count);

        return chatMessages.map(communicationClientModelMapper::toClientChatMessage);
    }

    @ApiOperation(value = "Get a specific chat",
            notes = "Checked if the current user is a participant")
    @ApiExceptions({
        ClientExceptionType.CHAT_NOT_FOUND,
        ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @GetMapping(value = "{chatId}")
    public ClientChat getChat(
            @PathVariable String chatId)  {

        Person user = getCurrentPersonNotNull();
        Chat chat = chatService.findChatByIdAndCheckAccess(chatId, user);
        return communicationClientModelMapper.toClientChat(chat, user);
    }

    @ApiOperation(value = "Get a specific chat message",
            notes = "Checked if the current user is a participant")
    @ApiExceptions({
        ClientExceptionType.CHAT_NOT_FOUND,
        ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            notes = "User must be participant of chat")
    @GetMapping(value = "/message/{chatMessageId}")
    public ClientChatMessage getChatMessage(
            @PathVariable String chatMessageId)  {

        Person user = getCurrentPersonNotNull();
        ChatMessage chatMessage = chatService.findChatMessageByIdAndCheckAccess(chatMessageId, user);
        return communicationClientModelMapper.toClientChatMessage(chatMessage);
    }

}
