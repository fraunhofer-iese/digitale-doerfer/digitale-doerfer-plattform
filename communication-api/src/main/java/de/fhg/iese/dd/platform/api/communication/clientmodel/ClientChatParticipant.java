/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication.clientmodel;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientChatParticipant extends ClientBaseEntity {

    private ClientPersonReference person;

    @ApiModelProperty(
            value = "Timestamp up to which the participant has already received messages. " +
                    "The timestamp refers to the `created` field of the last message. " +
                    "Value can be null if none have been received so far."
    )
    private Long receivedTime;

    @ApiModelProperty(
            value = "Timestamp up to which the participant has already read messages. " +
                    "The timestamp refers to the `created` field of the last message. " +
                    "Value can be null if none have been read so far."
    )
    private Long readTime;

}
