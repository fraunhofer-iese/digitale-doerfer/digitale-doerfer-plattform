/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.services;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.communication.exceptions.ChatNotFoundException;
import de.fhg.iese.dd.platform.business.communication.exceptions.ChatNotUniqueBySubjectsException;
import de.fhg.iese.dd.platform.business.communication.exceptions.NotParticipantOfChatException;
import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatSubject;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

public interface IChatService extends IService {

    /**
     * Create a new chat that pushes notifications about new messages to the app
     * and category specified by the pushCategory. It can be retrieved by the
     * according subjects.
     * <p/>
     * Take care to check if there is a chat with the same subjects already
     * existing that you would like to reuse. There is no check about this,
     * since the semantic of the subject can be different depending on the app.
     *
     * @param topic
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param pushCategory
     *            push category that will be used for push notifications
     * @param subjects
     *            Entities that serve as markers for the chat. They will be
     *            condensed to their short class name and their Id.
     * @return The newly created chat
     */
    Chat createChatWithSubjects(String topic, PushCategory pushCategory, BaseEntity... subjects);

    /**
     * Create a new chat that pushes notifications about new messages to the app
     * and category specified by the pushCategory. It can be retrieved by the
     * according subjects.
     * <p/>
     * Take care to check if there is a chat with the same subjects already
     * existing that you would like to reuse. There is no check about this,
     * since the semantic of the subject can be different depending on the app.
     * <p/>
     * For new messages in the future a push notification to these persons will
     * be sent, if the person is registered for a push endpoint with the app
     * that is referenced by the subjects of the chat.
     *
     * @param topic
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param pushCategory
     *            push category that will be used for push notifications
     * @param subjects
     *            Entities that serve as markers for the chat. They will be
     *            condensed to their short class name and their Id.
     * @param participants
     *            The persons that should be added as participants
     * @return The newly created chat
     */
    Chat createChatWithSubjectsAndParticipants(String topic, PushCategory pushCategory, Collection<BaseEntity> subjects, Collection<Person> participants);

    /**
     * Add the given subjects to the existing chat. The chat can later on be
     * retrieved by the according subjects.
     * <p/>
     * Push notifications about new messages will be sent to the specified push
     * category
     *
     * @param pushCategory
     *            push category that will be used for push notifications
     * @param chat
     *            The chat that will be extended by the subjects
     * @param subjects
     *            Entities that serve as markers for the chat. They will be
     *            condensed to their short class name and their Id.
     * @return The modified chat
     */
    Chat addSubjectsToChat(PushCategory pushCategory, Chat chat, BaseEntity... subjects);

    /**
     * Adds persons to the given chat if they are not already in the list of participants.
     * <p/>
     * These persons can read all previous messages. For new messages in the
     * future a push notification to these persons will be sent, if the person
     * is registered for a push endpoint with the app that is referenced by the
     * subjects of the chat.
     *
     * @param chat
     *            The chat that gets a new participant
     * @param participants
     *            The persons that should be added as participants
     * @return The chat with the new participant
     */
    Chat addParticipantsToChat(Chat chat, Person... participants);

    /**
     * Removes a persons from the given chat if it is in the list of participants.
     * <p/>
     * The removed person can not read any messages of the chat anymore, even the own ones.
     *
     * @param chat
     *         The chat that gets the participant removed
     * @param participantToRemove
     *         The person that should be removed as participant
     *
     * @return The chat with the removed participant
     *
     * @throws NotParticipantOfChatException
     *         if the participant was not part of the chat.
     */
    Chat removeParticipantFromChat(Chat chat, Person participantToRemove) throws NotParticipantOfChatException;

    /**
     * Checks if a person is participant of a given chat.
     *
     * @param chat
     *            That chat that should be used for the chat
     * @param person
     *            The person that should be checked if it is a participant
     * @return true if the person is participant of the chat
     */
    boolean isChatParticipant(Chat chat, Person person);

    /**
     * Checks if a person is allowed to access a given chat.
     *
     * @param chat
     *            That chat that should be used for the chat
     * @param person
     *            The person that should be checked if it is allowed to read/write to that chat
     * @return true if the person is allowed to access the given chat
     */
    boolean isAllowedToAccessChat(Chat chat, Person person);

    /**
     * Checks if a person is allowed to access a given chat and throws an
     * {@link NotParticipantOfChatException} if the person is not allowed to
     * access the given chat.
     *
     * @param chat
     *            That chat that should be used for the chat
     * @param person
     *            The person that should be checked if it is allowed to
     *            read/write to that chat
     * @throws NotParticipantOfChatException
     *             if the person is not allowed to access the given chat.
     */
    void checkIsAllowedToAccessChat(Chat chat, Person person) throws NotParticipantOfChatException;

    /**
     * Create and persist a chat subject entity that can be used to create chats
     * that can be found by the given entity. The entity will be condensed to
     * its short class name and id.
     *
     * @param chat
     *            The chat that should be used for this subject
     * @param subject
     *            An entity that will be condensed into a chat subject. Only the
     *            short class name and id is relevant for that.
     * @param pushCategory
     *            push category that will be used for push notifications
     * @return A chat subject that can be later on used to find chats that deal
     *         with the given entity.
     */
    ChatSubject createChatSubject(Chat chat, BaseEntity subject, PushCategory pushCategory);

    /**
     * Find a chat by id.
     *
     * @param chatId Id of the chat to look for.
     *
     * @return The chat with that id
     *
     * @throws ChatNotFoundException No chat exists with that id
     */
    Chat findChatById(String chatId) throws ChatNotFoundException;

    /**
     * Find a chat by id and check if the person is allowed to access it.
     *
     * @param chatId   Id of the chat to look for.
     * @param accessor The person that wants to access the chat
     *
     * @return The chat with that id
     *
     * @throws ChatNotFoundException         No chat exists with that id
     * @throws NotParticipantOfChatException if the person is not allowed to access the given chat.
     */
    Chat findChatByIdAndCheckAccess(String chatId, Person accessor)
            throws ChatNotFoundException, NotParticipantOfChatException;

    /**
     * Find a chat that is connected to all of these subjects.
     *
     * @param subject
     *            Entity that serves as markers for the chat. It will be
     *            condensed to its short class name and Id.
     * @return The chat that uses all the given entities as subject
     * @throws ChatNotFoundException
     *             No chat was connected to these subjects
     * @throws ChatNotUniqueBySubjectsException
     *             More than one chat is connected to these subjects
     */
    Chat findChatBySubject(BaseEntity subject) throws ChatNotFoundException, ChatNotUniqueBySubjectsException;

    /**
     * Find all chats that are connected to the subject.
     *
     * @param subject
     *            Entity that serves as markers for the chat. It will be
     *            condensed to its short class name and Id.
     * @return All chats that use the given entity as subject
     */
    Collection<Chat> findChatsBySubject(BaseEntity subject);

    /**
     * Find a chat with that topic that is connected to all of these subjects.
     *
     * @param topic
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param subject
     *            Entity that serves as markers for the chat. It will be
     *            condensed to its short class name and Id.
     * @return The chat with that topic that uses all the given entities as subject
     * @throws ChatNotFoundException
     *             No chat was connected to these subjects
     * @throws ChatNotUniqueBySubjectsException
     *             More than one chat is connected to these subjects
     */
    Chat findChatBySubjectAndTopic(String topic, BaseEntity subject) throws ChatNotFoundException, ChatNotUniqueBySubjectsException;

    /**
     * Find all chats with that topic that are connected to all of these subjects.
     *
     * @param topic
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param subject
     *            Entity that serves as markers for the chat. It will be
     *            condensed to its short class name and Id.
     * @return All chats with that topic that use all the given entities as subject
     */
    Collection<Chat> findChatsBySubjectAndTopic(String topic, BaseEntity subject);

    /**
     * Find all chats with that has the person as one of the participants.
     *
     * @param participant
     *         The user that has to be one participant of the chats
     *
     * @return The chats that have the participant in the set of participants
     */
    List<Chat> findChatsByParticipant(Person participant);

    /**
     * Find all chats with that topic that has the person as one of the participants.
     *
     * @param caller      the person to which the chats are listed
     * @param topic       An identifier to additionally distinguish chats that share the same subject
     * @param participant The user that has to be one participant of the chats
     *
     * @return The chats with this topic and the participants include the given one
     */
    List<Chat> findChatsByTopicAndParticipant(Person caller, String topic, Person participant);

    /**
     * Find all chats with that topic that has the person as one of the participants.
     *
     * @param topics
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param participant
     *            The user that has to be one participant of the chats
     * @return The chats with the given topics and the participants include the given one
     */
    List<Chat> findChatsByTopicsAndParticipant(Set<String> topics, Person participant);

    /**
     * Find all chats with that topic that has exactly the person as participants.
     *
     * @param topic
     *            An identifier to additionally distinguish chats that share the
     *            same subject
     * @param participants
     *            The users that are the participants of the chats
     * @return The chats with this topic and exactly these participants
     */
    List<Chat> findChatsByTopicAndParticipants(String topic, Set<Person> participants);

    @Transactional
    void deleteChat(Chat chat);

    /**
     * Add a user written message to the chat.
     * <p/>
     * Push notifications will be sent out to all participants of the chat. To
     * determine which push endpoint to use the push category of the subjects are used.
     *
     * @param chat
     *            The chat to add the message to
     * @param sender
     *            The sender of the message
     * @param message
     *            The actual textual message
     * @param sentTime
     *            Time the message was sent by the client
     * @return The modified chat and the added chat message
     */
    Pair<Chat, ChatMessage> addUserTextMessageToChat(Chat chat, Person sender, String message, long sentTime);

    /**
     * Add a user written message to the chat.
     * <p/>
     * Push notifications will be sent out to all participants of the chat. To
     * determine which push endpoint to use the push category of the subjects
     * are used.
     *
     * @param chat
     *            The chat to add the message to
     * @param sender
     *            The sender of the message
     * @param message
     *            The actual textual message, can be empty
     * @param mediaItem
     *            The actual media item as message
     * @param sentTime
     *            Time the message was sent by the client
     * @return The modified chat and the added chat message
     */
    Pair<Chat, ChatMessage> addUserMediaMessageToChat(Chat chat, Person sender, String message, MediaItem mediaItem, long sentTime);

    /**
     * Add an auto generated message to the chat.
     * <p/>
     * Push notifications will be sent out to all participants of the chat. To
     * determine which push endpoint to use the push category of the subjects are used.
     *
     * @param chat
     *            The chat to add the message to
     * @param message
     *            The actual textual message
     * @param sentTime
     *            Time the message was sent by the client
     * @param loudPush
     *            If true a loud push notification will be sent to the
     *            participants of the chat, otherwise a silent notification is
     *            sent.
     * @return The modified chat and the added chat message
     */
    Pair<Chat, ChatMessage> addAutoGeneratedMessageToChat(Chat chat, String message, long sentTime, boolean loudPush);

    /**
     * Add an auto-generated message with sender to the chat that references an entity.
     *
     * @param chat
     *         The chat to add the message to
     * @param sender
     *         The sender of the message
     * @param message
     *         A textual message that is also part of the chat message, can be null or empty
     * @param referencedEntity
     *         The entity that should be reference in the message
     * @param sentTime
     *         Time the message was sent by the client
     * @param loudPush
     *         If true a loud push notification will be sent to the participants of the chat, otherwise a silent
     *         notification is sent.
     *
     * @return The modified chat and the added chat message
     */
    Pair<Chat, ChatMessage> addReferenceToEntityMessageToChat(Chat chat, Person sender, String message,
            BaseEntity referencedEntity, long sentTime, boolean loudPush);

    /**
     * Get all chat messages that have been sent since the specified time stamp.
     *
     * @param chat
     *            The chat to return the new messages from
     * @param from
     *            Time stamp that will be used to determine if a message will be
     *            returned or not (ChatMessage.created > from)
     * @param user
     *            User for which the received timestamp is to be updated
     * @param page
     *            Page number of the paged list of chat messages
     * @param count
     *            Maximum number of chat messages returned in an page
     *
     * @return All new messages sent since the specified time stamp
     */
    Page<ChatMessage> getLatestMessagesByChatAndUpdateReceived(Chat chat, long from, Person user, int page, int count);

    /**
     * Get a specific chat message.
     *
     * @param chatMessageId
     *            the id of the chat message
     * @return the actual message with the id if it is existing
     * @throws ChatNotFoundException
     *             if there is no message with this id
     * @throws NotParticipantOfChatException
     *             if the person is not allowed to access the given chat.
     */
    ChatMessage findChatMessageByIdAndCheckAccess(String chatMessageId, Person accessor) throws ChatNotFoundException, NotParticipantOfChatException;

    /**
     * Get all push categories for a chat
     *
     * @param chat
     *          Chat for which the push categories are to be retrieved
     * @return
     *          All push categories from the related chat subjects
     */
    Set<PushCategory> getPushCategoriesForChat(Chat chat);

    /**
     * Gets the last chat message in the chat that is not auto-generated or null
     * if there is none.
     *
     * @param chat
     *            The chat to find the last message
     * @return
     *  the latest not auto-generated message or null
     */
    @Nullable
    ChatMessage getLastMessageNotAutoGenerated(Chat chat);

    /**
     * Gets the participant for a person in a chat
     *
     * @return Participant in the chat
     */
    ChatParticipant getParticipantByPerson(Chat chat, Person person) throws NotParticipantOfChatException;

    Long getNumberOfMessagesByChat(Chat chat);

    List<ChatMessage> getAllMessagesByChat(Chat chat);

    void deleteChatBySubjectAndTopic(String topic, BaseEntity subject);

    Long getChatReadTime(Chat chat, Person person);

    void setChatReadTime(Chat chat, Person person, long readTime);

    void unsetChatReadTime(Chat chat, Person person);

    Long getChatReceivedTime(Chat chat, Person person);

    void setChatReceivedTime(Chat chat, Person person, long receivedTime);

    void unsetChatReceivedTime(Chat chat, Person person);

    /**
     * Updates a chat with the correct last chat message and unread count per participant.
     * Only required by admin task, do not use it in regular business logic.
     *
     * @param chat the chat to clean up
     * @param dryRun if true the chat is changed and saved
     *
     * @return a human readable summary of the found inconsistencies, empty if there were none
     */
    String updateLastMessageAndUnreadMessageCount(Chat chat, boolean dryRun);

}
