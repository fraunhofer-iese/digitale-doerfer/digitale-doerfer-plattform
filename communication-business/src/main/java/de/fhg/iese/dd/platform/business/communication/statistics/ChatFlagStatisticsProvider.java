/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagSimpleCountByGeoArea;

@Component
public class ChatFlagStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_FLAGGED_CONTENT_COUNT_CHAT = "communication.flagged-content.count.chat";
    private static final String CHAT_ENTITY_TYPE = Chat.class.getName();

    @Autowired
    private UserGeneratedContentFlagRepository userGeneratedContentFlagRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_FLAGGED_CONTENT_COUNT_CHAT)
                .machineName("flagged-content-count-chat")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(
                t -> userGeneratedContentFlagRepository.countByEntityTypeByGeoAreaId(CHAT_ENTITY_TYPE, t),
                Map.of(STATISTICS_ID_FLAGGED_CONTENT_COUNT_CHAT,
                        UserGeneratedContentFlagSimpleCountByGeoArea::getUserGeneratedContentFlagCount),
                geoAreaInfos, definition);
    }

}
