/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.admintasks;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class RestoreConsistencyOnChatsAdminTask extends BaseAdminTask {

    @Autowired
    private IChatService chatService;
    @Autowired
    private ChatRepository chatRepository;

    @Override
    public String getName() {
        return "RestoreConsistencyOnChats";
    }

    @Override
    public String getDescription() {
        return "Restores consistency on all chats by setting the last message and the unread message count correctly.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws InterruptedException {

        logSummary.info("Restoring consistency for {} chats", chatRepository.count());

        processPages(logSummary, parameters,
                //process all chats
                (pageRequest) -> chatRepository.findAll(pageRequest),
                //and restore the consistency in parallel, according to the parameters
                (chatPage) -> processParallel(
                        (Chat chat) -> restoreConsistency(chat, logSummary, parameters.isDryRun()),
                        chatPage.getContent(), logSummary, parameters));
    }

    private void restoreConsistency(Chat chat, LogSummary logSummary, boolean dryRun){
        String logMessage = chatService.updateLastMessageAndUnreadMessageCount(chat, dryRun);
        logSummary.info("Checked{} {} chat: {}", dryRun ? "" : " and fixed", chat.getId(),
                StringUtils.isEmpty(logMessage) ? "OK" : logMessage);
    }

}
