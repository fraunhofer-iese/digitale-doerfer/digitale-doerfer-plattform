/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.results.ChatCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.results.ChatMessageCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

@Component
public class ChatStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_CHAT_COUNT = "communication.chat.count";
    public static final String STATISTICS_ID_MESSAGE_COUNT = "communication.chat.message.count";

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_CHAT_COUNT)
                .machineName("chat-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .timeRelation(StatisticsTimeRelation.ACTIVE)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_MESSAGE_COUNT)
                .machineName("chat-message-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(chatRepository::countByGeoAreaId, Map.of(
                        STATISTICS_ID_CHAT_COUNT, ChatCountByGeoAreaId::getChatCount),
                geoAreaInfos, definition);
        querySummingStatistics(chatMessageRepository::countByGeoAreaId, Map.of(
                        STATISTICS_ID_MESSAGE_COUNT, ChatMessageCountByGeoAreaId::getMessageCount),
                geoAreaInfos, definition);

        calculateActiveChatStatistics(geoAreaInfos, definition);
    }

    private void calculateActiveChatStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {
        if (!definition.getStatisticsIds().contains(STATISTICS_ID_CHAT_COUNT)) {
            return;
        }

        final List<ChatCountByGeoAreaId> rawCountsActive =
                chatRepository.countActiveChatsByGeoAreaId(definition.getStartTimeActive());
        final Map<String, ChatCountByGeoAreaId> chatCountByGeoAreaIdActive = rawCountsActive.stream()
                .collect(Collectors.toMap(ChatCountByGeoAreaId::getGeoAreaId, Function.identity()));

        for (GeoAreaStatistics geoAreaInfo : geoAreaInfos) {
            Collection<GeoArea> childAreas = geoAreaService.getAllChildAreasIncludingSelf(geoAreaInfo.getGeoArea());
            geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_CHAT_COUNT, StatisticsTimeRelation.ACTIVE,
                    sumValues(chatCountByGeoAreaIdActive, childAreas, ChatCountByGeoAreaId::getChatCount));
        }
    }

}
