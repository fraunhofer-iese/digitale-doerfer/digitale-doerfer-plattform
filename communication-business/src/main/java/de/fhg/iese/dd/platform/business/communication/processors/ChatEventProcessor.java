/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.processors;

import java.util.Objects;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.communication.events.ChatMessageSendConfirmation;
import de.fhg.iese.dd.platform.business.communication.events.ChatMessageSendRequest;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@EventProcessor
public class ChatEventProcessor extends BaseEventProcessor {

    @Autowired
    private IChatService chatService;

    @Autowired
    private IMediaItemService mediaItemService;

    @EventProcessing
    private ChatMessageSendConfirmation handleChatMessageSendRequest(ChatMessageSendRequest chatMessageSendRequest) {

        final TemporaryMediaItem temporaryMediaItem = chatMessageSendRequest.getTemporaryMediaItem();

        Pair<Chat, ChatMessage> pairChatChatMessage;

        if (Objects.isNull(temporaryMediaItem)) {
            pairChatChatMessage = chatService.addUserTextMessageToChat(
                    chatMessageSendRequest.getChat(),
                    chatMessageSendRequest.getSender(),
                    chatMessageSendRequest.getMessage(),
                    chatMessageSendRequest.getSentTime());
        } else {
            pairChatChatMessage = chatService.addUserMediaMessageToChat(
                    chatMessageSendRequest.getChat(),
                    chatMessageSendRequest.getSender(),
                    chatMessageSendRequest.getMessage(),
                    mediaItemService.useItem(temporaryMediaItem, FileOwnership.of(temporaryMediaItem.getOwner())),
                    chatMessageSendRequest.getSentTime());
        }

        return new ChatMessageSendConfirmation(pairChatChatMessage.getRight());
    }

}
