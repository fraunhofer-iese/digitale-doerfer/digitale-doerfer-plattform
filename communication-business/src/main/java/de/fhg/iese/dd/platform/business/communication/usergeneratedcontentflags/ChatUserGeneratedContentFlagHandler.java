/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.usergeneratedcontentflags;

import java.util.Collection;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.UserGeneratedContentFlagEntityCanNotBeDeletedException;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.BaseUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;

@Component
public class ChatUserGeneratedContentFlagHandler extends BaseUserGeneratedContentFlagHandler {

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES = unmodifiableList(Chat.class);

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag)
            throws UserGeneratedContentFlagEntityCanNotBeDeletedException {
        throw UserGeneratedContentFlagEntityCanNotBeDeletedException.forNotDeletableEntity(flag);
    }

    @Override
    public String getAdminUiDetailPagePath(UserGeneratedContentFlag flag) {
        return "flagged-contents/details/chat/" + flag.getId();

    }

}
