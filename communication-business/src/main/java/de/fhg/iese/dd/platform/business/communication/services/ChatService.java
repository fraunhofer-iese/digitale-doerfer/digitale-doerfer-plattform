/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Stefan Schweitzer, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.communication.events.ChatMessageReceivedEvent;
import de.fhg.iese.dd.platform.business.communication.events.ChatReadEvent;
import de.fhg.iese.dd.platform.business.communication.events.ChatReceivedEvent;
import de.fhg.iese.dd.platform.business.communication.exceptions.ChatNotFoundException;
import de.fhg.iese.dd.platform.business.communication.exceptions.ChatNotUniqueBySubjectsException;
import de.fhg.iese.dd.platform.business.communication.exceptions.InvalidChatMessageException;
import de.fhg.iese.dd.platform.business.communication.exceptions.NotParticipantOfChatException;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatSubject;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatSubjectRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.NonNull;

@Service
class ChatService extends BaseService implements IChatService {

    private static final long ALLOWED_SENT_TIME_RANGE = TimeUnit.HOURS.toMillis(1);

    @Autowired
    private ChatRepository chatRepository;
    @Autowired
    private ChatSubjectRepository chatSubjectRepository;
    @Autowired
    private ChatParticipantRepository chatParticipantRepository;
    @Autowired
    private ChatMessageRepository chatMessageRepository;
    @Autowired
    private IPushCategoryService pushCategoryService;

    private ChatParticipant participantOf(final Chat chat, final Person person) {
        return ChatParticipant.builder()
                .chat(chat)
                .person(person)
                .build();
    }

    @Override
    @Transactional
    public Chat addParticipantsToChat(Chat chat, Person... participants) {
        for (final Person person : participants) {
            if (!isChatParticipant(chat, person)) {
                chat.getParticipants().add(participantOf(chat, person));
            }
        }
        return chatRepository.saveAndFlush(chat);
    }

    @Override
    public Chat removeParticipantFromChat(@NonNull Chat chat, @NonNull Person participantToRemove) throws NotParticipantOfChatException {
        checkIsAllowedToAccessChat(chat, participantToRemove);
        chat.setParticipants(chat.getParticipants().stream()
                .filter(cp -> ! participantToRemove.equals(cp.getPerson()))
                .collect(Collectors.toSet()));
        return chatRepository.saveAndFlush(chat);
    }

    @Override
    public boolean isChatParticipant(Chat chat, Person person) {
        return chat.getParticipants().stream()
                .anyMatch(participant -> person.equals(participant.getPerson()));
    }

    @Override
    public boolean isAllowedToAccessChat(Chat chat, Person person) {
        return isChatParticipant(chat, person);
    }

    @Override
    public void checkIsAllowedToAccessChat(Chat chat, Person person) throws NotParticipantOfChatException {
        if (!isAllowedToAccessChat(chat, person)) {
            throw new NotParticipantOfChatException("The person '{}' is not a participant of chat '{}'",
                    person.getId(), chat.getId()).withDetail(chat.getId());
        }
    }

    @Override
    @Transactional
    public Chat createChatWithSubjects(String topic, PushCategory pushCategory, BaseEntity... subjects) {
        Chat chat = new Chat(topic);
        chat = timeService.setCreatedToNow(chat);
        chat = chatRepository.save(chat);
        Set<ChatSubject> chatSubjects = createChatSubjects(chat, pushCategory, Arrays.asList(subjects));
        chat.getSubjects().addAll(chatSubjects);
        return chatRepository.saveAndFlush(chat);
    }

    @Override
    @Transactional
    public Chat createChatWithSubjectsAndParticipants(String topic, PushCategory pushCategory,
            Collection<BaseEntity> subjects, Collection<Person> participants) {
        Chat chat = new Chat(topic);
        chat = timeService.setCreatedToNow(chat);
        final Chat createdChat = chatRepository.save(chat);
        Set<ChatSubject> chatSubjects = createChatSubjects(createdChat, pushCategory, subjects);
        createdChat.getSubjects().addAll(chatSubjects);

        participants.stream()
                .distinct() // prevents persons from appearing more than once in a chat
                .map(person -> participantOf(createdChat, person))
                .forEachOrdered(participant -> createdChat.getParticipants().add(participant));

        return chatRepository.saveAndFlush(createdChat);
    }

    @Override
    @Transactional
    public Chat addSubjectsToChat(PushCategory pushCategory, Chat chat, BaseEntity... subjects) {
        Set<ChatSubject> chatSubjects = createChatSubjects(chat, pushCategory, Arrays.asList(subjects));
        chat.getSubjects().addAll(chatSubjects);
        return chatRepository.saveAndFlush(chat);
    }

    private String toEntityName(BaseEntity entity) {
        return entity.getClass().getName();
    }

    private String toSubjectMessage(BaseEntity entity) {
        return "[entityName= " + toEntityName(entity) + ", entityId= " + entity.getId() + "]";
    }

    private Set<ChatSubject> createChatSubjects(Chat chat, PushCategory pushCategory, Collection<BaseEntity> subjects) {
        return subjects.stream()
                .map(subject -> createChatSubject(chat, subject, pushCategory))
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public ChatSubject createChatSubject(Chat chat, BaseEntity subject, PushCategory pushCategory) {
        ChatSubject chatSubject = ChatSubject.builder()
                .entityName(toEntityName(subject))
                .entityId(subject.getId())
                .pushCategory(pushCategory)
                .chat(chat)
                .build();
        chatSubject = timeService.setCreatedToNow(chatSubject);
        return chatSubjectRepository.saveAndFlush(chatSubject);
    }

    @Override
    @Transactional(readOnly = true)
    public Chat findChatById(String chatId) throws ChatNotFoundException {
        return chatRepository.findById(chatId)
                .orElseThrow(() -> new ChatNotFoundException("Chat with id {} not found.", chatId));
    }

    @Override
    @Transactional(readOnly = true)
    public Chat findChatByIdAndCheckAccess(String chatId, Person accessor)
            throws ChatNotFoundException, NotParticipantOfChatException {
        Chat chat = findChatById(chatId);
        checkIsAllowedToAccessChat(chat, accessor);
        return chat;
    }

    @Override
    @Transactional(readOnly = true)
    public Chat findChatBySubject(BaseEntity subject) throws ChatNotFoundException, ChatNotUniqueBySubjectsException {
        Collection<Chat> chats = findChatsBySubject(subject);
        if (chats == null || chats.isEmpty()) {
            String subjectMessage = toSubjectMessage(subject);
            throw new ChatNotFoundException("Chat with subject '{}' not found.", subjectMessage);
        }
        if (chats.size() > 1) {
            String subjectMessage = toSubjectMessage(subject);
            throw new ChatNotUniqueBySubjectsException("Multiple chats with subject '{}' found.", subjectMessage);
        }
        return chats.iterator().next();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Chat> findChatsBySubject(BaseEntity subject) {
        return chatRepository.findBySubjectEntityOrderByCreatedDesc(toEntityName(subject), subject.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chat> findChatsByParticipant(Person participant) {
        return chatRepository.findByParticipantsContainingOrderByCreatedDesc(participant);
    }

    @Override
    @Transactional(readOnly = true)
    public Chat findChatBySubjectAndTopic(String topic, BaseEntity subject)
            throws ChatNotFoundException, ChatNotUniqueBySubjectsException {
        Collection<Chat> chats = findChatsBySubjectAndTopic(topic, subject);
        if (chats == null || chats.isEmpty()) {
            String subjectMessage = toSubjectMessage(subject);
            throw new ChatNotFoundException("Chat with subject '{}' and topic '{}' not found.", subjectMessage, topic);
        }
        if (chats.size() > 1) {
            String subjectMessage = toSubjectMessage(subject);
            throw new ChatNotUniqueBySubjectsException("Multiple chats with subject '{}' and '{}' found.",
                    subjectMessage, topic);
        }
        return chats.iterator().next();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Chat> findChatsBySubjectAndTopic(String topic, BaseEntity subject) {
        return chatRepository.findByTopicAndSubjectEntityOrderByCreatedDesc(topic, toEntityName(subject),
                subject.getId());
    }

    @Override
    public List<Chat> findChatsByTopicAndParticipants(String topic, Set<Person> participants) {
        return chatRepository.findByTopicAndParticipantsOrderByCreatedDesc(topic, participants, participants.size());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chat> findChatsByTopicAndParticipant(Person caller, String topic, Person participant) {

        if (caller.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT)) {
            //the person is parallel world inhabitant -> all chats are returned
            return chatRepository.findByTopicAndParticipantsOrderByCreatedDesc(topic, participant);
        } else {
            //the person is not parallel world inhabitant -> all chats with person from the parallel world should be removed
            return chatRepository.findByTopicAndParticipantsOrderByCreatedDesc(topic, participant)
                    .stream()
                    .filter(chat -> chat.getParticipants().stream()
                            .noneMatch(
                                    p -> p.getPerson().getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT)))
                    .collect(Collectors.toList());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chat> findChatsByTopicsAndParticipant(Set<String> topics, Person participant) {
        return chatRepository.findByTopicsAndParticipantsOrderByCreatedDesc(topics, participant);
    }

    @Override
    @Transactional
    public void deleteChatBySubjectAndTopic(String topic, BaseEntity subject) {
        Collection<Chat> chats = findChatsBySubjectAndTopic(topic, subject);
        chats.forEach(c -> chatMessageRepository.deleteByChatId(c.getId()));
        chatRepository.deleteAll(chats);
    }

    @Override
    @Transactional
    public void deleteChat(Chat chat) {
        Objects.requireNonNull(chat);
        chatMessageRepository.deleteByChatId(chat.getId());
        chatRepository.delete(chat);
    }

    @Override
    @Transactional
    public Pair<Chat, ChatMessage> addUserTextMessageToChat(Chat chat, Person sender, String message, long sentTime) {
        if (StringUtils.isEmpty(message)) {
            throw new InvalidChatMessageException("message is empty").withDetail("message");
        }
        ChatMessage chatMessage = ChatMessage.builder()
                .sender(sender)
                .message(message)
                .mediaItem(null)
                .autoGenerated(false)
                .sentTime(adjustSentTime(sentTime))
                .referencedEntityId(null)
                .referencedEntityName(null)
                .chat(chat)
                .build();
        chatMessage = timeService.setCreatedToNow(chatMessage);
        return addMessageToChat(chatMessage, chat, true);
    }

    @Override
    @Transactional
    public Pair<Chat, ChatMessage> addUserMediaMessageToChat(Chat chat, Person sender, String message,
            MediaItem mediaItem, long sentTime) {
        if (mediaItem == null) {
            throw new InvalidChatMessageException("mediaItem is null").withDetail("mediaItem");
        }
        ChatMessage chatMessage = ChatMessage.builder()
                .sender(sender)
                .message(message)
                .mediaItem(mediaItem)
                .autoGenerated(false)
                .sentTime(adjustSentTime(sentTime))
                .referencedEntityId(null)
                .referencedEntityName(null)
                .chat(chat)
                .build();
        chatMessage = timeService.setCreatedToNow(chatMessage);
        return addMessageToChat(chatMessage, chat, true);
    }

    @Override
    @Transactional
    public Pair<Chat, ChatMessage> addAutoGeneratedMessageToChat(Chat chat, String message, long sentTime,
            boolean loudPush) {
        ChatMessage chatMessage = ChatMessage.builder()
                .sender(null)
                .message(message)
                .mediaItem(null)
                .autoGenerated(true)
                .sentTime(adjustSentTime(sentTime))
                .referencedEntityId(null)
                .referencedEntityName(null)
                .chat(chat)
                .build();
        chatMessage = timeService.setCreatedToNow(chatMessage);
        return addMessageToChat(chatMessage, chat, loudPush);
    }

    @Override
    @Transactional
    public Pair<Chat, ChatMessage> addReferenceToEntityMessageToChat(Chat chat, Person sender, String message,
            BaseEntity referencedEntity, long sentTime, boolean loudPush) {
        ChatMessage chatMessage = ChatMessage.builder()
                .sender(sender)
                .message(message)
                .mediaItem(null)
                .autoGenerated(true)
                .sentTime(adjustSentTime(sentTime))
                .referencedEntityId(referencedEntity.getId())
                .referencedEntityName(toEntityName(referencedEntity))
                .chat(chat)
                .build();
        chatMessage = timeService.setCreatedToNow(chatMessage);
        return addMessageToChat(chatMessage, chat, loudPush);
    }

    @Transactional
    public Pair<Chat, ChatMessage> addMessageToChat(ChatMessage chatMessage, Chat chat, boolean loudPush) {

        //we need to ensure that the new message we want to add is a bit later than the previous one, so that we have a stable ordering
        ChatMessage lastMessage = getLastMessage(chat);
        if (lastMessage != null && lastMessage.getCreated() == chatMessage.getCreated()) {
            chatMessage.setCreated(chatMessage.getCreated() + 1);
        }
        chatMessage.setChat(chat);

        ChatMessage savedChatMessage = chatMessageRepository.saveAndFlush(chatMessage);
        if(!chatMessage.isAutoGenerated()){
            //in case the message is a user generated message save it in the chat
            chat.setLastMessageNotAutoGenerated(savedChatMessage);
            chatRepository.saveAndFlush(chat);
        }

        updateUnreadMessageCountPerParticipant(chat);

        final ChatParticipant sendingChatParticipant;
        if(chatMessage.getSender() != null) {
            sendingChatParticipant = chat.getParticipants().stream()
                    .filter(cp -> Objects.equals(cp.getPerson(), chatMessage.getSender()))
                    .findFirst()
                    .orElse(null);
        }else{
            sendingChatParticipant = null;
        }

        notify(ChatMessageReceivedEvent.builder()
                .chat(chat)
                .chatMessage(chatMessage)
                .sender(sendingChatParticipant)
                .loudPush(loudPush)
                .build());
        return Pair.of(chat, savedChatMessage);
    }

    @Override
    public ChatParticipant getParticipantByPerson(final Chat chat, final Person person) throws NotParticipantOfChatException {
        return chat.getParticipants().stream()
                .filter(chatParticipant -> Objects.equals(chatParticipant.getPerson(), person))
                .findFirst()
                .orElseThrow(() ->
                        new NotParticipantOfChatException("The person '{}' is not a participant of chat '{}'",
                                person.getId(), chat.getId()).withDetail(chat.getId()));
    }

    @Override
    @Transactional(readOnly = true)
    public Long getNumberOfMessagesByChat(Chat chat) {
        return chatMessageRepository.countAllByChat(chat);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChatMessage> getAllMessagesByChat(Chat chat) {
        return chatMessageRepository.findAllByChatOrderByCreatedDesc(chat);
    }

    @Override
    @Transactional
    public Page<ChatMessage> getLatestMessagesByChatAndUpdateReceived(final Chat chat, final long from,
            final Person user, int page, int count) {
        final Pageable pageable = PageRequest.of(page, count, Sort.Direction.DESC, "created");
        final Page<ChatMessage> chatMessages =
                chatMessageRepository.findAllByChatAndCreatedGreaterThan(chat, from, pageable);
        chatMessages.stream()
                .findFirst()
                .ifPresent(lastChatMessage -> setChatReceivedTime(chat, user, lastChatMessage.getCreated()));
        return chatMessages;
    }

    @Override
    @Transactional(readOnly = true)
    public ChatMessage getLastMessageNotAutoGenerated(Chat chat) {
        return chatMessageRepository.findFirstByChatAndAutoGeneratedFalseOrderByCreatedDesc(chat);
    }

    @Transactional(readOnly = true)
    public ChatMessage getLastMessage(Chat chat) {
        return chatMessageRepository.findFirstByChatOrderByCreatedDesc(chat);
    }

    @Override
    public ChatMessage findChatMessageByIdAndCheckAccess(String chatMessageId, Person accessor)
            throws ChatNotFoundException, NotParticipantOfChatException {

        ChatMessage chatMessage = chatMessageRepository.findById(chatMessageId)
                .orElseThrow(
                        () -> new ChatNotFoundException("Chat message with id {} could not be found.", chatMessageId));

        checkIsAllowedToAccessChat(chatMessage.getChat(), accessor);

        return chatMessage;
    }

    private long adjustSentTime(long actualSentTime) {
        long now = timeService.currentTimeMillisUTC();

        if (actualSentTime > now + ALLOWED_SENT_TIME_RANGE || actualSentTime < now - ALLOWED_SENT_TIME_RANGE) {
            return now;
        } else {
            return actualSentTime;
        }
    }

    @Override
    public Set<PushCategory> getPushCategoriesForChat(final Chat chat) {
        return chat.getSubjects().stream()
                .map(s -> pushCategoryService.findPushCategoryById(s.getPushCategory().getId()))
                .collect(Collectors.toSet());
    }

    @Override
    public Long getChatReadTime(final Chat chat, final Person person) throws NotParticipantOfChatException {

        return getChatParticipant(chat, person).getReadTime();
    }

    @Override
    public void setChatReadTime(final Chat chat, final Person person, final long readTime) {
        ChatParticipant chatParticipant = getChatParticipant(chat, person);
        final Long lastReadTime = chatParticipant.getReadTime();
        if ((lastReadTime == null) || (readTime > lastReadTime)) {
            //the new read time is actually newer, so we use it and push it
            chatParticipant.setReadTime(readTime);
            updateUnreadMessageCount(chatParticipant);
            chatParticipantRepository.saveAndFlush(chatParticipant);
            notify(new ChatReadEvent(chat, person, readTime));
        }
    }

    @Override
    public void unsetChatReadTime(Chat chat, Person person) {
        ChatParticipant chatParticipant = getChatParticipant(chat, person);
        chatParticipant.setReadTime(null);
        updateUnreadMessageCount(chatParticipant);
        chatParticipantRepository.saveAndFlush(chatParticipant);
    }

    @Override
    public Long getChatReceivedTime(final Chat chat, final Person person) {

        return getChatParticipant(chat, person).getReceivedTime();
    }

    @Override
    public void setChatReceivedTime(final Chat chat, final Person person, final long receivedTime) {
        ChatParticipant chatParticipant = getChatParticipant(chat, person);
        final Long lastReceivedTime = chatParticipant.getReceivedTime();
        if ((lastReceivedTime == null) || (receivedTime > lastReceivedTime)) {
            //the new received time is actually newer, so we use it and push it
            chatParticipant.setReceivedTime(receivedTime);
            chatParticipantRepository.saveAndFlush(chatParticipant);
            notify(new ChatReceivedEvent(chat, person, receivedTime));
        }
    }

    @Override
    public void unsetChatReceivedTime(Chat chat, Person person) {
        ChatParticipant chatParticipant = getChatParticipant(chat, person);
        chatParticipant.setReceivedTime(null);
        chatParticipantRepository.saveAndFlush(chatParticipant);
    }

    private ChatParticipant getChatParticipant(Chat chat, Person person) throws NotParticipantOfChatException {
        return chat.getParticipants().stream()
                .filter(cp -> person.equals(cp.getPerson()))
                .findAny()
                .orElseThrow(() ->
                        new NotParticipantOfChatException("The person '{}' is not a participant of chat '{}'",
                                person.getId(), chat.getId()).withDetail(chat.getId()));
    }

    private void updateUnreadMessageCountPerParticipant(Chat chat){
        for (ChatParticipant chatParticipant : chat.getParticipants()) {
            updateUnreadMessageCount(chatParticipant);
            chatParticipantRepository.save(chatParticipant);
        }
        chatParticipantRepository.flush();
    }

    private void updateUnreadMessageCount(ChatParticipant chatParticipant){
        chatParticipant.setUnreadMessageCount(
                getUnreadMessageCount(chatParticipant.getChat(), chatParticipant.getPerson(),
                        chatParticipant.getReadTime()));
    }

    private long getUnreadMessageCount(Chat chat, Person person, Long readTime) {
        if (readTime == null) {
            return chatMessageRepository.countChatMessagesNotSentByPerson(chat, person);
        } else {
            return chatMessageRepository.countChatMessagesNewerThanTimestampAndNotSentByPerson(chat, readTime, person);
        }
    }

    @Override
    public String updateLastMessageAndUnreadMessageCount(Chat chat, boolean dryRun) {

        //check if the last message saved in the chat actually is the last one
        boolean lastMessageInconsistent = false;
        ChatMessage lastMessageNotAutoGenerated = getLastMessageNotAutoGenerated(chat);
        if (!Objects.equals(chat.getLastMessageNotAutoGenerated(), lastMessageNotAutoGenerated)) {
            if (!dryRun) {
                chat.setLastMessageNotAutoGenerated(lastMessageNotAutoGenerated);
                chatRepository.saveAndFlush(chat);
            }
            lastMessageInconsistent = true;
        }

        //check for each chat participant if the unread message count is correct
        boolean unreadMessageCountInconsistent = false;
        for (ChatParticipant chatParticipant : chat.getParticipants()) {
            long unreadMessageCount = getUnreadMessageCount(chatParticipant.getChat(), chatParticipant.getPerson(),
                    chatParticipant.getReadTime());
            if ((chatParticipant.getUnreadMessageCount() != null &&
                    chatParticipant.getUnreadMessageCount() != unreadMessageCount)
                    || (chatParticipant.getUnreadMessageCount() == null && unreadMessageCount != 0)) {

                if (!dryRun) {
                    chatParticipant.setUnreadMessageCount(unreadMessageCount);
                    chatParticipantRepository.save(chatParticipant);
                }
                unreadMessageCountInconsistent = true;
            }
        }
        chatParticipantRepository.flush();

        if (lastMessageInconsistent && unreadMessageCountInconsistent) {
            return "lastMessage and unreadMessage count inconsistent";
        } else {
            if (lastMessageInconsistent) {
                return "lastMessage inconsistent";
            }
            if (unreadMessageCountInconsistent) {
                return "unreadMessage count inconsistent";
            }
        }
        return "";
    }

}
