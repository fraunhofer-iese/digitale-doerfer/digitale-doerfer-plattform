/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Adeline Silva Schäfer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication.dataprivacy;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Component
public class ChatDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Chat.class);

    @Autowired
    private IChatService chatService;

    @Autowired
    private ITimeService timeService;

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        List<Chat> ownChats = chatService.findChatsByParticipant(person);
        if (CollectionUtils.isEmpty(ownChats)) {
            return;
        }
        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("Chats", "Private Kommunikation");

        for (Chat chat : ownChats) {
            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Chat",
                            "Erstellt am " + timeService.toLocalTimeHumanReadable(chat.getCreated()));
            subSection.newContent("Teilnehmer", getParticipants(chat.getParticipants()));
            final Long readTime = chatService.getChatReadTime(chat, person);
            if (readTime != null) {
                subSection.newContent("Gelesen bis", timeService.toLocalTimeHumanReadable(readTime));
            }
            final Long receiveTime = chatService.getChatReceivedTime(chat, person);
            if (receiveTime != null) {
                subSection.newContent("Empfangen bis", timeService.toLocalTimeHumanReadable(receiveTime));
            }
            List<ChatMessage> chatMessages = chatService.getAllMessagesByChat(chat);
            for (ChatMessage chatMessage : chatMessages) {
                addMessage(subSection, person, chatMessage);
            }
        }
    }

    @Override
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {
        List<Chat> ownChats = chatService.findChatsByParticipant(person);
        if (!CollectionUtils.isEmpty(ownChats)) {
            for(Chat chat : ownChats) {
                //chat subjects and topics should not be erased, otherwise the chats can not be found anymore
                chatService.unsetChatReadTime(chat, person);
                chatService.unsetChatReceivedTime(chat, person);
                List<ChatMessage> chatMessages = chatService.getAllMessagesByChat(chat);
                for (ChatMessage chatMessage: chatMessages) {
                    if (chatMessage.getMediaItem() != null) {
                        chatMessage.setMediaItem(null);
                        personDeletionReport.addEntry(chatMessage, DeleteOperation.DELETED);
                    }
                }
                personDeletionReport.addEntry(chat, DeleteOperation.ERASED);
            }
        }
    }

    private String getParticipants (Set<ChatParticipant> participants) {
        return participants.stream()
                .map(ChatParticipant::getPerson)
                .map(Person::getFirstNameFirstCharLastDot)
                .collect(Collectors.joining(", "));
    }

    private void addMessage(IDataPrivacyReport.IDataPrivacyReportSubSection subSection, Person chatOwner, ChatMessage chatMessage) {
        String senderName;
        Person sender = chatMessage.getSender();
        if (sender == null) {
            senderName = "System";
        } else if (chatOwner.equals(sender)) {
            senderName = "Du";
        } else {
            senderName = sender.getFirstNameFirstCharLastDot();
        }

        final String prefix = getChatTime(chatMessage.getSentTime()) + " " + senderName;
        if (StringUtils.isNotEmpty(chatMessage.getMessage())) {
            subSection.newContent(prefix, chatMessage.getMessage());
        } else if (chatMessage.getMediaItem() != null) {
            subSection.newImageContent(prefix, chatMessage.getMediaItem());

        } else if (StringUtils.isNotEmpty(chatMessage.getReferencedEntityId())) {
            //this is not really human-readable, but helps to understand roughly what was going on
            subSection.newContent(prefix, "[Referenz zu: {"+chatMessage.getReferencedEntityId()+"}]");
        }
    }

    private String getChatTime(long sentTime) {
        return "[" + timeService.toLocalTimeHumanReadable(sentTime) + "]";
    }

}
