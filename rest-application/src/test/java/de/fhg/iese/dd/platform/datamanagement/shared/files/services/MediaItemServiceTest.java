/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import com.twelvemonkeys.imageio.plugins.jpeg.JPEGImageReader;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.DefaultMediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import okio.Buffer;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.withinPercentage;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles({"test", "MediaItemServiceTest"})
public class MediaItemServiceTest extends BaseServiceTest {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private DefaultMediaItemRepository defaultMediaItemRepository;
    @Autowired
    private TestFileStorage fileStorage;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private MediaConfig mediaConfig;
    private MockWebServer mockExternalSystem;
    private String mockExternalSystemBasePath;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        mockExternalSystem = new MockWebServer();
        mockExternalSystem.start();
        mockExternalSystemBasePath = "http://" + mockExternalSystem.getHostName() + ":" + mockExternalSystem.getPort();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
        mockExternalSystem.shutdown();
    }

    @Test
    public void createMediaItemWithAllSizes() throws IOException {
        byte[] imageData = th.loadTestResource("testImageBigger1MB.jpg");
        MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());
    }

    @Test
    public void createMediaItemFromDefault() {
        String identifier = "myimages/beautiful_landscape.png";
        MediaItem mediaItem = mediaItemService.createMediaItemFromDefault(identifier, FileOwnership.UNKNOWN);
        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        DefaultMediaItem defaultMediaItem = defaultMediaItemRepository.findByIdentifier(identifier);
        assertThat(defaultMediaItem).isNotNull();
        assertSizesAreCreated(defaultMediaItem.getMediaItem(), mediaItemService.getDefinedSizesCurrentSizeVersion());

        mediaItemService.createMediaItemFromDefault(identifier, FileOwnership.UNKNOWN);
        //there should not be a new one created
        DefaultMediaItem defaultMediaItem2 = defaultMediaItemRepository.findByIdentifier(identifier);
        assertThat(defaultMediaItem2).isEqualTo(defaultMediaItem);
    }

    @Test
    @Transactional
    public void createMediaItemFromDefault_DifferentFileOwnerships() {
        final Person owner = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;
        final App app = th.app2;

        MediaItem mediaItem = mediaItemService.createMediaItemFromDefault("default1", FileOwnership.UNKNOWN);
        assertThat(mediaItem.getOwner()).isNull();
        assertThat(mediaItem.getAppVariant()).isNull();
        assertThat(mediaItem.getApp()).isNull();

        mediaItem = mediaItemService.createMediaItemFromDefault("default2", FileOwnership.of(owner));
        assertThat(mediaItem.getOwner()).isEqualTo(owner);
        assertThat(mediaItem.getAppVariant()).isNull();
        assertThat(mediaItem.getApp()).isNull();

        mediaItem = mediaItemService.createMediaItemFromDefault("default3", FileOwnership.of(owner, appVariant));
        assertThat(mediaItem.getOwner()).isEqualTo(owner);
        assertThat(mediaItem.getAppVariant()).isEqualTo(appVariant);
        assertThat(mediaItem.getApp()).isEqualTo(appVariant.getApp());

        mediaItem = mediaItemService.createMediaItemFromDefault("default4", FileOwnership.of(owner, app));
        assertThat(mediaItem.getOwner()).isEqualTo(owner);
        assertThat(mediaItem.getAppVariant()).isNull();
        assertThat(mediaItem.getApp()).isEqualTo(app);

        mediaItem = mediaItemService.createMediaItemFromDefault("default5", FileOwnership.of(appVariant));
        assertThat(mediaItem.getOwner()).isNull();
        assertThat(mediaItem.getAppVariant()).isEqualTo(appVariant);
        assertThat(mediaItem.getApp()).isEqualTo(appVariant.getApp());

        mediaItem = mediaItemService.createMediaItemFromDefault("default6", FileOwnership.of(app));
        assertThat(mediaItem.getOwner()).isNull();
        assertThat(mediaItem.getAppVariant()).isNull();
        assertThat(mediaItem.getApp()).isEqualTo(app);
    }

    @Test
    public void createMediaItemFromURL() throws Exception {

        String path = "/myImage.jpg";
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), path);

        MediaItem mediaItem =
                mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);

        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(2);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("GET");
    }

    @Test
    public void createMediaItemFromURL_UnencodedUrl() throws Exception {

        String pathQueried = "/myImöge.jpg?p=1&b=2&ö=ü";
        //fragment is only client side and not send, umlaute in the query do not get escaped
        String pathUnescaped = "/myImöge.jpg?p=1&b=2&ö=ü#abc";
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), pathQueried);

        MediaItem mediaItem = mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + pathUnescaped),
                FileOwnership.UNKNOWN);

        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(2);
        RecordedRequest headRequest = mockExternalSystem.takeRequest();
        RecordedRequest getRequest = mockExternalSystem.takeRequest();
        assertThat(headRequest.getMethod()).isEqualTo("HEAD");
        assertThat(headRequest.getPath()).isEqualTo(pathQueried);
        assertThat(getRequest.getMethod()).isEqualTo("GET");
        assertThat(getRequest.getPath()).isEqualTo(pathQueried);
    }

    @Test
    public void createMediaItemFromURL_EncodedUrl() throws Exception {

        String pathQueried = "/my%20Image.jpg";
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), pathQueried);

        MediaItem mediaItem = mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + pathQueried),
                FileOwnership.UNKNOWN);

        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(2);
        RecordedRequest headRequest = mockExternalSystem.takeRequest();
        RecordedRequest getRequest = mockExternalSystem.takeRequest();
        assertThat(headRequest.getMethod()).isEqualTo("HEAD");
        assertThat(headRequest.getPath()).isEqualTo(pathQueried);
        assertThat(getRequest.getMethod()).isEqualTo("GET");
        assertThat(getRequest.getPath()).isEqualTo(pathQueried);
    }

    @Test
    public void createMediaItemFromURL_BasicAuth() throws Exception {

        String path = "/myImage.jpg";
        String basicAuthCredentials = "user:passwörd";
        String basicAuthHeader = "Basic dXNlcjpwYXNzd/ZyZA==";
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), path);

        MediaItem mediaItem = mediaItemService.createMediaItem(
                new URL("http://" + basicAuthCredentials + "@" + mockExternalSystem.getHostName() + ":" +
                        mockExternalSystem.getPort() + path), FileOwnership.UNKNOWN);

        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(2);
        RecordedRequest headRequest = mockExternalSystem.takeRequest();
        RecordedRequest getRequest = mockExternalSystem.takeRequest();
        assertThat(headRequest.getMethod()).isEqualTo("HEAD");
        assertThat(headRequest.getHeader("Authorization")).isEqualTo(basicAuthHeader);
        assertThat(getRequest.getMethod()).isEqualTo("GET");
        assertThat(headRequest.getHeader("Authorization")).isEqualTo(basicAuthHeader);
    }

    @Test
    public void createMediaItemFromURL_Redirect() throws Exception {

        String pathOldLocation = "/myImageOldLocation.jpg";
        String pathNewLocation = "/myImageNewLocation.jpg";
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), pathNewLocation, MediaType.IMAGE_JPEG_VALUE,
                r -> {
                    if (StringUtils.equals(r.getPath(), pathOldLocation)) {
                        return Optional.of(new MockResponse()
                                .setResponseCode(302)
                                .setHeader("location", pathNewLocation));
                    }
                    return Optional.empty();
                });

        MediaItem mediaItem =
                mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + pathOldLocation),
                        FileOwnership.UNKNOWN);

        assertSizesAreCreated(mediaItem, mediaItemService.getDefinedSizesCurrentSizeVersion());

        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(4);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("GET");
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("GET");
    }

    @Test
    public void createMediaItemFromURL_InvalidScheme() throws Exception {

        try {
            mediaItemService.createMediaItem(new URL("ftp://brain.my/thoughts.jpg"), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("ftp");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(0);
    }

    @Test
    public void createMediaItemFromURL_NotFound() throws Exception {

        setupMockResponse(new byte[]{}, "/PiepPiep.jpg");
        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + "/MiepMiep.png"),
                    FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("Not Found");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(1);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
    }

    @Test
    public void createMediaItemFromURL_ContentEmpty() throws Exception {

        String path = "/myImage_NoContent.jpg";
        setupMockResponse(new byte[]{}, path);

        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("smaller than the allowed minimum file size");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(1);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
    }

    @Test
    public void createMediaItemFromURL_ContentTooSmall() throws Exception {

        String path = "/myImage_TooSmallContent.jpg";
        setupMockResponse(new byte[]{0, 1, 2}, path);

        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("smaller than the allowed minimum file size");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(1);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
    }

    @Test
    public void createMediaItemFromURL_ContentTooBig() throws Exception {

        String path = "/myImage_InvalidSize.jpg";
        setupMockResponse(th.loadTestResource("testImageTooBig.jpg"), path);

        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            log.debug(e.getMessage());
            assertThat(e.getMessage()).contains("size");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(1);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
    }

    @Test
    public void createMediaItemFromURL_InvalidMediaTypeContent() throws Exception {

        String path = "/myImage_InvalidMimeType.jpg";
        setupMockResponse(th.loadTestResource("google_maps_directions_example_backend.json"), path);

        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("media type");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(2);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("GET");
    }

    @Test
    public void createMediaItemFromURL_InvalidMediaTypeHead() throws Exception {

        String path = "/myImage_InvalidMimeType.jpg";
        setupMockResponse(th.loadTestResource("google_maps_directions_example_backend.json"), path,
                MediaType.APPLICATION_JSON_VALUE, null);

        try {
            mediaItemService.createMediaItem(new URL(mockExternalSystemBasePath + path), FileOwnership.UNKNOWN);
            fail("Exception not thrown");
        } catch (FileDownloadException e) {
            assertThat(e.getMessage()).contains("media type");
        }
        assertThat(mockExternalSystem.getRequestCount()).isEqualTo(1);
        assertThat(mockExternalSystem.takeRequest().getMethod()).isEqualTo("HEAD");
    }

    @Test
    public void createMediaItem4Sizes() throws IOException {

        byte[] imageData = th.loadTestResource("thing1.jpg");
        MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);

        List<MediaItemSize> expectedSizes =
                new ArrayList<>(mediaItemService.getDefinedSizesCurrentSizeVersion().subList(0, 3));
        expectedSizes.add(MediaItemSize.ORIGINAL_SIZE);

        assertSizesAreCreated(mediaItem, expectedSizes);
    }

    @Test
    public void createMediaItem2Sizes() throws IOException {
        byte[] imageData = th.loadTestResource("creditcard.jpg");
        MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);

        List<MediaItemSize> expectedSizes =
                new ArrayList<>(mediaItemService.getDefinedSizesCurrentSizeVersion().subList(0, 1));
        expectedSizes.add(MediaItemSize.ORIGINAL_SIZE);

        assertSizesAreCreated(mediaItem, expectedSizes);
    }

    @Test
    public void createMediaItemWithCrop_NoCrop() throws IOException {
        String path = "/myImage.jpg";
        // image size: width=4096, height=2731
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), path);

        // absolute crop
        CroppedImageReference absoluteCroppedImageReference = CroppedImageReference.builder()
                .url(new URL(mockExternalSystemBasePath + path))
                .imageCropDefinition(AbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0)
                        .absoluteOffsetY(0)
                        .absoluteWidth(4096)
                        .absoluteHeight(2731)
                        .build())
                .build();
        MediaItem mediaItemAbsoluteCropped =
                mediaItemService.createMediaItem(absoluteCroppedImageReference, FileOwnership.UNKNOWN);
        assertThat(mediaItemAbsoluteCropped.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItemAbsoluteCropped.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1d);
        assertThat(mediaItemAbsoluteCropped.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1);
        assertThat(mediaItemAbsoluteCropped.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0d);
        assertThat(mediaItemAbsoluteCropped.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0d);
        assertSizesAreCreated(mediaItemAbsoluteCropped, mediaItemService.getDefinedSizesCurrentSizeVersion());

        // relative crop
        CroppedImageReference relativeCroppedImageReference = CroppedImageReference.builder()
                .url(new URL(mockExternalSystemBasePath + path))
                .imageCropDefinition(RelativeImageCropDefinition.builder()
                        .relativeOffsetX(0d)
                        .relativeOffsetY(0d)
                        .relativeWidth(1d)
                        .relativeHeight(1d)
                        .build())
                .build();
        MediaItem mediaItemRelativeCropped =
                mediaItemService.createMediaItem(relativeCroppedImageReference, FileOwnership.UNKNOWN);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1d);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0d);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0d);
        assertSizesAreCreated(mediaItemRelativeCropped, mediaItemService.getDefinedSizesCurrentSizeVersion());
    }

    @Test
    public void createMediaItemWithCrop_Tolerance() throws IOException {

        String path = "/myImage.jpg";
        // image size: width=4096, height=2731
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), path);

        // the error should be tolerated and removed
        final CroppedImageReference relativeCroppedImageReference = CroppedImageReference.builder()
                .url(new URL(mockExternalSystemBasePath + path))
                .imageCropDefinition(RelativeImageCropDefinition.builder()
                        .relativeOffsetX(0.5d)
                        .relativeOffsetY(0.5d)
                        .relativeWidth(0.5d + MediaItemService.MAX_CROP_SUM_ERROR)
                        .relativeHeight(0.5d + MediaItemService.MAX_CROP_SUM_ERROR)
                        .build())
                .build();
        MediaItem mediaItemRelativeCropped =
                mediaItemService.createMediaItem(relativeCroppedImageReference, FileOwnership.UNKNOWN);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5d);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5d);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.5d);
        assertThat(mediaItemRelativeCropped.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.5d);
        assertSizesAreCreated(mediaItemRelativeCropped, mediaItemService.getDefinedSizesCurrentSizeVersion());
    }

    @Test
    public void createMediaItemWithCrop_AllDimensionsCropped() throws IOException {
        String path = "/myImage.jpg";
        // image size: width=4096, height=2731
        setupMockResponse(th.loadTestResource("testImageBigger1MB.jpg"), path);

        // absolute crop
        final CroppedImageReference absoluteCroppedImageReference = CroppedImageReference.builder()
                .url(new URL(mockExternalSystemBasePath + path))
                .imageCropDefinition(AbsoluteImageCropDefinition.builder()
                        // x and y offset are quarter of original width and height
                        .absoluteOffsetX(1024)
                        .absoluteOffsetY(682)
                        // width and height are half size of original width and height
                        .absoluteWidth(2048)
                        .absoluteHeight(1365)
                        .build())
                .build();
        MediaItem mediaItemAbsoluteCropped =
                mediaItemService.createMediaItem(absoluteCroppedImageReference, FileOwnership.UNKNOWN);
        RelativeImageCropDefinition cropDefinition = mediaItemAbsoluteCropped.getRelativeImageCropDefinition();
        assertThat(cropDefinition).isNotNull();
        assertThat(cropDefinition.getRelativeHeight()).isCloseTo(0.499816916, withinPercentage(1));
        assertThat(cropDefinition.getRelativeWidth()).isEqualTo(0.5);
        assertThat(cropDefinition.getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(cropDefinition.getRelativeOffsetY()).isCloseTo(0.2497, withinPercentage(1));
        assertSizesAreCreated(mediaItemAbsoluteCropped, mediaItemService.getDefinedSizesCurrentSizeVersion());

        // relative crop
        final CroppedImageReference relativeCroppedImageReference = CroppedImageReference.builder()
                .url(new URL(mockExternalSystemBasePath + path))
                .imageCropDefinition(RelativeImageCropDefinition.builder()
                        // x and y offset are quarter of original width and height
                        .relativeOffsetX(0.25)
                        .relativeOffsetY(0.25)
                        // width and height are half size of original width and height
                        .relativeWidth(0.5)
                        .relativeHeight(0.5)
                        .build())
                .build();
        MediaItem mediaItemRelativeCropped =
                mediaItemService.createMediaItem(relativeCroppedImageReference, FileOwnership.UNKNOWN);
        cropDefinition = mediaItemRelativeCropped.getRelativeImageCropDefinition();
        assertThat(cropDefinition).isNotNull();
        assertThat(cropDefinition.getRelativeHeight()).isEqualTo(0.5);
        assertThat(cropDefinition.getRelativeWidth()).isEqualTo(0.5);
        assertThat(cropDefinition.getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(cropDefinition.getRelativeOffsetY()).isEqualTo(0.25);
        assertSizesAreCreated(mediaItemRelativeCropped, mediaItemService.getDefinedSizesCurrentSizeVersion());
    }

    @Test
    public void getCurrentSizeVersion() {
        int expectedCurrentVersion = mediaConfig.getImageSizeVersions().keySet()
                .stream()
                .max(Integer::compareTo).orElseThrow(AssertionError::new);
        //check if the configuration changed
        assertEquals(expectedCurrentVersion, 2);

        assertEquals(2, mediaItemService.getCurrentSizeVersion());
    }

    @Test
    public void getDefinedSizesCurrentSizeVersion() {

        List<MediaConfig.ImageSizeConfig> expectedSizeConfigs = mediaConfig.getImageSizeVersions().get(2);
        assertEquals(6, expectedSizeConfigs.size());

        List<MediaItemSize> actualSizes = mediaItemService.getDefinedSizesCurrentSizeVersion();
        assertEquals(7, actualSizes.size());

        Map<String, MediaItemSize> nameToActualSize = actualSizes.stream()
                .collect(Collectors.toMap(MediaItemSize::getName, Function.identity()));

        for (MediaConfig.ImageSizeConfig imageSizeConfig : expectedSizeConfigs) {
            MediaItemSize actualSize = nameToActualSize.get(imageSizeConfig.getName());
            assertNotNull(actualSize);
            assertEquals(imageSizeConfig.getMaxSize(), actualSize.getMaxSize());
            if (StringUtils.isNotEmpty(imageSizeConfig.getFileSuffix())) {
                assertEquals(imageSizeConfig.getFileSuffix(), actualSize.getFileSuffix());
            } else {
                assertEquals(imageSizeConfig.getName(), actualSize.getFileSuffix());
            }
            assertEquals(imageSizeConfig.isSquared(), actualSize.isSquared());
        }

        MediaItemSize originalSize = nameToActualSize.get("original");
        assertEquals(Integer.MAX_VALUE, originalSize.getMaxSize());
        assertNull(originalSize.getFileSuffix());
        assertFalse(originalSize.isSquared());
    }

    @Test
    public void getDefinedSizes() {
        //invalid size number
        assertNull(mediaItemService.getDefinedSizes(3));

        //legacy size version
        assertEquals(2, mediaItemService.getDefinedSizes(0).size());

        //current size version
        assertEquals(mediaItemService.getDefinedSizesCurrentSizeVersion(), mediaItemService.getDefinedSizes(2));
    }

    @Test
    public void deleteMediaItem() throws Exception {

        byte[] imageData = th.loadTestResource("thing1.jpg");
        MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);

        mediaItemService.deleteItem(mediaItem);

        waitForEventProcessing();

        Map<String, String> actualUrls = mediaItem.getUrls();

        for (String url : actualUrls.values()) {
            String internalFileName = fileStorage.getInternalFileName(url);
            assertThat(fileStorage.fileExists(internalFileName))
                    .as("File " + internalFileName + " should be deleted")
                    .isFalse();
        }
    }

    @Test
    @Transactional
    public void copyMediaItem() throws Exception {

        byte[] imageData = th.loadTestResource("thing1.jpg");
        MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.of(th.personRegularAnna,
                th.app1Variant1));

        FileOwnership newOwnership = FileOwnership.of(th.app2);
        MediaItem clonedItem = mediaItemService.cloneItem(mediaItem, newOwnership);

        assertThat(clonedItem.getId()).isNotEqualTo(mediaItem.getId());
        assertThat(clonedItem.getApp()).isEqualTo(newOwnership.getApp());
        assertThat(clonedItem.getAppVariant()).isEqualTo(newOwnership.getAppVariant());
        assertThat(clonedItem.getOwner()).isEqualTo(newOwnership.getOwner());
        assertThat(clonedItem.getSizeVersion()).isEqualTo(mediaItem.getSizeVersion());
        assertThat(clonedItem.getUrlsJson()).isNotEqualTo(mediaItem.getUrlsJson());

        Map<String, String> clonedUrls = clonedItem.getUrls();

        for (String url : clonedUrls.values()) {
            String internalFileName = fileStorage.getInternalFileName(url);
            assertTrue(fileStorage.fileExists(internalFileName));
        }
    }

    @Test
    public void loadJpgWithCmykColorProfile() throws Exception {

        //check that TwelveMonkey ImageIO extension plugin has been loaded
        assertTrue(iteratorToStream(ImageIO.getImageReadersByFormatName("JPEG"))
                        .anyMatch(imageReader -> imageReader instanceof JPEGImageReader),
                "com.twelvemonkeys.imageio.plugins.jpeg.JPEGImageReader should be registered");

        //creating CMYK JPG image should work without errors
        final byte[] imageData = th.loadTestResource("cmykColorProfile.jpg");
        mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
    }

    private void setupMockResponse(byte[] imageBytes, String path) {
        setupMockResponse(imageBytes, path, MediaType.IMAGE_JPEG_VALUE, null);
    }

    private void setupMockResponse(byte[] bodyBytes, String path, String mediaType,
            @Nullable Function<RecordedRequest, Optional<MockResponse>> additionalResponse) {

        mockExternalSystem.setDispatcher(new Dispatcher() {
            @SuppressWarnings("ConstantConditions")
            @NonNull
            @Override
            public MockResponse dispatch(@NonNull RecordedRequest recordedRequest) {
                log.debug("Request: {}", recordedRequest.toString());
                if (StringUtils.equals(recordedRequest.getPath(), path)) {
                    switch (recordedRequest.getMethod()) {
                        case "GET":
                            //noinspection resource
                            return new MockResponse()
                                    .setResponseCode(200)
                                    .setHeader(HttpHeaders.CONTENT_TYPE, mediaType)
                                    .setHeader(HttpHeaders.CONTENT_LENGTH, bodyBytes.length)
                                    .setBody(new Buffer().write(bodyBytes));
                        case "HEAD":
                            return new MockResponse()
                                    .setResponseCode(200)
                                    .setHeader(HttpHeaders.CONTENT_TYPE, mediaType)
                                    .setHeader(HttpHeaders.CONTENT_LENGTH, bodyBytes.length);
                    }
                }
                if (additionalResponse != null) {
                    Optional<MockResponse> additionalResponseOpt = additionalResponse.apply(recordedRequest);
                    if (additionalResponseOpt.isPresent()) {
                        return additionalResponseOpt.get();
                    }
                }

                log.debug("Not found: '{}' != '{}'", recordedRequest.getPath(), path);
                return new MockResponse().setResponseCode(404);
            }
        });
    }

    private void assertSizesAreCreated(MediaItem mediaItem, List<MediaItemSize> expectedSizes) {

        assertNotNull(mediaItem);

        Map<String, String> actualUrls = mediaItem.getUrls();

        final List<String> expectedSizesNames = expectedSizes.stream().
                map(MediaItemSize::getName)
                .collect(Collectors.toList());

        assertThat(actualUrls.keySet()).containsAll(expectedSizesNames);

        assertThat(actualUrls).hasSameSizeAs(expectedSizesNames);

        int previousActualSize = 0;
        for (MediaItemSize expectedSize : expectedSizes) {
            String url = actualUrls.get(expectedSize.getName());
            String internalFileName = fileStorage.getInternalFileName(url);
            assertTrue(fileStorage.fileExists(internalFileName));
            int actualSize = fileStorage.fileSize(internalFileName);
            assertThat(actualSize)
                    .as("File sizes do not decrease in order of the media item size")
                    .isGreaterThan(previousActualSize);
        }
    }

    //Q: http://www.lambdafaq.org/how-do-i-turn-an-iterator-into-a-stream/
    private static <T> Stream<T> iteratorToStream(Iterator<T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
    }

}
