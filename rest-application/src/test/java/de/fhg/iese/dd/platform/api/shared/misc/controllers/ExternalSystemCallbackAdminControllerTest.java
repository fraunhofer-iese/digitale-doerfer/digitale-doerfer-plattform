/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.callback.providers.IWebClientProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import okhttp3.mockwebserver.*;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.lang.NonNull;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ExternalSystemCallbackAdminControllerTest extends BaseServiceTest {

    private static final String HEADER_NAME_API_KEY = "apiKey";
    private static final String BASE_URL = "http://localhost:8080";
    private static final String BASE_URL_IP = "http://127.0.0.1:8080";
    private static final String JSON_PAYLOAD = "{ id: 123, content: 'It is a test' }";

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @MockBean
    private IWebClientProvider webClientProvider;

    private static MockWebServer mockExternalSystem;
    private static WebClient webClient;

    @BeforeAll
    public static void startMockServer() throws IOException {
        mockExternalSystem = new MockWebServer();
        mockExternalSystem.start(8080);
        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 300)
                .doOnConnected(connection -> {
                    ReadTimeoutHandler readTimeoutHandler = new ReadTimeoutHandler(300, TimeUnit.MILLISECONDS);
                    connection.addHandlerLast(readTimeoutHandler);
                    connection.addHandlerLast(new WriteTimeoutHandler(300, TimeUnit.MILLISECONDS));
                });
        webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    @AfterAll
    public static void shutdownMockServer() throws Exception {
        mockExternalSystem.shutdown();
    }

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createLegalEntities();
        th.createUserDataEntities(th.personRegularAnna);
        //we always need to return the same web client, since it is cached after getting it from the provider
        Mockito.when(webClientProvider.createWebClient()).thenReturn(webClient);
        mockExternalSystem.setDispatcher(new QueueDispatcher());
    }

    @Override
    public void tearDown() {

        th.deleteAllData();
    }

    @Test
    public void callExternalSystem() throws Exception {

        // set baseUrl of app variant to url of MockServer (http://localhost:8080/)
        th.app1Variant1.setCallBackUrl(mockExternalSystem.url("/").toString());
        th.app1Variant1.setExternalApiKey("super-secret-variant1");
        th.app1Variant1 = appVariantRepository.saveAndFlush(th.app1Variant1);
        // set baseUrl of app variant to url of MockServer (http://localhost:8080/)
        th.app1Variant2.setCallBackUrl(mockExternalSystem.url("/") + "/");
        th.app1Variant2.setExternalApiKey("super-secret-variant2");
        th.app1Variant2 = appVariantRepository.saveAndFlush(th.app1Variant2);
        // set baseUrl of app variant to url of MockServer (http://localhost:8080/)
        th.app2Variant1.setCallBackUrl(mockExternalSystem.url("/") + "/");
        th.app2Variant1.setExternalApiKey(null);
        th.app2Variant1 = appVariantRepository.saveAndFlush(th.app2Variant1);
        // set baseUrl of app variant to url of MockServer (http://localhost:8080/)
        th.app2Variant2.setCallBackUrl(mockExternalSystem.url("/") + "/");
        th.app2Variant2.setExternalApiKey(null);
        th.app2Variant2.setExternalBasicAuth("bob:marley");
        th.app2Variant2 = appVariantRepository.saveAndFlush(th.app2Variant2);

        final String normalizedUrlSuffix = "/test";
        final String urlSuffix = "test";
        final String responseBodyGet = "Test successful with GET";
        final String responseBodyPost = "Test successful with POST";
        final String responseBodyPut = "Test successful with PUT";
        final String responseBodyDelete = "{ \"id\": \"007\", \"message\": \"Test successful with DELETE\"}";

        mockExternalSystem.setDispatcher(new Dispatcher() {
            @SuppressWarnings("ConstantConditions")
            @NonNull
            @Override
            public MockResponse dispatch(@NonNull RecordedRequest recordedRequest) {
                if (recordedRequest.getPath().equals(normalizedUrlSuffix)) {
                    switch (recordedRequest.getMethod()) {
                        case "GET":
                            return new MockResponse()
                                    .setResponseCode(200)
                                    .setBody(responseBodyGet);
                        case "POST":
                            return new MockResponse()
                                    .setResponseCode(200)
                                    .setBody(responseBodyPost);
                        case "PUT":
                            return new MockResponse()
                                    .setResponseCode(201)
                                    .setBody(responseBodyPut);
                        case "DELETE":
                            return new MockResponse()
                                    .setResponseCode(200)
                                    .setHeader("Content-Type", contentType)
                                    .setBody(responseBodyDelete);
                    }
                }
                return new MockResponse().setResponseCode(404);
            }
        });
        // HttpMethod.GET
        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                        .param("urlSuffix", normalizedUrlSuffix))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.GET.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry(HEADER_NAME_API_KEY,
                                Collections.singletonList(maskValue(th.app1Variant1.getExternalApiKey())))))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.OK.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyGet))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
        assertThat(mockExternalSystem.takeRequest().getHeader(HEADER_NAME_API_KEY))
                .isEqualTo(th.app1Variant1.getExternalApiKey());

        // HttpMethod.GET without api key
        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app2Variant1.getAppVariantIdentifier())
                        .param("urlSuffix", normalizedUrlSuffix))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.GET.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders", Matchers.anEmptyMap()))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.OK.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyGet))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
        assertThat(mockExternalSystem.takeRequest().getHeader(HEADER_NAME_API_KEY)).isNull();

        // HttpMethod.GET with basic auth
        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app2Variant2.getAppVariantIdentifier())
                        .param("urlSuffix", normalizedUrlSuffix))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.GET.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry("Authorization", Collections.singletonList(
                                maskValue("Basic " + th.app2Variant2.getExternalBasicAuth())))))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.OK.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyGet))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
        String expectedAuthHeader =
                "Basic " + Base64.getEncoder()
                        .encodeToString(th.app2Variant2.getExternalBasicAuth().getBytes(StandardCharsets.UTF_8));
        assertThat(mockExternalSystem.takeRequest().getHeader("Authorization")).isEqualTo(expectedAuthHeader);

        // HttpMethod.POST
        mockMvc.perform(post("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant2.getAppVariantIdentifier())
                        .param("urlSuffix", urlSuffix)
                        .contentType(contentType)
                        .content(JSON_PAYLOAD))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.POST.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry(HEADER_NAME_API_KEY,
                                Collections.singletonList(maskValue(th.app1Variant2.getExternalApiKey())))))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.OK.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyPost))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
        assertThat(mockExternalSystem.takeRequest().getHeader(HEADER_NAME_API_KEY))
                .isEqualTo(th.app1Variant2.getExternalApiKey());

        // HttpMethod.PUT
        mockMvc.perform(put("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant2.getAppVariantIdentifier())
                        .param("urlSuffix", urlSuffix)
                        .contentType(contentType)
                        .content(JSON_PAYLOAD))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.PUT.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry(HEADER_NAME_API_KEY,
                                Collections.singletonList(maskValue(th.app1Variant2.getExternalApiKey())))))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.CREATED.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyPut))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
        assertThat(mockExternalSystem.takeRequest().getHeader(HEADER_NAME_API_KEY))
                .isEqualTo(th.app1Variant2.getExternalApiKey());

        // HttpMethod.DELETE
        mockMvc.perform(delete("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant2.getAppVariantIdentifier())
                        .param("urlSuffix", urlSuffix)
                        .contentType(contentType)
                        .content(JSON_PAYLOAD))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.DELETE.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + normalizedUrlSuffix, BASE_URL_IP + normalizedUrlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry(HEADER_NAME_API_KEY,
                                Collections.singletonList(maskValue(th.app1Variant2.getExternalApiKey())))))
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.OK.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(responseBodyDelete))
                .andExpect(jsonPath("$.responseBodyJson.id").value("007"))
                .andExpect(jsonPath("$.responseBodyJson.message").value("Test successful with DELETE"));
        assertThat(mockExternalSystem.takeRequest().getHeader(HEADER_NAME_API_KEY))
                .isEqualTo(th.app1Variant2.getExternalApiKey());
    }

    @Test
    public void callExternalSystem_Timeout() throws Exception {

        String callBackUrl = "http://localhost:9090";
        th.app1Variant1.setCallBackUrl(callBackUrl);
        th.app1Variant1 = appVariantRepository.saveAndFlush(th.app1Variant1);

        mockExternalSystem.setDispatcher(new Dispatcher() {
            @NonNull
            @Override
            public MockResponse dispatch(@NonNull RecordedRequest recordedRequest) throws InterruptedException {
                //the sleeping time is slightly higher than the timeout
                Thread.sleep(310);
                return new MockResponse().setResponseCode(404);
            }
        });

        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                        .param("urlSuffix", "blub")
                        .contentType(contentType)
                        .content(JSON_PAYLOAD))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.EXTERNAL_SYSTEM_CALL_FAILED, callBackUrl));

        waitForEventProcessingLong();
    }

    @Test
    public void callExternalSystem_HostNotFound() throws Exception {

        String callBackUrl = "http://localhost:9090";
        th.app1Variant1.setCallBackUrl(callBackUrl);
        th.app1Variant1 = appVariantRepository.saveAndFlush(th.app1Variant1);

        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", "blub")
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.EXTERNAL_SYSTEM_CALL_FAILED, callBackUrl));
    }

    @Test
    public void callExternalSystem_ErrorFromExternalSystem() throws Exception {

        th.app1Variant1.setCallBackUrl(mockExternalSystem.url("/").toString());
        th.app1Variant1 = appVariantRepository.saveAndFlush(th.app1Variant1);

        final String urlSuffix = "/test";

        final String errorMessage = "Server failed to process request";
        mockExternalSystem.enqueue(new MockResponse()
                .setResponseCode(500)
                .setBody(errorMessage));

        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                        .param("urlSuffix", urlSuffix))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.requestMethod").value(HttpMethod.GET.name()))
                .andExpect(jsonPath("$.requestURI",
                        Matchers.oneOf(BASE_URL + urlSuffix, BASE_URL_IP + urlSuffix)))
                .andExpect(jsonPath("$.requestHeaders",
                        Matchers.hasEntry(HEADER_NAME_API_KEY,
                                Collections.singletonList(maskValue(th.app1Variant1.getExternalApiKey())))))
                .andExpect(jsonPath("$.requestBody").doesNotExist())
                .andExpect(jsonPath("$.responseStatus").value(HttpStatus.INTERNAL_SERVER_ERROR.toString()))
                .andExpect(jsonPath("$.responseHeaders").isMap())
                .andExpect(jsonPath("$.responseBodyString").value(errorMessage))
                .andExpect(jsonPath("$.responseBodyJson").doesNotExist());
    }

    @Test
    public void callExternalSystem_AppVariantMissingCallbackUrl() throws Exception {

        final String urlSuffix = "/test";

        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(post("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void callExternalSystem_Unauthorized() throws Exception {

        final String urlSuffix = "/secret/suffix";

        assertOAuth2(get("/administration/external")
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix));

        assertOAuth2(post("/administration/external")
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD));

        assertOAuth2(delete("/administration/external")
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD));
    }

    @Test
    public void callExternalSystem_NoPermission() throws Exception {

        final String urlSuffix = "/secret/suffix";

        mockMvc.perform(get("/administration/external")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix))
                .andExpect(isNotAuthorized());

        mockMvc.perform(post("/administration/external")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(isNotAuthorized());

        mockMvc.perform(delete("/administration/external")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void callExternalSystem_AppVariantNotFound() throws Exception {

        final String invalidAppVariantIdentifier = BaseTestHelper.INVALID_UUID;
        final String urlSuffix = "/secret/suffix";

        mockMvc.perform(get("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", invalidAppVariantIdentifier)
                .param("urlSuffix", urlSuffix))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        mockMvc.perform(post("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", invalidAppVariantIdentifier)
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        mockMvc.perform(delete("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", invalidAppVariantIdentifier)
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(JSON_PAYLOAD))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void callExternalSystem_MissingOrEmptyPayload() throws Exception {

        final String urlSuffix = "/secret/suffix";

        // payload missing
        mockMvc.perform(post("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(delete("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        // payload empty
        mockMvc.perform(post("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(""))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(delete("/administration/external")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("appVariantIdentifierToCall", th.app1Variant1.getAppVariantIdentifier())
                .param("urlSuffix", urlSuffix)
                .contentType(contentType)
                .content(""))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    private static String maskValue(String s) {
        //the first 5 chars are kept, the rest is masked
        return StringUtils.truncate(s, 5) + StringUtils.repeat("*", StringUtils.length(s) - 5);
    }

}
