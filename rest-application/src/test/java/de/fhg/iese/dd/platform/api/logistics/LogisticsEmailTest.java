/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredPlacementRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientSignature;
import de.fhg.iese.dd.platform.business.logistics.LogisticsEmailTemplate;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.logistics.config.LogisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.logistics.feature.LogisticsShopFeature;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import freemarker.template.TemplateException;

public class LogisticsEmailTest extends BaseLogisticsEventTest {

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private LogisticsConfig logisticsConfig;

    @Autowired
    private IAuthorizationService authorizationService;

    @Autowired
    private IFeatureService featureService;

    private Delivery delivery;

    private TransportAssignment transportAssignment;

    private TransportConfirmation pickupConfirmation;
    private TransportConfirmation deliveredConfirmation;
    private TransportConfirmation receivedConfirmation;

    /**
     * Manually set to true to check the mails at %temp%/dd-tests/
     */
    private static final boolean EMAILS_TO_TEMP = false;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createLieferBarAppVariant();
        featureService.invalidateCache();
        th.createLieferBarShopFeatures();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception {

        String shopOrderId = purchaseOrderReceived();

        purchaseOrderReadyForTransport(shopOrderId);

        waitForEventProcessing();

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        delivery = deliveries.get(0);

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        transportAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        waitForEventProcessing();

        transportPickupRequest(carrier, // carrier
                transportAssignment.getId(), // transportAssignmentId
                deliveryAddress, // deliveryAddress of transportAssignment
                receiver, // receiver
                delivery.getId(), // deliveryId
                deliveryAddress, // deliveryAddress of delivery
                delivery.getTrackingCode()); // deliveryTrackingCode

        waitForEventProcessing();

        String signature = "signature";

        String placementDescription = "Ablageort";

        ClientTransportDeliveredPlacementRequest request1 = ClientTransportDeliveredPlacementRequest.builder()
                .transportAssignmentId(transportAssignment.getId()).deliveryTrackingCode(delivery.getTrackingCode())
                .placementDescription(placementDescription).build();

        mockMvc.perform(post("/logistics/event/transportDeliveredPlacementRequest").headers(authHeadersFor(carrier))
                .contentType(contentType).content(json(request1))).andExpect(status().isOk());

        waitForEventProcessing();

        ClientTransportDeliveredReceivedRequest request2 = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignment.getId()).deliveryTrackingCode(delivery.getTrackingCode())
                .signature(new ClientSignature(signature)).build();

        mockMvc.perform(post("/logistics/event/transportDeliveredReceivedRequest").headers(authHeadersFor(carrier))
                .contentType(contentType).content(json(request2))).andExpect(status().isOk());

        waitForEventProcessing();

        List<TransportConfirmation> actualConfirmations = th.transportConfirmationRepository.findAll();

        pickupConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentPickedUp(), transportAssignment)).findFirst().get();
        deliveredConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentDelivered(), transportAssignment)).findFirst()
                .get();
        receivedConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentReceived(), transportAssignment)).findFirst().get();
    }

    @Test
    public void generateMails() throws IOException, TemplateException {

        Tenant tenant = delivery.getTenant();

        String acknowledgeUrl = StringUtils.removeEnd(featureService.getFeature(LogisticsShopFeature.class,
                FeatureTarget.of(tenant, th.appLieferbar)).getShopUrl(), "/")
                + StringUtils.prependIfMissing(logisticsConfig.getDeliveryReceivedMailUrl(), "/") + "?deliveryId="
                + delivery.getId() + "&mailToken="
                + authorizationService.generateAuthToken(
                new DeliveryReceivedRequest(delivery, delivery.getReceiver().getPerson()), Duration.ofMinutes(5));

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("acknowledgeUrl", acknowledgeUrl);
        templateModel.put("senderShop", delivery.getSender().getShop());
        templateModel.put("senderPerson", delivery.getSender().getPerson());
        templateModel.put("receiverShop", delivery.getReceiver().getShop());
        templateModel.put("receiverPerson", delivery.getReceiver().getPerson());
        templateModel.put("delivery", delivery);
        templateModel.put("transportAssignment", transportAssignment);
        templateModel.put("pickupConfirmation", pickupConfirmation);
        templateModel.put("receivedConfirmation", receivedConfirmation);
        templateModel.put("deliveredConfirmation", deliveredConfirmation);
        templateModel.put("contactPerson", th.contactPersonTenant1);

        String mail = emailSenderService.createTextWithTemplate(LogisticsEmailTemplate.DELIVERY_DELIVERED_OTHER_PERSON,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryDeliveredOtherPerson");

        mail = emailSenderService.createTextWithTemplate(LogisticsEmailTemplate.DELIVERY_DELIVERED_PLACEMENT,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryDeliveredPlacement");

        mail = emailSenderService.createTextWithTemplate(
                LogisticsEmailTemplate.DELIVERY_DELIVERED_POOLING_STATION_RECEIVER,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryDeliveredPoolingStationReceiver");

        mail = emailSenderService.createTextWithTemplate(LogisticsEmailTemplate.DELIVERY_DELIVERED_RECEIVER,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryDeliveredReceiver");

        mail = emailSenderService.createTextWithTemplate(LogisticsEmailTemplate.DELIVERY_DELIVERED_UNKNOWN,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryDeliveredUnknown");

        mail = emailSenderService.createTextWithTemplate(LogisticsEmailTemplate.DELIVERY_RECEIVED_RECEIVER,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "deliveryReceivedReceiver");
    }

    private void writeToFile(String content, String fileName) throws IOException {

        if (EMAILS_TO_TEMP) {
            File dir = new File(System.getProperty("java.io.tmpdir") + "/dd-tests/");
            dir.mkdir();

            try (Writer writer = new OutputStreamWriter(new FileOutputStream(new File(dir, fileName + ".html")),
                    StandardCharsets.UTF_8)) {
                writer.write(content);
            }
        }
    }

}
