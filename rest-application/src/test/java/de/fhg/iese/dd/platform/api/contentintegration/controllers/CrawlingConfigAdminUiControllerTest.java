/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.contentintegration.ContentIntegrationTestHelper;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CrawlingConfigAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private ContentIntegrationTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createNewsSourcesAndCrawlingConfigs();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getCrawlingConfigs() throws Exception {

        final List<CrawlingConfig> expectedCrawlingConfigs = th.crawlingConfigRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(CrawlingConfig::getId))
                .toList();

        CrawlingConfig crawlingConfig0 = expectedCrawlingConfigs.get(0);
        CrawlingConfig crawlingConfig1 = expectedCrawlingConfigs.get(1);
        CrawlingConfig crawlingConfig2 = expectedCrawlingConfigs.get(2);
        mockMvc.perform(get("/adminui/crawlingconfig")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(7))
                .andExpect(jsonPath("$.content[0].id").value(crawlingConfig0.getId()))
                .andExpect(jsonPath("$.content[0].newsSourceId").value(crawlingConfig0.getNewsSource().getId()))
                .andExpect(jsonPath("$.content[0].sourceUrl").value(crawlingConfig0.getSourceUrl()))
                .andExpect(jsonPath("$.content[0].accessType").value(crawlingConfig0.getAccessType().name()))
                .andExpect(jsonPath("$.content[0].authenticationHeaderKey").value(
                        crawlingConfig0.getAuthenticationHeaderKey()))
                .andExpect(jsonPath("$.content[0].authenticationHeaderValue").value(
                        crawlingConfig0.getAuthenticationHeaderValue()))
                .andExpect(jsonPath("$.content[0].basicAuthUsername").value(crawlingConfig0.getBasicAuthUsername()))
                .andExpect(jsonPath("$.content[0].basicAuthPassword").value(crawlingConfig0.getBasicAuthPassword()))
                .andExpect(jsonPath("$.content[0].webFeedParametersMap").isNotEmpty())
                .andExpect(jsonPath("$.content[1].id").value(crawlingConfig1.getId()))
                .andExpect(jsonPath("$.content[1].newsSourceId").value(crawlingConfig1.getNewsSource().getId()))
                .andExpect(jsonPath("$.content[1].sourceUrl").value(crawlingConfig1.getSourceUrl()))
                .andExpect(jsonPath("$.content[1].accessType").value(crawlingConfig1.getAccessType().name()))
                .andExpect(jsonPath("$.content[1].authenticationHeaderKey").value(
                        crawlingConfig1.getAuthenticationHeaderKey()))
                .andExpect(jsonPath("$.content[1].authenticationHeaderValue").value(
                        crawlingConfig1.getAuthenticationHeaderValue()))
                .andExpect(jsonPath("$.content[1].basicAuthUsername").value(crawlingConfig1.getBasicAuthUsername()))
                .andExpect(jsonPath("$.content[1].basicAuthPassword").value(crawlingConfig1.getBasicAuthPassword()))
                .andExpect(jsonPath("$.content[1].webFeedParametersMap").isNotEmpty())
                .andExpect(jsonPath("$.content[2].id").value(crawlingConfig2.getId()))
                .andExpect(jsonPath("$.content[2].newsSourceId").value(crawlingConfig2.getNewsSource().getId()))
                .andExpect(jsonPath("$.content[2].sourceUrl").value(crawlingConfig2.getSourceUrl()))
                .andExpect(jsonPath("$.content[2].accessType").value(crawlingConfig2.getAccessType().name()))
                .andExpect(jsonPath("$.content[2].authenticationHeaderKey").value(
                        crawlingConfig2.getAuthenticationHeaderKey()))
                .andExpect(jsonPath("$.content[2].authenticationHeaderValue").value(
                        crawlingConfig2.getAuthenticationHeaderValue()))
                .andExpect(jsonPath("$.content[2].basicAuthUsername").value(crawlingConfig2.getBasicAuthUsername()))
                .andExpect(jsonPath("$.content[2].basicAuthPassword").value(crawlingConfig2.getBasicAuthPassword()))
                .andExpect(jsonPath("$.content[2].webFeedParametersMap").isNotEmpty());
    }

    @Test
    public void getCrawlingConfigs_PageParameterInvalid() throws Exception {

        // invalid parameter page
        mockMvc.perform(get("/adminui/crawlingconfig")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .param("page", String.valueOf(-1))
                .param("count", String.valueOf(1)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // invalid parameter count
        mockMvc.perform(get("/adminui/crawlingconfig")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .param("page", String.valueOf(0))
                .param("count", String.valueOf(0)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getCrawlingConfigs_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/crawlingconfig")
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/crawlingconfig"));
    }

    @Test
    public void getCrawlingConfigById() throws Exception {

        mockMvc.perform(
                        get("/adminui/crawlingconfig/{crawlingConfigId}",
                                th.crawlingConfigATOMNewsItemDorfallerlei.getId())
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.crawlingConfigATOMNewsItemDorfallerlei.getId()))
                .andExpect(jsonPath("$.newsSourceId").value(
                        th.crawlingConfigATOMNewsItemDorfallerlei.getNewsSource().getId()))
                .andExpect(jsonPath("$.sourceUrl").value(
                        th.crawlingConfigATOMNewsItemDorfallerlei.getSourceUrl()))
                .andExpect(jsonPath("$.accessType").value(
                        th.crawlingConfigATOMNewsItemDorfallerlei.getAccessType().name()))
                .andExpect(jsonPath("$.authenticationHeaderKey").isEmpty())
                .andExpect(jsonPath("$.authenticationHeaderValue").isEmpty())
                .andExpect(jsonPath("$.basicAuthUsername").value(
                        th.crawlingConfigATOMNewsItemDorfallerlei.getBasicAuthUsername()))
                .andExpect(jsonPath("$.basicAuthPassword").value(
                        th.crawlingConfigATOMNewsItemDorfallerlei.getBasicAuthPassword()))
                .andExpect(jsonPath("$.webFeedParametersMap").isNotEmpty());
    }

    @Test
    public void getCrawlingConfigById_CrawlingConfigNotFound() throws Exception {

        mockMvc.perform(get("/adminui/crawlingconfig/{crawlingConfigId}", BaseTestHelper.INVALID_UUID)
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(isException(ClientExceptionType.CRAWLING_CONFIG_NOT_FOUND));
    }

    @Test
    public void getCrawlingConfigById_Unauthorized() throws Exception {

        mockMvc.perform(
                        get("/adminui/crawlingconfig/{crawlingConfigId}",
                                th.crawlingConfigRSSNewsItemZukunftstheim.getId())
                                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(
                get("/adminui/crawlingconfig/{crawlingConfigId}",
                        th.crawlingConfigRSSNewsItemZukunftstheim.getId()));
    }

    @Test
    public void getCrawlingConfigsByNewsSource() throws Exception {

        final List<CrawlingConfig> expectedCrawlingConfigs = th.crawlingConfigRepository.findAll()
                .stream()
                .filter(cc -> cc.getNewsSource().equals(th.newsSourceZukunftstheim))
                .sorted(Comparator.comparing(CrawlingConfig::getId))
                .collect(Collectors.toList());

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}/crawlingconfig", th.newsSourceZukunftstheim.getId())
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.[0].id").value(expectedCrawlingConfigs.get(0).getId()))
                .andExpect(jsonPath("$.[0].newsSourceId").value(
                        expectedCrawlingConfigs.get(0).getNewsSource().getId()))
                .andExpect(jsonPath("$.[0].sourceUrl").value(expectedCrawlingConfigs.get(0).getSourceUrl()))
                .andExpect(jsonPath("$.[0].accessType").value(
                        expectedCrawlingConfigs.get(0).getAccessType().name()))
                .andExpect(jsonPath("$.[0].authenticationHeaderKey").value(
                        expectedCrawlingConfigs.get(0).getAuthenticationHeaderKey()))
                .andExpect(jsonPath("$.[0].authenticationHeaderValue").value(
                        expectedCrawlingConfigs.get(0).getAuthenticationHeaderValue()))
                .andExpect(jsonPath("$.[0].basicAuthUsername").value(
                        expectedCrawlingConfigs.get(0).getBasicAuthUsername()))
                .andExpect(jsonPath("$.[0].basicAuthPassword").value(
                        expectedCrawlingConfigs.get(0).getBasicAuthPassword()))
                .andExpect(jsonPath("$.[1].id").value(expectedCrawlingConfigs.get(1).getId()))
                .andExpect(jsonPath("$.[1].newsSourceId").value(
                        expectedCrawlingConfigs.get(1).getNewsSource().getId()))
                .andExpect(jsonPath("$.[1].sourceUrl").value(expectedCrawlingConfigs.get(1).getSourceUrl()))
                .andExpect(jsonPath("$.[1].accessType").value(
                        expectedCrawlingConfigs.get(1).getAccessType().name()))
                .andExpect(jsonPath("$.[1].authenticationHeaderKey").value(
                        expectedCrawlingConfigs.get(1).getAuthenticationHeaderKey()))
                .andExpect(jsonPath("$.[1].authenticationHeaderValue").value(
                        expectedCrawlingConfigs.get(1).getAuthenticationHeaderValue()))
                .andExpect(jsonPath("$.[1].basicAuthUsername").value(
                        expectedCrawlingConfigs.get(1).getBasicAuthUsername()))
                .andExpect(jsonPath("$.[1].basicAuthPassword").value(
                        expectedCrawlingConfigs.get(1).getBasicAuthPassword()))
                .andExpect(jsonPath("$.[1].webFeedParametersMap").isNotEmpty());
    }

    @Test
    public void getCrawlingConfigsByNewsSource_NewsSourceNotFound() throws Exception {

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}/crawlingconfig", BaseTestHelper.INVALID_UUID)
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));
    }

    @Test
    public void getCrawlingConfigsByNewsSource_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}/crawlingconfig", th.newsSourceZukunftstheim.getId())
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/newssource/{newsSourceId}/crawlingconfig", th.newsSourceZukunftstheim.getId()));
    }

}
