/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientSuggestionCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientSuggestionWithdrawRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
@ExtendWith(AssumeIntegrationTestExtension.class)
public class SuggestionEventControllerSearchTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;

    private AppVariant appVariantSuggestionCreationEnabled;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createSuggestionCategories();
        createFeatureSuggestionCreationEnabled();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    private void createFeatureSuggestionCreationEnabled() {
        appVariantSuggestionCreationEnabled = th.appVariant4AllTenants;
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantSuggestionCreationEnabled)
                .enabled(true)
                .featureClass(SuggestionCreationFeature.class.getName())
                .configValues(null)
                .build());
    }

    @Test
    public void suggestionCreateRequest_suggestionWithdrawRequest_Search() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertThat(postCreator.getHomeArea()).isNotNull();
        String text = "Ich kann meine Vorschläge nicht finden!";

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        waitForEventProcessingLong();

        final String postId = suggestionCreateConfirmation.getPostId();
        final SearchPost indexedPost = elasticsearchTestUtil.elasticsearchOperations.get(postId, SearchPost.class);
        assertThat(indexedPost).isNotNull();
        assertThat(indexedPost.getText()).isEqualTo(text);

        mockMvc.perform(get("/grapevine/search")
                        .param("search", "Vorschläge")
                        .param("page", "0")
                        .param("count", "1")
                        .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.SUGGESTION.toString()))
                .andExpect(jsonPath("$.content[0].suggestion.id").value(postId))
                .andExpect(jsonPath("$.content[0].suggestion.text").value(text));

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                        .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled))
                        .contentType(contentType)
                        .content(json(new ClientSuggestionWithdrawRequest(postId, "nö"))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        final SearchPost indexedPost2 = elasticsearchTestUtil.elasticsearchOperations.get(postId, SearchPost.class);
        assertThat(indexedPost2).isNull();

        mockMvc.perform(get("/grapevine/search")
                        .param("search", "Vorschläge")
                        .param("page", "0")
                        .param("count", "1")
                        .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

}
