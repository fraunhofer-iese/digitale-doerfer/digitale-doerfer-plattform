/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework.environment;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.framework.environment.services.TestLeaderService;

public class WorkerServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestLeaderService testLeaderService;
    @Autowired
    private TestWorkerTask testWorkerTask;
    @Autowired
    private List<IWorkerTask> workerTasks;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        workerTasks.remove(testWorkerTask);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        testLeaderService.stopLeader();
        testWorkerTask.resetCount();
    }

    @Test
    public void workersTriggerNotOverlapping() {

        MultiValueMap<String, IWorkerTask> workerByTrigger = new LinkedMultiValueMap<>();
        for (IWorkerTask workerTask : workerTasks) {
            final Trigger taskTrigger = workerTask.getTaskTrigger();
            if (taskTrigger instanceof CronTrigger) {
                CronTrigger cronTrigger = (CronTrigger) taskTrigger;
                workerByTrigger.add(cronTrigger.getExpression(), workerTask);
            }
            if (taskTrigger instanceof PeriodicTrigger) {
                PeriodicTrigger periodicTrigger = (PeriodicTrigger) taskTrigger;
                workerByTrigger.add("🔄 " + periodicTrigger.getPeriod() + " " + periodicTrigger.getTimeUnit(),
                        workerTask);
            }
        }

        final List<Map.Entry<String, List<IWorkerTask>>> sortedEntries = workerByTrigger.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toList());
        for (Map.Entry<String, List<IWorkerTask>> entry : sortedEntries) {
            log.debug(StringUtils.rightPad(entry.getKey(), 20) + " " +
                    entry.getValue().stream()
                            .map(w -> w.getName() + "(" + w.getClass().getSimpleName() + ")")
                            .collect(Collectors.joining(", ")));
            assertThat(entry.getValue()).as("At least two workers have the exact same trigger")
                    .hasSizeLessThanOrEqualTo(1);
        }
    }

    @Test
    public void workersStartStopOnLeaderStartStop() throws Exception {

        //check that test worker is not running
        assertThatTestWorkerTaskIsRunning(false);

        //start the leadership
        testLeaderService.startLeader();
        waitForEventProcessing();

        //check that test worker got started
        assertThatTestWorkerTaskIsRunning(true);
        testLeaderService.stopLeader();

        //check that test worker got stopped
        assertThatTestWorkerTaskIsRunning(false);

        //restart the leadership
        testLeaderService.startLeader();
        waitForEventProcessing();

        //check that test worker got started again
        assertThatTestWorkerTaskIsRunning(true);

        //check that even exceptions do not make it stop
        testWorkerTask.setThrowException(true);
        assertThatTestWorkerTaskIsRunning(true);

        testLeaderService.stopLeader();

        //check that test worker got stopped again
        assertThatTestWorkerTaskIsRunning(false);
    }

    private void assertThatTestWorkerTaskIsRunning(boolean running) throws Exception {

        //leave the worker some time to finish
        if (!running) {
            Thread.sleep(10);
        }

        int oldRunCount = testWorkerTask.getRunCount();

        //this is to check that the worker had enough time to run
        Thread.sleep(100);
        int newRunCount = testWorkerTask.getRunCount();

        if (running) {
            //there should have been at least one run
            assertThat(newRunCount).as("at least one run should be in 100 ms").isGreaterThan(oldRunCount);
        } else {
            //there should not be a single new run
            assertThat(oldRunCount).as("no run should be in 100 ms").isEqualTo(newRunCount);
        }
    }

}
