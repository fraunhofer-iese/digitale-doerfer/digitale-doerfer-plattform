/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientComment;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientUserGeneratedContentFlagComment;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientUserGeneratedContentFlagPost;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers.UserGeneratedContentFlagAdminUiControllerTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GrapevineUserGeneratedContentFlagAdminUiControllerTest extends UserGeneratedContentFlagAdminUiControllerTest {

    // set true to manually check the returned csv file of the flag export endpoint
    final private static boolean SAVE_EXPORT_CSV = false;

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
        th.createUserGeneratedContentAdmins();
        th.newsItem1.setTenant(null);
        th.postRepository.save(th.newsItem1);
        super.createFlags();
    }

    @Override
    protected BaseSharedTestHelper getTestHelper() {
        return th;
    }

    @Override
    protected Comment getFlagEntityFlag1Tenant1() {
        return th.gossip2Comment2Deleted;
    }

    @Override
    protected Gossip getFlagEntityFlag2Tenant1() {
        return th.gossip2WithComments;
    }

    @Override
    protected Comment getFlagEntityFlag3Tenant1() {
        return th.gossip2Comment1;
    }

    @Override
    protected Comment getFlagEntityFlag1Tenant2() {
        return th.offer1Comment2;
    }

    @Override
    protected Offer getFlagEntityFlag2Tenant2() {
        return th.offer1;
    }

    @Override
    protected Gossip getFlagEntityFlag3Tenant2() {
        return th.tenant2Gossip1; //this one has also comments
    }

    @Override
    protected NewsItem getFlagEntityNoTenant() {
        return th.newsItem1;
    }

    @Override
    protected Collection<String> getSingleFlagUrlTemplates() {
        return Arrays.asList("/grapevine/adminui/flag/post/{flagId}", "/grapevine/adminui/flag/comment/{flagId}");
    }

    @Override
    public void getFlagById_GlobalAdmin() throws Exception {

        getFlagByIdPost_GlobalAdmin();
        getFlagByIdComment_GlobalAdmin();
    }

    private void getFlagByIdPost_GlobalAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant2;
        Gossip flaggedGossip = getFlagEntityFlag3Tenant2();
        assertEquals(flaggedGossip.getId(), expectedFlag.getEntityId());
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords().size()).isGreaterThan(1);
        assertThat(flaggedGossip.getCommentCount()).isGreaterThan(1L);

        assertFlagEquals(flaggedGossip, expectedFlag,
                toObject(mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagPost.class));
    }

    private void getFlagByIdComment_GlobalAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant1;
        Comment flaggedComment = getFlagEntityFlag3Tenant1();
        assertEquals(flaggedComment.getId(), expectedFlag.getEntityId());
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords().size()).isGreaterThan(1);
        assertThat(flaggedComment.getPost().getCommentCount()).isGreaterThan(0L);
        assertTrue(th.commentRepository.findAllByPostOrderByCreatedAsc(flaggedComment.getPost()).stream()
                        .anyMatch(Comment::isDeleted),
                "There should be at least one deleted comment");

        assertFlagEquals(flaggedComment, expectedFlag,
                toObject(mockMvc.perform(
                        get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagComment.class));
    }

    @Override
    public void getFlagById_TenantAdmin() throws Exception {

        getFlagByIdPost_TenantAdmin();
        getFlagByIdComment_TenantAdmin();
    }

    private void getFlagByIdComment_TenantAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant1;
        Comment flaggedComment = getFlagEntityFlag3Tenant1();
        assertEquals(flaggedComment.getId(), expectedFlag.getEntityId());
        assertThat(flaggedComment.getPost().getCommentCount()).isGreaterThan(0L);

        assertFlagEquals(flaggedComment, expectedFlag,
                toObject(mockMvc.perform(
                        get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagComment.class));
    }

    private void getFlagByIdPost_TenantAdmin() throws Exception {
        UserGeneratedContentFlag expectedFlag = flag2Tenant2;
        Offer flaggedOffer = getFlagEntityFlag2Tenant2();
        assertEquals(flaggedOffer.getId(), expectedFlag.getEntityId());
        assertThat(flaggedOffer.getCommentCount()).isGreaterThan(1L);

        assertFlagEquals(flaggedOffer, expectedFlag,
                toObject(mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagPost.class));
    }

    @Test
    public void getFlagById_DeletedEntity() throws Exception {

        UserGeneratedContentFlag expectedFlag;
        expectedFlag = flag1Tenant1;
        Comment flaggedComment = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedComment.getId(), expectedFlag.getEntityId());
        assertThat(flaggedComment.getPost().getCommentCount()).isGreaterThan(0L);
        assertThat(flaggedComment.getText()).isNotBlank();
        assertTrue(flaggedComment.isDeleted(), "The comment should be deleted");

        // flagged comment is deleted
        ClientUserGeneratedContentFlagComment actualFlagComment = toObject(mockMvc.perform(
                get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                        .andExpect(status().isOk())
                        .andReturn(),
                ClientUserGeneratedContentFlagComment.class);
        assertFlagEquals(flaggedComment, expectedFlag, actualFlagComment);

        // post of flagged comment is deleted
        flaggedComment.getPost().setDeleted(true);
        th.postRepository.save(flaggedComment.getPost());

        actualFlagComment = toObject(mockMvc.perform(
                get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                        .andExpect(status().isOk())
                        .andReturn(),
                ClientUserGeneratedContentFlagComment.class);
        assertThat(actualFlagComment.isPostOfCommentDeleted()).isTrue();
        assertFlagEquals(flaggedComment, expectedFlag, actualFlagComment);

        //deleted flagged post is deleted
        expectedFlag = flag2Tenant2;
        Offer flaggedOffer = getFlagEntityFlag2Tenant2();
        flaggedOffer.setDeleted(true);
        th.postRepository.save(flaggedOffer);
        assertEquals(flaggedOffer.getId(), expectedFlag.getEntityId());
        assertThat(flaggedOffer.getText()).isNotBlank();
        assertTrue(flaggedOffer.isDeleted(), "The offer should be deleted");

        ClientUserGeneratedContentFlagPost actualFlagPost =
                toObject(mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagPost.class);
        assertThat(actualFlagPost.isPostDeleted()).isTrue();
        assertFlagEquals(flaggedOffer, expectedFlag, actualFlagPost);
    }

    @Test
    public void getFlagByIdPost_WrongType() throws Exception {

        // comment is not a post
        UserGeneratedContentFlag expectedFlag = flag3Tenant1;
        Comment flaggedComment = getFlagEntityFlag3Tenant1();
        assertEquals(flaggedComment.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagByIdComment_WrongType() throws Exception {

        // offer is not a comment
        UserGeneratedContentFlag expectedFlag;
        expectedFlag = flag2Tenant2;
        Offer flaggedOffer = getFlagEntityFlag2Tenant2();
        assertEquals(flaggedOffer.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagByIdComment_InvalidType() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant1;
        expectedFlag.setEntityType("de.fhg.iese.dd.🐛");
        flagRepository.save(expectedFlag);
        Comment flaggedComment = getFlagEntityFlag3Tenant1();
        assertEquals(flaggedComment.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));

        expectedFlag.setEntityType("");
        flagRepository.save(expectedFlag);

        mockMvc.perform(get("/grapevine/adminui/flag/comment/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagByIdPost_InvalidType() throws Exception {

        UserGeneratedContentFlag expectedFlag;
        expectedFlag = flag2Tenant2;
        expectedFlag.setEntityType("de.fhg.iese.dd.🐸");
        flagRepository.save(expectedFlag);
        Offer flaggedOffer = getFlagEntityFlag2Tenant2();
        assertEquals(flaggedOffer.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));

        expectedFlag.setEntityType("");
        flagRepository.save(expectedFlag);

        mockMvc.perform(get("/grapevine/adminui/flag/post/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void deleteFlagEntity_Post() throws Exception {

        UserGeneratedContentFlag flag = flag2Tenant1;
        Gossip flaggedEntity = getFlagEntityFlag2Tenant1();
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        //check that a push event was triggered
        ClientPostDeleteConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(flaggedEntity.getGeoAreas(),
                        flaggedEntity.isHiddenForOtherHomeAreas(),
                        ClientPostDeleteConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertThat(pushedEvent.getPostId()).isEqualTo(flaggedEntity.getId());

        Post changedPost = th.postRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedPost.isDeleted()).as("Post should be deleted afterwards").isTrue();
        assertEquals(timeServiceMock.currentTimeMillisUTC(), changedPost.getDeletionTime());
    }

    @Test
    public void deleteFlagEntity_DeletedPost() throws Exception {

        UserGeneratedContentFlag flag = flag2Tenant1;
        Gossip flaggedEntity = getFlagEntityFlag2Tenant1();
        flaggedEntity.setDeleted(true);
        th.postRepository.save(flaggedEntity);
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        Post changedPost = th.postRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedPost.isDeleted()).as("Post should still be deleted afterwards").isTrue();
    }

    @Test
    public void deleteFlagEntity_Comment() throws Exception {

        UserGeneratedContentFlag flag = flag1Tenant2;
        Comment flaggedEntity = getFlagEntityFlag1Tenant2();
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        Comment changedComment = th.commentRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedComment.isDeleted()).as("Comment should be deleted afterwards").isTrue();
        assertEquals(timeServiceMock.currentTimeMillisUTC(), changedComment.getDeletionTime());
    }

    @Test
    public void deleteFlagEntity_DeletedComment() throws Exception {

        UserGeneratedContentFlag flag = flag1Tenant2;
        Comment flaggedEntity = getFlagEntityFlag1Tenant2();
        flaggedEntity.setDeleted(true);
        th.commentRepository.save(flaggedEntity);
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        Comment changedComment = th.commentRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedComment.isDeleted()).as("Comment should still be deleted afterwards").isTrue();
    }

    @Test
    public void exportFlags() throws Exception {

        th.newsItem1.setText(th.newsItem1.getText() + "\";\",'\n");
        th.postRepository.save(th.newsItem1);
        th.tenant2Gossip1Comment2.setText(th.tenant2Gossip1Comment2.getText() + "\n\";\",'\n");
        th.commentRepository.saveAndFlush(th.tenant2Gossip1Comment2);
        //second flag for same entity to test summing
        flagRepository.saveAndFlush(UserGeneratedContentFlag.builder()
                .created(flag1Tenant1.getCreated() + 10)
                .entityId(flag1Tenant1.getEntityId())
                .entityType(flag1Tenant1.getEntityType())
                .entityAuthor(flag1Tenant1.getEntityAuthor())
                .flagCreator(th.personRegularAnna)
                .status(flag1Tenant1.getStatus())
                .lastStatusUpdate(flag1Tenant1.getLastStatusUpdate())
                .tenant(flag1Tenant1.getTenant())
                .build());
        //flag for post without text
        Gossip emptyPost = th.postRepository.save(Gossip.builder()
                .creator(th.personRegularAnna)
                .geoAreas(Set.of(th.geoAreaDeutschland))
                .build());
        th.commentRepository.save(Comment.builder()
                .post(emptyPost)
                .creator(th.personRegularAnna)
                .text("Kommentar zu leerem Post")
                .build());
        flagRepository.saveAndFlush(UserGeneratedContentFlag.builder()
                .created(4711)
                .entityId(emptyPost.getId())
                .entityType(emptyPost.getClass().getName())
                .entityAuthor(th.personRegularAnna)
                .flagCreator(th.personShopManager)
                .status(UserGeneratedContentFlagStatus.ACCEPTED)
                .lastStatusUpdate(5711)
                .tenant(th.tenant1)
                .build());
        // all flags
        exportFlagsCheckCsvFile(0, System.currentTimeMillis(), false, 20, 0);
        // only non empty posts
        exportFlagsCheckCsvFile(0, System.currentTimeMillis(), true, 20, 0);
        // restricted by start time
        long mediumCreationTime = userGeneratedContentFlags.stream()
                .sorted(Comparator.comparing(UserGeneratedContentFlag::getCreated))
                .collect(Collectors.toList()).get(userGeneratedContentFlags.size() / 2).getCreated();
        exportFlagsCheckCsvFile(mediumCreationTime, System.currentTimeMillis(), false, 20, 0);

        // restricted by end time
        exportFlagsCheckCsvFile(0, mediumCreationTime, false, 20, 0);
    }

    @Test
    public void exportFlags_BadRequest() throws Exception {

        // start > end
        mockMvc.perform(exportFlagsRequest(th.personGlobalUserGeneratedContentAdmin, 10, 0, false, 10, 0))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //negative start
        mockMvc.perform(exportFlagsRequest(th.personGlobalUserGeneratedContentAdmin, -1, 0, false, 10, 0))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void exportFlags_NotAuthorized() throws Exception {

        mockMvc.perform(exportFlagsRequest(th.personRegularKarl, 0, System.currentTimeMillis(), false, 10, 0))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(get("/grapevine/adminui/flag/exportFlags")
                .param("start", "0")
                .param("end", "10")
                .param("statuses", Stream.of(UserGeneratedContentFlagStatus.OPEN)
                        .map(UserGeneratedContentFlagStatus::toString)
                        .toArray(String[]::new)));
    }

    protected void assertFlagEquals(Post flaggedPost, UserGeneratedContentFlag expectedFlag,
            ClientUserGeneratedContentFlagPost actualFlag) {

        assertPostEquals(flaggedPost, actualFlag.getFlagPost());
        assertThat(flaggedPost.isDeleted()).isEqualTo(actualFlag.isPostDeleted());

        assertCommentsEqual(flaggedPost, actualFlag.getCommentsOfPost());

        super.assertFlagEquals(expectedFlag, actualFlag);
    }

    protected void assertFlagEquals(Comment flaggedComment, UserGeneratedContentFlag expectedFlag,
            ClientUserGeneratedContentFlagComment actualFlag) {

        assertCommentEquals(flaggedComment, actualFlag.getFlagComment());

        assertPostEquals(flaggedComment.getPost(), actualFlag.getPostOfComment());
        assertThat(flaggedComment.getPost().isDeleted()).isEqualTo(actualFlag.isPostOfCommentDeleted());

        assertCommentsEqual(flaggedComment.getPost(), actualFlag.getCommentsOfPost());

        super.assertFlagEquals(expectedFlag, actualFlag);
    }

    private void assertCommentsEqual(Post post, List<ClientComment> actualComments) {

        List<Comment> expectedComments = th.commentRepository.findAllByPostOrderByCreatedAsc(post);
        //this is to ensure integrity of test data
        assertEquals(expectedComments.size(), post.getCommentCount());
        assertEquals(expectedComments.size(), actualComments.size());
        for (int i = 0; i < expectedComments.size(); i++) {
            assertCommentEquals(expectedComments.get(i), actualComments.get(i));
        }
    }

    private void assertCommentEquals(Comment expectedComment, ClientComment actualComment) {
        //not tested in detail, just to ensure that the right entity is used
        assertEquals(expectedComment.getId(), actualComment.getId());
        assertEquals(expectedComment.isDeleted(), actualComment.isDeleted());
        //we also want the text, even if it is deleted
        assertEquals(expectedComment.getText(), actualComment.getText());
        assertEquals(expectedComment.getCreator().getId(), actualComment.getCreator().getId());
    }

    private void assertPostEquals(Post expectedPost, ClientPost actualPost) {
        //not tested in detail, just to ensure that the right entity is used
        assertEquals(expectedPost.getId(), actualPost.getId());
        assertEquals(expectedPost.getPostType(), actualPost.getType().toPostType());
        expectedPost.accept(new Post.Visitor<Void>() {

            @Override
            public Void whenGossip(Gossip gossip) {
                assertEquals(gossip.getId(), actualPost.getGossip().getId());
                assertEquals(gossip.getText(), actualPost.getGossip().getText());
                return null;
            }

            @Override
            public Void whenNewsItem(NewsItem newsItem) {
                assertEquals(newsItem.getId(), actualPost.getNewsItem().getId());
                assertEquals(newsItem.getText(), actualPost.getNewsItem().getText());
                return null;
            }

            @Override
            public Void whenSuggestion(Suggestion suggestion) {
                assertEquals(suggestion.getId(), actualPost.getSuggestion().getId());
                assertEquals(suggestion.getText(), actualPost.getSuggestion().getText());
                return null;
            }

            @Override
            public Void whenSeeking(Seeking seeking) {
                assertEquals(seeking.getId(), actualPost.getSeeking().getId());
                assertEquals(seeking.getText(), actualPost.getSeeking().getText());
                return null;
            }

            @Override
            public Void whenOffer(Offer offer) {
                assertEquals(offer.getId(), actualPost.getOffer().getId());
                assertEquals(offer.getText(), actualPost.getOffer().getText());
                return null;
            }

            @Override
            public Void whenHappening(Happening happening) {
                assertEquals(happening.getId(), actualPost.getHappening().getId());
                assertEquals(happening.getText(), actualPost.getHappening().getText());
                return null;
            }

            @Override
            public Void whenSpecialPost(SpecialPost specialPost) {
                assertEquals(specialPost.getId(), actualPost.getSpecialPost().getId());
                assertEquals(specialPost.getText(), actualPost.getSpecialPost().getText());
                return null;
            }
        });
    }

    private MockHttpServletRequestBuilder exportFlagsRequest(Person caller, long start, long end, boolean ignoreEmpty, int pageSize, int pageNum) {

        return get("/grapevine/adminui/flag/exportFlags")
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end))
                .param("pageSize", String.valueOf(pageSize))
                .param("pageNumber", String.valueOf(pageNum))
                .param("ignoreEmpty", String.valueOf(ignoreEmpty))
                .headers(authHeadersFor(caller));
    }

    private void exportFlagsCheckCsvFile(long start, long end, boolean ignoreEmpty, int pageSize, int pageNum) throws Exception {

        MockHttpServletResponse response = mockMvc
                .perform(exportFlagsRequest(th.personGlobalUserGeneratedContentAdmin, start, end, ignoreEmpty, pageSize,
                        pageNum))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType("text", "csv", StandardCharsets.UTF_8)))
                .andReturn()
                .getResponse();
        String headerContentDisposition = response.getHeader(HttpHeaders.CONTENT_DISPOSITION);
        String fileName = StringUtils.substringBetween(headerContentDisposition, "\"");
        final byte[] csvBytes = response.getContentAsByteArray();
        if (SAVE_EXPORT_CSV) {
            final Path tempFile = Files.createTempFile(getClass().getSimpleName(), "-" + fileName);
            log.info("Saving flag export to {}", tempFile.toUri());
            Files.write(tempFile, csvBytes);
        }

        try (CSVReader csvReader = new CSVReaderBuilder(new StringReader(new String(csvBytes, StandardCharsets.UTF_8)))
                .withCSVParser(new CSVParserBuilder()
                        .withSeparator(';')
                        .withQuoteChar('"')
                        .build())
                .build()) {
            List<String[]> csvContent = csvReader.readAll();
            assertThat(csvContent.get(0)).isEqualTo(new String[]{
                    "\ufeffENTITYID",
                    "ENTITYTYPE",
                    "FLAGGEDTEXTINDEX",
                    "NUMFLAGSACCEPTED",
                    "NUMFLAGSINPROGRESS",
                    "NUMFLAGSOPEN",
                    "NUMFLAGSREJECTED",
                    "POSTANDCOMMENTSTEXT",
                    "POSTID",
                    "TEXT"
            });
        }
    }

}
