/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import com.google.common.collect.ImmutableSet;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.*;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PersonAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    private static final String PERSON_HOME_AREA_AND_TENANT_IN_COMMUNITY1_ID = "712ebe07-c3bc-41ea-bda4-b855e01880da";

    private Person personWithOauthAccountTenant1;
    private OauthAccount oAuthAccount;
    private String nonBackendPersonEmail;
    private OauthAccount nonBackendPersonOauthAccount;
    private Person personNoOauthAccount;
    private Person personNoTenantNoHomeArea;
    private Person personTenant1HomeAreaTenant1;
    private Person personTenant1NoHomeArea;
    private Person personNoTenantHomeAreaTenant1;
    private Person personTenant2;
    private Person personTenant2RoleManagerForTenant1;
    private Person personTenant3;
    private Person personGlobalUserAdmin;
    private Person personUserAdminTenant1;
    private Person personUserAdminTenant2;
    private Person personShopOwnerTenant1;
    private Person personShopOwnerTenant2;
    private Person personWithDifferentRoles;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPersons();
        th.createShops();

        personWithOauthAccountTenant1 = th.personRegularAnna;
        personWithOauthAccountTenant1 = th.addVerificationStatusToPerson(th.personRegularAnna,
                PersonVerificationStatus.EMAIL_VERIFIED,
                PersonVerificationStatus.PHONE_NUMBER_VERIFIED);
        assertEquals(th.tenant1, personWithOauthAccountTenant1.getTenant());
        oAuthAccount = OauthAccount.builder()
                .oauthId(personWithOauthAccountTenant1.getOauthId())
                .name(personWithOauthAccountTenant1.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.FACEBOOK))
                .devices(Arrays.asList(
                        OauthAccount.OauthAccountDevice.builder()
                                .clientIdentifier("4711")
                                .deviceName("My device")
                                .build(),
                        OauthAccount.OauthAccountDevice.builder()
                                .clientIdentifier(th.app1Variant1OauthClient.getOauthClientIdentifier())
                                .deviceName("Sesamphone")
                                .build(),
                        // duplicated device
                        OauthAccount.OauthAccountDevice.builder()
                                .clientIdentifier("4711")
                                .deviceName("My device")
                                .build()))
                .blocked(true)
                .blockReason(OauthAccount.BlockReason.MANUALLY_BLOCKED)
                .loginCount(42)
                .build();
        // this will overwrite the existing account
        testOauthManagementService.addOAuthAccount(oAuthAccount, personWithOauthAccountTenant1.getEmail());

        nonBackendPersonEmail = "this.is.somebody@example.org";
        nonBackendPersonOauthAccount = OauthAccount.builder()
                .oauthId("oauthId592847582")
                .name("Name " + nonBackendPersonEmail)
                .authenticationMethods(ImmutableSet.of(OauthAccount.AuthenticationMethod.APPLE,
                        OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .blocked(false)
                .blockReason(null)
                .loginCount(21)
                .build();
        // this will overwrite the existing account
        testOauthManagementService.addOAuthAccount(nonBackendPersonOauthAccount, nonBackendPersonEmail);

        personNoOauthAccount =
                th.createPersonWithDefaultAddress("personNoOauthAccount", th.tenant1,
                        "5d192084-bd18-4009-8b88-93dd86a0db5a");
        personNoOauthAccount = th.addVerificationStatusToPerson(personNoOauthAccount,
                PersonVerificationStatus.NAME_VERIFIED);
        testOauthManagementService.deleteUser(personNoOauthAccount.getOauthId());

        personNoTenantNoHomeArea =
                th.createPersonWithDefaultAddress("personNoTenantNoHomeArea", th.tenant1,
                        "61b933be-e1f3-40b9-8918-dcd823172596");
        personNoTenantNoHomeArea.setTenant(null);
        personNoTenantNoHomeArea.setHomeArea(null);
        personNoTenantNoHomeArea = th.personRepository.save(personNoTenantNoHomeArea);
        personNoTenantNoHomeArea = th.addVerificationStatusToPerson(personNoTenantNoHomeArea,
                PersonVerificationStatus.EMAIL_VERIFIED);

        personTenant1HomeAreaTenant1 =
                th.createPersonWithDefaultAddress("personTenant1HomeAreaTenant1", th.tenant1,
                        PERSON_HOME_AREA_AND_TENANT_IN_COMMUNITY1_ID);

        personTenant1NoHomeArea =
                th.createPersonWithDefaultAddress("personTenant1NoHomeArea", th.tenant1,
                        "19f8453f-5782-4fac-bfa4-23e1a199242a");
        personTenant1NoHomeArea.setHomeArea(null);
        personTenant1NoHomeArea = th.personRepository.save(personTenant1NoHomeArea);
        personTenant1NoHomeArea = th.addVerificationStatusToPerson(personTenant1NoHomeArea,
                PersonVerificationStatus.NAME_VERIFIED,
                PersonVerificationStatus.EMAIL_VERIFIED);

        personNoTenantHomeAreaTenant1 =
                th.createPersonWithDefaultAddress("personNoTenantHomeAreaTenant1", th.tenant1,
                        "5ff7abbb-88e7-4eb3-adbe-5f2957e27eec");
        personNoTenantHomeAreaTenant1.setTenant(null);
        personNoTenantHomeAreaTenant1 = th.personRepository.save(personNoTenantHomeAreaTenant1);
        personNoTenantHomeAreaTenant1 = th.addVerificationStatusToPerson(personNoTenantHomeAreaTenant1,
                PersonVerificationStatus.HOME_AREA_VERIFIED,
                PersonVerificationStatus.PHONE_NUMBER_VERIFIED,
                PersonVerificationStatus.EMAIL_VERIFIED);

        personTenant2 = th.personRoleManagerTenant2;

        personTenant2RoleManagerForTenant1 =
                th.createPersonWithDefaultAddress("personTenant2RoleManagerForTenant1", th.tenant2,
                        "886a98ed-f64d-44cb-92d3-38445f45828d");
        th.assignRoleToPerson(personTenant2RoleManagerForTenant1, RoleManager.class, th.tenant1.getId());
        personTenant2RoleManagerForTenant1 = th.addVerificationStatusToPerson(personTenant2RoleManagerForTenant1,
                PersonVerificationStatus.PHONE_NUMBER_VERIFIED,
                PersonVerificationStatus.EMAIL_VERIFIED);

        personTenant3 = th.personRegularHorstiTenant3;

        personGlobalUserAdmin =
                th.createPersonWithDefaultAddress("personGlobalUserAdmin", th.tenant1,
                        "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalUserAdmin, GlobalUserAdmin.class, null);

        personUserAdminTenant1 =
                th.createPersonWithDefaultAddress("personUserAdminTenant1", th.tenant1,
                        "6b0239cf-f13d-4af0-b401-e984686194f2");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());

        personUserAdminTenant2 =
                th.createPersonWithDefaultAddress("personUserAdminTenant2", th.tenant2,
                        "8426493e-7498-408b-8d96-32684c82a7a3");
        th.assignRoleToPerson(personUserAdminTenant2, UserAdmin.class, th.tenant2.getId());
        personUserAdminTenant2 = th.addVerificationStatusToPerson(personUserAdminTenant2,
                PersonVerificationStatus.PHONE_NUMBER_VERIFIED,
                PersonVerificationStatus.EMAIL_VERIFIED);

        personShopOwnerTenant1 =
                th.createPersonWithDefaultAddress("personShopOwnerTenant1", th.tenant1,
                        "5a81deb2-0828-4b8c-ba81-39f7f5fd7999");
        th.assignRoleToPerson(personShopOwnerTenant1, ShopOwner.class, th.shop1.getId());

        personShopOwnerTenant2 =
                th.createPersonWithDefaultAddress("personShopOwnerTenant2", th.tenant2,
                        "91419d8f-5b67-4cfb-882d-735a965cd2f3");
        th.assignRoleToPerson(personShopOwnerTenant2, ShopOwner.class, th.shop4.getId());

        personWithDifferentRoles =
                th.createPersonWithDefaultAddress("personWithDifferentRoles", th.tenant1,
                        "58a4914a-1615-4619-8fec-1d4cdfb71d6b");
        th.assignRoleToPerson(personWithDifferentRoles, ShopOwner.class, th.shop1.getId());
        th.assignRoleToPerson(personWithDifferentRoles, UserAdmin.class, th.tenant1.getId());
        th.assignRoleToPerson(personWithDifferentRoles, UserAdmin.class, th.tenant3.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getPerson_SuperAdminAndGlobalUserAdminAndUserAdmin() throws Exception {

        final Person person = personWithOauthAccountTenant1;
        //this should be delivered to the admin ui, too
        person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
        th.personRepository.saveAndFlush(person);

        List<Person> personsWithSuitableRoles =
                Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin, personUserAdminTenant1);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                    .headers(authHeadersFor(caller)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                    .andExpect(jsonPath("oauthAccount.authenticationMethods",
                            hasSize(oAuthAccount.getAuthenticationMethods().size())))
                    .andExpect(jsonPath("oauthAccount.authenticationMethods[0]").value(
                            oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                    .andExpect(jsonPath("oauthAccount.devices", hasSize(2)))
                    .andExpect(jsonPath("oauthAccount.devices[0].clientId").value(
                            oAuthAccount.getDevices().get(0).getClientIdentifier()))
                    .andExpect(jsonPath("oauthAccount.devices[0].clientName").value("-"))
                    .andExpect(jsonPath("oauthAccount.devices[0].deviceName").value(
                            oAuthAccount.getDevices().get(0).getDeviceName()))
                    .andExpect(jsonPath("oauthAccount.devices[1].clientId").value(
                            oAuthAccount.getDevices().get(1).getClientIdentifier()))
                    .andExpect(jsonPath("oauthAccount.devices[1].clientName").value(
                            th.app1Variant1OauthClient.getName()))
                    .andExpect(jsonPath("oauthAccount.devices[1].deviceName").value(
                            oAuthAccount.getDevices().get(1).getDeviceName()))
                    .andExpect(jsonPath("oauthAccount.devices[0].clientName").value("-"))
                    .andExpect(jsonPath("oauthAccount.blockReason").value(oAuthAccount.getBlockReason().name()))
                    .andExpect(jsonPath("oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                    .andExpect(jsonPath("oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                    .andExpect(jsonPath("oauthAccount.retrievalFailed").value(false))
                    .andExpect(jsonPath("oauthAccount.failedReason").isEmpty())
                    .andReturn();
            ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
            assertPersonsEqual(person, actualPerson);
        }
    }

    @Test
    public void getPerson_GlobalUserAdmin_RoleAssignments() throws Exception {

        final Person caller = personGlobalUserAdmin;
        final Person person = personWithDifferentRoles;

        MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount.oauthId").value(person.getOauthId()))
                .andExpect(jsonPath("roleAssignments", hasSize(3)))
                .andExpect(jsonPath("roleAssignments[0].roleKey").value("SHOP_OWNER"))
                .andExpect(jsonPath("roleAssignments[0].personId").value(person.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityId").value(th.shop1.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityName").value(th.shop1.getName()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityTenantName").value(th.tenant1.getName()))
                .andExpect(jsonPath("roleAssignments[1].roleKey").value("USER_ADMIN"))
                .andExpect(jsonPath("roleAssignments[1].personId").value(person.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityName").value(th.tenant1.getName()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityTenantName").value(th.tenant1.getName()))
                .andExpect(jsonPath("roleAssignments[2].roleKey").value("USER_ADMIN"))
                .andExpect(jsonPath("roleAssignments[2].personId").value(person.getId()))
                .andExpect(jsonPath("roleAssignments[2].relatedEntityId").value(th.tenant3.getId()))
                .andExpect(jsonPath("roleAssignments[2].relatedEntityName").value(th.tenant3.getName()))
                .andExpect(jsonPath("roleAssignments[2].relatedEntityTenantId").value(th.tenant3.getId()))
                .andExpect(jsonPath("roleAssignments[2].relatedEntityTenantName").value(th.tenant3.getName()))
                .andReturn();
        ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqual(person, actualPerson);
    }

    @Test
    public void getPerson_RoleManager_RoleAssignments() throws Exception {

        final Person caller = th.personRoleManagerTenant1;
        Person person = personWithOauthAccountTenant1;

        MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount").isEmpty())
                .andExpect(jsonPath("roleAssignments").isEmpty())//this person has no roles
                .andReturn();
        ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqualLimitedAttributes(person, actualPerson);

        person = personWithDifferentRoles;
        result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount").isEmpty())
                .andExpect(jsonPath("roleAssignments", hasSize(2)))
                .andExpect(jsonPath("roleAssignments[0].roleKey").value("SHOP_OWNER"))
                .andExpect(jsonPath("roleAssignments[0].personId").value(person.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityId").value(th.shop1.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityName").value(th.shop1.getName()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[0].relatedEntityTenantName").value(th.tenant1.getName()))
                .andExpect(jsonPath("roleAssignments[1].roleKey").value("USER_ADMIN"))
                .andExpect(jsonPath("roleAssignments[1].personId").value(person.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityName").value(th.tenant1.getName()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("roleAssignments[1].relatedEntityTenantName").value(th.tenant1.getName()))
                .andReturn();
        actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqualLimitedAttributes(person, actualPerson);
    }

    @Test
    public void getPerson_NoOauthAccount() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person person = personNoOauthAccount;

        MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount.oauthId").value(person.getOauthId()))
                .andExpect(jsonPath("oauthAccount.authenticationMethods").isEmpty())
                .andExpect(jsonPath("oauthAccount.retrievalFailed").value(true))
                .andExpect(jsonPath("oauthAccount.failedReason").value(
                        String.format("No OAuth account found for user with oauth id %s", person.getOauthId())))
                .andReturn();
        ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqual(person, actualPerson);
    }

    @Test
    public void getPerson_DeletedPersonWithoutOauthAccount() throws Exception {

        final Person person = th.personDeleted;
        final Person caller = personGlobalUserAdmin;

        MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount.oauthId").value(person.getOauthId()))
                .andExpect(jsonPath("oauthAccount.authenticationMethods").isEmpty())
                .andExpect(jsonPath("oauthAccount.blockReason").isEmpty())
                .andExpect(jsonPath("oauthAccount.loginCount").isEmpty())
                .andExpect(jsonPath("oauthAccount.blocked").value(false))
                .andExpect(jsonPath("oauthAccount.retrievalFailed").value(true))
                .andExpect(jsonPath("oauthAccount.failedReason").value(
                        "No OAuth account found for user with oauth id " + person.getOauthId()))
                .andReturn();
        ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqual(person, actualPerson);
    }

    @Test
    public void getPerson_BlockedDeletedPersonWithOauthAccount() throws Exception {

        final Person person = th.personDeleted;
        person.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        th.personRepository.saveAndFlush(person);
        final Person caller = personGlobalUserAdmin;
        final OauthAccount oauthAccount = OauthAccount.builder()
                .blocked(true)
                .blockReason(OauthAccount.BlockReason.MANUALLY_BLOCKED)
                .loginCount(2)
                .emailVerified(false)
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .oauthId(person.getOauthId())
                .build();
        testOauthManagementService.addOAuthAccount(oauthAccount, person.getEmail());

        MvcResult result = mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("oauthAccount.oauthId").value(oauthAccount.getOauthId()))
                .andExpect(jsonPath("oauthAccount.name").value(oauthAccount.getName()))
                .andExpect(jsonPath("oauthAccount.authenticationMethods",
                        hasSize(oauthAccount.getAuthenticationMethods().size())))
                .andExpect(jsonPath("oauthAccount.authenticationMethods[0]").value(
                        oauthAccount.getAuthenticationMethods().iterator().next().name()))
                .andExpect(jsonPath("oauthAccount.blockReason").value(oauthAccount.getBlockReason().name()))
                .andExpect(jsonPath("oauthAccount.loginCount").value(oauthAccount.getLoginCount()))
                .andExpect(jsonPath("oauthAccount.blocked").value(oauthAccount.isBlocked()))
                .andExpect(jsonPath("oauthAccount.retrievalFailed").value(false))
                .andExpect(jsonPath("oauthAccount.failedReason").isEmpty())
                .andReturn();
        ClientPersonExtended actualPerson = toObject(result, ClientPersonExtended.class);
        assertPersonsEqual(person, actualPerson);
    }

    @Test
    public void getPerson_unauthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final Person person = personWithOauthAccountTenant1;

        assertOAuth2(get("/adminui/person/{personId}", person.getId()));

        mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getPerson_checkAppVariantUsage() throws Exception {

        final Person person = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;
        final GeoArea selectedGeoArea = th.geoAreaEisenberg;

        th.selectGeoAreaForAppVariant(appVariant, person, selectedGeoArea);

        mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(personGlobalUserAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("appVariantUsages[0].appVariant.appVariantIdentifier")
                        .value(appVariant.getAppVariantIdentifier()))
                .andExpect(jsonPath("appVariantUsages[0].selectedGeoAreas[0].id")
                        .value(selectedGeoArea.getId()));

        //no permission to get the app variant usages
        mockMvc.perform(get("/adminui/person/{personId}", person.getId())
                .headers(authHeadersFor(th.personRoleManagerTenant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("appVariantUsages").isEmpty());
    }

    @Test
    public void getOauthUser_superAdminAndGlobalUserAdmin() throws Exception {

        for (Person caller : Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin)) {
            log.info("Calling as {}", caller);
            mockMvc.perform(get("/adminui/oauthUserByEmail/{email}", personWithOauthAccountTenant1.getEmail())
                            .headers(authHeadersFor(caller)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("oauthId").value(oAuthAccount.getOauthId()))
                    .andExpect(jsonPath("name").value(oAuthAccount.getName()))
                    .andExpect(jsonPath("authenticationMethods",
                            hasSize(oAuthAccount.getAuthenticationMethods().size())))
                    .andExpect(jsonPath("authenticationMethods[0]").value(
                            oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                    .andExpect(jsonPath("devices", hasSize(2)))
                    .andExpect(jsonPath("devices[0].clientId").value(
                            oAuthAccount.getDevices().get(0).getClientIdentifier()))
                    .andExpect(jsonPath("devices[0].clientName").value("-"))
                    .andExpect(jsonPath("devices[0].deviceName").value(
                            oAuthAccount.getDevices().get(0).getDeviceName()))
                    .andExpect(jsonPath("devices[1].clientId").value(
                            oAuthAccount.getDevices().get(1).getClientIdentifier()))
                    .andExpect(jsonPath("devices[1].clientName").value(
                            th.app1Variant1OauthClient.getName()))
                    .andExpect(jsonPath("devices[1].deviceName").value(
                            oAuthAccount.getDevices().get(1).getDeviceName()))
                    .andExpect(jsonPath("devices[0].clientName").value("-"))
                    .andExpect(jsonPath("blockReason").value(oAuthAccount.getBlockReason().name()))
                    .andExpect(jsonPath("loginCount").value(oAuthAccount.getLoginCount()))
                    .andExpect(jsonPath("blocked").value(oAuthAccount.isBlocked()))
                    .andExpect(jsonPath("retrievalFailed").value(false))
                    .andExpect(jsonPath("failedReason").isEmpty());

            mockMvc.perform(get("/adminui/oauthUserByEmail/{email}", nonBackendPersonEmail)
                    .headers(authHeadersFor(caller)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("oauthId").value(nonBackendPersonOauthAccount.getOauthId()))
                    .andExpect(jsonPath("name").value(nonBackendPersonOauthAccount.getName()))
                    .andExpect(jsonPath("authenticationMethods",
                            hasSize(nonBackendPersonOauthAccount.getAuthenticationMethods().size())))
                    .andExpect(jsonPath("authenticationMethods[0]").value(
                            nonBackendPersonOauthAccount.getAuthenticationMethods().iterator().next().name()))
                    .andExpect(jsonPath("blockReason").isEmpty())
                    .andExpect(jsonPath("loginCount").value(nonBackendPersonOauthAccount.getLoginCount()))
                    .andExpect(jsonPath("blocked").value(nonBackendPersonOauthAccount.isBlocked()))
                    .andExpect(jsonPath("retrievalFailed").value(false))
                    .andExpect(jsonPath("failedReason").isEmpty());
        }
    }

    @Test
    public void getOauthUser_personsWithInsufficientPrivileges() throws Exception {

        for (Person caller : Arrays.asList(personUserAdminTenant1, th.personRoleManagerTenant1, th.personRegularKarl)) {
            log.info("Calling as {}", caller);
            mockMvc.perform(get("/adminui/oauthUserByEmail/{email}", nonBackendPersonEmail)
                    .headers(authHeadersFor(caller)))
                    .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
        }
    }

    @Test
    public void getOauthUser_userNotFound() throws Exception {

        final String nonExistentEmail = "doesNot@exist";
        mockMvc.perform(get("/adminui/oauthUserByEmail/{email}", nonExistentEmail)
                .headers(authHeadersFor(personGlobalUserAdmin)))
                .andExpect(isException(nonExistentEmail, ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));
    }

    @Test
    public void listPersons_superAdminAndGlobalUserAdmin() throws Exception {

        for (Person caller : Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin)) {
            log.info("Calling as {}", caller);
            final ClientPersonExtendedPageBean pagedResponse = listPersons(caller);
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.keySet().containsAll(Arrays.asList(
                    personNoTenantNoHomeArea.getId(),
                    personTenant1HomeAreaTenant1.getId(),
                    personTenant1NoHomeArea.getId(),
                    personNoTenantHomeAreaTenant1.getId(),
                    personTenant2.getId(),
                    personTenant3.getId()
            )));
            assertEquals(th.personRepository.findAll()
                    .stream()
                    .filter(Person::isNotDeleted)
                    .count(), personById.size()); //all users are listed!
            final Person expectedPerson = personTenant1HomeAreaTenant1;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersons_userAdmin() throws Exception {

        final Person caller = personUserAdminTenant1;
        //also tests that a search parameter with only spaces is ignored
        final ClientPersonExtendedPageBean pagedResponse = listPersons(caller, "  ");
        final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
        assertTrue(personById.keySet().containsAll(Arrays.asList(
                personTenant1HomeAreaTenant1.getId(),
                personTenant1NoHomeArea.getId(),
                personNoTenantHomeAreaTenant1.getId()
        )));
        assertFalse(personById.containsKey(personNoTenantNoHomeArea.getId()));
        assertFalse(personById.containsKey(personTenant2.getId()));
        assertFalse(personById.containsKey(personTenant3.getId()));

        final Person expectedPerson = personTenant1HomeAreaTenant1;
        final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
        assertPersonsEqual(expectedPerson, actualPerson);
    }

    @Test
    public void listPersons_roleManager() throws Exception {

        final Person caller = personTenant2RoleManagerForTenant1;
        final ClientPersonExtendedPageBean pagedResponse = listPersons(caller);
        final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
        assertTrue(personById.keySet().containsAll(Arrays.asList(
                personTenant1HomeAreaTenant1.getId(),
                personTenant1NoHomeArea.getId(),
                personNoTenantHomeAreaTenant1.getId()
        )));
        assertFalse(personById.containsKey(personNoTenantNoHomeArea.getId()));
        assertFalse(personById.containsKey(personTenant2.getId()));
        assertFalse(personById.containsKey(personTenant3.getId()));
        assertFalse(personById.containsKey(th.personDeleted.getId()));

        final Person expectedPerson = personTenant1HomeAreaTenant1;
        final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
        assertPersonsEqualLimitedAttributes(expectedPerson, actualPerson);
    }

    @Test
    public void listPersons_differentSorting() throws Exception {

        final Person caller = personGlobalUserAdmin;
        for (PersonAdminUiController.PersonSortColumn column : PersonAdminUiController.PersonSortColumn.values()) {
            for (Sort.Direction direction : Sort.Direction.values()) {
                final Person expectedFirstPerson = th.personRepository.findAllByDeletedFalse(
                        PageRequest.of(0, 1, Sort.by(direction, column.getColumn()))).getContent().get(0);
                final ClientPersonExtended actualFirstPerson =
                        listPersons(caller, 0, 1, column.name(), direction.name(), null).getContent().get(0);
                assertEquals(expectedFirstPerson.getId(), actualFirstPerson.getId(), "Sorting by " + column + "." + direction + " should work as expected");
            }
        }
    }

    @Test
    public void listPersons_deletedPersonIsOnlyListedWhenSearchIsUsed() throws Exception {

        final Person caller = personGlobalUserAdmin;
        final Person deletedPerson = th.personDeleted;
        Map<String, ClientPersonExtended> personById = buildIdToPersonMap(listPersons(caller));
        assertFalse(personById.containsKey(deletedPerson.getId()));

        //empty search
        personById = buildIdToPersonMap(listPersons(caller, ""));
        assertFalse(personById.containsKey(deletedPerson.getId()));

        personById = buildIdToPersonMap(listPersons(caller, deletedPerson.getId()));
        assertTrue(personById.containsKey(deletedPerson.getId()));

        final ClientPersonExtended returnedDeletedPerson = personById.get(deletedPerson.getId());
        assertEquals(ClientPersonStatus.DELETED, returnedDeletedPerson.getStatus());
        assertEquals(Long.valueOf(deletedPerson.getCreated()), returnedDeletedPerson.getCreated());
        assertNull(returnedDeletedPerson.getLastLoggedIn());
        assertNull(returnedDeletedPerson.getHomeAreaId());
        assertNull(returnedDeletedPerson.getHomeAreaName());
        assertNull(returnedDeletedPerson.getHomeAreaTenantId());
        assertNull(returnedDeletedPerson.getHomeAreaTenantName());
        assertNull(returnedDeletedPerson.getHomeCommunityId());
        assertNull(returnedDeletedPerson.getHomeCommunityName());
        assertNull(returnedDeletedPerson.getAddress());
        assertNull(returnedDeletedPerson.getEmail());
        assertNull(returnedDeletedPerson.getAccountType());
        assertNull(returnedDeletedPerson.getOauthAccount());
        assertNull(returnedDeletedPerson.getNickName());
        assertNull(returnedDeletedPerson.getPhoneNumber());
        assertNull(returnedDeletedPerson.getFirstName());
        assertNull(returnedDeletedPerson.getLastName());
        assertNull(returnedDeletedPerson.getProfilePicture());
    }

    @Test
    public void listPersons_unauthorized() throws Exception {

        Person caller = th.personRegularKarl;

        assertOAuth2(get("/adminui/person")
                .param("page", "0")
                .param("sortBy", "firstNameAsc"));

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void listPersons_wrongParameters() throws Exception {

        final Person caller = personGlobalUserAdmin;

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("page", "a"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("count", "b"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("sortColumn", "unavailable"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("sortDirection", "DSC"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void listPersonsWithSearch_superAdminOrGlobalOrLocalUserAdmin_infix() throws Exception {

        List<Person> personsWithSuitableRoles =
                Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin, personUserAdminTenant1);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            //Search for "First" returns all persons since this is a common prefix to all persons' first name
            final ClientPersonExtendedPageBean pagedResponse = listPersons(caller, "First");
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.keySet().containsAll(Arrays.asList(
                    personTenant1HomeAreaTenant1.getId(),
                    personTenant1NoHomeArea.getId(),
                    personNoTenantHomeAreaTenant1.getId()
            )));
            final Person expectedPerson = personTenant1HomeAreaTenant1;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsWithSearch_superAdminOrGlobalOrLocalUserAdmin_firstAndLastName() throws Exception {

        List<Person> personsWithSuitableRoles =
                Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin, personUserAdminTenant1);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            //Search for "First Last" returns all persons since this First is a common prefix to all persons' first name
            //and Last is a common prefix to all persons' last names
            final ClientPersonExtendedPageBean pagedResponse = listPersons(caller, "First Last");
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.keySet().containsAll(Arrays.asList(
                    personTenant1HomeAreaTenant1.getId(),
                    personTenant1NoHomeArea.getId(),
                    personNoTenantHomeAreaTenant1.getId()
            )));
            final Person expectedPerson = personTenant1HomeAreaTenant1;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsWithSearch_superAdminOrGlobalOrLocalUserAdmin_multipleFirstNames() throws Exception {

        Person annaWithMultipleFirstNames = th.personRegularAnna;
        annaWithMultipleFirstNames.setFirstName("Anna Maria");
        annaWithMultipleFirstNames = th.personRepository.saveAndFlush(th.personRegularAnna);

        List<Person> personsWithSuitableRoles =
                Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin, personUserAdminTenant1);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);

            final ClientPersonExtendedPageBean pagedResponse = listPersons(caller, "anna maria");
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.keySet().containsAll(Collections.singletonList(
                    annaWithMultipleFirstNames.getId()
            )));
            final Person expectedPerson = annaWithMultipleFirstNames;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsWithSearch_superAdminOrGlobalOrLocalUserAdmin_personById() throws Exception {

        List<Person> personsWithSuitableRoles =
                Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin, personUserAdminTenant1);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            //Search for complete UUID should only return one person
            final ClientPersonExtendedPageBean pagedResponse =
                    listPersons(caller, PERSON_HOME_AREA_AND_TENANT_IN_COMMUNITY1_ID);
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.containsKey(personTenant1HomeAreaTenant1.getId()));
            assertEquals(1, personById.keySet().size(), "only the person with this id should be returned");
            final Person expectedPerson = personTenant1HomeAreaTenant1;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsWithSearch_globalUserAdmin_noResult() throws Exception {

        final Person caller = this.personGlobalUserAdmin;
        final String not_existing_UUID = "3fbc7dec-3724-4db0-8f69-6a4b575def89";

        final ClientPersonExtendedPageBean pagedResponse = listPersons(caller, not_existing_UUID);
        assertEquals(0, pagedResponse.getContent().size());
    }

    @Test
    public void listPersonsWithSearch_roleManager_infix() throws Exception {

        final Person caller = th.personRoleManagerTenant1;
        //This persons email address is unique, so only one person is returned, also tests trimming of additional white space
        final ClientPersonExtendedPageBean pagedResponse =
                listPersons(caller, personTenant1HomeAreaTenant1.getEmail() + " ");
        final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
        assertTrue(personById.containsKey(personTenant1HomeAreaTenant1.getId()));
        assertEquals(1, personById.keySet().size(), "only the person with this id should be returned");

        final Person expectedPerson = personTenant1HomeAreaTenant1;
        final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
        assertPersonsEqualLimitedAttributes(expectedPerson, actualPerson);
    }

    @Test
    public void listPersonsWithSearch_roleManager_searchParamToShort() throws Exception {

        final Person caller = this.personGlobalUserAdmin;

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("search", "a"))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));

        mockMvc.perform(get("/adminui/person")
                .headers(authHeadersFor(caller))
                .param("search", "AB"))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));
    }

    @Test
    public void listPersonsByRole_superAdminAndGlobalUserAdmin() throws Exception {

        List<Person> personsWithSuitableRoles = Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            final ClientPersonExtendedPageBean pagedResponse = listPersonsByRoleNoTenantId(caller, "USER_ADMIN");
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.keySet().containsAll(Arrays.asList(personUserAdminTenant1.getId(),
                    personUserAdminTenant2.getId())));
            final Person expectedPerson = personUserAdminTenant2;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsByRoleWithTenant_superAdminAndGlobalUserAdmin() throws Exception {

        List<Person> personsWithSuitableRoles = Arrays.asList(th.personSuperAdmin, personGlobalUserAdmin);
        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            final ClientPersonExtendedPageBean pagedResponse =
                    listPersonsByRole(caller, "USER_ADMIN", th.tenant1.getId());
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.containsKey(personUserAdminTenant1.getId()));
            assertFalse(personById.containsKey(personUserAdminTenant2.getId()));
            final Person expectedPerson = personUserAdminTenant1;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }

        for (Person caller : personsWithSuitableRoles) {
            log.info("Calling as {}", caller);
            final ClientPersonExtendedPageBean pagedResponse =
                    listPersonsByRole(caller, "USER_ADMIN", th.tenant2.getId());
            final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
            assertTrue(personById.containsKey(personUserAdminTenant2.getId()));
            assertFalse(personById.containsKey(personUserAdminTenant1.getId()));
            final Person expectedPerson = personUserAdminTenant2;
            final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
            assertPersonsEqual(expectedPerson, actualPerson);
        }
    }

    @Test
    public void listPersonsByRole_userAdmin() throws Exception {

        final Person caller1 = personUserAdminTenant1;
        final ClientPersonExtendedPageBean pagedResponse1 = listPersonsByRoleNoTenantId(caller1, "SHOP_OWNER");
        final Map<String, ClientPersonExtended> personById1 = buildIdToPersonMap(pagedResponse1);
        assertTrue(personById1.containsKey(personShopOwnerTenant1.getId()));
        assertFalse(personById1.containsKey(personShopOwnerTenant2.getId()));

        final Person expectedPerson1 = personShopOwnerTenant1;
        final ClientPersonExtended actualPerson1 = personById1.get(expectedPerson1.getId());
        assertPersonsEqual(expectedPerson1, actualPerson1);

        final Person caller2 = personUserAdminTenant2;
        final ClientPersonExtendedPageBean pagedResponse2 = listPersonsByRoleNoTenantId(caller2, "SHOP_OWNER");
        final Map<String, ClientPersonExtended> personById2 = buildIdToPersonMap(pagedResponse2);
        assertTrue(personById2.containsKey(personShopOwnerTenant2.getId()));
        assertFalse(personById2.containsKey(personShopOwnerTenant1.getId()));

        final Person expectedPerson2 = personShopOwnerTenant2;
        final ClientPersonExtended actualPerson2 = personById2.get(expectedPerson2.getId());
        assertPersonsEqual(expectedPerson2, actualPerson2);
    }

    @Test
    public void listPersonsByRoleWithTenant_userAdmin() throws Exception {

        final Person caller1 = personUserAdminTenant1;
        final ClientPersonExtendedPageBean pagedResponse1 =
                listPersonsByRole(caller1, "SHOP_OWNER", caller1.getTenant().getId());
        final Map<String, ClientPersonExtended> personById1 = buildIdToPersonMap(pagedResponse1);
        assertTrue(personById1.containsKey(personShopOwnerTenant1.getId()));
        assertFalse(personById1.containsKey(personShopOwnerTenant2.getId()));

        final Person expectedPerson1 = personShopOwnerTenant1;
        final ClientPersonExtended actualPerson1 = personById1.get(expectedPerson1.getId());
        assertPersonsEqual(expectedPerson1, actualPerson1);

        final Person caller2 = personUserAdminTenant2;
        final ClientPersonExtendedPageBean pagedResponse2 =
                listPersonsByRole(caller2, "SHOP_OWNER", caller2.getTenant().getId());
        final Map<String, ClientPersonExtended> personById2 = buildIdToPersonMap(pagedResponse2);
        assertTrue(personById2.containsKey(personShopOwnerTenant2.getId()));
        assertFalse(personById2.containsKey(personShopOwnerTenant1.getId()));

        final Person expectedPerson2 = personShopOwnerTenant2;
        final ClientPersonExtended actualPerson2 = personById2.get(expectedPerson2.getId());
        assertPersonsEqual(expectedPerson2, actualPerson2);
    }

    @Test
    public void listPersonsByRoleWrongTenant_userAdmin() throws Exception {

        final Person caller = personUserAdminTenant1;
        final String wrongTenant = th.tenant2.getId();

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("page", "0")
                .param("sortBy", "firstNameAsc")
                .param("roleKey", "SHOP_OWNER")
                .param("tenantId", wrongTenant))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void listPersonsByRole_roleManager() throws Exception {

        final Person caller = th.personRoleManagerTenant1;
        final ClientPersonExtendedPageBean pagedResponse = listPersonsByRoleNoTenantId(caller, "ROLE_MANAGER");
        final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
        assertTrue(personById.containsKey(th.personRoleManagerTenant1.getId()));
        assertFalse(personById.containsKey(th.personRoleManagerTenant2.getId()));

        final Person expectedPerson = th.personRoleManagerTenant1;
        final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
        assertPersonsEqualLimitedAttributes(expectedPerson, actualPerson);
    }

    @Test
    public void listPersonsByRoleWithTenant_roleManager() throws Exception {

        final Person caller = th.personRoleManagerTenant1;
        final ClientPersonExtendedPageBean pagedResponse =
                listPersonsByRole(caller, "ROLE_MANAGER", th.tenant1.getId());
        final Map<String, ClientPersonExtended> personById = buildIdToPersonMap(pagedResponse);
        assertTrue(personById.containsKey(th.personRoleManagerTenant1.getId()));
        assertFalse(personById.containsKey(th.personRoleManagerTenant2.getId()));

        final Person expectedPerson = th.personRoleManagerTenant1;
        final ClientPersonExtended actualPerson = personById.get(expectedPerson.getId());
        assertPersonsEqualLimitedAttributes(expectedPerson, actualPerson);
    }

    @Test
    public void listPersonsByRoleWrongTenant_roleManager() throws Exception {

        final Person caller = th.personRoleManagerTenant2;
        final String wrongTenant = th.tenant1.getId();

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("page", "0")
                .param("sortBy", "firstNameAsc")
                .param("roleKey", "SHOP_OWNER")
                .param("tenantId", wrongTenant))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void listPersonsByRole_unauthorized() throws Exception {

        final Person caller = th.personRegularKarl;

        assertOAuth2(get("/adminui/personByRole")
                .param("page", "0")
                .param("sortBy", "firstNameAsc")
                .param("roleKey", "SHOP_OWNER"));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("roleKey", "SHOP_OWNER"))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void listPersonsByRole_wrongParameters() throws Exception {

        final Person caller = personGlobalUserAdmin;

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("page", "a")
                .param("roleKey", "SHOP_OWNER"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("count", "b")
                .param("roleKey", "SHOP_OWNER"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("sortColumn", "unavailable")
                .param("roleKey", "SHOP_OWNER"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("sortDirection", "DSC")
                .param("roleKey", "SHOP_OWNER"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("roleKey", "unknown-role"))
                .andExpect(isException(ClientExceptionType.ROLE_NOT_FOUND));

        mockMvc.perform(get("/adminui/personByRole")
                .headers(authHeadersFor(caller))
                .param("roleKey", "SHOP_OWNER")
                .param("tenantId", "unknown-tenant"))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    private ClientPersonExtendedPageBean listPersons(Person caller) throws Exception {

        return listPersons(caller, 0, 1000, "FIRST_NAME", "DESC", null);
    }

    private ClientPersonExtendedPageBean listPersons(Person caller, String search) throws Exception {

        return listPersons(caller, 0, 1000, "FIRST_NAME", "DESC", search);
    }

    @SuppressWarnings("SameParameterValue")
    private ClientPersonExtendedPageBean listPersons(Person caller, int page, int count, String sortColumn,
            String sortDirection, String search) throws Exception {

        MockHttpServletRequestBuilder builder = get("/adminui/person")
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count))
                .param("sortColumn", sortColumn)
                .param("sortDirection", sortDirection);
        if (search != null) {
            builder = builder.param("search", search);
        }

        final ClientPersonExtendedPageBean pagedResponse = toObject(mockMvc.perform(builder
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn()
                .getResponse(), ClientPersonExtendedPageBean.class);
        log.info("Persons in response: {}", pagedResponse.getContent()
                .stream()
                .map(p -> String.format("%s %s", p.getId(), p.getFirstName()))
                .collect(Collectors.joining("\n")));
        return pagedResponse;
    }

    private ClientPersonExtendedPageBean listPersonsByRoleNoTenantId(Person caller, String roleKey) throws Exception {

        return listPersonsByRole(caller, 0, 1000, "FIRST_NAME", "DESC", roleKey, null);
    }

    private ClientPersonExtendedPageBean listPersonsByRole(Person caller, String roleKey, String tenantId) throws Exception {

        return listPersonsByRole(caller, 0, 1000, "FIRST_NAME", "DESC", roleKey, tenantId);
    }

    @SuppressWarnings("SameParameterValue")
    private ClientPersonExtendedPageBean listPersonsByRole(Person caller, int page, int count, String sortColumn,
            String sortDirection, String roleKey, String tenantId) throws Exception {

        MockHttpServletRequestBuilder builder = get("/adminui/personByRole")
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count))
                .param("sortColumn", sortColumn)
                .param("sortDirection", sortDirection)
                .param("roleKey", roleKey);
        if (tenantId != null) {
            builder = builder.param("tenantId", tenantId);
        }

        final ClientPersonExtendedPageBean pagedResponse = toObject(mockMvc.perform(builder
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn()
                .getResponse(), ClientPersonExtendedPageBean.class);
        log.info("Persons in response: {}", pagedResponse.getContent()
                .stream()
                .map(p -> String.format("%s %s", p.getId(), p.getFirstName()))
                .collect(Collectors.joining("\n")));
        return pagedResponse;
    }

    private static Map<String, ClientPersonExtended> buildIdToPersonMap(ClientPersonExtendedPageBean pagedResponse) {
        return pagedResponse.getContent()
                .stream()
                .collect(Collectors.toMap(ClientPersonExtended::getId, Function.identity()));
    }

    private void assertPersonsEqual(Person expectedPerson, ClientPersonExtended actualPerson) {
        assertNotNull(actualPerson);
        assertEquals(expectedPerson.getId(), actualPerson.getId());
        assertEquals(expectedPerson.isDeleted(), actualPerson.isDeleted());
        assertVerificationStatusesEqual(expectedPerson.getVerificationStatuses().getValues(),
                actualPerson.getVerificationStatuses());
        assertStatusesEqual(expectedPerson.getStatuses().getValues(), actualPerson.getStatuses());
        if (expectedPerson.isDeleted()) {
            assertNull(actualPerson.getFirstName());
            assertNull(actualPerson.getLastName());
            assertNull(actualPerson.getEmail());
            assertNull(actualPerson.getProfilePicture());
            assertNull(actualPerson.getAddress());
            assertNull(actualPerson.getAccountType());
            assertEquals(ClientPersonStatus.DELETED, actualPerson.getStatus());
            assertNull(actualPerson.getLastLoggedIn());
            assertNull(actualPerson.getHomeAreaId());
            assertNull(actualPerson.getHomeAreaName());
            assertNull(actualPerson.getHomeAreaTenantId());
            assertNull(actualPerson.getHomeAreaTenantName());
            assertNull(actualPerson.getHomeCommunityId());
            assertNull(actualPerson.getHomeCommunityName());
            assertNull(actualPerson.getNickName());
            assertNull(actualPerson.getPhoneNumber());
        } else {
            assertEquals(expectedPerson.getFirstName(), actualPerson.getFirstName());
            assertEquals(expectedPerson.getLastName(), actualPerson.getLastName());
            assertEquals(expectedPerson.getEmail(), actualPerson.getEmail());
            assertEquals(expectedPerson.getProfilePicture().getId(), actualPerson.getProfilePicture().getId());
            assertAddressesEqual(th.personRepository.findAllAddressListEntriesByPerson(expectedPerson),
                    actualPerson.getAddress());
            assertEquals(expectedPerson.getAccountType(), actualPerson.getAccountType());
            assertEquals(ClientPersonStatus.REGISTERED, actualPerson.getStatus());
            assertEquals(expectedPerson.getLastLoggedIn(), actualPerson.getLastLoggedIn());
            assertEquals(expectedPerson.getHomeArea().getId(), actualPerson.getHomeAreaId());
            assertEquals(expectedPerson.getHomeArea().getName(), actualPerson.getHomeAreaName());
            assertEquals(expectedPerson.getHomeArea().getTenant().getId(), actualPerson.getHomeAreaTenantId());
            assertEquals(expectedPerson.getHomeArea().getTenant().getName(), actualPerson.getHomeAreaTenantName());
            assertEquals(expectedPerson.getTenant().getId(), actualPerson.getHomeCommunityId());
            assertEquals(expectedPerson.getTenant().getName(), actualPerson.getHomeCommunityName());
            assertEquals(expectedPerson.getNickName(), actualPerson.getNickName());
            assertEquals(expectedPerson.getPhoneNumber(), actualPerson.getPhoneNumber());
        }
    }

    private static void assertPersonsEqualLimitedAttributes(Person expectedPerson, ClientPersonExtended actualPerson) {
        assertNotNull(actualPerson);
        assertEquals(expectedPerson.getFirstName(), actualPerson.getFirstName());
        assertEquals(expectedPerson.getLastName(), actualPerson.getLastName());
        assertEquals(expectedPerson.getEmail(), actualPerson.getEmail());
        assertEquals(expectedPerson.getProfilePicture().getId(), actualPerson.getProfilePicture().getId());
        assertEquals(expectedPerson.isDeleted(), actualPerson.isDeleted());
        if (expectedPerson.isDeleted()) {
            assertEquals(ClientPersonStatus.DELETED, actualPerson.getStatus());
        } else {
            assertEquals(ClientPersonStatus.REGISTERED, actualPerson.getStatus());
        }
        assertNull(actualPerson.getAddress());
        assertNull(actualPerson.getAccountType());
        assertNull(actualPerson.getHomeAreaId());
        assertNull(actualPerson.getHomeAreaName());
        assertNull(actualPerson.getHomeAreaTenantId());
        assertNull(actualPerson.getHomeAreaTenantName());
        assertNull(actualPerson.getNickName());
        assertNull(actualPerson.getPhoneNumber());
        assertNull(actualPerson.getFilterStatus());
        assertNull(actualPerson.getCreated());
        assertNull(actualPerson.getLastLoggedIn());
        assertThat(actualPerson.getVerificationStatuses()).isNullOrEmpty();
        assertThat(actualPerson.getStatuses()).isNullOrEmpty();
    }

    private static void assertAddressesEqual(Collection<AddressListEntry> expectedAddresses,
            ClientAddress actualAddress) {
        if (expectedAddresses == null) {
            assertThat(actualAddress)
                    .as("actual address is null, expected is not null")
                    .isNull();
        } else {
            Optional<AddressListEntry> expectedAddressOpt = expectedAddresses.stream()
                    .filter(ale -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(ale.getName()))
                    .findFirst();
            if (expectedAddressOpt.isPresent()) {
                Address expectedAddress = expectedAddressOpt.get().getAddress();
                assertThat(actualAddress.getId()).isEqualTo(expectedAddress.getId());
                assertThat(actualAddress.getName()).isEqualTo(expectedAddress.getName());
                assertThat(actualAddress.getStreet()).isEqualTo(expectedAddress.getStreet());
                assertThat(actualAddress.getZip()).isEqualTo(expectedAddress.getZip());
                assertThat(actualAddress.getCity()).isEqualTo(expectedAddress.getCity());
                assertThat(actualAddress.getGpsLocation().getLatitude()).isEqualTo(
                        expectedAddress.getGpsLocation().getLatitude());
                assertThat(actualAddress.getGpsLocation().getLongitude()).isEqualTo(
                        expectedAddress.getGpsLocation().getLongitude());
                assertThat(actualAddress.isVerified()).isEqualTo(expectedAddress.isVerified());
            } else {
                assertThat(actualAddress)
                        .as("actual address is null, expected is not null")
                        .isNull();
            }
        }
    }

    private static void assertStatusesEqual(
            Collection<PersonStatus> expectedStatuses,
            Collection<ClientPersonStatusExtended> actualStatuses) {

        if (actualStatuses == null) {
            assertThat(expectedStatuses)
                    .as("actual status is null, expected is not empty")
                    .isEmpty();
        } else {
            Set<ClientPersonStatusExtended> expectedClientPersonStatuses =
                    toClientPersonStatuses(expectedStatuses);
            assertThat(actualStatuses).containsExactlyInAnyOrderElementsOf(
                    expectedClientPersonStatuses);
        }
    }

    private static void assertVerificationStatusesEqual(
            Collection<PersonVerificationStatus> expectedVerificationStatuses,
            Collection<ClientPersonVerificationStatus> actualVerificationStatuses) {

        if (actualVerificationStatuses == null) {
            assertThat(expectedVerificationStatuses)
                    .as("actual verification status is null, expected is not empty")
                    .isEmpty();
        } else {
            Set<ClientPersonVerificationStatus> expectedClientPersonVerificationStatuses =
                    toClientPersonVerificationStatuses(expectedVerificationStatuses);
            assertThat(actualVerificationStatuses).containsExactlyInAnyOrderElementsOf(
                    expectedClientPersonVerificationStatuses);
        }
    }

    private static Set<ClientPersonVerificationStatus> toClientPersonVerificationStatuses(
            Collection<PersonVerificationStatus> personVerificationStatuses) {

        Set<ClientPersonVerificationStatus> result = EnumSet.noneOf(ClientPersonVerificationStatus.class);

        for (PersonVerificationStatus personVerificationStatus : personVerificationStatuses) {
            switch (personVerificationStatus) {
                case PHONE_NUMBER_VERIFIED:
                    result.add(ClientPersonVerificationStatus.PHONE_NUMBER_VERIFIED);
                    break;
                case EMAIL_VERIFIED:
                    result.add(ClientPersonVerificationStatus.EMAIL_VERIFIED);
                    break;
                case NAME_VERIFIED:
                    result.add(ClientPersonVerificationStatus.NAME_VERIFIED);
                    break;
                case HOME_AREA_VERIFIED:
                    result.add(ClientPersonVerificationStatus.HOME_AREA_VERIFIED);
                    break;
                case ADDRESS_VERIFIED:
                    result.add(ClientPersonVerificationStatus.ADDRESS_VERIFIED);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + personVerificationStatus);
            }
        }
        return result;
    }

    private static Set<ClientPersonStatusExtended> toClientPersonStatuses(Collection<PersonStatus> personStatuses) {

        Set<ClientPersonStatusExtended> result = EnumSet.noneOf(ClientPersonStatusExtended.class);

        for (PersonStatus personStatus : personStatuses) {
            switch (personStatus) {
                case LOGIN_BLOCKED:
                    result.add(ClientPersonStatusExtended.LOGIN_BLOCKED);
                    break;
                case PENDING_DELETION:
                    result.add(ClientPersonStatusExtended.PENDING_DELETION);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + personStatus);
            }
        }
        return result;
    }

}
