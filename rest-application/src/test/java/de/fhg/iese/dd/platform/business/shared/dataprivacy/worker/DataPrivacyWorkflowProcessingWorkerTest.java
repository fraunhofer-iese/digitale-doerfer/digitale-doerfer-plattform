/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.worker;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;

public class DataPrivacyWorkflowProcessingWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private DataPrivacyWorkflowProcessingWorker dataPrivacyWorkflowProcessingWorker;

    @MockBean
    private ICommonDataPrivacyService commonDataPrivacyService;

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void processUnfinishedDataPrivacyWorkflows() {

        Mockito.when(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).thenReturn(42);
        dataPrivacyWorkflowProcessingWorker.run();
        Mockito.verify(commonDataPrivacyService).processAllUnfinishedDataPrivacyWorkflows();
    }

}
