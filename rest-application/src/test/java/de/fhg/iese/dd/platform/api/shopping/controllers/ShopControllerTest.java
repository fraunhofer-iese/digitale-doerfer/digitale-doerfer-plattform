/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ClientShop;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;

public class ShopControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createShops();
        th.createAppEntities();
        th.createAppVariantUsages();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAllShopsInGivenCommunity() throws Exception{

        List<Shop> expectedShops = th.shopList.stream()
                .filter(s -> s.getTenant().equals(th.tenant1))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                .collect(Collectors.toList());

        assertEquals(2, expectedShops.size());

        mockMvc.perform(get("/shop/")
                .param("communityId", th.tenant1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedShops.size())))
                .andExpect(jsonPath("$[0].id").value(expectedShops.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedShops.get(1).getId()));
    }

    @Test
    public void getAllShopsForSelectedGeoAreas() throws Exception{

        List<Shop> expectedShops = th.shopList.stream()
                .filter(s -> s.getTenant().equals(th.tenant1) || s.getTenant().equals(th.tenant2))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                .collect(Collectors.toList());

        assertEquals(4, expectedShops.size());

        mockMvc.perform(get("/shop/")
                        .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedShops.size())))
                .andExpect(jsonPath("$[0].id").value(expectedShops.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedShops.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(expectedShops.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(expectedShops.get(3).getId()));

        expectedShops = th.shopList.stream()
                .filter(s -> s.getTenant().equals(th.tenant1))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                .collect(Collectors.toList());

        assertEquals(2, expectedShops.size());

        mockMvc.perform(get("/shop/")
                        .headers(authHeadersFor(th.personVgAdmin, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedShops.size())))
                .andExpect(jsonPath("$[0].id").value(expectedShops.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedShops.get(1).getId()));

        expectedShops = th.shopList.stream()
                .filter(s -> s.getTenant().equals(th.tenant3))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated))
                .collect(Collectors.toList());

        assertEquals(1, expectedShops.size());

        mockMvc.perform(get("/shop/")
                        .headers(authHeadersFor(th.personRegularHorstiTenant3, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedShops.size())))
                .andExpect(jsonPath("$[0].id").value(expectedShops.get(0).getId()));
    }

    @Test
    public void getShopById() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/shop/" + th.shop1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.shop1.getId()))
                .andExpect(jsonPath("$.name").value(th.shop1.getName()))
                .andExpect(jsonPath("$.phone").value(th.shop1.getPhone()))
                .andExpect(jsonPath("$.email").value(th.shop1.getEmail()))
                .andExpect(jsonPath("$.webShopUrl").value(th.shop1.getWebShopURL()))
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        ClientShop clientShop = toObject(response, ClientShop.class);
        Address expectedAddress = th.shop1.getAddresses().iterator().next().getAddress();
        assertEquals(expectedAddress.getId(), clientShop.getAddress().getId());
        assertEquals(expectedAddress.getName(), clientShop.getAddress().getName());
        assertEquals(expectedAddress.getStreet(), clientShop.getAddress().getStreet());
        assertEquals(expectedAddress.getZip(), clientShop.getAddress().getZip());
        assertEquals(expectedAddress.getCity(), clientShop.getAddress().getCity());
        assertEquals(expectedAddress.getGpsLocation().getLatitude(),
                clientShop.getAddress().getGpsLocation().getLatitude());
        assertEquals(expectedAddress.getGpsLocation().getLongitude(),
                clientShop.getAddress().getGpsLocation().getLongitude());
        assertEquals(th.shop1.getProfilePicture().getId(), clientShop.getProfilePicture().getId());
        assertEquals(th.shop1.getOpeningHours().getId(), clientShop.getOpeningHours().getId());
    }

    @Test
    public void getAllShops_InvalidId_TenantNotFound() throws Exception {

        String invalidId = UUID.randomUUID().toString();
        mockMvc.perform(get("/shop/")
                .param("communityId", invalidId))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getAllShops_NoAppVariantUsage_TenantNotFound() throws Exception {

        mockMvc.perform(get("/shop/")
                .headers(authHeadersFor(th.personRegularKarl, th.app1Variant1))                )
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getAllShops_MissingTenantId_TenantNotFound() throws Exception {

        mockMvc.perform(get("/shop/"))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getShopById_NotFound() throws Exception {

        String invalidId = UUID.randomUUID().toString();
        mockMvc.perform(get("/shop/" + invalidId))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_FOUND));
    }

    @Test
    public void getShopOwners() throws Exception {

        Person shopOwner = th.personShopOwner;
        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;

        mockMvc.perform(get("/shop/{shopId}/owner", shop.getId())
                .headers(authHeadersFor(shopOwner))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(shopOwner.getId()));

        mockMvc.perform(get("/shop/{shopId}/owner", shop.getId())
                .headers(authHeadersFor(shopManager))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].id").value(shopOwner.getId()));
    }

    @Test
    public void getShopOwners_Unauthorized() throws Exception {

        mockMvc.perform(get("/shop/{shopId}/owner", th.shop3.getId())
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(get("/shop/{shopId}/owner", th.shop3.getId())
                .contentType(contentType));
    }

    @Test
    public void getShopOwners_ShopNotFound() throws Exception {

        mockMvc.perform(get("/shop/{shopId}/owner", BaseTestHelper.INVALID_UUID)
                .headers(authHeadersFor(th.personShopOwner))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_FOUND));
    }

}
