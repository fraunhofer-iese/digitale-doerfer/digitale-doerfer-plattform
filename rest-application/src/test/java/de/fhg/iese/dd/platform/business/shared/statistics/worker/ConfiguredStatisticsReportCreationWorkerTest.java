/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics.worker;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.test.mocks.TestTeamFileStorageService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.config.StatisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.StatisticsCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public class ConfiguredStatisticsReportCreationWorkerTest extends BaseServiceTest {

    private static final boolean SAVE_REPORTS = false;

    @Autowired
    private StatisticsConfig statisticsConfig;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestTeamFileStorageService testTeamFileStorageService;
    @Autowired
    private FeatureConfigRepository featureConfigRepository;

    @Autowired
    private ConfiguredStatisticsReportCreationWorker configuredStatisticsReportCreationWorker;

    private Tenant tenant1;
    private Tenant tenant2;
    private Tenant tenant3;

    @Override
    public void createEntities() {
        createTenantsAndGeoAreas();
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
    }

    public void createTenantsAndGeoAreas() {

        tenant1 = tenantRepository.save(Tenant.builder()
                .id("74dc2ee1-025e-4f5e-82bb-34b552e292e4")
                .name("VG 1")
                .build());

        tenant2 = tenantRepository.save(Tenant.builder()
                .id("c2c8af27-bedc-423a-a6ec-3e978180fd8f")
                .name("VG 2")
                .build());

        tenant3 = tenantRepository.save(Tenant.builder()
                .id("c2e6aa6c-2efe-4127-8009-152e2c47ed9c")
                .name("VG 3")
                .build());

        createGeoAreas();
    }

    private void createGeoAreas() {

        GeoArea geoAreaRheinlandPfalz = geoAreaRepository.save(GeoArea.builder()
                .id("76a2860b-a3ba-4299-955d-3859aedaa114")
                .name("Rheinland-Pfalz")
                .type(GeoAreaType.FEDERAL_STATE)
                .boundaryJson("[[[49.3,8.1], [49.3,8.4], [49.1,8.4], [49.1,8.1]]]")
                .center(new GPSLocation(49.2, 8.25))
                .build());

        GeoArea geoAreaTenant1 = geoAreaRepository.save(GeoArea.builder()
                .id("08964780-b3db-4988-b18f-3a2c643d8f1f")
                .tenant(tenant1)
                .name("Eisenbuckel")
                .type(GeoAreaType.ASSOCIATION_OF_MUNICIPALITIES)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson("[[[49.3,8.3], [49.3,8.4], [49.1,8.4], [49.1,8.3]]]")
                .center(new GPSLocation(49.2, 8.35))
                .build());
        tenant1.setRootArea(geoAreaTenant1);
        tenant1 = tenantRepository.save(tenant1);

        GeoArea geoAreaTenant2 = geoAreaRepository.save(GeoArea.builder()
                .id("b9b94a62-3a4e-4f74-baac-64492a9cf4d1")
                .tenant(tenant2)
                .name("Mainz")
                .type(GeoAreaType.CITY)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson("[[[49.3,8.2], [49.3,8.3], [49.1,8.3], [49.1,8.2]]]")
                .center(new GPSLocation(49.2, 8.25))
                .build());
        tenant2.setRootArea(geoAreaTenant2);
        tenant2 = tenantRepository.save(tenant2);

        GeoArea geoAreaTenant3 = geoAreaRepository.save(GeoArea.builder()
                .id("c22f2452-915d-4bb5-8328-15c450ca181f")
                .tenant(tenant3)
                .name("Geo Area 3")
                .type(GeoAreaType.CITY)
                .boundaryJson("[[[1,2]]]")
                .center(new GPSLocation(20.15, 18.35))
                .build());
        tenant3.setRootArea(geoAreaTenant3);
        tenant3 = tenantRepository.save(tenant3);
    }

    @Override
    public void tearDown() {
        th.deleteAllData();
    }

    @Test
    public void generateReports() throws IOException {

        String customReportPath1 = "my-folder-rlp";
        String customReportPath2 = "my-folder-mz";

        List<StatisticsMetaData> statisticsMetaData = th.getAllStatisticsMetaData();

        List<String> statisticsIds = statisticsMetaData.stream()
                .map(StatisticsMetaData::getId)
                .collect(Collectors.toList());

        String statisticsIdArray = statisticsIds.stream()
                .map(id -> "\"" + id + "\"")
                .collect(Collectors.joining(",", "[", "]"));
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(StatisticsCreationFeature.class.getName())
                .geoAreasIncluded(Collections.singleton(th.geoAreaRheinlandPfalz))
                .enabled(true)
                .configValues("{  " +
                        "\"creationConfigurations\": [\n" +
                        "    {\n" +
                        "      \"teamFileStoragePath\": \"" + customReportPath1 + "\",\n" +
                        "      \"csvReport\": true,\n" +
                        "      \"csvSeparator\": \";\",\n" +
                        "      \"frequencies\": [\n" +
                        "        \"DAYS\"\n" +
                        "      ],\n" +
                        "      \"statisticsIds\": " + statisticsIdArray + "\n" +
                        "    }" +
                        "  ]\n" +
                        "}")
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(StatisticsCreationFeature.class.getName())
                .geoAreasIncluded(Collections.singleton(th.geoAreaMainz))
                .enabled(true)
                .configValues("{  " +
                        "\"creationConfigurations\": [\n" +
                        "    {\n" +
                        "      \"teamFileStoragePath\": \"" + customReportPath2 + "\",\n" +
                        "      \"reportNamePrefix\": \"bericht\",\n" +
                        "      \"csvReport\": true,\n" +
                        "      \"csvSeparator\": \";\",\n" +
                        "      \"frequencies\": [\n" +
                        "        \"DAYS\"\n" +
                        "      ],\n" +
                        "      \"statisticsIds\": " + statisticsIdArray + "\n" +
                        "    }" +
                        "  ]\n" +
                        "}")
                .build());

        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-10-04T00:01:34+02:00[Europe/Berlin]"));

        //we want all persons to not have logged in recently, so that we can control it
        th.personRepository.findAll().stream()
                .peek(p -> p.setLastLoggedIn(0))
                .forEach(th.personRepository::save);

        th.personRegularAnna.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC());
        th.personRepository.save(th.personRegularAnna);

        th.personIeseAdmin.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(8));
        th.personRepository.save(th.personIeseAdmin);

        th.personRegularKarl.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(3));
        th.personRegularKarl.setHomeArea(th.geoAreaDorf1InEisenberg);
        th.personRepository.save(th.personRegularKarl);

        configuredStatisticsReportCreationWorker.run();

        String expectedFileNameReport1 =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath1 + "/report_2019-10-03.csv";
        String expectedFileNameReport1Metadata =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath1 +
                        "/report_2019-10-03_readme.csv";
        String expectedFileNameReport2 =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath2 + "/bericht_2019-10-03.csv";
        String expectedFileNameReport2Metadata =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath2 +
                        "/bericht_2019-10-03_readme.csv";

        assertStatisticsReportIsCreatedGeoAreaSpecific(expectedFileNameReport1,
                List.of(th.geoAreaRheinlandPfalz, th.geoAreaEisenberg),
                List.of(th.geoAreaDeutschland));
        assertStatisticsReportMetadataIsCreated(expectedFileNameReport1Metadata);
        assertStatisticsReportIsCreatedGeoAreaSpecific(expectedFileNameReport2,
                List.of(th.geoAreaMainz),
                List.of(th.geoAreaDeutschland, th.geoAreaRheinlandPfalz));
        assertStatisticsReportMetadataIsCreated(expectedFileNameReport2Metadata);

        long lastModifiedFirstRunReport1 =
                testTeamFileStorageService.getFileAttributes(expectedFileNameReport1).getLastModified();
        long lastModifiedFirstRunReport2 =
                testTeamFileStorageService.getFileAttributes(expectedFileNameReport2).getLastModified();

        //modify the time stamp minimally
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-10-04T00:01:35+02:00[Europe/Berlin]"));

        // the reports should not be re-created since they already are there
        configuredStatisticsReportCreationWorker.run();

        assertThat(lastModifiedFirstRunReport1)
                .isEqualTo(testTeamFileStorageService.getFileAttributes(expectedFileNameReport1).getLastModified());
        assertThat(lastModifiedFirstRunReport2)
                .isEqualTo(testTeamFileStorageService.getFileAttributes(expectedFileNameReport2).getLastModified());

        if (SAVE_REPORTS) {
            testTeamFileStorageService.saveFilesToTemp(getClass().getSimpleName());
        }
    }

    @Test
    public void generateReports_EndTime() {

        String customReportPath = "my-folder-rlp";

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(StatisticsCreationFeature.class.getName())
                .geoAreasIncluded(Collections.singleton(th.geoAreaRheinlandPfalz))
                .enabled(true)
                .configValues("{  " +
                        "\"creationConfigurations\": [\n" +
                        "    {\n" +
                        "      \"teamFileStoragePath\": \"" + customReportPath + "\",\n" +
                        "      \"csvReport\": true,\n" +
                        "      \"csvSeparator\": \";\",\n" +
                        "      \"frequencies\": [\n" +
                        "        \"MONTHS\"\n" +
                        "      ],\n" +
                        "      \"statisticsIds\": [\n" +
                        "        \"participants.person.undeleted\",\n" +
                        "        \"participants.person.created\",\n" +
                        "        \"participants.person.deleted\",\n" +
                        "        \"participants.person.active\"\n" +
                        "      ]\n" +
                        "    }" +
                        "  ]\n" +
                        "}")
                .build());

        String expectedFileNameReport =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath + "/report_2019-10.csv";
        String expectedFileNameReportMetadata =
                statisticsConfig.getTeamFileStorageFolder() + "/" + customReportPath +
                        "/report_2019-10_readme.csv";

        // monthly reports should not be created unless it is the first day of the next month
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-11-15T00:01:34+01:00[Europe/Berlin]"));

        configuredStatisticsReportCreationWorker.run();

        assertFalse(testTeamFileStorageService.fileExists(expectedFileNameReport));
        assertFalse(testTeamFileStorageService.fileExists(expectedFileNameReportMetadata));

        //create report at the beginning of a month
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-11-01T00:01:35+01:00[Europe/Berlin]"));

        configuredStatisticsReportCreationWorker.run();

        assertTrue(testTeamFileStorageService.fileExists(expectedFileNameReport));
        assertTrue(testTeamFileStorageService.fileExists(expectedFileNameReportMetadata));

    }

    private void assertStatisticsReportIsCreatedGeoAreaSpecific(String expectedReport,
            Collection<GeoArea> expectedGeoAreas, Collection<GeoArea> unExpectedGeoAreas) {
        TestTeamFileStorageService.FileAttributes fileAttributes = testTeamFileStorageService
                .getFileAttributes(expectedReport);
        assertNotNull(fileAttributes, "Report " + expectedReport + " is not existing");
        assertEquals("text/csv", fileAttributes.getMimeType());
        String reportText = new String(fileAttributes.getContent(), StandardCharsets.UTF_8);

        for (GeoArea expectedGeoArea : expectedGeoAreas) {
            assertThat(reportText).contains(expectedGeoArea.getName());
            assertThat(reportText).contains(expectedGeoArea.getId());
        }
        for (GeoArea unExpectedGeoArea : unExpectedGeoAreas) {
            assertThat(reportText).doesNotContain(unExpectedGeoArea.getName());
            assertThat(reportText).doesNotContain(unExpectedGeoArea.getId());
        }
    }

    private void assertStatisticsReportMetadataIsCreated(String expectedReport) {
        TestTeamFileStorageService.FileAttributes fileAttributes = testTeamFileStorageService
                .getFileAttributes(expectedReport);
        assertNotNull(fileAttributes, "Report " + expectedReport + " is not existing");
        assertEquals("text/csv", fileAttributes.getMimeType());
        String reportText = new String(fileAttributes.getContent(), StandardCharsets.UTF_8);

        assertThat(reportText).contains("statistics-description");
    }

}
