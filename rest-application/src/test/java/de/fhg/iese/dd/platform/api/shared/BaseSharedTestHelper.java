/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Alberto Lara, Tahmid Ekram, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.PoolingstationOperator;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos.PersonBlockingRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.OpeningHoursRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.ShopRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.SharedConstants;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantTenantContractRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageAreaSelectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantVersionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.enums.LegalTextType;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextAcceptanceRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalConfigurationAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.repos.UserDataCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.repos.UserDataKeyValueEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.roles.GlobalUserGeneratedContentAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.roles.UserGeneratedContentAdmin;

/**
 * Intended as a base class for test helpers of modules that depend on shared
 */
public abstract class BaseSharedTestHelper extends BaseTestHelper {

    public static final String UPDATE_URL_ANDROID =
            "https://play.google.com/store/apps/details?id=de.fhg.iese.dd.dorffunk.android";
    public static final String UPDATE_URL_IOS = "https://itunes.apple.com/de/app/dorffunk/id1348748008";
    public static final String UPDATE_URL_GENERIC = "https://www.digitale-doerfer.de/";

    @Autowired
    public AppRepository appRepository;
    @Autowired
    public AppVariantRepository appVariantRepository;
    @Autowired
    public AppVariantGeoAreaMappingRepository appVariantGeoAreaMappingRepository;
    @Autowired
    public AppVariantTenantContractRepository appVariantTenantContractRepository;
    @Autowired
    public OauthClientRepository oauthClientRepository;
    @Autowired
    public AppVariantVersionRepository appVariantVersionRepository;
    @Autowired
    public AppVariantUsageRepository appVariantUsageRepository;
    @Autowired
    public AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;

    @Autowired
    public PushCategoryRepository pushCategoryRepository;
    @Autowired
    public PushCategoryUserSettingRepository pushCategoryUserSettingRepository;

    @Autowired
    public UserDataCategoryRepository userDataCategoryRepository;
    @Autowired
    public UserDataKeyValueEntryRepository userDataKeyValueEntryRepository;

    @Autowired
    public OpeningHoursRepository openingHoursRepository;
    @Autowired
    public OpeningHoursEntryRepository openingHoursEntryRepository;
    @Autowired
    public ShopRepository shopRepository;
    @Autowired
    private PoolingStationRepository poolingStationRepository;

    @Autowired
    public LegalTextRepository legalTextRepository;
    @Autowired
    public LegalTextAcceptanceRepository legalTextAcceptanceRepository;

    @Autowired
    public GeoAreaRepository geoAreaRepository;

    @Autowired
    public PersonBlockingRepository personBlockingRepository;

    public App app1;
    public App app2;
    public App appPlatform;
    public App appInvalid;

    public OauthClient app1Variant1OauthClient;
    public AppVariant app1Variant1;
    //two oauth clients for the same app variant
    public OauthClient app1Variant2OauthClient1;
    public OauthClient app1Variant2OauthClient2;
    public AppVariant app1Variant2;

    //used by both app variants
    public OauthClient app2OauthClient;
    public AppVariant app2Variant1;
    public AppVariant app2Variant2;
    public AppVariant appVariantPlatform;

    public AppVariant appVariantInvalidVersion;

    public AppVariantVersion app1Variant1Version1Android;
    public AppVariantVersion app1Variant1Version1Ios;

    public AppVariantVersion app1Variant2Version1Android;
    public AppVariantVersion app1Variant2Version1Ios;
    public AppVariantVersion app1Variant2Version2Android;
    public AppVariantVersion app1Variant2Version2Ios;

    public AppVariantVersion app2Variant1Version1Android;
    public AppVariantVersion app2Variant1Version1Ios;
    public AppVariantVersion app2Variant2Version1Android;
    public AppVariantVersion app2Variant2Version1Ios;

    public AppVariantVersion appInvalidVersion;
    public AppVariantVersion appVersionPlatform;

    public LegalText legalTextPlatformRequired;
    public LegalText legalTextPlatformOptional;
    public LegalText legalText1App1Variant1Required;
    public LegalText legalText2App1Variant1Optional;
    public LegalText legalText3App1Variant1Expired;
    public LegalText legalText4App1Variant1Upcoming;

    public PushCategory pushCategory1App1;
    public PushCategory pushCategory2App1;
    public PushCategory pushCategory3App1;

    public List<PushCategory> pushCategoriesApp1;

    public PushCategory pushCategory1App2;

    public List<UserDataCategory> userDataCategoryList;
    public List<UserDataKeyValueEntry> userDataKeyValueEntryList;

    public UserDataCategory userDataCategory1;
    public UserDataCategory userDataCategory2;
    public UserDataCategory userDataCategory3;
    public UserDataCategory userDataCategory4;
    public UserDataCategory userDataCategory5;
    public UserDataCategory userDataCategory6;

    public UserDataKeyValueEntry userDataKeyValueEntry1Cat1;
    public UserDataKeyValueEntry userDataKeyValueEntry2Cat1;
    public UserDataKeyValueEntry userDataKeyValueEntry3Cat1;
    public UserDataKeyValueEntry userDataKeyValueEntry4Cat2;
    public UserDataKeyValueEntry userDataKeyValueEntry5Cat2;
    public UserDataKeyValueEntry userDataKeyValueEntry6Cat2;

    public Person personDeleted;

    public Shop shop1;
    public Shop shop2;

    public PersonBlocking annaBlocksKarlInApp1;
    public PersonBlocking annaBlocksDeletedInApp1;
    public PersonBlocking annaBlocksContactPersonCommunity1InApp2;

    public Person personGlobalUserGeneratedContentAdmin;
    public Person personUserGeneratedContentAdminTenant1;
    public Person personUserGeneratedContentAdminTenant2;

    public Person personGlobalConfigurationAdmin;

    public void createAppEntities() {

        app1 = appRepository.save(App.builder()
                .id(DorfFunkConstants.APP_ID)
                .name("App1")
                .appIdentifier("app1_appIdentifier")
                .logo(createMediaItem("app1-logo"))
                .build());

        app2 = appRepository.save(App.builder()
                .name("App2")
                .appIdentifier("app2_appIdentifier")
                .logo(createMediaItem("app2-logo"))
                .build());

        appPlatform = appRepository.save(App.builder()
                .id(SharedConstants.PLATFORM_APP_ID)
                .name("AppPlatform")
                .appIdentifier("appPlatform_appIdentifier")
                .logo(createMediaItem("appPlatform-logo"))
                .build());

        appInvalid = appRepository.save(App.builder()
                .name("AppInvalid")
                .appIdentifier("appInvalid_appIdentifier")
                .build());

        appRepository.flush();

        app1Variant1OauthClient = oauthClientRepository.save(OauthClient.builder()
                .name("App1 Variant1 OauthClient")
                .oauthClientIdentifier("app1Variant1OauthClient-identifier")
                .build());
        app1Variant1 = appVariantRepository.save(AppVariant.builder()
                .name("App1Variant1")
                .appVariantIdentifier("app1.appVariant1.appVariantIdentifier")
                .oauthClients(Collections.singleton(app1Variant1OauthClient))
                .logo(createMediaItem("app1Variant1-logo"))
                .app(app1)
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .externalApiKey(UUID.randomUUID().toString())
                .externalBasicAuth("username:password")
                .updateUrlAndroid(UPDATE_URL_ANDROID)
                .updateUrlIOS(UPDATE_URL_IOS)
                .build());
        mapGeoAreaToAppVariant(app1Variant1, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(app1Variant1, geoAreaKaiserslautern, tenant1);

        app1Variant2OauthClient1 = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("app1Variant2OauthClient1-identifier")
                .build());
        app1Variant2OauthClient2 = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("app1Variant2OauthClient2-identifier")
                .build());
        app1Variant2 = appVariantRepository.save(AppVariant.builder()
                .name("App1Variant2")
                .appVariantIdentifier("app1.appVariant2.appVariantIdentifier")
                .oauthClients(new HashSet<>(Arrays.asList(app1Variant2OauthClient1, app1Variant2OauthClient2)))
                .logo(createMediaItem("appVariant2-logo"))
                .app(app1)
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .externalApiKey(UUID.randomUUID().toString())
                .build());
        mapGeoAreaToAppVariant(app1Variant2, geoAreaTenant1, tenant1);

        app2OauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("app2OauthClient1-identifier")
                .build());
        app2Variant1 = appVariantRepository.save(AppVariant.builder()
                .name("App2Variant1")
                .appVariantIdentifier("app2.appVariant1.appVariantIdentifier")
                .oauthClients(Collections.singleton(app2OauthClient))
                .logo(createMediaItem("app2Variant1-logo"))
                .app(app2)
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .externalApiKey(UUID.randomUUID().toString())
                .build());
        mapGeoAreaToAppVariant(app2Variant1, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(app2Variant1, geoAreaKaiserslautern, tenant1);
        mapGeoAreaToAppVariant(app2Variant1, geoAreaTenant2, tenant2);

        app2Variant2 = appVariantRepository.save(AppVariant.builder()
                .name("App2Variant2")
                .appVariantIdentifier("app2.appVariant2.appVariantIdentifier")
                .oauthClients(Collections.singleton(app2OauthClient))
                .logo(createMediaItem("app2Variant2-logo"))
                .app(app2)
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .externalApiKey(UUID.randomUUID().toString())
                .build());
        mapGeoAreaToAppVariant(app2Variant2, geoAreaTenant3, tenant3);

        appVariantInvalidVersion = appVariantRepository.save(AppVariant.builder()
                .name("AppVariantInvalid")
                .appVariantIdentifier("AppVariantInvalid.appVariantIdentifier")
                .oauthClients(Collections.emptySet())
                .app(appInvalid)
                .build());
        mapGeoAreaToAppVariant(appVariantInvalidVersion, geoAreaTenant3, tenant3);

        appVariantPlatform = AppVariant.builder()
                .name("AppVariantPlatform")
                .appVariantIdentifier(SharedConstants.PLATFORM_APP_VARIANT_IDENTIFIER)
                .oauthClients(Collections.emptySet())
                .app(appPlatform)
                .build();
        appVariantPlatform.setId(SharedConstants.PLATFORM_APP_VARIANT_ID);
        appVariantPlatform = appVariantRepository.save(appVariantPlatform);

        oauthClientRepository.flush();
        appVariantRepository.flush();
        appVariantGeoAreaMappingRepository.flush();

        app1Variant1Version1Android = appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(app1Variant1)
                .versionIdentifier("1.1")
                .platform(AppPlatform.ANDROID)
                .supported(false)
                .build());

        app1Variant1Version1Ios = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app1Variant1)
            .versionIdentifier("1.1")
            .platform(AppPlatform.IOS)
            .supported(false)
            .build());

        app1Variant2Version1Android = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app1Variant2)
            .versionIdentifier("1.0")
            .platform(AppPlatform.ANDROID)
            .supported(true)
            .build());

        app1Variant2Version1Ios = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app1Variant2)
            .versionIdentifier("1.0")
            .platform(AppPlatform.IOS)
            .supported(true)
            .build());

        app1Variant2Version2Android = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app1Variant2)
            .versionIdentifier("2.0")
            .platform(AppPlatform.ANDROID)
            .supported(true)
            .build());

        app1Variant2Version2Ios = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app1Variant2)
            .versionIdentifier("2.0")
            .platform(AppPlatform.IOS)
            .supported(true)
            .build());

        app2Variant1Version1Android = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app2Variant1)
            .versionIdentifier("1.0")
            .platform(AppPlatform.ANDROID)
            .supported(true)
            .build());

        app2Variant1Version1Ios = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app2Variant1)
            .versionIdentifier("1.0")
            .platform(AppPlatform.IOS)
            .supported(true)
            .build());

        app2Variant2Version1Android = appVariantVersionRepository.save(AppVariantVersion.builder()
            .appVariant(app2Variant2)
            .versionIdentifier("4.0")
            .platform(AppPlatform.ANDROID)
            .supported(true)
            .build());

        app2Variant2Version1Ios = appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(app2Variant2)
                .versionIdentifier("4.0")
                .platform(AppPlatform.IOS)
                .supported(true)
                .build());

        appInvalidVersion = appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(appVariantInvalidVersion)
                .versionIdentifier("not major minor") //not following major.minor.bugfix naming
                .platform(AppPlatform.IOS)
                .supported(true)
                .build());

        appVersionPlatform = appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(appVariantPlatform)
                .versionIdentifier("1.0")
                .platform(AppPlatform.WEB)
                .supported(true)
                .build());

        appVariantVersionRepository.flush();
    }

    public void unmapAllGeoAreasFromAppVariant(AppVariant appVariant) {
        appVariantGeoAreaMappingRepository.deleteByAppVariant(appVariant);
    }

    public void mapGeoAreaToAppVariant(AppVariant appVariant, GeoArea geoArea, Tenant tenant) {
        appVariantGeoAreaMappingRepository.save(AppVariantGeoAreaMapping.builder()
                .geoArea(geoArea)
                .contract(createContract(appVariant, tenant))
                .build()
                .withConstantId());
    }

    public void excludeGeoAreaFromAppVariant(AppVariant appVariant, GeoArea geoArea, Tenant tenant) {
        appVariantGeoAreaMappingRepository.save(AppVariantGeoAreaMapping.builder()
                .geoArea(geoArea)
                .contract(createContract(appVariant, tenant))
                .excluded(true)
                .build()
                .withConstantId());
    }

    @NotNull
    private AppVariantTenantContract createContract(AppVariant appVariant, Tenant tenant) {
        return appVariantTenantContractRepository.save(AppVariantTenantContract.builder()
                .appVariant(appVariant)
                .tenant(tenant)
                .notes("Autogenerated for " + tenant.getName() + " -> " + appVariant.getName())
                .build()
                //this results in the same id for the same app variant and tenant, so there will be no duplicates
                .withAutogeneratedId());
    }

    public void createPushEntities() {

        assertNotNull(app1);
        assertNotNull(app2);

        pushCategory1App1 = PushCategory.builder()
                .app(app1)
                .name("pushCategory1App1")
                .description("pushCategory1App1 description")
                .orderValue(10)
                .defaultLoudPushEnabled(true)
                .configurable(true)
                .build();
        pushCategory1App1.setCreated(nextTimeStamp());
        pushCategory1App1 = pushCategoryRepository.save(pushCategory1App1);

        pushCategory2App1 = PushCategory.builder()
                .app(app1)
                .name("pushCategory_app1_2")
                .description("pushCategory_app1_2 description")
                .orderValue(11)
                .defaultLoudPushEnabled(false)
                .configurable(false)
                .build();
        pushCategory2App1.setCreated(nextTimeStamp());
        pushCategory2App1 = pushCategoryRepository.save(pushCategory2App1);

        pushCategory3App1 = PushCategory.builder()
                .app(app1)
                .name("pushCategory3App1")
                .description("pushCategory3App1 description")
                .orderValue(12)
                .defaultLoudPushEnabled(true)
                .configurable(true)
                .build();
        pushCategory3App1.setCreated(nextTimeStamp());
        pushCategory3App1 = pushCategoryRepository.save(pushCategory3App1);

        pushCategoriesApp1 = new ArrayList<>();
        pushCategoriesApp1.add(pushCategory1App1);
        pushCategoriesApp1.add(pushCategory2App1);
        pushCategoriesApp1.add(pushCategory3App1);

        pushCategory1App2 = PushCategory.builder()
                .app(app2)
                .name("pushCategory1App2")
                .description("pushCategory1App2 description")
                .orderValue(13)
                .defaultLoudPushEnabled(true)
                .configurable(true)
                .build();
        pushCategory1App2.setCreated(nextTimeStamp());
        pushCategory1App2 = pushCategoryRepository.save(pushCategory1App2);

        pushCategoryRepository.flush();
    }

    public void createUserDataEntities(Person person){
        createUserDataCategories(person);
        createUserDataKeyValueEntries();
    }

    private void createUserDataCategories(Person person) {

        userDataCategoryList = new ArrayList<>();

        userDataCategory1 = new UserDataCategory(nextTimeStamp(), "Category 1", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory1));

        userDataCategory2 = new UserDataCategory(nextTimeStamp(), "Category 2", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory2));

        userDataCategory3 = new UserDataCategory(nextTimeStamp(), "Category 3", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory3));

        userDataCategory4 = new UserDataCategory(nextTimeStamp(), "Category 4", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory4));

        userDataCategory5 = new UserDataCategory(nextTimeStamp(), "Category 5", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory5));

        userDataCategory6 = new UserDataCategory(nextTimeStamp(), "Category 6", person);
        userDataCategoryList.add(userDataCategoryRepository.save(userDataCategory6));
    }

    private void createUserDataKeyValueEntries() {

        userDataKeyValueEntryList = new ArrayList<>();

        userDataKeyValueEntry1Cat1 = new UserDataKeyValueEntry(nextTimeStamp(), "1", "value1", userDataCategory1);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry1Cat1));

        userDataKeyValueEntry2Cat1 = new UserDataKeyValueEntry(nextTimeStamp(), "2", "value2", userDataCategory1);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry2Cat1));

        userDataKeyValueEntry3Cat1 = new UserDataKeyValueEntry(nextTimeStamp(), "3", "value3", userDataCategory1);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry3Cat1));

        userDataKeyValueEntry4Cat2 = new UserDataKeyValueEntry(nextTimeStamp(), "4", "value4", userDataCategory2);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry4Cat2));

        userDataKeyValueEntry5Cat2 = new UserDataKeyValueEntry(nextTimeStamp(), "5", "value5", userDataCategory2);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry5Cat2));

        userDataKeyValueEntry6Cat2 = new UserDataKeyValueEntry(nextTimeStamp(), "6", "value6", userDataCategory2);
        userDataKeyValueEntryList.add(userDataKeyValueEntryRepository.save(userDataKeyValueEntry6Cat2));
    }

    @Override
    public void createPersons() {

        super.createPersons();
        createPersonDeleted();
        createPersonGlobalConfigurationAdmin();
    }

    private void createPersonDeleted() {

        personDeleted = personRepository.save(Person.builder()
                .id("0d26ae30-8ac3-41f4-b940-39d53e271276")
                .created(0)
                .deleted(true)
                .oauthId("auth0|5dc2a5103fbe500e58bbea11")
                .lastLoggedIn(0)
                .firstName(" ")
                .lastName(" ")
                .nickName(null)
                .phoneNumber(null)
                .profilePicture(null)
                .email("00000000-8ac3-41f4-b940-39d53e271276")
                .accountType(AccountType.OAUTH)
                .addresses(null)
                .community(tenant1)
                .homeArea(geoAreaTenant1)
                .build());
    }

    private void createPersonGlobalConfigurationAdmin() {

        personGlobalConfigurationAdmin =
                createPersonWithDefaultAddress("personGlobalConfigurationAdmin", tenant3,
                        "6357822b-376c-40fa-8576-341c1e70fdea");
        assignRoleToPerson(personGlobalConfigurationAdmin, GlobalConfigurationAdmin.class, null);
    }

    protected OpeningHours createOpeningHours(OpeningHoursEntry... openingHoursEntries) {
        return openingHoursRepository.save(
                new OpeningHours(
                        new HashSet<>(openingHoursEntryRepository.saveAll(Arrays.asList(openingHoursEntries)))));
    }

    public void createShops() {
        shop1 = createShop1(nextTimeStamp());
        shop2 = createShop2(nextTimeStamp());
    }

    protected Shop createShop1(long created) {

        OpeningHours openingHours = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        Shop shop1 = Shop.builder()
                .name("Test Shop1")
                .webShopURL("https://testshop1.example.org")
                .profilePicture(createMediaItem("shop-ikone"))
                .phone("1234")
                .smsNotificationNumber("0815")
                .email("shop1@exmaple")
                .openingHours(openingHours)
                .community(tenant1)
                .owner(personShopOwner)
                .addresses(createAddressSet(
                        Address.builder()
                                .name("Test Shop1")
                                .street("Fraunhofer-Platz 1")
                                .zip("67663")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.431605, 7.752773)).build()))
                .build();
        shop1.setCreated(created);
        shop1 = shopRepository.saveAndFlush(shop1);
        assignRoleToPerson(personShopOwner, ShopOwner.class, shop1.getId());
        return shop1;
    }

    protected Shop createShop2(long created) {

        OpeningHours openingHours = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        Shop shop2 = Shop.builder()
                .name("Test Shop2")
                .webShopURL("https://testshop2.example.org")
                .profilePicture(createMediaItem("shop-ikone"))
                .phone("4321")
                .smsNotificationNumber("1508")
                .email("shop2@exmaple")
                .openingHours(openingHours)
                .community(tenant2)
                .owner(personShopOwner2)
                .addresses(createAddressSet(
                        Address.builder()
                                .name("Test Shop2")
                                .street("Fraunhofer-Platz 1")
                                .zip("67663")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.431605, 7.752773)).build()))
                .build();
        shop2.setCreated(created);
        shop2 = shopRepository.saveAndFlush(shop2);
        assignRoleToPerson(personShopOwner2, ShopOwner.class, shop2.getId());
        return shop2;
    }

    public void createPoolingStations() {

        OpeningHours openingHoursPoolingStation0 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation0 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station Bahnhof")
                                .street("Zollamtstraße 6")
                                .zip(
                                        "67663")
                                .city("Kaiserslautern").gpsLocation(
                                new GPSLocation(49.435450, 7.769099)).build()),
                "Q-Station Bahnhof", "42",
                mediaItemRepository.save(createMediaItem("imagePoolingStation1")),
                tenant2, openingHoursPoolingStation0, PoolingStationType.AUTOMATIC, true, true, false, true);
        poolingStation0.setCreated(nextTimeStamp());
        poolingStation0 = poolingStationRepository.save(poolingStation0);
        assignRoleToPerson(personPoolingStationOp, PoolingstationOperator.class, poolingStation0.getId());
    }

    public void createLegalEntities(){

        legalTextPlatformRequired = legalTextRepository.save(LegalText.builder()
                .appVariant(null)
                .legalTextIdentifier("legalTextPlatformRequired")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.TERMS_AND_CONDITIONS)
                .orderValue(0)
                .required(true)
                .dateActive(toMillisUTC("2018-05-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2042-02-12T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());

        legalTextPlatformOptional = legalTextRepository.save(LegalText.builder()
                .appVariant(null)
                .legalTextIdentifier("legalTextPlatformOptional")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.IMPRINT)
                .orderValue(1)
                .required(false)
                .dateActive(toMillisUTC("2018-05-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2042-02-12T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());

        legalText1App1Variant1Required = legalTextRepository.save(LegalText.builder()
                .appVariant(app1Variant1)
                .legalTextIdentifier("legalText1App1Variant1Required")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.DATA_PRIVACY_STATEMENT)
                .orderValue(10)
                .required(true)
                .dateActive(toMillisUTC("2018-05-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2042-02-12T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());

        legalText2App1Variant1Optional = legalTextRepository.save(LegalText.builder()
                .appVariant(app1Variant1)
                .legalTextIdentifier("legalText2App1Variant1Optional")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.IMPRINT)
                .orderValue(11)
                .required(false)
                .dateActive(toMillisUTC("2018-04-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2042-02-12T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());

        legalText3App1Variant1Expired = legalTextRepository.save(LegalText.builder()
                .appVariant(app1Variant1)
                .legalTextIdentifier("legalText3App1Variant1Expired")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.TERMS_AND_CONDITIONS)
                .orderValue(12)
                .required(true)
                .dateActive(toMillisUTC("2018-04-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2018-05-08T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());

        legalText4App1Variant1Upcoming = legalTextRepository.save(LegalText.builder()
                .appVariant(app1Variant1)
                .legalTextIdentifier("legalText4App1Variant1Upcoming")
                .name(UUID.randomUUID().toString())
                .legalTextType(LegalTextType.TERMS_AND_CONDITIONS)
                .orderValue(12)
                .required(true)
                .dateActive(toMillisUTC("2025-04-01T00:00:00Z"))
                .dateInactive(toMillisUTC("2025-05-08T00:00:00Z"))
                .urlText("https://example.org/" + UUID.randomUUID())
                .build()
                .withConstantId());
    }

    private long toMillisUTC(String isoDateTime){
        return Instant.from(DateTimeFormatter.ISO_DATE_TIME.parse(isoDateTime)).toEpochMilli();
    }

    public void createPersonBlockings() {
        annaBlocksKarlInApp1 = personBlockingRepository.saveAndFlush(PersonBlocking.builder()
                .app(app1)
                .blockingPerson(personRegularAnna)
                .blockedPerson(personRegularKarl)
                .build());
        annaBlocksDeletedInApp1 = personBlockingRepository.saveAndFlush(PersonBlocking.builder()
                .app(app1)
                .blockingPerson(personRegularAnna)
                .blockedPerson(personDeleted)
                .build());
        annaBlocksContactPersonCommunity1InApp2 = personBlockingRepository.saveAndFlush(PersonBlocking.builder()
                .app(app2)
                .blockingPerson(personRegularAnna)
                .blockedPerson(contactPersonTenant1)
                .build());
    }

    public void createUserGeneratedContentAdmins() {
        personGlobalUserGeneratedContentAdmin =
                createPersonWithDefaultAddress("personGlobalUserGeneratedContentAdmin", tenant1,
                        "dc9488ee-8e07-4f57-8140-613774860309");
        assignRoleToPerson(personGlobalUserGeneratedContentAdmin, GlobalUserGeneratedContentAdmin.class, null);

        personUserGeneratedContentAdminTenant1 =
                createPersonWithDefaultAddress("personUserGeneratedContentFlagAdminTenant1", tenant1,
                        "800eefb6-d26a-4453-a4a0-b585f928b9ec");
        assignRoleToPerson(personUserGeneratedContentAdminTenant1, UserGeneratedContentAdmin.class,
                tenant1.getId());

        personUserGeneratedContentAdminTenant2 =
                createPersonWithDefaultAddress("personUserGeneratedContentFlagAdminTenant2", tenant2,
                        "6ed9d2f6-635c-4df2-b0df-8a390fb52c72");
        assignRoleToPerson(personUserGeneratedContentAdminTenant2, UserGeneratedContentAdmin.class,
                tenant2.getId());
    }

}
