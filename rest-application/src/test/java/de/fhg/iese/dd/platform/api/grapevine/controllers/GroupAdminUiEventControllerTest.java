/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupAddPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeVisibilityAndAccessibilityByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupModifyGeoAreasByAdminConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupModifyGeoAreasByAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupRemoveGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupExtendedDetail;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalGroupAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GroupAdmin;

public class GroupAdminUiEventControllerTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private TestEmailSenderService emailSenderService;

    private Person personGlobalGroupAdmin;
    private Person personGroupAdmin;
    private Person personGroupAdminOtherTenant;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();

        personGlobalGroupAdmin = th.createPersonWithDefaultAddress("personGlobalGroupAdmin", th.tenant1,
                "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalGroupAdmin, GlobalGroupAdmin.class, null);

        personGroupAdmin = th.createPersonWithDefaultAddress("personGroupAdmin", th.tenant1,
                "9b2f9a1b-39e6-4536-b0ca-8775909f0c6b");
        th.assignRoleToPerson(personGroupAdmin, GroupAdmin.class, th.tenant1.getId());

        personGroupAdminOtherTenant = th.createPersonWithDefaultAddress("personGroupAdminOtherTenant", th.tenant1,
                "aebd649f-144c-4dd9-829f-0e8183639820");
        th.assignRoleToPerson(personGroupAdminOtherTenant, GroupAdmin.class, th.tenant2.getId());

        // map a second geo area to group
        th.groupGeoAreaMappingRepository.save(GroupGeoAreaMapping.builder()
                .group(th.groupSchachvereinAAA)
                .geoArea(th.geoAreaMainz)
                .build());

        // add two group membership admins to groupWaffenfreundeAMA
        th.assignRoleToPerson(th.personChloeTenant1Dorf1, GroupMembershipAdmin.class,
                th.groupWaffenfreundeAMA.getId());
        th.assignRoleToPerson(th.personAnjaTenant1Dorf1, GroupMembershipAdmin.class,
                th.groupWaffenfreundeAMA.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void createGroup_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String name = "Beste Gruppe";
        final String shortName = "BG";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> includedGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());
        final List<String> excludedGeoAreaIds = Collections.singletonList(th.geoAreaMainz.getId());
        final List<String> groupAdminIds = Arrays.asList(th.personRegularAnna.getId(), th.personRegularKarl.getId());

        final ClientGroupCreateByAdminRequest request = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName(shortName)
                .tenantId(th.tenant1.getId())
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .groupMembershipAdminIds(groupAdminIds)
                .build();
        ClientGroupCreateByAdminConfirmation clientGroupCreateByAdminConfirmation =
                toObject(mockMvc.perform(groupCreateByAdminRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateByAdminConfirmation.class);
        ClientGroupExtendedDetail clientGroupExtendedDetail = clientGroupCreateByAdminConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqual(clientGroupExtendedDetail, request,
                Arrays.asList(th.personRegularAnna, th.personRegularKarl));

        // same group details but different tenant
        final ClientGroupCreateByAdminRequest request2 = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName(shortName)
                .tenantId(th.tenant2.getId())
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .groupMembershipAdminIds(groupAdminIds)
                .build();
        clientGroupCreateByAdminConfirmation =
                toObject(mockMvc.perform(groupCreateByAdminRequest(caller, request2))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateByAdminConfirmation.class);
        clientGroupExtendedDetail = clientGroupCreateByAdminConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqual(clientGroupExtendedDetail, request2,
                Arrays.asList(th.personRegularAnna, th.personRegularKarl));
    }

    @Test
    public void createGroup_GroupAdmin() throws Exception {

        final String name = "Katzen Gruppe";
        final String shortName = "KG";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> includedGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());
        final List<String> excludedGeoAreaIds = Collections.singletonList(th.geoAreaMainz.getId());
        final List<String> groupAdminIds = Arrays.asList(th.personRegularAnna.getId(), th.personRegularKarl.getId());

        final ClientGroupCreateByAdminRequest request = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName(shortName)
                .tenantId(th.tenant1.getId())
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .groupMembershipAdminIds(groupAdminIds)
                .build();
        ClientGroupCreateByAdminConfirmation clientGroupCreateByAdminConfirmation =
                toObject(mockMvc.perform(groupCreateByAdminRequest(personGroupAdmin, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateByAdminConfirmation.class);
        ClientGroupExtendedDetail clientGroupExtendedDetail = clientGroupCreateByAdminConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqual(clientGroupExtendedDetail, request,
                Arrays.asList(th.personRegularAnna, th.personRegularKarl));

        // same group details but different tenant
        final ClientGroupCreateByAdminRequest request2 = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName(shortName)
                .tenantId(th.tenant2.getId())
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .groupMembershipAdminIds(groupAdminIds)
                .build();
        clientGroupCreateByAdminConfirmation =
                toObject(mockMvc.perform(groupCreateByAdminRequest(personGroupAdminOtherTenant, request2))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateByAdminConfirmation.class);
        clientGroupExtendedDetail = clientGroupCreateByAdminConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqual(clientGroupExtendedDetail, request2,
                Arrays.asList(th.personRegularAnna, th.personRegularKarl));

        // same group details and same tenant
        final ClientGroupCreateByAdminRequest request3 = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName(shortName)
                .tenantId(th.tenant2.getId())
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .groupMembershipAdminIds(groupAdminIds)
                .build();
        clientGroupCreateByAdminConfirmation =
                toObject(mockMvc.perform(groupCreateByAdminRequest(personGroupAdminOtherTenant, request3))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateByAdminConfirmation.class);
        clientGroupExtendedDetail = clientGroupCreateByAdminConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqual(clientGroupExtendedDetail, request3,
                Arrays.asList(th.personRegularAnna, th.personRegularKarl));
    }

    @Test
    public void createGroup_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;

        final ClientGroupCreateByAdminRequest request = ClientGroupCreateByAdminRequest.builder()
                .name("Illegale Gruppe")
                .shortName("IG")
                .tenantId(th.tenant1.getId())
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                .groupMembershipAdminIds(Collections.emptyList())
                .build();
        assertOAuth2(groupCreateByAdminRequest(caller, request));

        mockMvc.perform(groupCreateByAdminRequest(caller, request))
                .andExpect(isNotAuthorized());

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, "Illegale Gruppe"));
    }

    @Test
    public void createGroup_InvalidAttributes() throws Exception {

        final Person caller = th.personRegularKarl;
        final String name = "Invalide Kruppe";
        final String shortname = "IK";
        final String tenantId = th.tenant1.getId();
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;

        // name too long
        int charCount = th.grapevineConfig.getMaxNumberCharsPerGroupName() + 1;
        mockMvc.perform(groupCreateByAdminRequest(caller,
                ClientGroupCreateByAdminRequest.builder()
                        .name(StringUtils.repeat('a', charCount))
                        .shortName(shortname)
                        .tenantId(tenantId)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                        .groupMembershipAdminIds(Collections.emptyList())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // shortname too long
        charCount = th.grapevineConfig.getMaxNumberCharsPerGroupShortName() + 1;
        mockMvc.perform(groupCreateByAdminRequest(caller,
                ClientGroupCreateByAdminRequest.builder()
                        .name(name)
                        .shortName(StringUtils.repeat('a', charCount))
                        .tenantId(tenantId)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                        .groupMembershipAdminIds(Collections.emptyList())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // tenantId NULL
        mockMvc.perform(groupCreateByAdminRequest(caller,
                ClientGroupCreateByAdminRequest.builder()
                        .name(name)
                        .shortName(shortname)
                        .tenantId(null)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                        .groupMembershipAdminIds(Collections.emptyList())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // tenantId empty
        mockMvc.perform(groupCreateByAdminRequest(caller,
                ClientGroupCreateByAdminRequest.builder()
                        .name(name)
                        .shortName(shortname)
                        .tenantId("")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                        .groupMembershipAdminIds(Collections.emptyList())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
    }

    @Test
    public void createGroup_PermissionDenied() throws Exception {

        // Caller does not have required role
        final String name = "Beste Gruppe";
        final ClientGroupCreateByAdminRequest request = ClientGroupCreateByAdminRequest.builder()
                .name(name)
                .shortName("BG")
                .tenantId(th.tenant1.getId())
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                .groupMembershipAdminIds(Collections.emptyList())
                .build();
        mockMvc.perform(groupCreateByAdminRequest(th.personRegularKarl, request))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but for different tenant
        mockMvc.perform(groupCreateByAdminRequest(personGroupAdminOtherTenant, request))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
    }

    @Test
    public void createGroup_GeoAreaNotFound() throws Exception {

        final String name = "Beste Gruppe";
        final String shortname = "BG";
        final String tenantId = th.tenant1.getId();
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> geoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), "invalidGeoAreaId");

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {
            // only invalid geo area ids
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(Collections.singletonList("invalidGeoAreaId"))
                            .excludedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                            .groupMembershipAdminIds(Collections.emptyList())
                            .build()))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                            .excludedGeoAreaIds(Collections.singletonList("invalidGeoAreaId"))
                            .groupMembershipAdminIds(Collections.emptyList())
                            .build()))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
            // valid and invalid geo area ids
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(geoAreaIds)
                            .groupMembershipAdminIds(Collections.emptyList())
                            .build()))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                            .excludedGeoAreaIds(geoAreaIds)
                            .groupMembershipAdminIds(Collections.emptyList())
                            .build()))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

            assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
        }
    }

    @Test
    public void createGroup_PersonNotFound() throws Exception {

        final String name = "Beste Gruppe";
        final String shortname = "BG";
        final String tenantId = th.tenant1.getId();
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> groupAdminIds = Arrays.asList(th.personRegularAnna.getId(), "invalidGroupAdminId");

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {
            // only invalid person ids
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                            .groupMembershipAdminIds(Collections.singletonList("invalidGroupAdminId"))
                            .build()))
                    .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
            // valid and invalid person ids
            mockMvc.perform(groupCreateByAdminRequest(caller,
                    ClientGroupCreateByAdminRequest.builder()
                            .name(name)
                            .shortName(shortname)
                            .tenantId(tenantId)
                            .groupVisibility(visibility)
                            .groupAccessibility(accessibility)
                            .groupContentVisibility(contentVisibility)
                            .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                            .groupMembershipAdminIds(groupAdminIds)
                            .build()))
                    .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

            assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
        }
    }

    @Test
    public void deleteGroup_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupWaffenfreundeAMA;

        assertFalse(group.isDeleted());
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).hasSize(1);
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).hasSize(2);
        List<Post> expectedPosts = th.postRepository.findAllByGroupAndDeletedFalse(group);
        assertEquals(2, expectedPosts.size());

        mockMvc.perform(groupDeleteByAdminRequest(caller, group.getId(), true))
                .andExpect(status().isOk())
                .andExpect(jsonPath("groupId").value(group.getId()))
                .andExpect(jsonPath("deletedMemberCount").value(3));

        assertTrue(th.groupRepository.findById(group.getId()).get().isDeleted());
        assertEquals(0, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).hasSize(0);
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).hasSize(0);
        expectedPosts = th.postRepository.findAllByGroupAndDeletedFalse(group);
        assertEquals(0, expectedPosts.size());
    }

    @Test
    public void deleteGroup_GroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupWaffenfreundeAMA;

        assertFalse(group.isDeleted());
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).hasSize(1);
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).hasSize(2);
        List<Post> expectedPosts = th.postRepository.findAllByGroupAndDeletedFalse(group);
        assertEquals(2, expectedPosts.size());

        mockMvc.perform(groupDeleteByAdminRequest(caller, group.getId(), true))
                .andExpect(status().isOk())
                .andExpect(jsonPath("groupId").value(group.getId()))
                .andExpect(jsonPath("deletedMemberCount").value(3));

        assertTrue(th.groupRepository.findById(group.getId()).get().isDeleted());
        assertEquals(0, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).hasSize(0);
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).hasSize(0);
        expectedPosts = th.postRepository.findAllByGroupAndDeletedFalse(group);
        assertEquals(0, expectedPosts.size());
    }

    @Test
    public void deleteGroup_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final Group group = th.groupWaffenfreundeAMA;

        assertOAuth2(groupDeleteByAdminRequest(caller, group.getId(), true));

        mockMvc.perform(groupDeleteByAdminRequest(caller, group.getId(), true))
                .andExpect(isNotAuthorized());

        assertTrue(th.groupRepository.existsById(group.getId()));
    }

    @Test
    public void deleteGroup_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;

        // groupId NULL
        mockMvc.perform(
                groupDeleteByAdminRequest(caller, null, true))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(
                groupDeleteByAdminRequest(caller, "", true))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void deleteGroup_PermissionDenied() throws Exception {

        final Group group = th.groupWaffenfreundeAMA;

        // Caller does not have required role
        mockMvc.perform(groupDeleteByAdminRequest(th.personRegularKarl, group.getId(), true))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but for different tenant
        mockMvc.perform(groupDeleteByAdminRequest(personGroupAdminOtherTenant, group.getId(), true))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertTrue(th.groupRepository.existsById(group.getId()));
    }

    @Test
    public void deleteGroup_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;

        mockMvc.perform(groupDeleteByAdminRequest(caller, "invalidGroupId", true))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void deleteGroup_GroupCannotBeDeleted() throws Exception {

        final Person caller = personGlobalGroupAdmin;

        // group contains posts
        mockMvc.perform(groupDeleteByAdminRequest(caller, th.groupWaffenfreundeAMA.getId(), false))
                .andExpect(isException(ClientExceptionType.GROUP_CANNOT_BE_DELETED));
        // group containing no posts but more than 3 members
        th.groupDorfgeschichteSSA.setMemberCount(5);
        th.groupRepository.save(th.groupDorfgeschichteSSA);
        mockMvc.perform(groupDeleteByAdminRequest(caller, th.groupDorfgeschichteSSA.getId(), false))
                .andExpect(isException(ClientExceptionType.GROUP_CANNOT_BE_DELETED));
    }

    @Test
    public void addPerson_PublicGroup() throws Exception {

        final Group publicGroup = th.groupAdfcAAP;
        final Person personToAdd = th.personRegularAnna;
        final String memberIntroductionText = "Added by admin";

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {

            assertFalse(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(publicGroup, personToAdd,
                    GroupMembershipStatus.APPROVED));

            mockMvc.perform(
                    groupAddPersonRequest(caller, publicGroup.getId(), personToAdd.getId(),
                            memberIntroductionText))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedGroup.id").value(publicGroup.getId()))
                    .andExpect(jsonPath("$.updatedGroup.memberCount").value(4));

            waitForEventProcessing();
            // No email is sent to group admin since the membershipStatus should be APPROVED
            assertTrue(emailSenderService.getEmails().isEmpty());

            final GroupMembership updatedGroupMembership =
                    th.groupMembershipRepository.findByGroupAndMember(publicGroup, personToAdd).get();
            assertThat(updatedGroupMembership.getStatus()).isEqualTo(GroupMembershipStatus.APPROVED);

            th.groupMembershipRepository.delete(updatedGroupMembership);
        }
    }

    @Test
    public void addPerson_ApprovalRequiredGroup() throws Exception {

        final Group approvalRequiredGroup = th.groupSchachvereinAAA;
        final Person personToAdd = th.personRegularAnna;
        final String memberIntroductionText = "Added by admin";

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {

            assertFalse(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(approvalRequiredGroup, personToAdd,
                    GroupMembershipStatus.APPROVED));

            mockMvc.perform(
                    groupAddPersonRequest(caller, approvalRequiredGroup.getId(), personToAdd.getId(),
                            memberIntroductionText))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedGroup.id").value(approvalRequiredGroup.getId()))
                    .andExpect(jsonPath("$.updatedGroup.memberCount").value(2));
            waitForEventProcessing();
            // No email is sent to group admin since the membershipStatus should be APPROVED
            assertTrue(emailSenderService.getEmails().isEmpty());

            final GroupMembership updatedGroupMembership =
                    th.groupMembershipRepository.findByGroupAndMember(approvalRequiredGroup, personToAdd).get();
            assertThat(updatedGroupMembership.getStatus()).isEqualTo(GroupMembershipStatus.APPROVED);

            th.groupMembershipRepository.delete(updatedGroupMembership);
        }
    }

    @Test
    public void addPerson_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String personIdToAdd = th.personRegularAnna.getId();
        final String memberIntroductionText = "Added by admin";

        assertOAuth2(groupAddPersonRequest(caller, groupId, personIdToAdd, memberIntroductionText));

        mockMvc.perform(groupAddPersonRequest(caller, groupId, personIdToAdd, memberIntroductionText))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void addPerson_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String personIdToAdd = th.personRegularAnna.getId();
        final String memberIntroductionText = "Added by admin";

        // groupId NULL
        mockMvc.perform(groupAddPersonRequest(caller, null, personIdToAdd, memberIntroductionText))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupAddPersonRequest(caller, "", personIdToAdd, memberIntroductionText))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId NULL
        mockMvc.perform(groupAddPersonRequest(caller, groupId, null, memberIntroductionText))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId empty
        mockMvc.perform(groupAddPersonRequest(caller, groupId, "", memberIntroductionText))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // memberIntroductionText too long
        final int charCount = th.grapevineConfig.getMaxNumberCharsPerComment() + 1;
        mockMvc.perform(groupAddPersonRequest(caller, groupId, personIdToAdd,
                StringUtils.repeat('a', charCount)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void addPerson_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final String personIdToAdd = th.personRegularAnna.getId();
        final String memberIntroductionText = "Added by admin";

        // Caller does not have required role
        mockMvc.perform(
                groupAddPersonRequest(th.personRegularKarl, groupId, personIdToAdd, memberIntroductionText))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the person to be added has different tenant
        mockMvc.perform(groupAddPersonRequest(personGroupAdminOtherTenant, groupId, personIdToAdd,
                memberIntroductionText))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void addPerson_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String personIdToAdd = th.personRegularAnna.getId();
        final String memberIntroductionText = "Added by admin";

        mockMvc.perform(groupAddPersonRequest(caller, "InvalidGroupId", personIdToAdd, memberIntroductionText))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void addPerson_PersonNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String memberIntroductionText = "Added by admin";

        mockMvc.perform(groupAddPersonRequest(caller, groupId, "InvalidPersonId", memberIntroductionText))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void addGroupMembershipAdmin_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupSchachvereinAAA;
        final Person membershipAdminToAdd = th.personRegularKarl;

        assertFalse(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToAdd,
                GroupMembershipStatus.APPROVED));
        assertFalse(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToAdd, GroupMembershipAdmin.class));

        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, group.getId(), membershipAdminToAdd.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andExpect(jsonPath("$.updatedGroup.memberCount").value(2))
                .andExpect(jsonPath("$.updatedGroup.groupMembershipAdmins").value(hasSize(2)));

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToAdd,
                GroupMembershipStatus.APPROVED));
        assertTrue(th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToAdd, GroupMembershipAdmin.class));
    }

    @Test
    public void addGroupMembershipAdmin_GroupAdmin() throws Exception {

        final Person caller = personGroupAdmin;
        final Group group = th.groupSchachvereinAAA;
        final Person membershipAdminToAdd = th.personRegularKarl;

        assertFalse(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToAdd,
                GroupMembershipStatus.APPROVED));
        assertFalse(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToAdd, GroupMembershipAdmin.class));

        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, group.getId(), membershipAdminToAdd.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andExpect(jsonPath("$.updatedGroup.memberCount").value(2))
                .andExpect(jsonPath("$.updatedGroup.groupMembershipAdmins").value(hasSize(2)));

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToAdd,
                GroupMembershipStatus.APPROVED));
        assertTrue(th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToAdd, GroupMembershipAdmin.class));
    }

    @Test
    public void addGroupMembershipAdmin_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToAdd = th.personRegularAnna.getId();

        assertOAuth2(groupAddGroupMembershipAdminRequest(caller, groupId, membershipAdminToAdd));

        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, groupId, membershipAdminToAdd))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void addGroupMembershipAdmin_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToAdd = th.personRegularAnna.getId();

        // groupId NULL
        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, null, membershipAdminToAdd))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, "", membershipAdminToAdd))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId NULL
        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, groupId, null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId empty
        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, groupId, ""))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void addGroupMembershipAdmin_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToAdd = th.personRegularAnna.getId();

        // Caller does not have required role
        mockMvc.perform(groupAddGroupMembershipAdminRequest(th.personRegularKarl, groupId, membershipAdminToAdd))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group has a different tenant
        mockMvc.perform(
                groupAddGroupMembershipAdminRequest(personGroupAdminOtherTenant, groupId, membershipAdminToAdd))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void addGroupMembershipAdmin_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String membershipAdminToAdd = th.personRegularAnna.getId();

        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, "InvalidGroupId", membershipAdminToAdd))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void addGroupMembershipAdmin_PersonNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();

        mockMvc.perform(groupAddGroupMembershipAdminRequest(caller, groupId, "InvalidPersonId"))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void removeGroupMembershipAdmin_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupWaffenfreundeAMA;
        final Person membershipAdminToRemove = th.personAnjaTenant1Dorf1;

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToRemove,
                GroupMembershipStatus.APPROVED));
        assertTrue(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToRemove, GroupMembershipAdmin.class));
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));

        mockMvc.perform(
                groupRemoveGroupMembershipAdminRequest(caller, group.getId(), membershipAdminToRemove.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andExpect(jsonPath("$.updatedGroup.memberCount").value(3))
                .andExpect(jsonPath("$.updatedGroup.groupMembershipAdmins").value(hasSize(1)));

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToRemove,
                GroupMembershipStatus.APPROVED));
        assertFalse(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToRemove, GroupMembershipAdmin.class));
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
    }

    @Test
    public void removeGroupMembershipAdmin_GroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupWaffenfreundeAMA;
        final Person membershipAdminToRemove = th.personAnjaTenant1Dorf1;

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToRemove,
                GroupMembershipStatus.APPROVED));
        assertTrue(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToRemove, GroupMembershipAdmin.class));
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));

        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, group.getId(),
                membershipAdminToRemove.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andExpect(jsonPath("$.updatedGroup.memberCount").value(3))
                .andExpect(jsonPath("$.updatedGroup.groupMembershipAdmins").value(hasSize(1)));

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(group, membershipAdminToRemove,
                GroupMembershipStatus.APPROVED));
        assertFalse(
                th.roleAssignmentRepository.existsByPersonAndRole(membershipAdminToRemove, GroupMembershipAdmin.class));
        assertEquals(3, th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
    }

    @Test
    public void removeGroupMembershipAdmin_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToRemove = th.personRegularAnna.getId();

        assertOAuth2(groupRemoveGroupMembershipAdminRequest(caller, groupId, membershipAdminToRemove));

        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, groupId, membershipAdminToRemove))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void removeGroupMembershipAdmin_InvalidAttributes() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToRemove = th.personRegularAnna.getId();

        // groupId NULL
        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, null, membershipAdminToRemove))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, "", membershipAdminToRemove))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId NULL
        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, groupId, null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId empty
        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, groupId, ""))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void removeGroupMembershipAdmin_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final String membershipAdminToRemove = th.personRegularAnna.getId();

        // Caller does not have required role
        mockMvc.perform(
                groupRemoveGroupMembershipAdminRequest(th.personRegularKarl, groupId,
                        membershipAdminToRemove))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group has a different tenant
        mockMvc.perform(
                groupRemoveGroupMembershipAdminRequest(personGroupAdminOtherTenant, groupId,
                        membershipAdminToRemove))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void removeGroupMembershipAdmin_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String membershipAdminToRemove = th.personRegularAnna.getId();

        mockMvc.perform(
                groupRemoveGroupMembershipAdminRequest(caller, "InvalidGroupId", membershipAdminToRemove))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void removeGroupMembershipAdmin_PersonNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();

        mockMvc.perform(groupRemoveGroupMembershipAdminRequest(caller, groupId, "InvalidPersonId"))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void removeGroupMembershipAdmin_OnlyOneGroupMembershipAdminInGroup() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupSchachvereinAAA;
        final Person membershipAdminToRemove = th.personBenTenant1Dorf2;

        mockMvc.perform(
                groupRemoveGroupMembershipAdminRequest(caller, group.getId(),
                        membershipAdminToRemove.getId()))
                .andExpect(isException(ClientExceptionType.GROUP_MEMBERSHIP_ADMIN_REMOVAL_NOT_POSSIBLE));
    }

    @Test
    public void modifyGroupGeoAreas_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final Group group = th.groupSchachvereinAAA;
        final List<String> newGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());

        final MvcResult mvcResult = mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(group.getId())
                        .includedGeoAreaIds(newGeoAreaIds)
                        .build()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andReturn();

        assertGeoAreasMatch(group,
                toObject(mvcResult, ClientGroupModifyGeoAreasByAdminConfirmation.class).getUpdatedGroup());
    }

    @Test
    public void modifyGroupGeoAreas_GroupAdmin() throws Exception {

        final Person caller = personGroupAdmin;
        final Group group = th.groupSchachvereinAAA;
        final List<String> newGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());

        final MvcResult mvcResult = mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(group.getId())
                        .includedGeoAreaIds(newGeoAreaIds)
                        .build()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(group.getId()))
                .andReturn();
        assertGeoAreasMatch(group,
                toObject(mvcResult, ClientGroupModifyGeoAreasByAdminConfirmation.class).getUpdatedGroup());
    }

    @Test
    public void modifyGroupGeoAreas_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final List<String> geoAreaIds = Collections.singletonList(th.geoAreaDorf1InEisenberg.getId());

        assertOAuth2(groupModifyGeoAreasByAdminRequest(caller, ClientGroupModifyGeoAreasByAdminRequest.builder()
                .groupId(groupId)
                .includedGeoAreaIds(geoAreaIds)
                .build()));

        mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller, ClientGroupModifyGeoAreasByAdminRequest.builder()
                .groupId(groupId)
                .includedGeoAreaIds(geoAreaIds)
                .build()))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void modifyGroupGeoAreas_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final List<String> geoAreaIds = Collections.singletonList(th.geoAreaDorf1InEisenberg.getId());

        // groupId NULL
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(null)
                        .includedGeoAreaIds(geoAreaIds)
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId("")
                        .includedGeoAreaIds(geoAreaIds)
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // geoAreaIds NULL
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(groupId)
                        .includedGeoAreaIds(null)
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // geoAreaIds empty
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(groupId)
                        .includedGeoAreaIds(Collections.emptyList())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void modifyGroupGeoAreas_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final List<String> geoAreaIds = Collections.singletonList(th.geoAreaDorf1InEisenberg.getId());

        // Caller does not have required role
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(th.personRegularKarl,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(groupId)
                        .includedGeoAreaIds(geoAreaIds)
                        .build()))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group whose geo areas should be modified has different tenant
        mockMvc.perform(groupModifyGeoAreasByAdminRequest(personGroupAdminOtherTenant,
                ClientGroupModifyGeoAreasByAdminRequest.builder()
                        .groupId(groupId)
                        .includedGeoAreaIds(geoAreaIds)
                        .build()))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void modifyGroupGeoAreas_GroupNotFound() throws Exception {

        final List<String> geoAreaIds = Collections.singletonList(th.geoAreaDorf1InEisenberg.getId());

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {

            mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                    ClientGroupModifyGeoAreasByAdminRequest.builder()
                            .groupId("InvalidGroupId")
                            .includedGeoAreaIds(geoAreaIds)
                            .build()))
                    .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
        }
    }

    @Test
    public void modifyGroupGeoAreas_GeoAreaNotFound() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final List<String> geoAreaIds = Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), "invalidGeoAreaId");

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdmin);

        for (final Person caller : personsWithSuitableRoles) {

            mockMvc.perform(groupModifyGeoAreasByAdminRequest(caller,
                    ClientGroupModifyGeoAreasByAdminRequest.builder()
                            .groupId(groupId)
                            .includedGeoAreaIds(geoAreaIds)
                            .build()))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        }
    }

    @Test
    public void changeGroupName_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();

        // new name and short name
        String name = "Neuer Name";
        String shortName = "NN";
        mockMvc.perform(
                groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        Group updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // only new name
        name = "Besserer Name";
        mockMvc.perform(
                groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // only new short name
        shortName = "BN";
        mockMvc.perform(
                        groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // already existing name
        name = th.groupSchachvereinAAA.getName();
        shortName = th.groupSchachvereinAAA.getShortName();
        mockMvc.perform(
                        groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());
    }

    @Test
    public void changeGroupName_GroupAdmin() throws Exception {

        final Person caller = personGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();

        // new name and short name
        String name = "Neuer Name";
        String shortName = "NN";
        mockMvc.perform(
                groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        Group updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // only new name
        name = "Besserer Name";
        mockMvc.perform(
                groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // only new short name
        shortName = "BN";
        mockMvc.perform(
                        groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());

        // already existing name
        name = th.groupSchachvereinAAA.getName();
        shortName = th.groupSchachvereinAAA.getShortName();
        mockMvc.perform(
                        groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.name").value(name))
                .andExpect(jsonPath("$.updatedGroup.shortName").value(shortName));

        updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(name, updatedGroup.getName());
        assertEquals(shortName, updatedGroup.getShortName());
    }

    @Test
    public void changeGroupName_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String name = "Neuer Name";
        final String shortName = "NN";

        assertOAuth2(groupChangeNameByAdminRequest(caller, groupId, name, shortName));

        mockMvc.perform(groupChangeNameByAdminRequest(caller, groupId, name, shortName))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void changeGroupName_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final String name = "Neuer Name";
        final String shortName = "NN";

        // groupId NULL
        mockMvc.perform(groupChangeNameByAdminRequest(caller, null, name, shortName))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupChangeNameByAdminRequest(caller, "", name, shortName))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // name too long
        int charCount = th.grapevineConfig.getMaxNumberCharsPerGroupName() + 1;
        mockMvc.perform(
                groupChangeNameByAdminRequest(caller, groupId, StringUtils.repeat('a', charCount), shortName))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // shortName too long
        charCount = th.grapevineConfig.getMaxNumberCharsPerGroupShortName() + 1;
        mockMvc.perform(groupChangeNameByAdminRequest(caller, groupId, name, StringUtils.repeat('a', charCount)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void changeGroupName_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final String name = "Neuer Name";
        final String shortName = "NN";

        // Caller does not have required role
        mockMvc.perform(groupChangeNameByAdminRequest(th.personRegularKarl, groupId, name, shortName))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group whose name should be modified has different tenant
        mockMvc.perform(groupChangeNameByAdminRequest(personGroupAdminOtherTenant, groupId, name, shortName))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void changeGroupName_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String name = "Neuer Name";
        final String shortName = "NN";

        mockMvc.perform(groupChangeNameByAdminRequest(caller, "InvalidGroupId", name, shortName))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_GlobalGroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final GroupVisibility visibility = GroupVisibility.MEMBERS;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.MEMBERS;

        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, accessibility,
                        contentVisibility))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.visibility").value(visibility.name()))
                .andExpect(jsonPath("$.updatedGroup.accessibility").value(accessibility.name()))
                .andExpect(jsonPath("$.updatedGroup.contentVisibility").value(contentVisibility.name()));

        Group updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(visibility, updatedGroup.getVisibility());
        assertEquals(accessibility, updatedGroup.getAccessibility());
        assertEquals(contentVisibility, updatedGroup.getContentVisibility());
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_GroupAdmin() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final GroupVisibility visibility = GroupVisibility.SAME_HOME_AREA;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.SAME_HOME_AREA;

        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, accessibility,
                        contentVisibility))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedGroup.id").value(groupId))
                .andExpect(jsonPath("$.updatedGroup.visibility").value(visibility.name()))
                .andExpect(jsonPath("$.updatedGroup.accessibility").value(accessibility.name()))
                .andExpect(jsonPath("$.updatedGroup.contentVisibility").value(contentVisibility.name()));

        Group updatedGroup = th.groupRepository.findById(groupId).get();
        assertEquals(visibility, updatedGroup.getVisibility());
        assertEquals(accessibility, updatedGroup.getAccessibility());
        assertEquals(contentVisibility, updatedGroup.getContentVisibility());
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final String groupId = th.groupSchachvereinAAA.getId();
        final GroupVisibility visibility = GroupVisibility.MEMBERS;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.MEMBERS;

        assertOAuth2(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, accessibility,
                        contentVisibility));

        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, accessibility,
                        contentVisibility))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_InvalidAttributes() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupSchachvereinAAA.getId();
        final GroupVisibility visibility = GroupVisibility.MEMBERS;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.MEMBERS;

        // groupId NULL
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, null, visibility, accessibility,
                        contentVisibility))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupChangeVisibilityAndAccessibilityByAdminRequest(caller, "", visibility, accessibility,
                contentVisibility))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // visibility NULL
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, null, accessibility,
                        contentVisibility))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // accessibility NULL
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, null,
                        contentVisibility))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // contentVisibility NULL
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, groupId, visibility, accessibility,
                        null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_PermissionDenied() throws Exception {

        final String groupId = th.groupSchachvereinAAA.getId();
        final GroupVisibility visibility = GroupVisibility.MEMBERS;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.MEMBERS;

        // Caller does not have required role
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(th.personRegularKarl, groupId, visibility,
                        accessibility,
                        contentVisibility))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group whose name should be modified has different tenant
        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(personGroupAdminOtherTenant, groupId,
                        visibility, accessibility,
                        contentVisibility))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void changeGroupVisibilityAndAccessibility_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final GroupVisibility visibility = GroupVisibility.MEMBERS;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.MEMBERS;

        mockMvc.perform(
                groupChangeVisibilityAndAccessibilityByAdminRequest(caller, "InvalidGroupId", visibility,
                        accessibility, contentVisibility))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder groupCreateByAdminRequest(Person caller,
            ClientGroupCreateByAdminRequest request) throws IOException {
        return post("/adminui/group/event/groupCreateByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(request));
    }

    private MockHttpServletRequestBuilder groupDeleteByAdminRequest(Person caller, String groupId,
            boolean forceDeleteNonEmptyGroup) throws IOException {
        return post("/adminui/group/event/groupDeleteByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupDeleteByAdminRequest.builder()
                        .groupId(groupId)
                        .forceDeleteNonEmptyGroup(forceDeleteNonEmptyGroup)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupModifyGeoAreasByAdminRequest(Person caller,
            ClientGroupModifyGeoAreasByAdminRequest request) throws IOException {
        return post("/adminui/group/event/groupModifyGeoAreasByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(request));
    }

    private MockHttpServletRequestBuilder groupChangeNameByAdminRequest(Person caller, String groupId, String name,
            String shortName) throws IOException {

        return post("/adminui/group/event/groupChangeNameByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupChangeNameByAdminRequest.builder()
                        .groupId(groupId)
                        .newName(name)
                        .newShortName(shortName)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupAddPersonRequest(Person caller, String groupId, String personId,
            String introText) throws IOException {

        return post("/adminui/group/event/groupAddPersonRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupAddPersonRequest.builder()
                        .groupId(groupId)
                        .personIdToAdd(personId)
                        .memberIntroductionText(introText)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupAddGroupMembershipAdminRequest(Person caller, String groupId,
            String personId) throws IOException {
        return post("/adminui/group/event/groupAddGroupMembershipAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupAddGroupMembershipAdminRequest.builder()
                        .groupId(groupId)
                        .membershipAdminIdToAdd(personId)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupRemoveGroupMembershipAdminRequest(Person caller, String groupId,
            String personId) throws IOException {
        return post("/adminui/group/event/groupRemoveGroupMembershipAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupRemoveGroupMembershipAdminRequest.builder()
                        .groupId(groupId)
                        .membershipAdminIdToRemove(personId)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupChangeVisibilityAndAccessibilityByAdminRequest(Person caller,
            String groupId, GroupVisibility visibility, GroupAccessibility accessibility,
            GroupContentVisibility contentVisibility) throws IOException {
        return post("/adminui/group/event/groupChangeVisibilityAndAccessibilityByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupChangeVisibilityAndAccessibilityByAdminRequest.builder()
                        .groupId(groupId)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .build()));
    }

    private void assertGroupDetailsAreEqual(ClientGroupExtendedDetail actualGroup,
            ClientGroupCreateByAdminRequest createRequest, List<Person> groupAdmins) {

        assertEquals(createRequest.getName(), actualGroup.getName());
        assertEquals(createRequest.getTenantId(), actualGroup.getTenantId());
        assertEquals(createRequest.getShortName(), actualGroup.getShortName());
        assertEquals(createRequest.getGroupVisibility(), actualGroup.getVisibility());
        assertEquals(createRequest.getGroupAccessibility(), actualGroup.getAccessibility());
        assertEquals(createRequest.getGroupContentVisibility(), actualGroup.getContentVisibility());
        assertFalse(actualGroup.isDeleted());
        assertEquals(groupAdmins.size(), actualGroup.getMemberCount());
        assertThat(actualGroup.getIncludedGeoAreas()).map(ClientBaseEntity::getId)
                .containsAll(createRequest.getIncludedGeoAreaIds());
        assertThat(actualGroup.getExcludedGeoAreas()).map(ClientBaseEntity::getId)
                .containsAll(createRequest.getExcludedGeoAreaIds());
        assertEquals(groupAdmins.size(), actualGroup.getGroupMembershipAdmins().size());
        assertEquals(0, actualGroup.getPostCount());
        assertNull(actualGroup.getLastPost());
        assertNull(actualGroup.getLastCommentOfPost());

        assertThat(actualGroup.getIncludedGeoAreas()).map(ClientBaseEntity::getId)
                .containsAll(createRequest.getIncludedGeoAreaIds());
        assertThat(actualGroup.getExcludedGeoAreas()).map(ClientBaseEntity::getId)
                .containsAll(createRequest.getExcludedGeoAreaIds());

        final Group createdGroup = th.groupRepository.findById(actualGroup.getId()).get();
        assertGeoAreasMatch(createdGroup, actualGroup);

        for (Person groupAdmin : groupAdmins) {
            assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(createdGroup, groupAdmin,
                    GroupMembershipStatus.APPROVED));
            assertTrue(th.roleAssignmentRepository.existsByPersonAndRole(groupAdmin, GroupMembershipAdmin.class));
        }
    }

    private void assertGeoAreasMatch(Group expectedGroup, ClientGroupExtended actualGroup) {
        IGroupService.GroupGeoAreaIds geoAreaIds =
                th.findGeoAreaIdsByGroups(Collections.singleton(expectedGroup)).values().iterator().next();
        if (geoAreaIds.getIncludedGeoAreaIds().isEmpty()) {
            assertThat(actualGroup.getIncludedGeoAreas()).isNullOrEmpty();
        } else {
            assertThat(actualGroup.getIncludedGeoAreas()).map(ClientBaseEntity::getId)
                    .containsAll(geoAreaIds.getIncludedGeoAreaIds());
        }
        if (geoAreaIds.getExcludedGeoAreaIds().isEmpty()) {
            assertThat(actualGroup.getExcludedGeoAreas()).isNullOrEmpty();
        } else {
            assertThat(actualGroup.getExcludedGeoAreas()).map(ClientBaseEntity::getId)
                    .containsAll(geoAreaIds.getExcludedGeoAreaIds());
        }
    }

}
