/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.push.clientevent.ClientPushCategoryUserSettingChangeResponse;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PlatformNotSupportedException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotConfigurableException;
import de.fhg.iese.dd.platform.business.shared.push.providers.TestExternalPushProvider;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;

public class PushControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;

    @SpyBean
    private TestExternalPushProvider externalPushProvider;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Autowired
    private IPersonBlockingService personBlockingService;

    private Person person;
    private PushCategory pushCategoryParent1App1;
    private PushCategory pushCategoryParent1Child1App1;
    private PushCategory pushCategoryParent1Child2App1;
    private PushCategory pushCategoryParent1Child3App1;
    private PushCategory pushCategoryParent1Child4App1;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();

        pushCategoryParent1App1 = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .name("pushCategoryParent1App1")
                        .description("pushCategoryParent1App1 description")
                        .orderValue(21)
                        .defaultLoudPushEnabled(true)
                        .configurable(true)
                        .build(),
                th.nextTimeStamp()));

        //intentionally different creation order than order value
        pushCategoryParent1Child2App1 = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryParent1App1)
                        .name("pushCategoryParent1Child2App1")
                        .description("pushCategoryParent1Child2App1 description")
                        .orderValue(23)
                        .defaultLoudPushEnabled(false)
                        .configurable(true)
                        .visible(false)
                        .build(),
                th.nextTimeStamp()));

        pushCategoryParent1Child1App1 = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryParent1App1)
                        .name("pushCategoryParent1Child1App1")
                        .description("pushCategoryParent1Child1App1 description")
                        .orderValue(22)
                        .defaultLoudPushEnabled(true)
                        .configurable(true)
                        .build(),
                th.nextTimeStamp()));

        pushCategoryParent1Child3App1 = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryParent1App1)
                        .name("pushCategoryParent1Child3App1")
                        .description("pushCategoryParent1Child3App1 description")
                        .orderValue(24)
                        .defaultLoudPushEnabled(true)
                        .configurable(false)
                        .build(),
                th.nextTimeStamp()));

        pushCategoryParent1Child4App1 = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryParent1App1)
                        .name("pushCategoryParent1Child4App1")
                        .description("pushCategoryParent1Child4App1 description")
                        .orderValue(25)
                        .defaultLoudPushEnabled(true)
                        .configurable(true)
                        .visible(false)
                        .build(),
                th.nextTimeStamp()));

        th.pushCategoriesApp1.add(pushCategoryParent1App1);
        th.pushCategoriesApp1.add(pushCategoryParent1Child1App1);
        th.pushCategoriesApp1.add(pushCategoryParent1Child2App1);
        th.pushCategoriesApp1.add(pushCategoryParent1Child3App1);

        person = th.personRegularAnna;
        Person blockedPerson = th.personRegularKarl;
        personBlockingService.blockPerson(th.app1, person, blockedPerson);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void registerEndpointPerson() throws Exception {

        String testToken = "my-test-token";

        th.selectGeoAreaForAppVariant(th.app1Variant1, person, th.geoAreaEisenberg);

        mockMvc.perform(post("/push/person")
                .headers(authHeadersFor(person, th.app1Variant1))
                .param("token", testToken)
                .param("platform", PushPlatformType.FCM.toString()))
                .andExpect(status().isOk());

        waitForEventProcessing();

        Mockito.verify(externalPushProvider).registerPushEndpoint(Mockito.eq(testToken),
                Mockito.eq(PushPlatformType.FCM), Mockito.eq(th.app1Variant1),
                Mockito.eq(person)
        );
    }

    @Test
    public void registerEndpointPersonUnauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/push/person")
                        .param("token", "my-test-token")
                        .param("platform", PushPlatformType.APNS_PROD.toString()),
                th.app1Variant1,
                person);
    }

    @Test
    public void registerEndpointPersonWrongParameters() throws Exception {

        th.selectGeoAreaForAppVariant(th.app1Variant1, person, th.geoAreaEisenberg);

        //not found appId
        String testToken = "my-test-token";
        String notFoundAppVariantIdentifier = "testBar";

        mockMvc.perform(post("/push/person")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .appVariantIdentifier(notFoundAppVariantIdentifier)
                        .build()))
                .param("token", testToken)
                .param("platform", PushPlatformType.GCM.toString()))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        //empty platformType
        mockMvc.perform(post("/push/person")
                .headers(authHeadersFor(person, th.app1Variant1))
                .param("token", testToken)
                .param("platform", ""))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //empty token
        mockMvc.perform(post("/push/person")
                .headers(authHeadersFor(person, th.app1Variant1))
                .param("token", "")
                .param("platform", PushPlatformType.FCM.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //exception in push client -> Exception will be eaten
        Mockito.doThrow(new PlatformNotSupportedException("platform invalid"))
                .when(externalPushProvider)
                .registerPushEndpoint(
                        Mockito.eq(testToken),
                        Mockito.eq(PushPlatformType.GCM),
                        Mockito.eq(th.app1Variant1),
                        Mockito.eq(person)
                );

        mockMvc.perform(post("/push/person")
                .headers(authHeadersFor(person, th.app1Variant1))
                .param("token", testToken)
                .param("platform", PushPlatformType.GCM.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void unregisterEndpoint() throws Exception {

        String testToken = "my-test-token";

        mockMvc.perform(delete("/push")
                .headers(headersForAppVariant(th.app1Variant1))
                .param("token", testToken)
                .param("personId", person.getId())
                .param("platform", PushPlatformType.APNS_PROD.toString()))
                .andExpect(status().isOk());

        Mockito.verify(externalPushProvider, Mockito.times(1)).deletePushEndpoint(testToken,
                PushPlatformType.APNS_PROD, th.app1Variant1, person);
    }

    @Test
    public void unregisterEndpointWrongParameters() throws Exception {

        //not found appId
        String testToken = "my-test-token";
        String notFoundAppVariantIdentifier = "testBar";

        mockMvc.perform(delete("/push")
                .param("token", testToken)
                .param("personId", person.getId())
                .param("platform", PushPlatformType.GCM.toString())
                .headers(headersForAppVariant(notFoundAppVariantIdentifier)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        //empty platformType
        mockMvc.perform(delete("/push")
                .param("token", testToken)
                .param("personId", person.getId())
                .param("platform", "")
                .headers(headersForAppVariant(th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //empty token
        mockMvc.perform(delete("/push")
                .param("token", "")
                .param("personId", person.getId())
                .param("platform", PushPlatformType.APNS_DEV.toString())
                .headers(headersForAppVariant(th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //no person id
        mockMvc.perform(delete("/push")
                .param("token", testToken)
                .param("platform", PushPlatformType.APNS_DEV.toString())
                .headers(headersForAppVariant(th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //empty person id
        mockMvc.perform(delete("/push")
                .param("token", testToken)
                .param("personId", "")
                .param("platform", PushPlatformType.APNS_DEV.toString())
                .headers(headersForAppVariant(th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        //unknown person id
        mockMvc.perform(delete("/push")
                .param("token", testToken)
                .param("personId", "gibbetsnicht")
                .param("platform", PushPlatformType.APNS_DEV.toString())
                .headers(headersForAppVariant(th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void changePushCategoryUserSettingsInternalNotConfigurable() throws Exception {

        try {
            pushCategoryService.changePushCategoryUserSetting(th.pushCategory2App1, person, th.app1Variant1, true);
        } catch (PushCategoryNotConfigurableException e) {
            assertEquals(PushCategoryNotConfigurableException.class, e.getClass());
        }

        List<PushCategoryUserSetting> categoryUserSettingsAll = pushCategoryUserSettingRepository.findAll();
        assertEquals(0, categoryUserSettingsAll.size());
    }

    //get all push category user settings for app variant, authenticated user
    @Test
    public void getPushCategoryUserSettingsAuthenticated() throws Exception {

        AppVariant appVariant = th.app1Variant1;

        //filter out the non configurable and non visible and sort it by created descending
        List<PushCategory> expectedVisibleAndConfigurablePushCategories = th.pushCategoriesApp1.stream()
                .filter(PushCategory::isConfigurable)
                .filter(PushCategory::isVisible)
                .sorted(Comparator.comparing(PushCategory::getOrderValue))
                .collect(Collectors.toList());

        assertEquals(4, expectedVisibleAndConfigurablePushCategories.size());
        //check that there is really something to filter out
        assertEquals(7, th.pushCategoriesApp1.size());

        mockMvc.perform(get("/push/categorySetting")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedVisibleAndConfigurablePushCategories.size())))

                .andExpect(jsonPath("$[0].orderValue")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getOrderValue()))
                .andExpect(jsonPath("$[0].id")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getId()))
                .andExpect(jsonPath("$[0].name")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getName()))
                .andExpect(jsonPath("$[0].description")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getDescription()))
                .andExpect(jsonPath("$[0].loudPushEnabled")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).isDefaultLoudPushEnabled()))

                .andExpect(jsonPath("$[1].orderValue")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getOrderValue()))
                .andExpect(jsonPath("$[1].id")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getId()))
                .andExpect(jsonPath("$[1].name")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getName()))
                .andExpect(jsonPath("$[1].description")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getDescription()))
                .andExpect(jsonPath("$[1].loudPushEnabled")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).isDefaultLoudPushEnabled()));

        PushCategory changedPushCategory = expectedVisibleAndConfigurablePushCategories.get(0);

        //change a setting and check if the change is also visible
        pushCategoryService.changePushCategoryUserSetting(changedPushCategory, person, appVariant,
                !changedPushCategory.isDefaultLoudPushEnabled());

        mockMvc.perform(get("/push/categorySetting")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedVisibleAndConfigurablePushCategories.size())))

                .andExpect(jsonPath("$[0].id")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getId()))
                .andExpect(jsonPath("$[0].name")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getName()))
                .andExpect(jsonPath("$[0].description")
                        .value(expectedVisibleAndConfigurablePushCategories.get(0).getDescription()))
                .andExpect(jsonPath("$[0].loudPushEnabled")
                        .value(!expectedVisibleAndConfigurablePushCategories.get(0).isDefaultLoudPushEnabled()))

                .andExpect(jsonPath("$[1].id")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getId()))
                .andExpect(jsonPath("$[1].name")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getName()))
                .andExpect(jsonPath("$[1].description")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).getDescription()))
                .andExpect(jsonPath("$[1].loudPushEnabled")
                        .value(expectedVisibleAndConfigurablePushCategories.get(1).isDefaultLoudPushEnabled()));
    }

    @Test
    public void getPushCategoryUserSettingsInvalidParameter() throws Exception {

        assertOAuth2AppVariantRequired(get("/push/categorySetting"), th.app1Variant1, person);
    }

    @Test
    public void getPushCategoryUserSettingsUnauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/push/categorySetting"), th.app1Variant1, person);
    }

    @Test
    public void changePushCategoryUserSettings() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        PushCategory pushCategory = th.pushCategory1App1;
        boolean newLoudPushEnabled = !pushCategory.isDefaultLoudPushEnabled();

        ClientPushCategoryUserSetting expectedSetting =
                toClientPushCategoryUserSetting(pushCategory, newLoudPushEnabled);

        ClientPushCategoryUserSettingChangeResponse expectedResponse =
                ClientPushCategoryUserSettingChangeResponse.builder()
                        .changedPushCategoryUserSetting(expectedSetting)
                        .changedDependentPushCategoryUserSettings(Collections.emptyList())
                        .build();

        mockMvc.perform(post("/push/categorySetting/{pushCategoryId}", pushCategory.getId())
                .headers(authHeadersFor(person, appVariant))
                .param("loudPushEnabled", Boolean.toString(newLoudPushEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedResponse));

        final PushCategoryUserSetting userSetting =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(pushCategory, person,
                        appVariant);

        assertThat(userSetting.isLoudPushEnabled()).isEqualTo(newLoudPushEnabled);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void changePushCategoryUserSettings_ParentCategory() throws Exception {

        AppVariant appVariant = th.app1Variant1;

        boolean newLoudPushEnabled = !pushCategoryParent1App1.isDefaultLoudPushEnabled();
        assertFalse(newLoudPushEnabled);

        ClientPushCategoryUserSetting expectedSetting =
                toClientPushCategoryUserSetting(pushCategoryParent1App1, newLoudPushEnabled);

        ClientPushCategoryUserSetting expectedChild1Setting =
                toClientPushCategoryUserSetting(pushCategoryParent1Child1App1, newLoudPushEnabled);

        // visible == false && configurable == true --> not returned, but changed
        assertFalse(pushCategoryParent1Child2App1.isVisible());
        assertTrue(pushCategoryParent1Child2App1.isConfigurable());
        assertFalse(pushCategoryParent1Child4App1.isVisible());
        assertTrue(pushCategoryParent1Child4App1.isConfigurable());

        // configurable == false --> not returned
        assertFalse(pushCategoryParent1Child3App1.isConfigurable());

        ClientPushCategoryUserSettingChangeResponse expectedResponse =
                ClientPushCategoryUserSettingChangeResponse.builder()
                        .changedPushCategoryUserSetting(expectedSetting)
                        .changedDependentPushCategoryUserSettings(Collections.singletonList(expectedChild1Setting))
                        .build();

        mockMvc.perform(post("/push/categorySetting/{pushCategoryId}", pushCategoryParent1App1.getId())
                .headers(authHeadersFor(person, appVariant))
                .param("loudPushEnabled", Boolean.toString(newLoudPushEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedResponse));

        final PushCategoryUserSetting userSettingParent =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(pushCategoryParent1App1,
                        person, appVariant);

        assertEquals(newLoudPushEnabled, userSettingParent.isLoudPushEnabled());

        final PushCategoryUserSetting userSettingChild1 =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(
                        pushCategoryParent1Child1App1,
                        person, appVariant);

        assertEquals(newLoudPushEnabled, userSettingChild1.isLoudPushEnabled());

        final PushCategoryUserSetting userSettingChild2 =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(
                        pushCategoryParent1Child2App1,
                        person, appVariant);

        //the setting does not need to be saved, since .defaultLoudPushEnabled(false) for this setting
        assertNull(userSettingChild2);

        final PushCategoryUserSetting userSettingChild3 =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(
                        pushCategoryParent1Child3App1,
                        person, appVariant);

        //the setting should not exist, because it is not configurable
        assertNull(userSettingChild3);

        final PushCategoryUserSetting userSettingChild4 =
                pushCategoryUserSettingRepository.findByPushCategoryAndPersonAndAppVariant(
                        pushCategoryParent1Child4App1,
                        person, appVariant);

        assertEquals(newLoudPushEnabled, userSettingChild4.isLoudPushEnabled());
    }

    private ClientPushCategoryUserSetting toClientPushCategoryUserSetting(PushCategory pushCategory,
            boolean loudPushEnabled) {
        return ClientPushCategoryUserSetting.builder()
                .id(pushCategory.getId())
                .name(pushCategory.getName())
                .description(pushCategory.getDescription())
                .loudPushEnabled(loudPushEnabled)
                .parentPushCategoryId(BaseEntity.getIdOf(pushCategory.getParentCategory()))
                .orderValue(pushCategory.getOrderValue())
                .build();
    }

    //set push category user setting for app variant and user, no user
    @Test
    public void changePushCategoryUserSettings_OAuth2AppVariantRequired() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        PushCategory pushCategory = th.pushCategory1App1;

        assertOAuth2AppVariantRequired(post("/push/categorySetting/" + pushCategory.getId())
                        .param("loudPushEnabled", Boolean.toString(!pushCategory.isDefaultLoudPushEnabled())),
                appVariant,
                person);
    }

    //set push category user setting for app variant and user, category not configurable
    @Test
    public void changePushCategoryUserSettingsNotConfigurable() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        PushCategory pushCategory = th.pushCategory2App1;

        assertFalse(pushCategory.isConfigurable());

        mockMvc.perform(post("/push/categorySetting/" + pushCategory.getId())
                .headers(authHeadersFor(person, appVariant))
                .param("loudPushEnabled", Boolean.toString(!pushCategory.isDefaultLoudPushEnabled())))
                .andExpect(isException(pushCategory.getId(), ClientExceptionType.PUSH_CATEGORY_NOT_CONFIGURABLE));
    }

    //set push category user setting for app variant and user, category not of app variant
    @Test
    public void changePushCategoryUserSettingsCategoryNotInAppVariant() throws Exception {
        AppVariant appVariant = th.app1Variant1;
        PushCategory pushCategory = th.pushCategory1App2;

        assertTrue(pushCategory.isConfigurable());
        assertNotEquals(appVariant.getApp(), pushCategory.getApp());

        mockMvc.perform(post("/push/categorySetting/" + pushCategory.getId())
                .headers(authHeadersFor(person, appVariant))
                .param("loudPushEnabled", Boolean.toString(!pushCategory.isDefaultLoudPushEnabled())))
                .andExpect(isException(pushCategory.getId(), ClientExceptionType.PUSH_CATEGORY_NOT_CONFIGURABLE));
    }

}

