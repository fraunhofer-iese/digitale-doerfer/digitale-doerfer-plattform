/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.datadeletion.BaseDataDeletionHandlerTest;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public class GroupDataDeletionHandlerTest extends BaseDataDeletionHandlerTest {

    @Autowired
    private GroupTestHelper th;

    private Group group;

    @Override
    protected BaseSharedTestHelper getTestHelper() {
        return th;
    }

    @Override
    protected void createAdditionalEntities() {
        th.createGroupsAndGroupPostsWithComments();
        group = th.groupAdfcAAP;
        assertThat(group.getTenant()).isEqualTo(tenantToBeDeleted);
        assertThat(th.groupToGeoAreaMapping.get(group).getIncludedGeoAreas()).contains(geoAreaToBeDeleted);
    }

    @Override
    protected void assertAfterDeletion() throws Exception {

        //the group can still be queried
        mockMvc.perform(get("/grapevine/group/{groupId}", th.groupAdfcAAP.getId())
                        .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB)))
                .andExpect(status().isOk());
    }

    @Override
    protected void assertAfterDeletionGeoArea() throws Exception {
        List<GeoArea> geoAreasOfGroup =
                th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group).stream()
                        .map(GroupGeoAreaMapping::getGeoArea)
                        .collect(Collectors.toList());
        assertThat(geoAreasOfGroup).containsExactly(geoAreaToBeUsedInstead);
    }

    @Override
    protected void assertAfterDeletionTenant() throws Exception {

    }

}
