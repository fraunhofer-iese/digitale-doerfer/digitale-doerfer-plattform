/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Tahmid Ekram, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import de.fhg.iese.dd.platform.api.logistics.LogisticsTestHelper;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.business.logistics.services.IPoolingStationService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public class LogisticsDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private LogisticsTestHelper th;

    @Autowired
    private IPoolingStationService poolingStationService;

    private TransportAssignment transportAssignment;
    private TransportConfirmation transportConfirmation;
    private Delivery delivery;
    private Person receiver;
    private Person carrier;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createShops();
        th.createPoolingStations();
        th.createPushEntities();
        th.createAchievementRules();
        th.createScoreEntities();
        th.createBestellBarAppAndAppVariants();

        receiver = getPersonForDeletion();
        carrier = th.personPoolingStationOp;

        // create deliveries and corresponding entities
        createLogisticsDataForDeletionTest();
    }

    @Override
    public Collection<Class<? extends JpaRepository<?, ?>>> getExcludedRepositories() {

        /*
         * These have been deleted in the LogisticsDataDeletionService
         * We skip checking this as the workflow is too complex and is
         * not compatible with the other test workflows.
         */
        return Collections.singleton(ReceiverPickupAssignmentRepository.class);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return th.personRegularKarl;
    }

    @Override
    public Person getPersonForDeletion() {
        return getPersonForPrivacyReport();
    }

    private void createLogisticsDataForDeletionTest() throws Exception {

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = ClientPurchaseOrderReceivedEvent.builder()
                .shopOrderId("shopOrderId" + UUID.randomUUID())
                .communityId(th.tenant1.getId())
                .senderShopId(th.shop1.getId())
                .purchaseOrderNotes("purchaseOrderNotes" + UUID.randomUUID())
                .pickupAddress(ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build())
                .receiverPersonId(receiver.getId())
                .deliveryAddress(ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build())
                .transportNotes("transportNotes" + UUID.randomUUID())
                .orderedItems(Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")))
                .desiredDeliveryTime(new TimeSpan(timeServiceMock.currentTimeMillisUTC(),
                        timeServiceMock.currentTimeMillisUTC() + TimeUnit.HOURS.toMillis(3)))
                .deliveryPoolingStationId("")
                .credits(0)
                .build();

        String deliveryId = toObject(
                mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                        .andExpect(status().isOk()).andReturn(),
                ClientDeliveryCreatedEvent.class)
                .getDeliveryId();

        delivery = th.deliveryRepository.findById(deliveryId).get();

        // Allocate delivery to pooling station box
        poolingStationService.startAllocation(th.poolingStation1, delivery);

        // Manually set estimated delivery time
        delivery.setEstimatedDeliveryTime(new TimeSpan(System.currentTimeMillis(),
                System.currentTimeMillis() + 1000 * 60 * 60 * 2));
        th.deliveryRepository.saveAndFlush(delivery);

        // PurchaseOrderReadyForTransport
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                new ClientPurchaseOrderReadyForTransportRequest(
                        purchaseOrderReceivedEvent.getShopOrderId(),
                        null,
                        ClientDimension.builder()
                                .length(40.0)
                                .width(30.0)
                                .height(20.0)
                                .category(DimensionCategory.M)
                                .weight(0.7)
                                .build(),
                        "contentNotes",
                        PackagingType.PARCEL,
                        1,
                        null);

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                .contentType(contentType)
                .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk());

        TransportAlternative transportAlternative = th.transportAlternativeRepository.findAll().get(0);

        // TransportAlternativeSelectRequest
        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest =
                new ClientTransportAlternativeSelectRequest(transportAlternative.getId());

        String transportAssignmentId = toObject(
                mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                                .headers(authHeadersFor(carrier))
                                .contentType(contentType)
                                .content(json(transportAlternativeSelectRequest)))
                        .andExpect(status().isOk()).andReturn(),
                ClientTransportAlternativeSelectResponse.class)
                .getTransportAssignmentToLoadId();
        transportAssignment = th.transportAssignmentRepository.findById(transportAssignmentId).get();

        // create and fetch a transport confirmation for the delivery
        ClientTransportPickupRequest clientTransportPickupRequest = new ClientTransportPickupRequest();
        clientTransportPickupRequest.setTransportAssignmentId(transportAssignmentId);
        clientTransportPickupRequest.setDeliveryTrackingCode(delivery.getTrackingCode());

        mockMvc.perform(post("/logistics/event/transportPickupRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportPickupRequest)))
                .andExpect(status().isOk());

        transportConfirmation = th.transportConfirmationRepository
                .findByTransportAssignmentPickedUpOrderByCreatedDesc(transportAssignment).get(0);
    }

    @Test
    public void deliveryDataDeletionServiceTest() {

        checkLogisticsDataForDeletionTest();

        IPersonDeletionReport deletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(receiver, deletionReport);

        Delivery erasedDelivery = th.deliveryRepository.findById(delivery.getId()).get();

        assertThat(deletionReport).is(containsEntityWithOperation(erasedDelivery, DeleteOperation.ERASED));
        assertThat(erasedDelivery.getPickupAddress()).isNull();
        assertThat(erasedDelivery.getDeliveryAddress()).isNull();
        assertThat(erasedDelivery.getContentNotes()).is(erasedString());
        assertThat(erasedDelivery.getTransportNotes()).is(erasedString());
        assertThat(erasedDelivery.getDesiredDeliveryTime()).isNull();
        assertThat(erasedDelivery.getEstimatedDeliveryTime()).isNull();
        assertThat(erasedDelivery.getActualDeliveryTime()).is(erasedLong());
        assertThat(erasedDelivery.getCustomReferenceNumber()).is(erasedString());
        assertThat(erasedDelivery.getTrackingCodeLabelURL()).is(erasedString());
        assertThat(erasedDelivery.getTrackingCodeLabelA4URL()).is(erasedString());
        assertThat(erasedDelivery.getSize().getWeight()).is(erasedDouble());
        assertThat(erasedDelivery.getSize().getHeight()).is(erasedDouble());
        assertThat(erasedDelivery.getSize().getLength()).is(erasedDouble());
        assertThat(erasedDelivery.getSize().getWidth()).is(erasedDouble());
        assertThat(th.deliveryStatusRecordRepository.findAllByDeliveryOrderByTimeStampDesc(erasedDelivery)).isEmpty();
        assertThat(
                th.poolingStationBoxAllocationRepository.findAllByDeliveryOrderByCreatedDesc(erasedDelivery)).isEmpty();

        TransportAssignment erasedTransportAssignment = th.transportAssignmentRepository
                .findAllByDeliveryIdOrderByCreatedDesc(erasedDelivery.getId()).get(0);

        assertThat(deletionReport).is(containsEntityWithOperation(erasedTransportAssignment, DeleteOperation.ERASED));
        assertThat(erasedTransportAssignment.getDeliveryAddress()).isNull();
        assertThat(erasedTransportAssignment.getPickupAddress()).isNull();
        assertThat(erasedTransportAssignment.getDesiredDeliveryTime()).isNull();
        assertThat(erasedTransportAssignment.getDesiredPickupTime()).isNull();
        assertThat(erasedTransportAssignment.getLatestDeliveryTime()).is(erasedLong());
        assertThat(erasedTransportAssignment.getLatestPickupTime()).is(erasedLong());

        TransportConfirmation erasedConfirmation = th.transportConfirmationRepository
                .findByTransportAssignmentPickedUpOrderByCreatedDesc(erasedTransportAssignment).get(0);

        assertThat(deletionReport).is(containsEntityWithOperation(erasedConfirmation, DeleteOperation.ERASED));
        assertThat(erasedConfirmation.getNotes()).is(erasedString());

        TransportAlternativeBundle erasedAlternativeBundle = th.transportAlternativeBundleRepository
                .findAllByDeliveryOrderByCreatedDesc(erasedDelivery).get(0);

        assertThat(deletionReport).is(containsEntityWithOperation(erasedAlternativeBundle, DeleteOperation.ERASED));
        assertThat(erasedAlternativeBundle.getPickupAddress()).isNull();

        TransportAlternative erasedAlternative = th.transportAlternativeRepository
                .findAllByTransportAlternativeBundleOrderByCreatedDesc(erasedAlternativeBundle).get(0);

        assertThat(deletionReport).is(containsEntityWithOperation(erasedAlternative, DeleteOperation.ERASED));
        assertThat(erasedAlternative.getDeliveryAddress()).isNull();
        assertThat(erasedAlternative.getDesiredDeliveryTime()).isNull();
        assertThat(erasedAlternative.getDesiredPickupTime()).isNull();
        assertThat(erasedAlternative.getLatestDeliveryTime()).is(erasedLong());
        assertThat(erasedAlternative.getLatestPickupTime()).is(erasedLong());
    }

    @Test
    public void transportDataDeletionServiceTest() {

        checkLogisticsDataForDeletionTest();

        IPersonDeletionReport deletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(carrier, deletionReport);

        assertThat(deletionReport).is(containsEntityWithOperation(transportAssignment, DeleteOperation.DELETED));
        assertFalse(th.transportConfirmationRepository.findById(transportConfirmation.getId()).isPresent());
        assertTrue(th.transportAssignmentStatusRecordRepository
                .findAllByTransportAssignmentIdOrderByTimestampDesc(transportAssignment.getId()).isEmpty());
    }

    private void checkLogisticsDataForDeletionTest() {

        // check that the delivery for the specified person exists
        List<Delivery> deliveries = th.deliveryRepository.findAllByReceiverPersonOrderByCreatedDesc(receiver);
        assertThat(deliveries).hasSize(1);

        // check that specific details of the delivery that need to be deleted actually exist
        Delivery deliveryToBeDeleted = deliveries.get(0);
        assertThat(deliveryToBeDeleted).isEqualTo(delivery);

        assertThat(deliveryToBeDeleted.getPickupAddress()).isNotNull();
        assertThat(deliveryToBeDeleted.getDeliveryAddress()).isNotNull();
        assertThat(deliveryToBeDeleted.getContentNotes()).isNotNull();
        assertThat(deliveryToBeDeleted.getTransportNotes()).isNotNull();
        assertThat(deliveryToBeDeleted.getDesiredDeliveryTime()).isNotNull();
        assertThat(deliveryToBeDeleted.getEstimatedDeliveryTime()).isNotNull();
        assertThat(deliveryToBeDeleted.getActualDeliveryTime()).isNotNull();
        assertThat(deliveryToBeDeleted.getCustomReferenceNumber()).isNotNull();
        assertThat(deliveryToBeDeleted.getSize()).isNotNull();
        assertTrue(!deliveryToBeDeleted.getTrackingCodeLabelURL().isEmpty() ||
                !deliveryToBeDeleted.getTrackingCodeLabelA4URL().isEmpty());

        assertThat(th.deliveryStatusRecordRepository.findAllByDeliveryOrderByTimeStampDesc(deliveryToBeDeleted))
                .isNotEmpty();
        assertThat(th.poolingStationBoxAllocationRepository.findAllByDeliveryOrderByCreatedDesc(deliveryToBeDeleted))
                .isNotEmpty();
        assertThat(th.transportAssignmentRepository.findAllByDeliveryIdOrderByCreatedDesc(deliveryToBeDeleted.getId()))
                .isNotEmpty();
        assertThat(th.transportConfirmationRepository.findByTransportAssignmentPickedUpOrderByCreatedDesc(
                transportAssignment))
                .isNotNull();
        assertThat(th.transportAlternativeBundleRepository.findAllByDeliveryOrderByCreatedDesc(deliveryToBeDeleted))
                .isNotEmpty();
    }

}
