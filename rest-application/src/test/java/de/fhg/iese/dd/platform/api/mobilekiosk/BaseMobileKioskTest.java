/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.business.shopping.services.IPurchaseOrderService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.config.MobileKioskConfig;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public abstract class BaseMobileKioskTest extends BaseServiceTest {

    public Delivery delivery1ForSP4;
    public Delivery delivery2ForSP4;
    public Delivery deliveryForSP5;
    public Delivery deliveryForSP8;

    @Autowired
    protected MobileKioskTestHelper th;

    @Autowired
    protected MobileKioskConfig config;

    @Autowired
    protected ApplicationConfig applicationConfig;
    
    @Autowired
    protected IDeliveryService deliveryService;
    
    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;

    @Autowired
    private IPurchaseOrderService purchaseOrderService;

    @Autowired
    protected ITransportAssignmentService transportAssignmentService;

    @Autowired
    protected ITimeService timeService;

    private final Random random = new Random(System.currentTimeMillis());

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndVgAdmin();
        th.createPersons();
        th.createShops();
        th.createPushEntities();
        th.createScoreEntities();
        th.createAchievementRules();
        th.createVehiclesAndDrivers();
        th.createSellingPoints();
        th.createBestellBarAppAndAppVariants();
    }

    public Delivery createDeliveryForSellingPoint(SellingPoint sellingPoint) throws Exception {

        String shopOrderId = "BAY_" + random.nextInt(9999) + (random.nextBoolean()?"_"+random.nextInt(999):"");

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = ClientPurchaseOrderReceivedEvent.builder()
                .shopOrderId(shopOrderId)
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .senderShopId(th.shop1.getId())
                .purchaseOrderNotes("purchaseOrderNotes" + UUID.randomUUID())
                .pickupAddress(
                        toClientAddressDefinition(th.shop1.getAddresses().stream().findFirst().get().getAddress()))
                .receiverPersonId(th.personRegularAnna.getId())
                .deliveryAddress(toClientAddressDefinition(sellingPoint.getLocation()))
                .transportNotes("transportNotes" + UUID.randomUUID())
            .orderedItems(Arrays.asList(
                new ClientPurchaseOrderItem(2, "item", "unit"),
                new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")))
            .desiredDeliveryTime(new TimeSpan(timeService.currentTimeMillisUTC(), timeService.currentTimeMillisUTC() + TimeUnit.HOURS.toMillis(3L)))
            .credits(8)
            .build();

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andReturn();
     
        ClientDeliveryCreatedEvent deliveryCreatedEvent = toObject(response.getResponse(),
                ClientDeliveryCreatedEvent.class);
        
        return deliveryService.findDeliveryById(deliveryCreatedEvent.getDeliveryId());
    }
    
    public void markDeliveryAsReadyForTransport(Delivery delivery) throws Exception {

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                ClientPurchaseOrderReadyForTransportRequest.builder()
                        .shopOrderId(purchaseOrderService.findByDelivery(delivery).getShopOrderId())
                        .size(ClientDimension.builder()
                                .length(40.0)
                                .width(30.0)
                                .height(20.0)
                                .category(DimensionCategory.M)
                                .weight(0.7)
                                .build())
                        .contentNotes("contentNotes")
                        .packagingType(PackagingType.PARCEL)
                        .numParcels(1)
                        .build();

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        waitForEventProcessingLong();

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, delivery);
    }

    public ClientPurchaseOrderReadyForTransportRequest sendPurchaseOrderReadyForTransportRequest(
            String shopOrderId, Address pickupAddress, int numberAdditionalParcels) throws Exception {

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                ClientPurchaseOrderReadyForTransportRequest.builder()
                        .shopOrderId(shopOrderId)
                        .pickupAddress(toClientAddressDefinition(pickupAddress))
                        .size(ClientDimension.builder()
                                .length(4000.0)
                                .width(3000.0)
                                .height(2000.0)
                                .category(DimensionCategory.OVERSIZED)
                                .weight(0.1)
                                .build())
                        .contentNotes("contentNotes")
                        .packagingType(PackagingType.PARCEL)
                        .numParcels(1)
                        .build();

        if (numberAdditionalParcels > 0) {
            List<ClientPurchaseOrderParcel> additionalParcels = new ArrayList<>();

            for (int i = 0; i < numberAdditionalParcels; i++) {
                additionalParcels.add(ClientPurchaseOrderParcel.builder()
                        .contentNotes("contentNote" + i)
                        .packagingType(PackagingType.PARCEL)
                        .size(ClientDimension.builder()
                                .height((i + 1) * 1000)
                                .width((i + 2) * 1000)
                                .length((i + 3) * 1000)
                                .weight((i + 4) * 1000)
                                .category(DimensionCategory.OVERSIZED)
                                .build())
                        .build());
            }

            purchaseOrderReadyForTransportRequest.setAdditionalParcels(additionalParcels);
        }

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk());

        waitForEventProcessingLong();

        return purchaseOrderReadyForTransportRequest;
    }

    public Delivery createAndConnectDeliveryWithSellingPoint(SellingPoint sellingPoint) throws Exception{

        timeServiceMock.setOffset(-2, ChronoUnit.DAYS);

        Delivery delivery = createDeliveryForSellingPoint(sellingPoint);

        connectDeliveryWithSellingPoint(sellingPoint, delivery);

        markDeliveryAsReadyForTransport(delivery);

        if(!sellingPoint.equals(th.sp10)) {
            //this is super ugly :-(
            assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery).getCarrier(),
                    th.personTruckDriver);
        } else {
            assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery).getCarrier(),
                    th.personCarDriver);
        }

        timeServiceMock.resetOffset();

        return delivery;
    }

    private void connectDeliveryWithSellingPoint(SellingPoint sellingPoint, Delivery delivery) throws Exception {

        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId(delivery.getId())
                .sellingPointId(sellingPoint.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.deliveryId", is(delivery.getId())));

        waitForEventProcessingLong();

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery));
    }

    public void assertThatDeliveryIsInStatus(DeliveryStatus expectedStatus, Delivery delivery) {
        DeliveryStatus currentStatus =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery).getStatus();
        assertSame(expectedStatus, currentStatus);
    }

    protected ClientAddressDefinition toClientAddressDefinition(Address address) {
        return ClientAddressDefinition.builder()
                .name(address.getName())
                .street(address.getStreet())
                .zip(address.getZip())
                .city(address.getCity())
                .gpsLocation(address.getGpsLocation() == null ? null :
                        new ClientGPSLocation(address.getGpsLocation().getLatitude(),
                                address.getGpsLocation().getLongitude()))
                .build();
    }

}
