/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Offer;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Seeking;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Trade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GrapevineAdminControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    @SuppressWarnings("Duplicates")
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void adminEndpointsUnauthorized() throws Exception {

        assertRequiresAdmin(delete("/administration/grapevine/suggestionCategory/Id"));
        assertRequiresAdmin(delete("/administration/grapevine/tradingCategory/Id"));
    }

    @Test
    public void deleteTradingCategory() throws Exception {

        final Offer offerMisc = th.offer1;
        final Offer offerTools = th.offer2;
        final Offer offerService1 = th.offer3;
        final Offer offerService2 = th.offer4;
        final Seeking seekingMisc = th.seeking1;
        final Seeking seekingTools = th.seeking4;
        final Seeking seekingService = th.seeking3;

        //verify test data integrity
        assertThat(offerMisc.getTradingCategory()).isEqualTo(th.tradingCategoryMisc);
        assertThat(offerTools.getTradingCategory()).isEqualTo(th.tradingCategoryTools);
        assertThat(offerService1.getTradingCategory()).isEqualTo(th.tradingCategoryService);
        assertThat(offerService2.getTradingCategory()).isEqualTo(th.tradingCategoryService);
        assertThat(seekingMisc.getTradingCategory()).isEqualTo(th.tradingCategoryMisc);
        assertThat(seekingTools.getTradingCategory()).isEqualTo(th.tradingCategoryTools);
        assertThat(seekingService.getTradingCategory()).isEqualTo(th.tradingCategoryService);

        //delete service category
        mockMvc.perform(delete("/administration/grapevine/tradingCategory/" + th.tradingCategoryService.getId())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk());

        assertFalse(th.tradingCategoryRepository.existsById(th.tradingCategoryService.getId()));

        assertThat(((Trade) th.postRepository.findById(offerMisc.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryMisc);
        assertThat(((Trade) th.postRepository.findById(offerTools.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryTools);
        assertThat(((Trade) th.postRepository.findById(offerService1.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryMisc); //changed!
        assertThat(((Trade) th.postRepository.findById(offerService2.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryMisc); //changed!
        assertThat(((Trade) th.postRepository.findById(seekingMisc.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryMisc);
        assertThat(((Trade) th.postRepository.findById(seekingTools.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryTools);
        assertThat(((Trade) th.postRepository.findById(seekingService.getId()).get()).getTradingCategory())
                .isEqualTo(th.tradingCategoryMisc); //changed!
    }

    @Test
    public void deleteDefaultTradingCategory() throws Exception {

        mockMvc.perform(delete("/administration/grapevine/tradingCategory/" + th.tradingCategoryMisc.getId())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(isException(ClientExceptionType.DEFAULT_TRADING_CATEGORY_MUST_NOT_BE_DELETED));
    }

    @Test
    public void deleteTradingCategoryUnauthorizedOrNotAdmin() throws Exception {

        assertOAuth2(delete("/administration/grapevine/tradingCategory/" + th.tradingCategoryMisc.getId()));

        mockMvc.perform(delete("/administration/grapevine/tradingCategory/" + th.tradingCategoryMisc.getId())
                .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void deleteSuggestionCategory() throws Exception {

        final Suggestion suggestionDefect1 = th.suggestionDefect1;
        final Suggestion suggestionDefect2 = th.suggestionDefect2;
        final Suggestion suggestionIdea1 = th.suggestionIdea1;
        final Suggestion suggestionIdea2 = th.suggestionIdea2;
        final Suggestion suggestionWish1 = th.suggestionWish1;
        final Suggestion suggestionWish2 = th.suggestionWish2;

        // verify test data integrity
        assertThat(suggestionDefect1.getSuggestionCategory()).isEqualTo(th.suggestionCategoryDamage);
        assertThat(suggestionDefect2.getSuggestionCategory()).isEqualTo(th.suggestionCategoryWaste);
        assertThat(suggestionIdea1.getSuggestionCategory()).isEqualTo(th.suggestionCategoryRoadDamage);
        assertThat(suggestionIdea2.getSuggestionCategory()).isEqualTo(th.suggestionCategoryWaste);
        assertThat(suggestionWish1.getSuggestionCategory()).isEqualTo(th.suggestionCategoryWish);
        assertThat(suggestionWish2.getSuggestionCategory()).isEqualTo(th.suggestionCategoryWish);

        // delete WISH category
        mockMvc.perform(delete("/administration/grapevine/suggestionCategory/" + th.suggestionCategoryWish.getId())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk());

        // check that category is deleted
        assertFalse(th.suggestionCategoryRepository.existsById(th.suggestionCategoryWish.getId()));

        // check all suggestions with category WISH have been shifted to MISC
        assertThat(((Suggestion) th.postRepository.findById(suggestionDefect1.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryDamage);
        assertThat(((Suggestion) th.postRepository.findById(suggestionDefect2.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryWaste);
        assertThat(((Suggestion) th.postRepository.findById(suggestionIdea1.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryRoadDamage);
        assertThat(((Suggestion) th.postRepository.findById(suggestionIdea2.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryWaste);
        assertThat(((Suggestion) th.postRepository.findById(suggestionWish1.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryMisc); // changed!
        assertThat(((Suggestion) th.postRepository.findById(suggestionWish2.getId()).get()).getSuggestionCategory())
                .isEqualTo(th.suggestionCategoryMisc); // changed!
    }

    @Test
    public void deleteDefaultSuggestionCategory() throws Exception {
        mockMvc.perform(delete("/administration/grapevine/suggestionCategory/" + th.suggestionCategoryMisc.getId())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(isException(ClientExceptionType.DEFAULT_SUGGESTION_CATEGORY_MUST_NOT_BE_DELETED));
    }

    @Test
    public void deleteSuggestionCategoryUnauthorizedOrNotAdmin() throws Exception {

        assertOAuth2(delete("/administration/grapevine/suggestionCategory/" + th.suggestionCategoryMisc.getId()));

        mockMvc.perform(delete("/administration/grapevine/suggestionCategory/" + th.suggestionCategoryMisc.getId())
                .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(isNotAuthorized());
    }

    private void assertRequiresAdmin(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertOAuth2(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
