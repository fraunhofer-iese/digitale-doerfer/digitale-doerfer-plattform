/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagStatusChangeRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagStatusChangeResponse;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.BaseUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserGeneratedContentFlagAdminUiEventControllerTest extends BaseUserGeneratedContentFlagAdminUiTest {

    @Autowired
    private TestEmailSenderService emailSenderService;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createUserGeneratedContentAdmins();
        super.createFlags();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    /**
     * This handler will fail on Tenant entities
     */
    @Component
    static class ErrorHandler extends BaseUserGeneratedContentFlagHandler {

        @Override
        public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
            return Collections.singleton(Tenant.class);
        }

        @Override
        public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) {
            throw new RuntimeException("Exception on delete");
        }

    }

    /**
     * This handler "fakes" the deletion of a flagged media item
     */
    @Component
    @Log4j2
    static class FakeHandler extends BaseUserGeneratedContentFlagHandler {

        @Autowired
        private MediaItemRepository mediaItemRepository;

        @Override
        public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
            return Collections.singleton(MediaItem.class);
        }

        @Override
        public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) {
            MediaItem mediaItem = mediaItemRepository.findById(flag.getEntityId()).get();
            mediaItem.setUrlsJson("");
            mediaItemRepository.save(mediaItem);
            log.info("Fake deleted entity {} for flag {}", flag.getEntityId(), flag.getId());
            return DeletedFlagReport.builder()
                    .appName("DorfFunk")
                    .reportedText("Bild")
                    .build();
        }

    }

    /**
     * These handlers are intended to test failing on multiple handlers for same entity
     */
    @Component
    static class FirstHandlerForRoleAssignmentEntity extends BaseUserGeneratedContentFlagHandler {

        @Override
        public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
            return Collections.singleton(RoleAssignment.class);
        }

        @Override
        public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) {
            return null;
        }

    }

    @Component
    static class SecondHandlerForRoleAssignmentEntity extends BaseUserGeneratedContentFlagHandler {

        @Override
        public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
            return Collections.singleton(RoleAssignment.class);
        }

        @Override
        public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) {
            return null;
        }

    }

    @Test
    public void flagStatusChangeRequest_ChangeFlagStatus() throws Exception {

        UserGeneratedContentFlag flag = flag1Tenant1;
        String comment = "Ich kümmere mich drum";
        long changeTime = System.currentTimeMillis() + 1042;
        timeServiceMock.setConstantDateTime(changeTime);

        int sizeBeforeUpdate =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag).size();
        assertThat(flag.getUserGeneratedContentFlagStatusRecords()).hasSize(sizeBeforeUpdate);
        assertThat(sizeBeforeUpdate).isGreaterThan(0);

        UserGeneratedContentFlagStatus newStatus = UserGeneratedContentFlagStatus.IN_PROGRESS;
        ClientUserGeneratedContentFlagStatusChangeRequest request =
                ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag.getId())
                        .newStatus(newStatus)
                        .comment(comment)
                        .build();

        ClientUserGeneratedContentFlagStatusChangeResponse response =
                toObject(mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                                .contentType(contentType)
                                .content(json(request)))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(contentType))
                                .andReturn(),
                        ClientUserGeneratedContentFlagStatusChangeResponse.class);

        final List<UserGeneratedContentFlagStatusRecord> actualStatusRecords =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag);
        assertThat(actualStatusRecords).hasSize(sizeBeforeUpdate + 1);
        final UserGeneratedContentFlagStatusRecord newStatusRecord = actualStatusRecords.get(0);
        assertEquals(newStatus, newStatusRecord.getStatus());
        assertEquals(changeTime, newStatusRecord.getCreated());
        assertEquals(comment, newStatusRecord.getComment());

        final UserGeneratedContentFlag updatedFlag = flagRepository.findById(flag.getId()).get();
        assertEquals(newStatus, updatedFlag.getStatus());
        assertEquals(changeTime, updatedFlag.getLastStatusUpdate());

        List<UserGeneratedContentFlagStatusRecord> newStatusRecords = new ArrayList<>();
        newStatusRecords.add(newStatusRecord);
        newStatusRecords.addAll(flag.getUserGeneratedContentFlagStatusRecords());

        flag.setStatus(newStatus);
        flag.setLastStatusUpdate(changeTime);
        flag.setUserGeneratedContentFlagStatusRecords(newStatusRecords);

        assertFlagEquals(flag, response.getChangedFlag());
    }

    @Test
    public void flagStatusChangeRequest_AddCommentSameFlagStatus() throws Exception {

        final UserGeneratedContentFlag flag = flag1Tenant1;
        final String comment = "Ich kümmere mich drum";
        final UserGeneratedContentFlagStatus newStatus = UserGeneratedContentFlagStatus.OPEN;

        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag.getId())
                        .newStatus(newStatus)
                        .comment(comment)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.changedFlag.status").value(newStatus.name()))
                .andExpect(jsonPath("$.changedFlag.statusRecords[0].comment").value(comment));
    }

    @Test
    public void flagStatusChangeRequest_FlagWithoutTenant() throws Exception {

        final UserGeneratedContentFlag flag = flagNoTenant;
        final String comment = "Da fehlt ja der Mandant!";
        final UserGeneratedContentFlagStatus newStatus = UserGeneratedContentFlagStatus.OPEN;

        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag.getId())
                        .newStatus(newStatus)
                        .comment(comment)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.changedFlag.status").value(newStatus.name()))
                .andExpect(jsonPath("$.changedFlag.statusRecords[0].comment").value(comment));
    }

    @Test
    public void flagStatusChangeRequest_InvalidAttributes() throws Exception {

        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .newStatus(UserGeneratedContentFlagStatus.IN_PROGRESS)
                        .comment("Ich mache heute home office")
                        .build())))
                .andExpect(isException("flagId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag1Tenant1.getId())
                        .comment("Heute komme ich mal wieder später")
                        .build())))
                .andExpect(isException("newStatus", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag1Tenant1.getId())
                        .newStatus(UserGeneratedContentFlagStatus.IN_PROGRESS)
                        .build())))
                .andExpect(isException("comment", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void flagStatusChangeRequest_InvalidTransition() throws Exception {

        UserGeneratedContentFlag flag = flag3Tenant1;
        final String comment = "Ich mache alles!";

        UserGeneratedContentFlagStatus newStatus = UserGeneratedContentFlagStatus.IN_PROGRESS;
        ClientUserGeneratedContentFlagStatusChangeRequest request =
                ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag.getId())
                        .newStatus(newStatus)
                        .comment(comment)
                        .build();

        // Try to change flag status REJECTED -> IN_PROGRESS
        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_STATUS_CHANGE_INVALID));
    }

    @Test
    public void flagStatusChangeRequest_NotAuthorized() throws Exception {

        ClientUserGeneratedContentFlagStatusChangeRequest changeRequest =
                ClientUserGeneratedContentFlagStatusChangeRequest.builder()
                        .flagId(flag1Tenant1.getId())
                        .newStatus(UserGeneratedContentFlagStatus.IN_PROGRESS)
                        .comment("Heute muss ich früher gehen")
                        .build();

        assertOAuth2(post("/adminui/flag/event/flagStatusChangeRequest")
                .contentType(contentType)
                .content(json(changeRequest)));

        //no permission
        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .contentType(contentType)
                .headers(authHeadersFor(th.personRegularKarl))
                .content(json(changeRequest)))
                .andExpect(isNotAuthorized());

        //no permission for this tenant
        mockMvc.perform(post("/adminui/flag/event/flagStatusChangeRequest")
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2))
                .content(json(changeRequest)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void deleteFlagEntityByAdminRequest() throws Exception {

        final Person person = th.personGlobalUserGeneratedContentAdmin;
        UserGeneratedContentFlag flag = flag1Tenant1;
        BaseEntity flaggedEntity = getFlagEntityFlag1Tenant1();
        String comment = "Das Bild ist so hässlich!";

        //this will trigger the fake handler above
        assertThat(flaggedEntity).isInstanceOf(MediaItem.class);
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        //the fake handler will set it to empty
        assertThat(((MediaItem) flaggedEntity).getUrlsJson()).isNotEmpty();

        long changeTime = System.currentTimeMillis() + 1042;
        timeServiceMock.setConstantDateTime(changeTime);

        int sizeBeforeUpdate =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag).size();
        assertThat(flag.getUserGeneratedContentFlagStatusRecords()).hasSize(sizeBeforeUpdate);
        assertThat(sizeBeforeUpdate).isGreaterThan(0);

        ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse response =
                toObject(mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                                .headers(authHeadersFor(person))
                                .contentType(contentType)
                                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                                        .flagId(flag.getId())
                                        .comment(comment)
                                        .build())))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(contentType))
                                .andReturn(),
                        ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse.class);

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());

        //the fake handler has set it to empty, this proofs that it was triggered
        assertThat(th.mediaItemRepository.findById(flag.getEntityId()).get().getUrlsJson()).isEmpty();

        final List<UserGeneratedContentFlagStatusRecord> actualStatusRecords =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag);
        assertThat(actualStatusRecords).hasSize(sizeBeforeUpdate + 1);
        final UserGeneratedContentFlagStatusRecord newStatusRecord = actualStatusRecords.get(0);
        assertEquals(UserGeneratedContentFlagStatus.ACCEPTED, newStatusRecord.getStatus());
        assertEquals(changeTime, newStatusRecord.getCreated());
        assertEquals(comment, newStatusRecord.getComment());

        final UserGeneratedContentFlag updatedFlag = flagRepository.findById(flag.getId()).get();
        assertEquals(UserGeneratedContentFlagStatus.ACCEPTED, updatedFlag.getStatus());
        assertEquals(changeTime, updatedFlag.getLastStatusUpdate());

        List<UserGeneratedContentFlagStatusRecord> newStatusRecords = new ArrayList<>();
        newStatusRecords.add(newStatusRecord);
        newStatusRecords.addAll(flag.getUserGeneratedContentFlagStatusRecords());

        flag.setStatus(UserGeneratedContentFlagStatus.ACCEPTED);
        flag.setLastStatusUpdate(changeTime);
        flag.setUserGeneratedContentFlagStatusRecords(newStatusRecords);

        assertFlagEquals(flag, response.getChangedFlag());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_NonDeleteableEntity() throws Exception {

        final Person person = th.personUserGeneratedContentAdminTenant2;
        UserGeneratedContentFlag flag = flag1Tenant2;
        BaseEntity flaggedEntity = getFlagEntityFlag1Tenant2();
        //geo areas can not be deleted (that way)!
        assertThat(flaggedEntity).isInstanceOf(GeoArea.class);
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag.getId())
                        .comment("Ich lösch das jetzt einfach mal so, das geht bestimmt!")
                        .build())))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_CAN_NOT_BE_DELETED));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_InvalidType() throws Exception {

        final Person person = th.personUserGeneratedContentAdminTenant1;
        UserGeneratedContentFlag flag = flag1Tenant1;
        flag.setEntityType("platypus");
        flagRepository.save(flag);

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag.getId())
                        .comment("Die sind so süß!")
                        .build())))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_INVALID));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());

        flag.setEntityType("");
        flagRepository.save(flag);

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag.getId())
                        .comment("Ja, nö, doch nicht")
                        .build())))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_INVALID));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_MultipleHandlersForSameEntity() throws Exception {

        final Person person = th.personUserGeneratedContentAdminTenant1;
        UserGeneratedContentFlag flag = flag1Tenant1;
        flag.setEntityId(th.tenant1.getId());
        flag.setEntityType(RoleAssignment.class.getName());
        flagRepository.save(flag);

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag.getId())
                        .comment("Was jetzt wohl passiert? 🤔")
                        .build())))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_INVALID));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_ExceptionDuringDeletion() throws Exception {

        final Person person = th.personUserGeneratedContentAdminTenant1;
        //this flag will trigger the error handler above
        UserGeneratedContentFlag flag = flag1Tenant1;
        flag.setEntityId(th.tenant1.getId());
        flag.setEntityType(Tenant.class.getName());
        flagRepository.save(flag);

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag.getId())
                        .comment("Was jetzt wohl passiert? 🤔")
                        .build())))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ENTITY_DELETION_FAILED));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_InvalidAttributes() throws Exception {

        final Person person = th.personUserGeneratedContentAdminTenant1;

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .comment("Reminder: Heute habe ich Urlaub!")
                        .build())))
                .andExpect(isException("flagId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());

        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag1Tenant1.getId())
                        .build())))
                .andExpect(isException("comment", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail());
    }

    @Test
    public void deleteFlagEntityByAdminRequest_NotAuthorized() throws Exception {

        ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest deleteRequest =
                ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                        .flagId(flag1Tenant1.getId())
                        .comment("Ich drück da mal druff ⚡")
                        .build();

        assertOAuth2(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .contentType(contentType)
                .content(json(deleteRequest)));

        final Person person1 = th.personRegularKarl;
        //no permission
        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .contentType(contentType)
                .headers(authHeadersFor(person1))
                .content(json(deleteRequest)))
                .andExpect(isNotAuthorized());

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person1.getEmail());

        final Person person2 = th.personUserGeneratedContentAdminTenant2;
        //no permission for this tenant
        mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                .contentType(contentType)
                .headers(authHeadersFor(person2))
                .content(json(deleteRequest)))
                .andExpect(isNotAuthorized());

        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person2.getEmail());
    }

}
