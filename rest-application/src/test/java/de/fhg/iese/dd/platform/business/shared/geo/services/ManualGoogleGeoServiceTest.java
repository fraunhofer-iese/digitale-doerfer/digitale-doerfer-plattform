/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import static de.fhg.iese.dd.platform.api.BaseServiceTest.assertAddressContentsAreEqual;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

/**
 * This test is useful for trying out the Google geocoding API for testing and troubleshooting issues
 */
@Disabled("only for manual testing")
public class ManualGoogleGeoServiceTest {
    //FIXME: Logging does not work when running these test

    private IGeoService geoService;
    private static final String LOCATION_NAME = "test location";

    @BeforeEach
    public void init() throws Exception {
        if (geoService == null) {
            geoService = TestGoogleGeoService.initializeWithoutApplicationContext();
        }
    }

    @Test
    public void manualFiddling() throws Exception {

        Address address = geoService.resolveLocation("test", new GPSLocation(49.451111, 7.735445));
        System.out.println(address);
        address = geoService.resolveLocation("test", new GPSLocation(49.449259, 7.739804));
        System.out.println(address);
        address = geoService.resolveLocation("test", new GPSLocation(49.534192, 7.786251));

        //address = geoService.resolveLocation("test", "Fraunhofer-Platz 1, Kaiserslautern");
        System.out.println(address);

        System.out.println(geoService.getGPSLocation(IAddressService.AddressDefinition.builder().build()));
    }

    @Test
    public void resolveLocationByGps_locationNearStreetAddress() {

        final GPSLocation gpsLocation = new GPSLocation(49.449156, 7.739729);
        final Address expectedAddress = Address.builder()
                .name(LOCATION_NAME)
                .street("Königsberger Straße 3")
                .zip("67659")
                .city("Kaiserslautern")
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();

        final Address foundAddress = geoService.resolveLocation(LOCATION_NAME, gpsLocation);

        assertAddressContentsAreEqual(expectedAddress, foundAddress);
    }

    @Test
    public void resolveLocationByGps_locationTooFarFromStreetAddress() {

        final GPSLocation gpsLocation = new GPSLocation(49.451111, 7.735445);
        final Address expectedAddress = Address.builder()
                .name(LOCATION_NAME)
                .street(null)
                .zip("67659")
                .city("Kaiserslautern")
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();

        final Address foundAddress = geoService.resolveLocation(LOCATION_NAME, gpsLocation);

        assertAddressContentsAreEqual(expectedAddress, foundAddress);
    }

    @Test
    public void resolveLocationByGps_locationUnnamedRoad() {

        final GPSLocation gpsLocation = new GPSLocation(49.534192, 7.786251);
        final Address expectedAddress = Address.builder()
                .name(LOCATION_NAME)
                .street(null)
                .zip("67697")
                .city("Otterberg")
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();

        final Address foundAddress = geoService.resolveLocation(LOCATION_NAME, gpsLocation);

        assertAddressContentsAreEqual(expectedAddress, foundAddress);
    }

    @Test
    public void resolveLocationByGps_locationWithoutStreet() {

        final GPSLocation gpsLocation = new GPSLocation(49.654670, 7.771685);
        final Address expectedAddress = Address.builder()
                .name(LOCATION_NAME)
                .street("K11")
                .zip("67808")
                .city("Ransweiler")
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();

        final Address foundAddress = geoService.resolveLocation(LOCATION_NAME, gpsLocation);

        assertAddressContentsAreEqual(expectedAddress, foundAddress);
    }

    @Test
    public void resolveLocationByLocationLookupString_DD4760() {

        final GPSLocation gpsLocation = new GPSLocation(48.6177086, 8.127878100000002);
        final Address expectedAddress = Address.builder()
                .name(LOCATION_NAME)
                .street(null)
                .zip("77887")
                .city("Sasbachwalden")
                .gpsLocation(gpsLocation)
                .verified(true)
                .build();

        final Address foundAddress = geoService.resolveLocation(LOCATION_NAME, "Talstraße 35-33, Sasbachwalden");
        System.out.println(foundAddress.toHumanReadableString(false));

        assertAddressContentsAreEqual(expectedAddress, foundAddress);
    }

    @Test
    public void resolveLocationByGps_locationWithoutAddress() {

        final GPSLocation gpsLocation = new GPSLocation(49.874172, -34.404397);

        assertNull(geoService.resolveLocation(LOCATION_NAME, gpsLocation));
    }

}
