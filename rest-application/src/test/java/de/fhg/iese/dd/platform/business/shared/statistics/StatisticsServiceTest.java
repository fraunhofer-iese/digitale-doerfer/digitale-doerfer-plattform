/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.statistics;

import static de.fhg.iese.dd.platform.business.participants.person.statistics.PersonStatisticsProvider.STATISTICS_ID_PERSON_ACTIVE;
import static de.fhg.iese.dd.platform.business.participants.person.statistics.PersonStatisticsProvider.STATISTICS_ID_PERSON_CREATED;
import static de.fhg.iese.dd.platform.business.participants.person.statistics.PersonStatisticsProvider.STATISTICS_ID_PERSON_DELETED;
import static de.fhg.iese.dd.platform.business.participants.person.statistics.PersonStatisticsProvider.STATISTICS_ID_PERSON_UNDELETED;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.ACTIVE;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.NEW;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.TOTAL;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public class StatisticsServiceTest extends BaseServiceTest {

    private static final boolean SAVE_REPORTS = false;

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private IStatisticsService statisticsService;

    @Autowired
    private Set<IStatisticsProvider> statisticsProviders;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void generateReport_AllStatistics() throws IOException {

        long currentTime = timeServiceMock.currentTimeMillisUTC();
        timeServiceMock.setConstantDateTime(currentTime);
        long startTimeNew = currentTime - TimeUnit.DAYS.toMillis(7);
        long statTimeActive = currentTime - TimeUnit.DAYS.toMillis(1);
        long endTime = currentTime - 1;
        List<StatisticsMetaData> statisticsMetaData = th.getAllStatisticsMetaData();

        List<String> statisticsIds = statisticsMetaData.stream()
                .map(StatisticsMetaData::getId)
                .collect(Collectors.toList());

        List<String> statisticsMachineNames = statisticsMetaData.stream()
                .map(StatisticsMetaData::getMachineName)
                .collect(Collectors.toList());

        List<String> geoAreaNames = th.geoAreaList.stream()
                .map(GeoArea::getName)
                .collect(Collectors.toList());

        List<String> geoAreaIds = th.geoAreaList.stream()
                .map(GeoArea::getId)
                .collect(Collectors.toList());

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsIds(statisticsIds)
                .startTimeNew(startTimeNew)
                .startTimeActive(statTimeActive)
                .endTime(endTime)
                .allGeoAreas(true)
                .build();

        StatisticsReport report = statisticsService.generateStatisticsReport(statisticsReportDefinition);

        GeoAreaStatistics statisticsDe = findStatisticsFor(report, th.geoAreaDeutschland);
        assertThat(statisticsDe.getStatisticsValue(STATISTICS_ID_PERSON_CREATED, NEW).getValue()).isEqualTo(11L);
        assertThat(statisticsDe.getStatisticsValue(STATISTICS_ID_PERSON_UNDELETED, TOTAL).getValue()).isEqualTo(13L);
        assertThat(statisticsDe.getStatisticsValue(STATISTICS_ID_PERSON_DELETED, TOTAL).getValue()).isEqualTo(1L);
        assertThat(statisticsDe.getStatisticsValue(STATISTICS_ID_PERSON_ACTIVE, ACTIVE).getValue()).isEqualTo(12L);

        String csvReport = statisticsService.reportToCsv(report, ";");
        String csvMetadata = statisticsService.reportStatisticsMetadataToCsv(report, ";");
        String textReport = statisticsService.reportToText(report);

        assertThat(csvReport).contains(geoAreaNames);
        assertThat(csvReport).contains(geoAreaIds);
        assertThat(csvReport).contains(statisticsMachineNames);
        assertThat(csvMetadata).contains(statisticsMachineNames);
        assertThat(textReport).contains(geoAreaNames);
        if (SAVE_REPORTS) {
            saveFile(csvReport, "csv report", ".csv");
            saveFile(csvMetadata, "csv metadata", ".csv");
            saveFile(textReport, "text report", ".txt");
        }
    }

    private void saveFile(String content, String name, String suffix) throws IOException {
        Path tempFileCsv = Files.createTempFile(getClass().getSimpleName() + "-all-", suffix);
        log.info("Saving {} to {}", name, tempFileCsv.toUri());
        Files.write(tempFileCsv, content.getBytes(StandardCharsets.UTF_8));
    }

    private GeoAreaStatistics findStatisticsFor(StatisticsReport statisticsReport, GeoArea geoArea) {
        return statisticsReport.getGeoAreaStatistics().stream()
                .filter(geoAreaStatistics -> geoArea.equals(geoAreaStatistics.getGeoArea()))
                .findFirst()
                .get();
    }

}
