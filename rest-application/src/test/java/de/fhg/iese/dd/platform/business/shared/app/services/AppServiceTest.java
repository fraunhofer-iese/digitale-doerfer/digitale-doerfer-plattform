/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.app.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.ZoneOffset;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;

public class AppServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private IAppService appService;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void createOrUpdateAppVariantUsage() {
        long lastUsage = 0L;
        Person person = th.personRegularKarl;
        AppVariant appVariant = th.app1Variant1;
        AppVariantUsage appVariantUsage = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(appVariant)
                .person(person)
                .lastUsage(lastUsage)
                .build());

        long nowTime = lastUsage + IAppService.LAST_APP_VARIANT_USAGE_PERIOD + 1;

        assertTrue(nowTime - lastUsage > IAppService.LAST_APP_VARIANT_USAGE_PERIOD, "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        AppVariantUsage updateResult = appService.createOrUpdateAppVariantUsage(appVariant, person, false);
        AppVariantUsage updateInDB = th.appVariantUsageRepository.findById(appVariantUsage.getId()).get();

        assertEquals(appVariantUsage, updateInDB);
        assertEquals(nowTime, updateResult.getLastUsage());
        assertEquals(nowTime, updateInDB.getLastUsage());
    }

    @Test
    public void createOrUpdateAppVariantUsage_NotInUpdatePeriod_NoReturn() {
        long lastUsage = 12021983L;
        Person person = th.personRegularKarl;
        AppVariant appVariant = th.app1Variant1;
        AppVariantUsage appVariantUsage = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(appVariant)
                .person(person)
                .lastUsage(lastUsage)
                .build());

        long nowTime = lastUsage + IAppService.LAST_APP_VARIANT_USAGE_PERIOD - 1;

        assertTrue(nowTime - lastUsage < IAppService.LAST_APP_VARIANT_USAGE_PERIOD, "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        AppVariantUsage updateResult = appService.createOrUpdateAppVariantUsage(appVariant, person, false);

        AppVariantUsage updateInDB = th.appVariantUsageRepository.findById(appVariantUsage.getId()).get();

        //it should not be returned, since it is not changed
        assertNull(updateResult);
        assertEquals(lastUsage, updateInDB.getLastUsage());
    }

    @Test
    public void createOrUpdateAppVariantUsage_NotInUpdatePeriod_Return() {
        long lastUsage = 12021983L;
        Person person = th.personRegularKarl;
        AppVariant appVariant = th.app1Variant1;
        AppVariantUsage appVariantUsage = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(appVariant)
                .person(person)
                .lastUsage(lastUsage)
                .build());

        long nowTime = lastUsage + IAppService.LAST_APP_VARIANT_USAGE_PERIOD - 1;

        assertTrue(nowTime - lastUsage < IAppService.LAST_APP_VARIANT_USAGE_PERIOD, "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        AppVariantUsage updateResult = appService.createOrUpdateAppVariantUsage(appVariant, person, true);

        AppVariantUsage updateInDB = th.appVariantUsageRepository.findById(appVariantUsage.getId()).get();

        assertEquals(updateResult, updateInDB);
        assertEquals(lastUsage, updateResult.getLastUsage());
        assertEquals(lastUsage, updateInDB.getLastUsage());
    }

    @Test
    public void createOrUpdateAppVariantUsage_InvalidParameters() {
        appService.createOrUpdateAppVariantUsage(null, th.personRegularAnna, false);

        appService.createOrUpdateAppVariantUsage(th.app1Variant1, null, false);
    }

    @Test
    public void createOrUpdateAppVariantUsage_NoAppVariantUsage() {
        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        assertEquals(0, th.appVariantUsageRepository.count());
        AppVariantUsage updateResult =
                appService.createOrUpdateAppVariantUsage(th.app1Variant1, th.personRegularAnna, false);

        assertEquals(1, th.appVariantUsageRepository.count());
        AppVariantUsage updateInDB = th.appVariantUsageRepository.findById(updateResult.getId()).get();

        assertEquals(nowTime, updateResult.getLastUsage());
        assertEquals(nowTime, updateInDB.getLastUsage());
    }

}
