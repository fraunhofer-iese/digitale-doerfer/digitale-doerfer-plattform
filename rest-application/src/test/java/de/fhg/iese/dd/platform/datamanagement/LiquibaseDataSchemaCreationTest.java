/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import de.fhg.iese.dd.platform.api.BaseServiceTest;

@ActiveProfiles({"test", "liquibase-data-schema-creation-test"})
public class LiquibaseDataSchemaCreationTest extends BaseServiceTest {

    private static final String MARKER_NO_VALUE_CHECK = "no-value-check";

    @Override
    public void createEntities() throws Exception {

    }

    @Override
    public void tearDown() throws Exception {

    }

    @Test
    public void liquibaseHasCreatedSchemaAndSpringHasVerifiedIt() {
        assertTrue(true, "This statement should be reached without errors, indicating the all database" +
                " tables have been set up correctly by liquibase");
    }

    @Test
    public void liquibaseChangelogHasValueSetInAllAddColumnChanges()
            throws ParserConfigurationException, IOException, SAXException {

        // Path to current liquibase changelog
        final String changelogFilePath = "src/main/resources/liquibase/liquibase-changeLog-changes-2018-08.xml";

        // Exclude existing changeSetNodes without a set value
        final Set<String> excludedChangeSetNodes = new HashSet<>(Arrays.asList(
                "2018-09-19_13-52",
                "2018-10-08_11-00",
                "2018-10-15_10-42",
                "2018-10-23_14-22",
                "2018-12-03_14-11",
                "2018-12-03_15-08",
                "2019-02-15_15-39",
                "2019-02-18_11-49",
                "2019-02-25_18-11",
                "2019-03-03_20-00",
                "2019-04-19_11-25",
                "2019-05-26_22-20",
                "2019-06-12_13-53",
                "2019-06-30_17-45",
                "2019-07-09_20-45",
                "2019-08-08_08-35"
        ));

        // Load liquibase changelog file and parse it
        File changelogFile = new File(changelogFilePath);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(changelogFile);
        doc.getDocumentElement().normalize();

        // First get all elements with changeSet-tag
        NodeList changeSetNodeList = doc.getElementsByTagName("changeSet");

        // Iterate through all changeSetNodes
        for (int i = 0; i < changeSetNodeList.getLength(); i++) {

            Node changeSetNode = changeSetNodeList.item(i);

            // If current changeSetNode is defined in excludedChangeSetNodes, skip this iteration
            if (excludedChangeSetNodes.contains(changeSetNode.getAttributes().getNamedItem("id").getNodeValue())) {
                continue;
            }

            // Get all containing childNodes
            NodeList addColumnNodeList = changeSetNode.getChildNodes();

            // Iterate through all nodes of a changeSetNode
            for (int j = 0; j < addColumnNodeList.getLength(); j++) {

                Node addColumnNode = addColumnNodeList.item(j);

                // If current node has addColumn-tag, check childNodes for column-tags
                if (StringUtils.equals(addColumnNode.getNodeName(), "addColumn")) {

                    // Get all containing childNodes
                    NodeList columnNodeList = addColumnNode.getChildNodes();

                    // Iterate through all nodes of addColumnNode
                    for (int k = 0; k < columnNodeList.getLength(); k++) {

                        Node columnNode = columnNodeList.item(k);

                        if (StringUtils.equals(columnNode.getNodeName(), "column")) {

                            //Skip check if addColumn is preceded by a comment with no-value-check
                            final Comment precedingComment = findPrecedingCommentOrNullSkippingTextNodes(columnNode);
                            if (precedingComment != null &&
                                    StringUtils.contains(precedingComment.getNodeValue(), MARKER_NO_VALUE_CHECK)) {
                                continue;
                            }

                            // Checks if an attribute of name value is existing
                            Node value = columnNode.getAttributes().getNamedItem("value");
                            if (value == null) {
                                value = columnNode.getAttributes().getNamedItem("valueBoolean");
                            }
                            assertNotNull(value,
                                    "value or valueBoolean of a newly added column '"
                                            + columnNode.getAttributes().getNamedItem("name").getNodeValue() +
                                            "' should be set to avoid errors after schema migration!");
                        }
                    }
                }
            }
        }
    }

    private static Comment findPrecedingCommentOrNullSkippingTextNodes(Node node) {
        Node current = node.getPreviousSibling();
        while (current != null) {
            if (current instanceof Comment) {
                return (Comment) current;
            }
            if (current instanceof Text) {
                current = current.getPreviousSibling();
            } else {
                return null;
            }
        }
        return null;
    }

}
