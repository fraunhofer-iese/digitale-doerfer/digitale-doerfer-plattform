/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Jannis von Albedyll, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.feature.services;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotEnabledException;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature2;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature3;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Tag("feature")
public class FeatureServiceTest extends BaseServiceTest {

    public static final String COMMON_VALUES = """
            {
              "sampleStringValue" : "common",
              "sampleIntValue" : 47112,
              "sampleBooleanValue" : false,
              "sampleStringValues" : ["commonList1","commonList2"],
              "sampleEnum" : "POST"
            }
            """;
    public static final String COMMON_VALUES2 = """
            {
              "sampleStringValue" : "common2",
              "sampleIntValue" : 2222222,
              "sampleBooleanValue" : false,
              "sampleStringValues" : ["common2List1","common2List2","common2List3"],
              "sampleEnum" : "OPTIONS"
            }
            """;

    public static final String APP_VALUES = """
            {
              "sampleStringValue" : "app",
              "sampleStringValues" : ["appList1","appList2","appList3"]
            }
            """;

    public static final String GEOAREA1_VALUES = "{\n" +
            "   \"sampleStringValue\" : \"geoarea1\"" +
            " }";

    public static final String GEOAREA2_VALUES = "{\n" +
            "   \"sampleStringValue\" : \"geoarea2\"" +
            " }";

    public static final String APPVARIANT1_VALUES = """
            {
              "sampleStringValue" : "appvariant1",
              "sampleIntValue" : 999
            }
            """;

    public static final String APPVARIANT2_VALUES = """
            {
              "sampleStringValue" : "appvariant2"
            }
            """;

    public static final String TENANT1_VALUES = """
            {
              "sampleStringValue" : "tenant1",
              "sampleEnum" : "PUT"
            }
            """;

    public static final String APP_TENANT1_VALUES = """
            {
              "sampleStringValue" : "app_tenant1",
              "sampleEnum" : "GET"
            }
            """;

    public static final String APPVARIANT1_TENANT2_VALUES = """
            {
              "sampleStringValue" : "appvariant1_tenant2"
            }
            """;

    public static final String APP_TENANT3_VALUES = """
            {
              "sampleStringValue" : "app_tenant3"
            }
            """;

    public static final String TENANT3_VALUES3 = """
            {
              "sampleStringValue" : "tenant3",
              "sampleIntValue" : 303030,
              "sampleBooleanValue" : false,
              "sampleStringValues" : ["drei","three"],
              "sampleEnum" : "HEAD"
            }
            """;

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private FeatureService featureService;

    @Autowired
    private FeatureConfigRepository featureConfigRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        featureService.invalidateCache();
        saveFeatureConfigs();
    }

    private void saveFeatureConfigs() {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .configValues(COMMON_VALUES)
                .build());
        //Feature2
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature2.class.getName())
                .enabled(true)
                .configValues(COMMON_VALUES2)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .app(th.app1)
                .enabled(false)
                .configValues(APP_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .appVariant(th.app1Variant1)
                .enabled(true)
                .configValues(APPVARIANT1_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .appVariant(th.app1Variant2)
                .enabled(true)
                .configValues(APPVARIANT2_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .tenant(th.tenant1)
                .enabled(false)
                .configValues(TENANT1_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .geoAreasIncluded(Collections.singleton(th.geoAreaEisenberg))
                .geoAreaChildrenIncluded(true)
                .geoAreasExcluded(Collections.singleton(th.geoAreaDorf2InEisenberg))
                .configValues(GEOAREA1_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .geoAreasIncluded(Collections.singleton(th.geoAreaRheinlandPfalz))
                .geoAreaChildrenIncluded(false)
                .configValues(GEOAREA2_VALUES)
                .build());
        //Feature2
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature2.class.getName())
                .tenant(th.tenant1)
                .enabled(true)
                .configValues(TENANT1_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .app(th.app1)
                .tenant(th.tenant1)
                .enabled(true)
                .configValues(APP_TENANT1_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .appVariant(th.app1Variant1)
                .tenant(th.tenant2)
                .enabled(true)
                .configValues(APPVARIANT1_TENANT2_VALUES)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .app(th.app1)
                .tenant(th.tenant3)
                .enabled(false)
                .configValues(APP_TENANT3_VALUES)
                .build());
        //Feature3
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature3.class.getName())
                .tenant(th.tenant3)
                .enabled(true)
                .configValues(TENANT3_VALUES3)
                .build());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getFeature() throws Exception {

        assertGetFeatureEquals(FeatureTarget.common(),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("common")
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.app1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(false) //APP_VALUES
                        .sampleStringValue("app") //APP_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.app1Variant1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APPVARIANT1_VALUES
                        .sampleStringValue("appvariant1") //APPVARIANT1_VALUES
                        .sampleIntValue(999) //APPVARIANT1_VALUES
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.app1Variant2),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APPVARIANT2_VALUES
                        .sampleStringValue("appvariant2") //APPVARIANT2_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(false) //TENANT1_VALUES
                        .sampleStringValue("tenant1") //TENANT1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.PUT) //TENANT1_VALUES
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant1, th.app1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APP_TENANT1_VALUES
                        .sampleStringValue("app_tenant1") //APP_TENANT1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.GET) //APP_TENANT1_VALUES
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant1, th.app1Variant1),
                TestFeature1.builder() //APP_TENANT1_VALUES
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("app_tenant1") //APP_TENANT1_VALUES
                        //not APPVARIANT1_VALUES, since the config (tenant1 + app1) is the most specific one
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.GET) //APP_TENANT1_VALUES
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant1, th.app1Variant2),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APP_TENANT1_VALUES
                        .sampleStringValue("app_tenant1") //APP_TENANT1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.GET) //APP_TENANT1_VALUES
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant1, th.app2),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(false) //TENANT1_VALUES
                        .sampleStringValue("tenant1") //TENANT1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.PUT) //TENANT1_VALUES
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant2, th.app1Variant2),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APPVARIANT2_VALUES
                        .sampleStringValue("appvariant2") //APPVARIANT2_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant2, th.app1Variant1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //APPVARIANT1_TENANT2_VALUES
                        .sampleStringValue("appvariant1_tenant2") //APPVARIANT1_TENANT2_VALUES
                        .sampleIntValue(999) //APPVARIANT1_VALUES
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.tenant3, th.app1Variant1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(false) //APP_TENANT3_VALUES
                        .sampleStringValue("app_tenant3") //APP_TENANT3_VALUES
                        //not APPVARIANT1_VALUES, since the config (tenant3 + app1) is the most specific one
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("appList1", "appList2", "appList3")) //APP_VALUES
                        .sampleEnum(HttpMethod.POST)
                        .build());

        assertGetFeatureEquals(FeatureTarget.of(th.geoAreaEisenberg),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //GEOAREA1_VALUES
                        .sampleStringValue("geoarea1") //GEOAREA1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());

        //sub geo area that is included
        assertGetFeatureEquals(FeatureTarget.of(th.geoAreaDorf1InEisenberg),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //GEOAREA1_VALUES
                        .sampleStringValue("geoarea1") //GEOAREA1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());

        //sub geo area that is *not* included
        assertGetFeatureEquals(FeatureTarget.of(th.geoAreaDorf2InEisenberg),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("common")
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());

        //this geo area is upwards in the tree, but does not include the children
        assertGetFeatureEquals(FeatureTarget.of(th.geoAreaRheinlandPfalz),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true) //GEOAREA2_VALUES
                        .sampleStringValue("geoarea2") //GEOAREA2_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());

        //this geo area is a child of RLP, but the config is not including the children
        assertGetFeatureEquals(FeatureTarget.of(th.geoAreaMainz),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("common")
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build());
    }

    @Test
    public void getFeature_NotConfigured() {

        assertThrows(FeatureNotFoundException.class, () ->
                featureService.getFeature(TestFeature3.class, FeatureTarget.of(th.app1Variant1)));
        assertThat(featureService.getFeatureOrNull(TestFeature3.class, FeatureTarget.of(th.app1Variant1))).isNull();
    }

    @Test
    public void isFeatureEnabled() {

        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.common())).isTrue();
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1))).isFalse(); //APP_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1Variant1))).isTrue(); //APPVARIANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1Variant2))).isTrue(); //APPVARIANT2_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1))).isFalse(); //TENANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1))).isTrue(); //APP_TENANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1Variant1))).isTrue(); //APP_TENANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1Variant2))).isTrue(); //APP_TENANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app2))).isFalse(); //TENANT1_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant2, th.app1Variant2))).isTrue(); //APPVARIANT2_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant2, th.app1Variant1))).isTrue(); //APPVARIANT1_TENANT2_VALUES
        assertThat(featureService.isFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant3, th.app1Variant1))).isFalse(); //APP_TENANT3_VALUES
    }

    @Test
    public void isFeatureEnabled_NotConfigured() {

        assertThat(featureService.isFeatureEnabled(TestFeature3.class, FeatureTarget.of(th.app1Variant1))).isFalse();
    }

    @Test
    public void checkFeatureEnabled() {

        featureService.checkFeatureEnabled(TestFeature1.class, FeatureTarget.common());
        assertThrows(FeatureNotEnabledException.class, () -> featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1))); //APP_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1Variant1)); //APPVARIANT1_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.app1Variant2)); //APPVARIANT2_VALUES
        assertThrows(FeatureNotEnabledException.class, () -> featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1))); //TENANT1_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1)); //APP_TENANT1_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1Variant1)); //APP_TENANT1_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app1Variant2)); //APP_TENANT1_VALUES
        assertThrows(FeatureNotEnabledException.class, () -> featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant1, th.app2))); //TENANT1_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant2, th.app1Variant2)); //APPVARIANT2_VALUES
        featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant2, th.app1Variant1)); //APPVARIANT1_TENANT2_VALUES
        assertThrows(FeatureNotEnabledException.class, () -> featureService.checkFeatureEnabled(TestFeature1.class,
                FeatureTarget.of(th.tenant3, th.app1Variant1))); //APP_TENANT3_VALUES
    }

    @Test
    public void checkFeatureEnabled_NotConfigured() {

        assertThrows(FeatureNotEnabledException.class, () ->
                featureService.checkFeatureEnabled(TestFeature3.class, FeatureTarget.of(th.app1Variant1)));
    }

    @Test
    public void getAllFeatures() throws Exception {

        assertGetAllFeaturesEquals(FeatureTarget.common(),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("common")
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build(),
                TestFeature2.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature2")
                        .enabled(true)
                        .sampleStringValue("common2")
                        .sampleIntValue(2222222)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("common2List1", "common2List2", "common2List3"))
                        .sampleEnum(HttpMethod.OPTIONS)
                        .build());

        assertGetAllFeaturesEquals(FeatureTarget.of(th.tenant1),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(false) //TENANT1_VALUES
                        .sampleStringValue("tenant1") //TENANT1_VALUES
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.PUT) //TENANT1_VALUES
                        .build(),
                TestFeature2.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature2")
                        .enabled(true) //TENANT1_VALUES2
                        .sampleStringValue("tenant1") //TENANT1_VALUES
                        .sampleIntValue(2222222)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("common2List1", "common2List2", "common2List3"))
                        .sampleEnum(HttpMethod.PUT) //TENANT1_VALUES
                        .build());

        assertGetAllFeaturesEquals(FeatureTarget.of(th.tenant3),
                TestFeature3.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature3")
                        .enabled(true)
                        .sampleStringValue("tenant3")
                        .sampleIntValue(303030)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("drei", "three"))
                        .sampleEnum(HttpMethod.HEAD)
                        .build(),
                TestFeature1.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                        .enabled(true)
                        .sampleStringValue("common")
                        .sampleIntValue(47112)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("commonList1", "commonList2"))
                        .sampleEnum(HttpMethod.POST)
                        .build(),
                TestFeature2.builder()
                        .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature2")
                        .enabled(true)
                        .sampleStringValue("common2")
                        .sampleIntValue(2222222)
                        .sampleBooleanValue(false)
                        .sampleStringValues(Arrays.asList("common2List1", "common2List2", "common2List3"))
                        .sampleEnum(HttpMethod.OPTIONS)
                        .build());
    }

    @Test
    public void getAllFeatures_NotConfigured() {

        featureConfigRepository.deleteAll();

        assertThat(featureService.getAllFeaturesForTarget(FeatureTarget.common(), false)).isEmpty();
        assertThat(featureService.getAllFeaturesForTarget(FeatureTarget.of(th.app1), false)).isEmpty();
    }

    private void assertGetAllFeaturesEquals(FeatureTarget featureTarget, BaseFeature... expectedFeatures)
            throws Exception {

        List<BaseFeature> actualFeatures = featureService.getAllFeaturesForTarget(featureTarget, false);
        assertThat(actualFeatures).hasSize(expectedFeatures.length);
        for (int i = 0; i < expectedFeatures.length; i++) {
            BaseFeature expectedFeature = expectedFeatures[i];
            assertFeatureEquals(expectedFeature, actualFeatures.get(i));
        }

        List<BaseFeature> expectedEnabledFeatures = Arrays.stream(expectedFeatures)
                .filter(BaseFeature::isEnabled)
                .collect(Collectors.toList());
        List<BaseFeature> actualEnabledFeatures = featureService.getAllFeaturesForTarget(featureTarget, true);
        assertThat(actualEnabledFeatures).hasSameSizeAs(expectedEnabledFeatures);
        for (int i = 0; i < expectedEnabledFeatures.size(); i++) {
            BaseFeature expectedFeature = expectedEnabledFeatures.get(i);
            assertFeatureEquals(expectedFeature, actualEnabledFeatures.get(i));
        }
    }

    private void assertGetFeatureEquals(FeatureTarget featureTarget, BaseFeature expected) throws Exception {

        TestFeature1 actual = featureService.getFeature(TestFeature1.class, featureTarget);
        assertFeatureEquals(expected, actual);
        TestFeature1 actual2 = featureService.getFeatureOrNull(TestFeature1.class, featureTarget);
        assertFeatureEquals(expected, actual2);
    }

    private void assertFeatureEquals(BaseFeature expected, BaseFeature actual) throws Exception {
        assertThat(actual.isEnabled()).as("enabled flag of " + expected.getFeatureIdentifier()).isEqualTo(
                expected.isEnabled());
        assertJsonEquals(expected, actual, "featureValuesAsJson");
        assertJsonStringEquals(expected, actual.getFeatureValuesAsJson(),
                "enabled", "featureIdentifier", "featureValuesAsJson", "visibleForClient");
    }

}
