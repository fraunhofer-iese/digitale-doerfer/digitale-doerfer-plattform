/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.worker;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

@ActiveProfiles({"test", "TemporaryEmailStorageCleanUpWorkerTest"})
public class EmailStorageCleanUpWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestFileStorage testFileStorage;
    @Autowired
    private ApplicationConfig config;

    @Autowired
    private EmailStorageCleanUpWorker emailStorageCleanUpWorker;

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void cleanup() throws Exception {

        String testFileNameOld = config.getEmail().getEmailStoragePath() + "to-be-deleted.txt";
        String testFileNameNotOldEnough = config.getEmail().getEmailStoragePath() + "to-survive.txt";
        String testFileNameNotRelevant = "any-path/to-survive.txt";

        byte[] bytes = "does not matter".getBytes();
        long retentionTimeMillis = Duration.ofDays(config.getEmail().getMaxRetentionEmailStorageInDays()).toMillis();

        testFileStorage.addTestFile(testFileNameOld, bytes,
                System.currentTimeMillis() - retentionTimeMillis - 1);

        testFileStorage.addTestFile(testFileNameNotRelevant, bytes,
                System.currentTimeMillis() - retentionTimeMillis - 1);

        testFileStorage.addTestFile(testFileNameNotOldEnough, bytes,
                System.currentTimeMillis() - retentionTimeMillis + 15000);

        emailStorageCleanUpWorker.run();

        assertThat(testFileStorage.fileExists(testFileNameOld))
                .as("old files should be deleted").isFalse();
        assertThat(testFileStorage.fileExists(testFileNameNotOldEnough))
                .as("newer files should survive").isTrue();
        assertThat(testFileStorage.fileExists(testFileNameNotRelevant))
                .as("old, but not relevant files, should survive").isTrue();
    }

}
