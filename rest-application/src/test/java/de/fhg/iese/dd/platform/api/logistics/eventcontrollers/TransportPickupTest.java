/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public class TransportPickupTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    private TransportAssignment transportAssignment;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        String shopOrderId = purchaseOrderReceived();
        purchaseOrderReadyForTransport(shopOrderId);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        delivery = deliveries.get(0);

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        transportAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);
    }

    @Test
    public void onTransportPickupRequest() throws Exception {

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(
                delivery.getTrackingCode(), transportAssignment.getId());

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(carrier))
                        .contentType(contentType).content(json(transportPickupRequest))).andExpect(status().isOk())
                .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignment.getId()));

        // Give some time
        waitForEventProcessing();

        // --> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(
                get("/logistics/transport")
                        .headers(authHeadersFor(carrier))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1))).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType ").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddress.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddress.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddress.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddress.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.PICKED_UP.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        // --> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/").headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk()).andExpect(jsonPath("$[0].id").value(delivery.getId()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.IN_DELIVERY.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void onTransportPickupRequestWrongCarrier() throws Exception {

        // use another guy's authentication from BaseLogisticsEvent
        Person wrongCarrier = carrierLastStep;

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(
                delivery.getTrackingCode(), transportAssignment.getId());

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(wrongCarrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(isException(ClientExceptionType.WRONG_CARRIER_PICKUP));
    }

    @Test
    public void onTransportPickupRequestDeliveryTrackingCodeNull() throws Exception {

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(null,
                transportAssignment.getId());

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportPickupRequestDeliveryTrackingCodeInvalid() throws Exception {

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest("invalidtrackingcode",
                transportAssignment.getId());

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(isException(ClientExceptionType.WRONG_TRACKING_CODE));
    }

    @Test
    public void onTransportPickupRequestTransportAssignmentIdNull() throws Exception {

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(
                delivery.getTrackingCode(), null);

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportPickupRequestTransportAssignmentIdInvalid() throws Exception {

        // TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(
                delivery.getTrackingCode(), "invalidassignmentid");

        mockMvc.perform(post("/logistics/event/transportPickupRequest").headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));
    }

    @Test
    public void onTransportPickupRequestUnauthorized() throws Exception {
        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(
                delivery.getTrackingCode(), transportAssignment.getId());

        assertOAuth2(post("/logistics/event/transportPickupRequest")
                .content(json(transportPickupRequest))
                .contentType(contentType));
    }
    
}
