/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.framework;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import de.fhg.iese.dd.platform.api.RestApplication;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventA;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventB;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventF;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventG;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventH;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventI;
import de.fhg.iese.dd.platform.business.test.mocks.events.TestEventK;
import de.fhg.iese.dd.platform.business.test.mocks.exceptions.TestException;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor1;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor2;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor4;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor5;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor5Async;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor6;
import de.fhg.iese.dd.platform.business.test.mocks.processors.TestEventProcessor7;
import lombok.extern.log4j.Log4j2;

@SpringBootTest(classes = RestApplication.class)
@WebAppConfiguration
@Log4j2
public class EventBusAccessorTest {

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize() {
        eventBusAccessor.setOwner(this);
    }

    @Autowired
    private TestEventProcessor1 testEventProcessor1;
    @Autowired
    private TestEventProcessor2 testEventProcessor2;
    @Autowired
    private TestEventProcessor5 testEventProcessor5Sync;
    @Autowired
    private TestEventProcessor5Async testEventProcessor5Async;
    @Autowired
    private TestEventProcessor6 testEventProcessor6;
    @Autowired
    private TestEventProcessor7 testEventProcessor7;

    @Test
    public void simpleNotifyTriggerTest() throws Exception {

        BlockingQueue<BaseEvent> eventQueue = new LinkedBlockingQueue<>();

        testEventProcessor1.setTestEventAConsumer(eventQueue::add);

        eventBusAccessor.notifyTrigger(new TestEventA("Test: "));
        BaseEvent event = eventQueue.poll(1L, TimeUnit.SECONDS);

        assertNotNull(event, "event is null, timeout reached");
        assertEquals(TestEventA.class, event.getClass());
    }

    /*
     * Test notifyWithoutHandlingExceptions and notifyAndRegisterCompensate
     */
    @Test
    public void notifyRegisterCompensateAndRethrowExceptionTest() throws InterruptedException {

        BlockingQueue<Exception> exceptionQueue = new LinkedBlockingQueue<>();

        testEventProcessor2.setExceptionConsumer(exceptionQueue::add);

        eventBusAccessor.notifyTrigger(new TestEventB("Test: "));

        Exception exception = exceptionQueue.poll(1L, TimeUnit.SECONDS);

        assertNotNull(exception, "exception is null, timeout reached");
        assertEquals(TestException.class, exception.getClass());
        assertEquals(TestEventProcessor4.ERROR_MESSAGE_TEST_EVENT_PROCESSOR_4, exception.getMessage());
    }

    @Test
    public void notifyTriggerAndWaitForAnyReplyTest() throws EventProcessingException{

        BaseEvent reply = eventBusAccessor.notifyTriggerAndWaitForAnyReply(
                Collections.singletonList(new TestEventF("Test: ")),
                Collections.singletonList(TestEventG.class));

        assertEquals(TestEventG.class, reply.getClass());
    }

    @Test
    public void registerForEventsTest() throws InterruptedException{

        BlockingQueue<BaseEvent> eventQueue = new LinkedBlockingQueue<>();

        testEventProcessor6.setTestEventConsumer(eventQueue::add);

        eventBusAccessor.notifyTrigger(new TestEventH("Test: "));
        eventBusAccessor.notifyTrigger(new TestEventI("Test: "));

        BaseEvent event1 = eventQueue.poll(1L, TimeUnit.SECONDS);
        assertNotNull(event1, "event is null, timeout reached");
        assertThat(event1.getClass()).isIn(TestEventH.class, TestEventI.class);

        BaseEvent event2 = eventQueue.poll(1L, TimeUnit.SECONDS);
        assertNotNull(event2, "event is null, timeout reached");
        assertThat(event2.getClass()).isIn(TestEventH.class, TestEventI.class);
    }

    @Test
    public void registerForEventsTest_Async() {

        final String dataSync = "sync";
        final String dataAsync = "async";
        testEventProcessor5Sync.setTestEventFToGFunction(e -> new TestEventG(dataSync));
        testEventProcessor5Async.setTestEventFToGFunction(e -> new TestEventG(dataAsync));

        TestEventG receivedEvent = (TestEventG) eventBusAccessor.notifyTriggerAndWaitForAnyReply(
                Collections.singleton(new TestEventF("t")),
                Collections.singleton(TestEventG.class));

        log.debug("Reply received from {}", receivedEvent.getData());
        assertThat(receivedEvent).isNotNull();
        assertThat(receivedEvent.getData()).isIn(dataSync, dataAsync);
    }

    @Test
    public void registerForEventsTest_AsyncException() throws InterruptedException {

        final String dataSync = "sync";
        final String dataAsync = "async";
        final Semaphore semaphore = new Semaphore(1);
        testEventProcessor5Sync.setTestEventFToGFunction(e ->
                {
                    try {
                        //this is a hack to ensure that the exception is thrown first
                        Thread.sleep(20);
                        semaphore.acquire();
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    return new TestEventG(dataSync);
                }
        );
        //this exception needs to be ignored, since the exception strategy is determined to be IGNORE_EXCEPTION
        testEventProcessor5Async.setExceptionSupplier(() ->
        {
            semaphore.release();
            return new RuntimeException(dataAsync);
        });

        semaphore.acquire();
        TestEventG receivedEvent = null;
        try {
            receivedEvent = (TestEventG) eventBusAccessor.notifyTriggerAndWaitForAnyReply(
                    Collections.singleton(new TestEventF("t")),
                    Collections.singleton(TestEventG.class));
        } catch (Exception e) {
            fail("Exception from async event function received when waiting for reply", e);
        }
        semaphore.release();

        assertThat(receivedEvent).isNotNull();
        assertThat(receivedEvent.getData()).isEqualTo(dataSync);
    }

    @Test
    public void registerForEventAndAllSubtypesTest() throws InterruptedException {

        BlockingQueue<BaseEvent> eventQueue = new LinkedBlockingQueue<>();

        testEventProcessor7.setTestEventJConsumer(eventQueue::add);

        eventBusAccessor.notifyTrigger(new TestEventK("Test: "));

        BaseEvent event = eventQueue.poll(1L, TimeUnit.SECONDS);

        assertNotNull(event, "event is null, timeout reached");
        assertEquals(TestEventK.class, event.getClass());
    }

}
