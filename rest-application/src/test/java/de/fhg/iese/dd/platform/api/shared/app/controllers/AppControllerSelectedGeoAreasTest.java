/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class AppControllerSelectedGeoAreasTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_old_NoSelection() throws Exception {

        final Person personWithoutHomeArea = th.withoutHomeArea(th.personRegularKarl);

        assertEquals(0, th.appVariantUsageRepository.count(),
                "No app variant usages should be defined to test the default behavior");

        final AppVariant appVariantSingleTenant = th.app1Variant1; // available tenants: tenant1

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                appVariantSingleTenant.getAppVariantIdentifier())
                .headers(authHeadersFor(personWithoutHomeArea)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$").isEmpty());

        final AppVariant appVariantMultipleTenants = th.app2Variant1; // available tenants: tenant1, tenant2
        Person personWithHomeArea = th.personRegularAnna;
        assertThat(personWithHomeArea.getHomeArea()).isNotNull();

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                appVariantMultipleTenants.getAppVariantIdentifier())
                .headers(authHeadersFor(personWithHomeArea)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_old() throws Exception {
        //user settings exist

        final Person person = th.withoutHomeArea(th.personRegularAnna);

        assertEquals(0, th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined before creating ones for the test");

        final AppVariant appVariantSingleTenant = th.app1Variant1; // available tenants: tenant 1
        final List<GeoArea> expectedGeoAreasSingleTenant = th.geoAreasTenant1.stream()
                .limit(1)
                .collect(Collectors.toList());

        assertEquals(1, expectedGeoAreasSingleTenant.size());

        //create app variant usage for all tenants for the single tenant app variant
        th.selectGeoAreaForAppVariant(appVariantSingleTenant, person, expectedGeoAreasSingleTenant.get(0));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                appVariantSingleTenant.getAppVariantIdentifier())
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedGeoAreasSingleTenant.size())))
                .andExpect(jsonPath("$[0]").value(expectedGeoAreasSingleTenant.get(0).getId()));

        final AppVariant appVariantMultipleTenants =
                th.app2Variant1; // available tenants: tenant 1, tenant 2
        final List<GeoArea> expectedGeoAreasMultipleTenants = Stream.concat(
                th.geoAreasTenant1.stream()
                        .limit(1),
                th.geoAreasTenant2.stream()
                        .limit(1))
                .collect(Collectors.toList());

        assertEquals(2, expectedGeoAreasMultipleTenants.size());

        th.selectGeoAreaForAppVariant(appVariantMultipleTenants, person,
                expectedGeoAreasMultipleTenants.get(0));
        th.selectGeoAreaForAppVariant(appVariantMultipleTenants, person,
                expectedGeoAreasMultipleTenants.get(1));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                appVariantMultipleTenants.getAppVariantIdentifier())
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedGeoAreasMultipleTenants.size())))
                .andExpect(jsonPath("$", containsInAnyOrder(
                        expectedGeoAreasMultipleTenants.stream()
                                .map(GeoArea::getId)
                                .toArray(String[]::new))));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_old_WithHomeArea() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final Person person = th.withHomeArea(th.personRegularAnna, homeArea);

        assertEquals(0, th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined before creating ones for the test");

        final AppVariant app1Variant1 = th.app1Variant1; // available tenants: tenant 1
        final Collection<GeoArea> selectedGeoAreas = th.geoAreasTenant1.stream()
                .limit(2)
                .collect(Collectors.toList());

        assertEquals(2, selectedGeoAreas.size());

        th.selectGeoAreasForAppVariant(app1Variant1, person, selectedGeoAreas);

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                app1Variant1.getAppVariantIdentifier())
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(selectedGeoAreas.size())))
                .andExpect(jsonPath("$", containsInAnyOrder(
                        selectedGeoAreas.stream()
                                .map(GeoArea::getId)
                                .toArray(String[]::new))));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_old_Unauthorized() throws Exception {

        // we can not use assertOAuth2AppVariantRequired here, since the appVariantIdentifier is used as path param and not as header
        assertOAuth2(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas",
                th.app1Variant1.getAppVariantIdentifier()));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_old_InvalidAppVariant() throws Exception {

        String invalidId = "de.fhg.iese.dd.testbar";

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/selectedGeoAreas", invalidId)
                .headers(authHeadersFor(th.personRegularHorstiTenant3)))
                .andExpect(isException(invalidId, ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_NoSelection() throws Exception {
        //default behavior if no user settings exist: none

        final Person person = th.withoutHomeArea(th.personRegularKarl);

        assertEquals(0, th.appVariantUsageRepository.count(),
                "No app variant usages should be defined to test the default behavior");

        final AppVariant appVariantSingleTenant = th.app1Variant1; // available tenants: tenant1

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariantSingleTenant,
                () -> get("/app/appVariant/selectedGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$").isEmpty()));

        final AppVariant appVariantMultipleTenants = th.app2Variant1; // available tenants: tenant1, tenant 2
        Person personWithHomeArea = th.personRegularAnna;
        assertThat(personWithHomeArea.getHomeArea()).isNotNull();

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(personWithHomeArea,
                appVariantMultipleTenants,
                () -> get("/app/appVariant/selectedGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$").isEmpty()));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant() throws Exception {
        //user settings exist

        final Person person = th.withoutHomeArea(th.personRegularAnna);

        assertEquals(0, th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined before creating ones for the test");

        final AppVariant appVariantSingleTenant = th.app1Variant1; // available tenants: tenant 1
        final List<GeoArea> expectedGeoAreasSingleTenant = th.geoAreasTenant1.stream()
                .limit(1)
                .collect(Collectors.toList());

        assertEquals(1, expectedGeoAreasSingleTenant.size());

        //create app variant usage for all tenants for the single tenant app variant
        th.selectGeoAreaForAppVariant(appVariantSingleTenant, person, expectedGeoAreasSingleTenant.get(0));

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person,
                appVariantSingleTenant,
                () -> get("/app/appVariant/selectedGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$", hasSize(expectedGeoAreasSingleTenant.size())))
                        .andExpect(jsonPath("$[0]").value(expectedGeoAreasSingleTenant.get(0).getId())));

        final AppVariant appVariantMultipleTenants =
                th.app2Variant1; // available tenants: tenant 1, tenant 2
        final List<GeoArea> expectedGeoAreasMultipleTenants = Stream.concat(
                th.geoAreasTenant1.stream()
                        .limit(1),
                th.geoAreasTenant2.stream()
                        .limit(1))
                .collect(Collectors.toList());

        assertEquals(2, expectedGeoAreasMultipleTenants.size());

        th.selectGeoAreaForAppVariant(appVariantMultipleTenants, person,
                expectedGeoAreasMultipleTenants.get(0));
        th.selectGeoAreaForAppVariant(appVariantMultipleTenants, person,
                expectedGeoAreasMultipleTenants.get(1));

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person,
                appVariantMultipleTenants,
                () -> get("/app/appVariant/selectedGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$", hasSize(expectedGeoAreasMultipleTenants.size())))
                        .andExpect(jsonPath("$", containsInAnyOrder(
                                expectedGeoAreasMultipleTenants.stream()
                                        .map(GeoArea::getId)
                                        .toArray(String[]::new)))));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_WithHomeArea() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final Person person = th.withHomeArea(th.personRegularAnna, homeArea);

        assertEquals(0, th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined before creating ones for the test");

        final AppVariant app1Variant1 = th.app1Variant1; // available tenants: tenant 1
        final Collection<GeoArea> selectedGeoAreas = th.geoAreasTenant1.stream()
                .limit(2)
                .collect(Collectors.toList());

        assertEquals(2, selectedGeoAreas.size());

        th.selectGeoAreasForAppVariant(app1Variant1, person, selectedGeoAreas);

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, app1Variant1,
                () -> get("/app/appVariant/selectedGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(jsonPath("$", hasSize(selectedGeoAreas.size())))
                        .andExpect(jsonPath("$", containsInAnyOrder(
                                selectedGeoAreas.stream()
                                        .map(GeoArea::getId)
                                        .toArray(String[]::new)))));
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/app/appVariant/selectedGeoAreas"), th.app1Variant1, th.personRegularAnna);
    }

    @Test
    public void getSelectedGeoAreasForAppVariant_InvalidAppVariant() throws Exception {

        final String invalidId = "de.fhg.iese.dd.testbar";
        mockMvc.perform(get("/app/appVariant/selectedGeoAreas")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(invalidId)
                        .person(th.personRegularAnna)
                        .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

}
