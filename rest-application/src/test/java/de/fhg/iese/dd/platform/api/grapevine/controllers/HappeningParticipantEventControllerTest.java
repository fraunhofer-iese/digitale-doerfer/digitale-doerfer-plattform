/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationConfirmRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningParticipationRevokeRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.HappeningParticipant;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HappeningParticipantEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private HappeningRepository happeningRepository;
    @Autowired
    private HappeningParticipantRepository happeningParticipantRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();
        th.createOrganizations();
        th.createNewsSources();
        th.createHappeningsAndComments();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void confirmHappeningParticipation() throws Exception {

        final Person caller = th.personLaraSchaefer;
        final Happening happening = th.happening1;

        assertEquals(5, happening.getParticipantCount());
        assertFalse(happeningParticipantRepository.existsByHappeningAndPerson(happening, caller));

        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, happening.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("updatedHappening.happening.participantCount").value(6))
                .andExpect(jsonPath("updatedHappening.happening.participated").value(true));

        Happening updatedHappening = happeningRepository.findByIdAndDeletedFalse(happening.getId()).get();
        HappeningParticipant happeningParticipant =
                happeningParticipantRepository.findByHappeningAndPerson(updatedHappening, caller).get();

        assertEquals(6, updatedHappening.getParticipantCount());
        assertEquals(caller, happeningParticipant.getPerson());
        assertEquals(updatedHappening, happeningParticipant.getHappening());

        // confirm for the second time on the same happening -> should be idempotent
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, happening.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("updatedHappening.happening.participantCount").value(6))
                .andExpect(jsonPath("updatedHappening.happening.participated").value(true));

        updatedHappening = happeningRepository.findByIdAndDeletedFalse(happening.getId()).get();
        happeningParticipant = happeningParticipantRepository.findByHappeningAndPerson(updatedHappening, caller).get();

        assertEquals(happening.getLastModified(), updatedHappening.getLastModified());
        assertEquals(6, updatedHappening.getParticipantCount());
        assertEquals(caller, happeningParticipant.getPerson());
        assertEquals(updatedHappening, happeningParticipant.getHappening());
    }

    @Test
    public void confirmHappeningParticipation_NotAuthorized() throws Exception {

        final Happening happening = th.happening1;

        assertOAuth2(post("/grapevine/event/happeningParticipationConfirmRequest")
                .contentType(contentType)
                .content(json(ClientHappeningParticipationConfirmRequest.builder()
                        .happeningId(happening.getId())
                        .build())));
    }

    @Test
    public void confirmHappeningParticipation_InvalidAttributes() throws Exception {

        final Person caller = th.personRegularKarl;

        // happeningId null
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // happeningId empty
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, ""))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // happeningId invalid
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, "invalidHappeningId"))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        // happeningId deleted
        Happening deletedHappening = th.happening1;
        deletedHappening.setDeleted(true);
        deletedHappening = happeningRepository.saveAndFlush(th.happening1);
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, deletedHappening.getId()))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder buildHappeningParticipationConfirmRequest(Person caller, String happeningId)
            throws IOException {
        return post("/grapevine/event/happeningParticipationConfirmRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientHappeningParticipationConfirmRequest.builder()
                        .happeningId(happeningId)
                        .build()));
    }

    @Test
    public void revokeHappeningParticipation() throws Exception {

        final Person caller = th.personRegularKarl;
        final Happening happening = th.happening1;

        assertEquals(5, happening.getParticipantCount());
        assertTrue(happeningParticipantRepository.existsByHappeningAndPerson(happening, caller));

        mockMvc.perform(buildHappeningParticipationRevokeRequest(caller, happening.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("updatedHappening.happening.participantCount").value(4))
                .andExpect(jsonPath("updatedHappening.happening.participated").value(false));

        Happening updatedHappening = happeningRepository.findByIdAndDeletedFalse(happening.getId()).get();

        assertEquals(4, updatedHappening.getParticipantCount());
        assertFalse(happeningParticipantRepository.existsByHappeningAndPerson(happening, caller));

        // revoke for the second time on the same happening -> should be idempotent
        mockMvc.perform(buildHappeningParticipationRevokeRequest(caller, happening.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("updatedHappening.happening.participantCount").value(4))
                .andExpect(jsonPath("updatedHappening.happening.participated").value(false));

        updatedHappening = happeningRepository.findByIdAndDeletedFalse(happening.getId()).get();

        assertEquals(happening.getLastModified(), updatedHappening.getLastModified());
        assertEquals(4, updatedHappening.getParticipantCount());
        assertFalse(happeningParticipantRepository.existsByHappeningAndPerson(happening, caller));

    }

    @Test
    public void revokeHappeningParticipation_NotAuthorized() throws Exception {

        final Happening happening = th.happening1;

        assertOAuth2(post("/grapevine/event/happeningParticipationRevokeRequest")
                .contentType(contentType)
                .content(json(ClientHappeningParticipationRevokeRequest.builder()
                        .happeningId(happening.getId())
                        .build())));
    }

    @Test
    public void revokeHappeningParticipation_InvalidAttributes() throws Exception {

        final Person caller = th.personRegularKarl;

        // happeningId null
        mockMvc.perform(buildHappeningParticipationRevokeRequest(caller, null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // happeningId empty
        mockMvc.perform(buildHappeningParticipationRevokeRequest(caller, ""))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // happeningId invalid
        mockMvc.perform(buildHappeningParticipationRevokeRequest(caller, "invalidHappeningId"))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        // happeningId deleted
        Happening deletedHappening = th.happening1;
        deletedHappening.setDeleted(true);
        deletedHappening = happeningRepository.saveAndFlush(th.happening1);
        mockMvc.perform(buildHappeningParticipationConfirmRequest(caller, deletedHappening.getId()))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder buildHappeningParticipationRevokeRequest(Person caller, String happeningId)
            throws IOException {
        return post("/grapevine/event/happeningParticipationRevokeRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientHappeningParticipationRevokeRequest.builder()
                        .happeningId(happeningId)
                        .build()));
    }

}
