/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoAreaExtended;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReference;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonReferenceWithEmail;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonStatus;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.motivation.framework.IAchievementRule;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.test.mocks.TestOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopManager;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsageAreaSelection;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageAreaSelectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantUsageRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.*;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.DocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryDocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryMediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.RoleAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommunityHelpDesk;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.geojson.GeoJsonReader;
import org.locationtech.spatial4j.context.jts.JtsSpatialContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class BaseTestHelper {

    public static final String INVALID_UUID = "00000000-0000-0000-0000-000000000000";

    protected static final GPSLocation DEFAULT_LOCATION = new GPSLocation(3., 4.);
    protected static final GPSLocation GPS_LOCATION_DEFAULT_49_8 = new GPSLocation(49.0, 8.0);
    protected static final GPSLocation GPS_LOCATION_KL_UNI = new GPSLocation(49.4266035, 7.7409713);
    protected static final GPSLocation GPS_LOCATION_KL_CITY = new GPSLocation(49.441864, 7.7652635);

    private static final GeoJsonReader geoJsonReader =
            new GeoJsonReader(JtsSpatialContext.GEO.getShapeFactory().getGeometryFactory());

    protected final Logger log = LogManager.getLogger(this.getClass());

    //we should not delete the liquibase tables, since this will cause the changelog to be applied again!
    private static final String[] DELETION_EXCLUDED_TABLES = {
            "databasechangelog",
            "databasechangeloglock"};

    private static final String QUERY_SELECT_NAME_ALL_TABLES_H2 = "SELECT table_name "
            + "FROM information_schema.tables "
            //the schema name in h2 is public, ebdb is the db name, this is different from mysql
            //PUBLIC needs to be capital for SQL Server
            + "where table_schema='PUBLIC'";

    private static final String QUERY_SELECT_NAME_ALL_TABLES_MYSQL = "SELECT table_name "
            + "FROM information_schema.tables "
            + "where table_schema='ebdb" + System.getProperty("forkNumber") + "'";

    private static final String QUERY_SET_REFERENTIAL_INTEGRITY_FALSE_H2 = "SET REFERENTIAL_INTEGRITY FALSE; ";
    private static final String QUERY_SET_REFERENTIAL_INTEGRITY_TRUE_H2 = "SET REFERENTIAL_INTEGRITY TRUE; ";

    private static final String QUERY_SET_REFERENTIAL_INTEGRITY_FALSE_MYSQL = "SET @@foreign_key_checks = 0; ";
    private static final String QUERY_SET_REFERENTIAL_INTEGRITY_TRUE_MYSQL = "SET @@foreign_key_checks = 1; ";

    @Autowired
    public PersonRepository personRepository;
    @Autowired
    public GeoAreaRepository geoAreaRepository;
    @Autowired
    public TenantRepository tenantRepository;
    @Autowired
    public RoleAssignmentRepository roleAssignmentRepository;
    @Autowired
    public AppVariantUsageRepository appVariantUsageRepository;
    @Autowired
    public AppVariantUsageAreaSelectionRepository appVariantUsageAreaSelectionRepository;
    @Autowired
    public AddressRepository addressRepository;
    @Autowired
    public AddressListEntryRepository addressListEntryRepository;
    @Autowired
    public PushCategoryRepository pushCategoryRepository;
    @Autowired
    public MediaItemRepository mediaItemRepository;
    @Autowired
    public TemporaryMediaItemRepository temporaryMediaItemRepository;
    @Autowired
    public DocumentItemRepository documentItemRepository;
    @Autowired
    public TemporaryDocumentItemRepository temporaryDocumentItemRepository;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    public ApplicationConfig config;
    @Autowired
    protected IFileStorage fileStorage;
    @Autowired
    protected WebApplicationContext webApplicationContext;
    @Autowired
    protected IEnvironmentService environmentService;
    @Autowired
    private List<IAchievementRule<?>> achievementRules;
    @Autowired(required = false)
    private TestOauthManagementService testOauthManagementService;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;

    private long created = System.currentTimeMillis();

    public GeoArea geoAreaTenant1;
    public GeoArea subGeoArea1Tenant1;
    public GeoArea subGeoArea2Tenant1;
    public GeoArea geoAreaTenant2;
    public GeoArea geoAreaTenant3;

    public Tenant tenant1;
    public Tenant tenant2;
    public Tenant tenant3;
    public List<Tenant> tenantList;

    public GeoArea geoAreaDeutschland;
    public GeoArea geoAreaRheinlandPfalz;
    public GeoArea geoAreaKaiserslautern;
    public GeoArea geoAreaMainz;
    public GeoArea geoAreaEisenberg;
    public GeoArea geoAreaDorf1InEisenberg;
    public GeoArea geoAreaDorf2InEisenberg;
    public List<GeoArea> geoAreaList;

    public Collection<GeoArea> geoAreasTenant1;
    public Collection<GeoArea> geoAreasTenant2;

    public ClientGPSLocation pointClosestToTenant1;
    public ClientGPSLocation pointClosestToTenant2;
    public ClientGPSLocation pointClosestToTenant3;

    public Person personVgAdmin;
    public Person personIeseAdmin;
    public Person personPoolingStationOp;
    public Person personShopOwner;
    public Person personShopOwner2;
    public Person personShopManager;
    public Person personRegularAnna;
    public Person personRegularKarl;
    public Person personSuperAdmin;
    public Person personRoleManagerTenant1;
    public Person personRoleManagerTenant2;
    public Person personRegularHorstiTenant3;
    public Person contactPersonTenant1;
    public Person contactPersonTenant2;

    public RoleAssignment roleAssignmentPersonVgAdmin;
    public RoleAssignment roleAssignmentPersonIeseAdmin;
    public RoleAssignment roleAssignmentPersonSuperAdmin;

    public Address addressPersonVgAdmin;

    public PoolingStation poolingStation0;

    public long nextTimeStamp() {
        return created++;
    }

    protected Set<String> achievementRulesToCreate() {
        return new HashSet<>(Arrays.asList(
                "score.participants.created.1",
                "score.participants.registered.1"
        ));
    }

    public void createAchievementRules() {
        createAchievementRules(new String[]{});
    }

    public void createAchievementRules(String... additionalRules) {

        // We wait 200 ms after the creation of the achievement rules to ensure that they are fully created before we start the actual test.
        // This seems to be enough in most cases, but not in all, this leads to rare exceptions in the logs that do not harm the test result.
        // Sometimes the achievement would be deleted before the achievement is achieved, which can also cause exceptions.
        // For that reason we wait for event processing before we clean up the test environment.

        final Set<String> relevantAchievementRuleNames = achievementRulesToCreate();
        relevantAchievementRuleNames.addAll(Arrays.asList(additionalRules));
        achievementRules.stream()
                .filter(r -> relevantAchievementRuleNames.contains(r.getName()))
                .forEach(r -> {
                    final Collection<Achievement> createdAchievements = r.createOrUpdateRelevantAchievements();
                    log.info("Created {} achievements: {}", createdAchievements.size(), r.getName());
                });
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void createPersons() {

        personVgAdmin = createPerson("Marc", tenant1, subGeoArea1Tenant1,
                Address.builder()
                        .name("Marc Peters")
                        .street("Peterstraße 7")
                        .zip("67655")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4426796, 7.7590265)).build(),
                "4a6d3e20-7638-40da-8aa7-b376a3dc8036");
        addressPersonVgAdmin = personVgAdmin.getAddresses().iterator().next().getAddress();
        roleAssignmentPersonVgAdmin =
                assignRoleToPerson(personVgAdmin, LogisticsAdminUiRestrictedAdmin.class, tenant1.getId());

        personIeseAdmin = createPerson("Julia", tenant1,
                Address.builder()
                        .name("Julia Müller")
                        .street("Alex-Müller-Straße 160")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4555101, 7.7753689)).build(),
                "4b174fb0-c740-48ee-9d27-45f4e16a71cf");
        roleAssignmentPersonIeseAdmin =
                assignRoleToPerson(personIeseAdmin, LogisticsAdminUiAdmin.class, tenant1.getId());

        personSuperAdmin = createPerson("Rita", tenant1,
                Address.builder()
                        .name("Rita Super")
                        .street("Lindenstraße 16")
                        .zip("67999")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4555101, 7.7753689)).build(),
                "e09c367a-9ad4-4257-9a88-833f270efd53");
        roleAssignmentPersonSuperAdmin = assignRoleToPerson(personSuperAdmin, SuperAdmin.class, null);

        personPoolingStationOp = createPerson("Pooli", tenant1,
                Address.builder()
                        .name("Pooli Operateur")
                        .street("Mozartstraße 4")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4409838, 7.7682621))
                        .build(), "aad489fa-be24-4193-b4a8-bc42158d0c89");

        personRegularAnna = createPerson("Anna", tenant1,
                Address.builder()
                        .name("Anna Metner")
                        .street("Am Harzhübel 118")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4274535, 7.745186))
                        .build(), "045d0c01-50f8-4994-8f68-c63ced6da13c");

        personRegularKarl = createPerson("Karl", tenant1,
                Address.builder()
                        .name("Karl Napp")
                        .street("Am Harzhübel 118")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4274535, 7.745186))
                        .build(), "2f2a5ed9-e46c-4967-a6e4-e87985dee2e8");

        personShopOwner = createPerson("Shoppi", tenant1,
                Address.builder()
                        .name("Shop Owner")
                        .street("Mozartstraße 4")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4409838, 7.7682621)).build(),
                "78dd5398-79cf-4935-be6a-b263208d0699");

        personShopOwner2 = createPerson("Shoppi2", tenant2,
                Address.builder()
                        .name("Shop2 Owner")
                        .street("Mozartstraße 2")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4409838, 7.7682621)).build(),
                "789796c8-4baf-4df2-adb5-9d8e614e0690");

        personShopManager = createPerson("Shopmeister", tenant1,
                Address.builder()
                        .name("Shop Manager")
                        .street("Königstraße 85")
                        .zip("67655")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.439784, 7.758741)).build(),
                "823ba307-15aa-4876-9c55-bfb0a69460a9");
        assignRoleToPerson(personShopManager, ShopManager.class, tenant1.getId());

        personRoleManagerTenant1 = createPerson("Rolli-eins", tenant1,
                Address.builder()
                        .name("Role Manager Eins")
                        .street("Fraunhofer Platz 1")
                        .zip("67657")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.431768, 7.752154)).build(),
                "c311e663-ebe2-400b-a20e-1cf72f85a7d5");
        assignRoleToPerson(personRoleManagerTenant1, RoleManager.class, tenant1.getId());
        personRoleManagerTenant1.setCreated(7); //long ago, to have different numbers in the statistics
        personRepository.save(personRoleManagerTenant1);

        personRoleManagerTenant2 = createPerson("Rolli-zwei", tenant2,
                Address.builder()
                        .name("Role Manager Zwo")
                        .street("Fraunhofer Platz 1")
                        .zip("67657")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.431768, 7.752154)).build(),
                "e938c3be-06d9-45e0-8b41-e725c1a46bb2");
        assignRoleToPerson(personRoleManagerTenant2, RoleManager.class, tenant2.getId());

        contactPersonTenant1 = createPerson("Adeline", tenant1,
                Address.builder()
                        .name("Adeline")
                        .street("Gutenbergstrasse 18")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4312883, 7.7524382)).build(),
                "986333b9-1a3f-4d75-9b71-b162a698ca35");
        assignRoleToPerson(contactPersonTenant1, CommunityHelpDesk.class, tenant1.getId());
        contactPersonTenant1.setLastLoggedIn(42); //long ago, to have different numbers in the statistics
        personRepository.save(contactPersonTenant1);

        contactPersonTenant2 = createPerson("Helpina", tenant1,
                Address.builder()
                        .name("HelpDesk")
                        .street("Pirmasenser Strasse 8")
                        .zip("67655")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.441864, 7.7652635)).build(),
                "ebc2d1cc-4df7-4c97-907a-0cbda6f4e5e1");
        assignRoleToPerson(contactPersonTenant2, CommunityHelpDesk.class, tenant2.getId());
        contactPersonTenant2.setCreated(4711); //long ago, to have different numbers in the statistics
        personRepository.save(contactPersonTenant2);

        personRegularHorstiTenant3 = createPerson("Horsti", tenant3,
                Address.builder()
                        .name("Horsti Borsti")
                        .street("Am Harzhübel 118")
                        .zip("67308")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4274535, 7.745186))
                        .build(),
                "201a58e5-2b82-4945-ad8b-0aff2f4b389f");
    }

    public void createTenantsAndGeoAreas() {

        tenant1 = tenantRepository.save(Tenant.builder()
                //tenant1.id needs to be less than tenant2.id for RoleAssignmentRestControllerTest
                .id("74dc2ee1-025e-4f5e-82bb-34b552e292e4")
                .created(nextTimeStamp())
                .name("VG 1")
                .tag("rlp")
                .profilePicture(createMediaItem("VG1_ProfilePicture"))
                .build());

        tenant2 = tenantRepository.save(Tenant.builder()
                .id("c2c8af27-bedc-423a-a6ec-3e978180fd8f")
                .created(nextTimeStamp())
                .name("VG 2")
                .tag("rlp")
                .profilePicture(createMediaItem("VG2_ProfilePicture"))
                .build());

        tenant3 = tenantRepository.save(Tenant.builder()
                .id("c2e6aa6c-2efe-4127-8009-152e2c47ed9c")
                .created(nextTimeStamp())
                .name("VG 3")
                .tag("bayern")
                .profilePicture(createMediaItem("VG3_ProfilePicture"))
                .build());

        tenantList = Arrays.asList(tenant1, tenant2, tenant3);
        //since we always rely on the root geo area in the tenants this needs to be called
        createGeoAreas();
    }

    /**
     * Make sure to call {@link #createTenantsAndGeoAreas()} first if you need tenants for geo areas
     */
    protected void createGeoAreas() {
        /*
        Name                            Tenant
        -------------------------------------------
        Deutschland
        +- Rheinland-Pfalz
           +- Kaiserslautern            tenant1
           +- Mainz                     tenant2
           +- Eisenbuckel               tenant1
              +- Eisenbuckel-DorfEins   tenant1
              +- Eisenbuckel-DorfZwei   tenant1
        Geo Area Drei                   tenant3

        for a visual representation use logGeoJsonViewerURL(geoAreaList)
        */
        geoAreaDeutschland = geoAreaRepository.save(GeoArea.builder()
                .id("492cc76f-2980-4d73-a839-1450cc9ff01e")
                .name("Deutschland")
                .logo(createMediaItem("Deutschland"))
                .type(GeoAreaType.NATION)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.4,8.1],[49.4,8.4],[49.1,8.4],[49.1,8.1],[49.4,8.1]]]}")
                .center(new GPSLocation(49.25, 8.25))
                .build());
        geoAreaDeutschland.setDepth(0);
        geoAreaDeutschland.setCountDirectChildren(1);
        geoAreaDeutschland.setCountChildren(6);
        geoAreaDeutschland.setLeaf(false);

        geoAreaRheinlandPfalz = geoAreaRepository.save(GeoArea.builder()
                .id("76a2860b-a3ba-4299-955d-3859aedaa114")
                .name("Rheinland-Pfalz")
                .logo(createMediaItem("Rheinland-Pfalz"))
                .type(GeoAreaType.FEDERAL_STATE)
                .parentArea(geoAreaDeutschland)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.3,8.1],[49.3,8.4],[49.1,8.4],[49.1,8.1],[49.3,8.1]]]}")
                .center(new GPSLocation(49.2, 8.25))
                .customAttributesJson(
                        "{\"testBoolean\":true,\"testString\":\"banana\",\"testNumber\":42,\"testNull\":null," +
                                "\"testObject\":{\"something\":\"here\",\"really\":false}}")
                .build());
        geoAreaRheinlandPfalz.setDepth(1);
        geoAreaRheinlandPfalz.setCountDirectChildren(3);
        geoAreaRheinlandPfalz.setCountChildren(5);
        geoAreaRheinlandPfalz.setLeaf(false);

        geoAreaKaiserslautern = geoAreaRepository.save(GeoArea.builder()
                .id("2741c57d-30e9-414d-8055-7f05997a9101")
                .tenant(tenant1)
                .name("Kaiserslautern")
                .logo(createMediaItem("Kaiserslautern"))
                .type(GeoAreaType.CITY)
                .parentArea(geoAreaRheinlandPfalz)
                //boundary split into two polygons
                //the two polygons are _not_ valid if they share a common edge, since this should be one polygon
                .boundaryJson("{\"type\":\"MultiPolygon\"," +
                        "\"coordinates\":[[[[49.3,8.1],[49.3,8.15],[49.1,8.15],[49.1,8.1],[49.3,8.1]]]," +
                        "[[[49.3,8.15],[49.3,8.2],[49.1,8.2],[49.1,8.151],[49.3,8.15]]]]}")
                .zip("67655,67656,67657,67658,67659,67660,67661,67662,67663")
                .center(new GPSLocation(49.2, 8.15))
                .build());
        geoAreaKaiserslautern.setDepth(2);
        geoAreaKaiserslautern.setCountDirectChildren(0);
        geoAreaKaiserslautern.setCountChildren(0);
        geoAreaKaiserslautern.setLeaf(true);

        geoAreaMainz = geoAreaRepository.save(GeoArea.builder()
                .id("b9b94a62-3a4e-4f74-baac-64492a9cf4d1")
                .tenant(tenant2)
                .name("Mainz")
                .logo(createMediaItem("Mainz"))
                .type(GeoAreaType.CITY)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.3,8.2],[49.3,8.3],[49.1,8.3],[49.1,8.2],[49.3,8.2]]]}")
                .zip("55001,55002,55003,55004,55005,55006,55007,55008,55009,55010,55011,55012,55013,55014,55015,55016,55017,55018,55019,55020,55021,55022,55023,55024,55025,55026,55027,55028,55029,55030,55031,55032,55033,55034,55035,55036,55037,55038,55039,55040,55041,55042,55043,55044,55045,55046,55047,55048,55049,55050,55051,55052,55053,55054,55055,55056,55057,55058,55059,55060,55061,55062,55063,55064,55065,55066,55067,55068,55069,55070,55071,55072,55073,55074,55075,55076,55077,55078,55079,55080,55081,55082,55083,55084,55085,55086,55087,55088,55089,55090,55091,55092,55093,55094,55095,55096,55097,55098,55099,55100,55101,55102,55103,55104,55105,55106,55107,55108,55109,55110,55111,55112,55113,55114,55115,55116,55117,55118,55119,55120,55121,55122,55123,55124,55125,55126,55127,55128,55129,55130,55131")
                .center(new GPSLocation(49.2, 8.25))
                .build());
        geoAreaMainz.setDepth(2);
        geoAreaMainz.setCountDirectChildren(0);
        geoAreaMainz.setCountChildren(0);
        geoAreaMainz.setLeaf(true);
        geoAreaTenant2 = geoAreaMainz;
        pointClosestToTenant2 = new ClientGPSLocation(49.21, 8.251);

        geoAreaEisenberg = geoAreaRepository.save(GeoArea.builder()
                .id("08964780-b3db-4988-b18f-3a2c643d8f1f")
                .tenant(tenant1)
                .name("Eisenbuckel")
                .logo(createMediaItem("Eisenbuckel"))
                .type(GeoAreaType.ASSOCIATION_OF_MUNICIPALITIES)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.3,8.3],[49.3,8.4],[49.1,8.4],[49.1,8.3],[49.3,8.3]]]}")
                .zip("67304")
                .center(new GPSLocation(49.2, 8.35))
                .build());
        geoAreaEisenberg.setDepth(2);
        geoAreaEisenberg.setCountDirectChildren(2);
        geoAreaEisenberg.setCountChildren(2);
        geoAreaEisenberg.setLeaf(false);
        geoAreaTenant1 = geoAreaEisenberg;
        pointClosestToTenant1 = new ClientGPSLocation(49.21, 8.36);

        geoAreaDorf1InEisenberg = geoAreaRepository.save(GeoArea.builder()
                .id("67356b9c-ddea-4017-b5f0-3d7a6d7de01f")
                .tenant(tenant1)
                .name("Eisenbuckel-DorfEins")
                .logo(createMediaItem("Eisenbuckel-DorfEins"))
                .type(GeoAreaType.MUNICIPALITY)
                .parentArea(geoAreaEisenberg)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.3,8.3],[49.3,8.4],[49.25,8.4],[49.15,8.3],[49.3,8.3]]]}")
                .zip("67304")
                .center(new GPSLocation(49.25, 8.35))
                .build());
        geoAreaDorf1InEisenberg.setDepth(3);
        geoAreaDorf1InEisenberg.setCountDirectChildren(0);
        geoAreaDorf1InEisenberg.setCountChildren(0);
        geoAreaDorf1InEisenberg.setLeaf(true);
        subGeoArea1Tenant1 = geoAreaDorf1InEisenberg;

        geoAreaDorf2InEisenberg = geoAreaRepository.save(GeoArea.builder()
                .id("e96b1d90-08fd-4b11-ab81-e957b697c678")
                .tenant(tenant1)
                .name("Eisenbuckel-DorfZwei")
                .logo(createMediaItem("Eisenbuckel-DorfZwei"))
                .type(GeoAreaType.MUNICIPALITY)
                .parentArea(geoAreaEisenberg)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.15,8.3],[49.25,8.4],[49.1,8.4],[49.1,8.3],[49.15,8.3]]]}")
                .zip("67305")
                .center(new GPSLocation(49.15, 8.35))
                .build());
        geoAreaDorf2InEisenberg.setDepth(3);
        geoAreaDorf2InEisenberg.setCountDirectChildren(0);
        geoAreaDorf2InEisenberg.setCountChildren(0);
        geoAreaDorf2InEisenberg.setLeaf(true);
        subGeoArea2Tenant1 = geoAreaDorf2InEisenberg;

        geoAreaTenant3 = geoAreaRepository.save(GeoArea.builder()
                .id("c22f2452-915d-4bb5-8328-15c450ca181f")
                .tenant(tenant3)
                .name("Geo Area Drei")
                .type(GeoAreaType.CITY)
                .boundaryJson(
                        "{\"type\":\"Polygon\",\"coordinates\":[[[49.5,8.3],[49.5,8.2],[49.6,8.2],[49.6,8.3],[49.5,8.3]]]}")
                .center(new GPSLocation(49.54, 8.24))
                .build());
        geoAreaTenant3.setDepth(0);
        geoAreaTenant3.setCountDirectChildren(0);
        geoAreaTenant3.setCountChildren(0);
        geoAreaTenant3.setLeaf(true);
        pointClosestToTenant3 = new ClientGPSLocation(49.62, 8.25);

        geoAreasTenant1 = List.of(
                geoAreaKaiserslautern,
                geoAreaEisenberg,
                geoAreaDorf1InEisenberg,
                geoAreaDorf2InEisenberg);
        geoAreasTenant2 = List.of(
                geoAreaMainz);

        //Assert integrity of geoAreas and test boundary parsing
        Stream.concat(geoAreasTenant1.stream(), geoAreasTenant2.stream())
                .forEach(this::checkIntegrityOfGeoArea);
        assignRootAreasToTenants();
        geoAreaList = List.of(geoAreaDeutschland, geoAreaRheinlandPfalz, geoAreaKaiserslautern, geoAreaMainz,
                geoAreaEisenberg, geoAreaDorf1InEisenberg, geoAreaDorf2InEisenberg, geoAreaTenant3);
    }

    @SneakyThrows
    public void logGeoJsonViewerURL(List<GeoArea> geoAreas) {

        StringBuilder geoJsonViewerText = new StringBuilder("""
                {
                  "type": "FeatureCollection",
                  "features": [
                """);
        Random random = new Random();
        for (Iterator<GeoArea> iterator = geoAreas.iterator(); iterator.hasNext(); ) {
            GeoArea geoArea = iterator.next();
            String color = String.format("#%06x", random.nextInt(0xffffff + 1));
            Map<String, Object> properties = Map.of("title", geoArea.getName(),
                    "stroke-width", 20 - geoArea.getDepth() * 4,
                    "stroke", color);
            geoJsonViewerText.append("""
                     {
                          "type": "Feature",
                          "properties":
                    """);
            geoJsonViewerText.append(JsonMapping.defaultJsonWriter().writeValueAsString(properties));
            geoJsonViewerText.append("""
                          ,
                          "geometry":
                    """);
            geoJsonViewerText.append(geoArea.getBoundaryJson());
            geoJsonViewerText.append("}");
            if (iterator.hasNext()) {
                geoJsonViewerText.append(",\n");
            }
        }
        geoJsonViewerText.append("""
                    ]
                }
                """);
        log.info(UriComponentsBuilder
                .fromHttpUrl("https://geojson.io/#data=data:application/json,{geojson}")
                .build(geoJsonViewerText)
                .toString());
    }

    private void checkIntegrityOfGeoArea(GeoArea geoArea) {

        try {
            Geometry geometry = geoJsonReader.read(geoArea.getBoundaryJson());

            assertThat(geometry.isValid()).isTrue();
            if (geoArea == geoAreaKaiserslautern) {
                assertThat(geometry.getNumGeometries()).isEqualTo(2);
            } else {
                assertThat(geometry.getNumGeometries()).isEqualTo(1);
            }
            for (int i = 0; i < geometry.getNumGeometries(); i++) {
                final Geometry geometryN = geometry.getGeometryN(i);
                assertThat(geometryN.getNumPoints()).isEqualTo(5);
            }
        } catch (Exception e) {
            throw new IllegalStateException("Geo area '" + geoArea.getName() + "' is no valid geometry");
        }
    }

    protected void assignRootAreaToTenant(final Tenant tenant, final GeoArea geoArea,
            final BiConsumer<Tenant, GeoArea> result) {

        if ((tenant != null) && (geoArea != null)) {
            tenant.setRootArea(geoArea);
            final Tenant savedTenant = tenantRepository.saveAndFlush(tenant);
            geoArea.setTenant(savedTenant);
            final GeoArea savedGeoArea = geoAreaRepository.saveAndFlush(geoArea);
            savedGeoArea.setLeaf(geoArea.isLeaf());
            savedGeoArea.setDepth(geoArea.getDepth());
            savedGeoArea.setCountChildren(geoArea.getCountChildren());
            savedGeoArea.setCountDirectChildren(geoArea.getCountDirectChildren());
            savedTenant.setRootArea(savedGeoArea);
            result.accept(savedTenant, savedGeoArea);
        }
    }

    protected void assignRootAreasToTenants() {

        assignRootAreaToTenant(tenant1, geoAreaTenant1, (updatedTenant, updatedGeoArea) -> {
            tenant1 = updatedTenant;
            geoAreaTenant1 = updatedGeoArea;
        });
        assignRootAreaToTenant(tenant2, geoAreaTenant2, (updatedTenant, updatedGeoArea) -> {
            tenant2 = updatedTenant;
            geoAreaTenant2 = updatedGeoArea;
        });
        assignRootAreaToTenant(tenant3, geoAreaTenant3, (updatedTenant, updatedGeoArea) -> {
            tenant3 = updatedTenant;
            geoAreaTenant3 = updatedGeoArea;
        });
    }

    public void deleteAllData() {
        long start = System.currentTimeMillis();
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        String deleteSummary = transactionTemplate.execute(status -> {

            String selectNameAllTablesQuery;
            String disableReferentialIntegrityQuery;
            String enableReferentialIntegrityQuery;

            switch (environmentService.getActiveDatabaseVendor()) {
                case H2:
                    selectNameAllTablesQuery = QUERY_SELECT_NAME_ALL_TABLES_H2;
                    disableReferentialIntegrityQuery = QUERY_SET_REFERENTIAL_INTEGRITY_FALSE_H2;
                    enableReferentialIntegrityQuery = QUERY_SET_REFERENTIAL_INTEGRITY_TRUE_H2;
                    break;
                case MYSQL:
                    selectNameAllTablesQuery = QUERY_SELECT_NAME_ALL_TABLES_MYSQL;
                    disableReferentialIntegrityQuery = QUERY_SET_REFERENTIAL_INTEGRITY_FALSE_MYSQL;
                    enableReferentialIntegrityQuery = QUERY_SET_REFERENTIAL_INTEGRITY_TRUE_MYSQL;
                    break;
                default:
                    throw new IllegalStateException("Unknown DB vendor");
            }

            @SuppressWarnings("unchecked")
            List<String> tableNames =
                    ((Stream<String>) entityManager.createNativeQuery(selectNameAllTablesQuery).getResultStream())
                            .filter(tableName -> !StringUtils.equalsAnyIgnoreCase(tableName, DELETION_EXCLUDED_TABLES))
                            .collect(Collectors.toList());

            String deleteStatements = tableNames.stream()
                    .map(tn -> "TRUNCATE TABLE " + tn + "; ")
                    .collect(Collectors.joining());

            String deleteQuery =
                    disableReferentialIntegrityQuery + deleteStatements + enableReferentialIntegrityQuery;
            entityManager.createNativeQuery(deleteQuery).executeUpdate();

            return "#entity types: " + tableNames.size();
        });
        entityManager.getEntityManagerFactory().getCache().evictAll();
        log.debug("Deleted all data in " + (System.currentTimeMillis() - start) + " ms: " + deleteSummary);
    }

    public void simulateReferenceDataChange() {

        final ReferenceDataChangeLogEntry changeLogEntry =
                referenceDataChangeService.acquireReferenceDataChangeLock(Person.builder()
                        .id("4711")
                        .firstName("Tom")
                        .lastName("Tester")
                        .build(), "test change", false);
        referenceDataChangeService.releaseReferenceDataChangeLock(changeLogEntry, true);
    }

    protected Person createPerson(String name, Tenant tenant, GeoArea geoArea, Address address, String personId) {

        AddressListEntry addressListEntry = new AddressListEntry(
                addressRepository.save(address),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        String email = "email." + name.toLowerCase() + "@example.com";
        String oauthId = testOauthManagementService.createUser(email, "password", true);
        long nextTimeStamp = nextTimeStamp();
        Person person = personRepository.save(Person.builder()
                .id(personId)
                .created(nextTimeStamp)
                .lastLoggedIn(nextTimeStamp)
                .community(tenant)
                .homeArea(geoArea)
                .firstName("First " + name)
                .lastName("Last " + name)
                .nickName(name)
                .phoneNumber("0631 6800-" + (name.length() * 42))
                .email(email)
                .accountType(AccountType.OAUTH)
                .oauthId(oauthId)
                .addresses(Collections.singleton(addressListEntryRepository.save(addressListEntry)))
                .build());
        // profile picture must be set after the person is persisted due to file ownership
        person.setProfilePicture(createMediaItem("profile-" + name, FileOwnership.of(person)));
        return personRepository.save(person);
    }

    public Person createPerson(String name, Tenant tenant, Address address, String personId) {

        return createPerson(name, tenant, tenant.getRootArea(), address, personId);
    }

    public Person createPersonWithDefaultAddress(String name, Tenant tenant, String personId) {

        return createPersonWithDefaultAddress(name, tenant, tenant.getRootArea(), personId);
    }

    protected Person createPersonWithDefaultAddress(String name, Tenant tenant, GeoArea homeArea, String personId) {

        return createPerson(name, tenant, homeArea, Address.builder()
                .name(name)
                .street("Konrad-Adenauer-Strasse 5")
                .zip("67663")
                .city("Kaiserslautern")
                .gpsLocation(GPS_LOCATION_KL_UNI)
                .build(), personId);
    }

    public Person addVerificationStatusToPerson(Person person, PersonVerificationStatus... status) {
        for (PersonVerificationStatus personVerificationStatus : status) {
            person.getVerificationStatuses().addValue(personVerificationStatus);
        }
        return personRepository.save(person);
    }

    public RoleAssignment assignRoleToPerson(Person person, Class<? extends BaseRole<? extends NamedEntity>> role,
            String entityId) {
        return roleAssignmentRepository.save(new RoleAssignment(person, role, entityId));
    }

    protected Set<AddressListEntry> createAddressSet(Address address) {

        Address newAddress = addressRepository.saveAndFlush(address);
        return Collections.singleton(
                addressListEntryRepository.save(
                        new AddressListEntry(newAddress, "default")
                ));
    }

    protected PushCategory createPushCategory(App app, PushCategoryId pushCategoryId) {
        return pushCategoryRepository.save(PushCategory.builder()
                .app(app)
                .name("name-" + pushCategoryId.getId())
                .description("description-" + pushCategoryId.getId())
                .defaultLoudPushEnabled(true)
                .build()
                .withId(pushCategoryId));
    }

    protected PushCategory createPushCategory(App app, PushCategoryId pushCategoryId,
            PushCategoryId parentPushCategoryId) {
        return pushCategoryRepository.save(PushCategory.builder()
                .app(app)
                .name("name-" + pushCategoryId.getId())
                .description("description-" + pushCategoryId.getId())
                .defaultLoudPushEnabled(true)
                .parentCategory(pushCategoryRepository.findById(parentPushCategoryId.getId()).get())
                .build()
                .withId(pushCategoryId));
    }

    public void unSelectAllGeoAreaForAppVariant(final AppVariant appVariant, final Person person) {

        Optional<AppVariantUsage> appVariantUsageOptional = Optional.ofNullable(
                appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person));

        appVariantUsageOptional.ifPresent(
                appVariantUsage -> appVariantUsageAreaSelectionRepository.deleteByAppVariantUsage(appVariantUsage));
    }

    public AppVariantUsageAreaSelection selectGeoAreaForAppVariant(final AppVariant appVariant, final Person person,
            final GeoArea geoArea) {

        Optional<AppVariantUsage> appVariantUsageOptional = Optional.ofNullable(
                appVariantUsageRepository.findByAppVariantAndPerson(appVariant, person));
        final AppVariantUsage appVariantUsage =
                appVariantUsageOptional.orElseGet(() -> appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                        .appVariant(appVariant)
                        .person(person)
                        .build()));

        return appVariantUsageAreaSelectionRepository.saveAndFlush(AppVariantUsageAreaSelection.builder()
                .appVariantUsage(appVariantUsage)
                .selectedArea(geoArea)
                .build());
    }

    public List<AppVariantUsageAreaSelection> selectGeoAreasForAppVariant(final AppVariant appVariant,
            final Person person, GeoArea... geoAreas) {

        return Arrays.stream(geoAreas)
                .map(geoArea -> selectGeoAreaForAppVariant(appVariant, person, geoArea))
                .collect(Collectors.toList());
    }

    public List<AppVariantUsageAreaSelection> selectGeoAreasForAppVariant(final AppVariant appVariant,
            final Person person, Collection<GeoArea> geoAreas) {

        return geoAreas.stream()
                .map(geoArea -> selectGeoAreaForAppVariant(appVariant, person, geoArea))
                .collect(Collectors.toList());
    }

    public MediaItem createMediaItem(String name) {
        return createMediaItem(name, FileOwnership.UNKNOWN);
    }

    public MediaItem createMediaItem(String name, FileOwnership fileOwnership) {
        MediaItem mediaItem = MediaItem.builder()
                .created(nextTimeStamp())
                .sizeVersion(0)
                .build();
        mediaItem.setFileOwnership(fileOwnership);
        SortedMap<MediaItemSize, String> urls = new TreeMap<>();
        urls.put(MediaItemSize.ORIGINAL_SIZE, name + ".jpg");
        mediaItem.setUrls(urls);
        return mediaItemRepository.saveAndFlush(mediaItem);
    }

    public MediaItem createMediaItemWithMaxUrls(String name) {
        MediaItem mediaItem = MediaItem.builder()
                .created(nextTimeStamp())
                .sizeVersion(0)
                .build();
        SortedMap<MediaItemSize, String> urls = new TreeMap<>();
        int currentImageSizeVersion = config.getMedia().getImageSizeVersions().keySet().stream()
                .max(Integer::compareTo).get();
        urls.put(MediaItemSize.ORIGINAL_SIZE, name + ".jpg");
        config.getMedia().getImageSizeVersions().get(currentImageSizeVersion).stream()
                .map(is -> MediaItemSize.builder()
                        .name(is.getName())
                        .fileSuffix(StringUtils.isEmpty(is.getFileSuffix())
                                ? is.getName()
                                : is.getFileSuffix())
                        .squared(is.isSquared())
                        .maxSize(is.getMaxSize())
                        .build())
                .forEach(mis -> urls.put(mis, name + mis.getFileSuffix() + ".jpg"));
        mediaItem.setUrls(urls);
        return mediaItemRepository.saveAndFlush(mediaItem);
    }

    public MockMultipartFile createTestImageFile() throws IOException {

        return createTestImageFile("testImage.jpg");
    }

    public MockMultipartFile createTestInvalidImageFile() throws IOException {

        return createTestImageFile("application.yml");
    }

    public MockMultipartFile createTestImageFileBig() throws IOException {
        return createTestImageFile("testImageBigger1MB.jpg");
    }

    public ClassPathResource createTestImageTooBigFile() {
        ClassPathResource expectedFileTooBig = new ClassPathResource("/multipartTestImageTooBig.jpg", getClass());
        assertTrue(expectedFileTooBig.exists());
        return expectedFileTooBig;
    }

    public ClassPathResource createTestDocumentTooBigFile() {
        ClassPathResource expectedFileTooBig = new ClassPathResource("/multipartTestImageTooBig.pdf", getClass());
        assertTrue(expectedFileTooBig.exists());
        return expectedFileTooBig;
    }

    public MockMultipartFile createTestImageFile(String fileName) throws IOException {
        return createTestImageFile("file", fileName);
    }

    public MockMultipartFile createTestImageFile(String requestParameterName, String fileName) throws IOException {
        return createTestMultipartFile(requestParameterName, fileName, fileName, "image/jpg");
    }

    public MockMultipartFile createTestMultipartFile(String requestParameterName, String resourceName, String fileName,
            String contentType) throws IOException {
        byte[] imageBytes = loadTestResource(resourceName);
        return new MockMultipartFile(requestParameterName, fileName, contentType, imageBytes);
    }

    public byte[] loadTestResource(String fileName) throws IOException {
        ClassPathResource resource = new ClassPathResource("/" + fileName, getClass());
        return IOUtils.toByteArray(resource.getInputStream());
    }

    public String loadTextTestResource(String fileName) throws IOException {
        ClassPathResource resource = new ClassPathResource("/" + fileName, getClass());
        return IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
    }

    public InputStream loadTestImageFileStream(String fileName) throws IOException {
        ClassPathResource resource = new ClassPathResource("/" + fileName, getClass());
        return resource.getInputStream();
    }

    public TemporaryMediaItem createTemporaryMediaItemWithMaxUrls(Person owner, long created) {
        return temporaryMediaItemRepository.saveAndFlush(TemporaryMediaItem.builder()
                .fileItem(createMediaItemWithMaxUrls(UUID.randomUUID().toString()))
                .owner(owner)
                .expirationTime(0L)
                .created(created)
                .build());
    }

    public TemporaryMediaItem createTemporaryMediaItem(Person owner, long created) {
        return createTemporaryMediaItem(owner, 0L, created);
    }

    public TemporaryMediaItem createTemporaryMediaItem(Person owner, long expirationTime, long created) {
        return temporaryMediaItemRepository.saveAndFlush(TemporaryMediaItem.builder()
                .fileItem(createMediaItem(UUID.randomUUID().toString()))
                .owner(owner)
                .expirationTime(expirationTime)
                .created(created)
                .build());
    }

    public MockMultipartFile createTestDocumentMultipartFile() throws IOException {
        return createTestMultipartFile("file", "document.pdf", "My Document.pdf", "application/pdf");
    }

    public DocumentItem createDocumentItem(String title, MediaType mediaType, FileOwnership fileOwnership) {
        return documentItemRepository.saveAndFlush(DocumentItem.builder()
                .owner(fileOwnership.getOwner())
                .appVariant(fileOwnership.getAppVariant())
                .app(fileOwnership.getApp())
                //this is just an approximation of the correct url, but not necessary for the tests
                .url(fileStorage.getExternalUrl(title + "." + mediaType.getSubtype()))
                .title(title)
                .mediaType(mediaType.toString())
                .created(nextTimeStamp())
                .build());
    }

    public DocumentItem createDocumentItem(String title) {
        return createDocumentItem(title, MediaType.APPLICATION_PDF, FileOwnership.UNKNOWN);
    }

    public DocumentItem createDocumentItem(String title, MediaType mediaType) {
        return createDocumentItem(title, mediaType, FileOwnership.UNKNOWN);
    }

    public TemporaryDocumentItem createTemporaryDocumentItem(Person owner, long expirationTime, long created) {
        return createTemporaryDocumentItem(RandomStringUtils.randomAlphabetic(20), owner, expirationTime, created);
    }

    public TemporaryDocumentItem createTemporaryDocumentItem(String title, Person owner, long expirationTime,
            long created) {
        return temporaryDocumentItemRepository.saveAndFlush(TemporaryDocumentItem.builder()
                .fileItem(createDocumentItem(title, MediaType.APPLICATION_PDF, FileOwnership.of(owner)))
                .owner(owner)
                .expirationTime(expirationTime)
                .created(created)
                .build());
    }

    public Person withoutHomeArea(final Person person) {
        return withHomeArea(person, null);
    }

    public Person withHomeArea(final Person person, final GeoArea homeArea) {
        person.setHomeArea(homeArea);
        return personRepository.saveAndFlush(person);
    }

    public ClientGeoArea toClientGeoArea(final GeoArea geoArea, int depth, boolean isLeaf,
            ClientGeoArea... childGeoAreas) {
        return toClientGeoArea(geoArea, depth, isLeaf, false, childGeoAreas);
    }

    public ClientGeoArea toClientGeoAreaWithBoundaryPoints(final GeoArea geoArea, int depth, boolean isLeaf,
            ClientGeoArea... childGeoAreas) {
        return toClientGeoArea(geoArea, depth, isLeaf, true, childGeoAreas);
    }

    private ClientGeoArea toClientGeoArea(final GeoArea geoArea, int depth, boolean isLeaf,
            final boolean includeGeoJson, ClientGeoArea... childGeoAreas) {
        return ClientGeoArea.builder()
                .id(geoArea.getId())
                .name(geoArea.getName())
                .geoAreaType(geoArea.getType())
                .logo(toClientMediaItem(geoArea.getLogo()))
                .geoJson(includeGeoJson ? geoArea.getBoundaryJson() : null)
                .zip(geoArea.getZip())
                .center(geoArea.getCenter() == null ? null :
                        new ClientGPSLocation(geoArea.getCenter().getLatitude(), geoArea.getCenter().getLongitude()))
                .depth(depth)
                .leaf(isLeaf)
                .childGeoAreas(childGeoAreas == null || childGeoAreas.length == 0 ? null : Arrays.asList(childGeoAreas))
                .customAttributesJson(geoArea.getCustomAttributesJson())
                .build();
    }

    public ClientGeoAreaExtended toClientGeoAreaExtended(final GeoArea geoArea,
            ClientGeoAreaExtended... childGeoAreas) {
        return toClientGeoAreaExtended(geoArea, false, childGeoAreas);
    }

    public ClientGeoAreaExtended toClientGeoAreaExtended(final GeoArea geoArea, int depth,
            boolean isLeaf,
            ClientGeoAreaExtended... childGeoAreas) {
        return toClientGeoAreaExtended(geoArea, depth, isLeaf, false, childGeoAreas);
    }

    public ClientGeoAreaExtended toClientGeoAreaExtendedWithBoundaryPoints(final GeoArea geoArea,
            ClientGeoAreaExtended... childGeoAreas) {
        return toClientGeoAreaExtended(geoArea, true, childGeoAreas);
    }

    public ClientGeoAreaExtended toClientGeoAreaExtendedWithBoundaryPoints(final GeoArea geoArea, int depth,
            boolean isLeaf,
            ClientGeoAreaExtended... childGeoAreas) {
        return toClientGeoAreaExtended(geoArea, depth, isLeaf, true, childGeoAreas);
    }

    @SuppressWarnings("deprecation")
    private ClientGeoAreaExtended toClientGeoAreaExtended(final GeoArea geoArea, Integer depth,
            Boolean isLeaf, final boolean includeGeoJson,
            ClientGeoAreaExtended... childGeoAreas) {
        return ClientGeoAreaExtended.builder()
                .id(geoArea.getId())
                .name(geoArea.getName())
                .geoAreaType(geoArea.getType())
                .logo(toClientMediaItem(geoArea.getLogo()))
                .geoJson(includeGeoJson ? geoArea.getBoundaryJson() : null)
                .zip(geoArea.getZip())
                .center(geoArea.getCenter() == null ? null :
                        new ClientGPSLocation(geoArea.getCenter().getLatitude(), geoArea.getCenter().getLongitude()))
                .depth(depth)
                .leaf(isLeaf)
                .childGeoAreas(childGeoAreas == null || childGeoAreas.length == 0 ? null : Arrays.asList(childGeoAreas))
                .customAttributesJson(geoArea.getCustomAttributesJson())
                .tenantId(BaseEntity.getIdOf(geoArea.getTenant()))
                .build();
    }

    @SuppressWarnings("deprecation")
    private ClientGeoAreaExtended toClientGeoAreaExtended(final GeoArea geoArea, final boolean includeGeoJson,
            ClientGeoAreaExtended... childGeoAreas) {
        return ClientGeoAreaExtended.builder()
                .id(geoArea.getId())
                .parentId(BaseEntity.getIdOf(geoArea.getParentArea()))
                .name(geoArea.getName())
                .geoAreaType(geoArea.getType())
                .logo(toClientMediaItem(geoArea.getLogo()))
                .geoJson(includeGeoJson ? geoArea.getBoundaryJson() : null)
                .zip(geoArea.getZip())
                .center(geoArea.getCenter() == null ? null :
                        new ClientGPSLocation(geoArea.getCenter().getLatitude(), geoArea.getCenter().getLongitude()))
                .depth(geoArea.getDepth())
                .leaf(geoArea.isLeaf())
                .countChildren(geoArea.getCountChildren())
                .countDirectChildren(geoArea.getCountDirectChildren())
                .childGeoAreas(childGeoAreas == null || childGeoAreas.length == 0 ? null : Arrays.asList(childGeoAreas))
                .customAttributesJson(geoArea.getCustomAttributesJson())
                .tenantId(BaseEntity.getIdOf(geoArea.getTenant()))
                .build();
    }

    public ClientMediaItem toClientMediaItem(final MediaItem mediaItem) {
        if (mediaItem == null) {
            return null;
        }
        return ClientMediaItem.builder()
                .id(mediaItem.getId())
                .urls(mediaItem.getUrlsJson())
                .build();
    }

    public ClientDocumentItem toClientDocumentItem(DocumentItem documentItem) {
        if (documentItem == null) {
            return null;
        }
        return ClientDocumentItem.builder()
                .id(documentItem.getId())
                .url(documentItem.getUrl())
                .mediaType(documentItem.getMediaType())
                .title(documentItem.getTitle())
                .description(documentItem.getDescription())
                .build();
    }

    public <B extends BaseEntity, C extends ClientBaseEntity> List<C> toClientEntityList(List<B> entities,
            Function<B, C> mapper) {
        if (entities == null) {
            return Collections.emptyList();
        }
        return entities.stream().map(mapper).collect(Collectors.toList());
    }

    public ClientPersonReference toClientPersonReferenceWithCensoredLastName(Person person) {
        return ClientPersonReference.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(shorten(person.getLastName()))
                .profilePicture(toClientMediaItem(person.getProfilePicture()))
                .status(person.isDeleted() ? ClientPersonStatus.DELETED : ClientPersonStatus.REGISTERED)
                .deleted(person.isDeleted())
                .build();
    }

    public ClientPersonReferenceWithEmail toClientPersonReferenceWithEmail(Person person) {
        return ClientPersonReferenceWithEmail.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .profilePicture(toClientMediaItem(person.getProfilePicture()))
                .status(person.isDeleted() ? ClientPersonStatus.DELETED : ClientPersonStatus.REGISTERED)
                .deleted(person.isDeleted())
                .email(person.getEmail())
                .build();
    }

}
