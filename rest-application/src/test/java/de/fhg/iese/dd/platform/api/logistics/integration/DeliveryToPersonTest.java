/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.integration;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryReceivedRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryReceivedResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupResponse;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportType;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

public class DeliveryToPersonTest extends BaseLogisticsEventTest {

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();
        init();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void deliveryToPerson() throws Exception{

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = ClientPurchaseOrderReceivedEvent.builder()
                .shopOrderId("shopOrderId" + UUID.randomUUID())
                .communityId(tenant1Id)
                .senderShopId(sender1.getId())
                .purchaseOrderNotes("purchaseOrderNotes" + UUID.randomUUID())
                .pickupAddress(ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build())
                .receiverPersonId(receiver.getId())
                .deliveryAddress(ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build())
                .transportNotes("transportNotes" + UUID.randomUUID())
                .orderedItems(Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")))
                .desiredDeliveryTime(
                        new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3))
                .deliveryPoolingStationId("")
                .credits(0)
                .build();

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andReturn();

        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        //Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        Delivery delivery = deliveries.get(0);

        ParcelAddress actualPickupAddress = delivery.getPickupAddress();
        assertEquals(ParcelAddressType.SHOP, actualPickupAddress.getAddressType(), "Type of pickupAddress");
        assertEquals(th.shop1.getId(), actualPickupAddress.getShop().getId(), "Shop of pickupAddress");
        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(), actualPickupAddress.getAddress(),
                "Details of pickupAddress");

        ParcelAddress actualDeliveryAddress = delivery.getDeliveryAddress();
        assertEquals(ParcelAddressType.PRIVATE, actualDeliveryAddress.getAddressType(), "Type of deliveryAddress");
        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(), actualDeliveryAddress.getPerson().getId(),
                "Person of deliveryAddress");
        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(), actualDeliveryAddress.getAddress(),
                "Details of deliveryAddress");

        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime(),
                delivery.getDesiredDeliveryTime().getStartTime(), "DesiredDeliveryTime start");
        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getEndTime(),
                delivery.getDesiredDeliveryTime().getEndTime(), "DesiredDeliveryTime end");

        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(), delivery.getReceiver().getPerson().getId(),
                "Receiver");
        assertEquals(LogisticsParticipantType.PRIVATE, delivery.getReceiver().getParticipantType(), "Receiver");
        assertEquals(purchaseOrderReceivedEvent.getSenderShopId(), delivery.getSender().getShop().getId(), "Sender");
        assertEquals(LogisticsParticipantType.SHOP, delivery.getSender().getParticipantType(), "Sender");

        assertEquals(purchaseOrderReceivedEvent.getTransportNotes(), delivery.getTransportNotes(), "TransportNotes");

        //Check created purchase order
        List<PurchaseOrder> purchaseOrders = th.purchaseOrderRepository.findAll();
        assertEquals(1, purchaseOrders.size(), "Amount of purchaseOrders");
        PurchaseOrder purchaseOrder = purchaseOrders.get(0);

        assertEquals(purchaseOrderReceivedEvent.getPurchaseOrderNotes(), purchaseOrder.getPurchaseOrderNotes(),
                "PurchaseOrderNotes");

        //Check result
        ClientDeliveryCreatedEvent deliveryCreatedEvent =
                toObject(response.getResponse(), ClientDeliveryCreatedEvent.class);

        assertNotNull(deliveryCreatedEvent);
        assertEquals(delivery.getId(), deliveryCreatedEvent.getDeliveryId(), "DeliveryId");
        assertEquals(delivery.getTrackingCode(), deliveryCreatedEvent.getDeliveryTrackingCode(),
                "DeliveryTrackingCode");
        assertEquals(purchaseOrder.getId(), deliveryCreatedEvent.getPurchaseOrderId(), "PurchaseOrderId");

        //--> Check Delivery with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
                .headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(delivery.getId()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.ORDERED.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
        ;

        //PurchaseOrderReadyForTransport
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                ClientPurchaseOrderReadyForTransportRequest.builder()
                        .shopOrderId(purchaseOrderReceivedEvent.getShopOrderId())
                        .size(ClientDimension.builder()
                                .length(40.0)
                                .width(30.0)
                                .height(20.0)
                                .category(DimensionCategory.M)
                                .weight(0.7)
                                .build())
                        .contentNotes("contentNotes")
                        .packagingType(PackagingType.PARCEL)
                        .numParcels(1)
                        .build();

        response = mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        ClientPurchaseOrderReadyForTransportConfirmation purchaseOrderReadyForTransportConfirmation
                = toObject(response.getResponse(), ClientPurchaseOrderReadyForTransportConfirmation.class);

        delivery = th.deliveryRepository.findById(delivery.getId()).get();

        assertFalse(delivery.getTrackingCodeLabelURL().isEmpty(), "TrackingCodeLabel not created");
        assertFalse(delivery.getTrackingCodeLabelA4URL().isEmpty(), "TrackingCodeLabel not created");

        assertNotNull(purchaseOrderReadyForTransportConfirmation);
        assertEquals(delivery.getTrackingCodeLabelURL(),
                purchaseOrderReadyForTransportConfirmation.getTrackingCodeLabelURL(), "TrackingCodeLabel");
        assertEquals(delivery.getTrackingCodeLabelA4URL(),
                purchaseOrderReadyForTransportConfirmation.getTrackingCodeLabelA4URL(), "TrackingCodeLabel");

        //--> Check TransportAlternativeBundle and TransportAlternative with service (any person in community)
        List<TransportAlternativeBundle> transportAlternativeBundles =
                th.transportAlternativeBundleRepository.findAll();

        assertEquals(1, transportAlternativeBundles.size(), "Amount of transportAlternativeBundles");

        TransportAlternativeBundle transportAlternativeBundle = transportAlternativeBundles.get(0);

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();

        //No pooling station in between currently
        assertEquals(1, transportAlternatives.size(), "Amount of transportAlternatives");

        TransportAlternative transportAlternative = transportAlternatives.stream().filter(ta -> ta.getKind() == TransportKind.RECEIVER).findFirst().orElse(null);

        assertNotNull(transportAlternative);

        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(carrier))
                .param("communityId", tenant1Id))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAlternativeBundle.getId()))
                .andExpect(jsonPath("$[0].transportType").value(
                        ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            .andExpect(jsonPath("$[0].pickupAddress.address.name").value(purchaseOrderReceivedEvent.getPickupAddress().getName()))
            .andExpect(jsonPath("$[0].pickupAddress.address.street").value(purchaseOrderReceivedEvent.getPickupAddress().getStreet()))
            .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(purchaseOrderReceivedEvent.getPickupAddress().getZip()))
            .andExpect(jsonPath("$[0].pickupAddress.address.city").value(purchaseOrderReceivedEvent.getPickupAddress().getCity()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$[0].transportAlternatives", hasSize(1)))
            .andExpect(jsonPath("$[0].transportAlternatives[0].id").value(transportAlternative.getId()))
            .andExpect(jsonPath("$[0].transportAlternatives[0].kind").value(TransportKind.RECEIVER.toString()))
            .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            //we currently do not have an intermediate alternative
            //.andExpect(jsonPath("$[0].transportAlternatives[1].kind").value(TransportKind.INTERMEDIATE.toString()))
            ;

        //TransportAlternativeSelectRequest
        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest =
                new ClientTransportAlternativeSelectRequest(transportAlternative.getId());

        response = mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andReturn();
        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        ClientTransportAlternativeSelectResponse transportAlternativeSelectResponse
                = toObject(response.getResponse(), ClientTransportAlternativeSelectResponse.class);

        List<TransportAssignment> transportAssignments = th.transportAssignmentRepository.findAll();

        assertEquals(1, transportAssignments.size(), "Amount of transportAssignments");

        TransportAssignment transportAssignment = transportAssignments.get(0);

        assertTrue(transportAlternativeSelectResponse.isAccepted(), "Accepted");
        assertEquals(transportAlternativeBundle.getId(),
                transportAlternativeSelectResponse.getTransportAlternativeBundleToRemoveId(),
                "TransportAlternativeBundleToRemove");
        assertEquals(transportAlternative.getId(), transportAlternativeSelectResponse.getTransportAlternativeId(),
                "TransportAlternative");
        assertEquals(transportAssignment.getId(), transportAlternativeSelectResponse.getTransportAssignmentToLoadId(),
                "TransportAssignmentToLoad");

        //--> Check TransportAssignment (carrier)
        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(carrier))
                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            .andExpect(jsonPath("$[0].pickupAddress.address.name").value(purchaseOrderReceivedEvent.getPickupAddress().getName()))
            .andExpect(jsonPath("$[0].pickupAddress.address.street").value(purchaseOrderReceivedEvent.getPickupAddress().getStreet()))
            .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(purchaseOrderReceivedEvent.getPickupAddress().getZip()))
            .andExpect(jsonPath("$[0].pickupAddress.address.city").value(purchaseOrderReceivedEvent.getPickupAddress().getCity()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.ACCEPTED.toString()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        //--> Check TransportAlternativeBundle not available (any person in community)
        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", tenant1Id))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)))
            .andExpect(status().isOk());

        //TransportPickupRequest
        ClientTransportPickupRequest transportPickupRequest =
                new ClientTransportPickupRequest(delivery.getTrackingCode(), transportAssignment.getId());

        response = mockMvc.perform(post("/logistics/event/transportPickupRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andReturn();
        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        ClientTransportPickupResponse transportPickupResponse
            = toObject(response.getResponse(), ClientTransportPickupResponse.class);

        assertEquals(transportAssignment.getId(), transportPickupResponse.getTransportAssignmentId(),
                "TransportAssignmentId");

        //--> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(carrier))
                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            .andExpect(jsonPath("$[0].pickupAddress.address.name").value(purchaseOrderReceivedEvent.getPickupAddress().getName()))
            .andExpect(jsonPath("$[0].pickupAddress.address.street").value(purchaseOrderReceivedEvent.getPickupAddress().getStreet()))
            .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(purchaseOrderReceivedEvent.getPickupAddress().getZip()))
            .andExpect(jsonPath("$[0].pickupAddress.address.city").value(purchaseOrderReceivedEvent.getPickupAddress().getCity()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.PICKED_UP.toString()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.IN_DELIVERY.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            ;

        //TransportDeliveredRequest
        ClientTransportDeliveredRequest transportDeliveredRequest = ClientTransportDeliveredRequest.builder()
                .handover(HandoverType.PERSONALLY)
                .deliveryTrackingCode(delivery.getTrackingCode())
                .transportAssignmentId(transportAssignment.getId())
                .handoverNotes("handoverNotes")
                .build();

        response = mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportDeliveredRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();
        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        ClientTransportDeliveredResponse transportDeliveredResponse
                = toObject(response.getResponse(), ClientTransportDeliveredResponse.class);

        assertEquals(transportAssignment.getId(), transportDeliveredResponse.getTransportAssignmentId(),
                "TransportAssignmentId");
        assertEquals(delivery.getTrackingCode(), transportDeliveredResponse.getDeliveryTrackingCode(), "TrackingCode");

        //--> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(carrier))
                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            .andExpect(jsonPath("$[0].pickupAddress.address.name").value(purchaseOrderReceivedEvent.getPickupAddress().getName()))
            .andExpect(jsonPath("$[0].pickupAddress.address.street").value(purchaseOrderReceivedEvent.getPickupAddress().getStreet()))
            .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(purchaseOrderReceivedEvent.getPickupAddress().getZip()))
            .andExpect(jsonPath("$[0].pickupAddress.address.city").value(purchaseOrderReceivedEvent.getPickupAddress().getCity()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.DELIVERED.toString()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.DELIVERED.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            ;

        //DeliveryReceivedRequest
        ClientDeliveryReceivedRequest deliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        response = mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(deliveryReceivedRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();
        //in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        ClientDeliveryReceivedResponse deliveryReceivedResponse
                = toObject(response.getResponse(), ClientDeliveryReceivedResponse.class);

        assertEquals(delivery.getId(), deliveryReceivedResponse.getDeliveryId(), "DeliveryId");
        assertEquals(delivery.getTrackingCode(), deliveryReceivedResponse.getDeliveryTrackingCode(), "TrackingCode");

        //--> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(get("/logistics/transport")
                .headers(authHeadersFor(carrier))
                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(purchaseOrderReceivedEvent.getDeliveryAddress().getCity()))
            .andExpect(jsonPath("$[0].pickupAddress.address.name").value(purchaseOrderReceivedEvent.getPickupAddress().getName()))
            .andExpect(jsonPath("$[0].pickupAddress.address.street").value(purchaseOrderReceivedEvent.getPickupAddress().getStreet()))
            .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(purchaseOrderReceivedEvent.getPickupAddress().getZip()))
            .andExpect(jsonPath("$[0].pickupAddress.address.city").value(purchaseOrderReceivedEvent.getPickupAddress().getCity()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.DELIVERED.toString()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(delivery.getId()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.RECEIVED.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(
                        purchaseOrderReceivedEvent.getDeliveryAddress().getCity()));
    }

}
