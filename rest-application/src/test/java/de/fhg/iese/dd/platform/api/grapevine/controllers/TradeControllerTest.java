/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientTradingCategory;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class TradeControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();
        th.createTradingCategories();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getTradingCategoriesInCorrectOrder() throws Exception {

        final List<ClientTradingCategory> tradingCategories =
                toListOfType(mockMvc.perform(get("/grapevine/trade/tradingCategory"))
                                .andReturn()
                                .getResponse(),
                        ClientTradingCategory.class);

        assertThat(tradingCategories).hasSize(th.tradingCategoriesInOrder.size());

        for (int i = 0; i < th.tradingCategoriesInOrder.size(); i++) {
            assertThat(tradingCategories.get(i).getId())
                    .isEqualTo(th.tradingCategoriesInOrder.get(i).getId());
            assertThat(tradingCategories.get(i).getShortName())
                    .isEqualTo(th.tradingCategoriesInOrder.get(i).getShortName());
            assertThat(tradingCategories.get(i).getDisplayName())
                    .isEqualTo(th.tradingCategoriesInOrder.get(i).getDisplayName());
        }
    }

    @Test
    public void getTradingCategoryById() throws Exception {

        mockMvc.perform(get("/grapevine/trade/tradingCategory/{categoryId}", th.tradingCategoryComputer.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tradingCategoryComputer.getId()))
                .andExpect(jsonPath("$.displayName").value(th.tradingCategoryComputer.getDisplayName()))
                .andExpect(jsonPath("$.shortName").value(th.tradingCategoryComputer.getShortName()));
    }

    @Test
    public void getTradingCategoryByIdNotExisting() throws Exception {

        final String notExisting = "abc";

        mockMvc.perform(get("/grapevine/trade/tradingCategory/{categoryId}", notExisting))
                .andExpect(isException(notExisting, ClientExceptionType.TRADING_CATEGORY_NOT_FOUND));
    }

}
