/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroup;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GroupCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;

public class GroupEventControllerCreateUpdateDeleteTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;
    @Autowired
    private IGroupService groupService;
    private AppVariant appVariantGroupEditEnabled;
    private AppVariant appVariantGroupEditDisabled;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        createFeatureGroupCreationEnabled();

    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private void createFeatureGroupCreationEnabled() {
        appVariantGroupEditEnabled = th.appVariantKL_EB;
        appVariantGroupEditDisabled = th.appVariantMZ;
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantGroupEditEnabled)
                .enabled(true)
                .featureClass(GroupCreationFeature.class.getName())
                .configValues(null)
                .build());
    }

    @Test
    public void createGroup() throws Exception {

        Person caller = th.personRegularKarl;
        final String name = "Beste Gruppe";
        final String shortName = "BG";
        final String description = "Die beste Gruppe überhaupt!";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> includedGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());
        final List<String> excludedGeoAreaIds = Collections.singletonList(th.geoAreaMainz.getId());
        final TemporaryMediaItem logo =
                th.createTemporaryMediaItem(caller, th.nextTimeStamp(), th.nextTimeStamp());

        final ClientGroupCreateRequest request = ClientGroupCreateRequest.builder()
                .name(name)
                .shortName(shortName)
                .description(description)
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .logoTemporaryMediaItemId(logo.getId())
                .mainGeoAreaId(includedGeoAreaIds.get(0))
                .build();
        ClientGroupCreateConfirmation clientGroupCreateConfirmation =
                toObject(mockMvc.perform(groupCreateRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateConfirmation.class);
        ClientGroup clientGroup = clientGroupCreateConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqualCreateRequest(clientGroup, request, caller);
        Group createdGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertEquals(logo.getMediaItem(), createdGroup.getLogo());
        //check used temp images are deleted
        assertThat(th.temporaryMediaItemRepository.count()).isEqualTo(0);

        // same group details but different tenant
        caller = th.personRegularHorstiTenant3;
        final ClientGroupCreateRequest request2 = ClientGroupCreateRequest.builder()
                .name(name)
                .shortName(shortName)
                .description(description)
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .mainGeoAreaId(includedGeoAreaIds.get(0))
                .build();
        clientGroupCreateConfirmation =
                toObject(mockMvc.perform(groupCreateRequest(caller, request2))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateConfirmation.class);
        clientGroup = clientGroupCreateConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqualCreateRequest(clientGroup, request2, caller);

        // same group details and same tenant
        final ClientGroupCreateRequest request3 = ClientGroupCreateRequest.builder()
                .name(name)
                .shortName(shortName)
                .description(description)
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .mainGeoAreaId(includedGeoAreaIds.get(0))
                .build();
        clientGroupCreateConfirmation =
                toObject(mockMvc.perform(groupCreateRequest(caller, request3))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateConfirmation.class);
        clientGroup = clientGroupCreateConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqualCreateRequest(clientGroup, request3, caller);

        // empty description
        caller = th.personDagmarTenant2Mainz;
        final ClientGroupCreateRequest request4 = ClientGroupCreateRequest.builder()
                .name(name)
                .shortName(shortName)
                .description(" ")
                .groupVisibility(visibility)
                .groupAccessibility(accessibility)
                .groupContentVisibility(contentVisibility)
                .includedGeoAreaIds(includedGeoAreaIds)
                .excludedGeoAreaIds(excludedGeoAreaIds)
                .mainGeoAreaId(includedGeoAreaIds.get(0))
                .build();
        clientGroupCreateConfirmation =
                toObject(mockMvc.perform(groupCreateRequest(caller, request4))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateConfirmation.class);
        clientGroup = clientGroupCreateConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqualCreateRequest(clientGroup, request4, caller);
    }

    @Test
    public void createGroup_AutogeneratedShortName() throws Exception {

        createGroupAndCheckShortName(" Beste Gruppe", "BG");
        createGroupAndCheckShortName("Tierfreunde ", "T");
        createGroupAndCheckShortName("Tier-Freunde  ", "TF");
        createGroupAndCheckShortName("Gemeinderat", "G");
        createGroupAndCheckShortName("Gegen alles! (Bürgerinititive für die Kackstelze)", "GA");
        createGroupAndCheckShortName("42 Freunde", "F");
        createGroupAndCheckShortName("💘", "");
        createGroupAndCheckShortName("💘 Katzen-Liebhaber 💞", "KL");
    }

    @Test
    public void createGroup_InvalidAttributes() throws Exception {

        final Person caller = th.personRegularKarl;
        final String name = "Invalide Kruppe";
        final String shortname = "IK";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;

        // name too long
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(StringUtils.repeat('a', th.grapevineConfig.getMaxNumberCharsPerGroupName() + 1))
                        .shortName(shortname)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // shortname too long
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName(StringUtils.repeat('a', th.grapevineConfig.getMaxNumberCharsPerGroupShortName() + 1))
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // description too long
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName(shortname)
                        .description(StringUtils.repeat('a', th.grapevineConfig.getMaxNumberCharsPerGroupDescription() + 1))
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //no included geo areas
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.emptyList())
                        .mainGeoAreaId(th.geoAreaMainz.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        //no main geo area
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        //main geo area not in included
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaMainz.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GROUP_GEO_AREAS_INVALID));
        //excluded contains included
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .includedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .excludedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GROUP_GEO_AREAS_INVALID));
        //invalid geo area id
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(Collections.singletonList("meine-id"))
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        //invalid logo
        mockMvc.perform(groupCreateRequest(caller, ClientGroupCreateRequest.builder()
                        .name(name)
                        .shortName("sn")
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .includedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .excludedGeoAreaId(th.geoAreaMainz.getId())
                        .logoTemporaryMediaItemId("mein-bild")
                        .build()))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
    }

    @Test
    public void createGroup_FeatureDisabled() throws Exception {

        mockMvc.perform(post("/grapevine/group/event/groupCreateRequest")
                        .headers(authHeadersFor(th.personRegularKarl, appVariantGroupEditDisabled))
                        .contentType(contentType)
                        .content(json(ClientGroupCreateRequest.builder()
                                // test for case insensitivity
                                .name("Ohne Feature Nix Los")
                                .groupVisibility(GroupVisibility.ANYONE)
                                .groupAccessibility(GroupAccessibility.PUBLIC)
                                .groupContentVisibility(GroupContentVisibility.ANYONE)
                                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void createGroup_GeoAreaNotFound() throws Exception {

        final Person caller = th.personRegularKarl;
        final String name = "Beste Gruppe";
        final String shortname = "BG";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> geoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), "invalidGeoAreaId");

        // only invalid geo area ids
        mockMvc.perform(groupCreateRequest(caller,
                        ClientGroupCreateRequest.builder()
                                .name(name)
                                .shortName(shortname)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaIds(Collections.singletonList("invalidGeoAreaId"))
                                .excludedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                                .mainGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        mockMvc.perform(groupCreateRequest(caller,
                        ClientGroupCreateRequest.builder()
                                .name(name)
                                .shortName(shortname)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                                .excludedGeoAreaIds(Collections.singletonList("invalidGeoAreaId"))
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        // valid and invalid geo area ids
        mockMvc.perform(groupCreateRequest(caller,
                        ClientGroupCreateRequest.builder()
                                .name(name)
                                .shortName(shortname)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaIds(geoAreaIds)
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        mockMvc.perform(groupCreateRequest(caller,
                        ClientGroupCreateRequest.builder()
                                .name(name)
                                .shortName(shortname)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                                .excludedGeoAreaIds(geoAreaIds)
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        // invalid mainGeoAreaId
        mockMvc.perform(groupCreateRequest(caller,
                        ClientGroupCreateRequest.builder()
                                .name(name)
                                .shortName(shortname)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                                .mainGeoAreaId("invalidGeoAreaId")
                                .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
    }

    @Test
    public void createGroup_TemporaryFileItemNotFound() throws Exception {

        mockMvc.perform(groupCreateRequest(th.personRegularKarl,
                        ClientGroupCreateRequest.builder()
                                .name("Beste Gruppe")
                                .shortName("BG")
                                .groupVisibility(GroupVisibility.ANYONE)
                                .groupAccessibility(GroupAccessibility.PUBLIC)
                                .groupContentVisibility(GroupContentVisibility.ANYONE)
                                .includedGeoAreaIds(
                                        Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId()))
                                .excludedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .logoTemporaryMediaItemId("invalid id")
                                .build()))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void deleteGroup() throws Exception {

        final Person caller = th.personBenTenant1Dorf2;
        final Group group = th.groupSchachvereinAAA;

        assertFalse(group.isDeleted());
        assertThat(th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED)).isEqualTo(
                1);
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).hasSize(1);
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).hasSize(1);
        List<Post> expectedPosts = th.postRepository.findAllByGroupAndDeletedFalse(group);
        assertThat(expectedPosts).hasSize(1);

        mockMvc.perform(groupDeleteRequest(caller, group.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("groupId").value(group.getId()));

        assertTrue(th.groupRepository.findById(group.getId()).get().isDeleted());
        assertThat(th.groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED)).isEqualTo(
                0);
        assertThat(th.groupGeoAreaMappingRepository.findAllByGroupOrderByCreatedDesc(group)).isEmpty();
        assertThat(
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(GroupMembershipAdmin.class,
                        group.getId())).isEmpty();
        assertThat(th.postRepository.findAllByGroupAndDeletedFalse(group)).isEmpty();
    }

    @Test
    public void deleteGroup_InvalidAttributes() throws Exception {

        final Person caller = th.personBenTenant1Dorf2;

        // groupId NULL
        mockMvc.perform(groupDeleteRequest(caller, null))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupDeleteRequest(caller, ""))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void deleteGroup_PermissionDenied() throws Exception {

        final Group group = th.groupAdfcAAP;

        // Caller is not member of the group
        mockMvc.perform(groupDeleteRequest(th.personRegularKarl, group.getId()))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller is not admin of the group
        Person groupMemberNotAdmin = th.personAnjaTenant1Dorf1;
        groupService.joinGroupByAdmin(group, groupMemberNotAdmin, "Added for testing");
        assertTrue(groupService.isGroupMember(group, groupMemberNotAdmin));

        mockMvc.perform(groupDeleteRequest(groupMemberNotAdmin, group.getId()))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertTrue(th.groupRepository.existsById(group.getId()));
    }

    @Test
    public void deleteGroup_GroupNotFound() throws Exception {

        final Person caller = th.personRegularKarl;

        mockMvc.perform(groupDeleteRequest(caller, "invalidGroupId"))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void deleteGroup_FeatureDisabled() throws Exception {

        final Person caller = th.personBenTenant1Dorf2;
        final Group group = th.groupSchachvereinAAA;

        mockMvc.perform(post("/grapevine/group/event/groupDeleteRequest")
                        .headers(authHeadersFor(caller, appVariantGroupEditDisabled))
                        .contentType(contentType)
                        .content(json(ClientGroupDeleteRequest.builder()
                                .groupId(group.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void updateGroup() throws Exception {

        final Person caller = th.personBenTenant1Dorf2;
        Group group = th.groupSchachvereinAAA;
        TemporaryMediaItem newLogo =
                th.createTemporaryMediaItem(caller, th.nextTimeStamp(), th.nextTimeStamp());

        // general update
        MediaItem oldLogo = group.getLogo();
        ClientGroupChangeRequest request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name("Neuer Name 1")
                .shortName("N1")
                .description("Neue Beschreibung")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(
                        Arrays.asList(th.geoAreaRheinlandPfalz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                .excludedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                .mainGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                .logo(ClientMediaItemPlaceHolder.builder()
                        .temporaryMediaItemId(newLogo.getId())
                        .build())
                .build();

        ClientGroupChangeConfirmation clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        ClientGroup clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
        Group updatedGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertEquals(newLogo.getMediaItem(), updatedGroup.getLogo());
        //check used temp images are deleted
        assertThat(th.temporaryMediaItemRepository.count()).isEqualTo(0);
        //check if the old image is deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContain(oldLogo);

        // change geo areas only
        group = updatedGroup;
        MediaItem mediaItemBeforeUpdate = group.getLogo();
        request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name("Neuer Name 2")
                .shortName("N2")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                .excludedGeoAreaIds(Arrays.asList(th.geoAreaMainz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .logo(ClientMediaItemPlaceHolder.builder()
                        .mediaItemId(group.getLogo().getId())
                        .build())
                .build();

        clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
        // check that the logo is still the same
        updatedGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertEquals(mediaItemBeforeUpdate, updatedGroup.getLogo());

        // remove logo
        group = updatedGroup;
        oldLogo = group.getLogo();
        assertNotNull(oldLogo);
        request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name("Neuer Name 3")
                .shortName("N3")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                .excludedGeoAreaIds(Arrays.asList(th.geoAreaMainz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .build();

        clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
        // check that no logo is set
        updatedGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertNull(updatedGroup.getLogo());
        //Check that we do not produce orphans
        assertFalse(th.mediaItemRepository.existsById(oldLogo.getId()));

        // add logo to group without logo
        group = updatedGroup;
        assertNull(group.getLogo());
        newLogo = th.createTemporaryMediaItem(caller, th.nextTimeStamp(), th.nextTimeStamp());
        request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name("Neuer Name 4")
                .shortName("N4")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .excludedGeoAreaId(th.geoAreaMainz.getId())
                .excludedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .logo(ClientMediaItemPlaceHolder.builder()
                        .temporaryMediaItemId(newLogo.getId())
                        .build())
                .build();

        clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
        updatedGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertEquals(newLogo.getMediaItem(), updatedGroup.getLogo());

        // change name to already existing name
        request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name(th.groupDorfgeschichteSSA.getName())
                .description("Neue Beschreibung")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(
                        Arrays.asList(th.geoAreaRheinlandPfalz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                .excludedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                .mainGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                .build();

        clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
    }

    @Test
    public void updateGroup_AutogeneratedShortName() throws Exception {

        updateGroupAndCheckShortName(" Beste Gruppe", "BG");
        updateGroupAndCheckShortName("Tierfreunde ", "T");
        updateGroupAndCheckShortName("Tier-Freunde  ", "TF");
        updateGroupAndCheckShortName("Gemeinderat", "G");
        updateGroupAndCheckShortName("Gegen alles! (Bürgerinititive für die Kackstelze)", "GA");
        updateGroupAndCheckShortName("42 Freunde", "F");
        updateGroupAndCheckShortName("💘", "");
        updateGroupAndCheckShortName("💘 Katzen-Liebhaber 💞", "KL");
    }

    @Test
    public void updateGroup_InvalidAttributes() throws Exception {

        final Person caller = th.personChloeTenant1Dorf1;
        final String groupId = th.groupAdfcAAP.getId();
        final String name = "Neuer Name";
        final String shortName = "NN";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;

        // groupId NULL
        mockMvc.perform(groupChangeRequest(caller,
                        ClientGroupChangeRequest.builder()
                                .groupId(null)
                                .name(name)
                                .shortName(shortName)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // groupId empty
        mockMvc.perform(groupChangeRequest(caller,
                        ClientGroupChangeRequest.builder()
                                .groupId("")
                                .name(name)
                                .shortName(shortName)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // name too long
        int charCount = th.grapevineConfig.getMaxNumberCharsPerGroupName() + 1;
        mockMvc.perform(groupChangeRequest(caller,
                        ClientGroupChangeRequest.builder()
                                .groupId(groupId)
                                .name(StringUtils.repeat("a", charCount))
                                .shortName(shortName)
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // shortName too long
        charCount = th.grapevineConfig.getMaxNumberCharsPerGroupShortName() + 1;
        mockMvc.perform(groupChangeRequest(caller,
                        ClientGroupChangeRequest.builder()
                                .groupId(groupId)
                                .name(name)
                                .shortName(StringUtils.repeat("a", charCount))
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // description too long
        mockMvc.perform(groupChangeRequest(caller,
                        ClientGroupChangeRequest.builder()
                                .groupId(groupId)
                                .name(name)
                                .shortName(shortName)
                                .description(
                                        StringUtils.repeat('a', th.grapevineConfig.getMaxNumberCharsPerGroupDescription() + 1))
                                .groupVisibility(visibility)
                                .groupAccessibility(accessibility)
                                .groupContentVisibility(contentVisibility)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void updateGroup_PermissionDenied() throws Exception {

        final Group group = th.groupAdfcAAP;

        ClientGroupChangeRequest request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name("Neuer Name")
                .shortName("NN")
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .build();

        // Caller is not member of the group
        mockMvc.perform(groupChangeRequest(th.personRegularKarl, request))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller is not admin of the group
        Person groupMemberNotAdmin = th.personAnjaTenant1Dorf1;
        assertTrue(groupService.isGroupMember(group, groupMemberNotAdmin));
        mockMvc.perform(groupChangeRequest(groupMemberNotAdmin, request))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void updateGroup_FeatureDisabled() throws Exception {

        mockMvc.perform(post("/grapevine/group/event/groupChangeRequest")
                        .headers(authHeadersFor(th.personChloeTenant1Dorf1, appVariantGroupEditDisabled))
                        .contentType(contentType)
                        .content(json(ClientGroupChangeRequest.builder()
                                .groupId(th.groupAdfcAAP.getId())
                                .name("Neuer Cooler Name")
                                .groupVisibility(GroupVisibility.ANYONE)
                                .groupAccessibility(GroupAccessibility.PUBLIC)
                                .groupContentVisibility(GroupContentVisibility.ANYONE)
                                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void updateGroup_GroupNotFound() throws Exception {

        mockMvc.perform(groupChangeRequest(th.personRegularKarl, ClientGroupChangeRequest.builder()
                        .groupId("InvalidGroupId")
                        .name("Neuer Name")
                        .shortName("NN")
                        .groupVisibility(GroupVisibility.ANYONE)
                        .groupAccessibility(GroupAccessibility.PUBLIC)
                        .groupContentVisibility(GroupContentVisibility.ANYONE)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void updateGroup_GeoAreaNotFound() throws Exception {

        final Person caller = th.personChloeTenant1Dorf1;
        final String groupId = th.groupAdfcAAP.getId();
        final String name = "Neuer Name";
        final String shortName = "NN";
        final GroupVisibility visibility = GroupVisibility.ANYONE;
        final GroupAccessibility accessibility = GroupAccessibility.PUBLIC;
        final GroupContentVisibility contentVisibility = GroupContentVisibility.ANYONE;
        final List<String> validGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId());
        final List<String> invalidGeoAreaIds =
                Arrays.asList(th.geoAreaDorf1InEisenberg.getId(), "invalidGeoAreaId");

        // only invalid geo area ids
        mockMvc.perform(groupChangeRequest(caller, ClientGroupChangeRequest.builder()
                        .groupId(groupId)
                        .name(name)
                        .shortName(shortName)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId("invalidGeoAreaId")
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        mockMvc.perform(groupChangeRequest(caller, ClientGroupChangeRequest.builder()
                        .groupId(groupId)
                        .name(name)
                        .shortName(shortName)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(validGeoAreaIds)
                        .excludedGeoAreaId("invalidGeoAreaId")
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        // valid and invalid geo area ids
        mockMvc.perform(groupChangeRequest(caller, ClientGroupChangeRequest.builder()
                        .groupId(groupId)
                        .name(name)
                        .shortName(shortName)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaIds(invalidGeoAreaIds)
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        mockMvc.perform(groupChangeRequest(caller, ClientGroupChangeRequest.builder()
                        .groupId(groupId)
                        .name(name)
                        .shortName(shortName)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .excludedGeoAreaIds(invalidGeoAreaIds)
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        // invalid mainGeoAreaId
        mockMvc.perform(groupChangeRequest(caller, ClientGroupChangeRequest.builder()
                        .groupId(groupId)
                        .name(name)
                        .shortName(shortName)
                        .groupVisibility(visibility)
                        .groupAccessibility(accessibility)
                        .groupContentVisibility(contentVisibility)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .mainGeoAreaId("invalidGeoAreaId")
                        .build()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        assertFalse(th.groupRepository.existsByTenantAndNameIgnoreCase(th.tenant1, name));
    }

    @Test
    public void updateGroup_TemporaryFileItemNotFound() throws Exception {

        mockMvc.perform(groupChangeRequest(th.personBenTenant1Dorf2, ClientGroupChangeRequest.builder()
                        .groupId(th.groupSchachvereinAAA.getId())
                        .name("Neuer Name")
                        .shortName("NN")
                        .groupVisibility(GroupVisibility.ANYONE)
                        .groupAccessibility(GroupAccessibility.PUBLIC)
                        .groupContentVisibility(GroupContentVisibility.ANYONE)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .includedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .logo(ClientMediaItemPlaceHolder.builder()
                                .temporaryMediaItemId("invalid id")
                                .build())
                        .build()))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void updateGroup_FileItemNotFound() throws Exception {

        mockMvc.perform(groupChangeRequest(th.personBenTenant1Dorf2, ClientGroupChangeRequest.builder()
                        .groupId(th.groupSchachvereinAAA.getId())
                        .name("Neuer Name")
                        .shortName("NN")
                        .groupVisibility(GroupVisibility.ANYONE)
                        .groupAccessibility(GroupAccessibility.PUBLIC)
                        .groupContentVisibility(GroupContentVisibility.ANYONE)
                        .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .includedGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                        .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .logo(ClientMediaItemPlaceHolder.builder()
                                .mediaItemId("invalid id")
                                .build())
                        .build()))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder groupCreateRequest(Person caller, ClientGroupCreateRequest request)
            throws IOException {

        return post("/grapevine/group/event/groupCreateRequest")
                .headers(authHeadersFor(caller, appVariantGroupEditEnabled))
                .contentType(contentType)
                .content(json(request));
    }

    private MockHttpServletRequestBuilder groupDeleteRequest(Person caller, String groupId) throws IOException {

        return post("/grapevine/group/event/groupDeleteRequest")
                .headers(authHeadersFor(caller, appVariantGroupEditEnabled))
                .contentType(contentType)
                .content(json(ClientGroupDeleteRequest.builder()
                        .groupId(groupId)
                        .build()));
    }

    private MockHttpServletRequestBuilder groupChangeRequest(Person caller, ClientGroupChangeRequest request)
            throws IOException {

        return post("/grapevine/group/event/groupChangeRequest")
                .headers(authHeadersFor(caller, appVariantGroupEditEnabled))
                .contentType(contentType)
                .content(json(request));
    }

    private void createGroupAndCheckShortName(String name, String expectedShortName) throws Exception {
        Person caller = th.personRegularKarl;
        final ClientGroupCreateRequest request = ClientGroupCreateRequest.builder()
                .name(name)
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .mainGeoAreaId(th.geoAreaDorf1InEisenberg.getId())
                .build();
        ClientGroupCreateConfirmation clientGroupCreateConfirmation =
                toObject(mockMvc.perform(groupCreateRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupCreateConfirmation.class);
        ClientGroup clientGroup = clientGroupCreateConfirmation.getCreatedGroup();

        assertGroupDetailsAreEqualCreateRequest(clientGroup, request, caller);
        Group createdGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertThat(createdGroup.getShortName()).isEqualTo(expectedShortName);
    }

    private void updateGroupAndCheckShortName(String name, String expectedShortName) throws Exception {

        final Person caller = th.personBenTenant1Dorf2;
        Group group = th.groupSchachvereinAAA;
        ClientGroupChangeRequest request = ClientGroupChangeRequest.builder()
                .groupId(group.getId())
                .name(name)
                .groupVisibility(GroupVisibility.ANYONE)
                .groupAccessibility(GroupAccessibility.PUBLIC)
                .groupContentVisibility(GroupContentVisibility.ANYONE)
                .includedGeoAreaIds(
                        Arrays.asList(th.geoAreaRheinlandPfalz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                .excludedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                .mainGeoAreaId(th.geoAreaDorf2InEisenberg.getId())
                .build();
        ClientGroupChangeConfirmation clientGroupChangeConfirmation =
                toObject(mockMvc.perform(groupChangeRequest(caller, request))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(), ClientGroupChangeConfirmation.class);
        ClientGroup clientGroup = clientGroupChangeConfirmation.getUpdatedGroup();

        assertGroupDetailsAreEqualChangeRequest(clientGroup, request);
        Group createdGroup = th.groupRepository.findById(clientGroup.getId()).get();
        assertThat(createdGroup.getShortName()).isEqualTo(expectedShortName);
    }

    private void assertGroupDetailsAreEqualChangeRequest(ClientGroup actualGroup,
            ClientGroupChangeRequest changeRequest) {

        assertEquals(StringUtils.trim(changeRequest.getName()), actualGroup.getName());
        if (changeRequest.getShortName() != null) {
            assertEquals(StringUtils.trim(changeRequest.getShortName()), actualGroup.getShortName());
        }
        assertGroupDescriptionIsEqual(changeRequest.getDescription(), actualGroup.getDescription());
        assertEquals(changeRequest.getGroupVisibility(), actualGroup.getVisibility());
        assertEquals(changeRequest.getGroupAccessibility(), actualGroup.getAccessibility());
        assertEquals(changeRequest.getGroupContentVisibility(), actualGroup.getContentVisibility());
        assertEquals(changeRequest.getMainGeoAreaId(), actualGroup.getMainGeoAreaId());

        final Group changedGroup = th.groupRepository.findById(actualGroup.getId()).get();
        assertGeoAreasMatch(changedGroup, changeRequest.getMainGeoAreaId(), changeRequest.getIncludedGeoAreaIds(),
                changeRequest.getExcludedGeoAreaIds());
    }

    private void assertGroupDetailsAreEqualCreateRequest(ClientGroup actualGroup,
            ClientGroupCreateRequest createRequest, Person creator) {

        assertEquals(StringUtils.trim(createRequest.getName()), actualGroup.getName());
        if (createRequest.getShortName() != null) {
            assertEquals(StringUtils.trim(createRequest.getShortName()), actualGroup.getShortName());
        }
        assertGroupDescriptionIsEqual(createRequest.getDescription(), actualGroup.getDescription());
        assertEquals(createRequest.getGroupVisibility(), actualGroup.getVisibility());
        assertEquals(createRequest.getGroupAccessibility(), actualGroup.getAccessibility());
        assertEquals(createRequest.getGroupContentVisibility(), actualGroup.getContentVisibility());
        assertEquals(createRequest.getMainGeoAreaId(), actualGroup.getMainGeoAreaId());
        assertFalse(actualGroup.isDeleted());
        assertThat(actualGroup.getMemberCount()).isEqualTo(1);

        final Group createdGroup = th.groupRepository.findById(actualGroup.getId()).get();
        assertThat(createdGroup.getCreator()).isEqualTo(creator);
        assertGeoAreasMatch(createdGroup, createRequest.getMainGeoAreaId(), createRequest.getIncludedGeoAreaIds(),
                createRequest.getExcludedGeoAreaIds());

        assertTrue(th.groupMembershipRepository.existsByGroupAndMemberAndStatus(createdGroup, creator,
                GroupMembershipStatus.APPROVED));
        assertTrue(th.roleAssignmentRepository.existsByPersonAndRole(creator, GroupMembershipAdmin.class));
    }

    private void assertGeoAreasMatch(Group expectedGroup, String mainGeoAreaId, List<String> geoAreaIdsToBeIncluded,
            List<String> geoAreaIdsToBeExcluded) {
        IGroupService.GroupGeoAreaIds geoAreaIds =
                th.findGeoAreaIdsByGroups(Collections.singleton(expectedGroup)).values().iterator().next();
        assertThat(geoAreaIds.getMainGeoAreaId()).isEqualTo(mainGeoAreaId);
        if (geoAreaIds.getIncludedGeoAreaIds().isEmpty()) {
            assertThat(geoAreaIdsToBeIncluded).isNullOrEmpty();
        } else {
            assertThat(geoAreaIds.getIncludedGeoAreaIds())
                    .containsAll(geoAreaIdsToBeIncluded);
            assertTrue(geoAreaIds.getIncludedGeoAreaIds().contains(mainGeoAreaId));
        }
        if (geoAreaIds.getExcludedGeoAreaIds().isEmpty()) {
            assertThat(geoAreaIdsToBeExcluded).isNullOrEmpty();
        } else {
            assertThat(geoAreaIds.getExcludedGeoAreaIds())
                    .containsAll(geoAreaIdsToBeExcluded);
            assertFalse(geoAreaIds.getExcludedGeoAreaIds().contains(mainGeoAreaId));
        }
    }

    private void assertGroupDescriptionIsEqual(String requestDescription, String expectedDescription) {
        requestDescription = StringUtils.trim(requestDescription);
        if (requestDescription != null && requestDescription.isEmpty()) {
            assertNull(expectedDescription);
        } else {
            assertEquals(requestDescription, expectedDescription);
        }
    }

}
