/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.motivation.MotivationTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormat;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormatVisitor;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;

public class MotivationDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private MotivationTestHelper motivationTestHelper;

    @Autowired
    private DataPrivacyDataCollectionRepository dataCollectionRepository;

    @Override
    public void createEntities() throws Exception {
        motivationTestHelper.createTenantsAndGeoAreas();
        motivationTestHelper.createPersons();
        motivationTestHelper.createShops();
        motivationTestHelper.createAchievementRules();
        motivationTestHelper.createPushEntities();
        motivationTestHelper.createScoreEntities();
        motivationTestHelper.createAchievementEntities();
    }

    @Override
    public void tearDown() throws Exception {
        motivationTestHelper.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return motivationTestHelper.personVgAdmin;
    }

    @Test
    public void dataPrivacyReportContainsAccountData() {

        Person person = getPersonForPrivacyReport();
        String digiTalerSenderName = motivationTestHelper.personIeseAdmin.getFullName();

        assertNotNull(person);

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        Stream.of(DataPrivacyReportFormat.values()).forEach(format -> {
            //log which report is handled (for test debugging)
            log.info("Processing {} report", format);

            final String report = converterService.convert(dataPrivacyReport, format);

            assertFalse(report.isEmpty(), format + " report should not be empty");
            assertTrue(report.contains("Informationen zum Account"));
            assertTrue(report.contains("Aktuelles Guthaben"));
            assertTrue(report.contains("70 DigiTaler"));
            assertTrue(report.contains("DigiTaler-Transaktionen"));
            assertTrue(report.contains("Initial score"));
            assertTrue(report.contains("Inquiry 1 offerer account entry"));
            assertTrue(report.contains("10 DigiTaler von " + digiTalerSenderName + " erhalten."));
            assertTrue(report.contains(
                    format.accept(new DataPrivacyReportFormatVisitor<String>() {
                        @Override
                        public String whenHtml() {
                            return "10 DigiTaler als &#39;erhalten von " + digiTalerSenderName + "&#39; vorgemerkt.";
                        }

                        @Override
                        public String whenText() {
                            return "10 DigiTaler als 'erhalten von " + digiTalerSenderName + "' vorgemerkt.";
                        }

                        @Override
                        public String whenJson() {
                            return whenText();
                        }
                    })
            ));
        });
    }

    @Test
    public void dataPrivacyReportContainsAchievementData() {

        IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(motivationTestHelper.personPoolingStationOp,
                dataPrivacyReport);

        Stream.of(DataPrivacyReportFormat.values()).forEach(format -> {
            //log which report is handled (for test debugging)
            log.info("Processing {} report", format);

            final String report = converterService.convert(dataPrivacyReport, format);
            assertFalse(report.isEmpty(), format + " report should not be empty");
            assertTrue(report.contains("Auszeichnung"));
            assertTrue(report.contains("erhaltene Auszeichnungen"));
            assertTrue(report.contains("Novize"));
            assertTrue(report.contains("Rufe Service A einmal auf"));
        });
    }

    @Override
    public Person getPersonForDeletion() {
        return motivationTestHelper.personPoolingStationOp;
    }

    @Test
    public void motivationDataIsDeleted() {

        final IPersonDeletionReport personDeletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), personDeletionReport);

        assertThat(personDeletionReport).is(
                containsEntityWithOperation(motivationTestHelper.achievementPersonPairingPoolingStationOp,
                        DeleteOperation.DELETED));
        assertThat(motivationTestHelper.achievementPersonPairingRepository.existsById(
                motivationTestHelper.achievementPersonPairingPoolingStationOp.getId())).isFalse();

    }

    @Test
    public void accountDataIsDeleted() {

        final List<AccountEntry> accountEntries = Arrays.asList(motivationTestHelper.offerInquiryAccountEntry1_1,
                motivationTestHelper.offerInquiryAccountEntry1_2,
                motivationTestHelper.offerInquiryAccountEntry2_1, motivationTestHelper.offerInquiryAccountEntry2_2,
                motivationTestHelper.seekingInquiryAccountEntry1_1, motivationTestHelper.seekingInquiryAccountEntry1_2,
                motivationTestHelper.seekingInquiryAccountEntry2_1, motivationTestHelper.seekingInquiryAccountEntry2_2);

        for (AccountEntry accountEntry : accountEntries) {
            assertThat(motivationTestHelper.accountEntryRepository.findById(
                    accountEntry.getId()).get().getPostingText()).isNotNull();
        }

        final IPersonDeletionReport personDeletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(motivationTestHelper.personIeseAdmin, personDeletionReport);

        for (AccountEntry accountEntry : accountEntries) {
            assertThat(personDeletionReport).is(containsEntityWithOperation(accountEntry, DeleteOperation.ERASED));
            assertThat(motivationTestHelper.accountEntryRepository.findById(
                    accountEntry.getId()).get().getPostingText()).isNull();
        }
    }

}
