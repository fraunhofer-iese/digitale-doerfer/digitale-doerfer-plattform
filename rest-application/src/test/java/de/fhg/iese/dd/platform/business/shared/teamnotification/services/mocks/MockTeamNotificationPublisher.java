/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.services.mocks;

import java.util.function.Supplier;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.teamnotification.ITeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationSendRequest;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Profile({"test"})
@Component
@Getter
@Log4j2
public class MockTeamNotificationPublisher implements ITeamNotificationPublisher {

    private String lastChannel = null;
    private String lastTopic = null;
    private Tenant lastTenant = null;
    private String lastMessage = null;

    private int countMessages = 0;

    @Setter
    private Supplier<RuntimeException> exceptionOnSend;

    @Override
    public void sendTeamNotification(TeamNotificationSendRequest request) {
        lastChannel = request.getChannel();
        lastTopic = request.getTopic();
        lastTenant = request.getTenant();
        lastMessage = request.getMessage().getPlainText();
        countMessages++;
        log.log(request.getMessage().getPriority().getLogLevel(),
                "Team notification: {}\n{}\n-----------------\n{}\n-----------------",
                lastTopic, request.getTenantName("-"), lastMessage);
        if (exceptionOnSend != null) {
            RuntimeException exception = exceptionOnSend.get();
            log.error("Throwing test exception: {}", exception.getMessage());
            throw exception;
        }
    }

    @Override
    public void shutdownAndWait() {
        log.info("shutting down {}", getName());
    }

    public void reset() {
        lastChannel = null;
        lastTopic = null;
        lastTenant = null;
        lastMessage = null;
        countMessages = 0;
        exceptionOnSend = null;
    }

}
