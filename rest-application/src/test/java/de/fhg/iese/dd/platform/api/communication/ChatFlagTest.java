/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatFlagRequest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.config.UserGeneratedContentFlagConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagStatusRecordRepository;

public class ChatFlagTest extends BaseServiceTest {

    @Autowired
    private CommunicationTestHelper th;
    @Autowired
    private UserGeneratedContentFlagRepository flagRepository;
    @Autowired
    private UserGeneratedContentFlagStatusRecordRepository flagStatusRecordRepository;

    @Autowired
    private UserGeneratedContentFlagConfig userGeneratedContentFlagConfig;

    private Chat flaggedEntity;
    private Person flaggedEntityAuthor;
    private Person flagCreator;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
        th.createChatEntities();

        flaggedEntity = th.chat1;
        flagCreator = th.personIeseAdmin;
        flaggedEntityAuthor = th.personVgAdmin; //the second chat participant
        assertThat(flaggedEntity).isNotNull();
        assertThat(flagCreator).isNotNull();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void flagChat() throws Exception {

        final ClientChatFlagRequest flagRequest = ClientChatFlagRequest.builder()
                .chatId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(createRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flagCreator.getTenant().getId());
        assertTrue(flagRepository.existsById(response.getUserGeneratedContentFlagId()));
        UserGeneratedContentFlag flag = flagRepository.findById(response.getUserGeneratedContentFlagId()).get();
        assertEquals(flaggedEntityAuthor, flag.getEntityAuthor());
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        //now test that also filterStatus of chat is set correctly
        mockMvc.perform(get("/communication/chat/" + flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagChatMultipleTimes() throws Exception {

        assertEquals(0, flagStatusRecordRepository.count());

        final ClientChatFlagRequest flagRequest = ClientChatFlagRequest.builder()
                .chatId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(createRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        final ClientChatFlagRequest flagRequest2 = ClientChatFlagRequest.builder()
                .chatId(flaggedEntity.getId())
                .comment("Der is schon wieder echt gemein, ey!")
                .build();

        MvcResult createRequestResult2 = mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest2)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response2 = toObject(createRequestResult2.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertEquals(response.getUserGeneratedContentFlagId(), response2.getUserGeneratedContentFlagId());
        assertEquals(1, flagRepository.count());
        assertEquals(2, flagStatusRecordRepository.count());
    }

    @Test
    public void flagChatUnauthorized() throws Exception {

        final ClientChatFlagRequest flagRequest = ClientChatFlagRequest.builder()
                .chatId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey! Meine Mutter is nicht fett!")
                .build();

        assertOAuth2(post("/communication/chat/event/chatFlagRequest")
                .contentType(contentType)
                .content(json(flagRequest)));
    }

    @Test
    public void flagChatMissingEventAttributes() throws Exception {

        mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(ClientChatFlagRequest.builder()
                                .chatId(flaggedEntity.getId())
                                .comment(StringUtils.repeat('X',
                                        userGeneratedContentFlagConfig.getMaxNumberCharsPerFlagComment() + 1))
                                .build())))
                .andExpect(isException("comment", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(ClientChatFlagRequest.builder()
                                .chatId(flaggedEntity.getId())
                                .build())))
                .andExpect(isException("comment", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/communication/chat/event/chatFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(ClientChatFlagRequest.builder()
                                .comment("Der is echt gemein, ey!")
                                .build())))
                .andExpect(isException("chatId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

}
