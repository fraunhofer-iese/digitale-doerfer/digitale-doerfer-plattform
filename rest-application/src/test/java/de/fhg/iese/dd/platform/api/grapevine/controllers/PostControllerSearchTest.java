/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.commons.lang3.StringUtils;
import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
@ExtendWith(AssumeIntegrationTestExtension.class)
public class PostControllerSearchTest extends BasePostControllerTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    @Test
    public void searchPosts() throws Exception {

        final Gossip post = th.gossip2WithComments;
        String searchTerm = "Restaurant";
        final Person callerCreator = post.getCreator();
        final Person callerOther = th.personNinaBauer;
        assertThat(post.getText()).contains(searchTerm);
        assertThat(callerCreator).isNotEqualTo(callerOther);

        final AppVariant appVariant = th.appVariant1Tenant1;
        final Collection<GeoArea> selectedGeoAreasCallerOther =
                Arrays.asList(th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg, th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);

        assertThat(post.getGeoAreas()).containsAnyElementsOf(selectedGeoAreasCallerOther);

        th.unSelectAllGeoAreaForAppVariant(appVariant, callerCreator);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerOther);
        th.selectGeoAreasForAppVariant(appVariant, callerOther, selectedGeoAreasCallerOther);

        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //creator always gets own posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(post.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(post.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.created").value(post.getCreated()))
                .andExpect(jsonPath("$.content[0].gossip.images", hasSize(post.getImages().size())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(post.getText()));

        //caller with selected area gets posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerOther, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(post.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(post.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.created").value(post.getCreated()))
                .andExpect(jsonPath("$.content[0].gossip.images", hasSize(post.getImages().size())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(post.getText()));

        th.unSelectAllGeoAreaForAppVariant(appVariant, callerOther);

        //caller without selected area gets no posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerOther, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    public void searchPosts_OnlyHomeArea() throws Exception {

        final Gossip post = th.gossip2WithComments;
        String searchTerm = "Restaurant";
        final Person callerCreator = post.getCreator();
        final Person callerSameHome = th.personNinaBauer;
        final Person callerOtherHome = th.personAnnikaSchneider;
        assertThat(post.getText()).contains(searchTerm);
        assertThat(callerCreator).isNotEqualTo(callerSameHome);
        assertThat(callerCreator).isNotEqualTo(callerOtherHome);
        final AppVariant appVariant = th.appVariant1Tenant1;

        post.setHiddenForOtherHomeAreas(true);
        post.setGeoAreas(Set.of(th.geoAreaDorf1InEisenberg));
        //this is to test if the creator is still able to get the post
        callerCreator.setHomeArea(th.geoAreaKaiserslautern);
        callerSameHome.setHomeArea(th.geoAreaDorf1InEisenberg);
        callerOtherHome.setHomeArea(th.geoAreaDorf2InEisenberg);
        th.postRepository.save(post);
        th.personRepository.save(callerSameHome);
        th.personRepository.save(callerOtherHome);
        th.personRepository.save(callerCreator);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerCreator);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerSameHome);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerOtherHome);

        grapevineSearchService.indexOrUpdateIndexedPost(post);

        final Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(
                        th.geoAreaDorf2InEisenberg,
                        th.geoAreaDorf1InEisenberg,
                        th.geoAreaEisenberg,
                        th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);

        th.unSelectAllGeoAreaForAppVariant(appVariant, callerSameHome);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerOtherHome);
        th.unSelectAllGeoAreaForAppVariant(appVariant, callerCreator);
        th.selectGeoAreasForAppVariant(appVariant, callerSameHome, selectedGeoAreas);
        th.selectGeoAreasForAppVariant(appVariant, callerOtherHome, selectedGeoAreas);
        th.selectGeoAreasForAppVariant(appVariant, callerCreator, selectedGeoAreas);

        //creator always gets own posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(post.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(post.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.created").value(post.getCreated()))
                .andExpect(jsonPath("$.content[0].gossip.images", hasSize(post.getImages().size())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(post.getText()));

        //caller with some home area gets posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerSameHome, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(post.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(post.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.created").value(post.getCreated()))
                .andExpect(jsonPath("$.content[0].gossip.images", hasSize(post.getImages().size())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(post.getText()));

        //caller with different home area gets no posts
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerOtherHome, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    public void searchPosts_InternalSuggestions() throws Exception {

        final Suggestion post = th.suggestionInternal;
        String searchTerm = "intern";
        final Person callerCreator = post.getCreator();

        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //internal suggestions can not be searched
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    public void searchPosts_DeletedPost() throws Exception {

        final Gossip post = th.gossip2WithComments;
        String searchTerm = "Restaurant";
        final Person callerCreator = post.getCreator();
        post.setDeleted(true);
        th.postRepository.save(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //deleted posts can not be searched
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));

        post.setDeleted(false);
        th.postRepository.save(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //undeleted post can be searched
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].gossip.id").value(post.getId()));

        post.setDeleted(true);
        th.postRepository.save(post);

        //post that is still in the index but deleted can not be searched
        mockMvc.perform(get("/grapevine/search")
                .param("search", searchTerm)
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(callerCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    public void searchPosts_SearchOffline() throws Exception {

        elasticsearchTestUtil.disableElasticsearch();

        mockMvc.perform(get("/grapevine/search")
                .param("search", "Off the line")
                .param("page", "0")
                .param("count", "10")
                .headers(authHeadersFor(th.personNinaBauer, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.SEARCH_NOT_AVAILABLE));

        elasticsearchTestUtil.enableElasticsearch();
    }

    @Test
    public void searchPosts_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/search")
                .param("search", "suche")
                .param("page", "0")
                .param("count", "1"), th.appVariant4AllTenants, th.personNinaBauer);
    }

    @Test
    public void searchPosts_InvalidParameters() throws Exception {

        //search parameter missing
        mockMvc.perform(get("/grapevine/search")
                        .param("page", "0")
                        .param("count", "1")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //search parameter too short
        mockMvc.perform(get("/grapevine/search")
                .param("search", "ab")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));

        //search parameter too long
        mockMvc.perform(get("/grapevine/search")
                .param("search", StringUtils.repeat("x", 101))
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_LONG));

        //invalid page param
        mockMvc.perform(get("/grapevine/search")
                .param("search", "abcde")
                .param("page", "-1")
                .param("count", "1")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/grapevine/search")
                .param("search", "abcde")
                .param("page", "0")
                .param("count", "-1")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

}
