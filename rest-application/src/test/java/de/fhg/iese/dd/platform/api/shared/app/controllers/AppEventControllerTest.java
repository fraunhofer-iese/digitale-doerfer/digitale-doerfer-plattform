/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantChangeSelectedGeoAreasRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

public class AppEventControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private AppVariant appVariantNoGeoAreas;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();

        appVariantNoGeoAreas = th.appVariantRepository.save(AppVariant.builder()
                .name("app variant no tenant")
                .appVariantIdentifier("appvariant.no.tenant")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(th.app1)
                .oauthClients(Collections.singleton(th.oauthClientRepository.save(OauthClient.builder()
                        .oauthClientIdentifier("oauth-client-no-tenant")
                        .build())))
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_SingleSelection() throws Exception {

        assertEquals(0,
                th.appVariantUsageRepository.count(),
                "No app variant usages should be defined to test the default behavior");
        assertEquals(0,
                th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined to test the default behavior");

        final Person person = th.personRegularAnna;
        //take care not to set the home area, otherwise it is returned as selected area
        th.withoutHomeArea(person);
        final AppVariant appVariant = th.app1Variant1;

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaKaiserslautern.getId()))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(),
                th.geoAreaRheinlandPfalz.getId(),
                th.geoAreaKaiserslautern.getId()
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_MultipleSelections() throws Exception {

        assertEquals(0, th.appVariantUsageRepository.count(),
                "No app variant usages should be defined to test the default behavior");
        assertEquals(0, th.appVariantUsageAreaSelectionRepository.count(),
                "No app variant usage area selections should be defined to test the default behavior");

        final Person person = th.personRegularAnna;
        final AppVariant appVariant = th.app2Variant1;

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(
                                Arrays.asList(th.geoAreaMainz.getId(), th.geoAreaDorf2InEisenberg.getId()))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(), //no tenant
                th.geoAreaRheinlandPfalz.getId(), //no tenant
                th.geoAreaMainz.getId(), //selected
                th.geoAreaEisenberg.getId(), //up the hierarchy and in the tenants of the app variant
                th.geoAreaDorf2InEisenberg.getId() //selected
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_SelectionUpdate() throws Exception {

        appVariantChangeSelectedGeoAreasRequest_SingleSelection();

        assertEquals(1, th.appVariantUsageRepository.count());

        final Person person = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaEisenberg.getId()))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(),
                th.geoAreaRheinlandPfalz.getId(),
                th.geoAreaEisenberg.getId()
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_SelectionNotInAvailableAreas() throws Exception {

        final AppVariant appVariant = th.app1Variant1;

        ClientAppVariantChangeSelectedGeoAreasRequest request = ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                .build();

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(th.personRegularAnna,
                appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(isException(ClientExceptionType.APP_VARIANT_USAGE_INVALID)));
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_AppVariantWithoutAvailableAreas() throws Exception {

        ClientAppVariantChangeSelectedGeoAreasRequest request = ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaMainz.getId()))
                .build();

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(th.personRegularAnna,
                appVariantNoGeoAreas,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(isException(ClientExceptionType.APP_VARIANT_USAGE_INVALID)));
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_SelectedAreaNoTenant() throws Exception {

        final Person person = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;
        final String geoAreaId = th.geoAreaDeutschland.getId(); //no tenant

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(geoAreaId))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(),
                th.geoAreaRheinlandPfalz.getId(),
                th.geoAreaEisenberg.getId(),
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_HomeAreaInAvailableGeoAreas() throws Exception {

        //the home area is in the available geo areas of the app variant, so it should be added to the selection, together with the parents
        final Person person = th.withHomeArea(th.personRegularAnna, th.geoAreaDorf1InEisenberg);
        final AppVariant appVariant = th.app1Variant1;
        final String selectedGeoAreaId = th.geoAreaKaiserslautern.getId();

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(selectedGeoAreaId))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(), //parent
                th.geoAreaRheinlandPfalz.getId(), //parent of selectedGeoAreaId and parent of parent of home area
                selectedGeoAreaId,
                th.geoAreaEisenberg.getId(), //parent of home area
                person.getHomeArea().getId() //home area
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_HomeAreaNotInAvailableGeoAreas() throws Exception {

        //the home area is not in the available geo areas of the app variant, so it should not be added to the selection
        GeoArea homeArea = th.geoAreaTenant3;
        final Person person = th.withHomeArea(th.personRegularAnna, homeArea);
        final AppVariant appVariant = th.app1Variant1;
        //not really clean to call this service here, but it is tested at other locations before
        assertThat(appService.getAvailableGeoAreas(appVariant)).doesNotContain(homeArea);
        final String selectedGeoAreaId = th.geoAreaDorf1InEisenberg.getId();

        final ClientAppVariantChangeSelectedGeoAreasRequest request =
                ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(selectedGeoAreaId))
                        .build();

        final String[] expectedResult = {
                th.geoAreaDeutschland.getId(),
                th.geoAreaRheinlandPfalz.getId(),
                th.geoAreaEisenberg.getId(),
                th.geoAreaDorf1InEisenberg.getId()
        };

        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(person, appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(request)),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.personId").value(person.getId()))
                        .andExpect(jsonPath("$.newSelectedGeoAreaIds", containsInAnyOrder(expectedResult))));

        waitForEventProcessing();

        assertEquals(1, th.appVariantUsageRepository.count());
        assertEquals(expectedResult.length, th.appVariantUsageAreaSelectionRepository.count());
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_InvalidParameters() throws Exception {

        final AppVariant appVariant = th.app1Variant1;

        //empty selection
        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(th.personRegularAnna,
                appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                                .newSelectedGeoAreaIds(Collections.emptyList())
                                .build())),
                r -> r.andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID)));

        //null selection
        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(th.personRegularAnna,
                appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                                .newSelectedGeoAreaIds(null)
                                .build())),
                r -> r.andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID)));

        //invalid id
        callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(th.personRegularAnna,
                appVariant,
                () -> post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .contentType(contentType)
                        .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                                .newSelectedGeoAreaIds(
                                        Arrays.asList(BaseTestHelper.INVALID_UUID, th.geoAreaKaiserslautern.getId()))
                                .build())),
                r -> r.andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND)));

        //invalid app variant identifier
        final OauthClient oauthClientWithoutAppVariant = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-oauthClientWithoutAppVariant-identifier")
                .build());

        mockMvc.perform(post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .person(th.personRegularAnna)
                                .oauthClient(oauthClientWithoutAppVariant)
                                .appVariantIdentifier("invalidAppVariantIdentifier")
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                                .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaKaiserslautern.getId()))
                                .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        mockMvc.perform(post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .person(th.personRegularAnna)
                                .oauthClient(oauthClientWithoutAppVariant)
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                                .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaKaiserslautern.getId()))
                                .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void appVariantChangeSelectedGeoAreasRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/app/event/appVariantChangeSelectedGeoAreasRequest")
                .contentType(contentType)
                .content(json(ClientAppVariantChangeSelectedGeoAreasRequest.builder()
                        .newSelectedGeoAreaIds(Collections.singletonList(th.geoAreaDorf1InEisenberg.getId()))
                        .build())), th.app1Variant1, th.personRegularAnna);
    }

}
