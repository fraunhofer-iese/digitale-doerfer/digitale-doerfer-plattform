/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsTestHelper;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientUpdatedSellingPoint;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IRawLocationRecordService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPointStatus;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.enums.LocationSourceType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.SellingPointRepository;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.SellingVehicleRepository;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

@Component
public class MobileKioskTestHelper extends BaseLogisticsTestHelper {

    public static final String TEST_TENANT_ID = "4d0e0ccc-9adc-44bc-a8de-6d54109c0a48";
    private static final String ERBENDORF_ADDRESS_ID = "6a53dda0-cfc8-47a6-a3ff-b5045ca5160e";
    private static final String FALKENBERG_ADDRESS_ID = "9ab7c0ed-9bbf-45a1-b28d-d5749b219c3d";
    private static final String TIRSCHENREUTH_ADDRESS_ID = "88629d44-102c-4185-8d6a-c8d3be677767";
    private static final String SP1_ID = "76eeaad1-5de7-45a9-ab04-237e6a66f9d1";
    private static final String SP2_ID = "9ceefc1b-29f4-4e17-bf99-123e4b769185";
    private static final String SP3_ID = "041f9f16-d424-4338-835e-17d0a3ee7c10";
    private static final String SP4_ID = "c7c97537-25a9-4c24-8a8a-6332386b0569";
    private static final String SP5_ID = "72a476dc-b9d7-425f-9cf2-3d0d97d46c90";
    private static final String SP6_ID = "709ab4de-0c64-46eb-8875-8675c315e659";
    private static final String SP7_ID = "7b5d42b5-b779-44fb-8a30-a44ec4ea4bae";
    private static final String SP8_ID = "edb7c423-1799-4abd-ae2b-e413a22d02f9";
    private static final String SP9_ID = "d9666ae6-2121-4128-a021-244903a27e05";
    private static final String SP10_ID = "ce292bad-69a7-43a0-9ff0-36ba8f83afc1";
    private static final String SP11_ID = "734fe439-5d13-4bb1-a0d1-5f504821dd81";
    private static final String TRUCK_ID = "man1";
    private static final String CAR_ID = "car1";

    public Address erbendorf;
    public Address falkenberg;
    public Address tirschenreuth;

    public Person personVgAdminInTestTenant;
    public Person personTruckDriver;
    public Person personCarDriver;

    public SellingPoint sp1;
    public SellingPoint sp2;
    public SellingPoint sp3;
    public SellingPoint sp4;
    public SellingPoint sp5;
    public SellingPoint            sp6;
    public SellingPoint            sp7;
    public SellingPoint            sp8;
    public SellingPoint            sp9;
    public SellingPoint            sp10;
    public SellingPoint            sp11;

    public SellingVehicle          truck;
    public SellingVehicle          car;

    public List<RawLocationRecord> truckLocationRecords;

    public Tenant tenant;

    @Autowired
    private SellingPointRepository sellingPointRepository;

    @Autowired
    private SellingVehicleRepository sellingVehicleRepository;

    @Autowired
    private IRoleService           roleService;

    @Autowired
    private ITimeService           timeService;

    @Autowired
    private IRawLocationRecordService rawLocationRecordService;

    @Autowired
    protected MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Override
    protected Set<String> achievementRulesToCreate() {
        Set<String> achievementRules = super.achievementRulesToCreate();
        achievementRules.add("score.account.received.1");
        return achievementRules;
    }

    public void createTenantsAndVgAdmin() {

        super.createTenantsAndGeoAreas();

        tenant = tenantRepository.save(Tenant.builder()
                .id(TEST_TENANT_ID)
                .name("Selling Point Test Tenant")
                .build());

        AddressListEntry addressListEntry = new AddressListEntry(
                addressRepository.save(
                        Address.builder()
                                .name("Super Wichtig")
                                .street("Mozartstraße 4")
                                .zip("67308")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.4409838, 7.7682621)).build()),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        personVgAdminInTestTenant = Person.builder()
                .id("78eaadc0-e61c-42a1-b42c-57305a9a9453")
                .firstName("Super")
                .lastName("Wichtig")
                .nickName("sw")
                .phoneNumber("06131828281")
                .email("digdorfdev+vgtestcomm@gmail.com")
                .accountType(AccountType.OAUTH)
                .addresses(Collections.singleton((addressListEntryRepository.save(addressListEntry))))
                .build();
        personVgAdminInTestTenant.setTenant(tenant);
        personVgAdminInTestTenant.setCreated(nextTimeStamp());
        personVgAdminInTestTenant.setOauthId("custom|vgadmintestcomm");
        personVgAdminInTestTenant = personRepository.save(personVgAdminInTestTenant);
        roleService.assignRoleToPerson(personVgAdminInTestTenant, LogisticsAdminUiRestrictedAdmin.class, tenant);
    }

    public void createVehiclesAndDrivers() {

        truck = sellingVehicleRepository.save(SellingVehicle.builder()
                .id(TRUCK_ID)
                .name("Großer LKW")
                .community(tenant)
                .build());

        car = sellingVehicleRepository.save(SellingVehicle.builder()
                .id(CAR_ID)
                .name("Auto")
                .community(tenant)
                .build());

        AddressListEntry addressListEntry = new AddressListEntry(
                addressRepository.save(
                        Address.builder()
                                .name("Truck Driver")
                                .street("Mozartstraße 4")
                                .zip("67308")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.4409838, 7.7682621)).build()),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        personTruckDriver = Person.builder()
                .id("e7eb7bdf-a004-4cb0-b752-60728901849c")
                .firstName("Truck")
                .lastName("Driver")
                .nickName("td")
                .phoneNumber("0613182828")
                .email("digdorfdev+td@gmail.com")
                .accountType(AccountType.OAUTH)
                .addresses(Collections.singleton((addressListEntryRepository.save(addressListEntry))))
                .build();
        personTruckDriver.setTenant(tenant);
        personTruckDriver.setCreated(nextTimeStamp());
        personTruckDriver.setOauthId("custom|td");
        personTruckDriver = personRepository.save(personTruckDriver);
        roleService.assignRoleToPerson(personTruckDriver, SellingVehicleDriver.class, truck);

        addressListEntry = new AddressListEntry(
                addressRepository.save(
                        Address.builder()
                                .name("Car Driver")
                                .street("Mozartstraße 4")
                                .zip("67308")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.4409838, 7.7682621)).build()),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        personCarDriver = Person.builder()
                .id("382672a7-a753-4728-8dbd-1a9814ebb134")
                .firstName("Car")
                .lastName("Driver")
                .nickName("cd")
                .phoneNumber("0613182828")
                .email("digdorfdev+cd@gmail.com")
                .accountType(AccountType.OAUTH)
                .addresses(Collections.singleton((addressListEntryRepository.save(addressListEntry))))
                .build();
        personCarDriver.setTenant(tenant);
        personCarDriver.setCreated(nextTimeStamp());
        personCarDriver.setOauthId("custom|cd");
        personCarDriver = personRepository.save(personCarDriver);
        roleService.assignRoleToPerson(personCarDriver, SellingVehicleDriver.class, car);
    }

    @Override
    public void createScoreEntities() {
        super.createScoreEntities();

        TenantAccount accountCommunity = new TenantAccount();
        accountCommunity.setOwner(tenant);
        accountCommunity.setAppAccountType(AppAccountType.LOGISTICS);
        accountCommunity.setBalance(100);
        accountCommunity.setCreated(nextTimeStamp());
        accountCommunity = tenantAccountRepository.save(accountCommunity);
    }

    public void createSellingPoints() {
        ZonedDateTime now = timeService.nowLocal();

        erbendorf = addressRepository.save(Address.builder()
                .id(ERBENDORF_ADDRESS_ID)
                .name("Erbendorf Marktplatz")
                .zip("92681")
                .city("Erbendorf")
                .gpsLocation(new GPSLocation(49.837384, 12.049411))
                .street("Unterer Markt 15")
                .build());

        falkenberg = addressRepository.save(Address.builder()
                .id(FALKENBERG_ADDRESS_ID)
                .name("Falkenberg Marktplatz")
                .zip("95685")
                .city("Falkenberg")
                .gpsLocation(new GPSLocation(49.858783, 12.224679))
                .street("Marktplatz 7")
                .build());

        tirschenreuth = addressRepository.save(Address.builder()
                .id(TIRSCHENREUTH_ADDRESS_ID)
                .name("Tirschenreuth Park")
                .zip("95643")
                .city("Tirschenreuth")
                .gpsLocation(new GPSLocation(49.881138, 12.335547))
                .street("Mühlbühlstraße 4")
                .build());

        sp1 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP1_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(erbendorf)
                        .plannedStay(getPastSellingPointStayOnDay(now, 0, 1, 2))
                        .status(SellingPointStatus.ARCHIVED)
                        .build());

        sp2 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP2_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getPastSellingPointStayOnDay(now, 0, 2, 2))
                        .status(SellingPointStatus.ARCHIVED)
                        .build());

        sp3 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP3_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(tirschenreuth)
                        .plannedStay(getFutureSellingPointStayOnDay(now, 0, 1, 1))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp4 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP4_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(erbendorf)
                        .plannedStay(getSellingPointStayOnDay(now, 1, 1, 4))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp5 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP5_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(tirschenreuth)
                        .plannedStay(getSellingPointStayOnDay(now, 1, 2, 4))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp6 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP6_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getSellingPointStayOnDay(now, 1, 3, 4))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp7 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP7_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(erbendorf)
                        .plannedStay(getSellingPointStayOnDay(now, 1, 4, 4))
                        .status(SellingPointStatus.DELETED)
                        .build());

        sp8 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP8_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getSellingPointStayOnDay(now, 2, 1, 1))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp9 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP9_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getSellingPointStayOnDay(now, 3, 1, 1))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp10 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP10_ID)
                        .vehicle(car)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getSellingPointStayOnDay(now, 4, 1, 1))
                        .status(SellingPointStatus.ACTIVE)
                        .build());

        sp11 = sellingPointRepository.save(
                SellingPoint.builder()
                        .id(SP11_ID)
                        .vehicle(truck)
                        .community(tenant)
                        .location(falkenberg)
                        .plannedStay(getSellingPointStayOnDay(now, -1, 1, 1))
                        .status(SellingPointStatus.ARCHIVED)
                        .build());
    }

    private long getTimestamp(ZonedDateTime localDateTime) {
        return localDateTime
                .toInstant()
                .toEpochMilli();
    }

    public TimeSpan getPastSellingPointStayOnDay(ZonedDateTime current, int dayDifference, long numberOfStayOneBased, long totalNumberOfStays) {
        if(numberOfStayOneBased<1) numberOfStayOneBased = 1;
        long millisBetweenStartOfDayAndCurrent = getTimestamp(current)-getTimestamp(current.truncatedTo(ChronoUnit.DAYS));
        long millisBetweenSellingPointStarts = Math.round(millisBetweenStartOfDayAndCurrent/(totalNumberOfStays+1L));
        long millisForSellingPointStartFromStartOfDay = numberOfStayOneBased * millisBetweenSellingPointStarts;
        ZonedDateTime start = current.truncatedTo(ChronoUnit.DAYS).plusDays(dayDifference).plus(millisForSellingPointStartFromStartOfDay, ChronoUnit.MILLIS);
        return new TimeSpan(
                getTimestamp(start),
                getTimestamp(start.plus(Math.round(millisBetweenSellingPointStarts/2), ChronoUnit.MILLIS)));
    }

    public TimeSpan getFutureSellingPointStayOnDay(ZonedDateTime current, int dayDifference, long numberOfStayOneBased, long totalNumberOfStays) {
        if(numberOfStayOneBased<1) numberOfStayOneBased = 1;
        long millisBetweenEndOfDayAndCurrent = getTimestamp(current.plusDays(1).truncatedTo(ChronoUnit.DAYS))-getTimestamp(current);
        long millisBetweenSellingPointStarts = Math.round(millisBetweenEndOfDayAndCurrent/(totalNumberOfStays+1L));
        long millisForSellingPointStartBeforeEndOfDay = numberOfStayOneBased * millisBetweenSellingPointStarts;
        ZonedDateTime start = current.truncatedTo(ChronoUnit.DAYS).plusDays(dayDifference+1).minus(millisForSellingPointStartBeforeEndOfDay, ChronoUnit.MILLIS);
        return new TimeSpan(
                getTimestamp(start),
                getTimestamp(start.plus(Math.round(millisBetweenSellingPointStarts/2), ChronoUnit.MILLIS)));
    }

    public TimeSpan getSellingPointStayOnDay(ZonedDateTime current, int dayDifference, long numberOfStayOneBased, long totalNumberOfStays) {
        if(numberOfStayOneBased<1) numberOfStayOneBased = 1;
        long millisBetweenStartOfDayAndCurrent = getTimestamp(current.truncatedTo(ChronoUnit.DAYS).plusDays(1))-getTimestamp(current.truncatedTo(ChronoUnit.DAYS));
        long millisBetweenSellingPointStarts = Math.round(millisBetweenStartOfDayAndCurrent/(totalNumberOfStays+1L));
        long millisForSellingPointStartFromStartOfDay = numberOfStayOneBased * millisBetweenSellingPointStarts;
        ZonedDateTime start = current.truncatedTo(ChronoUnit.DAYS).plusDays(dayDifference).plus(millisForSellingPointStartFromStartOfDay, ChronoUnit.MILLIS);
        return new TimeSpan(
                getTimestamp(start),
                getTimestamp(start.plus(Math.round(millisBetweenSellingPointStarts/2), ChronoUnit.MILLIS)));
    }

    public ClientUpdatedSellingPoint getClientUpdatedSellingPointForSellingPoint(SellingPoint sellingPoint) {
        return ClientUpdatedSellingPoint.builder()
                .vehicleId(sellingPoint.getVehicle().getId())
                .vehicleName(sellingPoint.getVehicle().getName())
                .plannedArrival(sellingPoint.getPlannedStay().getStartTime())
                .plannedDeparture(sellingPoint.getPlannedStay().getEndTime())
                .location(ClientAddress.builder()
                        .city(sellingPoint.getLocation().getCity())
                        .name(sellingPoint.getLocation().getName())
                        .street(sellingPoint.getLocation().getStreet())
                        .zip(sellingPoint.getLocation().getZip())
                        .build())
                .build();
    }

    public void createLocationHistory() {
        long time = timeService.currentTimeMillisUTC();
        truckLocationRecords = new ArrayList<>();
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(1.0)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time-120000)
                .location(new GPSLocation(49.427398, 7.749107))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(0.8)
                .sourceType(LocationSourceType.UNDEFINED)
                .timestamp(time-100000)
                .location(new GPSLocation(49.429864, 7.750052))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(1.0)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time-65000)
                .location(new GPSLocation(49.431867, 7.751533))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(0.9)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time-50000)
                .location(new GPSLocation(49.432844, 7.752320))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(0.7)
                .sourceType(LocationSourceType.UNDEFINED)
                .timestamp(time-40000)
                .location(new GPSLocation(49.432717, 7.754221))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(1.0)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time-30000)
                .location(new GPSLocation(49.432718, 7.754219))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(1.0)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time-20000)
                .location(new GPSLocation(49.432718, 7.754219))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(0.8)
                .sourceType(LocationSourceType.UNDEFINED)
                .timestamp(time-10000)
                .location(new GPSLocation(49.431811, 7.753789))
                .build()));
        truckLocationRecords.add(rawLocationRecordService.store(RawLocationRecord.builder()
                .vehicle(truck)
                .accuracy(0.9)
                .sourceId(personTruckDriver.getId())
                .sourceType(LocationSourceType.DRIVER_APP)
                .timestamp(time)
                .location(new GPSLocation(49.431016, 7.752899))
                .build()));
    }

}
