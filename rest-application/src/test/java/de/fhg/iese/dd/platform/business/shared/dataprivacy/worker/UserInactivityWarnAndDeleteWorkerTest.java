/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.worker;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInactivityDataPrivacyService;

public class UserInactivityWarnAndDeleteWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private UserInactivityWarningMailWorker userInactivityWarningMailWorker;
    @Autowired
    private UserInactivitySetPendingDeletionWorker userInactivitySetPendingDeletionWorker;
    @Autowired
    private UserInactivityDeleteWarnedWorker userInactivityDeleteWarnedWorker;
    @Autowired
    private UserInactivityDeleteUnverifiedEmailWorker userInactivityDeleteUnverifiedEmailWorker;

    @MockBean
    private IInactivityDataPrivacyService inactivityDataPrivacyServiceMock;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPersons();
    }

    @Override
    public void tearDown() {
        
        th.deleteAllData();
    }

    /**
     * We only verify interaction with the service in this test. The actual functionality of the service is tested in
     * SharedDataPrivacyServiceTest
     */
    @Test
    public void sendWarningEmailToInactivePersons() {

        userInactivityWarningMailWorker.run();
        Mockito.verify(inactivityDataPrivacyServiceMock, Mockito.times(1))
                .sendWarningEmailToInactivePersons();
    }

    @Test
    public void setPendingDeletionOfUnverifiedEmailInactivePersons() {

        userInactivitySetPendingDeletionWorker.run();
        Mockito.verify(inactivityDataPrivacyServiceMock, Mockito.times(1))
                .setPendingDeletionOfUnverifiedEmailInactivePersons();
    }

    @Test
    public void deletePendingDeletionWarnedPersons() {

        userInactivityDeleteWarnedWorker.run();
        Mockito.verify(inactivityDataPrivacyServiceMock, Mockito.times(1))
                .deletePendingDeletionWarnedPersons();
    }

    @Test
    public void deletePendingDeletionUnverifiedEmailPersons() {

        userInactivityDeleteUnverifiedEmailWorker.run();
        Mockito.verify(inactivityDataPrivacyServiceMock, Mockito.times(1))
                .deletePendingDeletionUnverifiedEmailPersons();
    }

}
