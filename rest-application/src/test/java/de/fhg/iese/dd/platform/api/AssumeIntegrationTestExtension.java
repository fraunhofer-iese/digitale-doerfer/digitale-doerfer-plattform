/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Test Rule that helps to prevent the execution of a complete test class if the current test run is no integration test
 * run. This rule activates the integration test if the environment or system property {@code dd.test.integration.run}
 * (value of {@link BaseServiceTest#FLAG_RUN_INTEGRATION_TESTS}) is set to {@code true}
 * <p>
 * The reason for this is to prevent JUnit starting up a complete Application just to find out that all tests use
 * {@code
 *
 * @ExtendWith(AssumeIntegrationTestExtension.class)}. <p>
 */
public class AssumeIntegrationTestExtension implements BeforeAllCallback, BeforeEachCallback {

    private static final String FLAG_RUN_INTEGRATION_TESTS = "dd.test.integration.run";

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        assumeTrue(integrationTestActivated(),
                "Integration test run (" + FLAG_RUN_INTEGRATION_TESTS + ") not active, skipping all tests");
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        assumeTrue(integrationTestActivated(),
                "Integration test run (" + FLAG_RUN_INTEGRATION_TESTS + ") not active, skipping test");
    }

    private static boolean integrationTestActivated() {
        boolean systemPropertyRunIntegrationTests = StringUtils.equalsAnyIgnoreCase(
                System.getProperty(FLAG_RUN_INTEGRATION_TESTS),
                "true", "yes");
        boolean environmentPropertyRunIntegrationTests = StringUtils.equalsAnyIgnoreCase(
                System.getenv(FLAG_RUN_INTEGRATION_TESTS),
                "true", "yes");
        return systemPropertyRunIntegrationTests || environmentPropertyRunIntegrationTests;
    }

}
