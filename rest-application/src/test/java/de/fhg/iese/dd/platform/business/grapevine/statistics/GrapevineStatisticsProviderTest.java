/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Johannes Eveslage, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReport;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static de.fhg.iese.dd.platform.business.grapevine.statistics.GrapevineStatisticsProvider.*;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.NEW;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.TOTAL;
import static org.assertj.core.api.Assertions.assertThat;

public class GrapevineStatisticsProviderTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private IStatisticsService statisticsService;

    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void generateStatisticsReport_Views() {
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2023-03-21T12:01:34+01:00[Europe/Berlin]"));
        final long currentTime = timeServiceMock.currentTimeMillisUTC();
        final Person person = th.personSusanneChristmann;
        final Person person2 = th.personFriedaFischer;
        final Person person3 = th.personLaraSchaefer;
        Gossip gossip1 = th.gossip4;
        Gossip gossip2 = th.gossip5;

        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip1)
                .person(person)
                .viewedDetailTime(currentTime)
                .viewedOverviewTime(currentTime)
                .build());
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip1)
                .person(person2)
                .viewedDetailTime(currentTime)
                .viewedOverviewTime(currentTime)
                .build());
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip1)
                .person(person3)
                .viewedDetailTime(currentTime)
                .build());
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip2)
                .person(person)
                .viewedDetailTime(currentTime)
                .viewedOverviewTime(currentTime)
                .build());

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT)
                .statisticsId(STATISTICS_ID_POST_VIEW_DETAIL_COUNT)
                .statisticsId(STATISTICS_ID_POST_COUNT_WITH_IMAGE)
                .startTimeNew(currentTime - TimeUnit.DAYS.toMillis(7))
                .endTime(currentTime)
                .allGeoAreas(true)
                .build();
        StatisticsReport statisticsReport = statisticsService.generateStatisticsReport(statisticsReportDefinition);
        assertThat(statisticsReport.getTitle()).isEqualTo("Current statistics for test at 21.03.2023 12:01:34");
        assertThat(statisticsReport.getDefinition()).isEqualTo(statisticsReportDefinition);
        assertThat(statisticsReport.getCreationTime()).isEqualTo(currentTime);
        assertThat(statisticsReport.getGeoAreaStatistics()).hasSize(8);

        GeoAreaStatistics statisticsDeutschland = findStatisticsFor(statisticsReport, th.geoAreaDeutschland);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, NEW).getValue())
                .isEqualTo(3L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, TOTAL).getValue())
                .isEqualTo(4L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, NEW).getValue())
                .isEqualTo(4L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, TOTAL).getValue())
                .isEqualTo(34L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, NEW).getValue())
                .isEqualTo(34L);

        GeoAreaStatistics statisticsEisenberg = findStatisticsFor(statisticsReport, th.geoAreaEisenberg);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, NEW).getValue())
                .isEqualTo(3L);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, TOTAL).getValue())
                .isEqualTo(4L);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, NEW).getValue())
                .isEqualTo(4L);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, TOTAL).getValue())
                .isEqualTo(29L);
        assertThat(statisticsEisenberg.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, NEW).getValue())
                .isEqualTo(29L);

        GeoAreaStatistics statisticsK1 = findStatisticsFor(statisticsReport, th.geoAreaDorf1InEisenberg);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, NEW).getValue())
                .isEqualTo(3L);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, TOTAL).getValue())
                .isEqualTo(4L);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_VIEW_DETAIL_COUNT, NEW).getValue())
                .isEqualTo(4L);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, TOTAL).getValue())
                .isEqualTo(16L);
        assertThat(statisticsK1.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, NEW).getValue())
                .isEqualTo(16L);
    }

    @Test
    public void generateStatisticsReport_Happenings() {
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2023-03-21T12:01:34+01:00[Europe/Berlin]"));
        final long currentTime = timeServiceMock.currentTimeMillisUTC();
        Happening happeningPersonCreated1 = th.happening1;
        happeningPersonCreated1.setCreated(currentTime);
        happeningPersonCreated1.setCreator(th.personThomasBecker);
        happeningPersonCreated1.setNewsSource(null);
        th.postRepository.save(happeningPersonCreated1);
        Happening happeningPersonCreated2 = th.happening2;
        happeningPersonCreated2.setCreated(currentTime);
        happeningPersonCreated2.setCreator(th.personFriedaFischer);
        happeningPersonCreated2.setNewsSource(null);
        happeningPersonCreated2.setCreated(currentTime - TimeUnit.DAYS.toMillis(10));
        th.postRepository.save(happeningPersonCreated2);

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(STATISTICS_ID_POST_COUNT_HAPPENING)
                .statisticsId(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED)
                .startTimeNew(currentTime - TimeUnit.DAYS.toMillis(7))
                .endTime(currentTime)
                .allGeoAreas(true)
                .build();
        StatisticsReport statisticsReport = statisticsService.generateStatisticsReport(statisticsReportDefinition);
        assertThat(statisticsReport.getTitle()).isEqualTo("Current statistics for test at 21.03.2023 12:01:34");
        assertThat(statisticsReport.getDefinition()).isEqualTo(statisticsReportDefinition);
        assertThat(statisticsReport.getCreationTime()).isEqualTo(currentTime);
        assertThat(statisticsReport.getGeoAreaStatistics()).hasSize(8);

        GeoAreaStatistics statisticsDeutschland = findStatisticsFor(statisticsReport, th.geoAreaDeutschland);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING, TOTAL).getValue())
                .isEqualTo(5L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING, NEW).getValue())
                .isEqualTo(4L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED, TOTAL)
                .getValue())
                .isEqualTo(2L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED, NEW)
                .getValue())
                .isEqualTo(1L);
    }

    @Test
    public void generateStatisticsReport_Likes() {
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2023-03-21T12:01:34+01:00[Europe/Berlin]"));
        final long currentTime = timeServiceMock.currentTimeMillisUTC();
        final Set<GeoArea> geoAreas = Set.of(th.geoAreaTenant1, th.geoAreaTenant2);
        th.gossip1withImages.setGeoAreas(geoAreas);
        postRepository.save(th.gossip1withImages);

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(STATISTICS_ID_LIKE_COUNT_POST)
                .statisticsId(STATISTICS_ID_LIKE_COUNT_COMMENT)
                .startTimeNew(currentTime - TimeUnit.DAYS.toMillis(7))
                .endTime(currentTime)
                .allGeoAreas(true)
                .build();
        StatisticsReport statisticsReport = statisticsService.generateStatisticsReport(statisticsReportDefinition);
        assertThat(statisticsReport.getTitle()).isEqualTo("Current statistics for test at 21.03.2023 12:01:34");
        assertThat(statisticsReport.getDefinition()).isEqualTo(statisticsReportDefinition);
        assertThat(statisticsReport.getCreationTime()).isEqualTo(currentTime);
        assertThat(statisticsReport.getGeoAreaStatistics()).hasSize(8);

        GeoAreaStatistics statisticsGeoAreaTenant1 = findStatisticsFor(statisticsReport, th.geoAreaTenant1);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_POST, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_POST, NEW).getValue())
                .isEqualTo(3L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_COMMENT, TOTAL)
                .getValue())
                .isEqualTo(4L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_COMMENT, NEW)
                .getValue())
                .isEqualTo(4L);

        GeoAreaStatistics statisticsGeoAreaTenant2 = findStatisticsFor(statisticsReport, th.geoAreaTenant2);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_POST, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_POST, NEW).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_COMMENT, TOTAL).getValue())
                .isEqualTo(0L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_LIKE_COUNT_COMMENT, NEW).getValue())
                .isEqualTo(0L);
    }

    @Test
    public void generateStatisticsReport_MultiGeoAreaPosts() {
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2023-03-21T12:01:34+01:00[Europe/Berlin]"));
        final long currentTime = timeServiceMock.currentTimeMillisUTC();
        final Set<GeoArea> geoAreas = Set.of(th.geoAreaTenant1, th.geoAreaTenant2);
        final Group group = groupRepository.save(Group.builder()
                .name("Group1")
                .shortName("G1")
                .tenant(th.tenant1)
                .creator(th.personLaraSchaefer)
                .visibility(GroupVisibility.ANYONE)
                .accessibility(GroupAccessibility.PUBLIC)
                .contentVisibility(GroupContentVisibility.ANYONE)
                .deleted(false)
                .build());
        th.gossip1withImages.setGeoAreas(geoAreas);
        th.gossip1withImages.setGroup(group);
        postRepository.save(th.gossip1withImages);
        th.suggestionIdea1.setGeoAreas(geoAreas);
        th.suggestionIdea1.setGroup(group);
        th.postRepository.save(th.suggestionIdea1);
        th.offer1.setGeoAreas(geoAreas);
        th.postRepository.save(th.offer1);
        th.seeking1.setGeoAreas(geoAreas);
        th.postRepository.save(th.seeking1);
        th.happening1.setGeoAreas(geoAreas);
        th.happening1.setNewsSource(null);
        postRepository.save(th.happening1);
        th.newsItem1.setGeoAreas(geoAreas);
        postRepository.save(th.newsItem1);
        th.specialPost1withImages.setGeoAreas(geoAreas);
        postRepository.save(th.specialPost1withImages);

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(STATISTICS_ID_POST_COUNT_GOSSIP)
                .statisticsId(STATISTICS_ID_POST_COUNT_SUGGESTION)
                .statisticsId(STATISTICS_ID_POST_COUNT_OFFER)
                .statisticsId(STATISTICS_ID_POST_COUNT_SEEKING)
                .statisticsId(STATISTICS_ID_POST_COUNT_HAPPENING)
                .statisticsId(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED)
                .statisticsId(STATISTICS_ID_POST_COUNT_NEWSITEM)
                .statisticsId(STATISTICS_ID_POST_COUNT_SPECIALPOST)
                .statisticsId(STATISTICS_ID_POST_COUNT_IN_GROUP)
                .statisticsId(STATISTICS_ID_POST_COUNT_WITH_IMAGE)
                .statisticsId(STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT)
                .startTimeNew(currentTime - TimeUnit.DAYS.toMillis(7))
                .endTime(currentTime)
                .allGeoAreas(true)
                .build();
        StatisticsReport statisticsReport = statisticsService.generateStatisticsReport(statisticsReportDefinition);
        assertThat(statisticsReport.getTitle()).isEqualTo("Current statistics for test at 21.03.2023 12:01:34");
        assertThat(statisticsReport.getDefinition()).isEqualTo(statisticsReportDefinition);
        assertThat(statisticsReport.getCreationTime()).isEqualTo(currentTime);
        assertThat(statisticsReport.getGeoAreaStatistics()).hasSize(8);

        GeoAreaStatistics statisticsGeoAreaTenant1 = findStatisticsFor(statisticsReport, th.geoAreaTenant1);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_GOSSIP, TOTAL).getValue())
                .isEqualTo(5L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_SUGGESTION, TOTAL).getValue())
                .isEqualTo(8L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_OFFER, TOTAL).getValue())
                .isEqualTo(5L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_SEEKING, TOTAL).getValue())
                .isEqualTo(5L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING, TOTAL).getValue())
                .isEqualTo(5L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED,
                TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_NEWSITEM, TOTAL).getValue())
                .isEqualTo(6L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_SPECIALPOST, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_IN_GROUP, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, TOTAL).getValue())
                .isEqualTo(29L);
        assertThat(
                statisticsGeoAreaTenant1.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT, TOTAL).getValue())
                .isEqualTo(6L);

        GeoAreaStatistics statisticsGeoAreaTenant2 = findStatisticsFor(statisticsReport, th.geoAreaTenant2);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_GOSSIP, TOTAL).getValue())
                .isEqualTo(8L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_SUGGESTION, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_OFFER, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_SEEKING, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED,
                TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_NEWSITEM, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_SPECIALPOST, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_IN_GROUP, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_IMAGE, TOTAL).getValue())
                .isEqualTo(12L);
        assertThat(
                statisticsGeoAreaTenant2.getStatisticsValue(STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT, TOTAL).getValue())
                .isEqualTo(5L);
    }

    private GeoAreaStatistics findStatisticsFor(StatisticsReport statisticsReport, GeoArea geoArea) {
        return statisticsReport.getGeoAreaStatistics().stream()
                .filter(geoAreaStatistics -> geoArea.equals(geoAreaStatistics.getGeoArea()))
                .findFirst()
                .get();
    }

}
