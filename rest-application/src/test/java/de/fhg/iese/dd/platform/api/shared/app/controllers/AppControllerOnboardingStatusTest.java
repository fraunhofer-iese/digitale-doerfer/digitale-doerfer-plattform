/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientAppVariantVersionCheckResult;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientGeoAreaSelectionStatus;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientInitialOnboardingStatus;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientOnboardingStatus;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeature;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalText;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextAcceptance;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextStatus;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

public class AppControllerOnboardingStatusTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private final ClientFeature expectedFeature = ClientFeature.builder()
            .id(UUID.nameUUIDFromBytes("de.fhg.iese.dd.platform.test.TestFeature1".getBytes(StandardCharsets.US_ASCII))
                    .toString())
            .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
            .enabled(true)
            .featureValues("{" +
                    "   \"sampleStringValue\" : \"BeispielSchnurWert\"," +
                    "   \"sampleIntValue\" : 47112," +
                    "   \"sampleBooleanValue\" : false," +
                    "   \"sampleStringValues\" : [\"commonList1\",\"commonList2\"]," +
                    "   \"sampleEnum\" : \"POST\"" +
                    " }")
            .build();
    private AppVariantVersion app1Variant1Version2Android;
    private AppVariantVersion app1Variant1Version2Ios;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createLegalEntities();
        featureConfigRepository.save(FeatureConfig.builder()
                .app(th.app1)
                .enabled(true)
                .featureClass(TestFeature1.class.getName())
                .configValues(expectedFeature.getFeatureValues())
                .build());
        app1Variant1Version2Android = th.appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(th.app1Variant1)
                .versionIdentifier("1.2")
                .platform(AppPlatform.ANDROID)
                .supported(true)
                .build());
        app1Variant1Version2Ios = th.appVariantVersionRepository.save(AppVariantVersion.builder()
                .appVariant(th.app1Variant1)
                .versionIdentifier("1.2")
                .platform(AppPlatform.IOS)
                .supported(true)
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getOnboardingStatus() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final GeoArea selectedGeoArea2 = th.geoAreaDorf2InEisenberg;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;
        final Person personWithHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        th.selectGeoAreasForAppVariant(appVariant, personWithHomeArea, homeArea, selectedGeoArea2);

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithHomeArea.getId())
                .ownPerson(toClientPersonOwn(personWithHomeArea))
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(true)
                        .build())
                .features(Collections.singletonList(expectedFeature))
                .legalTextStatus(ClientLegalTextStatus.builder()
                        .id(null) //ignored in test
                        .allRequiredAccepted(false)
                        .acceptedLegalTexts(Collections.emptyList())
                        .openLegalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build())
                .homeGeoArea(th.toClientGeoArea(homeArea, homeArea.getDepth(), homeArea.isLeaf()))
                .homeGeoAreaSelectionStatus(ClientGeoAreaSelectionStatus.VALID)
                .selectedGeoAreas(Arrays.asList(th.toClientGeoArea(homeArea, homeArea.getDepth(), homeArea.isLeaf()),
                        th.toClientGeoArea(selectedGeoArea2, selectedGeoArea2.getDepth(), selectedGeoArea2.isLeaf())))
                .selectedGeoAreaIdsInvalid(Collections.emptyMap())
                .availableLeafGeoAreasCount(3)
                .onboardingRequired(false)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));

        expectedClientOnboardingStatus.setVersionCheckResult(null);
        mockMvc.perform(get("/app/onboardingStatus")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.versionCheckResult").doesNotExist())
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_VersionCheckUnsupported_Android() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final GeoArea selectedGeoArea2 = th.geoAreaDorf2InEisenberg;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersionAndroid = th.app1Variant1Version1Android;
        final Person personWithHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        th.selectGeoAreasForAppVariant(appVariant, personWithHomeArea, homeArea, selectedGeoArea2);

        assertThat(checkedVersionAndroid.isSupported()).isFalse();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithHomeArea.getId())
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(checkedVersionAndroid.isSupported())
                        .url(checkedVersionAndroid.getAppVariant().getUpdateUrlAndroid())
                        .text("Es ist eine neue Version der App verfügbar! Die aktuelle Version der App kann leider nicht mehr verwendet werden.")
                        .build())
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersionAndroid.getVersionIdentifier())
                        .param("platform", checkedVersionAndroid.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_VersionCheckUnsupported_Ios() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final GeoArea selectedGeoArea2 = th.geoAreaDorf2InEisenberg;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersionIos = th.app1Variant1Version1Ios;
        final Person personWithHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        th.selectGeoAreasForAppVariant(appVariant, personWithHomeArea, homeArea, selectedGeoArea2);

        assertThat(checkedVersionIos.isSupported()).isFalse();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithHomeArea.getId())
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(checkedVersionIos.isSupported())
                        .url(checkedVersionIos.getAppVariant().getUpdateUrlIOS())
                        .text("Es ist eine neue Version der App verfügbar! Die aktuelle Version der App kann leider nicht mehr verwendet werden.")
                        .build())
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersionIos.getVersionIdentifier())
                        .param("platform", checkedVersionIos.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Egg Phöne")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_HomeAreaNoLeaf() throws Exception {

        final GeoArea homeArea = th.geoAreaEisenberg;
        final GeoArea selectedGeoArea2 = th.geoAreaDorf2InEisenberg;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Ios;
        final Person personWithNonLeafHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        th.selectGeoAreasForAppVariant(appVariant, personWithNonLeafHomeArea, homeArea, selectedGeoArea2);

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithNonLeafHomeArea.getId())
                .ownPerson(toClientPersonOwn(personWithNonLeafHomeArea))
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(true)
                        .build())
                .features(Collections.singletonList(expectedFeature))
                .legalTextStatus(ClientLegalTextStatus.builder()
                        .id(null) //ignored in test
                        .allRequiredAccepted(false)
                        .acceptedLegalTexts(Collections.emptyList())
                        .openLegalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build())
                .homeGeoArea(th.toClientGeoArea(homeArea, homeArea.getDepth(), homeArea.isLeaf()))
                .homeGeoAreaSelectionStatus(ClientGeoAreaSelectionStatus.INVALID_NOT_LEAF)
                .selectedGeoAreas(Collections.singletonList(
                        th.toClientGeoArea(homeArea, homeArea.getDepth(), homeArea.isLeaf(),
                                th.toClientGeoArea(selectedGeoArea2, selectedGeoArea2.getDepth(),
                                        selectedGeoArea2.isLeaf()))))
                .selectedGeoAreaIdsInvalid(Collections.emptyMap())
                .availableLeafGeoAreasCount(3)
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "10.10")
                        .param("device", "Hello Kitty Phone 1.0")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithNonLeafHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));

        expectedClientOnboardingStatus.setVersionCheckResult(null);
        mockMvc.perform(get("/app/onboardingStatus")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithNonLeafHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.versionCheckResult").doesNotExist())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_NoHomeArea() throws Exception {

        final Person personWithoutHomeArea = th.withoutHomeArea(th.personRegularAnna);
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithoutHomeArea.getId())
                .ownPerson(toClientPersonOwn(personWithoutHomeArea))
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(true)
                        .build())
                .features(Collections.singletonList(expectedFeature))
                .legalTextStatus(ClientLegalTextStatus.builder()
                        .id(null) //ignored in test
                        .allRequiredAccepted(false)
                        .acceptedLegalTexts(Collections.emptyList())
                        .openLegalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build())
                .homeGeoArea(null)
                .homeGeoAreaSelectionStatus(ClientGeoAreaSelectionStatus.INVALID_NO_SELECTION)
                .selectedGeoAreas(Collections.emptyList())
                .selectedGeoAreaIdsInvalid(Collections.emptyMap())
                .availableLeafGeoAreasCount(3)
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "Krümel 1.0")
                        .param("device", "Zwieback-Telefon")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithoutHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));

        expectedClientOnboardingStatus.setVersionCheckResult(null);
        mockMvc.perform(get("/app/onboardingStatus")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithoutHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.versionCheckResult").doesNotExist())
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_HomeAreaNotInAvailableGeoAreas() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant3;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;
        final Person personWithHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        th.selectGeoAreasForAppVariant(appVariant, personWithHomeArea, homeArea);

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithHomeArea.getId())
                .ownPerson(toClientPersonOwn(personWithHomeArea))
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(true)
                        .build())
                .features(Collections.singletonList(expectedFeature))
                .legalTextStatus(ClientLegalTextStatus.builder()
                        .id(null) //ignored in test
                        .allRequiredAccepted(false)
                        .acceptedLegalTexts(Collections.emptyList())
                        .openLegalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build())
                .homeGeoArea(th.toClientGeoArea(homeArea, 0, true))
                .homeGeoAreaSelectionStatus(ClientGeoAreaSelectionStatus.INVALID_NOT_AVAILABLE)
                .selectedGeoAreas(Collections.singletonList(th.toClientGeoArea(homeArea, 0, true)))
                .selectedGeoAreaIdsInvalid(Collections.singletonMap(homeArea.getId(),
                        ClientGeoAreaSelectionStatus.INVALID_NOT_AVAILABLE))
                .availableLeafGeoAreasCount(3)
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "Quack 42")
                        .param("device", "FroschOphone")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));

        expectedClientOnboardingStatus.setVersionCheckResult(null);
        mockMvc.perform(get("/app/onboardingStatus")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.versionCheckResult").doesNotExist())
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_InvalidSelectedGeoAreas() throws Exception {

        final GeoArea homeArea = th.geoAreaDorf1InEisenberg;
        final Person personWithHomeArea = th.withHomeArea(th.personRegularAnna, homeArea);
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;
        th.selectGeoAreasForAppVariant(appVariant, personWithHomeArea, homeArea, th.geoAreaKaiserslautern,
                th.geoAreaTenant3);

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientOnboardingStatus expectedClientOnboardingStatus = ClientOnboardingStatus.builder()
                .id(personWithHomeArea.getId())
                .ownPerson(toClientPersonOwn(personWithHomeArea))
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(true)
                        .build())
                .features(Collections.singletonList(expectedFeature))
                .legalTextStatus(ClientLegalTextStatus.builder()
                        .id(null) //ignored in test
                        .allRequiredAccepted(false)
                        .acceptedLegalTexts(Collections.emptyList())
                        .openLegalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build())
                .homeGeoArea(th.toClientGeoArea(homeArea, 3, true))
                .homeGeoAreaSelectionStatus(ClientGeoAreaSelectionStatus.VALID)
                .selectedGeoAreas(Arrays.asList(th.toClientGeoArea(homeArea, 3, true),
                        th.toClientGeoArea(th.geoAreaTenant3, 0, true),
                        th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true)))
                .selectedGeoAreaIdsInvalid(Collections.singletonMap(th.geoAreaTenant3.getId(),
                        ClientGeoAreaSelectionStatus.INVALID_NOT_AVAILABLE))
                .availableLeafGeoAreasCount(3)
                .onboardingRequired(true)
                .build();

        mockMvc.perform(get("/app/onboardingStatus")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "Krümel 2.0")
                        .param("device", "Knäckebrot-Phone")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk()).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));

        expectedClientOnboardingStatus.setVersionCheckResult(null);
        mockMvc.perform(get("/app/onboardingStatus")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .person(personWithHomeArea)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.versionCheckResult").doesNotExist())
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatus_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/app/onboardingStatus"), th.app1Variant1, th.personRegularAnna);
    }

    @Test
    public void getOnboardingStatusInitial() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;

        assertThat(checkedVersion.isSupported()).isTrue();

        final ClientInitialOnboardingStatus expectedClientInitialOnboardingStatus =
                ClientInitialOnboardingStatus.builder()
                        .id(appVariant.getId())
                        .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                                .supported(true)
                                .build())
                        .features(Collections.singletonList(expectedFeature))
                        .legalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build();

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .param("homeAreaId", th.geoAreaEisenberg.getId())
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientInitialOnboardingStatus));
    }

    @Test
    public void getOnboardingStatusInitial_GeoArea() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;
        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .appVariant(appVariant)
                .geoAreasIncluded(Collections.singleton(geoArea))
                .configValues("{\n" +
                        "   \"sampleStringValue\" : \"geoarea1\"" +
                        " }")
                .build());

        final ClientInitialOnboardingStatus expectedClientInitialOnboardingStatus =
                ClientInitialOnboardingStatus.builder()
                        .id(appVariant.getId())
                        .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                                .supported(true)
                                .build())
                        .features(Collections.singletonList(ClientFeature.builder()
                                .id(UUID.nameUUIDFromBytes(
                                                "de.fhg.iese.dd.platform.test.TestFeature1".getBytes(StandardCharsets.US_ASCII))
                                        .toString())
                                .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                                .enabled(true)
                                //only one feature, because the same feature was configured twice
                                // - with the app
                                // - with the app variant and geo area
                                // -> Expected result: the config for the geo area wins and overwrites the others
                                .featureValues("{\n" +
                                        "   \"sampleStringValue\" : \"geoarea1\"," + //overwritten from geo area config
                                        "   \"sampleIntValue\" : 47112,\n" +
                                        "   \"sampleBooleanValue\" : false,\n" +
                                        "   \"sampleStringValues\" : [\"commonList1\",\"commonList2\"],\n" +
                                        "   \"sampleEnum\" : \"POST\"\n" +
                                        " }")
                                .build()))
                        .legalTexts(Arrays.asList(
                                toClientLegalText(th.legalTextPlatformRequired),
                                toClientLegalText(th.legalTextPlatformOptional),
                                toClientLegalText(th.legalText1App1Variant1Required),
                                toClientLegalText(th.legalText2App1Variant1Optional)))
                        .build();

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .param("homeAreaId", th.geoAreaEisenberg.getId())
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEqualsIgnoreAdditionalValues(expectedClientInitialOnboardingStatus));
    }

    @Test
    public void getOnboardingStatusInitial_VersionCheckUnsupported_Android() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersionAndroid = th.app1Variant1Version1Android;

        assertThat(checkedVersionAndroid.isSupported()).isFalse();

        final ClientInitialOnboardingStatus expectedClientOnboardingStatus = ClientInitialOnboardingStatus.builder()
                .id(appVariant.getId())
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(checkedVersionAndroid.isSupported())
                        .url(checkedVersionAndroid.getAppVariant().getUpdateUrlAndroid())
                        .text("Es ist eine neue Version der App verfügbar! Die aktuelle Version der App kann leider nicht mehr verwendet werden.")
                        .build())
                .build();

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .param("appVariantVersionIdentifier", checkedVersionAndroid.getVersionIdentifier())
                        .param("platform", checkedVersionAndroid.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatusInitial_VersionCheckUnsupported_Ios() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersionIos = th.app1Variant1Version1Ios;

        assertThat(checkedVersionIos.isSupported()).isFalse();

        final ClientInitialOnboardingStatus expectedClientOnboardingStatus = ClientInitialOnboardingStatus.builder()
                .id(appVariant.getId())
                .versionCheckResult(ClientAppVariantVersionCheckResult.builder()
                        .supported(checkedVersionIos.isSupported())
                        .url(checkedVersionIos.getAppVariant().getUpdateUrlIOS())
                        .text("Es ist eine neue Version der App verfügbar! Die aktuelle Version der App kann leider nicht mehr verwendet werden.")
                        .build())
                .build();

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .param("appVariantVersionIdentifier", checkedVersionIos.getVersionIdentifier())
                        .param("platform", checkedVersionIos.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Egg Phöne")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedClientOnboardingStatus));
    }

    @Test
    public void getOnboardingStatusInitial_InvalidParameters() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final AppVariantVersion checkedVersion = app1Variant1Version2Android;

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G")
                        .param("homeAreaId", "4711")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        mockMvc.perform(get("/app/onboardingStatus/initial")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .appVariant(appVariant)
                                .build())))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        assertAppVariantRequired(get("/app/onboardingStatus/initial")
                .param("appVariantVersionIdentifier", app1Variant1Version2Android.getVersionIdentifier())
                .param("platform", app1Variant1Version2Android.getPlatform().name())
                .param("osversion", "Operationssystem")
                .param("device", "Skalpell"));
    }

    private ClientPersonOwn toClientPersonOwn(Person person) {
        return ClientPersonOwn.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .homeAreaId(BaseEntity.getIdOf(person.getHomeArea()))
                .email(person.getEmail())
                .build();
    }

    private ClientLegalText toClientLegalText(LegalText legalText) {
        return toClientLegalText(legalText, null);
    }

    private ClientLegalText toClientLegalText(LegalText legalText, LegalTextAcceptance legalTextAcceptance) {
        return ClientLegalText.builder()
                .id(legalText.getId())
                .legalTextIdentifier(legalText.getLegalTextIdentifier())
                .name(legalText.getName())
                .legalTextType(legalText.getLegalTextType())
                .orderValue(legalText.getOrderValue())
                .required(legalText.isRequired())
                .urlText(legalText.getUrlText())
                .acceptance(toClientLegalTextAcceptance(legalTextAcceptance))
                .build();
    }

    private ClientLegalTextAcceptance toClientLegalTextAcceptance(LegalTextAcceptance legalTextAcceptance) {
        if (legalTextAcceptance == null) {
            return null;
        } else {
            return ClientLegalTextAcceptance.builder()
                    .id(legalTextAcceptance.getId())
                    .timestamp(legalTextAcceptance.getTimestamp())
                    .build();
        }
    }

}
