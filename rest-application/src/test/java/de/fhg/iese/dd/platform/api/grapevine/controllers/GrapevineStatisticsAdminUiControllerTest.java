/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GrapevineStatisticsAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    private Person personGlobalUserAdmin;
    private Person personUserAdminTenant1;
    private Person personUserAdminTenant2;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();

        personGlobalUserAdmin = th.createPersonWithDefaultAddress("personGlobalUserAdmin", th.tenant1,
                "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalUserAdmin, GlobalUserAdmin.class, null);

        personUserAdminTenant1 = th.createPersonWithDefaultAddress("personUserAdminTenant1", th.tenant1,
                "9b2f9a1b-39e6-4536-b0ca-8775909f0c6b");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());

        personUserAdminTenant2 = th.createPersonWithDefaultAddress("personUserAdminTenant2", th.tenant2,
                "0e562bec-0ce3-4477-b363-c3be0037d18f");
        th.assignRoleToPerson(personUserAdminTenant2, UserAdmin.class, th.tenant2.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getGrapevineStatsForGeoArea() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;
        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        final long expectedPersonCount = personRepository.findAll().stream()
                .filter(p -> p.getHomeArea() != null && p.getHomeArea().equals(geoArea) && !p.isDeleted())
                .count();
        final long expectedPostCount = postRepository.findAll().stream()
                .filter(p -> p.getGeoAreas().contains(geoArea) && !p.isDeleted())
                .count();
        final long expectedCommentCount = commentRepository.findAll().stream()
                .filter(c -> c.getPost().getGeoAreas().contains(geoArea) && !c.isDeleted())
                .count();

        for (final Person caller : personsWithSuitableRoles) {

            mockMvc.perform(get("/grapevine/adminui/statistics/geoArea/{geoAreaId}", geoArea.getId())
                    .headers(authHeadersFor(caller)))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.geoAreaId").value(geoArea.getId()))
                    .andExpect(jsonPath("$.personCount").value(expectedPersonCount))
                    .andExpect(jsonPath("$.postCount").value(expectedPostCount))
                    .andExpect(jsonPath("$.commentCount").value(expectedCommentCount));
        }
    }

    @Test
    public void getGrapevineStatsForGeoArea_GeoAreaNotFound() throws Exception {

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {

            mockMvc.perform(get("/grapevine/adminui/statistics/geoArea/{geoAreaId}", BaseTestHelper.INVALID_UUID)
                    .headers(authHeadersFor(caller)))
                    .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        }
    }

    @Test
    public void getGrapevineStatsForGeoArea_NotAuthorized() throws Exception {

        final String geoAreaId = th.geoAreaKaiserslautern.getId();

        // caller does not have required role
        mockMvc.perform(get("/grapevine/adminui/statistics/geoArea/{geoAreaId}", geoAreaId)
                .headers(authHeadersFor(th.personRegularAnna)))
                .andExpect(isNotAuthorized());

        // caller has required role but the tenantId belongs to different tenant
        mockMvc.perform(get("/grapevine/adminui/statistics/geoArea/{geoAreaId}", geoAreaId)
                .headers(authHeadersFor(personUserAdminTenant2)))
                .andExpect(isNotAuthorized());
    }

}
