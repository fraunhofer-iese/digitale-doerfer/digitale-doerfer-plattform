/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
public class GrapevineSearchCleanupWorkerTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;
    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private GrapevineSearchCleanupWorker grapevineSearchCleanupWorker;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws InterruptedException {

        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    @Test
    public void cleanupSearchItems() throws Exception {

        final long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);
        Post postToKeep1 = th.gossip2WithComments;
        Post postToKeep2 = th.happening2;
        Post postToKeep3 = th.gossip1withImages;
        Post postToDelete1 = th.happening1;
        Post postToDelete2 = th.gossip3WithComment;

        final long lastTimeToKeep = (now - grapevineConfig.getSearchablePostTime().toMillis()) + 1;
        final long firstTimeToDelete = (now - grapevineConfig.getSearchablePostTime().toMillis()) - 1;

        postToKeep1.setLastActivity(lastTimeToKeep);
        postToKeep1.setLastModified(firstTimeToDelete);

        postToKeep2.setLastActivity(firstTimeToDelete);
        postToKeep2.setLastModified(lastTimeToKeep);

        postToKeep3.setLastActivity(lastTimeToKeep);
        postToKeep3.setLastModified(lastTimeToKeep);

        postToDelete1.setLastActivity(firstTimeToDelete);
        postToDelete1.setLastModified(firstTimeToDelete);

        postToDelete2.setLastActivity(firstTimeToDelete);
        postToDelete2.setLastModified(firstTimeToDelete);

        th.postRepository.saveAll(Arrays.asList(postToKeep1, postToKeep2, postToKeep3,
                postToDelete1, postToDelete2));

        grapevineSearchService.indexOrUpdateIndexedPost(postToKeep1);
        grapevineSearchService.indexOrUpdateIndexedPost(postToKeep2);
        grapevineSearchService.indexOrUpdateIndexedPost(postToKeep3);
        grapevineSearchService.indexOrUpdateIndexedPost(postToDelete1);
        grapevineSearchService.indexOrUpdateIndexedPost(postToDelete2);

        grapevineSearchCleanupWorker.run();

        waitForEventProcessing();

        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToKeep1.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToKeep2.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToKeep3.getId(), SearchPost.class))
                .isNotNull();

        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToDelete1.getId(), SearchPost.class))
                .isNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToDelete2.getId(), SearchPost.class))
                .isNull();
    }

}
