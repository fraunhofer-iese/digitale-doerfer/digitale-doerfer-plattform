/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.worker;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.security.worker.ApiKeyExpirationWarningWorker;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.test.mocks.TestDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.SecurityConfig;

public class ApiKeyExpirationWarningWorkerTest extends BaseServiceTest {

    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private TestTimeService testTimeService;
    @Autowired
    private TestDefaultTeamNotificationPublisher testDefaultTeamNotificationPublisher;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private ApiKeyExpirationWarningWorker apiKeyExpirationWarningWorker;

    private AppVariant appVariantTwoExpired;
    private AppVariant appVariantNoApiKeys;
    private AppVariant appVariantNoExpired;
    private AppVariant appVariantOneExpired;
    private AppVariant appVariantOneReminder;

    @Override
    public void createEntities() throws Exception {

        testDefaultTeamNotificationPublisher.reset();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        testDefaultTeamNotificationPublisher.reset();
    }

    private void createAppVariants() {
        // appVariant with expired api keys
        appVariantTwoExpired = appVariantRepository.save(AppVariant.builder()
                .apiKey1("random-key-3")
                .apiKey1Created(
                        testTimeService.currentTimeMillisUTC() - securityConfig.getApiKeyWarningTime().toMillis())
                .apiKey2("random-key-4")
                .apiKey2Created(
                        testTimeService.currentTimeMillisUTC() - securityConfig.getApiKeyWarningTime().toMillis())
                .appVariantIdentifier("two-keys-expired")
                .build());

        // appVariant without api keys
        appVariantNoApiKeys = appVariantRepository.save(AppVariant.builder()
                .appVariantIdentifier("no-keys")
                .build());

        // appVariant with non-expired api keys
        appVariantNoExpired = appVariantRepository.save(AppVariant.builder()
                .apiKey1("random-key-3")
                .apiKey1Created(testTimeService.currentTimeMillisUTC())
                .apiKey2("random-key-4")
                .apiKey2Created(testTimeService.currentTimeMillisUTC())
                .appVariantIdentifier("valid-keys")
                .build());

        // appVariant with one expired api keys
        appVariantOneExpired = appVariantRepository.save(AppVariant.builder()
                .apiKey1("random-key-3")
                .apiKey1Created(
                        testTimeService.currentTimeMillisUTC() - securityConfig.getApiKeyWarningTime().toMillis())
                .appVariantIdentifier("one-key-expired")
                .build());

        // appVariant with one expired api keys
        appVariantOneReminder = appVariantRepository.save(AppVariant.builder()
                .apiKey1("random-key-3")
                .apiKey1Created(
                        testTimeService.currentTimeMillisUTC() - securityConfig.getApiKeyReminderTime().toMillis())
                .appVariantIdentifier("one-key-reminder")
                .build());
        testDefaultTeamNotificationPublisher.reset();
    }

    @Test
    public void apiKeyExpiringWarningControllerTest() {

        createAppVariants();

        apiKeyExpirationWarningWorker.run();

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(1);
        final Triple<String, Tenant, TeamNotificationMessage> lastMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0);
        assertThat(lastMessage.getRight().getText()).contains(appVariantTwoExpired.getAppVariantIdentifier());
        assertThat(lastMessage.getRight().getText()).doesNotContain(appVariantNoApiKeys.getAppVariantIdentifier());
        assertThat(lastMessage.getRight().getText()).doesNotContain(appVariantNoExpired.getAppVariantIdentifier());
        assertThat(lastMessage.getRight().getText()).contains(appVariantOneExpired.getAppVariantIdentifier());
        assertThat(lastMessage.getRight().getText()).contains(appVariantOneReminder.getAppVariantIdentifier());
    }

    @Test
    public void apiKeyExpiringWarningControllerTest_NoWarnings() {

        appVariantRepository.save(AppVariant.builder()
                .apiKey1("random-key-3")
                .apiKey1Created(testTimeService.currentTimeMillisUTC())
                .apiKey2("random-key-4")
                .apiKey2Created(testTimeService.currentTimeMillisUTC())
                .appVariantIdentifier("valid-keys")
                .build());

        apiKeyExpirationWarningWorker.run();

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(0);
    }

}
