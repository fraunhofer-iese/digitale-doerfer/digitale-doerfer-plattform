/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import de.fhg.iese.dd.platform.business.shared.address.services.AddressServiceTest;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import lombok.extern.log4j.Log4j2;

/**
 * "Hack" to make this service publicly available for manual tests. See {@link AddressServiceTest} for an example how
 * this is used. Never make this a spring service/component.
 */
@Log4j2
public class TestGoogleGeoService extends GoogleGeoService {

    final private ApplicationConfig config;

    /**
     * initializes this service for local manual testing without having to load the whole application context
     *
     * @return
     * @throws IOException
     */
    public static TestGoogleGeoService initializeWithoutApplicationContext() throws IOException {

        String apiKey = null;
        try (InputStream inputStream = TestGoogleGeoService.class.getResourceAsStream("/application.yml")) {
            if (inputStream == null) {
                throw new IllegalStateException("Could not access test application.yml");
            }
            final List<String> lines = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.toList());
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).contains("google:")) {
                    apiKey = lines.get(i+1).replace("api-key:", "").trim();
                    break;
                }
            }
        }
        if (StringUtils.isEmpty(apiKey)) {
            throw new IllegalStateException("Could not read Google api key from test application.yml");
        }
        log.info("Using Google API Key from application.yml: {}", apiKey);

        final ApplicationConfig.Google googleConfig = new ApplicationConfig.Google();
        googleConfig.setApiKey(apiKey);
        final ApplicationConfig config = new ApplicationConfig();
        config.setGoogle(googleConfig);
        return new TestGoogleGeoService(config);
    }

    public TestGoogleGeoService(ApplicationConfig config) {
        this.config = config;
        this.spatialService = new SpatialService();
        super.initialize();
    }

    @Override
    protected ApplicationConfig getConfig() {
        return config;
    }

    @Override
    public Address resolveLocation(String locationName, String locationLookupString) {
        final Address address = super.resolveLocation(locationName, locationLookupString);
        log.info("resolveLocation({}, {}) returned {}", locationName, locationLookupString, address);
        return address;
    }

    @Override
    public Address resolveLocation(String locationName, GPSLocation gpsLocation) {
        final Address address = super.resolveLocation(locationName, gpsLocation);
        log.info("resolveLocation({}, {}) returned {}", locationName, gpsLocation, address);
        return address;
    }

}
