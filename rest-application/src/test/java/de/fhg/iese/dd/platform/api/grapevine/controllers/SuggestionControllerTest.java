/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Tahmid Ekram, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionCategory;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionExtended;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SuggestionControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenarioWithoutInternalSuggestions();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getSuggestionCategoriesInCorrectOrder() throws Exception {
        final List<ClientSuggestionCategory> suggestionCategories =
                toListOfType(mockMvc.perform(get("/grapevine/suggestion/suggestionCategory"))
                                .andReturn()
                                .getResponse(),
                        ClientSuggestionCategory.class);

        assertThat(suggestionCategories).hasSize(th.suggestionCategoriesInOrder.size());

        for (int i = 0; i < th.suggestionCategoriesInOrder.size(); i++) {
            assertEquals(suggestionCategories.get(i).getId(), th.suggestionCategoriesInOrder.get(i).getId());
            assertEquals(suggestionCategories.get(i).getShortName(), th.suggestionCategoriesInOrder.get(i).getShortName());
            assertEquals(suggestionCategories.get(i).getDisplayName(), th.suggestionCategoriesInOrder.get(i).getDisplayName());
            assertEquals(suggestionCategories.get(i).getOrderValue(), th.suggestionCategoriesInOrder.get(i).getOrderValue());
        }
    }

    @Test
    public void getSuggestionCategoryById() throws Exception {
        mockMvc.perform(get("/grapevine/suggestion/suggestionCategory/" + th.suggestionCategoryRoadDamage.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.suggestionCategoryRoadDamage.getId()))
                .andExpect(jsonPath("$.displayName").value(th.suggestionCategoryRoadDamage.getDisplayName()))
                .andExpect(jsonPath("$.shortName").value(th.suggestionCategoryRoadDamage.getShortName()))
                .andExpect(jsonPath("$.orderValue").value(th.suggestionCategoryRoadDamage.getOrderValue()));
    }

    @Test
    public void getSuggestionCategoryByIdNotExisting() throws Exception {
        final String notExisting = "abc";

        mockMvc.perform(get("/grapevine/suggestion/suggestionCategory/" + notExisting))
                .andExpect(isException(notExisting, ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND));
    }

    @Test
    public void getPotentialWorkers_Unauthorized() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;

        assertOAuth2(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId()));
    }

    @Test
    public void getPotentialWorkers_MissingRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personNinaBauer)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getPotentialWorkers_RoleForDifferentTenant() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        assertNotEquals(suggestion.getTenant(), th.tenant2);
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getPotentialWorkers_InvalidId() throws Exception {

        String invalidId = "42";
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", invalidId)
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(invalidId,ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getPotentialWorkers_DifferentRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        assertEquals(4, th.personsWithSuggestionRoleTenant1.size());

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(th.personsWithSuggestionRoleTenant1.get(0).getId()))
                .andExpect(jsonPath("$[0].firstName").value(th.personsWithSuggestionRoleTenant1.get(0).getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(th.personsWithSuggestionRoleTenant1.get(0).getLastName()))
                .andExpect(jsonPath("$[1].id").value(th.personsWithSuggestionRoleTenant1.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(th.personsWithSuggestionRoleTenant1.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(th.personsWithSuggestionRoleTenant1.get(3).getId()));

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(th.personsWithSuggestionRoleTenant1.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(th.personsWithSuggestionRoleTenant1.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(th.personsWithSuggestionRoleTenant1.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(th.personsWithSuggestionRoleTenant1.get(3).getId()));
    }

    @Test
    public void getPotentialWorkers_ExcludeDeletedPersons() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        assertEquals(4, th.personsWithSuggestionRoleTenant1.size());

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(th.personsWithSuggestionRoleTenant1.get(0).getId()))
                .andExpect(jsonPath("$[0].firstName").value(th.personsWithSuggestionRoleTenant1.get(0).getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(th.personsWithSuggestionRoleTenant1.get(0).getLastName()))
                .andExpect(jsonPath("$[1].id").value(th.personsWithSuggestionRoleTenant1.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(th.personsWithSuggestionRoleTenant1.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(th.personsWithSuggestionRoleTenant1.get(3).getId()));

        Person deletedPerson = th.personSuggestionWorkerAndFirstResponderTenant1Carmen;
        deletedPerson.setDeleted(true);
        th.personRepository.saveAndFlush(deletedPerson);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(th.personsWithSuggestionRoleTenant1.get(1).getId()))
                .andExpect(jsonPath("$[0].firstName").value(th.personsWithSuggestionRoleTenant1.get(1).getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(th.personsWithSuggestionRoleTenant1.get(1).getLastName()))
                .andExpect(jsonPath("$[1].id").value(th.personsWithSuggestionRoleTenant1.get(2).getId()))
                .andExpect(jsonPath("$[2].id").value(th.personsWithSuggestionRoleTenant1.get(3).getId()));
    }

    @Test
    public void getPotentialWorkers_DeletedSuggestion() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;
        suggestion.setDeleted(true);
        suggestion.setOverrideDeletedForSuggestionRoles(true);
        suggestion = th.postRepository.save(suggestion);
        assertEquals(4, th.personsWithSuggestionRoleTenant1.size());

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(th.personsWithSuggestionRoleTenant1.get(0).getId()))
                .andExpect(jsonPath("$[0].firstName").value(th.personsWithSuggestionRoleTenant1.get(0).getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(th.personsWithSuggestionRoleTenant1.get(0).getLastName()))
                .andExpect(jsonPath("$[1].id").value(th.personsWithSuggestionRoleTenant1.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(th.personsWithSuggestionRoleTenant1.get(2).getId()))
                .andExpect(jsonPath("$[3].id").value(th.personsWithSuggestionRoleTenant1.get(3).getId()));

        suggestion.setOverrideDeletedForSuggestionRoles(false);
        suggestion = th.postRepository.save(suggestion);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/potentialWorker", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(suggestion.getId(), ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getAllExtendedSuggestions_Unauthorized() throws Exception {

        assertOAuth2(get("/grapevine/suggestion/"));
    }

    @Test
    public void getAllExtendedSuggestions_MissingRoles() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/")
                .headers(authHeadersFor(th.personNinaBauer)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllExtendedSuggestions_RoleForDifferentTenant() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/")
                        .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz)))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 0));
    }

    @Test
    public void getAllExtendedSuggestions_OrderByCreated() throws Exception {

        Suggestion withdrawnSuggestion = th.suggestionDefect1;
        withdrawnSuggestion.setDeleted(true);
        withdrawnSuggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(withdrawnSuggestion);

        Suggestion deletedSuggestion = th.suggestionWish2;
        deletedSuggestion.setDeleted(true);
        deletedSuggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(deletedSuggestion);

        List<ClientSuggestionExtended> expectedSuggestions = Arrays.asList(
                th.toClientSuggestionExtended(th.suggestionDefect1),
                th.toClientSuggestionExtended(th.suggestionDefect2),
                th.toClientSuggestionExtended(th.suggestionIdea1),
                th.toClientSuggestionExtended(th.suggestionIdea2),
                th.toClientSuggestionExtended(th.suggestionIdea3),
                th.toClientSuggestionExtended(th.suggestionWish1)
                //deleted suggestion should not be returned
                //th.toClientSuggestionExtended(th.suggestionWish2)
        );

        checkGetAllExtendedSuggestionsWithFiltering("CREATED", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getCreated).reversed(), false);

        //this should hide the suggestions this person created
        Suggestion parallelWorldSuggestion = th.suggestionIdea2;
        parallelWorldSuggestion.setParallelWorld(true);
        th.postRepository.save(th.suggestionIdea2);

        List<ClientSuggestionExtended> filteredSuggestions = expectedSuggestions.stream()
                .filter(s -> !StringUtils.equals(s.getId(), parallelWorldSuggestion.getId()))
                .collect(Collectors.toList());

        assertThat(filteredSuggestions).hasSizeLessThan(expectedSuggestions.size());

        checkGetAllExtendedSuggestionsWithFiltering("CREATED", filteredSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getCreated).reversed(), false);
    }

    @Test
    public void getAllExtendedSuggestions_OrderByLastActivity() throws Exception {

        Suggestion withdrawnSuggestion = th.suggestionDefect1;
        withdrawnSuggestion.setDeleted(true);
        withdrawnSuggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(withdrawnSuggestion);

        Suggestion deletedSuggestion = th.suggestionWish2;
        deletedSuggestion.setDeleted(true);
        deletedSuggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(deletedSuggestion);

        //we change the last commented, so that the order changes
        th.suggestionIdea1.setLastActivity(th.nextTimeStamp());
        th.postRepository.save(th.suggestionIdea1);

        List<ClientSuggestionExtended> expectedSuggestions = Arrays.asList(
                th.toClientSuggestionExtended(th.suggestionDefect1),
                th.toClientSuggestionExtended(th.suggestionDefect2),
                th.toClientSuggestionExtended(th.suggestionIdea1),
                th.toClientSuggestionExtended(th.suggestionIdea2),
                th.toClientSuggestionExtended(th.suggestionIdea3),
                th.toClientSuggestionExtended(th.suggestionWish1)
                //deleted suggestion should not be returned
                //th.toClientSuggestionExtended(th.suggestionWish2)
        );

        checkGetAllExtendedSuggestionsWithFiltering("LAST_ACTIVITY", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getLastActivity).reversed(), false);
    }

    @Test
    public void getAllExtendedSuggestions_OrderByLastSuggestionActivity() throws Exception {

        Suggestion withdrawnSuggestion = th.suggestionDefect1;
        withdrawnSuggestion.setDeleted(true);
        withdrawnSuggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(withdrawnSuggestion);

        Suggestion deletedSuggestion = th.suggestionWish2;
        deletedSuggestion.setDeleted(true);
        deletedSuggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(deletedSuggestion);

        //we change the last activities, so that the order changes
        th.suggestionIdea2.setLastSuggestionActivity(th.nextTimeStamp());
        th.postRepository.save(th.suggestionIdea2);

        List<ClientSuggestionExtended> expectedSuggestions = Arrays.asList(
                th.toClientSuggestionExtended(th.suggestionDefect1),
                th.toClientSuggestionExtended(th.suggestionDefect2),
                th.toClientSuggestionExtended(th.suggestionIdea1),
                th.toClientSuggestionExtended(th.suggestionIdea2),
                th.toClientSuggestionExtended(th.suggestionIdea3),
                th.toClientSuggestionExtended(th.suggestionWish1)
                //deleted suggestion should not be returned
                //th.toClientSuggestionExtended(th.suggestionWish2)
        );

        checkGetAllExtendedSuggestionsWithFiltering("LAST_SUGGESTION_ACTIVITY", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getLastSuggestionActivity).reversed(), false);
    }

    @Test
    public void getAllExtendedSuggestions_FilterByWorkerAndOrderByCreated() throws Exception {

        Suggestion withdrawnSuggestion = th.suggestionDefect1;
        withdrawnSuggestion.setDeleted(true);
        withdrawnSuggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(withdrawnSuggestion);

        Suggestion deletedSuggestion = th.suggestionWish2;
        deletedSuggestion.setDeleted(true);
        deletedSuggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(deletedSuggestion);

        List<ClientSuggestionExtended> expectedSuggestions = Arrays.asList(
                th.toClientSuggestionExtended(th.suggestionDefect1),
                th.toClientSuggestionExtended(th.suggestionDefect2),
                th.toClientSuggestionExtended(th.suggestionIdea1),
                th.toClientSuggestionExtended(th.suggestionIdea2),
                th.toClientSuggestionExtended(th.suggestionIdea3),
                th.toClientSuggestionExtended(th.suggestionWish1)
                //deleted suggestion should not be returned
                //th.toClientSuggestionExtended(th.suggestionWish2)
        );

        checkGetAllExtendedSuggestionsWithFiltering("CREATED", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getCreated).reversed(), false);
    }

    @Test
    public void getAllExtendedSuggestions_FilterByWorkerAllSortByOptions() throws Exception{

        // Hilde is assigned to 1 open suggestion
        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionFirstResponderTenant1Hilde.getId())
                .param("sortBy", "CREATED")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.suggestionIdea1WorkerMappingHilde.getSuggestion().getId()));

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionFirstResponderTenant1Hilde.getId())
                .param("sortBy", "LAST_ACTIVITY")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.suggestionIdea1WorkerMappingHilde.getSuggestion().getId()));

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionFirstResponderTenant1Hilde.getId())
                .param("sortBy", "LAST_SUGGESTION_ACTIVITY")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.suggestionIdea1WorkerMappingHilde.getSuggestion().getId()));

        // Dieter has 1 inactivated suggestionWorkerMapping and should get an empty list
        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionWorkerTenant1Dieter.getId())
                .param("sortBy", "CREATED")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(0));

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionWorkerTenant1Dieter.getId())
                .param("sortBy", "LAST_ACTIVITY")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(0));

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSuggestionWorkerTenant1Dieter.getId())
                .param("sortBy", "LAST_SUGGESTION_ACTIVITY")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(0));
    }

    @Test
    public void getAllExtendedSuggestions_FilterByUnassignedAllSortByOptions() throws Exception{

        Suggestion withdrawnSuggestion = th.suggestionDefect1;
        withdrawnSuggestion.setDeleted(true);
        withdrawnSuggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(withdrawnSuggestion);

        Suggestion deletedSuggestion = th.suggestionWish2;
        deletedSuggestion.setDeleted(true);
        deletedSuggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(deletedSuggestion);

        //we change the last commented, so that the order changes
        th.suggestionIdea2.setLastActivity(th.nextTimeStamp());
        th.postRepository.save(th.suggestionIdea2);

        //we change the last activities, so that the order changes
        th.suggestionIdea2.setLastSuggestionActivity(th.nextTimeStamp());
        th.postRepository.save(th.suggestionIdea2);

        List<ClientSuggestionExtended> expectedSuggestions = Arrays.asList(
                th.toClientSuggestionExtended(th.suggestionDefect1),
                th.toClientSuggestionExtended(th.suggestionDefect2),
                //idea1 has existing suggestionWorkerMappings
                //th.toClientSuggestionExtended(th.suggestionIdea1),
                th.toClientSuggestionExtended(th.suggestionIdea2),
                //idea3 has existing suggestionWorkerMappings
                //th.toClientSuggestionExtended(th.suggestionIdea3),
                th.toClientSuggestionExtended(th.suggestionWish1)
                //deleted suggestion should not be returned
                //th.toClientSuggestionExtended(th.suggestionWish2)
        );

        checkGetAllExtendedSuggestionsWithFiltering("CREATED", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getCreated).reversed(), true);

        checkGetAllExtendedSuggestionsWithFiltering("LAST_ACTIVITY", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getLastSuggestionActivity).reversed(), true);

        checkGetAllExtendedSuggestionsWithFiltering("LAST_SUGGESTION_ACTIVITY", expectedSuggestions,
                Comparator.comparing(ClientSuggestionExtended::getLastSuggestionActivity).reversed(), true);
    }

    public void checkGetAllExtendedSuggestionsWithFiltering(String sortParameter, List<ClientSuggestionExtended> expectedSuggestions,
            Comparator<ClientSuggestionExtended> comparator, boolean onlyUnassignedSuggestions) throws Exception {

        List<ClientSuggestionExtended> expectedSuggestionsSorted = expectedSuggestions.stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        int maxPageSize1 = Math.min(4, expectedSuggestions.size());
        int maxPageSize2 = Math.min(3, expectedSuggestions.size());

        checkGetAllExtendedSuggestions(sortParameter, null, null, maxPageSize1, expectedSuggestionsSorted,
                th.personSuggestionFirstResponderTenant1Hilde, th.personSuggestionWorkerTenant1Horst,
                onlyUnassignedSuggestions);

        Set<SuggestionStatus> excludedStatus =
                new HashSet<>(Arrays.asList(SuggestionStatus.IN_PROGRESS, SuggestionStatus.DONE));

        List<ClientSuggestionExtended> expectedSuggestionsExcluded = expectedSuggestionsSorted.stream()
                .filter(s -> !excludedStatus.contains(s.getSuggestionStatus()))
                .collect(Collectors.toList());

        checkGetAllExtendedSuggestions(sortParameter, null, excludedStatus, maxPageSize2, expectedSuggestionsExcluded,
                th.personSuggestionFirstResponderTenant1Hilde, th.personSuggestionWorkerTenant1Dieter,
                onlyUnassignedSuggestions);

        Set<SuggestionStatus> includedStatus = new HashSet<>(Set.of(SuggestionStatus.OPEN));

        List<ClientSuggestionExtended> expectedSuggestionsIncluded = expectedSuggestionsSorted.stream()
                .filter(s -> includedStatus.contains(s.getSuggestionStatus()))
                .collect(Collectors.toList());

        checkGetAllExtendedSuggestions(sortParameter, includedStatus, null, maxPageSize2, expectedSuggestionsIncluded,
                th.personSuggestionFirstResponderTenant1Hilde, th.personSuggestionWorkerTenant1Horst,
                onlyUnassignedSuggestions);

        //Since this status is already in the excluded status it should not change the result
        includedStatus.add(SuggestionStatus.IN_PROGRESS);

        checkGetAllExtendedSuggestions(sortParameter, includedStatus, excludedStatus, maxPageSize2,
                expectedSuggestionsIncluded,
                th.personSuggestionFirstResponderTenant1Hilde, th.personSuggestionWorkerTenant1Dieter,
                onlyUnassignedSuggestions);
    }

    public void checkGetAllExtendedSuggestions(String sortParameter, Set<SuggestionStatus> includedStatus,
            Set<SuggestionStatus> excludedStatus, int pageSize, List<ClientSuggestionExtended> expectedSuggestions,
            Person caller1, Person caller2, boolean onlyUnassignedSuggestions) throws Exception {

        int totalCount = expectedSuggestions.size();

        Page<ClientSuggestionExtended> expectedPage = new PageImpl<>(expectedSuggestions,
                simplePageRequest(0, totalCount), totalCount);

        MockHttpServletRequestBuilder requestAll = get("/grapevine/suggestion/")
                .param("page", "0")
                .param("count", String.valueOf(expectedSuggestions.size()))
                .param("sortBy", sortParameter)
                .param("unassigned", String.valueOf(onlyUnassignedSuggestions));
        requestAll = addRequestParameters(requestAll, "excludedStatus", excludedStatus);
        requestAll = addRequestParameters(requestAll, "includedStatus", includedStatus);

        mockMvc.perform(requestAll
                .headers(authHeadersFor(caller1)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedPage));

        //two subpages
        Page<ClientSuggestionExtended> expectedSubPage1 = new PageImpl<>(expectedSuggestions.subList(0, pageSize),
                simplePageRequest(0, pageSize), totalCount);
        Page<ClientSuggestionExtended> expectedSubPage2 = new PageImpl<>(expectedSuggestions.subList(pageSize, totalCount),
                simplePageRequest(1, pageSize), totalCount);

        MockHttpServletRequestBuilder requestPage0 = get("/grapevine/suggestion/")
                .param("page", "0")
                .param("count", String.valueOf(pageSize))
                .param("sortBy", sortParameter)
                .param("unassigned", String.valueOf(onlyUnassignedSuggestions));
        requestPage0 = addRequestParameters(requestPage0, "excludedStatus", excludedStatus);
        requestPage0 = addRequestParameters(requestPage0, "includedStatus", includedStatus);

        mockMvc.perform(requestPage0
                .headers(authHeadersFor(caller2)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedSubPage1));

        MockHttpServletRequestBuilder requestPage1 = get("/grapevine/suggestion/")
                .param("page", "1")
                .param("count", String.valueOf(pageSize))
                .param("sortBy", sortParameter)
                .param("unassigned", String.valueOf(onlyUnassignedSuggestions));
        requestPage1 = addRequestParameters(requestPage1, "excludedStatus", excludedStatus);
        requestPage1 = addRequestParameters(requestPage1, "includedStatus", includedStatus);

        mockMvc.perform(requestPage1
                .headers(authHeadersFor(caller2)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedSubPage2));
    }

    @Test
    public void getAllExtendedSuggestions_TooRestrictiveFilter() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("excludedStatus", Stream.of(SuggestionStatus.values())
                        .map(Object::toString)
                        .toArray(String[]::new))
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(isException("excludedStatus", ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAllExtendedSuggestions_InvalidSorting() throws Exception {

        String invalidSort = "HAPPENING_START";
        mockMvc.perform(get("/grapevine/suggestion/")
                .param("page", "0")
                .param("count", "42")
                .param("sortBy", invalidSort)
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(isException(invalidSort, ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void getAllExtendedSuggestions_InvalidFiltering() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("containedWorkerId", th.personSusanneChristmann.getId())
                .param("unassigned", "true")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(isException(ClientExceptionType.INVALID_FILTER_CRITERIA));
    }

    @Test
    public void getAllExtendedSuggestions_InvalidPaging() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("page", "-1")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(isException("page", ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/grapevine/suggestion/")
                .param("count", "-1")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(isException("count", ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getOneExtendedSuggestion_Unauthorized() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;

        assertOAuth2(get("/grapevine/suggestion/{suggestionId}", suggestion.getId()));
    }

    @Test
    public void getOneExtendedSuggestion_MissingRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personNinaBauer)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getOneExtendedSuggestion_RoleForDifferentTenant() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        assertNotEquals(suggestion.getTenant(), th.tenant2);
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getOneExtendedSuggestion_InvalidId() throws Exception {

        String invalidId = "42";
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", invalidId)
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(invalidId, ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getOneExtendedSuggestion_StatusHistoryAndWorkers() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientSuggestionExtended(suggestion)));

    }

    @Test
    public void getOneExtendedSuggestion_Deleted() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;
        suggestion = setDeletedAndOverrideDeleted(suggestion);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientSuggestionExtended(suggestion)));

        suggestion.setOverrideDeletedForSuggestionRoles(false);
        suggestion = th.postRepository.save(suggestion);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(suggestion.getId(), ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getSuggestionComments() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());

        final Comment comment = addCommentTo(suggestion);
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(th.toClientCommentWithCensoredLastName(comment))));
    }

    @Test
    public void getSuggestionComments_DeletedSuggestion() throws Exception {

        Suggestion suggestion = setDeletedAndOverrideDeleted(th.suggestionDefect1);
        final Comment comment = addCommentTo(suggestion);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(th.toClientCommentWithCensoredLastName(comment))));

        suggestion.setOverrideDeletedForSuggestionRoles(false);
        suggestion = th.postRepository.save(suggestion);

        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(suggestion.getId(), ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getSuggestionComments_Unauthorized() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;

        assertOAuth2(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId()));
    }

    @Test
    public void getSuggestionComments_MissingRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personNinaBauer)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getSuggestionComments_RoleForDifferentTenant() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        assertNotEquals(suggestion.getTenant(), th.tenant2);
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getSuggestionComments_InvalidId() throws Exception {

        String invalidId = "42";
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}/comments", invalidId)
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(isException(invalidId, ClientExceptionType.POST_NOT_FOUND));
    }

    private Suggestion setDeletedAndOverrideDeleted(Suggestion suggestion) {
        suggestion.setDeleted(true);
        suggestion.setOverrideDeletedForSuggestionRoles(true);
        List<SuggestionStatusRecord> statusRecords = suggestion.getSuggestionStatusRecords();
        suggestion = th.postRepository.save(suggestion);
        suggestion.setSuggestionStatusRecords(statusRecords);
        return suggestion;
    }

    private Comment addCommentTo(Suggestion suggestion) {
        final Comment comment = Comment.builder()
                .post(suggestion)
                .creator(th.personSusanneChristmann)
                .text("some comment")
                .deleted(false)
                .build();
        th.commentRepository.save(comment);
        return comment;
    }

}
