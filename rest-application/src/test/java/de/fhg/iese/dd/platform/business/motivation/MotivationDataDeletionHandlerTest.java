/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.motivation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.motivation.MotivationTestHelper;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.datadeletion.BaseDataDeletionHandlerTest;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;

public class MotivationDataDeletionHandlerTest extends BaseDataDeletionHandlerTest {

    @Autowired
    private MotivationTestHelper th;

    @Override
    protected BaseSharedTestHelper getTestHelper() {
        return th;
    }

    @Override
    protected void createAdditionalEntities() {
        th.createScoreEntities();
        assertThat(th.accountTenant1.getOwner()).isEqualTo(tenantToBeDeleted);
    }

    @Override
    protected void assertAfterDeletion() throws Exception {
        for (PersonAccount personAccount : th.personAccountList) {
            mockMvc.perform(get("/score/account/")
                            .headers(authHeadersFor(personAccount.getOwner())))
                    .andExpect(status().isOk());
        }
    }

    @Override
    protected void assertAfterDeletionGeoArea() throws Exception {

    }

    @Override
    protected void assertAfterDeletionTenant() throws Exception {

    }

}
