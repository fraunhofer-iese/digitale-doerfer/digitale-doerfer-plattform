/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Danielle Korth, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalConfigurationAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

public class GeoAreaAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private Person personGlobalConfigurationAdmin;
    private Person personUserAdminTenant1;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();

        personGlobalConfigurationAdmin =
                th.createPersonWithDefaultAddress("GlobalConfigurationAdmin", th.tenant1,
                        "18256640-3421-422f-992c-7306235e62e");
        th.assignRoleToPerson(personGlobalConfigurationAdmin, GlobalConfigurationAdmin.class, null);

        personUserAdminTenant1 =
                th.createPersonWithDefaultAddress("UserAdminTenant1", th.tenant1,
                        "54c14492-4f9b-4e04-9e4f-7791fc700840");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllGeoAreas() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/")
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))))));
    }

    @Test
    public void getAllGeoAreas_IncludeBoundaryPoints() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .param("includeBoundaryPoints", "true")
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaMainz))),
                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/")
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .param("includeBoundaryPoints", "true")
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaKaiserslautern))))));
    }

    @Test
    public void getAllGeoAreas_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/geoArea/"));

        mockMvc.perform(get("/adminui/geoArea/")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGeoAreaChildren() throws Exception {

        GeoArea rootArea = th.geoAreaDeutschland;

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "42")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));
    }

    @Test
    public void getGeoAreaChildren_GlobalRoots() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("depth", "42")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("depth", "42")
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))))));
    }

    @Test
    public void getGeoAreaChildren_MultipleRoots() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId())
                        .param("depth", "1")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaDeutschland.getId(), th.geoAreaEisenberg.getId())
                        .param("depth", "1")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)))))));
    }

    @Test
    public void getGeoAreaChildren_IncludeBoundaryPoints() throws Exception {

        GeoArea rootArea = th.geoAreaDeutschland;

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "42")
                        .param("includeBoundaryPoints", "true")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtendedWithBoundaryPoints(rootArea,
                                th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtendedWithBoundaryPoints(
                                                        th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtendedWithBoundaryPoints(th.geoAreaMainz))))));
    }

    @Test
    public void getGeoAreaChildren_Depth() throws Exception {

        GeoArea rootArea = th.geoAreaDeutschland;

        //default value 1
        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz)))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz)))));

        //depth 2
        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "2")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "2")
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))))));

        //depth 1
        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "1")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)))));

        //depth 0
        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "0")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaEisenberg))));

        //depth invalid -> 0
        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "-42")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaEisenberg))));
    }

    @Test
    public void getGeoAreaChildren_RevealParentGeoAreas() throws Exception {

        GeoArea rootArea = th.geoAreaDeutschland;

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaDorf1InEisenberg.getId())
                        .param("revealParentGeoAreas", "true")
                        .param("depth", "0")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg)))))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("revealParentGeoAreas", "true")
                        .param("depth", "1")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)))))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaMainz.getId())
                        .param("revealParentGeoAreas", "true")
                        .param("depth", "0")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));
    }

    @Test
    public void getGeoAreaChildren_RevealVisibleGeoAreas() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaDorf1InEisenberg.getId())
                        .param("revealVisibleGeoAreas", "true")
                        .param("depth", "0")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("revealVisibleGeoAreas", "true")
                        .param("depth", "1")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaMainz.getId())
                        .param("revealVisibleGeoAreas", "true")
                        .param("depth", "0")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));
    }

    @Test
    public void getGeoAreaChildren_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/geoArea/children")
                .contentType(contentType));

        mockMvc.perform(get("/adminui/geoArea/children")
                        .headers(authHeadersFor(th.personRegularAnna))
                        .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGeoAreaChildrenSearch_Infix() throws Exception {

        // search for geo areas with id LIKE '2741c57d-30e9-414d-8055-7f05997a9101' (GeoAreaKaiserslautern)
        final String infixIdFull = "2741c57d-30e9-414d-8055-7f05997a9101";
        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixIdFull)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern)
                )));

        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixIdFull)
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern)
                )));

        // search for geo areas with id LIKE '2741c57d-' (GeoAreaKaiserslautern)
        final String infixIdPart = "2741c57d-";
        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixIdPart)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern)
                )));

        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixIdPart)
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern)
                )));

        // search for geo areas with name LIKE 'deutsch' (GeoAreaDeutschland)
        final String infixName = "deutsch";
        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "42")
                        .param("search", infixName)
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "42")
                        .param("search", infixName)
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))))));

        // search for geo areas with name LIKE 'buckel-dorf' (GeoAreaDorf1InEisenberg, GeoAreaDorf2InEisenberg)
        final String infixNameTwo = "buckel-dorf";
        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixNameTwo)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                        th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)
                )));

        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infixNameTwo)
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                        th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)
                )));

        // no result for infix
        final String infix = "blablub";
        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infix)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.emptyList()));

        mockMvc.perform(get("/adminui/geoArea/search")
                .param("depth", "42")
                .param("search", infix)
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.emptyList()));
    }

    @Test
    public void getGeoAreaChildrenSearch_IncludeBoundaryPoints() throws Exception {

        final GeoArea rootArea = th.geoAreaDeutschland;

        final String infixName = "deutsch";
        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "42")
                        .param("search", infixName)
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg),
                                                th.toClientGeoAreaExtended(th.geoAreaDorf2InEisenberg)),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));
    }

    @Test
    public void getGeoAreaChildrenSearch_RevealParentGeoAreas() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "0")
                        .param("search", "DorfEins")
                        .param("revealParentGeoAreas", "true")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg,
                                                th.toClientGeoAreaExtended(th.geoAreaDorf1InEisenberg)))))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "1")
                        .param("search", "Pfalz")
                        .param("revealParentGeoAreas", "true")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));
    }

    @Test
    public void getGeoAreaChildrenSearch_RevealVisibleGeoAreas() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "0")
                        .param("search", "Mainz")
                        .param("revealVisibleGeoAreas", "true")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "1")
                        .param("search", "Pfalz")
                        .param("revealVisibleGeoAreas", "true")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))),
                        th.toClientGeoAreaExtended(th.geoAreaTenant3))));
    }

    @Test
    public void getGeoAreaChildrenSearch_Depth() throws Exception {

        final GeoArea rootArea = th.geoAreaDeutschland;

        //default value 1
        final String infixName = "deutsch";
        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("search", infixName)
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz)))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("search", infixName)
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz)))));

        //depth 2
        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "2")
                        .param("search", infixName)
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern),
                                        th.toClientGeoAreaExtended(th.geoAreaMainz))))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "2")
                        .param("search", infixName)
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(rootArea,
                                th.toClientGeoAreaExtended(th.geoAreaRheinlandPfalz,
                                        th.toClientGeoAreaExtended(th.geoAreaEisenberg),
                                        th.toClientGeoAreaExtended(th.geoAreaKaiserslautern))))));

        //depth invalid -> 0
        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "-42")
                        .param("search", infixName)
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland))));

        mockMvc.perform(get("/adminui/geoArea/search")
                        .param("depth", "-42")
                        .param("search", infixName)
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoAreaExtended(th.geoAreaDeutschland))));
    }

    @Test
    public void getGeoAreaChildrenSearch_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/geoArea/search")
                .param("search", "blub"));

        mockMvc.perform(get("/adminui/geoArea/search")
                .param("search", "blub")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGeoAreaChildrenSearch_SearchParamMissing() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/search")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/geoArea/search")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getGeoAreaChildrenSearch_SearchParamToShort() throws Exception {

        mockMvc.perform(get("/adminui/geoArea/search")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", "de")
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));
    }

}
