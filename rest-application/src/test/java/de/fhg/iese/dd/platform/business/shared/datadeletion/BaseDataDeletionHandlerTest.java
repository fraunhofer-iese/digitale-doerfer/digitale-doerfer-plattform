/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.datadeletion;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.IReferenceDataDeletionService;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataChangeResult;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataCheckDeletionImpactRequest;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataDeletionRequest;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Inherit from this test to test the specific data handlers.
 */
public abstract class BaseDataDeletionHandlerTest extends BaseServiceTest {

    @Autowired
    protected SharedTestHelper sharedTestHelper;

    protected BaseSharedTestHelper th;
    @Autowired
    protected IReferenceDataDeletionService referenceDataDeletionService;
    @Autowired
    private IGeoAreaService geoAreaService;

    protected Tenant tenantToBeDeleted;
    protected Tenant tenantToBeUsedInstead;
    protected GeoArea geoAreaToBeDeleted;
    protected GeoArea geoAreaToBeUsedInstead;

    @Override
    public void createEntities() {

        createRequiredEntities();
        tenantToBeDeleted = th.tenant1;
        tenantToBeUsedInstead = th.tenant2;
        geoAreaToBeDeleted = th.geoAreaTenant1;
        geoAreaToBeUsedInstead = th.geoAreaTenant2;

        createAdditionalEntities();
    }

    protected void createRequiredEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
    }

    ;

    /**
     * These entities should be changed or deleted by the handler under test
     */
    protected abstract void createAdditionalEntities();

    /**
     * These assertions are checked after every deletion
     */
    protected abstract void assertAfterDeletion() throws Exception;

    protected abstract void assertAfterDeletionGeoArea() throws Exception;

    protected abstract void assertAfterDeletionTenant() throws Exception;

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    protected BaseSharedTestHelper getTestHelper() {
        return sharedTestHelper;
    }

    @PostConstruct
    private void initTestHelper() {
        //this is required because we use different test helpers in the subclasses of this test
        th = getTestHelper();
    }

    @Test
    @Tag("delete_reference_data")
    public void delete_Tenant() throws Exception {

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.toString())
                .enabled(false)
                .build());
        FeatureConfig configTenant = featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.toString())
                .tenant(tenantToBeDeleted)
                .enabled(true)
                .build());

        Person personImpacted = th.personRegularAnna;
        personImpacted.setTenant(tenantToBeDeleted);
        th.personRepository.saveAndFlush(personImpacted);

        checkImpactAndDelete(tenantToBeDeleted, tenantToBeUsedInstead, false);

        assertFalse(th.tenantRepository.existsById(tenantToBeDeleted.getId()));
        Person personChanged = th.personRepository.findById(personImpacted.getId()).get();
        assertThat(personChanged.getTenant()).isEqualTo(tenantToBeUsedInstead);
        assertFalse(featureConfigRepository.existsById(configTenant.getId()));

        //make sure to not have stale data
        appService.invalidateCache();
        geoAreaService.invalidateCache();

        assertAfterDeletionTenant();
        assertAfterDeletion();
    }

    @Test
    @Tag("delete_reference_data")
    public void delete_GeoArea() throws Exception {

        Person personImpacted = th.personRegularAnna;
        personImpacted.setHomeArea(geoAreaToBeDeleted);
        personImpacted.setTenant(geoAreaToBeDeleted.getTenant());
        th.personRepository.saveAndFlush(personImpacted);

        checkImpactAndDelete(geoAreaToBeDeleted, geoAreaToBeUsedInstead, false);

        assertFalse(th.geoAreaRepository.existsById(geoAreaToBeDeleted.getId()));
        Person personChanged = th.personRepository.findById(personImpacted.getId()).get();
        assertThat(personChanged.getHomeArea()).isEqualTo(geoAreaToBeUsedInstead);
        assertThat(personChanged.getTenant()).isEqualTo(geoAreaToBeUsedInstead.getTenant());
        
        //make sure to not have stale data
        appService.invalidateCache();
        geoAreaService.invalidateCache();

        assertAfterDeletionGeoArea();
        assertAfterDeletion();
    }

    protected <T extends BaseEntity> void checkImpactAndDelete(T entityToBeDeleted, T entityToBeUsedInstead,
            boolean deleteDependentEntities) {
        ReferenceDataChangeResult result =
                referenceDataDeletionService.checkReferenceDataDeletionImpact(
                        ReferenceDataCheckDeletionImpactRequest.builder()
                                .entityToBeDeleted(entityToBeDeleted)
                                .entityToBeUsedInstead(entityToBeUsedInstead)
                                .deleteDependentEntities(deleteDependentEntities)
                                .build(),
                        th.personIeseAdmin);
        log.info("Result check impact:\n{}", result.getMessage());
        assertThat(result.getExceptions()).isEmpty();
        result = referenceDataDeletionService.deleteReferenceData(ReferenceDataDeletionRequest.builder()
                        .entityToBeDeleted(entityToBeDeleted)
                        .entityToBeUsedInstead(entityToBeUsedInstead)
                        .deleteDependentEntities(deleteDependentEntities)
                        .build(),
                th.personIeseAdmin);
        log.info("Result delete:\n{}", result.getMessage());
        assertThat(result.getExceptions()).isEmpty();
    }

}
