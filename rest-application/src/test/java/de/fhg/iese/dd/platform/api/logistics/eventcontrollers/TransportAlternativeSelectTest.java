/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class TransportAlternativeSelectTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        String shopOrderId = purchaseOrderReceived();

        delivery = purchaseOrderReadyForTransport(shopOrderId);
     }

    @Test
    public void onTransportAlternativeSelectRequest() throws Exception {

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        transportAlternativeSelectRequest(delivery, transportAlternative, carrier);
    }

    @Test
    public void onTransportAlternativeSelectRequestRejected() throws Exception {

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest = new ClientTransportAlternativeSelectRequest(
            transportAlternative.getId());

        mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrierLastStep))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.accepted").value(false))
            .andExpect(jsonPath("$.transportAlternativeId").value(transportAlternative.getId()))
            .andExpect(jsonPath("$.transportAlternativeBundleToRemoveId").value(transportAlternative.getTransportAlternativeBundle().getId()))
            .andExpect(jsonPath("$.transportAssignmentToLoadId").isEmpty());
    }

    @Test
    public void onTransportAlternativeSelectRequestDuplicated() throws Exception {

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        TransportAssignment firstAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        waitForEventProcessing();

        TransportAssignment secondAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        assertEquals(firstAssignment, secondAssignment);
    }

    @Test
    public void onTransportAlternativeSelectRequestTransportAlternativeIdInvalid() throws Exception {

        String transportAlternativeId = "";
        ClientTransportAlternativeSelectRequest alternativeSelectRequest = new ClientTransportAlternativeSelectRequest(transportAlternativeId);

        mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(alternativeSelectRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportAlternativeSelectRequestTransportAlternativeIdNull() throws Exception {

        ClientTransportAlternativeSelectRequest alternativeSelectRequest = new ClientTransportAlternativeSelectRequest(null);

        mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(alternativeSelectRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportAlternativeSelectRequestTransportAlternativeUnauthorized() throws Exception {
        ClientTransportAlternativeSelectRequest alternativeSelectRequest =
                new ClientTransportAlternativeSelectRequest("randomID");

        assertOAuth2(post("/logistics/event/transportAlternativeSelectRequest")
                .content(json(alternativeSelectRequest))
                .contentType(contentType));

    }

}
