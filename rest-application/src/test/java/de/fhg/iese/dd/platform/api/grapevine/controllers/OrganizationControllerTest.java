/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationPerson;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrganizationControllerTest extends BaseServiceTest {

    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createOrganizations();
        th.createNewsSources();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllAvailableOrganizations() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        //no selection
        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        //selection of geo area that has one organization
        th.selectGeoAreaForAppVariant(appVariant, caller, th.geoAreaDorf2InEisenberg);
        OrganizationPerson organizationPerson0 = th.organizationGartenbau.getOrganizationPersons().get(0);
        OrganizationPerson organizationPerson1 = th.organizationGartenbau.getOrganizationPersons().get(1);
        //no tag filter

        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(th.organizationGartenbau.getId()))
                .andExpect(jsonPath("$[0].name").value(th.organizationGartenbau.getName()))
                .andExpect(jsonPath("$[0].locationDescription")
                        .value(th.organizationGartenbau.getLocationDescription()))
                .andExpect(jsonPath("$[0].description").value(th.organizationGartenbau.getDescription()))
                .andExpect(jsonPath("$[0].overviewImage.id").value(th.organizationGartenbau.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].logoImage.id").value(th.organizationGartenbau.getLogoImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id")
                        .value(th.organizationGartenbau.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[0].url").value(th.organizationGartenbau.getUrl()))
                .andExpect(jsonPath("$[0].phone").value(th.organizationGartenbau.getPhone()))
                .andExpect(jsonPath("$[0].tags").value(th.organizationGartenbau.getTags().getValues().stream()
                        .sorted()
                        .map(OrganizationTag::toString)
                        .collect(Collectors.toList())))
                .andExpect(jsonPath("$[0].emailAddress").value(th.organizationGartenbau.getEmailAddress()))
                .andExpect(jsonPath("$[0].address.id").value(th.organizationGartenbau.getAddress().getId()))
                .andExpect(jsonPath("$[0].address.name").value(th.organizationGartenbau.getAddress().getName()))
                .andExpect(jsonPath("$[0].address.street").value(th.organizationGartenbau.getAddress().getStreet()))
                .andExpect(jsonPath("$[0].address.zip").value(th.organizationGartenbau.getAddress().getZip()))
                .andExpect(jsonPath("$[0].address.city").value(th.organizationGartenbau.getAddress().getCity()))
                .andExpect(jsonPath("$[0].facts[0].name").value(th.organizationGartenbau.getFacts().get(0).getName()))
                .andExpect(jsonPath("$[0].facts[0].value").value(th.organizationGartenbau.getFacts().get(0).getValue()))
                .andExpect(jsonPath("$[0].facts[1].name").value(th.organizationGartenbau.getFacts().get(1).getName()))
                .andExpect(jsonPath("$[0].facts[1].value").value(th.organizationGartenbau.getFacts().get(1).getValue()))
                .andExpect(jsonPath("$[0].persons[0].id").value(organizationPerson0.getId()))
                .andExpect(jsonPath("$[0].persons[0].category").value(organizationPerson0.getCategory()))
                .andExpect(jsonPath("$[0].persons[0].position").value(organizationPerson0.getPosition()))
                .andExpect(jsonPath("$[0].persons[0].name").value(organizationPerson0.getName()))
                .andExpect(jsonPath("$[0].persons[0].profilePicture.id")
                        .value(organizationPerson0.getProfilePicture().getId()))
                .andExpect(jsonPath("$[0].persons[1].id").value(organizationPerson1.getId()))
                .andExpect(jsonPath("$[0].persons[1].category").value(organizationPerson1.getCategory()))
                .andExpect(jsonPath("$[0].persons[1].position").value(organizationPerson1.getPosition()))
                .andExpect(jsonPath("$[0].persons[1].name").value(organizationPerson1.getName()))
                .andExpect(jsonPath("$[0].persons[1].profilePicture.id")
                        .value(organizationPerson1.getProfilePicture().getId()))
                .andExpect(jsonPath("$[0].geoAreaIds")
                        .value(Matchers.contains(th.geoAreaDorf1InEisenberg.getId(),
                                th.geoAreaDorf2InEisenberg.getId())));

        //selection of additional geo area that has both organizations
        th.selectGeoAreaForAppVariant(appVariant, caller, th.geoAreaDorf1InEisenberg);
        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(th.organizationFeuerwehr.getId()))
                .andExpect(jsonPath("$[0].name").value(th.organizationFeuerwehr.getName()))
                .andExpect(jsonPath("$[0].description").value(th.organizationFeuerwehr.getDescription()))
                .andExpect(jsonPath("$[0].url").value(th.organizationFeuerwehr.getUrl()))
                .andExpect(jsonPath("$[0].tags").value(th.organizationFeuerwehr.getTags().getValues().stream()
                        .sorted()
                        .map(OrganizationTag::toString)
                        .collect(Collectors.toList())))
                .andExpect(jsonPath("$[0].overviewImage.id").value(th.organizationFeuerwehr.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id").value(
                        th.organizationFeuerwehr.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[0].detailImages[1].id").value(
                        th.organizationFeuerwehr.getDetailImages().get(1).getId()))
                .andExpect(jsonPath("$[0].geoAreaIds")
                        .value(Matchers.contains(th.geoAreaDorf1InEisenberg.getId())))
                .andExpect(jsonPath("$[1].id").value(th.organizationGartenbau.getId()))
                .andExpect(jsonPath("$[1].name").value(th.organizationGartenbau.getName()))
                .andExpect(jsonPath("$[1].description").value(th.organizationGartenbau.getDescription()))
                .andExpect(jsonPath("$[1].url").value(th.organizationGartenbau.getUrl()))
                .andExpect(jsonPath("$[1].tags").value(th.organizationGartenbau.getTags().getValues().stream()
                        .sorted()
                        .map(OrganizationTag::toString)
                        .collect(Collectors.toList())))
                .andExpect(jsonPath("$[1].overviewImage.id").value(th.organizationGartenbau.getOverviewImage().getId()))
                .andExpect(jsonPath("$[1].detailImages[0].id").value(
                        th.organizationGartenbau.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[1].geoAreaIds")
                        .value(Matchers.contains(th.geoAreaDorf1InEisenberg.getId(),
                                th.geoAreaDorf2InEisenberg.getId())));
    }

    @Test
    public void getAllAvailableOrganizations_Deleted() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        th.organizationFeuerwehr.setDeleted(true);
        th.organizationFeuerwehr.setDeletionTime(12021983L);
        th.organizationFeuerwehr = organizationRepository.save(th.organizationFeuerwehr);

        //selection of geo area that has both organizations, but one is deleted
        th.selectGeoAreaForAppVariant(appVariant, caller, th.geoAreaDorf1InEisenberg);
        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(th.organizationGartenbau.getId()));
    }

    @Test
    public void getAllAvailableOrganizations_TagFilter() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        //selection of geo area that has both organizations
        th.selectGeoAreaForAppVariant(appVariant, caller, th.geoAreaDorf1InEisenberg);

        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(th.organizationFeuerwehr.getId()));

        //both now have the organization-tag
        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredTags", ClientOrganizationTag.EMERGENCY_AID.toString(),
                                ClientOrganizationTag.VOLUNTEERING.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(th.organizationFeuerwehr.getId()))
                .andExpect(jsonPath("$[1].id").value(th.organizationGartenbau.getId()));
    }


    @Test
    public void getAllAvailableOrganizations_InvalidParameters() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/organization")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredTags", "MY_OWN_TAG"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAllAvailableOrganizations_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/organization"),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

    @Test
    public void getOrganizationById() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        //geo area selection at app variant is not required
        th.appVariantUsageAreaSelectionRepository.deleteAll();

        OrganizationPerson organizationPerson0 = th.organizationFeuerwehr.getOrganizationPersons().get(0);
        OrganizationPerson organizationPerson1 = th.organizationFeuerwehr.getOrganizationPersons().get(1);
        mockMvc.perform(get("/grapevine/organization/{organizationId}", th.organizationFeuerwehr.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.organizationFeuerwehr.getId()))
                .andExpect(jsonPath("$.name").value(th.organizationFeuerwehr.getName()))
                .andExpect(jsonPath("$.locationDescription").value(th.organizationFeuerwehr.getLocationDescription()))
                .andExpect(jsonPath("$.description").value(th.organizationFeuerwehr.getDescription()))
                .andExpect(jsonPath("$.overviewImage.id").value(th.organizationFeuerwehr.getOverviewImage().getId()))
                .andExpect(jsonPath("$.logoImage.id").value(th.organizationFeuerwehr.getLogoImage().getId()))
                .andExpect(jsonPath("$.detailImages[0].id").value(
                        th.organizationFeuerwehr.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$.url").value(th.organizationFeuerwehr.getUrl()))
                .andExpect(jsonPath("$.phone").value(th.organizationFeuerwehr.getPhone()))
                .andExpect(jsonPath("$.emailAddress").value(th.organizationFeuerwehr.getEmailAddress()))
                .andExpect(jsonPath("$.address.id").value(th.organizationFeuerwehr.getAddress().getId()))
                .andExpect(jsonPath("$.address.name").value(th.organizationFeuerwehr.getAddress().getName()))
                .andExpect(jsonPath("$.address.street").value(th.organizationFeuerwehr.getAddress().getStreet()))
                .andExpect(jsonPath("$.address.zip").value(th.organizationFeuerwehr.getAddress().getZip()))
                .andExpect(jsonPath("$.address.city").value(th.organizationFeuerwehr.getAddress().getCity()))
                .andExpect(jsonPath("$.facts[0].name").value(th.organizationFeuerwehr.getFacts().get(0).getName()))
                .andExpect(jsonPath("$.facts[0].value").value(th.organizationFeuerwehr.getFacts().get(0).getValue()))
                .andExpect(jsonPath("$.facts[1].name").value(th.organizationFeuerwehr.getFacts().get(1).getName()))
                .andExpect(jsonPath("$.facts[1].value").value(th.organizationFeuerwehr.getFacts().get(1).getValue()))
                .andExpect(jsonPath("$.persons[0].id").value(organizationPerson0.getId()))
                .andExpect(jsonPath("$.persons[0].category").value(organizationPerson0.getCategory()))
                .andExpect(jsonPath("$.persons[0].position").value(organizationPerson0.getPosition()))
                .andExpect(jsonPath("$.persons[0].name").value(organizationPerson0.getName()))
                .andExpect(jsonPath("$.persons[0].profilePicture.id").value(
                        organizationPerson0.getProfilePicture().getId()))
                .andExpect(jsonPath("$.persons[1].id").value(organizationPerson1.getId()))
                .andExpect(jsonPath("$.persons[1].category").value(organizationPerson1.getCategory()))
                .andExpect(jsonPath("$.persons[1].position").value(organizationPerson1.getPosition()))
                .andExpect(jsonPath("$.persons[1].name").value(organizationPerson1.getName()))
                .andExpect(jsonPath("$.persons[1].profilePicture.id")
                        .value(organizationPerson1.getProfilePicture().getId()))
                .andExpect(jsonPath("$.geoAreaIds")
                        .value(Matchers.contains(th.geoAreaDorf1InEisenberg.getId())));
    }

    @Test
    public void getOrganizationById_NotFound() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/organization/{organizationId}", UUID.randomUUID().toString())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));

        mockMvc.perform(get("/grapevine/organization/{organizationId}", "🐖")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));
    }

    @Test
    public void getOrganizationById_Deleted() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        th.organizationFeuerwehr.setDeleted(true);
        th.organizationFeuerwehr.setDeletionTime(12021983L);
        th.organizationFeuerwehr = organizationRepository.save(th.organizationFeuerwehr);

        mockMvc.perform(get("/grapevine/organization/{organizationId}", th.organizationFeuerwehr.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));
    }

    @Test
    public void getOrganizationById_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/organization/{organizationId}", th.organizationFeuerwehr.getId()),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

}
