/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.Builder;
import lombok.Getter;

@Getter
public class AuthorizationData {

    private final boolean noAccessToken;
    private final String oauthUserId;
    private final String verifiedEmail;
    private final String oauthClientIdentifier;
    private final String appVariantIdentifier;
    private final String apiKey;

    @Builder
    public AuthorizationData(
            boolean noAccessToken,
            String oauthUserId,
            String verifiedEmail,
            String oauthClientIdentifier,
            String appVariantIdentifier,
            Person person,
            OauthClient oauthClient,
            AppVariant appVariant,
            String apiKey) {
        this.verifiedEmail = verifiedEmail;
        if (appVariantIdentifier == null && appVariant != null) {
            appVariantIdentifier = appVariant.getAppVariantIdentifier();
        }
        if (oauthClientIdentifier == null) {
            if (oauthClient != null) {
                oauthClientIdentifier = oauthClient.getOauthClientIdentifier();
            } else {
                if (appVariant != null && !CollectionUtils.isEmpty(appVariant.getOauthClients())) {
                    //we just take any of the oauth clients
                    oauthClientIdentifier =
                            appVariant.getOauthClients().iterator().next().getOauthClientIdentifier();
                }
            }
        }
        if (oauthUserId == null && person != null) {
            oauthUserId = person.getOauthId();
        }
        this.noAccessToken = noAccessToken;
        this.oauthUserId = oauthUserId;
        this.appVariantIdentifier = appVariantIdentifier;
        this.oauthClientIdentifier = oauthClientIdentifier;
        this.apiKey = apiKey;
    }

    public AuthorizationData withoutAccessToken() {
        return AuthorizationData.builder()
                .oauthUserId(null)
                .noAccessToken(true)
                .verifiedEmail(verifiedEmail)
                .oauthClientIdentifier(oauthClientIdentifier)
                .appVariantIdentifier(appVariantIdentifier)
                .apiKey(apiKey)
                .build();
    }

}
