/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.ClientPostPageBean;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInternalDataPrivacyService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostControllerActivityTest extends BasePostControllerTest {

    @Autowired
    private IInternalDataPrivacyService internalDataPrivacyService;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllPostsWithOwnActivities() throws Exception {

        // personLaraSchaefer takes only part in happening2 without leaving comment
        Person caller = th.personLaraSchaefer;
        th.happeningParticipantRepository.save(new HappeningParticipant(th.happening2, caller));
        th.happening2.setParticipantCount(1);
        th.happening2 = th.happeningRepository.saveAndFlush(th.happening2);

        List<Post> expectedPosts = new ArrayList<>();
        expectedPosts.add(th.tenant2Gossip1Comment2.getPost());
        expectedPosts.add(th.newsItem2Comment2.getPost());
        expectedPosts.add(th.happening1Comment1.getPost());
        expectedPosts.add(th.happening2);
        expectedPosts.add(th.offer1Comment1.getPost());
        expectedPosts.add(th.offer4Comment1.getPost());
        expectedPosts.add(th.specialPost2Comment1.getPost());

        // verify test data integrity. If failing, test data in GrapevineTestHelper has been altered.
        // at the moment we have 2 Gossips (but the first one deleted), 1 News, 2 Offers, 1 SpecialPost, 1 Happening commented
        // and participation in 1 Happening by Lara Schäfer
        assertEquals(7, expectedPosts.size());
        Post firstPost = expectedPosts.get(0);
        int pageSize = 2;
        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        Post thirdPost = expectedPosts.get(2);
        Post fourthPost = expectedPosts.get(3);
        assertEquals(thirdPost.getPostType(), PostType.Happening);
        assertEquals(fourthPost.getPostType(), PostType.Happening);

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize))
                //already tested with the assertion above, just for clarification
                .andExpect(jsonPath("$.content[0].happening.id").value(thirdPost.getId()))
                .andExpect(jsonPath("$.content[0].happening.participated").value(false))
                .andExpect(jsonPath("$.content[1].happening.id").value(fourthPost.getId()))
                .andExpect(jsonPath("$.content[1].happening.participantCount").value(1))
                .andExpect(jsonPath("$.content[1].happening.participated").value(true));

        Post seventhPost = expectedPosts.get(6);
        // If failing, please check GrapevineTestHelper
        assertEquals(seventhPost.getPostType(), PostType.SpecialPost);

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "3")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 3, pageSize))
                //already tested with the assertion above, just for clarification
                .andExpect(jsonPath("$.content[0].specialPost.id").value(seventhPost.getId()));
    }

    @Test
    public void getAllPostsWithOwnActivities_AfterOtherUserHasBeenDeleted() throws Exception {

        Person caller = th.personLaraSchaefer;
        Person personToBeDeleted = th.personMonikaHess;

        List<Post> expectedPostsBeforeUserDeletion = new ArrayList<>();
        expectedPostsBeforeUserDeletion.add(th.tenant2Gossip1Comment2.getPost());
        expectedPostsBeforeUserDeletion.add(th.newsItem2Comment2.getPost());
        expectedPostsBeforeUserDeletion.add(th.happening1Comment1.getPost());
        expectedPostsBeforeUserDeletion.add(th.offer1Comment1.getPost());
        expectedPostsBeforeUserDeletion.add(th.offer4Comment1.getPost());
        expectedPostsBeforeUserDeletion.add(th.specialPost2Comment1.getPost());

        //we have 2 Gossips (but the first one deleted), 1 NewsItem, 2 Offers, 1 SpecialPost and 1 Happening commented by Lara Schaefer
        assertEquals(6, expectedPostsBeforeUserDeletion.size());
        int pageSize = 4;
        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPostsBeforeUserDeletion, 0, pageSize));

        //delete one user in whose posts person has commented something
        internalDataPrivacyService.deleteInternalUserData(personToBeDeleted, new PersonDeletionReport());

        //we wait here to ensure that the deletion was done completely
        waitForEventProcessing();

        List<Post> expectedPostsAfterUserDeletion = new ArrayList<>();
        // th.tenant2Gossip1Comment2.getPost() is not expected, as this post was from the deleted user
        expectedPostsAfterUserDeletion.add(th.newsItem2Comment2.getPost());
        expectedPostsAfterUserDeletion.add(th.happening1Comment1.getPost());
        expectedPostsAfterUserDeletion.add(th.offer1Comment1.getPost());
        expectedPostsAfterUserDeletion.add(th.offer4Comment1.getPost());
        expectedPostsAfterUserDeletion.add(th.specialPost2Comment1.getPost());

        //we refresh them, so that all values are updated
        expectedPostsAfterUserDeletion = expectedPostsAfterUserDeletion.stream()
                .map(post -> th.postRepository.findById(post.getId()).get())
                .collect(Collectors.toList());

        //make sure there is at least one post less
        assertTrue(expectedPostsBeforeUserDeletion.size() > expectedPostsAfterUserDeletion.size());

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPostsAfterUserDeletion, 0, pageSize));
    }

    @Test
    public void getAllPostsWithOwnActivities_InternalSuggestion() throws Exception {

        Person caller = th.personAnnikaSchneider;
        AppVariant appVariant = th.appVariant4AllTenants;

        //internal suggestions should not be delivered at this endpoint
        final Suggestion internalSuggestion = th.suggestionInternal;
        final Suggestion normalSuggestion = th.suggestionDefect2;

        th.setTimeStampsAndSave(Comment.builder()
                .creator(caller)
                .text("Mein internes Kommentar!")
                .post(internalSuggestion)
                .build());
        th.setTimeStampsAndSave(Comment.builder()
                .creator(caller)
                .text("Mein normales Kommentar!")
                .post(normalSuggestion)
                .build());

        ClientPostPageBean allActualPosts = toObject(mockMvc.perform(get("/grapevine/activity")
                        .param("page", "0")
                        .param("count", "100")
                        .headers(authHeadersFor(caller, appVariant)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(),
                ClientPostPageBean.class);

        assertThat(allActualPosts.getContent()).extracting(ClientPost::getId)
                .doesNotContain(internalSuggestion.getId());
        assertThat(allActualPosts.getContent()).extracting(ClientPost::getId)
                .contains(normalSuggestion.getId());
    }

    @Test
    public void getAllPostsWithOwnActivities_Unauthorized() throws Exception {
        assertOAuth2(get("/grapevine/activity"));
    }

}
