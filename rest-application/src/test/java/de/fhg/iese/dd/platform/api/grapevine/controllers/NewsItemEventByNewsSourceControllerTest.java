/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.newsitem.ClientNewsItemCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientCroppedImageReference;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientAbsoluteImageCropDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientRelativeImageCropDefinition;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsSourceFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsItem;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileDownloadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NewsItemEventByNewsSourceControllerTest extends BasePostEventByNewsSourceControllerTest<NewsItem> {

    @Override
    protected NewsItem getTestPost1() {
        return th.newsItem1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personSusanneChristmann;
    }

    @Override
    protected NewsItem getTestPost2() {
        return th.newsItem4;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.gossip1withImages;
    }

    @Test
    public void newsItemPublishRequest_RequestAsJsonString() throws Exception {

        String author = "Susanne Christmann";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        // field publishImmediately is missing in json string but the default value is set to true -> post should be published
                        .content("{\n" +
                                "\"authorName\": \"" + author + "\",\n" +
                                "\"categories\": \"" + categories + "\",\n" +
                                "\"externalId\": \"" + externalId + "\",\n" +
                                "\"geoAreaId\": \"" + geoArea.getId() + "\",\n" +
                                "\"newsURL\": \"" + newsURL + "\",\n" +
                                "\"text\": \"" + text + "\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemCreateConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);
        assertThat(publishedNewsItem.isPublished()).isTrue();
        assertThat(publishedNewsItem.isCommentsAllowed()).isTrue();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemCreateConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemCreateConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_CompleteVerifyingPushMessage() throws Exception {

        String author = "Susanne Christmann";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";

        URL imageURL1 = new URL("https://example.org/image1.png");
        URL imageURL2 = new URL("https://example.org/image2.jpg");
        MediaItem mediaItem1 = testMediaItemService.setupDownloadMock(imageURL1);
        MediaItem mediaItem2 = testMediaItemService.setupDownloadMock(imageURL2);
        Organization organization = th.organizationFeuerwehr;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(geoArea.getId())
                        .authorName(author)
                        .text(text)
                        .organizationId(organization.getId())
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(categories)
                        .commentsDisallowed(true)
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(newsItemCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemCreateConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getOrganization()).isEqualTo(organization);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(publishedNewsItem.getImages()).hasSize(2);
        assertThat(publishedNewsItem.getImages()).containsExactly(mediaItem1, mediaItem2);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);
        assertThat(publishedNewsItem.isCommentsAllowed()).isFalse();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemCreateConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemCreateConfirmation.getPostId());
        assertThat(pushedEvent.getOrganizationTags()).containsExactly(ClientOrganizationTag.EMERGENCY_AID,
                ClientOrganizationTag.VOLUNTEERING);
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_CompleteVerifyingPushMessage_MultiGeoArea() throws Exception {

        String author = "Susanne Christmann";
        Set<GeoArea> geoAreas = Set.of(th.subGeoArea1Tenant1, th.subGeoArea2Tenant1, th.geoAreaKaiserslautern);
        List<String> geoAreaIds = geoAreas.stream()
                .map(GeoArea::getId)
                .toList();
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";

        URL imageURL1 = new URL("https://example.org/image1.png");
        URL imageURL2 = new URL("https://example.org/image2.jpg");
        MediaItem mediaItem1 = testMediaItemService.setupDownloadMock(imageURL1);
        MediaItem mediaItem2 = testMediaItemService.setupDownloadMock(imageURL2);
        Organization organization = th.organizationFeuerwehr;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaIds(geoAreaIds)
                        .authorName(author)
                        .text(text)
                        .organizationId(organization.getId())
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(categories)
                        .commentsDisallowed(true)
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(newsItemCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemCreateConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoAreas.stream().findFirst().get().getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(geoAreas);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getOrganization()).isEqualTo(organization);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(publishedNewsItem.getImages()).hasSize(2);
        assertThat(publishedNewsItem.getImages()).containsExactly(mediaItem1, mediaItem2);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);
        assertThat(publishedNewsItem.isCommentsAllowed()).isFalse();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemCreateConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemCreateConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_ImageDownloadFailed() throws Exception {

        URL imageURL1 = new URL("https://example.org/image1.png");
        URL imageURL2 = new URL("https://example.org/image2.jpg");
        URL imageURLFail = new URL("https://example.org/imageFail");
        testMediaItemService.addTestImageToDownload(imageURL1, th.createTestImageFile().getBytes());
        testMediaItemService.addTestImageToDownload(imageURL2, th.createTestImageFile().getBytes());
        testMediaItemService.addTestException(imageURLFail, new FileDownloadException("Wordpress spackt"));
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName("Karl Napp")
                                .text("Neue Bilder")
                                .externalId("miau")
                                .newsURL("https://www.iese.de/dse")
                                .categories("Katzenbilder")
                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString(), imageURLFail.toString()))
                                .build())))
                .andExpect(isException(ClientExceptionType.FILE_DOWNLOAD_FAILED));
    }

    @Test
    public void newsItemPublishRequest_NotPublished() throws Exception {

        String author = "Susanne Christmann";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem mediaItem1 = testMediaItemService.setupDownloadMock(imageURL1);
        MediaItem mediaItem2 = testMediaItemService.setupDownloadMock(imageURL2);

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        //the item should not be published, but created silently
                        .publishImmediately(false)
                        .geoAreaId(geoArea.getId())
                        .authorName(author)
                        .text(text)
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(categories)
                        .commentsDisallowed(true)
                        .build();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation = toObject(
                mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .content(json(newsItemCreateRequest)))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse(),
                ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemCreateConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(2);
        assertThat(publishedNewsItem.getImages()).containsExactly(mediaItem1, mediaItem2);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);
        assertThat(publishedNewsItem.isCommentsAllowed()).isFalse();
        assertThat(publishedNewsItem.isPublished()).isFalse();

        // verify push message
        waitForEventProcessing();

        //no push, since it is not published
        testClientPushService.assertNoMorePushedEvents();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest
                newsItemCreateRequest2 = ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                //it should not get "changed", but published
                .publishImmediately(true)
                .geoAreaId(geoArea.getId())
                .authorName(author)
                .text(text)
                .externalId(externalId)
                .newsURL(newsURL)
                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                .categories(categories)
                .commentsDisallowed(true)
                .build();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation2 = toObject(
                mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .content(json(newsItemCreateRequest2)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse(),
                ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem2 =
                (NewsItem) th.externalPostRepository.findById(newsItemCreateConfirmation2.getPostId()).get();
        assertThat(publishedNewsItem2.isPublished()).isTrue();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem2.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertThat(newsItemCreateConfirmation.getPostId()).isEqualTo(newsItemCreateConfirmation2.getPostId());
        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemCreateConfirmation2.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemCreateConfirmation2.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_CompleteVerifyingPushMessage() throws Exception {

        String author = "Susanne Weihnachtsfrau";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MockMultipartFile imageDownload1 = th.createTestImageFile("thing1.jpg");
        MockMultipartFile imageDownload2 = th.createTestImageFile("thing2.jpg");
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");
        testMediaItemService.addTestImageToDownload(imageURL1, imageDownload1.getBytes());
        testMediaItemService.addTestImageToDownload(imageURL2, imageDownload2.getBytes());

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0d).relativeOffsetY(0d)
                        .relativeHeight(1d).relativeWidth(1d)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(300).absoluteOffsetY(400)
                        .absoluteHeight(800).absoluteWidth(600)
                        .build())
                .build();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(geoArea.getId())
                        .authorName(author)
                        .text(text)
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(categories)
                        .commentsDisallowed(false)
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .file(imageUpload)
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(newsItemCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(5);
        assertThat(publishedNewsItem.isCommentsAllowed()).isTrue();
        //tests for the actual image content is tested at newsItemPublishRequestWithImageUpload_ImageOrder

        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemPublishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemPublishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_ImageOrder() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem imageDownload1 = th.createMediaItem("thing1.jpg");
        MediaItem imageDownload2 = th.createMediaItem("thing2.jpg");
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");
        testMediaItemService.addTestMediaItem(imageURL1, imageDownload1);
        testMediaItemService.addTestMediaItem(imageURL2, imageDownload2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0.25).relativeOffsetY(0.25)
                        .relativeHeight(0.5).relativeWidth(0.5)
                        .build())
                .build();

        MvcResult publishRequestResult =
                mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                                .file(imageUpload)
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .param("request", json(
                                        ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                                .geoAreaId(th.geoAreaEisenberg.getId())
                                                .authorName("Fieser Fritz")
                                                .text("Neues Rezept für Hundekuchen")
                                                .externalId("oi-die")
                                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                                .categories("Mampf")
                                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                                                .croppedImages(List.of(clientCroppedImageReference1,
                                                        clientCroppedImageReference2)).build())))
                        .andExpect(status().isOk())
                        .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getImages().size()).isEqualTo(5);

        //the first image can not be verified, since it is created as a new media item from the uploaded content
        assertThat(publishedNewsItem.getImages().get(1)).isEqualTo(imageDownload1);
        assertThat(publishedNewsItem.getImages().get(2)).isEqualTo(imageDownload2);
        MediaItem mediaItem3 = publishedNewsItem.getImages().get(3);
        assertThat(mediaItem3.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1.0);
        MediaItem mediaItem4 = publishedNewsItem.getImages().get(4);
        assertThat(mediaItem4.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.25);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5);
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_ImageOrder_Legacy() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem imageDownload1 = th.createMediaItem("thing1.jpg");
        MediaItem imageDownload2 = th.createMediaItem("thing2.jpg");
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");
        testMediaItemService.addTestMediaItem(imageURL1, imageDownload1);
        testMediaItemService.addTestMediaItem(imageURL2, imageDownload2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .x(0).y(0)
                .height(900).width(1200)
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .x(300).y(400)
                .height(800).width(600)
                .build();

        MvcResult publishRequestResult =
                mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                                .file(imageUpload)
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .param("request", json(
                                        ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                                .geoAreaId(th.geoAreaEisenberg.getId())
                                                .authorName("Fieser Fritz")
                                                .text("Neues Rezept für Hundekuchen")
                                                .externalId("oi-die")
                                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                                .categories("Mampf")
                                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                                                .croppedImages(List.of(clientCroppedImageReference1,
                                                        clientCroppedImageReference2)).build())))
                        .andExpect(status().isOk())
                        .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getImages().size()).isEqualTo(5);

        //the first image can not be verified, since it is created as a new media item from the uploaded content
        assertThat(publishedNewsItem.getImages().get(1)).isEqualTo(imageDownload1);
        assertThat(publishedNewsItem.getImages().get(2)).isEqualTo(imageDownload2);
        MediaItem mediaItem3 = publishedNewsItem.getImages().get(3);
        assertThat(mediaItem3.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1.0);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1.0);
        MediaItem mediaItem4 = publishedNewsItem.getImages().get(4);
        assertThat(mediaItem4.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.25);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5);
        assertThat(mediaItem4.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5);
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_ClientMediaItem() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem imageDownload1 = th.createMediaItem("thing1.jpg");
        MediaItem imageDownload2 = th.createMediaItem("thing2.jpg");
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");
        testMediaItemService.addTestMediaItem(imageURL1, imageDownload1);
        testMediaItemService.addTestMediaItem(imageURL2, imageDownload2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0d).relativeOffsetY(0d)
                        .relativeHeight(1d).relativeWidth(1d)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0.25).relativeOffsetY(0.25)
                        .relativeHeight(0.5).relativeWidth(0.5)
                        .build())
                .build();

        MvcResult publishRequestResult =
                mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                                .file(imageUpload)
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .param("request", json(
                                        ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                                .geoAreaId(th.geoAreaEisenberg.getId())
                                                .authorName("Fieser Fritz")
                                                .text("Neues Rezept für Hundekuchen")
                                                .externalId("oi-die")
                                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                                .categories("Mampf")
                                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                                                .croppedImages(List.of(clientCroppedImageReference1,
                                                        clientCroppedImageReference2))
                                                .build())))
                        .andExpect(status().isOk())
                        .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();

        Person caller = th.personSusanneChristmann;
        mockMvc.perform(get("/grapevine/post/{postId}", publishedNewsItem.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, publishedNewsItem));
    }

    @Test
    public void newsItemPublishRequest_NoImages() throws Exception {

        String author = "Susanne Christman";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Beim Spiel am Sonntag hat sich der FSV Digitalbach zuhause erfolgreich gegen den FC Analogtal" +
                " durchgesetzt. Die Analogtaler unterlagen dem Tabellenführer mit 1:2. In der ersten Hälfte " +
                "verpassten die Digitalbacher…";
        String categories = "Sport";
        String externalId = "external id for published news";
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(geoArea.getId())
                        .authorName(author)
                        .text(text)
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .categories(categories)
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(newsItemCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(CollectionUtils.isEmpty(publishedNewsItem.getImages())).isTrue();
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);
        assertThat(publishedNewsItem.isCommentsAllowed()).isTrue();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemPublishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemPublishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_NoImages() throws Exception {

        String author = "Susanne Farmer";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Rhabarber Rhabarber";
        String categories = "Gemüse";
        String externalId = "ei-die";
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = newsSource.getSiteUrl() + "/rezept.html";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(geoArea.getId())
                        .authorName(author)
                        .text(text)
                        .externalId(externalId)
                        .newsURL(newsURL)
                        .categories(categories)
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(newsItemCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemPublishConfirmation.getPostId()).get();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(geoArea.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(geoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(author);
        assertThat(publishedNewsItem.getText()).isEqualTo(text);
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(externalId);
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsURL);
        assertThat(CollectionUtils.isEmpty(publishedNewsItem.getImages())).isTrue();
        assertThat(publishedNewsItem.getCategories()).isEqualTo(categories);

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedNewsItem.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(newsItemPublishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(newsItemPublishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_Warning_GeoArea() throws Exception {

        GeoArea geoArea = th.geoAreaMainz;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(NewsSourceFeature.class.getName())
                .configValues(
                        "{ \"enforceGeoAreaCheck\": false, \"enforcePostURLCheck\": false }")
                .build()
                .withConstantId());

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaIds(List.of(geoArea.getId(), th.subGeoArea1Tenant1.getId()))
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Hundekuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(status().isOk());

        List<Triple<String, Tenant, TeamNotificationMessage>> newsConfigMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().stream()
                        .filter(m -> "grapevine-news-config".equals(m.getLeft()))
                        .collect(Collectors.toList());
        assertThat(newsConfigMessage).hasSize(1);
        String messageText = newsConfigMessage.get(0).getRight().getText();

        assertThat(messageText).containsIgnoringCase(geoArea.getId());
        assertThat(messageText).containsIgnoringCase(newsSource.getAppVariant().getAppVariantIdentifier());

        testDefaultTeamNotificationPublisher.reset();

        geoArea = th.geoAreaDeutschland;
        assertThat(geoArea.getTenant()).isNull();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Fischkuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(status().isOk());

        newsConfigMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().stream()
                        .filter(m -> "grapevine-news-config".equals(m.getLeft()))
                        .collect(Collectors.toList());
        assertThat(newsConfigMessage).hasSize(1);
        messageText = newsConfigMessage.get(0).getRight().getText();

        assertThat(messageText).containsIgnoringCase(geoArea.getId());
        assertThat(messageText).containsIgnoringCase(newsSource.getAppVariant().getAppVariantIdentifier());
    }

    @Test
    public void newsItemPublishRequest_Error_GeoArea() throws Exception {

        GeoArea geoArea = th.geoAreaMainz;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(NewsSourceFeature.class.getName())
                .configValues(
                        "{ \"enforceGeoAreaCheck\": true, \"enforcePostURLCheck\": true }")
                .build()
                .withConstantId());

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaIds(List.of(geoArea.getId(), th.subGeoArea1Tenant1.getId()))
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Hundekuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        List<Triple<String, Tenant, TeamNotificationMessage>> newsConfigMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().stream()
                        .filter(m -> "grapevine-news-config".equals(m.getLeft()))
                        .collect(Collectors.toList());
        assertThat(newsConfigMessage).hasSize(1);
        String messageText = newsConfigMessage.get(0).getRight().getText();

        assertThat(messageText).containsIgnoringCase(geoArea.getId());
        assertThat(messageText).containsIgnoringCase(newsSource.getAppVariant().getAppVariantIdentifier());

        testDefaultTeamNotificationPublisher.reset();

        geoArea = th.geoAreaDeutschland;
        assertThat(geoArea.getTenant()).isNull();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Fischkuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        newsConfigMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().stream()
                        .filter(m -> "grapevine-news-config".equals(m.getLeft()))
                        .collect(Collectors.toList());
        assertThat(newsConfigMessage).hasSize(1);
        messageText = newsConfigMessage.get(0).getRight().getText();

        assertThat(messageText).containsIgnoringCase(geoArea.getId());
        assertThat(messageText).containsIgnoringCase(newsSource.getAppVariant().getAppVariantIdentifier());
    }

    @Test
    public void newsItemPublishRequest_Warning_URL() throws Exception {

        GeoArea geoArea = th.subGeoArea1Tenant1;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String newsURL = "https://www.chefkoch.de/katzenkuchen";
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Katzenkuchen")
                                .externalId("oi-die")
                                .newsURL(newsURL)
                                .categories("Mampf")
                                .build())))
                .andExpect(status().isOk());

        List<Triple<String, Tenant, TeamNotificationMessage>> newsConfigMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().stream()
                        .filter(m -> "grapevine-news-config".equals(m.getLeft()))
                        .collect(Collectors.toList());
        assertThat(newsConfigMessage).hasSize(1);
        String messageText = newsConfigMessage.get(0).getRight().getText();

        assertThat(messageText).containsIgnoringCase(newsURL);
        assertThat(messageText).containsIgnoringCase(newsSource.getAppVariant().getAppVariantIdentifier());
    }

    @Test
    public void newsItemPublishRequest_InvalidImageURL() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();
        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        URL imageURL1 = new URL("https://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        String invalidImageURL = "telepathic://in.my.brain";

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .imageURLs(Arrays.asList(imageURL1.toString(), invalidImageURL))
                        .newsURL(newsURL)
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(invalidImageURL, ClientExceptionType.POST_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(invalidImageURL, ClientExceptionType.POST_INVALID));
    }

    @Test
    public void newsItemPublishRequest_InvalidCroppedImageURL() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();
        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        String invalidImageURL = "telepathic://in.my.brain";
        URL croppedImageURL1 = new URL("https://example.org/image1");
        MockMultipartFile imageDownload = th.createTestImageFile("thing1.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0d).relativeOffsetY(0d)
                        .relativeHeight(1d).relativeWidth(1d)
                        .build())
                .build();
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(invalidImageURL)
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(300).absoluteOffsetY(400)
                        .absoluteHeight(800).absoluteWidth(600)
                        .build())
                .build();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .newsURL(newsURL)
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(invalidImageURL, ClientExceptionType.POST_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(invalidImageURL, ClientExceptionType.POST_INVALID));
    }

    @Test
    public void newsItemPublishRequest_InvalidCropDefinitions() throws Exception {

        // original image width=1200, height=1600

        //x negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(-15).absoluteOffsetY(10)
                                .absoluteHeight(5).absoluteWidth(20)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(-0.5).relativeOffsetY(0.2)
                                .relativeHeight(0.1).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //y negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(15).absoluteOffsetY(-10)
                                .absoluteHeight(10).absoluteWidth(20)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(-0.5)
                                .relativeHeight(0.1).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //height negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(15).absoluteOffsetY(10)
                                .absoluteHeight(-2).absoluteWidth(20)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.2)
                                .relativeHeight(-1d).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //width negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(15).absoluteOffsetY(10)
                                .absoluteHeight(10).absoluteWidth(-5)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.2)
                                .relativeHeight(0.1).relativeWidth(-0.8)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //height 0
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(15).absoluteOffsetY(10)
                                .absoluteHeight(0).absoluteWidth(1200)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0d).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //width 0
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(15).absoluteOffsetY(10)
                                .absoluteHeight(10).absoluteWidth(0)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0.1).relativeWidth(0d)
                                .build())
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //x > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(1201).absoluteOffsetY(1600)
                                .absoluteHeight(1600).absoluteWidth(1200)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(1.2).relativeOffsetY(0.5)
                                .relativeHeight(0.1).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //y > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(1200).absoluteOffsetY(1601)
                                .absoluteHeight(1600).absoluteWidth(1200)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(1.5)
                                .relativeHeight(0.1).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //width > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(1200).absoluteOffsetY(1600)
                                .absoluteHeight(1600).absoluteWidth(1201)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0.1).relativeWidth(2.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //height > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(1200).absoluteOffsetY(1600)
                                .absoluteHeight(1601).absoluteWidth(1200)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(1.1).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //width + x > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(200).absoluteOffsetY(0)
                                .absoluteHeight(1600).absoluteWidth(1150)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0.1).relativeWidth(0.9)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //height + y > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(0).absoluteOffsetY(300)
                                .absoluteHeight(1350).absoluteWidth(1200)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0.6).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        // absolute and relative crop definitions in one image reference
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                                .absoluteOffsetX(0).absoluteOffsetY(300)
                                .absoluteHeight(1100).absoluteWidth(1100)
                                .build())
                        .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                                .relativeOffsetX(0.2).relativeOffsetY(0.5)
                                .relativeHeight(0.4).relativeWidth(0.2)
                                .build())
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        // image reference without any crop definition
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);
    }

    @Test
    public void newsItemPublishRequest_InvalidCropDefinitions_Legacy() throws Exception {

        // original image width=1200, height=1600

        //x negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(-15).y(10)
                        .height(5).width(20)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //y negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(15).y(-10)
                        .height(10).width(20)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //height negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(15).y(10)
                        .height(-2).width(20)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //width negative
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(15).y(10)
                        .height(10).width(-5)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //height 0
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(15).y(10)
                        .height(0).width(1200)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //width 0
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(15).y(10)
                        .height(10).width(0)
                        .build(),
                ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        //x > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(1201).y(1600)
                        .height(1600).width(1200)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //y > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(1200).y(1601)
                        .height(1600).width(1200)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //width > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(1200).y(1600)
                        .height(1600).width(1201)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //height > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(1200).y(1600)
                        .height(1601).width(1200)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //width + x > actual width
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(200).y(0)
                        .height(1600).width(1150)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);

        //height +y > actual height
        expectFileDownloadFailed(ClientCroppedImageReference.builder()
                        .url("https://example.org/image4")
                        .x(0).y(300)
                        .height(1350).width(1200)
                        .build(),
                ClientExceptionType.INVALID_IMAGE_CROP_DEFINITION);
    }

    public void expectFileDownloadFailed(ClientCroppedImageReference clientCroppedImageReference, ClientExceptionType exceptionType) throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();
        String newsURL = newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/";

        URL croppedImageURL = new URL("https://example.org/image4");
        // original image width=1200, height=1600
        MockMultipartFile imageDownload = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL, imageDownload.getBytes());

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .croppedImages(List.of(clientCroppedImageReference))
                        .newsURL(newsURL)
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(exceptionType));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(exceptionType));
    }

    @Test
    public void newsItemPublishRequest_MissingApiKey() throws Exception {

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .newsURL(th.newsSourceDigitalbach.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void newsItemPublishRequest_WrongApiKey() throws Exception {

        NewsSource newsSource1 = th.newsSourceDigitalbach;
        assertThat(newsSource1.getAppVariant()).isNotNull();
        NewsSource newsSource2 = th.newsSourceAnalogheim;
        assertThat(newsSource2.getAppVariant()).isNotNull();
        assertThat(newsSource1.getAppVariant().getApiKey1())
                .as("Test setup wrong, same api key for news sources")
                .isNotEqualTo(newsSource2.getAppVariant().getApiKey1());

        String wrongApiKey = UUID.randomUUID().toString();
        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .newsURL(newsSource1.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, wrongApiKey)
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, wrongApiKey)
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void newsItemPublishRequest_InvalidAttributes() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        //missing news url
        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .externalId("external id for published news")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("newsURL", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("newsURL", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing external id
        request = ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .newsURL(newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("externalId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("externalId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // invalid geo area id
        request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId("99")
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .newsURL(newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .externalId("42")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
    }

    @Test
    public void newsItemPublishRequest_InvalidOrganizationId() throws Exception {

        NewsSource newsSource1 = th.newsSourceDigitalbach;
        assertThat(newsSource1.getAppVariant()).isNotNull();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.geoAreaEisenberg.getId())
                        .organizationId("0000")
                        .authorName("Susanne Christman")
                        .text("Irgendein spannender Text")
                        .newsURL(newsSource1.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .externalId("42")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource1.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource1.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));

        //no organizations mapped for this news source
        NewsSource newsSource2 = th.newsSourceAnalogheim;
        assertThat(newsSource2.getAppVariant()).isNotNull();
        request.setOrganizationId(th.organizationFeuerwehr.getId());

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource2.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_AVAILABLE));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource2.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_AVAILABLE));
    }

    @Test
    public void newsItemPublishRequest_HappeningWithSameExternalId() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String externalId = "4711-2";

        ClientHappeningCreateOrUpdateByNewsSourceRequest happeningCreateRequest =
                ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Karl Napp")
                        .text("Text…")
                        .externalId(externalId)
                        .newsURL(th.newsSourceDigitalbach.getSiteUrl() + "/url1")
                        .categories("Katze")
                        .organizer("Kater Carlos")
                        .venue("Veranstaltungsort")
                        .startTime(System.currentTimeMillis())
                        .endTime(System.currentTimeMillis() + 10)
                        .allDayEvent(false)
                        .build();

        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest newsItemCreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text("Text…")
                        .externalId(externalId)
                        .newsURL(th.newsSourceDigitalbach.getSiteUrl() + "/url2")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(newsItemCreateRequest)))
                .andExpect(isException(ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED));
    }

    @Test
    public void newsItemPublishRequest_MissingRequiredAttributes() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .text("Attribute sind wichtig!")
                        .newsURL(newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .categories("Sport")
                        .externalId("external id for published news")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("authorName", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("authorName", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        request.setAuthorName("author");
        request.setGeoAreaId(null);

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("geoAreaId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("geoAreaId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        request.setGeoAreaId(th.subGeoArea1Tenant1.getId());
        request.setText(null);

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void newsItemPublishRequest_InvalidRequest() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        //valid json, bot not the right class
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content("{ \"Name\": \"Mustermann\" }"))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", "{ \"Name\": \"Mustermann\" }"))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //invalid json
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content("\"Name\": \"Mustermann"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", "\"Name\": \"Mustermann"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void newsItemPublishRequest_ExceedingMaxNumChars() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Susanne Christman")
                        .text(text)
                        .externalId("external id for published news")
                        .newsURL(newsSource.getSiteUrl() + "/fsv-digitalbach-auf-der-ueberholspur/")
                        .categories("Sport")
                        .build();

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
        mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .param("request", json(request)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void newsItemPublishRequest_republish_VerifyingPushMessage() throws Exception {

        NewsItem newsToBeRepublished = th.newsItem1;
        String newText = "NEW " + th.newsItem1.getText();
        Organization newOrganization = th.organizationGartenbau;

        //currently we can not check if the images have been updated, so we download them again
        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        MediaItem expectedImage2 = th.createMediaItem("fsv digitalbach2.png");
        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        testMediaItemService.addTestMediaItem(imageURL2, expectedImage2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, th.createTestImageFile("thing3.jpg").getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, th.createTestImageFile("thing4.jpg").getBytes());
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(300).absoluteOffsetY(400)
                        .absoluteHeight(800).absoluteWidth(600)
                        .build())
                .build();

        List<MediaItem> oldImagesToBeDeleted = newsToBeRepublished.getImages();
        assertThat(oldImagesToBeDeleted.size()).isEqualTo(2);

        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(newsToBeRepublished.getGeoAreas().iterator().next().getId())
                        .authorName(newsToBeRepublished.getAuthorName())
                        .text(newText)
                        .organizationId(newOrganization.getId())
                        .externalId(newsToBeRepublished.getExternalId())
                        .newsURL(newsToBeRepublished.getUrl())
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(newsToBeRepublished.getCategories())
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .build();

        NewsSource newsSource = newsToBeRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();
        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(recreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getTenant()).isEqualTo(newsToBeRepublished.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).isEqualTo(newsToBeRepublished.getGeoAreas());
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(newsToBeRepublished.getAuthorName());
        assertThat(publishedNewsItem.getText()).isEqualTo(newText);
        assertThat(publishedNewsItem.getOrganization()).isEqualTo(newOrganization);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(newsToBeRepublished.getExternalId());
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsToBeRepublished.getUrl());
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        //check that the created date is not changed, but the last modified is
        assertThat(publishedNewsItem.getCreated()).isEqualTo(newsToBeRepublished.getCreated());
        assertThat(publishedNewsItem.getLastModified()).isNotEqualTo(newsToBeRepublished.getLastModified());

        //currently the images are downloaded again
        assertThat(publishedNewsItem.getImages()).contains(expectedImage1, expectedImage2);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(4);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(newsToBeRepublished.getCategories());

        assertThat(th.mediaItemRepository.findById(oldImagesToBeDeleted.get(0).getId())).isEmpty();
        assertThat(th.mediaItemRepository.findById(oldImagesToBeDeleted.get(1).getId())).isEmpty();

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(publishedNewsItem.getGeoAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPostId()).isEqualTo(republishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(republishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequestWithImageUpload_republish_VerifyingPushMessage() throws Exception {

        NewsItem newsToBeRepublished = th.newsItem1;
        String newText = "NEW " + th.newsItem1.getText();

        //currently we can not check if the images have been updated, so we download them again
        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        MediaItem expectedImage2 = th.createMediaItem("fsv digitalbach2.png");
        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        testMediaItemService.addTestMediaItem(imageURL2, expectedImage2);
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(300).absoluteOffsetY(400)
                        .absoluteHeight(800).absoluteWidth(600)
                        .build())
                .build();

        List<MediaItem> oldImagesToBeDeleted = newsToBeRepublished.getImages();
        assertThat(oldImagesToBeDeleted).hasSize(2);

        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(newsToBeRepublished.getGeoAreas().iterator().next().getId())
                        .authorName(newsToBeRepublished.getAuthorName())
                        .text(newText)
                        .externalId(newsToBeRepublished.getExternalId())
                        .newsURL(newsToBeRepublished.getUrl())
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(newsToBeRepublished.getCategories())
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .build();

        NewsSource newsSource = newsToBeRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();
        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation =
                toObject(mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                                        .file(imageUpload)
                                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                        .contentType(contentType)
                                        .param("request", json(recreateRequest)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getTenant()).isEqualTo(newsToBeRepublished.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).isEqualTo(newsToBeRepublished.getGeoAreas());
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(newsToBeRepublished.getAuthorName());
        assertThat(publishedNewsItem.getText()).isEqualTo(newText);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(newsToBeRepublished.getExternalId());
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsToBeRepublished.getUrl());
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        //check that the created date is not changed, but the last modified is
        assertThat(publishedNewsItem.getCreated()).isEqualTo(newsToBeRepublished.getCreated());
        assertThat(publishedNewsItem.getLastModified()).isNotEqualTo(newsToBeRepublished.getLastModified());

        assertThat(publishedNewsItem.getImages())
                //the first image can not be checked
                .contains(publishedNewsItem.getImages().get(0), expectedImage1, expectedImage2);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(5);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(newsToBeRepublished.getCategories());

        waitForEventProcessing();

        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(0).getId())).isFalse();
        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(1).getId())).isFalse();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(publishedNewsItem.getGeoAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPostId()).isEqualTo(republishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(republishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_republishDifferentGeoArea_VerifyingPushMessage() throws Exception {

        NewsItem newsToBeRepublished = th.newsItem1;
        GeoArea oldGeoArea = newsToBeRepublished.getGeoAreas().iterator().next();
        GeoArea newGeoArea = th.geoAreaKaiserslautern;
        assertThat(oldGeoArea).isNotEqualTo(newGeoArea);
        Person creator = newsToBeRepublished.getCreator();
        assertThat(creator).isNotNull();
        String newText = "NEW " + th.newsItem1.getText();

        //currently we can not check if the images have been updated, so we download them again
        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        MediaItem expectedImage2 = th.createMediaItem("fsv digitalbach2.png");
        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        testMediaItemService.addTestMediaItem(imageURL2, expectedImage2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0.25).relativeOffsetY(0.25)
                        .relativeHeight(0.5).relativeWidth(0.5)
                        .build())
                .build();

        List<MediaItem> oldImagesToBeDeleted = newsToBeRepublished.getImages();
        assertThat(oldImagesToBeDeleted).hasSize(2);

        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(newGeoArea.getId())
                        .authorName(newsToBeRepublished.getAuthorName())
                        .text(newText)
                        .externalId(newsToBeRepublished.getExternalId())
                        .newsURL(newsToBeRepublished.getUrl())
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(newsToBeRepublished.getCategories())
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .build();

        NewsSource newsSource = newsToBeRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();
        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(recreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getTenant()).isEqualTo(newsToBeRepublished.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).containsOnly(newGeoArea);
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(newsToBeRepublished.getAuthorName());
        assertThat(publishedNewsItem.getText()).isEqualTo(newText);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(newsToBeRepublished.getExternalId());
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsToBeRepublished.getUrl());
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        //check that the created date is not changed, but the last modified is
        assertThat(publishedNewsItem.getCreated()).isEqualTo(newsToBeRepublished.getCreated());
        assertThat(publishedNewsItem.getLastModified()).isNotEqualTo(newsToBeRepublished.getLastModified());

        //currently the images are downloaded again
        assertThat(publishedNewsItem.getImages()).contains(expectedImage1, expectedImage2);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(4);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(newsToBeRepublished.getCategories());

        waitForEventProcessing();

        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(0).getId())).isFalse();
        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(1).getId())).isFalse();

        ClientPostDeleteConfirmation pushedEventOldGeoArea =
                testClientPushService.getPushedEventToGeoAreasSilent(
                        Set.of(oldGeoArea),
                        ClientPostDeleteConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);
        assertThat(pushedEventOldGeoArea.getPostId()).isEqualTo(republishConfirmation.getPostId());

        ClientPostCreateConfirmation pushedEventNewGeoArea =
                testClientPushService.getPushedEventToGeoAreasLoud(
                        creator,
                        Set.of(newGeoArea),
                        false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);
        assertThat(pushedEventNewGeoArea.getPostId()).isEqualTo(republishConfirmation.getPostId());
        assertThat(pushedEventNewGeoArea.getPost().getNewsItem().getId()).isEqualTo(republishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void newsItemPublishRequest_republish_DeletedNewsItem() throws Exception {

        NewsItem newsToBeRepublished = th.newsItem1;

        newsToBeRepublished.setDeleted(true);
        th.postRepository.save(newsToBeRepublished);

        String newText = "ZOMBIE";

        //currently we can not check if the images have been updated, so we download them again
        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        MediaItem expectedImage2 = th.createMediaItem("fsv digitalbach2.png");
        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        testMediaItemService.addTestMediaItem(imageURL2, expectedImage2);
        MockMultipartFile imageUpload = th.createTestImageFile("image", "thing3.jpg");

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0.25).relativeOffsetY(0.25)
                        .relativeHeight(0.5).relativeWidth(0.5)
                        .build())
                .build();

        List<MediaItem> oldImagesToBeDeleted = newsToBeRepublished.getImages();
        assertThat(oldImagesToBeDeleted.size()).isEqualTo(2);

        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(newsToBeRepublished.getGeoAreas().iterator().next().getId())
                        .authorName(newsToBeRepublished.getAuthorName())
                        .text(newText)
                        .externalId(newsToBeRepublished.getExternalId())
                        .newsURL(newsToBeRepublished.getUrl())
                        .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                        .categories(newsToBeRepublished.getCategories())
                        .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                        .build();

        NewsSource newsSource = newsToBeRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();
        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation =
                toObject(mockMvc.perform(multipart("/grapevine/event/newsItemPublishRequestImageUpload")
                                        .file(imageUpload)
                                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                        .contentType(contentType)
                                        .param("request", json(recreateRequest)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation.getPostId()).get();

        assertThat(publishedNewsItem.getDeletionTime()).isNull();
        assertThat(publishedNewsItem.isDeleted()).isFalse();
        assertThat(publishedNewsItem.getTenant()).isEqualTo(newsToBeRepublished.getTenant());
        assertThat(publishedNewsItem.getGeoAreas()).isEqualTo(newsToBeRepublished.getGeoAreas());
        assertThat(publishedNewsItem.getAuthorName()).isEqualTo(newsToBeRepublished.getAuthorName());
        assertThat(publishedNewsItem.getText()).isEqualTo(newText);
        assertThat(publishedNewsItem.getExternalId()).isEqualTo(newsToBeRepublished.getExternalId());
        assertThat(publishedNewsItem.getUrl()).isEqualTo(newsToBeRepublished.getUrl());
        assertThat(publishedNewsItem.getNewsSource()).isEqualTo(newsSource);
        //check that the created date is not changed, but the last modified is
        assertThat(publishedNewsItem.getCreated()).isEqualTo(newsToBeRepublished.getCreated());
        assertThat(publishedNewsItem.getLastModified()).isNotEqualTo(newsToBeRepublished.getLastModified());

        //currently the images are downloaded again
        assertThat(publishedNewsItem.getImages())
                .contains(publishedNewsItem.getImages().get(0), expectedImage1, expectedImage2);
        MediaItem image3 = publishedNewsItem.getImages().get(3);
        assertThat(image3.getRelativeImageCropDefinition()).isNotNull();
        assertThat(image3.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.0);
        assertThat(image3.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.0);
        assertThat(image3.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1.0);
        assertThat(image3.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1.0);
        MediaItem image4 = publishedNewsItem.getImages().get(4);
        assertThat(image4.getRelativeImageCropDefinition()).isNotNull();
        assertThat(image4.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(image4.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.25);
        assertThat(image4.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5);
        assertThat(image4.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5);
        assertThat(publishedNewsItem.getImages().size()).isEqualTo(5);
        assertThat(publishedNewsItem.getCategories()).isEqualTo(newsToBeRepublished.getCategories());

        waitForEventProcessing();

        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(0).getId())).isFalse();
        assertThat(th.mediaItemRepository.existsById(oldImagesToBeDeleted.get(1).getId())).isFalse();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(publishedNewsItem.getGeoAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPostId()).isEqualTo(republishConfirmation.getPostId());
        assertThat(pushedEvent.getPost().getNewsItem().getId()).isEqualTo(republishConfirmation.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void deleteAndRepublishNewsItem() throws Exception {

        NewsItem newsToBeDeletedAndRepublished = th.newsItem1;
        NewsSource newsSource = newsToBeDeletedAndRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();
        String apiKey = newsSource.getAppVariant().getApiKey1();
        ClientPostDeleteRequest postDeleteRequest = ClientPostDeleteRequest.builder()
                .postId(newsToBeDeletedAndRepublished.getId())
                .build();

        MvcResult deleteRequest = mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                        .header(HEADER_NAME_API_KEY, apiKey)
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(postDeleteRequest.getPostId()))
                .andReturn();

        ClientPostDeleteConfirmation newsItemDeleteConfirmation = toObject(deleteRequest.getResponse(),
                ClientPostDeleteConfirmation.class);

        NewsItem deletedNewsItem =
                (NewsItem) th.externalPostRepository.findById(newsItemDeleteConfirmation.getPostId()).get();
        assertThat(deletedNewsItem.isDeleted()).isTrue();

        // Get the newsItem - it should return POST_NOT_FOUND
        mockMvc.perform(get("/grapevine/post/{postId}", newsItemDeleteConfirmation.getPostId())
                        .headers(authHeadersFor(th.personLaraSchaefer, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //we want to have this image downloaded if we re-publish
        MediaItem expectedImage1 = th.createMediaItem("fsv digitalbach1.png");
        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest =
                createNewsItemRePublishRequest(newsToBeDeletedAndRepublished, expectedImage1, "thing3.jpg");

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, apiKey)
                        .contentType(contentType)
                        .content(json(recreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation.getPostId()).get();
        //we want to have this image downloaded if we re-publish
        MediaItem expectedImage2 = th.createMediaItem("fsv digitalbach2.png");
        ClientNewsItemCreateOrUpdateByNewsSourceRequest recreateRequest2 =
                createNewsItemRePublishRequest(publishedNewsItem, expectedImage2, "thing4.jpg");

        //try to publish it again
        MvcResult publishRequestResult2 = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, apiKey)
                        .contentType(contentType)
                        .content(json(recreateRequest2)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation republishConfirmation2 =
                toObject(publishRequestResult2.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        NewsItem publishedNewsItem2 =
                (NewsItem) th.externalPostRepository.findById(republishConfirmation2.getPostId()).get();

        //since no data has changed, the news item should have the same values as in the original newsItem
        assertThat(publishedNewsItem2.getTenant()).isEqualTo(newsToBeDeletedAndRepublished.getTenant());
        assertThat(publishedNewsItem2.getGeoAreas()).isEqualTo(newsToBeDeletedAndRepublished.getGeoAreas());
        assertThat(publishedNewsItem2.getAuthorName()).isEqualTo(newsToBeDeletedAndRepublished.getAuthorName());
        assertThat(publishedNewsItem2.getText()).isEqualTo(newsToBeDeletedAndRepublished.getText());
        assertThat(publishedNewsItem2.getExternalId()).isEqualTo(newsToBeDeletedAndRepublished.getExternalId());
        assertThat(publishedNewsItem2.getUrl()).isEqualTo(newsToBeDeletedAndRepublished.getUrl());
        assertThat(publishedNewsItem2.getNewsSource()).isEqualTo(newsSource);
        assertThat(publishedNewsItem2.getImages()).contains(expectedImage2);
        assertThat(publishedNewsItem2.getImages().size()).isEqualTo(2);
        assertThat(publishedNewsItem2.getCategories()).isEqualTo(newsToBeDeletedAndRepublished.getCategories());
        assertThat(publishedNewsItem2.isDeleted()).isFalse();
    }

    @Test
    public void newsItemPublishRequest_PublishAndUnpublishTime() throws Exception {

        //test of the actual publishing is tested at the post publish worker tests

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        ClientNewsItemCreateOrUpdateByNewsSourceRequest request =
                ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                        .publishImmediately(false)
                        .geoAreaId(th.subGeoArea1Tenant1.getId())
                        .authorName("Zeit Late")
                        .text("Late Rhabarber")
                        .newsURL(newsSource.getSiteUrl() + "/later.html")
                        .categories("Zeit")
                        .publishImmediately(false)
                        .build();
        long now = timeServiceMock.currentTimeMillisUTC();

        //positive time
        request.setDesiredPublishTime(now + 10000);
        request.setDesiredUnpublishTime(now + 30000);

        NewsItem createdNewsItem = callNewsItemCreateRequest(request);

        assertThat(createdNewsItem.getDesiredPublishTime()).isEqualTo(request.getDesiredPublishTime());
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(request.getDesiredUnpublishTime());

        //null time
        request.setDesiredPublishTime(null);
        request.setDesiredUnpublishTime(666L);

        createdNewsItem = callNewsItemCreateRequest(request);

        assertThat(createdNewsItem.getDesiredPublishTime()).isEqualTo(request.getDesiredPublishTime());
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(request.getDesiredUnpublishTime());

        //null time
        request.setDesiredPublishTime(42L);
        request.setDesiredUnpublishTime(null);

        createdNewsItem = callNewsItemCreateRequest(request);

        assertThat(createdNewsItem.getDesiredPublishTime()).isEqualTo(request.getDesiredPublishTime());
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(request.getDesiredUnpublishTime());

        //negative time
        request.setDesiredPublishTime(-1L);
        request.setDesiredUnpublishTime(0L);

        createdNewsItem = callNewsItemCreateRequest(request);

        assertThat(createdNewsItem.getDesiredPublishTime()).isEqualTo(request.getDesiredPublishTime());
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(request.getDesiredUnpublishTime());

        //unpublish before publish
        request.setDesiredPublishTime(now + 42);
        request.setDesiredUnpublishTime(now + 7);

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, "publish time"));
    }

    @Test
    public void newsItemPublishRequest_GeoAreaIds() throws Exception {

        List<String> geoAreaIds = Stream.of(th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId())
                .collect(Collectors.toList());

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        // no id at all
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Hundekuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // single id and list of ids
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.geoAreaDorf1InEisenberg.getId())
                                .geoAreaIds(geoAreaIds)
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Hundekuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // list of ids with duplicates
        geoAreaIds.add(th.geoAreaEisenberg.getId());
        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaIds(geoAreaIds)
                                .authorName("Fieser Fritz")
                                .text("Neues Rezept für Hundekuchen")
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/mein-rezept")
                                .categories("Mampf")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    private NewsItem callNewsItemCreateRequest(ClientNewsItemCreateOrUpdateByNewsSourceRequest request) throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        request.setExternalId(UUID.randomUUID().toString());

        String postId = toObject(mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                                .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                                .contentType(contentType)
                                .content(json(request)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse(),
                ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class).getPostId();

        return (NewsItem) th.externalPostRepository.findById(postId).get();
    }

    protected ClientNewsItemCreateOrUpdateByNewsSourceRequest createNewsItemRePublishRequest(
            NewsItem newToBeRePublished, MediaItem mediaItemToBeDownloaded, String croppedMediaItemToBeDownloaded)
            throws IOException {

        //this image url will be used to download the image
        URL imageURL1 = new URL("https://example.org/image" + mediaItemToBeDownloaded.getId());
        //instead of downloading it, we directly return the media item we want
        testMediaItemService.addTestMediaItem(imageURL1, mediaItemToBeDownloaded);

        URL croppedImageURL1 = new URL("https://example.org/image3" + croppedMediaItemToBeDownloaded);
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(0).absoluteOffsetY(0)
                        .absoluteHeight(900).absoluteWidth(1200)
                        .build())
                .build();

        return ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                .geoAreaId(newToBeRePublished.getGeoAreas().iterator().next().getId())
                .authorName(newToBeRePublished.getAuthorName())
                .text(newToBeRePublished.getText())
                .externalId(newToBeRePublished.getExternalId())
                .newsURL(newToBeRePublished.getUrl())
                .categories(newToBeRePublished.getCategories())
                .imageURLs(Collections.singletonList(imageURL1.toString()))
                .croppedImages(List.of(clientCroppedImageReference1))
                .build();
    }

}
