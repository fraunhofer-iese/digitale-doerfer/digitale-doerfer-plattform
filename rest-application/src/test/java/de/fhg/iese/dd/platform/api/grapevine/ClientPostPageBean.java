/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine;

import java.util.List;

import de.fhg.iese.dd.platform.api.PageBean;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;

public class ClientPostPageBean extends PageBean<ClientPost> {

    /**
     * Simple factory method that creates a ClientPostPageBean with all the given posts on one page
     *
     * @param posts
     * @return
     */
    public static ClientPostPageBean from(List<ClientPost> posts, int size) {

        final ClientPostPageBean pageBean = new ClientPostPageBean();
        pageBean.setContent(posts);
        pageBean.setEmpty(posts.isEmpty());
        pageBean.setFirst(true);
        pageBean.setLast(true);
        pageBean.setNumber(0);
        pageBean.setSize(size);
        pageBean.setTotalElements(posts.size());
        pageBean.setNumberOfElements(posts.size());
        pageBean.setTotalPages(1);

        return pageBean;
    }

}
