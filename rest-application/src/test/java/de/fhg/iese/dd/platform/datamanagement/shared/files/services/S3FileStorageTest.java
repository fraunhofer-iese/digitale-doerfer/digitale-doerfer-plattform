/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Stefan Schweitzer, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.ServerSideEncryption;

@ActiveProfiles({"test", "test-aws", "integration-test", "S3FileStorageTest"})
@ExtendWith(AssumeIntegrationTestExtension.class)
public class S3FileStorageTest extends BaseServiceTest {

    private static final byte[] TEST_IMAGE_DATA = {0, 1, 2, 3, 4, 5};

    private static final String CONTENT_TYPE = "image/jpeg";

    private static final String LOCAL_TEST_IMAGE_FILENAME = "testImage.jpg";

    private static final String REMOTE_TEST_IMAGE_FILENAME = "test/test.jpg";

    private static final String ROOT_DIRECTORY = "";
    private static final String TEST_DIRECTORY = "myimages/";
    
    // Maximum allowed deviation timespan between local file creation time and time received from s3
    // (processing time, time difference between local and s3, ...)
    private static final Duration MAX_ALLOWED_AGE_DEVIATION = Duration.ofSeconds(5);

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private AWSConfig awsConfig;
    @Autowired
    private FileStorageConfig fileStorageConfig;
    @Autowired
    private S3FileStorage s3FileStorage;

    private S3Client s3client;

    @Override
    public void createEntities() throws Exception {

        s3client = S3Client.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();
        CompletableFuture.allOf(
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getStaticContentBucket())),
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getTrashBucket())),
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getDefaultDataBucket())))
                .get(5, TimeUnit.SECONDS);

        uploadFileToBucket(awsConfig.getS3().getDefaultDataBucket());
    }

    @Override
    public void tearDown() throws Exception {

        CompletableFuture.allOf(
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getStaticContentBucket())),
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getTrashBucket())),
                        CompletableFuture.runAsync(() -> deleteAllFilesInBucket(awsConfig.getS3().getDefaultDataBucket())))
                .get(5, TimeUnit.SECONDS);
    }

    @Test
    public void saveFile() throws Exception {

        final String internalFilename = TEST_DIRECTORY + "test_create_file.jpg";

        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);

        s3FileStorage.saveFile(TEST_IMAGE_DATA, CONTENT_TYPE, internalFilename);

        assertFileIsOnlyFileInStorage(internalFilename);
        assertFileHasCorrectContentTypeAndEncryption(internalFilename, "image/jpeg");
        assertValidExternalName(internalFilename);
        assertExternalContentIsTestImageData(s3FileStorage.getExternalUrl(internalFilename));
        assertThat(s3FileStorage.getAge(internalFilename)).isLessThan(MAX_ALLOWED_AGE_DEVIATION);

        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void copyFileFromDefault() throws IOException {

        final String internalFilename = TEST_DIRECTORY + "test_file_from_default.jpg";

        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));
        assertFileIsOnlyFileInStorage(internalFilename);
        assertFileHasCorrectContentTypeAndEncryption(internalFilename, "image/jpeg");
        assertValidExternalName(internalFilename);
        assertExternalContentIsLocalTestImage(s3FileStorage.getExternalUrl(internalFilename));
        assertThat(s3FileStorage.getAge(internalFilename)).isLessThan(MAX_ALLOWED_AGE_DEVIATION);

        s3FileStorage.deleteFile(internalFilename);
        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void copyFile() throws IOException {

        final String internalFilenameSource = TEST_DIRECTORY + "test_file_to_be_copied.jpg";
        final String internalFilenameTarget = TEST_DIRECTORY + "test_file_copied.jpg";
        s3FileStorage.saveFile(TEST_IMAGE_DATA, CONTENT_TYPE, internalFilenameSource);
        assertExternalContentIsTestImageData(s3FileStorage.getExternalUrl(internalFilenameSource));

        assertTrue(s3FileStorage.fileExists(internalFilenameSource));

        s3FileStorage.copyFile(internalFilenameSource, internalFilenameTarget);
        assertTrue(s3FileStorage.fileExists(internalFilenameSource));
        assertTrue(s3FileStorage.fileExists(internalFilenameTarget));

        assertFileHasCorrectContentTypeAndEncryption(internalFilenameTarget, "image/jpeg");
        assertValidExternalName(internalFilenameTarget);
        assertExternalContentIsTestImageData(s3FileStorage.getExternalUrl(internalFilenameTarget));
        assertThat(s3FileStorage.getAge(internalFilenameSource)).isLessThan(MAX_ALLOWED_AGE_DEVIATION);

        s3FileStorage.deleteFile(internalFilenameSource);
        s3FileStorage.deleteFile(internalFilenameTarget);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilenameSource);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilenameTarget);
    }

    @Test
    public void moveFileToTrash() throws IOException {

        final String internalFilename = TEST_DIRECTORY + "test_move_file_to_trash.png";

        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);

        s3FileStorage.saveFile(TEST_IMAGE_DATA, CONTENT_TYPE, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsTestImageData(s3FileStorage.getExternalUrl(internalFilename));
        assertThat(s3FileStorage.getAge(internalFilename)).isLessThan(MAX_ALLOWED_AGE_DEVIATION);

        s3FileStorage.moveToTrash(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void readFromDefaultFile() throws IOException {

        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));

        try (final InputStream fileStorageStream = s3FileStorage.getFileFromDefault(REMOTE_TEST_IMAGE_FILENAME)) {
            assertExternalContentIsLocalTestImage(fileStorageStream);
        }

        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));
    }

    @Test
    public void replaceFile() throws IOException {

        final String internalFilename = TEST_DIRECTORY + "test_replace_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsLocalTestImage(s3FileStorage.getExternalUrl(internalFilename));

        s3FileStorage.saveFile(TEST_IMAGE_DATA, CONTENT_TYPE, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsTestImageData(s3FileStorage.getExternalUrl(internalFilename));

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsLocalTestImage(s3FileStorage.getExternalUrl(internalFilename));

        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void getFile() throws IOException {

        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsLocalTestImage(s3FileStorage.getExternalUrl(internalFilename));

        s3FileStorage.getFile(internalFilename);

        assertValidExternalName(internalFilename);
        assertThat(s3FileStorage.getAge(internalFilename)).isLessThan(MAX_ALLOWED_AGE_DEVIATION);

        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void fileExists() throws IOException {

        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);
        assertExternalContentIsLocalTestImage(s3FileStorage.getExternalUrl(internalFilename));

        assertFileIsOnlyFileInStorage(internalFilename);

        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void findAll() {
        final String internalFilename1 = TEST_DIRECTORY + "test_file1.jpg";
        final String internalFilename2 = TEST_DIRECTORY + "test_file2.jpg";
        final String internalFilename3 = TEST_DIRECTORY + "test_file3.jpg";

        // create 3 files in test directory
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename1);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename2);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename3);

        List<String> files = s3FileStorage.findAll(TEST_DIRECTORY);

        // check that all 3 files are retrieved
        assertEquals(files.size(), 3);

        // check key names of files
        assertThat(files).containsExactlyInAnyOrder(internalFilename1, internalFilename2, internalFilename3);

        s3FileStorage.deleteFile(internalFilename1);
        s3FileStorage.deleteFile(internalFilename2);
        s3FileStorage.deleteFile(internalFilename3);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename1);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename2);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename3);
    }

    @Test
    public void fileExistsDefault() {
        assertTrue(s3FileStorage.fileExistsDefault(REMOTE_TEST_IMAGE_FILENAME));
    }

    @Test
    public void deleteFile() {
        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);

        assertFileIsOnlyFileInStorage(internalFilename);
        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void getExternalURL() {
        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);

        String fileURL = s3FileStorage.getExternalUrl(internalFilename);
        assertEquals(fileURL, fileStorageConfig.getExternalBaseUrl() + internalFilename);
        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void getInternalFileName() {
        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);

        String fileName = s3FileStorage.getInternalFileName(internalFilename);

        assertEquals(fileName, internalFilename);
        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }
    
    @Test
    public void getAge() {
        final String internalFilename = TEST_DIRECTORY + "test_file.jpg";

        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename);
        assertFileIsOnlyFileInStorage(internalFilename);

        Duration age = s3FileStorage.getAge(internalFilename);

        assertThat(age.toMillis()).isLessThan(Instant.now().toEpochMilli());
        s3FileStorage.deleteFile(internalFilename);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename);
    }

    @Test
    public void createZipFile() {
        String internalZipFileName = "testZip";
        List<String> internalFileNames = new ArrayList<>();
        final String internalFilename1 = TEST_DIRECTORY + "test_file1.jpg";
        final String internalFilename2 = TEST_DIRECTORY + "test_file2.jpg";
        final String internalFilename3 = TEST_DIRECTORY + "test_file3.jpg";

        // create 3 files in test directory
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename1);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename2);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename3);

        internalFileNames.add(internalFilename1);
        internalFileNames.add(internalFilename2);
        internalFileNames.add(internalFilename3);

        String zipResponse = s3FileStorage.createZipFile(internalZipFileName, internalFileNames, null);
        final String actualInternalZipFileName = s3FileStorage.getInternalFileName(zipResponse);

        assertThat(actualInternalZipFileName).isEqualTo(internalZipFileName);
        assertThat(zipResponse).contains(internalZipFileName);

        try (final InputStream inputStream = s3FileStorage.getFile(internalZipFileName)) {
            List<String> fileNames = new ArrayList<>();
            try (ZipInputStream zis = new ZipInputStream(inputStream)) {
                ZipEntry zipEntry = zis.getNextEntry();
                while (zipEntry != null) {
                    fileNames.add(zipEntry.getName());
                    zipEntry = zis.getNextEntry();
                }
            }
            assertThat(fileNames)
                    .containsExactlyInAnyOrder(internalFilename1, internalFilename2, internalFilename3);

        } catch (MalformedURLException e) {
            throw new RuntimeException("Zip file URL is invalid", e);
        } catch (IOException e) {
            throw new RuntimeException("Could not write file", e);
        }

        s3FileStorage.deleteFile(actualInternalZipFileName);
        s3FileStorage.deleteFile(internalFilename1);
        s3FileStorage.deleteFile(internalFilename2);
        s3FileStorage.deleteFile(internalFilename3);
        assertFileDoesNotExistAndStorageIsEmpty(actualInternalZipFileName);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename1);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename2);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename3);
    }

    @Test
    public void createZipFileWithFlattenedPaths() {

        String internalZipFileName = "testZip";
        List<String> internalFileNames = new ArrayList<>();
        final String internalFilename1 = TEST_DIRECTORY + "folder1/folder2/test_file1.jpg";
        final String zipInternalFilename1 = TEST_DIRECTORY + "test_file1.jpg";
        final String internalFilename2 = TEST_DIRECTORY + "folder1/test_file2.jpg";
        final String internalFilename3 = TEST_DIRECTORY + "folder3/test_file3.jpg";
        final String zipInternalFilename3 = TEST_DIRECTORY + "test_file3.jpg";
        final String internalFilename4 = TEST_DIRECTORY + "folder4/test_file4.jpg";

        // create 4 files in test directory
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename1);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename2);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename3);
        s3FileStorage.copyFileFromDefault(REMOTE_TEST_IMAGE_FILENAME, internalFilename4);

        internalFileNames.add(internalFilename1);
        internalFileNames.add(internalFilename2);
        internalFileNames.add(internalFilename3);
        internalFileNames.add(internalFilename4);

        List<String> pathsToFlatten = new ArrayList<>();
        pathsToFlatten.add("folder1/folder2/");
        pathsToFlatten.add("folder3/");
        pathsToFlatten.add("folder5/");

        String zipResponse = s3FileStorage.createZipFile(internalZipFileName, internalFileNames, pathsToFlatten);
        final String actualInternalZipFileName = s3FileStorage.getInternalFileName(zipResponse);

        assertThat(actualInternalZipFileName).isEqualTo(internalZipFileName);
        assertThat(zipResponse).contains(internalZipFileName);

        try (final InputStream inputStream = s3FileStorage.getFile(internalZipFileName)) {
            List<String> fileNames = new ArrayList<>();
            try (ZipInputStream zis = new ZipInputStream(inputStream)) {
                ZipEntry zipEntry = zis.getNextEntry();
                while (zipEntry != null) {
                    fileNames.add(zipEntry.getName());
                    zipEntry = zis.getNextEntry();
                }
            }
            assertThat(fileNames).containsExactlyInAnyOrder(zipInternalFilename1, internalFilename2,
                    zipInternalFilename3, internalFilename4);

        } catch (MalformedURLException e) {
            throw new RuntimeException("Zip file URL is invalid", e);
        } catch (IOException e) {
            throw new RuntimeException("Could not write file", e);
        }

        s3FileStorage.deleteFile(actualInternalZipFileName);
        s3FileStorage.deleteFile(internalFilename1);
        s3FileStorage.deleteFile(internalFilename2);
        s3FileStorage.deleteFile(internalFilename3);
        s3FileStorage.deleteFile(internalFilename4);
        assertFileDoesNotExistAndStorageIsEmpty(actualInternalZipFileName);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename1);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename2);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename3);
        assertFileDoesNotExistAndStorageIsEmpty(internalFilename4);
    }

    private void uploadFileToBucket(final String bucketName) throws IOException {

        byte[] imageData = th.loadTestResource(S3FileStorageTest.LOCAL_TEST_IMAGE_FILENAME);

        s3client.putObject(PutObjectRequest.builder()
                        .bucket(bucketName)
                        .key(S3FileStorageTest.REMOTE_TEST_IMAGE_FILENAME)
                        .serverSideEncryption(ServerSideEncryption.AES256)
                        .contentLength((long) imageData.length)
                        .contentType("image/jpeg")
                        .build(),
                RequestBody.fromBytes(imageData));
    }

    private void assertFileIsOnlyFileInStorage(final String internalFilename) {
        assertTrue(s3FileStorage.fileExists(internalFilename));

        assertEquals(Collections.singletonList(internalFilename), s3FileStorage.findAll(ROOT_DIRECTORY));
        assertEquals(Collections.singletonList(internalFilename), s3FileStorage.findAll(TEST_DIRECTORY));
    }

    private void assertFileDoesNotExistAndStorageIsEmpty(final String internalFilename) {
        assertFalse(s3FileStorage.fileExists(internalFilename));

        assertEquals(Collections.emptyList(), s3FileStorage.findAll(ROOT_DIRECTORY));
        assertEquals(Collections.emptyList(), s3FileStorage.findAll(TEST_DIRECTORY));
    }

    private void assertFileHasCorrectContentTypeAndEncryption(final String internalFilename, String contentType) {

        HeadObjectResponse response = s3client.headObject(HeadObjectRequest.builder()
                .bucket(awsConfig.getS3().getStaticContentBucket())
                .key(internalFilename)
                .build());

        assertThat(response.contentType()).isEqualTo(contentType);
        assertThat(response.serverSideEncryption()).isEqualTo(ServerSideEncryption.AES256);
    }

    private void assertValidExternalName(final String internalFilename) {

        final String externalBaseUrl = s3FileStorage.getExternalBaseUrl();
        assertFalse(externalBaseUrl.isEmpty());
        assertEquals(fileStorageConfig.getExternalBaseUrl(), externalBaseUrl);

        final String externalUrl = s3FileStorage.getExternalUrl(internalFilename);
        assertTrue(externalUrl.startsWith(externalBaseUrl));

        assertEquals(internalFilename, s3FileStorage.getInternalFileName(externalUrl));
    }

    private void assertExternalContentIsLocalTestImage(final String externalUrl) throws IOException {
        assertExternalDownloadedDataAndStreamEquals(externalUrl, th.loadTestImageFileStream(LOCAL_TEST_IMAGE_FILENAME));
    }

    private void assertExternalContentIsTestImageData(final String externalUrl) throws IOException {
        try (final InputStream dataInputStream = new ByteArrayInputStream(S3FileStorageTest.TEST_IMAGE_DATA)) {
            assertExternalDownloadedDataAndStreamEquals(externalUrl, dataInputStream);
        }
    }

    private void assertExternalContentIsLocalTestImage(final InputStream externalInputStream) throws IOException {
        assertNotNull(externalInputStream);
        try (final InputStream localInputStream = th.loadTestImageFileStream(LOCAL_TEST_IMAGE_FILENAME)) {
            assertTrue(IOUtils.contentEquals(externalInputStream, localInputStream));
        }
    }

    private void assertExternalDownloadedDataAndStreamEquals(final String externalUrl,
            final InputStream localInputStream) throws IOException {

        String key = StringUtils.removeStart(externalUrl, fileStorageConfig.getExternalBaseUrl());

        assertNotNull(localInputStream);
        InputStream externalInputStream = s3client.getObjectAsBytes(GetObjectRequest.builder()
                        .bucket(awsConfig.getS3().getStaticContentBucket())
                        .key(key)
                        .build())
                .asInputStream();
        assertTrue(IOUtils.contentEquals(externalInputStream, localInputStream));
    }

    /**
     * Does only delete the first 150 files
     */
    private void deleteAllFilesInBucket(String bucketName) {
        log.info("Deleting all files (max 150) in bucket {}", bucketName);

        s3client.listObjects(ListObjectsRequest.builder()
                        .bucket(bucketName)
                        .maxKeys(150)
                        .build())
                .contents()
                .parallelStream()
                .forEach(ol -> s3client.deleteObject(DeleteObjectRequest.builder()
                        .bucket(bucketName)
                        .key(ol.key())
                        .build()));
    }

}
