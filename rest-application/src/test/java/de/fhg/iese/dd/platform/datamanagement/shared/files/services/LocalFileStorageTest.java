/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.FileSystemUtils;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import lombok.extern.log4j.Log4j2;

@ActiveProfiles({"test"})
@Log4j2
public class LocalFileStorageTest extends BaseServiceTest {

    private static final String TEST_FILE_NAME = "test.txt";
    private static final String TEST_FILE_NAME_TWO = "testTwo.txt";
    private static final byte[] TEST_FILE_CONTENT = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    private static String testFolderPath;
    private static final Path tempDir = getLocalTestFolder();
    private final Path defaultStorageLocation = tempDir.resolve("dd-platform").resolve("dd-default-data");
    private final Path staticStorageLocation = tempDir.resolve("dd-platform").resolve("dd-static-data");
    private final Path trashStorageLocation = tempDir.resolve("dd-platform").resolve("dd-trash-data");

    @Autowired
    private FileStorageConfig fileStorageConfig;

    private LocalFileStorage localFileStorage;

    private static Path getLocalTestFolder() {
        testFolderPath = StringUtils.appendIfMissing(System.getProperty("java.io.tmpdir"), File.separator) +
                UUID.randomUUID();
        return Paths.get(testFolderPath);
    }

    @AfterAll
    public static void deleteTestFolder() {
        try {
            FileSystemUtils.deleteRecursively(tempDir);
        } catch (IOException e) {
            log.error("Failed to delete test folder", e);
        }
    }

    @Override
    public void createEntities() throws Exception {
        System.setProperty(LocalFileStorage.SYSTEM_PROPERTY_LOCAL_FILE_STORAGE_FOLDER, testFolderPath);
        localFileStorage = new LocalFileStorage(fileStorageConfig);
    }

    @Override
    public void tearDown() throws Exception {

    }

    @Test
    public void saveFile() throws IOException {
        localFileStorage.saveFile(TEST_FILE_CONTENT, "text/plain", TEST_FILE_NAME);
        File checkFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();

        assertTrue(checkFile.exists());

        try (FileInputStream fileInputStream = new FileInputStream(checkFile)) {
            byte[] retrievedContents = IOUtils.toByteArray(fileInputStream);
            assertArrayEquals(TEST_FILE_CONTENT, retrievedContents);
        } finally {
            FileUtils.deleteQuietly(checkFile);
        }
    }

    @Test
    public void getFile() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();
        testFile.createNewFile();
        FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

        try (InputStream readStream = localFileStorage.getFile(TEST_FILE_NAME)) {
            byte[] readBytes = IOUtils.toByteArray(readStream);

            assertArrayEquals(TEST_FILE_CONTENT, readBytes);
        } finally {
            FileSystemUtils.deleteRecursively(testFile);
        }
    }

    @Test
    public void fileExistsDefault() throws IOException {
        File testFile = defaultStorageLocation.resolve(TEST_FILE_NAME_TWO).toFile();
        testFile.createNewFile();
        try {
            boolean fileExistsDefault = localFileStorage.fileExistsDefault(TEST_FILE_NAME_TWO);
            assertTrue(fileExistsDefault);
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

    @Test
    public void copyFileFromDefault() throws IOException {
        String internalFileName = "test_copied.txt";

        File testFile = defaultStorageLocation.resolve(TEST_FILE_NAME_TWO).toFile();
        FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

        File copiedFile = null;
        try {
            localFileStorage.copyFileFromDefault(TEST_FILE_NAME_TWO, internalFileName);

            copiedFile = staticStorageLocation.resolve(internalFileName).toFile();

            assertTrue(copiedFile.exists());

            byte[] copiedFileData = FileUtils.readFileToByteArray(copiedFile);

            assertArrayEquals(TEST_FILE_CONTENT, copiedFileData);
        } finally {
            FileUtils.deleteQuietly(testFile);
            FileUtils.deleteQuietly(copiedFile);
        }
    }

    @Test
    public void copyFile() throws IOException {

        String internalFileNameSource = "test_copied.txt";
        String internalFileNameTarget = "test_copied.txt";

        File testFileSource = staticStorageLocation.resolve(internalFileNameSource).toFile();
        FileUtils.writeByteArrayToFile(testFileSource, TEST_FILE_CONTENT);

        File copiedFile = null;
        try {
            localFileStorage.copyFile(internalFileNameSource, internalFileNameTarget);

            copiedFile = staticStorageLocation.resolve(internalFileNameTarget).toFile();

            assertTrue(copiedFile.exists());

            byte[] copiedFileData = FileUtils.readFileToByteArray(copiedFile);

            assertArrayEquals(TEST_FILE_CONTENT, copiedFileData);
        } finally {
            FileUtils.deleteQuietly(testFileSource);
            FileUtils.deleteQuietly(copiedFile);
        }
    }

    @Test
    public void getFileFromDefault() throws IOException {
        File testFile = defaultStorageLocation.resolve(TEST_FILE_NAME).toFile();

        FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

        try (InputStream fileContentStream = localFileStorage.getFileFromDefault(TEST_FILE_NAME)) {
            byte[] fileData = IOUtils.toByteArray(fileContentStream);

            assertArrayEquals(fileData, TEST_FILE_CONTENT);
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

    @Test
    public void fileExists() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();
        testFile.createNewFile();
        try {
            assertTrue(localFileStorage.fileExists(TEST_FILE_NAME));
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

    @Test
    public void findAll() throws IOException {
        String testFolderName = "tempTestFolder";
        Path testDirectory = staticStorageLocation.resolve(testFolderName);
        try {
            FileUtils.forceMkdir(testDirectory.toFile());

            String internalFilename1 = "testFile1.txt";
            String internalFilename2 = "testFile2.txt";
            String internalFilename3 = "testFile3.txt";

            File testFile1 = testDirectory.resolve(internalFilename1).toFile();
            File testFile2 = testDirectory.resolve(internalFilename2).toFile();
            File testFile3 = testDirectory.resolve(internalFilename3).toFile();

            testFile1.createNewFile();
            testFile2.createNewFile();
            testFile3.createNewFile();

            List<String> fileNames = localFileStorage.findAll(testFolderName);

            assertEquals(fileNames.size(), 3);
            assertThat(fileNames)
                    .extracting(s -> StringUtils.removeStart(s, testFolderName + File.separator))
                    .containsExactlyInAnyOrder(internalFilename1, internalFilename2, internalFilename3);
        } finally {
            FileUtils.deleteDirectory(testDirectory.toFile());
        }
    }

    @Test
    public void moveToTrash() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME_TWO).toFile();
        File trashFile = null;

        try {
            FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

            localFileStorage.moveToTrash(TEST_FILE_NAME_TWO);

            // check if file is in trash folder

            trashFile = trashStorageLocation.resolve(TEST_FILE_NAME_TWO).toFile();
            assertTrue(trashFile.exists());
            assertArrayEquals(TEST_FILE_CONTENT, FileUtils.readFileToByteArray(trashFile));

            // check if file is removed from static folder

            assertFalse(testFile.exists());
        } finally {
            FileUtils.deleteQuietly(trashFile);
        }
    }

    @Test
    public void deleteFile() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME_TWO).toFile();

        try {
            FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

            localFileStorage.deleteFile(TEST_FILE_NAME_TWO);

            assertFalse(testFile.exists());
        } finally {
            //delete file in case the it still exists
            FileUtils.deleteQuietly(testFile);
        }

    }

    @Test
    public void getExternalUrl() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();

        try {
            FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

            String expectedUrl = fileStorageConfig.getExternalBaseUrl() +
                    staticStorageLocation.resolve(TEST_FILE_NAME).toAbsolutePath();
            String actualUrl = localFileStorage.getExternalUrl(TEST_FILE_NAME);

            assertEquals(expectedUrl, actualUrl);
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

    @Test
    public void getExternalBaseUrl() {
        assertEquals(localFileStorage.getExternalBaseUrl(), fileStorageConfig.getExternalBaseUrl());
    }

    @Test
    public void getInternalFileName() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();

        try {
            FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

            String externalUrl = localFileStorage.getExternalUrl(TEST_FILE_NAME);
            String internalFileName = localFileStorage.getInternalFileName(externalUrl);

            assertEquals(internalFileName, staticStorageLocation.relativize(Paths.get(
                    StringUtils.removeStart(externalUrl, fileStorageConfig.getExternalBaseUrl()))).toString());
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

    @Test
    public void createZipFile() throws IOException {

        String internalFilename1 = "testFile1.txt";
        String internalFilename2 = "testFile2.txt";
        String internalFilename3 = "testFile3.txt";
        String internalZipFileName = "test.zip";

        List<String> internalFileNames = new ArrayList<>();

        File testFile1 = staticStorageLocation.resolve(internalFilename1).toFile();
        File testFile2 = staticStorageLocation.resolve(internalFilename2).toFile();
        File testFile3 = staticStorageLocation.resolve(internalFilename3).toFile();

        try {
            FileUtils.writeByteArrayToFile(testFile1, TEST_FILE_CONTENT);
            FileUtils.writeByteArrayToFile(testFile2, TEST_FILE_CONTENT);
            FileUtils.writeByteArrayToFile(testFile3, TEST_FILE_CONTENT);

            internalFileNames.add(internalFilename1);
            internalFileNames.add(internalFilename2);
            internalFileNames.add(internalFilename3);

            String zipOutput = localFileStorage.createZipFile(internalZipFileName, internalFileNames, null);

            String zipFileFullPath = staticStorageLocation.resolve(internalZipFileName).toString();
            assertThat(zipOutput).isEqualTo(fileStorageConfig.getExternalBaseUrl() + zipFileFullPath);

            List<String> fileNames = new ArrayList<>();
            try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFileFullPath))) {
                ZipEntry zipEntry = zis.getNextEntry();
                while (zipEntry != null) {
                    fileNames.add(zipEntry.getName());
                    zipEntry = zis.getNextEntry();
                }
            }
            assertThat(fileNames)
                    .containsExactlyInAnyOrder(internalFilename1, internalFilename2, internalFilename3);
        } finally {
            FileUtils.deleteQuietly(staticStorageLocation.resolve(internalZipFileName).toFile());
            FileUtils.deleteQuietly(testFile1);
            FileUtils.deleteQuietly(testFile2);
            FileUtils.deleteQuietly(testFile3);
        }
    }

    @Test
    public void createZipFileWithFlattenedPaths() throws IOException {

        String internalFilename1 = "folder1/folder2/testFile1.txt";
        String zipInternalFilename1 = "testFile1.txt";
        String internalFilename2 = "folder1/testFile2.txt";
        String internalFilename3 = "folder3/testFile3.txt";
        String zipInternalFilename3 = "testFile3.txt";
        String internalFilename4 = "folder4/testFile4.txt";
        String internalZipFileName = "test.zip";

        List<String> internalFileNames = new ArrayList<>();
        List<String> pathsToFlatten = new ArrayList<>();
        pathsToFlatten.add("folder1/folder2/");
        pathsToFlatten.add("folder3/");
        pathsToFlatten.add("folder5/");

        File testFile1 = staticStorageLocation.resolve(internalFilename1).toFile();
        File testFile2 = staticStorageLocation.resolve(internalFilename2).toFile();
        File testFile3 = staticStorageLocation.resolve(internalFilename3).toFile();
        File testFile4 = staticStorageLocation.resolve(internalFilename4).toFile();

        try {
            FileUtils.writeByteArrayToFile(testFile1, TEST_FILE_CONTENT);
            FileUtils.writeByteArrayToFile(testFile2, TEST_FILE_CONTENT);
            FileUtils.writeByteArrayToFile(testFile3, TEST_FILE_CONTENT);
            FileUtils.writeByteArrayToFile(testFile4, TEST_FILE_CONTENT);

            internalFileNames.add(internalFilename1);
            internalFileNames.add(internalFilename2);
            internalFileNames.add(internalFilename3);
            internalFileNames.add(internalFilename4);

            String zipOutput = localFileStorage.createZipFile(internalZipFileName, internalFileNames, pathsToFlatten);

            String zipFileFullPath = staticStorageLocation.resolve(internalZipFileName).toString();
            assertThat(zipOutput).isEqualTo(fileStorageConfig.getExternalBaseUrl() + zipFileFullPath);

            List<String> fileNames = new ArrayList<>();
            try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFileFullPath))) {
                ZipEntry zipEntry = zis.getNextEntry();
                while (zipEntry != null) {
                    fileNames.add(zipEntry.getName());
                    zipEntry = zis.getNextEntry();
                }
            }
            assertThat(fileNames).containsExactlyInAnyOrder(zipInternalFilename1, internalFilename2,
                    zipInternalFilename3, internalFilename4);
        } finally {
            FileUtils.deleteQuietly(staticStorageLocation.resolve(internalZipFileName).toFile());
            FileUtils.deleteQuietly(testFile1);
            FileUtils.deleteQuietly(testFile2);
            FileUtils.deleteQuietly(testFile3);
            FileUtils.deleteQuietly(testFile4);
        }
    }

    @Test
    public void getAge() throws IOException {
        File testFile = staticStorageLocation.resolve(TEST_FILE_NAME).toFile();
        try {
            FileUtils.writeByteArrayToFile(testFile, TEST_FILE_CONTENT);

            Duration age = localFileStorage.getAge(TEST_FILE_NAME);

            assertThat(age.toMillis()).isLessThan(Instant.now().toEpochMilli());
        } finally {
            FileUtils.deleteQuietly(testFile);
        }
    }

}
