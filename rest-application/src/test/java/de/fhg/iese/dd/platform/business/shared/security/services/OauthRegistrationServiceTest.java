/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;

public class OauthRegistrationServiceTest extends BaseServiceTest {

    @MockBean
    private IOauthManagementService oauthManagementServiceMock;
    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;

    @Autowired
    private IOauthRegistrationService oauthRegistrationService;

    @Autowired
    private IPersonService personService;

    private final String oauthId = "auth0|123123123123";
    private final String email = "digdorfdev+test1@gmail.com";

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        oauthRegistrationRepository.deleteAll();
        oauthRegistrationRepository.flush();
    }

    @Test
    public void deleteUnusedRegistration() {
        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .created(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(25))
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        Mockito.verify(oauthManagementServiceMock, Mockito.times(1))
                .deleteUser(oauthId);
        assertThat(oauthRegistrationRepository.findByOauthId(oauthId)).isNull();
    }

    @Test
    public void doNotDeleteExistingPerson() {

        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .created(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(25))
                .build());

        personService.create(Person.builder()
                .oauthId(oauthId)
                .email("example@test.de")
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        Mockito.verify(oauthManagementServiceMock, Mockito.times(0))
                .deleteUser(oauthId);
        final OauthRegistration registrationInRepository = oauthRegistrationRepository.findByOauthId(oauthId);
        assertThat(registrationInRepository.getOauthId()).isEqualTo(oauthId);
        assertThat(registrationInRepository.getErrorMessage()).isNullOrEmpty();
    }

    @Test
    public void doNotDeleteBeforeConfiguredDuration() {
        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        Mockito.verify(oauthManagementServiceMock, Mockito.times(0))
                .deleteUser(oauthId);
        final OauthRegistration registrationInRepository = oauthRegistrationRepository.findByOauthId(oauthId);
        assertThat(registrationInRepository.getOauthId()).isEqualTo(oauthId);
        assertThat(registrationInRepository.getErrorMessage()).isNullOrEmpty();
    }

    @Test
    public void deleteNoOauthId() {

        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId("")
                .created(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(25))
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        assertThat(oauthRegistrationRepository.findByOauthId(oauthId))
                .as("Should be deleted as well, as there is no corresponding backend person for oauthId")
                .isNull();
    }

    @Test
    public void doNotDeleteWithErrorMessage() {

        final String errorMessage = "This is an error message for test purposes.";
        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .errorMessage(errorMessage)
                .created(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(25))
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        Mockito.verify(oauthManagementServiceMock, Mockito.times(0))
                .deleteUser(oauthId);
        final OauthRegistration registrationInRepository = oauthRegistrationRepository.findByOauthId(oauthId);
        assertThat(registrationInRepository.getOauthId()).isEqualTo(oauthId);
        assertThat(registrationInRepository.getErrorMessage()).isEqualTo(errorMessage);
    }

    @Test
    public void deletionError() {

        OauthManagementException expectedException = new OauthManagementException("Error for test purposes");
        Mockito.doThrow(expectedException).when(oauthManagementServiceMock).deleteUser(oauthId);

        oauthRegistrationRepository.saveAndFlush(OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .created(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(25))
                .build());

        oauthRegistrationService.checkAndDeleteUnusedRegistrations();

        Mockito.verify(oauthManagementServiceMock, Mockito.times(1))
                .deleteUser(oauthId);

        final OauthRegistration registrationInRepository = oauthRegistrationRepository.findByOauthId(oauthId);
        assertThat(registrationInRepository.getOauthId()).isEqualTo(oauthId);
        assertThat(registrationInRepository.getErrorMessage()).contains(expectedException.toString());
    }
    
}
