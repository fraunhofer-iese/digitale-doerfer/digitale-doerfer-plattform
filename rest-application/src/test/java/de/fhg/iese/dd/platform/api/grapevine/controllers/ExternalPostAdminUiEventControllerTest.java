/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteByIdRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteConfirmation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GlobalExternalPostAdmin;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.NewsSourcePostAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ExternalPostAdminUiEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
        th.assignRoleToPerson(th.personSebastianBauer, GlobalExternalPostAdmin.class, null);
        globalExternalPostAdmin = th.personSebastianBauer;
        th.assignRoleToPerson(th.personSusanneChristmann, NewsSourcePostAdmin.class, th.newsSourceDigitalbach.getId());
        digitalbachNewsSourcePostAdmin = th.personSusanneChristmann;
        th.assignRoleToPerson(th.personThomasBecker, NewsSourcePostAdmin.class, th.newsSourceAnalogheim.getId());
        analogheimNewsSourcePostAdmin = th.personThomasBecker;
    }

    private Person globalExternalPostAdmin;
    private Person digitalbachNewsSourcePostAdmin;
    private Person analogheimNewsSourcePostAdmin;

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    //global: personSebastianBauer
    //newsSourceDigitalbach:
    //  posts:  newsItem1, unpublishedNewsItem1
    //  person: personSusanneChristmann
    //newsSource1: newsItem6
    @Test
    public void deleteExternalPost() throws Exception {
        //GlobalExternalPostAdmin
        deleteExternalPostAndExpectOk(th.newsItem1, globalExternalPostAdmin);
        deleteExternalPostAndExpectOk(th.unpublishedNewsItem1, globalExternalPostAdmin);
        deleteExternalPostAndExpectOk(th.happening1, globalExternalPostAdmin);

        //NewsSourcePostAdmin
        deleteExternalPostAndExpectOk(th.newsItem2, digitalbachNewsSourcePostAdmin);
        deleteExternalPostAndExpectOk(th.happening2, digitalbachNewsSourcePostAdmin);
    }

    private void deleteExternalPostAndExpectOk(ExternalPost externalPost, Person person) throws Exception{
        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .postId(externalPost.getId())
                .build();

        MvcResult deleteRequest = mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(postDeleteRequest.getPostId()))
                .andReturn();

        ClientExternalPostDeleteConfirmation externalPostDeleteConfirmation = toObject(deleteRequest.getResponse(),
                ClientExternalPostDeleteConfirmation.class);

        ExternalPost deletedExternalPost = th.externalPostRepository.findById(externalPostDeleteConfirmation.getPostId()).get();
        assertTrue(deletedExternalPost.isDeleted());
    }

    @Test
    public void deleteExternalPost_Unauthorized() throws Exception {
        //not matching news source
        deleteExternalPostAndExpectNotAuthorized(th.newsItem1, analogheimNewsSourcePostAdmin);

        //no role
        deleteExternalPostAndExpectNotAuthorized(th.newsItem1, th.personRegularKarl);
    }

    private void deleteExternalPostAndExpectNotAuthorized(ExternalPost externalPost, Person person) throws Exception{

        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .postId(externalPost.getId())
                .build();

        mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isNotAuthorized());

        ExternalPost notDeletedExternalPost = th.externalPostRepository.findById(externalPost.getId()).get();
        assertFalse(notDeletedExternalPost.isDeleted());
    }

    @Test
    public void deleteExternalPost_NotFound() throws Exception {
        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .postId("not-even-a-uuid")
                .build();

        mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(globalExternalPostAdmin))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void deleteExternalPost_AlreadyDeleted() throws Exception {
        final ExternalPost postToBeDeleted = th.newsItem1;
        postToBeDeleted.setDeleted(true);
        th.postRepository.save(postToBeDeleted);

        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .postId(postToBeDeleted.getId())
                .build();

        mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(globalExternalPostAdmin))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        ExternalPost deletedExternalPost = th.externalPostRepository.findById(postToBeDeleted.getId()).get();
        assertTrue(deletedExternalPost.isDeleted());
    }

    @Test
    public void deleteExternalPost_WrongType() throws Exception {

        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .postId(th.gossip1withImages.getId())
                .build();

        mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(globalExternalPostAdmin))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        Post notDeletedPost = th.postRepository.findById(th.gossip1withImages.getId()).get();
        assertFalse(notDeletedPost.isDeleted());
    }

    @Test
    public void deleteExternalPost_MissingId() throws Exception {
        ClientExternalPostDeleteByIdRequest postDeleteRequest = ClientExternalPostDeleteByIdRequest.builder()
                .build();

        mockMvc.perform(post("/adminui/post/event/externalPostDeleteRequest")
                        .headers(authHeadersFor(globalExternalPostAdmin))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

}
