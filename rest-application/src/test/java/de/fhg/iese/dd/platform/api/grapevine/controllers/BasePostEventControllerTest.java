/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class BasePostEventControllerTest<T extends Post> extends BaseAbstractPostEventControllerTest<T> {

    private static final String REPLACEMENT_TEXT = "My new post text";
    private static final String SECOND_REPLACEMENT_TEXT = "Another updated text";

    protected abstract ClientBasePostChangeByPersonRequest getPostChangeRequest();

    @Test
    public void verifyTestData() throws Exception {
        super.verifyTestData();
        assertThat(getTestPost1NotOwner()).isNotEqualTo(getTestPost1().getCreator());
    }

    @Test
    public void postChangeRequest_Nothing() throws Exception {
        postChangeRequest_Nothing(getTestPost1());
    }

    @Test
    public void postChangeRequest_WithoutUseEmptyFields() throws Exception {
        postChangeRequest(getTestPost1(), false);
    }

    @Test
    public void postChangeRequest_WithUseEmptyFields() throws Exception {
        postChangeRequest(getTestPost1(), true);
    }

    @Test
    public void postChangeRequest_Text() throws Exception {
        postChangeRequest_Text(getTestPost1());
    }

    @Test
    public void postChangeRequest_TextInvalid() throws Exception {
        postChangeRequest_TextInvalid(getTestPost1());
    }

    @Test
    public void postChangeRequest_Address() throws Exception {
        postChangeRequest_Address(getTestPost1());
    }

    @Test
    public void postChangeRequest_AddressRemove() throws Exception {
        postChangeRequest_AddressRemove(getTestPost1());
    }

    @Test
    public void postChangeRequest_Images() throws Exception {
        postChangeRequest_Images(getTestPost1());
    }

    @Test
    public void postChangeRequest_ImagesRemoveAll() throws Exception {
        postChangeRequest_ImagesRemoveAll(getTestPost1());
    }

    @Test
    public void postChangeRequest_ImagesInvalid() throws Exception {
        postChangeRequest_ImagesInvalid(getTestPost2());
    }

    @Test
    public void postChangeRequest_ImagesExceedingMaxNumberImages() throws Exception {
        postChangeRequest_ImagesExceedingMaxNumberImages(getTestPost1());
    }

    @Test
    public void postChangeRequest_Documents() throws Exception {
        postChangeRequest_Documents(getTestPost1());
    }

    @Test
    public void postChangeRequest_DocumentsRemoveAll() throws Exception {
        postChangeRequest_DocumentsRemoveAll(getTestPost1());
    }

    @Test
    public void postChangeRequest_DocumentsInvalid() throws Exception {
        postChangeRequest_DocumentsInvalid(getTestPost1());
    }

    @Test
    public void postChangeRequest_DocumentsExceedingMaxNumberDocuments() throws Exception {
        postChangeRequest_DocumentsExceedingMaxNumberDocuments(getTestPost1());
    }

    @Test
    public void postChangeRequest_VerifyingPushMessage() throws Exception {
        postChangeRequest_VerifyingPushMessage(getTestPost1());
    }

    @Test
    public void postChangeRequest_WrongType() throws Exception {
        postChangeRequest_WrongType(getTestPostDifferentType());
    }

    @Test
    public void postChangeRequest_Unauthorized() throws Exception {
        postChangeRequest_Unauthorized(getTestPost1());
    }

    @Test
    public void postDeleteRequest_VerifyingPushMessage() throws Exception {
        postDeleteRequest_VerifyingPushMessage(getTestPost1());
    }

    @Test
    public void postDeleteRequest_MissingAttributes() throws Exception {
        postDeleteRequest_MissingAttributes(getTestPost1());
    }

    private void postDeleteRequest_VerifyingPushMessage(Post postToBeDeleted) throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person creator = postToBeDeleted.getCreator();

        // Suggestion cannot be deleted, but only withdrawn
        if (postToBeDeleted.getPostType() == PostType.Suggestion) {
            return;
        }

        String postToBeDeletedId = postToBeDeleted.getId();
        ClientPostDeleteRequest postDeleteRequest = ClientPostDeleteRequest.builder()
                .postId(postToBeDeletedId)
                .build();

        mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(postToBeDeletedId));

        // Get the post - it should return POST_NOT_FOUND
        mockMvc.perform(get("/grapevine/post/" + postToBeDeletedId)
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        // Check whether the post is also deleted from the repository
        Post deletedPost = th.postRepository.findById(postToBeDeletedId).get();
        assertThat(deletedPost.isDeleted()).isTrue();
        assertThat(deletedPost.getDeletionTime()).isEqualTo(nowTime);

        // verify push message
        waitForEventProcessing();

        ClientPostDeleteConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(postToBeDeleted.getGeoAreas(),
                        postToBeDeleted.isHiddenForOtherHomeAreas(),
                        ClientPostDeleteConfirmation.class, DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertThat(pushedEvent.getPostId()).isEqualTo(postToBeDeletedId);

        testClientPushService.assertNoMorePushedEvents();
    }

    private void postDeleteRequest_MissingAttributes(Post postToBeDeleted) throws Exception {

        Person creator = postToBeDeleted.getCreator();

        ClientPostDeleteRequest invalidPostDeleteRequest = ClientPostDeleteRequest.builder()
                .build();

        mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(invalidPostDeleteRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        assertPostIsNotDeleted(postToBeDeleted);
    }

    /*
                                        useEmptyFields  setText     setMediaItemPlaceHolders
    changePostNothing                   -               -           -
    changePostImages                    -               -           +
    changePostText                      -               +           -
    changePostWithoutUseEmptyFields     -               +           +
    changePostTextInvalid               +               -
    changePostImagesRemoveAll           +               +           -
    changePostWithUseEmptyFields        +               +           +
    */

    private void postChangeRequest_Nothing(final Post post) throws Exception {

        Person creator = post.getCreator();
        String typeName = getTypeName(post);

        assertThat(post.getText()).as("Post has no message, adjust test helper").isNotEmpty();
        // Make sure there are images
        assertThat(post.getImages().isEmpty()).as("Post has no images, adjust test helper").isFalse();

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getId()).isEqualTo(post.getId());
        assertThat(updatedPost.getText()).isEqualTo(post.getText());
        assertThat(updatedPost.getImages()).isEqualTo(post.getImages());
    }

    private void postChangeRequest_VerifyingPushMessage(final Post post) throws Exception {

        assertThat(post.getImages()).as("Post has no images, adjust test helper").isNotEmpty();

        final ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText(REPLACEMENT_TEXT);
        postChangeRequest.setUseEmptyFields(false);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(post) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        final Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getId()).isEqualTo(postChangeRequest.getPostId());
        assertThat(updatedPost.getText()).isEqualTo(REPLACEMENT_TEXT);
        assertThat(updatedPost.getImages().isEmpty()).isFalse();

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(post.getGeoAreas(),
                        post.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class, DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPostId()).isEqualTo(post.getId());

        testClientPushService.assertNoMorePushedEvents();

        Post hiddenPost = post;
        hiddenPost.setHiddenForOtherHomeAreas(true);
        hiddenPost = th.postRepository.saveAndFlush(hiddenPost);

        final ClientBasePostChangeByPersonRequest hiddenPostChangeRequest = getPostChangeRequest();
        hiddenPostChangeRequest.setPostId(hiddenPost.getId());
        hiddenPostChangeRequest.setText(SECOND_REPLACEMENT_TEXT);
        hiddenPostChangeRequest.setUseEmptyFields(false);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(hiddenPost) + "ChangeRequest")
                        .headers(authHeadersFor(hiddenPost.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(hiddenPostChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + getTypeName(hiddenPost) + ".id").value(hiddenPost.getId()))
                .andExpect(jsonPath("$.postId").value(hiddenPost.getId()));

        // Check whether the post is also updated in the repository
        final Post updatedHiddenPost = th.postRepository.findById(hiddenPost.getId()).get();

        assertThat(updatedHiddenPost.getId()).isEqualTo(postChangeRequest.getPostId());
        assertThat(updatedHiddenPost.getText()).isEqualTo(SECOND_REPLACEMENT_TEXT);
        assertThat(updatedHiddenPost.getImages().isEmpty()).isFalse();

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation hiddenPushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(hiddenPost.getGeoAreas(),
                        hiddenPost.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class, DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(hiddenPushedEvent.getPostId()).isEqualTo(hiddenPost.getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    private void postChangeRequest(final Post post, boolean useEmptyFields) throws Exception {

        final int imageIndexToDrop = 0;
        final int imageIndexToKeep = 1;

        Person creator = post.getCreator();
        List<MediaItem> currentImages = post.getImages();
        String typeName = getTypeName(post);

        //create temp images
        TemporaryMediaItem tempImage = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        assertThat(post.getText()).as("Post has no message, adjust test helper").isNotEmpty();
        // Make sure there are images to change
        assertThat(currentImages).as("Post has too less images, adjust test helper").hasSizeGreaterThanOrEqualTo(2);

        ClientMediaItemPlaceHolder mediaHolder = ClientMediaItemPlaceHolder.builder()
                .mediaItemId(currentImages.get(imageIndexToKeep).getId())
                .build();
        ClientMediaItemPlaceHolder tempMediaHolder = ClientMediaItemPlaceHolder.builder()
                .temporaryMediaItemId(tempImage.getId())
                .build();

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText(REPLACEMENT_TEXT);
        postChangeRequest.setMediaItemPlaceHolders(Arrays.asList(mediaHolder, tempMediaHolder));
        postChangeRequest.setUseEmptyFields(useEmptyFields);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        List<MediaItem> updatedMediaItems = updatedPost.getImages();

        assertThat(updatedPost.getId()).isEqualTo(postChangeRequest.getPostId());
        assertThat(updatedPost.getText()).isEqualTo(REPLACEMENT_TEXT);
        assertThat(updatedMediaItems.size()).isEqualTo(2);
        assertThat(updatedMediaItems.get(0)).isEqualTo(currentImages.get(imageIndexToKeep));
        assertThat(updatedMediaItems.get(1)).isEqualTo(tempImage.getMediaItem());

        //Check that we do not produce orphans
        assertThat(th.mediaItemRepository.existsById(currentImages.get(imageIndexToDrop).getId())).isFalse();
    }

    private void postChangeRequest_Text(final Post post) throws Exception {

        assertThat(post.getImages()).as("Post has no images, adjust test helper").isNotEmpty();

        final ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText(REPLACEMENT_TEXT);
        postChangeRequest.setUseEmptyFields(false);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(post) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        final Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getId()).isEqualTo(postChangeRequest.getPostId());
        assertThat(updatedPost.getText()).isEqualTo(REPLACEMENT_TEXT);
        assertThat(updatedPost.getImages().isEmpty()).isFalse();
    }

    private void postChangeRequest_Address(final Post post) throws Exception {

        final ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        ClientLocationDefinition locationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;
        postChangeRequest.setCustomLocationDefinition(locationDefinition);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(post) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".id").value(post.getId()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.name")
                        .value(locationDefinition.getLocationName()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.street")
                        .value(locationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.zip")
                        .value(locationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.city")
                        .value(locationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.gpsLocation.latitude")
                        .value(locationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress.gpsLocation.longitude")
                        .value(locationDefinition.getGpsLocation().getLongitude()))

                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        final Post updatedPost = th.postRepository.findById(post.getId()).get();
        assertThat(updatedPost.getCustomAddress().getName()).isEqualTo(locationDefinition.getLocationName());
        assertThat(updatedPost.getCustomAddress().getStreet()).isEqualTo(locationDefinition.getAddressStreet());
        assertThat(updatedPost.getCustomAddress().getZip()).isEqualTo(locationDefinition.getAddressZip());
        assertThat(updatedPost.getCustomAddress().getCity()).isEqualTo(locationDefinition.getAddressCity());
        assertThat(updatedPost.getCustomAddress().getGpsLocation().getLatitude())
                .isEqualTo(locationDefinition.getGpsLocation().getLatitude());
        assertThat(updatedPost.getCustomAddress().getGpsLocation().getLongitude())
                .isEqualTo(locationDefinition.getGpsLocation().getLongitude());
    }

    private void postChangeRequest_AddressRemove(final Post post) throws Exception {

        //ensure there is an address
        post.setCustomAddress(th.addressPersonVgAdmin);
        th.postRepository.save(post);
        final ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        postChangeRequest.setRemoveCustomAddress(true);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(post) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".id").value(post.getId()))
                .andExpect(jsonPath("$.post." + getTypeName(post) + ".customAddress").doesNotExist())
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        final Post updatedPost = th.postRepository.findById(post.getId()).get();
        assertThat(updatedPost.getCustomAddress()).isNull();
    }

    private void postChangeRequest_TextInvalid(final Post post) throws Exception {

        final ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText(null);
        postChangeRequest.setUseEmptyFields(true);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(post) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    private void postChangeRequest_Images(Post post) throws Exception {

        Person creator = post.getCreator();
        List<MediaItem> currentImages = post.getImages();
        String typeName = getTypeName(post);

        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        assertThat(post.getText()).as("Post has no message, adjust test helper").isNotEmpty();
        // Make sure there are images to change
        assertThat(currentImages).as("Post has too less images, adjust test helper").hasSizeGreaterThanOrEqualTo(2);

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(1).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        List<MediaItem> updatedMediaItems = updatedPost.getImages();

        assertThat(updatedPost.getText()).isNotEmpty();
        assertThat(updatedMediaItems).containsExactly(currentImages.get(1), tempImage1.getMediaItem(),
                tempImage2.getMediaItem());

        //Check that we do not produce orphans
        assertThat(th.mediaItemRepository.existsById(currentImages.get(0).getId())).isFalse();
    }

    private void postChangeRequest_ImagesExceedingMaxNumberImages(Post post) throws Exception {

        Person creator = post.getCreator();
        List<MediaItem> currentImages = post.getImages();
        String typeName = getTypeName(post);
        // Make sure there are images to change
        assertThat(currentImages).as("Post has too less images, adjust test helper").hasSizeGreaterThanOrEqualTo(2);

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(1).getId()).build());

        //create temp images, one more than max
        int numImagesToCreate = (th.grapevineConfig.getMaxNumberImagesPerPost() - 2) + 1;
        for (int i = 0; i < numImagesToCreate; i++) {
            String temporaryMediaItemId = th.createTemporaryMediaItem(creator, th.nextTimeStamp()).getId();
            mediaHolders.add(ClientMediaItemPlaceHolder.builder()
                    .temporaryMediaItemId(temporaryMediaItemId)
                    .build());
        }

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("mediaItemPlaceHolders", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // Check whether the post remains the same
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        List<MediaItem> updatedMediaItems = updatedPost.getImages();

        assertThat(updatedMediaItems.size()).isEqualTo(currentImages.size());
        assertThat(updatedMediaItems).isEqualTo(currentImages);
    }

    private void postChangeRequest_ImagesRemoveAll(Post post) throws Exception {

        Person creator = post.getCreator();
        List<MediaItem> currentImages = post.getImages();
        String typeName = getTypeName(post);

        // Make sure there are images to remove
        assertThat(currentImages).as("Post has too less images, adjust test helper").hasSizeGreaterThanOrEqualTo(2);

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(true);
        postChangeRequest.setText(post.getText());
        postChangeRequest.setMediaItemPlaceHolders(Collections.emptyList());

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        waitForEventProcessing();

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getImages()).isEmpty();

        //Check that we do not produce orphans
        assertThat(currentImages.stream().noneMatch(m -> th.mediaItemRepository.existsById(m.getId()))).isTrue();
    }

    private void postChangeRequest_ImagesInvalid(Post post) throws Exception {

        Person creator = post.getCreator();
        List<MediaItem> currentImages = post.getImages();
        String typeName = getTypeName(post);

        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are images to change
        assertThat(currentImages).as("Post has too less images, adjust test helper").hasSizeGreaterThanOrEqualTo(2);

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        //media item id instead of temp media item id
        mediaHolders.add(
                ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getMediaItem().getId()).build());

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(tempImage1.getMediaItem().getId(),
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage1.getId())).isTrue();

        //create media item that does not belong to the post
        MediaItem otherMediaItem = th.createMediaItem("other media item");

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        //media item that does not belong to the post
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(otherMediaItem.getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(otherMediaItem.getId(), ClientExceptionType.FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage1.getId())).isTrue();

        //invalid place holder

        //create additional temp image
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());
        //invalid, because both IDs are set
        ClientMediaItemPlaceHolder invalidPlaceHolder = new ClientMediaItemPlaceHolder();
        invalidPlaceHolder.setMediaItemId(currentImages.get(0).getId());
        invalidPlaceHolder.setTemporaryMediaItemId(tempImage1.getId());

        mediaHolders.add(invalidPlaceHolder);

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(invalidPlaceHolder.toString(), ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage1.getId())).isTrue();
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage2.getId())).isTrue();

        //duplicate temporary media items

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("[" + tempImage2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage1.getId())).isTrue();
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage2.getId())).isTrue();
        //duplicate  media items

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setMediaItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("[" + currentImages.get(0).getId() + "]",
                        ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage1.getId())).isTrue();
        assertThat(th.temporaryMediaItemRepository.existsById(tempImage2.getId())).isTrue();
    }

    private void postChangeRequest_Documents(Post post) throws Exception {

        Person creator = post.getCreator();
        List<DocumentItem> currentDocuments = new ArrayList<>(post.getAttachments());
        String typeName = getTypeName(post);
        String existingText = post.getText();

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        assertThat(post.getText()).as("Post has no message, adjust test helper").isNotEmpty();
        // Make sure there are documents to change
        assertThat(currentDocuments).as("Post has too few attachments, adjust test helper")
                .hasSizeGreaterThanOrEqualTo(2);

        List<ClientDocumentItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(1).getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getText()).isEqualTo(existingText);
        assertThat(updatedPost.getAttachments()).containsExactlyInAnyOrder(currentDocuments.get(1),
                tempDocument1.getDocumentItem(), tempDocument2.getDocumentItem());
        //Check that we do not produce orphans
        assertThat(th.documentItemRepository.existsById(currentDocuments.get(0).getId())).isFalse();
    }

    private void postChangeRequest_DocumentsExceedingMaxNumberDocuments(Post post) throws Exception {

        Person creator = post.getCreator();
        List<DocumentItem> currentDocuments = new ArrayList<>(post.getAttachments());
        String typeName = getTypeName(post);
        // Make sure there are documents to change
        assertThat(currentDocuments).as("Post has too few attachments, adjust test helper")
                .hasSizeGreaterThanOrEqualTo(2);

        List<ClientDocumentItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientDocumentItemPlaceHolder.builder()
                .documentItemId(currentDocuments.get(0).getId())
                .build());
        mediaHolders.add(ClientDocumentItemPlaceHolder.builder()
                .documentItemId(currentDocuments.get(1).getId())
                .build());

        //create temp documents, one more than max
        int numDocumentsToCreate = (th.grapevineConfig.getMaxNumberAttachmentsPerPost() - 2) + 1;
        for (int i = 0; i < numDocumentsToCreate; i++) {
            String temporaryDocumentItemId =
                    th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp()).getId();
            mediaHolders.add(ClientDocumentItemPlaceHolder.builder()
                    .temporaryDocumentItemId(temporaryDocumentItemId)
                    .build());
        }

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("documentItemPlaceHolders", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // Check whether the post remains the same
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getAttachments()).containsExactlyInAnyOrderElementsOf(currentDocuments);
    }

    private void postChangeRequest_DocumentsRemoveAll(Post post) throws Exception {

        Person creator = post.getCreator();
        List<DocumentItem> currentDocuments = new ArrayList<>(post.getAttachments());
        String typeName = getTypeName(post);

        // Make sure there are documents to remove
        assertThat(currentDocuments).as("Post has too few attachments, adjust test helper")
                .hasSizeGreaterThanOrEqualTo(2);

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(true);
        postChangeRequest.setText(post.getText());
        postChangeRequest.setDocumentItemPlaceHolders(Collections.emptyList());

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post." + typeName + ".id").value(post.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()));

        waitForEventProcessing();

        // Check whether the post is also updated in the repository
        Post updatedPost = th.postRepository.findById(post.getId()).get();

        assertThat(updatedPost.getAttachments()).isEmpty();

        //Check that we do not produce orphans
        assertThat(currentDocuments.stream().noneMatch(m -> th.documentItemRepository.existsById(m.getId()))).isTrue();
    }

    private void postChangeRequest_DocumentsInvalid(Post post) throws Exception {

        Person creator = post.getCreator();
        List<DocumentItem> currentDocuments = new ArrayList<>(post.getAttachments());
        String typeName = getTypeName(post);

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are documents to change
        assertThat(currentDocuments).as("Post has too few attachments, adjust test helper")
                .hasSizeGreaterThanOrEqualTo(2);

        List<ClientDocumentItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        //document item id instead of temp document item id
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(
                        tempDocument1.getDocumentItem().getId()).build());

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setUseEmptyFields(false);
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(tempDocument1.getDocumentItem().getId(),
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId())).isTrue();

        //create document item that does not belong to the post
        DocumentItem otherDocumentItem = th.createDocumentItem("other document item");

        mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        //document item that does not belong to the post
        mediaHolders.add(ClientDocumentItemPlaceHolder.builder().documentItemId(otherDocumentItem.getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(otherDocumentItem.getId(), ClientExceptionType.FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId())).isTrue();

        //invalid place holder

        //create additional temp document
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());
        //invalid, because both IDs are set
        ClientDocumentItemPlaceHolder invalidPlaceHolder = new ClientDocumentItemPlaceHolder();
        invalidPlaceHolder.setDocumentItemId(currentDocuments.get(0).getId());
        invalidPlaceHolder.setTemporaryDocumentItemId(tempDocument1.getId());

        mediaHolders.add(invalidPlaceHolder);

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(invalidPlaceHolder.toString(), ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId())).isTrue();
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument2.getId())).isTrue();

        //duplicate temporary document items

        mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("[" + tempDocument2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId())).isTrue();
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument2.getId())).isTrue();
        //duplicate  document items

        mediaHolders = new ArrayList<>();
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        mediaHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());

        postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setDocumentItemPlaceHolders(mediaHolders);

        mockMvc.perform(post("/grapevine/event/" + typeName + "ChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException("[" + currentDocuments.get(0).getId() + "]",
                        ClientExceptionType.FILE_ITEM_DUPLICATE_USAGE));

        //check that the item is not deleted if the request fails
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId())).isTrue();
        assertThat(th.temporaryDocumentItemRepository.existsById(tempDocument2.getId())).isTrue();
    }

    private void postChangeRequest_WrongType(Post post) throws Exception {

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText("bla bla bla");
        postChangeRequest.setUseEmptyFields(false);

        mockMvc.perform(post("/grapevine/event/" + getTypeName(getTestPost1()) + "ChangeRequest")
                        .headers(authHeadersFor(post.getCreator(), th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(postChangeRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    private void postChangeRequest_Unauthorized(T post) throws Exception {

        ClientBasePostChangeByPersonRequest postChangeRequest = getPostChangeRequest();
        postChangeRequest.setPostId(post.getId());
        postChangeRequest.setText("bla bla bla");
        postChangeRequest.setUseEmptyFields(false);

        assertOAuth2AppVariantRequired(post("/grapevine/event/" + getTypeName(getTestPost1()) + "ChangeRequest")
                        .contentType(contentType)
                        .content(json(postChangeRequest)),
                th.appVariant1Tenant1, th.personAnnikaSchneider);

        postChangeRequest.setPostId("unknown");

        assertOAuth2AppVariantRequired(post("/grapevine/event/" + getTypeName(getTestPost1()) + "ChangeRequest")
                        .contentType(contentType)
                        .content(json(postChangeRequest)),
                th.appVariant1Tenant1, th.personAnnikaSchneider);
    }
    
}
