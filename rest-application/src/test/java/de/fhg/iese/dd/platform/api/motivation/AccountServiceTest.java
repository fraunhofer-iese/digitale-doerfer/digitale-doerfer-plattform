/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.Optional;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountCreditLimitExceededException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountEntryAlreadyBookedException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountEntryNotFoundException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.AccountNotFoundException;
import de.fhg.iese.dd.platform.business.motivation.exceptions.ReservationsNotMatchingException;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;

public class AccountServiceTest extends BaseServiceTest {

    @Autowired
    private MotivationTestHelper th;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createScoreEntities();
    }

    @Override
    public void tearDown() {

        th.deleteAllData();
    }

    @Autowired
    private IAccountService accountService;

    @Autowired
    private PersonAccountRepository personAccountRepository;

    @Autowired
    private AccountEntryRepository accountEntryRepository;

    @Test
    public void reserveScore() {

        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;
        int expectedBalanceFromAccount = fromAccount.getBalance();
        int expectedBalanceToAccount = toAccount.getBalance();
        long timestampGreaterThan = System.currentTimeMillis();
        Pair<AccountEntry, AccountEntry> result =
                accountService.reserveScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1);

        AccountEntry fromAccountEntry = result.getLeft();
        AccountEntry toAccountEntry = result.getRight();

        assertEquals(fromAccount, fromAccountEntry.getAccount());
        assertEquals(AccountEntryType.RESERVATION_MINUS, fromAccountEntry.getAccountEntryType());
        assertEquals(-amount, fromAccountEntry.getAmount());
        assertEquals(toAccount, fromAccountEntry.getPartner());
        assertEquals(postingText, fromAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntry.getTimestamp());

        assertEquals(toAccount, toAccountEntry.getAccount());
        assertEquals(AccountEntryType.RESERVATION_PLUS, toAccountEntry.getAccountEntryType());
        assertEquals(amount, toAccountEntry.getAmount());
        assertEquals(fromAccount, toAccountEntry.getPartner());
        assertEquals(postingText, toAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntry.getTimestamp());

        final AccountEntry fromAccountEntryRepository = accountEntryRepository.findById(fromAccountEntry.getId()).get();
        final AccountEntry toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId()).get();

        assertEquals(fromAccount, fromAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.RESERVATION_MINUS, fromAccountEntryRepository.getAccountEntryType());
        assertEquals(-amount, fromAccountEntryRepository.getAmount());
        assertEquals(toAccount, fromAccountEntryRepository.getPartner());
        assertEquals(postingText, fromAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntryRepository.getTimestamp());

        assertEquals(toAccount, toAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.RESERVATION_PLUS, toAccountEntryRepository.getAccountEntryType());
        assertEquals(amount, toAccountEntryRepository.getAmount());
        assertEquals(fromAccount, toAccountEntryRepository.getPartner());
        assertEquals(postingText, toAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntryRepository.getTimestamp());

        final Account fromAccountTest = personAccountRepository.findById(fromAccount.getId()).get();
        final Account toAccountTest = personAccountRepository.findById(toAccount.getId()).get();

        assertEquals(expectedBalanceFromAccount, fromAccountTest.getBalance());
        assertEquals(expectedBalanceToAccount, toAccountTest.getBalance());
    }

    @Test
    public void bookScore() {

        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - bookScore && internalBookScore";
        int amount = 42;
        int expectedBalanceFromAccount = fromAccount.getBalance() - amount;
        int expectedBalanceToAccount = toAccount.getBalance() + amount;
        long timestampGreaterThan = System.currentTimeMillis();
        Pair<AccountEntry, AccountEntry> result = accountService.bookScore(fromAccount, toAccount, postingText, amount,
                //just using any base entity, does not matter for that test
            th.personVgAdmin, "", th.pushCategory_app1_1);

        AccountEntry fromAccountEntry = result.getLeft();
        AccountEntry toAccountEntry = result.getRight();

        assertEquals(fromAccount, fromAccountEntry.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntry.getAccountEntryType());
        assertEquals(-amount, fromAccountEntry.getAmount());
        assertEquals(toAccount, fromAccountEntry.getPartner());
        assertEquals(postingText, fromAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntry.getTimestamp());

        assertEquals(toAccount, toAccountEntry.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntry.getAccountEntryType());
        assertEquals(amount, toAccountEntry.getAmount());
        assertEquals(fromAccount, toAccountEntry.getPartner());
        assertEquals(postingText, toAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntry.getTimestamp());

        final AccountEntry fromAccountEntryRepository = accountEntryRepository.findById(fromAccountEntry.getId()).get();
        final AccountEntry toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId()).get();

        assertEquals(fromAccount, fromAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntryRepository.getAccountEntryType());
        assertEquals(-amount, fromAccountEntryRepository.getAmount());
        assertEquals(toAccount, fromAccountEntryRepository.getPartner());
        assertEquals(postingText, fromAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntryRepository.getTimestamp());

        assertEquals(toAccount, toAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntryRepository.getAccountEntryType());
        assertEquals(amount, toAccountEntryRepository.getAmount());
        assertEquals(fromAccount, toAccountEntryRepository.getPartner());
        assertEquals(postingText, toAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntryRepository.getTimestamp());

        final Account fromAccountTest = personAccountRepository.findById(fromAccount.getId()).get();
        final Account toAccountTest = personAccountRepository.findById(toAccount.getId()).get();

        assertEquals(expectedBalanceFromAccount, fromAccountTest.getBalance());
        assertEquals(expectedBalanceToAccount, toAccountTest.getBalance());
    }

    @Test
    public void bookReservedScore() {

        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        String postingText =
                "Testing accountService - bookReservedScore, checkReservationsMatch && checkIsBookingWithinCreditLimitIgnoreReserved";
        int expectedBalanceFromAccount = fromAccountEntry.getAccount().getBalance() + fromAccountEntry.getAmount();
        int expectedBalanceToAccount = toAccountEntry.getAccount().getBalance() + toAccountEntry.getAmount();
        long timestampGreaterThan = System.currentTimeMillis();

        Pair<AccountEntry, AccountEntry> result =
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1);

        fromAccountEntry = result.getLeft();
        toAccountEntry = result.getRight();

        assertEquals(th.accountPersonIeseAdmin, fromAccountEntry.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntry.getAccountEntryType());
        assertEquals(-10, fromAccountEntry.getAmount());
        assertEquals(th.accountPersonVgAdmin, fromAccountEntry.getPartner());
        assertEquals(postingText, fromAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntry.getTimestamp());

        assertEquals(th.accountPersonVgAdmin, toAccountEntry.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntry.getAccountEntryType());
        assertEquals(10, toAccountEntry.getAmount());
        assertEquals(th.accountPersonIeseAdmin, toAccountEntry.getPartner());
        assertEquals(postingText, toAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntry.getTimestamp());

        final AccountEntry fromAccountEntryRepository = accountEntryRepository.findById(fromAccountEntry.getId()).get();
        final AccountEntry toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId()).get();

        assertEquals(th.accountPersonIeseAdmin, fromAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntryRepository.getAccountEntryType());
        assertEquals(-10, fromAccountEntryRepository.getAmount());
        assertEquals(th.accountPersonVgAdmin, fromAccountEntryRepository.getPartner());
        assertEquals(postingText, fromAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntryRepository.getTimestamp());

        assertEquals(th.accountPersonVgAdmin, toAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntryRepository.getAccountEntryType());
        assertEquals(10, toAccountEntryRepository.getAmount());
        assertEquals(th.accountPersonIeseAdmin, toAccountEntryRepository.getPartner());
        assertEquals(postingText, toAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntryRepository.getTimestamp());

        final Account fromAccount = personAccountRepository.findById(th.accountPersonIeseAdmin.getId()).get();
        final Account toAccount = personAccountRepository.findById(th.accountPersonVgAdmin.getId()).get();

        assertEquals(expectedBalanceFromAccount, fromAccount.getBalance());
        assertEquals(expectedBalanceToAccount, toAccount.getBalance());
    }

    @Test
    public void freeReservedScore() {

        final AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        final AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        accountService.freeReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1);

        final Optional<AccountEntry> fromAccountEntryRepository =
                accountEntryRepository.findById(fromAccountEntry.getId());
        final Optional<AccountEntry> toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId());

        assertFalse(fromAccountEntryRepository.isPresent());
        assertFalse(toAccountEntryRepository.isPresent());
    }

    @Test
    public void getReservedMinusAmountForAccount() {

        int result = accountService.getReservedMinusAmountForAccount(th.accountPersonIeseAdmin);

        assertEquals(-10, result);
    }

    @Test
    public void getPersonAccountById() {

        Account result = accountService.getAccountById(th.accountPersonVgAdmin.getId());

        assertNotNull(result);
        assertEquals(th.accountPersonVgAdmin.getId(), result.getId());
        assertEquals(th.accountPersonVgAdmin.getBalance(), result.getBalance());
        assertEquals(th.accountPersonVgAdmin.getOwner(), ((PersonAccount) result).getOwner());
    }

    @Test
    public void getTenantAccountById() {

        Account result = accountService.getAccountById(th.accountTenant1.getId());

        assertNotNull(result);
        assertEquals(th.accountTenant1.getId(), result.getId());
        assertEquals(th.accountTenant1.getBalance(), result.getBalance());
        assertEquals(th.accountTenant1.getOwner(), ((TenantAccount) result).getOwner());
    }

    @Test
    public void getAccountByIdNullAccountId() {
        assertThrows(AccountNotFoundException.class, () -> accountService.getAccountById(null));
    }

    @Test
    public void save() {

        personAccountRepository.deleteAll();

        PersonAccount account = new PersonAccount(th.personVgAdmin, Collections.emptyList());
        account.setBalance(500);
        Account result = accountService.save(account);

        assertNotNull(result);
        assertEquals(account.getId(), result.getId());
        assertEquals(account.getBalance(), result.getBalance());
        assertEquals(account.getOwner(), ((PersonAccount) result).getOwner());

        final PersonAccount resultAccountRepository = personAccountRepository.findById(result.getId()).get();
        assertEquals(account.getId(), resultAccountRepository.getId());
        assertEquals(account.getBalance(), resultAccountRepository.getBalance());
        assertEquals(account.getOwner(), resultAccountRepository.getOwner());
    }

    @Test
    public void reserveScoreWithNegativeAmount() {

        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = -20;
        int expectedBalanceFromAccount = fromAccount.getBalance();
        int expectedBalanceToAccount = toAccount.getBalance();
        long timestampGreaterThan = System.currentTimeMillis();
        Pair<AccountEntry, AccountEntry> result =
                accountService.reserveScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1);

        AccountEntry fromAccountEntry = result.getLeft();
        AccountEntry toAccountEntry = result.getRight();

        assertEquals(toAccount, fromAccountEntry.getAccount());
        assertEquals(AccountEntryType.RESERVATION_MINUS, fromAccountEntry.getAccountEntryType());
        assertEquals(amount, fromAccountEntry.getAmount());
        assertEquals(fromAccount, fromAccountEntry.getPartner());
        assertEquals(postingText, fromAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntry.getTimestamp());

        assertEquals(fromAccount, toAccountEntry.getAccount());
        assertEquals(AccountEntryType.RESERVATION_PLUS, toAccountEntry.getAccountEntryType());
        assertEquals(-amount, toAccountEntry.getAmount());
        assertEquals(toAccount, toAccountEntry.getPartner());
        assertEquals(postingText, toAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntry.getTimestamp());

        final AccountEntry fromAccountEntryRepository = accountEntryRepository.findById(fromAccountEntry.getId()).get();
        final AccountEntry toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId()).get();

        assertEquals(toAccount, fromAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.RESERVATION_MINUS, fromAccountEntryRepository.getAccountEntryType());
        assertEquals(amount, fromAccountEntryRepository.getAmount());
        assertEquals(fromAccount, fromAccountEntryRepository.getPartner());
        assertEquals(postingText, fromAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntryRepository.getTimestamp());

        assertEquals(fromAccount, toAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.RESERVATION_PLUS, toAccountEntryRepository.getAccountEntryType());
        assertEquals(-amount, toAccountEntryRepository.getAmount());
        assertEquals(toAccount, toAccountEntryRepository.getPartner());
        assertEquals(postingText, toAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntryRepository.getTimestamp());

        final Account fromAccountTest = personAccountRepository.findById(fromAccount.getId()).get();
        final Account toAccountTest = personAccountRepository.findById(toAccount.getId()).get();

        assertEquals(expectedBalanceFromAccount, fromAccountTest.getBalance());
        assertEquals(expectedBalanceToAccount, toAccountTest.getBalance());
    }

    @Test
    public void reserveScoreNullFromAccount() {

        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        assertThrows(AccountNotFoundException.class, () ->
                //no fromAccount
                accountService.reserveScore(null, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void reserveScoreNonExistingFromAccount() {
        //no fromAccount
        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        personAccountRepository.deleteById(fromAccount.getId());

        assertThrows(AccountNotFoundException.class, () ->
                accountService.reserveScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1)
        );
    }

    @Test
    public void reserveScoreNullToAccount() {
        //no toAccount
        Account fromAccount = th.accountPersonVgAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        //no toAccount
        assertThrows(AccountNotFoundException.class,
                () -> accountService.reserveScore(fromAccount, null, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1)
        );
    }

    @Test
    public void reserveScoreNonExistingToAccount() {
        //no toAccount
        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        personAccountRepository.deleteById(toAccount.getId());

        assertThrows(AccountNotFoundException.class,
                () -> accountService.reserveScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1)
        );
    }

    @Test
    public void bookScoreWithNegativeAmount() {

        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = -23;
        int expectedBalanceFromAccount = fromAccount.getBalance() - amount;
        int expectedBalanceToAccount = toAccount.getBalance() + amount;
        long timestampGreaterThan = System.currentTimeMillis();
        Pair<AccountEntry, AccountEntry> result = accountService.bookScore(fromAccount, toAccount, postingText, amount,
                //just using any base entity, does not matter for that test
                th.personVgAdmin, "", th.pushCategory_app1_1);

        AccountEntry fromAccountEntry = result.getLeft();
        AccountEntry toAccountEntry = result.getRight();

        assertEquals(toAccount, fromAccountEntry.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntry.getAccountEntryType());
        assertEquals(amount, fromAccountEntry.getAmount());
        assertEquals(fromAccount, fromAccountEntry.getPartner());
        assertEquals(postingText, fromAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntry.getTimestamp());

        assertEquals(fromAccount, toAccountEntry.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntry.getAccountEntryType());
        assertEquals(-amount, toAccountEntry.getAmount());
        assertEquals(toAccount, toAccountEntry.getPartner());
        assertEquals(postingText, toAccountEntry.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntry.getTimestamp());

        final AccountEntry fromAccountEntryRepository = accountEntryRepository.findById(fromAccountEntry.getId()).get();
        final AccountEntry toAccountEntryRepository = accountEntryRepository.findById(toAccountEntry.getId()).get();

        assertEquals(toAccount, fromAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.MINUS, fromAccountEntryRepository.getAccountEntryType());
        assertEquals(amount, fromAccountEntryRepository.getAmount());
        assertEquals(fromAccount, fromAccountEntryRepository.getPartner());
        assertEquals(postingText, fromAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= fromAccountEntryRepository.getTimestamp());

        assertEquals(fromAccount, toAccountEntryRepository.getAccount());
        assertEquals(AccountEntryType.PLUS, toAccountEntryRepository.getAccountEntryType());
        assertEquals(-amount, toAccountEntryRepository.getAmount());
        assertEquals(toAccount, toAccountEntryRepository.getPartner());
        assertEquals(postingText, toAccountEntryRepository.getPostingText());
        assertTrue(timestampGreaterThan <= toAccountEntryRepository.getTimestamp());

        final Account fromAccountTest = personAccountRepository.findById(fromAccount.getId()).get();
        final Account toAccountTest = personAccountRepository.findById(toAccount.getId()).get();

        assertEquals(expectedBalanceFromAccount, fromAccountTest.getBalance());
        assertEquals(expectedBalanceToAccount, toAccountTest.getBalance());
    }

    @Test
    public void bookScoreNullFromAccount() {

        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        //null fromAccount
        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookScore(null, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void bookScoreNonExistingFromAccount() {

        //non-existing fromAccount
        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        personAccountRepository.deleteById(fromAccount.getId());

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void bookScoreNullToAccount() {

        Account fromAccount = th.accountPersonVgAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        //null toAccount
        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookScore(fromAccount, null, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void bookScoreNonExistingToAccount() {

        //non-existing toAccount
        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20;

        personAccountRepository.deleteById(toAccount.getId());

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void bookScoreCreditsLimitExceeded() {

        Account fromAccount = th.accountPersonVgAdmin;
        Account toAccount = th.accountPersonIeseAdmin;
        String postingText = "Testing accountService - reserveScore && internalBookScore";
        int amount = 20000000;

        assertThrows(AccountCreditLimitExceededException.class, () ->
                accountService.bookScore(fromAccount, toAccount, postingText, amount,
                        //just using any base entity, does not matter for that test
                        th.personVgAdmin, "", th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullFromAccountEntry() {

        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        //null fromAccountEntry
        assertThrows(AccountEntryNotFoundException.class, () ->
                accountService.bookReservedScore(null, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullToAccountEntry() {

        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;

        //null toAccountEntry
        assertThrows(AccountEntryNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, null, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNonExistingAccount() {

        //non-existing account
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        personAccountRepository.deleteById(fromAccountEntry.getAccount().getId());

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNonExistingPartner() {

        //non-existing partner
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        personAccountRepository.deleteById(fromAccountEntry.getPartner().getId());

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullAccountInFromAccountEntry() {

        //null account in FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setAccount(null);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullPartnerInFromAccountEntry() {

        //null partner in FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setPartner(null);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullAccountInToAccountEntry() {

        //null account in toAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAccount(null);

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreNullPartnerInToAccountEntry() {

        //null partner in toAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setPartner(null);

        assertThrows(AccountNotFoundException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreCreditsLimitExceeded() {

        //credits limit exceeded while booking reserved score
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setAmount(-20000000);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAmount(20000000);

        assertThrows(AccountCreditLimitExceededException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreFromAccountEntryAlreadyBooked() {

        //fromAccountEntry already booked
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setAccountEntryType(AccountEntryType.MINUS);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(AccountEntryAlreadyBookedException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreToAccountEntryAlreadyBooked() {

        //toAccountEntry already booked
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAccountEntryType(AccountEntryType.PLUS);

        assertThrows(AccountEntryAlreadyBookedException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1)
        );
    }

    @Test
    public void bookReservedScoreFromAndToAccountEntryAreEqual() {

        //toAccountEntry already booked
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setAccountEntryType(AccountEntryType.RESERVATION_PLUS);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAccountEntryType(AccountEntryType.RESERVATION_PLUS);

        assertThrows(ReservationsNotMatchingException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreFromAccountEntryPartnerNotMatching() {

        //partner from FromAccountEntry does not match account from ToAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setPartner(fromAccountEntry.getAccount());
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(ReservationsNotMatchingException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreToAccountEntryPartnerNotMatching() {

        //partner from ToAccountEntry does not match account from FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setPartner(toAccountEntry.getAccount());

        assertThrows(ReservationsNotMatchingException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void bookReservedScoreAmmountsNotMatching() {

        //partner from ToAccountEntry does not match account from FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAmount(123456789);

        assertThrows(ReservationsNotMatchingException.class, () ->
                accountService.bookReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullFromAccountEntry() {

        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        //null FromAccountEntry
        assertThrows(AccountEntryNotFoundException.class, () ->
                accountService.freeReservedScore(null, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullToAccountEntry() {

        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;

        //null toAccountEntry
        assertThrows(AccountEntryNotFoundException.class, () ->
                accountService.freeReservedScore(fromAccountEntry, null, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullAccountInFromAccountEntry() {

        //null account in FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setAccount(null);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(AccountNotFoundException.class, () ->
                accountService.freeReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullPartnerInFromAccountEntry() {

        //null partner in FromAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        fromAccountEntry.setPartner(null);
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;

        assertThrows(AccountNotFoundException.class, () ->
                accountService.freeReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullAccountInToAccountEntry() {

        //null account in toAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setAccount(null);

        assertThrows(AccountNotFoundException.class, () ->
                accountService.freeReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void freeReservedScoreNullPartnerInToAccountEntry() {

        //null partner in toAccountEntry
        AccountEntry fromAccountEntry = th.seekingInquiryAccountEntry2_1;
        AccountEntry toAccountEntry = th.seekingInquiryAccountEntry2_2;
        toAccountEntry.setPartner(null);

        assertThrows(AccountNotFoundException.class, () ->
                accountService.freeReservedScore(fromAccountEntry, toAccountEntry, th.pushCategory_app1_1));
    }

    @Test
    public void recalculateAllAccountBalances() {

        assertEquals(0, accountService.recalculateAllAccountBalances(false));

        AccountEntry invalidAccountEntry = th.offerInquiryAccountEntry1_1;

        invalidAccountEntry.setAmount(invalidAccountEntry.getAmount() + 7);

        th.accountEntryRepository.save(invalidAccountEntry);

        assertEquals(7, accountService.recalculateAllAccountBalances(false));
        assertEquals(7, accountService.recalculateAllAccountBalances(true));

        assertEquals(0, accountService.recalculateAllAccountBalances(false));

    }

}
