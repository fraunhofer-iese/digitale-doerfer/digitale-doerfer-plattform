/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Adeline Silva Schäfer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shopping.ShoppingTestHelper;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shopping.ShoppingEmailTemplate;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import freemarker.template.TemplateException;

public class ShoppingEmailTest extends BaseServiceTest {

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private ShoppingTestHelper th;

    private Person carrier;
    private Delivery delivery;
    private PurchaseOrder purchaseOrder;
    private TransportAssignment transportAssignment;

    /**
     * Manually set to true to check the mails at %temp%/dd-tests/
     */
    private static final boolean EMAILS_TO_TEMP = false;

    public void init() throws Exception {

        carrier = th.personRegularKarl;
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();

        init();

    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    // At the moment, this tests only tests the creation of e-mail with the
    // corresponding template. A more complete test,
    // including the transportAssignment needs to be created.
    @Test
    public void generateMails() throws IOException, TemplateException {

        purchaseOrder = PurchaseOrder.builder().community(th.tenant1).sender(th.shop1).shopOrderId(th.shop1.getId())
                .receiver(th.personRegularAnna).build();

        TransportAssignment assignment = createTransportAssignment();
        Person carrier = assignment.getCarrier();
        Delivery delivery = assignment.getDelivery();

        Shop shop = purchaseOrder.getSender();
        String desiredDeliveryTime = TimeSpan.toFriendlyHumanReadableString(assignment.getDesiredDeliveryTime());
        String desiredPickupTime = toFriendlyHumanReadableString(assignment.getDesiredPickupTime());

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("shop", shop);
        templateModel.put("purchaseOrder", purchaseOrder);
        templateModel.put("delivery", delivery);
        templateModel.put("carrier", carrier);
        templateModel.put("desiredPickupTime", desiredPickupTime);
        templateModel.put("desiredDeliveryTime", desiredDeliveryTime);
        templateModel.put("contactPerson", th.contactPersonTenant1);

        String mail = emailSenderService.createTextWithTemplate(ShoppingEmailTemplate.TRANSPORT_ASSIGNMENT_CREATED,
                templateModel);
        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "transportAssignmentCreated");

        mail = emailSenderService.createTextWithTemplate(ShoppingEmailTemplate.TRANSPORT_ASSIGNMENT_CANCELLED,
                templateModel);

        assertTrue(StringUtils.isNotEmpty(mail));
        writeToFile(mail, "transportAssignmentCancelled");

    }

    private TransportAssignment createTransportAssignment() {

        Person receiver = th.personRegularAnna;

        ParcelAddress pickupAddress = new ParcelAddress(th.shop1.getAddresses().iterator().next().getAddress(), th.shop1);

        ParcelAddress receiverAddress = new ParcelAddress(receiver.getAddresses().iterator().next().getAddress(),
                receiver);

        TimeSpan desiredPickupTime = new TimeSpan(System.currentTimeMillis(),
                System.currentTimeMillis() + TimeUnit.HOURS.toMillis(3L));
        TimeSpan desiredDeliveryTime = new TimeSpan(System.currentTimeMillis(),
                System.currentTimeMillis() + TimeUnit.HOURS.toMillis(4L));

        delivery = Delivery.builder().trackingCodeLabelURL("URL tracking code URL")
                .trackingCodeLabelA4URL("URL A4 tracking code URL").trackingCode("12345")
                .desiredDeliveryTime(desiredDeliveryTime).pickupAddress(pickupAddress).deliveryAddress(receiverAddress)
                .build();

        transportAssignment = TransportAssignment.builder().carrier(carrier).delivery(delivery)
                .desiredDeliveryTime(desiredDeliveryTime).desiredPickupTime(desiredPickupTime).build();

        return transportAssignment;
    }

    private String toFriendlyHumanReadableString(TimeSpan timespan) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy").withZone(ZoneOffset.UTC);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(ZoneOffset.UTC);

        Instant endTime = Instant.ofEpochMilli(timespan.getEndTime());
        return dateFormatter.format(endTime) + " um " + timeFormatter.format(endTime) + " Uhr";
    }

    private void writeToFile(String content, String fileName) throws IOException {

        if (EMAILS_TO_TEMP) {
            File dir = new File(System.getProperty("java.io.tmpdir") + "/dd-tests/");
            dir.mkdir();

            try (Writer writer = new OutputStreamWriter(new FileOutputStream(new File(dir, fileName + ".html")),
                    StandardCharsets.UTF_8)) {
                writer.write(content);
            }
        }
    }

}
