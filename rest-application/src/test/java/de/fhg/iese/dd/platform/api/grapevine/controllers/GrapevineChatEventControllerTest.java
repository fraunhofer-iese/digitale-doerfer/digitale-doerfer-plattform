/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer, Jannis von Albedyll, Adeline Schäfer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageReceivedEvent;
import de.fhg.iese.dd.platform.api.communication.clientevent.ClientChatMessageSendRequest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatParticipant;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnCommentConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientChatStartWithSuggestionWorkerConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.ClientChatStartWithSuggestionWorkerRequest;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GrapevineChatEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private SuggestionRepository suggestionRepository;

    @Override
    public void createEntities() {

        th.createGrapevineScenarioWithoutSuggestions();
    }

    private static Set<Person> participantsAsPersons(final Chat chat) {
        return chat.getParticipants().stream()
                .map(ChatParticipant::getPerson)
                .collect(Collectors.toSet());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void chatStartOnPostRequest() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your post! 💛";
        long sentTime = System.currentTimeMillis();

        assertNotEquals(creator, chatter);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message(message)
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(post, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());

        waitForEventProcessing();

        ClientChatMessageReceivedEvent clientChatMessageReceivedEvent =
                testClientPushService.getPushedEventToPersonLoud(
                        chatter,
                        creator,
                        ClientChatMessageReceivedEvent.class,
                        new PushCategory.PushCategoryId(th.pushCategoryChatMessageReceived.getId()));

        assertEquals(clientChatMessageReceivedEvent.getChatId(), chatStartConfirmation.getChat().getId());
    }

    @Test
    public void chatStartOnPostRequest_Happening() throws Exception {

        Post post = th.happening1;
        Person creator = post.getCreator();
        Person chatter = th.personLaraSchaefer;
        String message = "I want to join the party, do I need to put clothes on? 👖";
        long sentTime = System.currentTimeMillis();

        assertThat(creator).isNotNull();
        assertNotEquals(creator, chatter);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message(message)
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(post, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());

        waitForEventProcessing();

        ClientChatMessageReceivedEvent clientChatMessageReceivedEvent =
                testClientPushService.getPushedEventToPersonLoud(
                        chatter,
                        creator,
                        ClientChatMessageReceivedEvent.class,
                        new PushCategory.PushCategoryId(th.pushCategoryChatMessageReceived.getId()));

        assertEquals(clientChatMessageReceivedEvent.getChatId(), chatStartConfirmation.getChat().getId());
    }

    @Test
    public void chatStartOnPostRequest_ParallelWorldInhabitant() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        Person parallelWorldInhabitant = th.personNinaBauer;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        assertNotEquals(creator, parallelWorldInhabitant);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message("Viel spass beim beim Sperren ich komme wieder ok lach")
                .sentTime(System.currentTimeMillis())
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(parallelWorldInhabitant, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        waitForEventProcessing();

        // no push messages sent
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void chatStartOnPostRequest_TwoParallelWorldInhabitants() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        creator.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(creator);

        Person parallelWorldInhabitant = th.personNinaBauer;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        assertNotEquals(creator, parallelWorldInhabitant);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message("Du alte Mumie!")
                .sentTime(System.currentTimeMillis())
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(parallelWorldInhabitant, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        // verify push message
        waitForEventProcessing();

        // two parallel world inhabitants can talk to each other
        testClientPushService.getPushedEventToPersonLoud(
                parallelWorldInhabitant,
                creator,
                ClientChatMessageReceivedEvent.class,
                new PushCategory.PushCategoryId(th.pushCategoryChatMessageReceived.getId()));
    }

    @Test
    public void chatStartOnPostRequest_WithImage() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        Person chatter = th.personAnnikaSchneider;
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(chatter, th.nextTimeStamp(), th.nextTimeStamp());

        long sentTime = System.currentTimeMillis();

        assertNotEquals(creator, chatter);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .temporaryMediaItemId(tempImage1.getId())
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(post, creator, chatter, null, sentTime, 2, 0, chatStartConfirmation.getPostId(),
                chatStartConfirmation.getChat());
        assertTrue(th.mediaItemRepository.existsById(tempImage1.getMediaItem().getId()));
    }

    @Test
    public void chatStartOnPostRequest_ExistingChatIntermediateMessage() throws Exception {

        Post firstPost = th.gossip1withImages;
        Post secondPost = th.gossip2WithComments;
        assertEquals(firstPost.getCreator(), secondPost.getCreator());
        Person creator = firstPost.getCreator();
        Person chatter = th.personAnnikaSchneider;
        assertNotEquals(creator, chatter);
        String message = "Hihihi! 💛";
        long sentTime = System.currentTimeMillis();

        //first chat start request on firstPost
        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnPostRequest.builder()
                        .postId(firstPost.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(firstPost, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());

        //add response from creator (intermediate message)
        sentTime += 1000;
        message = "Lustig, was?";
        mockMvc.perform(post("/communication/chat/event/chatMessageSendRequest")
                .headers(authHeadersFor(creator))
                .contentType(contentType)
                .content(json(ClientChatMessageSendRequest.builder()
                        .chatId(chatStartConfirmation.getChat().getId())
                        .sentTime(sentTime)
                        .message(message)
                        .build())))
                .andExpect(status().isOk());

        //second chat start request on secondPost
        sentTime += 1000;
        message = "Findest Du wirklich?";
        startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnPostRequest.builder()
                        .postId(secondPost.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(secondPost, creator, chatter, message, sentTime, 5, 1,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());
    }

    @Test
    public void chatStartOnPostRequest_ExistingChat() throws Exception {

        Post firstPost = th.gossip1withImages;
        Post secondPost = th.gossip2WithComments;
        assertEquals(firstPost.getCreator(), secondPost.getCreator());
        Person creator = firstPost.getCreator();
        Person chatter = th.personAnnikaSchneider;
        assertNotEquals(creator, chatter);
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(chatter, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(chatter, th.nextTimeStamp(), th.nextTimeStamp());
        long sentTime = System.currentTimeMillis();

        //first chat start request on firstPost
        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnPostRequest.builder()
                        .postId(firstPost.getId())
                        .temporaryMediaItemId(tempImage1.getId())
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(firstPost, creator, chatter, null, sentTime, 2, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());
        assertTrue(th.mediaItemRepository.existsById(tempImage1.getMediaItem().getId()));

        //second chat start request on secondPost
        sentTime += 1000;
        startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnPostRequest.builder()
                        .postId(secondPost.getId())
                        .temporaryMediaItemId(tempImage2.getId())
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(secondPost, creator, chatter, null, sentTime, 4, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());
        assertTrue(th.mediaItemRepository.existsById(tempImage2.getMediaItem().getId()));
    }

    @Test
    public void chatStartOnPostRequest_OwnPost() throws Exception {

        Post post = th.gossip2WithComments;
        Person creator = post.getCreator();
        Person chatter = creator;
        String message = "Wann habe ich das denn geschrieben? 🦕";
        long sentTime = System.currentTimeMillis();

        assertEquals(creator, chatter);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
            .postId(post.getId())
            .message(message)
            .sentTime(sentTime)
            .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        assertThatChatIsInCorrectState(post, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getPostId(), chatStartConfirmation.getChat());
    }

    @Test
    public void chatStartOnPostRequest_ContentCreationRestricted() throws Exception {

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/chat/event/chatStartOnPostRequest")
                        .contentType(contentType)
                        .content(json(ClientChatStartOnPostRequest.builder()
                                .postId(th.gossip1withImages.getId())
                                .message("Bist du noch single?")
                                .sentTime(System.currentTimeMillis())
                                .build())));
    }

    private void assertThatChatIsInCorrectState(BaseEntity referencedEntity, Person postCreator, Person chatter,
            String message, long sentTime, int messageCount, int unreadMessageCount,
            String referencedEntityIdFromConfirmation, ClientChat chatFromConfirmation) {

        List<Chat> chats = chatRepository.findAll();
        assertEquals(1, chats.size());

        final boolean isMonologue = postCreator.equals(chatter);
        final int expectedNrOfParticipants = isMonologue ? 1 : 2;

        Chat chat = chats.get(0);
        final Set<Person> participants = participantsAsPersons(chat);
        assertEquals(expectedNrOfParticipants, participants.size());
        assertTrue(participants.contains(postCreator));
        assertTrue(participants.contains(chatter));
        assertEquals(1, chat.getSubjects().size());

        List<ChatMessage> chatMessages = chatMessageRepository.findAll(Sort.by(Sort.Direction.ASC, "sentTime"));
        //actual message + autogenerated message
        assertEquals(messageCount, chatMessages.size());
        final int messageIndex = firstIndexOf(chatMessages, cm -> !cm.isAutoGenerated() && cm.getSentTime() == sentTime);
        assertTrue(messageIndex > 0);
        ChatMessage personChatMessage = chatMessages.get(messageIndex);
        ChatMessage entityReferenceChatMessage = chatMessages.get(messageIndex-1);

        assertEquals(chat, personChatMessage.getChat());
        assertEquals(chatter, personChatMessage.getSender());
        assertEquals(message, personChatMessage.getMessage());
        assertEquals(sentTime, personChatMessage.getSentTime());
        assertNull(personChatMessage.getReferencedEntityId());
        assertNull(personChatMessage.getReferencedEntityName());

        assertEquals(chat, entityReferenceChatMessage.getChat());
        assertEquals(chatter, entityReferenceChatMessage.getSender());
        assertNull(entityReferenceChatMessage.getMessage());
        assertEquals(sentTime, entityReferenceChatMessage.getSentTime());
        assertEquals(referencedEntity.getId(), entityReferenceChatMessage.getReferencedEntityId());
        assertEquals(referencedEntity.getClass().getName(), entityReferenceChatMessage.getReferencedEntityName());

        //the autogenerated chat message should be just a glimpse earlier
        assertThat(personChatMessage.getCreated()).isGreaterThan(entityReferenceChatMessage.getCreated());

        assertEquals(referencedEntity.getId(), referencedEntityIdFromConfirmation);
        assertEquals(chat.getId(), chatFromConfirmation.getId());
        assertEquals(expectedNrOfParticipants, chatFromConfirmation.getChatParticipants().size());
        assertThat(chatFromConfirmation.getChatParticipants().get(0).getPerson().getId())
                .isIn((postCreator.getId()), chatter.getId());
        if (!isMonologue) {
            assertThat(chatFromConfirmation.getChatParticipants().get(1).getPerson().getId())
                    .isIn(postCreator.getId(), chatter.getId());
        }
        assertEquals(personChatMessage.getId(), chatFromConfirmation.getLastMessage().getId());
        assertEquals(message, chatFromConfirmation.getLastMessage().getMessage());
        assertEquals(chatter.getId(), chatFromConfirmation.getLastMessage().getSenderId());
        assertEquals(chatter.getFirstName(), chatFromConfirmation.getLastMessage().getSenderFirstName());
        assertEquals(shorten(chatter.getLastName()), chatFromConfirmation.getLastMessage().getSenderLastName());
        assertEquals(personChatMessage.getSentTime(), chatFromConfirmation.getLastMessage().getSentTime());
        assertEquals(unreadMessageCount, chatFromConfirmation.getUnreadMessageCount());
    }

    private <T> int firstIndexOf(List<T> collection, Predicate<T> predicate) {
        int i = 0;
        for (T element : collection) {
            if (predicate.test(element)) {
                return i;
            }
            i++;
        }
        throw new NoSuchElementException("No element matching the filter in the collection");
    }

    @Test
    public void chatStartOnPostRequest_SentTimeOutOfRange() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your post! 💛";
        //assume the message was sent days ago
        long actualSentTime = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1);
        //this is the earliest sent time that would be acceptable, it should be close to NOW
        long earliestSentTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(2);

        assertTrue(actualSentTime < earliestSentTime);

        assertNotEquals(creator, chatter);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
            .postId(post.getId())
            .message(message)
            .sentTime(actualSentTime)
            .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        List<ChatMessage> chatMessages = chatMessageRepository.findAll();
        //actual message + autogenerated message
        assertEquals(2, chatMessages.size());
        ChatMessage personChatMessage = chatMessages.stream().filter(c -> !c.isAutoGenerated()).findFirst().get();
        ChatMessage entityReferenceChatMessage = chatMessages.stream().filter(ChatMessage::isAutoGenerated).findFirst().get();

        assertTrue(personChatMessage.getSentTime() > earliestSentTime);
        assertTrue(entityReferenceChatMessage.getSentTime() > earliestSentTime);

        assertEquals(personChatMessage.getSentTime(), chatStartConfirmation.getChat().getLastMessage().getSentTime());
        assertEquals(0, chatStartConfirmation.getChat().getUnreadMessageCount());
    }

    @Test
    public void chatStartOnPostRequest_InvalidParameters() throws Exception {

        Post post = th.gossip1withImages;
        Person creator = post.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your post! 💛";
        long sentTime = System.currentTimeMillis();

        assertNotEquals(creator, chatter);

        //postId null
        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(null)
                .message(message)
                .sentTime(sentTime)
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(chatStartOnPostRequest)))
                .andExpect(isException("postId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //post has no creator
        Post postNoCreator = th.happening2;
        assertThat(postNoCreator.getCreator()).isNull();
        chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(postNoCreator.getId())
                .message(message)
                .sentTime(sentTime)
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(chatStartOnPostRequest)))
                .andExpect(isException(ClientExceptionType.POST_HAS_NO_CREATOR));

        //postId empty
        chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId("")
                .message(message)
                .sentTime(sentTime)
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(isException("postId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //postId not existing
        chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
            .postId("0815")
            .message(message)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(isException(chatStartOnPostRequest.getPostId(), ClientExceptionType.POST_NOT_FOUND));

        //message null and no image
        chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
            .postId(post.getId())
            .message(null)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(isException("message", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //message empty and no image
        chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
            .postId(post.getId())
            .message("")
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(isException("message", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void chatStartOnPostRequest_Unauthorized() throws Exception {

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(th.gossip1withImages.getId())
                .message("message")
                .sentTime(System.currentTimeMillis())
                .build();

        assertOAuth2AppVariantRequired(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .contentType(contentType)
                        .content(json(chatStartOnPostRequest)),
                th.appVariant4AllTenants,
                th.personAnnikaSchneider);
    }

    @Test
    public void chatStartOnCommentRequest() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your comment! 💛";
        long sentTime = System.currentTimeMillis();

        assertNotEquals(creator, chatter);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
                .commentId(comment.getId())
                .message(message)
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnCommentConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        assertThatChatIsInCorrectState(comment, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getCommentId(), chatStartConfirmation.getChat());

        waitForEventProcessing();

        ClientChatMessageReceivedEvent clientChatMessageReceivedEvent =
                testClientPushService.getPushedEventToPersonLoud(
                        chatter,
                        creator,
                        ClientChatMessageReceivedEvent.class,
                        new PushCategory.PushCategoryId(th.pushCategoryChatMessageReceived.getId()));

        assertEquals(clientChatMessageReceivedEvent.getChatId(), chatStartConfirmation.getChat().getId());
    }

    @Test
    public void chatStartOnCommentRequest_ParallelWorldInhabitant() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person parallelWorldInhabitant = th.personNinaBauer;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        assertNotEquals(creator, parallelWorldInhabitant);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
                .commentId(comment.getId())
                .message("wenn ich dir treffe da hau dir paar aufs maul ok")
                .sentTime(System.currentTimeMillis())
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(parallelWorldInhabitant, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        waitForEventProcessing();

        // no push messages sent
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void chatStartOnCommentRequest_TwoParallelWorldInhabitants() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        creator.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(creator);

        Person parallelWorldInhabitant = th.personNinaBauer;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        assertNotEquals(creator, parallelWorldInhabitant);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
                .commentId(comment.getId())
                .message("Mach dir bloß vom. Acker")
                .sentTime(System.currentTimeMillis())
                .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(parallelWorldInhabitant, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        waitForEventProcessing();

        // two parallel world inhabitants can talk to each other
        testClientPushService.getPushedEventToPersonLoud(
                parallelWorldInhabitant,
                creator,
                ClientChatMessageReceivedEvent.class,
                new PushCategory.PushCategoryId(th.pushCategoryChatMessageReceived.getId()));
    }

    @Test
    public void chatStartOnCommentRequest_WithImage() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person chatter = th.personAnnikaSchneider;
        long sentTime = System.currentTimeMillis();
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(chatter, th.nextTimeStamp(), th.nextTimeStamp());

        assertNotEquals(creator, chatter);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
                .commentId(comment.getId())
                .temporaryMediaItemId(tempImage1.getId())
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnCommentConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        assertThatChatIsInCorrectState(comment, creator, chatter, null, sentTime, 2, 0,
                chatStartConfirmation.getCommentId(), chatStartConfirmation.getChat());
        assertTrue(th.mediaItemRepository.existsById(tempImage1.getMediaItem().getId()));
    }

    @Test
    public void chatStartOnCommentRequest_OwnComment() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person chatter = creator;
        String message = "I have a question about your comment! 💛";
        long sentTime = System.currentTimeMillis();

        assertEquals(creator, chatter);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId(comment.getId())
            .message(message)
            .sentTime(sentTime)
            .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnCommentConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        assertThatChatIsInCorrectState(comment, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getCommentId(), chatStartConfirmation.getChat());
    }

    @Test
    public void chatStartOnCommentRequest_ContentCreationRestricted() throws Exception {

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/chat/event/chatStartOnCommentRequest")
                        .contentType(contentType)
                        .content(json(ClientChatStartOnCommentRequest.builder()
                                .commentId(th.gossip2Comment1.getId())
                                .message("Lust auf einen Kaffee? ☕")
                                .sentTime(System.currentTimeMillis())
                                .build())));
    }

    @Test
    public void chatStartOnCommentRequest_ExistingChatIntermediateMessage() throws Exception {

        Comment comment1 = th.gossip2Comment1;
        Comment comment2 = th.gossip2Comment4withImages;
        Person creator = comment1.getCreator();
        assertEquals(comment1.getCreator(), comment2.getCreator());
        Person chatter = th.personAnnikaSchneider;
        assertNotEquals(creator, chatter);
        String message = "I have a question about your comment! 💛";
        TemporaryMediaItem tempImage = th.createTemporaryMediaItem(chatter, th.nextTimeStamp(), th.nextTimeStamp());
        long sentTime = System.currentTimeMillis();

        //start chat on comment1
        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnCommentRequest.builder()
                        .commentId(comment1.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnCommentConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        assertThatChatIsInCorrectState(comment1, creator, chatter, message, sentTime, 2, 0,
                chatStartConfirmation.getCommentId(), chatStartConfirmation.getChat());

        //add response from creator (intermediate message)
        sentTime += 1000;
        message = "Can you send me the image?";
        mockMvc.perform(post("/communication/chat/event/chatMessageSendRequest")
                .headers(authHeadersFor(creator))
                .contentType(contentType)
                .content(json(ClientChatMessageSendRequest.builder()
                        .chatId(chatStartConfirmation.getChat().getId())
                        .sentTime(sentTime)
                        .message(message)
                        .build())))
                .andExpect(status().isOk());

        //start chat on comment2
        sentTime += 1000;
        startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientChatStartOnCommentRequest.builder()
                        .commentId(comment2.getId())
                        .temporaryMediaItemId(tempImage.getId())
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        assertThatChatIsInCorrectState(comment2, creator, chatter, null, sentTime, 5, 1,
                chatStartConfirmation.getCommentId(), chatStartConfirmation.getChat());
        assertTrue(th.mediaItemRepository.existsById(tempImage.getMediaItem().getId()));
    }

    @Test
    public void chatStartOnCommentRequest_SentTimeOutOfRange() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your comment! 💛";
        //assume the message was sent days ago
        long actualSentTime = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1);
        //this is the earliest sent time that would be acceptable, it should be close to NOW
        long earliestSentTime = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(2);

        assertTrue(actualSentTime < earliestSentTime);

        assertNotEquals(creator, chatter);

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId(comment.getId())
            .message(message)
            .sentTime(actualSentTime)
            .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnCommentConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnCommentConfirmation.class);

        List<ChatMessage> chatMessages = chatMessageRepository.findAll();
        //actual message + autogenerated message
        assertEquals(2, chatMessages.size());
        ChatMessage personChatMessage = chatMessages.stream().filter(c -> !c.isAutoGenerated()).findFirst().get();
        ChatMessage entityReferenceChatMessage = chatMessages.stream().filter(ChatMessage::isAutoGenerated).findFirst().get();

        assertTrue(personChatMessage.getSentTime() > earliestSentTime);
        assertTrue(entityReferenceChatMessage.getSentTime() > earliestSentTime);

        assertEquals(personChatMessage.getSentTime(), chatStartConfirmation.getChat().getLastMessage().getSentTime());
        assertEquals(0, chatStartConfirmation.getChat().getUnreadMessageCount());
    }

    @Test
    public void chatStartOnCommentRequest_InvalidParameters() throws Exception {

        Comment comment = th.gossip2Comment1;
        Person creator = comment.getCreator();
        Person chatter = th.personAnnikaSchneider;
        String message = "I have a question about your comment! 💛";
        long sentTime = System.currentTimeMillis();

        assertNotEquals(creator, chatter);

        //commentId null
        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId(null)
            .message(message)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(isException("commentId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //commentId empty
        chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId("")
            .message(message)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(isException("commentId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //commentId not existing
        chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId("0815")
            .message(message)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        //message null
        chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId(comment.getId())
            .message(null)
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(isException("message", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //message empty
        chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
            .commentId(comment.getId())
            .message("")
            .sentTime(sentTime)
            .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartOnCommentRequest")
                .headers(authHeadersFor(chatter, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnCommentRequest)))
                .andExpect(isException("message", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void chatStartOnCommentRequest_Unauthorized() throws Exception {

        ClientChatStartOnCommentRequest chatStartOnCommentRequest = ClientChatStartOnCommentRequest.builder()
                .commentId(th.gossip2Comment1.getId())
                .message("message")
                .sentTime(System.currentTimeMillis())
                .build();

        assertOAuth2AppVariantRequired(post("/grapevine/chat/event/chatStartOnCommentRequest")
                        .contentType(contentType)
                        .content(json(chatStartOnCommentRequest)),
                th.appVariant4AllTenants,
                th.personAnnikaSchneider);
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_onlyMessage() throws Exception {

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final String message = "Ich brauch mahl deinn Rad";
        final long sentTime = System.currentTimeMillis();

        final ClientChatStartWithSuggestionWorkerRequest chatStartWithWorkerRequest =
                ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(chatStartWithWorkerRequest)))
                .andExpect(status().isOk())
                .andReturn();

        final ClientChatStartWithSuggestionWorkerConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatWorkerChatIsCreatedCorrectly(chatInitiator, addressedWorker, message, sentTime, 1, 0,
                chatStartConfirmation.getChat(), null);
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_referenceToSuggestionOnlyMessage() throws Exception {

        Suggestion suggestion = Suggestion.builder()
                .text("This is a suggestion")
                .build();
        suggestion = suggestionRepository.saveAndFlush(suggestion);

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final String message = "Ich brauch mahl deinn Rad";
        final long sentTime = System.currentTimeMillis();

        assertThatChatWithWrongSuggestionIdFails(addressedWorker, message, chatInitiator);

        final ClientChatStartWithSuggestionWorkerRequest chatStartWithWorkerRequest =
                ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .suggestionId(suggestion.getId())
                        .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(chatStartWithWorkerRequest)))
                .andExpect(status().isOk())
                .andReturn();

        final ClientChatStartWithSuggestionWorkerConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatChatIsInCorrectState(suggestion, addressedWorker, chatInitiator, message, sentTime, 2, 0,
                suggestion.getId(), chatStartConfirmation.getChat());
    }

    public void assertThatChatWithWrongSuggestionIdFails(Person addressedWorker, String message, Person chatInitiator) throws Exception {
        final long sentTime = System.currentTimeMillis();
        final String wrongSuggestionId = "abc";

        final ClientChatStartWithSuggestionWorkerRequest chatStartWithWorkerRequest =
                ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .suggestionId(wrongSuggestionId)
                        .build();

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(chatStartWithWorkerRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_onlyImage() throws Exception {

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final long sentTime = System.currentTimeMillis();
        final TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(chatInitiator, th.nextTimeStamp(),
                th.nextTimeStamp());

        final ClientChatStartWithSuggestionWorkerRequest chatStartWithWorkerRequest =
                ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .temporaryMediaItemId(tempImage1.getId())
                        .sentTime(sentTime)
                        .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(chatStartWithWorkerRequest)))
                .andExpect(status().isOk())
                .andReturn();

        final ClientChatStartWithSuggestionWorkerConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatWorkerChatIsCreatedCorrectly(chatInitiator, addressedWorker, null, sentTime, 1, 0,
                chatStartConfirmation.getChat(), null);
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_messageAndImage() throws Exception {

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final String message = "Hilfe, bin ins Buffet gefallen";
        final long sentTime = System.currentTimeMillis();
        final TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(chatInitiator, th.nextTimeStamp(),
                th.nextTimeStamp());

        final ClientChatStartWithSuggestionWorkerRequest chatStartWithWorkerRequest =
                ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .temporaryMediaItemId(tempImage1.getId())
                        .sentTime(sentTime)
                        .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(chatStartWithWorkerRequest)))
                .andExpect(status().isOk())
                .andReturn();

        final ClientChatStartWithSuggestionWorkerConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatWorkerChatIsCreatedCorrectly(chatInitiator, addressedWorker, message, sentTime, 1, 0,
                chatStartConfirmation.getChat(), null);
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_IntermediateMessage() throws Exception {

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        String message = "Ich brauch mahl deinn Rad";
        long sentTime = System.currentTimeMillis();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartWithSuggestionWorkerConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatWorkerChatIsCreatedCorrectly(chatInitiator, addressedWorker, message, sentTime, 1, 0,
                chatStartConfirmation.getChat(), null);

        //add response from creator (intermediate message)
        sentTime += 1000;
        message = "Wozu?";
        mockMvc.perform(post("/communication/chat/event/chatMessageSendRequest")
                .headers(authHeadersFor(addressedWorker))
                .contentType(contentType)
                .content(json(ClientChatMessageSendRequest.builder()
                        .chatId(chatStartConfirmation.getChat().getId())
                        .sentTime(sentTime)
                        .message(message)
                        .build())))
                .andExpect(status().isOk());

        //start chat on comment2
        sentTime += 1000;
        startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();

        chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartWithSuggestionWorkerConfirmation.class);

        assertThatWorkerChatIsCreatedCorrectly(chatInitiator, addressedWorker, message, sentTime, 3, 1,
                chatStartConfirmation.getChat(), null);
    }

    private void assertThatWorkerChatIsCreatedCorrectly(Person chatInitiator, Person chatPartner, String message,
            long sentTime, int expectedNrOfMessages, int expectedNrOfUnreadMessages, ClientChat chatFromConfirmation,
            TemporaryMediaItem temporaryMediaItem) {
        final List<Chat> chats = chatRepository.findAll();

        assertEquals(1, chats.size());
        final Chat chat = chats.get(0);
        final Set<Person> participants = participantsAsPersons(chat);
        assertEquals(2, participants.size());
        assertTrue(participants.contains(chatInitiator));
        assertTrue(participants.contains(chatPartner));
        assertEquals(1, chat.getSubjects().size());

        final List<ChatMessage> chatMessages = chatMessageRepository.findAll();
        //actual message + no autogenerated message
        assertEquals(expectedNrOfMessages, chatMessages.size());
        final ChatMessage personChatMessage = chatMessages.stream()
                .filter(cm -> !cm.isAutoGenerated() && cm.getSentTime() == sentTime)
                .findFirst()
                .get();

        assertEquals(chat, personChatMessage.getChat());
        assertEquals(chatInitiator, personChatMessage.getSender());
        assertEquals(message, personChatMessage.getMessage());
        assertEquals(sentTime, personChatMessage.getSentTime());
        assertNull(personChatMessage.getReferencedEntityId());
        assertNull(personChatMessage.getReferencedEntityName());

        assertEquals(chat.getId(), chatFromConfirmation.getId());
        final List<ClientChatParticipant> chatParticipantsFromConfirmation =
                chatFromConfirmation.getChatParticipants();
        assertEquals(2, chatParticipantsFromConfirmation.size());
        assertTrue(chatParticipantsFromConfirmation
                .stream()
                .map(ccp -> ccp.getPerson().getId())
                .anyMatch(id -> chatInitiator.getId().equals(id)));
        assertTrue(chatParticipantsFromConfirmation
                .stream()
                .map(ccp -> ccp.getPerson().getId())
                .anyMatch(id -> chatPartner.getId().equals(id)));
        assertEquals(personChatMessage.getId(), chatFromConfirmation.getLastMessage().getId());
        assertEquals(message, chatFromConfirmation.getLastMessage().getMessage());
        assertEquals(chatInitiator.getId(), chatFromConfirmation.getLastMessage().getSenderId());
        assertEquals(personChatMessage.getSentTime(), chatFromConfirmation.getLastMessage().getSentTime());
        assertEquals(expectedNrOfUnreadMessages, chatFromConfirmation.getUnreadMessageCount());

        if (temporaryMediaItem != null) {
            assertTrue(th.mediaItemRepository.existsById(temporaryMediaItem.getMediaItem().getId()));
        }
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_invalidAttributes() throws Exception {

        final Person chatInitiator = th.personSuggestionWorkerTenant1Dieter;
        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final String message = "Ich brauch mahl deinn Rad";
        final long sentTime = System.currentTimeMillis();

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message("")
                        .sentTime(sentTime)
                        .build())))
                .andExpect(isException("message",
                        ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(null)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(isException(
                        "message",
                        ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(null)
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(chatInitiator))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId("invalidPersonId")
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void chatStartWithSuggestionWorkerRequest_Unauthorized() throws Exception {

        final Person addressedWorker = th.personSuggestionWorkerTenant1Horst;
        final String message = "Ich brauch mahl deinn Rad";
        final long sentTime = System.currentTimeMillis();

        assertOAuth2(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())));

        mockMvc.perform(post("/grapevine/chat/event/chatStartWithSuggestionWorkerRequest")
                .headers(authHeadersFor(th.personSusanneChristmann))
                .contentType(contentType)
                .content(json(ClientChatStartWithSuggestionWorkerRequest
                        .builder()
                        .workerPersonId(addressedWorker.getId())
                        .message(message)
                        .sentTime(sentTime)
                        .build())))
                .andExpect(isNotAuthorized());
    }

}
