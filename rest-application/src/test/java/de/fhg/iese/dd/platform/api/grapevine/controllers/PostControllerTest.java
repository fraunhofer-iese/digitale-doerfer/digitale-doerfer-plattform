/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostControllerTest extends BasePostControllerTest {

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getPostById_Gossip() throws Exception {

        Person caller = th.personSusanneChristmann;
        Gossip gossip = th.gossip1withImages;

        assertThat(gossip.getCreator()).isNotNull();
        mockMvc.perform(get("/grapevine/post/{postId}", gossip.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, gossip));

        gossip.setOrganization(th.organizationFeuerwehr);
        th.postRepository.saveAndFlush(gossip);
        mockMvc.perform(get("/grapevine/post/{postId}", gossip.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, gossip));
    }

    @Test
    public void getPostById_CorrectCommentCounts() throws Exception {

        for (Map.Entry<String, Integer> gossipIdAndCommentCount : th.gossipIdToCommentCount().entrySet()) {
            log.debug("checking post {}", gossipIdAndCommentCount.getKey());
            mockMvc.perform(get("/grapevine/post/{postId}", gossipIdAndCommentCount.getKey())
                            .headers(authHeadersFor(th.personThomasBecker, th.appVariant4AllTenants)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.gossip.id").value(gossipIdAndCommentCount.getKey()))
                    .andExpect(jsonPath("$.gossip.commentCount").value(gossipIdAndCommentCount.getValue()));
        }
    }

    @Test
    public void getPostById_NewsItem() throws Exception {
        Person caller = th.personSusanneChristmann;
        NewsItem newsItem1 = th.newsItem1;
        NewsItem newsItem2 = th.newsItem2;
        NewsItem newsItem3 = th.newsItem3;
        newsItem3.setNewsSource(null);
        th.postRepository.saveAndFlush(newsItem3);

        assertThat(newsItem1.getCreator()).isNotNull();
        mockMvc.perform(get("/grapevine/post/{postId}", newsItem1.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, newsItem1));

        assertThat(newsItem2.getCreator()).isNull();
        mockMvc.perform(get("/grapevine/post/{postId}", newsItem2.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, newsItem2));

        assertThat(newsItem3.getNewsSource()).isNull();
        mockMvc.perform(get("/grapevine/post/{postId}", newsItem3.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, newsItem3));
    }

    @Test
    public void getPostById_NewsItem_TemplatedNewsSource() throws Exception {

        Person caller = th.personSusanneChristmann;
        NewsItem newsItem = th.newsItem1;
        NewsSource newsSource = newsItem.getNewsSource();
        newsSource.setSiteName("Neues aus $post.geoArea.name$ frisch auf den Tisch");
        th.newsSourceRepository.saveAndFlush(newsSource);
        String expectedSiteName = "Neues aus " + newsItem.getGeoAreas().iterator().next()
                .getName() + " frisch auf den Tisch";

        mockMvc.perform(get("/grapevine/post/{postId}", newsItem.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, newsItem))
                .andExpect(jsonPath("$.newsItem.siteName").value(expectedSiteName));
    }

    @Test
    public void getPostById_Happening() throws Exception {

        Person caller = th.personSusanneChristmann;
        Happening happening1 = th.happening1;
        Happening happening2 = th.happening2;
        Happening happening3 = th.happening3;
        happening3.setNewsSource(null);
        th.postRepository.saveAndFlush(happening3);

        assertThat(happening1.getCreator()).isNotNull();
        mockMvc.perform(get("/grapevine/post/{postId}", happening1.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, happening1));

        assertThat(happening2.getCreator()).isNull();
        mockMvc.perform(get("/grapevine/post/{postId}", happening2.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, happening2));

        assertThat(happening3.getNewsSource()).isNull();
        mockMvc.perform(get("/grapevine/post/{postId}", happening3.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, happening3));
    }

    @Test
    public void getPostById_Happening_TemplatedNewsSource() throws Exception {
        Person caller = th.personSusanneChristmann;
        Happening happening = th.happening1;
        NewsSource newsSource = happening.getNewsSource();
        newsSource.setSiteName("Spannende Veranstaltung aus $post.geoArea.name$ zum Teilnehmen");
        th.newsSourceRepository.saveAndFlush(newsSource);
        String expectedSiteName = "Spannende Veranstaltung aus " + happening.getGeoAreas().iterator().next()
                .getName() + " zum Teilnehmen";

        mockMvc.perform(get("/grapevine/post/{postId}", happening.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, happening))
                .andExpect(jsonPath("$.happening.siteName").value(expectedSiteName));
    }

    @Test
    public void getPostById_Suggestion() throws Exception {

        Person caller = th.personSusanneChristmann;
        Suggestion suggestion = th.suggestionDefect2;

        mockMvc.perform(get("/grapevine/post/{postId}", suggestion.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, suggestion));
    }

    @Test
    public void getPostById_InternalSuggestion() throws Exception {

        Person caller = th.personLaraSchaefer;
        Suggestion suggestion = th.suggestionDefect2;
        suggestion.setInternal(true);
        th.postRepository.saveAndFlush(suggestion);
        assertThat(suggestion.getCreator()).isNotEqualTo(caller);

        //not visible as post on DorfFunk endpoints for any caller
        mockMvc.perform(get("/grapevine/post/{postId}", suggestion.getId())
                .contentType(contentType)
                .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
        //not visible as post on DorfFunk endpoints even for the creator
        mockMvc.perform(get("/grapevine/post/{postId}", suggestion.getId())
                .contentType(contentType)
                .headers(authHeadersFor(suggestion.getCreator(), th.appVariant4AllTenants)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
        //not visible as post on DorfFunk endpoints even for a suggestion worker
        mockMvc.perform(get("/grapevine/post/{postId}", suggestion.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter, th.appVariant4AllTenants)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getPostById_Seeking() throws Exception {

        Person caller = th.personSusanneChristmann;
        Seeking seeking = th.seeking1;

        mockMvc.perform(get("/grapevine/post/{postId}", seeking.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, seeking));
    }

    @Test
    public void getPostById_Offer() throws Exception {

        Person caller = th.personSusanneChristmann;
        Offer offer = th.offer1;

        mockMvc.perform(get("/grapevine/post/{postId}", offer.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, offer));
    }

    @Test
    public void getPostById_SpecialPost() throws Exception {

        Person caller = th.personSusanneChristmann;
        SpecialPost specialPost = th.specialPost2withComments;

        mockMvc.perform(get("/grapevine/post/{postId}", specialPost.getId())
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostAttributes(caller, specialPost));
    }

    @Test
    public void getPostById_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/post/{id}", th.tenant2Gossip1.getId()),
                th.appVariant1Tenant1, th.personSusanneChristmann);
    }

    @Test
    public void getAllExternalPostsByGeoAreas() throws Exception {

        final GeoArea geoArea = th.geoAreaTenant1;
        int pageSize = 2;

        // Only NewsItems
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p.getGeoAreas().contains(geoArea) &&
                        p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished())
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedNewsItems.size());
        final NewsItem firstNewsItem = expectedNewsItems.get(0);

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                        .param("geoAreaIds", geoArea.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(expectedNewsItems, 0, pageSize));

        // Only Happenings
        final List<Happening> expectedHappenings = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p.getGeoAreas().contains(geoArea) &&
                        p instanceof Happening &&
                        p.isNotDeleted() &&
                        ((Happening) p).isPublished())
                .map(Happening.class::cast)
                .sorted(Comparator.comparingLong(Happening::getStartTime))
                .collect(Collectors.toList());

        assertEquals(2, expectedHappenings.size());
        final Happening firstHappening = expectedHappenings.get(0);

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                        .param("geoAreaIds", geoArea.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "HAPPENING_START"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedHappenings.size()))
                .andExpect(assertPostsInOrder(expectedHappenings, 0, pageSize));
    }

    @Test
    public void getAllExternalPostsByGeoAreas_DeletedAreFiltered() throws Exception {

        final GeoArea geoArea = th.geoAreaTenant1;
        int pageSize = 1;

        // Only NewsItems
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p.getGeoAreas().contains(geoArea) &&
                        p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished())
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedNewsItems.size());
        final NewsItem firstNewsItem = expectedNewsItems.get(0);
        final NewsItem secondNewsItem = expectedNewsItems.get(1);
        secondNewsItem.setDeleted(true);
        th.postRepository.saveAndFlush(secondNewsItem);
        expectedNewsItems.remove(secondNewsItem);

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                        .param("geoAreaIds", geoArea.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(expectedNewsItems, 0, pageSize));

    }

    @Test
    public void getAllExternalPostsByGeoAreas_OutOfRange() throws Exception {

        final GeoArea geoArea = th.geoAreaTenant1;
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p.getGeoAreas().contains(geoArea) &&
                        p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished())
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedNewsItems.size());
        int pageSize = 2;

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getId())
                .param("page", "1")
                .param("count", String.valueOf(pageSize))
                .param("postTypes", "NEWS_ITEM")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0)))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.totalElements").value(expectedNewsItems.size()));
    }

    @Test
    public void getAllExternalPostsByGeoAreas_PageParameterInvalid() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;

        // Maximum number of posts smaller than 1
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("page", String.valueOf(0))
                .param("count", String.valueOf(0))
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Page number smaller than 0
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("page", String.valueOf(-1))
                .param("count", String.valueOf(10))
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Maximum number of posts smaller than 1 and page number smaller than 0
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("page", String.valueOf(-1))
                .param("count", String.valueOf(0))
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getAllExternalPostsByGeoAreas_InvalidSortingCriterion() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;

        // Invalid PostSortCriteria for NewsItems
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "NEWS_ITEM")
                .param("sortBy", "INVALID"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "NEWS_ITEM")
                .param("sortBy", "HAPPENING_START"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        // Invalid PostSortCriteria for Happenings
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "HAPPENING")
                .param("sortBy", "INVALID"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        // Invalid PostSortCriteria for both
        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("sortBy", "LAST_ACTIVITY"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("sortBy", "LAST_SUGGESTION_ACTIVITY"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void getAllExternalPostsByGeoAreas_InvalidPostType() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "INVALID")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "GOSSIP")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "SUGGESTION")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "SEEKING")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", geoArea.getName())
                .param("postTypes", "OFFER")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAllExternalPostsByGeoAreas_MissingGeoAreaIds() throws Exception {

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("postTypes", "HAPPENING")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                .param("geoAreaIds", "")
                .param("postTypes", "HAPPENING")
                .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAllExternalPostsByNewsSource() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;
        int pageSize = 2;

        // Only NewsItems
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished() &&
                        ((NewsItem) p).getNewsSource().getId().equals(newsSource.getId()))
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedNewsItems.size());

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(expectedNewsItems, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(expectedNewsItems, 1, pageSize));

        // Only Happenings
        final List<Happening> expectedHappenings = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p instanceof Happening &&
                        p.isNotDeleted() &&
                        ((Happening) p).isPublished() &&
                        ((Happening) p).getNewsSource().getId().equals(newsSource.getId()))
                .map(Happening.class::cast)
                .sorted(Comparator.comparingLong(Happening::getStartTime))
                .collect(Collectors.toList());

        assertEquals(4, expectedHappenings.size());
        final Happening firstHappening = expectedHappenings.get(0);

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "HAPPENING_START"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedHappenings.size()))
                .andExpect(assertPostsInOrder(expectedHappenings, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "HAPPENING_START"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedHappenings.size()))
                .andExpect(assertPostsInOrder(expectedHappenings, 1, pageSize));

        //empty newsSource
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", th.newsSourceAnalogheim.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.totalElements").value(0));
    }

    @Test
    public void getAllExternalPostsByNewsSource_DeletedAreFiltered() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;
        int pageSize = 4;

        // Only NewsItems
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished() &&
                        ((NewsItem) p).getNewsSource().getId().equals(newsSource.getId()))
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedNewsItems.size());
        final NewsItem firstNewsItem = expectedNewsItems.get(0);
        final NewsItem secondNewsItem = expectedNewsItems.get(1);
        secondNewsItem.setDeleted(true);
        th.postRepository.saveAndFlush(secondNewsItem);
        expectedNewsItems.remove(secondNewsItem);

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(expectedNewsItems, 0, pageSize));
    }

    @Test
    public void getAllExternalPostsByNewsSource_OutOfRange() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;
        final List<NewsItem> expectedNewsItems = th.postRepository.findAllFull()
                .stream()
                .filter(p -> p instanceof NewsItem &&
                        p.isNotDeleted() &&
                        ((NewsItem) p).isPublished() &&
                        ((NewsItem) p).getNewsSource().getId().equals(newsSource.getId()))
                .map(NewsItem.class::cast)
                .sorted(Comparator.comparingLong(NewsItem::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedNewsItems.size());
        int pageSize = 4;

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(0)))
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.totalElements").value(expectedNewsItems.size()));
    }

    @Test
    public void getAllExternalPostsByNewsSource_PageParameterInvalid() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;

        // Maximum number of posts smaller than 1
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(0))
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Page number smaller than 0
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(10))
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Maximum number of posts smaller than 1 and page number smaller than 0
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(0))
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getAllExternalPostsByNewsSource_InvalidSortingCriterion() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;

        // Invalid PostSortCriteria for NewsItems
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "INVALID"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "NEWS_ITEM")
                        .param("sortBy", "HAPPENING_START"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        // Invalid PostSortCriteria for Happenings
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "INVALID"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        // Invalid PostSortCriteria for both
        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("sortBy", "LAST_ACTIVITY"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("sortBy", "LAST_SUGGESTION_ACTIVITY"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void getAllExternalPostsByNewsSource_InvalidPostType() throws Exception {

        final NewsSource newsSource = th.newsSourceDigitalbach;

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "INVALID")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "GOSSIP")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "SUGGESTION")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByGeoAreas")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "SEEKING")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("newsSourceId", newsSource.getId())
                        .param("postTypes", "OFFER")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAllExternalPostsByNewsSource_MissingNewsSourceId() throws Exception {

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/post/externalByNewsSource")
                        .param("geoAreaIds", "")
                        .param("postTypes", "HAPPENING")
                        .param("sortBy", "CREATED"))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

}
