/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GrapevineExternalIdMigrationAdminTaskTest extends BaseServiceTest {

    @Autowired
    private IAdminTaskService adminTaskService;

    @Autowired
    private GrapevineExternalIdMigrationAdminTask grapevineExternalIdMigrationAdminTask;

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createOrganizations();
        th.createNewsSources();
        th.createNewsItemsAndComments();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void migrateFromURLWithDot() {

        th.newsItem1.setExternalId("https://dev-dorfnews.digitale-doerfer.de/.123");
        th.externalPostRepository.saveAndFlush(th.newsItem1);

        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(grapevineExternalIdMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals("123", th.externalPostRepository.findById(th.newsItem1.getId()).get().getExternalId());
    }

    @Test
    public void migrateFromURLWithUnderscore() {

        th.newsItem1.setExternalId("https://dev-dorfnews.digitale-doerfer.de_123");
        th.externalPostRepository.saveAndFlush(th.newsItem1);

        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(grapevineExternalIdMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals("123", th.externalPostRepository.findById(th.newsItem1.getId()).get().getExternalId());
    }

}
