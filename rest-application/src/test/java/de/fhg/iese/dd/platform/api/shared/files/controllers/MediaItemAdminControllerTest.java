/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.TestMediaItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MediaItemAdminControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestMediaItemService testMediaItemService;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void downloadMediaItem() throws Exception {

        MediaItem expectedMediaItem = th.createMediaItem("image");
        URL imageUrl = new URL("https://example.org/image");

        testMediaItemService.addTestMediaItem(imageUrl, expectedMediaItem);

        mockMvc.perform(post("/administration/media/download")
                        .param("url", imageUrl.toString())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.mediaItem.id").value(expectedMediaItem.getId()));

        List<TemporaryMediaItem> temporaryMediaItems = th.temporaryMediaItemRepository.findAll();
        assertThat(temporaryMediaItems).hasSize(1);
        assertThat(temporaryMediaItems.get(0).getMediaItem()).isEqualTo(expectedMediaItem);
    }

    @Test
    public void downloadMediaItem_Unencoded() throws Exception {

        MediaItem expectedMediaItem = th.createMediaItem("image");
        String givenUrl = "https://example.org/image - blank";
        String expectedCalledUrl = "https://example.org/image%20-%20blank";

        testMediaItemService.addTestMediaItem(new URL(expectedCalledUrl), expectedMediaItem);

        mockMvc.perform(post("/administration/media/download")
                        .param("url", givenUrl)
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.mediaItem.id").value(expectedMediaItem.getId()));

        List<TemporaryMediaItem> temporaryMediaItems = th.temporaryMediaItemRepository.findAll();
        assertThat(temporaryMediaItems).hasSize(1);
        assertThat(temporaryMediaItems.get(0).getMediaItem()).isEqualTo(expectedMediaItem);
    }

    @Test
    public void downloadMediaItem_Encoded() throws Exception {

        MediaItem expectedMediaItem = th.createMediaItem("image");
        String givenUrl = "https://example.org/image%20-%20blank";
        String expectedCalledUrl = "https://example.org/image%20-%20blank";

        testMediaItemService.addTestMediaItem(new URL(expectedCalledUrl), expectedMediaItem);

        mockMvc.perform(post("/administration/media/download")
                        .param("url", givenUrl)
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.mediaItem.id").value(expectedMediaItem.getId()));

        List<TemporaryMediaItem> temporaryMediaItems = th.temporaryMediaItemRepository.findAll();
        assertThat(temporaryMediaItems).hasSize(1);
        assertThat(temporaryMediaItems.get(0).getMediaItem()).isEqualTo(expectedMediaItem);
    }

    @Test
    public void downloadMediaItem_Unauthorized() throws Exception {

        mockMvc.perform(post("/administration/media/download")
                        .param("url", "xxx")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/administration/media/download")
                .param("url", "xxx"));
    }

}
