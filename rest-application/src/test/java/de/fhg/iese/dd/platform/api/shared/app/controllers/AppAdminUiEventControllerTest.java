/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantTenantContractChangeAdditionalNotesRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;

public class AppAdminUiEventControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private AppVariantTenantContract contract;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        contract = th.appVariantTenantContractRepository.save(AppVariantTenantContract.builder()
                .appVariant(th.app1Variant1)
                .tenant(th.tenant1)
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void changeAdditionalNotes() throws Exception {

        final String changedNotes = "My new additional notes";
        AppVariantTenantContract existingContract =
                th.appVariantTenantContractRepository.findById(contract.getId()).get();
        assertThat(existingContract.getAdditionalNotes()).isNotEqualTo(changedNotes);

        ClientAppVariantTenantContractChangeAdditionalNotesRequest request =
                ClientAppVariantTenantContractChangeAdditionalNotesRequest.builder()
                        .contractId(contract.getId())
                        .changedNotes(changedNotes)
                        .build();

        mockMvc.perform(post("/adminui/app/appVariantTenantContractChangeAdditionalNotesRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("updatedAppVariantTenantContract.additionalNotes").value(request.getChangedNotes()))
                .andExpect(jsonPath("updatedAppVariantTenantContract.id").value(request.getContractId()));

        AppVariantTenantContract changedContract =
                th.appVariantTenantContractRepository.findById(request.getContractId()).get();
        assertThat(changedContract.getAdditionalNotes()).isEqualTo(changedNotes);
    }

    @Test
    public void changeAdditionalNotes_Unauthorized() throws Exception {

        final String changedNotes = "test";

        ClientAppVariantTenantContractChangeAdditionalNotesRequest request =
                ClientAppVariantTenantContractChangeAdditionalNotesRequest.builder()
                        .contractId(contract.getId())
                        .changedNotes(changedNotes)
                        .build();

        mockMvc.perform(post("/adminui/app/appVariantTenantContractChangeAdditionalNotesRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/app/appVariantTenantContractChangeAdditionalNotesRequest")
                .contentType(contentType)
                .content(json(request)));

    }

    @Test
    public void changeAdditionalNotes_InvalidContractId() throws Exception {

        final String wrongContractId = "StringThatIsNoId";
        final String changedNotes = "test";

        ClientAppVariantTenantContractChangeAdditionalNotesRequest request =
                ClientAppVariantTenantContractChangeAdditionalNotesRequest.builder()
                        .contractId(wrongContractId)
                        .changedNotes(changedNotes)
                        .build();

        mockMvc.perform(post("/adminui/app/appVariantTenantContractChangeAdditionalNotesRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_TENANT_CONTRACT_NOT_FOUND));
    }

    @Test
    public void changeAdditionalNotes_emptyNotes() throws Exception {

        final String changedNotes = "";
        AppVariantTenantContract existingContract =
                th.appVariantTenantContractRepository.findById(contract.getId()).get();
        assertThat(existingContract.getAdditionalNotes()).isNotEqualTo(changedNotes);

        ClientAppVariantTenantContractChangeAdditionalNotesRequest request =
                ClientAppVariantTenantContractChangeAdditionalNotesRequest.builder()
                        .contractId(contract.getId())
                        .changedNotes(changedNotes)
                        .build();

        mockMvc.perform(post("/adminui/app/appVariantTenantContractChangeAdditionalNotesRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("updatedAppVariantTenantContract.additionalNotes").isEmpty())
                .andExpect(jsonPath("updatedAppVariantTenantContract.id").value(request.getContractId()));

        AppVariantTenantContract changedContract =
                th.appVariantTenantContractRepository.findById(request.getContractId()).get();
        assertThat(changedContract.getAdditionalNotes()).isEqualTo(null);
    }

}
