/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Danielle Korth, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GeoAreaControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getGeoArea() throws Exception {

        final GeoArea expectedGeoArea = th.geoAreaRheinlandPfalz;

        mockMvc.perform(get("/geoArea/{id}", expectedGeoArea.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientGeoArea(expectedGeoArea, 1, false)));
    }

    @Test
    public void getGeoArea_IncludeBoundaryPoints() throws Exception {

        final GeoArea expectedGeoArea = th.geoAreaRheinlandPfalz;

        mockMvc.perform(get("/geoArea/{id}", expectedGeoArea.getId())
                .param("includeBoundaryPoints", "true")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientGeoAreaWithBoundaryPoints(expectedGeoArea, 1, false)));
    }

    @Test
    public void getGeoArea_InvalidId() throws Exception {

        mockMvc.perform(get("/geoArea/{id}", BaseTestHelper.INVALID_UUID))
                .andExpect(isException(BaseTestHelper.INVALID_UUID, ClientExceptionType.GEO_AREA_NOT_FOUND));
    }

    @Test
    public void getGeoAreas() throws Exception {

        mockMvc.perform(get("/geoAreas")
                .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId(),
                        th.geoAreaMainz.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Arrays.asList(th.toClientGeoArea(th.geoAreaEisenberg, 2, false),
                        th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                        th.toClientGeoArea(th.geoAreaMainz, 2, true))));
    }

    @Test
    public void getGeoAreas_IncludeBoundaryPoints() throws Exception {

        mockMvc.perform(get("/geoAreas")
                .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId(),
                        th.geoAreaMainz.getId())
                .param("includeBoundaryPoints", "true")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(
                        Arrays.asList(th.toClientGeoAreaWithBoundaryPoints(th.geoAreaEisenberg, 2, false),
                                th.toClientGeoAreaWithBoundaryPoints(th.geoAreaKaiserslautern, 2, true),
                                th.toClientGeoAreaWithBoundaryPoints(th.geoAreaMainz, 2, true))));
    }

    @Test
    public void getGeoAreas_ParentAndChildGeoAreas() throws Exception {

        mockMvc.perform(get("/geoAreas")
                        .param("geoAreaIds", th.geoAreaRheinlandPfalz.getId(), th.geoAreaMainz.getId(),
                                th.geoAreaKaiserslautern.getId())
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(Collections.singletonList(th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                        th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                        th.toClientGeoArea(th.geoAreaMainz, 2, true)))));
    }

    @Test
    public void getGeoAreas_InvalidId() throws Exception {

        mockMvc.perform(get("/geoAreas")
                .param("geoAreaIds", th.geoAreaMainz.getId(), BaseTestHelper.INVALID_UUID))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.GEO_AREA_NOT_FOUND,
                        BaseTestHelper.INVALID_UUID));
    }

}
