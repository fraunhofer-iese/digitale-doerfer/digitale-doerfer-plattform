/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostEventControllerGossipTest extends BasePostEventControllerTest<Gossip> {

    protected AppVariant appVariantContentCreationRestricted;
    protected EnumSet<PersonVerificationStatus> allowedVerificationStatuses =
            EnumSet.of(PersonVerificationStatus.PHONE_NUMBER_VERIFIED, PersonVerificationStatus.EMAIL_VERIFIED);

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return new ClientGossipChangeRequest();
    }

    @Override
    protected Gossip getTestPost1() {
        return th.gossip4;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personLaraSchaefer;
    }

    @Override
    protected Gossip getTestPost2() {
        return th.gossip5;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.offer1;
    }

    private void createFeatureContentCreationRestriction() {
        appVariantContentCreationRestricted = th.appVariant1Tenant1;

        String allowedStatusArray = allowedVerificationStatuses.stream()
                .map(PersonVerificationStatus::name)
                .collect(Collectors.joining("\",\"", "[\"", "\"]"));

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantContentCreationRestricted)
                .enabled(true)
                .featureClass(ContentCreationRestrictionFeature.class.getName())
                .configValues(
                        "{\"allowedStatusesAnyOf\": " + allowedStatusArray + " }")
                .build());
    }

    @Test
    public void gossipCreateRequest_VerifyingPushMessage() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());
        String text = "gothic text 😈";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        String postId = gossipCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(postId))
                .andExpect(jsonPath("$.gossip.commentsAllowed").value(true))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.gossip.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the gossip is also added to the repository
        Gossip gossipFromRepo = (Gossip) th.postRepository.findById(postId).get();

        assertThat(gossipFromRepo.getCreator()).isEqualTo(postCreator);
        assertEquals(createdLocation.getLatitude(), gossipFromRepo.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), gossipFromRepo.getCreatedLocation().getLongitude());
        assertEquals(text, gossipFromRepo.getText());
        assertEquals(postCreator.getTenant(), gossipFromRepo.getTenant());
        assertTrue(gossipFromRepo.getImages() == null || gossipFromRepo.getImages().isEmpty());
        assertThat(gossipFromRepo.getGeoAreas()).containsExactly(postCreator.getHomeArea());
        assertFalse(gossipFromRepo.isHiddenForOtherHomeAreas());
        assertTrue(gossipFromRepo.isCommentsAllowed());
        assertThat(gossipFromRepo.isParallelWorld()).isFalse();

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, gossipFromRepo.getGeoAreas(), false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED);

        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPost().getGossip().getId());
        //check that the last name is cut
        assertEquals(postCreator.getFirstName(), pushedEvent.getPost().getGossip().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(postCreator.getLastName()),
                pushedEvent.getPost().getGossip().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void gossipCreateRequest_ContentCreationRestricted() throws Exception {

        //this is not tested using assertIsPersonVerificationStatusRestricted in order to test it in more detail

        createFeatureContentCreationRestriction();

        Person postCreator = th.personThomasBecker;

        assertTrue(postCreator.getVerificationStatuses().getValues().isEmpty(), "Post creator needs to be unverified");

        String text = "Salat! 🥗";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .build();

        //no verification at all
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //set the verification status to one that is not in the list of allowed statuses
        postCreator.getVerificationStatuses().addValue(
                EnumSet.complementOf(allowedVerificationStatuses).iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //one of the allowed verifications
        postCreator.getVerificationStatuses().addValue(allowedVerificationStatuses.iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the gossip
        mockMvc.perform(get("/grapevine/post/" + gossipCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipCreateConfirmation.getPost().getGossip().getId()))
                .andExpect(jsonPath("$.gossip.id").value(gossipCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()));
    }

    @Test
    public void gossipCreateRequest_ParallelWorld() throws Exception {

        Person postCreator = th.personThomasBecker;
        postCreator.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(postCreator);

        String text = "ICH REG MICH GRUNDLOS AUF!!!!";

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();

        assertThat(addedGossip.isParallelWorld()).isTrue();

        // Get the gossip
        mockMvc.perform(get("/grapevine/post/" + gossipCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipCreateConfirmation.getPost().getGossip().getId()))
                .andExpect(jsonPath("$.gossip.id").value(gossipCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()));
    }

    @Test
    public void gossipCreateRequest_TooBigPushMessage() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());
        String text = "Hier ist meine Dia-Sammlung!";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i < th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(th.createTemporaryMediaItemWithMaxUrls(postCreator, 0).getId());
        }

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(temporaryIds)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();

        assertNotNull(addedGossip);

        // verify push message
        waitForEventProcessing();

        //this event can only be pushed if it was minimized
        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedGossip.getGeoAreas(), false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED);

        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPost().getGossip().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void gossipCreateRequest_WithImagesAndDocumentsVerifyingPushMessage() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        assertNotNull(postCreator.getHomeArea());
        String text = "Hier ist meine Dia-Sammlung und alle meine geheimen Dokumente";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //GPSLocation customLocation = new GPSLocation(0.8d, 1.5d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images and documents
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                //.customLocation(customLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(gossipCreateConfirmation.getPost().getGossip().getText()).isEqualTo(text);
        assertThat(gossipCreateConfirmation.getPost().getGossip().getImages()).hasSize(3);
        assertThat(gossipCreateConfirmation.getPost().getGossip().getAttachments()).hasSize(3);

        // Get the gossip
        //check new entity uses images and documents
        String postId = gossipCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(postId))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.gossip.images", hasSize(3)))
                .andExpect(jsonPath("$.gossip.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.gossip.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.gossip.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.gossip.attachments", hasSize(3)))
                .andExpect(jsonPath("$.gossip.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.gossip.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.gossip.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist())
                //.andExpect(jsonPath("$.gossip.customLocation.latitude").value(customLocation.getLatitude()))
                //.andExpect(jsonPath("$.gossip.customLocation.longitude").value(customLocation.getLongitude()))
                .andExpect(jsonPath("$.gossip.hiddenForOtherHomeAreas").value(true));

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(postId).get();

        assertEquals(postCreator, addedGossip.getCreator());
        assertEquals(createdLocation.getLatitude(), addedGossip.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedGossip.getCreatedLocation().getLongitude());
        //assertEquals(customLocation, addedGossip.getCustomAddress().getGpsLocation());
        assertEquals(text, addedGossip.getText());
        assertEquals(postCreator.getTenant(), addedGossip.getTenant());
        assertEquals(3, addedGossip.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedGossip.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedGossip.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedGossip.getImages().get(2));
        assertThat(addedGossip.getAttachments()).hasSize(3);
        assertThat(addedGossip.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(addedGossip.getGeoAreas()).containsExactly(postCreator.getHomeArea());
        assertTrue(addedGossip.isHiddenForOtherHomeAreas());

        //check used temp images and documents are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        //check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedGossip.getGeoAreas(), true,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED);

        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(gossipCreateConfirmation.getPostId(), pushedEvent.getPost().getGossip().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void gossipCreateRequest_InvalidImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "gothic text 😈";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .build();

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void gossipCreateRequest_InvalidDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "gothic text 😈";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempDocumentItemId = "invalid id";

        //create request using temp documents and invalid id
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidTempDocumentItemId))
                .build();

        //check temp documents not found exception
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("[" + invalidTempDocumentItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    @Test
    public void gossipCreateRequest_DuplicateImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "gothic text 😈";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using temp images and invalid id
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage2.getId()))
                .build();

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("[" + tempImage2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void gossipCreateRequest_DuplicateDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "gothic text 😈";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using temp documents and invalid id
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), tempDocument2.getId()))
                .build();

        //check temp document not found exception
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("[" + tempDocument2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    @Test
    public void gossipCreateRequest_OptionalAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        assertNotNull(person.getHomeArea());
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed chars!
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the gossip
        mockMvc.perform(get("/grapevine/post/" + gossipCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(person.getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist());

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();

        assertEquals(person, addedGossip.getCreator());
        assertEquals(createdLocation.getLatitude(), addedGossip.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedGossip.getCreatedLocation().getLongitude());
        assertNull(addedGossip.getCustomAddress());
        assertEquals(text, addedGossip.getText());
        assertEquals(person.getTenant(), addedGossip.getTenant());
        assertThat(addedGossip.getImages()).isNullOrEmpty();
        assertThat(addedGossip.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void gossipCreateRequest_NoCreatedLocation() throws Exception {

        Person person = th.personSusanneChristmann;
        assertNotNull(person.getHomeArea());
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed
        // chars!
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();

        assertEquals(person, addedGossip.getCreator());
        assertEquals(person.getHomeArea().getCenter(), addedGossip.getCreatedLocation());
        assertNull(addedGossip.getCustomAddress());
        assertEquals(text, addedGossip.getText());
        assertEquals(person.getTenant(), addedGossip.getTenant());
        assertTrue(addedGossip.getImages() == null || addedGossip.getImages().isEmpty());
        assertThat(addedGossip.getGeoAreas()).containsExactly(person.getHomeArea());
    }

    @Test
    public void gossipCreateRequest_ZeroZeroCreatedLocation() throws Exception {

        Person person = th.personSusanneChristmann;
        assertNotNull(person.getHomeArea());
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .createdLocation(new ClientGPSLocation(0.0, 0.0))
                .text(text)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the gossip is also added to the repository
        Gossip addedGossip = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();

        assertEquals(person, addedGossip.getCreator());
        assertEquals(person.getHomeArea().getCenter(), addedGossip.getCreatedLocation());
        assertNull(addedGossip.getCustomAddress());
        assertEquals(text, addedGossip.getText());
        assertEquals(person.getTenant(), addedGossip.getTenant());
        assertThat(addedGossip.getImages()).isNullOrEmpty();
        assertThat(addedGossip.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void gossipCreateRequest_Unauthorized() throws Exception {

        String text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //GPSLocation customLocation = new GPSLocation(0.8d, 1.5d);

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .build();

        assertOAuth2AppVariantRequired(post("/grapevine/event/gossipCreateRequest")
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)),
                th.appVariant4AllTenants,
                th.personSusanneChristmann);
    }

    @Test
    public void gossipCreateRequest_ExceedingMaxTextLength() throws Exception {

        Person person = th.personThomasBecker;
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //GPSLocation customLocation = new GPSLocation(0.8d, 1.5d);

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .build();

        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void gossipCreateRequest_ExceedingMaxNumberImages() throws Exception {

        Person person = th.personThomasBecker;
        String text = "gossip with too many images";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void gossipCreateRequest_ExceedingMaxNumberDocuments() throws Exception {

        Person person = th.personThomasBecker;
        String text = "gossip with too many documents";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void gossipCreateRequest_MissingAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        String text = "gossip with missing attributes";

        // missing text
        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing home area and no tenant
        person.setHomeArea(null);
        person = th.personRepository.save(person);
        gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .build();
        mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));
    }

    /* ***** CHANGE OPERATIONS *******/

    @Test
    public void gossipChangeRequest_TextVerifyingPushMessage() throws Exception {

        Person person = th.personSusanneChristmann;
        Gossip gossipToBeChanged = th.gossip6;

        String text = "this is the new text for gossip 6";

        ClientGossipChangeRequest gossipChangeRequest = ClientGossipChangeRequest.builder()
                .postId(gossipToBeChanged.getId())
                .text(text)
                .build();

        MvcResult createChangeResult = mockMvc.perform(post("/grapevine/event/gossipChangeRequest")
                        .headers(authHeadersFor(person, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(gossipChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(gossipChangeRequest.getPostId()))
                .andExpect(jsonPath("$.post.gossip.id").value(gossipChangeRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation gossipChangeConfirmation = toObject(createChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the gossip is also updated in the repository
        Gossip updatedGossip = (Gossip) th.postRepository.findById(gossipChangeConfirmation.getPost().getId()).get();
        assertEquals(text, updatedGossip.getText());

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(gossipToBeChanged.getGeoAreas(),
                        gossipToBeChanged.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(gossipChangeRequest.getPostId(), pushedEvent.getPostId());
        assertEquals(gossipChangeRequest.getPostId(), pushedEvent.getPost().getGossip().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void gossipChangeRequest_Images() throws Exception {

        Gossip gossipToBeChanged = th.gossip4;
        Person creator = gossipToBeChanged.getCreator();
        final List<MediaItem> existingImages = gossipToBeChanged.getImages();

        //create additional temp image
        TemporaryMediaItem newTempMediaItem =
                th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        ClientGossipChangeRequest gossipChangeRequest = ClientGossipChangeRequest.builder()
                .postId(gossipToBeChanged.getId())
                .mediaItemPlaceHolders(Collections.singletonList(
                        ClientMediaItemPlaceHolder.builder()
                                .temporaryMediaItemId(newTempMediaItem.getId()).build()))
                .build();

        mockMvc.perform(post("/grapevine/event/gossipChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(gossipChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(gossipChangeRequest.getPostId()))
                .andExpect(jsonPath("$.post.gossip.id").value(gossipChangeRequest.getPostId()));

        // Check whether the gossip is also updated in the repository
        Gossip updatedGossip = (Gossip) th.postRepository.findById(gossipToBeChanged.getId()).get();
        assertThat(updatedGossip.getImages()).contains(newTempMediaItem.getMediaItem());

        //check if the old images are deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContainAnyElementsOf(existingImages);
    }

    @Test
    public void gossipChangeRequest_NotOwner() throws Exception {

        Gossip gossipToBeChanged = th.gossip4;
        Person changingPerson = th.personLaraSchaefer;

        // Make sure that the changing person is not the owner
        assertNotSame(gossipToBeChanged.getCreator(), changingPerson);

        List<MediaItem> imagesToBeChanged = gossipToBeChanged.getImages();

        // Make sure there are images to change
        assertEquals(3, imagesToBeChanged.size());
        imagesToBeChanged.remove(0);
        MediaItem newImage = th.createMediaItem("newImageForGossip4");
        imagesToBeChanged.add(newImage);

        String text = "this is the new text for gossip 4";

        ClientGossipChangeRequest gossipChangeRequest = ClientGossipChangeRequest.builder()
                .postId(gossipToBeChanged.getId())
                .text(text)
                .build();

        mockMvc.perform(post("/grapevine/event/gossipChangeRequest")
                        .headers(authHeadersFor(changingPerson, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(gossipChangeRequest)))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));

        // Check whether the gossip is not updated in the repository
        Gossip gossipFromRepository = (Gossip) th.postRepository.findById(gossipToBeChanged.getId()).get();
        assertNotEquals(text, gossipFromRepository.getText());
        assertNotEquals(imagesToBeChanged, gossipFromRepository.getImages());
    }

}
