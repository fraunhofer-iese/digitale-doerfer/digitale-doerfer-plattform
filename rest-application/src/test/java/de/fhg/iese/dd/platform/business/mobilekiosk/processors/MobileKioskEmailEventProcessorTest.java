/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.mobilekiosk.processors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.fhg.iese.dd.platform.api.RestApplication;

@SpringBootTest(classes = RestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MobileKioskEmailEventProcessorTest {
    
    @Autowired
    private MobileKioskEmailEventProcessor mobileKioskEmailEventProcessor;
    
    @Test
    public void shopOrderIdIsExtractedCorrectlyFromBayFormat() {
        assertEquals("0", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_0_1"));
        assertEquals("1", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_1_2"));
        assertEquals("12", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_12_34"));
        assertEquals("123", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_123_0"));
        assertEquals("1234", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_1234_5"));
        assertEquals("54321", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_54321_0"));
    }

    @Test
    public void shopOrderIdIsExtractedCorrectlyFromBayFormatWithoutShopId() {
        assertEquals("0", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_0"));
        assertEquals("1", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_1"));
        assertEquals("12", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_12"));
        assertEquals("123", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_123"));
        assertEquals("1234", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_1234"));
        assertEquals("54321", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY_54321"));
    }
    
    @Test
    public void shopOrderIdIsExtractedCorrectlyFromStandardFormat() {
        assertEquals("0", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("abc_0_1"));
        assertEquals("1", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("ABC_1_2"));
        assertEquals("12", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("A_12_34"));
        assertEquals("123", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("CD_123_0"));
        assertEquals("1234", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("DEMO_1234_5"));
        assertEquals("54321", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("DBK_54321_0"));
    }
    
    @Test
    public void shopOrderIdIsExtractedCorrectlyFromStandardFormatWithoutShopId() {
        assertEquals("0", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("abc_0"));
        assertEquals("1", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("ABC_1"));
        assertEquals("12", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("A_12"));
        assertEquals("123", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("CD_123"));
        assertEquals("1234", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("DEMO_1234"));
        assertEquals("54321", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("DBK_54321"));
    }
    
    @Test
    public void shopOrderIdIsExtractedCorrectlyWhenAlreadyInCorrectFormat() {
        assertEquals("0", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("0"));
        assertEquals("1", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("1"));
        assertEquals("12", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("12"));
        assertEquals("123", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("123"));
        assertEquals("1234", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("1234"));
        assertEquals("54321", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("54321"));
    }
    
    @Test
    public void shopOrderIdIsReturnedUnchangedIfNotInCorrectFormat() {
        assertEquals("DEMO", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("DEMO"));
        assertEquals("BAY-1234-5", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("BAY-1234-5"));
        assertEquals("_", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("_"));
        assertEquals("__", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId("__"));
        assertEquals("", mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId(""));
        assertNull(mobileKioskEmailEventProcessor.extractOrderIdFromShopOrderId(null));
    }

}
