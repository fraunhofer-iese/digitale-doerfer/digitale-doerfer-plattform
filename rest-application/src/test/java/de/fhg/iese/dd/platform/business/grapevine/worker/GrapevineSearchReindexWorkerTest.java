/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
public class GrapevineSearchReindexWorkerTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;
    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private GrapevineSearchReindexWorker grapevineSearchReindexWorker;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws InterruptedException {

        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    @Test
    public void reindexFailedPosts() throws Exception {

        Post postToReindex1 = th.gossip2WithComments;
        Post postToReindex2 = th.happening2;
        Post postToDelete1 = th.gossip1withImages;
        Post postUnchanged1 = th.happening1;
        Post postUnchanged2 = th.gossip3WithComment;

        //the posts get indexed first
        //grapevineSearchService.updateIndexedPost(postToReindex1);
        //grapevineSearchService.updateIndexedPost(postToReindex2);
        grapevineSearchService.indexOrUpdateIndexedPost(postToDelete1);
        grapevineSearchService.indexOrUpdateIndexedPost(postUnchanged1);
        grapevineSearchService.indexOrUpdateIndexedPost(postUnchanged2);

        //indexing should have been successful or never done
        assertThat(th.postRepository.findById(postToReindex1.getId()).get().getSearchIndexFailed()).isNull();
        assertThat(th.postRepository.findById(postToReindex2.getId()).get().getSearchIndexFailed()).isNull();
        assertThat(th.postRepository.findById(postToDelete1.getId()).get().getSearchIndexFailed()).isNull();
        assertThat(th.postRepository.findById(postUnchanged1.getId()).get().getSearchIndexFailed()).isNull();
        assertThat(th.postRepository.findById(postUnchanged2.getId()).get().getSearchIndexFailed()).isNull();

        //check that the ones that were indexed are also in the search engine
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToReindex1.getId(), SearchPost.class))
                .isNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToReindex2.getId(), SearchPost.class))
                .isNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToDelete1.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postUnchanged1.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postUnchanged2.getId(), SearchPost.class))
                .isNotNull();

        //we manipulate the posts to be failed
        postToReindex1.setSearchIndexFailed(true);
        postToReindex2.setSearchIndexFailed(true);
        postToDelete1.setSearchIndexFailed(true);
        postToDelete1.setDeleted(true);
        postUnchanged1.setSearchIndexFailed(null);
        postUnchanged2.setSearchIndexFailed(false);

        th.postRepository.saveAll(Arrays.asList(postToReindex1, postToReindex2, postToDelete1,
                postUnchanged1, postUnchanged2));

        grapevineSearchReindexWorker.run();

        waitForEventProcessing();

        //the posts should be in the search engine or deleted
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToReindex1.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToReindex2.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postToDelete1.getId(), SearchPost.class))
                .isNull(); //has been deleted
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postUnchanged1.getId(), SearchPost.class))
                .isNotNull();
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postUnchanged2.getId(), SearchPost.class))
                .isNotNull();

        //all indexing should have been successful now
        assertThat(th.postRepository.findById(postToReindex1.getId()).get().getSearchIndexFailed()).isFalse();
        assertThat(th.postRepository.findById(postToReindex2.getId()).get().getSearchIndexFailed()).isFalse();
        assertThat(th.postRepository.findById(postToDelete1.getId()).get().getSearchIndexFailed()).isFalse();
        assertThat(th.postRepository.findById(postUnchanged1.getId()).get().getSearchIndexFailed()).isNull();
        assertThat(th.postRepository.findById(postUnchanged2.getId()).get().getSearchIndexFailed()).isFalse();
    }

}
