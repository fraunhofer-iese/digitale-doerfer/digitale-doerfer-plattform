/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class PushAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getPushEventOverviewNotAuthorized() throws Exception {

        assertOAuth2(get("/adminui/push/pushEvents"));

        mockMvc.perform(get("/adminui/push/pushEvents")
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void getPushEventOverview() throws Exception {

        mockMvc.perform(get("/adminui/push/pushEvents")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .contentType(contentType))
                .andExpect(status().isOk());

        mockMvc.perform(get("/adminui/push/pushEvents")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk());
    }

}
