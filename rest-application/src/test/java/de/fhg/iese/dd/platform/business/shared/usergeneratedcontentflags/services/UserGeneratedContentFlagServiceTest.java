/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.UserGeneratedContentFlagAlreadyExistsException;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.exceptions.UserGeneratedContentFlagNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagStatusRecordRepository;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserGeneratedContentFlagServiceTest extends BaseServiceTest {

    @Autowired
    public UserGeneratedContentFlagRepository flagRepository;
    @Autowired
    public UserGeneratedContentFlagStatusRecordRepository flagStatusRecordRepository;
    @Autowired
    private SharedTestHelper th;

    @Autowired
    private IUserGeneratedContentFlagService flagService;

    private UserGeneratedContentFlag flagTenant1;
    private BaseEntity flagTenant1FlaggedEntity;
    private String flagTenant1InitialComment;
    public UserGeneratedContentFlag flagTenant2;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createUserGeneratedContentAdmins();
        createUserGeneratedContentFlags();

        flagTenant1InitialComment = flagTenant1.getUserGeneratedContentFlagStatusRecords().get(0).getComment();
    }

    public void createUserGeneratedContentFlags() {

        flagTenant1FlaggedEntity = th.personVgAdmin;

        flagTenant1 = flagRepository.save(UserGeneratedContentFlag.builder()
                .entityId(flagTenant1FlaggedEntity.getId())
                .entityType(flagTenant1FlaggedEntity.getClass().getName())
                .entityAuthor(th.personIeseAdmin)
                .flagCreator(th.personRegularKarl)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build());

        flagTenant1.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Das ist aber nicht nett!")
                        .userGeneratedContentFlag(flagTenant1)
                        .build())));

        BaseEntity flaggedEntityTenant2 = th.personVgAdmin.getProfilePicture();

        flagTenant2 = flagRepository.save(UserGeneratedContentFlag.builder()
                .entityId(flaggedEntityTenant2.getId())
                .entityType(flaggedEntityTenant2.getClass().getName())
                .entityAuthor(th.personIeseAdmin)
                .flagCreator(th.personRegularHorstiTenant3)
                .status(UserGeneratedContentFlagStatus.REJECTED)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build());

        UserGeneratedContentFlagStatusRecord flag3Tenant1StatusRecord1 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Das Bild gefällt mir nicht, da sind keine Katzen drauf 🐈")
                        .userGeneratedContentFlag(flagTenant2)
                        .build());
        UserGeneratedContentFlagStatusRecord flag3Tenant1StatusRecord2 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .status(UserGeneratedContentFlagStatus.REJECTED)
                        .comment("Der Hund ist doch süß! 🐕")
                        .userGeneratedContentFlag(flagTenant2)
                        .build());
        flagTenant2.setUserGeneratedContentFlagStatusRecords(Arrays.asList(
                flag3Tenant1StatusRecord2,
                flag3Tenant1StatusRecord1));
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void findById() {
        //verify test data integrity
        assertTrue(flagRepository.existsById(flagTenant1.getId()));

        final UserGeneratedContentFlag flagFromDb = flagService.findById(flagTenant1.getId());

        //the status records are not returned from the service, so we remove them for comparison
        flagTenant1.setUserGeneratedContentFlagStatusRecords(null);
        //compare all attributes
        assertTrue(new EqualsBuilder()
                .setReflectUpToClass(BaseEntity.class) //include all fields defined in base entity, too
                .reflectionAppend(flagFromDb, flagTenant1) //this tests all fields defined in the two entities
                .build());
    }

    @Test
    public void findById_NotExisting() {
        final String notExistingId = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        try {
            flagService.findById(notExistingId);
            Assertions.fail("UserGeneratedContentFlagNotFoundException expected");
        } catch (UserGeneratedContentFlagNotFoundException ex) {
            assertThat(ex.getDetail()).isEqualTo(notExistingId);
            assertThat(ex.getClientExceptionType()).isEqualTo(
                    ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND);
        }
    }

    @Test
    public void create_DifferentEntityAndCreator() {

        Person flagCreator = th.personRegularAnna;
        BaseEntity flaggedEntity = th.personRegularKarl;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = flagTenant1.getTenant();

        final String comment = "N Piraden aufzuhalden is nich okeh";

        //assert test data integrity
        assertThat(flagCreator).isNotEqualTo(flagTenant1.getFlagCreator());
        assertThat(flaggedEntity.getId()).isNotEqualTo(flagTenant1.getEntityId());

        createFlagAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant, flagCreator,
                comment);
    }

    @Test
    public void create_NoTenant() {

        Person flagCreator = th.personRegularAnna;
        BaseEntity flaggedEntity = th.personRegularKarl;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();

        final String comment = "Kein Tenant ist auch okeh";

        //assert test data integrity
        assertThat(flagCreator).isNotEqualTo(flagTenant1.getFlagCreator());
        assertThat(flaggedEntity.getId()).isNotEqualTo(flagTenant1.getEntityId());

        createFlagAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, null, flagCreator,
                comment);
    }

    @Test
    public void create_SameEntityDifferentCreator() {

        Person flagCreator = th.personRegularAnna;
        BaseEntity flaggedEntity = flagTenant1FlaggedEntity;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = flagTenant1.getTenant();

        final String comment = "Unicode sollte auch gehen ڧ ڨ ک ↩ ↪ ↫ ↬ ↭ İ ı";

        //assert test data integrity
        assertThat(flagCreator).isNotEqualTo(flagTenant1.getFlagCreator());
        assertThat(flaggedEntity.getId()).isEqualTo(flagTenant1.getEntityId());

        createFlagAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant, flagCreator,
                comment);
    }

    @Test
    public void create_SameEntityAndCreator() {

        Person flagCreator = flagTenant1.getFlagCreator();
        BaseEntity flaggedEntity = flagTenant1FlaggedEntity;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = flagTenant1.getTenant();

        //assert test data integrity
        assertThat(flaggedEntity.getId()).isEqualTo(flagTenant1.getEntityId());

        try {
            flagService.create(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant, flagCreator, "Lass das!");
            Assertions.fail("Exception expected");
        } catch (UserGeneratedContentFlagAlreadyExistsException ex) {
            assertThat(ex.getDetail()).isEqualTo(flagTenant1.getEntityId());
            assertThat(ex.getClientExceptionType())
                    .isEqualTo(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS);
        }
    }

    @Test
    public void createOrUpdate_StatusDifferentEntityAndCreator() {

        Person flagCreator = th.personRegularAnna;
        BaseEntity flaggedEntity = th.personRegularKarl;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = th.tenant1;

        final String comment = "N Piraden aufzuhalden is nich okeh";

        //assert test data integrity
        assertThat(flagCreator).isNotEqualTo(flagTenant1.getFlagCreator());
        assertThat(flaggedEntityAuthor).isEqualTo(flagTenant1.getEntityAuthor());
        assertThat(flaggedEntity.getId()).isNotEqualTo(flagTenant1.getEntityId());
        assertThat(flaggedEntity.getClass().getName()).isEqualTo(flagTenant1.getEntityType());

        createOrUpdateStatusAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant,
                flagCreator, comment);
    }

    @Test
    public void createOrUpdate_StatusSameEntityDifferentCreator() {

        Person flagCreator = th.personRegularAnna;
        BaseEntity flaggedEntity = flagTenant1FlaggedEntity;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = flagTenant1.getTenant();

        final String comment = "Unicode sollte auch gehen ڧ ڨ ک ↩ ↪ ↫ ↬ ↭ İ ı";

        //assert test data integrity
        assertThat(flagCreator).isNotEqualTo(flagTenant1.getFlagCreator());
        assertThat(flaggedEntity.getId()).isEqualTo(flagTenant1.getEntityId());

        createOrUpdateStatusAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant,
                flagCreator, comment);
    }

    @Test
    public void createOrUpdate_StatusSameEntityAndCreator() {

        Person flagCreator = flagTenant1.getFlagCreator();
        BaseEntity flaggedEntity = flagTenant1FlaggedEntity;
        Person flaggedEntityAuthor = flagTenant1.getEntityAuthor();
        Tenant flaggedEntityTenant = flagTenant1.getTenant();

        //assert test data integrity
        assertThat(flaggedEntity.getId()).isEqualTo(flagTenant1.getEntityId());

        createOrUpdateStatusAndAssertCorrectCreation(flaggedEntity, flaggedEntityAuthor, flaggedEntityTenant,
                flagCreator, "Finger weck!");
    }

    @Test
    public void updateStatus() {

        final String comment = "Ich kläre mal, was bei den beiden im Argen liegt...";
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.IN_PROGRESS,
                comment);

        final UserGeneratedContentFlag flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getEntityId()).isEqualTo(flagTenant1.getEntityId());
        assertThat(flagFromRepo.getEntityType()).isEqualTo(flagTenant1.getEntityType());
        assertThat(flagFromRepo.getFlagCreator()).isEqualTo(flagTenant1.getFlagCreator());
        assertThat(flagFromRepo.getEntityAuthor()).isEqualTo(flagTenant1.getEntityAuthor());
        assertThat(flagFromRepo.getCreated()).isEqualTo(flagTenant1.getCreated());
        assertThat(flagFromRepo.getTenant()).isEqualTo(flagTenant1.getTenant());
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.IN_PROGRESS);

        final List<UserGeneratedContentFlagStatusRecord> allStatusRecords =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flagTenant1);
        assertThat(allStatusRecords).hasSize(2);
        final UserGeneratedContentFlagStatusRecord newestStatusRecord = allStatusRecords.get(0);
        assertThat(newestStatusRecord.getUserGeneratedContentFlag()).isEqualTo(flagFromRepo);
        assertThat(newestStatusRecord.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.IN_PROGRESS);
        assertThat(newestStatusRecord.getComment()).isEqualTo(comment);
        final UserGeneratedContentFlagStatusRecord initialStatusRecord = allStatusRecords.get(1);
        assertThat(initialStatusRecord.getUserGeneratedContentFlag()).isEqualTo(flagFromRepo);
        assertThat(initialStatusRecord.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(initialStatusRecord.getComment()).isEqualTo(flagTenant1InitialComment);
    }

    @Test
    public void updateStatus_AllValidTransitions() {

        final String comment = "Immer muss ich mich um alles kümmern ...";
        // Transition OPEN -> IN_PROGRESS
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.IN_PROGRESS,
                comment);
        UserGeneratedContentFlag flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getEntityId()).isEqualTo(flagTenant1.getEntityId());
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.IN_PROGRESS);

        // Transition IN_PROGRESS -> OPEN
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.OPEN, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);

        // Transition OPEN -> OPEN
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.OPEN, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);

        // Transition OPEN -> REJECTED
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.REJECTED, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.REJECTED);

        // Transition OPEN -> ACCEPTED (Before: Set status to OPEN manually)
        flagTenant1.setStatus(UserGeneratedContentFlagStatus.OPEN);
        flagRepository.saveAndFlush(flagTenant1);
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.ACCEPTED, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.ACCEPTED);

        // Transition IN_PROGRESS -> IN_PROGRESS (Before: Set status to IN_PROGRESS manually)
        flagTenant1.setStatus(UserGeneratedContentFlagStatus.IN_PROGRESS);
        flagRepository.saveAndFlush(flagTenant1);
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.IN_PROGRESS, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.IN_PROGRESS);

        // Transition IN_PROGRESS -> REJECTED
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.REJECTED, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.REJECTED);

        // Transition IN_PROGRESS -> ACCEPTED (Before: Set status to IN_PROGRESS manually)
        flagTenant1.setStatus(UserGeneratedContentFlagStatus.IN_PROGRESS);
        flagRepository.saveAndFlush(flagTenant1);
        flagService.updateStatus(flagTenant1, th.personIeseAdmin, UserGeneratedContentFlagStatus.ACCEPTED, comment);
        flagFromRepo = flagRepository.findById(flagTenant1.getId()).orElseThrow(
                () -> new IllegalArgumentException("flag should still exist in repo"));
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.ACCEPTED);
    }

    @Test
    public void existsForPersonAndEntity() {
        assertTrue(flagService.existsForPersonAndEntity(th.personRegularKarl, th.personVgAdmin.getId(),
                Person.class.getName()));
        //other flagCreator
        assertFalse(flagService.existsForPersonAndEntity(th.personVgAdmin, th.personVgAdmin.getId(),
                Person.class.getName()));
        //other entityId
        assertFalse(flagService.existsForPersonAndEntity(th.personRegularKarl, th.personRegularKarl.getId(),
                Person.class.getName()));
        //other entityType
        assertFalse(flagService.existsForPersonAndEntity(th.personRegularKarl, th.personVgAdmin.getId(),
                Tenant.class.getName()));
    }

    @Test
    public void findByFlagCreator() {
        final Set<UserGeneratedContentFlag> flagsByKarl = flagService.findByFlagCreator(th.personRegularKarl);

        assertThat(flagsByKarl).hasSize(1);

        //the status records are not returned from the service, so we remove them for comparison
        flagTenant1.setUserGeneratedContentFlagStatusRecords(null);
        //compare all attributes
        assertTrue(new EqualsBuilder()
                .setReflectUpToClass(BaseEntity.class) //include all fields defined in base entity, too
                .reflectionAppend(flagsByKarl.iterator().next(),
                        flagTenant1) //this tests all fields defined in the two entities
                .build());

        final Set<UserGeneratedContentFlag> flagsByAnna = flagService.findByFlagCreator(th.personRegularAnna);

        assertThat(flagsByAnna).hasSize(0);
    }

    @Test
    public void findFlaggedEntityIdsByFlagCreator() {
        final Set<String> flaggedEntityIdsByKarl = flagService.findFlaggedEntityIdsByFlagCreator(th.personRegularKarl);

        assertThat(flaggedEntityIdsByKarl).hasSize(1);
        assertThat(flaggedEntityIdsByKarl.iterator().next()).isEqualTo(flagTenant1.getEntityId());

        final Set<String> flaggedEntityIdsByAnna = flagService.findFlaggedEntityIdsByFlagCreator(th.personRegularAnna);

        assertThat(flaggedEntityIdsByAnna).hasSize(0);
    }

    private void createFlagAndAssertCorrectCreation(BaseEntity entityToFlag, Person entityAuthor,
            Tenant tenantOfEntityToFlag, Person flagCreator, String comment) {

        flagService.create(entityToFlag, entityAuthor, tenantOfEntityToFlag, flagCreator, comment);

        String entityId = entityToFlag.getId();
        String entityType = entityToFlag.getClass().getName();

        final UserGeneratedContentFlag flagFromRepo =
                flagRepository.findByFlagCreatorAndEntityIdAndEntityType(flagCreator, entityId, entityType)
                        .orElseThrow(() -> new IllegalArgumentException("flag should exist in repo"));
        assertThat(flagFromRepo.getEntityId()).isEqualTo(entityId);
        assertThat(flagFromRepo.getEntityType()).isEqualTo(entityType);
        assertThat(flagFromRepo.getFlagCreator()).isEqualTo(flagCreator);
        assertThat(flagFromRepo.getEntityAuthor()).isEqualTo(entityAuthor);
        assertThat(flagFromRepo.getTenant()).isEqualTo(tenantOfEntityToFlag);
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);

        final List<UserGeneratedContentFlagStatusRecord> allStatusRecords =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flagFromRepo);
        assertThat(allStatusRecords).hasSize(1);
        final UserGeneratedContentFlagStatusRecord statusRecord = allStatusRecords.get(0);
        assertThat(statusRecord.getUserGeneratedContentFlag()).isEqualTo(flagFromRepo);
        assertThat(statusRecord.getInitiator()).isEqualTo(flagCreator);
        assertThat(statusRecord.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(statusRecord.getComment()).isEqualTo(comment);
    }

    private void createOrUpdateStatusAndAssertCorrectCreation(BaseEntity entityToFlag, Person entityAuthor,
            Tenant tenantOfEntityToFlag, Person flagCreator, String comment) {

        int previousNumStatusRecords = Math.toIntExact(flagStatusRecordRepository.count());

        flagService.createOrUpdateStatus(entityToFlag, entityAuthor, tenantOfEntityToFlag, flagCreator, comment);

        String entityId = entityToFlag.getId();
        String entityType = entityToFlag.getClass().getName();

        final UserGeneratedContentFlag flagFromRepo =
                flagRepository.findByFlagCreatorAndEntityIdAndEntityType(flagCreator, entityId, entityType)
                        .orElseThrow(() -> new IllegalArgumentException("flag should exist in repo"));
        assertThat(flagFromRepo.getEntityId()).isEqualTo(entityId);
        assertThat(flagFromRepo.getEntityType()).isEqualTo(entityType);
        assertThat(flagFromRepo.getFlagCreator()).isEqualTo(flagCreator);
        assertThat(flagFromRepo.getEntityAuthor()).isEqualTo(entityAuthor);
        assertThat(flagFromRepo.getTenant()).isEqualTo(tenantOfEntityToFlag);
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);

        final List<UserGeneratedContentFlagStatusRecord> allStatusRecords = flagStatusRecordRepository
                .findAll(Sort.by(Sort.Direction.DESC, "created"));
        assertThat(allStatusRecords).hasSize(previousNumStatusRecords + 1);
        final UserGeneratedContentFlagStatusRecord statusRecord = allStatusRecords.get(0);
        assertThat(statusRecord.getUserGeneratedContentFlag()).isEqualTo(flagFromRepo);
        assertThat(statusRecord.getInitiator()).isEqualTo(flagCreator);
        assertThat(statusRecord.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(statusRecord.getComment()).isEqualTo(comment);
    }

}
