/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Alberto Lara, Johannes Schwarz, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.misc.clientevent.ClientQrCodeLabelCreateRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;

public class LogisticsAdminControllerTest extends BaseServiceTest {

    @Autowired
    private LogisticsAdministrationTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void adminEndpointsUnauthorized() throws Exception {

        assertRequiresAdmin(delete("/administration/logistics/cancelDelivery/deliveryId"));
        assertRequiresAdmin(delete("/administration/logistics/deleteDemoLogisticsData"));
        assertRequiresAdmin(put("/administration/logistics/event/messagerelatedto/delivery"));
        assertRequiresAdmin(put("/administration/logistics/event/messagerelatedto/transportAlternative"));
        assertRequiresAdmin(put("/administration/logistics/event/messagerelatedto/transportAlternativeBundle"));
        assertRequiresAdmin(put("/administration/logistics/event/messagerelatedto/transportAssignment"));
        assertRequiresAdmin(post("/administration/logistics/generateQrCodeLabel"));
        assertRequiresAdmin(post("/administration/logistics/notification/send/testall/person"));
        assertRequiresAdmin(post("/administration/logistics/poolingstation/allocatePoolingStation"));
        assertRequiresAdmin(post("/administration/logistics/poolingstation/allocatePoolingStationBox"));
        assertRequiresAdmin(post("/administration/logistics/poolingstation/deallocatePoolingStation"));
        assertRequiresAdmin(post("/administration/logistics/poolingstation/sendOpenDoorRequest"));
    }

    @Test
    public void cancelDelivery() throws Exception {
        th.createShops();
        th.createPoolingStations();
        th.createLogisticScenario();

        String deliveryId = th.delivery1.getId();
        mockMvc.perform(delete("/administration/logistics/cancelDelivery/" + deliveryId)
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk());

        List<Delivery> list = th.deliveryRepository.findAll();
        Delivery delivery = list.get(0);
        assertEquals(deliveryId, delivery.getId());
        DeliveryStatus currentStatus = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery).getStatus();
        assertEquals(DeliveryStatus.CANCELLED, currentStatus);
    }

    @Test
    public void generateQRCodeLabel() throws Exception {

        mockMvc.perform(post("/administration/logistics/generateQrCodeLabel")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .contentType(contentType)
                .content(json(ClientQrCodeLabelCreateRequest.builder()
                        .receiverName("receiver")
                        .receiverAddressName("receiverAddressName")
                        .receiverStreet("receiverStreet")
                        .receiverCity("receiverCity")
                        .senderName("senderName")
                        .senderStreet("senderStreet")
                        .senderCity("senderCity")
                        .transportNotes("transportNotes")
                        .contentNotes("contentNotes")
                        .trackingCode("trackingCode")
                        .qrCodeText("qrCodeText")
                        .parcelCount(42)
                        .build()
                )))
                .andExpect(status().isOk())
                .andExpect(content().string(startsWith(testFileStorage.getExternalBaseUrl())))
                .andExpect(content().string(endsWith(".pdf")));
    }

    private void assertRequiresAdmin(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertOAuth2(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
