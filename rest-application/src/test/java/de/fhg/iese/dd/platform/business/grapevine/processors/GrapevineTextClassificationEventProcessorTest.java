/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentClassificationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagStatusRecordRepository;
import okhttp3.mockwebserver.*;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class GrapevineTextClassificationEventProcessorTest extends BaseServiceTest {

    private static final double INAPPROPRIATE_VALUE = 0.8;
    private static final double APPROPRIATE_VALUE = 0.2;
    private static final double THRESHOLD_VALUE = 0.35;

    private static MockWebServer mockExternalSystem;

    @Autowired
    private GrapevineTextClassificationEventProcessor grapevineTextClassificationEventProcessor;
    @Autowired
    private UserGeneratedContentFlagRepository userGeneratedContentFlagRepository;
    @Autowired
    private UserGeneratedContentFlagStatusRecordRepository flagStatusRecordRepository;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private GrapevineTestHelper th;

    private AppVariant appVariant;
    private GeoArea deactivatedGeoArea;
    private String apiKey;

    @BeforeAll
    public static void startMockServer() throws IOException {

        mockExternalSystem = new MockWebServer();
        mockExternalSystem.start(8080);
    }

    @AfterAll
    public static void shutdownMockServer() throws Exception {

        mockExternalSystem.shutdown();
    }

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenarioWithoutSuggestions();

        appVariant = th.app1Variant1;
        deactivatedGeoArea = th.geoAreaMainz;
        apiKey = grapevineConfig.getClassificationService().getApiKey();
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(ContentClassificationFeature.class.getName())
                .configValues("{\"thresholdValue\": " + THRESHOLD_VALUE + " }")
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .geoAreasIncluded(Set.of(th.geoAreaMainz))
                .enabled(false)
                .featureClass(ContentClassificationFeature.class.getName())
                .build());

        mockExternalSystem.setDispatcher(new QueueDispatcher());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    private static Dispatcher returnsValue(double value) {

        return new Dispatcher() {

            @NonNull
            @Override
            public MockResponse dispatch(@NonNull RecordedRequest recordedRequest) {

                if ("POST".equals(recordedRequest.getMethod())) {
                    return new MockResponse()
                            .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .setResponseCode(200)
                            .setBody("{ \"result\": [" + value + "]}");
                }
                return new MockResponse()
                        .setResponseCode(400);
            }
        };
    }

    @Test
    void handlePostCreateConfirmation_Inappropriate() throws Exception {

        Post post = th.gossip1withImages;

        double inappropriateValue = INAPPROPRIATE_VALUE + 0.01;
        mockExternalSystem.setDispatcher(returnsValue(inappropriateValue));

        grapevineTextClassificationEventProcessor
                .handlePostCreateConfirmation(new PostCreateConfirmation<>(post, appVariant, true));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertCorrectContentFlagCreation(post, post.getCreator(),
                "⚠ Autogeneriert ⚠ Wert: " + inappropriateValue + " (>" + THRESHOLD_VALUE + ")");
    }

    @Test
    void handlePostCreateConfirmation_Appropriate() throws Exception {

        Post post = th.gossip1withImages;

        mockExternalSystem.setDispatcher(returnsValue(APPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(post, appVariant, true));


        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(post, post.getCreator());
    }

    @Test
    void handlePostCreateConfirmation_WrongReplyFromExternalService() throws Exception {

        Post post = th.gossip1withImages;

        mockExternalSystem.setDispatcher(new Dispatcher() {

            @NotNull
            @Override
            public MockResponse dispatch(@NotNull RecordedRequest recordedRequest) {

                return new MockResponse()
                        .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .setResponseCode(200)
                        .setBody("{ \"result\": [1.0, 1.1]}");
            }
        });

        grapevineTextClassificationEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(post, appVariant, true));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(post, post.getCreator());
    }

    @Test
    void handlePostCreateConfirmation_ErrorFromExternalService() throws Exception {

        Post post = th.gossip1withImages;

        String errorMessage = "Server failed to process request";
        mockExternalSystem.enqueue(new MockResponse()
                .setResponseCode(500)
                .setBody(errorMessage));

        grapevineTextClassificationEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(post, appVariant, true));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(post, post.getCreator());
    }

    @Test
    void handlePostCreateConfirmation_FeatureDeactivated() throws Exception {

        Post post = th.gossip1withImages;
        post.setGeoAreas(Set.of(deactivatedGeoArea));
        th.postRepository.save(post);

        mockExternalSystem.setDispatcher(returnsValue(INAPPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(post, appVariant, true));

        assertNoContentFlagCreation(post, post.getCreator());
    }

    @Test
    void handlePostChangeConfirmation_Inappropriate() throws Exception {

        Post post = th.gossip1withImages;

        mockExternalSystem.setDispatcher(returnsValue(INAPPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handlePostChangeConfirmation(
                PostChangeConfirmation.builder()
                        .post(post)
                        .appVariant(appVariant)
                        .published(true)
                        .build());

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertCorrectContentFlagCreation(post, post.getCreator(),
                "⚠ Autogeneriert ⚠ Wert: " + INAPPROPRIATE_VALUE + " (>" + THRESHOLD_VALUE + ")");
    }

    @Test
    void handlePostChangeConfirmation_Appropriate() throws Exception {

        Post post = th.gossip1withImages;

        mockExternalSystem.setDispatcher(returnsValue(APPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handlePostChangeConfirmation(
                PostChangeConfirmation.builder()
                        .post(post)
                        .appVariant(appVariant)
                        .published(true)
                        .build());

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + post.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(post, post.getCreator());
    }

    @Test
    void handleCommentCreateConfirmation_Inappropriate() throws Exception {

        Comment comment = th.gossip2Comment1;

        mockExternalSystem.setDispatcher(returnsValue(INAPPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handleCommentCreateConfirmation(
                new CommentCreateConfirmation(comment, appVariant));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + comment.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertCorrectContentFlagCreation(comment, comment.getCreator(),
                "⚠ Autogeneriert ⚠ Wert: " + INAPPROPRIATE_VALUE + " (>" + THRESHOLD_VALUE + ")");
    }

    @Test
    void handleCommentCreateConfirmation_Appropriate() throws Exception {

        Comment comment = th.gossip2Comment1;

        mockExternalSystem.setDispatcher(returnsValue(APPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handleCommentCreateConfirmation(
                new CommentCreateConfirmation(comment, appVariant));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + comment.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(comment, comment.getCreator());
    }

    @Test
    void handleCommentChangeConfirmation_Inappropriate() throws Exception {

        Comment comment = th.gossip2Comment1;

        mockExternalSystem.setDispatcher(returnsValue(INAPPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handleCommentChangeConfirmation(
                new CommentChangeConfirmation(comment, appVariant));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + comment.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertCorrectContentFlagCreation(comment, comment.getCreator(),
                "⚠ Autogeneriert ⚠ Wert: " + INAPPROPRIATE_VALUE + " (>" + THRESHOLD_VALUE + ")");

    }

    @Test
    void handleCommentChangeConfirmation_Appropriate() throws Exception {

        Comment comment = th.gossip2Comment1;

        mockExternalSystem.setDispatcher(returnsValue(APPROPRIATE_VALUE));

        grapevineTextClassificationEventProcessor.handleCommentChangeConfirmation(
                new CommentChangeConfirmation(comment, appVariant));

        assertThat(mockExternalSystem.takeRequest().getBody().readUtf8())
                .isEqualTo("{\"texts\":[\"" + comment.getText() + "\"],\"api_key\":\"" + apiKey + "\"}");

        assertNoContentFlagCreation(comment, comment.getCreator());
    }

    private void assertNoContentFlagCreation(BaseEntity post, Person flagCreator) {

        String entityId = post.getId();
        String entityType = post.getClass().getName();

        assertThat(
                userGeneratedContentFlagRepository.existsByFlagCreatorAndEntityIdAndEntityType(flagCreator, entityId,
                        entityType)).isFalse();
    }

    private void assertCorrectContentFlagCreation(BaseEntity post, Person postCreator, String statusComment) {

        String entityId = post.getId();
        String entityType = post.getClass().getName();

        Optional<UserGeneratedContentFlag> flagOpt = userGeneratedContentFlagRepository
                .findByFlagCreatorAndEntityIdAndEntityType(null, entityId, entityType);
        assertThat(flagOpt).as("Flag should be created in database").isPresent();
        UserGeneratedContentFlag flagFromRepo = flagOpt.get();
        assertThat(flagFromRepo.getEntityId()).isEqualTo(entityId);
        assertThat(flagFromRepo.getEntityType()).isEqualTo(entityType);
        assertThat(flagFromRepo.getFlagCreator()).isNull();
        assertThat(flagFromRepo.getEntityAuthor()).isEqualTo(postCreator);
        assertThat(flagFromRepo.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);

        List<UserGeneratedContentFlagStatusRecord> allStatusRecords = flagStatusRecordRepository
                .findAll(Sort.by(Sort.Direction.DESC, "created"));
        assertThat(allStatusRecords).hasSize(1);
        UserGeneratedContentFlagStatusRecord statusRecord = allStatusRecords.get(0);
        assertThat(statusRecord.getUserGeneratedContentFlag()).isEqualTo(flagFromRepo);
        assertThat(statusRecord.getInitiator()).isNull();
        assertThat(statusRecord.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(statusRecord.getComment()).isEqualTo(statusComment);
    }

}
