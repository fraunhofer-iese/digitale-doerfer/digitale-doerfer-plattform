/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2019 Johannes Schneider, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.push.clientevent.ClientInformationMessageEvent;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushRegistrationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;

public class PushAdminControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;

    @Autowired
    private IPushRegistrationService pushRegistrationService;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void adminEndpointsUnauthorized() throws Exception {

        assertRequiresAdmin(delete("/administration/shared/push/pushCategory"));
        assertRequiresAdmin(get("/administration/shared/push/registrations"));
        assertRequiresAdmin(post("/administration/shared/push/send/geoArea"));
        assertRequiresAdmin(post("/administration/shared/push/send/persons"));
    }

    @Test
    public void deletePushCategory() throws Exception {

        PushCategory pushCategoryToDelete = th.pushCategory1App1;
        pushCategoryUserSettingRepository.saveAndFlush(PushCategoryUserSetting.builder()
                .person(th.personRegularKarl)
                .appVariant(th.app1Variant1)
                .loudPushEnabled(true)
                .pushCategory(pushCategoryToDelete).build());

        mockMvc.perform(delete("/administration/shared/push/pushCategory")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("pushCategoryId", pushCategoryToDelete.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(th.pushCategory1App1.getId())));

        assertFalse(th.pushCategoryRepository.existsById(th.pushCategory1App1.getId()));
        assertEquals(0L, pushCategoryUserSettingRepository.count());

    }

    @Test
    public void deletePushCategoryUnauthorized() throws Exception {

        mockMvc.perform(delete("/administration/shared/push/pushCategory")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("pushCategoryId", th.pushCategory1App1.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void deletePushCategoryInvalidId() throws Exception {

        mockMvc.perform(delete("/administration/shared/push/pushCategory")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("pushCategoryId", "not-existing-id"))
                .andExpect(isException("not-existing-id", ClientExceptionType.PUSH_CATEGORY_NOT_FOUND));
    }

    @Test
    public void sendPushToGeoArea() throws Exception {
        PushCategory category = th.pushCategory1App1;
        GeoArea geoArea = th.geoAreaKaiserslautern;
        String message = "Random Push Message!";

        mockMvc.perform(post("/administration/shared/push/send/geoArea")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("geoAreaId", geoArea.getId())
                .param("message", message)
                .param("categoryId", category.getId()))
                .andExpect(status().isOk());

        //verify push message
        waitForEventProcessing();

        ClientInformationMessageEvent pushedEvent = testClientPushService.getPushedEventToGeoAreaLoud(geoArea,
                ClientInformationMessageEvent.class,
                new PushCategory.PushCategoryId(category.getId()));

        assertThat(pushedEvent.getMessage()).isEqualTo(message);
    }

    @Test
    public void sendPushToGeoAreaUnauthorized() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/geoArea")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("geoAreaId", th.geoAreaKaiserslautern.getId())
                .param("message", "Random push message!")
                .param("categoryId", th.pushCategory1App1.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void sendPushToGeoAreaPushCategoryInvalid() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/geoArea")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("geoAreaId", th.geoAreaKaiserslautern.getId())
                .param("message", "Random push message!")
                .param("categoryId", "fake category id"))
                .andExpect(isException(ClientExceptionType.PUSH_CATEGORY_NOT_FOUND));
    }

    @Test
    public void sendPushToGeoAreaGeoAreaInvalid() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/geoArea")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("geoAreaId", "fake geoArea id")
                .param("message", "Random push message!")
                .param("categoryId", th.pushCategory1App1.getId()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
    }

    @Test
    public void sendPushToPersons() throws Exception {
        PushCategory category = th.pushCategory1App1;
        Person person1 = th.personRegularKarl;
        Person person2 = th.personRegularAnna;
        String message = "Random Push Message!";

        mockMvc.perform(post("/administration/shared/push/send/persons")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personIds", person1.getId(), person2.getId())
                .param("message", message)
                .param("categoryId", category.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Message sent to 2 persons")));

        //verify push message
        waitForEventProcessing();

        ClientInformationMessageEvent pushedEvent1 = testClientPushService.getPushedEventToPersonLoud(person1,
                ClientInformationMessageEvent.class,
                new PushCategory.PushCategoryId(category.getId()));

        ClientInformationMessageEvent pushedEvent2 = testClientPushService.getPushedEventToPersonLoud(person2,
                ClientInformationMessageEvent.class,
                new PushCategory.PushCategoryId(category.getId()));

        assertThat(pushedEvent1.getMessage()).isEqualTo(message);
        assertThat(pushedEvent2.getMessage()).isEqualTo(message);
    }

    @Test
    public void sendPushToPersonsUnauthorized() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/persons")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("personIds", th.personRegularAnna.getId(), th.personShopOwner.getId())
                .param("message", "Random push message!")
                .param("categoryId", th.pushCategory1App1.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void sendPushToPersonsPersonInvalid() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/persons")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personIds", "random id 1", "random id 2")
                .param("message", "Random push message!")
                .param("categoryId", th.pushCategory1App1.getId()))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void sendPushToPersonsCategoryInvalid() throws Exception {
        mockMvc.perform(post("/administration/shared/push/send/persons")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personIds", th.personRegularAnna.getId(), th.personShopOwner.getId())
                .param("message", "Random push message!")
                .param("categoryId", "fake push category"))
                .andExpect(isException(ClientExceptionType.PUSH_CATEGORY_NOT_FOUND));
    }

    @Test
    public void getAllPushRegistrations() throws Exception {
        String testToken = "my-test-token1";
        String testToken2 = "my-test-token2";
        AppVariant variant = th.app1Variant1;
        Person person = th.personRegularAnna;

        pushRegistrationService.registerEndpoint(testToken, PushPlatformType.FCM, variant, person);
        pushRegistrationService.registerEndpoint(testToken2, PushPlatformType.GCM, variant, person);
        appService.createOrUpdateAppVariantUsage(variant, person, false);

        waitForEventProcessing();

        String result = mockMvc.perform(get("/administration/shared/push/registrations")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", person.getId()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        // check if summary string contains correct data
        assertThat(result).contains(
                person.getFullName(),
                person.getId(),
                person.getEmail(),
                "FCM",
                "GCM",
                testToken,
                testToken2,
                variant.getId());
    }

    @Test
    public void getAllPushRegistrationsUnauthorized() throws Exception {
        mockMvc.perform(get("/administration/shared/push/registrations")
                .headers(authHeadersFor(th.personRegularAnna))
                .param("personId", th.personRegularAnna.getId()))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllPushRegistrationsInvalidPerson() throws Exception {
        mockMvc.perform(get("/administration/shared/push/registrations")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", "randomID"))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    private void assertRequiresAdmin(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertOAuth2(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}

