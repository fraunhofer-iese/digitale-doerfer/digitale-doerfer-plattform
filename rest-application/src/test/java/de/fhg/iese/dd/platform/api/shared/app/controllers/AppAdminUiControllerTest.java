/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;

public class AppAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void appVariantByTenant() throws Exception {
        
        final int expectedNumberOfEntries = (int) th.appVariantGeoAreaMappingRepository.findAll()
                .stream()
                .map(mapping -> Pair.of(mapping.getContract().getAppVariant(), mapping.getContract().getTenant()))
                .distinct()
                .count();

        assertThat(expectedNumberOfEntries)
                .as("There should be at least 5 entries, otherwise the test data changed or is insufficient")
                .isGreaterThanOrEqualTo(5);

        mockMvc.perform(get("/adminui/app/appVariantByTenant")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedNumberOfEntries)));

        mockMvc.perform(get("/adminui/app/appVariantByTenant")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedNumberOfEntries)));
    }

    @Test
    public void appVariantByTenant_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/appVariantByTenant")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/appVariantByTenant"));
    }

    @Test
    public void getAppVariantWithSecretsById() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        mockMvc.perform(get("/adminui/app/appVariant/{appVariantId}/secrets", appVariant.getId())
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("appVariantIdentifier").value(appVariant.getAppVariantIdentifier()))
                .andExpect(jsonPath("name").value(appVariant.getName()))
                .andExpect(jsonPath("app.appIdentifier").value(appVariant.getApp().getAppIdentifier()))
                .andExpect(jsonPath("app.name").value(appVariant.getApp().getName()))
                .andExpect(jsonPath("apiKey1").value(appVariant.getApiKey1()))
                .andExpect(jsonPath("apiKey2").value(appVariant.getApiKey2()))
                .andExpect(jsonPath("callBackUrl").isEmpty())
                .andExpect(jsonPath("externalApiKey").value(appVariant.getExternalApiKey()))
                .andExpect(jsonPath("externalBasicAuthUsername").value(appVariant.getExternalBasicAuthUsername()))
                .andExpect(jsonPath("externalBasicAuthPassword").value(appVariant.getExternalBasicAuthPassword()));
    }

    @Test
    public void getAppVariantWithSecretsById_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/appVariant/{appVariantId}/secrets", th.app1Variant1.getId())
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/appVariant/{appVariantId}/secrets", th.app1Variant1.getId()));
    }

    @Test
    public void getAppVariantById() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        mockMvc.perform(get("/adminui/app/appVariant/{appVariantId}", appVariant.getId())
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("appVariantIdentifier").value(appVariant.getAppVariantIdentifier()))
                .andExpect(jsonPath("name").value(appVariant.getName()))
                .andExpect(jsonPath("app.appIdentifier").value(appVariant.getApp().getAppIdentifier()))
                .andExpect(jsonPath("app.name").value(appVariant.getApp().getName()));
    }

    @Test
    public void getAppVariantById_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/appVariant/{appVariantId}", th.app1Variant1.getId())
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/appVariant/{appVariantId}", th.app1Variant1.getId()));
    }

    @Test
    public void getTenantsByContractOrAppVariant() throws Exception {

        mockMvc.perform(get("/adminui/app/tenant")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(3))
                .andExpect(jsonPath("$.content[0].id").value(th.tenant1.getId()))
                .andExpect(jsonPath("$.content[1].id").value(th.tenant2.getId()))
                .andExpect(jsonPath("$.content[2].id").value(th.tenant3.getId()));

        mockMvc.perform(get("/adminui/app/tenant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("search", th.tenant3.getName()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.tenant3.getId()));

        mockMvc.perform(get("/adminui/app/tenant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("appVariantIds", th.app1Variant1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.tenant1.getId()));

        final Set<AppVariantTenantContract> contracts =
                th.appVariantTenantContractRepository.findAllByTenantAndAppVariant(th.tenant1, th.app1Variant2);
        assertThat(contracts).hasSize(1);
        final AppVariantTenantContract contract = contracts.iterator().next();
        mockMvc.perform(get("/adminui/app/tenant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("contractIds", contract.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.tenant1.getId()));
    }

    @Test
    public void getTenantsByContractOrAppVariant_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/tenant")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/tenant"));
    }

    @Test
    public void getAppVariantsByTenantOrContract() throws Exception {

        mockMvc.perform(get("/adminui/app/appVariant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(6))
                .andExpect(jsonPath("$.content[0].id").value(th.appVariantInvalidVersion.getId()))
                .andExpect(jsonPath("$.content[1].id").value(th.app1Variant1.getId()))
                .andExpect(jsonPath("$.content[2].id").value(th.app1Variant2.getId()))
                .andExpect(jsonPath("$.content[3].id").value(th.app2Variant1.getId()))
                .andExpect(jsonPath("$.content[4].id").value(th.app2Variant2.getId()))
                .andExpect(jsonPath("$.content[5].id").value(th.appVariantPlatform.getId()));

        mockMvc.perform(get("/adminui/app/appVariant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("search", th.app2Variant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(th.app2Variant1.getId()));

        mockMvc.perform(get("/adminui/app/appVariant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("tenantIds", th.tenant3.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(th.appVariantInvalidVersion.getId()))
                .andExpect(jsonPath("$.content[1].id").value(th.app2Variant2.getId()));

        final Set<AppVariantTenantContract> contractsTenant3 =
                th.appVariantTenantContractRepository.findAllByTenant(th.tenant3);
        assertThat(contractsTenant3).hasSize(2);
        final String[] contractIds = contractsTenant3.stream().map(BaseEntity::getId).toArray(String[]::new);
        mockMvc.perform(get("/adminui/app/appVariant")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("contractIds", contractIds))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(th.appVariantInvalidVersion.getId()))
                .andExpect(jsonPath("$.content[1].id").value(th.app2Variant2.getId()));
    }

    @Test
    public void getAppVariantsByTenantOrContract_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/appVariant")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/appVariant"));
    }

    @Test
    public void getContractsByTenantOrAppVariant() throws Exception {

        final List<AppVariantTenantContract> allContracts = th.appVariantTenantContractRepository.findAll().stream()
                .sorted(Comparator.comparingLong(AppVariantTenantContract::getCreated)
                        .thenComparing(AppVariantTenantContract::getId))
                .collect(Collectors.toList());

        mockMvc.perform(get("/adminui/app/contract")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(allContracts.size()))
                .andExpect(jsonPath("$.content[0].id").value(allContracts.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(allContracts.get(1).getId()))
                .andExpect(jsonPath("$.content[2].id").value(allContracts.get(2).getId()));

        final List<AppVariantTenantContract> contractsTenant3 = allContracts.stream()
                .filter(c -> th.tenant3.equals(c.getTenant()))
                .collect(Collectors.toList());
        assertThat(contractsTenant3).hasSize(2);

        mockMvc.perform(get("/adminui/app/contract")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("search", th.tenant3.getName()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(contractsTenant3.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(contractsTenant3.get(1).getId()));

        mockMvc.perform(get("/adminui/app/contract")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("tenantIds", th.tenant3.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(contractsTenant3.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(contractsTenant3.get(1).getId()));

        final List<AppVariantTenantContract> contractsApp2Variant1 = allContracts.stream()
                .filter(c -> th.app2Variant1.equals(c.getAppVariant()))
                .collect(Collectors.toList());
        assertThat(contractsApp2Variant1).hasSize(2);

        mockMvc.perform(get("/adminui/app/contract")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .param("appVariantIds", th.app2Variant1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(contractsApp2Variant1.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(contractsApp2Variant1.get(1).getId()));
    }

    @Test
    public void getContractsByTenantOrAppVariant_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/app/contract")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/app/contract"));
    }

}
