/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteRequest;
import de.fhg.iese.dd.platform.business.test.mocks.TestDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.TestMediaItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public abstract class BasePostEventByNewsSourceControllerTest<T extends ExternalPost> extends BaseAbstractPostEventControllerTest<T> {

    @Autowired
    protected TestMediaItemService testMediaItemService;
    @Autowired
    protected TestDefaultTeamNotificationPublisher testDefaultTeamNotificationPublisher;

    @Test
    public void verifyTestData() throws Exception {
        super.verifyTestData();
        assertNotEquals(getTestPost1().getAuthorName(), getTestPost2().getAuthorName());
        assertNotEquals(getTestPost1().getNewsSource(), th.newsSourceAnalogheim);
        assertNotEquals(getTestPost2().getNewsSource(), th.newsSourceAnalogheim);
    }

    @Test
    public void postDeleteRequest_WrongApiKey() throws Exception {

        final ExternalPost postToBeDeleted = getTestPost1();

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, "invalidApiKey")
                .contentType(contentType)
                .content(json(ClientExternalPostDeleteRequest.builder()
                        .postId(postToBeDeleted.getId())
                        .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertPostIsNotDeleted(postToBeDeleted);

        //it is the api key of another news source!
        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, th.newsSourceAnalogheim.getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientExternalPostDeleteRequest.builder()
                        .postId(postToBeDeleted.getId())
                        .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        //in this case it is even not found because the external post id is relative to the news source
        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, th.newsSourceAnalogheim.getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientExternalPostDeleteRequest.builder()
                        .externalId(postToBeDeleted.getExternalId())
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        assertPostIsNotDeleted(postToBeDeleted);
    }

    @Test
    public void postDeleteRequest_AlreadyDeleted() throws Exception {

        final ExternalPost postToBeDeleted = getTestPost1();
        postToBeDeleted.setDeleted(true);
        th.postRepository.save(postToBeDeleted);

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, postToBeDeleted.getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientExternalPostDeleteRequest.builder()
                        .postId(postToBeDeleted.getId())
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, postToBeDeleted.getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientExternalPostDeleteRequest.builder()
                        .externalId(postToBeDeleted.getExternalId())
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void postDeleteRequest_WrongType() throws Exception {

        final Post postToBeDeleted = getTestPostDifferentType();

        final ClientPostDeleteRequest postDeleteRequest = ClientPostDeleteRequest.builder()
                .postId(postToBeDeleted.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, getTestPost1().getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        assertPostIsNotDeleted(postToBeDeleted);
    }

    @Test
    public void postDeleteRequest_InvalidParameters() throws Exception {

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, getTestPost1().getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .postId(UUID.randomUUID().toString())
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, getTestPost1().getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void postDeleteRequest_ViaApiKey() throws Exception {

        Person person = th.personLaraSchaefer;

        ExternalPost newsToBeDeleted1 = getTestPost1();
        ExternalPost newsToBeDeleted2 = getTestPost2();

        assertThat(newsToBeDeleted1).isNotEqualTo(newsToBeDeleted2);

        deleteExternalPostViaApiKey(person, newsToBeDeleted1, ClientExternalPostDeleteRequest.builder()
                .postId(newsToBeDeleted1.getId())
                .build());
        deleteExternalPostViaApiKey(person, newsToBeDeleted2, ClientExternalPostDeleteRequest.builder()
                .externalId(newsToBeDeleted2.getExternalId())
                .build());
    }

    private void deleteExternalPostViaApiKey(Person person, ExternalPost postToBeDeleted,
            ClientExternalPostDeleteRequest deleteRequest) throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        String postToBeDeletedId = postToBeDeleted.getId();

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                .header(HEADER_NAME_API_KEY, postToBeDeleted.getNewsSource().getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(deleteRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(postToBeDeletedId))
                .andExpect(jsonPath("$.externalId").value(postToBeDeleted.getExternalId()));

        // Get the newsItem - it should return POST_NOT_FOUND
        mockMvc.perform(get("/grapevine/post/{postId}", postToBeDeletedId)
                .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        // Check whether the post is also deleted from the repository
        ExternalPost deletedPost = th.externalPostRepository.findById(postToBeDeletedId).get();
        assertTrue(deletedPost.isDeleted());
        assertEquals(nowTime, deletedPost.getDeletionTime());

        // verify push message
        waitForEventProcessing();

        ClientPostDeleteConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(postToBeDeleted.getGeoAreas(),
                        ClientPostDeleteConfirmation.class, DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertEquals(postToBeDeletedId, pushedEvent.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

}
