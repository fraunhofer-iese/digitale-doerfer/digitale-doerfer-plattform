/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningListParticipantsFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.HappeningParticipant;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HappeningControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private PersonRepository personRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();
        th.createOrganizations();
        th.createNewsSources();
        th.createHappeningsAndComments();

        // happeningListParticipantsFeature
        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .featureClass(HappeningListParticipantsFeature.class.getName())
                .appVariant(th.appVariant4AllTenants)
                .enabled(true)
                .build());

        // PARALLEL_WORLD_INHABITANT'
        th.personMonikaHess.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        personRepository.saveAndFlush(th.personMonikaHess);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllParticipantsById_FeatureDefaultEnabled() throws Exception {

        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();

        // caller has not status 'PARALLEL_WORLD_INHABITANT' -> all participants not having status 'PARALLEL_WORLD_INHABITANT'
        // are returned
        List<Person> expectedParticipants = th.happeningParticipantRepository.findAll()
                .stream()
                .filter(hp -> hp.getHappening().getId().equals(happeningId))
                .map(HappeningParticipant::getPerson)
                .filter(person -> !person.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT))
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());
        int pageSize = 10;

        mockMvc.perform(buildGetAllParticipantsById(th.personRegularKarl, appVariant, happeningId, 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(expectedParticipants.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedParticipants.get(1).getId()))
                .andExpect(jsonPath("$.content[2].id").value(expectedParticipants.get(2).getId()))
                .andExpect(jsonPath("$.content[3].id").value(expectedParticipants.get(3).getId()))
                .andExpect(hasCorrectPageData(0, pageSize, expectedParticipants.size()));

        // caller has status 'PARALLEL_WORLD_INHABITANT' -> all participants returned
        expectedParticipants = th.happeningParticipantRepository.findAll()
                .stream()
                .filter(hp -> hp.getHappening().getId().equals(happeningId))
                .map(HappeningParticipant::getPerson)
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());

        mockMvc.perform(buildGetAllParticipantsById(th.personMonikaHess, appVariant, happeningId, 0, pageSize))
                .andExpect(jsonPath("$.content[0].id").value(expectedParticipants.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedParticipants.get(1).getId()))
                .andExpect(jsonPath("$.content[2].id").value(expectedParticipants.get(2).getId()))
                .andExpect(jsonPath("$.content[3].id").value(expectedParticipants.get(3).getId()))
                .andExpect(jsonPath("$.content[4].id").value(expectedParticipants.get(4).getId()))
                .andExpect(hasCorrectPageData(0, pageSize, expectedParticipants.size()));
    }

    @Test
    public void getAllParticipantsById_Paged() throws Exception {

        final Person caller = th.personMonikaHess;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();
        final List<Person> expectedParticipants = th.happeningParticipantRepository.findAll()
                .stream()
                .filter(hp -> hp.getHappening().getId().equals(happeningId))
                .map(HappeningParticipant::getPerson)
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());

        assertEquals(5, expectedParticipants.size());
        int pageSize = 3;

        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(expectedParticipants.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedParticipants.get(1).getId()))
                .andExpect(jsonPath("$.content[2].id").value(expectedParticipants.get(2).getId()))
                .andExpect(hasCorrectPageData(0, pageSize, expectedParticipants.size()));

        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(expectedParticipants.get(3).getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedParticipants.get(4).getId()))
                .andExpect(hasCorrectPageData(1, pageSize, expectedParticipants.size()));
    }

    @Test
    public void getAllParticipantsById_NotAuthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();

        assertOAuth2AppVariantRequired(
                buildGetAllParticipantsById(null, null, happeningId, 0, 10), appVariant, caller);
    }

    @Test
    public void getAllParticipantsById_OutOfRange() throws Exception {

        final Person caller = th.personMonikaHess;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();
        final List<Person> expectedParticipants = th.happeningParticipantRepository.findAll()
                .stream()
                .filter(hp -> hp.getHappening().getId().equals(happeningId))
                .map(HappeningParticipant::getPerson)
                .sorted(Comparator.comparing(Person::getFirstName))
                .collect(Collectors.toList());

        assertEquals(5, expectedParticipants.size());
        int pageSize = 5;

        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedParticipants.size()));
    }

    @Test
    public void getAllParticipantsById_PageParametersInvalid() throws Exception {

        final Person caller = th.personRegularKarl;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();

        // only page negative
        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, -1, 10))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // only count negative
        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, 0, -1))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // both parameters negative
        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, -1, -1))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getAllParticipantsById_PostNotFound() throws Exception {

        final Person caller = th.personRegularKarl;
        final AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, "invalidHappeningId", 0, 10))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getAllParticipantsById_FeatureNotEnabledForTenant() throws Exception {

        final Person caller = th.personRegularKarl;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final String happeningId = th.happening1.getId();

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(HappeningListParticipantsFeature.class.getName())
                .enabled(false)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        // feature not enabled for tenant
        mockMvc.perform(buildGetAllParticipantsById(caller, appVariant, happeningId, 0, 10))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    private MockHttpServletRequestBuilder buildGetAllParticipantsById(Person caller, AppVariant appVariant,
            String happeningId, int page, int count) throws IOException {
        MockHttpServletRequestBuilder builder = get("/grapevine/happening/{happeningId}/participants", happeningId)
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count));
        if (caller != null && appVariant != null) {
            builder = builder.headers(authHeadersFor(caller, appVariant));
        } else if (caller != null && appVariant == null) {
            builder = builder.headers(authHeadersFor(caller));
        }
        return builder;
    }

}
