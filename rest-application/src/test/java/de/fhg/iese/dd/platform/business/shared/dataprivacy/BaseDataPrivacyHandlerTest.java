/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel, Johannes Schneider, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormat;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.OauthUser;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IDataPrivacyReportConverterService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInactivityDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInternalDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

@Tag("data-privacy")
public abstract class BaseDataPrivacyHandlerTest extends BaseServiceTest {

    //set to true to inspect the actual reports. Check the log for the output file name
    protected static final boolean DUMP_REPORTS_TO_TMP = false;

    private static final Pattern UUID_PATTERN =
            Pattern.compile("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}");
    private static final Pattern DELETED_OAUTH_ID_PATTERN =
            Pattern.compile("deleted\\|[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}");

    @Autowired
    protected ICommonDataPrivacyService commonDataPrivacyService;
    @Autowired
    protected IInternalDataPrivacyService internalDataPrivacyService;
    @Autowired
    protected IInactivityDataPrivacyService inactivityDataPrivacyService;
    @Autowired
    protected IDataPrivacyReportConverterService converterService;
    @Autowired
    protected TestEmailSenderService emailSenderService;

    @Autowired
    protected PersonRepository personRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private AddressListEntryRepository addressListEntryRepository;

    @Autowired
    private List<JpaRepository<?, ?>> repositories;

    public abstract Person getPersonForPrivacyReport();

    public abstract Person getPersonForDeletion();

    /**
     * All repositories that are explicitly excluded in the data privacy test. If a repository is listed here, there
     * needs to be a reason why we do not need to test the reporting and deletion of the entities in that repository
     */
    public Collection<Class<? extends JpaRepository<?, ?>>> getExcludedRepositories() {

        return Collections.emptyList();
    }

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_REPORTS_TO_TMP) {
            deleteTempFilesStartingWithClassName(BaseDataPrivacyHandlerTest.class);
        }
    }

    @AfterEach
    public void dumpEmailsToTmp(TestInfo testInfo) throws Exception {
        if (DUMP_REPORTS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(
                                getClass().getSimpleName() + "-" + testInfo.getDisplayName().replace("()", "") + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile.toUri());
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
            for (String zipFile : testFileStorage.getAllZipFiles()) {
                String zipFileContent = testFileStorage.getZippedFiles(zipFile).stream()
                        .collect(Collectors.joining(", ", "[", "]"));
                log.info("Zip file {} contains {}", zipFile, zipFileContent);
            }
        }
    }

    @Test
    public void collectUserDataInternal_NoException() throws Exception {

        Person person = getPersonForPrivacyReport();

        assertNotNull(person);

        IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(person, dataPrivacyReport);
        for (DataPrivacyReportFormat format : DataPrivacyReportFormat.values()) {
            final String report = converterService.convert(dataPrivacyReport, format);
            assertFalse(report.isEmpty(), format + " report should not be empty");
            assertTrue(report.contains(person.getFirstName()), format + " report should contain first name");
            assertTrue(report.contains(person.getLastName()), format + " report should contain last name");
            assertTrue(report.contains(person.getEmail()), format + " report should contain email");
            if (DUMP_REPORTS_TO_TMP) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-collection-", "." + format.name());
                log.info("Saving {} to {}", dataPrivacyReport.getTitle(), tempFile.toUri());
                Files.write(tempFile, report.getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Test
    public void deletePerson_NoExceptionAndCorrectHandlingOfBasicPersonData() throws IOException {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        ensureAllEntitiesOfModuleAreCoveredInTest();

        final Person personForDeletion = getPersonForDeletion();
        personForDeletion.setPendingNewEmail("new-email" + UUID.randomUUID());
        personForDeletion.getVerificationStatuses().addValue(PersonVerificationStatus.NAME_VERIFIED);
        personRepository.save(personForDeletion);
        final AddressListEntry originalAddressListEntry =
                personRepository.findAllAddressListEntriesByPerson(personForDeletion).iterator().next();
        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(personForDeletion, report);

        //checking person erasure
        final Person deletedPerson = personRepository.findById(personForDeletion.getId()).get();
        assertThat(personRepository.findAllAddressListEntriesByPerson(deletedPerson)).isEmpty();
        assertThat(deletedPerson.getFirstName()).is(erasedString());
        assertThat(deletedPerson.getLastName()).is(erasedString());
        assertThat(deletedPerson.getEmail()).is(randomString());
        assertThat(deletedPerson.getPendingNewEmail()).isNull();
        assertThat(deletedPerson.getVerificationStatus()).isEqualTo(0);
        if (personForDeletion.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED)) {
            assertThat(deletedPerson.getOauthId()).isNot(deletedOauthId());
        } else {
            assertThat(deletedPerson.getOauthId()).is(deletedOauthId());
        }
        assertThat(deletedPerson.getOauthId()).isNotNull();
        assertThat(deletedPerson.getProfilePicture()).isNull();
        assertThat(deletedPerson.getNickName()).isNull();
        assertThat(deletedPerson.getPhoneNumber()).isNull();
        assertThat(deletedPerson.getTenant()).isEqualTo(personForDeletion.getTenant());
        assertThat(deletedPerson.getAccountType()).isEqualTo(personForDeletion.getAccountType());
        assertThat(deletedPerson.isDeleted()).isTrue();
        assertThat(deletedPerson.getDeletionTime()).isEqualTo(nowTime);

        //checking address entry deletion
        assertFalse(addressListEntryRepository.existsById(originalAddressListEntry.getId()));

        //checking address erasure
        final Address deletedAddress = addressRepository.findById(originalAddressListEntry.getAddress().getId()).get();
        assertThat(deletedAddress.getName()).is(erasedString());
        assertThat(deletedAddress.getStreet()).is(erasedString());
        assertThat(deletedAddress.getZip()).isEqualTo("00000");
        assertThat(deletedAddress.getCity()).is(erasedString());
        assertThat(deletedAddress.isDeleted()).isTrue();
        assertThat(deletedAddress.getDeletionTime()).isEqualTo(nowTime);

        //logging report
        final String reportText = converterService.convertToJson(report);
        if (DUMP_REPORTS_TO_TMP) {
            final Path tempFile = Files.createTempFile(getClass().getSimpleName() + "-collection-", ".json");
            log.info("Saving deletion report to {}", tempFile.toUri());
            Files.write(tempFile, reportText.getBytes(StandardCharsets.UTF_8));
        }

        //checking for correct deleted entities in the report
        assertThat(report).is(containsEntityWithOperation(personForDeletion,
                DeleteOperation.ERASED));
        assertThat(report).is(containsEntityWithOperation(originalAddressListEntry,
                DeleteOperation.DELETED));
        assertThat(report).is(containsEntityWithOperation(originalAddressListEntry.getAddress(),
                DeleteOperation.ERASED));
        assertThat(report).is(containsEntityWithOperation(OauthUser.forOauthId(personForDeletion.getOauthId()),
                DeleteOperation.DELETED));

        //check if a report for a deleted person works
        final IDataPrivacyReport collectionReport = new DataPrivacyReport("i am dead inside already");
        internalDataPrivacyService.collectInternalUserData(personForDeletion, collectionReport);

        //check if second deletion of a deleted person works
        final IPersonDeletionReport secondDeletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(personForDeletion, secondDeletionReport);
    }

    protected Condition<String> erasedString() {
        return new Condition<>(" "::equals, "an erased string");
    }

    protected Condition<Long> erasedLong() {
        return new Condition<>(value -> value.equals(-1L), "an erased long");
    }

    protected Condition<Double> erasedDouble() {
        return new Condition<>(value -> value.equals(-1D), "an erased double");
    }

    protected Condition<String> randomString() {
        return new Condition<>(s -> ((s != null) && UUID_PATTERN.matcher(s).matches()), "a UUID");
    }

    protected Condition<String> deletedOauthId() {
        return new Condition<>(s -> ((s != null) && DELETED_OAUTH_ID_PATTERN.matcher(s).matches()), "deleted|<UUID>");
    }

    protected Condition<GPSLocation> erasedGpsLocation() {
        return new Condition<>(g -> new GPSLocation(0, 0).equals(g), "an erased GPS location");
    }

    protected Condition<IPersonDeletionReport> containsEntityWithOperation(BaseEntity entity,
            DeleteOperation operation) {
        assertThat(entity).isNotNull();
        assertThat(operation).isNotNull();
        return new Condition<>(r ->
                r.getDeletedEntities()
                        .stream()
                        .anyMatch(deletedEntity -> deletedEntity.getId().equals(entity.getId())
                                && deletedEntity.getEntityClass().equals(entity.getClass())
                                && deletedEntity.getOperation() == operation),
                "A report containing an entity of type %s with id %s and operation %s",
                entity.getClass().getName(),
                entity.getId(),
                operation.toString());
    }

    protected Condition<IDataPrivacyReport> containsMediaItemWithFilename(String fileName) {
        return new Condition<>(r ->
                r.getMediaItems()
                        .stream()
                        .anyMatch(mediaItem -> mediaItem.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName())
                                .equals(fileName)),
                "A report containing a MediaItem where urlsJson contains \"original\":\"%s\"",
                fileName);
    }

    protected Condition<IDataPrivacyReport> containsDocumentItemWithTitle(String title) {
        return new Condition<>(r ->
                r.getDocumentItems()
                        .stream()
                        .anyMatch(documentItem -> documentItem.getTitle().equals(title)),
                "A report containing a DocumentItem where the title is:\"%s\"",
                title);
    }

    protected static Condition<IDataPrivacyReport> sectionExists(final String name,
            final Consumer<IDataPrivacyReport.IDataPrivacyReportSection> andDo) {
        return new Condition<>(report -> {
            final Optional<IDataPrivacyReport.IDataPrivacyReportSection> section =
                    report.getSections().stream()
                            .filter(s -> s.getName().equals(name))
                            .findFirst();
            if (section.isEmpty()) {
                return false;
            }
            if (andDo != null) {
                andDo.accept(section.get());
            }
            return true;
        }, "A report containing a section with \"name\": \"%s\"", name);
    }

    protected static Condition<IDataPrivacyReport.IDataPrivacyReportSection> subSectionExists(final String name,
            final Consumer<IDataPrivacyReport.IDataPrivacyReportSubSection> andDo) {
        return new Condition<>(section -> {
            final Optional<IDataPrivacyReport.IDataPrivacyReportSubSection> subsection =
                    section.getSubSections().stream()
                            .filter(s -> s.getName().equals(name))
                            .findFirst();
            if (subsection.isEmpty()) {
                return false;
            }
            if (andDo != null) {
                andDo.accept(subsection.get());
            }
            return true;
        }, "A report containing a subsection with \"name\": \"%s\"", name);
    }

    private void ensureAllEntitiesOfModuleAreCoveredInTest() {

        //package name of the repositories we are looking at
        String packageName = "de.fhg.iese.dd.platform.datamanagement."+ StringUtils.split(this.getClass().getName(), '.')[6];

        //unfortunately we only get proxies here, due to the AOP nature of spring data
        //we can ask them for the interfaces they implement and get the JpaRepository interface
        //since we want to order them, they need to be stored in the map
        Map<Class<?>, JpaRepository<?, ?>> repositoriesInterfaces = new HashMap<>();

        Collection<Class<? extends JpaRepository<?, ?>>> excludedRepositories = getExcludedRepositories();
        for (JpaRepository<?, ?> repository : repositories) {
            //get all the interfaces of the proxies and
            //filter out the right interface that we are interested in
            Class<?> repositoryInterface = extractRepositoryInterface(repository);
            if (repositoryInterface != null &&
                    StringUtils.startsWith(repositoryInterface.getName(), packageName) &&
                    excludedRepositories.stream().noneMatch(excludedRepository -> excludedRepository.isAssignableFrom(repositoryInterface))
            ) {
                repositoriesInterfaces.put(repositoryInterface, repository);
            }
        }
        assertFalse(repositoriesInterfaces.isEmpty(), "No repositories to check! Check packageName ("+packageName+") and excludedRepositories");

        StringBuilder assertionMessage = new StringBuilder();
        //sort it according to the domain name
        repositoriesInterfaces.entrySet().stream()
                .sorted(Comparator.comparing(e -> e.getKey().getName()))
                .forEach(e -> {
                    JpaRepository<?, ?> repository = e.getValue();
                    if (repository.count() == 0L) {
                        assertionMessage.append("No entities of repository ")
                                .append(e.getKey().getName())
                                .append(" are created in the test setup\n");
                    }
                });
        if (assertionMessage.length() > 0) {
            Assertions.fail("Missing entities in DataPrivacyServiceTest:\n" + assertionMessage);
        }
    }

    private Class<?> extractRepositoryInterface(JpaRepository<?,?> repository){
        //get all the interfaces of the proxies and
        //filter out the right interface that we are interested in
        return Arrays.stream(repository.getClass().getInterfaces())
                .filter(JpaRepository.class::isAssignableFrom)
                .findFirst()
                .orElse(null);
    }

}
