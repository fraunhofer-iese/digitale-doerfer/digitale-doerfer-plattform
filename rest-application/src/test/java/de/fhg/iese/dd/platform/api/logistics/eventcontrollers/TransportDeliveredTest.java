/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredOtherPersonRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredPlacementRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredReceivedRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredRequest;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientSignature;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignmentStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportConfirmation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAssignmentStatus;

public class TransportDeliveredTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    private TransportAssignment transportAssignment;

    private String transportAssignmentId;

    private String deliveryTrackingCode;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        String shopOrderId = purchaseOrderReceived();
        purchaseOrderReadyForTransport(shopOrderId);

        waitForEventProcessing();

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        delivery = deliveries.get(0);
        deliveryTrackingCode = delivery.getTrackingCode();

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        transportAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);
        transportAssignmentId = transportAssignment.getId();

        waitForEventProcessing();

        transportPickupRequest(
            carrier, //carrier
            transportAssignmentId, //transportAssignmentId
            deliveryAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        waitForEventProcessing();

    }

    @Test
    public void onTransportDeliveredRequestHandOverPersonally() throws Exception {

        HandoverType handover = HandoverType.PERSONALLY;
        String deliveryTrackingCode = delivery.getTrackingCode();

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(handover)
                .deliveryTrackingCode(deliveryTrackingCode)
                .transportAssignmentId(transportAssignmentId)
                .handoverNotes("handoverNotes")
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));
    }

    @Test
    public void onTransportDeliveredRequestHandOverPlacement() throws Exception {

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(HandoverType.PLACEMENT)
                .deliveryTrackingCode(deliveryTrackingCode)
                .transportAssignmentId(transportAssignmentId)
                .handoverNotes("handoverNotes")
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));
    }

    @Test
    public void onTransportDeliveredRequestDeliveryTrackingCodeInvalid() throws Exception {

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(HandoverType.PLACEMENT)
                .deliveryTrackingCode("invalidcode")
                .transportAssignmentId(transportAssignmentId)
                .handoverNotes("handoverNotes")
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.WRONG_TRACKING_CODE));
    }

    @Test
    public void onTransportDeliveredRequestDeliveryTrackingCodeNull() throws Exception {

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
            .handover(HandoverType.PLACEMENT)
            .deliveryTrackingCode(null)
            .transportAssignmentId(transportAssignmentId)
            .handoverNotes("handoverNotes")
            .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportDeliveredRequestTransportAssignmentIdInvalid() throws Exception {

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(HandoverType.PLACEMENT)
                .deliveryTrackingCode(deliveryTrackingCode)
                .transportAssignmentId("invalidId")
                .handoverNotes("handoverNotes")
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));
    }

    @Test
    public void onTransportDeliveredRequestTransportAssignmentIdNull() throws Exception {

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(HandoverType.PLACEMENT)
                .deliveryTrackingCode(deliveryTrackingCode)
                .transportAssignmentId(null)
                .handoverNotes("handoverNotes")
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportDeliveredRequestUnauthorized() throws Exception {
        HandoverType handover = HandoverType.PERSONALLY;
        String deliveryTrackingCode = delivery.getTrackingCode();

        ClientTransportDeliveredRequest request = ClientTransportDeliveredRequest.builder()
                .handover(handover)
                .deliveryTrackingCode(deliveryTrackingCode)
                .transportAssignmentId(transportAssignmentId)
                .handoverNotes("handoverNotes")
                .build();

        assertOAuth2(post("/logistics/event/transportDeliveredRequest")
                .content(json(request))
                .contentType(contentType));
    }

    @Test
    public void onTransportDeliveredReceivedRequest() throws Exception {

        String signature = "signature" + UUID.randomUUID();

        ClientTransportDeliveredReceivedRequest request = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(signature))
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredReceivedRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));

        waitForEventProcessing();

        DeliveryStatusRecord deliveryStatusRecord =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery);
        assertEquals(DeliveryStatus.RECEIVED, deliveryStatusRecord.getStatus());

        TransportAssignmentStatusRecord assignmentStatusRecord =
                th.transportAssignmentStatusRecordRepository.findFirstByTransportAssignmentOrderByTimestampDesc(transportAssignment);
        assertEquals(TransportAssignmentStatus.RECEIVED, assignmentStatusRecord.getStatus());

        List<TransportConfirmation> actualConfirmations = th.transportConfirmationRepository.findAll();

        assertEquals(2, actualConfirmations.size());
        TransportConfirmation actualPickupConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentPickedUp(), transportAssignment)).findFirst().get();
        TransportConfirmation actualReceivedConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentReceived(), transportAssignment)).findFirst().get();

        assertEquals(CreatorType.CARRIER, actualPickupConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualPickupConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualPickupConfirmation.getTransportAssignmentPickedUp().getId());

        assertEquals(CreatorType.CARRIER, actualReceivedConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualReceivedConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualReceivedConfirmation.getTransportAssignmentReceived().getId());
        assertEquals(signature, actualReceivedConfirmation.getNotes());

    }

    @Test
    public void onTransportDeliveredReceivedRequestAfterDelivered() throws Exception {

        //deliver it as placement first
        String placementDescription = "placementDescription" + UUID.randomUUID();

        ClientTransportDeliveredPlacementRequest request = ClientTransportDeliveredPlacementRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .placementDescription(placementDescription)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredPlacementRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));

        waitForEventProcessing();

        //deliver and receive it afterwards

        String signature = "signature" + UUID.randomUUID();

        ClientTransportDeliveredReceivedRequest request2 = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(signature))
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredReceivedRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request2)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));

        waitForEventProcessing();

        DeliveryStatusRecord deliveryStatusRecord =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery);
        assertEquals(DeliveryStatus.RECEIVED, deliveryStatusRecord.getStatus());

        TransportAssignmentStatusRecord assignmentStatusRecord =
                th.transportAssignmentStatusRecordRepository.findFirstByTransportAssignmentOrderByTimestampDesc(transportAssignment);
        assertEquals(TransportAssignmentStatus.RECEIVED, assignmentStatusRecord.getStatus());

        List<TransportConfirmation> actualConfirmations = th.transportConfirmationRepository.findAll();

        assertEquals(3, actualConfirmations.size());
        TransportConfirmation actualPickupConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentPickedUp(), transportAssignment)).findFirst().get();
        TransportConfirmation actualDeliveredConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentDelivered(),
                        transportAssignment)).findFirst().get();
        TransportConfirmation actualReceivedConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentReceived(), transportAssignment)).findFirst().get();

        assertEquals(CreatorType.CARRIER, actualPickupConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualPickupConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualPickupConfirmation.getTransportAssignmentPickedUp().getId());

        assertEquals(CreatorType.CARRIER, actualDeliveredConfirmation.getCreator());
        assertEquals(HandoverType.PLACEMENT, actualDeliveredConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualDeliveredConfirmation.getTransportAssignmentDelivered().getId());
        assertEquals(placementDescription, actualDeliveredConfirmation.getNotes());

        assertEquals(CreatorType.CARRIER, actualReceivedConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualReceivedConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualReceivedConfirmation.getTransportAssignmentReceived().getId());
        assertEquals(signature, actualReceivedConfirmation.getNotes());

    }

    @Test
    public void onTransportDeliveredReceivedRequestUnauthorized() throws Exception {
        String signature = "signature" + UUID.randomUUID();

        ClientTransportDeliveredReceivedRequest request = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(signature))
                .build();

        assertOAuth2(post("/logistics/event/transportDeliveredReceivedRequest")
                .content(json(request))
                .contentType(contentType));

    }

    @Test
    public void onTransportDeliveredReceivedRequest_InvalidSignature() throws Exception {
        ClientTransportDeliveredReceivedRequest request = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(null)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredReceivedRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        request = ClientTransportDeliveredReceivedRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(""))
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredReceivedRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportDeliveredPlacementRequest() throws Exception {

        String placementDescription = "placementDescription" + UUID.randomUUID();

        ClientTransportDeliveredPlacementRequest request = ClientTransportDeliveredPlacementRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .placementDescription(placementDescription)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredPlacementRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));

        waitForEventProcessing();

        DeliveryStatusRecord deliveryStatusRecord =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery);
        assertEquals(DeliveryStatus.DELIVERED, deliveryStatusRecord.getStatus());

        TransportAssignmentStatusRecord assignmentStatusRecord =
                th.transportAssignmentStatusRecordRepository.findFirstByTransportAssignmentOrderByTimestampDesc(transportAssignment);
        assertEquals(TransportAssignmentStatus.DELIVERED, assignmentStatusRecord.getStatus());

        List<TransportConfirmation> actualConfirmations = th.transportConfirmationRepository.findAll();

        assertEquals(2, actualConfirmations.size());
        TransportConfirmation actualPickupConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentPickedUp(), transportAssignment)).findFirst().get();
        TransportConfirmation actualDeliveredConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentDelivered(),
                        transportAssignment)).findFirst().get();

        assertEquals(CreatorType.CARRIER, actualPickupConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualPickupConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualPickupConfirmation.getTransportAssignmentPickedUp().getId());

        assertEquals(CreatorType.CARRIER, actualDeliveredConfirmation.getCreator());
        assertEquals(HandoverType.PLACEMENT, actualDeliveredConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualDeliveredConfirmation.getTransportAssignmentDelivered().getId());
        assertEquals(placementDescription, actualDeliveredConfirmation.getNotes());
    }

    @Test
    public void onTransportDeliveredPlacementRequestUnauthorized() throws Exception {
        String placementDescription = "placementDescription" + UUID.randomUUID();

        ClientTransportDeliveredPlacementRequest request = ClientTransportDeliveredPlacementRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .placementDescription(placementDescription)
                .build();

        assertOAuth2(post("/logistics/event/transportDeliveredPlacementRequest")
                .content(json(request))
                .contentType(contentType));
    }

    @Test
    public void onTransportDeliveredOtherPersonRequest() throws Exception {

        String signature = "signature" + UUID.randomUUID();
        String acceptorName = "acceptorName" + UUID.randomUUID();

        ClientTransportDeliveredOtherPersonRequest request = ClientTransportDeliveredOtherPersonRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(signature))
                .acceptorName(acceptorName)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredOtherPersonRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignmentId))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(deliveryTrackingCode));

        waitForEventProcessing();

        DeliveryStatusRecord deliveryStatusRecord =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery);
        assertEquals(DeliveryStatus.DELIVERED, deliveryStatusRecord.getStatus());

        TransportAssignmentStatusRecord assignmentStatusRecord =
                th.transportAssignmentStatusRecordRepository.findFirstByTransportAssignmentOrderByTimestampDesc(transportAssignment);
        assertEquals(TransportAssignmentStatus.DELIVERED, assignmentStatusRecord.getStatus());

        List<TransportConfirmation> actualConfirmations = th.transportConfirmationRepository.findAll();

        assertEquals(2, actualConfirmations.size());
        TransportConfirmation actualPickupConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentPickedUp(), transportAssignment)).findFirst().get();
        TransportConfirmation actualDeliveredConfirmation = actualConfirmations.stream()
                .filter(c -> Objects.equals(c.getTransportAssignmentDelivered(),
                        transportAssignment)).findFirst().get();

        assertEquals(CreatorType.CARRIER, actualPickupConfirmation.getCreator());
        assertEquals(HandoverType.PERSONALLY, actualPickupConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualPickupConfirmation.getTransportAssignmentPickedUp().getId());

        assertEquals(CreatorType.CARRIER, actualDeliveredConfirmation.getCreator());
        assertEquals(HandoverType.OTHER_PERSON, actualDeliveredConfirmation.getHandover());
        assertEquals(transportAssignmentId, actualDeliveredConfirmation.getTransportAssignmentDelivered().getId());
        assertEquals(acceptorName, actualDeliveredConfirmation.getAcceptorName());
        assertEquals(signature, actualDeliveredConfirmation.getNotes());
    }

    @Test
    public void onTransportDeliveredOtherPersonRequestUnauthorized() throws Exception {
        String signature = "signature" + UUID.randomUUID();
        String acceptorName = "acceptorName" + UUID.randomUUID();

        ClientTransportDeliveredOtherPersonRequest request = ClientTransportDeliveredOtherPersonRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(signature))
                .acceptorName(acceptorName)
                .build();

        assertOAuth2(post("/logistics/event/transportDeliveredOtherPersonRequest")
                .content(json(request))
                .contentType(contentType));
    }

    @Test
    public void onTransportDeliveredOtherPersonRequest_InvalidSignature() throws Exception {
        String acceptorName = "acceptorName" + UUID.randomUUID();

        ClientTransportDeliveredOtherPersonRequest request = ClientTransportDeliveredOtherPersonRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(null)
                .acceptorName(acceptorName)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredOtherPersonRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        request = ClientTransportDeliveredOtherPersonRequest.builder()
                .transportAssignmentId(transportAssignmentId)
                .deliveryTrackingCode(deliveryTrackingCode)
                .signature(new ClientSignature(""))
                .acceptorName(acceptorName)
                .build();

        mockMvc.perform(post("/logistics/event/transportDeliveredOtherPersonRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

}
