/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

public class AppControllerAvailableGeoAreaNearestTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private AppVariant appVariantNoGeoAreas;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        final App apiKeyApp = th.appRepository.save(App.builder()
                .name("api key app")
                .appIdentifier("apikey.app")
                .build());

        appVariantNoGeoAreas = th.appVariantRepository.save(AppVariant.builder()
                .name("app variant no tenant")
                .appVariantIdentifier("appvariant.no.tenant")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .oauthClients(Collections.singleton(th.oauthClientRepository.save(OauthClient.builder()
                        .oauthClientIdentifier("oauth-client-no-tenant")
                        .build())))
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getNearestAvailableGeoAreasForAppVariant() throws Exception {

        final List<ClientGeoArea> expectedResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true))));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/nearest")
                        .param("latitude", "49.2")
                        .param("longitude", "8.35")
                        .param("radiusKM", "7.5")
                        .param("onlyLeaves", "false"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult)));
    }

    @Test
    public void getNearestAvailableGeoAreasForAppVariant_OnlyLeafGeoAreas() throws Exception {

        final List<ClientGeoArea> expectedResult = Arrays.asList(
                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/nearest")
                        .param("latitude", "49.2")
                        .param("longitude", "8.35")
                        .param("radiusKM", "7.5")
                        .param("onlyLeaves", "true"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult)));
    }

    @Test
    public void getNearestAvailableGeoAreasForAppVariant_AppVariantWithoutAvailableGeoAreas() throws Exception {

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariantNoGeoAreas,
                () -> get("/app/appVariant/availableGeoAreas/nearest")
                        .param("latitude", "49.2")
                        .param("longitude", "8.35"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$").isEmpty()));
    }

    @Test
    public void getNearestAvailableGeoAreasForAppVariant_InvalidAppVariant() throws Exception {

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/nearest")
                .param("latitude", "49.2")
                .param("longitude", "8.35")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(BaseTestHelper.INVALID_UUID)
                        .build()
                        .withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getNearestAvailableGeoAreasForAppVariant_NegativeRadius() throws Exception {

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/nearest")
                        .param("latitude", "49.2")
                        .param("longitude", "8.35")
                        .param("radiusKM", "-1.0"),
                r -> r.andExpect(isException(ClientExceptionType.QUERY_PARAMETER_INVALID)));
    }

    @Test
    public void getNearestGeoAreaForAppVariant() throws Exception {

        final AppVariant appVariantWithTenant1Only = th.app1Variant1;
        final AppVariant appVariantWithTenant1And2 = th.app2Variant1;

        //see "graphical" illustration in SharedTestHelper#createGeoAreas
        final GPSLocation pointInDorf2 = new GPSLocation(49.12, 8.315);
        final GPSLocation pointClosestToButOutsideKL = new GPSLocation(49.27, 8.01);
        final GPSLocation pointInMainz = new GPSLocation(49.28, 8.202);
        final GPSLocation pointFarFarAway = new GPSLocation(1.12, 1.315);

        assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(appVariantWithTenant1And2,
                pointInDorf2,
                th.geoAreaDorf2InEisenberg);
        assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(appVariantWithTenant1And2,
                pointClosestToButOutsideKL,
                th.geoAreaKaiserslautern);
        assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(appVariantWithTenant1And2,
                pointInMainz,
                th.geoAreaMainz);
        //Mainz does not belong to tenant "tenant1". Nearest geoArea of tenant "tenant1" is Kaiserslautern
        assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(appVariantWithTenant1Only,
                pointInMainz,
                th.geoAreaKaiserslautern);
        assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(appVariantWithTenant1And2,
                pointFarFarAway,
                null);
    }

    private void assertNearestGeoAreaForAppVariantAndLocationReturnsGeoArea(AppVariant appVariant,
            GPSLocation location, GeoArea expectedGeoArea) throws Exception {

        final ResultActions resultActions =
                mockMvc.perform(get("/app/appVariant/nearestGeoArea")
                                .headers(authHeadersFor(AuthorizationData.builder()
                                        .appVariantIdentifier(appVariant.getAppVariantIdentifier())
                                        .build().withoutAccessToken()))
                                .param("latitude", String.valueOf(location.getLatitude()))
                                .param("longitude", String.valueOf(location.getLongitude())))
                        .andExpect(status().isOk());

        if (expectedGeoArea == null) {
            resultActions.andExpect(content().string(""));
        } else {

            resultActions.andExpect(content().contentType(contentType))
                    //.andDo(MockMvcResultHandlers.print())
                    .andExpect(jsonPath("$.name").value(expectedGeoArea.getName()))
                    .andExpect(jsonPath("$.id").value(expectedGeoArea.getId()))
                    .andExpect(jsonPath("$.boundaryPoints").doesNotExist())
                    .andExpect(jsonPath("$.center.latitude").value(expectedGeoArea.getCenter().getLatitude()))
                    .andExpect(jsonPath("$.center.longitude").value(expectedGeoArea.getCenter().getLongitude()))
                    .andExpect(jsonPath("$.logo").isNotEmpty());
        }

        // TODO: Can be removed after the current endpoint is changed to the new one
        final ResultActions resultActionsOld =
                mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/nearestGeoArea",
                                appVariant.getAppVariantIdentifier())
                                .param("latitude", String.valueOf(location.getLatitude()))
                                .param("longitude", String.valueOf(location.getLongitude())))
                        .andExpect(status().isOk());

        if (expectedGeoArea == null) {
            resultActionsOld.andExpect(content().string(""));
        } else {

            resultActionsOld.andExpect(content().contentType(contentType))
                    //.andDo(MockMvcResultHandlers.print())
                    .andExpect(jsonPath("$.id").value(expectedGeoArea.getId()))
                    .andExpect(jsonPath("$.name").value(expectedGeoArea.getName()))
                    .andExpect(jsonPath("$.boundaryPoints").doesNotExist())
                    .andExpect(jsonPath("$.center.latitude").value(expectedGeoArea.getCenter().getLatitude()))
                    .andExpect(jsonPath("$.center.longitude").value(expectedGeoArea.getCenter().getLongitude()))
                    .andExpect(jsonPath("$.logo").isNotEmpty());
        }
    }

}
