/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlag;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagDetail;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagStatusRecordRepository;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("flag-admin-ui")
public abstract class BaseUserGeneratedContentFlagAdminUiTest extends BaseServiceTest {

    @Autowired
    protected SharedTestHelper sharedTestHelper;

    @Autowired
    protected UserGeneratedContentFlagRepository flagRepository;

    @Autowired
    protected UserGeneratedContentFlagStatusRecordRepository flagStatusRecordRepository;

    protected BaseSharedTestHelper th;

    protected UserGeneratedContentFlag flag1Tenant1;
    protected UserGeneratedContentFlag flag2Tenant1;
    protected UserGeneratedContentFlag flag3Tenant1;
    protected UserGeneratedContentFlag flag1Tenant2;
    protected UserGeneratedContentFlag flag2Tenant2;
    protected UserGeneratedContentFlag flag3Tenant2;
    protected UserGeneratedContentFlag flagNoTenant;
    protected UserGeneratedContentFlag flagNoFlagCreator;
    protected List<UserGeneratedContentFlag> userGeneratedContentFlags;
    protected Map<UserGeneratedContentFlagStatus, List<UserGeneratedContentFlag>> userGeneratedContentFlagsByStatus;

    @PostConstruct
    private void initTestHelper() {
        //this is required because we use different test helpers in the subclasses of this test
        th = getTestHelper();
    }

    protected void createFlags() {

        // Flags Tenant 1
        flag1Tenant1 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityFlag1Tenant1().getId())
                .entityType(getFlagEntityFlag1Tenant1().getClass().getName())
                .entityAuthor(th.personRegularAnna)
                .flagCreator(th.personUserGeneratedContentAdminTenant1)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build());

        flag1Tenant1.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personUserGeneratedContentAdminTenant1)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Blöder Global Admin und so...")
                        .userGeneratedContentFlag(flag1Tenant1)
                        .build())));

        flag2Tenant1 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(th.nextTimeStamp())
                .entityId(getFlagEntityFlag2Tenant1().getId())
                .entityType(getFlagEntityFlag2Tenant1().getClass().getName())
                .entityAuthor(th.personVgAdmin)
                .flagCreator(th.personUserGeneratedContentAdminTenant1)
                .status(UserGeneratedContentFlagStatus.IN_PROGRESS)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build());

        flag2Tenant1.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personUserGeneratedContentAdminTenant1)
                        .status(UserGeneratedContentFlagStatus.IN_PROGRESS)
                        .comment("Die schreibt nur Mist")
                        .userGeneratedContentFlag(flag2Tenant1)
                        .build())));

        flag3Tenant1 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityFlag3Tenant1().getId())
                .entityType(getFlagEntityFlag3Tenant1().getClass().getName())
                .entityAuthor(th.personShopOwner)
                .flagCreator(th.personRegularAnna)
                .status(UserGeneratedContentFlagStatus.REJECTED)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build());

        UserGeneratedContentFlagStatusRecord flag3Tenant1StatusRecord1 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personRegularAnna)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Kuck mal wie der kuckt! 😮")
                        .userGeneratedContentFlag(flag3Tenant1)
                        .build());
        UserGeneratedContentFlagStatusRecord flag3Tenant1StatusRecord2 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personRegularAnna)
                        .status(UserGeneratedContentFlagStatus.REJECTED)
                        .comment("Du kuckst auch so dusselig! 😂")
                        .userGeneratedContentFlag(flag3Tenant1)
                        .build());
        flag3Tenant1.setUserGeneratedContentFlagStatusRecords(Arrays.asList(
                flag3Tenant1StatusRecord2,
                flag3Tenant1StatusRecord1));

        // Flags Tenant 2
        flag1Tenant2 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityFlag1Tenant2().getId())
                .entityType(getFlagEntityFlag1Tenant2().getClass().getName())
                .entityAuthor(null)
                .flagCreator(th.personUserGeneratedContentAdminTenant2)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant2)
                .build());

        flag1Tenant2.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personUserGeneratedContentAdminTenant2)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Hat einen doofen Namen")
                        .userGeneratedContentFlag(flag1Tenant2)
                        .build())));

        flag2Tenant2 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityFlag2Tenant2().getId())
                .entityType(getFlagEntityFlag2Tenant2().getClass().getName())
                .entityAuthor(th.personRegularHorstiTenant3)
                .flagCreator(th.personGlobalUserGeneratedContentAdmin)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant2)
                .build());

        flag2Tenant2.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personGlobalUserGeneratedContentAdmin)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Bla bla bla")
                        .userGeneratedContentFlag(flag2Tenant2)
                        .build())));

        flag3Tenant2 = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityFlag3Tenant2().getId())
                .entityType(getFlagEntityFlag3Tenant2().getClass().getName())
                .entityAuthor(th.personIeseAdmin)
                .flagCreator(th.personShopManager)
                .status(UserGeneratedContentFlagStatus.ACCEPTED)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant2)
                .build());

        UserGeneratedContentFlagStatusRecord flag3Tenant2StatusRecord1 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personShopManager)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Da ist ein fleckiger Fleck! 🐛")
                        .userGeneratedContentFlag(flag3Tenant2)
                        .build());
        UserGeneratedContentFlagStatusRecord flag3Tenant2StatusRecord2 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personShopManager)
                        .status(UserGeneratedContentFlagStatus.IN_PROGRESS)
                        .comment("Ich kuck mal wie fleckig der ist 😏")
                        .userGeneratedContentFlag(flag3Tenant2)
                        .build());
        UserGeneratedContentFlagStatusRecord flag3Tenant2StatusRecord3 =
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personShopManager)
                        .status(UserGeneratedContentFlagStatus.ACCEPTED)
                        .comment("Jep, ist ein Fettfleck")
                        .userGeneratedContentFlag(flag3Tenant2)
                        .build());
        flag3Tenant2.setUserGeneratedContentFlagStatusRecords(Arrays.asList(
                flag3Tenant2StatusRecord3,
                flag3Tenant2StatusRecord2,
                flag3Tenant2StatusRecord1));

        //Flags no tenant
        flagNoTenant = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityNoTenant().getId())
                .entityType(getFlagEntityNoTenant().getClass().getName())
                .entityAuthor(th.personRegularAnna)
                .flagCreator(th.personUserGeneratedContentAdminTenant1)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(null)
                .build());

        flagNoTenant.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(th.personUserGeneratedContentAdminTenant1)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("Kein Mandant ist nicht illegal!")
                        .userGeneratedContentFlag(flagNoTenant)
                        .build())));

        //Flags no flag creator
        flagNoFlagCreator = flagRepository.save(UserGeneratedContentFlag.builder()
                .created(getTestHelper().nextTimeStamp())
                .entityId(getFlagEntityNoTenant().getId())
                .entityType(getFlagEntityNoTenant().getClass().getName())
                .entityAuthor(th.personRegularAnna)
                .flagCreator(null)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(null)
                .build());

        flagNoFlagCreator.setUserGeneratedContentFlagStatusRecords(Collections.singletonList(
                flagStatusRecordRepository.save(UserGeneratedContentFlagStatusRecord.builder()
                        .created(th.nextTimeStamp())
                        .initiator(null)
                        .status(UserGeneratedContentFlagStatus.OPEN)
                        .comment("⚠ Autogeneriert ⚠ Wert: " + 0.8 + " (>" + 0.7 + ")")
                        .userGeneratedContentFlag(flagNoFlagCreator)
                        .build())));

        userGeneratedContentFlags =
                Arrays.asList(flag1Tenant1, flag2Tenant1, flag3Tenant1, flag1Tenant2, flag2Tenant2, flag3Tenant2,
                        flagNoTenant, flagNoFlagCreator);

        userGeneratedContentFlagsByStatus = userGeneratedContentFlags.stream()
                .collect(Collectors.groupingBy(UserGeneratedContentFlag::getStatus));
    }

    protected BaseSharedTestHelper getTestHelper() {
        return sharedTestHelper;
    }

    protected BaseEntity getFlagEntityFlag1Tenant1() {
        // Person was changed in order to trigger the test handler
        return th.personIeseAdmin.getProfilePicture();
    }

    protected BaseEntity getFlagEntityFlag2Tenant1() {
        return th.personRegularAnna;
    }

    protected BaseEntity getFlagEntityFlag3Tenant1() {
        return th.personUserGeneratedContentAdminTenant1;
    }

    protected BaseEntity getFlagEntityFlag1Tenant2() {
        return th.geoAreaTenant1;
    }

    protected BaseEntity getFlagEntityFlag2Tenant2() {
        return th.personRoleManagerTenant2;
    }

    protected BaseEntity getFlagEntityFlag3Tenant2() {
        return th.geoAreaDorf2InEisenberg;
    }

    protected BaseEntity getFlagEntityNoTenant() {
        return th.geoAreaDeutschland;
    }

    protected void assertFlagsEqual(List<UserGeneratedContentFlag> expected,
            List<ClientUserGeneratedContentFlag> actual) {

        assertThat(expected).as("empty expectations are not useful").isNotEmpty();
        assertThat(actual).hasSameSizeAs(expected);

        for (int i = 0; i < expected.size(); i++) {
            UserGeneratedContentFlag expectedFlag = expected.get(i);
            ClientUserGeneratedContentFlag actualFlag = actual.get(i);

            assertFlagEquals(expectedFlag, actualFlag);
        }
    }

    protected void assertFlagEquals(UserGeneratedContentFlag expectedFlag,
            ClientUserGeneratedContentFlagDetail actualFlag) {

        assertFlagEquals(expectedFlag, (ClientUserGeneratedContentFlag) actualFlag);

        List<UserGeneratedContentFlagStatusRecord> expectedStatusRecords =
                expectedFlag.getUserGeneratedContentFlagStatusRecords();
        List<ClientUserGeneratedContentFlagStatusRecord> actualStatusRecords = actualFlag.getStatusRecords();

        assertEquals(expectedStatusRecords.size(), actualStatusRecords.size(), "Status records differ");

        for (int i = 0; i < expectedStatusRecords.size(); i++) {
            UserGeneratedContentFlagStatusRecord expectedStatusRecord = expectedStatusRecords.get(i);
            ClientUserGeneratedContentFlagStatusRecord actualStatusRecord = actualStatusRecords.get(i);
            assertFlagStatusRecordEquals(expectedStatusRecord, actualStatusRecord);
        }
    }

    protected void assertFlagEquals(UserGeneratedContentFlag expectedFlag, ClientUserGeneratedContentFlag actualFlag) {

        assertThat(actualFlag.getId()).isEqualTo(expectedFlag.getId());
        assertThat(actualFlag.getEntityId()).isEqualTo(expectedFlag.getEntityId());
        assertThat(actualFlag.getEntityType()).isEqualTo(expectedFlag.getEntityType());
        assertThat(actualFlag.getStatus()).isEqualTo(expectedFlag.getStatus());
        assertThat(actualFlag.getLastStatusUpdate()).isEqualTo(expectedFlag.getLastStatusUpdate());
        if (expectedFlag.getTenant() == null) {
            assertThat(actualFlag.getTenant()).isNull();
        } else {
            assertThat(actualFlag.getTenant().getId()).isEqualTo(expectedFlag.getTenant().getId());
        }
        if (expectedFlag.getFlagCreator() == null) {
            assertThat(actualFlag.getFlagCreator()).isNull();
        } else {
            assertThat(actualFlag.getFlagCreator().getId()).isEqualTo(expectedFlag.getFlagCreator().getId());
        }
        if (expectedFlag.getEntityAuthor() != null) {
            assertThat(actualFlag.getEntityAuthor().getId()).isEqualTo(expectedFlag.getEntityAuthor().getId());
        }
        assertFlagStatusRecordEquals(expectedFlag.getUserGeneratedContentFlagStatusRecords().get(0),
                actualFlag.getLastStatusRecord());
    }

    protected void assertFlagStatusRecordEquals(UserGeneratedContentFlagStatusRecord expectedStatusRecord,
            ClientUserGeneratedContentFlagStatusRecord actualStatusRecord) {

        assertThat(expectedStatusRecord).isNotNull();
        assertThat(actualStatusRecord).isNotNull();
        assertThat(actualStatusRecord.getId()).isEqualTo(expectedStatusRecord.getId());
        if (expectedStatusRecord.getInitiator() == null) {
            assertThat(actualStatusRecord.getInitiator()).isNull();
        } else {
            assertThat(actualStatusRecord.getInitiator().getId()).isEqualTo(
                    expectedStatusRecord.getInitiator().getId());
        }
        assertThat(actualStatusRecord.getCreated()).isEqualTo(expectedStatusRecord.getCreated());
        assertThat(actualStatusRecord.getComment()).isEqualTo(expectedStatusRecord.getComment());
        assertThat(actualStatusRecord.getStatus()).isEqualTo(expectedStatusRecord.getStatus());
    }

}
