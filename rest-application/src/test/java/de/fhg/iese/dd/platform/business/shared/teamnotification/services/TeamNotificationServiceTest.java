/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.temporal.ChronoUnit;
import java.util.EnumSet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.mocks.MockTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.mocks.MockTeamNotificationPublisherProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationChannelMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.TeamNotificationConnection;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;

public class TeamNotificationServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    protected TestTimeService timeService;

    //Service implementation is used directly to call invalidateCacheAndStopRetry
    @Autowired
    private TeamNotificationService teamNotificationService;

    @Autowired
    private MockTeamNotificationPublisher mockTeamNotificationPublisher;

    private TeamNotificationConnection logConnection;
    private TeamNotificationChannelMapping channelMappingTenant1Topic1;
    private TeamNotificationChannelMapping channelMappingTenant2Topic1;
    private TeamNotificationChannelMapping channelMappingTenant3Common;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();

        updateConfiguration();
        mockTeamNotificationPublisher.reset();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        mockTeamNotificationPublisher.reset();
    }

    public void updateConfiguration() {

        logConnection = TeamNotificationConnection.builder()
                .name("Test-Log-Connection")
                .connectionConfig("")
                .teamNotificationConnectionType(MockTeamNotificationPublisherProvider.CONNECTION_TYPE)
                .build();

        channelMappingTenant1Topic1 = TeamNotificationChannelMapping.builder()
                .topic("Topic1")
                .channel("channelTenant1Topic1")
                .tenant(th.tenant1)
                .teamNotificationConnection(logConnection)
                .build();
        channelMappingTenant1Topic1.getAllowedPriorities().setValues(EnumSet.allOf(TeamNotificationPriority.class));

        channelMappingTenant2Topic1 = TeamNotificationChannelMapping.builder()
                .topic("Topic1")
                .channel("channelTenant2Topic1")
                .tenant(th.tenant2)
                .teamNotificationConnection(logConnection)
                .build();
        channelMappingTenant2Topic1.getAllowedPriorities().setValues(EnumSet.allOf(TeamNotificationPriority.class));

        channelMappingTenant3Common = TeamNotificationChannelMapping.builder()
                .topic("*")
                .channel("channeTenanty2Topic1")
                .tenant(th.tenant3)
                .teamNotificationConnection(logConnection)
                .build();
        channelMappingTenant3Common.getAllowedPriorities().setValues(EnumSet.allOf(TeamNotificationPriority.class));

        teamNotificationService.store(logConnection);
        teamNotificationService.store(channelMappingTenant1Topic1);
        teamNotificationService.store(channelMappingTenant2Topic1);
        teamNotificationService.store(channelMappingTenant3Common);

        teamNotificationService.invalidateCache();
    }

    @Test
    public void sendTeamNotification() {

        teamNotificationService.sendTeamNotification(th.tenant1, channelMappingTenant1Topic1.getTopic(),
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}", 42);

        assertEquals(channelMappingTenant1Topic1.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals(channelMappingTenant1Topic1.getTopic(), mockTeamNotificationPublisher.getLastTopic());
        assertEquals(th.tenant1, mockTeamNotificationPublisher.getLastTenant());
        assertEquals("Message 42", mockTeamNotificationPublisher.getLastMessage());
        assertEquals(1, mockTeamNotificationPublisher.getCountMessages());
    }

    @Test
    public void sendTeamNotification_Common() {

        teamNotificationService.sendTeamNotification(th.tenant3, "Any topic",
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}{}", 4, 2);

        assertEquals(channelMappingTenant3Common.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals("Message 42", mockTeamNotificationPublisher.getLastMessage());
        assertEquals(1, mockTeamNotificationPublisher.getCountMessages());
    }

    @Test
    public void sendTeamNotification_NoMatchingTopic() {

        teamNotificationService.sendTeamNotification(th.tenant1, "my-secret-topic",
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}", 42);

        assertNull(mockTeamNotificationPublisher.getLastChannel());
        assertNull(mockTeamNotificationPublisher.getLastMessage());
        assertEquals(0, mockTeamNotificationPublisher.getCountMessages());
    }

    @Test
    public void sendTeamNotification_FallbackTopic() {

        TeamNotificationChannelMapping fallback = TeamNotificationChannelMapping.builder()
                .topic("[*]")
                .channel("fallback")
                .tenant(null)
                .teamNotificationConnection(logConnection)
                .build();
        fallback.getAllowedPriorities().setValues(EnumSet.allOf(TeamNotificationPriority.class));
        teamNotificationService.store(fallback);
        teamNotificationService.invalidateCache();

        teamNotificationService.sendTeamNotification(th.tenant1, "my-secret-topic",
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Hidden {}", "massage");

        assertEquals(fallback.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals("Hidden massage", mockTeamNotificationPublisher.getLastMessage());
        assertEquals(1, mockTeamNotificationPublisher.getCountMessages());
    }

    @Test
    public void sendTeamNotification_FallbackTopicNotTriggered() {

        TeamNotificationChannelMapping fallback = TeamNotificationChannelMapping.builder()
                .topic("[*]")
                .channel("fallback")
                .tenant(null)
                .teamNotificationConnection(logConnection)
                .build();
        fallback.getAllowedPriorities().setValues(EnumSet.allOf(TeamNotificationPriority.class));
        teamNotificationService.store(fallback);
        teamNotificationService.invalidateCache();

        teamNotificationService.sendTeamNotification(th.tenant3, "Any topic",
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}{}", 4, 2);

        assertEquals(channelMappingTenant3Common.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals("Message 42", mockTeamNotificationPublisher.getLastMessage());
        assertEquals(1, mockTeamNotificationPublisher.getCountMessages());
    }

    @Test
    public void sendTeamNotification_ExceptionInPublisher() throws Exception {

        mockTeamNotificationPublisher.setExceptionOnSend(() -> new RuntimeException("Test-Exception"));

        teamNotificationService.sendTeamNotification(th.tenant1, channelMappingTenant1Topic1.getTopic(),
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}", "Exceptionem");

        assertEquals(channelMappingTenant1Topic1.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals(channelMappingTenant1Topic1.getTopic(), mockTeamNotificationPublisher.getLastTopic());
        assertEquals(th.tenant1, mockTeamNotificationPublisher.getLastTenant());
        assertEquals("Message Exceptionem", mockTeamNotificationPublisher.getLastMessage());
        //Exceptions are only logged and no retry happens. Any retry is done in the publisher itself
        assertEquals(1, mockTeamNotificationPublisher.getCountMessages());
        teamNotificationService.invalidateCache();
    }

    @Test
    public void updateConfig() {

        //this is required to update the cache, so that it is in the "not update" interval
        teamNotificationService.sendTeamNotification(th.tenant1, channelMappingTenant1Topic1.getTopic(),
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "1");

        String oldChannel = channelMappingTenant1Topic1.getChannel();
        channelMappingTenant1Topic1.setChannel("newChannelTenant1Topic1");
        teamNotificationService.store(channelMappingTenant1Topic1);

        //change of the reference data is required to trigger a cache change
        th.simulateReferenceDataChange();

        teamNotificationService.sendTeamNotification(th.tenant1, channelMappingTenant1Topic1.getTopic(),
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "2");

        //the change should not trigger the cache to reload, since it is withing the reload interval
        assertEquals(oldChannel, mockTeamNotificationPublisher.getLastChannel());

        //this triggers the cache reload in teamNotificationService
        timeService.setOffset(5, ChronoUnit.MINUTES);

        teamNotificationService.sendTeamNotification(th.tenant1, channelMappingTenant1Topic1.getTopic(),
                TeamNotificationPriority.DEBUG_TECHNICAL_SINGLE_USER, "Message {}", 42);

        assertEquals(channelMappingTenant1Topic1.getChannel(), mockTeamNotificationPublisher.getLastChannel());
        assertEquals("Message 42", mockTeamNotificationPublisher.getLastMessage());
        assertEquals(3, mockTeamNotificationPublisher.getCountMessages());
        teamNotificationService.invalidateCache();
    }

}
