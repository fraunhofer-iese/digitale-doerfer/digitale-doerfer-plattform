/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Johannes Schneider, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.ClientPostPageBean;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests that group posts are listed along the other posts when calling get post for selected geo areas, own
 * posts and activity.
 */
public class GroupPostControllerTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    private AppVariant appVariant;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        th.createAdditionalGossipsAndCommentsWithoutGroup();

        appVariant = th.appVariantKL_EB;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void postsForSelectedGeoArea_groupMember() throws Exception {
        Person caller = th.personAnjaTenant1Dorf1;

        Set<Group> memberGroups = th.groupMembershipRepository.findAllByMemberOrderByCreatedDesc(caller).stream()
                .filter(groupMembership -> groupMembership.getStatus() == GroupMembershipStatus.APPROVED)
                .map(GroupMembership::getGroup)
                .collect(Collectors.toSet());

        List<ClientPost> expectedPosts = Stream.concat(
                th.groupsToPostsAndComments.entrySet()
                        .stream()
                        .filter(groupListEntry -> ! groupListEntry.getKey().isDeleted())
                        .filter(groupListEntry -> memberGroups.contains(groupListEntry.getKey()))
                        .map(Map.Entry::getValue)
                        .flatMap(gossipsWithComments -> gossipsWithComments.stream()
                                .map(Pair::getLeft)),
                Stream.of(th.gossipDorf1WithComments.getLeft())
        )
                .filter(Gossip::isNotDeleted)
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .map(th::toCensoredClientPost)
                .collect(Collectors.toList());
        assertFalse(expectedPosts.isEmpty());

        assertPostsFromRestEndpointContainPosts("/grapevine/postBySelectedGeoAreas", caller, expectedPosts);

        //result should not change when caller has other group membership status w.r.t. a restricted group
        final Group restrictedGroup = th.groupMessdienerSMA;
        th.iterateOverNonMemberGroupMembershipStatus(caller, restrictedGroup,
                membershipStatus -> assertPostsFromRestEndpointContainPosts("/grapevine/postBySelectedGeoAreas", caller,
                        expectedPosts));
    }

    @Test
    public void postsForSelectedGeoArea_notGroupMember() throws Exception {
        Person caller = th.personEckhardTenant1Dorf1NoMember;

        List<ClientPost> expectedPosts =
                Collections.singletonList(th.toCensoredClientPost(th.gossipDorf1WithComments.getLeft()));

        assertPostsFromRestEndpointContainPosts("/grapevine/postBySelectedGeoAreas", caller, expectedPosts);

        //result should not change when caller has other group membership status w.r.t. a restricted group
        final Group restrictedGroup = th.groupErfahrungMMA;
        th.iterateOverNonMemberGroupMembershipStatus(caller, restrictedGroup,
                membershipStatus -> assertPostsFromRestEndpointContainPosts("/grapevine/postBySelectedGeoAreas", caller,
                        expectedPosts));
    }

    @Test
    public void postsForSelectedGeoArea_emptyList() throws Exception {

        Person caller = th.personIeseAdmin; //should have no geo areas of the GroupTestHelper posts selected

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .param("page", "0")
                .param("count", "50")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    public void postsForSelectedGeoArea_PersonWithoutHomeAreaAndSelectedAreas() throws Exception {

        Person person = th.personDieterTenant2MainzNoMember;
        person.setHomeArea(null);
        person = th.personRepository.saveAndFlush(person);
        th.unmapAllGeoAreasFromAppVariant(appVariant);
        th.appVariantUsageAreaSelectionRepository.deleteAll();

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .param("page", "0")
                .param("count", "50")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    public void getOwnPosts_groupMember() throws Exception {

        Person caller = th.personAnjaTenant1Dorf1;

        List<ClientPost> expectedPosts = Stream.concat(
                        th.groupsToPostsAndComments.values().stream()
                        .flatMap(gossipsWithComments -> gossipsWithComments.stream()
                                .map(Pair::getLeft)),
                Stream.of(th.gossipDorf1WithComments.getLeft())
        )
                .filter(gossip -> !gossip.isDeleted() && gossip.getCreator().equals(caller))
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .map(th::toCensoredClientPost)
                .collect(Collectors.toList());
        assertFalse(expectedPosts.isEmpty());

        assertPostsFromRestEndpointContainPosts("/grapevine/post/own", caller, expectedPosts);
    }

    @Test
    public void getOwnPosts_notGroupMember() throws Exception {

        Person caller = th.personDieterTenant2MainzNoMember;

        List<ClientPost> expectedPosts = Collections.singletonList(
                th.toCensoredClientPost(th.gossipMainzWithComments.getLeft()));

        assertPostsFromRestEndpointContainPosts("/grapevine/post/own", caller, expectedPosts);
    }

    @Test
    public void getOwnPosts_emptyList() throws Exception {

        Person caller = th.personEckhardTenant1Dorf1NoMember;
        //test data integrity
        assertFalse(th.groupsToPostsAndComments
                .values()
                .stream()
                .flatMap(gossipsWithComments -> gossipsWithComments.stream()
                        .map(Pair::getLeft))
                .anyMatch(gossip -> gossip.getCreator().equals(caller)));

        mockMvc.perform(get("/grapevine/post/own")
                .param("page", "0")
                .param("count", "50")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    public void activity_groupMember() throws Exception {

        Person caller = th.personAnjaTenant1Dorf1;

        List<ClientPost> expectedPosts = Stream.concat(
                th.groupsToPostsAndComments
                        .values()
                        .stream()
                        .flatMap(Collection::stream),
                Stream.of(th.gossipDorf1WithComments, th.gossipMainzWithComments)
        )
                .filter(pair -> {
                    final Gossip gossip = pair.getLeft();
                    final List<Comment> comments = pair.getRight();
                    if (gossip.isDeleted() || gossip.getCreator().equals(caller)) {
                        return false;
                    }
                    for (Comment c : comments) {
                        if (c.getCreator().equals(caller) && c.isDeleted()) {
                            return false;
                        }
                    }
                    return comments.stream()
                            .anyMatch(comment -> comment.getCreator().equals(caller));
                })
                .map(Pair::getLeft)
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .map(th::toCensoredClientPost)
                .collect(Collectors.toList());
        assertFalse(expectedPosts.isEmpty());

        assertPostsFromRestEndpointContainPosts("/grapevine/activity", caller, expectedPosts);

        //result should not change when caller has other group membership status w.r.t. a restricted group
        final Group restrictedGroup = th.groupMessdienerSMA;
        th.iterateOverNonMemberGroupMembershipStatus(caller, restrictedGroup,
                membershipStatus -> assertPostsFromRestEndpointContainPosts("/grapevine/activity", caller,
                        expectedPosts));
    }

    @Test
    public void activity_notGroupMember() throws Exception {

        Person caller = th.personEckhardTenant1Dorf1NoMember;

        List<ClientPost> expectedPosts = Collections.singletonList(
                th.toCensoredClientPost(th.gossipDorf1WithComments.getLeft()));

        assertPostsFromRestEndpointContainPosts("/grapevine/activity", caller, expectedPosts);

        //result should not change when caller has other group membership status w.r.t. a restricted group
        final Group restrictedGroup = th.groupErfahrungMMA;
        th.iterateOverNonMemberGroupMembershipStatus(caller, restrictedGroup,
                membershipStatus -> assertPostsFromRestEndpointContainPosts("/grapevine/activity", caller,
                        expectedPosts));
    }

    @Test
    public void activity_emptyList() throws Exception {

        Person caller = th.personFranziTenant1Dorf2NoMember;
        //test data integrity
        assertFalse(Stream.concat(
                th.groupsToPostsAndComments
                        .values()
                        .stream()
                        .flatMap(gossipsWithComments -> gossipsWithComments.stream()
                                .map(Pair::getLeft)),
                Stream.of(th.gossipDorf1WithComments.getLeft(), th.gossipMainzWithComments.getLeft()))
                .anyMatch(gossip -> gossip.getCreator().equals(caller)));

        assertFalse(Stream.concat(
                th.groupsToPostsAndComments
                        .values()
                        .stream()
                        .flatMap(gossipsWithComments -> gossipsWithComments.stream()
                                .map(Pair::getRight)),
                Stream.of(th.gossipDorf1WithComments.getRight(), th.gossipMainzWithComments.getRight()))
                .flatMap(List::stream)
                .anyMatch(comment -> comment.getCreator().equals(caller)));

        mockMvc.perform(get("/grapevine/activity")
                        .param("page", "0")
                        .param("count", "50")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    public void getPostsOfAllMemberGroups() throws Exception {

        Person caller = th.personChloeTenant1Dorf1;

        Set<Group> memberGroups = th.groupMembershipRepository.findAllByMemberOrderByCreatedDesc(caller).stream()
                .filter(groupMembership -> groupMembership.getStatus() == GroupMembershipStatus.APPROVED)
                .map(GroupMembership::getGroup)
                .collect(Collectors.toSet());

        List<Gossip> postsInMemberGroups = th.groupsToPostsAndComments.entrySet()
                .stream()
                //group not deleted
                .filter(groupListEntry -> !groupListEntry.getKey().isDeleted())
                //only member groups
                .filter(groupListEntry -> memberGroups.contains(groupListEntry.getKey()))
                //get all posts in member groups
                .flatMap(e -> e.getValue().stream()
                        .map(Pair::getLeft))
                //post not deleted
                .filter(Gossip::isNotDeleted)
                //sort descending by created
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .collect(Collectors.toList());
        assertThat(postsInMemberGroups).hasSize(9);
        int pageSize = 3;
        Post firstPost = postsInMemberGroups.get(0);
        Post secondPost = postsInMemberGroups.get(1);
        Post thirdPost = postsInMemberGroups.get(2);

        // sort by created
        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(postsInMemberGroups.size()))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(firstPost.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(firstPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(firstPost.getText()))
                .andExpect(jsonPath("$.content[0].gossip.created").value(firstPost.getCreated()))
                .andExpect(jsonPath("$.content[1].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[1].gossip.id").value(secondPost.getId()))
                .andExpect(jsonPath("$.content[1].gossip.geoAreaIds")
                        .value(secondPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[1].gossip.text").value(secondPost.getText()))
                .andExpect(jsonPath("$.content[1].gossip.created").value(secondPost.getCreated()))
                .andExpect(jsonPath("$.content[2].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[2].gossip.id").value(thirdPost.getId()))
                .andExpect(jsonPath("$.content[2].gossip.geoAreaIds")
                        .value(thirdPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[2].gossip.text").value(thirdPost.getText()))
                .andExpect(jsonPath("$.content[2].gossip.created").value(thirdPost.getCreated()));

        // sort by last activity
        postsInMemberGroups = postsInMemberGroups.stream()
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .collect(Collectors.toList());
        firstPost = postsInMemberGroups.get(0);
        secondPost = postsInMemberGroups.get(1);
        thirdPost = postsInMemberGroups.get(2);

        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("type", "GOSSIP")
                        .param("sortBy", "LAST_ACTIVITY")
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(postsInMemberGroups.size()))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(firstPost.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(firstPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(firstPost.getText()))
                .andExpect(jsonPath("$.content[0].gossip.created").value(firstPost.getCreated()))
                .andExpect(jsonPath("$.content[1].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[1].gossip.id").value(secondPost.getId()))
                .andExpect(jsonPath("$.content[1].gossip.geoAreaIds")
                        .value(secondPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[1].gossip.text").value(secondPost.getText()))
                .andExpect(jsonPath("$.content[1].gossip.created").value(secondPost.getCreated()))
                .andExpect(jsonPath("$.content[2].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[2].gossip.id").value(thirdPost.getId()))
                .andExpect(jsonPath("$.content[2].gossip.geoAreaIds")
                        .value(thirdPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[2].gossip.text").value(thirdPost.getText()))
                .andExpect(jsonPath("$.content[2].gossip.created").value(thirdPost.getCreated()));

        // filter by start and end time
        final long startTime = postsInMemberGroups.get(3).getLastActivity();
        final long endTime = secondPost.getLastActivity();
        postsInMemberGroups = postsInMemberGroups.stream()
                .filter(post -> post.getLastActivity() >= startTime && post.getLastActivity() <= endTime)
                .collect(Collectors.toList());
        firstPost = postsInMemberGroups.get(0);
        secondPost = postsInMemberGroups.get(1);
        thirdPost = postsInMemberGroups.get(2);

        assertThat(postsInMemberGroups).hasSize(3);

        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("type", "GOSSIP")
                        .param("sortBy", "LAST_ACTIVITY")
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime))
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(postsInMemberGroups.size()))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(firstPost.getId()))
                .andExpect(jsonPath("$.content[0].gossip.geoAreaIds")
                        .value(firstPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[0].gossip.text").value(firstPost.getText()))
                .andExpect(jsonPath("$.content[0].gossip.created").value(firstPost.getCreated()))
                .andExpect(jsonPath("$.content[1].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[1].gossip.id").value(secondPost.getId()))
                .andExpect(jsonPath("$.content[1].gossip.geoAreaIds")
                        .value(secondPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[1].gossip.text").value(secondPost.getText()))
                .andExpect(jsonPath("$.content[1].gossip.created").value(secondPost.getCreated()))
                .andExpect(jsonPath("$.content[2].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[2].gossip.id").value(thirdPost.getId()))
                .andExpect(jsonPath("$.content[2].gossip.geoAreaIds")
                        .value(thirdPost.getGeoAreas().stream()
                                .map(GeoArea::getId)
                                .sorted()
                                .collect(Collectors.toList())))
                .andExpect(jsonPath("$.content[2].gossip.text").value(thirdPost.getText()))
                .andExpect(jsonPath("$.content[2].gossip.created").value(thirdPost.getCreated()));
    }

    @Test
    public void getPostsOfAllMemberGroups_emptyPage() throws Exception {
        Person caller = th.personRegularKarl;

        // no group memberships
        Set<Group> memberGroups = th.groupMembershipRepository.findAllByMemberOrderByCreatedDesc(caller).stream()
                .filter(groupMembership -> groupMembership.getStatus() == GroupMembershipStatus.APPROVED)
                .map(GroupMembership::getGroup)
                .collect(Collectors.toSet());

        List<Gossip> postsInMemberGroups = th.groupsToPostsAndComments.entrySet()
                .stream()
                //group not deleted
                .filter(groupListEntry -> !groupListEntry.getKey().isDeleted())
                //only member groups
                .filter(groupListEntry -> memberGroups.contains(groupListEntry.getKey()))
                //get all posts in member groups
                .flatMap(e -> e.getValue().stream()
                        .map(Pair::getLeft))
                //post not deleted
                .filter(Gossip::isNotDeleted)
                //sort descending by created
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .collect(Collectors.toList());
        assertThat(postsInMemberGroups).hasSize(0);

        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", "0")
                        .param("count", "3")
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.empty").value(true))
                .andExpect(jsonPath("$.content").isEmpty());

        // no posts in groups
        final Group groupToJoin = th.groupDorfgeschichteSSA; // group without posts
        th.groupMembershipRepository.save(GroupMembership.builder()
                .group(groupToJoin)
                .member(caller)
                .status(GroupMembershipStatus.APPROVED)
                .build());
        assertThat(th.groupMembershipRepository.findByGroupAndMember(groupToJoin, caller)).isPresent();

        postsInMemberGroups = th.groupsToPostsAndComments.entrySet()
                .stream()
                //group not deleted
                .filter(groupListEntry -> !groupListEntry.getKey().isDeleted())
                //only member groups
                .filter(groupListEntry -> groupListEntry.getKey().equals(groupToJoin))
                //get all posts in member groups
                .flatMap(e -> e.getValue().stream()
                        .map(Pair::getLeft))
                //post not deleted
                .filter(Gossip::isNotDeleted)
                //sort descending by created
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .collect(Collectors.toList());
        assertThat(postsInMemberGroups).hasSize(0);

        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", "0")
                        .param("count", "3")
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(0))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.empty").value(true))
                .andExpect(jsonPath("$.content").isEmpty());
    }

    @Test
    public void getPostsOfAllMemberGroups_PageParameterInvalid() throws Exception {

        final Person caller = th.personRegularKarl;

        // Maximum number of posts smaller than 1
        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(0))
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Page number smaller than 0
        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(10))
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Maximum number of posts smaller than 1 and page number smaller than 0
        mockMvc.perform(get("/grapevine/group/post")
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(0))
                        .param("type", "GOSSIP")
                        .param("sortBy", "CREATED")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getPostsOfAllMemberGroups_InvalidSortingCriterion() throws Exception {

        final Person caller = th.personChloeTenant1Dorf1;

        mockMvc.perform(get("/grapevine/group/post")
                        .param("postTypes", "GOSSIP")
                        .param("sortBy", "INVALID")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/grapevine/group/post")
                        .param("postTypes", "GOSSIP")
                        .param("sortBy", "LAST_MODIFIED")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        mockMvc.perform(get("/grapevine/group/post")
                        .param("postTypes", "GOSSIP")
                        .param("sortBy", "LAST_SUGGESTION_ACTIVITY")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        mockMvc.perform(get("/grapevine/group/post")
                        .param("postTypes", "GOSSIP")
                        .param("sortBy", "HAPPENING_START")
                        .headers(authHeadersFor(caller)))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void getPostsOfAllMemberGroups_NotAuthorized() throws Exception {

        final Person caller = th.personChloeTenant1Dorf1;
        assertOAuth2(get("/grapevine/group/post")
                .param("page", "0")
                .param("count", "3")
                .param("type", "GOSSIP")
                .param("sortBy", "LAST_ACTIVITY")
                .headers(authHeadersFor(caller)));
    }

    private void assertPostsFromRestEndpointContainPosts(String endpoint, Person caller, List<ClientPost> expectedPosts)
            throws Exception {
        mockMvc.perform(get(endpoint)
                        .param("page", "0")
                        .param("count", "100")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(ClientPostPageBean.from(expectedPosts, 100), "pageable", "sort"));
    }

}
