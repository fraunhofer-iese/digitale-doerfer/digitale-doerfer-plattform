/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.offer.ClientOfferCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.specialpost.ClientSpecialPostCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.business.grapevine.worker.GrapevineSearchReindexWorker;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
@ExtendWith(AssumeIntegrationTestExtension.class)
public class PostEventControllerSearchTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;
    @Autowired
    private GrapevineSearchReindexWorker grapevineSearchReindexWorker;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createTradingCategories();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    @Test
    public void gossipCreateRequest_Search() throws Exception {

        Person postCreator = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        assertThat(postCreator.getHomeArea()).isNotNull();
        String text = "Kuckt mal, habe ein neues Pony 🐎";

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                .headers(authHeadersFor(postCreator, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipCreateRequest.builder()
                        .text(text)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        waitForEventProcessingLong();

        final String postId = gossipCreateConfirmation.getPostId();
        final SearchPost indexedPost =
                elasticsearchTestUtil.elasticsearchOperations.get(postId,
                        SearchPost.class);
        assertThat(indexedPost).isNotNull();
        assertThat(indexedPost.getText()).isEqualTo(text);

        mockMvc.perform(get("/grapevine/search")
                .param("search", "pony")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(postId))
                .andExpect(jsonPath("$.content[0].gossip.text").value(text));
    }

    @Test
    public void gossipCreateRequest_SearchOffline() throws Exception {

        elasticsearchTestUtil.disableElasticsearch();

        Person postCreator = th.personAnnikaSchneider;
        AppVariant appVariant = th.appVariant4AllTenants;
        assertThat(postCreator.getHomeArea()).isNotNull();
        String text = "Kann mir jemand beim Pilze sammeln helfen?";

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                .headers(authHeadersFor(postCreator, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipCreateRequest.builder()
                        .text(text)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        waitForEventProcessingLong();

        mockMvc.perform(get("/grapevine/search")
                .param("search", "Pilz")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(isException(ClientExceptionType.SEARCH_NOT_AVAILABLE));

        //search engine was offline, so the post should be marked that indexing failed
        final String postId = gossipCreateConfirmation.getPostId();
        assertThat(th.postRepository.findById(postId).get().getSearchIndexFailed()).isTrue();

        //search engine comes back on
        elasticsearchTestUtil.enableElasticsearch();

        //check that the post is not indexed
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postId, SearchPost.class)).isNull();

        //reindex worker now should push the post into the search engine
        grapevineSearchReindexWorker.run();

        waitForEventProcessingLong();

        //now the post should be in the search engine and marked that the indexing did not fail anymore
        assertThat(elasticsearchTestUtil.elasticsearchOperations.get(postId, SearchPost.class)).isNotNull();
        assertThat(th.postRepository.findById(postId).get().getSearchIndexFailed()).isFalse();

        mockMvc.perform(get("/grapevine/search")
                .param("search", "Pilz")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.GOSSIP.toString()))
                .andExpect(jsonPath("$.content[0].gossip.id").value(postId))
                .andExpect(jsonPath("$.content[0].gossip.text").value(text));
    }

    @Test
    public void specialPostCreateRequest_Search() throws Exception {

        Person postCreator = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        assertThat(postCreator.getHomeArea()).isNotNull();
        String text = "Suche Hilfe für mein neues Pony 🐎";

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                .headers(authHeadersFor(postCreator, appVariant))
                .contentType(contentType)
                .content(json(ClientSpecialPostCreateRequest.builder()
                        .text(text)
                        .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        waitForEventProcessingLong();

        final String postId = specialPostCreateConfirmation.getPostId();
        final SearchPost indexedPost =
                elasticsearchTestUtil.elasticsearchOperations.get(postId, SearchPost.class);
        assertThat(indexedPost).isNotNull();
        assertThat(indexedPost.getText()).isEqualTo(text);

        mockMvc.perform(get("/grapevine/search")
                .param("search", "pony")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.SPECIAL_POST.toString()))
                .andExpect(jsonPath("$.content[0].specialPost.id").value(postId))
                .andExpect(jsonPath("$.content[0].specialPost.text").value(text));
    }

    @Test
    public void offerCreateRequest_Search() throws Exception {

        Person postCreator = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        assertThat(postCreator.getHomeArea()).isNotNull();
        String text = "Biete Lasagne an!";

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                .headers(authHeadersFor(postCreator, appVariant))
                .contentType(contentType)
                .content(json(ClientOfferCreateRequest.builder()
                        .text(text)
                        .tradingCategoryId(th.tradingCategoryService.getId())
                        .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation offerCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        waitForEventProcessingLong();

        final String postId = offerCreateConfirmation.getPostId();
        final SearchPost indexedPost =
                elasticsearchTestUtil.elasticsearchOperations.get(postId,
                        SearchPost.class);
        assertThat(indexedPost).isNotNull();
        assertThat(indexedPost.getText()).isEqualTo(text);

        mockMvc.perform(get("/grapevine/search")
                .param("search", "Lasagne")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.OFFER.toString()))
                .andExpect(jsonPath("$.content[0].offer.id").value(postId))
                .andExpect(jsonPath("$.content[0].offer.text").value(text));
    }

}
