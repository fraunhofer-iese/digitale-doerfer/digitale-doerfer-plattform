/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;

public class DimensionTest {

    @Test
    public void dimensionCategoryTest(){
        double x = 0.01d;
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(20d-x, 10d,   5d,   0.7d).getCategory());
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(20d,   10d-x, 5d,   0.7d).getCategory());
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(20d,   10d,   5d-x, 0.7d).getCategory());

        assertEquals(DimensionCategory.S, new Dimension(20d,   10d,    5d,   0.7d).getCategory());
        assertEquals(DimensionCategory.S, new Dimension(20d+x, 10d,    5d,   0.7d).getCategory());
        assertEquals(DimensionCategory.S, new Dimension(20d,   10d+x,  5d,   0.7d).getCategory());
        assertEquals(DimensionCategory.S, new Dimension(20d,   10d,    5d+x, 0.7d).getCategory());
        assertEquals(DimensionCategory.S, new Dimension(30d,   20d,   10d,   0.7d).getCategory());

        assertEquals(DimensionCategory.M, new Dimension(30d+x, 20d,   10d,   0.7d).getCategory());
        assertEquals(DimensionCategory.M, new Dimension(30d,   20d+x, 10d,   0.7d).getCategory());
        assertEquals(DimensionCategory.M, new Dimension(30d,   20d,   10d+x, 0.7d).getCategory());
        assertEquals(DimensionCategory.M, new Dimension(40d,   30d,   20d,   0.7d).getCategory());

        assertEquals(DimensionCategory.L, new Dimension(40d+x, 30d,   20d,   0.7d).getCategory());
        assertEquals(DimensionCategory.L, new Dimension(40d,   30d+x, 20d,   0.7d).getCategory());
        assertEquals(DimensionCategory.L, new Dimension(40d,   30d,   20d+x, 0.7d).getCategory());
        assertEquals(DimensionCategory.L, new Dimension(60d,   40d,   30d,   0.7d).getCategory());

        assertEquals(DimensionCategory.OVERSIZED, new Dimension(60d+x, 40d,   30d,   0.7d).getCategory());
        assertEquals(DimensionCategory.OVERSIZED, new Dimension(60d,   40d+x, 30d,   0.7d).getCategory());
        assertEquals(DimensionCategory.OVERSIZED, new Dimension(60d,   40d,   30d+x, 0.7d).getCategory());
    }

    @Test
    public void dimensionCategoryBuilderTest(){
        double x = 0.01d;
        assertEquals(DimensionCategory.UNDERSIZED, Dimension.builder()
                .length(20d-x)
                .width(10d)
                .height(5d)
                .weight(0.7d)
                .build().getCategory());

        assertEquals(DimensionCategory.M, Dimension.builder()
                .length(40d)
                .width(30d)
                .height(20d)
                .weight(0.7d)
                .build().getCategory());
    }

    @Test
    public void dimensionCategoryDimensionsWrongOrderTest(){
        double x = 0.01d;
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(10d,   20d,   5d-x, 0.7d).getCategory());
        assertEquals(DimensionCategory.S, new Dimension(10d,   20d,   30d,   0.7d).getCategory());
        assertEquals(DimensionCategory.M, new Dimension(20d,   30d,   40d,   0.7d).getCategory());
        assertEquals(DimensionCategory.L, new Dimension(30d,   40d,   60d,   0.7d).getCategory());
    }

    @Test
    public void dimensionCategoryInvalidTest(){
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(0d, 0d,   0d,   0.7d).getCategory());
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(-1d, 0d,  0d,   0.7d).getCategory());
        assertEquals(DimensionCategory.UNDERSIZED, new Dimension(-1d, -1d,  -1d,   0.7d).getCategory());

        //not really useful behavior, just do not crash
        assertNotNull(new Dimension(Double.NaN, 1d,  2d,   0.7d).getCategory());
        assertNotNull(new Dimension(Double.NaN, Double.NaN,  Double.NaN,   0.7d).getCategory());
        assertNotNull(new Dimension(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,  Double.NEGATIVE_INFINITY,   0.7d).getCategory());
        assertNotNull(new Dimension(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY,  Double.NEGATIVE_INFINITY,   0.7d).getCategory());
    }

    @Test
    public void toNormalizedDimensionTest(){
        Dimension actual =  new Dimension(20d,   40d,   60d,   0.66d).toNormalizedDimension();
        assertArrayEquals(
                new double[]{60d, 40d, 20d} ,
                new double[]{actual.getLength(), actual.getWidth(), actual.getHeight()},
                0d);
        assertEquals(0.66d, actual.getWeight(), 0d);
        assertEquals(DimensionCategory.L, actual.getCategory());

        actual =  new Dimension(20d,   10d,   40d,   10d).toNormalizedDimension();
        assertArrayEquals(
                new double[]{40d, 20d, 10d} ,
                new double[]{actual.getLength(), actual.getWidth(), actual.getHeight()},
                0d);
        assertEquals(10d, actual.getWeight(), 0d);
        assertEquals(DimensionCategory.M, actual.getCategory());

    }

}
