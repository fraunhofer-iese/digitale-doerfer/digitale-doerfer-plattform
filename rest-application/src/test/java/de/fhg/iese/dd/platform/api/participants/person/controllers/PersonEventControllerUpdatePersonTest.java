/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeHomeAreaResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonExtendedUpdateRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonUpdateRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.ParticipantsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;

public class PersonEventControllerUpdatePersonTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAddresses();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void personUpdateRequest() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String nickName = "NewNickName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final String phoneNumber = "NewPhoneNumber";
        final GeoArea homeArea = person.getHomeArea();

        ClientPersonUpdateRequest personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();

        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also updated in the repository
        Person updatedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, updatedPerson.getNickName());
        assertEquals(firstName, updatedPerson.getFirstName());
        assertEquals(lastName, updatedPerson.getLastName());
        assertEquals(phoneNumber, updatedPerson.getPhoneNumber());
        assertEquals(email, updatedPerson.getEmail());
        assertEquals(homeArea, updatedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), updatedPerson.getTenant());

        //check the address that is updated
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(updatedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());
    }

    @Test
    public void personUpdateRequest_WithoutPhone() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String nickName = "NewNickName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final GeoArea homeArea = person.getHomeArea();

        ClientPersonUpdateRequest personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also updated in the repository
        Person updatedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, updatedPerson.getNickName());
        assertEquals(firstName, updatedPerson.getFirstName());
        assertEquals(lastName, updatedPerson.getLastName());
        assertNull(updatedPerson.getPhoneNumber());
        assertEquals(email, updatedPerson.getEmail());
        assertEquals(homeArea, updatedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), updatedPerson.getTenant());

        //check the address that is not updated
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(updatedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());
    }

    @Test
    public void personUpdateRequest_WithoutNickname() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final String phoneNumber = "NewPhoneNumber";
        final GeoArea homeArea = person.getHomeArea();

        ClientPersonUpdateRequest personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").doesNotExist())
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").doesNotExist())
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also updated in the repository
        Person updatedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertNull(updatedPerson.getNickName());
        assertEquals(firstName, updatedPerson.getFirstName());
        assertEquals(lastName, updatedPerson.getLastName());
        assertEquals(phoneNumber, updatedPerson.getPhoneNumber());
        assertEquals(email, updatedPerson.getEmail());
        assertEquals(homeArea, updatedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), updatedPerson.getTenant());

        //check the address that is not updated
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(updatedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());
    }

    @Test
    public void personUpdateRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personUpdateRequest")
                .contentType(contentType)
                .content(json(ClientPersonUpdateRequest.builder()
                        .firstName("NewFirstName")
                        .lastName("NewLastName")
                        .nickName("NewNickName")
                        .phoneNumber("NewPhoneNumber")
                        .build())));
    }

    @Test
    public void personUpdateRequest_InvalidParameters() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String phoneNumber = "+4949188174949";

        // firstName empty
        ClientPersonUpdateRequest personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName("")
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // firstName null
        personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(null)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // lastName empty
        personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(firstName)
                .lastName("")
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // lastName null
        personUpdateRequest = ClientPersonUpdateRequest.builder()
                .firstName(firstName)
                .lastName(null)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personUpdateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));
    }

    @Test
    public void personExtendedUpdateRequest() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String nickName = "NewNickName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final String phoneNumber = "NewPhoneNumber";
        final ClientMediaItemPlaceHolder clientMediaItemPlaceHolder = ClientMediaItemPlaceHolder.builder()
                .mediaItemId(person.getProfilePicture().getId())
                .build();
        final GeoArea homeArea = th.geoAreaTenant2;

        assertThat(person.getHomeArea()).isNotNull().isNotEqualTo(homeArea);

        ClientPersonExtendedUpdateRequest personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .profilePicture(clientMediaItemPlaceHolder)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(jsonPath("$.updatedPerson.profilePicture.id").value(
                        clientMediaItemPlaceHolder.getMediaItemId()))
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.profilePicture.id").value(clientMediaItemPlaceHolder.getMediaItemId()))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();

        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also updated in the repository
        Person updatedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(person, updatedPerson);
        assertEquals(nickName, updatedPerson.getNickName());
        assertEquals(firstName, updatedPerson.getFirstName());
        assertEquals(lastName, updatedPerson.getLastName());
        assertEquals(phoneNumber, updatedPerson.getPhoneNumber());
        assertEquals(email, updatedPerson.getEmail());
        assertEquals(homeArea, updatedPerson.getHomeArea());
        assertEquals(clientMediaItemPlaceHolder.getMediaItemId(), updatedPerson.getProfilePicture().getId());
        assertEquals(homeArea.getTenant(), updatedPerson.getTenant());

        //check the address that is updated
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(updatedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonSilent was triggered
        ClientPersonChangeHomeAreaResponse pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(updatedPerson,
                        ClientPersonChangeHomeAreaResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_OWN_PERSON_CHANGES));
        assertEquals(homeArea.getId(), pushedEvent.getHomeAreaId());
        assertEquals(person.getId(), pushedEvent.getPersonId());
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personExtendedUpdateRequest_NoHomeAreaChange() throws Exception {

        Person person = th.personVgAdmin;
        GeoArea homeArea = person.getHomeArea();
        ClientPersonExtendedUpdateRequest personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName("NewFirstName")
                .lastName("NewLastName")
                .nickName("NewNickName")
                .phoneNumber("NewPhoneNumber")
                .profilePicture(ClientMediaItemPlaceHolder.builder()
                        .mediaItemId(person.getProfilePicture().getId())
                        .build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();

        // verify push message
        waitForEventProcessing();

        // Assert that no push message was sent
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personExtendedUpdateRequest_WithTempMediaItem() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String nickName = "NewNickName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final String phoneNumber = "NewPhoneNumber";
        final TemporaryMediaItem temporaryMediaItem = th.createTemporaryMediaItem(person, 1);
        final ClientMediaItemPlaceHolder clientMediaItemPlaceHolder = ClientMediaItemPlaceHolder.builder()
                .temporaryMediaItemId(temporaryMediaItem.getId())
                .build();
        final GeoArea homeArea = th.geoAreaTenant2;
        final MediaItem oldProfilePicture = person.getProfilePicture();

        ClientPersonExtendedUpdateRequest personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .profilePicture(clientMediaItemPlaceHolder)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(
                        jsonPath("$.updatedPerson.profilePicture.id").value(temporaryMediaItem.getMediaItem().getId()))
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.profilePicture.id").value(temporaryMediaItem.getMediaItem().getId()))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();

        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also updated in the repository
        Person updatedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, updatedPerson.getNickName());
        assertEquals(firstName, updatedPerson.getFirstName());
        assertEquals(lastName, updatedPerson.getLastName());
        assertEquals(phoneNumber, updatedPerson.getPhoneNumber());
        assertEquals(email, updatedPerson.getEmail());
        assertEquals(homeArea, updatedPerson.getHomeArea());
        assertEquals(temporaryMediaItem.getMediaItem(), updatedPerson.getProfilePicture());
        assertEquals(homeArea.getTenant(), updatedPerson.getTenant());

        //check if the old image is deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContain(oldProfilePicture);

        //check the address that is updated
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(updatedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());
    }

    @Test
    public void personExtendedUpdateRequest_WithoutMediaItem() throws Exception {

        Person person = th.personVgAdmin;
        final String firstName = "NewFirstName";
        final String lastName = "NewLastName";
        final String nickName = "NewNickName";
        final String name = th.addressPersonVgAdmin.getName();
        final String city = th.addressPersonVgAdmin.getCity();
        final String street = th.addressPersonVgAdmin.getStreet();
        final String zip = th.addressPersonVgAdmin.getZip();
        final String email = person.getEmail();
        final String phoneNumber = "NewPhoneNumber";
        final GeoArea homeArea = th.geoAreaTenant2;
        final MediaItem oldProfilePicture = person.getProfilePicture();

        ClientPersonExtendedUpdateRequest personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .profilePicture(null)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updatedPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.updatedPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.updatedPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.updatedPerson.email").value(email))
                .andExpect(jsonPath("$.updatedPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.updatedPerson.address.name").value(name))
                .andExpect(jsonPath("$.updatedPerson.address.street").value(street))
                .andExpect(jsonPath("$.updatedPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.updatedPerson.address.city").value(city))
                .andExpect(jsonPath("$.updatedPerson.profilePicture").doesNotExist())
                .andExpect(jsonPath("$.updatedPerson.homeAreaId").value(homeArea.getId()));

        //check if the old image is deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContain(oldProfilePicture);

    }

    @Test
    public void personExtendedUpdateRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personExtendedUpdateRequest")
                .contentType(contentType)
                .content(json(ClientPersonExtendedUpdateRequest.builder()
                        .firstName("NewFirstName")
                        .lastName("NewLastName")
                        .nickName("NewNickName")
                        .phoneNumber("NewPhoneNumber")
                        .homeAreaId(th.geoAreaTenant2.getId())
                        .build())));
    }

    @Test
    public void personExtendedUpdateRequest_InvalidParameters() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String phoneNumber = "+4949188174949";
        Person person = th.personVgAdmin;

        // firstName empty
        ClientPersonExtendedUpdateRequest personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName("")
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(person.getHomeArea().getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // firstName null
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(null)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(person.getHomeArea().getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // lastName empty
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName("")
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(person.getHomeArea().getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        // lastName null
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(null)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(person.getHomeArea().getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        //home area null
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //home area invalid
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(th.geoAreaDeutschland.getId())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_IS_NO_LEAF));

        // not a media item of the person
        MediaItem notMyPicture = th.createMediaItem("not my picture");
        personExtendedUpdateRequest = ClientPersonExtendedUpdateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .homeAreaId(person.getHomeArea().getId())
                .profilePicture(ClientMediaItemPlaceHolder.builder().mediaItemId(notMyPicture.getId()).build())
                .build();

        mockMvc.perform(post("/person/event/personExtendedUpdateRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(personExtendedUpdateRequest)))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_NOT_FOUND));
    }

}
