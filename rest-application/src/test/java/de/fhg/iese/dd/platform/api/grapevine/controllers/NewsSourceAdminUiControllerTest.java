/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Dominik Schnier, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NewsSourceAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createOrganizations();
        th.createNewsSources();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getNewsSources() throws Exception {

        NewsSource expectedNewsSource0 = th.newsSourceDigitalbach;
        NewsSource expectedNewsSource1 = th.newsSourceAnalogheim;
        NewsSource expectedNewsSource2 = th.newsSourceKatzenbach;

        mockMvc.perform(get("/adminui/newssource")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(3))
                .andExpect(jsonPath("$.content[0].id").value(expectedNewsSource0.getId()))
                .andExpect(jsonPath("$.content[0].appVariantId").value(expectedNewsSource0.getAppVariant().getId()))
                .andExpect(jsonPath("$.content[0].siteUrl").value(expectedNewsSource0.getSiteUrl()))
                .andExpect(jsonPath("$.content[0].siteName").value(expectedNewsSource0.getSiteName()))
                .andExpect(jsonPath("$.content[0].logo").exists())
                .andExpect(jsonPath("$.content[0].apiManaged").value(false))

                .andExpect(jsonPath("$.content[1].id").value(expectedNewsSource1.getId()))
                .andExpect(jsonPath("$.content[1].appVariantId").value(expectedNewsSource1.getAppVariant().getId()))
                .andExpect(jsonPath("$.content[1].siteUrl").value(expectedNewsSource1.getSiteUrl()))
                .andExpect(jsonPath("$.content[1].siteName").value(expectedNewsSource1.getSiteName()))
                .andExpect(jsonPath("$.content[1].logo").exists())
                .andExpect(jsonPath("$.content[1].apiManaged").value(false))

                .andExpect(jsonPath("$.content[2].id").value(expectedNewsSource2.getId()))
                .andExpect(jsonPath("$.content[2].appVariantId").value(expectedNewsSource2.getAppVariant().getId()))
                .andExpect(jsonPath("$.content[2].siteUrl").value(expectedNewsSource2.getSiteUrl()))
                .andExpect(jsonPath("$.content[2].siteName").value(expectedNewsSource2.getSiteName()))
                .andExpect(jsonPath("$.content[2].logo").exists())
                .andExpect(jsonPath("$.content[2].apiManaged").value(false));
    }

    @Test
    public void getNewsSources_PageParameterInvalid() throws Exception {

        // invalid parameter page
        mockMvc.perform(get("/adminui/newssource")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .param("page", String.valueOf(-1))
                .param("count", String.valueOf(1)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // invalid parameter count
        mockMvc.perform(get("/adminui/newssource")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .param("page", String.valueOf(0))
                .param("count", String.valueOf(0)))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getNewsSources_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/newssource")
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/newssource"));
    }

    @Test
    public void getNewsSourceById() throws Exception {

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}", th.newsSourceAnalogheim.getId())
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.newsSourceAnalogheim.getId()))
                .andExpect(jsonPath("$.appVariantId").value(th.newsSourceAnalogheim.getAppVariant().getId()))
                .andExpect(jsonPath("$.siteUrl").value(th.newsSourceAnalogheim.getSiteUrl()))
                .andExpect(jsonPath("$.siteName").value(th.newsSourceAnalogheim.getSiteName()))
                .andExpect(jsonPath("$.logo").exists())
                .andExpect(jsonPath("$.apiManaged").value(false));
    }

    @Test
    public void getNewsSourceById_NewsSourceNotFound() throws Exception {

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}", BaseTestHelper.INVALID_UUID)
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));
    }

    @Test
    public void getNewsSourceById_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/newssource/{newsSourceId}", th.newsSourceAnalogheim.getId())
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/newssource/{newsSourceId}", th.newsSourceAnalogheim.getId()));
    }

}
