/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentFlagRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.config.UserGeneratedContentFlagConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CommentEventControllerFlagTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private UserGeneratedContentFlagRepository flagRepository;

    @Autowired
    private UserGeneratedContentFlagConfig userGeneratedContentFlagConfig;

    private Comment flaggedEntity;
    private Person flagCreator;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();

        flaggedEntity = th.gossip2Comment1;
        flagCreator = th.personLaraSchaefer;
        assertThat(flaggedEntity).isNotNull();
        assertThat(flagCreator).isNotNull();
        assertThat(flagCreator).isNotEqualTo(flaggedEntity.getCreator());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void flagComment() throws Exception {

        final ClientCommentFlagRequest flagRequest = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/comment/event/commentFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(createRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedEntity.getPost().getTenant().getId());
        assertTrue(flagRepository.existsById(response.getUserGeneratedContentFlagId()));
        UserGeneratedContentFlag flag = flagRepository.findById(response.getUserGeneratedContentFlagId()).get();
        assertEquals(flaggedEntity.getCreator(), flag.getEntityAuthor());
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        mockMvc.perform(get("/grapevine/comment/" + flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagCommentMultipleTimes() throws Exception {

        final ClientCommentFlagRequest flagRequest = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/grapevine/comment/event/commentFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS));
    }

    @Test
    public void flagCommentThatHasBeenDeleted() throws Exception {

        flaggedEntity.setDeleted(true);
        th.commentRepository.save(flaggedEntity);

        final ClientCommentFlagRequest flagRequest = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void flagCommentNoComment() throws Exception {

        final ClientCommentFlagRequest flagRequest = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/grapevine/comment/" + flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagCommentUnauthorized() throws Exception {

        final ClientCommentFlagRequest flagRequest = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey! Meine Mutter is nicht fett!")
                .build();

        assertOAuth2(post("/grapevine/comment/event/commentFlagRequest")
                .contentType(contentType)
                .content(json(flagRequest)));
    }

    @Test
    public void flagCommentWrongOrEmptyEventAttributes() throws Exception {

        final ClientCommentFlagRequest request = ClientCommentFlagRequest.builder()
                .commentId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();
        assertWrongOrEmptyEventAttributesAreInvalid(post("/grapevine/comment/event/commentFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType), request);

    }
    
}
