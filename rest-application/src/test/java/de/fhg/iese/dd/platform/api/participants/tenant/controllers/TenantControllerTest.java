/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.tenant.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TenantControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private AppVariantRepository appVariantRepository;

    private AppVariant appVariantTenant1;
    private AppVariant appVariantTenant2;
    private AppVariant appVariantTenant1And2And3;
    private AppVariant appVariantTenant3;
    private AppVariant appVariantNoTenant;

    @Override
    public void createEntities() {

        // ensure that the "super admin tenant" is deleted
        th.deleteAllData();

        th.createTenantsAndGeoAreas();
        th.createPersons();
        createAppVariants();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    private void createAppVariants() {

        App appDorffunk = appRepository.save(App.builder()
                .name("DorfFunk")
                .appIdentifier("dd-dorffunk")
                .build());

        appVariantTenant1 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .appVariantIdentifier("de.fhg.iese.dd.dorffunk")
                .build());
        th.mapGeoAreaToAppVariant(appVariantTenant1, th.geoAreaTenant1, th.tenant1);

        appVariantTenant2 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .appVariantIdentifier("de.fhg.iese.dd.dorffunk2")
                .build());
        th.mapGeoAreaToAppVariant(appVariantTenant2, th.geoAreaTenant2, th.tenant2);

        appVariantTenant3 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .appVariantIdentifier("de.fhg.iese.dd.dorffunk3")
                .build());
        th.mapGeoAreaToAppVariant(appVariantTenant3, th.geoAreaTenant3, th.tenant3);

        appVariantTenant1And2And3 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .appVariantIdentifier("de.fhg.iese.dd.dorffunk4")
                .build());
        th.mapGeoAreaToAppVariant(appVariantTenant1And2And3, th.geoAreaTenant1, th.tenant1);
        th.mapGeoAreaToAppVariant(appVariantTenant1And2And3, th.geoAreaTenant2, th.tenant2);
        th.mapGeoAreaToAppVariant(appVariantTenant1And2And3, th.geoAreaTenant3, th.tenant3);

        appVariantNoTenant = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .appVariantIdentifier("de.fhg.iese.dd.dorffunk5")
                .build());

        appVariantRepository.flush();
    }

    @Test
    public void getOwnTenant() throws Exception {

        Person personWithTenant = th.personVgAdmin;
        Tenant expectedTenant = personWithTenant.getTenant();

        assertNotNull(expectedTenant);

        mockMvc.perform(get("/community/own")
                .headers(authHeadersFor(personWithTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(expectedTenant.getId()))
                .andExpect(jsonPath("$.name").value(expectedTenant.getName()));
    }

    @Test
    public void getOwnTenantsUnauthorized() throws Exception {

        assertOAuth2(get("/community/own"));
    }

    @Test
    public void getAllTenants() throws Exception {

        List<Tenant> expectedTenants = th.tenantList.stream()
                .sorted(Comparator.comparing(Tenant::getName))
                .collect(Collectors.toList());

        assertEquals(3, expectedTenants.size());

        mockMvc.perform(get("/community"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTenants.size())))
                .andExpect(jsonPath("$[0].id").value(expectedTenants.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedTenants.get(1).getId()))
                .andExpect(jsonPath("$[2].id").value(expectedTenants.get(2).getId()));
    }

    @Test
    public void getTenant() throws Exception {

        mockMvc.perform(get("/community/" + th.tenant2.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.tenant2.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant2.getName()));
    }

    @Test
    public void getNotExistingTenant() throws Exception {

        mockMvc.perform(get("/community/4711"))
                .andExpect(isException("4711", ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getContactPersonsForTenant() throws Exception {

        Person authenticatedPerson = th.personVgAdmin;
        Tenant tenant = th.tenant1;
        List<Person> expectedContactPersons = new ArrayList<>();
        expectedContactPersons.add(th.contactPersonTenant1);

        assertEquals(1, expectedContactPersons.size());

        mockMvc.perform(get("/community/" + tenant.getId() + "/contacts/")
                .headers(authHeadersFor(authenticatedPerson))
                .contentType(contentType))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedContactPersons.size())))
                .andExpect(jsonPath("$[0].firstName").value(expectedContactPersons.get(0).getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(expectedContactPersons.get(0).getLastName()))
                .andExpect(jsonPath("$[0].email").value(expectedContactPersons.get(0).getEmail()));
    }

    @Test
    public void getContactPersonEmailForTenant() throws Exception {

        Person authenticatedPerson = th.personVgAdmin;
        Tenant tenant = th.tenant1;
        List<Person> listContactPersons = new ArrayList<>();
        listContactPersons.add(th.contactPersonTenant1); // was saved first, this one should be returned
        listContactPersons.add(th.contactPersonTenant2);

        final MediaType contentTypeTextPlainUTF8 = new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);
        mockMvc.perform(get("/community/" + tenant.getId() + "/contactEmail/")
                .headers(authHeadersFor(authenticatedPerson))
                .contentType(contentType)).andExpect(status().isOk())
                .andExpect(content().contentType(contentTypeTextPlainUTF8))
                .andExpect(content().string(listContactPersons.get(0).getEmail()));
    }

    @Test
    public void getNearestTenantWithinAppVariant() throws Exception {

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant1.getLatitude(),
                        th.pointClosestToTenant1.getLongitude())
                        .headers(headersForAppVariant(appVariantTenant1))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tenant1.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant1.getName()));

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant1.getLatitude(),
                        th.pointClosestToTenant1.getLongitude())
                        .headers(headersForAppVariant(appVariantTenant2))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tenant2.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant2.getName()));

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant1.getLatitude(),
                        th.pointClosestToTenant1.getLongitude())
                        .headers(headersForAppVariant(appVariantTenant1And2And3))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tenant1.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant1.getName()));

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant2.getLatitude(),
                        th.pointClosestToTenant2.getLongitude())
                        .headers(headersForAppVariant(appVariantTenant1And2And3))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tenant2.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant2.getName()));

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant3.getLatitude(),
                        th.pointClosestToTenant3.getLongitude())
                        .headers(headersForAppVariant(appVariantTenant1And2And3))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(th.tenant3.getId()))
                .andExpect(jsonPath("$.name").value(th.tenant3.getName()));
    }

    @Test
    public void getNearestTenantAllTenantsAreTooFarAway() throws Exception {

        mockMvc.perform(get("/community/nearestCommunity/{latitude}/{longitude}", 10.10, 11.11)
                        .headers(headersForAppVariant(appVariantTenant3))
                        .contentType(contentType))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getNearestTenantNotExistingTenant() throws Exception {

        mockMvc.perform(
                get("/community/nearestCommunity/{latitude}/{longitude}", th.pointClosestToTenant1.getLatitude(),
                        th.pointClosestToTenant1.getLongitude())
                        .headers(headersForAppVariant(appVariantNoTenant))
                        .contentType(contentType))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

}
