/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled(
        "Only for manual testing since it requires internet connection and the provided urls might become unavailable")
@ActiveProfiles({"test", "MediaItemServiceTest"})
public class MediaItemServiceDownloadTest extends BaseServiceTest {

    @Autowired
    private MediaItemService mediaItemService;

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
    }

    @Test
    public void downloadUriFromWordpress() {
        assertImageCanBeDownloaded(
                "https://dorfnews.digitale-doerfer.de/wp-content/uploads/2017/07/pen-2181101_1920-800x400.jpg");
    }

    @Test
    public void downloadUriFromStageWithAuthentication() {
        assertImageCanBeDownloaded(
                "https://iese:digdorfdev@stage-dorfnews.digitale-doerfer.de/wp-content/uploads/2019/06/26241_bild_gross1_digitaleDoerferKopie440.jpg"
        );
    }

    @Test
    public void downloadUriFromS3() {
        assertImageCanBeDownloaded(
                "https://s3.eu-central-1.amazonaws.com/dev-shop-digitale-doerfer-de-uploads/uploads/2019/03/bewerbung_available-1-100x100.jpg");
    }

    @Test
    public void downloadUriFromS3WithUmlaut() {
        assertImageCanBeDownloaded(
                "https://s3.eu-central-1.amazonaws.com/dev-shop-digitale-doerfer-de-uploads/test/glöbal.png");
    }

    @Test
    public void downloadUriFromPlaceholderWithUmlaut() {
        assertImageCanBeDownloaded("https://via.placeholder.com/300/0000FF/808080?text=HälloWörld");
    }
    @Test
    public void downloadUriFromLoremFlickrWithUmlaut() {
        assertImageCanBeDownloaded("https://loremflickr.com/g/320/240/straße");
    }

    private void assertImageCanBeDownloaded(String url) {

        final byte[] bytes;
        try {
            bytes = mediaItemService.downloadImage(new URL(url));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        assertNotNull(bytes);
        assertTrue(bytes.length > 0);
    }

}
