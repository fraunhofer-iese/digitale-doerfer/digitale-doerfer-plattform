/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Stefan Schweitzer, Dominik Schnier, Benjamin Hassenfratz, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.motivation.MotivationTestHelper;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeCommunityRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeHomeAreaRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeHomeAreaResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangePasswordRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonCreateResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonCreateWithRegistrationTokenRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonProfilePictureChangeConfirmation;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonProfilePictureChangeRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonRegistrationTokenRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonRegistrationTokenResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonVerifyEmailAddressRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonVerifyEmailAddressResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusRequest;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.test.mocks.TestOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.ParticipantsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

public class PersonEventControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private MotivationTestHelper motivationTestHelper;

    @Autowired
    private IAuthorizationService authorizationService;

    @Autowired
    private TestEmailSenderService emailSenderService;

    @Autowired
    private TestTimeService timeService;

    @Autowired
    private ParticipantsConfig participantsConfig;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private IPersonService personService;

    @Autowired
    private TestOauthManagementService oauthManagementService;

    private Person personUserAdminTenant1;
    private Person personUserAdminTenant2;
    private Person personGlobalUserAdmin;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAddresses();
        th.createAppEntities();
        th.createPushEntities();
        //required since the community accounts are used when creating persons
        motivationTestHelper.createTenantAccount(th.tenant1);
        motivationTestHelper.createTenantAccount(th.tenant2);
        motivationTestHelper.createTenantAccount(th.tenant3);

        personGlobalUserAdmin =
                th.createPersonWithDefaultAddress("GlobalUserAdmin", th.tenant1,
                        "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalUserAdmin, GlobalUserAdmin.class, null);

        personUserAdminTenant1 =
                th.createPersonWithDefaultAddress("UserAdminTenant1", th.tenant1,
                        "6b0239cf-f13d-4af0-b401-e984686194f2");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());

        personUserAdminTenant2 =
                th.createPersonWithDefaultAddress("UserAdminTenant2", th.tenant2,
                        "8426493e-7498-408b-8d96-32684c82a7a3");
        th.assignRoleToPerson(personUserAdminTenant2, UserAdmin.class, th.tenant2.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void personCreateRequest() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|hansi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city)
                        .build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.createdPerson.address.name").value(name))
                .andExpect(jsonPath("$.createdPerson.address.street").value(street))
                .andExpect(jsonPath("$.createdPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.createdPerson.address.city").value(city))
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()))
                .andExpect(jsonPath("$.createdPerson.verificationStatuses").isEmpty());

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andExpect(jsonPath("$.verificationStatuses").isEmpty())
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(phoneNumber, addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());
        assertTrue(addedPerson.getVerificationStatuses().getValues().isEmpty(), "No verification status should be set");

        //check the address that is automatically added
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(addedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(name, addressListEntry.getAddress().getName());
        assertEquals(street, addressListEntry.getAddress().getStreet());
        assertEquals(zip, addressListEntry.getAddress().getZip());
        assertEquals(city, addressListEntry.getAddress().getCity());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_EmailNormalized() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String phoneNumber = "+4949188174949";
        final String newEmailNormalized = "corona@virus.international";
        final String newEmailRequested = "Corona@virus.international ";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|hansi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(newEmailRequested)
                .homeAreaId(homeArea.getId())
                .build();

        ClientPersonCreateResponse createResponse = toObject(
                mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                        .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                        .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                        .andExpect(jsonPath("$.createdPerson.email").value(newEmailNormalized))
                        .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                        .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()))
                        .andExpect(jsonPath("$.createdPerson.verificationStatuses").isEmpty())
                        .andReturn(),
                ClientPersonCreateResponse.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(createResponse.getCreatedPerson().getId()).get();

        assertEquals(newEmailNormalized, addedPerson.getEmail());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_WithoutAddress() throws Exception {

        final String firstName = "Karl";
        final String lastName = "Napp";
        final String nickName = "Nappi";
        final String phoneNumber = "+4949188174949";
        final String email = "nappi@example.org";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|nappi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.createdPerson.address").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()))
                .andExpect(jsonPath("$.createdPerson.verificationStatuses").isEmpty());

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address").doesNotExist())
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andExpect(jsonPath("$.verificationStatuses").isEmpty())
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(phoneNumber, addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        //check that no address is added
        assertThat(th.personRepository.findAllAddressListEntriesByPerson(addedPerson)).isEmpty();

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_WithoutPhone() throws Exception {

        final String firstName = "Karl";
        final String lastName = "Napp";
        final String nickName = "Nappi";
        final String email = "nappi@example.org";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|nappi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.address").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()));

        waitForEventProcessing();

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.address").doesNotExist())
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertNull(addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_WithoutNickName() throws Exception {

        final String firstName = "Karl";
        final String lastName = "Napp";
        final String email = "nappi@example.org";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|nappi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.address").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").doesNotExist())
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.address").doesNotExist())
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertNull(addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertNull(addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_HomeAreaNoLeaf() throws Exception {

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName("Karl")
                .lastName("Napp")
                .nickName("Nappi")
                .phoneNumber("+4949188174949")
                .email("nappi@example.org")
                .homeAreaId(th.geoAreaDeutschland.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("auth0|nappi"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_IS_NO_LEAF));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);
    }

    @Test
    public void personCreateRequest_NoHomeAreaButHomeTenant() throws Exception {

        final String firstName = "Karl";
        final String lastName = "Napp";
        final String email = "nappi@example.org";
        final Tenant homeTenant = th.tenant1;
        final String oauthId = "auth0|nappi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeCommunityId(homeTenant.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.address").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.homeAreaId").doesNotExist());

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").doesNotExist())
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.address").doesNotExist())
                .andExpect(jsonPath("$.homeAreaId").doesNotExist())
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertNull(addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertNull(addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeTenant, addedPerson.getTenant());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_HomeAreaAndHomeTenant() throws Exception {

        final String firstName = "Karl";
        final String lastName = "Napp";
        final String email = "nappi@example.org";
        //home area should win, home tenant is ignored
        final Tenant homeTenant = th.tenant1;
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|nappi";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeCommunityId(homeTenant.getId())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.address").doesNotExist())
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").doesNotExist())
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").doesNotExist())
                .andExpect(jsonPath("$.address").doesNotExist())
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertNull(addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertNull(addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_WithSameEmail() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant2;
        final String firstName = "Karl";
        final String lastName = "Napp";
        final String nickName = "Nappi";
        final String phoneNumber = "+4949188174949";
        final String email = "nappi@example.org";
        final String oauthId = "auth0|nappi";

        final String firstName2 = "Hans";
        final String lastName2 = "Napp";
        final String nickName2 = "HNappi";
        final String phoneNumber2 = "+4949188174948";
        final String oauthId2 = "auth0|nappi2";

        long personCountBefore = th.personRepository.count();

        mockMvc.perform(post("/person/event/personCreateRequest")
                .headers(authHeadersFor(oauthId))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .nickName(nickName)
                        .phoneNumber(phoneNumber)
                        .email(email)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(status().isOk());

        mockMvc.perform(post("/person/event/personCreateRequest")
                .headers(authHeadersFor(oauthId2))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName2)
                        .lastName(lastName2)
                        .nickName(nickName2)
                        .phoneNumber(phoneNumber2)
                        .email(email)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(isException(email, ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        String emailWithBlanks = email + " ";
        mockMvc.perform(post("/person/event/personCreateRequest")
                .headers(authHeadersFor(oauthId2))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName2)
                        .lastName(lastName2)
                        .nickName(nickName2)
                        .phoneNumber(phoneNumber2)
                        .email(emailWithBlanks)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(isException(emailWithBlanks, ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        String upperCaseMail = email.toUpperCase();
        mockMvc.perform(post("/person/event/personCreateRequest")
                .headers(authHeadersFor(oauthId2))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName2)
                        .lastName(lastName2)
                        .nickName(nickName2)
                        .phoneNumber(phoneNumber2)
                        .email(upperCaseMail)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(isException(upperCaseMail, ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_WithSameOauthId() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant2;
        final String firstName = "Karl";
        final String lastName = "Napp";
        final String nickName = "Nappi";
        final String phoneNumber = "+4949188174949";
        final String email = "nappi@example.org";
        final String oauthId = "auth0|nappi";

        final String firstName2 = "Hans";
        final String lastName2 = "Napp";
        final String nickName2 = "HNappi";
        final String phoneNumber2 = "+4949188174948";
        final String email2 = "nappi2@example.org";

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();
        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk());

        ClientPersonCreateRequest personCreateRequest2 = ClientPersonCreateRequest.builder()
                .firstName(firstName2)
                .lastName(lastName2)
                .nickName(nickName2)
                .phoneNumber(phoneNumber2)
                .email(email2)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest2)))
                .andExpect(isException(oauthId, ClientExceptionType.PERSON_ALREADY_EXISTS));

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_SameOauthIdAsDeletedPerson() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant2;
        final String firstName = "Karl";
        final String lastName = "Napp";
        final String nickName = "Nappi";
        final String phoneNumber = "+4949188174949";
        final String email = "nappi@example.org";
        final Person deletedPerson = th.personDeleted;
        final String oauthIdOfExistingDeletedPerson = deletedPerson.getOauthId();

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        final ClientPersonOwn createdClientPerson = toObject(mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(th.personDeleted.getOauthId()))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse(), ClientPersonCreateResponse.class)
                .getCreatedPerson();

        assertTrue(th.personRepository.existsById(createdClientPerson.getId()));
        assertEquals(oauthIdOfExistingDeletedPerson,
                th.personRepository.findById(createdClientPerson.getId()).get().getOauthId());

        assertTrue(th.personRepository.existsById(deletedPerson.getId()));
        assertNotEquals(th.personRepository.findById(deletedPerson.getId()).get().getOauthId(),
                oauthIdOfExistingDeletedPerson);

        assertThat(th.personRepository.count()).as("Exactly one person should be created").isEqualTo(
                personCountBefore + 1);
    }

    @Test
    public void personCreateRequest_Unauthorized() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "H. Hinterseer";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant1;

        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        // this can not be tests with assertOAuth2, because it handles the oAuthId differently
        mockMvc.perform(post("/person/event/personCreateRequest")
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isUnauthorized());

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);
    }

    @Test
    public void personCreateRequest_InvalidParameters() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "H. Hinterseer";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant1;

        long personCountBefore = th.personRepository.count();

        // homeAreaId empty
        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId("")
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_homeAreaIdEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.PERSON_INFORMATION_INVALID, "homeAreaId"));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // homeAreaId null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(null)
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_homeAreaIdNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.PERSON_INFORMATION_INVALID, "homeAreaId"));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // homeAreaId invalid
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId("invalid")
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_homeAreaIdInvalid"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("invalid", ClientExceptionType.GEO_AREA_NOT_FOUND));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // email empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .email("")
                .phoneNumber(phoneNumber)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_emailEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("email", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // email null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .email(null)
                .phoneNumber(phoneNumber)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_emailNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("email", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // firstName empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName("")
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_firstNameEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // firstName null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(null)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_firstNameNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("firstName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // lastName empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName("")
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_lastNameEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // lastName null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(null)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_lastNameNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("lastName", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // name empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name("")
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_nameEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.name", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // name null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(null)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_nameNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.name", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // street empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street("")
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_streetEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.street", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // street null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(null)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_streetNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.street", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // zip empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip("")
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_zipEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.zip", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // zip null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(null)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_zipNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.zip", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // city empty
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .email(email)
                .phoneNumber(phoneNumber)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city("")
                        .build())
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_cityEmpty"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.city", ClientExceptionType.PERSON_INFORMATION_INVALID));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

        // city null
        personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .email(email)
                .phoneNumber(phoneNumber)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(null).build())
                .homeAreaId(homeArea.getId())
            .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor("oAuthTestId_cityNull"))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException("address.city", ClientExceptionType.PERSON_INFORMATION_INVALID));
    }

    @Test
    public void personCreateRequest_AddressGetsNormalized() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";

        final String phoneNumber = "+4949188174949";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|hansi";

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(th.unnormalizedAddressDefinition1)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                //expect response to contain properly formatted address
                .andExpect(jsonPath("$.createdPerson.address.name").value(th.normalizedAddress1.getName()))
                .andExpect(jsonPath("$.createdPerson.address.street").value(th.normalizedAddress1.getStreet()))
                .andExpect(jsonPath("$.createdPerson.address.zip").value(th.normalizedAddress1.getZip()))
                .andExpect(jsonPath("$.createdPerson.address.city").value(th.normalizedAddress1.getCity()))
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                //expect response to contain properly formatted address
                .andExpect(jsonPath("$.address.name").value(th.normalizedAddress1.getName()))
                .andExpect(jsonPath("$.address.street").value(th.normalizedAddress1.getStreet()))
                .andExpect(jsonPath("$.address.zip").value(th.normalizedAddress1.getZip()))
                .andExpect(jsonPath("$.address.city").value(th.normalizedAddress1.getCity()))
                .andExpect(jsonPath("$.homeAreaId").value(homeArea.getId()))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(phoneNumber, addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        //check the address that is automatically added
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(addedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        assertEquals(th.normalizedAddress1.getName(), addressListEntry.getAddress().getName());
        assertEquals(th.normalizedAddress1.getStreet(), addressListEntry.getAddress().getStreet());
        assertEquals(th.normalizedAddress1.getZip(), addressListEntry.getAddress().getZip());
        assertEquals(th.normalizedAddress1.getCity(), addressListEntry.getAddress().getCity());
    }

    @Test
    public void personCreateRequest_EmptyAddressStreet() throws Exception {

        final String oauthId = "auth0|hansi";
        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = createClientPersonCreateRequest();
        personCreateRequest.getAddress().setStreet("");

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type").value(ClientExceptionType.PERSON_INFORMATION_INVALID.name()));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);

    }

    @Test
    public void personCreateRequest_EmptyAddressZip() throws Exception {

        final String oauthId = "auth0|hansi";
        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = createClientPersonCreateRequest();
        personCreateRequest.getAddress().setZip("");

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type").value(ClientExceptionType.PERSON_INFORMATION_INVALID.name()));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);
    }

    @Test
    public void personCreateRequest_EmptyAddressCity() throws Exception {

        final String oauthId = "auth0|hansi";
        long personCountBefore = th.personRepository.count();

        ClientPersonCreateRequest personCreateRequest = createClientPersonCreateRequest();
        personCreateRequest.getAddress().setCity("");

        mockMvc.perform(post("/person/event/personCreateRequest")
                        .headers(authHeadersFor(oauthId))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type").value(ClientExceptionType.PERSON_INFORMATION_INVALID.name()));

        assertThat(th.personRepository.count()).as("No person should be created").isEqualTo(personCountBefore);
    }

    @Test
    public void personCreateByAdminRequest_SuperAdmin() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = UUID.randomUUID() + "mail@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city)
                        .build())
                .homeAreaId(homeArea.getId())
                .build();

        MvcResult personCreateByAdminMvcResult = mockMvc.perform(post("/person/event/personCreateByAdminRequest")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.createdPerson.address.name").value(name))
                .andExpect(jsonPath("$.createdPerson.address.street").value(street))
                .andExpect(jsonPath("$.createdPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.createdPerson.address.city").value(city))
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()))
                .andReturn();

        ClientPersonCreateResponse clientPersonCreateResponse =
                toObject(personCreateByAdminMvcResult.getResponse(), ClientPersonCreateResponse.class);

        final String oauthId = testOauthManagementService.getOauthIdForCustomUser(
                clientPersonCreateResponse.getCreatedPerson().getEmail());

        assertThat(oauthId).isNotBlank();

        final Person addedPerson = personRepository.findByOauthId(oauthId).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(phoneNumber, addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());

        //check the address that is automatically added
        final Set<AddressListEntry> addresses = th.personRepository.findAllAddressListEntriesByPerson(addedPerson);
        assertEquals(1, addresses.size());
        AddressListEntry addressListEntry = addresses.iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME, addressListEntry.getName());
        Address address = th.addressRepository.findById(addressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void personCreateByAdminRequest_UserAdmin() throws Exception {

        callPersonCreateByAdminRequestIsNotAllowed(personUserAdminTenant1, th.geoAreaTenant2);

        callPersonCreateByAdminRequest(personUserAdminTenant1, th.subGeoArea1Tenant1);
    }

    @Test
    public void personCreateByAdminRequest_GlobalUserAdmin() throws Exception {

        callPersonCreateByAdminRequest(personGlobalUserAdmin, th.subGeoArea1Tenant1);

    }

    @Test
    public void personCreateByAdminRequest_MinimalData() throws Exception {

        callPersonCreateByAdminRequest(th.personSuperAdmin, th.geoAreaTenant2);
    }

    @Test
    public void personCreateByAdminRequest_PersonWithoutAdminRole() throws Exception {

        callPersonCreateByAdminRequestIsNotAllowed(th.personRegularAnna, th.geoAreaTenant2);
    }

    private void callPersonCreateByAdminRequest(Person caller, GeoArea homeArea) throws Exception {

        final String firstName = "Frosch";
        final String lastName = "König";
        final String email = UUID.randomUUID() + ".kuess@mich.nicht";

        MvcResult personCreateByAdminMvcResult = mockMvc.perform(post("/person/event/personCreateByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .email(email)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.homeAreaId").value(homeArea.getId()))
                .andReturn();

        ClientPersonCreateResponse clientPersonCreateResponse =
                toObject(personCreateByAdminMvcResult.getResponse(), ClientPersonCreateResponse.class);

        final String oauthId = testOauthManagementService.getOauthIdForCustomUser(
                clientPersonCreateResponse.getCreatedPerson().getEmail());

        assertThat(oauthId).as("oAuth account was not created").isNotBlank();

        final Person addedPerson = personRepository.findByOauthId(oauthId).get();

        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());
    }

    private void callPersonCreateByAdminRequestIsNotAllowed(Person caller, GeoArea homeArea) throws Exception {

        final String firstName = "Frosch";
        final String lastName = "König";
        final String email = "küss@mich.nicht";

        mockMvc.perform(post("/person/event/personCreateByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientPersonCreateRequest.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .email(email)
                        .homeAreaId(homeArea.getId())
                        .build())))
                .andExpect(isNotAuthorized());

        assertNull(testOauthManagementService.getOauthIdForCustomUser(email), "oAuth user was created");

        assertFalse(personRepository.findByEmail(email).isPresent(), "person was created");
    }

    @Test
    public void personCreateByAdminRequest_UserAlreadyExistsBackend() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;

        // Create person in backend
        Person person = Person.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeArea(homeArea)
                .build();

        personService.create(person);

        // Try to create person through admin-endpoint
        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateByAdminRequest")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        // Check that the created oauth account is deleted again
        assertNull(testOauthManagementService.getOauthIdForCustomUser(email));
    }

    @Test
    public void personCreateByAdminRequest_UserAlreadyExistsOAuth() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;

        //oauth account exists, but no person
        testOauthManagementService.createUser(email, "passwort", true);

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .homeAreaId(homeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personCreateByAdminRequest")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_EMAIL_ALREADY_REGISTERED));
    }

    @Test
    public void personCreateByAdminRequest_Unauthorized() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "H. Hinterseer";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi@hi.com";
        final GeoArea homeArea = th.geoAreaTenant1;

        ClientPersonCreateRequest personCreateRequest = ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(ClientAddressDefinition.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build())
                .homeAreaId(homeArea.getId())
                .build();

        assertOAuth2(post("/person/event/personCreateByAdminRequest")
                .contentType(contentType)
                .content(json(personCreateRequest)));
    }

    @Test
    public void personChangePasswordRequest() throws Exception {

        final Person person = th.personRegularAnna;
        final String newPassword = UUID.randomUUID().toString();

        OauthAccount oauthAccount = oauthManagementService.queryUserByOauthId(person.getOauthId());
        assertEquals(person.getOauthId(), oauthAccount.getOauthId());
        assertEquals(person.getEmail(), oauthAccount.getName());
        assertEquals(0, oauthAccount.getLoginCount());
        // check old password
        String currentPassword = oauthManagementService.getEmailToPasswordMap().get(person.getEmail());
        assertEquals("password", currentPassword);

        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword(currentPassword.toCharArray())
                                .newPassword(newPassword.toCharArray())
                                .build())))
                .andExpect(status().isOk());

        oauthAccount = oauthManagementService.queryUserByOauthId(person.getOauthId());
        assertEquals(person.getOauthId(), oauthAccount.getOauthId());
        assertEquals(person.getEmail(), oauthAccount.getName());
        assertEquals(1, oauthAccount.getLoginCount());
        // check new password
        currentPassword = oauthManagementService.getEmailToPasswordMap().get(person.getEmail());
        assertEquals(newPassword, currentPassword);
    }

    @Test
    public void personChangePasswordRequest_NotUsernamePasswordAccount() throws Exception {

        final Person person = th.personRegularAnna;
        OauthAccount oauthAccount = oauthManagementService.queryUserByOauthId(person.getOauthId());
        oauthAccount.setAuthenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.APPLE));

        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword("password".toCharArray())
                                .newPassword(UUID.randomUUID().toString().toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.PASSWORD_CHANGE_NOT_POSSIBLE));
    }

    @Test
    public void personChangePasswordRequest_WrongPassword() throws Exception {

        final Person person = th.personRegularAnna;

        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword("invalidPassword".toCharArray())
                                .newPassword(UUID.randomUUID().toString().toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.PASSWORD_WRONG));
    }

    @Test
    public void personChangePasswordRequest_InvalidParameters() throws Exception {

        final Person person = th.personRegularAnna;

        // oldPassword null
        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .newPassword(UUID.randomUUID().toString().toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // oldPassword empty
        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword("".toCharArray())
                                .newPassword(UUID.randomUUID().toString().toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // newPassword null
        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword("password".toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // newPassword empty
        mockMvc.perform(post("/person/event/personChangePasswordRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(ClientPersonChangePasswordRequest.builder()
                                .oldPassword("password".toCharArray())
                                .newPassword("".toCharArray())
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void personChangePasswordRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personChangePasswordRequest")
                .contentType(contentType)
                .content(json(ClientPersonChangePasswordRequest.builder()
                        .oldPassword("password".toCharArray())
                        .newPassword(UUID.randomUUID().toString().toCharArray())
                        .build())));
    }

    @Test
    public void personRegistrationTokenRequest() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi2@hi.com";
        final GeoArea homeArea = th.geoAreaTenant2;
        final String oauthId = "auth0|hansi2";

        final MockHttpServletResponse tokenResponse = mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId(oauthId)
                                        .email(email)
                                        .build())))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        ClientPersonRegistrationTokenResponse clientPersonRegistrationTokenResponse =
                toObject(tokenResponse, ClientPersonRegistrationTokenResponse.class);
        assertFalse(clientPersonRegistrationTokenResponse.isPersonExists());
        final String registrationToken = clientPersonRegistrationTokenResponse.getToken();
        assertNotNull(registrationToken);

        ClientPersonCreateWithRegistrationTokenRequest personCreateRequest =
                ClientPersonCreateWithRegistrationTokenRequest.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .nickName(nickName)
                        .phoneNumber(phoneNumber)
                        .email(email)
                        .address(ClientAddressDefinition.builder()
                                .name(name)
                                .street(street)
                                .zip(zip)
                                .city(city).build())
                        .homeAreaId(homeArea.getId())
                        .oauthId(oauthId)
                        .build();

        mockMvc.perform(post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken={token}",
                        registrationToken)
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.createdPerson.firstName").value(firstName))
                .andExpect(jsonPath("$.createdPerson.lastName").value(lastName))
                .andExpect(jsonPath("$.createdPerson.nickName").value(nickName))
                .andExpect(jsonPath("$.createdPerson.email").value(email))
                .andExpect(jsonPath("$.createdPerson.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.createdPerson.address.name").value(name))
                .andExpect(jsonPath("$.createdPerson.address.street").value(street))
                .andExpect(jsonPath("$.createdPerson.address.zip").value(zip))
                .andExpect(jsonPath("$.createdPerson.address.city").value(city));

        // Get the person again
        MvcResult getMvcResult = mockMvc.perform(get("/person")
                .headers(authHeadersFor(oauthId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.nickName").value(nickName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phoneNumber").value(phoneNumber))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city))
                .andReturn();
        ClientPersonOwn personJsonResponse = toObject(getMvcResult.getResponse(), ClientPersonOwn.class);

        // Check whether the person is also added to the repository
        Person addedPerson = personRepository.findById(personJsonResponse.getId()).get();

        assertEquals(nickName, addedPerson.getNickName());
        assertEquals(firstName, addedPerson.getFirstName());
        assertEquals(lastName, addedPerson.getLastName());
        assertEquals(phoneNumber, addedPerson.getPhoneNumber());
        assertEquals(email, addedPerson.getEmail());
        assertEquals(homeArea, addedPerson.getHomeArea());
        assertEquals(homeArea.getTenant(), addedPerson.getTenant());
    }

    @Test
    public void personRegistrationTokenRequest_OnExistingPerson() throws Exception {

        final String existingOauthId = th.personRegularAnna.getOauthId();
        final String existingEmail = th.personRegularAnna.getEmail();
        final String nonExistingOauthId = "nonexisting";
        final String nonExistingEmail = "nonexisting@example.org";
        //verify test data integrity
        assertFalse(th.personRepository.findByOauthId(nonExistingOauthId).isPresent());
        assertFalse(th.personRepository.findByEmail(nonExistingEmail).isPresent());

        //test all three combinations which return that the person already exists

        mockMvc.perform(post("/person/event/personRegistrationTokenRequest")
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                        .content(json(ClientPersonRegistrationTokenRequest.builder()
                                .oauthId(existingOauthId)
                                .email(existingEmail)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("token").isEmpty())
                .andExpect(jsonPath("personExists").value(true));

        mockMvc.perform(post("/person/event/personRegistrationTokenRequest")
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                        .content(json(ClientPersonRegistrationTokenRequest.builder()
                                .oauthId(existingOauthId)
                                .email(nonExistingEmail)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("token").isEmpty())
                .andExpect(jsonPath("personExists").value(true));

        mockMvc.perform(post("/person/event/personRegistrationTokenRequest")
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                        .content(json(ClientPersonRegistrationTokenRequest.builder()
                                .oauthId(nonExistingOauthId)
                                .email(existingEmail)
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("token").isEmpty())
                .andExpect(jsonPath("personExists").value(true));
    }

    @Test
    public void personRegistrationTokenRequest_Unauthorized() throws Exception {

        final String email = "hansi2@hi.com";
        final String oauthId = "auth|hansi";

        mockMvc.perform(post("/person/event/personRegistrationTokenRequest")
                        .contentType(contentType)
                        .content(json(ClientPersonRegistrationTokenRequest.builder()
                                .oauthId(oauthId)
                                .email(email)
                                .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));

        mockMvc.perform(post("/person/event/personRegistrationTokenRequest")
                .contentType(contentType)
                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void personRegistrationTokenRequest_missingParameters() throws Exception {

        final String email = "hansi2@hi.com";
        final String oauthId = "auth|hansi";

        mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId(oauthId)
                                        .email(null)
                                        .build())))
                .andExpect(isException("email", ClientExceptionType.PERSON_INFORMATION_INVALID));

        mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId(oauthId)
                                        .email(" ")
                                        .build())))
                .andExpect(isException("email", ClientExceptionType.PERSON_INFORMATION_INVALID));

        mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId(null)
                                        .email(email)
                                        .build())))
                .andExpect(isException("oauthId", ClientExceptionType.PERSON_INFORMATION_INVALID));

        mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId("")
                                        .email(email)
                                        .build())))
                .andExpect(isException("oauthId", ClientExceptionType.PERSON_INFORMATION_INVALID));
    }

    @Test
    public void personRegistrationTokenRequest_FailsBecauseOfDifferentReasons() throws Exception {

        final String firstName = "Hansi";
        final String lastName = "Hinterseer";
        final String nickName = "HansiHi";
        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";
        final String phoneNumber = "+4949188174949";
        final String email = "hansi3@hi.com";
        final GeoArea homeArea = th.geoAreaTenant1;
        final String oauthId = "auth0|hansi3";

        final MockHttpServletResponse tokenResponse = mockMvc.perform(
                        post("/person/event/personRegistrationTokenRequest")
                                .contentType(contentType)
                                .header(HEADER_NAME_API_KEY, th.config.getOauth().getApiKeyForRegistrationToken())
                                .content(json(ClientPersonRegistrationTokenRequest.builder()
                                        .oauthId(oauthId)
                                        .email(email)
                                        .build())))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final String registrationToken =
                toObject(tokenResponse, ClientPersonRegistrationTokenResponse.class).getToken();

        ClientPersonCreateWithRegistrationTokenRequest personCreateRequest =
                ClientPersonCreateWithRegistrationTokenRequest.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .nickName(nickName)
                        .phoneNumber(phoneNumber)
                        .email(email)
                        .address(ClientAddressDefinition.builder()
                                .name(name)
                                .street(street)
                                .zip(zip)
                                .city(city).build())
                        .oauthId(oauthId)
                        .build();

        //no homeArea set
        mockMvc.perform(
                        post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken={token}",
                                registrationToken)
                                .contentType(contentType)
                                .content(json(personCreateRequest)))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.PERSON_INFORMATION_INVALID, "homeAreaId"));

        personCreateRequest.setHomeAreaId(homeArea.getId());

        //no registration token
        mockMvc.perform(post("/person/event/personCreateWithRegistrationTokenRequest")
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(
                        isExceptionWithMessageContains(ClientExceptionType.UNSPECIFIED_BAD_REQUEST, "registrationToken",
                                "not present"));

        //registration token has wrong form
        mockMvc.perform(post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken="
                        + registrationToken + "abc")
                        .contentType(contentType)
                        .content(json(personCreateRequest)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.NOT_AUTHORIZED, "Token is invalid"));

        //registration token has expired
        timeService.setOffset(6, ChronoUnit.MINUTES);
        mockMvc.perform(
                        post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken={token}",
                                registrationToken)
                                .contentType(contentType)
                                .content(json(personCreateRequest)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.NOT_AUTHORIZED, "Token is expired"));
        timeService.resetOffset();

        //registration token has wrong oauthId
        personCreateRequest.setOauthId("something_different");
        mockMvc.perform(
                        post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken={token}",
                                registrationToken)
                                .contentType(contentType)
                                .content(json(personCreateRequest)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.NOT_AUTHORIZED,
                        "Token values do not match"));

        //registration token has wrong email
        personCreateRequest.setOauthId(oauthId);
        personCreateRequest.setEmail("something_different");
        mockMvc.perform(
                        post("/person/event/personCreateWithRegistrationTokenRequest?registrationToken={token}",
                                registrationToken)
                                .contentType(contentType)
                                .content(json(personCreateRequest)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.NOT_AUTHORIZED,
                        "Token values do not match"));
    }

    @Test
    public void personChangeHomeAreaRequest() throws Exception {

        final Person person = th.personVgAdmin;
        final GeoArea expectedNewHomeArea = th.geoAreaTenant3;

        final ClientPersonChangeHomeAreaRequest request = ClientPersonChangeHomeAreaRequest.builder()
                .newHomeAreaId(expectedNewHomeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personChangeHomeAreaRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.personId").value(person.getId()))
                .andExpect(jsonPath("$.homeAreaId").value(expectedNewHomeArea.getId()))
                .andExpect(jsonPath("$.status").value("APPROVED"));

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);

        assertEquals(expectedNewHomeArea, changedPerson.getHomeArea());
        assertEquals(expectedNewHomeArea.getTenant(), changedPerson.getTenant());

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonSilent was triggered
        ClientPersonChangeHomeAreaResponse pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(changedPerson,
                        ClientPersonChangeHomeAreaResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_OWN_PERSON_CHANGES));
        assertEquals(expectedNewHomeArea.getId(), pushedEvent.getHomeAreaId());
        assertEquals(person.getId(), pushedEvent.getPersonId());
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personChangeHomeAreaRequest_InvalidHomeArea() throws Exception {

        final Person person = th.personVgAdmin;

        final GeoArea expectedOldHomeArea = th.personVgAdmin.getHomeArea();
        final String expectedNewHomeAreaId = BaseTestHelper.INVALID_UUID;

        assertNotEquals(expectedOldHomeArea.getId(), expectedNewHomeAreaId);

        final ClientPersonChangeHomeAreaRequest request = ClientPersonChangeHomeAreaRequest.builder()
                .newHomeAreaId(expectedNewHomeAreaId)
                .build();

        mockMvc.perform(post("/person/event/personChangeHomeAreaRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);

        assertEquals(expectedOldHomeArea, changedPerson.getHomeArea());

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonSilent was NOT triggered
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personChangeHomeAreaRequest_HomeAreaNotLeaf() throws Exception {
        final Person person = th.personVgAdmin;

        final GeoArea expectedOldHomeArea = th.personVgAdmin.getHomeArea();
        final GeoArea expectedNewHomeArea = th.geoAreaParent;

        assertNotEquals(expectedOldHomeArea.getId(), expectedNewHomeArea.getId());

        final ClientPersonChangeHomeAreaRequest request = ClientPersonChangeHomeAreaRequest.builder()
                .newHomeAreaId(expectedNewHomeArea.getId())
                .build();

        mockMvc.perform(post("/person/event/personChangeHomeAreaRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(isException(ClientExceptionType.GEO_AREA_IS_NO_LEAF));

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);

        assertEquals(expectedOldHomeArea, changedPerson.getHomeArea());

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonSilent was NOT triggered
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personChangeHomeAreaRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personChangeHomeAreaRequest")
                .contentType(contentType)
                .content(json(ClientPersonChangeHomeAreaRequest.builder()
                        .newHomeAreaId(th.geoAreaDorf1InEisenberg.getId())
                        .build())));
    }

    @Test
    public void personChangeCommunityRequest() throws Exception {

        Person personWithCommunity = th.personVgAdmin;
        personWithCommunity.setHomeArea(null);
        personWithCommunity = th.personRepository.saveAndFlush(personWithCommunity);
        final Tenant expectedNewHomeTenant = th.tenant2;

        assertNotEquals(personWithCommunity.getTenant(), expectedNewHomeTenant);
        assertNull(personWithCommunity.getHomeArea());

        final ClientPersonChangeCommunityRequest changeCommunityRequest = ClientPersonChangeCommunityRequest.builder()
                .newHomeCommunityId(expectedNewHomeTenant.getId())
                .build();

        mockMvc.perform(post("/person/event/personChangeCommunityRequest")
                        .headers(authHeadersFor(personWithCommunity))
                        .contentType(contentType)
                        .content(json(changeCommunityRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.personId").value(personWithCommunity.getId()))
                .andExpect(jsonPath("$.newHomeCommunityId").value(expectedNewHomeTenant.getId()));

        final Person changedPerson = personRepository.findById(personWithCommunity.getId()).orElse(null);

        assertEquals(expectedNewHomeTenant, changedPerson.getTenant());
        assertNull(changedPerson.getHomeArea());
    }

    @Test
    public void personChangeCommunityRequest_DoNotChangeHomeGeoArea() throws Exception {

        Person personWithCommunity = th.personVgAdmin;
        GeoArea expectedHomeGeoArea = personWithCommunity.getHomeArea();
        assertNotNull(expectedHomeGeoArea);
        final Tenant expectedNewHomeTenant = th.tenant2;

        assertNotEquals(personWithCommunity.getTenant(), expectedNewHomeTenant);

        final ClientPersonChangeCommunityRequest changeCommunityRequest = ClientPersonChangeCommunityRequest.builder()
                .newHomeCommunityId(expectedNewHomeTenant.getId())
                .build();

        mockMvc.perform(post("/person/event/personChangeCommunityRequest")
                        .headers(authHeadersFor(personWithCommunity))
                        .contentType(contentType)
                        .content(json(changeCommunityRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.personId").value(personWithCommunity.getId()))
                .andExpect(jsonPath("$.newHomeCommunityId").value(expectedNewHomeTenant.getId()));

        final Person changedPerson = personRepository.findById(personWithCommunity.getId()).orElse(null);

        assertEquals(expectedNewHomeTenant, changedPerson.getTenant());
        assertEquals(expectedHomeGeoArea, changedPerson.getHomeArea());
    }

    @Test
    public void personChangeCommunityRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personChangeCommunityRequest")
                .contentType(contentType)
                .content(json(ClientPersonChangeCommunityRequest.builder()
                        .newHomeCommunityId(th.tenant3.getId())
                        .build())));
    }

    @Test
    public void personChangeCommunityRequest_InvalidParameters() throws Exception {

        Person personWithCommunity = th.personVgAdmin;
        Tenant expectedNewHomeTenant = th.tenant3;
        Tenant expectedOldHomeTenant = personWithCommunity.getTenant();

        assertNotEquals(expectedOldHomeTenant, expectedNewHomeTenant);

        //newHomeCommunityId empty
        ClientPersonChangeCommunityRequest changeCommunityRequest = ClientPersonChangeCommunityRequest.builder()
                .newHomeCommunityId("")
                .build();

        mockMvc.perform(post("/person/event/personChangeCommunityRequest")
                        .headers(authHeadersFor(personWithCommunity))
                        .contentType(contentType)
                        .content(json(changeCommunityRequest)))
                .andExpect(isException("newHomeCommunityId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        Person changedPerson = personRepository.findById(personWithCommunity.getId()).orElse(null);

        assertEquals(expectedOldHomeTenant, changedPerson.getTenant());

        //newHomeCommunityId null
        changeCommunityRequest = ClientPersonChangeCommunityRequest.builder()
                .newHomeCommunityId(null)
                .build();

        mockMvc.perform(post("/person/event/personChangeCommunityRequest")
                        .headers(authHeadersFor(personWithCommunity))
                        .contentType(contentType)
                        .content(json(changeCommunityRequest)))
                .andExpect(isException("newHomeCommunityId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        changedPerson = personRepository.findById(personWithCommunity.getId()).orElse(null);

        assertEquals(expectedOldHomeTenant, changedPerson.getTenant());

        //newHomeCommunityId not existing
        changeCommunityRequest = ClientPersonChangeCommunityRequest.builder()
                .newHomeCommunityId("not existing")
                .build();

        mockMvc.perform(post("/person/event/personChangeCommunityRequest")
                        .headers(authHeadersFor(personWithCommunity))
                        .contentType(contentType)
                        .content(json(changeCommunityRequest)))
                .andExpect(isException("not existing", ClientExceptionType.TENANT_NOT_FOUND));

        changedPerson = personRepository.findById(personWithCommunity.getId()).orElse(null);

        assertEquals(expectedOldHomeTenant, changedPerson.getTenant());
    }

    @Test
    public void personResendVerificationEmailRequest_FeatureEnabledAppVariant() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final AppVariant appVariant = th.app1Variant1;
        final Person person = th.personRegularAnna;

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .appVariant(appVariant)
                .enabled(true)
                .build());

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType))
                .andExpect(status().isOk());

        final Person updatedPerson =
                personService.findPersonById(person.getId());
        assertEquals(timeServiceMock.currentTimeMillisUTC(), updatedPerson.getLastSentTimeEmailVerification());
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(updatedPerson.getEmail())
                .doesNotContain(updatedPerson.getPendingNewEmail());
    }

    @Test
    public void personResendVerificationEmailRequest_FeatureEnabledTenant() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person person = th.personRegularAnna;
        final Tenant tenant = th.tenant1;
        person.setTenant(tenant);
        th.personRepository.save(person);

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .tenant(tenant)
                .enabled(true)
                .build());

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(person, th.app1Variant1))
                .contentType(contentType))
                .andExpect(status().isOk());
        final Person updatedPerson =
                personService.findPersonById(person.getId());
        assertEquals(timeServiceMock.currentTimeMillisUTC(), updatedPerson.getLastSentTimeEmailVerification());
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(updatedPerson.getEmail())
                .doesNotContain(updatedPerson.getPendingNewEmail());
    }

    @Test
    public void personResendVerificationEmailRequest_FeatureDisabled() throws Exception {

        final AppVariant appVariantValid = th.app1Variant1;
        final AppVariant appVariantDisabled = th.app2Variant2;
        final Person personEnabled = th.personRegularAnna;
        final Tenant tenantEnabled = th.tenant1;
        final Person personDisabled = th.personIeseAdmin;
        final Tenant tenantDisabled = th.tenant2;

        personEnabled.setTenant(tenantEnabled);
        personDisabled.setTenant(tenantDisabled);
        th.personRepository.save(personEnabled);
        th.personRepository.save(personDisabled);

        // per default feature disabled
        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(personEnabled, appVariantValid))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .appVariant(appVariantValid)
                .enabled(true)
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .tenant(tenantEnabled)
                .enabled(true)
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .tenant(tenantDisabled)
                .enabled(false)
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .appVariant(appVariantDisabled)
                .enabled(false)
                .build());

        // feature enabled for tenantEnabled and appVariantValid
        // but disabled for tenantDisabled and appVariantDisabled
        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(personEnabled, appVariantDisabled))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(personDisabled, appVariantValid))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(personDisabled, appVariantDisabled))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(personDisabled.getEmail())
                .doesNotContain(personDisabled.getPendingNewEmail());
    }

    @Test
    public void personResendVerificationEmailRequest_WithPendingNewEmail() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final Person person = th.personRegularKarl;
        person.setPendingNewEmail("new@mail.de");
        person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.saveAndFlush(person);

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .build());

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType))
                .andExpect(status().isOk());

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail())
                .contains(person.getPendingNewEmail());
    }

    @Test
    public void personResendVerificationEmailRequest_EmailResendIntervalNotElapsed() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final AppVariant appVariant = th.app1Variant1;
        final Person person = th.personRegularKarl;
        final long lastSentTimeEmailVerification =
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(1);
        // sent last verification email 1 minute ago
        person.setLastSentTimeEmailVerification(lastSentTimeEmailVerification);
        th.personRepository.save(person);

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .build());

        long nextTimestampForResendingVerificationMail =
                lastSentTimeEmailVerification + participantsConfig.getEmailResendInterval().toMillis();
        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.RESEND_VERIFICATION_EMAIL_NOT_POSSIBLE,
                        timeServiceMock.toLocalTimeHumanReadable(nextTimestampForResendingVerificationMail)));

        final Person updatedPerson = personService.findPersonById(person.getId());
        assertEquals(updatedPerson.getLastSentTimeEmailVerification(), lastSentTimeEmailVerification);
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail())
                .doesNotContain(person.getPendingNewEmail());
    }

    @Test
    public void personResendVerificationEmailRequest_EmailAlreadyVerified() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final Person person = th.personRegularKarl;
        person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.saveAndFlush(person);

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .build());

        mockMvc.perform(post("/person/event/personResendVerificationEmailRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.RESEND_VERIFICATION_EMAIL_NOT_POSSIBLE));

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(person.getEmail())
                .doesNotContain(person.getPendingNewEmail());
    }

    @Test
    public void personVerifyEmailAddressRequest() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person person = th.personRegularKarl;
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(person.getEmail())
                        .build();

        // before verification, the verification status should not contain PersonVerificationStatus.EMAIL_VERIFIED
        assertFalse(person.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has verification status 'EMAIL_VERIFIED'");
        assertThat(person.getNumberOfEmailChanges()).isNull();

        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", authorizationService.generateAuthToken(
                                personChangeEmailAddressVerificationStatusRequest,
                                participantsConfig.getEmailVerificationExpirationTime()))
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.personWithVerifiedEmail.id").value(person.getId()))
                .andExpect(jsonPath("$.personWithVerifiedEmail.email").value(person.getEmail()))
                .andExpect(jsonPath("$.verificationStatusChanged").value(true))
                .andExpect(jsonPath("$.success").value(true));

        final Person updatedPerson = personService.findPersonById(person.getId());
        assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertEquals(0, updatedPerson.getNumberOfEmailChanges());

        // verify push message
        waitForEventProcessing();

        // assert that pushToPersonInLastUsedAppVariantLoud was triggered
        final Pair<String, ClientPersonVerifyEmailAddressResponse> pushedEvent =
                testClientPushService.getPushedEventToPersonInLastUsedAppVariantLoud(
                        updatedPerson,
                        ClientPersonVerifyEmailAddressResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED));

        assertEquals(updatedPerson.getId(), pushedEvent.getRight().getPersonWithVerifiedEmail().getId());
        assertEquals("E-Mail-Adresse '" + updatedPerson.getEmail() + "' wurde erfolgreich verifiziert",
                pushedEvent.getLeft());
        assertTrue(pushedEvent.getRight().isVerificationStatusChanged(),
                "'EMAIL_VERIFIED' verification status was not added to person");
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personVerifyEmailAddressRequest_WithPendingNewEmail() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person person = th.personRegularKarl;
        final String newEmail = "new@mail.de";
        person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        person.setPendingNewEmail(newEmail);
        person.setNumberOfEmailChanges(5);
        person = personRepository.saveAndFlush(person);
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(newEmail)
                        .build();

        assertTrue(person.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has verification status 'EMAIL_VERIFIED'");
        assertThat(person.getEmail()).isNotEqualTo(newEmail);
        assertThat(person.getPendingNewEmail()).isEqualTo(newEmail);

        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", authorizationService.generateAuthToken(
                                personChangeEmailAddressVerificationStatusRequest,
                                participantsConfig.getEmailVerificationExpirationTime()))
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.personWithVerifiedEmail.id").value(person.getId()))
                .andExpect(jsonPath("$.personWithVerifiedEmail.email").value(person.getPendingNewEmail()))
                .andExpect(jsonPath("$.verificationStatusChanged").value(false))
                .andExpect(jsonPath("$.success").value(true));

        final Person updatedPerson = personService.findPersonById(person.getId());
        assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertThat(updatedPerson.getEmail()).isEqualTo(newEmail);
        assertThat(updatedPerson.getPendingNewEmail()).isNull();
        assertEquals(0, updatedPerson.getNumberOfEmailChanges());

        // verify push message
        waitForEventProcessing();

        // assert that pushToPersonInLastUsedAppVariantLoud was triggered
        final Pair<String, ClientPersonVerifyEmailAddressResponse> pushedEvent =
                testClientPushService.getPushedEventToPersonInLastUsedAppVariantLoud(
                        updatedPerson,
                        ClientPersonVerifyEmailAddressResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED));

        assertEquals(updatedPerson.getId(), pushedEvent.getRight().getPersonWithVerifiedEmail().getId());
        assertEquals("E-Mail-Adresse wurde erfolgreich zu '" + updatedPerson.getEmail() + "' geändert",
                pushedEvent.getLeft());
        assertFalse(pushedEvent.getRight().isVerificationStatusChanged(),
                "'EMAIL_VERIFIED' verification status was added to person");
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personVerifyEmailAddressRequest_AlreadyVerified() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person person = th.personRegularKarl;
        person.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        person.setNumberOfEmailChanges(5);
        th.personRepository.saveAndFlush(person);
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(person.getEmail())
                        .build();

        // person already has PersonVerificationStatus.EMAIL_VERIFIED
        assertTrue(person.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");

        // verify again same email address
        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", authorizationService.generateAuthToken(
                                personChangeEmailAddressVerificationStatusRequest,
                                participantsConfig.getEmailVerificationExpirationTime()))
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.personWithVerifiedEmail.id").value(person.getId()))
                .andExpect(jsonPath("$.verificationStatusChanged").value(false))
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value(
                        "E-Mail-Adresse '" + person.getEmail() + "' wurde erfolgreich verifiziert."));

        final Person updatedPerson = personService.findPersonById(person.getId());
        assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertEquals(0, updatedPerson.getNumberOfEmailChanges());

        // verify push message
        waitForEventProcessing();

        // no push message should be sent when email verification status was already added
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void personVerifyEmailAddressRequest_Unauthorized() throws Exception {

        final Person person = th.personRegularKarl;
        final String mailToken = "invalidToken";

        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.personWithVerifiedEmail").doesNotExist())
                .andExpect(jsonPath("$.verificationStatusChanged").value(false))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value(
                        "Hier stimmt etwas nicht. Probiere den Link aus der E-Mail manuell zu kopieren (Rechtsklick, Link kopieren) und in den Browser einzufügen. Wenn auch das nicht klappt, fordere bitte in der App eine neue E-Mail zur Bestätigung deiner Adresse an."));
    }

    @Test
    public void personVerifyEmailAddressRequest_InvalidParameters() throws Exception {

        final String id = "Fake ID";
        final String mailToken = "validToken";

        // Person does not exist
        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(id))))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        // mailToken not provided
        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(id))))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        // personId empty
        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(""))))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId null
        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(null))))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void personVerifyEmailAddressRequest_EmailAddressChangedBeforeVerification() throws Exception {

        Person person = th.personRegularKarl;
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(person.getEmail())
                        .build();
        final String mailToken =
                authorizationService.generateAuthToken(personChangeEmailAddressVerificationStatusRequest,
                        Duration.ofMinutes(5));

        // change email address so that the mail tokens required for verification do not match
        person.setEmail("new@mail.de");
        person = th.personRepository.save(person);

        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.personWithVerifiedEmail").doesNotExist())
                .andExpect(jsonPath("$.verificationStatusChanged").value(false))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value(
                        "Die E-Mail ist leider schon zu alt und war für die Bestätigung einer anderen Adresse gedacht. Bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an."));
    }

    @Test
    public void personVerifyEmailAddressRequest_EmailExpirationTimeExceeded() throws Exception {

        final Person person = th.personRegularKarl;
        final PersonChangeEmailAddressVerificationStatusRequest personChangeEmailAddressVerificationStatusRequest =
                PersonChangeEmailAddressVerificationStatusRequest.builder()
                        .person(person)
                        .emailAddress(person.getEmail())
                        .build();
        final String mailToken = authorizationService.generateAuthToken(
                personChangeEmailAddressVerificationStatusRequest,
                Duration.ZERO);

        timeService.setOffset(5, ChronoUnit.MINUTES);

        mockMvc.perform(post("/person/event/personVerifyEmailRequest/mail")
                        .contentType(contentType)
                        .param("mailToken", mailToken)
                        .content(json(new ClientPersonVerifyEmailAddressRequest(person.getId()))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.personWithVerifiedEmail").doesNotExist())
                .andExpect(jsonPath("$.verificationStatusChanged").value(false))
                .andExpect(jsonPath("$.success").value(false))
                .andExpect(jsonPath("$.message").value(
                        "Die E-Mail ist leider schon zu alt, bitte fordere in der App eine neue E-Mail zur Bestätigung deiner Adresse an."));
    }

    @Test
    public void personChangeProfilePicture() throws Exception {

        final Person person = th.personRegularAnna;

        // check that the person already has a profile picture
        assertNotNull(person.getProfilePicture());

        final TemporaryMediaItem newProfilePicture =
                th.createTemporaryMediaItem(person, timeService.currentTimeMillisUTC());

        final ClientPersonProfilePictureChangeRequest request = ClientPersonProfilePictureChangeRequest.builder()
                .temporaryMediaItemId(newProfilePicture.getId())
                .build();

        MvcResult result = mockMvc.perform(post("/person/event/personProfilePictureChangeRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(status().isOk())
                .andReturn();

        final ClientPersonProfilePictureChangeConfirmation confirmation =
                toObject(result, ClientPersonProfilePictureChangeConfirmation.class);

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);

        // check that updated person contains the changed profile picture
        assertEquals(changedPerson.getProfilePicture(), newProfilePicture.getMediaItem());
    }

    @Test
    public void personChangeProfilePicture_NullTemporaryMediaItemId() throws Exception {

        final Person person = th.personRegularAnna;
        final ClientPersonProfilePictureChangeRequest request = ClientPersonProfilePictureChangeRequest.builder()
                .temporaryMediaItemId(null)
                .build();

        mockMvc.perform(post("/person/event/personProfilePictureChangeRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(request)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void personChangeProfilePicture_WrongTemporaryMediaItemId() throws Exception {

        final Person person = th.personRegularAnna;
        final ClientPersonProfilePictureChangeRequest request = ClientPersonProfilePictureChangeRequest.builder()
                .temporaryMediaItemId("random-temp-media-item-id")
                .build();

        mockMvc.perform(post("/person/event/personProfilePictureChangeRequest")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void personChangeProfilePicture_Unauthorized() throws Exception {

        final Person person = th.personRegularAnna;
        final ClientPersonProfilePictureChangeRequest request = ClientPersonProfilePictureChangeRequest.builder()
                .temporaryMediaItemId(th.createTemporaryMediaItem(person, timeService.currentTimeMillisUTC()).getId())
                .build();

        assertOAuth2(post("/person/event/personProfilePictureChangeRequest")
                .contentType(contentType)
                .content(json(request)));
    }

    @Test
    public void personDeleteProfilePicture() throws Exception {

        final Person person = th.personRegularAnna;

        // check that the person already has a profile picture
        assertNotNull(person.getProfilePicture());

        mockMvc.perform(post("/person/event/personProfilePictureDeleteRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType))
                .andExpect(status().isOk());

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);

        // check that the person now has no profile picture
        assertNull(changedPerson.getProfilePicture());
    }

    @Test
    public void personDeleteProfilePicture_NoProfilePictureAvailable() throws Exception {

        final Person person = th.personRegularAnna;

        // ensure that the person does not have a profile picture set
        if (person.getProfilePicture() != null) {
            person.setProfilePicture(null);
            personRepository.saveAndFlush(person);
        }

        mockMvc.perform(post("/person/event/personProfilePictureDeleteRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType))
                .andExpect(status().isOk());

        final Person changedPerson = personRepository.findById(person.getId()).orElse(null);
        assertNull(changedPerson.getProfilePicture());
    }

    @Test
    public void personDeleteProfilePicture_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personProfilePictureDeleteRequest")
                .contentType(contentType));
    }

    private HttpHeaders authHeadersFor(String oauthId) {
        return authHeadersFor(AuthorizationData.builder()
                .oauthUserId(oauthId)
                .build());
    }

    private ClientPersonCreateRequest createClientPersonCreateRequest() {

        String firstName = "Hansi";
        String lastName = "Hinterseer";
        String nickName = "HansiHi";
        String phoneNumber = "+4949188174949";
        String email = "hansi@hi.com";

        return ClientPersonCreateRequest.builder()
                .firstName(firstName)
                .lastName(lastName)
                .nickName(nickName)
                .phoneNumber(phoneNumber)
                .email(email)
                .address(th.normalizedAddressDefinition1)
                .homeAreaId(th.geoAreaTenant1.getId())
                .build();
    }

}
