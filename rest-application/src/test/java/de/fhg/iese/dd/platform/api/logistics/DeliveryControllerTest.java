/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;

public class DeliveryControllerTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
     }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    private void specificInit() throws Exception {

        String shopOrderId = purchaseOrderReceived();

        waitForEventProcessing();

        delivery = purchaseOrderReadyForTransport(shopOrderId);

        waitForEventProcessing();
    }

    @Test
    public void getAllDeliveries() throws Exception {

        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getAllDeliveriesNoDeliveries() throws Exception {

        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(th.personRegularAnna)))
            .andExpect(content().contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAllDeliveriesUnauthorized() throws Exception {
        assertOAuth2(get("/logistics/delivery/")
                .contentType(contentType));
    }

    @Test
    public void getOneDeliveryReceiver() throws Exception {

        mockMvc.perform(get("/logistics/delivery/" + delivery.getId())
            .headers(authHeadersFor(receiver)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(delivery.getId()))
            .andExpect(jsonPath("$.currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$.receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$.sender.shop.id").value(sender1Id))
            .andExpect(jsonPath("$.trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$.deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$.deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$.deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$.deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneDeliveryShopOwner() throws Exception {

        mockMvc.perform(get("/logistics/delivery/" + delivery.getId())
            .headers(authHeadersFor(th.personShopOwner)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(delivery.getId()))
            .andExpect(jsonPath("$.currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT.toString()))
            .andExpect(jsonPath("$.receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$.sender.shop.id").value(sender1Id))
            .andExpect(jsonPath("$.trackingCode").value(delivery.getTrackingCode()))
            .andExpect(jsonPath("$.deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$.deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$.deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$.deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneDeliveryUnauthorized() throws Exception {

        assertOAuth2(get("/logistics/delivery/" + delivery.getId())
                .contentType(contentType));

        mockMvc.perform(get("/logistics/delivery/" + delivery.getId())
                .headers(authHeadersFor(th.personRegularAnna)))
                .andExpect(isException(ClientExceptionType.ACCESSED_ANOTHERS_RESOURCES));
    }

    @Test
    public void getOneDeliveryInvalidId() throws Exception {

        mockMvc.perform(get("/logistics/delivery/" + "invalidID")
                .headers(authHeadersFor(th.personRegularAnna)))
                .andExpect(isException(ClientExceptionType.DELIVERY_NOT_FOUND));
    }
    
}
