/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxReadyForAllocationResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxReadyForDeallocationResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupReceivedResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportDeliveredResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransport;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransportAlternative;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportStatus;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportType;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportConfirmation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationBoxType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

public abstract class BaseLogisticsEventTest extends BaseServiceTest {

    protected Person receiver;
    protected String receiverId;
    protected Shop sender1;
    protected String sender1Id;
    protected Shop sender2;
    protected String sender2Id;
    protected Person carrier;
    protected Person carrierFirstStep;
    protected Person carrierLastStep;
    protected String tenant1Id;
    protected String tenant2Id;

    protected PoolingStation deliveryPoolingStation;
    protected PoolingStation intermediatePoolingStation;

    protected Address deliveryPoolingStationAddress;
    protected Address intermediatePoolingStationAddress;

    // only used when direct delivery
    protected ClientAddressDefinition deliveryAddress;
    protected ClientAddressDefinition pickupAddress;

    protected PoolingStationBox intermediatePoolingStationBox;
    protected PoolingStationBox deliveryPoolingStationBox;

    @Autowired
    protected LogisticsTestHelper th;

    @Autowired
    protected ApplicationConfig config;

    protected void init() {

        receiver = th.personVgAdmin;
        receiverId = receiver.getId();
        sender1 = th.shop1;
        sender1Id = sender1.getId();
        sender2 = th.shop2;
        sender2Id = sender2.getId();
        carrier = th.personRegularKarl;
        carrierFirstStep = th.personPoolingStationOp;
        carrierLastStep = th.personIeseAdmin;
        tenant1Id = th.tenant1.getId();
        tenant2Id = th.tenant2.getId();

        deliveryPoolingStation = th.poolingStation1;
        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();
        // first unused poolingStationBox is selected, therefore we can be sure that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox1;

        intermediatePoolingStation = th.poolingStation4;

        deliveryAddress = ClientAddressDefinition.builder()
                .name("Balthasar Weitzel")
                .street("Pfaffenbärstraße 42")
                .zip("67663")
                .city("Kaiserslautern")
                .build();
        pickupAddress = ClientAddressDefinition.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .build();

        assertNotEquals(receiver, carrier);
        assertNotEquals(receiver, carrierFirstStep);
        assertNotEquals(receiver, carrierLastStep);
        assertNotEquals(carrier, carrierFirstStep);
        assertNotEquals(carrier, carrierLastStep);
        assertNotEquals(carrierFirstStep, carrierLastStep);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    /**
     * Send and test a PurchaseOrderReceivedEvent, directly to receiver. Uses shop1 in tenant1
     * @return
     * @throws Exception
     */
    protected String purchaseOrderReceived() throws Exception {
        return purchaseOrderReceived("", receiver, pickupAddress);
    }

    /**
     * Send and test a PurchaseOrderReceivedEvent, directly to receiver. Uses shop2 in tenant2
     *
     * @return
     *
     * @throws Exception
     */
    protected String purchaseOrderReceivedShop2() throws Exception {
        return purchaseOrderReceived(sender2Id, tenant2Id, "", receiver, pickupAddress);
    }

    protected String purchaseOrderReceived(String deliveryPoolingStationId, Person receiverPerson,
            ClientAddressDefinition pickupAddress) throws Exception {
        return purchaseOrderReceived(sender1Id, tenant1Id, deliveryPoolingStationId, receiverPerson, pickupAddress);
    }

    protected String purchaseOrderReceived(String senderShopId, String tenantId, String deliveryPoolingStationId,
            Person receiverPerson, ClientAddressDefinition pickupAddress) throws Exception {

        int numberDeliveriesBefore = th.deliveryRepository.findAll().size();
        int numberPurchaseOrdersBefore = th.purchaseOrderRepository.findAll().size();

        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = ClientPurchaseOrderReceivedEvent.builder()
                .shopOrderId(shopOrderId)
                .communityId(tenantId)
                .senderShopId(senderShopId)
                .purchaseOrderNotes("purchaseOrderNotes" + UUID.randomUUID())
                .pickupAddress(pickupAddress)
                .receiverPersonId(receiverPerson.getId())
                .deliveryAddress(deliveryAddress)
                .transportNotes("transportNotes" + UUID.randomUUID())
                .orderedItems(Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")))
                .desiredDeliveryTime(new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + TimeUnit.HOURS.toMillis(3L)))
                .deliveryPoolingStationId(deliveryPoolingStationId)
                .credits(8)
                .build();

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(numberDeliveriesBefore+1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries
                .stream()
                .filter(d -> Objects.equals(d.getSender().getShop().getId(), senderShopId) &&
                        Objects.equals(d.getTenant().getId(), tenantId))
                .findFirst()
                .orElse(null);
        assertNotNull(delivery);

        ParcelAddress actualPickupAddress = delivery.getPickupAddress();
        assertEquals(ParcelAddressType.SHOP, actualPickupAddress.getAddressType(), "Type of pickupAddress");
        assertEquals(senderShopId, actualPickupAddress.getShop().getId(), "Shop of pickupAddress");
        assertAddressEquals(pickupAddress, actualPickupAddress.getAddress(), "Details of pickupAddress");

        ParcelAddress actualDeliveryAddress = delivery.getDeliveryAddress();
        ClientAddressDefinition expectedAddress = null;

        if (StringUtils.isEmpty(deliveryPoolingStationId)) {
            //delivery to person
            expectedAddress = deliveryAddress;
            assertEquals(ParcelAddressType.PRIVATE, actualDeliveryAddress.getAddressType(), "Type of deliveryAddress");
            assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(), actualDeliveryAddress.getPerson().getId(),
                    "Person of deliveryAddress");
            assertAddressEquals(deliveryAddress, actualDeliveryAddress.getAddress(), "Details of deliveryAddress");
        } else {
            //delivery to pooling station
            final PoolingStation poolingStation = th.poolingStationRepository
                    .findById(purchaseOrderReceivedEvent.getDeliveryPoolingStationId()).get();
            Address poolingStationAddress = poolingStation.getAddress();
            expectedAddress = ClientAddressDefinition.builder()
                    .name(poolingStationAddress.getName())
                    .street(poolingStationAddress.getStreet())
                    .zip(poolingStationAddress.getZip())
                    .city(poolingStationAddress.getCity())
                    .gpsLocation(poolingStationAddress.getGpsLocation() == null ? null :
                            new ClientGPSLocation(poolingStationAddress.getGpsLocation().getLatitude(),
                                    poolingStationAddress.getGpsLocation().getLongitude()))
                    .build();

            assertEquals(ParcelAddressType.POOLING_STATION, actualDeliveryAddress.getAddressType(),
                    "Type of deliveryAddress");
            assertEquals(purchaseOrderReceivedEvent.getDeliveryPoolingStationId(),
                    actualDeliveryAddress.getPoolingStation().getId(),
                    "PoolingStationId");
            assertAddressEquals(expectedAddress, actualDeliveryAddress.getAddress(),
                    "Details of deliveryAddress Poolingstation");
        }

        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime(),
                delivery.getDesiredDeliveryTime().getStartTime(),
                "DesiredDeliveryTime start");
        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getEndTime(),
                delivery.getDesiredDeliveryTime().getEndTime(),
                "DesiredDeliveryTime end");

        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(), delivery.getReceiver().getPerson().getId(),
                "Receiver");
        assertEquals(LogisticsParticipantType.PRIVATE, delivery.getReceiver().getParticipantType(), "Receiver");
        assertEquals(purchaseOrderReceivedEvent.getSenderShopId(), delivery.getSender().getShop().getId(),
                "Sender");
        assertEquals(LogisticsParticipantType.SHOP, delivery.getSender().getParticipantType(), "Sender");

        assertEquals(purchaseOrderReceivedEvent.getTransportNotes(), delivery.getTransportNotes(),
                "TransportNotes");

        // Check created purchase order
        List<PurchaseOrder> purchaseOrders = th.purchaseOrderRepository.findAll();
        assertEquals(numberPurchaseOrdersBefore+1, purchaseOrders.size(), "Amount of purchaseOrders");
        PurchaseOrder purchaseOrder = purchaseOrders
                .stream()
                .filter(po -> Objects.equals(po.getShopOrderId(), shopOrderId))
                .findFirst()
                .orElse(null);

        assertNotNull(purchaseOrder);

        assertEquals(purchaseOrderReceivedEvent.getPurchaseOrderNotes(), purchaseOrder.getPurchaseOrderNotes(),
                "PurchaseOrderNotes");

        // Check result
        ClientDeliveryCreatedEvent deliveryCreatedEvent = toObject(response.getResponse(),
                ClientDeliveryCreatedEvent.class);

        assertNotNull(deliveryCreatedEvent);
        assertEquals(delivery.getId(), deliveryCreatedEvent.getDeliveryId(), "DeliveryId");
        assertEquals(delivery.getTrackingCode(), deliveryCreatedEvent.getDeliveryTrackingCode(),
                "DeliveryTrackingCode");
        assertEquals(purchaseOrder.getId(), deliveryCreatedEvent.getPurchaseOrderId(), "PurchaseOrderId");

        // --> Check Delivery with service (receiver)
        mockMvc.perform(get("/logistics/delivery/"+delivery.getId()).headers(authHeadersFor(receiverPerson)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(delivery.getId()))
                .andExpect(jsonPath("$.currentStatus.status").value(ClientDeliveryStatus.ORDERED.toString()))
                .andExpect(jsonPath("$.receiver.person.id").value(receiverPerson.getId()))
                .andExpect(jsonPath("$.sender.shop.id").value(senderShopId))
                .andExpect(jsonPath("$.trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$.deliveryAddress.address.name").value(expectedAddress.getName()))
                .andExpect(jsonPath("$.deliveryAddress.address.street").value(expectedAddress.getStreet()))
                .andExpect(jsonPath("$.deliveryAddress.address.zip").value(expectedAddress.getZip()))
                .andExpect(jsonPath("$.deliveryAddress.address.city").value(expectedAddress.getCity()));

        return shopOrderId;
    }

    protected Delivery purchaseOrderReadyForTransport(String shopOrderId) throws Exception {

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                ClientPurchaseOrderReadyForTransportRequest.builder()
                        .shopOrderId(shopOrderId)
                        .size(ClientDimension.builder()
                                .length(40.0)
                                .width(30.0)
                                .height(20.0)
                                .category(DimensionCategory.M)
                                .weight(0.7)
                                .build())
                        .contentNotes("contentNotes")
                        .packagingType(PackagingType.PARCEL)
                        .numParcels(1)
                        .build();

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientPurchaseOrderReadyForTransportConfirmation purchaseOrderReadyForTransportConfirmation = toObject(
                response.getResponse(), ClientPurchaseOrderReadyForTransportConfirmation.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        // Check created delivery
        Delivery delivery = th.deliveryRepository.findAll()
                .stream()
                .filter(d -> d.getTrackingCodeLabelURL() != null
                        && Objects.equals(d.getTrackingCodeLabelURL(), purchaseOrderReadyForTransportConfirmation.getTrackingCodeLabelURL()))
                .findFirst()
                .orElse(null);

        assertNotNull(delivery);

        assertFalse(StringUtils.isEmpty(delivery.getTrackingCodeLabelURL()), "TrackingCodeLabel not created");
        assertFalse(StringUtils.isEmpty(delivery.getTrackingCodeLabelA4URL()), "TrackingCodeLabel not created");

        assertNotNull(purchaseOrderReadyForTransportConfirmation);
        assertEquals(delivery.getTrackingCodeLabelURL(),
                purchaseOrderReadyForTransportConfirmation.getTrackingCodeLabelURL(),
                "TrackingCodeLabel");
        assertEquals(delivery.getTrackingCodeLabelA4URL(),
                purchaseOrderReadyForTransportConfirmation.getTrackingCodeLabelA4URL(),
                "TrackingCodeLabel");

        return delivery;
    }

    protected TransportAssignment transportAlternativeSelectRequest(
            Delivery delivery,
            TransportAlternative transportAlternative,
            Person carrier) throws Exception {

        TransportAlternativeBundle transportAlternativeBundle = transportAlternative.getTransportAlternativeBundle();

        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest =
                new ClientTransportAlternativeSelectRequest(
                        transportAlternative.getId());

        MvcResult response = mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();
        ClientTransportAlternativeSelectResponse transportAlternativeSelectResponse = toObject(response.getResponse(),
                ClientTransportAlternativeSelectResponse.class);

        // in some cases the event processing is not yet finished, so as a workaround we wait
        waitForEventProcessing();

        List<TransportAssignment> transportAssignments =
                th.transportAssignmentRepository.findAll(Sort.by(Direction.DESC, "created"));

        assertEquals(1, transportAssignments.size(), "Amount of transportAssignments");

        TransportAssignment transportAssignment = transportAssignments.get(0);

        assertTrue(transportAlternativeSelectResponse.isAccepted(), "Accepted");
        assertEquals(transportAlternativeBundle.getId(),
                transportAlternativeSelectResponse.getTransportAlternativeBundleToRemoveId(),
                "TransportAlternativeBundleToRemove");
        assertEquals(transportAlternative.getId(), transportAlternativeSelectResponse.getTransportAlternativeId(),
                "TransportAlternative");
        assertEquals(transportAssignment.getId(), transportAlternativeSelectResponse.getTransportAssignmentToLoadId(),
                "TransportAssignmentToLoad");

        // --> Check TransportAssignment (carrier)
        mockMvc.perform(
                        get("/logistics/transport")
                                .headers(authHeadersFor(carrier))
                                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddress.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddress.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddress.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddress.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.ACCEPTED.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));

        // --> Check TransportAlternativeBundle not available (any person in
        // community)
        mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(th.personRegularAnna))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());

        return transportAssignment;
    }

    protected TransportAssignment transportAlternativeSelectRequestFirstStep(
            String transportAlternativeId,
            Person carrierThisStep,
            Address poolingStationAddress) throws Exception {

        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest = new ClientTransportAlternativeSelectRequest(
                transportAlternativeId);

        MvcResult response = mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrierThisStep))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientTransportAlternativeSelectResponse transportAlternativeSelectResponse = toObject(response.getResponse(),
                ClientTransportAlternativeSelectResponse.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        List<TransportAssignment> transportAssignments = th.transportAssignmentRepository.findAll();

        assertEquals(1, transportAssignments.size(), "Amount of transportAssignments");
        TransportAssignment transportAssignment = transportAssignments.get(0);

        assertTrue(transportAlternativeSelectResponse.isAccepted(), "Accepted");
        assertEquals(transportAlternativeId, transportAlternativeSelectResponse.getTransportAlternativeId(),
                "TransportAlternative");
        assertEquals(transportAssignment.getId(), transportAlternativeSelectResponse.getTransportAssignmentToLoadId(),
                "TransportAssignmentToLoad");

        // --> Check TransportAssignment (carrier)
        response = mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrierThisStep))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk()).andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1))).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverId))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andReturn();

        ClientTransportList transportList = toObject(response.getResponse(), ClientTransportList.class);

        ClientTransport clientTransport1 = transportList.get(0);

        assertEquals(poolingStationAddress.getName(), clientTransport1.getDeliveryAddress().getAddress().getName());

        return transportAssignment;
    }

    protected TransportAssignment transportAlternativeSelectRequestLastStep(String transportAlternativeId,
            Person carrierThisStep, Address poolingStationAddress) throws Exception {

        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest =
                new ClientTransportAlternativeSelectRequest(
                        transportAlternativeId);

        MvcResult response = mockMvc
                .perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrierThisStep)).contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
                .andExpect(status().isOk()).andExpect(content().contentType(contentType)).andReturn();
        ClientTransportAlternativeSelectResponse transportAlternativeSelectResponse = toObject(response.getResponse(),
                ClientTransportAlternativeSelectResponse.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        assertTrue(transportAlternativeSelectResponse.isAccepted(), "Accepted");

        assertEquals(transportAlternativeId, transportAlternativeSelectResponse.getTransportAlternativeId(),
                "TransportAlternative");

        final TransportAssignment transportAssignment = th.transportAssignmentRepository.
                findById(transportAlternativeSelectResponse.getTransportAssignmentToLoadId()).get();
        assertEquals(transportAssignment.getId(), transportAlternativeSelectResponse.getTransportAssignmentToLoadId(),
                "TransportAssignmentToLoad");

        // --> Check TransportAssignment (carrier)
        response = mockMvc
                .perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrierThisStep))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1))).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignment.getId()))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverId))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id)).andReturn();

        ClientTransportList transportList = toObject(response.getResponse(), ClientTransportList.class);

        ClientTransport clientTransport1 = transportList.get(0);

        assertEquals(poolingStationAddress.getName(),
                clientTransport1.getDeliveryAddress().getAddress().getName());

        return transportAssignment;
    }

    protected void transportPickupRequest(
            Person carrier,
            String transportAssignmentId,
            Address transportDeliveryAddress,
            Person receiver,
            String deliveryId,
            Address deliveryDeliveryAddress,
            String deliveryTrackingCode) throws Exception {
        transportPickupRequest(
                carrier,
                transportAssignmentId,
                toClientAddressDefinition(transportDeliveryAddress),
                receiver,
                deliveryId,
                toClientAddressDefinition(deliveryDeliveryAddress),
                deliveryTrackingCode);
    }

    /**
     * Pickup of a delivery that is directly delivered to a {@link PoolingStation}
     */
    protected void transportPickupRequest(
            Person carrier,
            String transportAssignmentId,
            ClientAddressDefinition transportDeliveryAddress,
            Person receiver,
            String deliveryId,
            ClientAddressDefinition deliveryDeliveryAddress,
            String deliveryTrackingCode) throws Exception {

        ClientTransportPickupRequest transportPickupRequest = new ClientTransportPickupRequest(deliveryTrackingCode, transportAssignmentId);

        MvcResult response = mockMvc.perform(post("/logistics/event/transportPickupRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportPickupRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientTransportPickupResponse transportPickupResponse = toObject(response.getResponse(), ClientTransportPickupResponse.class);

        assertEquals(transportAssignmentId, transportPickupResponse.getTransportAssignmentId(),
                "TransportAssignmentId");

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        // --> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrier))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAssignmentId))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(transportDeliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(transportDeliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(transportDeliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(transportDeliveryAddress.getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddress.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddress.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddress.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddress.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.PICKED_UP.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode));

        // --> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
                        .headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(deliveryId))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.IN_DELIVERY.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryDeliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryDeliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryDeliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryDeliveryAddress.getCity()));
    }

    protected void transportPickupPoolingStationRequest(
            String transportAssignmentId,
            String deliveryTrackingCode) throws Exception {

        ClientTransportPickupPoolingStationRequest pickupRequest = new ClientTransportPickupPoolingStationRequest(
                transportAssignmentId,
                intermediatePoolingStation.getId(),
                deliveryTrackingCode);

        MvcResult response = mockMvc.perform(post("/logistics/event/transportPickupPoolingStationRequest")
                        .headers(authHeadersFor(carrierLastStep))
                        .contentType(contentType)
                        .content(json(pickupRequest)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andReturn();

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        ClientTransportPickupResponse pickupResponse = toObject(response.getResponse(),
                ClientTransportPickupResponse.class);

        assertEquals(transportAssignmentId, pickupResponse.getTransportAssignmentId(), "TransportAssignmentId");
    }

    protected void transportPoolingStationBoxReadyForAllocationRequest(
            String poolingStationId,
            String deliveryTrackingCode,
            String transportAssignmentId,
            Person carrier,
            PoolingStationBox poolingStationBox) throws Exception {

        ClientTransportPoolingStationBoxReadyForAllocationRequest transportRequest = new ClientTransportPoolingStationBoxReadyForAllocationRequest(
                poolingStationId, deliveryTrackingCode, transportAssignmentId);

        MvcResult response = mockMvc
                .perform(post("/logistics/event/transportPoolingStationBoxReadyForAllocationRequest")
                        .headers(authHeadersFor(carrier)).contentType(contentType)
                        .content(json(transportRequest)))
                .andExpect(status().isOk()).andReturn();

        ClientPoolingStationBoxReadyForAllocationResponse poolingStationBoxResponse = toObject(response.getResponse(),
                ClientPoolingStationBoxReadyForAllocationResponse.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        assertEquals(poolingStationBox.getId(), poolingStationBoxResponse.getPoolingStationBoxId(),
                "PoolingStationBoxId");
        final PoolingStationBox boxToCompare =
                th.poolingStationBoxRepository.findById(poolingStationBoxResponse.getPoolingStationBoxId()).get();
        assertEquals(poolingStationId, boxToCompare.getPoolingStation().getId(),
                "Pooling station of pooling station box");
        assertEquals(boxToCompare.getName(), poolingStationBoxResponse.getPoolingStationBoxName(),
                "PoolingStationBoxName");
        assertEquals(boxToCompare.getConfiguration().getBoxType(), poolingStationBoxResponse.getPoolingStationBoxType(),
                "PoolingStationBoxTye");
        assertEquals(boxToCompare.getConfiguration().getTimeoutForDoorOpen(), poolingStationBoxResponse.getTimeoutForDoorOpen(),
                "PoolingStationBoxTimeoutForDoorOpen");

    }

    protected void transportPickupPoolingStationBoxReadyForDeallocationRequest(
            String deliveryId,
            String transportAssignmentId,
            PoolingStation poolingStation,
            PoolingStationBox poolingStationBox,
            Person carrierLastStep) throws Exception {

        ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest deallocationRequest =
                new ClientTransportPickupPoolingStationBoxReadyForDeallocationRequest(
                        transportAssignmentId, poolingStation.getId());

        MvcResult response =
                mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxReadyForDeallocationRequest")
                                .headers(authHeadersFor(carrierLastStep))
                                .contentType(contentType)
                                .content(json(deallocationRequest)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn();

        ClientPoolingStationBoxReadyForDeallocationResponse deallocationResponse = toObject(response.getResponse(),
                ClientPoolingStationBoxReadyForDeallocationResponse.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        assertEquals(deliveryId, deallocationResponse.getDeliveryId(), "DeliveryId");
        assertEquals(poolingStationBox.getId(), deallocationResponse.getPoolingStationBoxId(), "PoolingStationBoxId");
        assertEquals(PoolingStationBoxType.AUTOMATIC, deallocationResponse.getPoolingStationBoxType(),
                "PoolingStationBoxType");
        assertEquals(poolingStationBox.getConfiguration().getTimeoutForDoorOpen(), deallocationResponse.getTimeoutForDoorOpen(),
                "TimeoutForDoorOpen");
    }

    protected ClientTransportAlternative transportDeliveredPoolingStationRequestIntermediate(
            String intermediatePoolingStationId,
            String deliveryTrackingCode,
            String transportAssignmentId,
            Person carrierFirstStep,
            String deliveryPoolingStationId,
            String deliveryId) throws Exception {

        ClientTransportDeliveredPoolingStationRequest transportDeliveredPoolingStationRequest =
                new ClientTransportDeliveredPoolingStationRequest(
                        intermediatePoolingStationId,
                        deliveryTrackingCode,
                        transportAssignmentId);

        MvcResult response = mockMvc.perform(post("/logistics/event/transportDeliveredPoolingStationRequest")
                        .headers(authHeadersFor(carrierFirstStep))
                        .contentType(contentType)
                        .content(json(transportDeliveredPoolingStationRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientTransportDeliveredResponse transportResponse = toObject(response.getResponse(),
                ClientTransportDeliveredResponse.class);

        assertEquals(deliveryTrackingCode, transportResponse.getDeliveryTrackingCode(), "DeliveryTrackingCode");
        assertEquals(transportAssignmentId, transportResponse.getTransportAssignmentId(), "TransportAssignmentId");

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        assertEquals(2, th.transportAlternativeBundleRepository.count(), "Amount of transportAlternativeBundles");

        // Delivery to delivery address, nothing between
        assertEquals(3, th.transportAlternativeRepository.count(), "Amount of transportAlternatives");

        // --> Check TransportAssignment (status) with service (carrier)
        response = mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrierFirstStep))
                        .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientTransportList transportList = toObject(response.getResponse(), ClientTransportList.class);

        assertEquals(2, transportList.size(), "2 Transports");

        ClientTransport clientTransport1 = transportList.get(0);

        assertEquals(ClientTransportStatus.DELIVERED, clientTransport1.getCurrentStatus().getStatus());
        assertEquals(deliveryId, clientTransport1.getDeliveryId());
        assertEquals(ClientTransportType.TRANSPORT_ASSIGNMENT, clientTransport1.getTransportType());
        assertEquals(pickupAddress.getName(), clientTransport1.getPickupAddress().getAddress().getName());

        ClientTransport clientTransport2 = transportList.get(1);

        assertEquals(ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE, clientTransport2.getTransportType());
        assertNull(clientTransport2.getDeliveryId());
        assertEquals(ClientTransportStatus.AVAILABLE, clientTransport2.getCurrentStatus().getStatus());
        assertEquals(sender1Id, clientTransport2.getSender().getShop().getId());

        assertEquals(receiverId, clientTransport2.getReceiver().getPerson().getId());

        assertEquals(deliveryPoolingStationAddress.getName(),
                clientTransport2.getDeliveryAddress().getAddress().getName());
        assertEquals(intermediatePoolingStationAddress.getName(),
                clientTransport2.getPickupAddress().getAddress().getName());
        assertEquals(deliveryPoolingStationId,
                clientTransport2.getDeliveryAddress().getPoolingStation().getId());

        List<ClientTransportAlternative> clientTransportAlternatives = clientTransport2.getTransportAlternatives();

        assertEquals(1, clientTransportAlternatives.size(), "1 TransportAlternatives");

        ClientTransportAlternative clientTransportAlternative = clientTransportAlternatives.get(0);

        assertEquals(TransportKind.RECEIVER, clientTransportAlternative.getKind());
        assertEquals(deliveryPoolingStation.getId(),
                clientTransportAlternative.getDeliveryAddress().getPoolingStation().getId());

        // --> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/").headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk()).andExpect(jsonPath("$[0].id").value(deliveryId))
                .andExpect(jsonPath("$[0].currentStatus.status").value(
                        ClientDeliveryStatus.IN_DELIVERY_POOLING_STATION.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverId))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(
                        deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryPoolingStationAddress.getZip()))
                .andExpect(
                        jsonPath("$[0].deliveryAddress.address.city").value(deliveryPoolingStationAddress.getCity()));

        return clientTransportAlternative;
    }

    protected void transportDeliveredPoolingStationRequestFinal(
            PoolingStation poolingStation,
            String deliveryTrackingCode,
            String transportAssignmentId,
            String deliveryId,
            Person carrier,
            Address pickupAddressTransport) throws Exception {
        
        transportDeliveredPoolingStationRequestFinal(
                poolingStation,
                deliveryTrackingCode,
                transportAssignmentId,
                deliveryId,
                carrier,
                toClientAddressDefinition(pickupAddressTransport));
    }

    protected void transportDeliveredPoolingStationRequestFinal(
            PoolingStation poolingStation,
            String deliveryTrackingCode,
            String transportAssignmentId,
            String deliveryId,
            Person carrier,
            ClientAddressDefinition pickupAddressTransport) throws Exception {

        ClientTransportDeliveredPoolingStationRequest transportDeliveredPoolingStationRequest =
                new ClientTransportDeliveredPoolingStationRequest(
                        poolingStation.getId(),
                        deliveryTrackingCode,
                        transportAssignmentId);

        MvcResult response = mockMvc.perform(post("/logistics/event/transportDeliveredPoolingStationRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportDeliveredPoolingStationRequest)))
                .andExpect(status().isOk())
                .andReturn();

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        ClientTransportDeliveredResponse transportResponse = toObject(response.getResponse(),
                ClientTransportDeliveredResponse.class);

        assertEquals(deliveryTrackingCode, transportResponse.getDeliveryTrackingCode(), "DeliveryTrackingCode");
        assertEquals(transportAssignmentId, transportResponse.getTransportAssignmentId(),
                "TransportAssignmentId");

        // --> Check TransportAssignment (status) with service (carrier)
        mockMvc.perform(
                        get("/logistics/transport")
                                .headers(authHeadersFor(carrier))
                                .param("communityId", tenant1Id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(transportAssignmentId))
                .andExpect(jsonPath("$[0].transportType").value(ClientTransportType.TRANSPORT_ASSIGNMENT.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(poolingStation.getAddress().getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(poolingStation.getAddress().getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(poolingStation.getAddress().getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(poolingStation.getAddress().getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddressTransport.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddressTransport.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddressTransport.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddressTransport.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.DELIVERED.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode));

        // --> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
                        .headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(deliveryId))
                .andExpect(jsonPath("$[0].currentStatus.status").value(
                        ClientDeliveryStatus.DELIVERED_POOLING_STATION.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(poolingStation.getAddress().getName()))
                .andExpect(
                        jsonPath("$[0].deliveryAddress.address.street").value(poolingStation.getAddress().getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(poolingStation.getAddress().getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(poolingStation.getAddress().getCity()));
    }

    protected void receiverPickupPoolingStationBoxReadyForDeallocationRequest(
            String deliveryId,
            String poolingStationId,
            PoolingStationBox poolingStationBox) throws Exception {

        ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest deallocationRequest =
                new ClientReceiverPickupPoolingStationBoxReadyForDeallocationRequest(
                        deliveryId, poolingStationId);

        MvcResult response = mockMvc.perform(
                        post("/logistics/event/receiverPickupPoolingStationBoxReadyForDeallocationRequest")
                                .headers(authHeadersFor(receiver))
                                .contentType(contentType)
                                .content(json(deallocationRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientPoolingStationBoxReadyForDeallocationResponse deallocationResponse = toObject(response.getResponse(),
                ClientPoolingStationBoxReadyForDeallocationResponse.class);

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        assertEquals(poolingStationBox.getId(), deallocationResponse.getPoolingStationBoxId(), "PoolingStationBoxId");
        assertEquals(deliveryId, deallocationResponse.getDeliveryId(), "deliveryId");
        final PoolingStationBox boxToCompare =
                th.poolingStationBoxRepository.findById(deallocationResponse.getPoolingStationBoxId()).get();
        assertEquals(poolingStationId, boxToCompare.getPoolingStation().getId(),
                "Pooling station of pooling station box");
        assertEquals(boxToCompare.getName(), deallocationResponse.getPoolingStationBoxName(), "PoolingStationBoxName");
        assertEquals(boxToCompare.getConfiguration().getBoxType(), deallocationResponse.getPoolingStationBoxType(),
                "PoolingStationBoxTye");
        assertEquals(boxToCompare.getConfiguration().getTimeoutForDoorOpen(), deallocationResponse.getTimeoutForDoorOpen(),
                "PoolingStationBoxTimeoutForDoorOpen");

    }

    protected void receiverPickupReceivedPoolingStationRequest(String poolingStationId, String deliveryTrackingCode,
            String deliveryId, Address deliveryPoolingStationAddress) throws Exception {

        ClientReceiverPickupReceivedPoolingStationRequest pickupRequest = new ClientReceiverPickupReceivedPoolingStationRequest(
                poolingStationId, deliveryTrackingCode, deliveryId);

        MvcResult response = mockMvc.perform(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(pickupRequest)))
                .andExpect(status().isOk())
                .andReturn();

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        ClientReceiverPickupReceivedResponse receivedResponse = toObject(response.getResponse(),
                ClientReceiverPickupReceivedResponse.class);

        assertEquals(deliveryTrackingCode, receivedResponse.getDeliveryTrackingCode(), "DeliveryTrackingCode");

        // --> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/").headers(authHeadersFor(receiver)))
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk()).andExpect(jsonPath("$[0].id").value(deliveryId))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.RECEIVED.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].trackingCode").value(deliveryTrackingCode))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryPoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryPoolingStationAddress.getCity()));
    }

    protected TransportAlternative checkDeliveryAndTransportToPoolingStation() throws Exception {
        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        // --> Check TransportAlternativeBundle and TransportAlternative with
        // service (any person in community)
        List<TransportAlternativeBundle> transportAlternativeBundles = th.transportAlternativeBundleRepository
                .findAll();

        assertEquals(1, transportAlternativeBundles.size(), "Amount of transportAlternativeBundles");

        TransportAlternativeBundle transportAlternativeBundle = transportAlternativeBundles.get(0);

        assertEquals(TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT,
                th.transportAlternativeBundleStatusRecordRepository.findFirstByTransportAlternativeBundleOrderByTimestampDesc(
                        transportAlternativeBundle).getStatus());

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();

        // Delivery to pooling station, nothing between
        assertEquals(1, transportAlternatives.size(), "Amount of transportAlternatives");

        TransportAlternative transportAlternativeToDeliveryPS = transportAlternatives.stream()
                .filter(ta -> Objects.equals(ta.getDeliveryAddress().getPoolingStation().getId(),
                        deliveryPoolingStation.getId()))
                .findFirst().orElse(null);

        assertNotNull(transportAlternativeToDeliveryPS);

        mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrierFirstStep))
                        .param("communityId", tenant1Id))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(transportAlternativeBundle.getId()))
                .andExpect(jsonPath("$[0].transportType").value(
                        ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverId))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(
                        deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryPoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryPoolingStationAddress.getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddress.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddress.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddress.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddress.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$[0].transportAlternatives", hasSize(1)))

                .andExpect(jsonPath("$[0].transportAlternatives[0].id").value(transportAlternativeToDeliveryPS.getId()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].kind").value(TransportKind.RECEIVER.toString()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.name").value(deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.street").value(deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.zip").value(deliveryPoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.city").value(deliveryPoolingStationAddress.getCity()));

        return transportAlternativeToDeliveryPS;
    }

    protected TransportAlternative checkDeliveryAndTransportToPoolingStationViaIntermediatePoolingStation() throws Exception {

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        // --> Check TransportAlternativeBundle and TransportAlternative with
        // service (any person in community)
        List<TransportAlternativeBundle> transportAlternativeBundles = th.transportAlternativeBundleRepository
                .findAll();

        assertEquals(1, transportAlternativeBundles.size(), "Amount of transportAlternativeBundles");

        TransportAlternativeBundle transportAlternativeBundle = transportAlternativeBundles.get(0);

        assertEquals(TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT,
                th.transportAlternativeBundleStatusRecordRepository.findFirstByTransportAlternativeBundleOrderByTimestampDesc(
                        transportAlternativeBundle).getStatus());

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();

        assertEquals(2, transportAlternatives.size(), "Amount of transportAlternatives");

        TransportAlternative transportAlternativeToIntermediatePS = transportAlternatives.stream()
                .filter(ta -> Objects.equals(ta.getDeliveryAddress().getPoolingStation().getId(), intermediatePoolingStation.getId()))
                .findFirst().orElse(null);

        assertNotNull(transportAlternativeToIntermediatePS);

        TransportAlternative transportAlternativeToDeliveryPS = transportAlternatives.stream()
                .filter(ta -> Objects.equals(ta.getDeliveryAddress().getPoolingStation().getId(), deliveryPoolingStation.getId()))
                .findFirst().orElse(null);

        assertNotNull(transportAlternativeToDeliveryPS);

        mockMvc.perform(get("/logistics/transport")
                        .headers(authHeadersFor(carrierFirstStep))
                        .param("communityId", tenant1Id))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))

                .andExpect(jsonPath("$[0].id").value(transportAlternativeBundle.getId()))
                .andExpect(jsonPath("$[0].transportType").value(
                        ClientTransportType.TRANSPORT_ALTERNATIVE_BUNDLE.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverId))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1Id))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(
                        deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryPoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryPoolingStationAddress.getCity()))
                .andExpect(jsonPath("$[0].pickupAddress.address.name").value(pickupAddress.getName()))
                .andExpect(jsonPath("$[0].pickupAddress.address.street").value(pickupAddress.getStreet()))
                .andExpect(jsonPath("$[0].pickupAddress.address.zip").value(pickupAddress.getZip()))
                .andExpect(jsonPath("$[0].pickupAddress.address.city").value(pickupAddress.getCity()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))
                .andExpect(jsonPath("$[0].transportAlternatives", hasSize(2)))

                .andExpect(jsonPath("$[0].transportAlternatives[0].id").value(transportAlternativeToDeliveryPS.getId()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].kind").value(TransportKind.RECEIVER.toString()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.name").value(
                        deliveryPoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.street").value(
                        deliveryPoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.zip").value(
                        deliveryPoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].transportAlternatives[0].deliveryAddress.address.city").value(
                        deliveryPoolingStationAddress.getCity()))

                .andExpect(jsonPath("$[0].transportAlternatives[1].id").value(
                        transportAlternativeToIntermediatePS.getId()))
                .andExpect(jsonPath("$[0].transportAlternatives[1].kind").value(TransportKind.INTERMEDIATE.toString()))
                .andExpect(jsonPath("$[0].transportAlternatives[1].deliveryAddress.address.name").value(
                        intermediatePoolingStationAddress.getName()))
                .andExpect(jsonPath("$[0].transportAlternatives[1].deliveryAddress.address.street").value(
                        intermediatePoolingStationAddress.getStreet()))
                .andExpect(jsonPath("$[0].transportAlternatives[1].deliveryAddress.address.zip").value(
                        intermediatePoolingStationAddress.getZip()))
                .andExpect(jsonPath("$[0].transportAlternatives[1].deliveryAddress.address.city").value(
                        intermediatePoolingStationAddress.getCity()));

        return transportAlternativeToIntermediatePS;
    }

    private static class ClientTransportList extends ArrayList<ClientTransport> {

        private static final long serialVersionUID = 1L;

    }

    protected ClientAddressDefinition toClientAddressDefinition(Address address) {
        return ClientAddressDefinition.builder()
                .name(address.getName())
                .street(address.getStreet())
                .zip(address.getZip())
                .city(address.getCity())
                .gpsLocation(address.getGpsLocation() == null ? null :
                        new ClientGPSLocation(address.getGpsLocation().getLatitude(),
                                address.getGpsLocation().getLongitude()))
                .build();
    }

}
