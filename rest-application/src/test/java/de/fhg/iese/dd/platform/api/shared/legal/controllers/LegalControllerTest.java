/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.legal.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.legal.clientevent.ClientLegalTextAcceptRequest;
import de.fhg.iese.dd.platform.api.shared.legal.clientevent.ClientLegalTextAcceptResponse;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalText;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextAcceptance;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextStatus;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

public class LegalControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createLegalEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getCurrentLegalTexts() throws Exception {
        AppVariant appVariant = th.app1Variant1;

        assertTrue(th.legalTextRepository.findAll().stream()
                .anyMatch(l -> appVariant.equals(l.getAppVariant())));

        //unauthorized
        mockMvc.perform(get("/legal/current")
                .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Arrays.asList(
                        toClientLegalText(th.legalTextPlatformRequired),
                        toClientLegalText(th.legalTextPlatformOptional),
                        toClientLegalText(th.legalText1App1Variant1Required),
                        toClientLegalText(th.legalText2App1Variant1Optional))));

        //authorized
        mockMvc.perform(get("/legal/current")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Arrays.asList(
                        toClientLegalText(th.legalTextPlatformRequired),
                        toClientLegalText(th.legalTextPlatformOptional),
                        toClientLegalText(th.legalText1App1Variant1Required),
                        toClientLegalText(th.legalText2App1Variant1Optional))));
    }

    @Test
    public void getCurrentLegalTexts_Unauthorized() throws Exception {

        //app variant required
        mockMvc.perform(get("/legal/current")
                .headers(headersForAppVariant("invalid-app-variant-identifier")))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
        mockMvc.perform(get("/legal/current")
                .headers(headersForAppVariant("")))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
        mockMvc.perform(get("/legal/current"))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getCurrentLegalTextStatus() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;

        assertTrue(th.legalTextRepository.findAll().stream()
                .anyMatch(l -> appVariant.equals(l.getAppVariant())));

        ClientLegalTextStatus clientLegalTextStatus = ClientLegalTextStatus.builder()
                .id(UUID.nameUUIDFromBytes(
                        (person.getId() + appVariant.getId())
                                .getBytes(StandardCharsets.US_ASCII)).toString())
                .allRequiredAccepted(false)
                .acceptedLegalTexts(Collections.emptyList())
                .openLegalTexts(Arrays.asList(
                        toClientLegalText(th.legalTextPlatformRequired),
                        toClientLegalText(th.legalTextPlatformOptional),
                        toClientLegalText(th.legalText1App1Variant1Required),
                        toClientLegalText(th.legalText2App1Variant1Optional)))
                .build();

        //none accepted
        mockMvc.perform(get("/legal/status")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(clientLegalTextStatus));

        //some accepted, but not all required
        LegalTextAcceptance acceptance2 = acceptLegalInDB(th.legalText2App1Variant1Optional, person, 4711L);

        clientLegalTextStatus = ClientLegalTextStatus.builder()
                .id(UUID.nameUUIDFromBytes(
                        (person.getId() + appVariant.getId())
                                .getBytes(StandardCharsets.US_ASCII)).toString())
                .allRequiredAccepted(false)
                .acceptedLegalTexts(Collections.singletonList(
                        toClientLegalText(th.legalText2App1Variant1Optional, acceptance2)))
                .openLegalTexts(Arrays.asList(
                        toClientLegalText(th.legalTextPlatformRequired),
                        toClientLegalText(th.legalTextPlatformOptional),
                        toClientLegalText(th.legalText1App1Variant1Required)))
                .build();

        mockMvc.perform(get("/legal/status")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(clientLegalTextStatus));

        //all required accepted
        LegalTextAcceptance acceptance1 = acceptLegalInDB(th.legalText1App1Variant1Required, person, 4715L);
        LegalTextAcceptance acceptancePlatform = acceptLegalInDB(th.legalTextPlatformRequired, person, 4719L);

        clientLegalTextStatus = ClientLegalTextStatus.builder()
                .id(UUID.nameUUIDFromBytes(
                        (person.getId() + appVariant.getId())
                                .getBytes(StandardCharsets.US_ASCII)).toString())
                .allRequiredAccepted(true)
                .acceptedLegalTexts(Arrays.asList(
                        toClientLegalText(th.legalTextPlatformRequired, acceptancePlatform),
                        toClientLegalText(th.legalText1App1Variant1Required, acceptance1),
                        toClientLegalText(th.legalText2App1Variant1Optional, acceptance2)))
                .openLegalTexts(Collections.singletonList(
                        toClientLegalText(th.legalTextPlatformOptional)))
                .build();

        mockMvc.perform(get("/legal/status")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(clientLegalTextStatus));
    }

    @Test
    public void getCurrentLegalTextStatus_Unauthorized() throws Exception {

        //unauthorized and app variant required
        assertOAuth2AppVariantRequired(get("/legal/status"), th.app1Variant1, th.personRegularAnna);
    }

    @Test
    public void acceptLegalTextPlatform() throws Exception {

        Person person = th.personRegularAnna;
        AppVariant appVariantWithoutSpecificLegalTexts = th.app1Variant2;

        assertFalse(th.legalTextRepository.findAll().stream()
                .anyMatch(l -> appVariantWithoutSpecificLegalTexts.equals(l.getAppVariant())), "App variant has own legal texts");

        //accept legal text platform
        callAcceptLegalText(Collections.singletonList(th.legalTextPlatformRequired), person, 12021983L,
                12021983L);

        //check that now this acceptance is also available in other app variants
        LegalTextAcceptance acceptance = LegalTextAcceptance.builder()
                .legalText(th.legalTextPlatformRequired)
                .person(person)
                .timestamp(12021983L)
                .build();

        ClientLegalTextStatus clientLegalTextStatus = ClientLegalTextStatus.builder()
                .allRequiredAccepted(true)
                .acceptedLegalTexts(Collections.singletonList(
                        toClientLegalText(th.legalTextPlatformRequired, acceptance)))
                .openLegalTexts(Collections.singletonList(
                        toClientLegalText(th.legalTextPlatformOptional)))
                .build();

        mockMvc.perform(get("/legal/status")
                .headers(authHeadersFor(person, th.app1Variant2)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(clientLegalTextStatus, "id", "acceptedLegalTexts[0].acceptance.id"));
    }

    @Test
    public void acceptLegalTextRequired() throws Exception {

        //accept required legal text
        callAcceptLegalText(Collections.singletonList(th.legalText1App1Variant1Required), th.personRegularAnna,
                12021983L,
                12021983L);

    }

    @Test
    public void acceptLegalTextOptional() throws Exception {

        //accept not required legal text
        callAcceptLegalText(Collections.singletonList(th.legalText2App1Variant1Optional), th.personRegularAnna,
                12021983L,
                12021983L);

    }

    @Test
    public void acceptLegalTextAgain() throws Exception {

        //accept already accepted again -> timestamp not changing
        acceptLegalInDB(th.legalText2App1Variant1Optional, th.personRegularAnna, 4711L);
        callAcceptLegalText(Collections.singletonList(th.legalText2App1Variant1Optional), th.personRegularAnna,
                12021983L, 4711L);

    }

    @Test
    public void acceptMultipleLegalTexts() throws Exception {

        //accept multiple legal texts
        callAcceptLegalText(Arrays.asList(th.legalText1App1Variant1Required, th.legalText2App1Variant1Optional),
                th.personRegularAnna, 12021983L, 12021983L);
    }

    @Test
    public void acceptLegalTextsInvalidParameters() throws Exception {

        //invalid legal text identifier
        String invalidLegalTextIdentifier = "§4711-Lex-Ochs";

        ClientLegalTextAcceptRequest acceptRequest = ClientLegalTextAcceptRequest.builder()
                .legalTextIdentifiersToAccept(List.of(invalidLegalTextIdentifier))
                .build();

        mockMvc.perform(post("/legal/event/legalTextAcceptRequest")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType)
                .content(json(acceptRequest)))
                .andExpect(
                        isException("[" + invalidLegalTextIdentifier + "]", ClientExceptionType.LEGAL_TEXT_NOT_FOUND));

        //unauthorized
        acceptRequest = ClientLegalTextAcceptRequest.builder()
                .legalTextIdentifiersToAccept(
                        Collections.singletonList(th.legalText1App1Variant1Required.getLegalTextIdentifier()))
                .build();

        assertOAuth2(post("/legal/event/legalTextAcceptRequest")
                .contentType(contentType)
                .content(json(acceptRequest)));
    }

    private void callAcceptLegalText(List<LegalText> legalTexts, Person person, long currentTime,
            long expectedAcceptTime) throws Exception {

        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(currentTime).atZone(ZoneOffset.UTC));

        ClientLegalTextAcceptRequest acceptRequest = ClientLegalTextAcceptRequest.builder()
                .legalTextIdentifiersToAccept(legalTexts.stream()
                        .map(LegalText::getLegalTextIdentifier)
                        .collect(Collectors.toList()))
                .build();

        ClientLegalTextAcceptResponse acceptResponse = ClientLegalTextAcceptResponse.builder()
                .acceptedLegalTexts(
                        legalTexts.stream()
                                .map(l -> toClientLegalText(l, LegalTextAcceptance.builder()
                                        .legalText(l)
                                        .person(person)
                                        .timestamp(expectedAcceptTime)
                                        .build()))
                                .collect(Collectors.toList()))
                .build();

        mockMvc.perform(post("/legal/event/legalTextAcceptRequest")
                .headers(authHeadersFor(person))
                .contentType(contentType)
                .content(json(acceptRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(acceptResponse, "acceptedLegalTexts[0].acceptance.id",
                        "acceptedLegalTexts[1].acceptance.id"));

        assertEquals(legalTexts.size(), th.legalTextAcceptanceRepository.count());

        List<LegalTextAcceptance> actualAcceptances = th.legalTextAcceptanceRepository.findAll();

        Map<LegalText, LegalTextAcceptance> legalTextAcceptanceMap = actualAcceptances.stream()
                .collect(Collectors.toMap(LegalTextAcceptance::getLegalText, Function.identity()));

        for (LegalText legalText : legalTexts) {
            LegalTextAcceptance actualAcceptance = legalTextAcceptanceMap.get(legalText);

            assertEquals(legalText, actualAcceptance.getLegalText());
            assertEquals(person, actualAcceptance.getPerson());
            assertEquals(expectedAcceptTime, actualAcceptance.getTimestamp());
        }

        timeServiceMock.resetConstantDateTime();
    }

    private LegalTextAcceptance acceptLegalInDB(LegalText legalText, Person person, long timestamp) {
        return th.legalTextAcceptanceRepository.saveAndFlush(
                LegalTextAcceptance.builder()
                        .legalText(legalText)
                        .person(person)
                        .timestamp(timestamp)
                        .build()
        );
    }

    private ClientLegalText toClientLegalText(LegalText legalText) {
        return toClientLegalText(legalText, null);
    }

    private ClientLegalText toClientLegalText(LegalText legalText, LegalTextAcceptance legalTextAcceptance) {
        return ClientLegalText.builder()
                .id(legalText.getId())
                .legalTextIdentifier(legalText.getLegalTextIdentifier())
                .name(legalText.getName())
                .legalTextType(legalText.getLegalTextType())
                .orderValue(legalText.getOrderValue())
                .required(legalText.isRequired())
                .urlText(legalText.getUrlText())
                .acceptance(toClientLegalTextAcceptance(legalTextAcceptance))
                .build();
    }

    private ClientLegalTextAcceptance toClientLegalTextAcceptance(LegalTextAcceptance legalTextAcceptance) {
        if (legalTextAcceptance == null) {
            return null;
        } else {
            return ClientLegalTextAcceptance.builder()
                    .id(legalTextAcceptance.getId())
                    .timestamp(legalTextAcceptance.getTimestamp())
                    .build();
        }
    }

}
