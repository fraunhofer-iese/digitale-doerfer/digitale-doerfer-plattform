/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationPerson;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class VolunteeringControllerTest extends BaseServiceTest {

    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private OrganizationGeoAreaMappingRepository organizationGeoAreaMappingRepository;
    @Autowired
    private GrapevineTestHelper th;

    private Organization volunteeringFeuerwehr;
    private String volunteeringFeuerwehrFact1Name;
    private String volunteeringFeuerwehrFact1Value;
    private String volunteeringFeuerwehrFact2Name;
    private String volunteeringFeuerwehrFact2Value;

    private Organization volunteeringGartenbau;
    private String volunteeringGartenbauFact1Name;
    private String volunteeringGartenbauFact1Value;
    private String volunteeringGartenbauFact2Name;
    private String volunteeringGartenbauFact2Value;
    private GeoArea geoAreaFeuerwehr;
    private GeoArea geoAreaGartenbau;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        createVolunteerings();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private void createVolunteerings() {

        geoAreaFeuerwehr = th.geoAreaDorf1InEisenberg;
        geoAreaGartenbau = th.geoAreaDorf2InEisenberg;

        volunteeringFeuerwehrFact1Name = "Mitglieder";
        volunteeringFeuerwehrFact1Value = "40";
        volunteeringFeuerwehrFact2Name = "Fahrzeuge";
        volunteeringFeuerwehrFact2Value = "drei";
        volunteeringFeuerwehr = Organization.builder()
                .name("Feuerwehr")
                .locationDescription("Aschehausen")
                .description("Löscht und erfrischt")
                .overviewImage(th.createMediaItem("Feuerwehr_Overview"))
                .logoImage(th.createMediaItem("Feuerwehr_Logo"))
                .detailImages(
                        Arrays.asList(th.createMediaItem("Feuerwehr_Detail1"), th.createMediaItem("Feuerwehr_Detail2")))
                .url("https://www.feuerwehr-aschehausen.com")
                .phone("112")
                .emailAddress("feuer@feuerwehr-aschehausen.com")
                .address(th.addressRepository.saveAndFlush(Address.builder()
                        .name("Feuerwehr Aschehausen")
                        .street("Feuerstraße 1")
                        .zip("67663")
                        .city("Aschehausen")
                        .build()))
                .factsJson("[{" +
                        "\"name\":\"" + volunteeringFeuerwehrFact1Name + "\"," +
                        "\"value\":\"" + volunteeringFeuerwehrFact1Value + "\"" +
                        "},{" +
                        "\"name\":\"" + volunteeringFeuerwehrFact2Name + "\"," +
                        "\"value\":\"" + volunteeringFeuerwehrFact2Value + "\"" +
                        "}]")
                .tag(OrganizationTag.VOLUNTEERING.getBitMaskValue())
                .build();
        volunteeringFeuerwehr.setOrganizationPersons(Arrays.asList(OrganizationPerson.builder()
                        .organization(volunteeringFeuerwehr)
                        .category("Vorstand")
                        .position("Erster Brandmelder")
                        .name("Karl Flamme")
                        .profilePicture(th.createMediaItem("KF"))
                        .build(),
                OrganizationPerson.builder()
                        .organization(volunteeringFeuerwehr)
                        .category("Vorstand")
                        .position("Zweite Brandmelderin")
                        .name("Susi Flamme")
                        .profilePicture(th.createMediaItem("SF"))
                        .build()));

        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(volunteeringFeuerwehr)
                .geoArea(geoAreaFeuerwehr)
                .build()
                .withConstantId());

        volunteeringGartenbauFact1Name = "Mitglieder";
        volunteeringGartenbauFact1Value = "32";
        volunteeringGartenbauFact2Name = "Apfelbäume";
        volunteeringGartenbauFact2Value = "vierhundert";
        volunteeringGartenbau = Organization.builder()
                .name("Gartenbauverein")
                .locationDescription("Grünburg")
                .description("Apfel am eigenen Baum erquickt und belebt")
                .overviewImage(th.createMediaItem("Gartenbau_Overview"))
                .logoImage(th.createMediaItem("Gartenbau_Logo"))
                .detailImages(
                        Arrays.asList(th.createMediaItem("Gartenbau_Detail1"), th.createMediaItem("Gartenbau_Detail2")))
                .url("https://www.gartenbau-grünburg.com")
                .phone("112")
                .emailAddress("feuer@gartenbau-grünburg.com")
                .address(th.addressRepository.saveAndFlush(Address.builder()
                        .name("Gartenbau Grünburg")
                        .street("Apfelstraße 1")
                        .zip("67663")
                        .city("Grünburg")
                        .build()))
                .factsJson("[{" +
                        "\"name\":\"" + volunteeringGartenbauFact1Name + "\"," +
                        "\"value\":\"" + volunteeringGartenbauFact1Value + "\"" +
                        "},{" +
                        "\"name\":\"" + volunteeringGartenbauFact2Name + "\"," +
                        "\"value\":\"" + volunteeringGartenbauFact2Value + "\"" +
                        "}]")
                .tag(OrganizationTag.VOLUNTEERING.getBitMaskValue())
                .build();
        volunteeringGartenbau.setOrganizationPersons(Arrays.asList(
                OrganizationPerson.builder()
                        .organization(volunteeringGartenbau)
                        .category("Vorstand")
                        .position("Geprüfter Baumschneider")
                        .name("Carlo Birne")
                        .profilePicture(th.createMediaItem("CB"))
                        .build(),
                OrganizationPerson.builder()
                        .organization(volunteeringGartenbau)
                        .category("Vorstand")
                        .position("Glyphosat-Experte")
                        .name("Anni Lean")
                        .profilePicture(th.createMediaItem("AL"))
                        .build()));
        volunteeringGartenbau = organizationRepository.save(volunteeringGartenbau);
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(volunteeringGartenbau)
                .geoArea(geoAreaFeuerwehr)
                .build()
                .withConstantId());
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(volunteeringGartenbau)
                .geoArea(geoAreaGartenbau)
                .build()
                .withConstantId());
    }

    @Test
    public void getAllAvailableVolunteerings() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        //no selection
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        //selection of geo area that has one volunteering
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaGartenbau);
        OrganizationPerson volunteeringPerson0 = volunteeringGartenbau.getOrganizationPersons().get(0);
        OrganizationPerson volunteeringPerson1 = volunteeringGartenbau.getOrganizationPersons().get(1);
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(volunteeringGartenbau.getId()))
                .andExpect(jsonPath("$[0].name").value(volunteeringGartenbau.getName()))
                .andExpect(jsonPath("$[0].locationDescription").value(volunteeringGartenbau.getLocationDescription()))
                .andExpect(jsonPath("$[0].description").value(volunteeringGartenbau.getDescription()))
                .andExpect(jsonPath("$[0].overviewImage.id").value(volunteeringGartenbau.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].logoImage.id").value(volunteeringGartenbau.getLogoImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id").value(
                        volunteeringGartenbau.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[0].url").value(volunteeringGartenbau.getUrl()))
                .andExpect(jsonPath("$[0].phone").value(volunteeringGartenbau.getPhone()))
                .andExpect(jsonPath("$[0].emailAddress").value(volunteeringGartenbau.getEmailAddress()))
                .andExpect(jsonPath("$[0].address.id").value(volunteeringGartenbau.getAddress().getId()))
                .andExpect(jsonPath("$[0].address.name").value(volunteeringGartenbau.getAddress().getName()))
                .andExpect(jsonPath("$[0].address.street").value(volunteeringGartenbau.getAddress().getStreet()))
                .andExpect(jsonPath("$[0].address.zip").value(volunteeringGartenbau.getAddress().getZip()))
                .andExpect(jsonPath("$[0].address.city").value(volunteeringGartenbau.getAddress().getCity()))
                .andExpect(jsonPath("$[0].facts[0].name").value(volunteeringGartenbauFact1Name))
                .andExpect(jsonPath("$[0].facts[0].value").value(volunteeringGartenbauFact1Value))
                .andExpect(jsonPath("$[0].facts[1].name").value(volunteeringGartenbauFact2Name))
                .andExpect(jsonPath("$[0].facts[1].value").value(volunteeringGartenbauFact2Value))
                .andExpect(jsonPath("$[0].volunteeringPersons[0].id").value(volunteeringPerson0.getId()))
                .andExpect(jsonPath("$[0].volunteeringPersons[0].category").value(volunteeringPerson0.getCategory()))
                .andExpect(jsonPath("$[0].volunteeringPersons[0].position").value(volunteeringPerson0.getPosition()))
                .andExpect(jsonPath("$[0].volunteeringPersons[0].name").value(volunteeringPerson0.getName()))
                .andExpect(jsonPath("$[0].volunteeringPersons[0].profilePicture.id").value(
                        volunteeringPerson0.getProfilePicture().getId()))
                .andExpect(jsonPath("$[0].volunteeringPersons[1].id").value(volunteeringPerson1.getId()))
                .andExpect(jsonPath("$[0].volunteeringPersons[1].category").value(volunteeringPerson1.getCategory()))
                .andExpect(jsonPath("$[0].volunteeringPersons[1].position").value(volunteeringPerson1.getPosition()))
                .andExpect(jsonPath("$[0].volunteeringPersons[1].name").value(volunteeringPerson1.getName()))
                .andExpect(jsonPath("$[0].volunteeringPersons[1].profilePicture.id").value(
                        volunteeringPerson1.getProfilePicture().getId()));

        //selection of additional geo area that has both volunteerings
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaFeuerwehr);
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(volunteeringFeuerwehr.getId()))
                .andExpect(jsonPath("$[0].name").value(volunteeringFeuerwehr.getName()))
                .andExpect(jsonPath("$[0].description").value(volunteeringFeuerwehr.getDescription()))
                .andExpect(jsonPath("$[0].url").value(volunteeringFeuerwehr.getUrl()))
                .andExpect(jsonPath("$[0].overviewImage.id").value(volunteeringFeuerwehr.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id").value(
                        volunteeringFeuerwehr.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[0].detailImages[1].id").value(
                        volunteeringFeuerwehr.getDetailImages().get(1).getId()))
                .andExpect(jsonPath("$[1].id").value(volunteeringGartenbau.getId()))
                .andExpect(jsonPath("$[1].name").value(volunteeringGartenbau.getName()))
                .andExpect(jsonPath("$[1].description").value(volunteeringGartenbau.getDescription()))
                .andExpect(jsonPath("$[1].url").value(volunteeringGartenbau.getUrl()))
                .andExpect(jsonPath("$[1].overviewImage.id").value(volunteeringGartenbau.getOverviewImage().getId()))
                .andExpect(jsonPath("$[1].detailImages[0].id").value(
                        volunteeringGartenbau.getDetailImages().get(0).getId()));
    }

    @Test
    public void getAllAvailableVolunteerings_Deleted() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        volunteeringFeuerwehr.setDeleted(true);
        volunteeringFeuerwehr.setDeletionTime(12021983L);
        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);

        //selection of geo area that has both volunteerings, but one is deleted
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaFeuerwehr);
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(volunteeringGartenbau.getId()));
    }

    @Test
    public void getAllAvailableVolunteerings_TagFilter() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        volunteeringFeuerwehr.getTags().setValues(EnumSet.of(OrganizationTag.EMERGENCY_AID));
        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);

        //selection of geo area that has both volunteerings, but one is no volunteering
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaFeuerwehr);
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(volunteeringGartenbau.getId()));

        volunteeringFeuerwehr.getTags().addValue(OrganizationTag.VOLUNTEERING);
        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);

        //both now have the volunteering-tag
        mockMvc.perform(get("/grapevine/volunteering")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(volunteeringFeuerwehr.getId()))
                .andExpect(jsonPath("$[1].id").value(volunteeringGartenbau.getId()));
    }

    @Test
    public void getAllAvailableVolunteerings_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/volunteering"),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

    @Test
    public void getVolunteeringById() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        //geo area selection at app variant is not required
        th.appVariantUsageAreaSelectionRepository.deleteAll();

        OrganizationPerson volunteeringPerson0 = volunteeringFeuerwehr.getOrganizationPersons().get(0);
        OrganizationPerson volunteeringPerson1 = volunteeringFeuerwehr.getOrganizationPersons().get(1);
        mockMvc.perform(get("/grapevine/volunteering/{volunteeringId}", volunteeringFeuerwehr.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(volunteeringFeuerwehr.getId()))
                .andExpect(jsonPath("$.name").value(volunteeringFeuerwehr.getName()))
                .andExpect(jsonPath("$.locationDescription").value(volunteeringFeuerwehr.getLocationDescription()))
                .andExpect(jsonPath("$.description").value(volunteeringFeuerwehr.getDescription()))
                .andExpect(jsonPath("$.overviewImage.id").value(volunteeringFeuerwehr.getOverviewImage().getId()))
                .andExpect(jsonPath("$.logoImage.id").value(volunteeringFeuerwehr.getLogoImage().getId()))
                .andExpect(jsonPath("$.detailImages[0].id").value(
                        volunteeringFeuerwehr.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$.url").value(volunteeringFeuerwehr.getUrl()))
                .andExpect(jsonPath("$.phone").value(volunteeringFeuerwehr.getPhone()))
                .andExpect(jsonPath("$.emailAddress").value(volunteeringFeuerwehr.getEmailAddress()))
                .andExpect(jsonPath("$.address.id").value(volunteeringFeuerwehr.getAddress().getId()))
                .andExpect(jsonPath("$.address.name").value(volunteeringFeuerwehr.getAddress().getName()))
                .andExpect(jsonPath("$.address.street").value(volunteeringFeuerwehr.getAddress().getStreet()))
                .andExpect(jsonPath("$.address.zip").value(volunteeringFeuerwehr.getAddress().getZip()))
                .andExpect(jsonPath("$.address.city").value(volunteeringFeuerwehr.getAddress().getCity()))
                .andExpect(jsonPath("$.facts[0].name").value(volunteeringFeuerwehrFact1Name))
                .andExpect(jsonPath("$.facts[0].value").value(volunteeringFeuerwehrFact1Value))
                .andExpect(jsonPath("$.facts[1].name").value(volunteeringFeuerwehrFact2Name))
                .andExpect(jsonPath("$.facts[1].value").value(volunteeringFeuerwehrFact2Value))
                .andExpect(jsonPath("$.volunteeringPersons[0].id").value(volunteeringPerson0.getId()))
                .andExpect(jsonPath("$.volunteeringPersons[0].category").value(volunteeringPerson0.getCategory()))
                .andExpect(jsonPath("$.volunteeringPersons[0].position").value(volunteeringPerson0.getPosition()))
                .andExpect(jsonPath("$.volunteeringPersons[0].name").value(volunteeringPerson0.getName()))
                .andExpect(jsonPath("$.volunteeringPersons[0].profilePicture.id").value(
                        volunteeringPerson0.getProfilePicture().getId()))
                .andExpect(jsonPath("$.volunteeringPersons[1].id").value(volunteeringPerson1.getId()))
                .andExpect(jsonPath("$.volunteeringPersons[1].category").value(volunteeringPerson1.getCategory()))
                .andExpect(jsonPath("$.volunteeringPersons[1].position").value(volunteeringPerson1.getPosition()))
                .andExpect(jsonPath("$.volunteeringPersons[1].name").value(volunteeringPerson1.getName()))
                .andExpect(jsonPath("$.volunteeringPersons[1].profilePicture.id").value(
                        volunteeringPerson1.getProfilePicture().getId()));
    }

    @Test
    public void getVolunteeringById_NotFound() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        volunteeringFeuerwehr.getTags().setValues(EnumSet.of(OrganizationTag.EMERGENCY_AID));
        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);

        mockMvc.perform(get("/grapevine/volunteering/{volunteeringId}", volunteeringFeuerwehr.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));

        mockMvc.perform(get("/grapevine/volunteering/{volunteeringId}", UUID.randomUUID().toString())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));

        mockMvc.perform(get("/grapevine/volunteering/{volunteeringId}", "🐖")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));
    }

    @Test
    public void getVolunteeringById_Deleted() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        volunteeringFeuerwehr.setDeleted(true);
        volunteeringFeuerwehr.setDeletionTime(12021983L);
        volunteeringFeuerwehr = organizationRepository.save(volunteeringFeuerwehr);

        mockMvc.perform(get("/grapevine/volunteering/{volunteeringId}", volunteeringFeuerwehr.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.ORGANIZATION_NOT_FOUND));
    }

    @Test
    public void getVolunteeringById_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/volunteering/{volunteeringId}", volunteeringFeuerwehr.getId()),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

}
