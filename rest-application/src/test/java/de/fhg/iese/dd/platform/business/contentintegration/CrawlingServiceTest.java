/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Mher Ter-Tovmasyan, Ben Burkhard, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.contentintegration.ContentIntegrationTestHelper;
import de.fhg.iese.dd.platform.business.contentintegration.services.ICrawlingService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ExternalPostRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.TestMediaItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CrawlingServiceTest extends BaseServiceTest {

    @Autowired
    private ICrawlingService crawlingService;

    @Autowired
    private TestMediaItemService testMediaItemService;

    @Autowired
    private ExternalPostRepository externalPostRepository;

    @Autowired
    private ContentIntegrationTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createNewsSourcesAndCrawlingConfigs();
        th.createNewsItemTypeFeatures();
        th.createHappeningTypeFeatures();
        th.createPushEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void createPosts() throws Exception {

        MediaItem mediaItemGrillfest1 = testMediaItemService.setupDownloadMock(
                new URL("https://www.example.org/images/grillfest1.jpg"));
        MediaItem mediaItemGrillfest2 = testMediaItemService.setupDownloadMock(
                new URL("https://www.example.org/images/grillfest2.jpg"));
        MediaItem mediaItemHundefest = testMediaItemService.setupDownloadMock(
                new URL("https://www.example.org/images/hundefest.jpg"));

        crawlingService.createPostsFromExternalContent(new LogSummary(log), false);

        waitForEventProcessingLong();

        // Create updated crawling configs and crawl again in order to update existing posts
        th.updateCrawlingConfigs();
        crawlingService.createPostsFromExternalContent(new LogSummary(log), false);

        waitForEventProcessingLong();

        List<ExternalPost> externalPosts = externalPostRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(ExternalPost::getText))
                .collect(Collectors.toList());
        assertThat(externalPosts).hasSize(18);

        //ATOM post 1
        ExternalPost externalPostATOM1 = externalPosts.get(0);
        assertThat(externalPostATOM1.getNewsSource().getId()).isEqualTo(th.newsSourceDorfallerlei.getId());
        assertThat(externalPostATOM1.getOrganization()).isNull();
        assertThat(externalPostATOM1.getCategories()).isNullOrEmpty();
        assertThat(externalPostATOM1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrunga");
        assertThat(externalPostATOM1.getText())
                .isEqualTo("A Ein Muss für jeden Besucher: Begeben Sie sich auf die Spuren von Hinz und Kunz.");
        assertThat(externalPostATOM1.getAuthorName()).isEqualTo("Erika Mustermann, erika.mustermann@example.org");
        assertThat(externalPostATOM1.getImages()).isEmpty();
        assertThat(externalPostATOM1.isPublished()).isFalse();
        assertThat(externalPostATOM1.isCommentsAllowed()).isTrue();

        //ATOM post 2
        ExternalPost externalPostATOM2 = externalPosts.get(1);
        assertThat(externalPostATOM2.getNewsSource().getId()).isEqualTo(th.newsSourceDorfallerlei.getId());
        assertThat(externalPostATOM2.getOrganization()).isNull();
        assertThat(externalPostATOM2.getCategories()).isEqualTo(
                "Haupteintrag, Kultur & Veranstaltungen, Pressemitteilungen, Presse");
        assertThat(externalPostATOM2.getUrl()).isEqualTo(
                "https://www.example.org/presse/pressemitteilungen/kultur-veranstaltungen/hundefest-feiert-seinen-50-geburtstagb");
        assertThat(externalPostATOM2.getText()).isEqualTo(
                "BE Kraki findet am Samstag, 24. Juli, neben der Kaiserpfalzburg statt");
        assertThat(externalPostATOM2.getAuthorName()).isEqualTo("Max Mustermann, max.mustermann@example.org");
        assertThat(externalPostATOM2.getImages()).containsExactly(mediaItemHundefest);
        assertThat(externalPostATOM2.isPublished()).isFalse();
        assertThat(externalPostATOM2.isCommentsAllowed()).isTrue();

        //ATOM post 3
        ExternalPost externalPostATOM3 = externalPosts.get(2);
        assertThat(externalPostATOM3.getNewsSource().getId()).isEqualTo(th.newsSourceDorfallerlei.getId());
        assertThat(externalPostATOM3.getOrganization()).isNull();
        assertThat(externalPostATOM3.getCategories()).isEqualTo("Haupteintrag, Veranstaltungen, Kultur & Freizeit");
        assertThat(externalPostATOM3.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/524092-grillgladiatoren-grill-den-kaiser-22-juni-2023c");
        assertThat(externalPostATOM3.getText()).isEqualTo(
                "C Wer sagt, dass gesundes Essen nicht schmecken kann! 09.00 - 12.30 Uhr GRILLGLADIATOREN® 'Grill-den-Kaiser' Wettkampf Kinder (16 Teams Teststadter Kinder treten gegeneinander an) 16.00 - 20.00 Uhr GRILLGLADIATOREN® Jack the 1500 Jahre Teststadt Intern. Grill & BBQ Competition");
        assertThat(externalPostATOM3.getImages()).containsExactly(mediaItemGrillfest1, mediaItemGrillfest2);
        assertThat(externalPostATOM3.isPublished()).isFalse();
        assertThat(externalPostATOM3.isCommentsAllowed()).isTrue();

        //RSS post 1
        ExternalPost externalPostRSS1 = externalPosts.get(3);
        assertThat(externalPostRSS1.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSS1.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSS1.getGeoAreas()).containsExactlyInAnyOrder(th.geoAreaMainz, th.geoAreaKaiserslautern);
        assertThat(externalPostRSS1.getCategories()).isNullOrEmpty();
        assertThat(externalPostRSS1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungd");
        assertThat(externalPostRSS1.getText()).isEqualTo("D Ein Muss für jeden Besucher.");
        assertThat(externalPostRSS1.getAuthorName()).isEqualTo("erika.mustermann@example.org (Erika Mustermann)");
        assertThat(externalPostRSS1.getImages()).isEmpty();
        assertThat(externalPostRSS1.isPublished()).isFalse();
        assertThat(externalPostRSS1.isCommentsAllowed()).isFalse();

        //RSS post 2
        ExternalPost externalPostRSS2 = externalPosts.get(4);
        assertThat(externalPostRSS2.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSS2.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSS2.getCategories())
                .isEqualTo("Haupteintrag, Kultur & Veranstaltungen, Pressemitteilungen, Presse");
        assertThat(externalPostRSS2.getUrl()).isEqualTo(
                "https://www.example.org/presse/pressemitteilungen/kultur-veranstaltungen/hundefest-feiert-seinen-50-geburtstage");
        assertThat(externalPostRSS2.getText())
                .isEqualTo("E Kraki findet am Samstag, 24. Juli, neben der Kaiserpfalzburg statt");
        assertThat(externalPostRSS2.getAuthorName()).isEqualTo("max.mustermann@example.org (Max Mustermann)");
        assertThat(externalPostRSS2.getImages()).containsExactly(mediaItemHundefest);
        assertThat(externalPostRSS2.isPublished()).isFalse();
        assertThat(externalPostRSS2.isCommentsAllowed()).isFalse();

        //RSS post 3
        ExternalPost externalPostRSS3 = externalPosts.get(5);
        assertThat(externalPostRSS3.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSS3.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSS3.getCategories()).isEqualTo("Haupteintrag, Veranstaltungen, Kultur & Freizeit");
        assertThat(externalPostRSS3.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/524092-grillgladiatoren-grill-den-kaiser-22-juni-2023f");
        assertThat(externalPostRSS3.getText()).isEqualTo(
                "F Wer sagt, dass gesundes Essen nicht schmecken kann! 09.00 - 12.30 Uhr GRILLGLADIATOREN® 'Grill-den-Kaiser' Wettkampf Kinder (16 Teams Teststadter Kinder treten gegeneinander an) 16.00 - 20.00 Uhr GRILLGLADIATOREN® Jack the 1500 Jahre Teststadt Intern. Grill & BBQ Competition");
        assertThat(externalPostRSS3.getImages()).containsExactly(mediaItemGrillfest1, mediaItemGrillfest2);
        assertThat(externalPostRSS3.isPublished()).isFalse();
        assertThat(externalPostRSS3.isCommentsAllowed()).isFalse();

        //RSS Happening 1
        Happening externalHappeningPostRSS1 = (Happening) externalPosts.get(6);
        assertThat(externalHappeningPostRSS1.getNewsSource()).isEqualTo(th.newsSourceZukunftstheim);
        assertThat(externalHappeningPostRSS1.getOrganization())
                .isEqualTo(th.organizationCrawlingConfigRSSHappeningZukunftstheim);
        assertThat(externalHappeningPostRSS1.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningPostRSS1.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungg-event");
        assertThat(externalHappeningPostRSS1.getText()).isEqualTo("G Teststadt Event");
        assertThat(externalHappeningPostRSS1.getAuthorName()).isEqualTo(
                "erika.mustermann@example.org (Erika Mustermann)");
        assertThat(externalHappeningPostRSS1.getImages()).isEmpty();
        assertThat(externalHappeningPostRSS1.getOrganizer()).isEqualTo("Teststadt");
        assertThat(externalHappeningPostRSS1.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Thu, 15 Oct 2020 12:00:00 +0100"));
        assertThat(externalHappeningPostRSS1.getEndTime()).isEqualTo(
                toUTCMillisWithDefaultZone("Fri, 16 Oct 2020 16:00:00"));
        assertThat(externalHappeningPostRSS1.isAllDayEvent()).isFalse();
        assertThat(externalHappeningPostRSS1.isPublished()).isFalse();
        assertThat(externalHappeningPostRSS1.isCommentsAllowed()).isTrue();

        //RSS Happening 2
        Happening externalHappeningPostRSS2 = (Happening) externalPosts.get(7);
        assertThat(externalHappeningPostRSS2.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalHappeningPostRSS2.getOrganization())
                .isEqualTo(th.organizationCrawlingConfigRSSHappeningZukunftstheim);
        assertThat(externalHappeningPostRSS2.getCategories()).isEqualTo("Kultur & Veranstaltungen");
        assertThat(externalHappeningPostRSS2.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungh-trip");
        assertThat(externalHappeningPostRSS2.getText()).isEqualTo("H Stadtführung Event");
        assertThat(externalHappeningPostRSS2.getAuthorName()).isEqualTo(
                "erika.mustermann@example.org (Erika Mustermann)");
        assertThat(externalHappeningPostRSS2.getImages()).isEmpty();
        assertThat(externalHappeningPostRSS2.getOrganizer()).isEqualTo("Teststadt");
        assertThat(externalHappeningPostRSS2.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Thu, 15 Oct 2020 15:00:00 +0100"));
        assertThat(externalHappeningPostRSS2.getEndTime()).isEqualTo(-1);
        assertThat(externalHappeningPostRSS2.isAllDayEvent()).isTrue();
        assertThat(externalHappeningPostRSS2.isPublished()).isFalse();
        assertThat(externalHappeningPostRSS2.isCommentsAllowed()).isTrue();

        //RSS Happening 3
        Happening externalHappeningPostRSS3 = (Happening) externalPosts.get(8);
        assertThat(externalHappeningPostRSS3.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalHappeningPostRSS3.getOrganization()).isEqualTo(
                th.organizationCrawlingConfigRSSHappeningZukunftstheim);
        assertThat(externalHappeningPostRSS3.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningPostRSS3.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungi-trip");
        assertThat(externalHappeningPostRSS3.getText()).isEqualTo("I Stadtführung Event");
        assertThat(externalHappeningPostRSS3.getAuthorName()).isEqualTo(
                "erika.mustermann@example.org (Erika Mustermann)");
        assertThat(externalHappeningPostRSS3.getImages()).isEmpty();
        assertThat(externalHappeningPostRSS3.getOrganizer()).isEqualTo("Teststadt");
        assertThat(externalHappeningPostRSS3.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Thu, 15 Oct 2020 11:00:00 +0100"));
        assertThat(externalHappeningPostRSS3.getEndTime()).isEqualTo(
                toUTCMillisWithDefaultZone("Fri, 16 Oct 2020 13:00:00"));
        assertThat(externalHappeningPostRSS3.isAllDayEvent()).isTrue();
        assertThat(externalHappeningPostRSS3.isPublished()).isFalse();
        assertThat(externalHappeningPostRSS3.isCommentsAllowed()).isTrue();

        Happening externalHappeningMixedRegexPostRSS1 = (Happening) externalPosts.get(9);
        assertThat(externalHappeningMixedRegexPostRSS1.getNewsSource()).isEqualTo(th.newsSourceZukunftstheim);
        assertThat(externalHappeningMixedRegexPostRSS1.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS1.getUrl()).isEqualTo(
                "https://www.example.org/test-regex-format-1");
        assertThat(externalHappeningMixedRegexPostRSS1.getText()).isEqualTo("X1 10.11.2023 ab 17:30 Uhr");
        assertThat(externalHappeningMixedRegexPostRSS1.getAuthorName()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS1.getImages()).isEmpty();
        assertThat(externalHappeningMixedRegexPostRSS1.getOrganizer()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS1.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Fri, 10 Nov 2023 17:30:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS1.getEndTime()).isEqualTo(
                toUTCMillisWithZone("Fri, 10 Nov 2023 17:30:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS1.isAllDayEvent()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS1.isPublished()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS1.isCommentsAllowed()).isTrue();

        Happening externalHappeningMixedRegexPostRSS2 = (Happening) externalPosts.get(10);
        assertThat(externalHappeningMixedRegexPostRSS2.getNewsSource()).isEqualTo(th.newsSourceZukunftstheim);
        assertThat(externalHappeningMixedRegexPostRSS2.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS2.getUrl()).isEqualTo(
                "https://www.example.org/test-regex-format-2");
        assertThat(externalHappeningMixedRegexPostRSS2.getText()).isEqualTo("X2 05.11.2023 von 10:00 bis 18:30 Uhr");
        assertThat(externalHappeningMixedRegexPostRSS2.getAuthorName()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS2.getImages()).isEmpty();
        assertThat(externalHappeningMixedRegexPostRSS2.getOrganizer()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS2.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Sun, 05 Nov 2023 10:00:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS2.getEndTime()).isEqualTo(
                toUTCMillisWithZone("Sun, 05 Nov 2023 18:30:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS2.isAllDayEvent()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS2.isPublished()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS2.isCommentsAllowed()).isTrue();

        Happening externalHappeningMixedRegexPostRSS3 = (Happening) externalPosts.get(11);
        assertThat(externalHappeningMixedRegexPostRSS3.getNewsSource()).isEqualTo(th.newsSourceZukunftstheim);
        assertThat(externalHappeningMixedRegexPostRSS3.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS3.getUrl()).isEqualTo(
                "https://www.example.org/test-regex-format-3");
        assertThat(externalHappeningMixedRegexPostRSS3.getText()).isEqualTo("X3 11.11.2023");
        assertThat(externalHappeningMixedRegexPostRSS3.getAuthorName()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS3.getImages()).isEmpty();
        assertThat(externalHappeningMixedRegexPostRSS3.getOrganizer()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS3.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Sat, 11 Nov 2023 00:00:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS3.getEndTime()).isEqualTo(-1L);
        assertThat(externalHappeningMixedRegexPostRSS3.isAllDayEvent()).isTrue();
        assertThat(externalHappeningMixedRegexPostRSS3.isPublished()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS3.isCommentsAllowed()).isTrue();

        Happening externalHappeningMixedRegexPostRSS4 = (Happening) externalPosts.get(12);
        assertThat(externalHappeningMixedRegexPostRSS4.getNewsSource()).isEqualTo(th.newsSourceZukunftstheim);
        assertThat(externalHappeningMixedRegexPostRSS4.getCategories()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS4.getUrl()).isEqualTo(
                "https://www.example.org/test-regex-format-4");
        assertThat(externalHappeningMixedRegexPostRSS4.getText()).isEqualTo("X4 10.11.2023 bis 12.11.2023 von 16:00 bis 15:00 Uhr");
        assertThat(externalHappeningMixedRegexPostRSS4.getAuthorName()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS4.getImages()).isEmpty();
        assertThat(externalHappeningMixedRegexPostRSS4.getOrganizer()).isNullOrEmpty();
        assertThat(externalHappeningMixedRegexPostRSS4.getStartTime()).isEqualTo(
                toUTCMillisWithZone("Fri, 10 Nov 2023 16:00:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS4.getEndTime()).isEqualTo(
                toUTCMillisWithZone("Sun, 12 Nov 2023 15:00:00 +0100"));
        assertThat(externalHappeningMixedRegexPostRSS4.isAllDayEvent()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS4.isPublished()).isFalse();
        assertThat(externalHappeningMixedRegexPostRSS4.isCommentsAllowed()).isTrue();

        //RSS with format and fixed value post 1
        ExternalPost externalPostRSSWithFormatAndFixedValue1 = externalPosts.get(13);
        assertThat(externalPostRSSWithFormatAndFixedValue1.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSSWithFormatAndFixedValue1.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSSWithFormatAndFixedValue1.getCategories()).isNullOrEmpty();
        assertThat(externalPostRSSWithFormatAndFixedValue1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungd?format");
        assertThat(externalPostRSSWithFormatAndFixedValue1.getText()).isEqualTo("ZD Ein Muss für jeden Teststadt-Besucher: Begeben Sie sich auf die Spuren von Hinz und Kunz.");
        assertThat(externalPostRSSWithFormatAndFixedValue1.getAuthorName()).isEqualTo("Fixer Tipper");
        assertThat(externalPostRSSWithFormatAndFixedValue1.getImages()).isEmpty();
        assertThat(externalPostRSSWithFormatAndFixedValue1.isPublished()).isFalse();
        assertThat(externalPostRSSWithFormatAndFixedValue1.isCommentsAllowed()).isTrue();

        //RSS with format and fixed value post 2
        ExternalPost externalPostRSSWithFormatAndFixedValue2 = externalPosts.get(14);
        assertThat(externalPostRSSWithFormatAndFixedValue2.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSSWithFormatAndFixedValue2.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSSWithFormatAndFixedValue2.getCategories())
                .isEqualTo("Haupteintrag, Kultur & Veranstaltungen, Pressemitteilungen, Presse");
        assertThat(externalPostRSSWithFormatAndFixedValue2.getUrl()).isEqualTo(
                "https://www.example.org/presse/pressemitteilungen/kultur-veranstaltungen/hundefest-feiert-seinen-50-geburtstage?format");
        assertThat(externalPostRSSWithFormatAndFixedValue2.getText())
                .isEqualTo("ZE Kraki findet am Samstag, 24. Juli, neben der Kaiserpfalzburg statt");
        assertThat(externalPostRSSWithFormatAndFixedValue2.getAuthorName()).isEqualTo("Fixer Tipper");
        assertThat(externalPostRSSWithFormatAndFixedValue2.getImages()).containsExactly(mediaItemHundefest);
        assertThat(externalPostRSSWithFormatAndFixedValue2.isPublished()).isFalse();
        assertThat(externalPostRSSWithFormatAndFixedValue2.isCommentsAllowed()).isTrue();

        //RSS with format and fixed value post 3
        ExternalPost externalPostRSSWithFormatAndFixedValue3 = externalPosts.get(15);
        assertThat(externalPostRSSWithFormatAndFixedValue3.getNewsSource().getId()).isEqualTo(th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSSWithFormatAndFixedValue3.getOrganization()).isEqualTo(th.organizationCrawlingConfigRSSNewsItemZukunftstheim);
        assertThat(externalPostRSSWithFormatAndFixedValue3.getCategories()).isEqualTo("Haupteintrag, Veranstaltungen, Kultur & Freizeit");
        assertThat(externalPostRSSWithFormatAndFixedValue3.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/524092-grillgladiatoren-grill-den-kaiser-22-juni-2023f?format");
        assertThat(externalPostRSSWithFormatAndFixedValue3.getText()).isEqualTo(
                "ZF Wer sagt, dass gesundes Essen nicht schmecken kann! 09.00 - 12.30 Uhr GRILLGLADIATOREN® 'Grill-den-Kaiser' Wettkampf Kinder (16 Teams Teststadter Kinder treten gegeneinander an) 16.00 - 20.00 Uhr GRILLGLADIATOREN® Jack the 1500 Jahre Teststadt Intern. Grill & BBQ Competition");
        assertThat(externalPostRSSWithFormatAndFixedValue3.getAuthorName()).isEqualTo("Fixer Tipper");
        assertThat(externalPostRSSWithFormatAndFixedValue3.getImages()).containsExactly(mediaItemGrillfest1, mediaItemGrillfest2);
        assertThat(externalPostRSSWithFormatAndFixedValue3.isPublished()).isFalse();
        assertThat(externalPostRSSWithFormatAndFixedValue3.isCommentsAllowed()).isTrue();

        //RSS with composed text field - duplicate title
        ExternalPost externalPostRSSWithComposedText1 = externalPosts.get(16);
        assertThat(externalPostRSSWithComposedText1.getNewsSource().getId()).isEqualTo(
                th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSSWithComposedText1.getOrganization()).isNull();
        assertThat(externalPostRSSWithComposedText1.getCategories()).isEqualTo(
                "Haupteintrag, Kultur & Veranstaltungen, Pressemitteilungen");
        assertThat(externalPostRSSWithComposedText1.getUrl()).isEqualTo(
                "https://www.example.org/kultur-freizeit/veranstaltungen/tauschtage");
        assertThat(externalPostRSSWithComposedText1.getText()).isEqualTo(
                "ZX Total tolle teststädter Tausch-Tage, Besondere Besucher bekommen bessere Boni");
        assertThat(externalPostRSSWithComposedText1.getAuthorName()).isEqualTo("Peter Peters");
        assertThat(externalPostRSSWithComposedText1.isPublished()).isFalse();
        assertThat(externalPostRSSWithComposedText1.isCommentsAllowed()).isTrue();

        //RSS with composed text field - title in description
        ExternalPost externalPostRSSWithComposedText = externalPosts.get(17);
        assertThat(externalPostRSSWithComposedText.getNewsSource().getId()).isEqualTo(
                th.newsSourceZukunftstheim.getId());
        assertThat(externalPostRSSWithComposedText.getOrganization()).isNull();
        assertThat(externalPostRSSWithComposedText.getCategories()).isEqualTo(
                "Haupteintrag, Pressemitteilungen");
        assertThat(externalPostRSSWithComposedText.getUrl()).isEqualTo(
                "https://www.example.org/rückblick/tausend-tage");
        assertThat(externalPostRSSWithComposedText.getText()).isEqualTo(
                "ZZ Tausend Tage Teststadt: Ende einer ereignisreichen Epoche");
        assertThat(externalPostRSSWithComposedText.getAuthorName()).isEqualTo("Peter Peters");
        assertThat(externalPostRSSWithComposedText.isPublished()).isFalse();
        assertThat(externalPostRSSWithComposedText.isCommentsAllowed()).isTrue();
    }

    @Test
    public void createPosts_Exceptions() throws Exception {

        //testMediaItemService is intentionally not configured, so all media item creations should fail

        crawlingService.createPostsFromExternalContent(new LogSummary(log), false);

        waitForEventProcessingLong();

        List<ExternalPost> externalPosts = externalPostRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(ExternalPost::getText))
                .collect(Collectors.toList());
        assertThat(externalPosts).hasSize(12);

        //ATOM post 1
        ExternalPost externalPostATOM1 = externalPosts.get(0);
        assertThat(externalPostATOM1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrunga");

        //ATOM post 2 should fail

        //ATOM post 3 should fail

        //RSS post 1
        ExternalPost externalPostRSS1 = externalPosts.get(1);
        assertThat(externalPostRSS1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungd");

        //RSS post 2 should fail

        //RSS post 3 should fail

        //RSS Happening 1
        Happening externalHappeningPostRSS1 = (Happening) externalPosts.get(2);
        assertThat(externalHappeningPostRSS1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungg-event");

        //RSS Happening 2
        Happening externalHappeningPostRSS2 = (Happening) externalPosts.get(3);
        assertThat(externalHappeningPostRSS2.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungh-trip");

        //RSS Happening 3
        Happening externalHappeningPostRSS3 = (Happening) externalPosts.get(4);
        assertThat(externalHappeningPostRSS3.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungi-trip");

        Happening externalHappeningMixedRegexPostRSS1 = (Happening) externalPosts.get(5);
        assertThat(externalHappeningMixedRegexPostRSS1.getUrl())
                .isEqualTo("https://www.example.org/test-regex-format-1");

        Happening externalHappeningMixedRegexPostRSS2 = (Happening) externalPosts.get(6);
        assertThat(externalHappeningMixedRegexPostRSS2.getUrl())
                .isEqualTo("https://www.example.org/test-regex-format-2");


        Happening externalHappeningMixedRegexPostRSS3 = (Happening) externalPosts.get(7);
        assertThat(externalHappeningMixedRegexPostRSS3.getUrl())
                .isEqualTo("https://www.example.org/test-regex-format-3");

        Happening externalHappeningMixedRegexPostRSS4 = (Happening) externalPosts.get(8);
        assertThat(externalHappeningMixedRegexPostRSS4.getUrl())
                .isEqualTo("https://www.example.org/test-regex-format-4");

        //RSS with format and fixed value post 1
        ExternalPost externalPostRSSWithFormatAndFixedValue1 = externalPosts.get(9);
        assertThat(externalPostRSSWithFormatAndFixedValue1.getUrl())
                .isEqualTo("https://www.example.org/kultur-freizeit/veranstaltungen/teststadt-stadtfuehrungd?format");

        //RSS with format and fixed value post 2 should fail

        //RSS with format and fixed value post 3 should fail

    }

    private static long toUTCMillisWithZone(String dateString) {

        return ZonedDateTime.parse(dateString, DateTimeFormatter.RFC_1123_DATE_TIME)
                .toInstant()
                .toEpochMilli();
    }

    private static long toUTCMillisWithDefaultZone(String dateString) {

        return LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH))
                .atZone(ZoneId.of("Europe/Berlin"))
                .toInstant()
                .toEpochMilli();
    }

}
