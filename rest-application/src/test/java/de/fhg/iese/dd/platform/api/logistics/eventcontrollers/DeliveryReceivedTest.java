/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryReceivedRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.logistics.events.DeliveryReceivedRequest;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class DeliveryReceivedTest extends BaseLogisticsEventTest {

    @Autowired
    private IAuthorizationService authorizationService;

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        String shopOrderId = purchaseOrderReceived();

        purchaseOrderReadyForTransport(shopOrderId);

        waitForEventProcessing();

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        delivery = deliveries.get(0);

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        TransportAlternative transportAlternative = transportAlternatives.get(0);

        TransportAssignment transportAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        waitForEventProcessing();

        transportPickupRequest(
            carrier, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        waitForEventProcessing();
    }

    @Test
    public void onDeliveryReceivedRequest() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
                .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

        waitForEventProcessing();

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.RECEIVED.name()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));
    }

    @Test
    public void onDeliveryReceivedRequestMail() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest("", delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", authorizationService.generateAuthToken(
                                new DeliveryReceivedRequest(delivery, delivery.getReceiver().getPerson()),
                                Duration.ofMinutes(5)))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
                .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

        waitForEventProcessing();

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.RECEIVED.name()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));
    }

    @Test
    public void onDeliveryReceivedRequestMailUnauthorized() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest("", delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", "invalidToken")
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void onDeliveryReceivedRequestMailDeliveryIdInvalid() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    "");

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", authorizationService.generateAuthToken(
                                new DeliveryReceivedRequest(delivery, delivery.getReceiver().getPerson()),
                                Duration.ofMinutes(5)))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onDeliveryReceivedRequestMailWrongToken() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest("", delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", authorizationService.generateAuthToken(
                                new DeliveryReadyForTransportRequest(delivery), Duration.ofMinutes(5)))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void onDeliveryReceivedRequestMailIdempotence() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", authorizationService.generateAuthToken(
                                new DeliveryReceivedRequest(delivery, delivery.getReceiver().getPerson()),
                                Duration.ofMinutes(5)))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

        waitForEventProcessing();

        clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest/mail")
                        .param("mailToken", authorizationService.generateAuthToken(
                                new DeliveryReceivedRequest(delivery, delivery.getReceiver().getPerson()),
                                Duration.ofMinutes(5)))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

    }

    @Test
    public void onDeliveryReceivedRequestTrackingCodeNull() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    null);

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onDeliveryReceivedRequestTrackingCodeInvalid() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    "",
                    delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onDeliveryReceivedRequestDeliveryIdNull() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    null);

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onDeliveryReceivedRequestDeliveryUnauthorized() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    delivery.getId());

        assertOAuth2(post("/logistics/event/deliveryReceivedRequest")
                .contentType(contentType)
                .content(json(clientDeliveryReceivedRequest)));
    }

    @Test
    public void onDeliveryReceivedRequestDeliveryWrongUser() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.WRONG_RECEIVER_RECEIVED));
    }

    @Test
    public void onDeliveryReceivedRequestDeliveryIdInvalid() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(
                    delivery.getTrackingCode(),
                    "");

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onDeliveryReceivedRequestIdempotence() throws Exception {

        ClientDeliveryReceivedRequest clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

        waitForEventProcessing();

        clientDeliveryReceivedRequest =
                new ClientDeliveryReceivedRequest(delivery.getTrackingCode(), delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(clientDeliveryReceivedRequest)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
            .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()));

    }

    @Test
    public void onDeliveryReceivedRequestInvalidTrackingCode() throws Exception {

        ClientDeliveryReceivedRequest deliveryReceivedRequest = new ClientDeliveryReceivedRequest("invalid",
                delivery.getId());

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(deliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.WRONG_TRACKING_CODE));
    }

    @Test
    public void onDeliveryReceivedRequestInvalidDeliveryId() throws Exception {

        ClientDeliveryReceivedRequest deliveryReceivedRequest = new ClientDeliveryReceivedRequest(delivery.getTrackingCode(),
                "invalid");

        mockMvc.perform(post("/logistics/event/deliveryReceivedRequest").headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(deliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.DELIVERY_NOT_FOUND));
    }

}
