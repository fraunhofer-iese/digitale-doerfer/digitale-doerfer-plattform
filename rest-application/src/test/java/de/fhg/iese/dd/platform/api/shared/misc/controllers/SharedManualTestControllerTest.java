/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.TestTemplateInvocationContext;
import org.junit.jupiter.api.extension.TestTemplateInvocationContextProvider;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ManualTestConfig;

public class SharedManualTestControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private ManualTestConfig manualTestConfig;
    @Autowired
    private List<IManualTestDataInitializer> manualTestDataInitializers;

    private static class ManualTestDataScenarioTestContextProvider implements TestTemplateInvocationContextProvider {

        @Autowired
        private List<IManualTestDataInitializer> manualTestDataInitializers;

        @Override
        public boolean supportsTestTemplate(ExtensionContext context) {
            return context.getTestClass().orElse(Object.class).equals(SharedManualTestControllerTest.class);
        }

        @Override
        public Stream<TestTemplateInvocationContext> provideTestTemplateInvocationContexts(ExtensionContext context) {

            //unfortunately we need to autowire this class manually first, before we are able to use the @Autowired fields
            SpringExtension.getApplicationContext(context)
                    .getAutowireCapableBeanFactory().autowireBean(this);

            return manualTestDataInitializers.stream()
                    .map(IManualTestDataInitializer::getScenarioId)
                    .filter(Objects::nonNull)
                    .distinct()
                    .map(this::toTestInvocationContext);
        }

        private TestTemplateInvocationContext toTestInvocationContext(String scenarioId) {
            return new TestTemplateInvocationContext() {
                @Override
                public String getDisplayName(int invocationIndex) {
                    return "Manual test data init for: " + scenarioId;
                }

                @Override
                public List<Extension> getAdditionalExtensions() {
                    return Collections.singletonList(new TypeBasedParameterResolver<String>() {
                        @Override
                        public String resolveParameter(ParameterContext parameterContext,
                                ExtensionContext extensionContext) throws ParameterResolutionException {
                            return scenarioId;
                        }
                    });
                }
            };
        }

    }

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAvailableScenarios() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/manualtest"))
            .andExpect(status().isOk())
            .andReturn();

        @SuppressWarnings("unchecked")
        List<String> actualScenarios = toObject(mvcResult.getResponse(), List.class);
        List<String> expectedScenarios = manualTestDataInitializers.stream()
                .map(IManualTestDataInitializer::getScenarioId)
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        assertThat(actualScenarios).isEqualTo(expectedScenarios);
    }

    @Test
    public void wrongApiKey() throws Exception {

        String scenarioId = manualTestDataInitializers.stream()
                .map(IManualTestDataInitializer::getScenarioId)
                .findFirst().get();

        mockMvc.perform(put("/manualtest/start")
                .header(SharedManualTestController.HEADER_NAME_MANUAL_TEST_API_KEY, "")
                .param("scenarioId", scenarioId)
                .param("clean", "true"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));

        mockMvc.perform(put("/manualtest/start")
                .header(SharedManualTestController.HEADER_NAME_MANUAL_TEST_API_KEY, "wrong key")
                .param("scenarioId", scenarioId)
                .param("clean", "true"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void wrongScenarioId() throws Exception {
        mockMvc.perform(put("/manualtest/start")
            .header(SharedManualTestController.HEADER_NAME_MANUAL_TEST_API_KEY, manualTestConfig.getApiKey())
            .param("scenarioId", "unknown-demo")
            .param("clean", "true"))
            .andExpect(status().isNotFound());
    }

    // integration test because it checks out the data init files from git
    @TestTemplate
    @ExtendWith(AssumeIntegrationTestExtension.class)
    //this provides all the different scenario ids
    @ExtendWith(ManualTestDataScenarioTestContextProvider.class)
    @Tag("data-init")
    public void executeAllScenarios(String scenarioId) throws Exception {

        //we need this to be initialized because most data initializers depend on it
        mockMvc.perform(put("/administration/initialize/createInitialData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("topics", "tenant, account, app, oauth, push, geoarea"))
                .andExpect(status().isCreated());

        //the achievement rules are not actually required, but make the test more realistic
        th.createAchievementRules(
                "shopping.score.purchaseOrder.created.1",
                "shopping.score.purchaseOrder.created.3",
                "score.account.received.1",
                "score.account.sent.1");

        waitForEventProcessing();

        //required to skip the cool down
        timeServiceMock.setOffset(2, ChronoUnit.MINUTES);

        mockMvc.perform(put("/manualtest/start")
                        .header(SharedManualTestController.HEADER_NAME_MANUAL_TEST_API_KEY, manualTestConfig.getApiKey())
                        .param("scenarioId", scenarioId)
                        .param("clean", "true"))
                .andExpect(status().isCreated());

        //we need to wait here for a longer time, since some of the data initializers trigger events
        waitForEventProcessingLong();
    }

}
