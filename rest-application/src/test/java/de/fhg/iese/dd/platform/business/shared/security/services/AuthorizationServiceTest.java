/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public class AuthorizationServiceTest extends BaseServiceTest {

    @Autowired
    private AuthorizationService authorizationService;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class TestExpiringEvent extends BaseEvent {

        private String string;

        private int integer;

        private Person person;

        private GeoArea geoArea;

    }

    @Getter
    @Setter
    public static class TestDifferentExpiringEvent extends TestExpiringEvent {

        @Builder(builderMethodName = "bob")
        public TestDifferentExpiringEvent(String string, int integer,
                Person person, GeoArea geoArea) {
            super(string, integer, person, geoArea);
        }

    }

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
    }

    @Test
    public void getMailToken_Success() {

        TestExpiringEvent event = TestExpiringEvent.builder()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("35d1b15f-2c81-4383-933f-d086e1b9c2a5")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0920a662-c7e7-4af2-b47f-43b40b9c5284")
                        .build())
                .build();

        String mailToken = authorizationService.generateAuthToken(event, Duration.ofMinutes(1));

        log.debug("Mail token with length {} : {}", mailToken.length(), mailToken);

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event);
    }

    @Test
    public void getMailToken_Expired() {

        TestExpiringEvent event = TestExpiringEvent.builder()
                .string("string mit leerzeichen und 😇")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        String mailToken = authorizationService.generateAuthToken(event, Duration.ofMinutes(-1));

        log.debug("Mail token with length {} : {}", mailToken.length(), mailToken);

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event)));
    }

    @Test
    public void getMailToken_TokenChanged() {

        TestExpiringEvent event = TestExpiringEvent.builder()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        String mailToken = authorizationService.generateAuthToken(event, Duration.ofMinutes(1));

        log.debug("Mail token with length {} : {}", mailToken.length(), mailToken);

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(
                        StringUtils.truncate(mailToken, 150), event)));
        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(
                        StringUtils.lowerCase(mailToken), event)));
        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken + "super+sicher",
                        event)));
    }

    @Test
    public void getMailToken_ValueChanged() {

        TestExpiringEvent event = TestExpiringEvent.builder()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        String mailToken = authorizationService.generateAuthToken(event, Duration.ofMinutes(1));

        log.debug("Mail token with length {} : {}", mailToken.length(), mailToken);

        event.setString("strüng");

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event)));

        event.setString("string");

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event);

        event.setInteger(12021980);

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event)));

        event.setInteger(12021983);

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event);

        event.getPerson().setId("2020");

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event)));

        event.getPerson().setId("4711");

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event);

        event.getPerson().setFirstName("This value is not in the token");

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, event);

        TestExpiringEvent duplicateEventWithDifferentCreated = TestExpiringEvent.builder()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken,
                duplicateEventWithDifferentCreated);
    }

    @Test
    public void getMailToken_DifferentEvent() {

        TestExpiringEvent event = TestExpiringEvent.builder()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        String mailToken = authorizationService.generateAuthToken(event, Duration.ofMinutes(1));

        log.debug("Mail token with length {} : {}", mailToken.length(), mailToken);

        //the only difference is the different event name
        TestDifferentExpiringEvent differentEvent = TestDifferentExpiringEvent.bob()
                .string("string")
                .integer(12021983)
                .person(Person.builder()
                        .id("4711")
                        .build())
                .geoArea(GeoArea.builder()
                        .id("0815")
                        .build())
                .build();

        log.debug("expectedException", assertThrows(NotAuthorizedException.class, () ->
                authorizationService.checkAuthTokenAuthorizedForEventAndThrowNotAuthorized(mailToken, differentEvent)));
    }

}
