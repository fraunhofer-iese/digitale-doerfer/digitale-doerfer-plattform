/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.logistics.services.ITransportAlternativeService;
import de.fhg.iese.dd.platform.business.logistics.services.ITransportAssignmentService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public class DirectionsControllerTest extends BaseLogisticsEventTest {

    @Autowired
    private ITransportAlternativeService transportAlternativeService;

    @Autowired
    private ITransportAssignmentService transportAssignmentService;

    private String alternativeId;
    private String assignmentId;
    private Person notTheCarrier;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private void specificInit() throws Exception {
        String shopOrderId = purchaseOrderReceived();
        waitForEventProcessing();
        final Delivery delivery = purchaseOrderReadyForTransport(shopOrderId);
        waitForEventProcessing();
        final TransportAlternativeBundle bundle = transportAlternativeService.findAllAlternativeBundles().iterator().next();
        assertEquals(delivery.getId(), bundle.getDelivery().getId());
        final TransportAlternative alternative = transportAlternativeService.findAllAlternativesForPotentialCarrier(bundle, carrier).get(0);
        alternativeId = alternative.getId();
        transportAssignmentService.createAssignmentFromAlternative(carrier, alternative);
        final TransportAssignment assignment = transportAssignmentService.findAllTransportAssignments().iterator().next();
        assertEquals(delivery.getId(), assignment.getDelivery().getId());
        assignmentId = assignment.getId();
        notTheCarrier = th.personRegularAnna;
        assertNotNull(carrier);
        assertNotEquals(notTheCarrier, carrier);
    }

    @Test
    public void unauthorizedAccessIsForbiddenForDirectionsByAssignementId() throws Exception {

        assertOAuth2(get("/logistics/directions/toPickupByAssignmentId")
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5));

        assertOAuth2(get("/logistics/directions/toDeliveryByAssignmentId")
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5));

        assertOAuth2(get("/logistics/directions/pickupToDeliveryByAssignmentId")
                .param("assignmentId", assignmentId));
    }

    @Test
    public void notExistingAssignmentOrAlternativeIdCausesNotFound() throws Exception {
        final String notExistingId = "not_existing";

        mockMvc.perform(get("/logistics/directions/toPickupByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", notExistingId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));

        mockMvc.perform(get("/logistics/directions/toDeliveryByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", notExistingId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));

        mockMvc.perform(get("/logistics/directions/pickupToDeliveryByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", notExistingId))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));

        mockMvc.perform(get("/logistics/directions/pickupToDeliveryByAlternativeId")
                .headers(authHeadersFor(carrier))
                .param("alternativeId", notExistingId))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ALTERNATIVE_NOT_FOUND));
    }

    @Test
    public void byAssignmentIdEndpointsCannotBeCalledFromOtherPersonThanAssignee() throws Exception {

        mockMvc.perform(get("/logistics/directions/toPickupByAssignmentId")
                .headers(authHeadersFor(notTheCarrier))
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(isException(ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON));

        mockMvc.perform(get("/logistics/directions/toDeliveryByAssignmentId")
                .headers(authHeadersFor(notTheCarrier))
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(isException(ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON));

        mockMvc.perform(get("/logistics/directions/pickupToDeliveryByAssignmentId")
                .headers(authHeadersFor(notTheCarrier))
                .param("assignmentId", assignmentId))
                .andExpect(isException(ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON));
    }

    @Test
    public void toPickupCalledByAssigneeReturnsDirections() throws Exception {
        mockMvc.perform(get("/logistics/directions/toPickupByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.routes").isNotEmpty())
                .andExpect(jsonPath("$.routes").isArray())
                .andExpect(jsonPath("$.routes[0].legs[0].endAddress").value("Ripperter Str. 63, 67304 Eisenberg (Pfalz), Deutschland"));
    }

    @Test
    public void toDeliveryCalledByAssigneeReturnsDirections() throws Exception {
        mockMvc.perform(get("/logistics/directions/toDeliveryByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", assignmentId)
                .param("currentLatitude", "" + 49.1)
                .param("currentLongitude", "" + 7.5))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.routes").isNotEmpty())
                .andExpect(jsonPath("$.routes").isArray())
                .andExpect(jsonPath("$.routes[0].legs[0].endAddress").value("Ripperter Str. 63, 67304 Eisenberg (Pfalz), Deutschland"));
    }

    @Test
    public void pickupToDeliveryCalledByAssigneeReturnsDirections() throws Exception {
        mockMvc.perform(get("/logistics/directions/pickupToDeliveryByAssignmentId")
                .headers(authHeadersFor(carrier))
                .param("assignmentId", assignmentId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.routes").isNotEmpty())
                .andExpect(jsonPath("$.routes").isArray())
                .andExpect(jsonPath("$.routes[0].legs[0].endAddress").value("Ripperter Str. 63, 67304 Eisenberg (Pfalz), Deutschland"));
    }

    @Test
    public void pickupToDeliveryCalledByAlternativeIdReturnsDirections() throws Exception {
        mockMvc.perform(get("/logistics/directions/pickupToDeliveryByAlternativeId")
                .param("alternativeId", alternativeId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.routes").isNotEmpty())
                .andExpect(jsonPath("$.routes").isArray())
                .andExpect(jsonPath("$.routes[0].legs[0].endAddress").value("Ripperter Str. 63, 67304 Eisenberg (Pfalz), Deutschland"));
    }

}
