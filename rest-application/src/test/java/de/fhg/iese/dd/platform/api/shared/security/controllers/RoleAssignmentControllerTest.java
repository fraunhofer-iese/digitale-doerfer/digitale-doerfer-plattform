/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.security.RoleTestHelper;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

public class RoleAssignmentControllerTest extends BaseServiceTest {

    @Autowired
    private RoleTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createPoolingStations();
        th.createShops();
        th.createAdditionalPersonsWithSpecialRoles();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void roleManager1CanListRoleInformation() throws Exception {
        assertCorrectRoleInformationList(th.personRoleManagerTenant1);
    }

    @Test
    public void roleManager2CanListRoleInformation() throws Exception {
        assertCorrectRoleInformationList(th.personRoleManagerTenant2);
    }

    @Test
    public void superAdminCanListRoleInformation() throws Exception {
        assertCorrectRoleInformationList(th.personSuperAdmin);
    }

    @Test
    public void userAdminsCanListRoleInformation() throws Exception {
        assertCorrectRoleInformationList(th.personGlobalUserAdmin);
        assertCorrectRoleInformationList(th.personUserAdminTenant2);
    }

    private void assertCorrectRoleInformationList(final Person person) throws Exception {

        assertFalse(th.allRoles.isEmpty(), "Too few roles, test setup is wrong");

        Object[] allRoleKey = th.allRoles.stream()
                .map(BaseRole::getKey)
                .toArray();

        mockMvc.perform(get("/roleAssignment/roles")
                .headers(authHeadersFor(person))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(th.allRoles.size())))
                .andExpect(jsonPath("$[*].key", containsInAnyOrder(allRoleKey)))
                .andReturn();
    }

    @Test
    public void personsNotRoleManagerOrSuperAdminsCanOnlyGetOwnRoleInformation() throws Exception {
        assertResponseContainsOnlyPersonRoleInformation(th.personIeseAdmin, RoleTestHelper.LOGISTICS_ADMIN_UI_ADMIN);
        assertResponseContainsOnlyPersonRoleInformation(th.personVgAdmin,
                RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN);
        assertResponseContainsOnlyPersonRoleInformation(th.personPoolingStationOp,
                RoleTestHelper.POOLINGSTATION_OPERATOR);
        assertResponseContainsOnlyPersonRoleInformation(th.personShopOwner, RoleTestHelper.SHOP_OWNER);
        assertResponseContainsOnlyPersonRoleInformation(th.personRegularAnna, null);
    }

    private void assertResponseContainsOnlyPersonRoleInformation(Person person, String roleKey) throws Exception {

        if (StringUtils.isNotEmpty(roleKey)) {
            mockMvc.perform(get("/roleAssignment/roles")
                    .headers(authHeadersFor(person))
                    .contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$[*].key", containsInAnyOrder(roleKey)))
                    .andReturn();
        } else {
            mockMvc.perform(get("/roleAssignment/roles")
                    .headers(authHeadersFor(person))
                    .contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$", hasSize(0)))
                    .andReturn();
        }
    }

    @Test
    public void roleAssignmentEndpoints_Unauthorized() throws Exception {

        assertOAuth2(get("/roleAssignment/roles"));
        assertOAuth2(get("/roleAssignment/own"));
    }

    @Test
    public void personRegularCanListOwnRolesWhenAuthenticated() throws Exception {
        mockMvc.perform(get("/roleAssignment/own")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void personWithOneRoleCanListOwnRolesWhenAuthenticated() throws Exception {
        mockMvc.perform(get("/roleAssignment/own")
                        .headers(authHeadersFor(th.personVgAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].roleKey").value(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN))
                .andExpect(jsonPath("$[0].personId").value(th.personVgAdmin.getId()))
                .andExpect(jsonPath("$[0].relatedEntityId").value(th.tenant1.getId()))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn();
    }

    @Test
    public void personWithManyRolesCanListOwnRolesWhenAuthenticated() throws Exception {

        //testing extended values for some entries only
        mockMvc.perform(get("/roleAssignment/own")
                        .headers(authHeadersFor(th.personWithManyRoles))
                        .contentType(contentType))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].roleKey").value(RoleTestHelper.GLOBAL_USER_ADMIN))
                .andExpect(jsonPath("$[0].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[0].relatedEntityId").value(nullValue()))
                .andExpect(jsonPath("$[0].relatedEntityName").value(nullValue()))
                .andExpect(jsonPath("$[0].relatedEntityTenantId").value(nullValue()))
                .andExpect(jsonPath("$[0].relatedEntityTenantName").value(nullValue()))
                .andExpect(jsonPath("$[1].roleKey").value(RoleTestHelper.LOGISTICS_ADMIN_UI_ADMIN))
                .andExpect(jsonPath("$[1].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[1].relatedEntityId").value(th.tenant1.getId()))
                .andExpect(jsonPath("$[1].relatedEntityName").value(th.tenant1.getName()))
                .andExpect(jsonPath("$[1].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("$[1].relatedEntityTenantName").value(th.tenant1.getName()))
                .andExpect(jsonPath("$[2].roleKey").value(RoleTestHelper.LOGISTICS_ADMIN_UI_ADMIN))
                .andExpect(jsonPath("$[2].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[2].relatedEntityId").value(th.tenant2.getId()))
                .andExpect(jsonPath("$[3].roleKey").value(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN))
                .andExpect(jsonPath("$[3].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[3].relatedEntityId").value(th.tenant2.getId()))
                .andExpect(jsonPath("$[4].roleKey").value(RoleTestHelper.POOLINGSTATION_OPERATOR))
                .andExpect(jsonPath("$[4].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[4].relatedEntityId").value(th.poolingStation0.getId()))
                .andExpect(jsonPath("$[5].roleKey").value(RoleTestHelper.SHOP_OWNER))
                .andExpect(jsonPath("$[5].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[5].relatedEntityId").value(th.shop1.getId()))
                .andExpect(jsonPath("$[5].relatedEntityName").value(th.shop1.getName()))
                .andExpect(jsonPath("$[5].relatedEntityTenantId").value(th.tenant1.getId()))
                .andExpect(jsonPath("$[5].relatedEntityTenantName").value(th.tenant1.getName()))
                .andExpect(jsonPath("$[6].roleKey").value(RoleTestHelper.SUPER_ADMIN))
                .andExpect(jsonPath("$[6].personId").value(th.personWithManyRoles.getId()))
                .andExpect(jsonPath("$[6].relatedEntityId").value(nullValue()))
                .andExpect(jsonPath("$", hasSize(7)))
                .andReturn();
    }

    @Test
    public void personCanListOwnRoleAssignmentsEvenIfSomeAreBroken() throws Exception {

        final Person personWithBrokenRoleAssignments = th.createPerson("Broke", th.tenant1, th.addressPersonVgAdmin,
                "brokenid");

        //required related entity null
        th.roleAssignmentRepository.save(RoleAssignment.builder()
                .person(personWithBrokenRoleAssignments)
                .role(UserAdmin.class)
                .build());
        //required related entity not existing
        th.roleAssignmentRepository.save(RoleAssignment.builder()
                .person(personWithBrokenRoleAssignments)
                .role(LogisticsAdminUiAdmin.class)
                .relatedEntityId("not_existing")
                .build());
        //valid role assignment
        th.roleAssignmentRepository.save(RoleAssignment.builder()
                .person(personWithBrokenRoleAssignments)
                .role(LogisticsAdminUiRestrictedAdmin.class)
                .relatedEntityId(th.tenant1.getId())
                .build());

        //expecting that only the valid assignment is listed
        mockMvc.perform(get("/roleAssignment/own")
                        .headers(authHeadersFor(personWithBrokenRoleAssignments))
                        .contentType(contentType))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].roleKey").value(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN))
                .andExpect(jsonPath("$[0].personId").value(personWithBrokenRoleAssignments.getId()))
                .andExpect(jsonPath("$[0].relatedEntityId").value(th.tenant1.getId()))
                .andExpect(jsonPath("$", hasSize(1)));
    }

}
