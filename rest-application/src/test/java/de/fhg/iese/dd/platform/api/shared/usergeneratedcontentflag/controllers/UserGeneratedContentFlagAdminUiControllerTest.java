/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.controllers.PostEventControllerFlagTest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlag;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagDetail;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagPageBean;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlagStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests the UserGeneratedContentFlagAdminUiController.
 * <br>
 * An additional positive test case can be found at {@link PostEventControllerFlagTest#flagGossip()}
 */
public class UserGeneratedContentFlagAdminUiControllerTest extends BaseUserGeneratedContentFlagAdminUiTest {

    /**
     * Intended to be overwritten in sub-tests.
     * <p>
     * These endpoints are checked for correct authorization and invalid id handling
     */
    protected Collection<String> getSingleFlagUrlTemplates() {

        return Collections.singleton("/adminui/flag/{flagId}");
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createUserGeneratedContentAdmins();
        super.createFlags();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void listFlags_GlobalAdmin_WithFiltering() throws Exception {

        final Person caller = th.personGlobalUserGeneratedContentAdmin;

        //all flags
        assertFlagsEqual(userGeneratedContentFlags.stream()
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller).getContent());

        //filter by tenant id
        assertFlagsEqual(userGeneratedContentFlags.stream()
                        .filter(f -> Objects.equals(f.getTenant(), th.tenant2))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, th.tenant2.getId()).getContent());

        // filter by status = OPEN
        assertFlagsEqual(userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.OPEN).stream()
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, UserGeneratedContentFlagStatus.OPEN).getContent());

        //filter by status = OPEN and tenant id
        assertFlagsEqual(userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.OPEN).stream()
                        .filter(f -> Objects.equals(f.getTenant(), th.tenant1))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, th.tenant1.getId(), UserGeneratedContentFlagStatus.OPEN).getContent());

        // filter by status = IN_PROGRESS
        assertFlagsEqual(userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.IN_PROGRESS).stream()
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, UserGeneratedContentFlagStatus.IN_PROGRESS).getContent());

        // filter by status = ACCEPTED
        assertFlagsEqual(userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.ACCEPTED).stream()
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, UserGeneratedContentFlagStatus.ACCEPTED).getContent());

        // filter by status = REJECTED
        assertFlagsEqual(userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.REJECTED).stream()
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller, UserGeneratedContentFlagStatus.REJECTED).getContent());

        // filter by status = OPEN or REJECTED
        assertFlagsEqual(Stream.concat(
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.OPEN).stream(),
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.REJECTED).stream())
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller,
                        UserGeneratedContentFlagStatus.OPEN,
                        UserGeneratedContentFlagStatus.REJECTED).getContent());

        // filter by status = OPEN or REJECTED and tenant id
        assertFlagsEqual(Stream.concat(
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.OPEN).stream(),
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.REJECTED).stream())
                        .filter(f -> Objects.equals(f.getTenant(), th.tenant2))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller,
                        th.tenant2.getId(),
                        UserGeneratedContentFlagStatus.OPEN,
                        UserGeneratedContentFlagStatus.REJECTED).getContent());

        // filter by status = OPEN or IN_PROGRESS or ACCEPTED
        assertFlagsEqual(Stream.concat(Stream.concat(
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.OPEN).stream(),
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.IN_PROGRESS).stream()),
                userGeneratedContentFlagsByStatus.get(UserGeneratedContentFlagStatus.ACCEPTED).stream())
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(caller,
                        UserGeneratedContentFlagStatus.OPEN,
                        UserGeneratedContentFlagStatus.IN_PROGRESS,
                        UserGeneratedContentFlagStatus.ACCEPTED).getContent());
    }

    @Test
    public void listFlags_TenantRestricted() throws Exception {

        assertFlagsEqual(userGeneratedContentFlags.stream()
                        .filter(ugcf -> th.tenant1.equals(ugcf.getTenant()))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(th.personUserGeneratedContentAdminTenant1).getContent());

        assertFlagsEqual(userGeneratedContentFlags.stream()
                        .filter(ugcf -> th.tenant2.equals(ugcf.getTenant()))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(th.personUserGeneratedContentAdminTenant2).getContent());

        //this should return the same, since the admin is already restricted to tenant2
        assertFlagsEqual(userGeneratedContentFlags.stream()
                        .filter(ugcf -> th.tenant2.equals(ugcf.getTenant()))
                        .sorted(Comparator.comparing(UserGeneratedContentFlag::getLastStatusUpdate).reversed())
                        .collect(Collectors.toList()),
                callListFlags(th.personUserGeneratedContentAdminTenant2, th.tenant2.getId()).getContent());
    }

    @Test
    public void listFlags_DifferentSorting() throws Exception {

        final Person caller = th.personGlobalUserGeneratedContentAdmin;
        for (UserGeneratedContentFlagAdminUiController.FlagSortColumn column :
                UserGeneratedContentFlagAdminUiController.FlagSortColumn.values()) {
            for (Sort.Direction direction : Sort.Direction.values()) {
                final UserGeneratedContentFlag expectedUserGeneratedContentFlag = flagRepository.findAll(
                        PageRequest.of(0, 1, Sort.by(direction, column.getColumn()))).getContent().get(0);
                final ClientUserGeneratedContentFlag actualFirstUserGeneratedContentFlag =
                        callListFlags(caller, 0, 1, column.name(), direction.name(), null, null).getContent().get(0);
                assertEquals(expectedUserGeneratedContentFlag.getId(),
                        actualFirstUserGeneratedContentFlag.getId(), "Sorting by " + column + "." + direction + " did not sort correctly");
            }
        }
    }

    @Test
    public void listFlags_InvalidParameters() throws Exception {

        final Person caller = th.personGlobalUserGeneratedContentAdmin;

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("page", "a"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("page", "-1"))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("count", "b"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("count", "0"))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("sortColumn", "unavailable"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(caller))
                .param("sortDirection", "DSE"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void listFlags_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/flag")
                .param("tenantId", th.tenant1.getId()));

        //no permission
        mockMvc.perform(get("/adminui/flag")
                .param("tenantId", th.tenant1.getId())
                .headers(authHeadersFor(th.personRegularAnna)))
                .andExpect(isNotAuthorized());

        //no permission for this tenant
        mockMvc.perform(get("/adminui/flag")
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2))
                .param("tenantId", th.tenant1.getId()))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getFlagById_GlobalAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant2;
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords()).hasSizeGreaterThan(1);

        assertFlagEquals(expectedFlag,
                toObject(mockMvc.perform(get("/adminui/flag/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagDetail.class));
    }

    @Test
    public void getFlagById_GlobalAdmin_NoTenant() throws Exception {

        UserGeneratedContentFlag expectedFlag = flagNoTenant;
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords()).hasSizeGreaterThanOrEqualTo(1);

        assertFlagEquals(expectedFlag,
                toObject(mockMvc.perform(get("/adminui/flag/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagDetail.class));
    }

    @Test
    public void getFlagById_GlobalAdmin_NoFlagCreator() throws Exception {

        UserGeneratedContentFlag expectedFlag = flagNoFlagCreator;
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords()).hasSizeGreaterThanOrEqualTo(1);

        assertFlagEquals(expectedFlag,
                toObject(mockMvc.perform(get("/adminui/flag/{flagId}", expectedFlag.getId())
                                        .contentType(contentType)
                                        .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagDetail.class));
    }

    @Test
    public void getFlagById_TenantAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag3Tenant2;
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords()).hasSizeGreaterThan(1);

        assertFlagEquals(expectedFlag,
                toObject(mockMvc.perform(get("/adminui/flag/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagDetail.class));
    }

    @Test
    public void getFlagById_TenantAdmin_NoTenant() throws Exception {

        UserGeneratedContentFlag expectedFlag = flagNoTenant;
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords()).hasSizeGreaterThanOrEqualTo(1);

        mockMvc.perform(get("/adminui/flag/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getFlagById_InvalidParameters() throws Exception {

        for (String singleFlagUrlTemplate : getSingleFlagUrlTemplates()) {
            mockMvc.perform(get(singleFlagUrlTemplate, BaseTestHelper.INVALID_UUID)
                    .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                    .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND));
        }

    }

    @Test
    public void getFlagById_Unauthorized() throws Exception {

        for (String singleFlagUrlTemplate : getSingleFlagUrlTemplates()) {
            assertOAuth2(get(singleFlagUrlTemplate, flag1Tenant1.getId()));

            //no permission
            mockMvc.perform(get(singleFlagUrlTemplate, flag1Tenant1.getId())
                    .headers(authHeadersFor(th.personRegularAnna)))
                    .andExpect(isNotAuthorized());

            //no permission for this tenant
            mockMvc.perform(get(singleFlagUrlTemplate, flag1Tenant1.getId())
                    .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                    .andExpect(isNotAuthorized());
        }
    }

    protected void deleteFlaggedEntity(UserGeneratedContentFlag flag) throws Exception {

        String comment = "Isch mach den Fleck mal weck! ⚗️";

        long changeTime = System.currentTimeMillis() + 12021983;
        timeServiceMock.setConstantDateTime(changeTime);

        int sizeBeforeUpdate =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag).size();
        assertThat(flag.getUserGeneratedContentFlagStatusRecords()).hasSize(sizeBeforeUpdate);
        assertThat(sizeBeforeUpdate).isGreaterThan(0);

        ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse response =
                toObject(mockMvc.perform(post("/adminui/flag/event/deleteFlagEntityByAdminRequest")
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin))
                                .contentType(contentType)
                                .content(json(ClientUserGeneratedContentFlagDeleteFlagEntityByAdminRequest.builder()
                                        .flagId(flag.getId())
                                        .comment(comment)
                                        .build())))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(contentType))
                                .andReturn(),
                        ClientUserGeneratedContentFlagDeleteFlagEntityByAdminResponse.class);

        waitForEventProcessing();

        final List<UserGeneratedContentFlagStatusRecord> actualStatusRecords =
                flagStatusRecordRepository.findAllByUserGeneratedContentFlagOrderByCreatedDesc(flag);
        assertThat(actualStatusRecords).hasSize(sizeBeforeUpdate + 1);
        final UserGeneratedContentFlagStatusRecord latestActualStatusRecord = actualStatusRecords.get(0);
        assertEquals(UserGeneratedContentFlagStatus.ACCEPTED, latestActualStatusRecord.getStatus());
        assertEquals(changeTime, latestActualStatusRecord.getCreated());
        assertEquals(comment, latestActualStatusRecord.getComment());

        final UserGeneratedContentFlag updatedFlag = flagRepository.findById(flag.getId()).get();
        assertEquals(UserGeneratedContentFlagStatus.ACCEPTED, updatedFlag.getStatus());
        assertEquals(changeTime, updatedFlag.getLastStatusUpdate());

        List<UserGeneratedContentFlagStatusRecord> newStatusRecords = new ArrayList<>();
        newStatusRecords.add(latestActualStatusRecord);
        newStatusRecords.addAll(flag.getUserGeneratedContentFlagStatusRecords());

        flag.setStatus(UserGeneratedContentFlagStatus.ACCEPTED);
        flag.setLastStatusUpdate(changeTime);
        flag.setUserGeneratedContentFlagStatusRecords(newStatusRecords);

        assertFlagEquals(flag, response.getChangedFlag());
    }

    private ClientUserGeneratedContentFlagPageBean callListFlags(Person caller) throws Exception {

        return callListFlags(caller, 0, 50, "LAST_STATUS_UPDATE", "DESC", null, null);
    }

    private ClientUserGeneratedContentFlagPageBean callListFlags(Person caller, String tenantId) throws Exception {

        return callListFlags(caller, 0, 50, "LAST_STATUS_UPDATE", "DESC", tenantId, null);
    }

    private ClientUserGeneratedContentFlagPageBean callListFlags(Person caller,
            UserGeneratedContentFlagStatus... includedStatus) throws Exception {

        return callListFlags(caller, 0, 50, "LAST_STATUS_UPDATE", "DESC", null,
                Arrays.stream(includedStatus).collect(Collectors.toSet()));
    }

    private ClientUserGeneratedContentFlagPageBean callListFlags(Person caller, String tenantId,
            UserGeneratedContentFlagStatus... includedStatus) throws Exception {

        return callListFlags(caller, 0, 50, "LAST_STATUS_UPDATE", "DESC", tenantId,
                Arrays.stream(includedStatus).collect(Collectors.toSet()));
    }

    private ClientUserGeneratedContentFlagPageBean callListFlags(Person caller, int page, int count, String sortColumn,
            String sortDirection, String tenantId, Set<UserGeneratedContentFlagStatus> includedStatus)
            throws Exception {

        MockHttpServletRequestBuilder builder = get("/adminui/flag")
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count))
                .param("sortColumn", sortColumn)
                .param("sortDirection", sortDirection);
        if (tenantId != null) {
            builder = builder.param("tenantId", tenantId);
        }
        if (includedStatus != null) {
            builder = builder.param("includedStatus", includedStatus.stream()
                    .map(Object::toString)
                    .toArray(String[]::new));
        }

        final ClientUserGeneratedContentFlagPageBean pagedResponse = toObject(mockMvc.perform(builder
                .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn()
                .getResponse(), ClientUserGeneratedContentFlagPageBean.class);
        log.debug("Flags in response:\n{}", pagedResponse.getContent()
                .stream()
                .map(f -> String.format("%s [%s] %s",
                        f.getId(),
                        f.getTenant() == null ? "-no tenant-" : f.getTenant().getName(),
                        f.getFlagCreator() == null
                                ? "-no flag creator-"
                                : f.getFlagCreator().getFirstName()))
                .collect(Collectors.joining("\n")));
        return pagedResponse;
    }

}
