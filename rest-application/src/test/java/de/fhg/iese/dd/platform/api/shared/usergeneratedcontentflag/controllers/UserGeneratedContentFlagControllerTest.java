/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.controllers.PostEventControllerFlagTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;

/**
 * Tests the UserGeneratedContentFlagController.
 * <br>
 * Note that only negative test cases are given here. A positive test case can be found in {@link
 * PostEventControllerFlagTest#flagGossipAndAlsoTestGenericFlagEndpoint}
 */
public class UserGeneratedContentFlagControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() throws Exception {

    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void testNotAuthorizedFlagForFlagCreator() throws Exception {

        assertOAuth2(get("/flag/"));
    }

}
