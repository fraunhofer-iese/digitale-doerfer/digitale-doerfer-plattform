/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostRequest;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SuggestionChatControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChats() throws Exception {
        Person chatParticipant1 = th.personSuggestionWorkerTenant1Horst;
        Person chatParticipant2 = th.personSebastianBauer;

        ClientChat expectedChat = createChat(chatParticipant1, chatParticipant2);

        // InternalWorkerChat von Horst abfragen --> Idea1
        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                        .headers(authHeadersFor(chatParticipant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                //.andExpect(jsonEquals(expectedChat))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(expectedChat.getId()))
                .andExpect(jsonPath("$[0].chatTopic").value(DorfFunkConstants.CHAT_TOPIC))
                .andExpect(jsonPath("$[0].chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$[0].chatParticipants[0].id")
                        .value(expectedChat.getChatParticipants().get(0).getId()))
                .andExpect(jsonPath("$[0].chatParticipants[1].id")
                        .value(expectedChat.getChatParticipants().get(1).getId()))
                .andExpect(jsonPath("$[0].chatParticipants[1].id")
                        .value(expectedChat.getChatParticipants().get(1).getId()))
                .andExpect(jsonPath("$[0].subjects", hasSize(1)))
                .andExpect(jsonPath("$[0].subjects[0].entityName")
                        .value("de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip"))
                .andExpect(jsonPath("$[0].subjects[0].entityId").value(th.gossip1withImages.getId()))
                .andExpect(jsonPath("$[1].chatTopic").value(LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC))
                .andExpect(jsonPath("$[1].chatParticipants[1].person.id").value(chatParticipant1.getId()))
                .andExpect(jsonPath("$[1].subjects", hasSize(1)))
                .andExpect(jsonPath("$[1].subjects[0].entityName").value(
                        "de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion"))
                .andExpect(jsonPath("$[1].subjects[0].entityId").value(th.suggestionIdea1.getId()));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsNoChatsNoSuggestionInternalWorkerChats() throws Exception {

        // There are 7 Suggestions created, but we want an empty response body
        assertEquals(7, chatRepository.count());
        assertEquals(0, chatMessageRepository.count());

        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsPersonIsSuggestionWorker() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Horst)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].chatTopic").value(LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC))
                .andExpect(jsonPath("$[0].chatParticipants[1].person.id").value(
                        th.personSuggestionWorkerTenant1Horst.getId()));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsPersonIsSuggestionFirstResponder() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].chatTopic").value(LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC))
                .andExpect(jsonPath("$[0].chatParticipants[0].person.id").value(
                        th.personSuggestionFirstResponderTenant1Hilde.getId()));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsPersonIsSuggestionFirstResponderAndSuggestionWorker()
            throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                .headers(authHeadersFor(th.personSuggestionWorkerAndFirstResponderTenant1Carmen)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].chatTopic").value(LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC))
                .andExpect(jsonPath("$[0].chatParticipants[0].person.id").value(
                        th.personSuggestionWorkerAndFirstResponderTenant1Carmen.getId()));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsNotACaseHandler() throws Exception {

        mockMvc.perform(get("/grapevine/suggestion/chat/own")
                .headers(authHeadersFor(th.personLaraSchaefer)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void getOwnChatsAndSuggestionInternalWorkerChatsUnauthorized() throws Exception {

        assertOAuth2(get("/grapevine/suggestion/chat/own"));
    }

    private ClientChat createChat(Person chatParticipant1, Person chatParticipant2) throws Exception {

        Post post = th.gossip1withImages;
        post.setCreator(chatParticipant1);
        th.postRepository.saveAndFlush(post);

        String message = "I have a question about your post! 💛";
        long sentTime = System.currentTimeMillis();

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message(message)
                .sentTime(sentTime)
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatParticipant2, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult.getResponse(), ClientChatStartOnPostConfirmation.class);

        return chatStartConfirmation.getChat();
    }

}
