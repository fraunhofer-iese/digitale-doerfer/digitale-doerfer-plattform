/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public abstract class BasePostControllerTest extends BaseServiceTest {

    @Autowired
    private PostInteractionRepository postInteractionRepository;
    @Autowired
    private HappeningParticipantRepository happeningParticipantRepository;

    @Autowired
    protected GrapevineTestHelper th;

    protected static final int DEFAULT_PAGE_SIZE = 10;

    protected ResultMatcher assertPostsInOrder(final List<? extends Post> posts, final int start, final int count) {
        return assertPostsInOrder(null, posts, start, count);
    }

    protected ResultMatcher assertPostsInOrder(@Nullable Person caller, List<? extends Post> posts,
            int page, int pageSize) {
        return mvcResult -> {
            final Post[] postIdsInOrder = posts.stream()
                    .skip((long) pageSize * page)
                    .limit(pageSize)
                    .toArray(Post[]::new);
            assertThat(postIdsInOrder).isNotEmpty();
            jsonPath("$.content", hasSize(postIdsInOrder.length)).match(mvcResult);
            jsonPath("$.numberOfElements").value(postIdsInOrder.length).match(mvcResult);
            for (int postIndex = 0; postIndex < postIdsInOrder.length; ++postIndex) {
                final Post post = postIdsInOrder[postIndex];
                assertPostAttributes(caller, ".content[" + postIndex + "]", post).match(mvcResult);
            }
        };
    }

    protected ResultMatcher assertPostAttributes(Person caller, Post post) {
        return assertPostAttributes(caller, "", post);
    }

    protected ResultMatcher assertPostAttributes(Post post) {
        return assertPostAttributes(null, "", post);
    }

    protected ResultMatcher assertPostAttributes(String additionalPrefix, Post post) {
        return assertPostAttributes(null, additionalPrefix, post);
    }

    protected ResultMatcher assertPostAttributes(@Nullable Person caller, String additionalPrefix, Post post) {
        return mvcResult -> {
            String prefix = additionalPrefix + "." + StringUtils.uncapitalize(post.getClass().getSimpleName());
            String typeName = Arrays.stream(StringUtils.splitByCharacterTypeCamelCase(post.getClass().getSimpleName()))
                    .map(StringUtils::upperCase)
                    .collect(Collectors.joining("_"));
            jsonPath("$" + additionalPrefix + ".id").value(post.getId()).match(mvcResult);
            jsonPath("$" + additionalPrefix + ".type").value(typeName).match(mvcResult);
            jsonPath("$" + prefix + ".id").value(post.getId()).match(mvcResult);
            Person creator = post.getCreator();
            if (creator == null) {
                jsonPath("$" + prefix + ".creator").isEmpty().match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".creator.id").value(creator.getId()).match(mvcResult);
                jsonPath("$" + prefix + ".creator.firstName").value(creator.getFirstName()).match(mvcResult);
                jsonPath("$" + prefix + ".creator.lastName").value(shorten(creator.getLastName())).match(mvcResult);
                MediaItem profilePicture = creator.getProfilePicture();
                if (profilePicture != null) {
                    jsonPath("$" + prefix + ".creator.profilePicture.id").value(profilePicture.getId())
                            .match(mvcResult);
                }
            }
            List<String> geoAreaIds = post.getGeoAreas().stream()
                    .map(GeoArea::getId)
                    .sorted()
                    .collect(Collectors.toList());
            jsonPath("$" + prefix + ".geoAreaId").value(geoAreaIds.get(0)).match(mvcResult);
            jsonPath("$" + prefix + ".geoAreaIds").value(geoAreaIds).match(mvcResult);
            jsonPath("$" + prefix + ".hiddenForOtherHomeAreas").value(post.isHiddenForOtherHomeAreas())
                    .match(mvcResult);
            if (post.getGroup() == null) {
                jsonPath("$" + prefix + ".groupId").doesNotExist().match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".groupId").value(post.getGroup().getId()).match(mvcResult);
            }
            if (post.getOrganization() == null) {
                jsonPath("$" + prefix + ".organizationId").doesNotExist().match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".organizationId").value(post.getOrganization().getId()).match(mvcResult);
            }
            List<MediaItem> images = post.getImages();
            if (CollectionUtils.isEmpty(images)) {
                jsonPath("$" + prefix + ".images", empty()).match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".images", hasSize(images.size())).match(mvcResult);
                for (int i = 0; i < images.size(); i++) {
                    MediaItem image = images.get(i);
                    jsonPath("$" + prefix + ".images[" + i + "].id").value(image.getId()).match(mvcResult);
                    if (image.getRelativeImageCropDefinition() != null) {
                        jsonPath("$" + prefix + ".images[" + i + "].relativeImageCropDefinition.relativeOffsetX")
                                .value(image.getRelativeImageCropDefinition().getRelativeOffsetX()).match(mvcResult);
                        jsonPath("$" + prefix + ".images[" + i + "].relativeImageCropDefinition.relativeOffsetY")
                                .value(image.getRelativeImageCropDefinition().getRelativeOffsetY()).match(mvcResult);
                        jsonPath("$" + prefix + ".images[" + i + "].relativeImageCropDefinition.relativeWidth")
                                .value(image.getRelativeImageCropDefinition().getRelativeWidth()).match(mvcResult);
                        jsonPath("$" + prefix + ".images[" + i + "].relativeImageCropDefinition.relativeHeight")
                                .value(image.getRelativeImageCropDefinition().getRelativeHeight()).match(mvcResult);
                    }
                }
            }
            if (CollectionUtils.isEmpty(post.getAttachments())) {
                jsonPath("$" + prefix + ".attachments").doesNotExist().match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".attachments", hasSize(post.getAttachments().size())).match(mvcResult);
                List<DocumentItem> attachments = post.getAttachments().stream()
                        .sorted(Comparator.comparing(DocumentItem::getTitle))
                        .collect(Collectors.toList());
                for (int i = 0; i < attachments.size(); i++) {
                    DocumentItem documentItem = attachments.get(i);
                    jsonPath("$" + prefix + ".attachments[" + i + "].id").value(documentItem.getId()).match(mvcResult);
                }
            }
            jsonPath("$" + prefix + ".text").value(post.getText()).match(mvcResult);
            jsonPath("$" + prefix + ".created").value(post.getCreated()).match(mvcResult);
            jsonPath("$" + prefix + ".lastModified").value(post.getLastModified()).match(mvcResult);
            jsonPath("$" + prefix + ".commentCount").value(post.getCommentCount()).match(mvcResult);
            jsonPath("$" + prefix + ".commentsAllowed").value(post.isCommentsAllowed()).match(mvcResult);
            jsonPath("$" + prefix + ".likeCount").value(post.getLikeCount()).match(mvcResult);
            if (caller == null) {
                jsonPath("$" + prefix + ".liked").doesNotExist().match(mvcResult);
            } else {
                boolean liked = postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(post, caller);
                jsonPath("$" + prefix + ".liked").value(liked).match(mvcResult);
            }
            jsonPath("$" + prefix + ".lastActivity").value(post.getLastActivity()).match(mvcResult);
            jsonPath("$" + prefix + ".createdLocation").doesNotExist().match(mvcResult);
            Address customAddress = post.getCustomAddress();
            if (customAddress == null) {
                jsonPath("$" + prefix + ".customAddress").doesNotExist().match(mvcResult);
            } else {
                jsonPath("$" + prefix + ".customAddress.name")
                        .value(customAddress.getName()).match(mvcResult);
                jsonPath("$" + prefix + ".customAddress.id")
                        .value(customAddress.getId()).match(mvcResult);
                jsonPath("$" + prefix + ".customAddress.street")
                        .value(customAddress.getStreet()).match(mvcResult);
                jsonPath("$" + prefix + ".customAddress.zip")
                        .value(customAddress.getZip()).match(mvcResult);
                jsonPath("$" + prefix + ".customAddress.city")
                        .value(customAddress.getCity()).match(mvcResult);
                jsonPath("$" + prefix + ".customAddress.verified")
                        .value(customAddress.isVerified()).match(mvcResult);
                if (customAddress.getGpsLocation() != null) {
                    jsonPath("$" + prefix + ".customLocation.latitude")
                            .value(customAddress.getGpsLocation().getLatitude()).match(mvcResult);
                    jsonPath("$" + prefix + ".customLocation.longitude")
                            .value(customAddress.getGpsLocation().getLongitude()).match(mvcResult);
                    jsonPath("$" + prefix + ".customAddress.gpsLocation.latitude")
                            .value(customAddress.getGpsLocation().getLatitude()).match(mvcResult);
                    jsonPath("$" + prefix + ".customAddress.gpsLocation.longitude")
                            .value(customAddress.getGpsLocation().getLongitude()).match(mvcResult);
                }
            }
            if (post instanceof ExternalPost) {
                ExternalPost externalPost = (ExternalPost) post;
                jsonPath("$" + prefix + ".externalId").value(externalPost.getExternalId()).match(mvcResult);
                jsonPath("$" + prefix + ".authorName").value(externalPost.getAuthorName()).match(mvcResult);
                jsonPath("$" + prefix + ".categories").value(externalPost.getCategories()).match(mvcResult);
                jsonPath("$" + prefix + ".url").value(externalPost.getUrl()).match(mvcResult);
                NewsSource newsSource = externalPost.getNewsSource();
                if (newsSource != null) {
                    if (!newsSource.getSiteName().contains("$")) {
                        //templated site names can not be asserted here
                        jsonPath("$" + prefix + ".siteName").value(newsSource.getSiteName()).match(mvcResult);
                    }
                    jsonPath("$" + prefix + ".sitePicture.id").value(newsSource.getSiteLogo().getId()).match(mvcResult);
                }
            }
            if (post instanceof Happening) {
                Happening happening = (Happening) post;
                jsonPath("$" + prefix + ".startTime").value(happening.getStartTime()).match(mvcResult);
                jsonPath("$" + prefix + ".endTime").value(happening.getEndTime()).match(mvcResult);
                jsonPath("$" + prefix + ".allDayEvent").value(happening.isAllDayEvent()).match(mvcResult);
                jsonPath("$" + prefix + ".organizer").value(happening.getOrganizer()).match(mvcResult);
                jsonPath("$" + prefix + ".participantCount").value(happening.getParticipantCount()).match(mvcResult);
                if (caller != null) {
                    boolean participated = happeningParticipantRepository.existsByHappeningAndPerson(happening, caller);
                    jsonPath("$" + prefix + ".participated").value(participated).match(mvcResult);
                } else {
                    jsonPath("$" + prefix + ".participated").doesNotExist().match(mvcResult);
                }
                if (customAddress != null) {
                    jsonPath("$" + prefix + ".venue")
                            .value(customAddress.getName()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.name")
                            .value(customAddress.getName()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.id")
                            .value(customAddress.getId()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.street")
                            .value(customAddress.getStreet()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.zip")
                            .value(customAddress.getZip()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.city")
                            .value(customAddress.getCity()).match(mvcResult);
                    jsonPath("$" + prefix + ".venueAddress.verified")
                            .value(customAddress.isVerified()).match(mvcResult);
                    if (customAddress.getGpsLocation() != null) {
                        jsonPath("$" + prefix + ".venueAddress.gpsLocation.latitude")
                                .value(customAddress.getGpsLocation().getLatitude()).match(mvcResult);
                        jsonPath("$" + prefix + ".venueAddress.gpsLocation.longitude")
                                .value(customAddress.getGpsLocation().getLongitude()).match(mvcResult);
                    }
                }
            }
            if (post instanceof Suggestion) {
                Suggestion suggestion = (Suggestion) post;
                jsonPath("$" + prefix + ".suggestionCategory.id").value(
                        suggestion.getSuggestionCategory().getId()).match(mvcResult);
                jsonPath("$" + prefix + ".suggestionCategory.shortName")
                        .value(suggestion.getSuggestionCategory().getShortName()).match(mvcResult);
                jsonPath("$" + prefix + ".suggestionCategory.displayName")
                        .value(suggestion.getSuggestionCategory().getDisplayName()).match(mvcResult);
                jsonPath("$" + prefix + ".suggestionStatus")
                        .value(suggestion.getSuggestionStatus().toString()).match(mvcResult);
            }
            if (post instanceof Trade) {
                Trade trade = (Trade) post;
                jsonPath("$" + prefix + ".tradingCategoryEntity.id").value(trade.getTradingCategory().getId())
                        .match(mvcResult);
                jsonPath("$" + prefix + ".tradingStatus").value(trade.getTradingStatus().toString()).match(mvcResult);
            }
            if (post instanceof SpecialPost) {
                SpecialPost specialPost = (SpecialPost) post;
                if (CollectionUtils.isEmpty(specialPost.getCustomAttributes())) {
                    jsonPath("$" + prefix + ".customAttributes").doesNotExist().match(mvcResult);
                } else {
                    jsonPath("$" + prefix + ".customAttributes")
                            .value(specialPost.getCustomAttributes()).match(mvcResult);
                }
            }
        };
    }

}
