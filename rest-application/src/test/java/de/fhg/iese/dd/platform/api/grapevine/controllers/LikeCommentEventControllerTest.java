/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikeCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikeCommentRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.LikeComment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.LikeCommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LikeCommentEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private LikeCommentRepository likeCommentRepository;

    @Autowired
    private PostRepository postRepository;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void likeCommentRequest_verifyingPushMessage() throws Exception {

        Comment comment = th.offer1Comment1;
        assertEquals(0, comment.getLikeCount());

        Person liker = th.personSusanneChristmann;
        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(comment, liker));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        waitForEventProcessing();

        ClientCommentChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(liker,
                        ClientCommentChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED);

        assertThat(pushedEvent.getCommentId()).isEqualTo(comment.getId());
    }

    @Test
    public void likeCommentRequest_withDoubleLike() throws Exception {

        Person liker = th.personSusanneChristmann;
        Comment comment = th.offer1Comment1;

        final Comment commentToBeLiked = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentToBeLiked.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentToBeLiked,
                liker));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        final Comment commentAfterLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentAfterLike.getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentAfterLike,
                liker));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(comment.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_ALREADY_LIKED));

        final Comment commentAfterSecondLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentAfterSecondLike.getLikeCount());
    }

    @Test
    public void likeCommentRequest_relike() throws Exception {

        Comment comment = th.offer1Comment1;
        Person liker = th.personSusanneChristmann;

        final LikeComment likeComment = LikeComment.builder()
                .person(liker)
                .comment(comment)
                .liked(false)
                .build();

        likeCommentRepository.saveAndFlush(likeComment);

        comment.setLikeCount(0);
        comment = commentRepository.saveAndFlush(comment);

        final Comment commentFromRepoAfterUpdate = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentFromRepoAfterUpdate.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentFromRepoAfterUpdate,
                liker));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        final Comment commentFromRepoAfterLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentFromRepoAfterLike.getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentFromRepoAfterLike,
                liker));
    }

    @Test
    public void likeCommentRequest_wrongTenant() throws Exception {

        Gossip gossip = th.tenant2Gossip1;
        gossip.setHiddenForOtherHomeAreas(true);
        postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(th.tenant2Gossip1Comment.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void likeCommentRequest_wrongAppVariant() throws Exception {

        Gossip gossip = th.tenant2Gossip1;
        gossip.setHiddenForOtherHomeAreas(true);
        postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personAnnikaSchneider, th.appVariant3Tenant3))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(th.tenant2Gossip1Comment.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void likeCommentRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/comment/event/likeCommentRequest")
                        .contentType(contentType)
                        .content(json(new ClientLikeCommentRequest(th.gossip2Comment1.getId()))),
                th.appVariant2Tenant1And2,
                th.personSusanneChristmann);
    }

    @Test
    public void likeCommentRequest_InvalidParameters() throws Exception {

        //invalid id
        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest("unknown-comment"))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        //deleted comment
        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                .contentType(contentType)
                .content(json(new ClientLikeCommentRequest(th.gossip2Comment2Deleted.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        //no event
        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void unlikeCommentRequest_verifyingPushMessage() throws Exception {

        // Setup liked state in repo
        Comment commentToBeUnliked = th.offer1Comment1;

        likeCommentRepository.saveAndFlush(LikeComment.builder()
                .comment(commentToBeUnliked)
                .person(th.personSusanneChristmann)
                .liked(true)
                .build());

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(commentToBeUnliked.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(0))
                .andExpect(jsonPath("$.comment.liked").value(false));

        waitForEventProcessing();

        ClientCommentChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(th.personSusanneChristmann,
                        ClientCommentChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED);

        assertThat(pushedEvent.getCommentId()).isEqualTo(commentToBeUnliked.getId());
    }

    @Test
    public void unlikeCommentRequest_withDoubleUnlike() throws Exception {

        Comment comment = th.offer1Comment1;
        Person unliker = th.personSusanneChristmann;

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                        .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        assertEquals(1, commentRepository.findById(comment.getId()).get().getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(comment, unliker));

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(0))
                .andExpect(jsonPath("$.comment.liked").value(false));

        final Comment commentAfterUnlike = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentAfterUnlike.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(comment, unliker));

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(comment.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_ALREADY_UNLIKED));

        final Comment commentAfterSecondUnlike = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentAfterSecondUnlike.getLikeCount());
    }

    @Test
    public void unlikeCommentRequest_likeCommentNotFound() throws Exception {

        final Comment commentToBeUnliked = th.offer1Comment1;
        Person unliker = th.personSusanneChristmann;

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentToBeUnliked,
                unliker));

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(commentToBeUnliked.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_LIKED));
    }

    @Test
    public void unlikeCommentRequest_wrongTenant() throws Exception {

        Gossip gossip = th.gossip2WithComments;
        gossip.setHiddenForOtherHomeAreas(true);
        postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personAnnikaSchneider, th.appVariant3Tenant3))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(th.gossip2Comment3.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void unlikeCommentRequest_wrongAppVariant() throws Exception {

        Gossip gossip = th.gossip3WithComment;
        gossip.setHiddenForOtherHomeAreas(true);
        postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant3Tenant3))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(th.gossip3Comment1withOnlyImages.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void unlikeCommentRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/comment/event/unlikeCommentRequest")
                        .contentType(contentType)
                        .content(json(new ClientUnlikeCommentRequest(th.gossip2Comment1.getId()))),
                th.appVariant2Tenant1And2,
                th.personSusanneChristmann);
    }

    @Test
    public void unlikeCommentRequest_InvalidParameters() throws Exception {

        //invalid id
        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest("unknown-comment"))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        //deleted comment
        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(th.gossip2Comment2Deleted.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        //missing event
        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personSusanneChristmann))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

}
