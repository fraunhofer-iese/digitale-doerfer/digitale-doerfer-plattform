/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.dataprivacy.clientmodel.ClientDataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.business.shared.callback.exceptions.ExternalSystemCallFailedException;
import de.fhg.iese.dd.platform.business.shared.callback.services.ExternalSystemResponse;
import de.fhg.iese.dd.platform.business.shared.callback.services.IExternalSystemCallbackService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ExternalDataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ExternalPersonDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInternalDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.test.mocks.TestDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.test.mocks.TestOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflow;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileDownloadService;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

@Tag("data-privacy")
@ActiveProfiles({"test", "data-privacy-service-test"})
public class DataPrivacyServiceTest extends BaseServiceTest {

    private static final String EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION = "/dataprivacy/report";
    private static final String EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION = "/dataprivacy/delete";
    private static final long DELAY_CONTINUE_PROCESSING = TimeUnit.SECONDS.toMillis(30);

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private DataPrivacyWorkflowRepository dataPrivacyWorkflowRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantRepository dataPrivacyWorkflowAppVariantRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantStatusRecordRepository dataPrivacyWorkflowAppVariantStatusRecordRepository;
    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;
    @Autowired
    private ICommonDataPrivacyService commonDataPrivacyService;
    @Autowired
    private TestTimeService testTimeService;
    @Autowired
    private TestDefaultTeamNotificationPublisher testDefaultTeamNotificationPublisher;
    @MockBean
    private IExternalSystemCallbackService externalSystemCallbackServiceMock;
    @MockBean
    private IFileDownloadService fileDownloadServiceMock;
    @SpyBean
    private IInternalDataPrivacyService internalDataPrivacyServiceSpy;
    @SpyBean
    private TestFileStorage testFileStorageSpy;
    @SpyBean
    private TestOauthManagementService testOauthManagementServiceSpy;
    @SpyBean
    private TestEmailSenderService testEmailSenderServiceSpy;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
        testDefaultTeamNotificationPublisher.reset();
        //this is required since the IFileDownloadService is implemented by the mocked ExternalSystemCallbackService
        when(fileDownloadServiceMock.downloadFromExternalSystem(any(), any(), any(), anyList()))
                .thenReturn(new byte[]{0});
    }

    @Test
    public void dataPrivacyCollection_ProcessingFailed() throws Exception {

        // simulate a failure during the creation of data privacy report
        doThrow(RuntimeException.class).when(internalDataPrivacyServiceSpy).collectInternalUserData(any(), any());

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService.startDataPrivacyDataCollection(person,
                DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FAILED);

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(1);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyCollection_ZipFileCreationFailed() throws Exception {

        // simulate a failure during the creation of the zip file
        doThrow(FileStorageException.class).when(
                testFileStorageSpy).createZipFile(Mockito.anyString(), Mockito.anyList(), Mockito.anyList());

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService.startDataPrivacyDataCollection(person,
                DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed since the creation of the zip file failed
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FAILED);
        // no zip file should be created
        assertThat(testFileStorage.findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER))
                .as("file storage should not contain Datenschutzbericht.zip")
                .noneMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip"));

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FAILED);
        assertThat(testFileStorage.findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER))
                .as("file storage should not contain Datenschutzbericht.zip")
                .noneMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip"));

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(1);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyCollection_SendingEmailFailed() throws Exception {

        // simulate a failure while sending data privacy report via email
        doThrow(RuntimeException.class).when(testEmailSenderServiceSpy).sendEmail(any(), any(), any(), any(), any(),
                anyList(), anyBoolean());

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService.startDataPrivacyDataCollection(person,
                DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FAILED);

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final Triple<String, Tenant, TeamNotificationMessage> sendEmailFailedMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0);
        final Triple<String, Tenant, TeamNotificationMessage> createPrivacyReportFailedMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().get(1);
        assertThat(sendEmailFailedMessage.getRight().getText()).contains(person.getEmail());
        assertThat(sendEmailFailedMessage.getRight().getText()).contains(person.getId());
        assertThat(createPrivacyReportFailedMessage.getRight().getText()).contains(dataCollection.getId());
        assertThat(createPrivacyReportFailedMessage.getRight().getText()).contains(person.getId());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyCollection_NoExternalSystem() throws Exception {

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService.startDataPrivacyDataCollection(person,
                DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        //it should already be finished, because no external system is called
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_FileResponse() throws Exception {

        //one external system, one file response
        // -> call to external system
        // -> file from external system + FINISHED
        // -> worker triggers finalization
        // -> check result, especially if file from external system is included

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //external systems sends data
        String originalFilename = "new-file.jpg";
        MockMultipartFile expectedFile = new MockMultipartFile("file", originalFilename, "image/jpg",
                th.loadTestResource("testImage.jpg"));

        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(expectedFile)
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(status().isOk());

        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);

        //data collection can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataCollection.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataCollection);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
        checkDataPrivacyReport(dataCollection, Collections.singleton(originalFilename));
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_FileResponse_DuringTriggerCall() throws Exception {

        //one external system, one file response
        // -> call to external system that does not return immediately
        // -> file from external system + FINISHED
        // -> call to external system returns with 200
        // -> worker triggers finalization
        // -> check result, especially if file from external system is included

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        Semaphore externalSystemTriggerCallSemaphore = new Semaphore(0);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMockWaiting(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK, externalSystemTriggerCallSemaphore);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.OPEN);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //external systems sends data
        String originalFilename = "new-file.jpg";
        MockMultipartFile expectedFile = new MockMultipartFile("file", originalFilename, "image/jpg",
                th.loadTestResource("testImage.jpg"));

        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(expectedFile)
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(status().isOk());

        waitForProcessing();

        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);

        //now the trigger call of the external system should return
        externalSystemTriggerCallSemaphore.release();

        //wait for the callback to be run
        waitForProcessing();

        //check that the status is still finished
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
        //that's the just returned call, it should be added before
        assertPreviousDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //data collection can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataCollection.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataCollection);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
        checkDataPrivacyReport(dataCollection, Collections.singleton(originalFilename));
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_TwoFilesResponse() throws Exception {

        //one external system, two files response
        // -> call to external system
        // -> file from external system + IN_PROGRESS
        // -> file from external system + FINISHED
        // -> worker triggers finalization
        // -> check result, especially if files from external system is included

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //external systems sends data
        String originalFilename1 = "new-file.jpg";

        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(new MockMultipartFile("file", originalFilename1, "image/jpg",
                        th.loadTestResource("testImage.jpg")))
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(status().isOk());
        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);

        //data collection does not finalize, since it is still waiting for the next file
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);

        //external systems sends next data
        String originalFilename2 = "new-file.jpg";

        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(new MockMultipartFile("file", originalFilename1, "image/jpg",
                        th.loadTestResource("testImage.jpg")))
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(status().isOk());
        waitForProcessing();

        //data collection can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataCollection.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataCollection);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
        checkDataPrivacyReport(dataCollection, Arrays.asList(originalFilename1, originalFilename2));
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_FailedResponse() throws Exception {

        //one external system, description + failed response
        // -> call to external system
        // -> worker can not trigger finalization, still waiting
        // -> statusMessage from external system + FAILED
        // -> worker triggers finalization
        // -> check result, especially if files from external system is included

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //external systems sends description and failed
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FAILED.name())
                .param("statusMessage", "Is kaputt, wir schicken es per Brief"))
                .andExpect(status().isOk());
        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        //data collection finalizes report
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataCollection.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataCollection);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_PersonNotFoundResponse() throws Exception {

        //one external system, description + failed response
        // -> call to external system
        // -> worker can not trigger finalization, still waiting
        // -> statusMessage from external system + PERSON_NOT_FOUND
        // -> worker triggers finalization
        // -> check result, especially if files from external system is included

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //external systems sends description and failed
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND.name())
                .param("statusMessage", "Wahrscheinlich untergetaucht"))
                .andExpect(status().isOk());
        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND);

        //data collection finalizes report
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataCollection.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataCollection);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_Timeout() throws Exception {

        //one external system, call successful, but no reply
        // -> call to external system
        // -> worker can not trigger finalization (timeout not elapsed)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.OK);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //data collection can not run because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data collection can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_CallUnsuccessful() throws Exception {

        //one external system, system returns 404 and does not send responses
        // -> call to external system -> 404
        // -> worker can not trigger re-call (wait time for re-call not elapsed)
        //manipulate time (+wait time)
        // -> worker triggers re-call (wait time for re-call elapsed)
        // -> call to external system -> 404
        // -> worker triggers finalization (timeout not elapsed)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                setupReportCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION,
                        HttpStatus.NOT_FOUND);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //data collection can not finish because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        //wait time for re-calling is elapsed
        testTimeService.setOffset(
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getWaitTimeAfterFailedCall());
        //data collection calls the external system again
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 1);

        //overall timeout for data collection is elapsed
        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data collection can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyCollection_ExternalSystem_CallFailsHard() throws Exception {

        //one external system, call fails hard
        // -> call to external system -> no ip / timeout (exception)
        // -> worker can not trigger re-call (wait time for re-call not elapsed)
        // -> call to external system -> no ip / timeout (exception)
        //manipulate time (+wait time)
        // -> worker triggers re-call (wait time for re-call elapsed)
        // -> call to external system -> no ip / timeout (exception)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                ArgumentCaptor.forClass(ExternalDataPrivacyReportRequest.class);
        when(externalSystemCallbackServiceMock
                .callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION),
                        requestCaptor.capture(),
                        any()))
                .thenThrow(new ExternalSystemCallFailedException("PENG 💣"));

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService
                .startDataPrivacyDataCollection(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        assertThat(dataCollection.getPerson()).isEqualTo(person);
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 0);

        //data collection can not finish because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        //wait time for re-calling is elapsed
        testTimeService.setOffset(
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getWaitTimeAfterFailedCall());
        //data collection calls the external system again
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledReport(externalAppVariant, requestCaptor, dataCollection, 1);

        //overall timeout for data collection is elapsed
        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data collection can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataCollection, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataCollection, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);
        checkDataPrivacyReport(dataCollection, Collections.emptyList());

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataCollection.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyDeletion_ProcessingFailed() throws Exception {

        final Person person = th.personRegularAnna;

        // simulate a failure during the deletion of person data
        doThrow(RuntimeException.class).when(internalDataPrivacyServiceSpy).deleteInternalUserData(any(), any());

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(1);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataDeletion.getId());
        assertThat(lastMessageText).contains(person.getId());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyDeletion_DeletionAtAuth0Failed() throws Exception {

        final Person person = th.personRegularAnna;

        // simulate a failure during the deletion of the user at auth0
        doThrow(OauthManagementException.class).when(
                testOauthManagementServiceSpy).deleteUser(Mockito.eq(person.getOauthId()));

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed since the deletion at auth0 failed
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyDeletion_SendingEmailFailed() throws Exception {

        final Person person = th.personRegularAnna;

        // simulate a failure while sending data privacy report via email
        doThrow(RuntimeException.class).when(testEmailSenderServiceSpy).sendEmail(any(), any(), any(), any(), any(),
                anyList(), anyBoolean());

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        // it should have status failed
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        // failed data collections should not be processed
        assertThat(commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows()).isEqualTo(0);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final Triple<String, Tenant, TeamNotificationMessage> sendEmailFailedMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0);
        final Triple<String, Tenant, TeamNotificationMessage> deletionPersonDataFailedMessage =
                testDefaultTeamNotificationPublisher.getSentMessages().get(1);
        assertThat(sendEmailFailedMessage.getRight().getText()).contains(person.getEmail());
        assertThat(sendEmailFailedMessage.getRight().getText()).contains(person.getId());
        assertThat(deletionPersonDataFailedMessage.getRight().getText()).contains(dataDeletion.getId());
        assertThat(deletionPersonDataFailedMessage.getRight().getText()).contains(person.getId());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        // verify that email has not been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

    @Test
    public void dataPrivacyDeletion_NoExternalSystem() throws Exception {

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        //it should already be finished, because no external system is called
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_Response() throws Exception {

        //one external system, one file response
        // -> call to external system
        // -> response from external system + FINISHED
        // -> worker triggers finalization
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.OK);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //external systems sends data
        mockMvc.perform(post("/dataprivacy/deletion/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(status().isOk());

        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);

        //data deletion can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataDeletion.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_TwoFilesResponse() throws Exception {

        //one external system, two files response
        // -> call to external system
        // -> response from external system + IN_PROGRESS
        // -> response from external system + FINISHED
        // -> worker triggers finalization
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.OK);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //external systems sends data
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(status().isOk());
        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);

        //data deletion does not finalize, since it is still waiting for the next file
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);

        //external systems sends next data
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(status().isOk());
        waitForProcessing();

        //data deletion can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataDeletion.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_FailedResponse() throws Exception {

        //one external system, description + failed response
        // -> call to external system
        // -> worker can not trigger finalization, still waiting
        // -> response from external system + FAILED
        // -> worker triggers finalization
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.OK);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //external systems failed
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("statusMessage", "Habe mir in der Wörterpresse die Funger eingeklemmt!")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FAILED.name()))
                .andExpect(status().isOk());
        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        //data deletion finalizes workflow
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataDeletion.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataDeletion.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_PersonNotFoundResponse() throws Exception {

        //one external system, one file response
        // -> call to external system
        // -> response from external system + PERSON_NOT_FOUND
        // -> worker triggers finalization
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.OK);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();

        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //external systems sends data
        mockMvc.perform(post("/dataprivacy/deletion/external")
                .header("apiKey", externalAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("statusMessage", "Wir haben sogar unter den Schränken geschaut, nix da!")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND.name()))
                .andExpect(status().isOk());

        waitForProcessing();
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND);

        //data deletion can be finalized
        //it should not be able to process it, since the last processing timestamp is too early
        int processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(0);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);

        //now we set it to a later time, so that it can be picked up
        dataDeletion.setLastProcessed(timeServiceMock.currentTimeMillisUTC() - DELAY_CONTINUE_PROCESSING - 1);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        processedWorkflows = commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        assertThat(processedWorkflows).isEqualTo(1);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND);
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_Timeout() throws Exception {

        //one external system, call successful, but no reply
        // -> call to external system
        // -> worker can not trigger finalization (timeout not elapsed)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.OK);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //data deletion can not run because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data deletion can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataDeletion.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_CallUnsuccessful() throws Exception {

        //one external system, system returns 404 and does not send responses
        // -> call to external system -> 404
        // -> worker can not trigger re-call (wait time for re-call not elapsed)
        //manipulate time (+wait time)
        // -> worker triggers re-call (wait time for re-call elapsed)
        // -> call to external system -> 404
        // -> worker triggers finalization (timeout not elapsed)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                setupDeletionCallBackServiceMock(externalAppVariant, EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION,
                        HttpStatus.NOT_FOUND);

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //data deletion can not finish because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        //wait time for re-calling is elapsed
        testTimeService.setOffset(
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getWaitTimeAfterFailedCall());
        //data deletion calls the external system again
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 1);

        //overall timeout for data collection is elapsed
        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data deletion can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataDeletion.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    @Test
    public void dataPrivacyDeletion_ExternalSystem_CallFailsHard() throws Exception {

        //one external system, call fails hard
        // -> call to external system -> no ip / timeout (exception)
        // -> worker can not trigger re-call (wait time for re-call not elapsed)
        // -> call to external system -> no ip / timeout (exception)
        //manipulate time (+wait time)
        // -> worker triggers re-call (wait time for re-call elapsed)
        // -> call to external system -> no ip / timeout (exception)
        //manipulate time (+timeout)
        // -> worker triggers finalization (timeout elapsed)
        // -> check result

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant externalAppVariant = createExternalAppVariant(person);
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                ArgumentCaptor.forClass(ExternalPersonDeleteRequest.class);
        when(externalSystemCallbackServiceMock
                .callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION),
                        requestCaptor.capture(),
                        any()))
                .thenThrow(new ExternalSystemCallFailedException("PENG 💣"));

        commonDataPrivacyService.startDataPrivacyDataDeletion(person, DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        DataPrivacyDataDeletion dataDeletion = (DataPrivacyDataDeletion) dataPrivacyWorkflowRepository.findAll().get(0);

        assertThat(dataPrivacyWorkflowRepository.count()).isEqualTo(1);
        assertThat(dataDeletion.getPerson()).isEqualTo(person);

        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 0);

        //data collection can not finish because timeout is not elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //time for calling the external system again was not elapsed
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);

        //wait time for re-calling is elapsed
        testTimeService.setOffset(
                dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getWaitTimeAfterFailedCall());
        //data deletion calls the external system again
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.CALL_FAILED);
        //check that the external system was called properly
        checkExternalSystemWasCalledDeletion(externalAppVariant, requestCaptor, dataDeletion, 1);

        //overall timeout for data collection is elapsed
        testTimeService.setOffset(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().getTimeout());

        //data deletion can run because timeout elapsed
        commonDataPrivacyService.processAllUnfinishedDataPrivacyWorkflows();
        waitForProcessing();
        //timeout elapsed should not trigger the call again
        Mockito.verifyNoInteractions(externalSystemCallbackServiceMock);
        assertDataPrivacyWorkflowStatus(dataDeletion, DataPrivacyWorkflowStatus.FINISHED);
        assertDataPrivacyWorkflowAppVariantStatus(dataDeletion, externalAppVariant,
                DataPrivacyWorkflowAppVariantStatus.FAILED);

        assertThat(testDefaultTeamNotificationPublisher.getSentMessages()).hasSize(2);
        final String lastMessageText =
                testDefaultTeamNotificationPublisher.getSentMessages().get(0).getRight().getText();
        assertThat(lastMessageText).contains(dataDeletion.getId());
        assertThat(lastMessageText).contains(person.getId());
        assertThat(lastMessageText).contains(externalAppVariant.getAppVariantIdentifier());
    }

    private void checkDataPrivacyReport(DataPrivacyDataCollection dataCollection, Collection<String> fileNames) {

        //verify that email has been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(
                dataPrivacyConfig.getSenderEmailAddressAccount(),
                dataCollection.getPerson().getEmail()))
                .hasSize(1);
        TestEmailSenderService.TestEmail mail = emailSenderServiceMock.getEmails().get(0);
        assertThat(mail.getSubject()).isEqualTo("Deine angeforderten Daten");
        assertThat(mail.getText()).contains(th.personRegularAnna.getFirstName());
        assertThat(mail.isHtml()).isTrue();

        //Check that zip file has been written and contains the right files
        assertThat(testFileStorage.findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER))
                .as("file storage should contain Datenschutzbericht.zip")
                .anyMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip"));

        List<String> zippedFiles = testFileStorage.getZippedFiles(
                testFileStorage.appendFileName(dataCollection.getInternalFolderPath(), "Datenschutzbericht.zip"));

        for (String fileName : fileNames) {
            assertThat(zippedFiles)
                    .as("zip file should contain " + fileName + " from external system")
                    .anyMatch(zippedFile -> zippedFile.endsWith(fileName));
        }
    }

    private void checkExternalSystemWasCalledReport(AppVariant externalAppVariant,
            ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor, DataPrivacyWorkflow dataPrivacyWorkflow,
            int retryNumber) {
        Mockito.verify(externalSystemCallbackServiceMock)
                .callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(EXTERNAL_SYSTEM_URL_PATH_DATA_COLLECTION),
                        any(ExternalDataPrivacyReportRequest.class),
                        any());

        assertThat(requestCaptor.getValue().getDataCollectionId()).isEqualTo(dataPrivacyWorkflow.getId());
        assertThat(requestCaptor.getValue().getPersonId()).isEqualTo(dataPrivacyWorkflow.getPerson().getId());
        assertThat(requestCaptor.getValue().getRetryNumber()).isEqualTo(retryNumber);
        Mockito.clearInvocations(externalSystemCallbackServiceMock);
    }

    private void checkExternalSystemWasCalledDeletion(AppVariant externalAppVariant,
            ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor, DataPrivacyWorkflow dataPrivacyWorkflow,
            int retryNumber) {
        Mockito.verify(externalSystemCallbackServiceMock)
                .callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(EXTERNAL_SYSTEM_URL_PATH_DATA_DELETION),
                        any(ExternalPersonDeleteRequest.class),
                        any());

        assertThat(requestCaptor.getValue().getDataDeletionId()).isEqualTo(dataPrivacyWorkflow.getId());
        assertThat(requestCaptor.getValue().getPersonId()).isEqualTo(dataPrivacyWorkflow.getPerson().getId());
        assertThat(requestCaptor.getValue().getRetryNumber()).isEqualTo(retryNumber);
        Mockito.clearInvocations(externalSystemCallbackServiceMock);
    }

    private ArgumentCaptor<ExternalDataPrivacyReportRequest> setupReportCallBackServiceMock(AppVariant externalAppVariant,
            String externalSystemUrlPath, HttpStatus statusToReturn) {
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                ArgumentCaptor.forClass(ExternalDataPrivacyReportRequest.class);
        final ExternalSystemResponse<String> response = ExternalSystemResponse.<String>builder()
                .response(ResponseEntity.status(statusToReturn).build())
                .build();
        when(externalSystemCallbackServiceMock
                .<String>callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(externalSystemUrlPath),
                        requestCaptor.capture(),
                        any()))
                .thenReturn(response);
        return requestCaptor;
    }

    private ArgumentCaptor<ExternalDataPrivacyReportRequest> setupReportCallBackServiceMockWaiting(
            AppVariant externalAppVariant, String externalSystemUrlPath, HttpStatus statusToReturn,
            Semaphore externalSystemTriggerCallSemaphore
    ) {
        ArgumentCaptor<ExternalDataPrivacyReportRequest> requestCaptor =
                ArgumentCaptor.forClass(ExternalDataPrivacyReportRequest.class);
        final ExternalSystemResponse<String> response = ExternalSystemResponse.<String>builder()
                .response(ResponseEntity.status(statusToReturn).build())
                .build();
        when(externalSystemCallbackServiceMock
                .<String>callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(externalSystemUrlPath),
                        requestCaptor.capture(),
                        any()))
                .then(invocation -> {
                    log.debug("external system is processing..");
                    externalSystemTriggerCallSemaphore.acquire();
                    log.debug("..external system is processing finished");
                    return response;
                });
        return requestCaptor;
    }

    private ArgumentCaptor<ExternalPersonDeleteRequest> setupDeletionCallBackServiceMock(AppVariant externalAppVariant,
            String externalSystemUrlPath, HttpStatus statusToReturn) {
        ArgumentCaptor<ExternalPersonDeleteRequest> requestCaptor =
                ArgumentCaptor.forClass(ExternalPersonDeleteRequest.class);
        final ExternalSystemResponse<String> response = ExternalSystemResponse.<String>builder()
                .response(ResponseEntity.status(statusToReturn).build())
                .build();
        when(externalSystemCallbackServiceMock
                .<String>callExternalSystem(
                        Mockito.eq(externalAppVariant),
                        Mockito.eq(HttpMethod.POST),
                        Mockito.eq(externalSystemUrlPath),
                        requestCaptor.capture(),
                        any()))
                .thenReturn(response);
        return requestCaptor;
    }

    private AppVariant createExternalAppVariant(Person person) {
        AppVariant externalAppVariant = th.app1Variant1;
        externalAppVariant.setApiKey1("secure");
        externalAppVariant.setExternalApiKey("super-secure");
        externalAppVariant.setCallBackUrl("https://iese.de");
        th.appVariantRepository.saveAndFlush(externalAppVariant);

        assertThat(externalAppVariant.hasExternalSystem()).isTrue();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(externalAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());
        return externalAppVariant;
    }

    private void waitForProcessing() throws InterruptedException {
        waitForEventProcessing();
    }

    private void assertDataPrivacyWorkflowAppVariantStatus(DataPrivacyWorkflow dataPrivacyWorkflow,
            AppVariant appVariant,
            DataPrivacyWorkflowAppVariantStatus expectedStatus) {
        DataPrivacyWorkflowAppVariant dataCollectionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.findByDataPrivacyWorkflowAndAppVariant(
                        dataPrivacyWorkflow,
                        appVariant).get();
        DataPrivacyWorkflowAppVariantStatus lastStatus =
                dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(
                                dataCollectionAppVariant)
                        .map(DataPrivacyWorkflowAppVariantStatusRecord::getStatus).orElse(null);
        assertThat(lastStatus).isEqualTo(expectedStatus);
    }

    private void assertPreviousDataPrivacyWorkflowAppVariantStatus(DataPrivacyWorkflow dataPrivacyWorkflow,
            AppVariant appVariant,
            DataPrivacyWorkflowAppVariantStatus expectedStatus) {
        DataPrivacyWorkflowAppVariant dataCollectionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.findByDataPrivacyWorkflowAndAppVariant(
                        dataPrivacyWorkflow,
                        appVariant).get();
        final List<DataPrivacyWorkflowAppVariantStatusRecord> statusRecords =
                dataPrivacyWorkflowAppVariantStatusRecordRepository
                        .findAllByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(
                                dataCollectionAppVariant);

        assertThat(statusRecords).hasSizeGreaterThanOrEqualTo(2);

        DataPrivacyWorkflowAppVariantStatus previousStatus = statusRecords.get(1).getStatus();
        assertThat(previousStatus).isEqualTo(expectedStatus);
    }

    private void assertDataPrivacyWorkflowStatus(DataPrivacyWorkflow dataPrivacyWorkflow,
            DataPrivacyWorkflowStatus expectedStatus) {
        assertThat(dataPrivacyWorkflowRepository.findById(dataPrivacyWorkflow.getId()).get().getStatus())
                .isEqualTo(expectedStatus);
    }

}
