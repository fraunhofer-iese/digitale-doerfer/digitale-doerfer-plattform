/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2021 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantVersion;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;

public class AppControllerCheckVersionTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void checkVersion_ValidAppVersion() throws Exception {

        //unsupported, exact major minor match
        final AppVariantVersion checkedVersion1 = th.app1Variant1Version1Android;
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion1.getVersionIdentifier())
                        .param("platform", checkedVersion1.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion1.isSupported()))
                        .andExpect(jsonPath("$.url").value(checkedVersion1.getAppVariant().getUpdateUrlAndroid())));

        //unsupported, major minor match, bugfix version
        final AppVariantVersion checkedVersion2 = th.app1Variant1Version1Ios;
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion2.getVersionIdentifier() + ".42")
                        .param("platform", checkedVersion2.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion2.isSupported()))
                        .andExpect(jsonPath("$.url").value(checkedVersion2.getAppVariant().getUpdateUrlIOS())));

        //supported, exact major minor match
        final AppVariantVersion checkedVersion3 = th.app1Variant2Version1Android;
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant2,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion3.getVersionIdentifier())
                        .param("platform", checkedVersion3.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion3.isSupported()))
                        .andExpect(jsonPath("$.url").value(checkedVersion3.getAppVariant().getUpdateUrlIOS())));

        //supported, major minor match, bugfix version
        final AppVariantVersion checkedVersion4 = th.app1Variant2Version1Android;
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant2,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion4.getVersionIdentifier() + ".7")
                        .param("platform", checkedVersion4.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion4.isSupported()))
                        .andExpect(jsonPath("$.url").value(checkedVersion4.getAppVariant().getUpdateUrlIOS())));
    }

    @Test
    public void checkVersion_InvalidAppVersion() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final String appVariantVersionIdentifier = th.app1Variant1Version1Android.getVersionIdentifier();

        //unknown app version
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/checkversion")
                        .param("appVariantIdentifier", appVariant.getAppVariantIdentifier())
                        .param("appVariantVersionIdentifier", "0.8.15")
                        .param("platform", AppPlatform.ANDROID.name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(false))
                        .andExpect(
                                jsonPath("$.url").value(
                                        th.app1Variant1Version1Android.getAppVariant().getUpdateUrlAndroid())));

        //unknown platform
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", appVariantVersionIdentifier)
                        .param("platform", "RASPBERRY")
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isBadRequest()));

        //invalid version
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", "Aktuelle Version") //no version number
                        .param("platform", AppPlatform.ANDROID.name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(false))
                        .andExpect(
                                jsonPath("$.url").value(
                                        th.app1Variant1Version1Android.getAppVariant().getUpdateUrlAndroid())));

        //unknown app version, app variant without update urls
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant2,
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", "0.8.15")
                        .param("platform", AppPlatform.ANDROID.name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Moto G"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(false))
                        .andExpect(jsonPath("$.url").value(BaseSharedTestHelper.UPDATE_URL_GENERIC)));
    }

    @Test
    public void checkVersion_CacheRefresh() throws Exception {

        final AppVariantVersion checkedVersion = AppVariantVersion.builder()
                .appVariant(th.app1Variant2)
                .versionIdentifier("1.0")
                .platform(AppPlatform.ANDROID)
                .supported(true)
                .build();
        final String osversion = "6.0";
        final String device = "Motorola Razr HD";

        // ensure the cache is set
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, checkedVersion.getAppVariant(),
                () -> get("/app/checkversion")
                        .param("appVariantIdentifier", checkedVersion.getAppVariant().getAppVariantIdentifier())
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", osversion)
                        .param("device", device),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion.isSupported()))
                        .andExpect(jsonPath("$.url").isEmpty()));

        th.app1Variant2Version1Android.setVersionIdentifier("1.5");
        th.appVariantVersionRepository.saveAndFlush(th.app1Variant2Version1Android);

        // the change is not yet reflected, since the reference data change log entry is not done
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, checkedVersion.getAppVariant(),
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", osversion)
                        .param("device", device),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersion.isSupported()))
                        .andExpect(jsonPath("$.url").isEmpty()));

        th.simulateReferenceDataChange();

        timeServiceMock.setOffset(15, ChronoUnit.MINUTES);

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, checkedVersion.getAppVariant(),
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                        .param("platform", checkedVersion.getPlatform().name())
                        .param("osversion", osversion)
                        .param("device", device),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(false))
                        .andExpect(jsonPath("$.url").value(BaseSharedTestHelper.UPDATE_URL_GENERIC)));
    }

    @Test
    public void checkVersion_CacheDifferentPlatforms() throws Exception {

        final AppVariantVersion checkedVersionAndroid = AppVariantVersion.builder()
                .appVariant(th.app1Variant2)
                .versionIdentifier("1.0")
                .platform(AppPlatform.ANDROID)
                .supported(true)
                .build();

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, checkedVersionAndroid.getAppVariant(),
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersionAndroid.getVersionIdentifier())
                        .param("platform", checkedVersionAndroid.getPlatform().name())
                        .param("osversion", "6.0")
                        .param("device", "Motorola Razr HD"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersionAndroid.isSupported()))
                        .andExpect(jsonPath("$.url").isEmpty()));

        final AppVariantVersion checkedVersionIos = AppVariantVersion.builder()
                .appVariant(th.app1Variant2)
                .versionIdentifier("2.0")
                .platform(AppPlatform.IOS)
                .supported(true)
                .build();

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, checkedVersionAndroid.getAppVariant(),
                () -> get("/app/checkversion")
                        .param("appVariantVersionIdentifier", checkedVersionIos.getVersionIdentifier())
                        .param("platform", checkedVersionIos.getPlatform().name())
                        .param("osversion", "11.0")
                        .param("device", "Apple Iphone XR"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.supported").value(checkedVersionIos.isSupported()))
                        .andExpect(jsonPath("$.url").isEmpty()));
    }

    @Test
    public void checkVersion_InvalidParams() throws Exception {

        String appVariantIdentifier = th.app1Variant1Version1Android.getAppVariant().getAppVariantIdentifier();
        String appVariantVersionIdentifier = th.app1Variant1Version1Android.getVersionIdentifier();

        //invalid platform
        mockMvc.perform(get("/app/checkversion")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier("dd-super-duper-app")
                        .build().withoutAccessToken()))
                .param("appVariantVersionIdentifier", "0.1")
                .param("platform", "RASPBERRY")
                .param("osversion", "6.0")
                .param("device", "Motorola Moto G"))
                .andExpect(status().isBadRequest());

        //no version
        mockMvc.perform(get("/app/checkversion")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(appVariantIdentifier)
                        .build().withoutAccessToken()))
                .param("platform", AppPlatform.ANDROID.name())
                .param("osversion", "6.0")
                .param("device", "Motorola Moto G"))
                .andExpect(status().isBadRequest());

        //no appId
        mockMvc.perform(get("/app/checkversion")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(appVariantIdentifier)
                        .build().withoutAccessToken()))
                .param("platform", AppPlatform.ANDROID.name())
                .param("osversion", "6.0")
                .param("device", "Motorola Moto G"))
                .andExpect(status().isBadRequest());

        //no platform
        mockMvc.perform(get("/app/checkversion")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(appVariantIdentifier)
                        .build().withoutAccessToken()))
                .param("appVariantVersionIdentifier", appVariantVersionIdentifier)
                .param("osversion", "6.0")
                .param("device", "Motorola Moto G"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void checkVersion_InvalidConfiguration() throws Exception {

        AppVariantVersion checkedVersion = th.appInvalidVersion;
        mockMvc.perform(get("/app/checkversion")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(checkedVersion.getAppVariant().getAppVariantIdentifier())
                        .build().withoutAccessToken()))
                .param("appVariantVersionIdentifier", checkedVersion.getVersionIdentifier())
                .param("platform", checkedVersion.getPlatform().name())
                .param("osversion", "6.0")
                .param("device", "Motorola Moto G"))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.type").value(ClientExceptionType.VERSION_PARSING_FAILED.name()));
    }

}
