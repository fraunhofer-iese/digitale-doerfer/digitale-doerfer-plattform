/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransportAlternative;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class DeliveryOverPoolingStationToPoolingStationScanningTest extends BaseLogisticsEventTest {

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    // Sequence of:
    // Purchase Order Received
    // Purchase Order Ready For Transport
    // Transport Alternative Selected
    // Delivery to manual pooling station
    // Transport Alternative Selected
    // Carrier pickup from manual pooling station
    // Delivery to manual pooling station
    // Receiver pickup from manual pooling station
    @Test
    public void deliveryOverPoolingStationToPoolingStationScanning() throws Exception {

        intermediatePoolingStation = th.poolingStation4;
        deliveryPoolingStation = th.poolingStation2;

        intermediatePoolingStationAddress = intermediatePoolingStation.getAddress();
        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        pickupAddress = ClientAddressDefinition.builder()
                .name("Gartenschau Kaiserslautern")
                .street("Lauterstraße 51")
                .zip("67659")
                .city("Kaiserslautern")
                .build();

        intermediatePoolingStationBox = th.poolingStationBox6;
        deliveryPoolingStationBox = th.poolingStationBox3;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);
        TransportAlternative transportAlternativeToIntermediatePS =
                checkDeliveryAndTransportToPoolingStationViaIntermediatePoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment = transportAlternativeSelectRequestFirstStep(
                transportAlternativeToIntermediatePS.getId(),
                carrierFirstStep,
                intermediatePoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrierFirstStep, //carrier
            transportAssignment.getId(), //transportAssignmentId
            intermediatePoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(
            intermediatePoolingStation.getId(),
            delivery.getTrackingCode(),
            transportAssignment.getId(),
            carrierFirstStep,
            intermediatePoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        ClientTransportAlternative transportAlternativeToDeliveryPS = transportDeliveredPoolingStationRequestIntermediate(
            intermediatePoolingStation.getId(),
            delivery.getTrackingCode(),
            transportAssignment.getId(),
            carrierFirstStep,
            deliveryPoolingStation.getId(),
            delivery.getId());

        // TransportAlternativeSelectRequest
        // New transportAssignment
        transportAssignment = transportAlternativeSelectRequestLastStep(
            transportAlternativeToDeliveryPS.getId(),
            carrierLastStep,
            deliveryPoolingStationAddress);

        // TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest
        transportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
            delivery.getId(),
            intermediatePoolingStation.getId());

        // TransportPickupPoolingStationRequest
        transportPickupPoolingStationRequest(
                transportAssignment.getId(),
                delivery.getTrackingCode());

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(
            deliveryPoolingStation.getId(),
            delivery.getTrackingCode(),
            transportAssignment.getId(),
            carrierLastStep,
            deliveryPoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        transportDeliveredPoolingStationRequestFinal(
            deliveryPoolingStation, // poolingStation
            delivery.getTrackingCode(),// deliveryTrackingCode
            transportAssignment.getId(),// transportAssignmentId
            delivery.getId(),// deliveryId
            carrierLastStep,// carrier
            intermediatePoolingStation.getAddress());// pickupAddressTransport

        // ReceiverPickupPoolingStationBoxReadyForDeallocationRequest
        receiverPickupPoolingStationBoxReadyForDeallocationRequest(
            delivery.getId(),
            deliveryPoolingStation.getId(),
            deliveryPoolingStationBox);

        //Since there is no boxAllocation specified in the test, the previous method will resulting on selecting a random box from poolingStation1 in MySQL (SQL server also most likely)
        //For H2 it is always the first box saved in the repo
        List<PoolingStationBoxAllocation> allAllocations = th.poolingStationBoxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(deliveryPoolingStation.getId(), delivery.getId());
        assertEquals(1, allAllocations.size(),
                "Checking that there is only one allocation"); //If this fails the setup of the tests is wrong
        PoolingStationBoxAllocation allocation = allAllocations.get(0);
        deliveryPoolingStationBox = allocation.getPoolingStationBox();

        poolingStationBoxClosed();

        // ReceiverPickupReceivedPoolingStationRequest
        receiverPickupReceivedPoolingStationRequest(deliveryPoolingStation.getId(), delivery.getTrackingCode(),
                delivery.getId(), deliveryPoolingStationAddress);

    }

    private void poolingStationBoxClosed() throws Exception {
        ClientPoolingStationBoxClosedEvent clientDeliveryReceivedRequest = new ClientPoolingStationBoxClosedEvent(
                deliveryPoolingStationBox.getId());

        mockMvc.perform(
                        post("/logistics/event/poolingStationBoxClosed").header(HEADER_NAME_API_KEY,
                                        config.getDStation().getApiKey())
                                .contentType(contentType).content(json(clientDeliveryReceivedRequest)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        PoolingStationBoxAllocation boxAllocation = th.poolingStationBoxAllocationRepository
                .findFirstByPoolingStationBoxOrderByTimestampDesc(deliveryPoolingStationBox);
        assertFalse(boxAllocation.isDoorOpen(), "PoolingStationBox should be closed");

    }

    private void transportPickupPoolingStationBoxScannedReadyForDeallocationRequest(String deliveryId,
            String poolingStationId) throws Exception {

        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request =
                new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                        deliveryId, poolingStationId);
        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey()).contentType(contentType)
                        .content(json(request))).andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(deliveryId))
                .andExpect(jsonPath("$.poolingStationBoxId").value(intermediatePoolingStationBox.getId()))
                .andExpect(jsonPath("$.poolingStationBoxName").value(intermediatePoolingStationBox.getName()))
                .andExpect(jsonPath("$.poolingStationBoxType")
                        .value(intermediatePoolingStationBox.getConfiguration().getBoxType().toString()))
                .andExpect(jsonPath("$.timeoutForDoorOpen")
                        .value(intermediatePoolingStationBox.getConfiguration().getTimeoutForDoorOpen()));
    }

}
