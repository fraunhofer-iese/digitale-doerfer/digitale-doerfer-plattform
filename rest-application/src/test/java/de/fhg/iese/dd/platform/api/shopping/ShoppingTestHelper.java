/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Alberto Lara, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.logistics.LogisticsTestHelper;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.LogisticsParticipant;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.LogisticsParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ParcelAddressRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shopping.ShoppingConstants;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

@Component
public class ShoppingTestHelper extends BaseSharedTestHelper {

    @Autowired
    public DeliveryRepository deliveryRepository;
    @Autowired
    public DeliveryStatusRecordRepository deliveryStatusRecordRepository;
    @Autowired
    public PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    public PurchaseOrderItemRepository purchaseOrderItemRepository;
    @Autowired
    protected PoolingStationRepository poolingStationRepository;
    @Autowired
    protected PoolingStationBoxRepository poolingStationBoxRepository;
    @Autowired
    protected TransportAlternativeRepository transportAlternativeRepository;
    @Autowired
    protected TransportAssignmentRepository transportAssignmentRepository;
    @Autowired
    private PersonAccountRepository personAccountRepository;
    @Autowired
    private TenantAccountRepository tenantAccountRepository;
    @Autowired
    private ParcelAddressRepository parcelAddressRepository;
    @Autowired
    private LogisticsParticipantRepository logisticsParticipantRepository;
    @Autowired
    private LogisticsTestHelper logisticsTestHelper;

    public Shop shop1;
    public Shop shop2;
    public Shop shop3;
    public Shop shop4;
    public List<Shop> shopList;

    public List<PoolingStation> poolingStationList;
    public PoolingStation poolingStation1;
    public PoolingStation poolingStation2;
    public PoolingStation poolingStation3;
    public PoolingStation poolingStation4;

    public List<PoolingStationBox> poolingStationBoxList;
    public PoolingStationBox poolingStationBox1;
    public PoolingStationBox poolingStationBox2;
    public PoolingStationBox poolingStationBox3;
    public PoolingStationBox poolingStationBox4;
    public PoolingStationBox poolingStationBox5;
    public PoolingStationBox poolingStationBox6;

    public Delivery delivery;
    public PurchaseOrder purchaseOrder;

    public App appBestellBar;
    public AppVariant appVariantBestellBar1;
    public AppVariant appVariantBestellBar2;
    public AppVariant nonBestellBarAppVariantWithApiKeys;

    @Override
    protected Set<String> achievementRulesToCreate() {

        return new HashSet<>(Arrays.asList(
                "score.participants.created.1",
                "score.participants.registered.1",
                "shopping.score.purchaseOrder.created.1",
                "score.account.received.1",
                "score.account.sent.1",
                "logistics.score.transport.done.1"
        ));
    }

    @Override
    public void createShops() {
        shop1 = createShop1(nextTimeStamp());

        shop2 = new Shop();
        shop2.setCreated(nextTimeStamp());
        shop2.setTenant((tenant1));
        shop2 = shopRepository.save(shop2);

        shop3 = new Shop();
        shop3.setCreated(nextTimeStamp());
        shop3.setTenant((tenant2));
        shop3 = shopRepository.save(shop3);

        shop4 = new Shop();
        shop4.setCreated(nextTimeStamp());
        shop4.setTenant((tenant3));
        shop4 = shopRepository.save(shop4);

        shopList = Arrays.asList(shop1, shop2, shop3, shop4);
    }

    public void createPoolingStations() {
        super.createPoolingStations();
        createPoolingStation1();

        poolingStation2 = new PoolingStation();
        poolingStation2.setName("D-Station TU KL");
        poolingStation2.setTenant(tenant1);
        poolingStation2.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station TU KL")
                        .street("Gottlieb-Daimler-Straße 49")
                        .zip(
                                "67663")
                        .city("Kaiserslautern").gpsLocation(
                        new GPSLocation(49.423920, 7.755011)).build()));
        poolingStation2.setCreated(nextTimeStamp());
        poolingStation2 = poolingStationRepository.save(poolingStation2);

        poolingStation3 = new PoolingStation();
        poolingStation3.setName("D-Station Lidl");
        poolingStation3.setTenant(tenant2);
        poolingStation3.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station Lidl")
                        .street("Königstraße 115")
                        .zip("67655")
                        .city("Kaiserslautern").gpsLocation(new GPSLocation(49.437537, 7.756299)).build()));
        poolingStation3.setCreated(nextTimeStamp());
        poolingStation3 = poolingStationRepository.save(poolingStation3);

        poolingStation4 = new PoolingStation();
        poolingStation4.setName("D-Station ATU");
        poolingStation4.setTenant(tenant3);
        poolingStation4.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station ATU")
                        .street("Pariser Straße 196")
                        .zip("67663")
                        .city("Kaiserslautern").gpsLocation(new GPSLocation(49.441573, 7.743977)).build()));
        poolingStation4.setCreated(nextTimeStamp());
        poolingStation4 = poolingStationRepository.save(poolingStation4);

        poolingStationList = Arrays.asList(poolingStation1, poolingStation2, poolingStation3, poolingStation4);

        poolingStationBox1 = new PoolingStationBox("Fach 1", 1, null, poolingStation1);
        poolingStationBox1.setCreated(nextTimeStamp());
        poolingStationBox1 = poolingStationBoxRepository.save(poolingStationBox1);

        poolingStationBox2 = new PoolingStationBox("Fach 2", 2, null, poolingStation1);
        poolingStationBox2.setCreated(nextTimeStamp());
        poolingStationBox2 = poolingStationBoxRepository.save(poolingStationBox2);

        poolingStationBox3 = new PoolingStationBox("Fach 1", 1, null, poolingStation2);
        poolingStationBox3.setCreated(nextTimeStamp());
        poolingStationBox3 = poolingStationBoxRepository.save(poolingStationBox3);

        poolingStationBox4 = new PoolingStationBox("Fach 2", 2, null, poolingStation2);
        poolingStationBox4.setCreated(nextTimeStamp());
        poolingStationBox4 = poolingStationBoxRepository.save(poolingStationBox4);

        poolingStationBox5 = new PoolingStationBox("Fach 1", 1, null, poolingStation3);
        poolingStationBox5.setCreated(nextTimeStamp());
        poolingStationBox5 = poolingStationBoxRepository.save(poolingStationBox5);

        poolingStationBox6 = new PoolingStationBox("Fach 1", 1, null, poolingStation4);
        poolingStationBox6.setCreated(nextTimeStamp());
        poolingStationBox6 = poolingStationBoxRepository.save(poolingStationBox6);

        poolingStationBoxList = Arrays.asList(
                poolingStationBox1,
                poolingStationBox2,
                poolingStationBox3,
                poolingStationBox4,
                poolingStationBox5,
                poolingStationBox6);
    }

    public void createScoreEntities() {

        assertNotNull(tenant1);
        assertNotNull(tenant2);
        assertNotNull(tenant3);

        TenantAccount accountCommunity1 = new TenantAccount();
        accountCommunity1.setOwner(tenant1);
        accountCommunity1.setAppAccountType(AppAccountType.LOGISTICS);
        accountCommunity1.setBalance(100);
        accountCommunity1.setCreated(nextTimeStamp());
        accountCommunity1 = tenantAccountRepository.save(accountCommunity1);

        TenantAccount accountCommunity2 = new TenantAccount();
        accountCommunity2.setOwner(tenant2);
        accountCommunity2.setAppAccountType(AppAccountType.LOGISTICS);
        accountCommunity2.setBalance(100);
        accountCommunity2.setCreated(nextTimeStamp());
        accountCommunity2 = tenantAccountRepository.save(accountCommunity2);

        PersonAccount accountPersonVgAdmin = new PersonAccount();
        accountPersonVgAdmin.setOwner(personVgAdmin);
        accountPersonVgAdmin.setBalance(60);
        accountPersonVgAdmin.setCreated(nextTimeStamp());
        accountPersonVgAdmin = personAccountRepository.save(accountPersonVgAdmin);

        PersonAccount accountPersonIeseAdmin = new PersonAccount();
        accountPersonIeseAdmin.setOwner(personIeseAdmin);
        accountPersonIeseAdmin.setBalance(50);
        accountPersonIeseAdmin.setCreated(nextTimeStamp());
        accountPersonIeseAdmin = personAccountRepository.save(accountPersonIeseAdmin);

        PersonAccount accountPersonPoolingStationOp = new PersonAccount();
        accountPersonPoolingStationOp.setOwner(personPoolingStationOp);
        accountPersonPoolingStationOp.setBalance(10);
        accountPersonPoolingStationOp.setCreated(nextTimeStamp());
        accountPersonPoolingStationOp = personAccountRepository.save(accountPersonPoolingStationOp);

        PersonAccount accountPersonRegular = new PersonAccount();
        accountPersonRegular.setOwner(personRegularAnna);
        accountPersonRegular.setBalance(0);
        accountPersonRegular.setCreated(nextTimeStamp());
        accountPersonRegular = personAccountRepository.save(accountPersonRegular);

        PersonAccount accountPersonShopOwner = new PersonAccount();
        accountPersonShopOwner.setOwner(personShopOwner);
        accountPersonShopOwner.setBalance(42);
        accountPersonShopOwner.setCreated(nextTimeStamp());
        accountPersonShopOwner = personAccountRepository.save(accountPersonShopOwner);
    }

    private void createPoolingStation1() {
        OpeningHours openingHoursPoolingStation1 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation1 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station Bahnhof")
                                .street("Zollamtstraße 8")
                                .zip("67663")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.435450, 7.769099)).build()),
                "D-Station Bahnhof",
                "42",
                mediaItemRepository.save(createMediaItem("imagePoolingStation1")),
                tenant1,
                openingHoursPoolingStation1,
                PoolingStationType.MANUAL,
                false,
                true,
                true,
                true
        );
        poolingStation1.setCreated(nextTimeStamp());
        poolingStation1 = poolingStationRepository.save(poolingStation1);
    }

    public void createPushEntities() {
        logisticsTestHelper.createPushEntities();
    }

    public PurchaseOrder createPurchaseOrder(Shop sender, Person receiver) {

        delivery = Delivery.builder()
                .community(tenant1)
                .sender(logisticsParticipantRepository.saveAndFlush(
                        new LogisticsParticipant(sender)))
                .pickupAddress(parcelAddressRepository.saveAndFlush(
                        new ParcelAddress(
                                addressRepository.saveAndFlush(
                                        Address.builder()
                                                .name("sender")
                                                .street("sender-street")
                                                .zip(UUID.randomUUID().toString())
                                                .city("sender-city")
                                                .build()
                                ),
                                sender
                        )
                ))
                .receiver(logisticsParticipantRepository.saveAndFlush(
                        new LogisticsParticipant(receiver)))
                .deliveryAddress(parcelAddressRepository.saveAndFlush(
                        new ParcelAddress(
                                addressRepository.saveAndFlush(
                                        Address.builder()
                                                .name("receiver")
                                                .street("receiver-street")
                                                .zip(UUID.randomUUID().toString())
                                                .city("receiver-city")
                                                .build()
                                ),
                                receiver
                        )
                ))
                .contentNotes("Enthält Rhabarber!")
                .trackingCode("4711")
                .size(Dimension.builder()
                        .height(20d)
                        .length(10d)
                        .width(30d)
                        .weight(1.5d)
                        .build())
                .build();
        delivery.setTenant(tenant1);
        delivery.setCreated(nextTimeStamp());
        delivery = deliveryRepository.save(delivery);

        purchaseOrder = new PurchaseOrder();
        purchaseOrder.setPickupAddress(delivery.getPickupAddress());
        purchaseOrder.setDeliveryAddress(delivery.getDeliveryAddress());
        purchaseOrder.setTenant(delivery.getTenant());
        purchaseOrder.setDelivery(delivery);
        purchaseOrder.setReceiver(receiver);
        purchaseOrder.setSender(sender);
        purchaseOrder.setItems(
                Arrays.asList(
                        purchaseOrderItemRepository.save(
                                PurchaseOrderItem.builder()
                                        .amount(5)
                                        .itemName("Bananen")
                                        .unit("gebogene Stück")
                                        .build()),
                        purchaseOrderItemRepository.save(
                                PurchaseOrderItem.builder()
                                        .amount(1)
                                        .itemName("stumpfes Messer")
                                        .unit("Teil")
                                        .build()
                        )
                )
        );
        purchaseOrder.setShopOrderId("PurchaseOrder1");
        purchaseOrder.setTransportNotes(UUID.randomUUID().toString());
        purchaseOrder.setPurchaseOrderNotes(UUID.randomUUID().toString());
        purchaseOrder.setContentNotes(UUID.randomUUID().toString());
        purchaseOrder.setCreated(nextTimeStamp());
        purchaseOrder = purchaseOrderRepository.save(purchaseOrder);

        return purchaseOrder;
    }

    public void createBestellBarAppAndAppVariants(){
        appBestellBar = appRepository.save(App.builder()
                .id(ShoppingConstants.BESTELLBAR_APP_ID)
                .name("BestellBar")
                .appIdentifier("dd-bestellbar")
                .build());

        appRepository.flush();

        appVariantBestellBar1 = appVariantRepository.save(AppVariant.builder()
                .name("BestellBarAppVariant1")
                .appVariantIdentifier("de.fhg.iese.dd.bestellbar.1")
                .app(appBestellBar)
                .apiKey1("de.fhg.iese.dd.bestellbar.1.apiKey")
                .apiKey2("de.fhg.iese.dd.bestellbar.commonApiKey")
                .build());

        appVariantBestellBar2 = appVariantRepository.save(AppVariant.builder()
                .name("BestellBarAppVariant2")
                .appVariantIdentifier("de.fhg.iese.dd.bestellbar.2")
                .app(appBestellBar)
                .apiKey1("de.fhg.iese.dd.bestellbar.2.apiKey")
                .apiKey2("de.fhg.iese.dd.bestellbar.commonApiKey")
                .build());

        appVariantRepository.flush();
    }

    public void createNonBestellBarAppVariant(){
        nonBestellBarAppVariantWithApiKeys = appVariantRepository.save(AppVariant.builder()
                .name("NonBestellBarAppVariant")
                .appVariantIdentifier("de.fhg.iese.dd.not-a-bestellbar")
                .app(app1)
                .apiKey1("de.fhg.iese.dd.not-a-bestellbar.apiKey1")
                .apiKey2("de.fhg.iese.dd.not-a-bestellbar.apiKey2")
                .build());

        appVariantRepository.flush();
    }

}
