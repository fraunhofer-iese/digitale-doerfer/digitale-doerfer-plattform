/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;

public class AccountControllerTest extends BaseServiceTest {

    @Autowired
    private MotivationTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Autowired
    private PersonAccountRepository personAccountRepository;

    @Test
    public void getBalanceForPerson() throws Exception {

        mockMvc.perform(get("/score/account/balance/")
            .headers(authHeadersFor(th.personVgAdmin)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$").value(th.accountPersonVgAdmin.getBalance()));
    }

    @Test
    public void getAccount() throws Exception {

        mockMvc.perform(get("/score/account/")
                .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.balance").value(th.accountPersonVgAdmin.getBalance()))
                .andExpect(jsonPath("$.entries", hasSize((int) th.accountEntryList.stream()
                        .filter(i -> i.getAccount().equals(th.accountPersonVgAdmin))
                        .count())))
                .andExpect(jsonPath("$.entries[0].id").value(th.intialAccountEntryPersonVgAdmin.getId()))
                .andExpect(jsonPath("$.entries[1].id").value(th.offerInquiryAccountEntry1_1.getId()))
                .andExpect(jsonPath("$.entries[2].id").value(th.offerInquiryAccountEntry2_2.getId()))
                .andExpect(jsonPath("$.entries[3].id").value(th.seekingInquiryAccountEntry1_1.getId()))
                .andExpect(jsonPath("$.entries[4].id").value(th.seekingInquiryAccountEntry2_2.getId()));
    }

    @Test
    public void getBalanceForPersonWithoutAccount() throws Exception {

        personAccountRepository.delete(th.accountPersonPoolingStationOp);

        mockMvc.perform(get("/score/account/balance/")
                .headers(authHeadersFor(th.personPoolingStationOp)))
                .andExpect(isException(ClientExceptionType.ACCOUNT_NOT_FOUND));
    }

    @Test
    public void getAccountWithoutAccount() throws Exception {

        personAccountRepository.delete(th.accountPersonPoolingStationOp);

        mockMvc.perform(get("/score/account/")
                .headers(authHeadersFor(th.personPoolingStationOp)))
                .andExpect(isException(ClientExceptionType.ACCOUNT_NOT_FOUND));
    }

}
