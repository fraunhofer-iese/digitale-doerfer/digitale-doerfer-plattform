/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.security.RoleTestHelper;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientCreateRoleAssignmentRequest;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientCreateRoleAssignmentResponse;
import de.fhg.iese.dd.platform.api.shared.security.clientevent.ClientRemoveRoleAssignmentRequest;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

public class RoleAssignmentEventControllerTest extends BaseServiceTest {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private RoleTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createPoolingStations();
        th.createShops();
        th.createAdditionalPersonsWithSpecialRoles();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void roleAssignmentEventEndpoints_Unauthorized() throws Exception {

        assertOAuth2(post("/roleAssignment/event/createRoleAssignmentRequest")
                .contentType(contentType)
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId(th.personRegularKarl.getId())
                        .relatedEntityId(th.tenant1.getId())
                        .roleKey(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN)
                        .build())));
        assertOAuth2(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId(th.roleAssignmentPersonVgAdmin.getId())
                        .build())));
    }

    @Test
    public void roleAssignmentEventEndpointsReturnErrorWhenCalledWithWrongParameters() throws Exception {

        final Person manager = th.personGlobalUserAdmin;

        //CREATE ==============
        //person not existing
        mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                .contentType(contentType)
                .headers(authHeadersFor(manager))
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId("notexisting")
                        .relatedEntityId(th.tenant1.getId())
                        .roleKey(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN)
                        .build())))
                .andExpect(isException("notexisting", ClientExceptionType.PERSON_NOT_FOUND));
        //related entity is null
        mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                        .contentType(contentType)
                        .headers(authHeadersFor(manager))
                        .content(json(ClientCreateRoleAssignmentRequest.builder()
                                .personId(th.personRegularKarl.getId())
                                .relatedEntityId(null)
                                .roleKey(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN)
                                .build())))
                .andExpect(isException(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN,
                        ClientExceptionType.RELATED_ENTITY_MUST_NOT_BE_NULL));

        //related entity does not exist
        mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                .contentType(contentType)
                .headers(authHeadersFor(manager))
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId(th.personRegularKarl.getId())
                        .relatedEntityId("not_available")
                        .roleKey(RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN)
                        .build())))
                .andExpect(isException("not_available", ClientExceptionType.RELATED_ENTITY_NOT_FOUND));

        //invalid role key
        mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                .contentType(contentType)
                .headers(authHeadersFor(manager))
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId(th.personRegularKarl.getId())
                        .relatedEntityId(th.tenant1.getId())
                        .roleKey("UNKNOWN_ROLE")
                        .build())))
                .andExpect(isException("UNKNOWN_ROLE", ClientExceptionType.ROLE_NOT_FOUND));

        //REMOVE ==============
        //role assignment does not exist
        mockMvc.perform(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId("does-not-exist")
                        .build())))
                .andExpect(status().isNotFound());
    }

    @Test
    public void roleAssignmentsWithInvalidInternalStateCanBeRemoved() throws Exception {

        final RoleAssignment roleAssignmentWithNullRequiredRelatedEntity =
                th.roleAssignmentRepository.save(RoleAssignment.builder()
                        .person(th.personRegularAnna)
                        .role(UserAdmin.class)
                        .build());

        final RoleAssignment roleAssignmentWithNotExistingRelatedEntity =
                th.roleAssignmentRepository.save(RoleAssignment.builder()
                        .person(th.personRegularAnna)
                        .role(UserAdmin.class)
                        .relatedEntityId("not_existing")
                        .build());

        final Person manager = th.personGlobalUserAdmin;

        RoleAssignment roleAssignmentToDelete = roleAssignmentWithNullRequiredRelatedEntity;
        mockMvc.perform(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId(roleAssignmentToDelete.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.roleAssignmentId").value(roleAssignmentToDelete.getId()));
        assertFalse(th.roleAssignmentRepository.existsById(roleAssignmentToDelete.getId()));

        roleAssignmentToDelete = roleAssignmentWithNotExistingRelatedEntity;
        mockMvc.perform(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId(roleAssignmentToDelete.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.roleAssignmentId").value(roleAssignmentToDelete.getId()));
        assertFalse(th.roleAssignmentRepository.existsById(roleAssignmentToDelete.getId()));
    }

    @Test
    public void roleManagersCanAddAndDeleteRoleAssignmentsToPersonOfTheirCommunity() throws Exception {
        managerCanAddAndDeleteRoleAssignment(th.personRoleManagerTenant1,
                RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN,
                LogisticsAdminUiRestrictedAdmin.class,
                th.tenant1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personRoleManagerTenant1, RoleTestHelper.ROLE_MANAGER,
                RoleManager.class, th.tenant1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personRoleManagerTenant1And3, RoleTestHelper.SHOP_OWNER,
                ShopOwner.class, th.shop1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personRoleManagerTenant1And3, RoleTestHelper.USER_ADMIN,
                UserAdmin.class, th.tenant1, th.tenant1);
    }

    @Test
    public void superAdminCanAddAndDeleteRoleAssignmentsToPerson() throws Exception {
        managerCanAddAndDeleteRoleAssignment(th.personSuperAdmin, RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN,
                LogisticsAdminUiRestrictedAdmin.class,
                th.tenant1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personSuperAdmin, RoleTestHelper.ROLE_MANAGER, RoleManager.class,
                th.tenant1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personSuperAdmin, RoleTestHelper.SHOP_OWNER, ShopOwner.class,
                th.shop1, th.tenant1);
        managerCanAddAndDeleteRoleAssignment(th.personSuperAdmin, RoleTestHelper.SUPER_ADMIN, SuperAdmin.class, null,
                null);
        managerCanAddAndDeleteRoleAssignment(th.personSuperAdmin, RoleTestHelper.GLOBAL_USER_ADMIN,
                GlobalUserAdmin.class, null, null);
    }

    @Test
    public void globalUserAdminCanAddAndDeleteGlobalUserAdminRoleToPerson() throws Exception {
        //make Anna global user Admin
        final RoleAssignment globalUserAdminAssignment =
                th.roleAssignmentRepository.save(new RoleAssignment(th.personRegularAnna, GlobalUserAdmin.class, null));
        try {
            managerCanAddAndDeleteRoleAssignment(th.personRegularAnna, RoleTestHelper.GLOBAL_USER_ADMIN,
                    GlobalUserAdmin.class, null, null);
        } finally {
            th.roleAssignmentRepository.delete(globalUserAdminAssignment);
        }
    }

    @Test
    public void userAdminCanAddAndDeleteRoleAssignmentsToPerson() throws Exception {
        managerCanAddAndDeleteRoleAssignment(th.personUserAdminTenant2,
                RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN,
                LogisticsAdminUiRestrictedAdmin.class,
                th.tenant2, th.tenant2);
    }

    private void managerCanAddAndDeleteRoleAssignment(final Person manager, final String roleKey,
            final Class<?> roleClass,
            final NamedEntity relatedEntity,
            Tenant relatedEntityTenant) throws Exception {
        final String roleAssigneeId = th.personRegularKarl.getId();
        final String relatedEntityId = relatedEntity != null ? relatedEntity.getId() : null;
        final MvcResult createResult = mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId(roleAssigneeId)
                        .relatedEntityId(relatedEntityId)
                        .roleKey(roleKey)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.roleAssignment.id").value(not(emptyOrNullString())))
                .andExpect(jsonPath("$.roleAssignment.roleKey").value(roleKey))
                .andExpect(jsonPath("$.roleAssignment.personId").value(roleAssigneeId))
                .andExpect(jsonPath("$.roleAssignment.relatedEntityId").value(relatedEntityId))
                .andExpect(jsonPath("$.roleAssignment.relatedEntityName").value(
                        relatedEntity != null ? relatedEntity.getName() : null))
                .andExpect(jsonPath("$.roleAssignment.relatedEntityTenantId").value(
                        relatedEntityTenant != null ? relatedEntityTenant.getId() : null))
                .andExpect(jsonPath("$.roleAssignment.relatedEntityTenantName").value(
                        relatedEntityTenant != null ? relatedEntityTenant.getName() : null))
                .andReturn();

        final ClientCreateRoleAssignmentResponse createResponse =
                toObject(createResult.getResponse(), ClientCreateRoleAssignmentResponse.class);
        final String assignmentId = createResponse.getRoleAssignment().getId();
        final RoleAssignment assignment = th.roleAssignmentRepository.findById(assignmentId).get();
        assertThat(assignment.getPerson().getId()).isEqualTo(roleAssigneeId);
        assertThat(assignment.getRelatedEntityId()).isEqualTo(relatedEntityId);
        assertThat(assignment.getRole()).isEqualTo(roleClass);

        mockMvc.perform(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId(assignmentId)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.roleAssignmentId").value(assignmentId));
        assertFalse(th.roleAssignmentRepository.existsById(assignmentId));
    }

    @Test
    public void regularPersonCannotManageRoles() throws Exception {
        personCannotAddRoleAssignment(th.personRegularAnna, th.tenant1.getId(),
                RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN);
        personCannotAddRoleAssignment(th.personRegularAnna, th.tenant1.getId(), RoleTestHelper.ROLE_MANAGER);
        personCannotAddRoleAssignment(th.personRegularAnna, th.tenant1.getId(), RoleTestHelper.USER_ADMIN);
        personCannotAddRoleAssignment(th.personRegularAnna, th.tenant1.getId(), RoleTestHelper.GLOBAL_USER_ADMIN);
        personCannotRemoveRoleAssignment(th.personRegularAnna, th.roleAssignmentPersonVgAdmin);
        personCannotRemoveRoleAssignment(th.personRegularAnna, th.roleAssignmentPersonIeseAdmin);
        personCannotRemoveRoleAssignment(th.personRegularAnna, th.roleAssignmentPersonSuperAdmin);
    }

    @Test
    public void roleManagerAndUserAdminForCommunity2CannotManageRolesForCommunity1() throws Exception {
        personCannotAddRoleAssignment(th.personRoleManagerTenant2, th.tenant1.getId(),
                RoleTestHelper.LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN);
        personCannotAddRoleAssignment(th.personRoleManagerTenant2, th.tenant1.getId(), RoleTestHelper.ROLE_MANAGER);
        personCannotRemoveRoleAssignment(th.personRoleManagerTenant2, th.roleAssignmentPersonVgAdmin);
        personCannotRemoveRoleAssignment(th.personRoleManagerTenant2, th.roleAssignmentPersonIeseAdmin);
        personCannotRemoveRoleAssignment(th.personUserAdminTenant2, th.roleAssignmentPersonIeseAdmin);
    }

    @Test
    public void roleManagersCannotManageSuperAdminRoles() throws Exception {
        personCannotAddRoleAssignment(th.personRoleManagerTenant1, null, RoleTestHelper.SUPER_ADMIN);
        personCannotAddRoleAssignment(th.personRoleManagerTenant2, null, RoleTestHelper.SUPER_ADMIN);
        personCannotRemoveRoleAssignment(th.personRoleManagerTenant1, th.roleAssignmentPersonSuperAdmin);
        personCannotRemoveRoleAssignment(th.personRoleManagerTenant2, th.roleAssignmentPersonSuperAdmin);
    }

    @Test
    public void roleManagersCannotManageGlobalUserAdminRoles() throws Exception {
        personCannotAddRoleAssignment(th.personRoleManagerTenant1, null, RoleTestHelper.GLOBAL_USER_ADMIN);
        personCannotAddRoleAssignment(th.personRoleManagerTenant2, null, RoleTestHelper.GLOBAL_USER_ADMIN);
        personCannotRemoveRoleAssignment(th.personRoleManagerTenant1,
                th.roleAssignmentPersonWithManyRolesGlobalUserAdmin);
    }

    private void personCannotAddRoleAssignment(Person manager, String relatedEntityId, String roleKey)
            throws Exception {
        mockMvc.perform(post("/roleAssignment/event/createRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientCreateRoleAssignmentRequest.builder()
                        .personId(th.personRegularKarl.getId())
                        .relatedEntityId(relatedEntityId)
                        .roleKey(roleKey)
                        .build())))
                .andExpect(isException(roleKey, ClientExceptionType.ROLE_MANAGEMENT_NOT_ALLOWED))
                .andReturn();
    }

    private void personCannotRemoveRoleAssignment(Person manager, RoleAssignment assignment) throws Exception {
        final String roleKey = roleService.getRole(assignment.getRole()).getKey();

        mockMvc.perform(post("/roleAssignment/event/removeRoleAssignmentRequest")
                .headers(authHeadersFor(manager))
                .contentType(contentType)
                .content(json(ClientRemoveRoleAssignmentRequest.builder()
                        .roleAssignmentId(assignment.getId())
                        .build())))
                .andExpect(isException(roleKey, ClientExceptionType.ROLE_MANAGEMENT_NOT_ALLOWED));
    }

}
