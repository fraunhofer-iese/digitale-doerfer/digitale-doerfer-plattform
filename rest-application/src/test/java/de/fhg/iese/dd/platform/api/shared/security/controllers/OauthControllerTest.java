/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Jannis von Albedyll
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;

public class OauthControllerTest extends BaseServiceTest {

    @Autowired
    public OauthRegistrationRepository oauthRegistrationRepository;

    @Autowired
    private OauthConfig oauthConfig;

    private final String testMail = "digdorfdev+test1@gmail.com";

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
    }

    @Test
    public void registerOauthUserUnauthorized() throws Exception {
        performInvalidRegistration("invalid api key", "digdorfdev+test@gmail.com", "test",
                ClientExceptionType.NOT_AUTHORIZED, null);

        performInvalidRegistration("", "digdorfdev+test@gmail.com", "test",
                ClientExceptionType.NOT_AUTHENTICATED, null);
    }

    @Test
    public void registerOauthUserMissingArguments() throws Exception {
        performInvalidRegistration(oauthConfig.getApiKeyForPendingAccounts(), "digdorfdev+test@gmail.com", "",
                ClientExceptionType.QUERY_PARAMETER_INVALID, "oauthId");

        performInvalidRegistration(oauthConfig.getApiKeyForPendingAccounts(), "", "test",
                ClientExceptionType.QUERY_PARAMETER_INVALID, "email");
    }

    @Test
    public void registerOauthUserTwice() throws Exception {
        String oauthId = "auth0|123123123124";
        performValidRegistration(testMail, oauthId);
        assertMailForIdInRegistrations(testMail, oauthId);

        performInvalidRegistration(oauthConfig.getApiKeyForPendingAccounts(), "digdorfdev+test2@gmail.com", oauthId,
                ClientExceptionType.OAUTH_ID_REGISTRATION_ALREADY_EXISTS, oauthId);
    }

    @Test
    public void registerOauthUserSameMailDifferentIds() throws Exception {
        performValidRegistration(testMail, "auth0|123123123123");
        assertMailForIdInRegistrations(testMail, "auth0|123123123123");

        performValidRegistration(testMail, "auth0|456456456456");
        assertMailForIdInRegistrations(testMail, "auth0|456456456456");
    }

    @Test
    public void registerOauthUserSameIdDifferentMails() throws Exception {
        String oauthId = "auth0|123123123125";
        performValidRegistration(testMail, oauthId);
        assertMailForIdInRegistrations(testMail, oauthId);

        performInvalidRegistration(oauthConfig.getApiKeyForPendingAccounts(), testMail, oauthId,
                ClientExceptionType.OAUTH_ID_REGISTRATION_ALREADY_EXISTS, oauthId);
    }

    private void performValidRegistration(String email, String oauthId) throws Exception {
        mockMvc.perform(post("/oauth/registration")
                .contentType(contentType)
                .header(HEADER_NAME_API_KEY, oauthConfig.getApiKeyForPendingAccounts())
                .param("email", email)
                .param("oauthId", oauthId))
                .andExpect(status().isOk());
    }

    private void performInvalidRegistration(String apiKey, String email, String oauthId,
            ClientExceptionType expectedExceptionType, String detail) throws Exception {
        mockMvc.perform(post("/oauth/registration")
                .contentType(contentType)
                .header(HEADER_NAME_API_KEY, apiKey)
                .param("email", email)
                .param("oauthId", oauthId))
                .andExpect(detail != null
                        ? isException(detail, expectedExceptionType)
                        : isException(expectedExceptionType));
    }

    private void assertMailForIdInRegistrations(String mail, String id) {
        List<OauthRegistration> registrations = oauthRegistrationRepository.findAll();

        assertEquals(mail, registrations.stream()
                .filter(r -> r.getOauthId().equals(id))
                .findFirst()
                .get()
                .getEmail());
    }

}
