/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikeCommentRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikeCommentRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.LikeComment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.LikeCommentRepository;

public class GroupLikeCommentEventControllerTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private LikeCommentRepository likeCommentRepository;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void likeCommentRequest_groupMember_verifyingPushMessage() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        final Comment commentToBeLiked = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentToBeLiked.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentToBeLiked,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        waitForEventProcessing();

        ClientCommentChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(th.personChloeTenant1Dorf1,
                        ClientCommentChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED);

        assertThat(pushedEvent.getComment().getId()).isEqualTo(comment.getId());
    }

    @Test
    public void likeCommentRequest_groupMember_withDoubleLike() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        final Comment commentToBeLiked = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentToBeLiked.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentToBeLiked,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        final Comment commentAfterLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentAfterLike.getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentAfterLike,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_ALREADY_LIKED));

        final Comment commentAfterSecondLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentAfterSecondLike.getLikeCount());
    }

    @Test
    public void likeCommentRequest_groupMember_relike() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        final LikeComment likeComment = LikeComment.builder()
                .person(th.personChloeTenant1Dorf1)
                .comment(comment)
                .liked(false)
                .build();

        likeCommentRepository.saveAndFlush(likeComment);

        final Comment commentToBeLiked = commentRepository.findById(comment.getId()).get();

        commentToBeLiked.setLikeCount(0);
        commentRepository.saveAndFlush(commentToBeLiked);

        final Comment commentFromRepoAfterUpdate = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentFromRepoAfterUpdate.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentFromRepoAfterUpdate,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        final Comment commentFromRepoAfterLike = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentFromRepoAfterLike.getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentFromRepoAfterLike,
                th.personChloeTenant1Dorf1));
    }

    @Test
    public void likeCommentRequest_notGroupMember_publicGroup() throws Exception {

        final Group group = th.groupAdfcAAP;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));
    }

    @Test
    public void likeCommentRequest_notGroupMember() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

    @Test
    public void unlikeCommentRequest_groupMember_verifyingPushMessage() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        // Setup liked state in repo
        likeCommentRepository.saveAndFlush(LikeComment.builder()
                .comment(comment)
                .person(th.personChloeTenant1Dorf1)
                .liked(true)
                .build());

        final ClientUnlikeCommentRequest unlikeCommentRequest = new ClientUnlikeCommentRequest(comment.getId());

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(0))
                .andExpect(jsonPath("$.comment.liked").value(false));

        waitForEventProcessing();

        ClientCommentChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(th.personChloeTenant1Dorf1,
                        ClientCommentChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED);

        assertThat(pushedEvent.getComment().getId()).isEqualTo(comment.getId());
    }

    @Test
    public void unlikeCommentRequest_groupMember_withDoubleUnlike() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        // Setup liked state in repo
        final ClientLikeCommentRequest likeCommentRequest = new ClientLikeCommentRequest(comment.getId());

        mockMvc.perform(post("/grapevine/comment/event/likeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(1))
                .andExpect(jsonPath("$.comment.liked").value(true));

        final ClientUnlikeCommentRequest unlikeCommentRequest = new ClientUnlikeCommentRequest(comment.getId());

        final Comment commentToBeUnliked = commentRepository.findById(comment.getId()).get();
        assertEquals(1, commentToBeUnliked.getLikeCount());

        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentToBeUnliked,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikeCommentRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(0))
                .andExpect(jsonPath("$.comment.liked").value(false));

        final Comment commentAfterUnlike = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentAfterUnlike.getLikeCount());

        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(commentAfterUnlike,
                th.personChloeTenant1Dorf1));

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personChloeTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikeCommentRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_ALREADY_UNLIKED));

        final Comment commentAfterSecondUnlike = commentRepository.findById(comment.getId()).get();
        assertEquals(0, commentAfterSecondUnlike.getLikeCount());
    }

    @Test
    public void unlikeCommentRequest_notGroupMember_publicGroup() throws Exception {

        final Group group = th.groupAdfcAAP;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        // Setup liked state in repo
        likeCommentRepository.saveAndFlush(LikeComment.builder()
                .comment(comment)
                .person(th.personFranziTenant1Dorf2NoMember)
                .liked(true)
                .build());

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.comment.likeCount").value(0))
                .andExpect(jsonPath("$.comment.liked").value(false));
    }

    @Test
    public void unlikeCommentRequest_notGroupMember() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);

        mockMvc.perform(post("/grapevine/comment/event/unlikeCommentRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(new ClientUnlikeCommentRequest(comment.getId()))))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
    }

}
