/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CommentControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getComments_NoComments() throws Exception {
        mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(th.gossip2WithComments.getCreator(), th.appVariant4AllTenants))
                .param("postId", th.gossip1withImages.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getComments_ExactlyOneComment() throws Exception {
        //verify test helper data integrity
        assertEquals(th.gossip3WithComment, th.gossip3Comment1withOnlyImages.getPost());

        mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(th.gossip2WithComments.getCreator(), th.appVariant4AllTenants))
                .param("postId", th.gossip3WithComment.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(th.gossip3Comment1withOnlyImages.getId()))
                .andExpect(jsonPath("$[0].creator.id").value(th.gossip3Comment1withOnlyImages.getCreator().getId()))
                .andExpect(jsonPath("$[0].creator.firstName").value(th.gossip3Comment1withOnlyImages.getCreator().getFirstName()))
                .andExpect(jsonPath("$[0].creator.lastName").value(
                        shorten(th.gossip3Comment1withOnlyImages.getCreator().getLastName())))
                .andExpect(jsonPath("$[0].images", hasSize(th.gossip3Comment1withOnlyImages.getImages().size())))
                .andExpect(jsonPath("$[0].images[0].id").value(th.gossip3Comment1withOnlyImages.getImages().get(0).getId()))
                .andExpect(jsonPath("$[0].images[1].id").value(th.gossip3Comment1withOnlyImages.getImages().get(1).getId()))
                .andExpect(jsonPath("$[0].images[2].id").value(th.gossip3Comment1withOnlyImages.getImages().get(2).getId()))
                .andExpect(jsonPath("$[0].created").value(th.gossip3Comment1withOnlyImages.getCreated()))
                .andExpect(jsonPath("$[0].lastModified").value(th.gossip3Comment1withOnlyImages.getLastModified()))
                .andExpect(jsonPath("$[0].deleted").value(th.gossip3Comment1withOnlyImages.isDeleted()))
                .andExpect(jsonPath("$[0].postId").value(th.gossip3Comment1withOnlyImages.getPost().getId()))
                .andExpect(jsonPath("$[0].text", nullValue()))
                .andExpect(jsonPath("$[0].replyTo").doesNotExist());
    }

    @Test
    public void getComments_MoreThanOneComment() throws Exception {
        //verify test helper data integrity
        assertThat(th.gossip2Comments.size()).isGreaterThan(1);
        assertTrue(th.gossip2Comments
                .stream()
                .allMatch(c -> c.getPost().equals(th.gossip2WithComments)));

        mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(th.gossip2WithComments.getCreator(), th.appVariant4AllTenants))
                .param("postId", th.gossip2WithComments.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(th.gossip2Comments.size())))

                .andExpect(jsonPath("$[3].id").value(th.gossip2Comment1.getId()))
                .andExpect(jsonPath("$[3].creator.id").value(th.gossip2Comment1.getCreator().getId()))
                .andExpect(jsonPath("$[3].creator.firstName").value(th.gossip2Comment1.getCreator().getFirstName()))
                .andExpect(jsonPath("$[3].creator.lastName").value(
                        shorten(th.gossip2Comment1.getCreator().getLastName())))
                .andExpect(jsonPath("$[3].images").isEmpty())
                .andExpect(jsonPath("$[3].created").value(th.gossip2Comment1.getCreated()))
                .andExpect(jsonPath("$[3].lastModified").value(th.gossip2Comment1.getLastModified()))
                .andExpect(jsonPath("$[3].deleted").value(th.gossip2Comment1.isDeleted()))
                .andExpect(jsonPath("$[3].postId").value(th.gossip2Comment1.getPost().getId()))
                .andExpect(jsonPath("$[3].text").value(th.gossip2Comment1.getText()))
                .andExpect(jsonPath("$[3].replyTo").doesNotExist())

                .andExpect(jsonPath("$[2].id").value(th.gossip2Comment2Deleted.getId()))
                .andExpect(jsonPath("$[2].creator.id").value(th.gossip2Comment2Deleted.getCreator().getId()))
                .andExpect(jsonPath("$[2].creator.firstName").value(th.gossip2Comment2Deleted.getCreator().getFirstName()))
                .andExpect(jsonPath("$[2].creator.lastName").value(
                        shorten(th.gossip2Comment2Deleted.getCreator().getLastName())))
                .andExpect(jsonPath("$[2].images").isEmpty()) //since post is deleted, image must be censored
                .andExpect(jsonPath("$[2].created").value(th.gossip2Comment2Deleted.getCreated()))
                .andExpect(jsonPath("$[2].lastModified").value(th.gossip2Comment2Deleted.getLastModified()))
                .andExpect(jsonPath("$[2].deleted").value(th.gossip2Comment2Deleted.isDeleted()))
                .andExpect(jsonPath("$[2].postId").value(th.gossip2Comment2Deleted.getPost().getId()))
                .andExpect(jsonPath("$[2].text", nullValue())) //since post is deleted, text must be censored
                .andExpect(jsonPath("$[2].replyTo").doesNotExist())

                .andExpect(jsonPath("$[1].id").value(th.gossip2Comment3.getId()))
                .andExpect(jsonPath("$[1].creator.id").value(th.gossip2Comment3.getCreator().getId()))
                .andExpect(jsonPath("$[1].creator.firstName").value(th.gossip2Comment3.getCreator().getFirstName()))
                .andExpect(jsonPath("$[1].creator.lastName").value(
                        shorten(th.gossip2Comment3.getCreator().getLastName())))
                .andExpect(jsonPath("$[1].images").isEmpty())
                .andExpect(jsonPath("$[1].created").value(th.gossip2Comment3.getCreated()))
                .andExpect(jsonPath("$[1].lastModified").value(th.gossip2Comment3.getLastModified()))
                .andExpect(jsonPath("$[1].deleted").value(th.gossip2Comment3.isDeleted()))
                .andExpect(jsonPath("$[1].postId").value(th.gossip2Comment3.getPost().getId()))
                .andExpect(jsonPath("$[1].text").value(th.gossip2Comment3.getText()))
                .andExpect(jsonPath("$[1].replyTo.id").value(th.gossip2Comment1.getCreator().getId()))

                .andExpect(jsonPath("$[0].id").value(th.gossip2Comment4withImages.getId()))
                .andExpect(jsonPath("$[0].creator.id").value(th.gossip2Comment4withImages.getCreator().getId()))
                .andExpect(jsonPath("$[0].creator.firstName").value(
                        th.gossip2Comment4withImages.getCreator().getFirstName()))
                .andExpect(jsonPath("$[0].creator.lastName").value(
                        shorten(th.gossip2Comment4withImages.getCreator().getLastName())))
                .andExpect(jsonPath("$[0].replyTo.firstName").value(
                        th.gossip2Comment4withImages.getReplyTo().getCreator().getFirstName()))
                .andExpect(jsonPath("$[0].replyTo.lastName").value(shorten(
                        th.gossip2Comment4withImages.getReplyTo().getCreator().getLastName())))
                .andExpect(jsonPath("$[0].images", hasSize(th.gossip2Comment4withImages.getImages().size())))
                .andExpect(jsonPath("$[0].created").value(th.gossip2Comment4withImages.getCreated()))
                .andExpect(jsonPath("$[0].lastModified").value(th.gossip2Comment4withImages.getLastModified()))
                .andExpect(jsonPath("$[0].deleted").value(th.gossip2Comment4withImages.isDeleted()))
                .andExpect(jsonPath("$[0].postId").value(th.gossip2Comment3.getPost().getId()))
                .andExpect(jsonPath("$[0].text").value(th.gossip2Comment4withImages.getText()))
                .andExpect(jsonPath("$[0].replyTo.id").value(th.gossip2Comment3.getCreator().getId()));
    }

    @Test
    public void getComments_ParallelWorldInhabitant() throws Exception {
        //verify test helper data integrity
        int expectedNumComments = th.gossip2Comments.size();
        int expectedNumCommentsFiltered = th.gossip2Comments.size() - 1;
        assertThat(expectedNumComments).isGreaterThan(1);
        assertTrue(th.gossip2Comments
                .stream()
                .allMatch(c -> c.getPost().equals(th.gossip2WithComments)));

        //this should hide the comment this person created
        Person parallelWorldInhabitant = th.gossip2Comment3.getCreator();
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        Person caller = th.personAnnikaSchneider;
        assertNotEquals(caller, parallelWorldInhabitant);

        mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(caller, th.appVariant4AllTenants))
                .param("postId", th.gossip2WithComments.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedNumCommentsFiltered)));

        //no filtering should happen inside the parallel world
        mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(parallelWorldInhabitant, th.appVariant4AllTenants))
                .param("postId", th.gossip2WithComments.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedNumComments)));
    }

    @Test
    public void getComments_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/comment/")
                        .param("postId", th.gossip3WithComment.getId()),
                th.appVariant1Tenant1, th.personSusanneChristmann);
    }

    @Test
    public void getCommentById() throws Exception {

        for (Comment comment : th.gossip2Comments) {
            log.info("Checking comment {}", comment.getId());
            ResultActions actions = mockMvc.perform(get("/grapevine/comment/" + comment.getId())
                    .headers(authHeadersFor(th.gossip2WithComments.getCreator())))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.id").value(comment.getId()))
                    .andExpect(jsonPath("$.creator.id").value(comment.getCreator().getId()))
                    .andExpect(jsonPath("$.creator.firstName").value(comment.getCreator().getFirstName()))
                    .andExpect(jsonPath("$.creator.lastName").value(
                            shorten(comment.getCreator().getLastName())))
                    .andExpect(jsonPath("$.created").value(comment.getCreated()))
                    .andExpect(jsonPath("$.lastModified").value(comment.getLastModified()))
                    .andExpect(jsonPath("$.deleted").value(comment.isDeleted()))
                    .andExpect(jsonPath("$.postId").value(comment.getPost().getId()));
            if (comment.getReplyTo() != null) {
                actions.andExpect(jsonPath("$.replyTo.id").value(comment.getReplyTo().getCreator().getId()));
            }
            if (comment.isDeleted()) {
                actions.andExpect(jsonPath("$.text").isEmpty())
                        .andExpect(jsonPath("$.images").isEmpty());
            } else if (comment.getImages().isEmpty()) {
                actions.andExpect(jsonPath("$.text").value(comment.getText()))
                        .andExpect(jsonPath("$.images").isEmpty());
            } else {
                actions.andExpect(jsonPath("$.text").value(comment.getText()))
                        .andExpect(jsonPath("$.images", hasSize(comment.getImages().size())))
                        .andExpect(jsonPath("$.images[0].id").value(comment.getImages().get(0).getId()));
            }
        }
    }

    @Test
    public void getCommentById_Unauthorized() throws Exception {

        assertOAuth2(get("/grapevine/comment/" + th.gossip2Comment1.getId()));
    }

}
