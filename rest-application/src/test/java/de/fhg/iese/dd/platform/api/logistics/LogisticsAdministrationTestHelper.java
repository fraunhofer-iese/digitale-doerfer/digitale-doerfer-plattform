/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.DeliveryStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.LogisticsParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ParcelAddressRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxAllocationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxConfigurationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.ReceiverPickupAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.repos.LegalTextRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

@Component
public class LogisticsAdministrationTestHelper extends BaseSharedTestHelper {

    @Autowired
    protected PoolingStationRepository poolingStationRepository;

    @Autowired
    protected PoolingStationBoxRepository poolingStationBoxRepository;

    @Autowired
    public DeliveryRepository deliveryRepository;

    @Autowired
    public DeliveryStatusRecordRepository deliveryStatusRecordRepository;

    @Autowired
    protected TransportConfirmationRepository transportConfirmationRepository;

    @Autowired
    protected TransportAlternativeBundleRepository transportAlternativeBundleRepository;

    @Autowired
    protected TransportAlternativeBundleStatusRecordRepository transportAlternativeBundleStatusRecordRepository;

    @Autowired
    protected TransportAlternativeRepository transportAlternativeRepository;

    @Autowired
    protected TransportAlternativeStatusRecordRepository transportAlternativeStatusRecordRepository;

    @Autowired
    protected TransportAssignmentRepository transportAssignmentRepository;

    @Autowired
    protected TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;

    @Autowired
    protected PoolingStationBoxAllocationRepository poolingStationBoxAllocationRepository;

    @Autowired
    protected PoolingStationBoxConfigurationRepository poolingStationBoxConfigurationRepository;

    @Autowired
    protected ReceiverPickupAssignmentRepository receiverPickupAssignmentRepository;

    @Autowired
    protected PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    protected PurchaseOrderItemRepository purchaseOrderItemRepository;

    @Autowired
    protected ParcelAddressRepository parcelAddressRepository;

    @Autowired
    protected LogisticsParticipantRepository logisticsParticipantRepository;

    @Autowired
    protected LegalTextRepository legalTextRepository;

    @Autowired
    protected AppVariantRepository appVariantRepository;

    @Autowired
    protected IChatService chatService;

    protected List<Shop> shopList;
    protected Shop shop1;
    protected Shop shop2;
    protected Shop shop3;
    protected Shop shop4;

    protected List<PoolingStation> poolingStationList;
    protected PoolingStation poolingStation1;
    protected PoolingStation poolingStation2;
    protected PoolingStation poolingStation3;
    protected PoolingStation poolingStation4;

    protected List<PoolingStationBox> poolingStationBoxList;
    protected PoolingStationBox poolingStationBox1;
    protected PoolingStationBox poolingStationBox2;
    protected PoolingStationBox poolingStationBox3;
    protected PoolingStationBox poolingStationBox4;
    protected PoolingStationBox poolingStationBox5;
    protected PoolingStationBox poolingStationBox6;

    protected List<Delivery> deliveryList;
    public Delivery delivery1;
    protected Delivery delivery2;
    protected Delivery delivery3;
    protected Delivery delivery4;
    protected Delivery delivery5;

    protected List<DeliveryStatusRecord> deliveryStatusRecordList;
    protected DeliveryStatusRecord deliveryStatusRecord1_1;
    protected DeliveryStatusRecord deliveryStatusRecord1_2;

    protected List<TransportAlternativeBundle> transportAlternativeBundleList;
    protected TransportAlternativeBundle transportAlternativeBundle1;
    protected TransportAlternativeBundle transportAlternativeBundle2;
    protected TransportAlternativeBundle transportAlternativeBundle3;
    protected TransportAlternativeBundle transportAlternativeBundle4;
    protected TransportAlternativeBundle transportAlternativeBundle5;

    protected List<TransportAlternativeBundleStatusRecord> transportAlternativeBundleStatusRecordList;
    protected TransportAlternativeBundleStatusRecord transportAlternativeBundleStatusRecord4;

    protected List<TransportAssignment> transportAssignmentList;
    protected TransportAssignment transportAssignment1;
    protected TransportAssignment transportAssignment2;
    protected TransportAssignment transportAssignment3;

    protected List<PoolingStationBoxAllocation> poolingStationBoxAllocationList;
    protected PoolingStationBoxAllocation poolingStationBoxAllocation1;
    protected PoolingStationBoxAllocation poolingStationBoxAllocation2;

    protected List<PurchaseOrder> purchaseOrderList;
    protected PurchaseOrder purchaseOrder1;
    protected PurchaseOrder purchaseOrder2;
    protected PurchaseOrder purchaseOrder3;
    protected PurchaseOrder purchaseOrder4;
    protected PurchaseOrder purchaseOrder5;

    protected List<ParcelAddress> parcelAddressList;
    protected ParcelAddress parcelAddress1;
    protected ParcelAddress parcelAddress2;
    protected ParcelAddress parcelAddress3;

    public List<Address> addressList;
    public Address address1;
    public Address address2;
    public Address address3;

    @Override
    public void createShops() {
        shop1 = createShop1(nextTimeStamp());

        shop2 = new Shop();
        shop2.setCreated(nextTimeStamp());
        shop2.setTenant((tenant1));
        shop2 = shopRepository.save(shop2);

        shop3 = new Shop();
        shop3.setCreated(nextTimeStamp());
        shop3.setTenant((tenant2));
        shop3 = shopRepository.save(shop3);

        shop4 = new Shop();
        shop4.setCreated(nextTimeStamp());
        shop4.setTenant((tenant3));
        shop4 = shopRepository.save(shop4);

        shopList = Arrays.asList(shop1,shop2,shop3,shop4);
    }

    public void createPoolingStations() {

        super.createPoolingStations();
        createPoolingStation1();

        poolingStation2 = new PoolingStation();
        poolingStation2.setName("D-Station TU KL");
        poolingStation2.setTenant(tenant1);
        poolingStation2.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station TU KL")
                        .street("Gottlieb-Daimler-Straße 49")
                        .zip(
                                "67663")
                        .city("Kaiserslautern").gpsLocation(
                        new GPSLocation(49.423920, 7.755011)).build()));
        poolingStation2.setCreated(nextTimeStamp());
        poolingStation2 = poolingStationRepository.save(poolingStation2);

        poolingStation3 = new PoolingStation();
        poolingStation3.setName("D-Station Lidl");
        poolingStation3.setTenant(tenant2);
        poolingStation3.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station Lidl")
                        .street("Königstraße 115")
                        .zip("67655")
                        .city("Kaiserslautern").gpsLocation(new GPSLocation(49.437537, 7.756299)).build()));
        poolingStation3.setCreated(nextTimeStamp());
        poolingStation3 = poolingStationRepository.save(poolingStation3);

        poolingStation4 = new PoolingStation();
        poolingStation4.setName("D-Station ATU");
        poolingStation4.setTenant(tenant3);
        poolingStation4.setAddress(addressRepository.save(
                Address.builder()
                        .name("D-Station ATU")
                        .street("Pariser Straße 196")
                        .zip("67663")
                        .city("Kaiserslautern").gpsLocation(new GPSLocation(49.441573, 7.743977)).build()));
        poolingStation4.setCreated(nextTimeStamp());
        poolingStation4 = poolingStationRepository.save(poolingStation4);

        poolingStationList = Arrays.asList(poolingStation1, poolingStation2, poolingStation3, poolingStation4);

        poolingStationBox1 = new PoolingStationBox("Fach 1", 1, null, poolingStation1);
        poolingStationBox1.setCreated(nextTimeStamp());
        poolingStationBox1 = poolingStationBoxRepository.save(poolingStationBox1);

        poolingStationBox2 = new PoolingStationBox("Fach 2", 2, null, poolingStation1);
        poolingStationBox2.setCreated(nextTimeStamp());
        poolingStationBox2 = poolingStationBoxRepository.save(poolingStationBox2);

        poolingStationBox3 = new PoolingStationBox("Fach 1", 1, null, poolingStation2);
        poolingStationBox3.setCreated(nextTimeStamp());
        poolingStationBox3 = poolingStationBoxRepository.save(poolingStationBox3);

        poolingStationBox4 = new PoolingStationBox("Fach 2", 2, null, poolingStation2);
        poolingStationBox4.setCreated(nextTimeStamp());
        poolingStationBox4 = poolingStationBoxRepository.save(poolingStationBox4);

        poolingStationBox5 = new PoolingStationBox("Fach 1", 1, null, poolingStation3);
        poolingStationBox5.setCreated(nextTimeStamp());
        poolingStationBox5 = poolingStationBoxRepository.save(poolingStationBox5);

        poolingStationBox6 = new PoolingStationBox("Fach 1", 1, null, poolingStation4);
        poolingStationBox6.setCreated(nextTimeStamp());
        poolingStationBox6 = poolingStationBoxRepository.save(poolingStationBox6);

        poolingStationBoxList = Arrays.asList(
            poolingStationBox1,
            poolingStationBox2,
            poolingStationBox3,
            poolingStationBox4,
            poolingStationBox5,
            poolingStationBox6);
    }

    public void createLogisticScenario() {
        address1 = Address.builder()
                .name("Name1")
                .street("Street1")
                .zip("0815")
                .city("City1")
                .build();
        address1.setCreated(nextTimeStamp());
        address1 = addressRepository.save(address1);
        address2 = Address.builder()
                .name("Name2")
                .street("Street2")
                .zip("0815")
                .city("City2")
                .build();
        address2.setCreated(nextTimeStamp());
        address2 = addressRepository.save(address2);
        address3 = Address.builder()
                .name("Name3")
                .street("Street3")
                .zip("0815")
                .city("City3")
                .build();
        address3.setCreated(nextTimeStamp());
        address3 = addressRepository.save(address3);
        addressList = Arrays.asList(address1, address2, address3);

        parcelAddress1 = new ParcelAddress(address1, poolingStationBox1, poolingStation1);
        parcelAddress1.setCreated(nextTimeStamp());
        parcelAddress1 = parcelAddressRepository.save(parcelAddress1);
        parcelAddress2 = new ParcelAddress(address2, poolingStationBox2, poolingStation1);
        parcelAddress2.setCreated(nextTimeStamp());
        parcelAddress2 = parcelAddressRepository.save(parcelAddress2);
        parcelAddress3 = new ParcelAddress(address3, poolingStationBox3, poolingStation2);
        parcelAddress3.setCreated(nextTimeStamp());
        parcelAddress3 = parcelAddressRepository.save(parcelAddress3);
        parcelAddressList = Arrays.asList(parcelAddress1, parcelAddress2, parcelAddress3);

        delivery1 = Delivery.builder().build();
        delivery1.setTenant(tenant1);
        delivery1.setCreated(nextTimeStamp());
        delivery1.setContentNotes(
                "----------------------------------------------------------------------------------------------"
                        + "This is a test for the COBL type to be converted into String. Content Notes "
                        + "for delivery 1 in tenant 1, TEST"
                        +
                        "---------------------------------------------------------------------------------------------------------");
        delivery1 = deliveryRepository.save(delivery1);

        delivery2 = Delivery.builder().build();
        delivery2.setTenant(tenant1);
        delivery2.setCreated(nextTimeStamp());
        delivery2.setContentNotes(
                "----------------------------------------------------------------------------------------------"
                        + "This is a test for the COBL type to be converted into String. Content Notes "
                        + "for delivery 2 in tenant 1, TEST"
                        +
                        "---------------------------------------------------------------------------------------------------------");
        delivery2 = deliveryRepository.save(delivery2);

        delivery3 = Delivery.builder().build();
        delivery3.setTenant(tenant1);
        delivery3.setCreated(nextTimeStamp());
        delivery3.setContentNotes(
                "----------------------------------------------------------------------------------------------"
                        + "This is a test for the COBL type to be converted into String. Content Notes "
                        + "for delivery 3 in tenant 1, TEST"
                        +
                        "---------------------------------------------------------------------------------------------------------");
        delivery3 = deliveryRepository.save(delivery3);

        delivery4 = Delivery.builder().build();
        delivery4.setTenant(tenant1);
        delivery4.setCreated(nextTimeStamp());
        delivery4.setContentNotes(
                "----------------------------------------------------------------------------------------------"
                        + "This is a test for the COBL type to be converted into String. Content Notes "
                        + "for delivery 4 in tenant 1, TEST"
                        +
                        "---------------------------------------------------------------------------------------------------------");
        delivery4 = deliveryRepository.save(delivery4);

        delivery5 = Delivery.builder().build();
        delivery5.setTenant(tenant2);
        delivery5.setCreated(nextTimeStamp());
        delivery5.setContentNotes(
                "----------------------------------------------------------------------------------------------"
                        + "This is a test for the COBL type to be converted into String. Content Notes "
                        + "for delivery 5 in tenant 2, TEST"
                        +
                        "---------------------------------------------------------------------------------------------------------");
        delivery5 = deliveryRepository.save(delivery5);
        deliveryList = Arrays.asList(delivery1, delivery2, delivery3, delivery4, delivery5);

        deliveryStatusRecord1_1 = DeliveryStatusRecord.builder()
                .delivery(delivery2)
                .timeStamp(1512914400000L)
                .status(DeliveryStatus.CREATED)
                .build();
        deliveryStatusRecord1_1.setCreated(1512914400000L);
        deliveryStatusRecord1_1 = deliveryStatusRecordRepository.save(deliveryStatusRecord1_1);

        deliveryStatusRecord1_2 = DeliveryStatusRecord.builder()
                .delivery(delivery2)
                .timeStamp(1497099600000L)
                .status(DeliveryStatus.CREATED)
                .build();
        deliveryStatusRecord1_2.setCreated(1497099600000L);
        deliveryStatusRecord1_2 = deliveryStatusRecordRepository.save(deliveryStatusRecord1_2);
        deliveryStatusRecordList = Arrays.asList(deliveryStatusRecord1_1, deliveryStatusRecord1_2);

        purchaseOrder1 = new PurchaseOrder();
        purchaseOrder1.setTenant(tenant1);
        purchaseOrder1.setDelivery(delivery1);
        purchaseOrder1.setReceiver(personPoolingStationOp);
        purchaseOrder1.setShopOrderId("PurchaseOrder1");
        purchaseOrder1.setCreated(nextTimeStamp());
        purchaseOrder1 = purchaseOrderRepository.save(purchaseOrder1);

        purchaseOrder2 = new PurchaseOrder();
        purchaseOrder2.setTenant(tenant1);
        purchaseOrder2.setDelivery(delivery2);
        purchaseOrder2.setReceiver(personVgAdmin);
        purchaseOrder2.setShopOrderId("PurchaseOrder2");
        purchaseOrder2.setCreated(nextTimeStamp());
        purchaseOrder2 = purchaseOrderRepository.save(purchaseOrder2);

        purchaseOrder3 = new PurchaseOrder();
        purchaseOrder3.setTenant(tenant1);
        purchaseOrder3.setDelivery(delivery3);
        purchaseOrder3.setReceiver(personPoolingStationOp);
        purchaseOrder3.setShopOrderId("PurchaseOrder3");
        purchaseOrder3.setCreated(nextTimeStamp());
        purchaseOrder3 = purchaseOrderRepository.save(purchaseOrder3);

        purchaseOrder4 = new PurchaseOrder();
        purchaseOrder4.setTenant(tenant1);
        purchaseOrder4.setDelivery(delivery4);
        purchaseOrder4.setReceiver(personIeseAdmin);
        purchaseOrder4.setShopOrderId("PurchaseOrder4");
        purchaseOrder4.setCreated(nextTimeStamp());
        purchaseOrder4 = purchaseOrderRepository.save(purchaseOrder4);

        purchaseOrder5 = new PurchaseOrder();
        purchaseOrder5.setTenant(tenant2);
        purchaseOrder5.setDelivery(delivery5);
        purchaseOrder5.setReceiver(personVgAdmin);
        purchaseOrder5.setShopOrderId("PurchaseOrder5");
        purchaseOrder5.setCreated(nextTimeStamp());
        purchaseOrder5 = purchaseOrderRepository.save(purchaseOrder5);

        purchaseOrderList = Arrays.asList(purchaseOrder1, purchaseOrder2, purchaseOrder3, purchaseOrder4, purchaseOrder5);

        transportAlternativeBundle1 = new TransportAlternativeBundle();
        transportAlternativeBundle1.setDelivery(delivery1);
        transportAlternativeBundle1.setCreated(nextTimeStamp());
        transportAlternativeBundle1 = transportAlternativeBundleRepository.save(transportAlternativeBundle1);

        transportAlternativeBundle2 = new TransportAlternativeBundle();
        transportAlternativeBundle2.setDelivery(delivery2);
        transportAlternativeBundle2.setCreated(nextTimeStamp());
        transportAlternativeBundle2 = transportAlternativeBundleRepository.save(transportAlternativeBundle2);

        transportAlternativeBundle3 = new TransportAlternativeBundle();
        transportAlternativeBundle3.setDelivery(delivery3);
        transportAlternativeBundle3.setCreated(nextTimeStamp());
        transportAlternativeBundle3 = transportAlternativeBundleRepository.save(transportAlternativeBundle3);

        transportAlternativeBundle4 = new TransportAlternativeBundle();
        transportAlternativeBundle4.setDelivery(delivery4);
        transportAlternativeBundle4.setCreated(nextTimeStamp());
        transportAlternativeBundle4 = transportAlternativeBundleRepository.save(transportAlternativeBundle4);

        transportAlternativeBundle5 = new TransportAlternativeBundle();
        transportAlternativeBundle5.setDelivery(delivery5);
        transportAlternativeBundle5.setCreated(nextTimeStamp());
        transportAlternativeBundle5 = transportAlternativeBundleRepository.save(transportAlternativeBundle5);

        transportAlternativeBundleList = Arrays.asList(
            transportAlternativeBundle1,
            transportAlternativeBundle2,
            transportAlternativeBundle3,
            transportAlternativeBundle4,
            transportAlternativeBundle5);

        transportAlternativeBundleStatusRecord4 = TransportAlternativeBundleStatusRecord.builder().build();
        transportAlternativeBundleStatusRecord4.setTransportAlternativeBundle(transportAlternativeBundle4);
        transportAlternativeBundleStatusRecord4.setStatus(TransportAlternativeBundleStatus.WAITING_FOR_ASSIGNMENT);
        transportAlternativeBundleStatusRecord4.setTimestamp(nextTimeStamp());
        transportAlternativeBundleStatusRecord4 = transportAlternativeBundleStatusRecordRepository.save(transportAlternativeBundleStatusRecord4);
        transportAlternativeBundleStatusRecordList = Collections.singletonList(transportAlternativeBundleStatusRecord4);

        transportAssignment1 = new TransportAssignment();
        transportAssignment1.setDelivery(delivery1);
        transportAssignment1.setCarrier(personVgAdmin);
        transportAssignment1.setCreated(nextTimeStamp());
        transportAssignment1.setDeliveryAddress(parcelAddress1);
        transportAssignment1 = transportAssignmentRepository.save(transportAssignment1);

        transportAssignment2 = new TransportAssignment();
        transportAssignment2.setDelivery(delivery2);
        transportAssignment2.setCarrier(personIeseAdmin);
        transportAssignment2.setCreated(nextTimeStamp());
        transportAssignment2.setDeliveryAddress(parcelAddress2);
        transportAssignment2 = transportAssignmentRepository.save(transportAssignment2);

        transportAssignment3 = new TransportAssignment();
        transportAssignment3.setDelivery(delivery3);
        transportAssignment3.setCarrier(personIeseAdmin);
        transportAssignment3.setCreated(nextTimeStamp());
        transportAssignment3.setDeliveryAddress(parcelAddress3);
        transportAssignment3 = transportAssignmentRepository.save(transportAssignment3);
        transportAssignmentList = Arrays.asList(transportAssignment1, transportAssignment2, transportAssignment3);

        poolingStationBoxAllocation1 = poolingStationBoxAllocationRepository.save(PoolingStationBoxAllocation.builder()
            .delivery(delivery1)
            .poolingStationBox(poolingStationBox1)
            .timestamp(nextTimeStamp())
            .build());

        poolingStationBoxAllocation2 = poolingStationBoxAllocationRepository.save(PoolingStationBoxAllocation.builder()
            .delivery(delivery2)
            .poolingStationBox(poolingStationBox2)
            .timestamp(nextTimeStamp())
            .build());

        poolingStationBoxAllocationList = Arrays.asList(poolingStationBoxAllocation1, poolingStationBoxAllocation2);
    }

    private void createPoolingStation1() {
        OpeningHours openingHoursPoolingStation1 = createOpeningHours(
            new OpeningHoursEntry(DayOfWeek.MONDAY   ).withFromHours(9).withToHours(16),
            new OpeningHoursEntry(DayOfWeek.TUESDAY  ).withFromHours(9).withToHours(16),
            new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
            new OpeningHoursEntry(DayOfWeek.THURSDAY ).withFromHours(9).withToHours(16),
            new OpeningHoursEntry(DayOfWeek.FRIDAY   ).withFromHours(9).withToHours(16));

        poolingStation1 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station Bahnhof")
                                .street("Zollamtstraße 8")
                                .zip("67663")
                                .city("Kaiserslautern")
                                .gpsLocation(new GPSLocation(49.435450, 7.769099))
                                .build()),
                "D-Station Bahnhof",
                "42",
                mediaItemRepository.save(createMediaItem("imagePoolingStation1")),
                tenant1,
                openingHoursPoolingStation1,
                PoolingStationType.MANUAL,
                false,
                true,
                true,
                true);
        poolingStation1.setCreated(nextTimeStamp());
        poolingStation1 = poolingStationRepository.save(poolingStation1);
    }

}
