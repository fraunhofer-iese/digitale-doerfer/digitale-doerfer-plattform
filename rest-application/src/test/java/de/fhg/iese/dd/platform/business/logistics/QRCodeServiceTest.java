/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.logistics;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.RestApplication;
import de.fhg.iese.dd.platform.business.logistics.services.IQRCodeService;
import de.fhg.iese.dd.platform.business.shared.html2pdf.exceptions.Html2PdfException;
import de.fhg.iese.dd.platform.business.shared.html2pdf.services.IHtml2PdfService;

@SpringBootTest(classes = RestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class QRCodeServiceTest {

    private static final String PREFIX_TRANSPORT_NOTES = "Ablageort";
    private static final String PREFIX_CONTENT_NOTES = "Inhaltshinweis";

    String receiverName = "Anne Besteller";
    String receiverAddressName = "D-Station HIT Markt";
    String receiverStreet = "Konrad-Adenauer-Straße 1";
    String receiverCity = "67304 Eisenberg";

    String senderName = "Susi Sucher";
    String senderStreet = "Finderweg";
    String senderCity = "67304 Eisenberg";

    String trackingCode = "123456";
    String qrCodeText = "123456797298784378237";

    @Autowired
    private IQRCodeService qrCodeService;

    @Autowired
    private IHtml2PdfService html2PdfService;

    /**
     * Manually set to true to generate html labels at %temp%
     */
    private static final boolean HTML_TO_TMP = false;
    /**
     * Manually set to true to generate pdf labels at %temp%
     */
    private static final boolean PDF_TO_TEMP = false;

    @Test
    public void singleParcelNoNotes() throws IOException {
        int parcelCount = 1;
        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                null, null, trackingCode, qrCodeText, parcelCount);

        assertTrue(output.contains(receiverName));
        assertTrue(output.contains(receiverAddressName));
        assertTrue(output.contains(receiverStreet));
        assertTrue(output.contains(receiverCity));

        assertTrue(output.contains(senderName));
        assertTrue(output.contains(senderStreet));
        assertTrue(output.contains(senderCity));

        assertTrue(output.contains(trackingCode));

        //check if contains the prefix for Base64
        assertTrue(output.contains("data:image/png;base64,"));

        writeToFile(output, "singleParcelNoNotes");
    }

    @Test
    public void onlyOneParcel() throws IOException {
        int parcelCount = 1;
        int parcelNumber = 1;
        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                null, null, trackingCode, qrCodeText, parcelCount);
        assertFalse(output.contains("Paket " + parcelNumber + " von " + parcelCount));

        writeToFile(output, "onlyOneParcel");
    }

    @Test
    public void moreThanOneParcel() throws IOException {
        int parcelCount = 2;

        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                null, null, trackingCode, qrCodeText, parcelCount);

        for (int i = 1; i <= parcelCount; i++) {
            assertTrue(output.contains("Paket " + i + " von " + parcelCount));
        }

        writeToFile(output, "moreThanOneParcel");
    }

    @Test
    public void singleParcelWithNotes() throws IOException {
        int parcelCount = 1;
        String transportNotes = "Bitte kühl lagern";
        String contentNotes = "Bitte beim Nachbar abgeben";
        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                transportNotes, contentNotes, trackingCode, qrCodeText, parcelCount);

        assertTrue(output.contains(PREFIX_TRANSPORT_NOTES));
        assertTrue(output.contains(PREFIX_CONTENT_NOTES));
        assertTrue(output.contains(transportNotes));
        assertTrue(output.contains(contentNotes));

        writeToFile(output, "singleParcelWithNotes");
    }

    @Test
    public void noTransportNote() throws IOException {
        int parcelCount = 1;

        String contentNotes = "Bitte beim Nachbar abgeben";
        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                null, contentNotes, trackingCode, qrCodeText, parcelCount);

        assertFalse(output.contains(PREFIX_TRANSPORT_NOTES));
        assertTrue(output.contains(PREFIX_CONTENT_NOTES));
        assertTrue(output.contains(contentNotes));

        writeToFile(output, "noTransportNote");
    }

    @Test
    public void noContentNote() throws IOException {
        int parcelCount = 1;

        String transportNotes = "Bitte kühl lagern";
        String output = qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName,
                receiverStreet, receiverCity,
                senderName, senderStreet, senderCity,
                transportNotes, null, trackingCode, qrCodeText, parcelCount);
        assertFalse(output.contains(PREFIX_CONTENT_NOTES));
        assertTrue(output.contains(PREFIX_TRANSPORT_NOTES));
        assertTrue(output.contains(transportNotes));

        writeToFile(output, "noContentNote");
    }

    @Test
    public void multiPage() throws IOException {
        String transportNotes = "Bitte kühl lagern";
        String contentNotes = "Bitte beim Nachbar abgeben";
        int parcelCount = 3;
        String output =
                qrCodeService.generateQRCodeHTML(receiverName, receiverAddressName, receiverStreet, receiverCity,
                        senderName, senderStreet, senderCity, transportNotes, contentNotes, trackingCode, qrCodeText,
                        parcelCount);
        //TODO: check number of page-breaks maybe?

        writeToFile(output, "multiPage");
    }

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (HTML_TO_TMP || PDF_TO_TEMP) {
            BaseServiceTest.deleteTempFilesStartingWithClassName(QRCodeServiceTest.class);
        }
    }

    private void writeToFile(String content, String fileName) throws IOException {
        if (HTML_TO_TMP) {
            Path tempFile = Files.createTempFile(getClass().getSimpleName() + "-" + fileName, ".html");
            Files.write(tempFile, content.getBytes(StandardCharsets.UTF_8));
        }

        if (PDF_TO_TEMP) {
            try {
                byte[] fileContent = html2PdfService.convert(content, 160, 90);
                Path tempFile = Files.createTempFile(getClass().getSimpleName() + "-" + fileName, ".pdf");
                Files.write(tempFile, fileContent);
                fileContent = html2PdfService.convertA4(content);
                tempFile = Files.createTempFile(getClass().getSimpleName() + "-a4_" + fileName, ".pdf");
                Files.write(tempFile, fileContent);
            } catch (Html2PdfException ex) {
                throw new RuntimeException("error converting to pdf", ex);
            }
        }
    }

}
