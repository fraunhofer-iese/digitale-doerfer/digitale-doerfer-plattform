/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Alberto Lara, Johannes Schwarz, Johannes Schneider, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.misc.clientevent.ClientReferenceDataDeletionRequest;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

public class SharedAdminControllerTest extends BaseServiceTest {

    private static final String DROP_DATA_CHECK = "i am sure to drop all data";

    @Autowired
    private TestEnvironmentService environmentService;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestEmailSenderService testEmailSenderService;
    @Autowired
    private ApplicationConfig applicationConfig;
    @Autowired
    private List<BaseAdminTask> adminTasks;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        //valid selection
        th.selectGeoAreaForAppVariant(th.app1Variant1, th.personRegularAnna, th.geoAreaKaiserslautern);
        //invalid selection that is not available in app variant
        th.selectGeoAreaForAppVariant(th.app1Variant2, th.personVgAdmin, th.geoAreaMainz);
        //selection without any children, intended to trigger the according admin task CleanupGeoAreaSelectionsChildren
        th.selectGeoAreaForAppVariant(th.app1Variant2, th.personRegularKarl, th.geoAreaEisenberg);
        //selection without all parents, intended to trigger the according admin task CleanupGeoAreaSelectionsParents
        th.selectGeoAreaForAppVariant(th.app1Variant2, th.personIeseAdmin, th.geoAreaDorf2InEisenberg);
        Chat chat =
                th.chatService.createChatWithSubjectsAndParticipants("", th.pushCategory1App1, Collections.emptyList(),
                        Arrays.asList(th.personRegularAnna, th.personRegularHorstiTenant3));
        th.chatService.addUserTextMessageToChat(chat, th.personRegularAnna, "Do you have for me le massage?", 42L);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void adminEndpointsUnauthorized() throws Exception {

        assertRequiresAdmin(get("/administration/adminTasks"));
        assertRequiresAdmin(put("/administration/adminTasks/execute"));
        assertRequiresAdmin(get("/administration/info/clientexceptiontypes"));
        assertRequiresAdmin(get("/administration/initialize"));
        assertRequiresAdmin(get("/administration/initialize/topic"));
        assertRequiresAdmin(get("/administration/initialize/useCase"));
        assertRequiresAdmin(put("/administration/initialize/createInitialData"));
        assertRequiresAdmin(delete("/administration/initialize/dropData"));
        assertRequiresAdmin(delete("/administration/referenceData/delete"));
        assertRequiresAdmin(post("/administration/sendEmail"));
        assertRequiresAdmin(post("/administration/shared/sms/send"));
        assertRequiresAdmin(post("/administration/shared/sms/validate"));
    }

    @Test
    public void listAdminTasksTest() throws Exception {

        mockMvc.perform(get("/administration/adminTasks")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void executeAdminTasksTest_DryRun() throws Exception {

        List<String> adminTasksToTest = adminTasks.stream()
                .map(BaseAdminTask::getName)
                .collect(Collectors.toList());

        //some tasks throw exceptions on parallel run, because H2 can not handle concurrent transactions in the same way as MySQL
        for (String adminTask : adminTasksToTest) {

            log.info("\n\nExecuting {}\n", adminTask);

            mockMvc.perform(put("/administration/adminTasks/execute")
                            .param("adminTaskName", adminTask)
                            .param("dryRun", "true")
                            .headers(authHeadersFor(th.personSuperAdmin))
                    .contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$").isNotEmpty());

            log.info("\n\nFinished executing {}\n", adminTask);
        }
    }

    @Test
    public void executeSampleAdminTasksTest() throws Exception {

        List<String> adminTasksToTest = adminTasks.stream()
                .map(BaseAdminTask::getName)
                .collect(Collectors.toList());

        //some tasks throw exceptions on parallel run, because H2 can not handle concurrent transactions in the same way as MySQL
        for (String adminTask : adminTasksToTest) {

            log.info("\n\nExecuting {}\n", adminTask);

            mockMvc.perform(put("/administration/adminTasks/execute")
                            .param("adminTaskName", adminTask)
                            .param("dryRun", "false")
                            .headers(authHeadersFor(th.personSuperAdmin))
                    .contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$").isNotEmpty());

            log.info("\n\nFinished executing {}\n", adminTask);
        }
    }

    @Test
    public void getClientExceptionTypesTest() throws Exception {

        mockMvc.perform(get("/administration/info/clientexceptiontypes")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void listDataInitializers() throws Exception {

        mockMvc.perform(get("/administration/initialize")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());

        mockMvc.perform(get("/administration/initialize/topic")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());

        mockMvc.perform(get("/administration/initialize/useCase")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    /*
     * This test combines the create and drop test, because create initial data
     * is so time consuming and a prerequisite for drop data
     */
    @Test
    // integration test because it checks out the data init files from git
    @ExtendWith(AssumeIntegrationTestExtension.class)
    @Tag("data-init")
    public void createAndDropInitialDataTestAllData() throws Exception {

        //test create initial data
        mockMvc.perform(put("/administration/initialize/createInitialData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("topics", "*"))
                .andExpect(status().isCreated());

        //test if all app variants have at least one legal text
        List<String> appVariantIdentifiersWithoutLegals = th.appVariantRepository.findAll().stream()
                .map(a -> {
                    if (CollectionUtils.isEmpty(
                            th.legalTextRepository.findAllByAppVariantOrAppVariantNullAndCurrentlyActiveOrderByOrderValue(
                                    a, System.currentTimeMillis()))) {
                        return a.getAppVariantIdentifier();
                    } else {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        assertTrue(appVariantIdentifiersWithoutLegals.isEmpty(),
                "No legal texts defined for app variants, check data init configuration " +
                        "and provide LegalAppDataInitializer in all modules!\n" + appVariantIdentifiersWithoutLegals);

        //required to skip the cool down
        timeServiceMock.setOffset(2, ChronoUnit.MINUTES);

        //test drop initial data
        mockMvc.perform(delete("/administration/initialize/dropData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("topics", "*")
                        .param("check", DROP_DATA_CHECK))
                .andExpect(status().isOk());
        //the file deletion of the checked out data might take a while
        waitForEventProcessingLong();
    }

    @Test
    // integration test because it checks out the data init files from git
    @ExtendWith(AssumeIntegrationTestExtension.class)
    @Tag("data-init")
    public void createAndDropInitialDataTestAllDataStage() throws Exception {

        environmentService.setActiveProfilesOverride(environmentService.getActiveProfiles(false)
                .stream()
                .map(profile -> "test".equals(profile) ? "stage" : profile) //replace test with stage profile
                .collect(Collectors.toSet()));
        try {
            createAndDropInitialDataTestAllData(); //run test with changed profile
        } finally {
            environmentService.removeActiveProfilesOverride();
        }
        //the file deletion of the checked out data might take a while
        waitForEventProcessingLong();
    }

    @Test
    // integration test because it checks out the data init files from git
    @ExtendWith(AssumeIntegrationTestExtension.class)
    @Tag("data-init")
    public void createAndDropInitialDataTestAllDataProd() throws Exception {

        environmentService.setActiveProfilesOverride(environmentService.getActiveProfiles(false)
                .stream()
                .map(profile -> "test".equals(profile) ? "prod" : profile) //replace test with prod profile
                .collect(Collectors.toSet()));
        try {
            createAndDropInitialDataTestAllData(); //run test with changed profile
        } finally {
            environmentService.removeActiveProfilesOverride();
        }
        //the file deletion of the checked out data might take a while
        waitForEventProcessingLong();
    }

    @Test
    // integration test because it checks out the data init files from git
    @ExtendWith(AssumeIntegrationTestExtension.class)
    @Tag("data-init")
    public void createAndDropInitialDataTestPartial() throws Exception {

        mockMvc.perform(put("/administration/initialize/createInitialData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("topics", "tenant, person"))
                .andExpect(status().isCreated());

        //required to skip the cool down
        timeServiceMock.setOffset(2, ChronoUnit.MINUTES);

        mockMvc.perform(delete("/administration/initialize/dropData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("check", DROP_DATA_CHECK)
                .param("topics", "tenant, person"))
                .andExpect(status().isOk());
        //the file deletion of the checked out data might take a while
        waitForEventProcessingLong();
    }

    @Test
    // integration test because it checks out the data init files from git
    @ExtendWith(AssumeIntegrationTestExtension.class)
    @Tag("data-init")
    public void createInitialDataUseCase() throws Exception {

        //this is required for all apps to have valid oauth clients
        mockMvc.perform(put("/administration/initialize/createInitialData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("topics", "oauth"))
                .andExpect(status().isCreated());

        //required to skip the cool down
        timeServiceMock.setOffset(2, ChronoUnit.MINUTES);

        mockMvc.perform(put("/administration/initialize/createInitialData")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .param("useCase", "new_tenant"))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("tenant")))
                .andExpect(content().string(containsString("app")));
        //the file deletion of the checked out data might take a while
        waitForEventProcessingLong();
    }

    @Test
    public void sendEmail_Defaults() throws Exception {

        String emailAddress = "testmail@test";
        String subject = "test-subject";
        String text = "Email-Text";

        mockMvc.perform(post("/administration/sendEmail")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("toEmail", emailAddress)
                .param("subject", subject)
                .contentType(MediaType.TEXT_PLAIN)
                .content(text))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(emailAddress)))
                .andExpect(content().string(containsString(subject)))
                .andExpect(content().string(containsString(text)));

        TestEmailSenderService.TestEmail testEmail = testEmailSenderService.getEmails().get(0);
        assertEquals(applicationConfig.getEmail().getAddress(), testEmail.getSenderEmailAddress());
        assertEquals(emailAddress, testEmail.getRecipientEmailAddress());
        assertEquals(subject, testEmail.getSubject());
        assertEquals(text, testEmail.getText());
    }

    @Test
    public void sendEmail_Custom() throws Exception {

        String fromEmailAddress = "dd@dd";
        String emailAddress = "testmail@test";
        String toName = "Horsti Börsti 😎";
        String subject = "test-subject";
        String text = "Email-Text";

        mockMvc.perform(post("/administration/sendEmail")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("fromEmail", fromEmailAddress)
                .param("toName", toName)
                .param("toEmail", emailAddress)
                .param("subject", subject)
                .contentType(MediaType.TEXT_PLAIN)
                .content(text))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(emailAddress)))
                .andExpect(content().string(containsString(subject)))
                .andExpect(content().string(containsString(text)));

        TestEmailSenderService.TestEmail testEmail = testEmailSenderService.getEmails().get(0);
        assertEquals(fromEmailAddress, testEmail.getSenderEmailAddress());
        assertEquals(toName, testEmail.getRecipientName());
        assertEquals(emailAddress, testEmail.getRecipientEmailAddress());
        assertEquals(subject, testEmail.getSubject());
        assertEquals(text, testEmail.getText());
    }

    @Test
    public void sendSMS() throws Exception {

        mockMvc.perform(post("/administration/shared/sms/send")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("senderId", "test-sender")
                .param("recipientNumber", "0631/6800-2276")
                .param("check", "i have no idea what i am doing")
                .contentType(MediaType.TEXT_PLAIN)
                .content("¿A dónde va el mar?"))
                .andExpect(isNotAuthorized());

        mockMvc.perform(post("/administration/shared/sms/send")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("senderId", "test-sender")
                .param("recipientNumber", "0631/6800-2276")
                .param("check", "i know what i am doing")
                .contentType(MediaType.TEXT_PLAIN)
                .content("Hallo Balthasar!"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("deactivated")));
    }

    @Test
    public void validatePhoneNumber() throws Exception {

        String number = "0631/6800-2276";
        mockMvc.perform(post("/administration/shared/sms/validate")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("number", number)
                .param("countryCode", "ES")
                .param("check", "i have no idea what i am doing")
                .contentType(contentType))
                .andExpect(isNotAuthorized());

        // twilio is deactivated, so this will just return the same number
        mockMvc.perform(post("/administration/shared/sms/validate")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("number", number)
                .param("check", "i know what i am doing")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.number").value(number));
    }

    @Test
    public void getPushEventOverview() throws Exception {

        mockMvc.perform(get("/administration/info/pushEvents/plain")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .contentType(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk());
    }

    @Test
    @Tag("delete_reference_data")
    public void deleteReferenceData_AppVariant() throws Exception {

        AppVariant appVariant = th.app1Variant1;

        ClientReferenceDataDeletionRequest request = ClientReferenceDataDeletionRequest.builder()
                .classNameEntity(AppVariant.class.getName())
                .idEntityToBeDeleted(appVariant.getId())
                .deleteDependentEntities(true)
                .build();
        String check = callDeleteReferenceDataAndGetCheckToken(request, "not correct");

        callDeleteReferenceDataAndGetCheckToken(request, check);

        assertFalse(th.appVariantRepository.existsById(appVariant.getId()));
    }

    @Test
    @Tag("delete_reference_data")
    public void deleteReferenceData_Tenant() throws Exception {

        Tenant tenantToBeDeleted = th.tenant1;
        Tenant tenantToBeUsedInstead = th.tenant2;

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.toString())
                .enabled(false)
                .build());
        FeatureConfig configTenant = featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.toString())
                .tenant(tenantToBeDeleted)
                .enabled(true)
                .build());

        Person personImpacted = th.personRegularAnna;
        personImpacted.setTenant(tenantToBeDeleted);
        th.personRepository.saveAndFlush(personImpacted);

        ClientReferenceDataDeletionRequest request = ClientReferenceDataDeletionRequest.builder()
                .classNameEntity(Tenant.class.getName())
                .idEntityToBeDeleted(tenantToBeDeleted.getId())
                .idEntityToBeUsedInstead(tenantToBeUsedInstead.getId())
                .deleteDependentEntities(false)
                .build();
        String check = callDeleteReferenceDataAndGetCheckToken(request, "not correct");

        callDeleteReferenceDataAndGetCheckToken(request, check);

        assertFalse(th.tenantRepository.existsById(tenantToBeDeleted.getId()));
        Person personChanged = th.personRepository.findById(personImpacted.getId()).get();
        assertThat(personChanged.getTenant()).isEqualTo(tenantToBeUsedInstead);
        assertFalse(featureConfigRepository.existsById(configTenant.getId()));
    }

    @Test
    @Tag("delete_reference_data")
    public void deleteReferenceData_AppVariant_TooManyUsages() throws Exception {

        AppVariant appVariant = th.app1Variant1;

        for (int i = 0; i <= IAppService.APP_VARIANT_DELETION_MAX_USAGES; i++) {
            Person person = Person.builder()
                    .firstName("first " + i)
                    .email("email_" + i)
                    .build();
            person = th.personRepository.save(person);
            th.appVariantUsageRepository.save(AppVariantUsage.builder()
                    .appVariant(appVariant)
                    .person(person)
                    .build());
        }

        ClientReferenceDataDeletionRequest request = ClientReferenceDataDeletionRequest.builder()
                .classNameEntity(AppVariant.class.getName())
                .idEntityToBeDeleted(appVariant.getId())
                .deleteDependentEntities(true)
                .build();
        String check = callDeleteReferenceDataAndGetCheckToken(request, "not correct");

        callDeleteReferenceDataAndGetCheckToken(th.personSuperAdmin, request, check, status().isInternalServerError());

        assertTrue(th.appVariantRepository.existsById(appVariant.getId()));
    }

    @Test
    @Tag("delete_reference_data")
    public void deleteReferenceData_Unauthorized() throws Exception {

        ClientReferenceDataDeletionRequest request = ClientReferenceDataDeletionRequest.builder()
                .classNameEntity(Tenant.class.getName())
                .idEntityToBeDeleted(th.tenant1.getId())
                .deleteDependentEntities(true)
                .build();
        callDeleteReferenceDataAndGetCheckToken(th.personRegularKarl, request, "does not matter", isNotAuthorized());
    }

    @Test
    @Tag("delete_reference_data")
    public void deleteReferenceData_InvalidParameters() throws Exception {

        //invalid class name
        callDeleteReferenceDataAndGetCheckToken(
                th.personSuperAdmin,
                ClientReferenceDataDeletionRequest.builder()
                        .classNameEntity("Klasse 3a")
                        .idEntityToBeDeleted(th.tenant1.getId())
                        .deleteDependentEntities(true)
                        .build(),
                "does not matter",
                isException(ClientExceptionType.UNSPECIFIED_NOT_FOUND));

        //missing parameter
        callDeleteReferenceDataAndGetCheckToken(
                th.personSuperAdmin,
                ClientReferenceDataDeletionRequest.builder()
                        .classNameEntity(Tenant.class.getName())
                        .idEntityToBeDeleted(th.tenant1.getId())
                        .deleteDependentEntities(false)
                        .build(),
                "does not matter",
                isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //invalid id
        callDeleteReferenceDataAndGetCheckToken(
                th.personSuperAdmin,
                ClientReferenceDataDeletionRequest.builder()
                        .classNameEntity(Tenant.class.getName())
                        .idEntityToBeDeleted(th.tenant1.getId() + ":-(")
                        .idEntityToBeUsedInstead(th.tenant2.getId())
                        .deleteDependentEntities(false)
                        .build(),
                "does not matter",
                isException(ClientExceptionType.UNSPECIFIED_NOT_FOUND));

        callDeleteReferenceDataAndGetCheckToken(
                th.personSuperAdmin,
                ClientReferenceDataDeletionRequest.builder()
                        .classNameEntity(Tenant.class.getName())
                        .idEntityToBeDeleted(th.tenant1.getId())
                        .idEntityToBeUsedInstead(th.tenant2.getId() + ":-D")
                        .deleteDependentEntities(false)
                        .build(),
                "does not matter",
                isException(ClientExceptionType.UNSPECIFIED_NOT_FOUND));
    }

    public String callDeleteReferenceDataAndGetCheckToken(ClientReferenceDataDeletionRequest request, String check)
            throws Exception {
        return callDeleteReferenceDataAndGetCheckToken(th.personSuperAdmin, request, check, status().isOk());
    }

    public String callDeleteReferenceDataAndGetCheckToken(Person caller, ClientReferenceDataDeletionRequest request,
            String checkCode, ResultMatcher resultMatcher) throws Exception {

        MvcResult result = mockMvc.perform(delete("/administration/referenceData/delete")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(request))
                        .param("checkCode", checkCode))
                .andExpect(resultMatcher)
                .andReturn();

        String resultString = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        log.info("Result:\n{}", resultString);
        assertTrue(StringUtils.isNotEmpty(resultString));

        String[] resultTokens = StringUtils.split(resultString, " ");
        return resultTokens[resultTokens.length - 1];
    }

    private void assertRequiresAdmin(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertOAuth2(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
