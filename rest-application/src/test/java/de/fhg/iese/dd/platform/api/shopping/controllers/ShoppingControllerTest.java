/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Steffen Hupp, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.ShoppingTestHelper;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

public class ShoppingControllerTest extends BaseServiceTest {

    @Autowired
    private ShoppingTestHelper th;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        th.createAppEntities();
        th.createBestellBarAppAndAppVariants();
        th.createNonBestellBarAppVariant();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getParcelsByShopReferenceIdReturnsAllParcelsCorrectlyWithoutAdditionalParcels() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
            .param("referenceNumber", shopOrderId)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].contentNotes").value(purchaseOrderReadyForTransportRequest.getContentNotes()))
            .andExpect(jsonPath("$.[0].packagingType").value(purchaseOrderReadyForTransportRequest.getPackagingType().toString()))
            .andExpect(jsonPath("$.[0].size.weight").value(purchaseOrderReadyForTransportRequest.getSize().getWeight()))
            .andExpect(jsonPath("$.[0].deliveryId").exists())
            .andExpect(jsonPath("$.[0].trackingCodeLabelURL").exists());
    }

    @Test
    public void getParcelsByShopReferenceIdReturnsAllParcelsCorrectlyWithOneAdditionalParcel() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = sendPurchaseOrderReadyForTransportRequest(shopOrderId, 1);

        waitForEventProcessing();

        MvcResult response = mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
            .param("referenceNumber", shopOrderId)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andReturn();

        List<ClientPurchaseOrderParcel> parcels =
                Arrays.asList(toObject(response.getResponse(), ClientPurchaseOrderParcel[].class));

        Optional<ClientPurchaseOrderParcel> originalParcel = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(originalParcel.isPresent());
        assertTrue(originalParcel.get().getTrackingCodeLabelURL()!=null
                && originalParcel.get().getDeliveryId() != null
                && originalParcel.get().getContentNotes().equals(purchaseOrderReadyForTransportRequest.getContentNotes())
                && originalParcel.get().getPackagingType() == purchaseOrderReadyForTransportRequest.getPackagingType()
                && originalParcel.get().getSize().equals(purchaseOrderReadyForTransportRequest.getSize()));

        Optional<ClientPurchaseOrderParcel> additionalParcel1 = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(0).getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(additionalParcel1.isPresent());
        assertParcelsMatching(purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(0), additionalParcel1.get());
    }

    @Test
    public void getParcelsByShopReferenceIdReturnsAllParcelsCorrectlyWithSeveralAdditionalParcels() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = sendPurchaseOrderReadyForTransportRequest(shopOrderId, 3);

        waitForEventProcessing();

        MvcResult response = mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
            .param("referenceNumber", shopOrderId)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(4)))
            .andReturn();

        List<ClientPurchaseOrderParcel> parcels =
                Arrays.asList(toObject(response.getResponse(), ClientPurchaseOrderParcel[].class));

        Optional<ClientPurchaseOrderParcel> originalParcel = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(originalParcel.isPresent());
        assertTrue(originalParcel.get().getTrackingCodeLabelURL()!=null
                && originalParcel.get().getDeliveryId() != null
                && originalParcel.get().getContentNotes().equals(purchaseOrderReadyForTransportRequest.getContentNotes())
                && originalParcel.get().getPackagingType() == purchaseOrderReadyForTransportRequest.getPackagingType()
                && originalParcel.get().getSize().equals(purchaseOrderReadyForTransportRequest.getSize()));

        Optional<ClientPurchaseOrderParcel> additionalParcel1 = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(0).getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(additionalParcel1.isPresent());
        assertParcelsMatching(purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(0), additionalParcel1.get());

        Optional<ClientPurchaseOrderParcel> additionalParcel2 = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(1).getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(additionalParcel2.isPresent());
        assertParcelsMatching(purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(1), additionalParcel2.get());

        Optional<ClientPurchaseOrderParcel> additionalParcel3 = parcels.stream()
                .filter(p->purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(2).getContentNotes().equals(p.getContentNotes())).findFirst();
        assertTrue(additionalParcel3.isPresent());
        assertParcelsMatching(purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(2), additionalParcel3.get());
    }

    private void assertParcelsMatching(ClientPurchaseOrderParcel expected, ClientPurchaseOrderParcel present) {
        assertTrue(present.getTrackingCodeLabelURL()!=null
                && present.getDeliveryId() != null
                && present.getContentNotes().equals(expected.getContentNotes())
                && present.getPackagingType() == expected.getPackagingType()
                && present.getSize().equals(expected.getSize()));
    }

    @Test
    public void getParcelsByShopReferenceIdFailsDueToWrongOrMissingApiKey() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, "test")
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void getParcelsByShopReferenceIdIsEmptyDueToNotExistingReferenceId() throws Exception {

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
            .param("referenceNumber", "test")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(0)));
    }

    private ClientPurchaseOrderReceivedEvent createPurchaseOrderWithPersonDeliveryAddress(String shopOrderId) throws Exception {
        // PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                shopOrderId, // String shopOrderId;
                th.tenant1.getId(), // String communityId;
                th.shop1.getId(), // String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(), // String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), // Address pickupAddress;
                th.personVgAdmin.getId(), // String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), // Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(), // String transportNotes;
                Arrays.asList(new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
                // List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
                // TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk()).andReturn();

        return purchaseOrderReceivedEvent;
    }

    private ClientPurchaseOrderReadyForTransportRequest sendPurchaseOrderReadyForTransportRequest(String shopOrderId, int numberAdditionalParcels) throws Exception {
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(4000.0)
                        .width(3000.0)
                        .height(2000.0)
                        .category(DimensionCategory.OVERSIZED)
                        .weight(0.1)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                1, // int numParcels;
                null
        );

        if(numberAdditionalParcels > 0) {
            List<ClientPurchaseOrderParcel> additionalParcels = new ArrayList<>();

            for(int i = 0; i < numberAdditionalParcels; i++) {
                additionalParcels.add(ClientPurchaseOrderParcel.builder()
                    .contentNotes("contentNote"+i)
                    .packagingType(PackagingType.PARCEL)
                    .size(ClientDimension.builder()
                        .height((i+1)*1000)
                        .width((i+2)*1000)
                        .length((i+3)*1000)
                        .weight((i+4)*1000)
                        .category(DimensionCategory.OVERSIZED)
                        .build())
                    .build());
            }

            purchaseOrderReadyForTransportRequest.setAdditionalParcels(additionalParcels);
        }

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
            .andExpect(status().isOk());

        return purchaseOrderReadyForTransportRequest;
    }

    @Test
    public void accessIsGrantedForAllApiKeysAndAppVariants() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].contentNotes").value(purchaseOrderReadyForTransportRequest.getContentNotes()))
                .andExpect(jsonPath("$.[0].packagingType").value(purchaseOrderReadyForTransportRequest.getPackagingType().toString()))
                .andExpect(jsonPath("$.[0].size.weight").value(purchaseOrderReadyForTransportRequest.getSize().getWeight()))
                .andExpect(jsonPath("$.[0].deliveryId").exists())
                .andExpect(jsonPath("$.[0].trackingCodeLabelURL").exists());

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar2.getApiKey1())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].contentNotes").value(purchaseOrderReadyForTransportRequest.getContentNotes()))
                .andExpect(jsonPath("$.[0].packagingType").value(purchaseOrderReadyForTransportRequest.getPackagingType().toString()))
                .andExpect(jsonPath("$.[0].size.weight").value(purchaseOrderReadyForTransportRequest.getSize().getWeight()))
                .andExpect(jsonPath("$.[0].deliveryId").exists())
                .andExpect(jsonPath("$.[0].trackingCodeLabelURL").exists());
    }

    @Test
    public void accessIsNotGrantedForSharedApiKeys() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey2())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isForbidden());

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.appVariantBestellBar2.getApiKey2())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void accessIsUnauthorizedForMissingApiKey() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void accessIsForbiddenForInvalidApiKey() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, "notAnApiKey")
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void accessIsForbiddenForApiKeyOfNonBestellBarAppVariant() throws Exception {

        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);
        sendPurchaseOrderReadyForTransportRequest(shopOrderId, 0);

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.nonBestellBarAppVariantWithApiKeys.getApiKey1())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isForbidden());

        mockMvc.perform(get("/shopping/apiKey/parcels")
                .header(HEADER_NAME_API_KEY, th.nonBestellBarAppVariantWithApiKeys.getApiKey2())
                .param("referenceNumber", shopOrderId)
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

}
