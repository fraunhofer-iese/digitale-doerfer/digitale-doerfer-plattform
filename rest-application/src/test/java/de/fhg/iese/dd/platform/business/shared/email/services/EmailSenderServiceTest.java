/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.email.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

@ActiveProfiles({"test", "EmailSenderServiceTest"})
public class EmailSenderServiceTest extends BaseServiceTest {

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private TestFileStorage testFileStorage;

    @Override
    public void createEntities() throws Exception {

    }

    @Override
    public void tearDown() throws Exception {

    }

    @Test
    public void testFileStorageEmailEnabled() throws Exception {
        String testFromEmailAddress = "info@digitale-doerfer.de";
        String testRecipientName = "lovely";
        String testRecipientEmailAddress = "love@digtale-doerfer.de";
        String testSubject = "Liebes Digitale Dörfer-Team! ❤️";
        String testText = "Ich bin so glücklich, durch die KuppelBar habe ich endlich meinen Schatz gefunden! 👨‍❤️‍👨";

        timeServiceMock.setConstantDateTime(12021983);

        emailSenderService.sendEmail(testFromEmailAddress, testRecipientName, testRecipientEmailAddress, testSubject,
                testText, Collections.emptyList(), false);

        List<String> storedMails = testFileStorage.findAll(config.getEmail().getEmailStoragePath());

        assertThat(storedMails).hasSize(1);
        String fileName = storedMails.get(0);
        assertThat(fileName)
                .startsWith(config.getEmail().getEmailStoragePath() + "love_digtale-doerfer.de_12021983")
                .endsWith(".txt");

        testFileStorage.assertFileExistsWithContent(fileName, testText.getBytes(StandardCharsets.UTF_8));
    }

}
