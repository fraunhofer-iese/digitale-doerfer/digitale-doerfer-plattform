/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransportAlternative;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class DeliveryOverPoolingStationToPoolingStationTest extends BaseLogisticsEventTest {

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    // Sequence of:
    // Purchase Order Received
    // Purchase Order Ready For Transport
    // Transport Alternative Selected
    // Delivery to manual pooling station
    // Transport Alternative Selected
    // Carrier pickup from manual pooling station
    // Delivery to manual pooling station
    // Receiver pickup from manual pooling station
    @Test
    public void deliveryOverPoolingStationToPoolingStation() throws Exception {

        intermediatePoolingStation = th.poolingStation4;
        deliveryPoolingStation = th.poolingStation2;

        intermediatePoolingStationAddress = intermediatePoolingStation.getAddress();
        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        pickupAddress = ClientAddressDefinition.builder()
                .name("Gartenschau Kaiserslautern")
                .street("Lauterstraße 51")
                .zip("67659")
                .city("Kaiserslautern")
                .build();

        intermediatePoolingStationBox = th.poolingStationBox6;

        // first unused poolingStationBox is selected, therefore we can be sure that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox3;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);
        TransportAlternative transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStationViaIntermediatePoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment = transportAlternativeSelectRequestFirstStep(transportAlternativeToIntermediatePS.getId(), carrierFirstStep,
                intermediatePoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrierFirstStep, //carrier
            transportAssignment.getId(), //transportAssignmentId
            intermediatePoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(intermediatePoolingStation.getId(),
                delivery.getTrackingCode(), transportAssignment.getId(), carrierFirstStep,
                intermediatePoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        ClientTransportAlternative transportAlternativeToDeliveryPS = transportDeliveredPoolingStationRequestIntermediate(intermediatePoolingStation.getId(), delivery.getTrackingCode(), transportAssignment.getId(),
                carrierFirstStep, deliveryPoolingStation.getId(), delivery.getId());
        // TransportAlternativeSelectRequest
        // Attention new transportAssignment
        transportAssignment = transportAlternativeSelectRequestLastStep(transportAlternativeToDeliveryPS.getId(),
                carrierLastStep, deliveryPoolingStationAddress);

        // TransportPickupPoolingStationBoxReadyForDeallocationRequest
        transportPickupPoolingStationBoxReadyForDeallocationRequest(delivery.getId(), transportAssignment.getId(),
                intermediatePoolingStation, intermediatePoolingStationBox, carrierLastStep);

        // TransportPickupPoolingStationRequest
        transportPickupPoolingStationRequest(
                transportAssignment.getId(),
                delivery.getTrackingCode());

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(
            deliveryPoolingStation.getId(),
            delivery.getTrackingCode(),
            transportAssignment.getId(),
            carrierLastStep,
            deliveryPoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        transportDeliveredPoolingStationRequestFinal(
            deliveryPoolingStation, // poolingStation
            delivery.getTrackingCode(),// deliveryTrackingCode
            transportAssignment.getId(),// transportAssignmentId
            delivery.getId(),// deliveryId
            carrierLastStep,// carrier
            intermediatePoolingStation.getAddress());// pickupAddressTransport

        // ReceiverPickupPoolingStationBoxReadyForDeallocationRequest
        receiverPickupPoolingStationBoxReadyForDeallocationRequest(
            delivery.getId(),
            deliveryPoolingStation.getId(),
            deliveryPoolingStationBox);

        // ReceiverPickupReceivedPoolingStationRequest
        receiverPickupReceivedPoolingStationRequest(
            deliveryPoolingStation.getId(),
            delivery.getTrackingCode(),
            delivery.getId(),
            deliveryPoolingStationAddress);

    }

}
