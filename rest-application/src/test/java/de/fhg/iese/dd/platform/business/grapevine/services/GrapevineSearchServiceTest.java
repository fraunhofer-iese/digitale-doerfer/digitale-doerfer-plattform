/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
public class GrapevineSearchServiceTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private GrapevineSearchActivatedService grapevineSearchService;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws InterruptedException {

        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    @Test
    public void saveAndSearchPost() throws Exception {

        final Gossip post = th.gossip2WithComments;
        String searchTerm = "Restaurant";
        final Person caller = post.getCreator();
        final Person caller2 = th.personNinaBauer;
        assertThat(post.getText()).contains(searchTerm);
        assertThat(caller).isNotEqualTo(caller2);

        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //creator always gets own posts
        Page<Post> foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(post)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        //creator always gets own posts, even if the geo areas are not selected
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(th.geoAreaDeutschland.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        //not matching geo areas and not creator
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller2.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(th.geoAreaDeutschland.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);

        post.setDeleted(true);
        th.postRepository.save(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);

        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(post)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);
    }

    @Test
    public void saveAndSearchPost_MultiGeoArea() throws Exception {

        final Gossip post = th.gossip2WithComments;
        String searchTerm = "Restaurant";
        final Person caller = post.getCreator();
        final Person caller2 = th.personNinaBauer;
        assertThat(post.getText()).contains(searchTerm);
        assertThat(caller).isNotEqualTo(caller2);

        post.setGeoAreas(Set.of(findFirstGeoArea(post), th.geoAreaDorf2InEisenberg));
        th.postRepository.save(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);

        //creator always gets own posts
        Page<Post> foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Set.copyOf(BaseEntity.getIdsOf(post.getGeoAreas())),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        //creator always gets own posts, even if the geo areas are not selected
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Set.of(th.geoAreaDeutschland.getId(), th.geoAreaRheinlandPfalz.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        //not matching geo areas and not creator
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller2.getId(),
                caller2.getHomeArea().getId(),
                Set.of(th.geoAreaDeutschland.getId(), th.geoAreaMainz.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);

        //not creator and matching geo areas
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller2.getId(),
                caller2.getHomeArea().getId(),
                Set.of(th.geoAreaDeutschland.getId(), th.geoAreaDorf2InEisenberg.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        //hidden for other home areas and not matching home area
        post.setHiddenForOtherHomeAreas(true);
        th.postRepository.save(post);
        assertThat(post.getGeoAreas()).doesNotContain(caller2.getHomeArea());
        grapevineSearchService.indexOrUpdateIndexedPost(post);
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller2.getId(),
                caller2.getHomeArea().getId(),
                Set.of(th.geoAreaDeutschland.getId(), th.geoAreaDorf2InEisenberg.getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);

        //hidden for other home areas and matching home area
        caller2.setHomeArea(findFirstGeoArea(post));
        th.personRepository.save(caller2);
        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller2.getId(),
                caller2.getHomeArea().getId(),
                Set.of(th.geoAreaDeutschland.getId(), caller2.getHomeArea().getId()),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);

        post.setHiddenForOtherHomeAreas(false);
        post.setDeleted(true);
        th.postRepository.save(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);

        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(post)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);
    }

    @Test
    public void saveAndSearchComment() throws Exception {

        final Comment comment = th.offer4Comment1;
        final Post post = comment.getPost();
        String searchTerm = "Woche";
        final Person caller = comment.getCreator();
        assertThat(comment.getText()).contains(searchTerm);
        assertThat(post.getText()).doesNotContain(searchTerm);

        grapevineSearchService.indexOrUpdateIndexedComment(comment);

        Page<Post> foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(post)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(post);

        comment.setDeleted(true);
        th.commentRepository.save(comment);

        grapevineSearchService.indexOrUpdateIndexedComment(comment);

        foundPosts = grapevineSearchService.simpleSearch(
                searchTerm,
                caller.getId(),
                caller.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(post)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);
    }

    @Test
    public void deleteOldEntries() throws Exception {

        //this is to test if the service can deal with an existing index
        grapevineSearchService.setIndexChecked(false);

        final Gossip postToBeDeleted = th.gossip3WithComment;
        postToBeDeleted.setLastModified(
                timeServiceMock.currentTimeMillisUTC() - grapevineConfig.getSearchablePostTime().toMillis() - 1);
        postToBeDeleted.setLastActivity(
                timeServiceMock.currentTimeMillisUTC() - grapevineConfig.getSearchablePostTime().toMillis() - 1);
        th.postRepository.save(postToBeDeleted);
        String searchTermToBeDeleted = "Schule";

        final Gossip postToBeKept = th.gossip5;
        String searchTermToBeKept = "Dreirad";
        final Person callerPostToBeDeleted = postToBeDeleted.getCreator();
        final Person callerPostToBeKept = postToBeKept.getCreator();

        assertThat(postToBeKept.getText()).contains(searchTermToBeKept);
        assertThat(postToBeDeleted.getText()).contains(searchTermToBeDeleted);

        grapevineSearchService.indexOrUpdateIndexedPost(postToBeKept);
        grapevineSearchService.indexOrUpdateIndexedPost(postToBeDeleted);

        Page<Post> foundPosts = grapevineSearchService.simpleSearch(
                searchTermToBeKept,
                callerPostToBeKept.getId(),
                callerPostToBeKept.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(postToBeKept)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(postToBeKept);

        foundPosts = grapevineSearchService.simpleSearch(
                searchTermToBeDeleted,
                callerPostToBeDeleted.getId(),
                callerPostToBeDeleted.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(postToBeDeleted)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(postToBeDeleted);

        grapevineSearchService.deleteOldEntries();

        foundPosts = grapevineSearchService.simpleSearch(
                searchTermToBeDeleted,
                callerPostToBeDeleted.getId(),
                callerPostToBeDeleted.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(postToBeDeleted)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(0);

        foundPosts = grapevineSearchService.simpleSearch(
                searchTermToBeKept,
                callerPostToBeKept.getId(),
                callerPostToBeKept.getHomeArea().getId(),
                Collections.singleton(getFirstGeoAreaId(postToBeDeleted)),
                Collections.emptySet(),
                Pageable.unpaged());
        assertThat(foundPosts.getTotalElements()).isEqualTo(1);
        assertThat(foundPosts.getContent().get(0)).isEqualTo(postToBeKept);
    }

    private static GeoArea findFirstGeoArea(Post post) {

        return post.getGeoAreas().stream()
                .findFirst()
                .get();
    }

    private static String getFirstGeoAreaId(Post post) {

        return findFirstGeoArea(post).getId();
    }

}
