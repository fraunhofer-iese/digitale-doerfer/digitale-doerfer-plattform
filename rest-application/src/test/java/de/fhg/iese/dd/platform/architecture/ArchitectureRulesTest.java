/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.architecture;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.base.HasDescription;
import com.tngtech.archunit.core.domain.*;
import com.tngtech.archunit.core.domain.properties.CanBeAnnotated;
import com.tngtech.archunit.core.domain.properties.HasName;
import com.tngtech.archunit.core.domain.properties.HasSourceCodeLocation;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.shared.security.services.Action;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnum;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.repos.BaseCustomSQLQueryRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.repos.CustomRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.Feature;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

import static com.tngtech.archunit.base.DescribedPredicate.not;
import static com.tngtech.archunit.lang.conditions.ArchConditions.beAnnotatedWith;
import static com.tngtech.archunit.lang.conditions.ArchConditions.dependOnClassesThat;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;
import static com.tngtech.archunit.library.GeneralCodingRules.USE_JAVA_UTIL_LOGGING;
import static com.tngtech.archunit.library.GeneralCodingRules.USE_JODATIME;
import static org.assertj.core.api.Assertions.assertThat;

@AnalyzeClasses(packages = "de.fhg.iese.dd.platform",
        importOptions = {
                ImportOption.DoNotIncludeTests.class,
                ImportOption.DoNotIncludeJars.class,
                ImportOption.DoNotIncludeArchives.class
        })
@Log4j2
public class ArchitectureRulesTest {

    private static class Trace {

        private final StringBuilder content = new StringBuilder("--check trace--\n");
        private int indent = 0;

        public Trace append(Object obj) {
            return append(String.valueOf(obj));
        }

        public Trace append(String string) {
            content.append(string);
            return this;
        }

        public Trace newLine() {
            content.append('\n').append(StringUtils.repeat("  ", indent));
            return this;
        }

        public Trace indent() {
            indent++;
            return this;
        }

        @SuppressWarnings("UnusedReturnValue")
        public Trace outdent() {
            indent = Math.max(0, indent - 1);
            return this;
        }

        public String toString() {
            return StringUtils.appendIfMissing(content.toString(), "\n");
        }

    }

    @ArchTest
    public void dataPackageDependencyTest(JavaClasses importedClasses) {

        noClasses()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.datamanagement..")
                .should()
                .dependOnClassesThat()
                .resideInAPackage("de.fhg.iese.dd.platform.api..")
                .andShould()
                .dependOnClassesThat()
                .resideInAPackage("de.fhg.iese.dd.platform.business..")
                .check(importedClasses);
    }

    @ArchTest
    public void businessPackageDependencyTest(JavaClasses importedClasses) {

        noClasses()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.business..")
                .should()
                .dependOnClassesThat()
                .resideInAPackage("de.fhg.iese.dd.platform.api..")
                .check(importedClasses);
    }

    @ArchTest
    public void servicesTest(JavaClasses importedClasses) {

        classes()
                .that()
                .haveSimpleNameEndingWith("Service")
                .and()
                .haveSimpleNameNotStartingWith("Base")
                .and()
                .areNotInterfaces()
                .should()
                .beAnnotatedWith(Service.class)
                .andShould()
                .implement(interfaceNameStartsWithI())
                .andShould()
                .bePackagePrivate()
                .andShould(serviceInterfaceNameMatchesServiceName())
                .check(importedClasses);
    }

    @ArchTest
    public void interfaceNamingRulesTest(JavaClasses importedClasses) {

        classes()
                .that()
                .haveSimpleNameStartingWith("I")
                .and(secondCharOfNameIsUpperCase())
                .should()
                .beInterfaces()
                .check(importedClasses);
    }

    @ArchTest
    public void entitiesTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAnnotatedWith(Entity.class)
                .or()
                .areAnnotatedWith(Embeddable.class)
                .should()
                .resideInAPackage("de.fhg.iese.dd.platform.datamanagement..model")
                .check(importedClasses);

        classes()
                .that()
                .areAnnotatedWith(Entity.class)
                .and()
                .areNotAnnotatedWith(IgnoreArchitectureViolation.class)
                .should()
                .beAssignableTo(BaseEntity.class)
                .check(importedClasses);

        DescribedPredicate<JavaAnnotation<?>> columnAnnotationWithUnique =
                new DescribedPredicate<JavaAnnotation<?>>("@Column annotation with unique=true") {
                    @Override
                    public boolean test(JavaAnnotation<?> input) {
                        if (input.getRawType().isEquivalentTo(Column.class)) {
                            return input.get("unique").map(Boolean.TRUE::equals).orElse(false);
                        }
                        return false;
                    }
                };

        fields()
                .that()
                .areAnnotatedWith(Column.class)
                .should()
                .notBeAnnotatedWith(columnAnnotationWithUnique)
                .as("Use @Table and @Index instead on the entity itself. " +
                        "The reason is that otherwise the schema generation gets inconsistent since the generated names do not match.")
                .check(importedClasses);

        Set<String> SqlKeywords = new HashSet<>(Arrays.asList(
                "all", "and", "any", "array", "as", "asymmetric", "authorization", "between", "both", "case", "cast",
                "check", "constraint", "cross", "current_catalog", "current_date", "current_path", "current_role",
                "current_schema", "current_time", "current_timestamp", "current_user", "day", "default", "distinct",
                "else", "end", "except", "exists", "false", "fetch", "for", "foreign", "from", "full", "group",
                "groups", "having", "hour", "if", "ilike", "in", "inner", "intersect", "interval", "is", "join", "key",
                "leading", "left", "like", "limit", "localtime", "localtimestamp", "minus", "minute", "month",
                "natural", "not", "null", "offset", "on", "or", "order", "over", "partition", "primary", "qualify",
                "range", "regexp", "right", "row", "rownum", "rows", "second", "select", "session_user", "set", "some",
                "symmetric", "system_user", "table", "to", "top", "cs", "trailing", "true", "uescape", "union",
                "unique", "unknown", "user", "using", "value", "values", "when", "where", "window", "with", "year",
                "rowid"));
        ArchCondition<JavaField> validColumnIdentifier =
                new ArchCondition<JavaField>("Use valid column name") {
                    @Override
                    public void check(JavaField item, ConditionEvents events) {

                        String resultingColumName =
                                Arrays.stream(StringUtils.splitByCharacterTypeCamelCase(item.getName()))
                                        .map(StringUtils::lowerCase)
                                        .collect(Collectors.joining("_"));
                        if (SqlKeywords.contains(resultingColumName)) {
                            events.add(SimpleConditionEvent.violated(item.getOwner(),
                                    item.getDescription() + item.getSourceCodeLocation() + "\n" +
                                            "Column name '" + resultingColumName +
                                            "' of is in the list of reserved SQL keywords"));
                        }
                    }
                };

        fields()
                .that()
                .areAnnotatedWith(Column.class)
                .should(validColumnIdentifier)
                .check(importedClasses);
    }

    @ArchTest
    public void eventsTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAssignableTo(BaseEvent.class)
                .and()
                //this is necessary since there is a "fake" event that is required for getting an auth token
                .areNotNestedClasses()
                .should()
                .resideInAPackage("de.fhg.iese.dd.platform.business..events..")
                .check(importedClasses);
    }

    @ArchTest
    public void repositoriesTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAssignableTo(JpaRepository.class)
                .should()
                .beInterfaces()
                .andShould()
                .haveSimpleNameEndingWith("Repository")
                .andShould()
                .resideInAPackage("de.fhg.iese.dd.platform.datamanagement..repos")
                .check(importedClasses);

        classes()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.datamanagement..repos")
                .and()
                //that is a very special kind of construct with external SQL files
                .areNotAssignableTo(BaseCustomSQLQueryRepository.class)
                .and()
                //these are "fake" repositories that get implemented by the BaseCustomSQLQueryRepository above
                .areNotAssignableTo(CustomRepository.class)
                .should()
                .beAssignableTo(JpaRepository.class)
                .check(importedClasses);

        final Map<String, JavaClass> expectedRepoNameAndEntity = importedClasses.stream()
                .filter(c -> c.isAnnotatedWith(Entity.class))
                .filter(c -> !c.isAnnotatedWith(IgnoreArchitectureViolation.class))
                .collect(Collectors.toMap(
                        c -> c.getPackage().getName().replace(".model", ".repos") + "." + c.getSimpleName() +
                                "Repository",
                        c -> c));

        final Set<String> correctRepos = importedClasses.stream()
                .filter(c -> c.isAssignableTo(JpaRepository.class))
                .filter(r -> expectedRepoNameAndEntity.get(r.getFullName()) != null)
                .filter(r -> isCorrectJpaRepository(r, expectedRepoNameAndEntity.get(r.getFullName())))
                .map(JavaClass::getName)
                .collect(Collectors.toSet());

        final List<String> missingRepos = expectedRepoNameAndEntity.keySet().stream()
                .filter(expectedRepo -> !correctRepos.contains(expectedRepo))
                .sorted()
                .collect(Collectors.toList());

        assertThat(missingRepos).as("All entities need to define a repository, these are missing.").isEmpty();
    }

    private boolean isCorrectJpaRepository(JavaClass repositoryClass, JavaClass entityClass) {

        return anyOfCorrectJpaRepositoryInterface(repositoryClass.getInterfaces(), entityClass);
    }

    private boolean anyOfCorrectJpaRepositoryInterface(Set<JavaType> implementedInterfaces, JavaClass entityClass) {

        for (JavaType implementedInterface : implementedInterfaces) {
            final JavaClass implementedInterfaceErasure = implementedInterface.toErasure();
            if (implementedInterfaceErasure.isAssignableFrom(JpaRepository.class)) {
                if (implementedInterface instanceof JavaParameterizedType) {
                    JavaParameterizedType parameterizedType = (JavaParameterizedType) implementedInterface;
                    final List<JavaType> typeArguments = parameterizedType.getActualTypeArguments();
                    if (typeArguments.size() != 2) {
                        return false;
                    }
                    if (!typeArguments.get(0).toErasure().isAssignableFrom(entityClass.reflect())) {
                        return false;
                    }
                    return typeArguments.get(1).toErasure().isEquivalentTo(String.class);
                }
            } else {
                return anyOfCorrectJpaRepositoryInterface(implementedInterfaceErasure.getInterfaces(), entityClass);
            }
        }
        return false;
    }

    @ArchTest
    public void endpointAuthorizationTest(JavaClasses importedClasses) {

        methods()
                .that()
                .areAnnotatedWith(GetMapping.class)
                .or()
                .areAnnotatedWith(PostMapping.class)
                .or()
                .areAnnotatedWith(PutMapping.class)
                .or()
                .areAnnotatedWith(DeleteMapping.class)
                .should(beAnnotatedWith(ApiAuthentication.class))
                .andShould(haveWellConfiguredAuthentication())
                .check(importedClasses);
    }

    @ArchTest
    public void controllerTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAnnotatedWith(RestController.class)
                .should()
                .haveSimpleNameEndingWith("Controller")
                .andShould()
                .haveSimpleNameNotEndingWith("RestController")
                .andShould()
                .beAssignableTo(BaseController.class)
                .andShould()
                .resideInAPackage("de.fhg.iese.dd.platform.api..controllers")
                .check(importedClasses);

        classes()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.api..controllers")
                .and(not(isAnonymousOrInnerOrNestedClass()))
                .and()
                .doNotHaveFullyQualifiedName("de.fhg.iese.dd.platform.api.framework.controllers.RequestContext")
                .and()
                .doNotHaveFullyQualifiedName("de.fhg.iese.dd.platform.api.framework.controllers.RequestContextValue")
                .and()
                .doNotHaveFullyQualifiedName("de.fhg.iese.dd.platform.api.framework.controllers.CustomErrorController")
                .and()
                .doNotHaveFullyQualifiedName(
                        "de.fhg.iese.dd.platform.api.framework.controllers.CheckSuperAdminActionInterceptor")
                .and()
                .doNotHaveFullyQualifiedName("de.fhg.iese.dd.platform.api.framework.controllers.LogRequestsInterceptor")
                .and()
                .doNotHaveModifier(JavaModifier.ABSTRACT)
                .should()
                .beAnnotatedWith(RestController.class)
                .check(importedClasses);

        methods()
                .that()
                .areAnnotatedWith(GetMapping.class)
                .or()
                .areAnnotatedWith(PostMapping.class)
                .or()
                .areAnnotatedWith(PutMapping.class)
                .or()
                .areAnnotatedWith(DeleteMapping.class)
                .should(useOnlyAllowedApiClassesInSignature(false))
                .check(importedClasses);

        methods()
                .that()
                .areDeclaredInClassesThat().haveSimpleNameEndingWith("EventController")
                .and()
                //we assert that all endpoints have this annotation
                .areAnnotatedWith(ApiAuthentication.class)
                .should()
                .notBeAnnotatedWith(GetMapping.class)
                .andShould()
                .notBeAnnotatedWith(PutMapping.class)
                .andShould()
                .notBeAnnotatedWith(DeleteMapping.class)
                .andShould(useOnlyAllowedApiClassesInSignature(true))
                .check(importedClasses);
    }

    @ArchTest
    public void apiClientClassesTest(JavaClasses importedClasses) {

        classes()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.api..clientmodel")
                .and()
                .areNotAnnotatedWith(Component.class)
                .and()
                .areNotEnums()
                .and()
                .areNotAnonymousClasses()
                .and()
                .areNotInterfaces()
                .and(not(HasName.Predicates.nameMatching(".*Builder")))
                .and(not(HasName.Predicates.nameMatching(".*BuilderImpl")))
                .should(isAllowedApiClass(false))
                .check(importedClasses);

        classes()
                .that()
                .resideInAPackage("de.fhg.iese.dd.platform.api..clientevent")
                .and()
                .areNotAnnotatedWith(Component.class)
                .and()
                .areNotEnums()
                .and()
                .areNotAnonymousClasses()
                .and()
                .areNotAnnotations()
                .and()
                .areNotInterfaces()
                .and(not(HasName.Predicates.nameMatching(".*Builder")))
                .and(not(HasName.Predicates.nameMatching(".*BuilderImpl")))
                .should()
                .beAssignableTo(ClientBaseEvent.class)
                .andShould(isAllowedApiClass(true))
                .check(importedClasses);

        fields()
                .that()
                .areDeclaredInClassesThat(
                        JavaClass.Predicates.resideInAnyPackage("de.fhg.iese.dd.platform.api..clientmodel",
                                        "de.fhg.iese.dd.platform.api..clientevent")
                                .and(not(CanBeAnnotated.Predicates.annotatedWith(Component.class)))
                                .and(not(HasName.Predicates.nameMatching(".*Builder")))
                                .and(not(HasName.Predicates.nameMatching(".*BuilderImpl")))
                )
                .should(useOnlyAllowedApiClassesInField(false))
                .check(importedClasses);
    }

    @ArchTest
    public void eventProcessorTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAnnotatedWith(EventProcessor.class)
                .and()
                .haveSimpleNameNotStartingWith("Sample") // here we intentionally skip Sample event processors
                .should()
                .haveSimpleNameEndingWith("EventProcessor")
                .andShould()
                .beAssignableTo(BaseEventProcessor.class)
                .andShould()
                .resideInAPackage("..processors")
                .check(importedClasses);

        classes()
                .that()
                .haveSimpleNameEndingWith("EventProcessor")
                .and()
                .haveSimpleNameNotStartingWith("Base") // here we intentionally avoid Base classes
                .and()
                .areNotInterfaces()
                .should()
                .beAnnotatedWith(EventProcessor.class)
                .andShould()
                .notBeAnnotatedWith(Component.class)
                .andShould()
                .notBeAnnotatedWith(Service.class)
                .andShould()
                .notBeAnnotatedWith(Controller.class)
                .andShould()
                .notBeAnnotatedWith(RestController.class)
                .check(importedClasses);
    }

    @ArchTest
    public void unwantedLibrariesTest(JavaClasses importedClasses) {

        noClasses()
                .should(USE_JAVA_UTIL_LOGGING)
                .orShould(USE_JODATIME)
                .orShould(dependOnClassesThat(isStringUtilButNotApache()))
                .check(importedClasses);
    }

    @ArchTest
    public void actionsTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAssignableTo(Action.class)
                .and(not(isAbstractClass()))
                .should()
                .resideInAPackage("de.fhg.iese.dd.platform.business.(**).security")
                .check(importedClasses);
    }

    @ArchTest
    public void featureTest(JavaClasses importedClasses) {

        classes()
                .that()
                .areAssignableTo(BaseFeature.class)
                .and(not(isAbstractClass()))
                .should()
                .resideInAPackage("de.fhg.iese.dd.platform.datamanagement.(**).feature")
                .andShould(beAnnotatedWith(Feature.class))
                .check(importedClasses);
    }

    @ArchTest
    public void storableEnumTest(JavaClasses importedClasses) {

        classes()
                .that()
                .implement(StorableEnum.class)
                .should()
                .beEnums()
                .andShould(storableEnumDoesDefinesValidBitMaskValues())
                .check(importedClasses);
    }

    private static DescribedPredicate<JavaClass> isStringUtilButNotApache() {
        return new DescribedPredicate<JavaClass>("invalid String Utils") {
            @Override
            public boolean test(JavaClass input) {
                if ("StringUtil".equals(input.getSimpleName()) || "StringUtils".equals(input.getSimpleName())) {
                    return !"org.apache.commons.lang3.StringUtils".equals(input.getFullName());
                }
                return false;
            }
        };
    }

    private static ArchCondition<JavaClass> isAllowedApiClass(boolean eventApi) {

        return new ArchCondition<JavaClass>("is allowed api class") {

            @Override
            public void check(JavaClass item, ConditionEvents events) {
                Class<?> clazz = item.reflect();
                checkAllowedApiClass(eventApi, item, clazz, clazz, events, new Trace());
            }

        };
    }

    private static ArchCondition<JavaField> useOnlyAllowedApiClassesInField(boolean eventApi) {

        return new ArchCondition<JavaField>("use only client entities in fields") {

            @Override
            public void check(JavaField item, ConditionEvents events) {
                JavaClass owner = item.getOwner();
                if (owner.isEnum() || owner.isAnonymousClass()) {
                    return;
                }
                Field field = item.reflect();
                IgnoreArchitectureViolation fieldAnnotation = field.getAnnotation(IgnoreArchitectureViolation.class);
                if (fieldAnnotation != null &&
                        fieldAnnotation.value() == IgnoreArchitectureViolation.ArchitectureRule.API_MODEL) {
                    return;
                }
                Trace trace = new Trace()
                        .append("field '")
                        .append(field.getName())
                        .append("' of type '")
                        .append(field.getType())
                        .append("'⤵")
                        .indent();
                checkAllowedApiClass(eventApi, item, field.getGenericType(), field.getType(), events,
                        trace);
            }
        };
    }

    private static ArchCondition<JavaMethod> useOnlyAllowedApiClassesInSignature(boolean eventApi) {

        return new ArchCondition<JavaMethod>("use only client entities as return type and parameters") {

            @Override
            public void check(JavaMethod item, ConditionEvents events) {
                Method method = item.reflect();
                Class<?> declaringClass = method.getDeclaringClass();
                Trace trace = new Trace()
                        .append("method return type⤵")
                        .indent();
                if (isIgnorableApiModel(declaringClass, trace)) {
                    return;
                }
                if (isIgnorableApiModel(method, trace)) {
                    return;
                }
                checkAllowedApiClass(eventApi, item, method.getGenericReturnType(), method.getReturnType(), events,
                        trace);
                //one time tokens can be added as Strings
                ApiAuthentication apiAuthentication = method.getAnnotation(ApiAuthentication.class);
                boolean stringParametersAllowed = (apiAuthentication != null &&
                        apiAuthentication.value() == ApiAuthenticationType.ONE_TIME_TOKEN);
                for (Parameter parameter : method.getParameters()) {
                    trace = new Trace()
                            .append("method parameter '")
                            .append(parameter.getName())
                            .append("'⤵")
                            .indent();
                    if (stringParametersAllowed && String.class.equals(parameter.getType())) {
                        trace.newLine().append("one time token ✅").outdent();
                        continue;
                    }
                    if (isIgnorableApiModel(parameter, trace)) {
                        continue;
                    }
                    checkAllowedApiClass(eventApi, item, parameter.getParameterizedType(), parameter.getType(), events,
                            trace);
                }
            }
        };
    }

    private static ArchCondition<JavaMethod> haveWellConfiguredAuthentication() {

        return new ArchCondition<JavaMethod>("have well configured @ApiAuthentication") {

            @Override
            public void check(JavaMethod item, ConditionEvents events) {
                Method method = item.reflect();
                ApiAuthentication apiAuthentication = method.getAnnotation(ApiAuthentication.class);
                EnumSet<ApiAuthenticationType> allowedAuthentications;
                switch (apiAuthentication.appVariantIdentification()) {
                    case NONE:
                        allowedAuthentications =
                                EnumSet.of(ApiAuthenticationType.PUBLIC,
                                        ApiAuthenticationType.OAUTH2,
                                        ApiAuthenticationType.OAUTH2_OPTIONAL,
                                        ApiAuthenticationType.ONE_TIME_TOKEN,
                                        ApiAuthenticationType.API_KEY);
                        break;
                    case IDENTIFICATION_ONLY:
                        allowedAuthentications =
                                EnumSet.of(ApiAuthenticationType.PUBLIC,
                                        ApiAuthenticationType.OAUTH2,
                                        ApiAuthenticationType.OAUTH2_OPTIONAL);
                        break;
                    case AUTHENTICATION:
                        allowedAuthentications =
                                EnumSet.of(ApiAuthenticationType.API_KEY,
                                        ApiAuthenticationType.OAUTH2_AND_API_KEY);
                        break;
                    default:
                        allowedAuthentications = EnumSet.noneOf(ApiAuthenticationType.class);
                }
                if (!allowedAuthentications.contains(apiAuthentication.value())) {
                    events.add(SimpleConditionEvent.violated(item,
                            item.getDescription() + item.getSourceCodeLocation() + "\n" +
                                    "uses invalid authentication configuration for appVariantIdentification: " +
                                    apiAuthentication.appVariantIdentification() + ", allowed are " +
                                    allowedAuthentications));
                }
            }
        };
    }

    private static <J extends HasName.AndFullName & HasDescription & HasSourceCodeLocation>
    void checkAllowedApiClass(boolean eventApi, J javaThing, Type genericType, Class<?> clazz, ConditionEvents events,
            Trace trace) {

        if (genericType instanceof ParameterizedType) {
            for (Type actualTypeArgument : ((ParameterizedType) genericType).getActualTypeArguments()) {
                if (actualTypeArgument instanceof Class<?>) {
                    Class<?> clazzTypeArgument = (Class<?>) actualTypeArgument;
                    trace.newLine()
                            .append("type argument '")
                            .append(clazzTypeArgument)
                            .append("'⤵")
                            .indent();
                    if (eventApi) {
                        checkAllowedRawApiEventClass(clazzTypeArgument, javaThing, events, trace);
                    } else {
                        checkAllowedRawApiClass(clazzTypeArgument, javaThing, events, trace);
                    }
                } else {
                    if (actualTypeArgument instanceof ParameterizedType) {
                        trace.newLine()
                                .append("type argument '")
                                .append(actualTypeArgument)
                                .append("'")
                                .indent();
                        checkAllowedApiClass(eventApi, javaThing, actualTypeArgument, clazz, events, trace);
                    } else {
                        if (actualTypeArgument instanceof WildcardType) {
                            WildcardType wildcardType = (WildcardType) actualTypeArgument;
                            for (Type lowerBound : wildcardType.getLowerBounds()) {
                                trace.newLine()
                                        .append("wildcard type argument lower bound'")
                                        .append(lowerBound)
                                        .append("'⤵")
                                        .indent();
                                checkAllowedApiClass(eventApi, javaThing, lowerBound, (Class<?>) lowerBound, events,
                                        trace);
                            }
                            for (Type upperBound : wildcardType.getUpperBounds()) {
                                trace.newLine()
                                        .append("wildcard type argument upper bound'")
                                        .append(upperBound)
                                        .append("'⤵")
                                        .indent();
                                checkAllowedApiClass(eventApi, javaThing, upperBound, (Class<?>) upperBound, events,
                                        trace);
                            }
                        } else {
                            if (actualTypeArgument instanceof TypeVariable) {
                                TypeVariable<?> typeVariable = (TypeVariable<?>) actualTypeArgument;
                                for (Type bound : typeVariable.getBounds()) {
                                    if (bound instanceof ParameterizedType) {
                                        Type boundRawType = ((ParameterizedType) bound).getRawType();
                                        if (bound.equals(genericType)) {
                                            //a class uses itself as bound, would result in endless recursion
                                            continue;
                                        }
                                        trace.newLine()
                                                .append("type argument '")
                                                .append(boundRawType)
                                                .append("'⤵")
                                                .indent();
                                        checkAllowedApiClass(eventApi, javaThing, bound, (Class<?>) boundRawType,
                                                events, trace);
                                    } else {
                                        trace.newLine()
                                                .append("type argument '")
                                                .append(bound)
                                                .append("'⤵")
                                                .indent();
                                        checkAllowedApiClass(eventApi, javaThing, bound, (Class<?>) bound, events,
                                                trace);
                                    }
                                }
                            } else {
                                throw new IllegalStateException(
                                        "Can not understand inner parameterized type " + actualTypeArgument
                                                + "(" + actualTypeArgument.getClass() + ")" +
                                                " in " +
                                                javaThing +
                                                javaThing.getSourceCodeLocation());
                            }
                        }
                    }
                }
            }
        } else {
            if (eventApi) {
                trace.newLine().append("class '").append(clazz).append("'⤵").indent();
                checkAllowedRawApiEventClass(clazz, javaThing, events, trace);
            } else {
                trace.newLine().append("class '").append(clazz).append("'⤵").indent();
                checkAllowedRawApiClass(clazz, javaThing, events, trace);
            }
        }
    }

    private static <J extends HasName.AndFullName & HasDescription & HasSourceCodeLocation>
    void checkAllowedRawApiClass(Class<?> clazz, J javaThing, ConditionEvents events, Trace trace) {

        if (clazz.isEnum() || clazz.isPrimitive() ||
                String.class.equals(clazz) ||
                Number.class.isAssignableFrom(clazz) ||
                Boolean.class.equals(clazz) ||
                char[].class.equals(clazz)
        ) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (MultipartFile.class.isAssignableFrom(clazz)) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (ClientBaseEntity.class.isAssignableFrom(clazz)) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (ClientBaseEvent.class.isAssignableFrom(clazz)) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (isIgnorableApiModel(clazz, trace)) {
            return;
        }
        if (BaseEntity.class.isAssignableFrom(clazz)) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses '" + clazz + "' which is a subclass of BaseEntity \n" +
                    "    but should use only subclasses of ClientBaseEntity\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        Package clazzPackage = clazz.getPackage();
        if (clazzPackage == null) {
            //this might only happen for inner classes, should be filtered out before
            trace.newLine().append("❌").outdent();
            events.add(SimpleConditionEvent.violated(javaThing,
                    javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                            "  uses (or is) '" + clazz + "' that is not an api class, \n" +
                            "    it should use only classes from 'de.fhg.iese.dd.platform.api.*' or libraries\n" +
                            trace));
            return;
        }
        String packageName = clazzPackage.getName();
        if (!StringUtils.startsWithAny(packageName,
                "java.lang",
                "java.util",
                "de.fhg.iese.dd.platform.api.",
                "org.springframework")) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses (or is) '" + clazz + "' from package '" + packageName + "'\n" +
                    "    but should use only classes from 'de.fhg.iese.dd.platform.api.*' or libraries\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        if (!StringUtils.startsWith(clazz.getSimpleName(), "Client")) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses (or is) '" + clazz +
                    "' that does not start with prefix 'Client' and is not a primitive type\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        //this is a special case where the api class should only have valid fields
        ReflectionUtils.doWithFields(clazz,
                f -> {
                    trace.newLine()
                            .append("field '")
                            .append(f.getName())
                            .append("' of type '")
                            .append(f.getType())
                            .append("'⤵")
                            .indent();
                    if (isIgnorableApiModel(f, trace)) {
                        return;
                    }
                    checkAllowedApiClass(false, javaThing, f.getGenericType(), f.getType(), events, trace);
                });
    }

    private static <J extends HasName.AndFullName & HasDescription & HasSourceCodeLocation>
    void checkAllowedRawApiEventClass(Class<?> clazz, J javaThing, ConditionEvents events, Trace trace) {

        if (clazz.equals(Void.TYPE)) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (ClientBaseEvent.class.isAssignableFrom(clazz)) {
            trace.newLine().append("✅").outdent();
            return;
        }
        if (isIgnorableApiModel(clazz, trace)) {
            return;
        }
        if (clazz.isEnum() || clazz.isPrimitive() ||
                String.class.equals(clazz) ||
                Number.class.isAssignableFrom(clazz) ||
                Boolean.class.equals(clazz)
        ) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses '" + clazz + "' which is a primitive type \n" +
                    "    but should use only subclasses of ClientBaseEvent\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        if (BaseEntity.class.isAssignableFrom(clazz)) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses '" + clazz + "' which is a subclass of BaseEntity \n" +
                    "    but should use only subclasses of ClientBaseEvent\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        if (BaseEvent.class.isAssignableFrom(clazz)) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses '" + clazz + "' which is a subclass of BaseEvent \n" +
                    "    but should use only subclasses of ClientBaseEvent\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        if (ClientBaseEntity.class.isAssignableFrom(clazz)) {
            trace.newLine().append("❌").outdent();
            String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                    "  uses '" + clazz + "' which is a subclass of ClientBaseEntity \n" +
                    "    but should use only subclasses of ClientBaseEvent\n" + trace;
            events.add(SimpleConditionEvent.violated(javaThing, message));
            return;
        }
        trace.newLine().append("❌").outdent();
        String message = javaThing.getDescription() + javaThing.getSourceCodeLocation() + "\n" +
                "  uses (or is) '" + clazz + "'\n" +
                "    but should use only classes from 'de.fhg.iese.dd.platform.api.*' or libraries\n" + trace;
        events.add(SimpleConditionEvent.violated(javaThing, message));
    }

    private static boolean isIgnorableApiModel(AnnotatedElement annotatedElement, Trace trace) {
        IgnoreArchitectureViolation annotation = annotatedElement.getAnnotation(IgnoreArchitectureViolation.class);
        if (annotation != null &&
                annotation.value() == IgnoreArchitectureViolation.ArchitectureRule.API_MODEL) {
            trace.newLine().append("ignored").outdent();
            return true;
        }
        return false;
    }

    /**
     * Allowed combinations:
     * <ul>
     *     <li>IGroupService <- GroupService</li>
     *     <li>IGeoService <- GoogleGeoService</li>
     *     <li>IOauthManagementService <- Auth0OauthManagementService</li>
     * </ul>
     */
    private static ArchCondition<JavaClass> serviceInterfaceNameMatchesServiceName() {

        return new ArchCondition<JavaClass>("implemented service interface has I*Service name") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {
                Set<String> expectedInterfaceNames = new HashSet<>();
                //splits CamelCaseWordsService into Camel Case Words
                String[] nameParts = StringUtils.removeEnd(item.getSimpleName(), "Service")
                        .split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
                //we create the potential service names by adding the split words to "Service"
                //ABCService -> ICService, IBCService, IABCService
                String interfaceNamePart = "";
                for (int i = nameParts.length - 1; i >= 0; i--) {
                    String namePart = nameParts[i];
                    interfaceNamePart = namePart + interfaceNamePart;
                    expectedInterfaceNames.add("I" + interfaceNamePart + "Service");
                }
                //ABCService -> IAService, IABService, IABCService
                interfaceNamePart = "";
                for (String namePart : nameParts) {
                    interfaceNamePart = interfaceNamePart + namePart;
                    expectedInterfaceNames.add("I" + interfaceNamePart + "Service");
                }

                if (item.getAllRawInterfaces().stream()
                        .noneMatch(i -> expectedInterfaceNames.contains(i.getSimpleName()))) {

                    String message =
                            item.getDescription() + " does not implement any of the expected interface names " +
                                    expectedInterfaceNames.stream()
                                            .sorted()
                                            .collect(Collectors.toList()) + " in " + item.getSourceCodeLocation();
                    events.add(SimpleConditionEvent.violated(item, message));
                }
            }
        };
    }

    private static DescribedPredicate<JavaClass> isAnonymousOrInnerOrNestedClass() {
        return new DescribedPredicate<JavaClass>("anonymous/inner/nested classes") {
            @Override
            public boolean test(JavaClass input) {
                return input.isAnonymousClass() || input.isInnerClass() || input.isNestedClass();
            }
        };
    }

    private static ArchCondition<JavaClass> storableEnumDoesDefinesValidBitMaskValues() {
        return new ArchCondition<JavaClass>("define valid bit mask values without duplicates") {
            @Override
            public void check(JavaClass item, ConditionEvents events) {

                @SuppressWarnings("unchecked")
                Class<StorableEnum> storableEnumClass = (Class<StorableEnum>) item.reflect();

                Arrays.stream(storableEnumClass.getEnumConstants())
                        .collect(Collectors.groupingBy(StorableEnum::getBitMaskValue))
                        .values()
                        .stream()
                        .filter(p -> p.size() > 1)
                        .forEach(d -> events.add(SimpleConditionEvent.violated(item,
                                item.getDescription() + " has duplicate enum bit mask values for " + d)));

                Arrays.stream(storableEnumClass.getEnumConstants())
                        .filter(e -> !isValidBitMask(e.getBitMaskValue()))
                        .forEach(e -> events.add(SimpleConditionEvent.violated(item,
                                item.getDescription() + " defines invalid bit mask value '" + e.getBitMaskValue() +
                                        "' for " + e)));
            }
        };
    }

    private static boolean isValidBitMask(int bitMask) {

        byte numBits = 0;
        for (int i = 0; i < 32; i++) {
            //this counts the numbers of set bits
            numBits += ((bitMask & (1 << i)) >> i);
        }
        return numBits == 1;
    }

    private static DescribedPredicate<JavaClass> isAbstractClass() {
        return new DescribedPredicate<JavaClass>("abstract classes") {
            @Override
            public boolean test(JavaClass input) {
                return input.getModifiers().contains(JavaModifier.ABSTRACT);
            }
        };
    }

    private static DescribedPredicate<JavaClass> interfaceNameStartsWithI() {
        return new DescribedPredicate<JavaClass>("Interface starting with I") {
            @Override
            public boolean test(JavaClass javaClass) {
                return StringUtils.startsWith(javaClass.getSimpleName(), "I");
            }
        };
    }

    private static DescribedPredicate<JavaClass> secondCharOfNameIsUpperCase() {
        return new DescribedPredicate<JavaClass>("Second letter should be capital for interfaces") {
            @Override
            public boolean test(JavaClass javaClass) {
                return Character.isUpperCase(javaClass.getSimpleName().toCharArray()[1]);
            }
        };
    }

}
