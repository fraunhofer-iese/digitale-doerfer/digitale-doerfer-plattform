/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientTemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryMediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

public class MediaItemControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestFileStorage fileStorage;
    @Autowired
    private TemporaryMediaItemRepository temporaryMediaItemRepository;
    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    protected TestRestTemplate testRestTemplate;
    @Autowired
    private MediaConfig mediaConfig;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();

        createMediaItems();
    }

    private void createMediaItems() {

        List<MediaItem> mediaItems = new ArrayList<>();
        // media items not owned by user, only app variant
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(), FileOwnership.of(th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(), FileOwnership.of(th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(), FileOwnership.of(th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(), FileOwnership.of(th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(), FileOwnership.of(th.app1Variant1)));
        // media items also owned by user
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(),
                FileOwnership.of(th.personRegularAnna, th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(),
                FileOwnership.of(th.personRegularAnna, th.app1Variant1)));
        mediaItems.add(th.createMediaItem(UUID.randomUUID().toString(),
                FileOwnership.of(th.personRegularKarl, th.app1Variant1)));

        mediaItemRepository.saveAll(mediaItems);
        mediaItemRepository.flush();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void uploadTemporaryMediaItem() throws Exception {

        Person uploader = th.personRegularAnna;

        MockMultipartFile expectedFile = th.createTestImageFile();

        MvcResult result = mockMvc.perform(multipart("/media/temp/")
                        .file(expectedFile)
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isCreated())
                .andReturn();

        ClientTemporaryMediaItem clientTempImage = toObject(result.getResponse(), ClientTemporaryMediaItem.class);

        assertEquals(1, temporaryMediaItemRepository.count());

        TemporaryMediaItem tempImage = temporaryMediaItemRepository.findAll().get(0);

        assertEquals(uploader.getId(), tempImage.getOwner().getId());
        assertEquals(tempImage.getId(), clientTempImage.getId());
        assertEquals(tempImage.getMediaItem().getId(), clientTempImage.getMediaItem().getId());
        assertEquals(tempImage.getMediaItem().getUrlsJson(), clientTempImage.getMediaItem().getUrls());

        Map<String, String> urls = tempImage.getMediaItem().getUrls();

        assertFalse(CollectionUtils.isEmpty(urls));

        for (String url : urls.values()) {
            assertTrue(fileStorage.fileExists(
                    fileStorage.getInternalFileName(url)));
        }
    }

    @Test
    public void uploadTemporaryMediaItem_TooBig() throws Exception {

        Person uploader = th.personRegularKarl;

        ClassPathResource expectedFileTooBig = th.createTestImageTooBigFile();

        assertThat(expectedFileTooBig.contentLength()).isGreaterThan(mediaConfig.getMaxUploadFileSize().toBytes());

        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", expectedFileTooBig);

        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(parameters, authHeadersFor(uploader));
        try {
            ResponseEntity<String> result = testRestTemplate.postForEntity("/media/temp/", request, String.class);
            //when running this test with maven / surefire it will not throw an Exception, but return 400
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } catch (Exception e) {
            //when running this test with JUnit it will throw this Exception
            Assert.isInstanceOf(ResourceAccessException.class, e);
        }

        //this is the most important check
        assertEquals(0, temporaryMediaItemRepository.count());
    }

    @Test
    public void uploadTemporaryMediaItem_InvalidParameters() throws Exception {

        Person uploader = th.personRegularAnna;

        //no image
        mockMvc.perform(multipart("/media/temp/")
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //no valid image
        MockMultipartFile expectedFileInvalid = th.createTestInvalidImageFile();

        mockMvc.perform(multipart("/media/temp/")
                        .file(expectedFileInvalid)
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_UPLOAD_FAILED));
    }

    @Test
    public void uploadTemporaryMediaItem_LimitExceeded() throws Exception {

        Person uploader = th.personRegularKarl;

        int maxNumberTempItems = mediaConfig.getMaxNumberTemporaryItems();

        for (int i = 0; i < maxNumberTempItems - 1; i++) {
            th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());
        }

        assertEquals(maxNumberTempItems - 1, temporaryMediaItemRepository.count());

        MockMultipartFile expectedFileMax = th.createTestImageFile();
        MockMultipartFile expectedFileExceeding = th.createTestImageFile();

        mockMvc.perform(multipart("/media/temp/")
                        .file(expectedFileMax)
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isCreated());

        mockMvc.perform(multipart("/media/temp/")
                        .file(expectedFileExceeding)
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_LIMIT_EXCEEDED));

    }

    @Test
    public void uploadTemporaryMediaItem_Unauthorized() throws Exception {

        MockMultipartFile expectedFile = th.createTestImageFile();

        assertOAuth2(multipart("/media/temp/")
                .file(expectedFile));
    }

    @Test
    public void uploadMediaItemByAppVariant() throws Exception {

        MockMultipartFile expectedFile = th.createTestImageFile();

        MvcResult result = mockMvc.perform(multipart("/media/appVariant/")
                        .file(expectedFile)
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build())))
                .andExpect(status().isCreated())
                .andReturn();

        final ClientMediaItem clientMediaImage = toObject(result.getResponse(), ClientMediaItem.class);
        final MediaItem mediaItem = mediaItemRepository.findByIdFull(clientMediaImage.getId());

        assertThat(mediaItem.getOwner()).isNull();
        assertEquals(th.app1Variant1, mediaItem.getAppVariant());
        assertEquals(th.app1Variant1.getApp(), mediaItem.getApp());
        assertEquals(mediaItem.getId(), clientMediaImage.getId());
        assertEquals(mediaItem.getUrlsJson(), clientMediaImage.getUrls());

        Map<String, String> urls = mediaItem.getUrls();

        assertFalse(CollectionUtils.isEmpty(urls));

        for (String url : urls.values()) {
            assertTrue(fileStorage.fileExists(
                    fileStorage.getInternalFileName(url)));
        }
    }

    @Test
    public void uploadMediaItemByAppVariant_InvalidParameters() throws Exception {

        // no image
        mockMvc.perform(multipart("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build())))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        // no valid image
        MockMultipartFile expectedFileInvalid = th.createTestInvalidImageFile();

        mockMvc.perform(multipart("/media/appVariant")
                        .file(expectedFileInvalid)
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build())))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_UPLOAD_FAILED));
    }

    @Test
    public void uploadMediaItemByAppVariant_Unauthorized() throws Exception {

        final MockMultipartFile expectedFile = th.createTestImageFile();

        mockMvc.perform(multipart("/media/appVariant")
                        .file(expectedFile)
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey("my own api key")
                                .build())))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void deleteTemporaryMediaItem() throws Exception {

        Person uploader = th.personRegularKarl;

        mockMvc.perform(multipart("/media/temp/")
                        .file(th.createTestImageFile())
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isCreated());

        TemporaryMediaItem tempImage = temporaryMediaItemRepository.findAll().get(0);

        mockMvc.perform(delete("/media/temp/" + tempImage.getId())
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isOk());

        assertEquals(0, temporaryMediaItemRepository.count());
        assertFalse(mediaItemRepository.existsById(tempImage.getMediaItem().getId()));

        //we need to wait a bit, since the deletion of the files is asynchronous
        waitForEventProcessing();

        for (String url : tempImage.getMediaItem().getUrls().values()) {
            String internalFileName = fileStorage.getInternalFileName(url);
            assertFalse(fileStorage.fileExists(internalFileName));
        }
    }

    @Test
    public void deleteTemporaryMediaItem_InvalidParameters() throws Exception {
        Person uploader = th.personRegularAnna;

        th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //invalid id
        mockMvc.perform(delete("/media/temp/4711")
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void deleteTemporaryMediaItem_Unauthorized() throws Exception {
        Person uploader = th.personRegularAnna;
        Person intruder = th.personRegularKarl;

        assertNotEquals(uploader, intruder);

        TemporaryMediaItem expectedItem = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //no authentication
        assertOAuth2(delete("/media/temp/" + expectedItem.getId()));

        //wrong authentication for valid id
        mockMvc.perform(delete("/media/temp/" + expectedItem.getId())
                        .headers(authHeadersFor(intruder)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getTemporaryMediaItem() throws Exception {

        Person uploader = th.personRegularKarl;

        TemporaryMediaItem expectedItem = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(get("/media/temp/" + expectedItem.getId())
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedItem.getId()))
                .andExpect(jsonPath("$.expirationTime").value(expectedItem.getExpirationTime()))
                .andExpect(jsonPath("$.mediaItem.id").value(expectedItem.getMediaItem().getId()))
                .andExpect(jsonPath("$.mediaItem.urls").value(expectedItem.getMediaItem().getUrls()));

    }

    @Test
    public void getTemporaryMediaItem_InvalidParameters() throws Exception {
        Person uploader = th.personRegularKarl;

        th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(get("/media/temp/4711")
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getTemporaryMediaItem_Unauthorized() throws Exception {
        Person uploader = th.personRegularAnna;
        Person intruder = th.personRegularKarl;

        assertNotEquals(uploader, intruder);

        TemporaryMediaItem expectedItem = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //no authentication
        assertOAuth2(get("/media/temp/" + expectedItem.getId()));
        //wrong authentication for valid id
        mockMvc.perform(get("/media/temp/" + expectedItem.getId())
                        .headers(authHeadersFor(intruder)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getAllTemporaryMediaItems() throws Exception {

        Person uploader = th.personRegularKarl;

        TemporaryMediaItem expectedItem4 = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), 1);
        TemporaryMediaItem expectedItem3 = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), 2);
        TemporaryMediaItem expectedItem2 = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), 3);
        TemporaryMediaItem expectedItem1 = th.createTemporaryMediaItem(uploader, th.nextTimeStamp(), 4);

        mockMvc.perform(get("/media/temp/")
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(expectedItem1.getId()))
                .andExpect(jsonPath("$[0].expirationTime").value(expectedItem1.getExpirationTime()))
                .andExpect(jsonPath("$[0].mediaItem.id").value(expectedItem1.getMediaItem().getId()))
                .andExpect(jsonPath("$[0].mediaItem.urls").value(expectedItem1.getMediaItem().getUrls()))

                .andExpect(jsonPath("$[1].id").value(expectedItem2.getId()))
                .andExpect(jsonPath("$[1].expirationTime").value(expectedItem2.getExpirationTime()))
                .andExpect(jsonPath("$[1].mediaItem.id").value(expectedItem2.getMediaItem().getId()))
                .andExpect(jsonPath("$[1].mediaItem.urls").value(expectedItem2.getMediaItem().getUrls()))

                .andExpect(jsonPath("$[2].id").value(expectedItem3.getId()))
                .andExpect(jsonPath("$[2].expirationTime").value(expectedItem3.getExpirationTime()))
                .andExpect(jsonPath("$[2].mediaItem.id").value(expectedItem3.getMediaItem().getId()))
                .andExpect(jsonPath("$[2].mediaItem.urls").value(expectedItem3.getMediaItem().getUrls()))

                .andExpect(jsonPath("$[3].id").value(expectedItem4.getId()))
                .andExpect(jsonPath("$[3].expirationTime").value(expectedItem4.getExpirationTime()))
                .andExpect(jsonPath("$[3].mediaItem.id").value(expectedItem4.getMediaItem().getId()))
                .andExpect(jsonPath("$[3].mediaItem.urls").value(expectedItem4.getMediaItem().getUrls()));
    }

    @Test
    public void getAllTemporaryMediaItems_Unauthorized() throws Exception {
        assertOAuth2(get("/media/temp/"));
    }

    @Test
    public void getDefinedSizes() throws Exception {

        mockMvc.perform(get("/media/size"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(7)))
                .andExpect(jsonPath("$[0].name").value("thumbnail"))
                .andExpect(jsonPath("$[0].version").value(2))
                .andExpect(jsonPath("$[0].squared").value(true))
                .andExpect(jsonPath("$[0].maxSize").value(200))
                .andExpect(jsonPath("$[1].name").value("extraSmall"))
                .andExpect(jsonPath("$[1].version").value(2))
                .andExpect(jsonPath("$[1].squared").value(false))
                .andExpect(jsonPath("$[1].maxSize").value(640))
                .andExpect(jsonPath("$[2].name").value("small"))
                .andExpect(jsonPath("$[2].version").value(2))
                .andExpect(jsonPath("$[2].squared").value(false))
                .andExpect(jsonPath("$[2].maxSize").value(800))
                .andExpect(jsonPath("$[3].name").value("medium"))
                .andExpect(jsonPath("$[3].version").value(2))
                .andExpect(jsonPath("$[3].squared").value(false))
                .andExpect(jsonPath("$[3].maxSize").value(1280))
                .andExpect(jsonPath("$[4].name").value("large"))
                .andExpect(jsonPath("$[4].version").value(2))
                .andExpect(jsonPath("$[4].squared").value(false))
                .andExpect(jsonPath("$[4].maxSize").value(1920))
                .andExpect(jsonPath("$[5].name").value("extraLarge"))
                .andExpect(jsonPath("$[5].version").value(2))
                .andExpect(jsonPath("$[5].squared").value(false))
                .andExpect(jsonPath("$[5].maxSize").value(2560))
                .andExpect(jsonPath("$[6].name").value("original"))
                .andExpect(jsonPath("$[6].version").value(2))
                .andExpect(jsonPath("$[6].squared").value(false))
                .andExpect(jsonPath("$[6].maxSize").value(Integer.MAX_VALUE));
    }

    @Test
    public void getAllMediaItemsByAppVariant() throws Exception {

        final AppVariant appVariant = th.app1Variant1;

        final List<MediaItem> expectedMediaItems = mediaItemRepository.findAllFull()
                .stream()
                .filter(mi -> mi.getOwner() == null &&
                        mi.getAppVariant() != null &&
                        mi.getAppVariant().equals(appVariant))
                .sorted(Comparator.comparingLong(MediaItem::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(5, expectedMediaItems.size());

        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(appVariant.getApiKey1())
                                .build()))
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(3)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(3))
                .andExpect(jsonPath("$.totalElements").value(expectedMediaItems.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedMediaItems.get(0).getId()))
                .andExpect(jsonPath("$.content[0].urls").value(expectedMediaItems.get(0).getUrls()))
                .andExpect(jsonPath("$.content[1].id").value(expectedMediaItems.get(1).getId()))
                .andExpect(jsonPath("$.content[1].urls").value(expectedMediaItems.get(1).getUrls()))
                .andExpect(jsonPath("$.content[2].id").value(expectedMediaItems.get(2).getId()))
                .andExpect(jsonPath("$.content[2].urls").value(expectedMediaItems.get(2).getUrls()));

        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(appVariant.getApiKey1())
                                .build()))
                        .param("page", String.valueOf(1))
                        .param("count", String.valueOf(3)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(2))
                .andExpect(jsonPath("$.totalElements").value(expectedMediaItems.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedMediaItems.get(3).getId()))
                .andExpect(jsonPath("$.content[0].urls").value(expectedMediaItems.get(3).getUrls()))
                .andExpect(jsonPath("$.content[1].id").value(expectedMediaItems.get(4).getId()))
                .andExpect(jsonPath("$.content[1].urls").value(expectedMediaItems.get(4).getUrls()));
    }

    @Test
    public void getAllMediaItemsByAppVariant_WithUserOwned() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        int pageSize = 4;

        final List<MediaItem> expectedMediaItems = mediaItemRepository.findAllFull()
                .stream()
                .filter(mi -> mi.getAppVariant() != null &&
                        mi.getAppVariant().equals(appVariant))
                .sorted(Comparator.comparingLong(MediaItem::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(8, expectedMediaItems.size());

        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(appVariant.getApiKey1())
                                .build()))
                        .param("userOwned", String.valueOf(true))
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(pageSize)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(expectedMediaItems.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedMediaItems.get(0).getId()))
                .andExpect(jsonPath("$.content[0].urls").value(expectedMediaItems.get(0).getUrls()))
                .andExpect(jsonPath("$.content[1].id").value(expectedMediaItems.get(1).getId()))
                .andExpect(jsonPath("$.content[1].urls").value(expectedMediaItems.get(1).getUrls()))
                .andExpect(jsonPath("$.content[2].id").value(expectedMediaItems.get(2).getId()))
                .andExpect(jsonPath("$.content[2].urls").value(expectedMediaItems.get(2).getUrls()))
                .andExpect(jsonPath("$.content[3].id").value(expectedMediaItems.get(3).getId()))
                .andExpect(jsonPath("$.content[3].urls").value(expectedMediaItems.get(3).getUrls()));

        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(appVariant.getApiKey1())
                                .build()))
                        .param("userOwned", String.valueOf(true))
                        .param("page", String.valueOf(1))
                        .param("count", String.valueOf(pageSize)))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements").value(pageSize))
                .andExpect(jsonPath("$.totalElements").value(expectedMediaItems.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedMediaItems.get(4).getId()))
                .andExpect(jsonPath("$.content[0].urls").value(expectedMediaItems.get(4).getUrls()))
                .andExpect(jsonPath("$.content[1].id").value(expectedMediaItems.get(5).getId()))
                .andExpect(jsonPath("$.content[1].urls").value(expectedMediaItems.get(5).getUrls()))
                .andExpect(jsonPath("$.content[2].id").value(expectedMediaItems.get(6).getId()))
                .andExpect(jsonPath("$.content[2].urls").value(expectedMediaItems.get(6).getUrls()))
                .andExpect(jsonPath("$.content[3].id").value(expectedMediaItems.get(7).getId()))
                .andExpect(jsonPath("$.content[3].urls").value(expectedMediaItems.get(7).getUrls()));
    }

    @Test
    public void getAllMediaItemsByAppVariant_InvalidParameters() throws Exception {

        // maximum number of media items smaller than 1
        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(0)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // page number smaller than 0
        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .param("userOwned", String.valueOf(false))
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(10)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // maximum number of media items smaller than 1 and page number smaller than 0
        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .param("userOwned", String.valueOf(true))
                        .param("page", String.valueOf(-1))
                        .param("count", String.valueOf(0)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void getAllMediaItemsByAppVariant_Unauthorized() throws Exception {

        mockMvc.perform(get("/media/appVariant")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey("my own api key")
                                .build()))
                        .param("userOwned", String.valueOf(true))
                        .param("page", String.valueOf(0))
                        .param("count", String.valueOf(1)))
                .andExpect(isNotAuthorized());
    }

}
