/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine;

import static de.fhg.iese.dd.platform.api.grapevine.PostOnlyVisibleForHomeGeoAreaTest.ContainsType.containsGossip;
import static de.fhg.iese.dd.platform.api.grapevine.PostOnlyVisibleForHomeGeoAreaTest.ContainsType.doesNotContainGossip;
import static de.fhg.iese.dd.platform.api.grapevine.PostOnlyVisibleForHomeGeoAreaTest.ListType.activityList;
import static de.fhg.iese.dd.platform.api.grapevine.PostOnlyVisibleForHomeGeoAreaTest.ListType.generalPostList;
import static de.fhg.iese.dd.platform.api.grapevine.PostOnlyVisibleForHomeGeoAreaTest.ListType.ownPostList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateRequest;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GossipPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

public class PostOnlyVisibleForHomeGeoAreaTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(GossipPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Plausch\", \"itemName\": \"Plausch\", \"maxNumberCharsPerPost\": 280 }")
                .build()
                .withConstantId());

        createGossip();
    }

    private Person postCreator;
    private Person commentCreatorWithSameHomeArea;
    private Person personWithDifferentHomeArea;
    private String text;
    private String gossipId;
    private AppVariant appVariant;

    private void createGossip() throws Exception {
        postCreator = th.personSusanneChristmann;
        commentCreatorWithSameHomeArea = th.personLaraSchaefer;
        personWithDifferentHomeArea = th.personThomasBecker;
        assertEquals(postCreator.getHomeArea(), commentCreatorWithSameHomeArea.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), personWithDifferentHomeArea.getHomeArea());
        assertNotEquals(postCreator, commentCreatorWithSameHomeArea);
        assertNotEquals(postCreator, personWithDifferentHomeArea);
        assertNotEquals(commentCreatorWithSameHomeArea, personWithDifferentHomeArea);

        appVariant = th.appVariant1Tenant1;
        th.selectGeoAreaForAppVariant(appVariant, postCreator, postCreator.getHomeArea());
        th.selectGeoAreaForAppVariant(appVariant, postCreator, personWithDifferentHomeArea.getHomeArea());
        th.selectGeoAreaForAppVariant(appVariant, commentCreatorWithSameHomeArea, postCreator.getHomeArea());
        th.selectGeoAreaForAppVariant(appVariant, commentCreatorWithSameHomeArea,
                personWithDifferentHomeArea.getHomeArea());
        th.selectGeoAreaForAppVariant(appVariant, personWithDifferentHomeArea, postCreator.getHomeArea());
        th.selectGeoAreaForAppVariant(appVariant, personWithDifferentHomeArea,
                personWithDifferentHomeArea.getHomeArea());

        text = "Das ist nur für meine Homies!";

        ClientGossipCreateRequest gossipCreateRequest = ClientGossipCreateRequest.builder()
                .text(text)
                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                .hiddenForOtherHomeAreas(true)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/gossipCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariant))
                        .contentType(contentType)
                        .content(json(gossipCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        gossipId = gossipCreateConfirmation.getPost().getGossip().getId();

        assertTrue(th.postRepository.existsById(gossipId));

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(gossipId)
                .text("Yeah, yo!")
                .build();

        MvcResult commentCreateRequestResult = mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                        .headers(authHeadersFor(commentCreatorWithSameHomeArea, appVariant))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientCommentCreateConfirmation commentCreateConfirmation = toObject(commentCreateRequestResult.getResponse(),
                ClientCommentCreateConfirmation.class);

        assertTrue(th.commentRepository.existsById(commentCreateConfirmation.getCommentId()));
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void creatorCanAccessGossip() throws Exception {

        mockMvc.perform(get("/grapevine/post/{postId}", gossipId)
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.gossip.hiddenForOtherHomeAreas").value(true));

        assertThat(ownPostList, containsGossip, whenCalledBy(postCreator));
        assertThat(generalPostList, containsGossip, whenCalledBy(postCreator));
    }

    @Test
    public void creatorCanAccessGossipAfterHomeAreaChange() throws Exception {

        postCreator.setHomeArea(th.geoAreaTenant2);
        final Person postCreatorwithChangeHomeArea = th.personRepository.save(postCreator);

        mockMvc.perform(get("/grapevine/post/{postId}", gossipId)
                .headers(authHeadersFor(postCreatorwithChangeHomeArea, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.gossip.hiddenForOtherHomeAreas").value(true));

        assertThat(ownPostList, containsGossip, whenCalledBy(postCreatorwithChangeHomeArea));
        assertThat(generalPostList, containsGossip, whenCalledBy(postCreatorwithChangeHomeArea));
    }

    @Test
    public void otherUserWithSameHomeAreaCanAccessGossip() throws Exception {

        mockMvc.perform(get("/grapevine/post/{postId}", gossipId)
                .headers(authHeadersFor(commentCreatorWithSameHomeArea, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.text").value(text))
                .andExpect(jsonPath("$.gossip.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.gossip.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.gossip.hiddenForOtherHomeAreas").value(true));

        assertThat(generalPostList, containsGossip, whenCalledBy(commentCreatorWithSameHomeArea));
    }

    @Test
    public void otherUserWithDifferentHomeAreaCannotAccessGossip() throws Exception {

        mockMvc.perform(get("/grapevine/post/{postId}", gossipId)
                .headers(authHeadersFor(personWithDifferentHomeArea, appVariant)))
                .andExpect(isException(gossipId, ClientExceptionType.POST_NOT_FOUND));

        assertThat(generalPostList, doesNotContainGossip, whenCalledBy(personWithDifferentHomeArea));
    }

    @Test
    public void otherUserThatHasCommentedGetsPostInActivityList() throws Exception {

        assertThat(activityList, containsGossip, whenCalledBy(commentCreatorWithSameHomeArea));
    }

    @Test
    public void otherUserThatHasCommentedGetsPostInActivityListAfterHomeAreaChange() throws Exception {

        commentCreatorWithSameHomeArea.setHomeArea(th.geoAreaTenant2);
        final Person commentCreatorWithDifferentHomeArea = th.personRepository.save(commentCreatorWithSameHomeArea);

        assertThat(activityList, containsGossip, whenCalledBy(commentCreatorWithDifferentHomeArea));
    }

    @Test
    public void otherUserThatHasCommentedGetsPostNotInGenericListAfterHomeAreaChange() throws Exception {

        //we decided to design this behaviour because otherwise every "list post" operation has to join
        //the post and comment table. The user can still access the post in his/her activity list
        commentCreatorWithSameHomeArea.setHomeArea(th.geoAreaTenant2);
        final Person commentCreatorWithDifferentHomeArea = th.personRepository.save(commentCreatorWithSameHomeArea);

        assertThat(generalPostList, doesNotContainGossip, whenCalledBy(commentCreatorWithDifferentHomeArea));
    }

    @Test
    public void otherUserWithDifferentHomeAreaCannotPostComment() throws Exception {

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(gossipId)
                .text("Stimmt!")
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                        .headers(authHeadersFor(personWithDifferentHomeArea, appVariant))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(isException(gossipId, ClientExceptionType.POST_NOT_FOUND));
    }

    enum ListType {
        ownPostList("post/own"),
        generalPostList("postBySelectedGeoAreas"),
        activityList("activity");

        private final String urlSuffix;

        ListType(String urlSuffix) {
            this.urlSuffix = urlSuffix;
        }

        public String getUrlSuffix() {
            return urlSuffix;
        }
    }

    enum ContainsType {
        containsGossip, doesNotContainGossip
    }

    @FunctionalInterface
    interface CalledBy {
        Person person();
    }

    static CalledBy whenCalledBy(Person person) {
        return () -> person;
    }

    private void assertThat(ListType listType, ContainsType containsType, CalledBy calledBy) throws Exception {

        final boolean containsGossip = containsType == ContainsType.containsGossip;
        final String getSuffix = listType.getUrlSuffix();
        final Person caller = calledBy.person();

        if (listType == ListType.activityList) {
            checkActivityList(containsGossip, getSuffix, caller);
        } else {
            checkGenericAndOwnPostList(containsGossip, getSuffix, caller);
        }
    }

    private void checkActivityList(boolean containsGossip, String getSuffix, Person caller) throws Exception {

        ResultActions resultActions = mockMvc.perform(get("/grapevine/" + getSuffix)
                .headers(authHeadersFor(caller, appVariant)));

        if (containsGossip) {
            expectThatResultContainsGossip(resultActions);
        } else {
            expectThatResultNotContainsGossip(resultActions);
        }
    }

    private void checkGenericAndOwnPostList(boolean containsGossip, String getSuffix, Person caller) throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/grapevine/" + getSuffix)
                .headers(authHeadersFor(caller, appVariant))
                .param("type", "GOSSIP")
                .param("sortBy", "CREATED"));

        if (containsGossip) {
            expectThatResultContainsGossip(resultActions);
        } else {
            expectThatResultNotContainsGossip(resultActions);
        }

        resultActions = mockMvc.perform(get("/grapevine/" + getSuffix)
                .headers(authHeadersFor(caller, appVariant))
                .param("type", "GOSSIP")
                .param("sortBy", "LAST_ACTIVITY"));

        if (containsGossip) {
            expectThatResultContainsGossip(resultActions);
        } else {
            expectThatResultNotContainsGossip(resultActions);
        }

        resultActions = mockMvc.perform(get("/grapevine/" + getSuffix)
                .headers(authHeadersFor(caller, appVariant))
                .param("sortBy", "CREATED"));

        if (containsGossip) {
            expectThatResultContainsGossip(resultActions);
        } else {
            expectThatResultNotContainsGossip(resultActions);
        }

        resultActions = mockMvc.perform(get("/grapevine/" + getSuffix)
                .headers(authHeadersFor(caller, appVariant))
                .param("sortBy", "LAST_ACTIVITY"));

        if (containsGossip) {
            expectThatResultContainsGossip(resultActions);
        } else {
            expectThatResultNotContainsGossip(resultActions);
        }
    }

    private void expectThatResultContainsGossip(ResultActions resultActions) throws Exception {

        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.content[0].gossip.id").value(gossipId));
    }

    private void expectThatResultNotContainsGossip(ResultActions resultActions) throws Exception {

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(0)));
    }

}
