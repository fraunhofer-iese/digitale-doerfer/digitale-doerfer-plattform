/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Jannis von Albedyll
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.featureTestInvalid;

import java.util.List;

import org.springframework.http.HttpMethod;

import de.fhg.iese.dd.platform.api.shared.misc.controllers.SharedAdminControllerTest;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.Feature;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Used in {@link SharedAdminControllerTest#getAllFeatureMetaModelsWithWildcardAndInvalidFeatureClass()}
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PRIVATE)
@SuperBuilder
@NoArgsConstructor
@Feature(featureIdentifier = "de.fhg.iese.dd.platform.test.TestFeatureInvalid")
public class TestFeatureInvalid {

    private String sampleStringValue;

    private int sampleIntValue;

    private boolean sampleBooleanValue;

    private List<String> sampleStringValues;

    private HttpMethod sampleEnum;

}
