/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Tahmid Ekram, Dominik Schnier, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine;

import de.fhg.iese.dd.platform.api.grapevine.clientmodel.*;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItem;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.business.grapevine.init.GrapevineCategoryDataInitializer;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatSubject;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatSubjectRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfNewsConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.motivation.MotivationConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Component
public final class GrapevineTestHelper extends BaseSharedTestHelper {

    @Autowired
    public GrapevineConfig grapevineConfig;

    @Autowired
    public PostRepository postRepository;
    @Autowired
    public NewsSourceRepository newsSourceRepository;
    @Autowired
    public ExternalPostRepository externalPostRepository;
    @Autowired
    public CommentRepository commentRepository;
    @Autowired
    public HappeningRepository happeningRepository;
    @Autowired
    public PostInteractionRepository postInteractionRepository;
    @Autowired
    public LikeCommentRepository likeCommentRepository;
    @Autowired
    public HappeningParticipantRepository happeningParticipantRepository;
    @Autowired
    public PushCategoryRepository pushCategoryRepository;
    @Autowired
    public TradingCategoryRepository tradingCategoryRepository;
    @Autowired
    public SuggestionCategoryRepository suggestionCategoryRepository;
    @Autowired
    public SuggestionStatusRecordRepository suggestionStatusRecordRepository;
    @Autowired
    public SuggestionWorkerMappingRepository suggestionWorkerMappingRepository;
    @Autowired
    public ChatRepository chatRepository;
    @Autowired
    public ChatSubjectRepository chatSubjectRepository;
    @Autowired
    public ChatParticipantRepository chatParticipantRepository;
    @Autowired
    public OrganizationRepository organizationRepository;
    @Autowired
    public OrganizationGeoAreaMappingRepository organizationGeoAreaMappingRepository;
    @Autowired
    public NewsSourceOrganizationMappingRepository newsSourceOrganizationMappingRepository;

    @Autowired
    private INewsSourceService newsSourceService;

    public App appDorffunk;
    public App appDorfNews;
    public OauthClient appVariant1Tenant1OauthClient;
    public AppVariant appVariant1Tenant1;
    public OauthClient appVariant2Tenant1And2OauthClient;
    public AppVariant appVariant2Tenant1And2;
    public OauthClient appVariant3Tenant3OauthClient;
    public AppVariant appVariant3Tenant3;
    public OauthClient appVariant4AllTenantsOauthClient;
    public AppVariant appVariant4AllTenants;
    public OauthClient appVariantWithApiKey1Tenant1OauthClient;
    public AppVariant appVariantWithApiKey1Tenant1;
    public OauthClient appVariantWithApiKey4AllTenantsOauthClient;
    public AppVariant appVariantWithApiKey4AllTenants;
    public AppVariant appVariantWithApiKey4AllTenants2;

    public PushCategory pushCategorySuggestionInternalWorkerChat;
    public PushCategory pushCategoryChatMessageReceived;

    public Gossip gossip1withImages;
    public Gossip gossip2WithComments;
    public Comment gossip2Comment1;
    public Comment gossip2Comment2Deleted;
    public Comment gossip2Comment3;
    public Comment gossip2Comment4withImages;
    public List<Comment> gossip2Comments;

    public Gossip gossip3WithComment;
    public Comment gossip3Comment1withOnlyImages;
    public Gossip gossip4;
    public Gossip gossip5;
    public Gossip gossip6;
    public Gossip gossip7;
    public Gossip gossip8With9Images;
    public Gossip gossip9With10Images;

    public Gossip gossip1WithFixedCreationDate;
    public Gossip gossip2WithFixedCreationDate;
    public Gossip gossip3WithFixedCreationDate;
    public Gossip gossip4WithFixedCreationDate;
    public Gossip gossip5WithFixedCreationDate;
    public Gossip gossip6WithFixedCreationDate;
    public Gossip gossip7WithFixedCreationDate;

    public List<Gossip> gossipsTenant1;
    public List<Gossip> gossipsTenant2;

    public String specialPostTypeFeatureKey = "specialPostType";
    public String specialPostTypeFeatureValue = "dorfhelfer";
    public SpecialPost specialPost1withImages;
    public SpecialPost specialPost2withComments;
    public Comment specialPost2Comment1;
    public Comment specialPost2Comment2Deleted;
    public Comment specialPost2Comment3;
    public Comment specialPost2Comment4withImages;
    public List<Comment> specialPost2Comments;
    public SpecialPost specialPost3;
    public SpecialPost specialPost4withImages;

    public List<SpecialPost> specialPostsTenant1;
    public List<SpecialPost> specialPostsTenant2;

    public NewsSource newsSourceDigitalbach;
    public NewsSource newsSourceAnalogheim;
    public NewsSource newsSourceKatzenbach;
    public NewsSource newsSourceDeleted;

    public NewsItem newsItem1;
    public NewsItem newsItem2;
    public NewsItem newsItem3;
    public NewsItem newsItem4;
    public NewsItem newsItem5;
    public NewsItem newsItem6;
    public NewsItem unpublishedNewsItem1;

    public Comment newsItem2Comment1;
    public Comment newsItem2Comment2;
    public Comment newsItem2Comment3;
    public Comment newsItem3Comment1;

    public Happening happening1;
    public Happening happening2;
    public Happening happening3;
    public Happening happening4;
    public Happening unpublishedHappening1;
    public List<Happening> happeningsTenant1;

    public Comment happening1Comment1;
    public Comment happening1Comment2;
    public Comment happening2Comment1;

    public PostInteraction gossip1Post1Interaction;
    public PostInteraction gossip1Post2Interaction;
    public PostInteraction gossip2Post1Interaction;

    public LikeComment gossip2Comment1LikeComment1;
    public LikeComment gossip2Comment1LikeComment2;
    public LikeComment gossip2Comment3LikeComment1;
    public LikeComment gossip2Comment3LikeComment2;

    public List<NewsItem> newsItemsTenant1;

    public static final int GEO_AREA_TENANT_1_POST_COUNT = 15;
    public static final int SUB_GEO_AREA_TENANT_1_POST_COUNT = 17;

    public Person personThomasBecker;
    public Person personSebastianBauer;
    public Person personSusanneChristmann;
    public Person personLaraSchaefer;
    public Person personNinaBauer;
    public Person personAnnikaSchneider;
    public Person personMonikaHess;
    public Person personFriedaFischer;
    public Person personSuggestionWorkerTenant1Horst;
    public Person personSuggestionWorkerTenant1Dieter;
    public Person personSuggestionWorkerTenant2Heinz;
    public Person personSuggestionFirstResponderTenant1Hilde;
    public Person personSuggestionWorkerAndFirstResponderTenant1Carmen;
    public List<Person> personsWithSuggestionRoleTenant1;

    private static final GPSLocation gpsLocationPizzaNapoli = new GPSLocation(49.5576001, 8.0733356);
    private static final GPSLocation gpsLocationPizzaNapoliCustom = new GPSLocation(49.5576002, 8.0733355);
    private static final GPSLocation gpsLocationRestauranteDaNicola = new GPSLocation(49.5587171, 8.0723271);
    private static final GPSLocation gpsLocationPestalozziSchule = new GPSLocation(49.5606881, 8.0695051);

    public static final ClientLocationDefinition locationDefinitionDaNicola = ClientLocationDefinition.builder()
            .locationName("Restaurant Da Nicola")
            .locationLookupString("Restaurant Da Nicola Eisenberg")
            .addressStreet("Hauptstraße 111")
            .addressZip("67304")
            .addressCity("Eisenberg (Pfalz)")
            .gpsLocation(new ClientGPSLocation(49.55851999999999, 8.07485))
            .build();

    private static final GPSLocation playgroundLocation = new GPSLocation(50.7866827, 7.8737623);
    private static final GPSLocation trashLocation = new GPSLocation(50.7667246, 7.8525028);

    public Suggestion suggestionIdea1;
    public SuggestionStatusRecord suggestionIdea1StatusRecord1;
    public SuggestionStatusRecord suggestionIdea1StatusRecord2;
    public SuggestionWorkerMapping suggestionIdea1WorkerMappingHilde;
    public SuggestionWorkerMapping suggestionIdea1WorkerMappingHorst;
    public SuggestionWorkerMapping suggestionIdea1WorkerMappingInactive;
    public Suggestion suggestionIdea2;
    public Suggestion suggestionIdea3;
    public SuggestionWorkerMapping suggestionIdea3WorkerMappingCarmen;
    public Suggestion suggestionDefect1;
    public Suggestion suggestionDefect2;
    public Suggestion suggestionWish1;
    public Suggestion suggestionWish2;
    public Suggestion suggestionInternal;

    public List<Suggestion> suggestionsTenant1;

    public Gossip tenant2Gossip1;
    public Gossip tenant2Gossip2;
    public Gossip tenant2Gossip3;
    public Comment tenant2Gossip1Comment;
    public Comment tenant2Gossip1Comment2;
    public Comment tenant2Gossip1Comment3;

    public List<Comment> tenant2Gossip1Comments;

    public List<TradingCategory> tradingCategoriesInOrder;

    public List<SuggestionCategory> suggestionCategoriesInOrder;

    public TradingCategory tradingCategoryMisc;
    public TradingCategory tradingCategoryComputer;
    public TradingCategory tradingCategoryTools;
    public TradingCategory tradingCategoryService;

    public SuggestionCategory suggestionCategoryMisc;
    public SuggestionCategory suggestionCategoryWaste;
    public SuggestionCategory suggestionCategoryDamage;
    public SuggestionCategory suggestionCategoryRoadDamage;
    public SuggestionCategory suggestionCategoryWish;

    public Seeking seeking1;
    public Seeking seeking2;
    public Seeking seeking3;
    public Seeking seeking4;
    public Seeking seeking5;
    public List<Seeking> seekingsTenant1;

    public Offer offer1;
    public Offer offer2;
    public Offer offer3;
    public Offer offer4;
    public Offer offer5;

    public Comment offer1Comment1;
    public Comment offer1Comment2;
    public Comment offer4Comment1;

    public Organization organizationFeuerwehr;
    public Organization organizationGartenbau;

    public List<Offer> offersTenant1;

    public List<Post> postsCreatedBySusanneChristmann = new ArrayList<>();

    @Override
    public void deleteAllData() {
        super.deleteAllData();
        postsCreatedBySusanneChristmann = null;
        offersTenant1 = null;
        seekingsTenant1 = null;
        suggestionCategoriesInOrder = null;
        tradingCategoriesInOrder = null;
        tenant2Gossip1Comments = null;
        suggestionsTenant1 = null;
        newsItemsTenant1 = null;
        happeningsTenant1 = null;
        gossipsTenant2 = null;
        gossipsTenant1 = null;
        gossip2Comments = null;
        newsSourceService.invalidateCache();
    }

    @Override
    public void createPersons() {
        super.createPersons();
        personThomasBecker = createPerson("Thomas", tenant1, subGeoArea1Tenant1,
                Address.builder()
                        .name("Adeline")
                        .street("Gutenbergstrasse 18")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.4312883, 7.7524382)).build(),
                "22db06ed-f9d4-44c9-8a92-f2784cfa7dec");
        personSebastianBauer = createPerson("Sebastian", tenant1,
                Address.builder()
                        .name("Sebastian")
                        .street("Pirmasenser Strasse 8")
                        .zip("67655")
                        .city("Kaiserslautern")
                        .gpsLocation(GPS_LOCATION_KL_CITY).build(),
                "5acf7503-34ec-47aa-8b83-73878c46c5fd");

        personSusanneChristmann = createPersonWithDefaultAddress("Susanne", tenant1, geoAreaDorf2InEisenberg,
                "c881845c-c950-4ad2-a143-89c6a2feedd5");
        personLaraSchaefer = createPersonWithDefaultAddress("Lara", tenant1, geoAreaDorf2InEisenberg,
                "d83e1066-637d-4cfb-86d0-6296ff0654c9");
        personNinaBauer = createPersonWithDefaultAddress("Nina", tenant3, "f36ee1e2-8d1b-470f-a929-2eeda6e59ee3");
        personAnnikaSchneider =
                createPersonWithDefaultAddress("Annikar", tenant3, "ae68b0a2-2d34-4b3e-a927-87d9aee7bf20");
        personMonikaHess = createPersonWithDefaultAddress("Monika", tenant2, geoAreaTenant2,
                "6135ce15-9963-4f55-b74d-e2c56a97e78d");

        //special person for user statistics
        personFriedaFischer = createPersonWithDefaultAddress("Frieda", tenant1, "685d3468-2d8a-4ada-968a-9cf451f2019f");
        personFriedaFischer.setCreated(nextTimeStamp() - TimeUnit.DAYS.toMillis(15));
        personFriedaFischer.setLastLoggedIn(nextTimeStamp());
        personFriedaFischer = personRepository.save(personFriedaFischer);

        personSuggestionWorkerTenant1Horst = createPersonSuggestionWorker("Horst", tenant1,
                "cb49d09b-f7ce-44d4-b58d-038afa906dc5");
        personSuggestionWorkerTenant1Dieter = createPersonSuggestionWorker("Dieter", tenant1,
                "80972b02-a717-4be8-b333-964011416344");
        personSuggestionWorkerTenant2Heinz = createPersonSuggestionWorker("Heinz", tenant2,
                "f22a16ae-abaf-423e-a9b7-bdf0b9fad680");
        personSuggestionFirstResponderTenant1Hilde = createPersonSuggestionFirstResponder("Hilde", tenant1,
                "954f8344-5aea-42a4-9021-628a16dab094");
        personSuggestionWorkerAndFirstResponderTenant1Carmen =
                createPersonSuggestionWorkerAndFirstResponder("Carmen", tenant1,
                        "cf3f784c-eb6a-495a-a7b6-11faa99fe0b1");

        //in order as expected by /grapevine/suggestion/{suggestionId}/potentialWorker
        personsWithSuggestionRoleTenant1 = List.of(personSuggestionWorkerAndFirstResponderTenant1Carmen,
                personSuggestionFirstResponderTenant1Hilde,
                personSuggestionWorkerTenant1Dieter,
                personSuggestionWorkerTenant1Horst);
    }

    @Override
    public void createAppEntities() {

        appDorffunk = appRepository.save(App.builder()
                .id(DorfFunkConstants.APP_ID)
                .name("DorfFunk")
                .appIdentifier("dd-dorffunk")
                .logo(createMediaItem("dorffunk-logo"))
                .build());

        appDorfNews = appRepository.save(App.builder()
                .id(DorfNewsConstants.APP_ID)
                .name("DorfNews")
                .appIdentifier("dd-dorfnews")
                .logo(createMediaItem("dorfnews-logo"))
                .build());

        assertNotNull(tenant1);
        assertNotNull(tenant2);
        assertNotNull(tenant3);

        appVariant1Tenant1OauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariant1Tenant1OauthClient-identifier")
                .build());
        appVariant1Tenant1 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .oauthClients(Collections.singleton(appVariant1Tenant1OauthClient))
                .appVariantIdentifier("DorfFunk-variant1")
                .build());
        mapGeoAreaToAppVariant(appVariant1Tenant1, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariant1Tenant1, geoAreaKaiserslautern, tenant1);

        appVariant2Tenant1And2OauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariant2Tenant1And2OauthClient-identifier")
                .build());
        appVariant2Tenant1And2 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .oauthClients(Collections.singleton(appVariant2Tenant1And2OauthClient))
                .appVariantIdentifier("DorfFunk-variant2")
                .build());
        mapGeoAreaToAppVariant(appVariant2Tenant1And2, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariant2Tenant1And2, geoAreaKaiserslautern, tenant1);
        mapGeoAreaToAppVariant(appVariant2Tenant1And2, geoAreaTenant2, tenant2);

        appVariant3Tenant3OauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariant3Tenant3OauthClient-identifier")
                .build());
        appVariant3Tenant3 = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .oauthClients(Collections.singleton(appVariant3Tenant3OauthClient))
                .appVariantIdentifier("DorfFunk-variant3")
                .build());
        mapGeoAreaToAppVariant(appVariant3Tenant3, geoAreaTenant3, tenant3);

        appVariant4AllTenantsOauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariant4AllTenantsOauthClient-identifier")
                .build());
        appVariant4AllTenants = appVariantRepository.save(AppVariant.builder()
                .app(appDorffunk)
                .oauthClients(Collections.singleton(appVariant4AllTenantsOauthClient))
                .appVariantIdentifier("DorfFunk-alltenants")
                .build());
        mapGeoAreaToAppVariant(appVariant4AllTenants, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariant4AllTenants, geoAreaKaiserslautern, tenant1);
        mapGeoAreaToAppVariant(appVariant4AllTenants, geoAreaTenant2, tenant2);
        mapGeoAreaToAppVariant(appVariant4AllTenants, geoAreaTenant3, tenant3);

        appVariantWithApiKey1Tenant1OauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariantWithApiKey1Tenant1OauthClient-identifier")
                .build());
        appVariantWithApiKey1Tenant1 = appVariantRepository.save(AppVariant.builder()
                .app(appDorfNews)
                .oauthClients(Collections.singleton(appVariantWithApiKey1Tenant1OauthClient))
                .appVariantIdentifier("DorfNews-variant1")
                .apiKey1("newsTestApiKey")
                .apiKey2("secondTestApiKey")
                .callBackUrl("https://dorfnews.digitale-doerfer.de")
                .name("Dorfnews1")
                .build());
        mapGeoAreaToAppVariant(appVariantWithApiKey1Tenant1, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariantWithApiKey1Tenant1, geoAreaKaiserslautern, tenant1);

        appVariantWithApiKey4AllTenantsOauthClient = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("appVariantWithApiKey4AllTenantsOauthClient-identifier")
                .build());
        appVariantWithApiKey4AllTenants = appVariantRepository.save(AppVariant.builder()
                .app(appDorfNews)
                .oauthClients(Collections.singleton(appVariantWithApiKey4AllTenantsOauthClient))
                .appVariantIdentifier("DorfNews-alltenants")
                .apiKey1("0123456789")
                .callBackUrl("https://analog.digitale-doerfer.de")
                .name("DorfnewsAll")
                .build());
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants, geoAreaKaiserslautern, tenant1);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants, geoAreaTenant2, tenant2);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants, geoAreaTenant3, tenant3);

        appVariantWithApiKey4AllTenants2 = appVariantRepository.save(AppVariant.builder()
                .app(appDorfNews)
                .oauthClients(Collections.singleton(appVariantWithApiKey4AllTenantsOauthClient))
                .appVariantIdentifier("DorfNews-alltenants-2")
                .apiKey1("987654321")
                .callBackUrl("https://news.katzenbach.de")
                .name("DorfnewsAll2")
                .build());
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants2, geoAreaEisenberg, tenant1);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants2, geoAreaKaiserslautern, tenant1);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants2, geoAreaTenant2, tenant2);
        mapGeoAreaToAppVariant(appVariantWithApiKey4AllTenants2, geoAreaTenant3, tenant3);
    }

    @Override
    public void createPushEntities() {

        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_SEEKING_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_OFFER_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CREATED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_CHANGED);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_DELETED);

        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_REPLY);

        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_CHAT);
        createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_CHAT_CREATED);
        pushCategoryChatMessageReceived =
                createPushCategory(DorfFunkConstants.PUSH_CATEGORY_ID_CHAT_MESSAGE_RECEIVED);

        pushCategorySuggestionInternalWorkerChat =
                createPushCategory(LoesBarConstants.PUSH_CATEGORY_ID_INTERNAL_WORKER_CHAT_MESSAGE_RECEIVED);

        createPushCategory(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_CREATED);
        createPushCategory(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_COMMENT_CREATED);
        createPushCategory(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_STATUS_CHANGED);
        createPushCategory(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_WORKER_ADDED);

        pushCategoryRepository.save(PushCategory.builder()
                .app(appDorffunk)
                .name("Achievements")
                .description("Achievements")
                .subject(MotivationConstants.PUSH_CATEGORY_SUBJECT)
                .defaultLoudPushEnabled(true)
                .build()
                .withId(DorfFunkConstants.PUSH_CATEGORY_ID_MOTIVATION));
    }

    private PushCategory createPushCategory(PushCategoryId pushCategoryId) {
        return createPushCategory(appDorffunk, pushCategoryId);
    }

    private PushCategory createPushCategory(PushCategoryId pushCategoryId, PushCategoryId parentPushCategoryId) {
        return createPushCategory(appDorffunk, pushCategoryId, parentPushCategoryId);
    }

    private long currentPostTimeStamp = System.currentTimeMillis();

    public long nextPostTimeStamp() {
        //we want to create objects in reversed order such that the object created first is the newest and, thus, listed
        //first when calling GET /post
        return currentPostTimeStamp--;
    }

    private Person createPersonSuggestionFirstResponder(String name, Tenant tenant, String uuid) {

        Person person = createPersonWithDefaultAddress(name, tenant, uuid);
        assignRoleToPerson(person, SuggestionFirstResponder.class, tenant.getId());
        return person;
    }

    private Person createPersonSuggestionWorker(String name, Tenant tenant, String uuid) {

        Person person = createPersonWithDefaultAddress(name, tenant, uuid);
        assignRoleToPerson(person, SuggestionWorker.class, tenant.getId());
        return person;
    }

    private Person createPersonSuggestionWorkerAndFirstResponder(String name, Tenant tenant, String uuid) {

        Person person = createPersonWithDefaultAddress(name, tenant, uuid);
        assignRoleToPerson(person, SuggestionFirstResponder.class, tenant.getId());
        assignRoleToPerson(person, SuggestionWorker.class, tenant.getId());
        return person;
    }

    private <P extends Post> P setTimeStampsAndSave(P item) {
        return setTimeStampsAndSave(item, nextPostTimeStamp());
    }

    private Suggestion setTimeStampsAndSave(Suggestion item) {
        return setTimeStampsAndSave(item, nextPostTimeStamp());
    }

    private <P extends Post> P setTimeStampsAndSave(P item, long timeStamp) {
        return savePost(item, timeStamp);
    }

    private Suggestion setTimeStampsAndSave(Suggestion item, long timeStamp) {
        item.setLastSuggestionActivity(timeStamp);
        return savePost(item, timeStamp);
    }

    private <P extends Post> P savePost(P item, long timeStamp) {
        item.setCreated(timeStamp);
        item.setLastModified(timeStamp);
        item.setLastActivity(timeStamp);

        // since we do not check this in the tests, it might result in NPEs
        if (item.getImages() == null) {
            item.setImages(Collections.emptyList());
        }
        P post = postRepository.save(item);
        Post savedPost = postRepository.findByIdFull(post.getId());
        assertThat(item.getGeoAreas()).isNotEmpty();
        assertThat(savedPost.getGeoAreas()).isNotEmpty();
        assertThat(savedPost.getGeoAreas()).containsExactlyElementsOf(item.getGeoAreas());
        return post;
    }

    public void createGrapevineScenario() {

        createGrapevineScenarioWithoutSuggestionsAndWithoutLikes();
        createSuggestions();
        createInternalSuggestion();
        createLikes();
    }

    public void createGrapevineScenarioWithoutSuggestions() {

        createGrapevineScenarioWithoutSuggestionsAndWithoutLikes();
        createLikes();
    }

    public void createGrapevineScenarioWithoutInternalSuggestions() {

        createGrapevineScenarioWithoutSuggestionsAndWithoutLikes();
        createSuggestions();
        createLikes();
    }

    private void createGrapevineScenarioWithoutSuggestionsAndWithoutLikes() {

        createTenantsAndGeoAreas();
        createPersons();
        createAppEntities();
        createPushEntities();

        createOrganizations();
        createNewsSources();
        createSuggestionCategories();
        createTradingCategories();

        //some tests make assumptions of the order in which the posts are created
        //Note that items in GrapevineTestHelper are created with DESCENDING timestamp: The first post created
        //has the newest timestamp. This means that the first item created is also listed first when calling
        //GET /post, which makes these unit test easier to write since new data is added to the end.
        createGossipsAndGossipComments();
        createNewsItemsAndComments();
        createHappeningsAndComments();
        createSeekings();
        createOffersAndComments();
        createSpecialPostsAndSpecialPostComments();
    }

    private void createGossipsAndGossipComments() {

        gossip1withImages = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPizzaNapoli)
                .customAddress(createAddressFromLocation(gpsLocationPizzaNapoliCustom))
                .text("Bei Pizza Napoli gibt es ab heute neun Pizza-Sorten!")
                .images(List.of(createMediaItem("flyer1"),
                        createMediaItem("flyer2")))
                .build());

        gossip2WithComments = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Kann man bei Restaurant Da Nicola mit Karte bezahlen?")
                .build());

        gossip3WithComment = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personThomasBecker)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Weißt jemand wann es den Schnuppertag in der Pestalozzi-Schule gibt?")
                .build());

        gossip4 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personThomasBecker)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .customAddress(createAddressFromLocation(gpsLocationPizzaNapoli))
                .text("Ich habe eine Pizza zu verkaufen!")
                .images(List.of(createMediaItem("pizza1"), createMediaItem("pizza2"), createMediaItem("pizza3")))
                .attachments(Set.of(createDocumentItem("pizza"), createDocumentItem("preise")))
                .build());

        gossip5 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personThomasBecker)
                .createdLocation(gpsLocationPestalozziSchule)
                .customAddress(createAddressFromLocation(gpsLocationPestalozziSchule))
                .text("Ich habe ein Dreirad zu verkaufen!")
                .images(List.of(createMediaItem("dreirad_1"), createMediaItem("dreirad_2")))
                .build());

        gossip6 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPestalozziSchule)
                .customAddress(createAddressFromLocation(gpsLocationPestalozziSchule))
                .text("Gossip 6 ")
                .build());

        gossip7 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personLaraSchaefer)
                .createdLocation(gpsLocationPestalozziSchule)
                .customAddress(createAddressFromLocation(gpsLocationPestalozziSchule))
                .text("Gossip 7")
                .images(List.of(createMediaItem("g7_img1"), createMediaItem("g7_img2")))
                .build());

        gossip8With9Images = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPestalozziSchule)
                .customAddress(createAddressFromLocation(gpsLocationPestalozziSchule))
                .text("Gossip mit 9 Images")
                .images(List.of(createMediaItem("image1"),
                        createMediaItem("image2"),
                        createMediaItem("image3"),
                        createMediaItem("image4"),
                        createMediaItem("image5"),
                        createMediaItem("image6"),
                        createMediaItem("image7"),
                        createMediaItem("image8"),
                        createMediaItem("image9")))
                .build());

        gossip9With10Images = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personLaraSchaefer)
                .createdLocation(gpsLocationPestalozziSchule)
                .customAddress(createAddressFromLocation(gpsLocationPestalozziSchule))
                .text("Gossip mit 10 Images")
                .images(List.of(createMediaItem("image1"),
                        createMediaItem("image2"),
                        createMediaItem("image3"),
                        createMediaItem("image4"),
                        createMediaItem("image5"),
                        createMediaItem("image6"),
                        createMediaItem("image7"),
                        createMediaItem("image8"),
                        createMediaItem("image9"),
                        createMediaItem("image10")))
                .build());

        tenant2Gossip1 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personMonikaHess)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Um wie viel Uhr findet der Info-Abend statt?")
                .build());

        tenant2Gossip2 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personMonikaHess)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Kann man die Kinder zum Info-Abend mitbringen?")
                .build());

        tenant2Gossip3 = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Gossip 3")
                .deleted(true)
                .build());

        createGossipsWithFixedCreationDate();

        createGossipComments();

        if (postsCreatedBySusanneChristmann == null) {
            postsCreatedBySusanneChristmann = new ArrayList<>();
        }
        postsCreatedBySusanneChristmann.add(gossip1withImages);
        postsCreatedBySusanneChristmann.add(gossip2WithComments);
        postsCreatedBySusanneChristmann.add(gossip6);
        postsCreatedBySusanneChristmann.add(gossip8With9Images);
        postsCreatedBySusanneChristmann.add(tenant2Gossip3);

        gossipsTenant1 = List.of(gossip1withImages, gossip2WithComments, gossip3WithComment, gossip4, gossip5);
        gossipsTenant2 = List.of(gossip6, gossip7, gossip8With9Images, gossip9With10Images, tenant2Gossip1,
                tenant2Gossip2, tenant2Gossip3);
    }

    //These Gossips here are created with specific creation times in order to test the timestamp filters
    //don't change that
    private void createGossipsWithFixedCreationDate() {

        gossip1WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personNinaBauer)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Pizza für nur 5€ bei Restaurant Da Nicola")
                .build(), 1000);

        gossip2WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personAnnikaSchneider)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Leckere Pizza für nur 5€")
                .build(), 1100);

        gossip3WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personNinaBauer)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Wie lange ist das Restaurant Da Nicola am Samstags geöffnet?")
                .organization(organizationGartenbau)
                .build(), 1200);
        gossip3WithFixedCreationDate.setLastActivity(1350);
        gossip3WithFixedCreationDate = postRepository.save(gossip3WithFixedCreationDate);

        gossip4WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personAnnikaSchneider)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Texto generico em portugues")
                .organization(organizationGartenbau)
                .build(), 1300);
        gossip4WithFixedCreationDate.setLastActivity(1450);
        gossip4WithFixedCreationDate = postRepository.save(gossip4WithFixedCreationDate);

        gossip5WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personNinaBauer)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("mais um texto generico")
                .organization(organizationGartenbau)
                .build(), 1400);

        gossip6WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personNinaBauer)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Gibt es bei Da Nicola 'free refill'?")
                .organization(organizationGartenbau)
                .build(), 1500);

        gossip7WithFixedCreationDate = setTimeStampsAndSave(Gossip.builder()
                .tenant(tenant3)
                .geoAreas(Set.of(geoAreaTenant3))
                .creator(personAnnikaSchneider)
                .createdLocation(gpsLocationRestauranteDaNicola)
                .text("Ist das Restaurant Da Nicola zum Mittagessen geöffnet?")
                .build(), 1600);
    }

    public Comment setTimeStampsAndSave(Comment comment) {

        long nextPostTimeStamp = nextPostTimeStamp();
        comment.setCreated(nextPostTimeStamp);
        comment.setLastModified(nextPostTimeStamp);

        Post post = comment.getPost();
        post.setLastActivity(comment.getCreated());
        post.setCommentCount(post.getCommentCount() + 1);
        postRepository.save(post);

        // the saved comment has a !copy! of the post object
        // so changing the original post will not update the post of the comment
        // --> leading to bugs that take 2 hours to fix :-(
        // Solution: replacing the post of the saved comment with the original one
        Comment savedComment = commentRepository.save(comment);
        savedComment.setPost(post);
        return savedComment;
    }

    private void createGossipComments() {

        gossip2Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(gossip2WithComments)
                .creator(personThomasBecker)
                .text("Ja ich hab letztens mit Karte bezahlt")
                .build());

        gossip2Comment2Deleted = setTimeStampsAndSave(Comment.builder()
                .post(gossip2WithComments)
                .creator(personLaraSchaefer)
                .text("Ich zahle immer bar. Die Pizza dort ist lecker, hab'n Foto gemacht")
                .image(createMediaItem("unterbelichtetes Handybild"))
                .deleted(true)
                .build());

        gossip2Comment3 = setTimeStampsAndSave(Comment.builder()
                .post(gossip2WithComments)
                .creator(personSusanneChristmann)
                .replyTo(gossip2Comment1)
                .text("War das eine Kreditkarte?")
                .build());

        gossip2Comment4withImages = setTimeStampsAndSave(Comment.builder()
                .post(gossip2WithComments)
                .creator(personThomasBecker)
                .replyTo(gossip2Comment3)
                .text("Jip, Mastercard 💳 Schau hier ist der Beleg!")
                .image(createMediaItem("beleg"))
                .image(createMediaItem("kontoaufzug"))
                .attachments(Set.of(createDocumentItem("beleg.pdf"), createDocumentItem("kontoauszug.pdf")))
                .build());

        gossip3Comment1withOnlyImages = setTimeStampsAndSave(Comment.builder()
                .post(gossip3WithComment)
                .creator(personSebastianBauer)
                .image(createMediaItem("katzenbild1"))
                .image(createMediaItem("katzenbild2"))
                .image(createMediaItem("katzenbild3"))
                .build());

        tenant2Gossip1Comment = setTimeStampsAndSave(Comment.builder()
                .post(tenant2Gossip1)
                .creator(personMonikaHess)
                .text("Der findet um 19:00 Uhr statt")
                .build());

        tenant2Gossip1Comment2 = setTimeStampsAndSave(Comment.builder()
                .post(tenant2Gossip1)
                .creator(personLaraSchaefer)
                .text("Danke, ich dachte es wäre um 19:30 Uhr")
                .build());

        tenant2Gossip1Comment3 = setTimeStampsAndSave(Comment.builder()
                .post(tenant2Gossip1)
                .creator(personSusanneChristmann)
                .attachments(new HashSet<>(Collections.singletonList(createDocumentItem("uhrzeit"))))
                .text("Um 19:30 Uhr")
                .build());

        gossip2Comments =
                List.of(gossip2Comment1, gossip2Comment2Deleted, gossip2Comment3, gossip2Comment4withImages);
        tenant2Gossip1Comments =
                List.of(tenant2Gossip1Comment, tenant2Gossip1Comment2, tenant2Gossip1Comment3);
    }

    private void createSpecialPostsAndSpecialPostComments() {

        specialPost1withImages = setTimeStampsAndSave(SpecialPost.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPizzaNapoli)
                .customAddress(createAddressFromLocation(gpsLocationPizzaNapoli))
                .text("Suche Hilfe zum Austragen der Flyer für den neuen Pizzaladen")
                .images(List.of(createMediaItem("flyer1"), createMediaItem("flyer2")))
                .attachments(Set.of(createDocumentItem("Anleitung"),
                        createDocumentItem("Arbeitsvertrag"),
                        createDocumentItem("Kündigung")))
                .customAttributesJson(
                        "{\"specialPostType \": \"" + specialPostTypeFeatureValue + "\", \"type\": \"request\"}")
                .build());

        specialPost2withComments = setTimeStampsAndSave(SpecialPost.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personThomasBecker)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Biete meine Manneskraft zum Hausbau an!")
                .customAttributesJson(
                        "{\"specialPostType \": \"" + specialPostTypeFeatureValue + "\", \"type\": \"offer\"}")
                .build());

        specialPost3 = setTimeStampsAndSave(SpecialPost.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(gpsLocationPizzaNapoli)
                .text("Bin gelernte Pizzabäckerin und würde gerne beim Zubereiten der Pizzen helfen")
                .build());

        specialPost4withImages = setTimeStampsAndSave(SpecialPost.builder()
                .tenant(tenant2)
                .geoAreas(Set.of(geoAreaTenant2))
                .creator(personThomasBecker)
                .createdLocation(gpsLocationPestalozziSchule)
                .text("Verkaufe mein gesamtes Inventar!")
                .images(List.of(createMediaItem("image1"), createMediaItem("image2"), createMediaItem("image3")))
                .attachments(Set.of(
                        createDocumentItem("Liste", MediaType.parseMediaType("application/xlsx")),
                        createDocumentItem("Ikea1"),
                        createDocumentItem("Ikea2"),
                        createDocumentItem("Ikea3")))
                .customAttributesJson("{\"price\": 199.99, \"place\": \"Uli's Kiosk\"}")
                .build());

        createSpecialPostComments();

        specialPostsTenant1 = List.of(specialPost1withImages, specialPost3);
        specialPostsTenant2 = List.of(specialPost2withComments, specialPost4withImages);
    }

    private void createSpecialPostComments() {

        specialPost2Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(specialPost2withComments)
                .creator(personLaraSchaefer)
                .text("Das hört sich irgendwie merkwürdig an...")
                .build());

        specialPost2Comment2Deleted = setTimeStampsAndSave(Comment.builder()
                .post(specialPost2withComments)
                .creator(personNinaBauer)
                .text("Diese Dienste nehme ich doch gerne an :)")
                .image(createMediaItem("anzügliches Handybild"))
                .deleted(true)
                .build());

        specialPost2Comment3 = setTimeStampsAndSave(Comment.builder()
                .post(specialPost2withComments)
                .creator(personFriedaFischer)
                .replyTo(specialPost2Comment1)
                .text("Habe ich mir eben auch gedacht")
                .build());

        specialPost2Comment4withImages = setTimeStampsAndSave(Comment.builder()
                .post(specialPost2withComments)
                .creator(personThomasBecker)
                .replyTo(specialPost2Comment3)
                .text("")
                .image(createMediaItem("unnötiges Bild"))
                .build());

        specialPost2Comments =
                List.of(specialPost2Comment1, specialPost2Comment2Deleted, specialPost2Comment3,
                        specialPost2Comment4withImages);
    }

    public void createNewsSources() {

        newsSourceDigitalbach = newsSourceRepository.save(NewsSource.builder()
                .id("1c88eae7-97b5-4f78-ad03-083b7cec55d7")
                .siteUrl("https://dorfnews.digitale-doerfer.de")
                .siteName("Digitalbach Aktuell")
                .siteLogo(createMediaItem("news-bild-dicki"))
                .appVariant(appVariantWithApiKey1Tenant1)
                .build());

        newsSourceAnalogheim = newsSourceRepository.save(NewsSource.builder()
                .id("2d736761-796b-462f-9c24-537036f777db")
                .siteUrl("https://analog.digitale-doerfer.de")
                .siteName("Analogheim News")
                .siteLogo(createMediaItem("news-bild-anna"))
                .appVariant(appVariantWithApiKey4AllTenants)
                .build());

        newsSourceKatzenbach = newsSourceRepository.save(NewsSource.builder()
                .id("969dc64e-db7e-4c48-bddb-425d06b41426")
                .siteUrl("https://news.katzenbach.de")
                .siteName("Katzen News")
                .siteLogo(createMediaItem("news-bild-katze"))
                .appVariant(appVariantWithApiKey4AllTenants2)
                .build());

        newsSourceDeleted = newsSourceRepository.save(NewsSource.builder()
                .id("4f1c1a03-2be2-4154-9883-1202d478b74f")
                .siteUrl("https://dead.inside.de")
                .siteName("Keine News")
                .deleted(true)
                .deletionTime(4711L)
                .siteLogo(createMediaItem("dead inside"))
                .build());
        newsSourceOrganizationMappingRepository.save(NewsSourceOrganizationMapping.builder()
                .newsSource(newsSourceDigitalbach)
                .organization(organizationFeuerwehr)
                .build());
        newsSourceOrganizationMappingRepository.save(NewsSourceOrganizationMapping.builder()
                .newsSource(newsSourceDigitalbach)
                .organization(organizationGartenbau)
                .build());
    }

    public void createNewsItemsAndComments() {

        log.info(externalPostRepository.findAll().stream()
                .map(ExternalPost::toString)
                .collect(Collectors.joining("\n")));

        String text1 =
                "Weg sollte sie, die alte Dorfmühle, und drei neuen modernen Windrädern weichen. Die Bürger der " +
                        "Gemeinden Digitalbach, Miesnetzdorf und Analogtal waren entsetzt... ";
        String text2 = "Am 11. November ist es wieder soweit und alle Kinder der Gemeinden Digitalbach, Miesnetzdorf " +
                "und Analogtal dürfen sich wieder auf den diesjährigen St. Martins Umzug zur alten Dorfmühle freuen. ";
        String text3 = "Am 3. Dezember veranstaltet der FSV Digitalbach gemeinsam mit dem DRK-Blutspendedienst " +
                "Miesnetzdorf eine Blutspende-Aktion...";
        String text4 = "Am 10. September wird Digitalbach vom preisgekrönten Orchester „Digitale Sinfonie“ in eine " +
                "neue musikalische Welt entführt. Sie begeisterten bereits Hallen in Mannheim, Köln und Karlsruhe. Nun " +
                "darf auch Digitalbach entzück...";

        newsItem1 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personThomasBecker)
                .authorName("Tom Tamanini")
                .images(List.of(createMediaItem("image_news1"), createMediaItem("image2_news1")))
                .categories("Sport")
                .externalId("EXTERNAL-ID1")
                .text(text1)
                .url("https://dorfnews.digitale-doerfer.de/alte-dorfmuehle-soll-verschont-bleiben/")
                .newsSource(newsSourceDigitalbach)
                .build());

        newsItem2 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .authorName("Das Jillo")
                .images(Collections.singletonList(createMediaItem("image_news2")))
                .categories("Sport")
                .externalId("EXTERNAL-ID2")
                .text(text2)
                .url("https://dorfnews.digitale-doerfer.de/event/st-martins-umzug/")
                .newsSource(newsSourceDigitalbach)
                .build());

        newsItem3 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .authorName("Susi B")
                .images(List.of(createMediaItem("image_news3")))
                .categories("Sport")
                .externalId("EXTERNAL-ID3")
                .text(text3)
                .url("https://dorfnews.digitale-doerfer.de/event/blutspende-aktion-im-gemeindehaus/")
                .newsSource(newsSourceDigitalbach)
                .build());

        newsItem4 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .images(List.of(createMediaItem("image_news4")))
                .authorName("Horsti Borsti")
                .categories("Sport")
                .externalId("EXTERNAL-ID4")
                .text(text4)
                .url("https://dorfnews.digitale-doerfer.de/event/konzert-des-orchesters-digitale-sinfonie/")
                .newsSource(newsSourceDigitalbach)
                .build());

        newsItem5 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .images(List.of(createMediaItem("image_news5")))
                .authorName("El Huppo")
                .categories("Sport")
                .externalId("EXTERNAL-ID5")
                .text(text4)
                .organization(organizationFeuerwehr)
                .newsSource(newsSourceDigitalbach)
                .deleted(true)
                .build());

        newsItem6 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant2))
                .images(List.of(createMediaItem("image_news5")))
                .authorName("El Huppo")
                .categories("Sport")
                .externalId("EXTERNAL-ID6")
                .text(text4)
                .organization(organizationGartenbau)
                .newsSource(newsSourceKatzenbach)
                .deleted(true)
                .build());

        unpublishedNewsItem1 = setTimeStampsAndSave(NewsItem.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .authorName("Tom Tamanini")
                .images(List.of(createMediaItem("image_news1"), createMediaItem("image2_news1")))
                .categories("Sport")
                .externalId("EXTERNAL-UNPUBLISHED-ID1")
                .text(text1)
                .url("https://dorfnews.digitale-doerfer.de/alte-dorfmuehle-soll-verschont-bleiben/?unpublished")
                .newsSource(newsSourceDigitalbach)
                .published(false)
                .desiredPublishTime(Long.MAX_VALUE)
                .build());

        newsItem2Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(newsItem2)
                .creator(personSusanneChristmann)
                .text("Meine Kinder freuen sich schon")
                .build());

        newsItem2Comment2 = setTimeStampsAndSave(Comment.builder()
                .post(newsItem2)
                .creator(personLaraSchaefer)
                .text("Wir haben eine tolle Laterne gebastelt")
                .build());

        newsItem2Comment3 = setTimeStampsAndSave(Comment.builder()
                .post(newsItem2)
                .creator(personSusanneChristmann)
                .text("Wir auch")
                .build());

        newsItem3Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(newsItem2)
                .creator(personSusanneChristmann)
                .text("Ich bin dabei!")
                .build());

        newsItemsTenant1 = List.of(newsItem1, newsItem2, newsItem3, newsItem4, newsItem5, unpublishedNewsItem1);

    }

    public void createHappeningsAndComments() {

        long currentTime = System.currentTimeMillis();

        long time1 = currentTime + TimeUnit.DAYS.toMillis(1);
        long time2 = currentTime + TimeUnit.DAYS.toMillis(2);
        long time3 = currentTime + TimeUnit.DAYS.toMillis(7);
        long time4 = currentTime + TimeUnit.DAYS.toMillis(8);
        long time5 = currentTime + TimeUnit.DAYS.toMillis(9);
        long time6 = currentTime + TimeUnit.DAYS.toMillis(14);

        String text1 =
                "Bereits zum fünften mal findet der Kinderflohmarkt in Digitalbach am Sportplatz statt. Neben Second Hand Bekleidung für Babies, Kinder und Schwangere werden auch Spielsachen, Autositze, Kinderwagen etc. angeboten! Für das…";
        String text2 =
                "Die schönste Zeit des Jahres hat begonnen und auch unser jährliches Weinfest rückt näher. Am 20.09. erwartet Sie, neben köstlichen Weiß- und Rotweinen aus der Region Digitalbachs, Musik des Duos…";
        String text3 = "Am 3. Dezember veranstaltet der FSV Digitalbach gemeinsam mit dem DRK-Blutspendedienst " +
                "Miesnetzdorf eine Blutspende-Aktion...";
        String text4 = "Am 10. September wird Digitalbach vom preisgekrönten Orchester „Digitale Sinfonie“ in eine " +
                "neue musikalische Welt entführt. Sie begeisterten bereits Hallen in Mannheim, Köln und Karlsruhe. Nun " +
                "darf auch Digitalbach entzück...";

        Address flohmarktAddress = Address.builder()
                .street("Fraunhofer-Platz 2")
                .city("Kaiserslautern")
                .zip("67663")
                .name("Kita Klammeraeffchen")
                .build();

        Address blutSpendeAddress = Address.builder()
                .street("Hauptstraße 86")
                .city("Eisenberg")
                .zip("67304")
                .name("Gemeindezentrum")
                .build();

        Address konzertAddress = Address.builder()
                .street("Willy-Brandt-Platz 4-5")
                .city("Kaiserslautern")
                .zip("67657")
                .name("Pfalztheather")
                .build();

        happening1 = setTimeStampsAndSave(Happening.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personAnnikaSchneider)
                .authorName("Lara Schaefer")
                .images(List.of(createMediaItem("image1_happening1"), createMediaItem("image2_happening1")))
                .attachments(
                        Set.of(createDocumentItem("document1_happening1"), createDocumentItem("document2_happening1")))
                .categories("Veranstaltung")
                .externalId("EXTERNAL-ID_HAPPENING-1")
                .text(text1)
                .url("https://dorfnews.digitale-doerfer.de/event/5-digitalbacher-kinderflohmarkt/")
                .newsSource(newsSourceDigitalbach)
                .startTime(time3)
                .allDayEvent(true)
                .participantCount(5)
                .customAddress(addressRepository.save(flohmarktAddress))
                .organization(organizationFeuerwehr)
                .build());

        // add personRegularKarl and personSusanneChristmann as participants to happening1
        happeningParticipantRepository.save(new HappeningParticipant(happening1, personRegularKarl));
        happeningParticipantRepository.save(new HappeningParticipant(happening1, personRegularAnna));
        happeningParticipantRepository.save(new HappeningParticipant(happening1, personSusanneChristmann));
        happeningParticipantRepository.save(new HappeningParticipant(happening1, personMonikaHess));
        happeningParticipantRepository.save(new HappeningParticipant(happening1, personThomasBecker));

        happening2 = setTimeStampsAndSave(Happening.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .authorName("Lara Croft")
                .images(List.of(createMediaItem("image_happening2"), createMediaItem("image2_happening2")))
                .categories("Veranstaltung")
                .externalId("EXTERNAL-ID_HAPPENING-2")
                .text(text2)
                .url("https://dorfnews.digitale-doerfer.de/event/digitalbacher-weinfest/")
                .newsSource(newsSourceDigitalbach)
                .startTime(time1)
                .endTime(time3)
                .organizer("Winzer Digital E.V.")
                .organization(organizationGartenbau)
                .build());

        happening3 = setTimeStampsAndSave(Happening.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .authorName("Balthasar")
                .images(List.of(createMediaItem("image_happening3")))
                .categories("Veranstaltung")
                .externalId("EXTERNAL-ID_HAPPENING-3")
                .text(text3)
                .url("https://dorfnews.digitale-doerfer.de/event/blutspende-aktion-im-gemeindehaus/")
                .newsSource(newsSourceDigitalbach)
                .startTime(time2)
                .endTime(time3)
                .customAddress(addressRepository.save(blutSpendeAddress))
                .organizer("FSV Digitalbach")
                .build());

        happening4 = setTimeStampsAndSave(Happening.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .authorName("Heuptsling")
                .images(List.of(createMediaItem("image_happening4")))
                .categories("Veranstaltung")
                .externalId("EXTERNAL-ID_HAPPENING-4")
                .text(text4)
                .url("https://dorfnews.digitale-doerfer.de/event/konzert-des-orchesters-digitale-sinfonie/")
                .newsSource(newsSourceDigitalbach)
                .startTime(time4)
                .allDayEvent(true)
                .customAddress(addressRepository.save(konzertAddress))
                .organization(organizationFeuerwehr)
                .build());

        unpublishedHappening1 = setTimeStampsAndSave(Happening.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .authorName("Lara Croft")
                .images(List.of(createMediaItem("image_happening2"), createMediaItem("image2_happening2")))
                .categories("Veranstaltung")
                .externalId("EXTERNAL-UNPUBLISHED-ID_HAPPENING-1")
                .text(text2)
                .url("https://dorfnews.digitale-doerfer.de/event/digitalbacher-weinfest/?unpublished")
                .newsSource(newsSourceDigitalbach)
                .startTime(time5)
                .endTime(time6)
                .organizer("Winzer Digital E.V.")
                .published(false)
                .desiredPublishTime(Long.MAX_VALUE)
                .build());

        happeningsTenant1 = List.of(happening1, happening2, happening3, happening4, unpublishedHappening1);

        happening1Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(happening1)
                .creator(personLaraSchaefer)
                .text("Gibt es auch Kaffee und Kuchen?")
                .build());

        happening1Comment2 = setTimeStampsAndSave(Comment.builder()
                .post(happening1)
                .creator(personMonikaHess)
                .text("Ja und Wurst auch :-)")
                .build());

        happening2Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(happening2)
                .creator(personThomasBecker)
                .text("Ich freue mich schön!")
                .build());
    }

    public void createLikes() {

        gossip1Post1Interaction = postInteractionRepository.save(PostInteraction.builder()
                .post(gossip1withImages)
                .person(personSusanneChristmann)
                .liked(true)
                .build());

        gossip1Post2Interaction = postInteractionRepository.save(PostInteraction.builder()
                .post(gossip1withImages)
                .person(personThomasBecker)
                .liked(true)
                .build());
        gossip1withImages.setLikeCount(2);
        postRepository.save(gossip1withImages);

        gossip2Post1Interaction = postInteractionRepository.save(PostInteraction.builder()
                .post(gossip2WithComments)
                .person(personSusanneChristmann)
                .liked(true)
                .build());
        gossip2WithComments.setLikeCount(1);
        postRepository.save(gossip2WithComments);

        gossip2Comment1LikeComment1 = likeCommentRepository.save(LikeComment.builder()
                .comment(gossip2Comment1)
                .person(personSusanneChristmann)
                .liked(true)
                .build());

        gossip2Comment1LikeComment2 = likeCommentRepository.save(LikeComment.builder()
                .comment(gossip2Comment1)
                .person(personThomasBecker)
                .liked(true)
                .build());
        gossip2Comment1.setLikeCount(2);
        commentRepository.save(gossip2Comment1);

        gossip2Comment3LikeComment1 = likeCommentRepository.save(LikeComment.builder()
                .comment(gossip2Comment3)
                .person(personSusanneChristmann)
                .liked(true)
                .build());

        gossip2Comment3LikeComment2 = likeCommentRepository.save(LikeComment.builder()
                .comment(gossip2Comment3)
                .person(personThomasBecker)
                .liked(true)
                .build());
        gossip2Comment3.setLikeCount(2);
        commentRepository.save(gossip2Comment3);

        postInteractionRepository.flush();
        likeCommentRepository.flush();
        postRepository.flush();
        commentRepository.flush();
    }

    public void createSuggestions() {

        suggestionDefect1 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personLaraSchaefer)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .customAddress(createAddressFromLocation(playgroundLocation))
                .text("Am Spielplatz ist eine Schaukel kaputt")
                .images(List.of(createMediaItem("spielPlatz"), createMediaItem("kaputteSchaukel"),
                        createMediaItem("kaputteSchaukel2")))
                .attachments(Set.of(createDocumentItem("Beschreibung"),
                        createDocumentItem("Spielplatzordnung")))
                .suggestionStatus(SuggestionStatus.OPEN)
                .suggestionCategory(suggestionCategoryDamage)
                .build()));

        suggestionDefect2 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .customAddress(createAddressFromLocation(trashLocation))
                .text("Hier liegt leider jede Menge Müll im Gebüsch")
                .images(List.of(createMediaItem("gebuesch"), createMediaItem("muell")))
                .suggestionStatus(SuggestionStatus.OPEN)
                .suggestionCategory(suggestionCategoryWaste)
                .build()));

        suggestionIdea1 = setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Die Musikwerkstatt könnte jeden Monat stattfinden. Ich gehe da so gerne hin. Könnte man sich " +
                        "nicht vielleicht mit dem Nachbarort zusammen tun und es immer mal bei uns und bei denen " +
                        "stattfinden lassen? Bei uns sind eh immer nur so vier fünf Leute da...")
                .images(List.of(createMediaItem("noten"), createMediaItem("kontrabass")))
                .suggestionStatus(SuggestionStatus.IN_PROGRESS)
                .suggestionCategory(suggestionCategoryRoadDamage)
                .build());

        suggestionIdea2 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSebastianBauer)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Some Idea")
                .suggestionStatus(SuggestionStatus.DONE)
                .suggestionCategory(suggestionCategoryWaste)
                .build()));

        suggestionIdea3 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personLaraSchaefer)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Another Idea")
                .suggestionStatus(SuggestionStatus.IN_PROGRESS)
                .suggestionCategory(suggestionCategoryMisc)
                .build()));

        final Address wish1Address = addressRepository.save(Address.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .gpsLocation(new GPSLocation(49.431473, 7.752032))
                .verified(true)
                .build());
        suggestionWish1 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Wish 1")
                .suggestionStatus(SuggestionStatus.OPEN)
                .suggestionCategory(suggestionCategoryWish)
                .customAddress(wish1Address)
                .build()));

        suggestionWish2 = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSebastianBauer)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Wish 2")
                .suggestionStatus(SuggestionStatus.OPEN)
                .suggestionCategory(suggestionCategoryWish)
                .build()));

        if (postsCreatedBySusanneChristmann == null) {
            postsCreatedBySusanneChristmann = new ArrayList<>();
        }
        postsCreatedBySusanneChristmann.add(suggestionDefect2);
        postsCreatedBySusanneChristmann.add(suggestionIdea1);
        postsCreatedBySusanneChristmann.add(suggestionWish1);

        suggestionIdea1StatusRecord1 = SuggestionStatusRecord.builder()
                .status(SuggestionStatus.OPEN)
                .suggestion(suggestionIdea1)
                .initiator(personLaraSchaefer)
                .build();
        suggestionIdea1StatusRecord1.setCreated(nextTimeStamp());
        suggestionIdea1StatusRecord1 = suggestionStatusRecordRepository.saveAndFlush(suggestionIdea1StatusRecord1);

        suggestionIdea1StatusRecord2 = SuggestionStatusRecord.builder()
                .status(SuggestionStatus.IN_PROGRESS)
                .suggestion(suggestionIdea1)
                .initiator(personLaraSchaefer)
                .build();
        suggestionIdea1StatusRecord2.setCreated(nextTimeStamp());
        suggestionIdea1StatusRecord2 = suggestionStatusRecordRepository.saveAndFlush(suggestionIdea1StatusRecord2);

        suggestionIdea1.setSuggestionStatusRecords(
                List.of(suggestionIdea1StatusRecord2, suggestionIdea1StatusRecord1));

        suggestionIdea1WorkerMappingHilde = SuggestionWorkerMapping.builder()
                .suggestion(suggestionIdea1)
                .worker(personSuggestionFirstResponderTenant1Hilde)
                .initiator(personSuggestionFirstResponderTenant1Hilde)
                .build();
        suggestionIdea1WorkerMappingHilde.setCreated(nextTimeStamp());
        suggestionIdea1WorkerMappingHilde =
                suggestionWorkerMappingRepository.saveAndFlush(suggestionIdea1WorkerMappingHilde);

        suggestionIdea1WorkerMappingHorst = SuggestionWorkerMapping.builder()
                .suggestion(suggestionIdea1)
                .worker(personSuggestionWorkerTenant1Horst)
                .initiator(personSuggestionFirstResponderTenant1Hilde)
                .build();
        suggestionIdea1WorkerMappingHorst.setCreated(nextTimeStamp());
        suggestionIdea1WorkerMappingHorst =
                suggestionWorkerMappingRepository.saveAndFlush(suggestionIdea1WorkerMappingHorst);

        suggestionIdea1WorkerMappingInactive = SuggestionWorkerMapping.builder()
                .suggestion(suggestionIdea1)
                .worker(personSuggestionWorkerTenant1Dieter)
                .initiator(personSuggestionWorkerTenant1Horst)
                .inactivated(nextTimeStamp())
                .build();
        suggestionIdea1WorkerMappingInactive.setCreated(nextTimeStamp());
        suggestionIdea1WorkerMappingInactive =
                suggestionWorkerMappingRepository.saveAndFlush(suggestionIdea1WorkerMappingInactive);

        suggestionIdea1.setSuggestionWorkerMappings(
                List.of(suggestionIdea1WorkerMappingHilde, suggestionIdea1WorkerMappingHorst));

        suggestionIdea3WorkerMappingCarmen = SuggestionWorkerMapping.builder()
                .suggestion(suggestionIdea3)
                .worker(personSuggestionWorkerAndFirstResponderTenant1Carmen)
                .initiator(personSuggestionWorkerAndFirstResponderTenant1Carmen)
                .build();
        suggestionIdea3WorkerMappingCarmen.setCreated(nextTimeStamp());
        suggestionIdea3WorkerMappingCarmen =
                suggestionWorkerMappingRepository.saveAndFlush(suggestionIdea3WorkerMappingCarmen);

        suggestionIdea3.setSuggestionWorkerMappings(List.of(suggestionIdea3WorkerMappingCarmen));

        addInternalWorkerChat(suggestionDefect1);
        addInternalWorkerChat(suggestionDefect2);
        addInternalWorkerChat(suggestionIdea1);
        addInternalWorkerChat(suggestionIdea2);
        addInternalWorkerChat(suggestionIdea3);
        addInternalWorkerChat(suggestionWish1);
        addInternalWorkerChat(suggestionWish2);

        suggestionsTenant1 =
                List.of(suggestionDefect1, suggestionDefect2, suggestionIdea1, suggestionIdea2, suggestionIdea3,
                        suggestionWish1, suggestionWish2);
    }

    public void createInternalSuggestion() {

        suggestionInternal = createInitialSuggestionStatusRecord(setTimeStampsAndSave(Suggestion.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSuggestionWorkerTenant1Horst)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .customAddress(createAddressFromLocation(playgroundLocation))
                .text("Das ist nur für intern!")
                .images(List.of(createMediaItem("intern1"), createMediaItem("intern2")))
                .suggestionStatus(SuggestionStatus.OPEN)
                .suggestionCategory(suggestionCategoryDamage)
                .internal(true)
                .build()));
    }

    public void createOrganizations() {

        String organizationFeuerwehrFact1Name = "Mitglieder";
        String organizationFeuerwehrFact1Value = "40";
        String organizationFeuerwehrFact2Name = "Fahrzeuge";
        String organizationFeuerwehrFact2Value = "drei";
        organizationFeuerwehr = Organization.builder()
                .name("Feuerwehr")
                .locationDescription("Aschehausen")
                .description("Löscht und erfrischt")
                .overviewImage(createMediaItem("Feuerwehr_Overview"))
                .logoImage(createMediaItem("Feuerwehr_Logo"))
                .detailImages(
                        Arrays.asList(createMediaItem("Feuerwehr_Detail1"), createMediaItem("Feuerwehr_Detail2")))
                .url("https://www.feuerwehr-aschehausen.com")
                .phone("112")
                .emailAddress("feuer@feuerwehr-aschehausen.com")
                .address(addressRepository.saveAndFlush(Address.builder()
                        .name("Feuerwehr Aschehausen")
                        .street("Feuerstraße 1")
                        .zip("67663")
                        .city("Aschehausen")
                        .build()))
                .factsJson("[{" +
                        "\"name\":\"" + organizationFeuerwehrFact1Name + "\"," +
                        "\"value\":\"" + organizationFeuerwehrFact1Value + "\"" +
                        "},{" +
                        "\"name\":\"" + organizationFeuerwehrFact2Name + "\"," +
                        "\"value\":\"" + organizationFeuerwehrFact2Value + "\"" +
                        "}]")
                .tag(OrganizationTag.VOLUNTEERING.getBitMaskValue() | OrganizationTag.EMERGENCY_AID.getBitMaskValue())
                .build();

        organizationFeuerwehr.setOrganizationPersons(Arrays.asList(OrganizationPerson.builder()
                        .organization(organizationFeuerwehr)
                        .category("Vorstand")
                        .position("Erster Brandmelder")
                        .name("Karl Flamme")
                        .profilePicture(createMediaItem("KF"))
                        .build(),
                OrganizationPerson.builder()
                        .organization(organizationFeuerwehr)
                        .category("Vorstand")
                        .position("Zweite Brandmelderin")
                        .name("Susi Flamme")
                        .profilePicture(createMediaItem("SF"))
                        .build()));

        organizationFeuerwehr = organizationRepository.save(organizationFeuerwehr);
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(organizationFeuerwehr)
                .geoArea(geoAreaDorf1InEisenberg)
                .build()
                .withConstantId());

        String organizationGartenbauFact1Name = "Mitglieder";
        String organizationGartenbauFact1Value = "32";
        String organizationGartenbauFact2Name = "Apfelbäume";
        String organizationGartenbauFact2Value = "vierhundert";
        organizationGartenbau = Organization.builder()
                .name("Gartenbauverein")
                .locationDescription("Grünburg")
                .description("Apfel am eigenen Baum erquickt und belebt")
                .overviewImage(createMediaItem("Gartenbau_Overview"))
                .logoImage(createMediaItem("Gartenbau_Logo"))
                .detailImages(
                        Arrays.asList(createMediaItem("Gartenbau_Detail1"), createMediaItem("Gartenbau_Detail2")))
                .url("https://www.gartenbau-grünburg.com")
                .phone("112")
                .emailAddress("feuer@gartenbau-grünburg.com")
                .address(addressRepository.saveAndFlush(Address.builder()
                        .name("Gartenbau Grünburg")
                        .street("Apfelstraße 1")
                        .zip("67663")
                        .city("Grünburg")
                        .build()))
                .factsJson("[{" +
                        "\"name\":\"" + organizationGartenbauFact1Name + "\"," +
                        "\"value\":\"" + organizationGartenbauFact1Value + "\"" +
                        "},{" +
                        "\"name\":\"" + organizationGartenbauFact2Name + "\"," +
                        "\"value\":\"" + organizationGartenbauFact2Value + "\"" +
                        "}]")
                .tag(OrganizationTag.VOLUNTEERING.getBitMaskValue())
                .build();

        organizationGartenbau.setOrganizationPersons(Arrays.asList(
                OrganizationPerson.builder()
                        .organization(organizationGartenbau)
                        .category("Vorstand")
                        .position("Geprüfter Baumschneider")
                        .name("Carlo Birne")
                        .profilePicture(createMediaItem("CB"))
                        .build(),
                OrganizationPerson.builder()
                        .organization(organizationGartenbau)
                        .category("Vorstand")
                        .position("Glyphosat-Experte")
                        .name("Anni Lean")
                        .profilePicture(createMediaItem("AL"))
                        .build()));
        organizationGartenbau = organizationRepository.save(organizationGartenbau);
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(organizationGartenbau)
                .geoArea(geoAreaDorf1InEisenberg)
                .build()
                .withConstantId());
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(organizationGartenbau)
                .geoArea(geoAreaDorf2InEisenberg)
                .build()
                .withConstantId());
    }

    private void addInternalWorkerChat(Suggestion suggestion) {
        Chat chat = chatRepository.save(new Chat(LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC));
        chat.setSubjects(Collections.singleton(chatSubjectRepository.save(ChatSubject.builder()
                .entityName(suggestion.getClass().getName())
                .entityId(suggestion.getId())
                .pushCategory(pushCategorySuggestionInternalWorkerChat)
                .chat(chat)
                .build()
        )));
        if (suggestion.getSuggestionWorkerMappings() != null) {
            chat.setParticipants(new HashSet<>(chatParticipantRepository.saveAll(
                    suggestion.getSuggestionWorkerMappings().stream()
                            .map(swm -> ChatParticipant.builder()
                                    .chat(chat)
                                    .person(swm.getWorker())
                                    .build())
                            .collect(Collectors.toList())
            )));
        }
        suggestion.setInternalWorkerChat(chatRepository.save(chat));
        postRepository.save(suggestion);
    }

    private Suggestion createInitialSuggestionStatusRecord(Suggestion suggestion) {

        suggestion.setSuggestionStatusRecords(
                Collections.singletonList(suggestionStatusRecordRepository.saveAndFlush(SuggestionStatusRecord.builder()
                        .status(suggestion.getSuggestionStatus())
                        .suggestion(suggestion)
                        .initiator(personLaraSchaefer)
                        .created(nextTimeStamp())
                        .build())));
        return suggestion;
    }

    public void createTradingCategories() {

        tradingCategoriesInOrder = new ArrayList<>();
        tradingCategoryMisc =
                newTradingCategory(GrapevineCategoryDataInitializer.TRADING_CATEGORY_MISC_ID, 1, "Verschiedenes",
                        "MISC");
        tradingCategoryComputer = newTradingCategory(2, "Computer und Technik", "COMPUTER");
        tradingCategoryTools = newTradingCategory(3, "Werkzeug", "TOOLS");
        tradingCategoryService = newTradingCategory(4, "Dienstleistungen", "SERVICES");
        tradingCategoriesInOrder = List.of(tradingCategoryMisc, tradingCategoryComputer, tradingCategoryTools,
                tradingCategoryService);
    }

    private TradingCategory newTradingCategory(int orderValue, String displayName, String shortName) {
        return newTradingCategory(null, orderValue, displayName, shortName);
    }

    private TradingCategory newTradingCategory(String idOrNull, int orderValue, String displayName, String shortName) {

        TradingCategory tradingCategory = TradingCategory.builder()
                .displayName(displayName)
                .shortName(shortName)
                .orderValue(orderValue).build();
        tradingCategory.setCreated(nextTimeStamp());
        if (idOrNull != null) {
            tradingCategory.setId(idOrNull);
        }
        return tradingCategoryRepository.save(tradingCategory);
    }

    public void createSuggestionCategories() {

        suggestionCategoryMisc =
                newSuggestionCategory(GrapevineCategoryDataInitializer.SUGGESTION_CATEGORY_MISC_ID, 1, "MISC",
                        "Sonstiges");
        suggestionCategoryDamage = newSuggestionCategory("DAMAGE", 2, "Beschädigung");
        suggestionCategoryWaste = newSuggestionCategory("WASTE", 3, "Müll");
        suggestionCategoryRoadDamage = newSuggestionCategory("ROAD_DAMAGE", 4, "Straßenschäden");
        suggestionCategoryWish = newSuggestionCategory("WISH", 5, "Wunsch");
        suggestionCategoriesInOrder = List.of(suggestionCategoryMisc, suggestionCategoryDamage,
                suggestionCategoryWaste, suggestionCategoryRoadDamage, suggestionCategoryWish);
    }

    private SuggestionCategory newSuggestionCategory(String shortName, int orderValue, String displayName) {
        return newSuggestionCategory(null, orderValue, shortName, displayName);
    }

    private SuggestionCategory newSuggestionCategory(String idOrNull, int orderValue, String shortName,
            String displayName) {
        SuggestionCategory suggestionCategory = SuggestionCategory.builder()
                .displayName(displayName)
                .shortName(shortName)
                .orderValue(orderValue).build();
        suggestionCategory.setCreated(nextTimeStamp());
        if (idOrNull != null) {
            suggestionCategory.setId(idOrNull);
        }
        return suggestionCategoryRepository.save(suggestionCategory);
    }

    public void createSeekings() {

        seeking1 = setTimeStampsAndSave(Seeking.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_KL_UNI)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_UNI))
                .text("Hallo, ich suche eine Räumlichkeit für eine kleine Familienfeier. Für ungefähr 25 Leute. "
                        + "Für alle Hinweise wäare ich dankebar.")
                .images(List.of(createMediaItem("Sekt"), createMediaItem("Wein")))
                .attachments(Set.of(createDocumentItem("Bier"),
                        createDocumentItem("Kaffee")))
                .tradingCategory(tradingCategoryMisc)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        seeking2 = setTimeStampsAndSave(Seeking.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_KL_UNI)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_UNI))
                .text("Hallo, ich brauche noch für meine Feier ein paar Biergarnituren. Kann jemmand mir welche ausleihen?")
                .images(List.of(createMediaItem("Biergarnitur")))
                .tradingCategory(tradingCategoryMisc)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        seeking3 = setTimeStampsAndSave(Seeking.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personLaraSchaefer)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Hallo, wir brauchen für nächsten Samstag ein Babysitter")
                .images(List.of(createMediaItem("Schnuller")))
                .tradingCategory(tradingCategoryService)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        seeking4 = setTimeStampsAndSave(Seeking.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personThomasBecker)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Hallo, am Wochenente brauche ich eine Borhmaschine. Kann jemmand mir welche ausleihen? "
                        + "Gegen einem Kasten Bier :-)")
                .images(List.of(createMediaItem("Bohrmaschine"), createMediaItem("Drillbohrer")))
                .tradingCategory(tradingCategoryTools)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        seeking5 = setTimeStampsAndSave(Seeking.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_KL_UNI)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_UNI))
                .text("Hallo, ich brauche einen Handwerker, der unsere Küche reparieren kann")
                .images(List.of(createMediaItem("Kueche")))
                .tradingCategory(tradingCategoryService)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        if (postsCreatedBySusanneChristmann == null) {
            postsCreatedBySusanneChristmann = new ArrayList<>();
        }
        postsCreatedBySusanneChristmann.add(seeking1);
        postsCreatedBySusanneChristmann.add(seeking2);
        postsCreatedBySusanneChristmann.add(seeking5);

        seekingsTenant1 = List.of(seeking1, seeking2, seeking3, seeking4, seeking5);
    }

    public void createOffersAndComments() {

        offer1 = setTimeStampsAndSave(Offer.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personSusanneChristmann)
                .createdLocation(GPS_LOCATION_KL_UNI)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_UNI))
                .text("Hallo, ich biete Kinderkleidung für Mädchen 104 - 122. Bei Interesse bitte Melden.")
                .images(List.of(createMediaItem("Kleider"), createMediaItem("Schuhe")))
                .attachments(Set.of(createDocumentItem("Kleidung"),
                        createDocumentItem("Kleidung-Text", MediaType.TEXT_PLAIN)))
                .tradingCategory(tradingCategoryMisc)
                .tradingStatus(TradingStatus.OPEN)
                .organization(organizationFeuerwehr)
                .build());

        offer2 = setTimeStampsAndSave(Offer.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personThomasBecker)
                .createdLocation(GPS_LOCATION_KL_CITY)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_CITY))
                .text("Hallo, ich biete euch meinen PKW Anhänger (Ladefläche: L 258 cm x B 128 cm x H 40 cm) für "
                        + "Umzüge oder Ähnliches an. Da ich ihn selten brauche, könnt ihr ihn euch gegen eine "
                        + "Flasche Wein ausleihen")
                .images(List.of(createMediaItem("Anhänger")))
                .tradingCategory(tradingCategoryTools)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        offer3 = setTimeStampsAndSave(Offer.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(geoAreaTenant1))
                .creator(personLaraSchaefer)
                .createdLocation(GPS_LOCATION_KL_UNI)
                .text("Huhu! Ich bin Student (Mathematik) und habe gerade Semesterferien. Ich biete Nachhilfe für 8. "
                        + "bis 12. Klasse in den Fächern Mathe, Physik und Englisch an. Ich komme gerne zu euch nach "
                        + "Hause oder wir treffen uns bei mir. (11€/Std.)")
                .images(List.of(createMediaItem("lineal"), createMediaItem("radiergummi")))
                .tradingCategory(tradingCategoryService)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        offer4 = setTimeStampsAndSave(Offer.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personAnnikaSchneider)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .text("Ich heiße Anne (13) und bin schülerin (8.Klasse).Ich bin verantwortungsbewusst und kinderlieb"
                        + " und würde mich gerne als ihre Babysitterin anbieten. Zeit nach Absprache.")
                .images(List.of(createMediaItem("Schnuller")))
                .tradingCategory(tradingCategoryService)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        offer5 = setTimeStampsAndSave(Offer.builder()
                .tenant(tenant1)
                .geoAreas(Set.of(subGeoArea1Tenant1))
                .creator(personThomasBecker)
                .createdLocation(GPS_LOCATION_DEFAULT_49_8)
                .customAddress(createAddressFromLocation(GPS_LOCATION_KL_CITY))
                .text("Ich biete Reparaturen aller Art, Dachrinnenreinigung,Gartenarbeiten, Grabpflege, "
                        + "Hausmeisterservice und noch viel mehr ")
                .images(List.of(createMediaItem("Werkzeug")))
                .tradingCategory(tradingCategoryMisc)
                .tradingStatus(TradingStatus.OPEN)
                .build());

        offer1Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(offer1)
                .creator(personLaraSchaefer)
                .text("Ich habe Interesse an Mädchenkleidung Gr. 122")
                .build());

        offer1Comment2 = setTimeStampsAndSave(Comment.builder()
                .post(offer1)
                .creator(personThomasBecker)
                .text("Ich habe auch Interesse :-)")
                .build());

        offer4Comment1 = setTimeStampsAndSave(Comment.builder()
                .post(offer4)
                .creator(personLaraSchaefer)
                .text("Hätten Sie am kommende Woche Zeit? Ich brauche unbedingt eine Babysitterin")
                .build());

        if (postsCreatedBySusanneChristmann == null) {
            postsCreatedBySusanneChristmann = new ArrayList<>();
        }
        postsCreatedBySusanneChristmann.add(offer1);

        offersTenant1 = List.of(offer1, offer2, offer3, offer4, offer5);
    }

    public Map<String, Integer> gossipIdToCommentCount() {
        Map<String, Integer> gossipIdToCommentCount = new LinkedHashMap<>();
        gossipIdToCommentCount.put(gossip1withImages.getId(), (int) gossip1withImages.getCommentCount());
        gossipIdToCommentCount.put(gossip2WithComments.getId(), (int) gossip2WithComments.getCommentCount());
        gossipIdToCommentCount.put(gossip3WithComment.getId(), (int) gossip3WithComment.getCommentCount());
        gossipIdToCommentCount.put(gossip4.getId(), (int) gossip4.getCommentCount());
        gossipIdToCommentCount.put(gossip5.getId(), (int) gossip5.getCommentCount());
        gossipIdToCommentCount.put(gossip6.getId(), (int) gossip6.getCommentCount());
        gossipIdToCommentCount.put(gossip7.getId(), (int) gossip7.getCommentCount());
        return gossipIdToCommentCount;
    }

    public ClientSuggestionCategory toClientSuggestionCategory(SuggestionCategory suggestionCategory) {
        return ClientSuggestionCategory.builder()
                .id(suggestionCategory.getId())
                .shortName(suggestionCategory.getShortName())
                .displayName(suggestionCategory.getDisplayName())
                .orderValue(suggestionCategory.getOrderValue())
                .build();
    }

    private ClientSuggestionStatusRecord toClientSuggestionStatusRecord(SuggestionStatusRecord suggestionStatusRecord) {
        return ClientSuggestionStatusRecord.builder()
                .id(suggestionStatusRecord.getId())
                .created(suggestionStatusRecord.getCreated())
                .status(suggestionStatusRecord.getStatus())
                .build();
    }

    private ClientSuggestionWorkerMapping toClientSuggestionWorkerMapping(
            SuggestionWorkerMapping suggestionWorkerMapping) {
        return ClientSuggestionWorkerMapping.builder()
                .id(suggestionWorkerMapping.getId())
                .created(suggestionWorkerMapping.getCreated())
                .worker(toClientPersonReferenceWithEmail(suggestionWorkerMapping.getWorker()))
                .build();
    }

    public ClientSuggestionExtended toClientSuggestionExtended(Suggestion suggestion) {
        final ClientAddress clientCustomAddress;
        Address customAddress = suggestion.getCustomAddress();
        if (customAddress != null) {
            clientCustomAddress = ClientAddress.builder()
                    .id(customAddress.getId())
                    .name(customAddress.getName())
                    .street(customAddress.getStreet())
                    .zip(customAddress.getZip())
                    .city(customAddress.getCity())
                    .gpsLocation(customAddress.getGpsLocation() == null ? null :
                            new ClientGPSLocation(customAddress.getGpsLocation().getLatitude(),
                                    customAddress.getGpsLocation().getLongitude()))
                    .verified(customAddress.isVerified())
                    .build();
        } else {
            clientCustomAddress = null;
        }
        return ClientSuggestionExtended.builder()
                .id(suggestion.getId())
                .created(suggestion.getCreated())
                .liked(false)
                .creator(toClientPersonReferenceWithCensoredLastName(suggestion.getCreator()))
                .commentsAllowed(suggestion.isCommentsAllowed())
                .customAddress(clientCustomAddress)
                .geoAreaIds(suggestion.getGeoAreas()
                        .stream()
                        .map(GeoArea::getId)
                        .sorted()
                        .collect(Collectors.toList()))
                .images(toClientEntityList(suggestion.getImages(), this::toClientMediaItem))
                .attachments(toClientDocumentItems(suggestion.getAttachments()))
                .lastActivity(suggestion.getLastActivity())
                .lastSuggestionActivity(suggestion.getLastSuggestionActivity())
                .lastModified(suggestion.getLastModified())
                .suggestionCategory(toClientSuggestionCategory(suggestion.getSuggestionCategory()))
                .suggestionStatus(suggestion.getSuggestionStatus())
                .suggestionStatusRecords(toClientEntityList(suggestion.getSuggestionStatusRecords(),
                        this::toClientSuggestionStatusRecord))
                .suggestionWorkerMappings(toClientEntityList(suggestion.getSuggestionWorkerMappings(),
                        this::toClientSuggestionWorkerMapping))
                .text(suggestion.getText())
                .withdrawn(suggestion.isDeleted())
                .withdrawReason(suggestion.getWithdrawReason())
                .withdrawnTimestamp(suggestion.getWithdrawn())
                .internalWorkerChatId(suggestion.getInternalWorkerChat().getId())
                .build();
    }

    public ClientComment toClientCommentWithCensoredLastName(Comment comment) {
        if (comment == null) {
            return null;
        }
        ClientComment clientComment = ClientComment.builder()
                .id(comment.getId())
                .created(comment.getCreated())
                .lastModified(comment.getLastModified())
                .postId(comment.getPost().getId())
                .deleted(comment.isDeleted())
                .creator(toClientPersonReferenceWithCensoredLastName(comment.getCreator()))
                .liked(false)
                .likeCount(comment.getLikeCount())
                .build();

        if (!comment.isDeleted()) {
            clientComment.setText(comment.getText());
            clientComment.setImages(toClientEntityList(comment.getImages(), this::toClientMediaItem));
            clientComment.setAttachments(toClientDocumentItems(comment.getAttachments()));
        } // else: text and image are "censored"
        if (comment.getReplyTo() != null) {
            clientComment.setReplyTo(
                    toClientPersonReferenceWithCensoredLastName(comment.getReplyTo().getCreator()));
        }

        return clientComment;
    }

    public ClientOrganization toClientOrganization(Organization organization, Collection<String> geoAreaIds) {

        if (organization == null) {
            return null;
        }
        ClientAddress clientAddress = null;
        if (organization.getAddress() != null) {
            Address address = organization.getAddress();
            ClientGPSLocation clientGPSLocation = null;
            if (address.getGpsLocation() != null) {
                clientGPSLocation = ClientGPSLocation.builder()
                        .latitude(address.getGpsLocation().getLatitude())
                        .longitude(address.getGpsLocation().getLongitude())
                        .build();
            }
            clientAddress = ClientAddress.builder()
                    .id(address.getId())
                    .name(address.getName())
                    .street(address.getStreet())
                    .zip(address.getZip())
                    .city(address.getCity())
                    .gpsLocation(clientGPSLocation)
                    .verified(address.isVerified())
                    .build();
        }
        List<ClientOrganizationTag> clientOrganizationTags;
        Set<OrganizationTag> organizationTags = organization.getTags().getValues();
        if (CollectionUtils.isEmpty(organizationTags)) {
            clientOrganizationTags = Collections.emptyList();
        }
        else {
            EnumSet<ClientOrganizationTag> result = EnumSet.noneOf(ClientOrganizationTag.class);
            for (OrganizationTag organizationTag : organizationTags) {
                switch (organizationTag) {
                    case VOLUNTEERING:
                        result.add(ClientOrganizationTag.VOLUNTEERING);
                        break;
                    case EMERGENCY_AID:
                        result.add(ClientOrganizationTag.EMERGENCY_AID);
                        break;
                }
            }
            clientOrganizationTags = result.stream()
                    .sorted()
                    .collect(Collectors.toList());
        }

        return ClientOrganization.builder()
                .id(organization.getId())
                .name(organization.getName())
                .locationDescription(organization.getLocationDescription())
                .description(organization.getDescription())
                .overviewImage(toClientMediaItem(organization.getOverviewImage()))
                .logoImage(toClientMediaItem(organization.getLogoImage()))
                .detailImages(organization.getDetailImages().stream()
                        .map(super::toClientMediaItem)
                        .collect(Collectors.toList()))
                .url(organization.getUrl())
                .phone(organization.getPhone())
                .emailAddress(organization.getEmailAddress())
                .address(clientAddress)
                .facts(organization.getFactsJson())
                .persons(organization.getOrganizationPersons().stream()
                        .map(person -> ClientOrganizationPerson.builder()
                                .id(person.getId())
                                .profilePicture(toClientMediaItem(person.getProfilePicture()))
                                .name(person.getName())
                                .position(person.getPosition())
                                .category(person.getCategory())
                                .build())
                        .collect(Collectors.toList()))
                .tags(clientOrganizationTags)
                .geoAreaIds(geoAreaIds.stream()
                        .sorted()
                        .collect(Collectors.toList()))
                .build();
    }

    private List<ClientDocumentItem> toClientDocumentItems(Set<DocumentItem> attachments) {
        List<ClientDocumentItem> clientAttachments;
        if (CollectionUtils.isEmpty(attachments)) {
            clientAttachments = Collections.emptyList();
        } else {
            clientAttachments = toClientEntityList(attachments.stream()
                    .sorted(Comparator.comparing(DocumentItem::getTitle))
                    .collect(Collectors.toList()), this::toClientDocumentItem);
        }
        return clientAttachments;
    }

    //should be replaced by real address objects that then can be recycled
    private Address createAddressFromLocation(GPSLocation location) {
        final Address address = Address.builder()
                .name("Position")
                .gpsLocation(location)
                .build();
        addressRepository.save(address);
        return address;
    }

}
