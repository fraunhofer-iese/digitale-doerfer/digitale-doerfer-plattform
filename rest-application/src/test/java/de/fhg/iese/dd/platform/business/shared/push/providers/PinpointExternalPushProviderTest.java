/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.providers;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonCreateResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.APNSPush;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.FCMPush;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushMessageInvalidException;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.config.TestConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.enums.PushPlatformType;
import software.amazon.awssdk.services.pinpoint.PinpointClient;
import software.amazon.awssdk.services.pinpoint.model.ApplicationResponse;
import software.amazon.awssdk.services.pinpoint.model.ChannelType;
import software.amazon.awssdk.services.pinpoint.model.DeleteAppRequest;
import software.amazon.awssdk.services.pinpoint.model.EndpointRequest;
import software.amazon.awssdk.services.pinpoint.model.EndpointResponse;
import software.amazon.awssdk.services.pinpoint.model.EndpointUser;
import software.amazon.awssdk.services.pinpoint.model.GetAppRequest;
import software.amazon.awssdk.services.pinpoint.model.GetAppResponse;
import software.amazon.awssdk.services.pinpoint.model.GetAppsRequest;
import software.amazon.awssdk.services.pinpoint.model.GetUserEndpointsRequest;
import software.amazon.awssdk.services.pinpoint.model.NotFoundException;
import software.amazon.awssdk.services.pinpoint.model.UpdateEndpointRequest;

@ActiveProfiles({"test", "test-aws", "integration-test", "PinpointExternalPushProviderTest"})
@ExtendWith(AssumeIntegrationTestExtension.class)
public class PinpointExternalPushProviderTest extends BaseServiceTest {

    @Autowired
    private PinpointExternalPushProvider pushProvider;

    @Autowired
    private AWSConfig awsConfig;

    @Autowired
    private TestConfig testConfig;

    private String expectedPushApplicationName;
    private AppVariant appVariant;
    private IExternalPushProvider.ExternalPushAppConfiguration pushAppConfiguration;
    private Person person;
    protected PinpointClient pinpointClient;

    @Override
    public void createEntities() throws Exception {

        pinpointClient = PinpointClient.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();

        person = Person.builder()
                .id("test-person-id-" + UUID.randomUUID())
                .build();

        String testAppVariantIdentifier = "test-app-variant-identifier";
        appVariant = AppVariant.builder()
                .appVariantIdentifier(testAppVariantIdentifier)
                .pushAppId(null)
                .supportsAndroid(true)
                .build();
        expectedPushApplicationName = awsConfig.getPinpoint().getAppPrefix() + "." + testAppVariantIdentifier;

        pushAppConfiguration = IExternalPushProvider.ExternalPushAppConfiguration.builder()
                .fcmApiKey(testConfig.getPinpointTestFcmKey())
                .build();
    }

    @Override
    public void tearDown() throws Exception {

        //delete all "test-*" apps
        pinpointClient.getApps(GetAppsRequest.builder().build()).applicationsResponse().item().stream()
                .filter(applicationResponse -> applicationResponse.name().startsWith("test-"))
                .map(ApplicationResponse::id)
                .forEach(applicationId -> pinpointClient.deleteApp(
                        DeleteAppRequest.builder()
                                .applicationId(applicationId)
                                .build()));
    }

    @Test
    public void createOrUpdatePushApp_deletePushApp() {

        //create the app
        IExternalPushProvider.ExternalPushApp pushApp =
                pushProvider.createOrUpdatePushApp(appVariant, pushAppConfiguration);

        assertEquals(expectedPushApplicationName, pushApp.getPushAppName());

        //check that the app is properly configured at pinpoint
        GetAppResponse app = pinpointClient.getApp(GetAppRequest.builder()
                .applicationId(pushApp.getPushAppId())
                .build());

        assertEquals(expectedPushApplicationName, app.applicationResponse().name());
        assertThat(app.applicationResponse().tags()).containsExactlyInAnyOrderEntriesOf(awsConfig.getAWSTags());

        //update it
        IExternalPushProvider.ExternalPushApp pushAppUpdated =
                pushProvider.createOrUpdatePushApp(appVariant, pushAppConfiguration);

        assertEquals(pushApp.getPushAppId(), pushAppUpdated.getPushAppId());
        assertEquals(expectedPushApplicationName, pushAppUpdated.getPushAppName());

        GetAppResponse appUpdated = pinpointClient.getApp(GetAppRequest.builder()
                .applicationId(pushAppUpdated.getPushAppId())
                .build());

        assertEquals(expectedPushApplicationName, appUpdated.applicationResponse().name());
        assertThat(appUpdated.applicationResponse().tags()).containsExactlyInAnyOrderEntriesOf(
                awsConfig.getAWSTags());

        //now we delete it and also test the deletion
        appVariant.setPushAppId(pushApp.getPushAppId());

        pushProvider.deleteAppVariant(appVariant);

        try {
            //try to delete the app afterwards
            pinpointClient.getApp(GetAppRequest.builder()
                    .applicationId(pushAppUpdated.getPushAppId())
                    .build());
            fail("app is still existing on pinpoint");
        } catch (NotFoundException e) {
            //nothing to do
        }
    }

    @Test
    public void registerPushEndpoint_InvalidToken() {

        //create the app
        createPushApp();

        //register an invalid token, it will only get evaluated on sending
        String registrationToken = "reg-token";
        PushPlatformType platformType = PushPlatformType.GCM;

        pushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);

        List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(person.getId())
                        .build())
                .endpointsResponse()
                .item();

        assertEquals(1, userEndpoints.size());
        EndpointResponse endpoint = userEndpoints.get(0);
        assertEquals(registrationToken, endpoint.address());
        assertEquals(ChannelType.GCM, endpoint.channelType());

        // registering the same token again should not create a new endpoint
        pushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);

        List<EndpointResponse> userEndpoints2 = pinpointClient.getUserEndpoints(
                        GetUserEndpointsRequest.builder()
                                .applicationId(appVariant.getPushAppId())
                                .userId(person.getId())
                                .build())
                .endpointsResponse()
                .item();

        assertEquals(1, userEndpoints2.size());
        EndpointResponse endpoint2 = userEndpoints2.get(0);
        assertEquals(registrationToken, endpoint2.address());
        assertEquals(ChannelType.GCM, endpoint2.channelType());
    }

    @Test
    public void registerPushEndpoint_TokenLimit() {

        //create the app
        createPushApp();

        PushPlatformType platformType = PushPlatformType.GCM;

        //register the maximum amount of tokens
        String registrationTokenOld = "reg-token-old-";
        for (int i = 0; i < 10; i++) {
            createEndpoint(registrationTokenOld + i, platformType);
        }

        //register an invalid token, it will only get evaluated on sending
        String registrationTokenNew = "reg-token-new";
        pushProvider.registerPushEndpoint(registrationTokenNew, platformType, appVariant, person);

        List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(person.getId())
                        .build())
                .endpointsResponse()
                .item()
                .stream()
                .sorted(Comparator.comparing(EndpointResponse::creationDate))
                .collect(Collectors.toList());

        //one was deleted
        assertEquals(10, userEndpoints.size());
        EndpointResponse newestEndpoint = userEndpoints.get(9);
        assertEquals(registrationTokenNew, newestEndpoint.address());
        assertEquals(ChannelType.GCM, newestEndpoint.channelType());

        //registrationTokenOld + "0" should have been deleted
        EndpointResponse oldestEndpoint = userEndpoints.get(0);
        assertEquals(registrationTokenOld + "1", oldestEndpoint.address());
        assertEquals(ChannelType.GCM, oldestEndpoint.channelType());
    }

    @Test
    public void registerPushEndpoint_Duplicates() {

        //create the app
        createPushApp();

        PushPlatformType platformType = PushPlatformType.GCM;

        //duplicates of another token, they should be deleted, too
        // does not need to be the maximum amount of tokens
        String registrationTokenOther = "reg-token-other";
        for (int i = 0; i < 3; i++) {
            createEndpoint(registrationTokenOther, platformType);
        }
        //register some duplicates of the same token we want to create
        // does not need to be the maximum amount of tokens
        String registrationToken = "reg-token-new";
        for (int i = 0; i < 3; i++) {
            createEndpoint(registrationToken, platformType);
        }

        //register an invalid token, it will only get evaluated on sending
        pushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);

        List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(person.getId())
                        .build())
                .endpointsResponse()
                .item()
                .stream()
                .sorted(Comparator.comparing(EndpointResponse::creationDate))
                .collect(Collectors.toList());

        //all duplicates were deleted
        assertEquals(2, userEndpoints.size());
        //this is the other token
        EndpointResponse endpointOther = userEndpoints.get(0);
        assertEquals(registrationTokenOther, endpointOther.address());
        assertEquals(ChannelType.GCM, endpointOther.channelType());
        //this is the newly registered one
        EndpointResponse endpointNew = userEndpoints.get(1);
        assertEquals(registrationToken, endpointNew.address());
        assertEquals(ChannelType.GCM, endpointNew.channelType());

    }

    private void createEndpoint(String registrationToken, PushPlatformType platformType) {

        pinpointClient.updateEndpoint(UpdateEndpointRequest.builder()
                .applicationId(appVariant.getPushAppId())
                .endpointId(UUID.randomUUID().toString())
                .endpointRequest(EndpointRequest.builder()
                        .address(registrationToken)
                        .channelType(toChannelType(platformType))
                        .user(EndpointUser.builder()
                                .userId(person.getId())
                                .build())
                        .build())
                .build());
    }

    @Test
    public void sendMessageToPerson_Duplicates() throws Exception {

        //create the app
        createPushApp();

        //register an invalid token, it will only get evaluated on sending
        String registrationToken = "reg-token";
        PushPlatformType platformType = PushPlatformType.GCM;

        for (int i = 0; i < 3; i++) {
            createEndpoint(registrationToken, platformType);
        }
        final List<Future<?>> futures = new ArrayList<>();
        pushProvider.setBackgroundTaskFutureConsumer(futures::add);

        //send a push message to the person with the invalid + duplicate endpoint
        pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessage(), true);

        //we need to wait until all background tasks (the cleanup workers) are finished
        for (Future<?> future : futures) {
            future.get();
        }
        pushProvider.setBackgroundTaskFutureConsumer(f -> {
        });

        //check if the endpoints were deleted due to duplication and invalid
        try {
            List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                            .applicationId(appVariant.getPushAppId())
                            .userId(person.getId())
                            .build())
                    .endpointsResponse()
                    .item();

            //we can not assert that all are deleted, since the deletion of duplicates will leave one (invalid) endpoint alive
            assertThat(userEndpoints).hasSizeLessThanOrEqualTo(1);
        } catch (NotFoundException e) {
            log.info("user was not found, so all endpoints are gone, too");
        }

        //sending a message to a user without an endpoint should not throw an exception
        pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessage(), true);
    }

    @Test
    public void sendMessageToPerson_InvalidToken() throws Exception {

        //create the app
        createPushApp();

        //register an invalid token, it will only get evaluated on sending
        String registrationToken = "reg-token";
        PushPlatformType platformType = PushPlatformType.GCM;

        pushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);

        final List<Future<?>> futures = new ArrayList<>();
        pushProvider.setBackgroundTaskFutureConsumer(futures::add);

        //send a push message to the person with the invalid endpoint
        pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessage(), true);

        //we need to wait until all background tasks (the cleanup workers) are finished
        for (Future<?> future : futures) {
            future.get();
        }
        pushProvider.setBackgroundTaskFutureConsumer(f -> {
        });
        //check if the endpoint was deleted
        try {
            List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                            .applicationId(appVariant.getPushAppId())
                            .userId(person.getId())
                            .build())
                    .endpointsResponse()
                    .item();

            assertEquals(0, userEndpoints.size());
        } catch (NotFoundException e) {
            log.info("user was not found, so all endpoints are gone, too");
        }

        //sending a message to a user without an endpoint should not throw an exception
        pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessage(), true);
    }

    @Test
    public void sendMessageToPerson_UnknownPersonId() {

        //create the app
        createPushApp();

        person.setId(UUID.randomUUID().toString());

        //send a push message to a person that was not registered before
        pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessage(), true);
    }

    @Test
    public void sendMessageToPerson_TooBig() {

        //create the app
        createPushApp();

        //register an invalid token, it will only get evaluated on sending
        String registrationToken = "reg-token";
        PushPlatformType platformType = PushPlatformType.GCM;

        pushProvider.registerPushEndpoint(registrationToken, platformType, appVariant, person);

        try {
            //send a too big push message, it should be denied
            pushProvider.sendMessageToPerson(Collections.singleton(appVariant), person, createPushMessageTooBig(),
                    true);
            fail("Exception not thrown");
        } catch (PushMessageInvalidException e) {
            //the exception is expected
        }

        //check if no endpoint was deleted
        List<EndpointResponse> userEndpoints = pinpointClient.getUserEndpoints(GetUserEndpointsRequest.builder()
                        .applicationId(appVariant.getPushAppId())
                        .userId(person.getId())
                        .build())
                .endpointsResponse()
                .item();

        assertEquals(1, userEndpoints.size());
    }

    private void createPushApp() {
        IExternalPushProvider.ExternalPushApp pushApp =
                pushProvider.createOrUpdatePushApp(appVariant, pushAppConfiguration);

        appVariant.setPushAppId(pushApp.getPushAppId());
    }

    private PushMessage createPushMessage() throws PushMessageInvalidException {
        return createPushMessage(new ClientPersonCreateResponse());
    }

    private PushMessage createPushMessageTooBig() throws PushMessageInvalidException {
        return createPushMessage(ClientPersonCreateResponse.builder()
                .createdPerson(ClientPersonOwn.builder()
                        .firstName(StringUtils.repeat("x", 4096))
                        .build())
                .build());
    }

    private PushMessage createPushMessage(ClientBaseEvent event) throws PushMessageInvalidException {
        String message = "test-text";
        try {
            return PushMessage.builder()
                    .jsonMessageLoudApns(
                            defaultJsonWriter().writeValueAsString(new APNSPush(false, message, event, null)))
                    .jsonMessageSilentApns(
                            defaultJsonWriter().writeValueAsString(new APNSPush(true, null, event, null)))
                    .jsonMessageLoudFcm(defaultJsonWriter().writeValueAsString(new FCMPush(event, message)))
                    .jsonMessageSilentFcm(defaultJsonWriter().writeValueAsString(new FCMPush(event, null)))
                    .logMessage(toLogMessage(event))
                    .build();
        } catch (JsonProcessingException e) {
            throw new PushMessageInvalidException("The push message for event " + event + " was invalid.", e);
        }
    }

    private String toLogMessage(ClientBaseEvent event) {
        return StringUtils.remove(StringUtils.removeStart(
                event.getClass().getName(), "de.fhg.iese.dd.platform.api."), "clientevent.");
    }

    private ChannelType toChannelType(PushPlatformType pushPlatformType) {
        switch (pushPlatformType) {
            case GCM:
            case FCM:
                return ChannelType.GCM;
            case APNS_DEV:
            case APNS_DEV_ENTERPRISE:
                return ChannelType.APNS_SANDBOX;
            case APNS_PROD:
            case APNS_PROD_ENTERPRISE:
                return ChannelType.APNS;
            default:
                throw new IllegalStateException("Unexpected enum value PushPlatformType." + pushPlatformType);
        }
    }

}
