/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.address.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressListEntry;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;

@Transactional
public class AddressListControllerTest extends BaseServiceTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ParticipantsTestHelper th;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAddresses();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAddressListEntries() throws Exception{

        AddressListEntry addressListEntry1 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat1")
                                .street("street1")
                                .zip("11111")
                                .city("city1")
                                .build()),
                "address name 1"
        );
        addressListEntry1.setCreated(0);
        AddressListEntry addressListEntry2 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat2")
                                .street("street2")
                                .zip("22222")
                                .city("city2")
                                .build()),
                "address name 2"
        );
        addressListEntry1.setCreated(1);

        addressListEntry1 = th.addressListEntryRepository.save(addressListEntry1);
        addressListEntry2 = th.addressListEntryRepository.save(addressListEntry2);

        Person personWithAddress = th.personVgAdmin;
        personWithAddress.getAddresses().clear();
        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personWithAddress = personRepository.save(personWithAddress);

        mockMvc.perform(get("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(addressListEntry2.getId()))
                .andExpect(jsonPath("$[0].name").value("address name 2"))
                .andExpect(jsonPath("$[0].address.name").value("privat2"))
                .andExpect(jsonPath("$[0].address.street").value("street2"))
                .andExpect(jsonPath("$[0].address.zip").value("22222"))
                .andExpect(jsonPath("$[0].address.city").value("city2"))
                .andExpect(jsonPath("$[1].id").value(addressListEntry1.getId()))
                .andExpect(jsonPath("$[1].name").value("address name 1"))
                .andExpect(jsonPath("$[1].address.name").value("privat1"))
                .andExpect(jsonPath("$[1].address.street").value("street1"))
                .andExpect(jsonPath("$[1].address.zip").value("11111"))
                .andExpect(jsonPath("$[1].address.city").value("city1"))
                .andExpect(jsonPath("$[2]").doesNotExist());
    }

    @Test
    public void getDefaultAddressListEntry() throws Exception {

        Person personWithAddress = th.personIeseAdmin;
        //check if it has a default address
        AddressListEntry expectedAddressListEntry = th.personIeseAdmin.getAddresses().iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME,
                expectedAddressListEntry.getName(), "Test setup wrong, no default address");
        assertEquals(1, th.personIeseAdmin.getAddresses().size(), "Test setup wrong, too many addresses");

        //add other addresses
        final AddressListEntry addressListEntry1 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat1")
                                        .street("street1")
                                        .zip("11111")
                                        .city("city1")
                                        .build()),
                        "address name 1"
                ));
        final AddressListEntry addressListEntry2 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat2")
                                        .street("street2")
                                        .zip("22222")
                                        .city("city2")
                                        .build()),
                        "address name 2"
                ));

        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personRepository.save(personWithAddress);

        mockMvc.perform(get("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedAddressListEntry.getId()))
                .andExpect(jsonPath("$.name").value(expectedAddressListEntry.getName()))
                .andExpect(jsonPath("$.address.name").value(expectedAddressListEntry.getAddress().getName()))
                .andExpect(jsonPath("$.address.street").value(expectedAddressListEntry.getAddress().getStreet()))
                .andExpect(jsonPath("$.address.zip").value(expectedAddressListEntry.getAddress().getZip()))
                .andExpect(jsonPath("$.address.city").value(expectedAddressListEntry.getAddress().getCity()));
    }

    @Test
    public void getDefaultAddressListEntryNotExisting() throws Exception{

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        mockMvc.perform(get("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());

    }

    @Test
    public void deleteDefaultAddressListEntry() throws Exception {

        Person personWithAddress = th.personIeseAdmin;
        //check if it has a default address
        AddressListEntry expectedAddressListEntry = th.personIeseAdmin.getAddresses().iterator().next();
        assertEquals(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME,
                expectedAddressListEntry.getName(), "Test setup wrong, no default address");
        assertEquals(1, th.personIeseAdmin.getAddresses().size(), "Test setup wrong, too many addresses");

        //add other addresses
        final AddressListEntry addressListEntry1 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat1")
                                        .street("street1")
                                        .zip("11111")
                                        .city("city1")
                                        .build()),
                        "address name 1"
                ));
        final AddressListEntry addressListEntry2 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat2")
                                        .street("street2")
                                        .zip("22222")
                                        .city("city2")
                                        .build()),
                        "address name 2"
                ));

        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personWithAddress = personRepository.save(personWithAddress);

        mockMvc.perform(delete("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType))
                .andExpect(status().isOk());

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check that the address is deleted
        assertEquals(2, updatedPerson.getAddresses().size());

        assertFalse(updatedPerson.getAddresses().stream()
                .anyMatch(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())), "Default address not deleted");
    }

    @Test
    public void deleteDefaultAddressListEntryNotExisting() throws Exception{

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        mockMvc.perform(get("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void getAddressListEntry() throws Exception{

        AddressListEntry addressListEntry1 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat1")
                                .street("street1")
                                .zip("11111")
                                .city("city1")
                                .build()),
                "address name 1"
        );
        addressListEntry1.setCreated(0);
        AddressListEntry addressListEntry2 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat2")
                                .street("street2")
                                .zip("22222")
                                .city("city2")
                                .build()),
                "address name 2"
        );
        addressListEntry1.setCreated(1);

        addressListEntry1 = th.addressListEntryRepository.save(addressListEntry1);
        addressListEntry2 = th.addressListEntryRepository.save(addressListEntry2);

        Person personWithAddress = th.personVgAdmin;
        personWithAddress.getAddresses().clear();
        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personWithAddress = personRepository.save(personWithAddress);

        mockMvc.perform(get("/person/address/" + addressListEntry1.getId())
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(addressListEntry1.getId()))
                .andExpect(jsonPath("$.name").value("address name 1"))
                .andExpect(jsonPath("$.address.name").value("privat1"))
                .andExpect(jsonPath("$.address.street").value("street1"))
                .andExpect(jsonPath("$.address.zip").value("11111"))
                .andExpect(jsonPath("$.address.city").value("city1"));
    }

    @Test
    public void getAddressListEntryInvalidId() throws Exception{

        AddressListEntry addressListEntry1 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat1")
                                .street("street1")
                                .zip("11111")
                                .city("city1")
                                .build()),
                "address name 1"
        );
        addressListEntry1.setCreated(0);
        AddressListEntry addressListEntry2 = new AddressListEntry(
                th.addressRepository.saveAndFlush(
                        Address.builder()
                                .name("privat2")
                                .street("street2")
                                .zip("22222")
                                .city("city2")
                                .build()),
                "address name 2"
        );
        addressListEntry1.setCreated(1);

        addressListEntry1 = th.addressListEntryRepository.save(addressListEntry1);
        addressListEntry2 = th.addressListEntryRepository.save(addressListEntry2);

        Person personWithAddress = th.personVgAdmin;
        personWithAddress.getAddresses().clear();
        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personWithAddress = personRepository.save(personWithAddress);

        mockMvc.perform(get("/person/address/4711")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAddressListEntryOtherPerson() throws Exception{

        final AddressListEntry addressListEntry1 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat1")
                                        .street("street1")
                                        .zip("11111")
                                        .city("city1")
                                        .build()),
                        "address name 1"
                ));
        final AddressListEntry addressListEntry2 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat2")
                                        .street("street2")
                                        .zip("22222")
                                        .city("city2")
                                        .build()),
                        "address name 2"
                ));

        Person personWithAddress = th.personVgAdmin;
        personWithAddress.getAddresses().clear();
        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personRepository.save(personWithAddress);

        Person otherPersonWithAddress = th.personIeseAdmin;

        mockMvc.perform(get("/person/address/" + addressListEntry1.getId())
                .headers(authHeadersFor(otherPersonWithAddress))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addAddressListEntry() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithAddress = th.personIeseAdmin;

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("address name 1"))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city)).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check the address that is added
        assertEquals(2, updatedPerson.getAddresses().size());

        AddressListEntry previousAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();
        assertNotNull(previousAddressListEntry);

        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> "address name 1".equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void addAddressListEntryInvalidParameters() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithAddress = th.personIeseAdmin;

        // entry name empty

        ClientAddressListEntry addressListEntry = new ClientAddressListEntry(
                "",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // entry name null

        addressListEntry = new ClientAddressListEntry(
                null,
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // name empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name("")
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // name null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(null)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // street empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street("")
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // street null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(null)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // zip empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip("")
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // zip null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(null)
                        .city(city).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // city empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city("")
                        .build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // city null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(null).build());

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());
    }

    @Test
    public void addDefaultAddressListEntry() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        MvcResult mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city)).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithoutAddress.getId()).get();

        //check the address that is added
        assertEquals(1, updatedPerson.getAddresses().size());
        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void addDefaultAddressListEntryEmptyName() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        MvcResult mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city)).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithoutAddress.getId()).get();

        //check the address that is added
        assertEquals(1, updatedPerson.getAddresses().size());
        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void addDefaultAddressListEntryNullName() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                null,
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        MvcResult mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city)).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithoutAddress.getId()).get();

        //check the address that is added
        assertEquals(1, updatedPerson.getAddresses().size());
        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void addDefaultAddressListEntryUpdate() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        final String name2 = "NewName";
        final String city2 = "NewCity";
        final String street2 = "NewStreet";
        final String zip2 = "NewZip";

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        ClientAddressListEntry addressListEntry2 = new ClientAddressListEntry(
                "address name 2",
                ClientAddress.builder()
                        .name(name2)
                        .street(street2)
                        .zip(zip2)
                        .city(city2).build());

        //set the default address
        MvcResult mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                .andExpect(jsonPath("$.address.name").value(name))
                .andExpect(jsonPath("$.address.street").value(street))
                .andExpect(jsonPath("$.address.zip").value(zip))
                .andExpect(jsonPath("$.address.city").value(city)).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        //update the default address
        mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry2)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(personJsonResponse.getId()))
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                .andExpect(jsonPath("$.address.name").value(name2))
                .andExpect(jsonPath("$.address.street").value(street2))
                .andExpect(jsonPath("$.address.zip").value(zip2))
                .andExpect(jsonPath("$.address.city").value(city2)).andReturn();
        ClientAddressListEntry personJsonResponse2 = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithoutAddress.getId()).get();

        //check the address that is updated
        assertEquals(1, updatedPerson.getAddresses().size());
        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse2.getId(), newAddressListEntry.getId());
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(name2, address.getName());
        assertEquals(street2, address.getStreet());
        assertEquals(zip2, address.getZip());
        assertEquals(city2, address.getCity());
    }

    @Test
    public void addDefaultAddressListEntryInvalidParameters() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithAddress = th.personIeseAdmin;

        // name empty

        ClientAddressListEntry addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name("")
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // name null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(null)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // street empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street("")
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // street null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(null)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // zip empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip("")
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // zip null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(null)
                        .city(city).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // city empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city("")
                        .build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        // city null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(null).build());

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());
    }

    @Test
    public void changeAddressListEntry() throws Exception{

        final String addressEntryName = "address entry name 1";
        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        final String addressEntryName2 = "address entry name 2";
        final String name2 = "NewName";
        final String city2 = "NewCity";
        final String street2 = "NewStreet";
        final String zip2 = "NewZip";

        Person personWithAddress = th.personIeseAdmin;

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                addressEntryName,
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        ClientAddressListEntry addressListEntry2 = new ClientAddressListEntry(
                addressEntryName2,
                ClientAddress.builder()
                        .name(name2)
                        .street(street2)
                        .zip(zip2)
                        .city(city2).build());

        //update the address by using the same id
        mockMvc.perform(post("/person/address/" + personJsonResponse.getId())
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(personJsonResponse.getId()))
                .andExpect(jsonPath("$.name").value(addressEntryName2))
                .andExpect(jsonPath("$.address.name").value(name2))
                .andExpect(jsonPath("$.address.street").value(street2))
                .andExpect(jsonPath("$.address.zip").value(zip2))
                .andExpect(jsonPath("$.address.city").value(city2));

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check the address that is updated
        assertEquals(2, updatedPerson.getAddresses().size());

        AddressListEntry previousAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();
        assertNotNull(previousAddressListEntry);

        AddressListEntry updatedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> addressEntryName2.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), updatedAddressListEntry.getId());
        Address address = th.addressRepository.findById(updatedAddressListEntry.getAddress().getId()).get();
        assertEquals(name2, address.getName());
        assertEquals(street2, address.getStreet());
        assertEquals(zip2, address.getZip());
        assertEquals(city2, address.getCity());
    }

    @Test
    public void changeAddressListEntryOtherPerson() throws Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        final String name2 = "NewName";
        final String city2 = "NewCity";
        final String street2 = "NewStreet";
        final String zip2 = "NewZip";

        Person personWithAddress = th.personIeseAdmin;
        Person otherPerson = th.personVgAdmin;

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        ClientAddressListEntry addressListEntry2 = new ClientAddressListEntry(
                "address name 2",
                ClientAddress.builder()
                        .name(name2)
                        .street(street2)
                        .zip(zip2)
                        .city(city2).build());

        //update the address by using another person
        mockMvc.perform(post("/person/address/" + personJsonResponse.getId())
                .headers(authHeadersFor(otherPerson))
                .contentType(contentType)
                .content(json(addressListEntry2)))
                .andExpect(status().isNotFound());

        //check that the address is not updated and the other one not changed
        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        assertEquals(2, updatedPerson.getAddresses().size());

        AddressListEntry previousAddressListEntry = personWithAddress.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertNotNull(previousAddressListEntry);

        AddressListEntry notUpdatedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> "address name 1".equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), notUpdatedAddressListEntry.getId());
        Address address = th.addressRepository.findById(notUpdatedAddressListEntry.getAddress().getId()).get();
        assertEquals(name, address.getName());
        assertEquals(street, address.getStreet());
        assertEquals(zip, address.getZip());
        assertEquals(city, address.getCity());
    }

    @Test
    public void changeAddressListEntryInvalidParameters() throws Exception{

        final String entryName1 = "address name 1";
        final String name1 = "Bei mir zu Hause";
        final String street1 = "Zur betrunkenen Traube 1";
        final String zip1 = "12345";
        final String city1 = "Kaiserslautern";

        final String name = "NewName";
        final String city = "NewCity";
        final String street = "NewStreet";
        final String zip = "NewZip";

        Person personWithAddress = th.personIeseAdmin;
        personWithAddress.getAddresses().clear();

        personWithAddress = personRepository.save(personWithAddress);

        ClientAddressListEntry existingAddressListEntry = new ClientAddressListEntry(
                entryName1,
                ClientAddress.builder()
                        .name(name1)
                        .street(street1)
                        .zip(zip1)
                        .city(city1).build());

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(existingAddressListEntry)))
                .andExpect(status().isCreated()).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        final String existingAddressListEntryId = personJsonResponse.getId();

        // entry name empty

        ClientAddressListEntry addressListEntry = new ClientAddressListEntry(
                "",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        AddressListEntry unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName()))
                .findFirst()
                .get();

        Address address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // entry name null

        addressListEntry = new ClientAddressListEntry(
                null,
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // name empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name("")
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // name null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(null)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("name", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // street empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street("")
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // street null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(null)
                        .zip(zip)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("street", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // zip empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip("")
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // zip null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(null)
                        .city(city).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("zip", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // city empty

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city("")
                        .build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());

        // city null

        addressListEntry = new ClientAddressListEntry(
                "entry name",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(null).build());

        mockMvc.perform(post("/person/address/" + existingAddressListEntryId)
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry)))
                .andExpect(isException("city", ClientExceptionType.ADDRESS_INVALID));

        updatedPerson = personRepository.findById(personWithAddress.getId()).get();
        assertEquals(1, updatedPerson.getAddresses().size());

        unchangedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> entryName1.equals(a.getName())).findFirst().get();

        address = th.addressRepository.findById(unchangedAddressListEntry.getAddress().getId()).get();
        assertEquals(name1, address.getName());
        assertEquals(street1, address.getStreet());
        assertEquals(zip1, address.getZip());
        assertEquals(city1, address.getCity());
    }

    @Test
    public void deleteAddressListEntry() throws  Exception{

        final String name = "Bei mir zu Hause";
        final String street = "Zur betrunkenen Traube 1";
        final String zip = "12345";
        final String city = "Kaiserslautern";

        Person personWithAddress = th.personIeseAdmin;

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                ClientAddress.builder()
                        .name(name)
                        .street(street)
                        .zip(zip)
                        .city(city).build());

        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        mockMvc.perform(delete("/person/address/" + personJsonResponse.getId())
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType))
                .andExpect(status().isOk());

        //check the address that is deleted
        assertEquals(1, personWithAddress.getAddresses().size());

        //check that the other is still existing
        AddressListEntry previousAddressListEntry = personWithAddress.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertNotNull(previousAddressListEntry);
    }

    @Test
    public void deleteAddressListEntryOtherPerson() throws Exception{

        final AddressListEntry addressListEntry1 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat1")
                                        .street("street1")
                                        .zip("11111")
                                        .city("city1")
                                        .build()),
                        "address name 1"
                ));
        final AddressListEntry addressListEntry2 = th.addressListEntryRepository.save(
                new AddressListEntry(
                        th.addressRepository.saveAndFlush(
                                Address.builder()
                                        .name("privat2")
                                        .street("street2")
                                        .zip("22222")
                                        .city("city2")
                                        .build()),
                        "address name 2"
                ));

        Person personWithAddress = th.personVgAdmin;
        personWithAddress.getAddresses().clear();
        personWithAddress.getAddresses().add(addressListEntry1);
        personWithAddress.getAddresses().add(addressListEntry2);

        personWithAddress = personRepository.save(personWithAddress);

        Person otherPersonWithAddress = th.personIeseAdmin;

        mockMvc.perform(delete("/person/address/" + addressListEntry1.getId())
                .headers(authHeadersFor(otherPersonWithAddress))
                .contentType(contentType))
                .andExpect(status().isNotFound());

        //check that the address is not deleted
        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check the address that is added
        assertEquals(2, updatedPerson.getAddresses().size());
        AddressListEntry notDeletedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> addressListEntry1.getId().equals(a.getId())).findFirst().get();

        assertNotNull(notDeletedAddressListEntry);
    }

    @Test
    public void addAddressListEntryBadAddressFormat() throws Exception {

        Person personWithAddress = th.personIeseAdmin;

        //create address list entry with bad format address
        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                th.unnormalizedAddress1);

        //checking address formatting and corrections

        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("address name 1"))
                //check that response contains properly formatted address
                .andExpect(jsonPath("$.address.name").value(th.normalizedAddress1.getName()))
                .andExpect(jsonPath("$.address.street").value(th.normalizedAddress1.getStreet()))
                .andExpect(jsonPath("$.address.zip").value(th.normalizedAddress1.getZip()))
                .andExpect(jsonPath("$.address.city").value(th.normalizedAddress1.getCity())).andReturn();

        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check the address that is added
        assertEquals(2, updatedPerson.getAddresses().size());

        AddressListEntry previousAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();
        assertNotNull(previousAddressListEntry);

        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> "address name 1".equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        //check that repository returns properly formatted address
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(th.normalizedAddress1.getName(), address.getName());
        assertEquals(th.normalizedAddress1.getStreet(), address.getStreet());
        assertEquals(th.normalizedAddress1.getZip(), address.getZip());
        assertEquals(th.normalizedAddress1.getCity(), address.getCity());
    }

    @Test
    public void addAddressListEntryEmptyName() throws Exception{

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setName("");

        ClientAddressListEntry addressListEntryEmptyName = new ClientAddressListEntry(
                "address name 2",
                sampleAddress);

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyName)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addAddressListEntryEmptyStreet() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setStreet("");

        ClientAddressListEntry addressListEntryEmptyStreet = new ClientAddressListEntry(
                "address name 3",
                sampleAddress);

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyStreet)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addAddressListEntryEmptyZip() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setZip("");

        ClientAddressListEntry addressListEntryEmptyZip = new ClientAddressListEntry(
                "address name 4",
                sampleAddress);

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyZip)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addAddressListEntryEmptyCity() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setCity("");

        ClientAddressListEntry addressListEntryEmptyCity = new ClientAddressListEntry(
                "address name 5",
                sampleAddress);

        mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyCity)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addDefaultAddressListEntryBadAddressFormat() throws Exception{

        Person personWithoutAddress = th.personIeseAdmin;
        personWithoutAddress.getAddresses().clear();

        personWithoutAddress = personRepository.save(personWithoutAddress);

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "address name 1",
                th.unnormalizedAddress1);

        MvcResult mvcResult = mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(personWithoutAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME))
                //check that response contains properly formatted address
                .andExpect(jsonPath("$.address.name").value(th.normalizedAddress1.getName()))
                .andExpect(jsonPath("$.address.street").value(th.normalizedAddress1.getStreet()))
                .andExpect(jsonPath("$.address.zip").value(th.normalizedAddress1.getZip()))
                .andExpect(jsonPath("$.address.city").value(th.normalizedAddress1.getCity())).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        Person updatedPerson = personRepository.findById(personWithoutAddress.getId()).get();

        //check the address that is added
        assertEquals(1, updatedPerson.getAddresses().size());
        AddressListEntry newAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), newAddressListEntry.getId());
        //check that repository returns properly formatted address
        Address address = th.addressRepository.findById(newAddressListEntry.getAddress().getId()).get();
        assertEquals(th.normalizedAddress1.getName(), address.getName());
        assertEquals(th.normalizedAddress1.getStreet(), address.getStreet());
        assertEquals(th.normalizedAddress1.getZip(), address.getZip());
        assertEquals(th.normalizedAddress1.getCity(), address.getCity());
    }

    @Test
    public void addDefaultAddressListEntryEmptyAddressName() throws Exception{

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setName("");

        ClientAddressListEntry addressListEntryEmptyName = new ClientAddressListEntry(
                "address name 2",
                sampleAddress);

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyName)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addDefaultAddressListEntryEmptyStreet() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setStreet("");

        ClientAddressListEntry addressListEntryEmptyStreet = new ClientAddressListEntry(
                "address name 3",
                sampleAddress);

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyStreet)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addDefaultAddressListEntryEmptyZip() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setZip("");

        ClientAddressListEntry addressListEntryEmptyZip = new ClientAddressListEntry(
                "address name 4",
                sampleAddress);

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyZip)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void addDefaultAddressListEntryEmptyCity() throws Exception {

        ClientAddress sampleAddress = th.normalizedAddress1;
        sampleAddress.setCity("");

        ClientAddressListEntry addressListEntryEmptyCity = new ClientAddressListEntry(
                "address name 5",
                sampleAddress);

        mockMvc.perform(post("/person/address/default")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntryEmptyCity)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void changeAddressListEntryBadAddressFormat() throws Exception{

        final String addressEntryName = "address entry name 1";
        final String addressEntryName2 = "address entry name 2";

        Person personWithAddress = th.personIeseAdmin;

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                addressEntryName,
                th.normalizedAddress1);

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();
        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        ClientAddressListEntry addressListEntry2 = new ClientAddressListEntry(
                addressEntryName2,
                th.unnormalizedAddress1);

        //update the address by using the same id
        mockMvc.perform(post("/person/address/" + personJsonResponse.getId())
                .headers(authHeadersFor(personWithAddress))
                .contentType(contentType)
                .content(json(addressListEntry2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(personJsonResponse.getId()))
                .andExpect(jsonPath("$.name").value(addressEntryName2))
                //check that response contains properly formatted address
                .andExpect(jsonPath("$.address.name").value(th.normalizedAddress1.getName()))
                .andExpect(jsonPath("$.address.street").value(th.normalizedAddress1.getStreet()))
                .andExpect(jsonPath("$.address.zip").value(th.normalizedAddress1.getZip()))
                .andExpect(jsonPath("$.address.city").value(th.normalizedAddress1.getCity()));

        Person updatedPerson = personRepository.findById(personWithAddress.getId()).get();

        //check the address that is updated
        assertEquals(2, updatedPerson.getAddresses().size());

        AddressListEntry previousAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME.equals(a.getName())).findFirst().get();
        assertNotNull(previousAddressListEntry);

        AddressListEntry updatedAddressListEntry = updatedPerson.getAddresses().stream()
                .filter(a -> addressEntryName2.equals(a.getName())).findFirst().get();

        assertEquals(personJsonResponse.getId(), updatedAddressListEntry.getId());
        //check that repository returns properly formatted address
        Address address = th.addressRepository.findById(updatedAddressListEntry.getAddress().getId()).get();
        assertEquals(th.normalizedAddress1.getName(), address.getName());
        assertEquals(th.normalizedAddress1.getStreet(), address.getStreet());
        assertEquals(th.normalizedAddress1.getZip(), address.getZip());
        assertEquals(th.normalizedAddress1.getCity(), address.getCity());
    }

    @Test
    public void changeAddressListEntryEmptyAddressName() throws Exception{

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "Address name 1",
                th.normalizedAddress1);

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();

        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        String url = "/person/address/"+personJsonResponse.getId();

        //addresses with one component empty
        addressListEntry1.getAddress().setName("");

        //attempt to update address with empty component
        mockMvc.perform(post(url)
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void changeAddressListEntryEmptyStreet() throws Exception {

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "Address name 1",
                th.normalizedAddress1);

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();

        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        String url = "/person/address/"+personJsonResponse.getId();

        addressListEntry1.getAddress().setStreet("");

        //attempt to update address with empty component
        mockMvc.perform(post(url)
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void changeAddressListEntryEmptyZip() throws Exception {
        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "Address name 1",
                th.normalizedAddress1);

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();

        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        String url = "/person/address/"+personJsonResponse.getId();

        addressListEntry1.getAddress().setZip("");

        //attempt to update address with empty component
        mockMvc.perform(post(url)
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

    @Test
    public void changeAddressListEntryEmptyCity() throws Exception {

        ClientAddressListEntry addressListEntry1 = new ClientAddressListEntry(
                "Address name 1",
                th.normalizedAddress1);

        //add the address
        MvcResult mvcResult = mockMvc.perform(post("/person/address")
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(status().isCreated()).andReturn();

        ClientAddressListEntry personJsonResponse = toObject(mvcResult.getResponse(), ClientAddressListEntry.class);

        String url = "/person/address/"+personJsonResponse.getId();

        addressListEntry1.getAddress().setCity("");

        //attempt to update address with empty component
        mockMvc.perform(post(url)
                .headers(authHeadersFor(th.personIeseAdmin))
                .contentType(contentType)
                .content(json(addressListEntry1)))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));
    }

}
