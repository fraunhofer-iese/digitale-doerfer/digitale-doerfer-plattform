/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.seeking.ClientSeekingChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.seeking.ClientSeekingCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Seeking;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostEventControllerSeekingTest extends BasePostEventControllerTest<Seeking> {

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return new ClientSeekingChangeRequest();
    }

    @Override
    protected Seeking getTestPost1() {
        return th.seeking1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personLaraSchaefer;
    }

    @Override
    protected Seeking getTestPost2() {
        return th.seeking4;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.gossip1withImages;
    }

    @Test
    public void seekingCreateRequest_VerifyingPushMessage() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich brauche am Samstag einen Biergartentisch. Kann jemmand mir einen ausleihen?";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long date = System.currentTimeMillis();

        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .date(date)
                .createdLocation(createdLocation)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation tradingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the seeking
        mockMvc.perform(get("/grapevine/post/" + tradingCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.seeking.id").value(tradingCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.seeking.id").value(tradingCreateConfirmation.getPost().getSeeking().getId()))
                .andExpect(jsonPath("$.seeking.commentsAllowed").value(true))
                .andExpect(jsonPath("$.seeking.text").value(text))
                .andExpect(jsonPath("$.seeking.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.seeking.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.seeking.date").value(date))
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.id").value(th.tradingCategoryMisc.getId()))
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.displayName").value(
                        th.tradingCategoryMisc.getDisplayName()))
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.shortName").value(
                        th.tradingCategoryMisc.getShortName()))
                .andExpect(jsonPath("$.seeking.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the seeking is also added to the repository
        Seeking addedSeeking = (Seeking) th.postRepository.findById(tradingCreateConfirmation.getPost().getId())
                .get();

        assertEquals(postCreator, addedSeeking.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSeeking.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSeeking.getCreatedLocation().getLongitude());
        assertEquals(text, addedSeeking.getText());
        assertEquals(th.tradingCategoryMisc, addedSeeking.getTradingCategory());
        assertEquals(postCreator.getTenant(), addedSeeking.getTenant());
        assertTrue(addedSeeking.getImages() == null || addedSeeking.getImages().isEmpty());
        assertThat(addedSeeking.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertFalse(addedSeeking.isHiddenForOtherHomeAreas());

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(
                        addedSeeking.getCreator(),
                        addedSeeking.getGeoAreas(),
                        false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_SEEKING_CREATED);

        assertEquals(tradingCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(tradingCreateConfirmation.getPostId(), pushedEvent.getPost().getSeeking().getId());
        //check that the last name is cut
        assertEquals(postCreator.getFirstName(), pushedEvent.getPost().getSeeking().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(postCreator.getLastName()),
                pushedEvent.getPost().getSeeking().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void seekingCreateRequest_ContentCreationRestricted() throws Exception {

        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text("text")
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .date(System.currentTimeMillis())
                .createdLocation(th.pointClosestToTenant1)
                .build();

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/event/seekingCreateRequest")
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)));
    }

    @Test
    public void seekingCreateRequest_DefaultTradingCategory() throws Exception {

        Person person = th.personSusanneChristmann;
        String text = "Ich brauche am Samstag einen Biergartentisch. Kann jemmand mir einen ausleihen?";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long date = System.currentTimeMillis();

        //no seeking category set
        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .date(date)
                .createdLocation(createdLocation)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation tradingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the seeking
        mockMvc.perform(get("/grapevine/post/" + tradingCreateConfirmation.getPost().getSeeking().getId())
                .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.seeking.id").value(tradingCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.seeking.text").value(text))
                .andExpect(jsonPath("$.seeking.creator.id").value(person.getId()))
                .andExpect(jsonPath("$.seeking.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.seeking.date").value(date))
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.id").value(th.tradingCategoryMisc.getId()))
                .andExpect(jsonPath("$.seeking.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the seeking is also added to the repository
        Seeking addedSeeking = (Seeking) th.postRepository.findById(tradingCreateConfirmation.getPost().getId())
                .get();

        assertEquals(person, addedSeeking.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSeeking.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSeeking.getCreatedLocation().getLongitude());
        assertEquals(text, addedSeeking.getText());
        assertEquals(th.tradingCategoryMisc, addedSeeking.getTradingCategory());
        assertEquals(person.getTenant(), addedSeeking.getTenant());
        assertTrue(addedSeeking.getImages() == null || addedSeeking.getImages().isEmpty());
        assertThat(addedSeeking.getGeoAreas()).containsOnly(person.getHomeArea());
        assertFalse(addedSeeking.isHiddenForOtherHomeAreas());
    }

    @Test
    public void seekingCreateRequest_WithImagesAndDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich brauche eine Babysitterin für Samstag";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long date = 1521302400000L;
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images and documents
        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                //.customLocation(customLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .tradingCategoryId(th.tradingCategoryService.getId())
                .date(date)
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation seekingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(seekingCreateConfirmation.getPost().getSeeking().getText()).isEqualTo(text);
        assertThat(seekingCreateConfirmation.getPost().getSeeking().getImages()).hasSize(3);
        assertThat(seekingCreateConfirmation.getPost().getSeeking().getAttachments()).hasSize(3);

        // Get the seeking
        //check new entity uses images and documents
        String postId = seekingCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/" + postId)
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.seeking.id").value(postId))
                .andExpect(jsonPath("$.seeking.text").value(text))
                .andExpect(jsonPath("$.seeking.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.seeking.images", hasSize(3)))
                .andExpect(jsonPath("$.seeking.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.seeking.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.seeking.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.seeking.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.seeking.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.seeking.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.seeking.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.id").value(th.tradingCategoryService.getId()))
                .andExpect(jsonPath("$.seeking.date").value(String.valueOf(date)))
                .andExpect(jsonPath("$.seeking.hiddenForOtherHomeAreas").value(true));

        // Check whether the seeking is also added to the repository
        Seeking addedSeeking = (Seeking) th.postRepository.findById(postId).get();

        assertEquals(postCreator, addedSeeking.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSeeking.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSeeking.getCreatedLocation().getLongitude());
        assertEquals(text, addedSeeking.getText());
        assertEquals(postCreator.getTenant(), addedSeeking.getTenant());
        assertEquals(3, addedSeeking.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedSeeking.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedSeeking.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedSeeking.getImages().get(2));
        assertEquals(3, addedSeeking.getAttachments().size());
        assertThat(addedSeeking.getAttachments()).hasSize(3);
        assertThat(addedSeeking.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(addedSeeking.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedSeeking.isHiddenForOtherHomeAreas());

        //check used temp images and documents are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        //check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));
    }

    @Test
    public void seekingCreateRequest_InvalidImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "a problem";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id
        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .tradingCategoryId(th.tradingCategoryService.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .build();

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariant))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void seekingCreateRequest_InvalidDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "a new problem";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempDocumentItemId = "invalid id";

        //create request using temp documents and invalid id
        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .tradingCategoryId(th.tradingCategoryService.getId())
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidTempDocumentItemId))
                .build();

        //check temp documents not found exception
        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("[" + invalidTempDocumentItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    @Test
    public void seekingCreateRequest_ExceedingMaxNumImages() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "sea king 🌊 with too many images";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, appVariant))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void seekingCreateRequest_ExceedingMaxNumDocuments() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "sea king 🌊 with too many documents";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, appVariant))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void seekingCreateRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/seekingCreateRequest")
                        .contentType(contentType)
                        .content(json(ClientSeekingCreateRequest.builder()
                                .text("Ich brauche am Samstag einen Biergartentisch. Kann jemmand mir einen ausleihen?")
                                .tradingCategoryId(th.tradingCategoryMisc.getId())
                                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                                .build())),
                th.appVariant4AllTenants,
                th.personLaraSchaefer);
    }

    @Test
    public void seekingCreateRequest_ExceedingMaxNumChars() throws Exception {

        Person person = th.personAnnikaSchneider;
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        ClientSeekingCreateRequest tradingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .createdLocation(createdLocation)
                .build();

        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                .headers(authHeadersFor(person, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(tradingCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void seekingCreateRequest_MissingAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        String text = "seeking with missing attributes";

        // missing text
        ClientSeekingCreateRequest seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .tradingCategoryId(th.tradingCategoryComputer.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing category -- the category is set to GENERAL
        seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientPostCreateConfirmation tradingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the seeking
        mockMvc.perform(get("/grapevine/post/" + tradingCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.seeking.id").value(tradingCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.seeking.text").value(text))
                .andExpect(jsonPath("$.seeking.tradingCategoryEntity.id").value(th.tradingCategoryMisc.getId()));

        // missing tenant and homeArea
        person.setHomeArea(null);
        person = th.personRepository.save(person);
        seekingCreateRequest = ClientSeekingCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryComputer.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/seekingCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));
    }

    @Test
    public void seekingChangeRequest_VerifyingPushMessage() throws Exception {

        Person person = th.personSusanneChristmann;
        Seeking seekingToBeChanged = th.seeking1;

        TradingCategory newSeekingCategory = th.tradingCategoryTools;

        ClientSeekingChangeRequest seekingRequest = ClientSeekingChangeRequest.builder()
                .postId(seekingToBeChanged.getId())
                .tradingCategoryId(newSeekingCategory.getId())
                .build();

        MvcResult tradingChangeResult = mockMvc.perform(post("/grapevine/event/seekingChangeRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(seekingRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(seekingRequest.getPostId()))
                .andExpect(jsonPath("$.post.seeking.id").value(seekingRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation tradingChangeConfirmation = toObject(tradingChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the seeking is also updated in the repository
        Seeking updatedSeeking = (Seeking) th.postRepository.findById(tradingChangeConfirmation.getPostId())
                .get();

        assertEquals(newSeekingCategory, updatedSeeking.getTradingCategory());

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(seekingToBeChanged.getGeoAreas(),
                        seekingToBeChanged.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(tradingChangeConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(tradingChangeConfirmation.getPostId(), pushedEvent.getPost().getSeeking().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void seekingChangeRequest_NotOwner() throws Exception {

        Person person = th.personSebastianBauer;
        Seeking seekingToBeChanged = th.seeking1;

        TradingStatus newSeekingStatus = TradingStatus.DONE;

        ClientSeekingChangeRequest seekingChangeRequest = ClientSeekingChangeRequest.builder()
                .postId(seekingToBeChanged.getId())
                .tradingStatus(newSeekingStatus)
                .build();

        mockMvc.perform(post("/grapevine/event/seekingChangeRequest")
                .headers(authHeadersFor(person, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(seekingChangeRequest)))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));
    }

}
