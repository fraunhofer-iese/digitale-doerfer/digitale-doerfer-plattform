/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

public class OrphanImageFilesRemoverAdminTaskTest extends BaseServiceTest {

    @Autowired
    private IMediaItemService mediaItemService;

    @Autowired
    private MediaConfig mediaConfig;

    @Autowired
    private TestFileStorage fileStorage;

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private OrphanImageFilesRemoverAdminTask orphanImageFilesRemoverAdminTask;

    @Autowired
    private IAdminTaskService adminTaskService;

    @Override
    public void createEntities() throws Exception { }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void removeOrphansTestDryRun() throws Exception {

        byte[] imageData = th.loadTestResource("testImage.jpg");
        MediaItem orphanMediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
        String fileNameReferenced = orphanMediaItem.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName());
        String fileNameOrphan = mediaConfig.getInternalStoragePrefix() + "orphan";
        fileStorage.copyFileFromDefault("default", fileNameOrphan);

        assertTrue(fileStorage.fileExists(fileStorage.getInternalFileName(fileNameReferenced)));
        assertTrue(fileStorage.fileExists(fileNameOrphan));

        BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(true)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(orphanImageFilesRemoverAdminTask.getName(), adminTaskParameters);

        waitForEventProcessing();

        //assert that we do not delete files that are still referenced
        assertTrue(fileStorage.fileExists(fileStorage.getInternalFileName(fileNameReferenced)));
        //assert we do not delete the orphan in dry run
        assertTrue(fileStorage.fileExists(fileNameOrphan));
    }

    @Test
    public void removeOrphansTest() throws Exception {

        byte[] imageData = th.loadTestResource("testImage.jpg");
        MediaItem orphanMediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
        String fileNameReferenced = orphanMediaItem.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName());
        String fileNameOrphan = mediaConfig.getInternalStoragePrefix() + "orphan";
        fileStorage.copyFileFromDefault("default", fileNameOrphan);

        assertTrue(fileStorage.fileExists(fileStorage.getInternalFileName(fileNameReferenced)));
        assertTrue(fileStorage.fileExists(fileNameOrphan));

        BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(orphanImageFilesRemoverAdminTask.getName(), adminTaskParameters);

        waitForEventProcessing();

        //assert that we do not delete files that are still referenced
        assertTrue(fileStorage.fileExists(fileStorage.getInternalFileName(fileNameReferenced)));
        //assert we do delete the orphan
        assertFalse(fileStorage.fileExists(fileNameOrphan));

    }

}
