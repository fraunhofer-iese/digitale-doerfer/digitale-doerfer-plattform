/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Mher Ter-Tovmasyan
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.contentintegration.services;

import java.io.File;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Profile("test")
@Service
@Log4j2
public class TestWebFeedScraperService extends WebFeedScraperService implements IWebFeedScraperService {

    protected Document getDocument(String webFeedSourceUrl) throws IOException {
        final File input = new File(webFeedSourceUrl);
        if (input.exists()) {
            log.debug("Using test file '{}'", webFeedSourceUrl);
        } else {
            log.debug("No test file '{}' found, using empty document", webFeedSourceUrl);
            return Jsoup.parse("");
        }
        return Jsoup.parse(input, "UTF-8");
    }

}
