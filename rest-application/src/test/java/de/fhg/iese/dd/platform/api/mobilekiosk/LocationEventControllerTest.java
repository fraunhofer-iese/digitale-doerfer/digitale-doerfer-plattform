/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientLocationRecord;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientLocationRecordsReceivedEvent;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.enums.LocationSourceType;

public class LocationEventControllerTest extends BaseServiceTest {
    @Autowired
    private MobileKioskTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndVgAdmin();
        th.createPersons();
        th.createShops();
        th.createPushEntities();
        th.createScoreEntities();
        th.createAchievementRules();
        th.createVehiclesAndDrivers();
        th.createSellingPoints();
        th.createLocationHistory();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void sendClientLocationReceivedEvent() throws Exception{
        long time = timeServiceMock.currentTimeMillisUTC();
        List<ClientLocationRecord> locationRecords = new ArrayList<>();
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(0.9)
                .location(new ClientGPSLocation(49.432717, 7.754221))
                .timestamp(time-20000)
                .build());
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(1.0)
                .location(new ClientGPSLocation(49.431700, 7.753777))
                .timestamp(time-10000)
                .build());
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(0.8)
                .location(new ClientGPSLocation(49.430923, 7.753119))
                .timestamp(time)
                .build());

        ClientLocationRecordsReceivedEvent event = ClientLocationRecordsReceivedEvent
                .builder()
                .vehicleId(th.car.getId())
                .locationRecords(locationRecords)
                .build();

        mockMvc.perform(post("/mobile-kiosk/location/event/locationRecordsReceivedEvent")
                        .headers(authHeadersFor(th.personCarDriver))
                        .contentType(contentType)
                        .content(json(event)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.car.getId())
                        .param("start", "0")
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].latitude", is(locationRecords.get(0).getLocation().getLatitude())))
                .andExpect(jsonPath("$[0].longitude", is(locationRecords.get(0).getLocation().getLongitude())))
                .andExpect(jsonPath("$[0].sourceType", is(LocationSourceType.DRIVER_APP.toString())))
                .andExpect(jsonPath("$[0].sourceId", is(th.personCarDriver.getId())))
                .andExpect(jsonPath("$[0].vehicleId", is(th.car.getId())))
                .andExpect(jsonPath("$[0].accuracy", is(locationRecords.get(0).getAccuracy())))
                .andExpect(jsonPath("$[1].latitude", is(locationRecords.get(1).getLocation().getLatitude())))
                .andExpect(jsonPath("$[1].longitude", is(locationRecords.get(1).getLocation().getLongitude())))
                .andExpect(jsonPath("$[1].sourceType", is(LocationSourceType.DRIVER_APP.toString())))
                .andExpect(jsonPath("$[1].sourceId", is(th.personCarDriver.getId())))
                .andExpect(jsonPath("$[1].vehicleId", is(th.car.getId())))
                .andExpect(jsonPath("$[1].accuracy", is(locationRecords.get(1).getAccuracy())))
                .andExpect(jsonPath("$[2].latitude", is(locationRecords.get(2).getLocation().getLatitude())))
                .andExpect(jsonPath("$[2].longitude", is(locationRecords.get(2).getLocation().getLongitude())))
                .andExpect(jsonPath("$[2].sourceType", is(LocationSourceType.DRIVER_APP.toString())))
                .andExpect(jsonPath("$[2].sourceId", is(th.personCarDriver.getId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.car.getId())))
                .andExpect(jsonPath("$[2].accuracy", is(locationRecords.get(2).getAccuracy())));
    }

    @Test
    public void sendClientLocationReceivedEventForNotExistingVehicleAndExpectNotFound() throws Exception {
        long time = timeServiceMock.currentTimeMillisUTC();
        List<ClientLocationRecord> locationRecords = new ArrayList<>();
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(0.9)
                .location(new ClientGPSLocation(49.432717, 7.754221))
                .timestamp(time - 20000)
                .build());

        ClientLocationRecordsReceivedEvent event = ClientLocationRecordsReceivedEvent
                .builder()
                .vehicleId("notEvenAnUUID")
                .locationRecords(locationRecords)
                .build();

        mockMvc.perform(post("/mobile-kiosk/location/event/locationRecordsReceivedEvent")
                        .headers(authHeadersFor(th.personCarDriver))
                        .contentType(contentType)
                        .content(json(event)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void sendClientLocationReceivedEventForNotDriversVehicleAndExpectNotAuthorized() throws Exception {
        long time = timeServiceMock.currentTimeMillisUTC();
        List<ClientLocationRecord> locationRecords = new ArrayList<>();
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(0.9)
                .location(new ClientGPSLocation(49.432717, 7.754221))
                .timestamp(time - 20000)
                .build());

        ClientLocationRecordsReceivedEvent event = ClientLocationRecordsReceivedEvent
                .builder()
                .vehicleId(th.truck.getId())
                .locationRecords(locationRecords)
                .build();

        mockMvc.perform(post("/mobile-kiosk/location/event/locationRecordsReceivedEvent")
                        .headers(authHeadersFor(th.personCarDriver))
                        .contentType(contentType)
                        .content(json(event)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void sendClientLocationReceivedEventForNonDriverPersonAndExpectNotAuthorized() throws Exception {
        long time = timeServiceMock.currentTimeMillisUTC();
        List<ClientLocationRecord> locationRecords = new ArrayList<>();
        locationRecords.add(ClientLocationRecord
                .builder()
                .accuracy(0.9)
                .location(new ClientGPSLocation(49.432717, 7.754221))
                .timestamp(time - 20000)
                .build());

        ClientLocationRecordsReceivedEvent event = ClientLocationRecordsReceivedEvent
                .builder()
                .vehicleId(th.truck.getId())
                .locationRecords(locationRecords)
                .build();

        mockMvc.perform(post("/mobile-kiosk/location/event/locationRecordsReceivedEvent")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(event)))
                .andExpect(status().isForbidden());
    }
    
}
