/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeEmailAddressRequest;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;

public class PersonEventControllerChangeEmailAddressTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestEmailSenderService emailSenderService;

    private Person personWithOauthAccountTenant1;
    private OauthAccount oAuthAccount;
    private OauthAccount oAuthAccountWithoutUsernamePassword;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        personWithOauthAccountTenant1 = th.personRegularAnna;
        personWithOauthAccountTenant1 = th.addVerificationStatusToPerson(personWithOauthAccountTenant1,
                PersonVerificationStatus.EMAIL_VERIFIED);
        assertEquals(th.tenant1, personWithOauthAccountTenant1.getTenant());
        oAuthAccount = OauthAccount.builder()
                .oauthId(personWithOauthAccountTenant1.getOauthId())
                .name(personWithOauthAccountTenant1.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .emailVerified(false)
                .blocked(false)
                .blockReason(null)
                .loginCount(12)
                .build();
        testOauthManagementService.addOAuthAccount(oAuthAccount, personWithOauthAccountTenant1.getEmail());

        oAuthAccountWithoutUsernamePassword = OauthAccount.builder()
                .oauthId(th.personRegularHorstiTenant3.getOauthId())
                .name(th.personRegularHorstiTenant3.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.APPLE))
                .emailVerified(false)
                .blocked(false)
                .blockReason(null)
                .loginCount(21)
                .build();
        testOauthManagementService.addOAuthAccount(oAuthAccountWithoutUsernamePassword,
                th.personRegularHorstiTenant3.getEmail());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void changeEmailAddressRequest_CurrentEmailNotVerified() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));
        assertThat(personToBeChanged.getNumberOfEmailChanges()).isNull();

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();
        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(newEmail))
                .andExpect(jsonPath("$.person.pendingNewEmail").doesNotExist())
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(newEmail);
        assertFalse(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));
        assertEquals(1, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        // the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_CurrentEmailAlreadyVerified() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        personToBeChanged.setNumberOfEmailChanges(0);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();
        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(personToBeChanged.getEmail()))
                .andExpect(jsonPath("$.person.pendingNewEmail").value(newEmail))
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(personToBeChanged.getEmail());
        assertThat(personAfterChange.getPendingNewEmail()).isEqualTo(newEmail);
        assertTrue(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));
        assertEquals(1, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_NumberOfEmailChangesNotExceeded() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        // number of email changes = 2 and sent last verification email 1 minute ago
        personToBeChanged.setNumberOfEmailChanges(2);
        personToBeChanged.setLastSentTimeEmailVerification(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(1));
        th.personRepository.save(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();
        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(personToBeChanged.getEmail()))
                .andExpect(jsonPath("$.person.pendingNewEmail").value(newEmail))
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(personToBeChanged.getEmail());
        assertThat(personAfterChange.getPendingNewEmail()).isEqualTo(newEmail);
        assertTrue(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));
        assertEquals(personAfterChange.getNumberOfEmailChanges(), 3);

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_NumberOfEmailChangesExceededButChangeIntervalElapsed() throws Exception {

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().addValue(PersonVerificationStatus.EMAIL_VERIFIED);
        // number of email changes = 6 and sent last verification email 1 day ago
        personToBeChanged.setNumberOfEmailChanges(6);
        personToBeChanged.setLastSentTimeEmailVerification(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(1));
        th.personRepository.save(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();
        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(personToBeChanged.getEmail()))
                .andExpect(jsonPath("$.person.pendingNewEmail").value(newEmail))
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(personToBeChanged.getEmail());
        assertThat(personAfterChange.getPendingNewEmail()).isEqualTo(newEmail);
        assertTrue(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));
        assertEquals(personAfterChange.getNumberOfEmailChanges(), 7);

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_EmailNormalized() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmailRequested = "New@mail.de ";
        final String newEmailNormalized = "new@mail.de";
        personToBeChanged.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmailRequested));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmailRequested)
                        .build();
        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(newEmailNormalized))
                .andExpect(jsonPath("$.person.pendingNewEmail").doesNotExist())
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(newEmailNormalized);
        assertFalse(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was not send out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmailNormalized);
    }

    @Test
    public void changeEmailAddressRequest_EmailVerificationTurnedOff() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check that the verification mail is not sent out
        setEmailVerificationFeatureEnabled(false);

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.firstName").value(personToBeChanged.getFirstName()))
                .andExpect(jsonPath("$.person.lastName").value(personToBeChanged.getLastName()))
                .andExpect(jsonPath("$.person.email").value(newEmail))
                .andExpect(jsonPath("$.person.pendingNewEmail").doesNotExist())
                .andExpect(jsonPath("$.person.address.id").isNotEmpty())
                .andExpect(jsonPath("$.person.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(newEmail);
        assertFalse(personAfterChange.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED));

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was not send out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_SameEmail() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = personToBeChanged.getEmail();

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_EmailAlreadyRegistered() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final Person existingPerson = th.personRegularHorstiTenant3;
        final String currentEmail = personToBeChanged.getEmail();
        final String newEmail = existingPerson.getEmail();

        assertThat(currentEmail).isNotEqualToIgnoringCase(newEmail);
        assertThat(personToBeChanged).isNotEqualTo(existingPerson);
        assertThat(th.personRepository.findByEmail(newEmail)).isPresent();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build())))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(currentEmail);
    }

    @Test
    public void changeEmailAddressRequest_WithPendingNewEmailAlreadyRegistered() throws Exception {

        final Person personToBeChanged = personWithOauthAccountTenant1;
        final Person existingPerson = th.personRegularHorstiTenant3;
        final String currentEmail = personToBeChanged.getEmail();
        final String newEmail = "new@mail.de";
        existingPerson.setPendingNewEmail(newEmail);
        personRepository.saveAndFlush(existingPerson);

        assertThat(currentEmail).isNotEqualToIgnoringCase(newEmail);
        assertThat(personToBeChanged).isNotEqualTo(existingPerson);

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build())))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(currentEmail);
    }

    @Test
    public void changeEmailAddressRequest_InvalidOAuthId() throws Exception {

        final Person personToBeChanged = th.personRegularKarl;
        final String oldEmail = personToBeChanged.getEmail();
        final String newEmail = "new@mail.de";
        testOauthManagementService.deleteUser(personToBeChanged.getOauthId());

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);
    }

    @Test
    public void changeEmailAddressRequest_NoUsernamePasswordAccount() throws Exception {

        final Person personToBeChanged = th.personRegularHorstiTenant3;
        final String oldEmail = personToBeChanged.getEmail();
        final String newEmail = "new@mail.de";

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        final ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);
    }

    @Test
    public void changeEmailAddressRequest_EmailChangeIntervalNotElapsed() throws Exception {

        final Person personToBeChanged = th.personRegularKarl;
        final String oldEmail = personToBeChanged.getEmail();
        final String newEmail = "new@mail.de";

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        // number of email changes = 3 and sent last verification email 2 hours and 59 minutes ago
        personToBeChanged.setNumberOfEmailChanges(3);
        personToBeChanged.setLastSentTimeEmailVerification(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(179));
        th.personRepository.save(personToBeChanged);

        ClientPersonChangeEmailAddressRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE));

        Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);
        assertEquals(3, personAfterChange.getNumberOfEmailChanges());

        //email sending is asynchronous
        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(newEmail);

        // number of email changes = 10 and sent last verification email 6 hours and 59 minutes ago
        personToBeChanged.setNumberOfEmailChanges(10);
        personToBeChanged.setLastSentTimeEmailVerification(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(359));
        th.personRepository.save(personToBeChanged);

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE));

        personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);
        assertEquals(10, personAfterChange.getNumberOfEmailChanges());

        //email sending is asynchronous
        waitForEventProcessing();

        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(newEmail);
    }

    @Test
    public void changeEmailAddressRequest_InvalidRequest() throws Exception {

        final Person personToBeChanged = th.personRegularHorstiTenant3;
        final String oldEmail = personToBeChanged.getEmail();

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress("")
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress(null)
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);

        mockMvc.perform(post("/person/event/personChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress("not_valid_mail")
                        .build())))
                .andExpect(isException(ClientExceptionType.EMAIL_ADDRESS_INVALID));

        personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getEmail()).isEqualTo(oldEmail);
    }

    @Test
    public void changeEmailAddressRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personChangeEmailAddressRequest")
                .contentType(contentType)
                .content(json(ClientPersonChangeEmailAddressRequest.builder()
                        .newEmailAddress("mail")
                        .build())));
    }

    @Test
    public void cancelChangeEmailAddressRequest() throws Exception {

        Person personToBeChanged = th.personRegularHorstiTenant3;
        final String newEmail = "new@mail.de";
        personToBeChanged.setPendingNewEmail(newEmail);
        personToBeChanged = personRepository.saveAndFlush(personToBeChanged);

        assertThat(personToBeChanged.getPendingNewEmail()).isEqualTo(newEmail);

        mockMvc.perform(post("/person/event/personCancelChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType))
                .andExpect(status().isOk());

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getPendingNewEmail()).isNull();
    }

    @Test
    public void cancelChangeEmailAddressRequest_NoPendingNewEmail() throws Exception {

        Person personToBeChanged = th.personRegularHorstiTenant3;

        mockMvc.perform(post("/person/event/personCancelChangeEmailAddressRequest")
                .headers(authHeadersFor(personToBeChanged))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.PERSON_HAS_NO_PENDING_NEW_EMAIL));
    }

    @Test
    public void cancelChangeEmailAddressRequest_NotAuthorized() throws Exception {
        
        assertOAuth2(post("/person/event/personCancelChangeEmailAddressRequest")
                .contentType(contentType)
                .contentType(contentType));
    }

    private void setEmailVerificationFeatureEnabled(boolean b) {
        final FeatureConfig featureConfig = FeatureConfig.builder()
                .enabled(b)
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .build();
        featureConfigRepository.save(featureConfig);
    }

}
