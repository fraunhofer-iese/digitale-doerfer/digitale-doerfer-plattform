/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Tahmid Ekram, Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.suggestion.*;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionStatusRecord;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientSuggestionWorkerMapping;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SuggestionEventControllerTest extends BasePostEventControllerTest<Suggestion> {

    private AppVariant appVariantSuggestionCreationEnabled;

    private AppVariant appVariantSuggestionPublicCommentCreationRestricted;

    @Override
    public void createEntities() throws Exception {
        super.createEntities();
        createFeatureSuggestionCreationEnabled();
        createFeatureSuggestionPublicCommentCreation();
    }

    private void createFeatureSuggestionCreationEnabled() {
        appVariantSuggestionCreationEnabled = th.appVariant4AllTenants;
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantSuggestionCreationEnabled)
                .enabled(true)
                .featureClass(SuggestionCreationFeature.class.getName())
                .configValues(null)
                .build());
    }

    private void createFeatureSuggestionPublicCommentCreation() {

        appVariantSuggestionPublicCommentCreationRestricted = th.appVariant1Tenant1;
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantSuggestionPublicCommentCreationRestricted)
                .enabled(true)
                .featureClass(SuggestionPostTypeFeature.class.getName())
                .configValues("{\"publicCommentCreation\": " + false + " }")
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantSuggestionPublicCommentCreationRestricted)
                .enabled(true)
                .featureClass(SuggestionCreationFeature.class.getName())
                .configValues(null)
                .build());
    }

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return new ClientSuggestionChangeRequest();
    }

    @Override
    protected Suggestion getTestPost1() {
        return th.suggestionDefect1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personMonikaHess;
    }

    @Override
    protected Suggestion getTestPost2() {
        return th.suggestionDefect2;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.offer1;
    }

    @Test
    public void suggestionCreateRequest_VerifyingPushMessage() throws Exception {

        Person postCreator = th.personSusanneChristmann;

        String text = "Unsere Dorffunk-App ist echt cool :-)";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientLocationDefinition customLocationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;
        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(createdLocation)
                .customLocationDefinition(customLocationDefinition)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the suggestion
        mockMvc.perform(get("/grapevine/post/{postId}", suggestionCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(postCreator, appVariantSuggestionCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.suggestion.id").value(suggestionCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.suggestion.id").value(
                        suggestionCreateConfirmation.getPost().getSuggestion().getId()))
                .andExpect(jsonPath("$.suggestion.text").value(text))
                .andExpect(jsonPath("$.suggestion.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.suggestion.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.suggestion.lastActivity").value(nowTime))
                .andExpect(jsonPath("$.suggestion.customLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.suggestion.customLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.suggestion.customAddress.name")
                        .value(customLocationDefinition.getLocationName()))
                .andExpect(jsonPath("$.suggestion.customAddress.street").value(
                        customLocationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.suggestion.customAddress.zip").value(customLocationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.suggestion.customAddress.city").value(customLocationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.suggestion.customAddress.gpsLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.suggestion.customAddress.gpsLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.suggestion.suggestionCategory.id").value(th.suggestionCategoryWish.getId()))
                .andExpect(jsonPath("$.suggestion.hiddenForOtherHomeAreas").value(false)) //testing default setting
                .andExpect(jsonPath("$.suggestion.commentsAllowed").value(true)); //testing default setting

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion =
                (Suggestion) th.postRepository.findById(suggestionCreateConfirmation.getPost().getId())
                        .get();

        assertThat(addedSuggestion.isInternal()).isFalse();
        assertThat(addedSuggestion.getLastSuggestionActivity()).isEqualTo(nowTime);
        assertEquals(postCreator, addedSuggestion.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSuggestion.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSuggestion.getCreatedLocation().getLongitude());
        assertEquals(customLocationDefinition.getLocationName(), addedSuggestion.getCustomAddress().getName());
        assertEquals(customLocationDefinition.getAddressStreet(), addedSuggestion.getCustomAddress().getStreet());
        assertEquals(customLocationDefinition.getAddressZip(), addedSuggestion.getCustomAddress().getZip());
        assertEquals(customLocationDefinition.getAddressCity(), addedSuggestion.getCustomAddress().getCity());
        assertEquals(customLocationDefinition.getGpsLocation().getLatitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLatitude());
        assertEquals(customLocationDefinition.getGpsLocation().getLongitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLongitude());
        assertEquals(text, addedSuggestion.getText());
        assertEquals(postCreator.getTenant(), addedSuggestion.getTenant());
        assertThat(addedSuggestion.getImages()).isNullOrEmpty();
        assertThat(addedSuggestion.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertFalse(addedSuggestion.isHiddenForOtherHomeAreas());
        assertTrue(addedSuggestion.isCommentsAllowed());
        assertNotNull(addedSuggestion.getInternalWorkerChat());

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasLoud(
                addedSuggestion.getCreator(),
                addedSuggestion.getGeoAreas(),
                false,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED);

        assertEquals(suggestionCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(suggestionCreateConfirmation.getPostId(), pushedEvent.getPost().getSuggestion().getId());
        //check that the last name is cut
        assertEquals(postCreator.getFirstName(), pushedEvent.getPost().getSuggestion().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(postCreator.getLastName()),
                pushedEvent.getPost().getSuggestion().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionCreateRequest_PublicCommentsDisallowed() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Bürgerbeteiligung ist toll, aber noch besser ohne Bürger!";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientLocationDefinition customLocationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(createdLocation)
                .customLocationDefinition(customLocationDefinition)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(postCreator, appVariantSuggestionPublicCommentCreationRestricted))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the suggestion
        mockMvc.perform(get("/grapevine/post/{postId}", suggestionCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(postCreator, appVariantSuggestionPublicCommentCreationRestricted)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.suggestion.id").value(suggestionCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.suggestion.id").value(
                        suggestionCreateConfirmation.getPost().getSuggestion().getId()))
                .andExpect(jsonPath("$.suggestion.text").value(text))
                .andExpect(jsonPath("$.suggestion.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.suggestion.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.suggestion.customLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.suggestion.customLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(
                        jsonPath("$.suggestion.customAddress.name").value(customLocationDefinition.getLocationName()))
                .andExpect(jsonPath("$.suggestion.customAddress.street").value(
                        customLocationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.suggestion.customAddress.zip").value(customLocationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.suggestion.customAddress.city").value(customLocationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.suggestion.customAddress.gpsLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.suggestion.customAddress.gpsLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.suggestion.suggestionCategory.id").value(th.suggestionCategoryWish.getId()))
                .andExpect(jsonPath("$.suggestion.hiddenForOtherHomeAreas").value(false)) //testing default setting
                .andExpect(jsonPath("$.suggestion.commentsAllowed").value(
                        false)); //according to the setting of the feature

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion =
                (Suggestion) th.postRepository.findById(suggestionCreateConfirmation.getPost().getId())
                        .get();

        assertThat(addedSuggestion.isInternal()).isFalse();
        assertEquals(postCreator, addedSuggestion.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSuggestion.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSuggestion.getCreatedLocation().getLongitude());
        assertEquals(customLocationDefinition.getLocationName(), addedSuggestion.getCustomAddress().getName());
        assertEquals(customLocationDefinition.getAddressStreet(), addedSuggestion.getCustomAddress().getStreet());
        assertEquals(customLocationDefinition.getAddressZip(), addedSuggestion.getCustomAddress().getZip());
        assertEquals(customLocationDefinition.getAddressCity(), addedSuggestion.getCustomAddress().getCity());
        assertEquals(customLocationDefinition.getGpsLocation().getLatitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLatitude());
        assertEquals(customLocationDefinition.getGpsLocation().getLongitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLongitude());
        assertEquals(text, addedSuggestion.getText());
        assertEquals(postCreator.getTenant(), addedSuggestion.getTenant());
        assertThat(addedSuggestion.getImages()).isNullOrEmpty();
        assertThat(addedSuggestion.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertFalse(addedSuggestion.isHiddenForOtherHomeAreas());
        assertFalse(addedSuggestion.isCommentsAllowed());
        assertNotNull(addedSuggestion.getInternalWorkerChat());
    }

    @Test
    public void suggestionCreateRequest_ContentCreationRestricted() throws Exception {

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text("text")
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(th.pointClosestToTenant1)
                .customLocationDefinition(GrapevineTestHelper.locationDefinitionDaNicola)
                .build();

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/event/suggestionCreateRequest")
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)));
    }

    @Test
    public void suggestionCreateRequest_InternalSuggestion() throws Exception {

        final Person suggestionCreator = th.personSuggestionWorkerTenant1Dieter;
        String text = "Super, endlich ein Ticket-System!";
        ClientGPSLocation createdLocation = new ClientGPSLocation(12.02d, 19.83d);
        ClientLocationDefinition customLocationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(createdLocation)
                .customLocationDefinition(customLocationDefinition)
                .internal(true)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(suggestionCreator, appVariantSuggestionPublicCommentCreationRestricted))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        //not visible as post on DorfFunk endpoints for normal persons
        mockMvc.perform(get("/grapevine/post/{postId}", suggestionCreateConfirmation.getPost().getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personSusanneChristmann,
                        appVariantSuggestionPublicCommentCreationRestricted)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
        //not visible as post on DorfFunk endpoints even for the creator
        mockMvc.perform(get("/grapevine/post/{postId}", suggestionCreateConfirmation.getPost().getId())
                .contentType(contentType)
                .headers(authHeadersFor(suggestionCreator, appVariantSuggestionPublicCommentCreationRestricted)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        // Get the suggestion as suggestion worker
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestionCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(suggestionCreator,
                        appVariantSuggestionPublicCommentCreationRestricted)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(suggestionCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.id").value(
                        suggestionCreateConfirmation.getPost().getSuggestion().getId()))
                .andExpect(jsonPath("$.text").value(text))
                .andExpect(jsonPath("$.internal").value(true))
                .andExpect(jsonPath("$.creator.id").value(suggestionCreator.getId()))
                .andExpect(jsonPath("$.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.customLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.customLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(
                        jsonPath("$.customAddress.name").value(customLocationDefinition.getLocationName()))
                .andExpect(jsonPath("$.customAddress.street").value(
                        customLocationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.customAddress.zip").value(customLocationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.customAddress.city").value(customLocationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.customAddress.gpsLocation.latitude").value(
                        customLocationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.customAddress.gpsLocation.longitude").value(
                        customLocationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.suggestionCategory.id").value(th.suggestionCategoryWish.getId()))
                .andExpect(jsonPath("$.hiddenForOtherHomeAreas").value(false)) //testing default setting
                .andExpect(jsonPath("$.commentsAllowed").value(false)); //according to the setting of the feature

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion =
                (Suggestion) th.postRepository.findById(suggestionCreateConfirmation.getPost().getId())
                        .get();

        assertThat(addedSuggestion.isInternal()).isTrue();
        assertEquals(suggestionCreator, addedSuggestion.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSuggestion.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSuggestion.getCreatedLocation().getLongitude());
        assertEquals(customLocationDefinition.getLocationName(), addedSuggestion.getCustomAddress().getName());
        assertEquals(customLocationDefinition.getAddressStreet(), addedSuggestion.getCustomAddress().getStreet());
        assertEquals(customLocationDefinition.getAddressZip(), addedSuggestion.getCustomAddress().getZip());
        assertEquals(customLocationDefinition.getAddressCity(), addedSuggestion.getCustomAddress().getCity());
        assertEquals(customLocationDefinition.getGpsLocation().getLatitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLatitude());
        assertEquals(customLocationDefinition.getGpsLocation().getLongitude(),
                addedSuggestion.getCustomAddress().getGpsLocation().getLongitude());
        assertEquals(text, addedSuggestion.getText());
        assertEquals(suggestionCreator.getTenant(), addedSuggestion.getTenant());
        assertThat(addedSuggestion.getImages()).isNullOrEmpty();
        assertThat(addedSuggestion.getGeoAreas()).containsOnly(suggestionCreator.getHomeArea());
        assertFalse(addedSuggestion.isHiddenForOtherHomeAreas());
        assertFalse(addedSuggestion.isCommentsAllowed());
        assertNotNull(addedSuggestion.getInternalWorkerChat());

        // verify that no push message was sent
        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionCreateRequest_InternalSuggestion_NoPermission() throws Exception {

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(
                        authHeadersFor(th.personSusanneChristmann, appVariantSuggestionPublicCommentCreationRestricted))
                .contentType(contentType)
                .content(json(ClientSuggestionCreateRequest.builder()
                        .text("Ich lege denen mal ein Ticket an!")
                        .suggestionCategoryId(th.suggestionCategoryWish.getId())
                        .createdLocation(new ClientGPSLocation(12.02d, 19.83d))
                        .customLocationDefinition(GrapevineTestHelper.locationDefinitionDaNicola)
                        .internal(true)
                        .build())))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void suggestionCreateRequest_WithImagesAndDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "a problem";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images
        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .suggestionCategoryId(th.suggestionCategoryDamage.getId())
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariant))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(suggestionCreateConfirmation.getPost().getSuggestion().getText()).isEqualTo(text);
        assertThat(suggestionCreateConfirmation.getPost().getSuggestion().getImages()).hasSize(3);
        assertThat(suggestionCreateConfirmation.getPost().getSuggestion().getAttachments()).hasSize(3);

        // Get the suggestion
        //check new entity uses images
        String postId = suggestionCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.suggestion.id").value(postId))
                .andExpect(jsonPath("$.suggestion.id").value(
                        suggestionCreateConfirmation.getPost().getSuggestion().getId()))
                .andExpect(jsonPath("$.suggestion.text").value(text))
                .andExpect(jsonPath("$.suggestion.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.suggestion.images", hasSize(3)))
                .andExpect(jsonPath("$.suggestion.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.suggestion.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.suggestion.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.suggestion.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.suggestion.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.suggestion.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.suggestion.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.suggestion.suggestionCategory.id").value(th.suggestionCategoryDamage.getId()))
                .andExpect(jsonPath("$.suggestion.hiddenForOtherHomeAreas").value(true));

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion = (Suggestion) th.postRepository.findById(postId).get();

        assertThat(addedSuggestion.isInternal()).isFalse();
        assertEquals(postCreator, addedSuggestion.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSuggestion.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSuggestion.getCreatedLocation().getLongitude());
        assertEquals(text, addedSuggestion.getText());
        assertEquals(postCreator.getTenant(), addedSuggestion.getTenant());
        assertEquals(3, addedSuggestion.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedSuggestion.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedSuggestion.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedSuggestion.getImages().get(2));
        assertEquals(3, addedSuggestion.getAttachments().size());
        assertThat(addedSuggestion.getAttachments()).hasSize(3);
        assertThat(addedSuggestion.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(addedSuggestion.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedSuggestion.isHiddenForOtherHomeAreas());

        //check used temp images are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        //check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));
    }

    @Test
    public void suggestionCreateRequest_InvalidImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "a problem";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientGPSLocation customLocation = new ClientGPSLocation(0.8d, 1.5d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id
        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .customLocationDefinition(ClientLocationDefinition.builder().gpsLocation(customLocation).build())
                .suggestionCategoryId(th.suggestionCategoryDamage.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .build();

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariant))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void suggestionCreateRequest_InvalidDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "another problem";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientGPSLocation customLocation = new ClientGPSLocation(0.8d, 1.5d);
        //create temp images
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempDocumentItemId = "invalid id";

        //create request using temp documents and invalid id
        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .customLocationDefinition(ClientLocationDefinition.builder().gpsLocation(customLocation).build())
                .suggestionCategoryId(th.suggestionCategoryDamage.getId())
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidTempDocumentItemId))
                .build();

        //check temp document not found exception
        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariant))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(isException("[" + invalidTempDocumentItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    @Test
    public void suggestionCreateRequest_ExceedingMaxNumImages() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "suggestion with too many images";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(th.createTemporaryMediaItem(person, timeServiceMock.currentTimeMillisUTC()).getId());
        }

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(person, appVariant))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionCreateRequest_ExceedingMaxNumDocuments() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "suggestion with too many documents";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerPost(); i++) {
            temporaryIds.add(th.createTemporaryDocumentItem(person, timeServiceMock.currentTimeMillisUTC(),
                    th.nextTimeStamp()).getId());
        }

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(person, appVariant))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionCreateRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/suggestionCreateRequest")
                .content(json(ClientSuggestionCreateRequest.builder()
                        .text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr")
                        .suggestionCategoryId(th.suggestionCategoryWish.getId())
                        .createdLocation(th.pointClosestToTenant1)
                        .customLocationDefinition(GrapevineTestHelper.locationDefinitionDaNicola)
                        .build()))
                .contentType(contentType), th.appVariant4AllTenants, th.personThomasBecker);
    }

    @Test
    public void suggestionCreateRequest_FeatureDisabled() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant3Tenant3;

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text("Koin feature in dis äpp variant")
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void suggestionCreateRequest_ExceedingMaxNumChars() throws Exception {

        Person person = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);
        log.info("Using random text '{}'", text);

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientGPSLocation customLocation = new ClientGPSLocation(0.8d, 1.5d);

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(createdLocation)
                .customLocationDefinition(ClientLocationDefinition.builder().gpsLocation(customLocation).build())
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionCreateRequest_MissingAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        String text = "suggestion with missing attributes";

        // missing text
        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing homeArea and no tenant Id
        suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        person.setHomeArea(null);
        person = th.personRepository.save(person);
        mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));
    }

    @Test
    public void suggestionCreateRequest_DefaultCategory() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;
        Tenant tenant = th.tenant1;
        String text = "a problem with no category";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        ClientGPSLocation customLocation = new ClientGPSLocation(0.8d, 1.5d);

        // create request with no suggestion category id
        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .customLocationDefinition(ClientLocationDefinition.builder().gpsLocation(customLocation).build())
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                .headers(authHeadersFor(postCreator, appVariant))
                .contentType(contentType)
                .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // check that default suggestion category is used
        mockMvc.perform(get("/grapevine/post/" + suggestionCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(postCreator, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.suggestion.id").value(suggestionCreateConfirmation.getPostId()))
                .andExpect(jsonPath("$.suggestion.id").value(suggestionCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.suggestion.id").value(
                        suggestionCreateConfirmation.getPost().getSuggestion().getId()))
                .andExpect(jsonPath("$.suggestion.text").value(text))
                .andExpect(jsonPath("$.suggestion.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.suggestion.createdLocation").doesNotExist())
                .andExpect(
                        jsonPath("$.suggestion.customAddress.gpsLocation.latitude").value(customLocation.getLatitude()))
                .andExpect(jsonPath("$.suggestion.customAddress.gpsLocation.longitude").value(
                        customLocation.getLongitude()))
                .andExpect(jsonPath("$.suggestion.suggestionCategory.id").value(th.suggestionCategoryMisc.getId()))
                .andExpect(jsonPath("$.suggestion.hiddenForOtherHomeAreas").value(true));

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion =
                (Suggestion) th.postRepository.findById(suggestionCreateConfirmation.getPost().getId()).get();

        assertEquals(postCreator, addedSuggestion.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSuggestion.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSuggestion.getCreatedLocation().getLongitude());
        assertEquals(customLocation.getLatitude(), addedSuggestion.getCustomAddress().getGpsLocation().getLatitude());
        assertEquals(customLocation.getLongitude(), addedSuggestion.getCustomAddress().getGpsLocation().getLongitude());
        assertEquals(text, addedSuggestion.getText());
        assertEquals(tenant, addedSuggestion.getTenant());
        assertEquals(th.suggestionCategoryMisc, addedSuggestion.getSuggestionCategory());
        assertThat(addedSuggestion.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedSuggestion.isHiddenForOtherHomeAreas());
    }

    @Test
    public void suggestionChangeRequest_TextVerifyingPushMessage() throws Exception {

        Person person = th.personLaraSchaefer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;

        String newText = "this is the new text for suggestion 1";

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .text(newText)
                .build();

        MvcResult suggestionChangeResult = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andExpect(jsonPath("$.post.suggestion.id").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmation = toObject(suggestionChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the suggestion is also updated in the repository
        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestionChangeConfirmation.getPostId())
                .get();

        assertEquals(newText, updatedSuggestion.getText());

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                suggestionToBeChanged.getGeoAreas(),
                suggestionToBeChanged.isHiddenForOtherHomeAreas(),
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(suggestionChangeConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(suggestionChangeConfirmation.getPostId(), pushedEvent.getPost().getSuggestion().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionChangeRequest_TextAndCategory() throws Exception {

        Person person = th.personLaraSchaefer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;
        //verify that test data has not changed
        assertThat(suggestionToBeChanged.getSuggestionCategory()).isEqualTo(th.suggestionCategoryDamage);

        String newText = "this is the new text for suggestion 1";
        final SuggestionCategory newCategory = th.suggestionCategoryWish;

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .suggestionCategoryId(newCategory.getId())
                .text(newText)
                .build();

        MvcResult suggestionChangeResult = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andExpect(jsonPath("$.post.suggestion.id").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmation = toObject(suggestionChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the suggestion is also updated in the repository
        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestionChangeConfirmation.getPostId())
                .get();

        assertEquals(newText, updatedSuggestion.getText());
        assertEquals(newCategory, updatedSuggestion.getSuggestionCategory());
        assertEquals(suggestionToBeChanged.getSuggestionStatus(),
                updatedSuggestion.getSuggestionStatus(), "status should be unchanged");
    }

    @Test
    public void suggestionChangeRequest_CustomAddress() throws Exception {

        Person person = th.personLaraSchaefer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;
        //verify that test data has not changed
        assertThat(suggestionToBeChanged.getSuggestionCategory()).isEqualTo(th.suggestionCategoryDamage);

        ClientLocationDefinition customLocationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .text("supply a new customLocationDefinition")
                .customLocationDefinition(customLocationDefinition)
                .useEmptyFields(true)
                .build();

        MvcResult suggestionChangeResult = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andExpect(jsonPath("$.post.suggestion.id").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmation = toObject(suggestionChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestionChangeConfirmation.getPostId())
                .get();

        // Address should be updated now
        assertAddressEqualsCustomLocationDefinition(customLocationDefinition, updatedSuggestion.getCustomAddress());

        ClientSuggestionChangeRequest suggestionRequestWithUseEmptyFields = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .text("use empty fields now, do not supply a new address")
                .useEmptyFields(true)
                .build();

        MvcResult suggestionChangeResultEmptyFields = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequestWithUseEmptyFields)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmationEmptyFields = toObject(suggestionChangeResultEmptyFields.getResponse(),
                ClientPostChangeConfirmation.class);

        Suggestion updatedSuggestionEmptyFields = (Suggestion) th.postRepository.findById(suggestionChangeConfirmationEmptyFields.getPostId())
                .get();

        //Address should not be erased when using useEmptyFields
        assertAddressEqualsCustomLocationDefinition(customLocationDefinition,
                updatedSuggestionEmptyFields.getCustomAddress());

        ClientSuggestionChangeRequest suggestionRequestDeletingCustomAddress = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .text("always remove the address when 'removeCustomAddress' is set to true")
                .customLocationDefinition(customLocationDefinition)
                .useEmptyFields(true)
                .removeCustomAddress(true)
                .build();

        MvcResult suggestionChangeResultWithoutAddress = mockMvc.perform(
                post("/grapevine/event/suggestionChangeRequest")
                        .headers(authHeadersFor(person, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(suggestionRequestDeletingCustomAddress)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmationWithoutAddress =
                toObject(suggestionChangeResultWithoutAddress.getResponse(),
                        ClientPostChangeConfirmation.class);

        Suggestion updatedSuggestionWithoutAddress =
                (Suggestion) th.postRepository.findById(suggestionChangeConfirmationWithoutAddress.getPostId())
                .get();

        //Address should be erased due to removeCustomAddress == true
        assertNull(updatedSuggestionWithoutAddress.getCustomAddress());
    }

    @Test
    public void suggestionChangeRequest_changeSuggestionCategory() throws Exception {

        Person person = th.personLaraSchaefer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;
        //verify that test data has not changed
        assertThat(suggestionToBeChanged.getSuggestionCategory()).isEqualTo(th.suggestionCategoryDamage);

        final SuggestionCategory newCategory = th.suggestionCategoryWish;

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .suggestionCategoryId(newCategory.getId())
                .build();

        MvcResult suggestionChangeResult = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmation = toObject(suggestionChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the suggestion is also updated in the repository
        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestionChangeConfirmation.getPostId())
                .get();

        assertEquals(newCategory, updatedSuggestion.getSuggestionCategory());
        assertEquals(suggestionToBeChanged.getText(), updatedSuggestion.getText(), "text should be unchanged");
        assertEquals(suggestionToBeChanged.getSuggestionStatus(),
                updatedSuggestion.getSuggestionStatus(), "status should be unchanged");
    }


    @Test
    public void suggestionChangeRequest_PublicCommentsDisallowed() throws Exception {

        Person postCreator = th.personSusanneChristmann;

        ClientSuggestionCreateRequest suggestionCreateRequest = ClientSuggestionCreateRequest.builder()
                .text("Alter Text")
                .suggestionCategoryId(th.suggestionCategoryWish.getId())
                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                .customLocationDefinition(GrapevineTestHelper.locationDefinitionDaNicola)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/suggestionCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantSuggestionPublicCommentCreationRestricted))
                        .contentType(contentType)
                        .content(json(suggestionCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation suggestionCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the suggestion
        String suggestionId = suggestionCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", suggestionId)
                        .headers(authHeadersFor(postCreator, appVariantSuggestionPublicCommentCreationRestricted)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.suggestion.id").value(suggestionId))
                .andExpect(jsonPath("$.suggestion.commentsAllowed").value(
                        false)); //according to the setting of the feature

        // Check whether the suggestion is also added to the repository
        Suggestion addedSuggestion = (Suggestion) th.postRepository.findById(suggestionId).get();

        assertThat(addedSuggestion.isCommentsAllowed()).isFalse();

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionId)
                .text("Neuer Text")
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andExpect(jsonPath("$.post.suggestion.commentsAllowed").value(false));

        Suggestion changedSuggestion = (Suggestion) th.postRepository.findById(suggestionId).get();

        assertThat(changedSuggestion.isCommentsAllowed()).isFalse();
    }

    @Test
    public void suggestionChangeRequest_MissingOrWrongAttributes() throws Exception {

        Person person = th.personLaraSchaefer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;
        Suggestion suggestionWithoutAddress = th.suggestionIdea1;
        //verify that test data has not changed
        assertThat(suggestionToBeChanged.getSuggestionCategory()).isEqualTo(th.suggestionCategoryDamage);

        String newText = "this is the new text for suggestion 1";
        final SuggestionCategory newCategory = th.suggestionCategoryWish;
        final ClientLocationDefinition invalidLocation = ClientLocationDefinition.builder()
                .addressCity("Testcity")
                .build();

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(ClientSuggestionChangeRequest.builder()
                        .postId("")
                        .suggestionCategoryId(newCategory.getId())
                        .text(newText)
                        .build())))
                .andExpect(isException("postId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(ClientSuggestionChangeRequest.builder()
                        .postId("not existing")
                        .suggestionCategoryId(newCategory.getId())
                        .text(newText)
                        .build())))
                .andExpect(isException("not existing", ClientExceptionType.POST_NOT_FOUND));

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(ClientSuggestionChangeRequest.builder()
                        .postId(suggestionToBeChanged.getId())
                        .suggestionCategoryId("wrong id")
                        .text(newText)
                        .build())))
                .andExpect(isException("wrong id", ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND));

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(ClientSuggestionChangeRequest.builder()
                        .postId(suggestionToBeChanged.getId())
                        .suggestionCategoryId(newCategory.getId())
                        .customLocationDefinition(invalidLocation)
                        .text(newText)
                        .build())))
                .andExpect(isException(ClientExceptionType.ADDRESS_INVALID));

        MvcResult suggestionChangeResultWithoutAddress = mockMvc.perform(
                post("/grapevine/event/suggestionChangeRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(ClientSuggestionChangeRequest.builder()
                                .postId(suggestionWithoutAddress.getId())
                                .build())))
                .andExpect(jsonPath("$.postId").value(suggestionWithoutAddress.getId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmationWithoutAddress =
                toObject(suggestionChangeResultWithoutAddress.getResponse(),
                        ClientPostChangeConfirmation.class);

        Suggestion updatedSuggestionWithoutAddress =
                (Suggestion) th.postRepository.findById(suggestionChangeConfirmationWithoutAddress.getPostId())
                        .get();

        assertNull(updatedSuggestionWithoutAddress.getCustomAddress());
    }

    @Test
    public void suggestionChangeRequest_InternalSuggestion() throws Exception {

        Suggestion suggestionToBeChanged = th.suggestionInternal;

        String newText = "ach, ich schreib das doch nochmal um :-)";

        ClientSuggestionChangeRequest suggestionRequest = ClientSuggestionChangeRequest.builder()
                .postId(suggestionToBeChanged.getId())
                .text(newText)
                .build();

        MvcResult suggestionChangeResult = mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(suggestionToBeChanged.getCreator(), th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(suggestionRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(suggestionRequest.getPostId()))
                .andExpect(jsonPath("$.post.suggestion.id").value(suggestionRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation suggestionChangeConfirmation = toObject(suggestionChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the suggestion is also updated in the repository
        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestionChangeConfirmation.getPostId())
                .get();

        assertEquals(newText, updatedSuggestion.getText());

        // verify that no push message was sent
        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionChangeRequest_NotOwner() throws Exception {

        Person person = th.personSebastianBauer;
        Suggestion suggestionToBeChanged = th.suggestionDefect1;

        assertNotEquals(person, suggestionToBeChanged.getCreator());

        mockMvc.perform(post("/grapevine/event/suggestionChangeRequest")
                .headers(authHeadersFor(person, th.appVariant1Tenant1))
                .contentType(contentType)
                .content(json(ClientSuggestionChangeRequest.builder()
                        .postId(suggestionToBeChanged.getId())
                        .text("this is the new text for suggestion 1")
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));
    }

    @Test
    public void suggestionStatusChangeRequest_WorkerAndFirstResponder() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        SuggestionStatus newStatus = SuggestionStatus.DONE;

        assertEquals(SuggestionStatus.IN_PROGRESS, suggestion.getSuggestionStatus());

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        ClientSuggestionExtended expectedChangedSuggestion = th.toClientSuggestionExtended(suggestion);
        expectedChangedSuggestion.setSuggestionStatus(newStatus);
        expectedChangedSuggestion.setLastSuggestionActivity(nowTime);
        expectedChangedSuggestion.getSuggestionStatusRecords().add(0, ClientSuggestionStatusRecord.builder()
                .created(nowTime)
                .status(newStatus)
                .build());

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), newStatus))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionStatusChangeConfirmation(expectedChangedSuggestion),
                        "changedSuggestion.suggestionStatusRecords[0].id"));

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEventToGeoArea = testClientPushService.getPushedEventToGeoAreasSilent(
                suggestion.getGeoAreas(),
                suggestion.isHiddenForOtherHomeAreas(),
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);

        ClientPostChangeConfirmation pushedEventToPerson = testClientPushService.getPushedEventToPersonLoud(
                suggestion.getCreator(),
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);

        assertEquals(suggestion.getId(), pushedEventToGeoArea.getPostId());
        assertEquals(suggestion.getId(), pushedEventToGeoArea.getPost().getSuggestion().getId());

        assertEquals(suggestion.getId(), pushedEventToPerson.getPostId());
        assertEquals(suggestion.getId(), pushedEventToPerson.getPost().getSuggestion().getId());

        testClientPushService.assertNoMorePushedEvents();

        newStatus = SuggestionStatus.IN_PROGRESS;
        nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        expectedChangedSuggestion.setSuggestionStatus(newStatus);
        expectedChangedSuggestion.setLastSuggestionActivity(nowTime);
        expectedChangedSuggestion.getSuggestionStatusRecords().add(0, ClientSuggestionStatusRecord.builder()
                .created(nowTime)
                .status(newStatus)
                .build());

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), newStatus))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionStatusChangeConfirmation(expectedChangedSuggestion),
                        "changedSuggestion.suggestionStatusRecords[0].id",
                        "changedSuggestion.suggestionStatusRecords[1].id"));

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent2 = testClientPushService.getPushedEventToGeoAreasSilent(
                suggestion.getGeoAreas(),
                suggestion.isHiddenForOtherHomeAreas(),
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);

        ClientPostChangeConfirmation pushedEventToPerson2 = testClientPushService.getPushedEventToPersonLoud(
                suggestion.getCreator(),
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);

        assertEquals(suggestion.getId(), pushedEvent2.getPostId());
        assertEquals(suggestion.getId(), pushedEvent2.getPost().getSuggestion().getId());

        assertEquals(suggestion.getId(), pushedEventToPerson2.getPostId());
        assertEquals(suggestion.getId(), pushedEventToPerson2.getPost().getSuggestion().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionStatusChangeRequest_SameStatus() throws Exception {

        Suggestion suggestion = th.suggestionWish2;

        assertEquals(SuggestionStatus.OPEN, suggestion.getSuggestionStatus());

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), SuggestionStatus.OPEN))))
                .andExpect(isException("status", ClientExceptionType.SUGGESTION_STATUS_CHANGE_INVALID));
    }

    @Test
    public void suggestionStatusChangeRequest_InvalidTransition() throws Exception {

        Suggestion suggestion = th.suggestionIdea2;

        assertEquals(SuggestionStatus.DONE, suggestion.getSuggestionStatus());

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), SuggestionStatus.OPEN))))
                .andExpect(isException("status", ClientExceptionType.SUGGESTION_STATUS_CHANGE_INVALID));
    }

    @Test
    public void suggestionStatusChangeRequest_PersonWithoutRoles() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;

        assertEquals(SuggestionStatus.OPEN, suggestion.getSuggestionStatus());

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personNinaBauer))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), SuggestionStatus.IN_PROGRESS))))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void suggestionStatusChangeRequest_MissingAttributes() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), null))))
                .andExpect(isException("newStatus", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(null, SuggestionStatus.OPEN))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest("", SuggestionStatus.OPEN))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionStatusChangeRequest_NotAuthenticated() throws Exception {

        Suggestion suggestion = th.suggestionDefect2;

        assertEquals(SuggestionStatus.OPEN, suggestion.getSuggestionStatus());

        assertOAuth2(post("/grapevine/event/suggestionStatusChangeRequest")
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(),
                        SuggestionStatus.IN_PROGRESS))));
    }

    @Test
    public void suggestionWithdrawRequest_WithdrawOwnSuggestion() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;
        final Person withdrawingPerson = suggestion.getCreator();
        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        final String reason = "War meine Schuld. Sie hat meine 200kg nicht ausgehalten. Hab sie selbst reparieren " +
                "lassen.";

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(withdrawingPerson))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), reason))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.suggestionId").value(suggestion.getId()))
                .andReturn();

        // verify push message
        waitForEventProcessing();

        ClientPostDeleteConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                suggestion.getGeoAreas(),
                suggestion.isHiddenForOtherHomeAreas(),
                ClientPostDeleteConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertEquals(suggestion.getId(), pushedEvent.getPostId());

        testClientPushService.assertNoMorePushedEvents();

        Suggestion updatedSuggestion = (Suggestion) th.postRepository.findById(suggestion.getId()).get();
        assertThat(updatedSuggestion.getWithdrawReason()).isEqualTo(reason);
        assertThat(updatedSuggestion.getWithdrawn()).isEqualTo(nowTime);
        assertTrue(updatedSuggestion.isOverrideDeletedForSuggestionRoles());
        updatedSuggestion.setSuggestionStatusRecords(suggestion.getSuggestionStatusRecords());
        updatedSuggestion.setSuggestionWorkerMappings(suggestion.getSuggestionWorkerMappings());

        // test that extended suggestion GET endpoint also returns the withdrawn* fields
        mockMvc.perform(get("/grapevine/suggestion/{suggestionId}", suggestion.getId())
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientSuggestionExtended(updatedSuggestion)));

        // Check that no push is sent on change request
        SuggestionStatus newStatus = SuggestionStatus.DONE;
        mockMvc.perform(post("/grapevine/event/suggestionStatusChangeRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionStatusChangeRequest(suggestion.getId(), newStatus))))
                .andExpect(status().isOk());

        // verify no push messages are sent
        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void suggestionWithdrawRequest_WithdrawerIsNotCreator() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;
        Person withdrawingPerson = th.personSusanneChristmann;
        //verify test data integrity
        assertThat(withdrawingPerson).isNotEqualTo(suggestion.getCreator());

        final String reason = "Irgendjemand scheint die Schaukel repariert zu haben. Das hier kann geschlossen werden.";

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(withdrawingPerson))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), reason))))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));

        Suggestion suggestionReloaded = (Suggestion) th.postRepository.findById(suggestion.getId()).get();
        assertThat(suggestionReloaded.getWithdrawReason()).isNull();
        assertFalse(suggestionReloaded.isOverrideDeletedForSuggestionRoles());
    }

    @Test
    public void suggestionWithdrawRequest_MaximalReasonLength() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;

        final String maxLengthReason = Stream.generate(() -> "x")
                .limit(th.grapevineConfig.getMaxNumberCharsPerComment())
                .collect(Collectors.joining());

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), maxLengthReason))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.suggestionId").value(suggestion.getId()));

        final String tooLongReason = maxLengthReason + "y";
        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), tooLongReason))))
                .andExpect(isException("reason", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionWithdrawRequest_MissingOrWrongAttributes() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), null))))
                .andExpect(isException("reason", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), ""))))
                .andExpect(isException("reason", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(null, "Nicht mehr aktuell"))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWithdrawRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest("", "Nicht mehr aktuell"))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionWithdrawRequest_NotAuthenticated() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;

        assertOAuth2(post("/grapevine/event/suggestionWithdrawRequest")
                .contentType(contentType)
                .content(json(new ClientSuggestionWithdrawRequest(suggestion.getId(), "Nicht mehr aktuell"))));
    }

    @Test
    public void suggestionCategoryChangeRequest_WorkerAndFirstResponder() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        SuggestionCategory newCategory = th.suggestionCategoryDamage;
        //verify test data integrity
        assertThat(newCategory).isNotEqualTo(suggestion.getSuggestionCategory());

        //first: we _do_ change the category
        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        ClientSuggestionExtended expectedChangedSuggestion = th.toClientSuggestionExtended(suggestion);
        expectedChangedSuggestion.setSuggestionCategory(th.toClientSuggestionCategory(newCategory));

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(), newCategory.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionCategoryChangeConfirmation(expectedChangedSuggestion)));

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(suggestion.getGeoAreas(),
                        suggestion.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(suggestion.getId(), pushedEvent.getPostId());
        assertEquals(suggestion.getId(), pushedEvent.getPost().getSuggestion().getId());

        testClientPushService.assertNoMorePushedEvents();

        //second: we do _not_ change the category by sending the same category again. Thus, the
        //        lastModified timestamp still has the value of the last expectedChangedSuggestion
        nowTime = nowTime + 1000;
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(), newCategory.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionCategoryChangeConfirmation(expectedChangedSuggestion)));
    }

    @Test
    public void postDeleteRequest_NotAllowed() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;

        mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                .headers(authHeadersFor(suggestion.getCreator(), th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .postId(suggestion.getId())
                        .build())))
                .andExpect(isException(ClientExceptionType.SUGGESTION_CANNOT_BE_DELETED));

        assertTrue(th.postRepository.findByIdAndDeletedFalse(suggestion.getId()).isPresent());
    }

    @Test
    public void suggestionCategoryChangeRequest_UnknownCategory() throws Exception {

        Suggestion suggestion = th.suggestionIdea2;

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(th.personSuggestionFirstResponderTenant1Hilde))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(), "abc"))))
                .andExpect(isException("abc", ClientExceptionType.SUGGESTION_CATEGORY_NOT_FOUND));
    }

    @Test
    public void suggestionCategoryChangeRequest_PersonWithoutRoles() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(th.personNinaBauer))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(),
                        th.suggestionCategoryWish.getId()))))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void suggestionCategoryChangeRequest_MissingAttributes() throws Exception {

        final Suggestion suggestion = th.suggestionDefect1;

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(), null))))
                .andExpect(isException("newSuggestionCategoryId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(), ""))))
                .andExpect(isException("newSuggestionCategoryId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(null, th.suggestionCategoryWish.getId()))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionCategoryChangeRequest")
                .headers(authHeadersFor(suggestion.getCreator()))
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest("", th.suggestionCategoryWish.getId()))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void suggestionCategoryChangeRequest_NotAuthenticated() throws Exception {

        Suggestion suggestion = th.suggestionDefect1;

        assertOAuth2(post("/grapevine/event/suggestionCategoryChangeRequest")
                .contentType(contentType)
                .content(json(new ClientSuggestionCategoryChangeRequest(suggestion.getId(),
                        th.suggestionCategoryWish.getId()))));
    }

    @Test
    public void suggestionWorkerAddRequest_FirstResponder() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        Person initiator = th.personSuggestionFirstResponderTenant1Hilde;
        suggestionWorkerAddRequest(suggestion, workerToAdd, initiator);
    }

    @Test
    public void suggestionWorkerAddRequest_Worker() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        Person initiator = th.personSuggestionWorkerTenant1Dieter;

        //ensure the worker to add is not already contained
        suggestionWorkerAddRequest(suggestion, workerToAdd, initiator);
    }

    private void suggestionWorkerAddRequest(Suggestion suggestion, Person workerToAdd, Person initiator)
            throws Exception {
        //ensure the worker to add is not already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToAdd)));

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        ClientSuggestionExtended expectedChangedSuggestion = th.toClientSuggestionExtended(suggestion);
        expectedChangedSuggestion.setLastSuggestionActivity(nowTime);
        expectedChangedSuggestion.getSuggestionWorkerMappings().
                add(ClientSuggestionWorkerMapping.builder()
                        .created(nowTime)
                        .worker(th.toClientPersonReferenceWithEmail(workerToAdd))
                        .build());

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(initiator))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToAdd.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionWorkerAddConfirmation(expectedChangedSuggestion),
                        "changedSuggestion.suggestionWorkerMappings[2].id"));

        List<SuggestionWorkerMapping> workerMappings =
                th.suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion);
        assertEquals(4, workerMappings.size());
        SuggestionWorkerMapping newWorkerMapping = workerMappings.get(3);
        assertEquals(workerToAdd, newWorkerMapping.getWorker());
        assertEquals(initiator, newWorkerMapping.getInitiator());
        assertEquals(nowTime, newWorkerMapping.getCreated());
        assertTrue(newWorkerMapping.isActive());

        Suggestion changedSuggestion = (Suggestion) th.postRepository.findById(suggestion.getId()).get();
        Chat internalWorkerChat = changedSuggestion.getInternalWorkerChat();
        Set<ChatParticipant> internalWorkerChatParticipants = internalWorkerChat.getParticipants();
        Set<Person> expectedWorkers = workerMappings.stream()
                .filter(SuggestionWorkerMapping::isActive)
                .map(SuggestionWorkerMapping::getWorker)
                .collect(Collectors.toSet());
        Set<Person> actualChatParticipants = internalWorkerChatParticipants.stream()
                .map(ChatParticipant::getPerson)
                .collect(Collectors.toSet());
        assertThat(actualChatParticipants).containsExactlyInAnyOrderElementsOf(expectedWorkers);
    }

    @Test
    public void suggestionWorkerAddRequest_AlreadyWorker() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Horst;
        //ensure the worker to add is already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .anyMatch(swm -> swm.getWorker().equals(workerToAdd)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToAdd.getId()))))
                .andExpect(isException(ClientExceptionType.SUGGESTION_WORKER_ALREADY_WORKING_ON_SUGGESTION));
    }

    @Test
    public void suggestionWorkerAddRequest_PersonWithoutRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        //ensure the worker to add is not already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToAdd)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personNinaBauer))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToAdd.getId()))))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void suggestionWorkerAddRequest_PersonWithRoleInDifferentTenant() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        //ensure the worker to add is not already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToAdd)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToAdd.getId()))))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void suggestionWorkerAddRequest_WrongOrMissingAttributes() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        //ensure the worker to add is not already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToAdd)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest("", workerToAdd.getId()))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(null, workerToAdd.getId()))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), ""))))
                .andExpect(isException("workerPersonId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), null))))
                .andExpect(isException("workerPersonId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(UUID.randomUUID().toString(), workerToAdd.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant1Dieter))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), UUID.randomUUID().toString()))))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void suggestionWorkerAddRequest_NotAuthenticated() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToAdd = th.personSuggestionWorkerTenant1Dieter;
        //ensure the worker to add is not already contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToAdd)));

        assertOAuth2(post("/grapevine/event/suggestionWorkerAddRequest")
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToAdd.getId()))));
    }

    @Test
    public void suggestionWorkerRemoveRequest_FirstResponder() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personSuggestionFirstResponderTenant1Hilde;
        suggestionWorkerRemoveRequest(suggestion, workerToRemove);
    }

    @Test
    public void suggestionWorkerRemoveRequest_Worker() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personSuggestionWorkerTenant1Horst;
        suggestionWorkerRemoveRequest(suggestion, workerToRemove);
    }

    private void suggestionWorkerRemoveRequest(Suggestion suggestion, Person workerToRemove) throws Exception {
        //ensure the worker to remove is contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .anyMatch(swm -> swm.getWorker().equals(workerToRemove)));

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(Instant.ofEpochMilli(nowTime).atZone(ZoneOffset.UTC));

        ClientSuggestionExtended expectedChangedSuggestion = th.toClientSuggestionExtended(suggestion);
        expectedChangedSuggestion.setLastSuggestionActivity(nowTime);
        expectedChangedSuggestion.setSuggestionWorkerMappings(
                expectedChangedSuggestion.getSuggestionWorkerMappings().stream()
                        .filter(swm -> !swm.getWorker().getId().equals(workerToRemove.getId()))
                        .collect(Collectors.toList()));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(suggestion.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionWorkerRemoveConfirmation(expectedChangedSuggestion)));

        List<SuggestionWorkerMapping> workerMappings =
                th.suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion);
        //the mapping should not get removed, but get inactivated, so we still have three
        assertEquals(3, workerMappings.size());
        List<SuggestionWorkerMapping> changedWorkerMappings = workerMappings.stream()
                .filter(swm -> swm.getWorker().equals(workerToRemove))
                .collect(Collectors.toList());
        assertEquals(1, changedWorkerMappings.size());
        SuggestionWorkerMapping changedWorkerMapping = changedWorkerMappings.get(0);
        assertEquals(Long.valueOf(nowTime), changedWorkerMapping.getInactivated());
        assertFalse(changedWorkerMapping.isActive());

        Suggestion changedSuggestion = (Suggestion) th.postRepository.findById(suggestion.getId()).get();
        Chat internalWorkerChat = changedSuggestion.getInternalWorkerChat();
        Set<ChatParticipant> internalWorkerChatParticipants = internalWorkerChat.getParticipants();
        Set<Person> expectedWorkers = workerMappings.stream()
                .filter(SuggestionWorkerMapping::isActive)
                .map(SuggestionWorkerMapping::getWorker)
                .collect(Collectors.toSet());
        Set<Person> actualChatParticipants = internalWorkerChatParticipants.stream()
                .map(ChatParticipant::getPerson)
                .collect(Collectors.toSet());
        assertThat(actualChatParticipants).containsExactlyInAnyOrderElementsOf(expectedWorkers);
    }

    @Test
    public void suggestionWorkerRemoveRequest_RemoveAdd() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personSuggestionWorkerTenant1Horst;
        //ensure the worker to remove is contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .anyMatch(swm -> swm.getWorker().equals(workerToRemove)));

        assertEquals(3, th.suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion).size());

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(suggestion.getId()))))
                .andExpect(status().isOk());

        assertEquals(3, th.suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion).size());

        mockMvc.perform(post("/grapevine/event/suggestionWorkerAddRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerAddRequest(suggestion.getId(), workerToRemove.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new ClientSuggestionWorkerAddConfirmation(th.toClientSuggestionExtended(suggestion)),
                        "changedSuggestion.lastSuggestionActivity",
                        "changedSuggestion.suggestionWorkerMappings[1].id",
                        "changedSuggestion.suggestionWorkerMappings[1].created"));

        //ensure that the historic worker mapping was not re-used for adding the worker again
        assertEquals(4, th.suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion).size());
    }

    @Test
    public void suggestionWorkerRemoveRequest_WorkerNotInSuggestion() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personSuggestionWorkerTenant1Dieter;
        //ensure the worker to remove is not contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .noneMatch(swm -> swm.getWorker().equals(workerToRemove)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(suggestion.getId()))))
                .andExpect(isException(ClientExceptionType.SUGGESTION_WORKER_NOT_WORKING_ON_SUGGESTION));
    }

    @Test
    public void suggestionWorkerRemoveRequest_PersonWithoutRoles() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personNinaBauer;

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(suggestion.getId()))))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void suggestionWorkerRemoveRequest_WrongOrMissingAttributes() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;
        Person workerToRemove = th.personSuggestionWorkerTenant1Horst;
        //ensure the worker to remove is contained
        assertTrue(suggestion.getSuggestionWorkerMappings().stream()
                .anyMatch(swm -> swm.getWorker().equals(workerToRemove)));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(""))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(null))))
                .andExpect(isException("suggestionId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .headers(authHeadersFor(workerToRemove))
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(UUID.randomUUID().toString()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void suggestionWorkerRemoveRequest_NotAuthenticated() throws Exception {

        Suggestion suggestion = th.suggestionIdea1;

        assertOAuth2(post("/grapevine/event/suggestionWorkerRemoveRequest")
                .contentType(contentType)
                .content(json(new ClientSuggestionWorkerRemoveRequest(suggestion.getId()))));
    }

    private void assertAddressEqualsCustomLocationDefinition(ClientLocationDefinition customLocationDefinition,
            Address actualAddress) {
        assertEquals(customLocationDefinition.getLocationName(), actualAddress.getName());
        assertEquals(customLocationDefinition.getAddressStreet(), actualAddress.getStreet());
        assertEquals(customLocationDefinition.getAddressZip(), actualAddress.getZip());
        assertEquals(customLocationDefinition.getAddressCity(), actualAddress.getCity());
        assertEquals(customLocationDefinition.getGpsLocation().getLatitude(),
                actualAddress.getGpsLocation().getLatitude(), 0.01);
        assertEquals(customLocationDefinition.getGpsLocation().getLongitude(),
                actualAddress.getGpsLocation().getLongitude(), 0.01);
    }

}
