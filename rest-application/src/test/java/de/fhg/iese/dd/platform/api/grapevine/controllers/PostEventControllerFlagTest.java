/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostFlagRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlag;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagForFlagCreator;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagPageBean;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.config.UserGeneratedContentFlagConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostEventControllerFlagTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private UserGeneratedContentFlagRepository flagRepository;

    @Autowired
    private UserGeneratedContentFlagConfig userGeneratedContentFlagConfig;

    private Gossip flaggedEntity;
    private Person flagCreator;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
        th.createUserGeneratedContentAdmins();

        flaggedEntity = th.gossip1withImages;
        flagCreator = th.personLaraSchaefer;
        assertThat(flaggedEntity).isNotNull();
        assertThat(flagCreator).isNotNull();
        assertThat(flagCreator).isNotEqualTo(flaggedEntity.getCreator());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void flagGossip() throws Exception {

        //flag post
        String flagComment = "Der is echt gemein, ey!";
        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .comment(flagComment)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(createRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedEntity.getTenant().getId());
        String flagId = response.getUserGeneratedContentFlagId();
        assertTrue(flagRepository.existsById(flagId));
        UserGeneratedContentFlag flag = flagRepository.findById(flagId).get();
        assertEquals(flaggedEntity.getCreator(), flag.getEntityAuthor());
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        mockMvc.perform(get("/grapevine/post/" + flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.filterStatus").value(ClientFilterStatus.FLAGGED.name()));

        ClientUserGeneratedContentFlagPageBean pagedResult = toObject(mockMvc.perform(get("/adminui/flag")
                        .param("communityId", flaggedEntity.getTenant().getId())
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse(),
                ClientUserGeneratedContentFlagPageBean.class);
        assertThat(pagedResult.getContent()).hasSize(1);
        ClientUserGeneratedContentFlag clientFlag = pagedResult.getContent().get(0);
        assertThat(clientFlag.getId()).isEqualTo(flagId);
        assertThat(clientFlag.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(clientFlag.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(clientFlag.getFlagCreator().getId()).isEqualTo(flagCreator.getId());
        assertThat(clientFlag.getTenant().getId()).isEqualTo(flaggedEntity.getTenant().getId());
        assertThat(clientFlag.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(clientFlag.getLastStatusUpdate()).isGreaterThan(0L);
        assertEquals(flagComment, clientFlag.getLastStatusRecord().getComment());
        assertEquals(UserGeneratedContentFlagStatus.OPEN, clientFlag.getLastStatusRecord().getStatus());

        mockMvc.perform(get("/adminui/flag/{flagId}", flagId)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(flagId))
                .andExpect(jsonPath("$.entityId").value(flaggedEntity.getId()))
                .andExpect(jsonPath("$.entityType").value(flaggedEntity.getClass().getName()))
                .andExpect(jsonPath("$.entityAuthor.id").value(flaggedEntity.getCreator().getId()))
                .andExpect(jsonPath("$.flagCreator.id").value(flagCreator.getId()))
                .andExpect(jsonPath("$.tenant.id").value(flaggedEntity.getTenant().getId()))
                .andExpect(jsonPath("$.status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.lastStatusUpdate").value(flag.getLastStatusUpdate()))
                .andExpect(jsonPath("$.lastStatusRecord.status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.lastStatusRecord.comment").value(flagComment))
                .andExpect(jsonPath("$.statusRecords", hasSize(1)))
                .andExpect(jsonPath("$.statusRecords[0].status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.statusRecords[0].comment").value(flagComment));

        MvcResult flagListOwnContentResult = mockMvc.perform(get("/flag/")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        List<ClientUserGeneratedContentFlagForFlagCreator> flagListOwnContent =
                toListOfType(flagListOwnContentResult.getResponse(),
                        ClientUserGeneratedContentFlagForFlagCreator.class);
        ClientUserGeneratedContentFlagForFlagCreator clientFlagOwnContent = flagListOwnContent.get(0);
        assertThat(clientFlagOwnContent.getFlagEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(clientFlagOwnContent.getFlagEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(clientFlag.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
    }

    @Test
    public void flagGossip_MultipleTimes() throws Exception {

        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey!")
                .build();

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS));
    }

    @Test
    public void flagGossip_NoComment() throws Exception {

        //flag post without comment
        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/grapevine/post/{postId}", flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagGossip_CreatorSelfFlag() throws Exception {

        Person creator = flaggedEntity.getCreator();
        ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk());

        //the creator should not see his own flag
        mockMvc.perform(get("/grapevine/post/{postId}", flaggedEntity.getId())
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.filterStatus").doesNotExist());
    }

    @Test
    public void flagGossip_NoTenant() throws Exception {

        flaggedEntity.setTenant(null);
        th.postRepository.save(flaggedEntity);

        //flag post without comment
        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/grapevine/post/" + flaggedEntity.getId())
                .headers(authHeadersFor(flagCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagGossipUnauthorized() throws Exception {

        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedEntity.getId())
                .comment("Der is echt gemein, ey! Meine Mutter is nicht fett!")
                .build();

        assertOAuth2(post("/grapevine/event/postFlagRequest")
                .contentType(contentType)
                .content(json(flagRequest)));
    }

    @Test
    public void flagGossipMissingEventAttributes() throws Exception {

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(ClientPostFlagRequest.builder()
                        .postId(flaggedEntity.getId())
                        .comment(StringUtils.repeat('X',
                                userGeneratedContentFlagConfig.getMaxNumberCharsPerFlagComment() + 1))
                        .build())))
                .andExpect(isException("comment", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/postFlagRequest")
                .headers(authHeadersFor(flagCreator))
                .contentType(contentType)
                .content(json(ClientPostFlagRequest.builder()
                        .comment("Der is echt gemein, ey!")
                        .build())))
                .andExpect(isException("postId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

    }

    @Test
    public void flagNewsItem() throws Exception {
        flagExternalPost(th.newsItem2);
    }

    @Test
    public void flagHappening() throws Exception {
        flagExternalPost(th.happening2);
    }

    private void flagExternalPost(ExternalPost flaggedExternalPost) throws Exception {
        //flag external post
        String flagComment = "Das sowas von einer offiziellen Nachrichten-Seite kommt...";
        final ClientPostFlagRequest flagRequest = ClientPostFlagRequest.builder()
                .postId(flaggedExternalPost.getId())
                .comment(flagComment)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/postFlagRequest")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(createRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedExternalPost.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedExternalPost.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedExternalPost.getTenant().getId());
        String flagId = response.getUserGeneratedContentFlagId();
        assertTrue(flagRepository.existsById(flagId));
        UserGeneratedContentFlag flag = flagRepository.findById(flagId).get();
        assertNull(flag.getEntityAuthor());
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        String postTypePath = "newsItem";
        if(flaggedExternalPost instanceof Happening){
            postTypePath = "happening";
        }

        mockMvc.perform(get("/grapevine/post/" + flaggedExternalPost.getId())
                        .headers(authHeadersFor(flagCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$."+postTypePath+".filterStatus").value(ClientFilterStatus.FLAGGED.name()));

        ClientUserGeneratedContentFlagPageBean pagedResult = toObject(mockMvc.perform(get("/adminui/flag")
                                .param("communityId", flaggedExternalPost.getTenant().getId())
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1))
                                .contentType(contentType)
                                .content(json(flagRequest)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse(),
                ClientUserGeneratedContentFlagPageBean.class);
        assertThat(pagedResult.getContent()).hasSize(1);
        ClientUserGeneratedContentFlag clientFlag = pagedResult.getContent().get(0);
        assertThat(clientFlag.getId()).isEqualTo(flagId);
        assertThat(clientFlag.getEntityId()).isEqualTo(flaggedExternalPost.getId());
        assertThat(clientFlag.getEntityType()).isEqualTo(flaggedExternalPost.getClass().getName());
        assertThat(clientFlag.getFlagCreator().getId()).isEqualTo(flagCreator.getId());
        assertThat(clientFlag.getTenant().getId()).isEqualTo(flaggedExternalPost.getTenant().getId());
        assertThat(clientFlag.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
        assertThat(clientFlag.getLastStatusUpdate()).isGreaterThan(0L);
        assertEquals(flagComment, clientFlag.getLastStatusRecord().getComment());
        assertEquals(UserGeneratedContentFlagStatus.OPEN, clientFlag.getLastStatusRecord().getStatus());

        mockMvc.perform(get("/adminui/flag/{flagId}", flagId)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(flagId))
                .andExpect(jsonPath("$.entityId").value(flaggedExternalPost.getId()))
                .andExpect(jsonPath("$.entityType").value(flaggedExternalPost.getClass().getName()))
                .andExpect(jsonPath("$.flagCreator.id").value(flagCreator.getId()))
                .andExpect(jsonPath("$.tenant.id").value(flaggedExternalPost.getTenant().getId()))
                .andExpect(jsonPath("$.status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.lastStatusUpdate").value(flag.getLastStatusUpdate()))
                .andExpect(jsonPath("$.lastStatusRecord.status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.lastStatusRecord.comment").value(flagComment))
                .andExpect(jsonPath("$.statusRecords", hasSize(1)))
                .andExpect(jsonPath("$.statusRecords[0].status").value(UserGeneratedContentFlagStatus.OPEN.name()))
                .andExpect(jsonPath("$.statusRecords[0].comment").value(flagComment));

        MvcResult flagListOwnContentResult = mockMvc.perform(get("/flag/")
                        .headers(authHeadersFor(flagCreator))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk())
                .andReturn();
        List<ClientUserGeneratedContentFlagForFlagCreator> flagListOwnContent =
                toListOfType(flagListOwnContentResult.getResponse(),
                        ClientUserGeneratedContentFlagForFlagCreator.class);
        ClientUserGeneratedContentFlagForFlagCreator clientFlagOwnContent = flagListOwnContent.get(0);
        assertThat(clientFlagOwnContent.getFlagEntityId()).isEqualTo(flaggedExternalPost.getId());
        assertThat(clientFlagOwnContent.getFlagEntityType()).isEqualTo(flaggedExternalPost.getClass().getName());
        assertThat(clientFlag.getStatus()).isEqualTo(UserGeneratedContentFlagStatus.OPEN);
    }

}
