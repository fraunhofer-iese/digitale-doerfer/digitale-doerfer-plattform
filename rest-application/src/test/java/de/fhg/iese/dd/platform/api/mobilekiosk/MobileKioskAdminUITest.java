/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;

import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointsRequest;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;

public class MobileKioskAdminUITest extends BaseMobileKioskTest {

    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;

    @Autowired
    private IDeliveryService deliveryService;

    @Test
    public void deliveriesAndSellingPointsReturnsAllDifferentCases() throws Exception {
        final String tenDaysInFuture = String.valueOf(10 * 24 * 60 * 60 * 1000 + timeService.currentTimeMillisUTC());

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        final Delivery unconnectedDelivery = createDeliveryForSellingPoint(th.sp3);
        final Delivery connectedDelivery = createDeliveryAndConnectToSellingPoint(th.sp5);
        final Delivery deliverySellingPointDeleted = createDeliveryConnectedToSp4ReadyForTransportWithSellingPointDeleted();

        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("fromTime", "0")
                .param("toTime", tenDaysInFuture)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].ordered", notNullValue()))
                .andExpect(jsonPath("$[0].currentStatus", is("IN_DELIVERY")))
                .andExpect(jsonPath("$[0].deliveryId", is(deliverySellingPointDeleted.getId())))
                .andExpect(jsonPath("$[0].receiverId", notNullValue()))
                .andExpect(jsonPath("$[0].senderShopId", notNullValue()))
                .andExpect(jsonPath("$[0].sellingPointId", is(th.sp4.getId())))
                .andExpect(jsonPath("$[0].sellingPointStatus", is("DELETED")))
                .andExpect(jsonPath("$[1].ordered", notNullValue()))
                .andExpect(jsonPath("$[1].currentStatus", is("CREATED")))
                .andExpect(jsonPath("$[1].deliveryId", is(connectedDelivery.getId())))
                .andExpect(jsonPath("$[1].sellingPointId", is(th.sp5.getId())))
                .andExpect(jsonPath("$[1].sellingPointStatus", is("ACTIVE")))
                .andExpect(jsonPath("$[2].ordered", notNullValue()))
                .andExpect(jsonPath("$[2].currentStatus", is("CREATED")))
                .andExpect(jsonPath("$[2].deliveryId", is(unconnectedDelivery.getId())))
                .andExpect(jsonPath("$[2].sellingPointId", nullValue()))
                .andExpect(jsonPath("$[2].sellingPointStatus", nullValue()));

        timeServiceMock.resetOffset();
    }

    @Test
    public void deliveriesCalledWithWrongCommunityId() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .param("communityId", "not existing")
                .param("fromTime", "0")
                .param("toTime", "1")
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void deliveriesCalledUnauthenticated() throws Exception {
        assertOAuth2(get("/mobile-kiosk/adminui/delivery")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("fromTime", "0")
                .param("toTime", "1")
                .contentType(contentType));
    }

    @Test
    public void deliveriesCalledOfAPersonThatIsNotVgAdminInCurrentCommunity() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("fromTime", "0")
                .param("toTime", "1")
                .contentType(contentType))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.message", containsString("Person must be VG admin")));

        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personRegularAnna))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("fromTime", "0")
                .param("toTime", "1")
                .contentType(contentType))
                .andExpect(status().isForbidden())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.message", containsString("Person must be VG admin")));
    }

    @Test
    public void deliveriesCalledWithMissingParameters() throws Exception {
        //communityId missing
        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .param("fromTime", "0")
                .param("toTime", "1")
                .contentType(contentType))
                .andExpect(status().isBadRequest());

        //fromTime missing
        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("toTime", "1")
                .contentType(contentType))
                .andExpect(status().isBadRequest());

        //toTime missing
        mockMvc.perform(get("/mobile-kiosk/adminui/delivery")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("fromTime", "0")
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    private Delivery createDeliveryAndConnectToSellingPoint(SellingPoint sp) throws Exception {

        final Delivery delivery = createDeliveryForSellingPoint(sp);

        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest
                .builder()
                .deliveryId(delivery.getId())
                .sellingPointId(sp.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId", is(delivery.getId())));

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery));

        return delivery;
    }

    private Delivery createDeliveryConnectedToSp4ReadyForTransportWithSellingPointDeleted() throws Exception {
        final Delivery delivery = createDeliveryAndConnectToSellingPoint(th.sp4);

        //mark as ready for transport in order to have the content notes and tracking code fields filled
        markDeliveryAsReadyForTransport(delivery);
        delivery1ForSP4 = deliveryService.refresh(delivery);
        DeliveryStatus currentStatus = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(delivery).getStatus();

        assertSame(DeliveryStatus.IN_DELIVERY, currentStatus);

        ClientUpdateSellingPointsRequest sellingPointsRequest = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Lists.newArrayList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        //point 4 is deleted
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp8),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp9),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp10)
                )).build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(sellingPointsRequest)))
                .andExpect(status().isOk());

        return delivery;
    }

}
