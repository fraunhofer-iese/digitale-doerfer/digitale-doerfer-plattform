/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.contentintegration.ContentIntegrationTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.KatwarnConstants;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.feature.KatwarnFeature;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ExternalPostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class KatwarnAlertControllerTest extends BaseServiceTest {

    @Autowired
    private ContentIntegrationTestHelper th;

    @Autowired
    private ExternalPostRepository externalPostRepository;
    private AppVariant appVariantKatwarn;
    private NewsSource newsSourceKatwarn;
    private Organization organizationKatwarn;
    private static final String CONTENT_TYPE_KATWARN = "application/vnd.kwrn.v2+json";
    private static final String alertId1 = "7e5f5e0fa6358c7d6f1783e7153a6dc1ffb4aeef";
    private static final String alertId2 = "7e5f5e0fa6358c7d6f1783e7153a6dc1ffb4aeef";
    private static final String alertIdPartialMatch = "d7d6fb77-4349-42bc-a9b5-34c1d8809ddg";
    private static final String alertIdDwd = "6606e69125293717f6031569";

    protected static final String HEADER_NAME_API_KEY_KATWARN = "Authorization";

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createKatwarnEventDetails();
        createNewsSourceAndOrganization();
        createFeatureConfig();
    }

    public void createNewsSourceAndOrganization() {

        App katwarnApp = th.appRepository.save(App.builder()
                .id(KatwarnConstants.APP_ID)
                .name("Katwarn-App")
                .appIdentifier("katwarn.app")
                .build());
        appVariantKatwarn = th.appVariantRepository.save(AppVariant.builder()
                .name("Katwarn-AppVariant")
                .appVariantIdentifier("katwarn.appVariant")
                .app(katwarnApp)
                .apiKey1(UUID.randomUUID().toString())
                .build());
        th.mapGeoAreaToAppVariant(appVariantKatwarn, th.geoAreaMainz, th.tenant1);
        th.mapGeoAreaToAppVariant(appVariantKatwarn, th.geoAreaKaiserslautern, th.tenant1);
        newsSourceKatwarn = th.newsSourceRepository.save(NewsSource.builder()
                .siteUrl("https://katwarn.de")
                .siteName("Katwarn-Meldungen")
                .appVariant(appVariantKatwarn)
                .build());
        organizationKatwarn = Organization.builder()
                .name("Katwarn-Organization")
                .description("-")
                .locationDescription("-")
                .build();
        organizationKatwarn.getTags().addValue(OrganizationTag.CIVIL_PROTECTION);
        organizationKatwarn = th.organizationRepository.saveAndFlush(organizationKatwarn);
        th.newsSourceOrganizationMappingRepository.save(NewsSourceOrganizationMapping.builder()
                .newsSource(newsSourceKatwarn)
                .organization(organizationKatwarn)
                .build());
    }

    public void createFeatureConfig() {

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(KatwarnFeature.class.getName())
                .appVariant(appVariantKatwarn)
                .enabled(true)
                .build());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void katwarnAlertRequest_PushEvent() throws Exception {

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);
        String expectedIssuer = "Landkreis Osterwald";
        String expectedText = """
                Landkreis Osterwald: Warnung vor Großbrand
                                
                BESCHREIBUNG:
                Im Industriegebiet Nordstadt-Süd sind bei einem Brand einer Lagerhalle ...
                                
                HANDLUNGSEMPFEHLUNGEN:
                - Fenster und Türen schließen
                - Radio einschalten
                                
                BETROFFENE REGIONEN:
                Teile von Osterwald, Westerberg a.d. See und Umgebung
                                
                KONTAKT:
                Gefahrenschutz Kaiserslautern
                012120348301
                """;

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem createdNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(createdNewsItem).isNotNull();
        assertThat(createdNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(createdNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(createdNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/7b3f2e402ebdd8ba8d0f262a3617892322665ba4");
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(1509893642000L);
        assertThat(createdNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(createdNewsItem.getText()).isEqualTo(expectedText);
        assertThat(createdNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(createdNewsItem.getCategories()).isEqualTo("Großbrand: Erhöhte Gefahr!");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentId",
                "7e5f5e0fa6358c7d6f1783e7153a6dc1ffb4aeef");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "SEVERE");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "WARNING");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategory", "Erhöhte Gefahr!");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategoryColor", "#D94247");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "fire");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCodeText", "Großbrand");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.issuer", expectedIssuer);
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.effective", 1505893235000L);
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.expires", 1509893642000L);

        Pair<String, ClientPostCreateConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostCreateConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(createdNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(createdNewsItem.getText());

        assertThat(pushedMessage).isEqualTo("ERHÖHTE GEFAHR! | Landkreis Osterwald: Warnung vor Großbrand");

        //testing the customizations of the site details
        mockMvc.perform(get("/grapevine/post/{postId}", createdNewsItem.getId())
                        .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.newsItem.siteName").value(expectedIssuer))
                .andExpect(jsonPath("$.newsItem.sitePicture.id").value(th.katwarnEventDetailFire.getIcon().getId()));
    }

    @Test
    public void katwarnAlertRequest_NoPushEvent() throws Exception {

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);
        String expectedIssuer = "Landkreis Winterwald";
        String expectedText = """
                Landkreis Winterwald: Warnung vor Schneesturm
                                
                HANDLUNGSEMPFEHLUNGEN:
                - Fenster und Türen schließen
                - Radio einschalten
                - Mütze aufsetzen
                """;

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert_NoNotification.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem createdNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(createdNewsItem).isNotNull();
        assertThat(createdNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(createdNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(createdNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/7b3f2e402ebdd8ba8d0f262a3617892322665ba4");
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(1509893642000L);
        assertThat(createdNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(createdNewsItem.getText()).isEqualTo(expectedText);
        assertThat(createdNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.issuer", expectedIssuer);
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "blizzard");

        ClientPostCreateConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                expectedGeoAreas, ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        assertThat(pushedEvent.getPostId()).isEqualTo(createdNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(createdNewsItem.getText());
        testClientPushService.assertNoMorePushedEvents();

        //testing the customizations of the site details
        mockMvc.perform(get("/grapevine/post/{postId}", createdNewsItem.getId())
                        .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.newsItem.siteName").value(expectedIssuer))
                .andExpect(
                        jsonPath("$.newsItem.sitePicture.id").value(th.katwarnEventDetailBlizzard.getIcon().getId()));
    }

    @Test
    public void katwarnAlertRequest_InstructionsDWD() throws Exception {

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);
        String expectedIssuer = "Deutscher Wetterdienst";
        String expectedText = """
                DWD meldet über 1500 m: Amtliche UNWETTERWARNUNG vor ORKANARTIGEN BÖEN vom 30.03.2024 02:00 - 30.03.2024 20:00.
                                
                BESCHREIBUNG:
                Deutscher Wetterdienst meldet - Für Lagen über 1500 m: Es treten oberhalb 1500 m orkanartige Böen mit Geschwindigkeiten zwischen 80 km/h (22 m/s, 44 kn, Bft 9) und 105 km/h (29 m/s, 56 kn, Bft 11) aus südlicher Richtung auf. In exponierten Lagen muss mit Orkanböen bis 120 km/h (33 m/s, 64 kn, Bft 12) gerechnet werden.
                                
                HANDLUNGSEMPFEHLUNGEN:
                - Gefahr für Leib und Leben durch umstürzende Bäume, Hochspannungsleitungen und Gerüste; herabfallende Äste, Dachziegel und andere größere Gegenstände.
                - Aufenthalt im Freien vermeiden oder Schutz suchen (z.B. in Gebäuden)
                - Verhalten im Straßenverkehr anpassen, Behinderungen auf Verkehrswegen einplanen
                - Wenn ausreichend Zeit bleibt: Gegenstände im Freien sichern oder abbauen
                - Ausreichend Abstand von Gebäuden, Bäumen, Gerüsten und Hochspannungsleitungen halten
                - Alle Fenster und Türen schließen
                                
                KONTAKT:
                Deutscher Wetterdienst
                """;

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert_DWD.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem createdNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertIdDwd).get();

        assertThat(createdNewsItem).isNotNull();
        assertThat(createdNewsItem.getExternalId()).isEqualTo(alertIdDwd);
        assertThat(createdNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(createdNewsItem.getUrl()).isEqualTo("https://dwd.de/warnungen");
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(1509893642000L);
        assertThat(createdNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(createdNewsItem.getText()).isEqualTo(expectedText);
        assertThat(createdNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(createdNewsItem.getCategories()).isEqualTo("Extremwetter: Erhöhte Gefahr!");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentId",
                "6606e69125293717f6031569");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "SEVERE");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "WARNING");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategory", "Erhöhte Gefahr!");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategoryColor", "#D94247");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "storm");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCodeText", "Extremwetter");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.issuer", expectedIssuer);
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.effective", 1505893235000L);
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.expires", 1509893642000L);

        Pair<String, ClientPostCreateConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostCreateConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(createdNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(createdNewsItem.getText());

        assertThat(pushedMessage).isEqualTo(
                "ERHÖHTE GEFAHR! | DWD meldet über 1500 m: Amtliche UNWETTERWARNUNG vor ORKANARTIGEN BÖEN vom 30.03.2024 02:00 - 30.03.2024 20:00.");

        //testing the customizations of the site details
        mockMvc.perform(get("/grapevine/post/{postId}", createdNewsItem.getId())
                        .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.newsItem.siteName").value(expectedIssuer))
                .andExpect(jsonPath("$.newsItem.sitePicture.id").value(th.katwarnEventDetailStorm.getIcon().getId()));
    }

    @Test
    public void katwarnAlertRequest_MultiPolygon() throws Exception {

        String expectedIssuer = "Amt für Geo Areas";

        // katwarn uses either Polygon or MultiPolygon
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert2.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem createdNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId2).get();

        assertThat(createdNewsItem).isNotNull();
        assertThat(createdNewsItem.getExternalId()).isEqualTo(alertId2);
        assertThat(createdNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(createdNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/7e5f5e0fa6358c7d6f1783e7153a6dc1ffb4aeef");
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(1509893642000L);
        assertThat(createdNewsItem.getGeoAreas()).containsOnly(th.geoAreaMainz, th.geoAreaKaiserslautern);
        assertThat(createdNewsItem.getText()).contains("Meldung trifft mehrere Geo Areas",
                "Mehrere Treffer bei Geo Areas");
        assertThat(createdNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "EXTREME");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "WARNING");
        assertThat(createdNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "lavaFlow");

        //testing the customizations of the site details
        mockMvc.perform(get("/grapevine/post/{postId}", createdNewsItem.getId())
                        .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.newsItem.siteName").value(expectedIssuer))
                .andExpect(
                        jsonPath("$.newsItem.sitePicture.id").value(th.katwarnEventDetailLavaFlow.getIcon().getId()));
    }

    @Test
    public void katwarnAlertRequest_PartialMatch() throws Exception {

        // alert geo areas: geoAreaRheinlandPfalz, geoAreaEisenberg
        // available geo areas for app variant: geoAreaMainz, geoAreaKaiserslautern
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert_PartialMatch.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem createdNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertIdPartialMatch).get();

        assertThat(createdNewsItem).isNotNull();
        assertThat(createdNewsItem.getExternalId()).isEqualTo(alertIdPartialMatch);
        assertThat(createdNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(createdNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/d7d6fb77-4349-42bc-a9b5-34c1d8809ddg");
        assertThat(createdNewsItem.getDesiredUnpublishTime()).isEqualTo(1509893642000L);
        assertThat(createdNewsItem.getGeoAreas()).containsOnly(th.geoAreaMainz, th.geoAreaKaiserslautern);
        assertThat(createdNewsItem.getText()).contains("Meldung trifft nur verfügbare Geo Areas",
                "Treffer bei verfügbaren Geo Areas");
        assertThat(createdNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
    }

    @Test
    public void katwarnAlertRequest_DisabledFeature() throws Exception {

        featureConfigRepository.deleteAllByAppVariant(appVariantKatwarn);

        long numExistingPostsBefore = th.postRepository.count();

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();
        long numExistingPostsAfter = th.postRepository.count();
        assertThat(numExistingPostsBefore).isEqualTo(numExistingPostsAfter);
    }

    @Test
    public void katwarnAlertRequest_NoMatch() throws Exception {

        long numExistingPostsBefore = th.postRepository.count();
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert_NoMatch.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();
        long numExistingPostsAfter = th.postRepository.count();
        assertThat(numExistingPostsBefore).isEqualTo(numExistingPostsAfter);
    }

    @Test
    public void katwarnAlertRequest_TestAlert() throws Exception {

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert_Test.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        Optional<ExternalPost> createdNewsItem = externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1);

        // alert status different to 'actual' -> no news item created
        assertThat(createdNewsItem).isNotPresent();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void katwarnAlertRequest_UpdateInfo() throws Exception {

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);
        String expectedText = """
                Landkreis Osterwald: Aktualisierung Großbrand
                                
                BESCHREIBUNG:
                Der Brand der Lagerhalle ist unter Kontrolle ...
                                
                HANDLUNGSEMPFEHLUNG:
                - Radio einschalten
                """;

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(expectedGeoAreas)
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository.findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();
        long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1_UpdateInfo.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/96fe21ab98a79e97210ccfe7563b6076ad203ead");
        assertThat(updatedNewsItem.getDesiredUnpublishTime()).isNull();
        assertThat(updatedNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(updatedNewsItem.getText()).isEqualTo(expectedText);
        assertThat(updatedNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(updatedNewsItem.getLastActivity()).isEqualTo(now);
        assertThat(updatedNewsItem.getCategories()).isEqualTo("Großbrand: Information");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "MODERATE");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "INFO");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategory", "Information");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategoryColor", "#0098D4");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "fire");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCodeText", "Großbrand");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.effective", 1506066703000L);
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.expires", null);

        Pair<String, ClientPostChangeConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostChangeConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());

        assertThat(pushedMessage)
                .isEqualTo("Aktualisierung: INFORMATION | Landkreis Osterwald: Aktualisierung Großbrand");
    }

    @Test
    public void katwarnAlertRequest_UpdateClear() throws Exception {

        long now = timeServiceMock.currentTimeMillisUTC();
        timeServiceMock.setConstantDateTime(now);

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);
        String expectedText = """
                Landkreis Osterwald: Aufhebung Großbrand
                                
                BESCHREIBUNG:
                Der Brand der Lagerhalle ist unter Kontrolle ...
                """;

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(expectedGeoAreas)
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository.findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1_UpdateClear.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/96fe21ab98a79e97210ccfe7563b6076ad203ead");
        // the alert request has no expiration time, so we use (now + 30 min.)
        assertThat(updatedNewsItem.getDesiredUnpublishTime()).isEqualTo(now + TimeUnit.MINUTES.toMillis(30));
        assertThat(updatedNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(updatedNewsItem.getText()).isEqualTo(expectedText);
        assertThat(updatedNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "MODERATE");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "CLEAR");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategory", "Entwarnung!");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategoryColor", "#4AABA4");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "fire");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCodeText", "Großbrand");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.effective", 1506066703000L);

        Pair<String, ClientPostChangeConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostChangeConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());

        assertThat(pushedMessage).isEqualTo(
                "Aktualisierung: ENTWARNUNG! | Landkreis Osterwald: Aufhebung Großbrand");
    }

    @Test
    public void katwarnAlertRequest_Update_ChangingGeoAreas() throws Exception {

        long now = timeServiceMock.currentTimeMillisUTC();
        timeServiceMock.setConstantDateTime(now);

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(Set.of(th.geoAreaKaiserslautern, th.geoAreaEisenberg))
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository.findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1_UpdateClear.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);

        Pair<String, ClientPostCreateConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostCreateConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());


        assertThat(pushedMessage)
                .isEqualTo("Aktualisierung: ENTWARNUNG! | Landkreis Osterwald: Aufhebung Großbrand");

        ClientPostDeleteConfirmation pushedDeleteEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                Set.of(th.geoAreaEisenberg), ClientPostDeleteConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);
        assertThat(pushedDeleteEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
    }

    @Test
    public void katwarnAlertRequest_Update_NoNotification() throws Exception {

        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern, th.geoAreaMainz);

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(expectedGeoAreas)
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository.findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();

        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource(
                                "contentintegration/katwarn/katwarnAlert1_Update_NoNotification.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();
        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getUrl())
                .isEqualTo("https://katwarn/de/infos/96fe21ab98a79e97210ccfe7563b6076ad203ead");
        assertThat(updatedNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());

        ClientPostChangeConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                expectedGeoAreas, ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void katwarnAlertRequest_Cancel() throws Exception {

        long now = timeServiceMock.currentTimeMillisUTC();
        timeServiceMock.setConstantDateTime(now);
        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern);
        String expectedText = """
                Landkreis Osterwald: Entwarnung Großbrand
                """;

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(Set.of(th.geoAreaKaiserslautern))
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository.findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1_Cancel.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getUrl()).isBlank();
        // the alert request has no expiration time, so we use (now + 30 min.)
        assertThat(updatedNewsItem.getDesiredUnpublishTime()).isEqualTo(now + TimeUnit.MINUTES.toMillis(30));
        assertThat(updatedNewsItem.getGeoAreas()).containsExactlyInAnyOrderElementsOf(expectedGeoAreas);
        assertThat(updatedNewsItem.getText()).isEqualTo(expectedText);
        assertThat(updatedNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());
        assertThat(updatedNewsItem.getCategories()).isEqualTo("Großbrand: Entwarnung!");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.severity", "MODERATE");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.contentType", "CLEAR");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategory", "Entwarnung!");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.incidentCategoryColor", "#4AABA4");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCode", "fire");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.eventCodeText", "Großbrand");
        assertThat(updatedNewsItem.getCustomAttributes()).containsEntry("katwarn.effective", 1506153406000L);

        Pair<String, ClientPostChangeConfirmation> pushedEventAndMessage = testClientPushService.getPushedEventAndMessageToGeoAreasLoud(
                null, expectedGeoAreas, false,
                ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        ClientPostChangeConfirmation pushedEvent = pushedEventAndMessage.getRight();
        String pushedMessage = pushedEventAndMessage.getLeft();
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());

        assertThat(pushedMessage).isEqualTo(
                "ENTWARNUNG! | Landkreis Osterwald: Entwarnung Großbrand");
    }

    @Test
    public void katwarnAlertRequest_Cancel_NoNotification() throws Exception {

        long now = timeServiceMock.currentTimeMillisUTC();
        timeServiceMock.setConstantDateTime(now);
        Set<GeoArea> expectedGeoAreas = Set.of(th.geoAreaKaiserslautern);

        externalPostRepository.save(NewsItem.builder()
                .externalId(alertId1)
                .url("https://example.org")
                .newsSource(newsSourceKatwarn)
                .geoAreas(Set.of(th.geoAreaKaiserslautern))
                .organization(organizationKatwarn)
                .build());

        assertThat(externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1)).isPresent();
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, appVariantKatwarn.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(th.loadTextTestResource(
                                "contentintegration/katwarn/katwarnAlert1_Cancel_NoNotification.json")))
                .andExpect(status().isCreated());

        waitForEventProcessing();

        NewsItem updatedNewsItem = (NewsItem) externalPostRepository
                .findByNewsSourceAndExternalId(newsSourceKatwarn, alertId1).get();

        assertThat(updatedNewsItem).isNotNull();
        assertThat(updatedNewsItem.getExternalId()).isEqualTo(alertId1);
        assertThat(updatedNewsItem.getOrganization()).isEqualTo(organizationKatwarn);
        assertThat(updatedNewsItem.getNewsSource().getId()).isEqualTo(newsSourceKatwarn.getId());

        ClientPostChangeConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasSilent(
                expectedGeoAreas, ClientPostChangeConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
        assertThat(pushedEvent.getPostId()).isEqualTo(updatedNewsItem.getId());
        assertThat(pushedEvent.getPost().getNewsItem().getText()).isEqualTo(updatedNewsItem.getText());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void katwarnAlertRequest_Unauthorized() throws Exception {

        // missing api key
        String testAlert = th.loadTextTestResource("contentintegration/katwarn/katwarnAlert1.json");
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(testAlert))
                .andExpect(status().is4xxClientError());

        // wrong api key
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, "wrong api key")
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(testAlert))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        // wrong app
        mockMvc.perform(post("/katwarn/alerts/_default")
                        .header(HEADER_NAME_API_KEY_KATWARN, th.app1Variant1.getApiKey1())
                        .contentType(CONTENT_TYPE_KATWARN)
                        .content(testAlert))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.NOT_AUTHORIZED, "app"));
    }

}
