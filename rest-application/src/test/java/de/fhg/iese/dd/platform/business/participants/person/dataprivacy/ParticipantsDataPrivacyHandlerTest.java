/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.dataprivacy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;

import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.test.mocks.TestOauthManagementService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;

public class ParticipantsDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;
    @SpyBean
    private TestOauthManagementService testOauthManagementServiceSpy;

    @Override
    public Person getPersonForPrivacyReport() {
        return th.personRegularAnna;
    }

    @Override
    public Person getPersonForDeletion() {
        return getPersonForPrivacyReport();
    }

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createShops();
        th.createAppEntities();
        th.createPersonBlockings();
        th.withHomeArea(getPersonForPrivacyReport(), th.geoAreaTenant1);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void blockedPersonsInPrivacyReport() {

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        assertThat(dataPrivacyReport).has(sectionExists("Von dir blockierte Personen", blockedPersonsSection -> {

            final List<IDataPrivacyReport.IDataPrivacyReportSubSection> appSections =
                    blockedPersonsSection.getSubSections();
            assertEquals(2, appSections.size());

            // Blocked users in app1
            assertThat(blockedPersonsSection).has(subSectionExists(th.app1.getName(), app1Subsection -> {

                final List<IDataPrivacyReport.IDataPrivacyReportContent<?>> app1Contents = app1Subsection.getContents();
                assertEquals(2, app1Contents.size());
                assertPersonNameInContents(app1Contents, th.annaBlocksKarlInApp1.getBlockedPerson());
                assertPersonNameInContents(app1Contents, th.annaBlocksDeletedInApp1.getBlockedPerson());
            }));

            // Blocked users in app2
            assertThat(blockedPersonsSection).has(subSectionExists(th.app2.getName(), app2Subsection -> {

                final List<IDataPrivacyReport.IDataPrivacyReportContent<?>> app2Contents = app2Subsection.getContents();
                assertEquals(1, app2Contents.size());
                assertPersonNameInContents(app2Contents, th.annaBlocksContactPersonCommunity1InApp2.getBlockedPerson());
            }));
        }));
    }

    @Test
    public void personBlockingIsDeleted() {

        final IPersonDeletionReport personDeletionReportOfAnna = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(th.personRegularAnna, personDeletionReportOfAnna);

        assertThat(personDeletionReportOfAnna)
                .is(containsEntityWithOperation(th.annaBlocksKarlInApp1, DeleteOperation.DELETED));
        assertThat(personDeletionReportOfAnna)
                .is(containsEntityWithOperation(th.annaBlocksDeletedInApp1, DeleteOperation.DELETED));
        assertThat(personDeletionReportOfAnna)
                .is(containsEntityWithOperation(th.annaBlocksContactPersonCommunity1InApp2, DeleteOperation.DELETED));
        assertThat(th.personBlockingRepository.findAllByBlockingPersonOrderByCreatedDesc(th.personRegularAnna))
                .isEmpty();

        final IPersonDeletionReport personDeletionReportOfKarl = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(th.personRegularKarl, personDeletionReportOfKarl);

        personDeletionReportOfKarl.getDeletedEntities().forEach(deletedEntity ->
                assertNotEquals(deletedEntity.getEntityClass(), PersonBlocking.class));
        assertThat(th.personBlockingRepository.findAllByBlockingPersonOrderByCreatedDesc(th.personRegularKarl))
                .isEmpty();
    }

    @Test
    public void oauthRegistrationIsDeleted() {

        assertThat(oauthRegistrationRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .as("There should be an oauth registration, so that it can be deleted")
                .isTrue();

        //we do not care if it is in the deletion report, it's only important that it is deleted
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), new PersonDeletionReport());

        assertThat(oauthRegistrationRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .as("The oauth registration of the deleted person should be deleted, too")
                .isFalse();
    }

    @Test
    public void oauthRegistrationIsNotDeleted_FailedDeletion() {

        // simulate a failure during the deletion of the user at auth0
        doThrow(OauthManagementException.class).when(
                testOauthManagementServiceSpy).deleteUser(Mockito.eq(getPersonForDeletion().getOauthId()));

        assertThat(personRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .isTrue();
        assertThat(oauthRegistrationRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .isTrue();

        // we expect an exception to be thrown in due to a failed deletion
        assertThrows(OauthManagementException.class, () ->
                internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), new PersonDeletionReport()));

        // deletion failed -> oauth id and registration should still be there
        assertThat(personRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .isTrue();
        assertThat(oauthRegistrationRepository.existsByOauthId(getPersonForDeletion().getOauthId()))
                .isTrue();
    }

    private static void assertPersonNameInContents(
            final Collection<IDataPrivacyReport.IDataPrivacyReportContent<?>> contents, final Person person) {

        assertTrue(contents.stream()
                .anyMatch(content -> content.getContent().equals(person.getFirstNameFirstCharLastDot())));
    }

}
