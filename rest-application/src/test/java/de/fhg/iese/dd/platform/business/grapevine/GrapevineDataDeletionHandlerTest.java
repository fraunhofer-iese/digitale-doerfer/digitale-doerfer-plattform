/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.datadeletion.BaseDataDeletionHandlerTest;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GrapevineDataDeletionHandlerTest extends BaseDataDeletionHandlerTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private ConvenienceRepository convenienceRepository;
    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private OrganizationGeoAreaMappingRepository organizationGeoAreaMappingRepository;

    private Convenience convenienceBlutspende;
    private Organization organizationFeuerwehr;
    private Post postToBeChecked;

    @Override
    protected BaseSharedTestHelper getTestHelper() {

        return th;
    }

    @Override
    public void createRequiredEntities() {

        th.createGrapevineScenario();
    }

    @Override
    protected void createAdditionalEntities() {

        postToBeChecked = th.gossip1withImages;
        assertThat(postToBeChecked.getTenant()).isEqualTo(tenantToBeDeleted);
        assertThat(postToBeChecked.getGeoAreas()).containsExactly(geoAreaToBeDeleted);
        createConvenience();
        createOrganizations();
    }

    @Override
    protected void assertAfterDeletion() throws Exception {
        mockMvc.perform(get("/grapevine/post/{postId}", postToBeChecked.getId())
                        .headers(authHeadersFor(th.personThomasBecker, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId())
                        .headers(authHeadersFor(th.personThomasBecker, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));

        mockMvc.perform(get("/grapevine/organization/{organizationId}", organizationFeuerwehr.getId())
                        .headers(authHeadersFor(th.personThomasBecker, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Override
    protected void assertAfterDeletionGeoArea() throws Exception {
        List<GeoArea> geoAreasConvenienceBlutspende = convenienceGeoAreaMappingRepository.findAll().stream()
                .filter(c -> convenienceBlutspende.equals(c.getConvenience()))
                .map(ConvenienceGeoAreaMapping::getGeoArea)
                .collect(Collectors.toList());
        assertThat(geoAreasConvenienceBlutspende).containsExactly(geoAreaToBeUsedInstead);

        List<GeoArea> geoAreasOrganizationFeuerwehr = organizationGeoAreaMappingRepository.findAll().stream()
                .filter(c -> organizationFeuerwehr.equals(c.getOrganization()))
                .map(OrganizationGeoAreaMapping::getGeoArea)
                .collect(Collectors.toList());
        assertThat(geoAreasOrganizationFeuerwehr).containsExactly(geoAreaToBeUsedInstead);
    }

    @Override
    protected void assertAfterDeletionTenant() throws Exception {

    }

    private void createConvenience() {

        convenienceBlutspende = convenienceRepository.save(Convenience.builder()
                .name("Blutspende")
                .description("Blut am Morgen vertreibt Kummer und Sorgen")
                .organizationName("Vampir AG")
                .organizationLogo(th.createMediaItem("Blut_Logo"))
                .url("https://www.blutspendedienst.com/suche?term={homeArea.zipCodes} {homeArea.name}&radius=20")
                .urlLabel("Termin finden")
                .overviewImage(th.createMediaItem("Blut_Overview"))
                .detailImages(Arrays.asList(th.createMediaItem("Blut_Detail1"), th.createMediaItem("Blut_Detail2")))
                .build());
        convenienceGeoAreaMappingRepository.save(ConvenienceGeoAreaMapping.builder()
                .convenience(convenienceBlutspende)
                .geoArea(geoAreaToBeDeleted)
                .build()
                .withConstantId());
    }

    private void createOrganizations() {

        organizationFeuerwehr = Organization.builder()
                .name("Feuerwehr")
                .locationDescription("Aschehausen")
                .description("Löscht und erfrischt")
                .overviewImage(th.createMediaItem("Feuerwehr_Overview"))
                .logoImage(th.createMediaItem("Feuerwehr_Logo"))
                .detailImages(
                        Arrays.asList(th.createMediaItem("Feuerwehr_Detail1"), th.createMediaItem("Feuerwehr_Detail2")))
                .url("https://www.feuerwehr-aschehausen.com")
                .phone("112")
                .emailAddress("feuer@feuerwehr-aschehausen.com")
                .address(th.addressRepository.saveAndFlush(Address.builder()
                        .name("Feuerwehr Aschehausen")
                        .street("Feuerstraße 1")
                        .zip("67663")
                        .city("Aschehausen")
                        .build()))
                .factsJson("""
                        [{"name":"Mitglieder","value":"40"},{"name":"Fahrzeuge","value":"drei"}]""")
                .build();
        organizationFeuerwehr.setOrganizationPersons(Arrays.asList(OrganizationPerson.builder()
                        .organization(organizationFeuerwehr)
                        .category("Vorstand")
                        .position("Erster Brandmelder")
                        .name("Karl Flamme")
                        .profilePicture(th.createMediaItem("KF"))
                        .build(),
                OrganizationPerson.builder()
                        .organization(organizationFeuerwehr)
                        .category("Vorstand")
                        .position("Zweite Brandmelderin")
                        .name("Susi Flamme")
                        .profilePicture(th.createMediaItem("SF"))
                        .build()));

        organizationFeuerwehr = organizationRepository.save(organizationFeuerwehr);
        organizationGeoAreaMappingRepository.save(OrganizationGeoAreaMapping.builder()
                .organization(organizationFeuerwehr)
                .geoArea(geoAreaToBeDeleted)
                .build()
                .withConstantId());
    }

}
