/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Benjamin Hassenfratz, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics.legacy;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GrapevineStatisticsServiceTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private IGrapevineStatisticsService grapevineStatisticsService;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getPostStatsForTenantGroup() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        PostStats tenantGroupPostStats = grapevineStatisticsService
                .getPostStatsPerTenantGroup("rlp", start, end);

        assertEquals(12, tenantGroupPostStats.getGossipCount());
        assertEquals(7, tenantGroupPostStats.getNewsItemCount());
        assertEquals(5, tenantGroupPostStats.getSeekingCount());
        assertEquals(5, tenantGroupPostStats.getOfferCount());
        assertEquals(8, tenantGroupPostStats.getSuggestionCount());
        assertEquals(5, tenantGroupPostStats.getHappeningCount());
        assertEquals(42, tenantGroupPostStats.getPostCount());
        assertEquals(20, tenantGroupPostStats.getCommentCount());
    }

    @Test
    public void getUserStatsForTenantGroup() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();
        String tag = "rlp";

        long expectedCreatedUsersCount = th.personRepository.findAll()
                .stream()
                .filter(person -> tag.equals(person.getTenant().getTag())
                        && person.getCreated() >= start && person.getCreated() <= end)
                .count();

        UserStats tenantGroupUserStats = grapevineStatisticsService.getUserStatsPerTenantGroup(tag, start, end);

        assertEquals(expectedCreatedUsersCount, tenantGroupUserStats.getCreatedUsers());
    }

    @Test
    public void getActivityStatsForTenantGroup() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        ActivityStats tenantGroupActivityStats =
                grapevineStatisticsService.getActivityStatsPerTenantGroup("rlp", start, end);

        assertEquals(8, tenantGroupActivityStats.getUsersPosted());
        assertEquals(7, tenantGroupActivityStats.getUsersCommented());
        assertEquals(10, tenantGroupActivityStats.getUsersPostedOrCommented());
    }

    @Test
    public void getPostStatsForTenant() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        PostStats tenant1PostStats = grapevineStatisticsService
                .getPostStatsPerTenant(th.tenant1, start, end);

        assertEquals(5, tenant1PostStats.getGossipCount());
        assertEquals(7, tenant1PostStats.getNewsItemCount());
        assertEquals(5, tenant1PostStats.getSeekingCount());
        assertEquals(5, tenant1PostStats.getOfferCount());
        assertEquals(8, tenant1PostStats.getSuggestionCount());
        assertEquals(5, tenant1PostStats.getHappeningCount());
        assertEquals(35, tenant1PostStats.getPostCount());
        assertEquals(14, tenant1PostStats.getCommentCount());
    }

    @Test
    public void getUserStatsForTenant() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        UserStats Tenant1UserStats = grapevineStatisticsService.getUserStatsPerTenant(th.tenant1, start, end);

        //See: TestHelper.createPersons() -> 9 Persons
        //See: GrapevineTestHelper.createPersons() -> 8
        assertEquals(17, Tenant1UserStats.getCreatedUsers());
    }

    @Test
    public void getActivityStatsForTenant() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        ActivityStats tenant1ActivityStats =
                grapevineStatisticsService.getActivityStatsPerTenant(th.tenant1, start, end);

        assertEquals(7, tenant1ActivityStats.getUsersPosted());
        assertEquals(5, tenant1ActivityStats.getUsersCommented());
        assertEquals(8, tenant1ActivityStats.getUsersPostedOrCommented());
    }

    @Test
    public void getGrapevineStats() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        long expectedCreatedUsersCount = th.personRepository.findAll()
                .stream()
                .filter(person -> person.getCreated() >= start && person.getCreated() <= end)
                .count();

        GrapevineStats grapevineStats = grapevineStatisticsService.getGrapevineStats(start, end);

        assertThat(grapevineStats.getTenantGroupStats()).map(TenantGroupStats::getTenantGroupName)
                .containsExactlyInAnyOrder("rlp", "bayern");
        //tenant1, tenant2, tenant3
        assertEquals(3, grapevineStats.getTenantStats().size());

        assertEquals(expectedCreatedUsersCount, grapevineStats.getUserStats().getCreatedUsers());
    }

}
