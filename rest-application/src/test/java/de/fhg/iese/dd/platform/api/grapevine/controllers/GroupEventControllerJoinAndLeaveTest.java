/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupLeaveConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupLeaveRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroup;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

/**
 * Tests for:
 * <ul>
 * <li>A person joins (tries to join) a group with different levels of accessability.</li>
 * <li>The join confirmation process is played through.</li>
 * <li>Person leaves group.</li>
 * </ul>
 */
public class GroupEventControllerJoinAndLeaveTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    private AppVariant appVariant;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();

        appVariant = th.appVariantKL_EB;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void groupJoinRequest_publicGroup_withDoubleJoin() throws Exception {

        //first join
        GroupMembership groupMembershipFirst =
                assertSuccessfulFirstJoinRequest(th.personEckhardTenant1Dorf1NoMember, th.groupAdfcAAP,
                        "Ich binz die Carmen", GroupMembershipStatus.APPROVED);

        //second join, membership is not changed
        assertSuccessfulSecondJoinRequest(th.personEckhardTenant1Dorf1NoMember, th.groupAdfcAAP,
                "Lasst mich dabei sein", groupMembershipFirst);

        //join with text
        assertSuccessfulFirstJoinRequest(th.personFranziTenant1Dorf2NoMember, th.groupAdfcAAP,
                "Hallo, ich bin's, die Franzi!", GroupMembershipStatus.APPROVED);
    }

    @Test
    public void groupJoinRequest_privateGroup() throws Exception {

        assertSuccessfulFirstJoinRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                "Hallo Leute!", GroupMembershipStatus.PENDING);
    }

    @Test
    public void groupJoinRequest_publicGroup_NoText() throws Exception {

        //join no text
        assertSuccessfulFirstJoinRequest(th.personFranziTenant1Dorf2NoMember, th.groupAdfcAAP, null,
                GroupMembershipStatus.APPROVED);

        //join empty text
        assertSuccessfulFirstJoinRequest(th.personEckhardTenant1Dorf1NoMember, th.groupAdfcAAP, "",
                GroupMembershipStatus.APPROVED);
    }

    @Test
    public void groupJoinRequest_privateGroup_NoText() throws Exception {

        //join no text
        assertSuccessfulFirstJoinRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA, null,
                GroupMembershipStatus.PENDING);

        //join empty text
        assertSuccessfulFirstJoinRequest(th.personAnjaTenant1Dorf1, th.groupSchachvereinAAA, "",
                GroupMembershipStatus.PENDING);
    }

    @Test
    public void groupJoinRequest_publicGroup_ReuseMembershipInWrongStatus() throws Exception {

        //first join
        GroupMembership groupMembershipFirst =
                assertSuccessfulFirstJoinRequest(th.personEckhardTenant1Dorf1NoMember, th.groupAdfcAAP,
                        "Huhu", GroupMembershipStatus.APPROVED);

        //should not happen in production, but is a valid setting that can occur in the database
        groupMembershipFirst.setStatus(GroupMembershipStatus.NOT_MEMBER);
        th.groupMembershipRepository.saveAndFlush(groupMembershipFirst);

        //we expect the membership to be set to approved again
        groupMembershipFirst.setStatus(GroupMembershipStatus.APPROVED);
        //second join, membership is re-used
        assertSuccessfulSecondJoinRequest(th.personEckhardTenant1Dorf1NoMember, th.groupAdfcAAP,
                "Was issen da los warum bin ich nicht Mitglied alles voller Bugs hier", groupMembershipFirst);
    }

    @Test
    public void groupJoinRequest_privateGroup_ReuseMembershipInWrongStatus() throws Exception {

        //first join
        GroupMembership groupMembershipFirst =
                assertSuccessfulFirstJoinRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                        "Hallo Leute!", GroupMembershipStatus.PENDING);

        //should not happen in production, but is a valid setting that can occur in the database
        groupMembershipFirst.setStatus(GroupMembershipStatus.NOT_MEMBER);
        th.groupMembershipRepository.saveAndFlush(groupMembershipFirst);

        //we expect the membership to be set to pending again
        groupMembershipFirst.setStatus(GroupMembershipStatus.PENDING);
        assertSuccessfulSecondJoinRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                "Lasst mich endlich rein", groupMembershipFirst);
    }

    private GroupMembership assertSuccessfulFirstJoinRequest(Person caller, Group group, String introText,
            GroupMembershipStatus expectedMembershipStatus) throws Exception {

        ClientGroupJoinConfirmation groupJoinConfirmation = callGroupJoinRequest(caller, group, introText);
        ClientGroup expectedClientGroup = th.toClientGroup(group, caller);

        //this check is necessary, since the expectedClientGroup is created based on the result of a query
        assertEquals(expectedMembershipStatus, expectedClientGroup.getMembershipStatus());

        assertJsonEquals(expectedClientGroup, groupJoinConfirmation.getGroup());

        final GroupMembership membership = th.groupMembershipRepository.findByGroupAndMember(group, caller).get();
        assertEquals(caller, membership.getMember());
        assertEquals(group, membership.getGroup());
        assertEquals(expectedMembershipStatus, membership.getStatus());
        assertEquals(introText, membership.getMemberIntroductionText());
        return membership;
    }

    private void assertSuccessfulSecondJoinRequest(Person caller, Group group, String introText,
            GroupMembership expectedMembership) throws Exception {

        ClientGroupJoinConfirmation groupJoinConfirmation = callGroupJoinRequest(caller, group, introText);
        ClientGroup expectedClientGroup = th.toClientGroup(group, caller);

        //this check is necessary, since the expectedClientGroup is created based on the result of a query
        assertEquals(expectedMembership.getStatus(), expectedClientGroup.getMembershipStatus());

        assertJsonEquals(expectedClientGroup, groupJoinConfirmation.getGroup());

        final GroupMembership membership = th.groupMembershipRepository.findByGroupAndMember(group, caller).get();
        assertEquals(expectedMembership.getId(), membership.getId());
        assertEquals(expectedMembership.getCreated(), membership.getCreated());
        assertEquals(expectedMembership.getMember(), membership.getMember());
        assertEquals(expectedMembership.getGroup(), membership.getGroup());
        assertEquals(expectedMembership.getStatus(), membership.getStatus());
        assertEquals(expectedMembership.getMemberIntroductionText(), membership.getMemberIntroductionText());
    }

    private ClientGroupJoinConfirmation callGroupJoinRequest(Person caller, Group group, String introText)
            throws Exception {
        final MvcResult result = mockMvc.perform(buildJoinRequest(caller, group, introText))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        return toObject(result.getResponse(),
                ClientGroupJoinConfirmation.class);
    }

    private MockHttpServletRequestBuilder buildJoinRequest(Person caller, Group group, String introText)
            throws IOException {
        return post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(group.getId())
                        .memberIntroductionText(introText)
                        .build()));
    }

    @Test
    public void leaveGroupRequest_memberAndNoMember() throws Exception {

        //member
        assertSuccessfulLeaveRequest(th.personChloeTenant1Dorf1, th.groupWaffenfreundeAMA);
        //not a member
        assertUnsuccessfulLeaveRequest(th.personFranziTenant1Dorf2NoMember, th.groupAdfcAAP);
    }

    @Test
    public void leaveGroupRequest_rejoin() throws Exception {

        Group group = th.groupAdfcAAP;
        Person person = th.personAnjaTenant1Dorf1;

        assertTrue(th.groupMembershipRepository.findByGroupAndMember(group, person).isPresent());

        assertSuccessfulLeaveRequest(person, group);

        assertSuccessfulFirstJoinRequest(person, group, "Sorry, habe 🌭👆!", GroupMembershipStatus.APPROVED);
    }

    @Test
    public void leaveGroupRequest_ignoreMembershipWrongStatus() throws Exception {

        Group group = th.groupAdfcAAP;
        Person person = th.personAnjaTenant1Dorf1;

        //should not happen in production, but is a valid setting that can occur in the database
        GroupMembership groupMembership = th.groupMembershipRepository.findByGroupAndMember(group, person).get();
        groupMembership.setStatus(GroupMembershipStatus.NOT_MEMBER);
        th.groupMembershipRepository.saveAndFlush(groupMembership);

        assertUnsuccessfulLeaveRequest(person, group);
    }

    @Test
    public void groupJoinRequestMembershipAlreadyPending() throws Exception {

        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA,
                "first request"))
                .andExpect(status().isOk());

        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA,
                "second request"))
                .andExpect(isException(ClientExceptionType.GROUP_MEMBERSHIP_ALREADY_PENDING));
    }

    @Test
    public void groupLeaveRequestNoMemberOfGroup() throws Exception {
        mockMvc.perform(buildLeaveRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA))
                .andExpect(isException(ClientExceptionType.NOT_MEMBER_OF_GROUP));
    }

    @Test
    public void groupJoinRequestWithWrongAppVariant() throws Exception {
        mockMvc.perform(post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantMZ))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(th.groupSchachvereinAAA.getId()) //not member, should not see it
                        .memberIntroductionText("test")
                        .build())))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void groupLeaveRequestWithWrongAppVariant() throws Exception {

        //members can leave the group, no matter what app variant they are using
        final MvcResult result = mockMvc.perform(post("/grapevine/group/event/groupLeaveRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantMZ))
                .contentType(contentType)
                .content(json(ClientGroupLeaveRequest.builder()
                        .groupId(th.groupAdfcAAP.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientGroupLeaveConfirmation groupLeaveConfirmation = toObject(result.getResponse(),
                ClientGroupLeaveConfirmation.class);

        ClientGroup expectedClientGroup = th.toClientGroup(th.groupAdfcAAP, th.personAnjaTenant1Dorf1);

        //this check is necessary, since the expectedClientGroup is created based on the result of a query
        assertEquals(GroupMembershipStatus.NOT_MEMBER, expectedClientGroup.getMembershipStatus());

        assertJsonEquals(expectedClientGroup, groupLeaveConfirmation.getGroup());

        assertFalse(th.groupMembershipRepository.findByGroupAndMember(th.groupAdfcAAP,
                th.personAnjaTenant1Dorf1).isPresent());
    }

    private void assertSuccessfulLeaveRequest(Person caller, Group group) throws Exception {

        final MvcResult result = mockMvc.perform(buildLeaveRequest(caller, group))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientGroupLeaveConfirmation groupLeaveConfirmation = toObject(result.getResponse(),
                ClientGroupLeaveConfirmation.class);

        ClientGroup expectedClientGroup = th.toClientGroup(group, caller);

        //this check is necessary, since the expectedClientGroup is created based on the result of a query
        assertEquals(GroupMembershipStatus.NOT_MEMBER, expectedClientGroup.getMembershipStatus());

        assertJsonEquals(expectedClientGroup, groupLeaveConfirmation.getGroup());

        assertFalse(th.groupMembershipRepository.findByGroupAndMember(group, caller).isPresent());
    }

    private void assertUnsuccessfulLeaveRequest(Person caller, Group group) throws Exception {

        mockMvc.perform(buildLeaveRequest(caller, group))
                .andExpect(isException(ClientExceptionType.NOT_MEMBER_OF_GROUP));
    }

    private MockHttpServletRequestBuilder buildLeaveRequest(Person caller, Group group) throws IOException {
        return post("/grapevine/group/event/groupLeaveRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupLeaveRequest.builder()
                        .groupId(group.getId())
                        .build()));
    }

}
