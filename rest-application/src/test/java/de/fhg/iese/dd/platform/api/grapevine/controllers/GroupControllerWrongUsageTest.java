/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

/**
 * Tests GET endpoints without OAuth or wrong OAuth ID, wrong and missing parameters.<br> This test is for syntactically
 * incorrect requests (semantically incorrect requests should be tested withing the other tests that test the business
 * logic).<br> We only need one test person, so createEntities and tearDown are fast!
 */
public class GroupControllerWrongUsageTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupFeatureConfiguration();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void listGroups_unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/group"), th.appVariantKL_EB, th.personAnjaTenant1Dorf1);

    }

    @Test
    public void listGroup_OAuth2AppVariantRequired() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/group/abcdef"), th.appVariantKL_EB, th.personAnjaTenant1Dorf1);
    }

    @Test
    public void listGroups_appVariantNotFound() throws Exception {
        mockMvc.perform(get("/grapevine/group")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
        mockMvc.perform(get("/grapevine/group")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, AppVariant.builder()
                        .name("not existing")
                        .appVariantIdentifier("de.de.de")
                        .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void listGroups_appVariantMissing() throws Exception {
        mockMvc.perform(get("/grapevine/group")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void listGroup_appVariantNotFound() throws Exception {
        mockMvc.perform(get("/grapevine/group/abcdef")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
        mockMvc.perform(get("/grapevine/group/abcdef")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, AppVariant.builder()
                        .name("not existing")
                        .appVariantIdentifier("de.de.de")
                        .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void listGroup_appVariantMissing() throws Exception {
        mockMvc.perform(get("/grapevine/group/abcdef")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1)))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAllPostsInvalidSortCriteria() throws Exception {
        mockMvc.perform(get("/grapevine/group/abcdef/post")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1))
                .param("type", "GOSSIP")
                .param("sortBy", "LAST_SUGGESTION_ACTIVITY"))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void getAllPostsPersonOrAppVariantInvalid() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/group/abcdef/post"), th.appVariantKL_EB,
                th.personAnjaTenant1Dorf1);
    }

    @Test
    public void getAllPostsWrongOrEmptyGroup() throws Exception {
        mockMvc.perform(get("/grapevine/group/abc/post")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB)))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }
    
}
