/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.contentintegration.ContentIntegrationTestHelper;
import de.fhg.iese.dd.platform.api.contentintegration.clientmodel.*;
import de.fhg.iese.dd.platform.business.contentintegration.services.TestWebFeedScraperService;
import de.fhg.iese.dd.platform.business.contentintegration.services.WebFeedItemScrapingResult;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingAccessType;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.WebFeedParameter;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CrawlingAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private ContentIntegrationTestHelper th;

    @Autowired
    private TestWebFeedScraperService webFeedScraperService;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createNewsSourcesAndCrawlingConfigs();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void crawlWebFeedItems() throws Exception {

        // RSS
        checkWebFeedItems(th.crawlingConfigRSSNewsItemZukunftstheim);

        // ATOM
        checkWebFeedItems(th.crawlingConfigATOMNewsItemDorfallerlei);

        // API
        checkWebFeedItems(th.crawlingConfigAPINewsItemZukunftstheim);

        // RSS with format and fixed values
        checkWebFeedItems(th.crawlingConfigRSSWithFormatAndFixedValuesNewsItemZukunftsheim);
    }

    @Test
    public void crawlWebFeedItems_emptyItemList() throws Exception {

        final CrawlingConfig tempConfig = th.crawlingConfigRSSNewsItemZukunftstheim;

        final ClientTestWebFeedScraperRequest request = ClientTestWebFeedScraperRequest.builder()
                .webFeedSourceUrl("test-url/hier-ist-nichts")
                .accessType(tempConfig.getAccessType())
                .webFeedParameters(toClientFeedParameters(tempConfig.getWebFeedParameters()))
                .build();

        final ClientTestWebFeedScraperResponse clientTestWebFeedScraperResponse =
                toObject(mockMvc.perform(post("/adminui/testWebFeedScraperRequest")
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                                .contentType(contentType)
                                .content(json(request)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn().getResponse(), ClientTestWebFeedScraperResponse.class);

        assertThat(clientTestWebFeedScraperResponse).isNotNull();
        assertThat(clientTestWebFeedScraperResponse.getLog()).isNotBlank();
        assertThat(clientTestWebFeedScraperResponse.getWebFeedItems()).isEmpty();
    }

    @Test
    public void crawlWebFeedItems_Unauthorized() throws Exception {

        final CrawlingConfig tempConfig = th.crawlingConfigRSSNewsItemZukunftstheim;

        final ClientTestWebFeedScraperRequest request = ClientTestWebFeedScraperRequest.builder()
                .webFeedSourceUrl(tempConfig.getSourceUrl())
                .accessType(tempConfig.getAccessType())
                .webFeedParameters(toClientFeedParameters(tempConfig.getWebFeedParameters()))
                .build();

        mockMvc.perform(post("/adminui/testWebFeedScraperRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/testWebFeedScraperRequest")
                .contentType(contentType)
                .content(json(request)));
    }

    private void checkWebFeedItems(CrawlingConfig tempCrawlingConfig) throws Exception {

        // the crawling config is used to get a valid url, access type and feed parameters for the request
        final String url = tempCrawlingConfig.getSourceUrl();
        final CrawlingAccessType accessType = tempCrawlingConfig.getAccessType();

        final List<WebFeedItemScrapingResult> expectedWebFeedItemScrapingResults =
                webFeedScraperService.scrapeWebFeed(url, accessType, tempCrawlingConfig.getWebFeedParameters(),
                        new LogSummary());

        final ClientTestWebFeedScraperRequest request = ClientTestWebFeedScraperRequest.builder()
                .webFeedSourceUrl(tempCrawlingConfig.getSourceUrl())
                .accessType(tempCrawlingConfig.getAccessType())
                .webFeedParameters(toClientFeedParameters(tempCrawlingConfig.getWebFeedParameters()))
                .build();

        final ClientTestWebFeedScraperResponse actualResponse =
                toObject(mockMvc.perform(post("/adminui/testWebFeedScraperRequest")
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                                .contentType(contentType)
                                .content(json(request)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn().getResponse(), ClientTestWebFeedScraperResponse.class);

        assertThat(actualResponse.getLog()).isNotBlank();
        List<ClientWebFeedItem> actualWebFeedItems = actualResponse.getWebFeedItems();
        assertThat(actualWebFeedItems).hasSameSizeAs(expectedWebFeedItemScrapingResults);

        for (int i = 0; i < expectedWebFeedItemScrapingResults.size(); i++) {
            Map<String, String> expectedAttributeValueMap = expectedWebFeedItemScrapingResults
                    .get(i)
                    .toDebugMap();

            Map<String, String> returnedAttributeValueMap = actualWebFeedItems.get(i).getAttributeValueMap();

            assertThat(returnedAttributeValueMap).hasSameSizeAs(expectedAttributeValueMap);
            assertThat(returnedAttributeValueMap).containsAllEntriesOf(expectedAttributeValueMap);
        }
    }

    private Map<String, List<ClientWebFeedParameter>> toClientFeedParameters(
            Map<String, List<WebFeedParameter>> feedParameters) {
        return feedParameters.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        feedParameter -> feedParameter.getValue().stream()
                                .map(webFeedParameter -> ClientWebFeedParameter.builder()
                                        .tag(webFeedParameter.getTag())
                                        .fixedValue(webFeedParameter.getFixedValue())
                                        .webFeedValueSources(
                                                webFeedParameter.getWebFeedValueSources() != null
                                                ? webFeedParameter.getWebFeedValueSources().stream()
                                                            .map(webFeedValueSource -> ClientWebFeedValueSource.builder()
                                                                    .attribute(webFeedValueSource.getAttribute())
                                                                    .type(webFeedValueSource.getType())
                                                                    .child(webFeedValueSource.getChild())
                                                                    .format(webFeedValueSource.getFormat())
                                                                    .build())
                                                            .collect(Collectors.toList())
                                                : new ArrayList<>())
                                        .build())
                                .collect(Collectors.toList())));
    }

}
