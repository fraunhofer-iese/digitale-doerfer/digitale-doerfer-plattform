/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement;

import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import lombok.extern.log4j.Log4j2;

/**
 * Provides a testcontainer with elasticsearch running in it. The elasticsearch instance can be accessed via the url
 * that is emitted as environment variable.
 */
@Log4j2
public class ElasticsearchContainerProvider extends ElasticsearchContainer {

    private static final String ELASTICSEARCH_USERNAME = "elastic";
    private static final String ELASTICSEARCH_PASSWORD = "hundekuchen";
    private static final String IMAGE_VERSION = "docker.elastic.co/elasticsearch/elasticsearch:7.10.2";
    private static ElasticsearchContainerProvider instance;

    private ElasticsearchContainerProvider() {
        super(IMAGE_VERSION);
    }

    public static ElasticsearchContainerProvider getInstance() {
        if (instance == null) {
            instance = (ElasticsearchContainerProvider) new ElasticsearchContainerProvider()
                    .withPassword(ELASTICSEARCH_PASSWORD)
                    //this copies the custom dictionary file to the location where elasticsearch can find it
                    //the target name is weird, it's due to the way AWS manages these files when copying them
                    .withClasspathResourceMapping("/search-config/dictionary-compound-de.txt",
                            "/usr/share/elasticsearch/config/analyzers/F133218504", BindMode.READ_ONLY);
            instance.start();
        }
        return instance;
    }

    @Override
    public void start() {
        long start = System.currentTimeMillis();
        if (isRunning()) {
            return;
        }
        log.info("Starting Elasticsearch container '{}' ...", IMAGE_VERSION);
        super.start();
        waitingFor(Wait.forHealthcheck());
        final String address = "http://" + instance.getHttpHostAddress();
        System.setProperty("ELASTICSEARCH_URL", address);
        System.setProperty("ELASTICSEARCH_USERNAME", ELASTICSEARCH_USERNAME);
        System.setProperty("ELASTICSEARCH_PW", ELASTICSEARCH_PASSWORD);
        log.info("Started Elasticsearch container at {} in {} ms", address, System.currentTimeMillis() - start);
    }

}
