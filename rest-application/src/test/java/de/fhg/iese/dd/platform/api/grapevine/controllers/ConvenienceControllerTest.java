/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class ConvenienceControllerTest extends BaseServiceTest {

    @Autowired
    private ConvenienceRepository convenienceRepository;
    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;
    @Autowired
    private GrapevineTestHelper th;

    private Convenience convenienceBlutspende;
    private Convenience convenienceDorfNews;
    private GeoArea geoAreaBlutspendeDorfNews;
    private GeoArea geoAreaDorfNews;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        createConveniences();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private void createConveniences() {

        geoAreaBlutspendeDorfNews = th.geoAreaDorf1InEisenberg;
        geoAreaDorfNews = th.geoAreaDorf2InEisenberg;

        convenienceBlutspende = convenienceRepository.save(Convenience.builder()
                .name("Blutspende")
                .description("Blut am Morgen vertreibt Kummer und Sorgen")
                .organizationName("Vampir AG")
                .organizationLogo(th.createMediaItem("Blut_Logo"))
                .url("https://www.blutspendedienst.com/suche?term={homeArea.zipCodes} {homeArea.name}&radius=20")
                .urlLabel("Termin finden")
                .overviewImage(th.createMediaItem("Blut_Overview"))
                .detailImages(Arrays.asList(th.createMediaItem("Blut_Detail1"), th.createMediaItem("Blut_Detail2")))
                .build());
        convenienceGeoAreaMappingRepository.save(ConvenienceGeoAreaMapping.builder()
                .convenience(convenienceBlutspende)
                .geoArea(geoAreaBlutspendeDorfNews)
                .build()
                .withConstantId());

        convenienceDorfNews = convenienceRepository.save(Convenience.builder()
                .name("DorfNews")
                .description("Super informiert mit zuverlässiger WordPress-Technologie")
                .organizationName("Institut für Exzellentes Software Engineering")
                .organizationLogo(th.createMediaItem("DorfNews_Logo"))
                .url("https://www.dorfnews.de/my-dorf")
                .urlLabel("Zu den heißen News")
                .overviewImage(th.createMediaItem("DorfNews_Overview"))
                .detailImages(Collections.singletonList(th.createMediaItem("DorfNews_Detail")))
                .build());
        convenienceGeoAreaMappingRepository.save(ConvenienceGeoAreaMapping.builder()
                .convenience(convenienceDorfNews)
                .geoArea(geoAreaBlutspendeDorfNews)
                .build()
                .withConstantId());
        convenienceGeoAreaMappingRepository.save(ConvenienceGeoAreaMapping.builder()
                .convenience(convenienceDorfNews)
                .geoArea(geoAreaDorfNews)
                .build()
                .withConstantId());
    }

    @Test
    public void getAllAvailableConveniences() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;
        GeoArea callerHomeArea = caller.getHomeArea();

        assertThat(callerHomeArea).isNotNull();
        assertThat(callerHomeArea.getZip()).hasSizeGreaterThanOrEqualTo(5);
        assertThat(callerHomeArea.getName()).hasSizeGreaterThanOrEqualTo(5);
        String expectedUrlBlutspende =
                "https://www.blutspendedienst.com/suche?term=" + callerHomeArea.getZip() + "%20" +
                        callerHomeArea.getName() + "&radius=20";

        //no selection
        mockMvc.perform(get("/grapevine/convenience")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        //selection of geo area that has one convenience
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaDorfNews);
        mockMvc.perform(get("/grapevine/convenience")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(convenienceDorfNews.getId()))
                .andExpect(jsonPath("$[0].name").value(convenienceDorfNews.getName()))
                .andExpect(jsonPath("$[0].description").value(convenienceDorfNews.getDescription()))
                .andExpect(jsonPath("$[0].organizationName").value(convenienceDorfNews.getOrganizationName()))
                .andExpect(jsonPath("$[0].url").value(convenienceDorfNews.getUrl()))
                .andExpect(jsonPath("$[0].urlLabel").value(convenienceDorfNews.getUrlLabel()))
                .andExpect(
                        jsonPath("$[0].organizationLogo.id").value(convenienceDorfNews.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$[0].overviewImage.id").value(convenienceDorfNews.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id").value(
                        convenienceDorfNews.getDetailImages().get(0).getId()));

        //selection of additional geo area that has both conveniences
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaBlutspendeDorfNews);
        mockMvc.perform(get("/grapevine/convenience")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(convenienceBlutspende.getId()))
                .andExpect(jsonPath("$[0].name").value(convenienceBlutspende.getName()))
                .andExpect(jsonPath("$[0].description").value(convenienceBlutspende.getDescription()))
                .andExpect(jsonPath("$[0].organizationName").value(convenienceBlutspende.getOrganizationName()))
                .andExpect(jsonPath("$[0].url").value(expectedUrlBlutspende))
                .andExpect(jsonPath("$[0].urlLabel").value(convenienceBlutspende.getUrlLabel()))
                .andExpect(
                        jsonPath("$[0].organizationLogo.id").value(convenienceBlutspende.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$[0].overviewImage.id").value(convenienceBlutspende.getOverviewImage().getId()))
                .andExpect(jsonPath("$[0].detailImages[0].id").value(
                        convenienceBlutspende.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$[0].detailImages[1].id").value(
                        convenienceBlutspende.getDetailImages().get(1).getId()))

                .andExpect(jsonPath("$[1].id").value(convenienceDorfNews.getId()))
                .andExpect(jsonPath("$[1].name").value(convenienceDorfNews.getName()))
                .andExpect(jsonPath("$[1].description").value(convenienceDorfNews.getDescription()))
                .andExpect(jsonPath("$[1].organizationName").value(convenienceDorfNews.getOrganizationName()))
                .andExpect(jsonPath("$[1].url").value(convenienceDorfNews.getUrl()))
                .andExpect(jsonPath("$[1].urlLabel").value(convenienceDorfNews.getUrlLabel()))
                .andExpect(
                        jsonPath("$[1].organizationLogo.id").value(convenienceDorfNews.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$[1].overviewImage.id").value(convenienceDorfNews.getOverviewImage().getId()))
                .andExpect(jsonPath("$[1].detailImages[0].id").value(
                        convenienceDorfNews.getDetailImages().get(0).getId()));
    }

    @Test
    public void getAllAvailableConveniences_Deleted() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;

        convenienceBlutspende.setDeleted(true);
        convenienceBlutspende.setDeletionTime(12021983L);
        convenienceBlutspende = convenienceRepository.save(convenienceBlutspende);

        //selection of geo area that has both conveniences, but one is deleted
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaBlutspendeDorfNews);
        mockMvc.perform(get("/grapevine/convenience")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(convenienceDorfNews.getId()));
    }

    @Test
    public void getAllAvailableConveniences_NoHomeArea() throws Exception {

        Person caller = th.personLaraSchaefer;
        AppVariant appVariant = th.appVariant4AllTenants;
        caller.setHomeArea(null);
        caller = th.personRepository.save(caller);

        //the variables in the URL should be filled with ""
        String expectedUrlBlutspende =
                "https://www.blutspendedienst.com/suche?term=%20&radius=20";

        //selection of geo area that has both conveniences
        th.selectGeoAreaForAppVariant(appVariant, caller, geoAreaBlutspendeDorfNews);
        mockMvc.perform(get("/grapevine/convenience")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(convenienceBlutspende.getId()))
                .andExpect(jsonPath("$[0].url").value(expectedUrlBlutspende))
                .andExpect(jsonPath("$[1].id").value(convenienceDorfNews.getId()))
                .andExpect(jsonPath("$[1].url").value(convenienceDorfNews.getUrl()));
    }

    @Test
    public void getAllAvailableConveniences_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/convenience"),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

    @Test
    public void getConvenienceById() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;
        GeoArea callerHomeArea = caller.getHomeArea();

        //geo area selection at app variant is not required
        th.appVariantUsageAreaSelectionRepository.deleteAll();

        assertThat(callerHomeArea).isNotNull();
        assertThat(callerHomeArea.getZip()).hasSizeGreaterThanOrEqualTo(5);
        assertThat(callerHomeArea.getName()).hasSizeGreaterThanOrEqualTo(5);
        String expectedUrlBlutspende =
                "https://www.blutspendedienst.com/suche?term=" + callerHomeArea.getZip() + "%20" +
                        callerHomeArea.getName() + "&radius=20";

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(convenienceBlutspende.getId()))
                .andExpect(jsonPath("$.name").value(convenienceBlutspende.getName()))
                .andExpect(jsonPath("$.description").value(convenienceBlutspende.getDescription()))
                .andExpect(jsonPath("$.organizationName").value(convenienceBlutspende.getOrganizationName()))
                .andExpect(jsonPath("$.url").value(expectedUrlBlutspende))
                .andExpect(jsonPath("$.urlLabel").value(convenienceBlutspende.getUrlLabel()))
                .andExpect(jsonPath("$.organizationLogo.id").value(convenienceBlutspende.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$.overviewImage.id").value(convenienceBlutspende.getOverviewImage().getId()))
                .andExpect(jsonPath("$.detailImages[0].id").value(
                        convenienceBlutspende.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$.detailImages[1].id").value(
                        convenienceBlutspende.getDetailImages().get(1).getId()));
    }

    @Test
    public void getConvenienceById_InvalidUrl() throws Exception {

        Person caller = th.personMonikaHess;
        AppVariant appVariant = th.appVariant4AllTenants;
        GeoArea callerHomeArea = caller.getHomeArea();
        assertThat(callerHomeArea).isNotNull();
        assertThat(callerHomeArea.getZip()).hasSizeGreaterThanOrEqualTo(5);
        assertThat(callerHomeArea.getName()).hasSizeGreaterThanOrEqualTo(5);

        //url variable homoArea.id is not defined
        convenienceBlutspende.setUrl(
                "https://www.blutspendedienst.com/suche?term={homeArea.zipCodes} {homeArea.name}&radius=20&special={homoArea.id}");
        convenienceBlutspende = convenienceRepository.save(convenienceBlutspende);

        String expectedUrlBlutspende =
                "https://www.blutspendedienst.com/suche?term=" + callerHomeArea.getZip() + "%20" +
                        callerHomeArea.getName() + "&radius=20&special=";

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(convenienceBlutspende.getId()))
                .andExpect(jsonPath("$.url").value(expectedUrlBlutspende));

        //not a url
        convenienceBlutspende.setUrl("This is not a url, but I live in {homeArea.name}");
        convenienceBlutspende = convenienceRepository.save(convenienceBlutspende);

        expectedUrlBlutspende = "This%20is%20not%20a%20url,%20but%20I%20live%20in%20" + callerHomeArea.getName();

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(convenienceBlutspende.getId()))
                .andExpect(jsonPath("$.url").value(expectedUrlBlutspende));
    }

    @Test
    public void getConvenienceById_NotFound() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", UUID.randomUUID().toString())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.CONVENIENCE_NOT_FOUND));

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", "🐖")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.CONVENIENCE_NOT_FOUND));
    }

    @Test
    public void getConvenienceById_Deleted() throws Exception {

        Person caller = th.personFriedaFischer;
        AppVariant appVariant = th.appVariant4AllTenants;

        convenienceBlutspende.setDeleted(true);
        convenienceBlutspende.setDeletionTime(12021983L);
        convenienceBlutspende = convenienceRepository.save(convenienceBlutspende);

        mockMvc.perform(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.CONVENIENCE_NOT_FOUND));
    }

    @Test
    public void getConvenienceById_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/convenience/{convenienceId}", convenienceBlutspende.getId()),
                th.appVariant4AllTenants, th.personAnnikaSchneider);
    }

}
