/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AccountEntryType;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementLevelRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementPersonPairingRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AchievementRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;

@Component
public class MotivationTestHelper extends BaseSharedTestHelper {

    @Autowired
    public AccountEntryRepository accountEntryRepository;

    @Autowired
    protected PersonAccountRepository personAccountRepository;

    @Autowired
    protected TenantAccountRepository tenantAccountRepository;

    @Autowired
    public AchievementPersonPairingRepository achievementPersonPairingRepository;

    @Autowired
    protected AchievementRepository achievementRepository;

    @Autowired
    protected AchievementLevelRepository achievementLevelRepository;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private PushCategoryRepository pushCategoryRepository;

    public List<PersonAccount> personAccountList;
    protected PersonAccount accountPersonVgAdmin;
    protected PersonAccount accountPersonIeseAdmin;
    protected PersonAccount accountPersonPoolingStationOp;
    protected PersonAccount accountPersonRegularAnna;

    protected List<AccountEntry> accountEntryList;
    protected AccountEntry intialAccountEntryTenant1;
    protected AccountEntry intialAccountEntryTenant2;
    protected AccountEntry intialAccountEntryPersonVgAdmin;
    public AccountEntry offerInquiryAccountEntry1_1;
    public AccountEntry offerInquiryAccountEntry1_2;
    public AccountEntry offerInquiryAccountEntry2_1;
    public AccountEntry offerInquiryAccountEntry2_2;
    public AccountEntry seekingInquiryAccountEntry1_1;
    public AccountEntry seekingInquiryAccountEntry1_2;
    public AccountEntry seekingInquiryAccountEntry2_1;
    public AccountEntry seekingInquiryAccountEntry2_2;

    protected List<TenantAccount> tenantAccountList;
    public TenantAccount accountTenant1;
    protected TenantAccount accountTenant2;

    protected MediaItem achievementIcon;
    protected MediaItem achievementIconAchieved;
    protected MediaItem achievementIconNotAchieved;

    protected long achievedTimestamp;

    public AchievementPersonPairing achievementPersonPairingPoolingStationOp;

    public App app1;

    public PushCategory pushCategory_app1_1;

    protected static final String achievementLevel1ChallengeDescription = "Rufe Service A einmal auf";
    protected static final String achievementLevel2ChallengeDescription = "Rufe Service A zweimal auf";

    protected static final String demoAchievementDescription = "Rufe Services auf um Awards zu bekommen";
    protected static final String achievementLevel1Description = "Du hast zum ersten mal diesen Service aufgerufen";
    protected static final String achievementLevel2Description = "Du hast zum zweiten mal diesen Service aufgerufen";

    protected static final String demoAchievementName = "Test-Award";
    protected static final String achievementLevel1Name = "Novize";
    protected static final String achievementLevel2Name = "Experte";

    public void createTenantAccount(Tenant tenant) {
        for (AppAccountType appAccountType : AppAccountType.values()) {
            TenantAccount tenantAccount = new TenantAccount();
            tenantAccount.setOwner(tenant);
            tenantAccount.setAppAccountType(appAccountType);
            tenantAccount.setBalance(1000);
            tenantAccountRepository.saveAndFlush(tenantAccount);
        }
    }

    public void createScoreEntities() {

        accountEntryList = new ArrayList<>(20);

        accountTenant1 = new TenantAccount();
        accountTenant1.setOwner(tenant1);
        accountTenant1.setAppAccountType(AppAccountType.GENERAL);
        accountTenant1.setBalance(1000 - (3 * 100 + 110));
        accountTenant1.setCreated(nextTimeStamp());
        accountTenant1 = tenantAccountRepository.save(accountTenant1);

        intialAccountEntryTenant1 = accountEntryRepository.save(new AccountEntry(
                accountTenant1,
                AccountEntryType.PLUS,
                1000,
                null,
                "Initial score",
                "", //subjectMarker
                "", //subjectName
                "", //subjectId
                nextTimeStamp()).withCreated(nextTimeStamp()));
        accountEntryList.add(intialAccountEntryTenant1);

        accountTenant2 = new TenantAccount();
        accountTenant2.setOwner(tenant2);
        accountTenant2.setAppAccountType(AppAccountType.GENERAL);
        accountTenant2.setBalance(1000);
        accountTenant2.setCreated(nextTimeStamp());
        accountTenant2 = tenantAccountRepository.save(accountTenant2);

        intialAccountEntryTenant2 = accountEntryRepository.save(new AccountEntry(
                accountTenant2,
                AccountEntryType.PLUS,
                1000,
                null,
                "Initial score",
                "", //subjectMarker
                "", //subjectName
                "", //subjectId
                nextTimeStamp()).withCreated(nextTimeStamp()));
        accountEntryList.add(intialAccountEntryTenant2);

        tenantAccountList = Arrays.asList(accountTenant1, accountTenant2);

        accountPersonVgAdmin = new PersonAccount();
        accountPersonVgAdmin.setOwner(personVgAdmin);
        accountPersonVgAdmin.setBalance(100 + 10 - 30 - 10);
        accountPersonVgAdmin.setCreated(nextTimeStamp());
        accountPersonVgAdmin = personAccountRepository.save(accountPersonVgAdmin);
        Pair<AccountEntry, AccountEntry> initialEntries = bookInitialScore(accountPersonVgAdmin, accountTenant1, 100);
        intialAccountEntryPersonVgAdmin = initialEntries.getLeft();
        accountEntryList.add(initialEntries.getLeft());
        accountEntryList.add(initialEntries.getRight());

        accountPersonIeseAdmin = new PersonAccount();
        accountPersonIeseAdmin.setOwner(personIeseAdmin);
        accountPersonIeseAdmin.setBalance(100 - 10 + 30 + 10);
        accountPersonIeseAdmin.setCreated(nextTimeStamp());
        accountPersonIeseAdmin = personAccountRepository.save(accountPersonIeseAdmin);
        initialEntries = bookInitialScore(accountPersonIeseAdmin, accountTenant1, 100);
        accountEntryList.add(initialEntries.getLeft());
        accountEntryList.add(initialEntries.getRight());

        accountPersonPoolingStationOp = new PersonAccount();
        accountPersonPoolingStationOp.setOwner(personPoolingStationOp);
        accountPersonPoolingStationOp.setBalance(110);
        accountPersonPoolingStationOp.setCreated(nextTimeStamp());
        accountPersonPoolingStationOp = personAccountRepository.save(accountPersonPoolingStationOp);
        initialEntries = bookInitialScore(accountPersonPoolingStationOp, accountTenant1, 110);
        accountEntryList.add(initialEntries.getLeft());
        accountEntryList.add(initialEntries.getRight());

        accountPersonRegularAnna = new PersonAccount();
        accountPersonRegularAnna.setOwner(personRegularAnna);
        accountPersonRegularAnna.setBalance(100);
        accountPersonRegularAnna.setCreated(nextTimeStamp());
        accountPersonRegularAnna = personAccountRepository.save(accountPersonRegularAnna);
        initialEntries = bookInitialScore(accountPersonRegularAnna, accountTenant1, 100);
        accountEntryList.add(initialEntries.getLeft());
        accountEntryList.add(initialEntries.getRight());

        personAccountList = Arrays.asList(accountPersonVgAdmin, accountPersonIeseAdmin, accountPersonPoolingStationOp, accountPersonRegularAnna);

        long auxCreated = nextTimeStamp();
        offerInquiryAccountEntry1_1   =
                new AccountEntry(
                        accountPersonVgAdmin,
                        AccountEntryType.PLUS,
                        10,
                        accountPersonIeseAdmin,
                        "Inquiry 1 offerer account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        offerInquiryAccountEntry1_1.setCreated(auxCreated);
        offerInquiryAccountEntry1_1   = accountEntryRepository.save(offerInquiryAccountEntry1_1);
        accountEntryList.add(offerInquiryAccountEntry1_1);

        auxCreated = nextTimeStamp();
        offerInquiryAccountEntry1_2   =
                new AccountEntry(
                        accountPersonIeseAdmin,
                        AccountEntryType.MINUS,
                        -10,
                        accountPersonVgAdmin,
                        "Inquiry 1 inquirer account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        offerInquiryAccountEntry1_2.setCreated(auxCreated);
        offerInquiryAccountEntry1_2   = accountEntryRepository.save(offerInquiryAccountEntry1_2);
        accountEntryList.add(offerInquiryAccountEntry1_2);

        auxCreated = nextTimeStamp();
        offerInquiryAccountEntry2_1   =
                new AccountEntry(
                        accountPersonIeseAdmin,
                        AccountEntryType.PLUS,
                        30,
                        accountPersonVgAdmin,
                        "Inquiry 2 offerer account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        offerInquiryAccountEntry2_1.setCreated(auxCreated);
        offerInquiryAccountEntry2_1   = accountEntryRepository.save(offerInquiryAccountEntry2_1);
        accountEntryList.add(offerInquiryAccountEntry2_1);

        auxCreated = nextTimeStamp();
        offerInquiryAccountEntry2_2   =
                new AccountEntry(
                        accountPersonVgAdmin,
                        AccountEntryType.MINUS,
                        -30,
                        accountPersonIeseAdmin,
                        "Inquiry 2 inquierer account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        offerInquiryAccountEntry2_2.setCreated(auxCreated);
        offerInquiryAccountEntry2_2   = accountEntryRepository.save(offerInquiryAccountEntry2_2);
        accountEntryList.add(offerInquiryAccountEntry2_2);

        auxCreated = nextTimeStamp();
        seekingInquiryAccountEntry1_1 =
                new AccountEntry(
                        accountPersonVgAdmin,
                        AccountEntryType.MINUS,
                        -10,
                        accountPersonIeseAdmin,
                        "Inquiry 1 seeker account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        seekingInquiryAccountEntry1_1.setCreated(auxCreated);
        seekingInquiryAccountEntry1_1 = accountEntryRepository.save(seekingInquiryAccountEntry1_1);
        accountEntryList.add(seekingInquiryAccountEntry1_1);

        auxCreated = nextTimeStamp();
        seekingInquiryAccountEntry1_2 =
                new AccountEntry(
                        accountPersonIeseAdmin,
                        AccountEntryType.PLUS,
                        10,
                        accountPersonVgAdmin,
                        "Inquiry 1 inquirer account entry",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        seekingInquiryAccountEntry1_2.setCreated(auxCreated);
        seekingInquiryAccountEntry1_2 = accountEntryRepository.save(seekingInquiryAccountEntry1_2);
        accountEntryList.add(seekingInquiryAccountEntry1_2);

        auxCreated = nextTimeStamp();
        seekingInquiryAccountEntry2_1 =
                new AccountEntry(
                        accountPersonIeseAdmin,
                        AccountEntryType.RESERVATION_MINUS,
                        -10,
                        accountPersonVgAdmin,
                        "Testing accountService - bookReservedScore, checkReservationsMatch && checkIsBookingWithinCreditLimitIgnoreReserved",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        seekingInquiryAccountEntry2_1.setCreated(auxCreated);
        seekingInquiryAccountEntry2_1 = accountEntryRepository.save(seekingInquiryAccountEntry2_1);
        accountEntryList.add(seekingInquiryAccountEntry2_1);

        auxCreated = nextTimeStamp();
        seekingInquiryAccountEntry2_2 =
                new AccountEntry(
                        accountPersonVgAdmin,
                        AccountEntryType.RESERVATION_PLUS,
                        10,
                        accountPersonIeseAdmin,
                        "Testing accountService - bookReservedScore, checkReservationsMatch && checkIsBookingWithinCreditLimitIgnoreReserved",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp());
        seekingInquiryAccountEntry2_2.setCreated(auxCreated);
        seekingInquiryAccountEntry2_2 = accountEntryRepository.save(seekingInquiryAccountEntry2_2);
        accountEntryList.add(seekingInquiryAccountEntry2_2);
    }

    private Pair<AccountEntry, AccountEntry> bookInitialScore(PersonAccount target, TenantAccount source, int amount) {
        return Pair.of(
                accountEntryRepository.save(new AccountEntry(
                        target,
                        AccountEntryType.PLUS,
                        amount,
                        source,
                        "Initial score",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                        nextTimeStamp()).withCreated(nextTimeStamp())),
                accountEntryRepository.save(new AccountEntry(
                        source,
                        AccountEntryType.MINUS,
                        -amount,
                        target,
                        "Initial score",
                        "", //subjectMarker
                        "", //subjectName
                        "", //subjectId
                nextTimeStamp()).withCreated(nextTimeStamp())));
    }

    public void createAchievementEntities() {

        achievementIcon = mediaItemRepository.save(new MediaItem());

        achievementIconNotAchieved = mediaItemRepository.save(new MediaItem());
        achievementIconAchieved = mediaItemRepository.save(new MediaItem());

        Achievement demoAchievement = achievementRepository.save(Achievement.builder()
                .name(demoAchievementName)
                .category("Category1")
                .description(demoAchievementDescription)
                .pushCategory(pushCategory_app1_1)
                .icon(achievementIcon)
                .build());

        AchievementLevel achievementLevel1 = achievementLevelRepository.save(AchievementLevel.builder()
                .name(achievementLevel1Name)
                .orderValue(1)
                .description(achievementLevel1Description)
                .challengeDescription(achievementLevel1ChallengeDescription)
                .icon(achievementIconAchieved)
                .iconNotAchieved(achievementIconNotAchieved)
                .achievement(demoAchievement)
                .build());

        achievementLevelRepository.save(AchievementLevel.builder()
                .name(achievementLevel2Name)
                .orderValue(2)
                .description(achievementLevel2Description)
                .challengeDescription(achievementLevel2ChallengeDescription)
                .icon(achievementIconAchieved)
                .iconNotAchieved(achievementIconNotAchieved)
                .achievement(demoAchievement)
                .build());

        achievedTimestamp = System.currentTimeMillis();
        achievementPersonPairingPoolingStationOp = AchievementPersonPairing.builder()
                .person(personPoolingStationOp)
                .achievementLevel(achievementLevel1)
                .timestamp(achievedTimestamp)
                .build();

        achievementPersonPairingPoolingStationOp =
                achievementPersonPairingRepository.save(achievementPersonPairingPoolingStationOp);
    }

    public void createPushEntities() {

        app1 = appRepository.save(App.builder()
                .name("App1")
                .appIdentifier("app1_appid_short")
                .build());

        pushCategory_app1_1 = pushCategoryRepository.save(PushCategory.builder()
                .app(app1)
                .name("pushCategory1App1")
                .description("pushCategory1App1 description")
                .defaultLoudPushEnabled(true)
                .configurable(true)
                .build());

        //we need these categories for creating the achievement rules
        createLieferBarPushCategory();

        appRepository.flush();
        pushCategoryRepository.flush();
    }

    private void createLieferBarPushCategory() {
        App appLieferbar = appRepository.save(App.builder()
                .name("LieferBar")
                .appIdentifier("dd-logistics")
                .build());

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Digitaler und Achievements")
                .description("Digitaler und Achievementes erhalten")
                .defaultLoudPushEnabled(true)
                .configurable(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID));
    }

}
