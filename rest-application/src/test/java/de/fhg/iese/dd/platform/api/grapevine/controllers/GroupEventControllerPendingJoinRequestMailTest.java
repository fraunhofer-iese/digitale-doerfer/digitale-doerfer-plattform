/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class GroupEventControllerPendingJoinRequestMailTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private IAuthorizationService authorizationService;

    private AppVariant appVariant;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        th.createPushEntities();

        appVariant = th.appVariantKL_EB;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void groupPendingJoinAcceptRequestMail_withDoubleAcceptAndDeny() throws Exception {

        Group group = th.groupSchachvereinAAA;
        Person personToJoin = th.personAnjaTenant1Dorf1;
        // Ben has group admin rights
        Person groupAdmin = th.personBenTenant1Dorf2;
        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(personToJoin, group))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeApproval = th.groupMembershipRepository.findByGroupAndMember(
                group, personToJoin).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeApproval.getStatus());

        // Try to accept pending join request. Ben has group admin rights
        mockMvc.perform(buildJoinAcceptRequestMail(groupAdmin, group, personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinAcceptRequest.builder()
                                .group(group)
                                .personToAccept(personToJoin)
                                .acceptingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(true));

        GroupMembership groupMembershipAfterApproval = th.groupMembershipRepository.findByGroupAndMember(
                group, personToJoin).get();

        // Status should now be approved in repository
        assertEquals(GroupMembershipStatus.APPROVED, groupMembershipAfterApproval.getStatus());

        // Accept join request again and expect status to not change
        mockMvc.perform(buildJoinAcceptRequestMail(groupAdmin, group, personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinAcceptRequest.builder()
                                .group(group)
                                .personToAccept(personToJoin)
                                .acceptingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // Now deny request and expect status to not change
        mockMvc.perform(buildJoinDenyRequestMail(groupAdmin, group, personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinDenyRequest.builder()
                                .group(group)
                                .personToDeny(personToJoin)
                                .denyingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));
    }

    @Test
    public void groupPendingJoinAcceptRequestMailUnauthorized() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1, th.groupSchachvereinAAA))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeApproval = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeApproval.getStatus());

        // Try to accept pending join request. Chloe isn't even in the group
        mockMvc.perform(buildJoinAcceptRequestMail(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1, "invalid-token"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterApprovalChloe = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterApprovalChloe.getStatus());

        // Try to accept pending join request. Ben has group admin rights but wrong mailToken
        mockMvc.perform(buildJoinAcceptRequestMail(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1, "invalid-token"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterApprovalBen = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterApprovalBen.getStatus());
    }

    @Test
    public void groupPendingJoinDenyRequestMail_withDoubleDenyAndAccept() throws Exception {

        Group group = th.groupSchachvereinAAA;
        Person personToJoin = th.personAnjaTenant1Dorf1;
        // Ben has group admin rights
        Person groupAdmin = th.personBenTenant1Dorf2;
        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(personToJoin, group))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeApproval = th.groupMembershipRepository.findByGroupAndMember(
                group, personToJoin).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeApproval.getStatus());

        // Try to deny pending join request.
        mockMvc.perform(buildJoinDenyRequestMail(groupAdmin, group,
                personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinDenyRequest.builder()
                                .group(group)
                                .personToDeny(personToJoin)
                                .denyingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(true));

        GroupMembership groupMembershipAfterDenial = th.groupMembershipRepository.findByGroupAndMember(
                group, personToJoin).get();

        // Status should now be rejected in repository
        assertEquals(GroupMembershipStatus.REJECTED, groupMembershipAfterDenial.getStatus());

        // Deny pending join request again and expect status to not change
        mockMvc.perform(buildJoinDenyRequestMail(groupAdmin, group,
                personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinDenyRequest.builder()
                                .group(group)
                                .personToDeny(personToJoin)
                                .denyingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // Accept pending join request and expect status to not change
        mockMvc.perform(buildJoinAcceptRequestMail(groupAdmin, group,
                personToJoin,
                authorizationService.generateAuthToken(GroupPendingJoinAcceptRequest.builder()
                                .group(group)
                                .personToAccept(personToJoin)
                                .acceptingPerson(groupAdmin)
                                .build(),
                        Duration.ofMinutes(5))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(group.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));
    }

    @Test
    public void groupPendingJoinDenyRequestMailUnauthorized() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1, th.groupSchachvereinAAA))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeDenial = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeDenial.getStatus());

        // Try to deny pending join request. Chloe isn't even in the group
        mockMvc.perform(buildJoinDenyRequestMail(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1, "invalid-token"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterDenialChloe = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterDenialChloe.getStatus());

        // Try to accept pending join request. Ben has group admin rights but wrong mailToken
        mockMvc.perform(buildJoinDenyRequestMail(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1, "invalid-token"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterDenialBen = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterDenialBen.getStatus());
    }

    private MockHttpServletRequestBuilder buildJoinRequest(Person caller, Group group) throws IOException {
        return post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(group.getId())
                        .memberIntroductionText("Huhu")
                        .build()));
    }

    private MockHttpServletRequestBuilder buildJoinAcceptRequestMail(Person acceptingPerson, Group group,
            Person personToAccept, String mailToken) throws IOException {
        return post("/grapevine/group/event/groupPendingJoinAcceptRequest/mail")
                .contentType(contentType)
                .param("mailToken", mailToken)
                .param("acceptingPersonId", acceptingPerson.getId())
                .content(json(ClientGroupPendingJoinAcceptRequest.builder()
                        .groupId(group.getId())
                        .personIdToAccept(personToAccept.getId())
                        .build()));
    }

    private MockHttpServletRequestBuilder buildJoinDenyRequestMail(Person denyingPerson, Group group,
            Person personToDeny, String mailToken) throws IOException {
        return post("/grapevine/group/event/groupPendingJoinDenyRequest/mail")
                .contentType(contentType)
                .param("mailToken", mailToken)
                .param("denyingPersonId", denyingPerson.getId())
                .content(json(ClientGroupPendingJoinDenyRequest.builder()
                        .groupId(group.getId())
                        .personIdToDeny(personToDeny.getId())
                        .build()));
    }

}
