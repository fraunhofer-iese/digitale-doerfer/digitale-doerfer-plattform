/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.security.services;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PasswordWrongException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.Auth0ManagementException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthAccountNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthRegistrationRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles({"test", "integration-test", "Auth0OAuthManagementServiceTest"})
@ExtendWith(AssumeIntegrationTestExtension.class)
public class Auth0OauthManagementServiceTest extends BaseServiceTest {

    @Autowired
    private Auth0OauthManagementService auth0OauthManagementService;
    @Autowired
    private OauthRegistrationRepository oauthRegistrationRepository;

    private final Set<String> oAuthIdsToDelete = new HashSet<>();

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        for (String oauthId : oAuthIdsToDelete) {
            try {
                auth0OauthManagementService.deleteUser(oauthId);
            } catch (Auth0ManagementException e) {
                log.error("Failed to delete test user " + oauthId, e);
            }
        }
        oAuthIdsToDelete.clear();
    }

    @Test
    public void createUser_queryUser_deleteUser() {

        String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        String password = UUID.randomUUID().toString();

        String oAuthId = auth0OauthManagementService.createUser(email, password, true);
        oAuthIdsToDelete.add(oAuthId);

        assertThat(oauthRegistrationRepository.existsByOauthId(oAuthId))
                .as("There should be an oauth registration created if a new user gets created")
                .isTrue();

        OauthAccount oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);

        assertEquals(oAuthId, oauthAccount.getOauthId());
        assertEquals(email, oauthAccount.getName());
        Integer loginCount = oauthAccount.getLoginCount();
        assertTrue(loginCount == null || loginCount == 0, "login count invalid ");
        assertTrue(oauthAccount.isEmailVerified(), "email should be verified");
        assertFalse(oauthAccount.isBlocked(), "account should not be blocked");
        assertNull(oauthAccount.getBlockReason());

        oauthAccount = auth0OauthManagementService.queryUserByEmail(email);

        assertEquals(oAuthId, oauthAccount.getOauthId());
        assertEquals(email, oauthAccount.getName());
        loginCount = oauthAccount.getLoginCount();
        assertTrue(loginCount == null || loginCount == 0, "login count invalid ");
        assertTrue(oauthAccount.isEmailVerified(), "email should be verified");
        assertFalse(oauthAccount.isBlocked(), "account should not be blocked");
        assertNull(oauthAccount.getBlockReason());

        assertEquals(oAuthId, auth0OauthManagementService.getOauthIdForUser(email));
        assertEquals(oAuthId, auth0OauthManagementService.getOauthIdForCustomUser(email));

        auth0OauthManagementService.deleteUser(oAuthId);
        assertThat(oauthRegistrationRepository.existsByOauthId(oAuthId))
                .as("The oauth registration of the deleted person should be deleted, too")
                .isFalse();

        try {
            auth0OauthManagementService.queryUserByOauthId(oAuthId);
            fail("user should be deleted");
        } catch (OauthAccountNotFoundException e) {
            assertTrue(e.getMessage().contains(oAuthId), "exception misses oauth id");
        }
    }

    @Test
    public void createUser_changeEmail_changeEmailVerified_changePassword_deleteUser() throws Exception {

        String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        String newEmail = "integration-test-" + UUID.randomUUID() + "@example.org";
        String password = UUID.randomUUID().toString();

        String oAuthId = auth0OauthManagementService.createUser(email, password, true);
        oAuthIdsToDelete.add(oAuthId);
        assertThat(oauthRegistrationRepository.existsByOauthId(oAuthId))
                .as("There should be an oauth registration created if a new user gets created")
                .isTrue();

        auth0OauthManagementService.changeEmail(oAuthId, newEmail, false);
        assertThat(oauthRegistrationRepository.findByOauthId(oAuthId).getEmail())
                .as("The oauth registration email should be changed, too")
                .isEqualTo(newEmail);

        OauthAccount oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);

        assertEquals(oAuthId, oauthAccount.getOauthId());
        assertEquals(newEmail, oauthAccount.getName());
        assertFalse(oauthAccount.isEmailVerified(), "email should not be verified");

        auth0OauthManagementService.setEmailVerified(oAuthId, true);

        oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);

        assertEquals(newEmail, oauthAccount.getName());
        assertTrue(oauthAccount.isEmailVerified());
        assertTrue(oauthAccount.isEmailVerified(), "email should be verified");

        auth0OauthManagementService.changePassword(oAuthId, (password + "2").toCharArray());
        //no assertions here, we would need to log in, which seems to be a bit too complicated
    }

    @Test
    @Disabled("Errors at Auth0 cause revoking of the access tokens to return 404")
    public void createUser_block_unblock_deleteUser() throws Exception {

        String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        String password = UUID.randomUUID().toString();

        String oAuthId = auth0OauthManagementService.createUser(email, password, true);
        oAuthIdsToDelete.add(oAuthId);

        OauthAccount oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);
        assertThat(oauthAccount.getName()).isEqualTo(email);

        auth0OauthManagementService.blockUser(oAuthId);

        oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);
        assertTrue(oauthAccount.isBlocked(), "account should be blocked");
        assertEquals(OauthAccount.BlockReason.MANUALLY_BLOCKED, oauthAccount.getBlockReason());

        assertTrue(auth0OauthManagementService.isBlocked(oAuthId), "account should be blocked");

        auth0OauthManagementService.unblockUser(oAuthId);

        oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);
        assertFalse(oauthAccount.isBlocked(), "account should not be blocked");
        assertNull(oauthAccount.getBlockReason());

        assertFalse(auth0OauthManagementService.isBlocked(oAuthId), "account should not be blocked");
    }

    @Test
    public void createUser_changePasswordAndVerifyOldPassword_deleteUser() throws Exception {

        final String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        final String password = UUID.randomUUID().toString();

        final String oAuthId = auth0OauthManagementService.createUser(email, password, true);
        oAuthIdsToDelete.add(oAuthId);
        assertThat(oauthRegistrationRepository.existsByOauthId(oAuthId))
                .as("There should be an oauth registration created if a new user gets created")
                .isTrue();

        OauthAccount oauthAccount = auth0OauthManagementService.queryUserByOauthId(oAuthId);

        assertEquals(oAuthId, oauthAccount.getOauthId());
        assertEquals(email, oauthAccount.getName());
        Integer loginCount = oauthAccount.getLoginCount();
        assertTrue(loginCount == null || loginCount == 0, "login count invalid ");
        assertTrue(oauthAccount.isEmailVerified(), "email should be verified");
        assertFalse(oauthAccount.isBlocked(), "account should not be blocked");
        assertNull(oauthAccount.getBlockReason());

        final String newPassword = UUID.randomUUID().toString();
        auth0OauthManagementService.changePasswordAndVerifyOldPassword(oAuthId, email, password.toCharArray(),
                newPassword.toCharArray());

        oauthAccount = auth0OauthManagementService.queryUserByEmail(email);

        assertEquals(oAuthId, oauthAccount.getOauthId());
        assertEquals(email, oauthAccount.getName());
        loginCount = oauthAccount.getLoginCount();
        // the old password needs to be verified with a successful login
        assertEquals(1, (int) loginCount, "login count invalid ");
        assertTrue(oauthAccount.isEmailVerified(), "email should be verified");
        assertFalse(oauthAccount.isBlocked(), "account should not be blocked");
        assertNull(oauthAccount.getBlockReason());
    }

    @Test
    public void changePasswordAndVerifyOldPassword_WrongPassword() throws Exception {

        final String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        final String password = UUID.randomUUID().toString();

        final String oAuthId = auth0OauthManagementService.createUser(email, password, true);
        oAuthIdsToDelete.add(oAuthId);

        final String newPassword = UUID.randomUUID().toString();
        try {
            auth0OauthManagementService.changePasswordAndVerifyOldPassword(oAuthId, email,
                    "invalid-password".toCharArray(), newPassword.toCharArray());
            fail("user should not login and change password");
        } catch (PasswordWrongException e) {
            assertTrue(e.getMessage().contains(email), "exception misses " + email);
        }
    }

    @Test
    @Disabled("Errors at Auth0 cause revoking of the access tokens to return 404")
    public void invalidOAuthId() throws Exception {

        String email = "integration-test-" + UUID.randomUUID() + "@example.org";
        String invalidId = "auth0|" + UUID.randomUUID();

        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.queryUserByOauthId(invalidId));
        assertThrowsOauthAccountNotFoundException(email,
                () -> auth0OauthManagementService.queryUserByEmail(email));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.changeEmail(invalidId, email, true));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.changePassword(invalidId, "password".toCharArray()));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.changePasswordAndVerifyOldPassword(invalidId, email,
                        "oldPassword".toCharArray(), "password".toCharArray()));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.blockUser(invalidId));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.unblockUser(invalidId));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.unblockAutomaticallyBlockedUser(invalidId));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.triggerVerificationMail(invalidId));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.setEmailVerified(invalidId, true));
        assertThrowsOauthAccountNotFoundException(invalidId,
                () -> auth0OauthManagementService.isBlocked(invalidId));
        //this should not throw an exception
        auth0OauthManagementService.revokeAllRefreshTokens(invalidId);
        auth0OauthManagementService.deleteUser(invalidId);
    }

    @Test
    public void getOauthClients() throws Exception {

        List<OauthClient> configuredOauthClients = auth0OauthManagementService.getConfiguredOauthClients();

        configuredOauthClients.forEach(log::debug);

        assertThat(configuredOauthClients)
                .isNotEmpty()
                .extracting(OauthClient::getName).contains("backend-integration-test");
    }

    private void assertThrowsOauthAccountNotFoundException(String expectedExceptionText, Runnable action) {
        try {
            action.run();
            fail("user should not be found");
        } catch (OauthAccountNotFoundException e) {
            assertTrue(e.getMessage().contains(expectedExceptionText), "exception misses " + expectedExceptionText);
        }
    }

}
