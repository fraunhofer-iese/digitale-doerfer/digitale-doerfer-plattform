/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.tenant.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoAreaExtended;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientTenant;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientTenantExtended;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalConfigurationAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TenantAdminUiControllerTest extends BaseServiceTest {

    private static final Sort DEFAULT_TENANT_SORT = Sort.by("name");

    @Autowired
    private ParticipantsTestHelper th;

    private Person personGlobalConfigurationAdmin;
    private Person personUserAdminTenant1;

    @Override
    public void createEntities() {
        // ensure that the "super admin tenant" is deleted
        th.deleteAllData();

        th.createTenantsAndGeoAreas();
        th.createPersons();

        personGlobalConfigurationAdmin =
                th.createPersonWithDefaultAddress("GlobalConfigurationAdmin", th.tenant1,
                        "18256640-3421-422f-992c-7306235e62e");
        th.assignRoleToPerson(personGlobalConfigurationAdmin, GlobalConfigurationAdmin.class, null);

        personUserAdminTenant1 =
                th.createPersonWithDefaultAddress("UserAdminTenant1", th.tenant1,
                        "54c14492-4f9b-4e04-9e4f-7791fc700840");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getTenantById() throws Exception {

        mockMvc.perform(get("/adminui/tenant/{tenantId}", th.tenant2.getId())
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(toClientTenant(th.tenant2)));

        mockMvc.perform(get("/adminui/tenant/{tenantId}", th.tenant1.getId())
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(toClientTenant(th.tenant1)));
    }

    @Test
    public void getTenantById_InvalidId() throws Exception {

        mockMvc.perform(get("/adminui/tenant/{tenantId}", BaseTestHelper.INVALID_UUID)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getTenantById_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/tenant/{tenantId}", th.tenant2.getId())
                .contentType(contentType));

        mockMvc.perform(get("/adminui/tenant/{tenantId}", th.tenant2.getId())
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());

        mockMvc.perform(get("/adminui/tenant/{tenantId}", th.tenant2.getId())
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllTenants() throws Exception {

        List<Tenant> expectedTenants = th.tenantList;

        assertEquals(3, expectedTenants.size());

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        3)));

        // search parameter empty
        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", "")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        3)));

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.singletonList(toClientTenant(th.tenant1)),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        // search parameter empty
        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personUserAdminTenant1))
                .param("search", "")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.singletonList(toClientTenant(th.tenant1)),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));
    }

    @Test
    public void getAllTenants_Paged() throws Exception {

        List<Tenant> expectedTenants = th.tenantList;

        assertEquals(3, expectedTenants.size());

        mockMvc.perform(get("/adminui/tenant")
                .param("page", "0")
                .param("count", "2")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.subList(0, 2).stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 2, DEFAULT_TENANT_SORT),
                        3)));
        mockMvc.perform(get("/adminui/tenant")
                .param("page", "1")
                .param("count", "2")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.subList(2, 3).stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(1, 2, DEFAULT_TENANT_SORT),
                        3)));
    }

    @Test
    public void getAllTenants_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/tenant")
                .contentType(contentType));

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllTenantsWithSearch_Infix() throws Exception {

        final List<Tenant> expectedTenants = th.tenantList;

        assertEquals(3, expectedTenants.size());

        // search for tenants with id LIKE '74dc2ee1-025e-4f5e-82bb-34b552e292e4' (tenant1)
        final String idInfix = "74dc2ee1-025e-4f5e-82bb-34b552e292e4";
        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", idInfix)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getId().toLowerCase().contains(idInfix))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.singletonList(toClientTenant(th.tenant1)),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        // search for tenants with name LIKE 'VG'
        final String nameInfix = "vg";
        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", nameInfix)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getName().toLowerCase().contains(nameInfix))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        3)));

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.singletonList(toClientTenant(th.tenant1)),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        // search for tenants with tag LIKE 'bay'
        final String tagInfix = "rlp";
        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", tagInfix)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getTag().toLowerCase().contains(tagInfix))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        2)));

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.singletonList(toClientTenant(th.tenant1)),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        // no result for infix
        final String infix = "blablub";
        mockMvc.perform(get("/adminui/tenant")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .param("search", infix)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 0))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getName().toLowerCase().contains(infix))
                                .filter(t -> t.getTag().toLowerCase().contains(infix))
                                .map(this::toClientTenant)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        0)));

        mockMvc.perform(get("/adminui/tenant")
                        .headers(authHeadersFor(personUserAdminTenant1))
                        .param("search", infix)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 0))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(new PageImpl<>(
                        Collections.emptyList(),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        0)));
    }

    @Test
    public void getAllTenantsWithSearch_SearchParamToShort() throws Exception {

        mockMvc.perform(get("/adminui/tenant")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", "a")
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));
    }

    @Test
    public void getTenantExtendedById() throws Exception {

        mockMvc.perform(get("/adminui/tenant/{tenantId}/extended", th.tenant2.getId())
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(toClientTenantExtended(th.tenant2)));
    }

    @Test
    public void getTenantExtendedById_InvalidId() throws Exception {

        mockMvc.perform(get("/adminui/tenant/{tenantId}/extended", BaseTestHelper.INVALID_UUID)
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void getTenantExtendedById_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/tenant/{tenantId}/extended", th.tenant2.getId())
                .contentType(contentType));

        mockMvc.perform(get("/adminui/tenant/{tenantId}/extended", th.tenant2.getId())
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());

        mockMvc.perform(get("/adminui/tenant/{tenantId}/extended", th.tenant1.getId())
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllTenantsExtended() throws Exception {

        List<Tenant> expectedTenants = th.tenantList;

        assertEquals(3, expectedTenants.size());

        mockMvc.perform(get("/adminui/tenant/extended")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenantExtended)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT), 3)));

        // search parameter empty
        mockMvc.perform(get("/adminui/tenant/extended")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", "")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .map(this::toClientTenantExtended)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        3)));
    }

    @Test
    public void getAllTenantsExtended_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/tenant/extended")
                .contentType(contentType));

        mockMvc.perform(get("/adminui/tenant/extended")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType))
                .andExpect(isNotAuthorized());

        mockMvc.perform(get("/adminui/tenant/extended")
                .headers(authHeadersFor(personUserAdminTenant1))
                .contentType(contentType))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getAllTenantsExtendedWithSearch_Infix() throws Exception {

        final List<Tenant> expectedTenants = th.tenantList;

        assertEquals(3, expectedTenants.size());

        // search for tenants with name LIKE 'VG'
        final String nameInfix = "vg";
        mockMvc.perform(get("/adminui/tenant/extended")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .param("search", nameInfix)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 3))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getName().toLowerCase().contains(nameInfix))
                                .map(this::toClientTenantExtended)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        3)));

        // search for tenants with tag LIKE 'bay'
        final String tagInfix = "bay";
        mockMvc.perform(get("/adminui/tenant/extended")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .param("search", tagInfix)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 1))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getTag().toLowerCase().contains(tagInfix))
                                .map(this::toClientTenantExtended)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        1)));

        // no result for infix
        final String infix = "blablub";
        mockMvc.perform(get("/adminui/tenant/extended")
                        .headers(authHeadersFor(personGlobalConfigurationAdmin))
                        .param("search", infix)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, 10, 0))
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedTenants.stream()
                                .sorted(Comparator.comparing(Tenant::getName))
                                .filter(t -> t.getName().toLowerCase().contains(infix))
                                .filter(t -> t.getTag().toLowerCase().contains(infix))
                                .map(this::toClientTenantExtended)
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 10, DEFAULT_TENANT_SORT),
                        0)));
    }

    @Test
    public void getAllTenantsExtendedWithSearch_SearchParamToShort() throws Exception {

        mockMvc.perform(get("/adminui/tenant/extended")
                .headers(authHeadersFor(personGlobalConfigurationAdmin))
                .param("search", "a")
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));
    }

    private ClientTenant toClientTenant(Tenant tenant) {
        if (tenant == null) {
            return null;
        }
        return ClientTenant.builder()
                .id(tenant.getId())
                .name(tenant.getName())
                .profilePicture(th.toClientMediaItem(tenant.getProfilePicture()))
                .build();
    }

    @SuppressWarnings("deprecation")
    private ClientTenantExtended toClientTenantExtended(Tenant tenant) {
        if (tenant == null) {
            return null;
        }
        final GeoArea geoArea = tenant.getRootArea();
        return ClientTenantExtended.builder()
                .id(tenant.getId())
                .name(tenant.getName())
                .profilePicture(th.toClientMediaItem(tenant.getProfilePicture()))
                .tag(tenant.getTag())
                .rootArea(ClientGeoAreaExtended.builder()
                        .parentId(BaseEntity.getIdOf(geoArea.getParentArea()))
                        .id(geoArea.getId())
                        .name(geoArea.getName())
                        .geoAreaType(geoArea.getType())
                        .logo(th.toClientMediaItem(geoArea.getLogo()))
                        .zip(geoArea.getZip())
                        .center(geoArea.getCenter() == null ? null :
                                new ClientGPSLocation(geoArea.getCenter().getLatitude(),
                                        geoArea.getCenter().getLongitude()))
                        .customAttributesJson(geoArea.getCustomAttributesJson())
                        .tenantId(BaseEntity.getIdOf(geoArea.getTenant()))
                        .leaf(geoArea.isLeaf())
                        .depth(geoArea.getDepth())
                        .countChildren(geoArea.getCountChildren())
                        .countDirectChildren(geoArea.getCountDirectChildren())
                        .build())
                .build();
    }

}
