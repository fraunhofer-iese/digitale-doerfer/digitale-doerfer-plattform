/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.dataprivacy.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.dataprivacy.clientmodel.ClientDataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInternalDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollectionAppVariantResponse;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowRepository;

@Tag("data-privacy")
public class DataPrivacyControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private ParticipantsConfig participantsConfig;
    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;
    @Autowired
    private DataPrivacyWorkflowRepository dataPrivacyWorkflowRepository;
    @Autowired
    private DataPrivacyDataCollectionRepository dataCollectionRepository;
    @Autowired
    private DataPrivacyDataCollectionAppVariantResponseRepository dataPrivacyDataCollectionAppVariantResponseRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantRepository dataPrivacyWorkflowAppVariantRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantResponseRepository dataPrivacyWorkflowAppVariantResponseRepository;
    @Autowired
    private DataPrivacyDataCollectionAppVariantResponseRepository dataCollectionAppVariantResponseRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantStatusRecordRepository dataPrivacyWorkflowAppVariantStatusRecordRepository;

    @SpyBean
    private ICommonDataPrivacyService commonDataPrivacyServiceSpy;
    @SpyBean
    private IInternalDataPrivacyService internalDataPrivacyServiceSpy;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createLegalEntities();
        th.createUserDataEntities(th.personRegularAnna);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void dataPrivacyReport_NoExternalSystem() throws Exception {

        assertThat(dataCollectionRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);

        final Person person = th.personRegularAnna;

        AppVariant usedAppVariant = th.app1Variant1;
        assertThat(usedAppVariant.hasExternalSystem()).isFalse();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(usedAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());

        mockMvc.perform(post("/dataprivacy/report/mail")
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyString())));

        waitForEventProcessingLong();

        //verify that data privacy service has been called for the correct person
        Mockito.verify(commonDataPrivacyServiceSpy).startDataPrivacyDataCollection(
                ArgumentMatchers.eq(person), ArgumentMatchers.eq(DataPrivacyWorkflowTrigger.USER_TRIGGERED));

        //check that the according entities have been created
        assertThat(dataCollectionRepository.count()).isEqualTo(1);
        assertThat(dataCollectionRepository.findAll().get(0).getPerson()).isEqualTo(person);

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        //verify that email has been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(1);
        TestEmailSenderService.TestEmail mail = emailSenderServiceMock.getEmails().get(0);
        assertThat(mail.getSubject()).isEqualTo("Deine angeforderten Daten");
        assertThat(mail.getText()).contains(th.personRegularAnna.getFirstName());
        assertThat(mail.isHtml()).isTrue();

        //Check that zip file has been written
        assertThat(testFileStorage.findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER))
                .as("file storage should contain Datenschutzbericht.zip")
                .anyMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip"));
    }

    @Test
    public void dataPrivacyReport_ExternalSystemDeactivated() throws Exception {

        assertThat(dataCollectionRepository.count()).isEqualTo(0);
        //there should not be any app variant usage, so that we can control it in the test
        assertThat(th.appVariantUsageRepository.count()).isEqualTo(0);
        assertThat(dataPrivacyConfig.getExternalSystemDataPrivacyWorkflow().isEnabled())
                .as("The test is for disabled external system data collection")
                .isFalse();

        final Person person = th.personRegularAnna;

        createExternalAppVariant(person);

        mockMvc.perform(post("/dataprivacy/report/mail")
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyString())));

        waitForEventProcessing();

        //verify that data privacy service has been called for the correct person
        Mockito.verify(commonDataPrivacyServiceSpy).startDataPrivacyDataCollection(
                ArgumentMatchers.eq(person), ArgumentMatchers.eq(DataPrivacyWorkflowTrigger.USER_TRIGGERED));

        //check that the according entities have been created
        assertThat(dataCollectionRepository.count()).isEqualTo(1);
        assertThat(dataCollectionRepository.findAll().get(0).getPerson()).isEqualTo(person);

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        //verify that email has been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(1);
        TestEmailSenderService.TestEmail mail = emailSenderServiceMock.getEmails().get(0);
        assertThat(mail.getSubject()).isEqualTo("Deine angeforderten Daten");
        assertThat(mail.getText()).contains(th.personRegularAnna.getFirstName());
        assertThat(mail.isHtml()).isTrue();

        //Check that zip file has been written
        assertThat(testFileStorage.findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER))
                .as("file storage should contain Datenschutzbericht.zip")
                .anyMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip"));
    }

    @Test
    public void dataPrivacyReport_Unauthorized() throws Exception {

        assertOAuth2(post("/dataprivacy/report/mail"));
    }

    @Test
    public void personDeletion() throws Exception {

        final Person person = th.personRegularAnna;
        mockMvc.perform(delete("/dataprivacy/myPersonAndAllMyData")
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyString())));

        waitForEventProcessing();

        //verify that IDataPrivacyService#deletePerson has been called for the correct person
        Mockito.verify(internalDataPrivacyServiceSpy).deleteInternalUserData(ArgumentMatchers.eq(person),
                Mockito.any());

        final String fromEmailAddress = participantsConfig.getSenderEmailAddressAccount();
        //verify that email has been sent
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(1);
        TestEmailSenderService.TestEmail mail = emailSenderServiceMock.getEmails().get(0);
        assertThat(mail.getSubject()).isEqualTo("Dein Konto wurde gelöscht");
        assertThat(mail.getText()).contains(person.getFirstName());
        assertThat(mail.isHtml()).isTrue();
    }

    @Test
    public void personDeletion_Unauthorized() throws Exception {

        assertOAuth2(delete("/dataprivacy/myPersonAndAllMyData"));
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_File_FileName_Description() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);
        MockMultipartFile expectedFile = th.createTestImageFile();

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + expectedFile.getOriginalFilename(),
                "Beschreibung",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_PersonNotFound() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                null,
                null,
                null,
                ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND,
                "We searched hard, but could not find it!");
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_HtmlFile_FileName_Description() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);
        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "<title>Page Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>This is a Heading</h1>\n" +
                "<p>This is a paragraph.</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        MockMultipartFile expectedFile = new MockMultipartFile("file", "report.html", "image/jpg",
                html.getBytes(StandardCharsets.UTF_8));

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + expectedFile.getOriginalFilename(),
                "Beschreibung",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_File_LongFileName_Description() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);
        String fileNameTrimmed = StringUtils.repeat('f', 100);
        String tooLongFilename = fileNameTrimmed + ".jpg";
        byte[] imageBytes = th.loadTestResource("testImage.jpg");
        MockMultipartFile expectedFile = new MockMultipartFile("file", tooLongFilename, "image/jpg", imageBytes);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + fileNameTrimmed + ".jpg",
                "Beschreibung",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_File_FileName_NoDescription() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);
        MockMultipartFile expectedFile = th.createTestImageFile();

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + expectedFile.getOriginalFilename(),
                null,
                ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_File_NoFileName_Description() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);

        byte[] imageBytes = th.loadTestResource("testImage.jpg");
        MockMultipartFile expectedFile = new MockMultipartFile("file", imageBytes);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + "file.jpg",
                "Beschreibung",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_File_NoFileName_NoDescription() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.OPEN,
                        DataPrivacyWorkflowAppVariantStatus.OPEN);

        byte[] imageBytes = th.loadTestResource("testImage.jpg");
        MockMultipartFile expectedFile = new MockMultipartFile("file", imageBytes);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 1 + "_" + "file.jpg",
                null,
                ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                expectedFile,
                appVariant.getName() + "_" + 2 + "_" + "file.jpg",
                "",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_NoFile_NoFileName_Description() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        sendReportResponseAndCheck(dataCollectionAndAppVariant,
                null,
                null,
                "Lange Beschreibung",
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED,
                "Sorry, we only got this lame text and no file");
    }

    @Test
    public void receiveCollectionResponseFromExternalSystem_Invalid() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant =
                createDataCollection(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);
        DataPrivacyDataCollection dataCollection = dataCollectionAndAppVariant.getLeft();
        DataPrivacyWorkflowAppVariant dataCollectionAppVariant = dataCollectionAndAppVariant.getRight();

        //invalid file extension
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(new MockMultipartFile("file", "click-me.exe", "image/jpg",
                        "Boom".getBytes(StandardCharsets.UTF_8)))
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isExceptionWithMessageContains(
                        ClientExceptionType.DATA_PRIVACY_WORKFLOW_RESPONSE_REJECTED, "extension"));

        //invalid file type (not allowed in test config)
        mockMvc.perform(multipart("/dataprivacy/report/external")
                        .file(new MockMultipartFile("file", "your-pdf.pdf", "application/pdf",
                                th.loadTestResource("example-qr-code-label.pdf")))
                        .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                        .param("dataCollectionId", dataCollection.getId())
                        .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isExceptionWithMessageContains(
                        ClientExceptionType.DATA_PRIVACY_WORKFLOW_RESPONSE_REJECTED, "media type"));

        //no file and no description but finished
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FINISHED.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //no file and no description but in progress
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //file but failed
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(th.createTestImageFile())
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FAILED.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //description but failed
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "I am not sure what happens")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.FAILED.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //file but person not found
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .file(th.createTestImageFile())
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //description but person not found
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "I am not sure what happens")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //no status
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //description too long
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", StringUtils.repeat('d', 256))
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //invalid collection id
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", "invalidId")
                .param("description", "desc")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND));

        //app variant not in collection
        AppVariant invalidAppVariant = th.app2Variant2;
        assertThat(invalidAppVariant).isNotEqualTo(dataCollectionAppVariant.getAppVariant());
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", invalidAppVariant.getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "desc")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND));

        //not authorized
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", "my own key")
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "desc")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isNotAuthorized());

        //invalid status of data collection
        dataCollection.setStatus(DataPrivacyWorkflowStatus.EXTERNALS_FINISHED);
        dataCollectionRepository.saveAndFlush(dataCollection);
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "desc")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED));

        dataCollection.setStatus(DataPrivacyWorkflowStatus.FINISHED);
        dataCollectionRepository.saveAndFlush(dataCollection);
        mockMvc.perform(multipart("/dataprivacy/report/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("description", "desc")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED));
    }

    @Test
    public void receiveDeletionResponseFromExternalSystem_Finished() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> dataDeletionAndAppVariant =
                createDataDeletion(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        sendDeletionResponseAndCheck(dataDeletionAndAppVariant,
                ClientDataPrivacyWorkflowAppVariantStatus.FINISHED,
                null);
    }

    @Test
    public void receiveDeletionResponseFromExternalSystem_Failed() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> dataDeletionAndAppVariant =
                createDataDeletion(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        sendDeletionResponseAndCheck(dataDeletionAndAppVariant,
                ClientDataPrivacyWorkflowAppVariantStatus.FAILED,
                "The person is indestructible!");
    }

    @Test
    public void receiveDeletionResponseFromExternalSystem_PersonNotFound() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> dataDeletionAndAppVariant =
                createDataDeletion(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);

        sendDeletionResponseAndCheck(dataDeletionAndAppVariant,
                ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND,
                "We assume that the person is hiding somewhere");
    }

    @Test
    public void receiveDeletionResponseFromExternalSystem_Invalid() throws Exception {

        final Person person = th.personRegularAnna;
        AppVariant appVariant = createExternalAppVariant(person);
        Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> dataDeletionAndAppVariant =
                createDataDeletion(person, appVariant, DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        DataPrivacyWorkflowAppVariantStatus.CALL_SUCCESSFUL);
        DataPrivacyDataDeletion dataDeletion = dataDeletionAndAppVariant.getLeft();
        DataPrivacyWorkflowAppVariant dataDeletionAppVariant = dataDeletionAndAppVariant.getRight();

        //no status
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", dataDeletionAppVariant.getAppVariant().getApiKey1())
                .param("dataDeletionId", dataDeletion.getId()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //invalid deletion id
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", dataDeletionAppVariant.getAppVariant().getApiKey1())
                .param("dataDeletionId", "invalidId")
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND));

        //app variant not in deletion
        AppVariant invalidAppVariant = th.app2Variant2;
        assertThat(invalidAppVariant).isNotEqualTo(dataDeletionAppVariant.getAppVariant());
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", invalidAppVariant.getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND));

        //not authorized
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", "my own key")
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isNotAuthorized());

        //invalid status of data deletion
        dataDeletion.setStatus(DataPrivacyWorkflowStatus.EXTERNALS_FINISHED);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", dataDeletionAppVariant.getAppVariant().getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED));

        dataDeletion.setStatus(DataPrivacyWorkflowStatus.FINISHED);
        dataPrivacyWorkflowRepository.saveAndFlush(dataDeletion);
        mockMvc.perform(multipart("/dataprivacy/deletion/external")
                .header("apiKey", dataDeletionAppVariant.getAppVariant().getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS.name()))
                .andExpect(isException(ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED));
    }

    private void sendReportResponseAndCheck(
            Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant,
            MockMultipartFile file,
            String expectedFileName,
            String description,
            ClientDataPrivacyWorkflowAppVariantStatus status) throws Exception {
        sendReportResponseAndCheck(dataCollectionAndAppVariant, file, expectedFileName, description, status, null);
    }

    private void sendReportResponseAndCheck(
            Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> dataCollectionAndAppVariant,
            MockMultipartFile file,
            String expectedFileName,
            String description,
            ClientDataPrivacyWorkflowAppVariantStatus status,
            String statusMessage) throws Exception {

        DataPrivacyDataCollection dataCollection = dataCollectionAndAppVariant.getLeft();
        DataPrivacyWorkflowAppVariant dataCollectionAppVariant = dataCollectionAndAppVariant.getRight();

        MockMultipartHttpServletRequestBuilder requestBuilder = multipart("/dataprivacy/report/external");
        if (file != null) {
            requestBuilder.file(file);
        }
        if (description != null) {
            requestBuilder.param("description", description);
        }
        if (statusMessage != null) {
            requestBuilder.param("statusMessage", statusMessage);
        }

        long numResponsesBefore = dataCollectionAppVariantResponseRepository.count();
        long numStatusRecordsBefore = dataPrivacyWorkflowAppVariantStatusRecordRepository.count();

        mockMvc.perform(requestBuilder
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataCollectionId", dataCollection.getId())
                .param("status", status.name()))
                .andExpect(status().isOk());

        waitForEventProcessing();

        assertThat(dataCollectionAppVariantResponseRepository.count()).isEqualTo(numResponsesBefore + 1);
        assertThat(dataPrivacyWorkflowAppVariantStatusRecordRepository.count()).isEqualTo(numStatusRecordsBefore + 1);
        DataPrivacyWorkflowAppVariantStatusRecord newStatusRecord = dataPrivacyWorkflowAppVariantStatusRecordRepository
                .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataCollectionAppVariant).get();
        assertThat(newStatusRecord.getStatus())
                .isEqualTo(DataPrivacyWorkflowAppVariantStatus.valueOf(status.name()));
        DataPrivacyDataCollectionAppVariantResponse savedResponse =
                dataPrivacyDataCollectionAppVariantResponseRepository
                        .findAllByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataCollectionAppVariant).get(0);
        if (file != null) {
            String expectedInternalFileName = dataCollection.getInternalFolderPath() + expectedFileName;
            assertThat(savedResponse.getFileName()).isEqualTo(expectedFileName);
            assertThat(testFileStorage.fileExists(expectedInternalFileName)).isTrue();
        } else {
            assertThat(savedResponse.getFileName()).isNull();
        }
        if (StringUtils.isNotBlank(description)) {
            assertThat(savedResponse.getDescription()).isEqualTo(description);
        } else {
            assertThat(savedResponse.getDescription()).isNull();
        }
    }

    private void sendDeletionResponseAndCheck(
            Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> dataDeletionAndAppVariant,
            ClientDataPrivacyWorkflowAppVariantStatus status,
            String statusMessage) throws Exception {

        DataPrivacyDataDeletion dataDeletion = dataDeletionAndAppVariant.getLeft();
        DataPrivacyWorkflowAppVariant dataCollectionAppVariant = dataDeletionAndAppVariant.getRight();

        long numResponsesBefore = dataPrivacyWorkflowAppVariantResponseRepository.count();
        long numStatusRecordsBefore = dataPrivacyWorkflowAppVariantStatusRecordRepository.count();

        MockHttpServletRequestBuilder request = post("/dataprivacy/deletion/external")
                .header("apiKey", dataCollectionAppVariant.getAppVariant().getApiKey1())
                .param("dataDeletionId", dataDeletion.getId())
                .param("status", status.name());
        if (statusMessage != null) {
            request.param("statusMessage", statusMessage);
        }
        mockMvc.perform(request)
                .andExpect(status().isOk());

        waitForEventProcessing();

        assertThat(dataPrivacyWorkflowAppVariantResponseRepository.count()).isEqualTo(numResponsesBefore + 1);
        assertThat(dataPrivacyWorkflowAppVariantStatusRecordRepository.count()).isEqualTo(numStatusRecordsBefore + 1);
        DataPrivacyWorkflowAppVariantStatusRecord newStatusRecord = dataPrivacyWorkflowAppVariantStatusRecordRepository
                .findFirstByDataPrivacyWorkflowAppVariantOrderByCreatedDesc(dataCollectionAppVariant).get();
        assertThat(newStatusRecord.getStatus())
                .isEqualTo(DataPrivacyWorkflowAppVariantStatus.valueOf(status.name()));
    }

    private Pair<DataPrivacyDataCollection, DataPrivacyWorkflowAppVariant> createDataCollection(Person person,
            AppVariant appVariant,
            DataPrivacyWorkflowStatus statusDataCollection,
            DataPrivacyWorkflowAppVariantStatus statusAppVariant) {
        DataPrivacyDataCollection dataPrivacyDataCollection =
                dataCollectionRepository.saveAndFlush(DataPrivacyDataCollection.builder()
                        .workflowTrigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                        .internalFolderPath("/internal-path/")
                        .person(person)
                        .status(statusDataCollection)
                        .build());
        DataPrivacyWorkflowAppVariant dataPrivacyDataCollectionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.saveAndFlush(DataPrivacyWorkflowAppVariant.builder()
                        .appVariant(appVariant)
                        .dataPrivacyWorkflow(dataPrivacyDataCollection)
                        .build());
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAndFlush(
                DataPrivacyWorkflowAppVariantStatusRecord.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyDataCollectionAppVariant)
                        .status(statusAppVariant)
                        .build());
        assertThat(dataPrivacyWorkflowAppVariantStatusRecordRepository.count()).isEqualTo(1);
        return Pair.of(dataPrivacyDataCollection, dataPrivacyDataCollectionAppVariant);
    }

    private Pair<DataPrivacyDataDeletion, DataPrivacyWorkflowAppVariant> createDataDeletion(Person person,
            AppVariant appVariant,
            DataPrivacyWorkflowStatus statusDataDeletion,
            DataPrivacyWorkflowAppVariantStatus statusAppVariant) {
        DataPrivacyDataDeletion dataPrivacyDataDeletion =
                dataPrivacyWorkflowRepository.saveAndFlush(
                        DataPrivacyDataDeletion.builder()
                                .workflowTrigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                                .person(person)
                                .status(statusDataDeletion)
                                .build());
        DataPrivacyWorkflowAppVariant dataPrivacyDataDeletionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.saveAndFlush(DataPrivacyWorkflowAppVariant.builder()
                        .appVariant(appVariant)
                        .dataPrivacyWorkflow(dataPrivacyDataDeletion)
                        .build());
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAndFlush(
                DataPrivacyWorkflowAppVariantStatusRecord.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyDataDeletionAppVariant)
                        .status(statusAppVariant)
                        .build());
        assertThat(dataPrivacyWorkflowAppVariantStatusRecordRepository.count()).isEqualTo(1);
        return Pair.of(dataPrivacyDataDeletion, dataPrivacyDataDeletionAppVariant);
    }

    private AppVariant createExternalAppVariant(Person person) {
        AppVariant externalAppVariant = th.app1Variant1;
        externalAppVariant.setApiKey1("secure");
        externalAppVariant.setExternalApiKey("super-secure");
        externalAppVariant.setCallBackUrl("https://iese.de");
        th.appVariantRepository.saveAndFlush(externalAppVariant);

        assertThat(externalAppVariant.hasExternalSystem()).isTrue();
        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(externalAppVariant)
                .person(person)
                .lastUsage(12021983)
                .build());
        return externalAppVariant;
    }

}
