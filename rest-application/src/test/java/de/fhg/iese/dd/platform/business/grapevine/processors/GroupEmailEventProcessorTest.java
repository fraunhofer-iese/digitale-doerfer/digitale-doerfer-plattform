/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GroupEmailEventProcessorTest extends BaseServiceTest {

    //set to true if you want to inspect the actual email html. Then watch test case log for the output file name
    private static final boolean DUMP_MAILS_TO_TMP = false;

    @Autowired
    private GroupEmailEventProcessor groupEmailEventProcessor;

    @Autowired
    private TestEmailSenderService emailSenderService;

    @Autowired
    private IAuthorizationService authorizationService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private GrapevineConfig grapevineConfig;

    @Autowired
    private GroupTestHelper th;

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_MAILS_TO_TMP) {
            deleteTempFilesStartingWithClassName(GroupEmailEventProcessorTest.class);
        }
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createGroupsAndGroupPostsWithComments();

        emailSenderService.clearMailList();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @AfterEach
    public void dumpEmailsToTemp(TestInfo testInfo) throws Exception {
        if (DUMP_MAILS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-" + testInfo.getDisplayName() + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile);
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Test
    public void newGroupJoinRequestMail() {

        //time needs to be constant, so that the auth tokens get the same expiration time
        timeServiceMock.setConstantDateTime(12021983);

        final GroupMembership groupMembership = GroupMembership.builder()
                .group(th.groupSchachvereinAAA)
                .member(th.personAnjaTenant1Dorf1)
                .memberIntroductionText("Schachmatt in 10 Zügen")
                .status(GroupMembershipStatus.PENDING)
                .build();

        groupEmailEventProcessor.handleGroupJoinConfirmation(GroupJoinConfirmation.builder()
                .membership(groupMembership)
                .build());

        checkNumberOfSentEmailsToGroupMembershipAdmins(groupMembership.getGroup(), 1);

        Person groupMembershipAdmin = th.personBenTenant1Dorf2;
        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressDorffunk();
        checkBasicEmailBody(getMailTextForRecipient(fromEmailAddress, groupMembershipAdmin),
                groupMembership,
                groupMembershipAdmin);

        assertThatMailListContainsRecipient(groupMembershipAdmin.getEmail());
    }

    @Test
    public void newGroupJoinRequestMail_EmptyText() {

        //time needs to be constant, so that the auth tokens get the same expiration time
        timeServiceMock.setConstantDateTime(12021983);

        final GroupMembership groupMembership = GroupMembership.builder()
                .group(th.groupSchachvereinAAA)
                .member(th.personAnjaTenant1Dorf1)
                .memberIntroductionText("")
                .status(GroupMembershipStatus.PENDING)
                .build();

        groupEmailEventProcessor.handleGroupJoinConfirmation(GroupJoinConfirmation.builder()
                .membership(groupMembership)
                .build());

        checkNumberOfSentEmailsToGroupMembershipAdmins(groupMembership.getGroup(), 1);

        Person groupMembershipAdmin = th.personBenTenant1Dorf2;
        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressDorffunk();
        checkBasicEmailBody(getMailTextForRecipient(fromEmailAddress, groupMembershipAdmin),
                groupMembership,
                groupMembershipAdmin);

        assertThatMailListContainsRecipient(groupMembershipAdmin.getEmail());
    }

    @Test
    public void newGroupJoinRequestMail_NullText() {

        //time needs to be constant, so that the auth tokens get the same expiration time
        timeServiceMock.setConstantDateTime(12021983);

        final GroupMembership groupMembership = GroupMembership.builder()
                .group(th.groupSchachvereinAAA)
                .member(th.personAnjaTenant1Dorf1)
                .memberIntroductionText(null)
                .status(GroupMembershipStatus.PENDING)
                .build();

        groupEmailEventProcessor.handleGroupJoinConfirmation(GroupJoinConfirmation.builder()
                .membership(groupMembership)
                .build());

        checkNumberOfSentEmailsToGroupMembershipAdmins(groupMembership.getGroup(), 1);

        Person groupMembershipAdmin = th.personBenTenant1Dorf2;
        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressDorffunk();
        checkBasicEmailBody(getMailTextForRecipient(fromEmailAddress, groupMembershipAdmin),
                groupMembership,
                groupMembershipAdmin);

        assertThatMailListContainsRecipient(groupMembershipAdmin.getEmail());
    }

    @Test
    public void newGroupJoinRequestMailWithoutGroupMembershipAdmin(){

        final GroupMembership groupMembership = GroupMembership.builder()
                .group(th.groupWaffenfreundeAMA)
                .member(th.personBenTenant1Dorf2)
                .memberIntroductionText("Kann mich jemand annehmen?")
                .status(GroupMembershipStatus.PENDING)
                .build();

        groupEmailEventProcessor.handleGroupJoinConfirmation(GroupJoinConfirmation.builder()
                .membership(groupMembership)
                .build());

        checkNumberOfSentEmailsToGroupMembershipAdmins(groupMembership.getGroup(), 0);
    }

    private String getMailTextForRecipient(String fromEmailAddress, Person groupMembershipAdmin) {
        return emailSenderService.getMailsByFromAndToRecipient(fromEmailAddress,
                groupMembershipAdmin.getEmail()).stream().findAny().get().getText();
    }

    private void assertThatMailListContainsRecipient(String recipient) {
        assertTrue(emailSenderService
                .getEmails()
                .stream()
                .anyMatch(email -> email.getRecipientEmailAddress().equals(recipient)));
    }

    private void checkNumberOfSentEmailsToGroupMembershipAdmins(Group group, long count) {
        assertEquals(count, roleService.getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class,
                group).size());
        assertEquals(count, emailSenderService.getEmails().size());
    }

    private void checkBasicEmailBody(String emailBody, GroupMembership groupMembership, Person recipient) {

        Person member = groupMembership.getMember();
        Group group = groupMembership.getGroup();

        assertTrue(emailBody.contains(recipient.getFullName()));
        assertTrue(emailBody.contains(member.getFirstNameFirstCharLastDot()));
        assertTrue(emailBody.contains(group.getName()));
        assertTrue(emailBody.contains((StringUtils.isEmpty(groupMembership.getMemberIntroductionText())) ?
                "- Kein Vorstellungstext -" : groupMembership.getMemberIntroductionText()));

        assertThat(emailBody).contains(UriComponentsBuilder
                .fromHttpUrl(grapevineConfig.getDorffunkBaseUrl())
                .queryParam("type", "accept")
                .queryParam("group", group.getId())
                .queryParam("admin", recipient.getId())
                .queryParam("person", member.getId())
                .queryParam("token",
                        authorizationService.generateAuthToken(GroupPendingJoinAcceptRequest.builder()
                                        .group(group)
                                        .personToAccept(member)
                                        .acceptingPerson(recipient)
                                        .build(),
                                Duration.ofDays(120)))
                .build().toUriString());

        assertThat(emailBody).contains(UriComponentsBuilder
                .fromHttpUrl(grapevineConfig.getDorffunkBaseUrl())
                .queryParam("type", "deny")
                .queryParam("group", group.getId())
                .queryParam("admin", recipient.getId())
                .queryParam("person", member.getId())
                .queryParam("token",
                        authorizationService.generateAuthToken(GroupPendingJoinDenyRequest.builder()
                                        .group(group)
                                        .personToDeny(member)
                                        .denyingPerson(recipient)
                                        .build(),
                                Duration.ofDays(120)))
                .build().toUriString());
    }

}
