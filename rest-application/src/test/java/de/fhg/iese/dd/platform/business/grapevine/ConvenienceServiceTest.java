/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.services.IConvenienceService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;

public class ConvenienceServiceTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private IConvenienceService convenienceService;
    @Autowired
    private ConvenienceRepository convenienceRepository;
    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() {

        th.deleteAllData();
    }

    @Test
    public void saveConvenience() {

        final GeoArea geoArea1 = th.geoAreaDorf1InEisenberg;
        final GeoArea geoArea2 = th.geoAreaDorf2InEisenberg;
        final List<GeoArea> geoAreas = Arrays.asList(geoArea1, geoArea2);
        final MediaItem image1 = th.createMediaItem("Blut_Detail1");
        final MediaItem image2 = th.createMediaItem("Blut_Detail2");
        Convenience convenience = convenienceService.saveConvenience(Convenience.builder()
                        .name("Blutspende")
                        .description("Blut am Morgen vertreibt Kummer und Sorgen")
                        .organizationName("Vampir AG")
                        .organizationLogo(th.createMediaItem("Vampir_Logo"))
                        .url("https://www.blutspendedienst.com/suche?term={homeArea.zipCodes} {homeArea.name}&radius=20")
                        .overviewImage(th.createMediaItem("Blut_Overview"))
                        .detailImages(Arrays.asList(image1, image2))
                        .build(),
                geoAreas);

        assertThat(convenienceRepository.findById(convenience.getId()).get().getDetailImages())
                .containsExactly(image1, image2);
        final List<ConvenienceGeoAreaMapping> mappings = convenienceGeoAreaMappingRepository.findAll();
        assertThat(mappings).hasSize(2);
        assertThat(mappings).extracting(ConvenienceGeoAreaMapping::getGeoArea).containsExactlyInAnyOrder(geoArea1,
                geoArea2);

        //changing order fails if the wrong hibernate annotation is used
        Collections.reverse(convenience.getDetailImages());
        convenience = convenienceService.saveConvenience(convenience, geoAreas);

        Collections.reverse(convenience.getDetailImages());
        convenience = convenienceService.saveConvenience(convenience, geoAreas);

        //deleting images needs to be supported by deleting the orphans
        convenience.getDetailImages().remove(image2);
        convenience = convenienceService.saveConvenience(convenience, geoAreas);
        assertThat(convenienceRepository.findById(convenience.getId()).get().getDetailImages())
                .containsExactly(image1);
        assertThat(th.mediaItemRepository.existsById(image2.getId())).isFalse();

        //adding new images
        final MediaItem image3 = th.createMediaItem("Blut_neu");
        convenience.getDetailImages().add(image3);
        convenienceService.saveConvenience(convenience, geoAreas);
        assertThat(convenienceRepository.findById(convenience.getId()).get().getDetailImages())
                .containsExactly(image1, image3);
    }

}
