/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.feature.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureServiceTest;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature2;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeatureInvisibleForClients;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

@Tag("feature")
public class FeatureAdminControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        saveFeatureConfigs();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    private void saveFeatureConfigs() {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .configValues(FeatureServiceTest.COMMON_VALUES)
                .build());
        //Feature2
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature2.class.getName())
                .enabled(true)
                .configValues(FeatureServiceTest.COMMON_VALUES2)
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeatureInvisibleForClients.class.getName())
                .enabled(true)
                .build());
    }

    @Test
    public void adminEndpointsUnauthorized() throws Exception {

        assertRequiresAdmin(get("/administration/feature/metamodel"));
        assertRequiresAdmin(get("/administration/feature/target"));
    }

    @Test
    public void getAllFeatureMetaModels() throws Exception {

        mockMvc.perform(get("/administration/feature/metamodel")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.featureName=='de.fhg.iese.dd.platform.test.TestFeature1')]").exists())
                .andExpect(jsonPath("$[?(@.featureName=='de.fhg.iese.dd.platform.test.TestFeature2')]").exists())
                .andExpect(jsonPath(
                        "$[?(@.featureName=='de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature3')]").exists());
    }

    @Test
    public void getAllFeatures() throws Exception {

        mockMvc.perform(get("/administration/feature/target")
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3))) //actual content is tested at other places
                .andExpect(jsonPath("$[0].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature1")))
                .andExpect(jsonPath("$[0].visibleForClient").value(true))
                .andExpect(jsonPath("$[1].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature2")))
                .andExpect(jsonPath("$[1].visibleForClient").value(true))
                .andExpect(jsonPath("$[2].featureIdentifier",
                        is("de.fhg.iese.dd.platform.test.TestFeatureInvisibleForClients")))
                .andExpect(jsonPath("$[2].visibleForClient").value(false));

        mockMvc.perform(get("/administration/feature/target")
                        .param("tenantId", th.tenant1.getId())
                        .param("appIdentifier", th.app1.getAppIdentifier())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3))) //actual content is tested at other places
                .andExpect(jsonPath("$[0].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature1")))
                .andExpect(jsonPath("$[1].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature2")))
                .andExpect(jsonPath("$[2].featureIdentifier",
                        is("de.fhg.iese.dd.platform.test.TestFeatureInvisibleForClients")));

        mockMvc.perform(get("/administration/feature/target")
                        .param("tenantId", th.tenant1.getId())
                        .param("appVariantIdentifier", th.app1Variant1.getAppVariantIdentifier())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3))) //actual content is tested at other places
                .andExpect(jsonPath("$[0].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature1")))
                .andExpect(jsonPath("$[1].featureIdentifier", is("de.fhg.iese.dd.platform.test.TestFeature2")))
                .andExpect(jsonPath("$[2].featureIdentifier",
                        is("de.fhg.iese.dd.platform.test.TestFeatureInvisibleForClients")));
    }

    @Test
    public void getAllFeatures_Invalid() throws Exception {

        mockMvc.perform(get("/administration/feature/target")
                        .param("tenantId", th.tenant1.getId())
                        .param("appIdentifier", th.app1.getAppIdentifier())
                        .param("appVariantIdentifier", th.app1Variant1.getAppVariantIdentifier())
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    private void assertRequiresAdmin(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertOAuth2(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
