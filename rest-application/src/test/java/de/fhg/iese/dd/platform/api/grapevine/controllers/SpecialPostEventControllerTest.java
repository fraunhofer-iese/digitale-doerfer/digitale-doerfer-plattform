/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.specialpost.ClientSpecialPostChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.specialpost.ClientSpecialPostCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SpecialPost;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SpecialPostEventControllerTest extends BasePostEventControllerTest<SpecialPost> {

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return new ClientSpecialPostChangeRequest();
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personLaraSchaefer;
    }

    @Override
    protected SpecialPost getTestPost1() {
        return th.specialPost1withImages;
    }

    @Override
    protected SpecialPost getTestPost2() {
        return th.specialPost4withImages;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.offer1;
    }

    private AppVariant appVariantContentCreationRestricted;
    private final EnumSet<PersonVerificationStatus> allowedVerificationStatuses =
            EnumSet.of(PersonVerificationStatus.PHONE_NUMBER_VERIFIED, PersonVerificationStatus.EMAIL_VERIFIED);

    private void createFeatureContentCreationRestriction() {

        appVariantContentCreationRestricted = th.appVariant1Tenant1;
        final String allowedStatusArray = allowedVerificationStatuses.stream()
                .map(PersonVerificationStatus::name)
                .collect(Collectors.joining("\",\"", "[\"", "\"]"));

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantContentCreationRestricted)
                .enabled(true)
                .featureClass(ContentCreationRestrictionFeature.class.getName())
                .configValues(
                        "{\"allowedStatusesAnyOf\": " + allowedStatusArray + " }")
                .build());
    }

    @Test
    public void specialPostCreateRequest_VerifyingPushMessage() throws Exception {

        final Person postCreator = th.personThomasBecker;

        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());

        final String text = "help text";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = Map.of("type", "request", "shoesize", 47.5, "false", true);

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .createdLocation(createdLocation)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        mockMvc.perform(get("/grapevine/post/{postId}", specialPostCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.specialPost.id").value(
                        specialPostCreateConfirmation.getPost().getSpecialPost().getId()))
                .andExpect(jsonPath("$.specialPost.id").value(specialPostCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.specialPost.commentsAllowed").value(true))
                .andExpect(jsonPath("$.specialPost.text").value(text))
                .andExpect(jsonPath("$.specialPost.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.specialPost.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.specialPost.customAttributes").value(expectedCustomAttributes))
                .andExpect(jsonPath("$.specialPost.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        assertEquals(postCreator, addedSpecialPost.getCreator());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(createdLocation.getLatitude(), addedSpecialPost.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSpecialPost.getCreatedLocation().getLongitude());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(postCreator.getTenant(), addedSpecialPost.getTenant());
        assertThat(addedSpecialPost.getImages()).isNullOrEmpty();
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertFalse(addedSpecialPost.isHiddenForOtherHomeAreas());
        assertTrue(addedSpecialPost.isCommentsAllowed());

        // verify push message
        waitForEventProcessing();

        final ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedSpecialPost.getGeoAreas(), false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED);

        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPost().getSpecialPost().getId());
        // check that the last name is cut
        assertEquals(postCreator.getFirstName(), pushedEvent.getPost().getSpecialPost().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(postCreator.getLastName()),
                pushedEvent.getPost().getSpecialPost().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void specialPostCreateRequest_ContentCreationRestricted() throws Exception {

        // this is not tested using assertIsPersonVerificationStatusRestricted in order to test it in more detail

        createFeatureContentCreationRestriction();

        Person postCreator = th.personThomasBecker;

        assertTrue(postCreator.getVerificationStatuses().getValues().isEmpty(), "Post creator needs to be unverified");

        final String text = "Salat! 🥗";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = new HashMap<>();
        customAttributes.put("vegan", "true");
        customAttributes.put("color", "green");

        final ClientSpecialPostCreateRequest specialPostCreateRequest = ClientSpecialPostCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .customAttributes(customAttributes)
                .build();

        // no verification at all
        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        // set the verification status to one that is not in the list of allowed statuses
        postCreator.getVerificationStatuses().addValue(
                EnumSet.complementOf(allowedVerificationStatuses).iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        // one of the allowed verifications
        postCreator.getVerificationStatuses().addValue(allowedVerificationStatuses.iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // get the special post
        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        mockMvc.perform(get("/grapevine/post/{postId}", specialPostCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(
                        jsonPath("$.specialPost.id").value(
                                specialPostCreateConfirmation.getPost().getSpecialPost().getId()))
                .andExpect(jsonPath("$.specialPost.id").value(specialPostCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.specialPost.text").value(text))
                .andExpect(jsonPath("$.specialPost.customAttributes").value(expectedCustomAttributes))
                .andExpect(jsonPath("$.specialPost.creator.id").value(postCreator.getId()));
    }

    @Test
    public void specialPostCreateRequest_TooBigPushMessage() throws Exception {

        final Person postCreator = th.personThomasBecker;

        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());

        final String text = "gothic text 😈";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = Collections.singletonMap("type", "request");

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i < th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(th.createTemporaryMediaItemWithMaxUrls(postCreator, 0).getId());
        }

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .createdLocation(createdLocation)
                                .temporaryMediaItemIds(temporaryIds)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        assertNotNull(addedSpecialPost);

        // verify push message
        waitForEventProcessing();

        // this event can only be pushed if it was minimized
        final ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedSpecialPost.getGeoAreas(), false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED);

        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPost().getSpecialPost().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void specialPostCreateRequest_WithImagesAndDocumentsVerifyingPushMessage() throws Exception {

        final Person postCreator = th.personSusanneChristmann;

        assertNotNull(postCreator.getHomeArea());

        final String text = "Need help";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = Collections.singletonMap("type", "request");

        // create temp images
        final TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        // create request using subset of temp images
        final ClientSpecialPostCreateRequest specialPostCreateRequest = ClientSpecialPostCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .customAttributes(customAttributes)
                .build();

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(specialPostCreateConfirmation.getPost().getSpecialPost().getText()).isEqualTo(text);
        assertThat(specialPostCreateConfirmation.getPost().getSpecialPost().getImages()).hasSize(3);
        assertThat(specialPostCreateConfirmation.getPost().getSpecialPost().getAttachments()).hasSize(3);

        // get the special post
        // check new entity uses images
        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        mockMvc.perform(get("/grapevine/post/{postId}", specialPostCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.specialPost.id").value(specialPostCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.specialPost.text").value(text))
                .andExpect(jsonPath("$.specialPost.customAttributes").value(expectedCustomAttributes))
                .andExpect(jsonPath("$.specialPost.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.specialPost.images", hasSize(3)))
                .andExpect(jsonPath("$.specialPost.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.specialPost.images[1].id").value(
                        tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.specialPost.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.specialPost.attachments", hasSize(3)))
                .andExpect(jsonPath("$.specialPost.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.specialPost.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.specialPost.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.specialPost.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.specialPost.hiddenForOtherHomeAreas").value(true));

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        assertEquals(postCreator, addedSpecialPost.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSpecialPost.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSpecialPost.getCreatedLocation().getLongitude());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(postCreator.getTenant(), addedSpecialPost.getTenant());
        assertEquals(3, addedSpecialPost.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedSpecialPost.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedSpecialPost.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedSpecialPost.getImages().get(2));
        assertThat(addedSpecialPost.getAttachments()).hasSize(3);
        assertThat(addedSpecialPost.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedSpecialPost.isHiddenForOtherHomeAreas());

        // check used temp images are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        // check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));

        // verify push message
        waitForEventProcessing();

        final ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedSpecialPost.getGeoAreas(), true,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED);

        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPost().getSpecialPost().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void specialPostCreateRequest_WithMultipleCustomAttributesVerifyingPushMessage() throws Exception {

        final Person postCreator = th.personSusanneChristmann;

        assertNotNull(postCreator.getHomeArea());

        final String text = "Need help";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = new HashMap<>();
        customAttributes.put("type", "offer");
        customAttributes.put("price", 0.99);

        // create request using subset of temp images
        final ClientSpecialPostCreateRequest specialPostCreateRequest = ClientSpecialPostCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .customAttributes(customAttributes)
                .build();

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // get the special post
        // check new entity uses images
        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        mockMvc.perform(get("/grapevine/post/{postId}", specialPostCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.specialPost.id").value(specialPostCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.specialPost.text").value(text))
                .andExpect(jsonPath("$.specialPost.customAttributes").value(expectedCustomAttributes))
                .andExpect(jsonPath("$.specialPost.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.specialPost.images", hasSize(0)))
                .andExpect(jsonPath("$.specialPost.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.specialPost.hiddenForOtherHomeAreas").value(true));

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        assertEquals(postCreator, addedSpecialPost.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSpecialPost.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSpecialPost.getCreatedLocation().getLongitude());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(postCreator.getTenant(), addedSpecialPost.getTenant());
        assertEquals(0, addedSpecialPost.getImages().size());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedSpecialPost.isHiddenForOtherHomeAreas());

        // verify push message
        waitForEventProcessing();

        final ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedSpecialPost.getGeoAreas(), true,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED);

        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(specialPostCreateConfirmation.getPostId(), pushedEvent.getPost().getSpecialPost().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void specialPostCreateRequest_InvalidImages() throws Exception {

        final Person postCreator = th.personSusanneChristmann;
        final String text = "Need help";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        // create temp images
        final TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final String invalidTempMediaItemId = "invalid id";

        // create request using temp images and invalid id
        final ClientSpecialPostCreateRequest specialPostCreateRequest = ClientSpecialPostCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .build();

        // check temp image not found exception
        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        // check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void specialPostCreateRequest_DuplicateImages() throws Exception {

        final Person postCreator = th.personSusanneChristmann;
        final String text = "Need help";
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        // create temp images
        final TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        final TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        // create request using temp images and invalid id
        final ClientSpecialPostCreateRequest specialPostCreateRequest = ClientSpecialPostCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage2.getId()))
                .build();

        // check temp image not found exception
        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(specialPostCreateRequest)))
                .andExpect(isException("[" + tempImage2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        // check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void specialPostCreateRequest_OptionalAttributes() throws Exception {

        final Person person = th.personSusanneChristmann;

        assertNotNull(person.getHomeArea());

        final String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed chars!
        final ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        final Map<String, Object> customAttributes = new HashMap<>();
        customAttributes.put("name", "shark");
        customAttributes.put("count", "5");

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .createdLocation(createdLocation)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // get the special post
        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        mockMvc.perform(get("/grapevine/post/{postId}" , specialPostCreateConfirmation.getPost().getId())
                        .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.specialPost.id").value(specialPostCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.specialPost.text").value(text))
                .andExpect(jsonPath("$.specialPost.customAttributes").value(expectedCustomAttributes))
                .andExpect(jsonPath("$.specialPost.creator.id").value(person.getId()))
                .andExpect(jsonPath("$.specialPost.createdLocation").doesNotExist());

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        assertEquals(person, addedSpecialPost.getCreator());
        assertEquals(createdLocation.getLatitude(), addedSpecialPost.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedSpecialPost.getCreatedLocation().getLongitude());
        assertNull(addedSpecialPost.getCustomAddress());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(person.getTenant(), addedSpecialPost.getTenant());
        assertTrue(addedSpecialPost.getImages() == null || addedSpecialPost.getImages().isEmpty());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void specialPostCreateRequest_NoCreatedLocation() throws Exception {

        final Person person = th.personSusanneChristmann;

        assertNotNull(person.getHomeArea());

        final String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed chars!
        final Map<String, Object> customAttributes = Collections.singletonMap("type", "offer");

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);

        assertEquals(person, addedSpecialPost.getCreator());
        assertEquals(person.getHomeArea().getCenter(), addedSpecialPost.getCreatedLocation());
        assertNull(addedSpecialPost.getCustomAddress());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(person.getTenant(), addedSpecialPost.getTenant());
        assertTrue(addedSpecialPost.getImages() == null || addedSpecialPost.getImages().isEmpty());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void specialPostCreateRequest_ZeroZeroCreatedLocation() throws Exception {

        final Person person = th.personSusanneChristmann;

        assertNotNull(person.getHomeArea());

        final String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed chars!
        final Map<String, Object> customAttributes = Collections.singletonMap("type", "offer");

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .createdLocation(new ClientGPSLocation(0.0, 0.0))
                                .text(text)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);

        assertEquals(person, addedSpecialPost.getCreator());
        assertEquals(person.getHomeArea().getCenter(), addedSpecialPost.getCreatedLocation());
        assertNull(addedSpecialPost.getCustomAddress());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(person.getTenant(), addedSpecialPost.getTenant());
        assertTrue(addedSpecialPost.getImages() == null || addedSpecialPost.getImages().isEmpty());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void specialPostCreateRequest_NoCustomAttributes() throws Exception {

        final Person person = th.personSusanneChristmann;

        assertNotNull(person.getHomeArea());

        final String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost); // max allowed chars!

        final MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .createdLocation(new ClientGPSLocation(0.0, 0.0))
                                .text(text)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientPostCreateConfirmation specialPostCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // check whether the special post is also added to the repository
        final SpecialPost addedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostCreateConfirmation.getPost().getId()).get();

        Map<String, Object> expectedCustomAttributes =
                Collections.singletonMap(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);

        assertEquals(person, addedSpecialPost.getCreator());
        assertEquals(person.getHomeArea().getCenter(), addedSpecialPost.getCreatedLocation());
        assertNull(addedSpecialPost.getCustomAddress());
        assertEquals(text, addedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, addedSpecialPost.getCustomAttributes());
        assertEquals(person.getTenant(), addedSpecialPost.getTenant());
        assertTrue(addedSpecialPost.getImages() == null || addedSpecialPost.getImages().isEmpty());
        assertThat(addedSpecialPost.getGeoAreas()).containsOnly(person.getHomeArea());
    }

    @Test
    public void specialPostCreateRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/specialPostCreateRequest")
                .contentType(contentType)
                .content(json(ClientSpecialPostCreateRequest.builder()
                        .text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr")
                        .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                        .build())), th.appVariant4AllTenants, th.personSusanneChristmann);
    }

    @Test
    public void specialPostCreateRequest_ExceedingMaxTextLength() throws Exception {

        final Person person = th.personThomasBecker;
        final String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);

        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                                .customAttributes(Collections.singletonMap("reason", "test"))
                                .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void specialPostCreateRequest_ExceedingMaxNumberImages() throws Exception {

        final Person person = th.personThomasBecker;
        final String text = "special post with too many images";

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                                .temporaryMediaItemIds(temporaryIds)
                                .customAttributes(Collections.singletonMap("reason", "test"))
                                .build())))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void specialPostCreateRequest_MissingAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        final String text = "special post with missing attributes";

        // missing text
        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                                .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing home area and no tenant
        person.setHomeArea(null);
        person = th.personRepository.save(person);

        mockMvc.perform(post("/grapevine/event/specialPostCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostCreateRequest.builder()
                                .text(text)
                                .build())))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));
    }

    /* ***** CHANGE OPERATIONS *******/

    @Test
    public void specialPostChangeRequest_TextVerifyingPushMessage() throws Exception {

        long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);
        final SpecialPost specialPostToBeChanged = th.specialPost3;
        String postId = specialPostToBeChanged.getId();
        final Person caller = th.personSusanneChristmann;
        final String text = "Jetzt soll hier ein anderer Text stehen";
        final Map<String, Object> customAttributes = Collections.singletonMap("price", 2.95);
        final Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);

        specialPostToBeChanged.setCustomAttributes(expectedCustomAttributes);
        specialPostToBeChanged.setText(text);
        specialPostToBeChanged.setLastModified(now);

        final ClientSpecialPostChangeRequest specialPostChangeRequest = ClientSpecialPostChangeRequest.builder()
                .postId(postId)
                .text(text)
                .customAttributes(customAttributes)
                .build();

        final ClientPostChangeConfirmation specialPostChangeConfirmation =
                toObject(mockMvc.perform(post("/grapevine/event/specialPostChangeRequest")
                                .headers(authHeadersFor(caller, th.appVariant1Tenant1))
                                .contentType(contentType)
                                .content(json(specialPostChangeRequest)))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.postId").value(specialPostChangeRequest.getPostId()))
                        .andExpect(assertPostAttributes(caller, ".post", specialPostToBeChanged))
                        .andReturn(), ClientPostChangeConfirmation.class);

        // check whether the special post is also updated in the repository
        final SpecialPost updatedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostChangeConfirmation.getPost().getId()).get();
        assertThat(updatedSpecialPost.getText()).isEqualTo(text);
        assertThat(updatedSpecialPost.getCustomAttributes()).isEqualTo(expectedCustomAttributes);

        // verify push message
        waitForEventProcessing();

        final ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(specialPostToBeChanged.getGeoAreas(),
                        specialPostToBeChanged.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(postId, pushedEvent.getPostId());
        assertEquals(postId, pushedEvent.getPost().getSpecialPost().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void specialPostChangeRequest_Images() throws Exception {

        long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);
        final SpecialPost specialPostToBeChanged = th.specialPost4withImages;
        final Person caller = specialPostToBeChanged.getCreator();
        final List<MediaItem> existingImages = specialPostToBeChanged.getImages();

        // create additional temp image
        final TemporaryMediaItem newTempMediaItem =
                th.createTemporaryMediaItem(caller, th.nextTimeStamp(), th.nextTimeStamp());

        final ClientSpecialPostChangeRequest specialPostChangeRequest = ClientSpecialPostChangeRequest.builder()
                .postId(specialPostToBeChanged.getId())
                .mediaItemPlaceHolders(Collections.singletonList(
                        ClientMediaItemPlaceHolder.builder()
                                .temporaryMediaItemId(newTempMediaItem.getId())
                                .build()))
                .build();
        Map<String, Object> expectedCustomAttributes =
                Collections.singletonMap(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        specialPostToBeChanged.setImages(Collections.singletonList(newTempMediaItem.getMediaItem()));
        specialPostToBeChanged.setLastModified(now);
        specialPostToBeChanged.setCustomAttributes(expectedCustomAttributes);

        mockMvc.perform(post("/grapevine/event/specialPostChangeRequest")
                        .headers(authHeadersFor(caller, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(specialPostChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(specialPostChangeRequest.getPostId()))
                .andExpect(assertPostAttributes(caller, ".post", specialPostToBeChanged));

        // check whether the special post is also updated in the repository
        final SpecialPost updatedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostToBeChanged.getId()).get();
        assertThat(updatedSpecialPost.getImages()).contains(newTempMediaItem.getMediaItem());

        // check if the old images are deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContainAnyElementsOf(existingImages);
    }

    @Test
    public void specialPostChangeRequest_WithMultipleCustomAttributes() throws Exception {

        long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);
        SpecialPost specialPostToBeChanged = th.specialPost4withImages;
        Person caller = specialPostToBeChanged.getCreator();
        Map<String, Object> customAttributes = new HashMap<>();
        customAttributes.put("type", "request");
        customAttributes.put("price", 1.99);

        Map<String, Object> expectedCustomAttributes = new HashMap<>(customAttributes);
        expectedCustomAttributes.put(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        specialPostToBeChanged.setCustomAttributes(expectedCustomAttributes);
        specialPostToBeChanged.setLastModified(now);

        ClientSpecialPostChangeRequest specialPostChangeRequest = ClientSpecialPostChangeRequest.builder()
                .postId(specialPostToBeChanged.getId())
                .customAttributes(customAttributes)
                .build();

        mockMvc.perform(post("/grapevine/event/specialPostChangeRequest")
                        .headers(authHeadersFor(caller, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(specialPostChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(specialPostChangeRequest.getPostId()))
                .andExpect(assertPostAttributes(caller, ".post", specialPostToBeChanged));

        // check whether the special post is also updated in the repository
        final SpecialPost updatedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostToBeChanged.getId()).get();
        assertThat(updatedSpecialPost.getCustomAttributes()).isEqualTo(expectedCustomAttributes);
    }

    @Test
    public void specialPostChangeRequest_ChangeTextWithEmptyCustomAttributes() throws Exception {

        long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);
        final SpecialPost specialPostToBeChanged = th.specialPost4withImages;
        final Person caller = specialPostToBeChanged.getCreator();
        final String text = "Jetzt soll hier ein anderer Text stehen";

        final ClientSpecialPostChangeRequest specialPostChangeRequest = ClientSpecialPostChangeRequest.builder()
                .postId(specialPostToBeChanged.getId())
                .text(text)
                .build();
        Map<String, Object> expectedCustomAttributes =
                Collections.singletonMap(th.specialPostTypeFeatureKey, th.specialPostTypeFeatureValue);
        specialPostToBeChanged.setCustomAttributes(expectedCustomAttributes);
        specialPostToBeChanged.setText(text);
        specialPostToBeChanged.setLastModified(now);

        mockMvc.perform(post("/grapevine/event/specialPostChangeRequest")
                        .headers(authHeadersFor(caller, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(specialPostChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(specialPostChangeRequest.getPostId()))
                .andExpect(assertPostAttributes(caller, ".post", specialPostToBeChanged));

        // check whether the special post is also updated in the repository
        final SpecialPost updatedSpecialPost =
                (SpecialPost) th.postRepository.findById(specialPostToBeChanged.getId()).get();
        assertEquals(text, updatedSpecialPost.getText());
        assertEquals(expectedCustomAttributes, updatedSpecialPost.getCustomAttributes());
    }

    @Test
    public void specialPostChangeRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/specialPostChangeRequest")
                .contentType(contentType)
                .content(json(ClientSpecialPostChangeRequest.builder()
                        .postId(th.specialPost1withImages.getId())
                        .text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr")
                        .build())), th.appVariant4AllTenants, th.personSusanneChristmann);
    }

    @Test
    public void specialPostChangeRequest_NotOwner() throws Exception {

        final SpecialPost specialPostToBeChanged = th.specialPost4withImages;
        final Person changingPerson = th.personLaraSchaefer;

        // make sure that the changing person is not the owner
        assertNotSame(specialPostToBeChanged.getCreator(), changingPerson);

        final List<MediaItem> imagesToBeChanged = specialPostToBeChanged.getImages();

        // make sure there are images to change
        assertEquals(3, imagesToBeChanged.size());
        imagesToBeChanged.remove(0);
        final MediaItem newImage = th.createMediaItem("newImageForSpecialPost4");
        imagesToBeChanged.add(newImage);

        final String text = "Jetzt soll hier ein anderer Text stehen";
        final Map<String, Object> customAttributes = Collections.singletonMap("type", "request");

        mockMvc.perform(post("/grapevine/event/specialPostChangeRequest")
                        .headers(authHeadersFor(changingPerson, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(ClientSpecialPostChangeRequest.builder()
                                .postId(specialPostToBeChanged.getId())
                                .text(text)
                                .customAttributes(customAttributes)
                                .build())))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));

        // check whether the special post is not updated in the repository
        final SpecialPost specialPostFromRepository =
                (SpecialPost) th.postRepository.findById(specialPostToBeChanged.getId()).get();
        assertNotEquals(text, specialPostFromRepository.getText());
        assertNotEquals(imagesToBeChanged, specialPostFromRepository.getImages());
    }

}
