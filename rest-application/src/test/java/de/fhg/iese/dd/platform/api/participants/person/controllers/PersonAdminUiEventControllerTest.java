/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonBlockLoginByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonCancelChangeEmailAddressByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeEmailAddressByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeStatusRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonResetPasswordByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonSendVerificationMailByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonSetEmailVerifiedByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonSetEmailVerifiedByAdminResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonUnblockLoginByAdminRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonVerifyEmailAddressResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonStatus;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonStatusExtended;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonVerificationStatus;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.ParticipantsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

public class PersonAdminUiEventControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestEmailSenderService emailSenderService;

    private Person personWithOauthAccountTenant1;
    private Person personWithOauthAccountTenant2;
    private OauthAccount oAuthAccount;
    private OauthAccount oAuthAccountWithoutUsernamePassword;
    private Person personGlobalUserAdmin;
    private Person personUserAdminTenant1;
    private Person personUserAdminTenant2;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        personWithOauthAccountTenant1 = th.personRegularAnna;
        personWithOauthAccountTenant1 = th.addVerificationStatusToPerson(personWithOauthAccountTenant1,
                PersonVerificationStatus.EMAIL_VERIFIED);
        assertEquals(th.tenant1, personWithOauthAccountTenant1.getTenant());
        oAuthAccount = OauthAccount.builder()
                .oauthId(personWithOauthAccountTenant1.getOauthId())
                .name(personWithOauthAccountTenant1.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .emailVerified(false)
                .blocked(false)
                .blockReason(null)
                .loginCount(12)
                .build();
        testOauthManagementService.addOAuthAccount(oAuthAccount, personWithOauthAccountTenant1.getEmail());

        personWithOauthAccountTenant2 = th.personRegularKarl;
        assertEquals(th.tenant1, personWithOauthAccountTenant2.getTenant());
        testOauthManagementService.addOAuthAccount(oAuthAccount, personWithOauthAccountTenant2.getEmail());

        oAuthAccountWithoutUsernamePassword = OauthAccount.builder()
                .oauthId(th.personRegularHorstiTenant3.getOauthId())
                .name(th.personRegularHorstiTenant3.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.APPLE))
                .emailVerified(false)
                .blocked(false)
                .blockReason(null)
                .loginCount(21)
                .build();
        testOauthManagementService.addOAuthAccount(oAuthAccountWithoutUsernamePassword, th.personRegularHorstiTenant3.getEmail());

        personGlobalUserAdmin =
                th.createPersonWithDefaultAddress("personGlobalUserAdmin", th.tenant1,
                        "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalUserAdmin, GlobalUserAdmin.class, null);

        personUserAdminTenant1 =
                th.createPersonWithDefaultAddress("personUserAdminTenant1", th.tenant1,
                        "6b0239cf-f13d-4af0-b401-e984686194f2");
        th.assignRoleToPerson(personUserAdminTenant1, UserAdmin.class, th.tenant1.getId());

        personUserAdminTenant2 =
                th.createPersonWithDefaultAddress("personUserAdminTenant2", th.tenant2,
                        "8426493e-7498-408b-8d96-32684c82a7a3");
        th.assignRoleToPerson(personUserAdminTenant2, UserAdmin.class, th.tenant2.getId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void changeEmailAddressByAdminRequest_SuperAdmin() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        personToBeChanged.setNumberOfEmailChanges(1);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .newEmailAddressVerified(false)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientPerson.email").value(newEmail))
                .andExpect(jsonPath("$.clientPerson.pendingNewEmail").doesNotExist())
                .andExpect(jsonPath("$.clientPerson.address.id").isNotEmpty())
                .andExpect(jsonPath("$.clientPerson.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods",
                        hasSize(oAuthAccount.getAuthenticationMethods().size())))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods[0]").value(
                        oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                .andExpect(
                        jsonPath("$.clientPerson.oauthAccount.blockReason").isEmpty())
                .andExpect(
                        jsonPath("$.clientPerson.oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.retrievalFailed").value(false))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.failedReason").isEmpty())
                .andExpect(jsonPath("$.clientPerson.verificationStatuses").isEmpty());

        final Person personAfterChange = personRepository.findByEmail(newEmail).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
        // number of email changes should be unaffected by admin
        assertEquals(1, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressByAdminRequest_GlobalUserAdmin() throws Exception {

        final Person caller = personGlobalUserAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.setNumberOfEmailChanges(1);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .newEmailAddressVerified(false)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientPerson.email").value(newEmail))
                .andExpect(jsonPath("$.clientPerson.address.id").isNotEmpty())
                .andExpect(jsonPath("$.clientPerson.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods",
                        hasSize(oAuthAccount.getAuthenticationMethods().size())))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods[0]").value(
                        oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blockReason").isEmpty())
                .andExpect(
                        jsonPath("$.clientPerson.oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.retrievalFailed").value(false))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.failedReason").isEmpty())
                .andExpect(jsonPath("$.clientPerson.verificationStatuses").isEmpty());

        final Person personAfterChange = personRepository.findByEmail(newEmail).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
        // number of email changes should be unaffected by admin
        assertEquals(1, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        // the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressByAdminRequest_UserAdmin() throws Exception {

        final Person caller = personUserAdminTenant1;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.getVerificationStatuses().removeValue(PersonVerificationStatus.EMAIL_VERIFIED);
        personToBeChanged.setNumberOfEmailChanges(1);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .newEmailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientPerson.email").value(newEmail))
                .andExpect(jsonPath("$.clientPerson.address.id").isNotEmpty())
                .andExpect(jsonPath("$.clientPerson.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods",
                        hasSize(oAuthAccount.getAuthenticationMethods().size())))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods[0]").value(
                        oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blockReason").isEmpty())
                .andExpect(
                        jsonPath("$.clientPerson.oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.retrievalFailed").value(false))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.failedReason").isEmpty())
                .andExpect(jsonPath("$.clientPerson.verificationStatuses", contains(
                        ClientPersonVerificationStatus.EMAIL_VERIFIED.name())));

        final Person personAfterChange = personRepository.findByEmail(newEmail).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
        // number of email changes should be unaffected by admin
        assertEquals(1, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        //the verification mail was not send out due to newEmailAddressVerified = true
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(newEmail);
    }

    @Test
    public void changeEmailAddressByAdminRequest_EmailVerificationTurnedOff() throws Exception {

        final Person caller = personUserAdminTenant1;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check that the verification mail is not sent out
        setEmailVerificationFeatureEnabled(false);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .newEmailAddressVerified(false)
                        .build();

        //no need to check the response in detail again
        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk());

        // email sending is asynchronous
        waitForEventProcessing();

        // the verification mail was not sent out due to EmailVerificationFeatureEnabled = false
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .doesNotContain(newEmail);
    }

    @Test
    public void changeEmailAddressByAdminRequest_ChangeIntervalIsIgnored() throws Exception {

        final Person caller = personUserAdminTenant1;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        //we want to check if the verification mail is sent out
        setEmailVerificationFeatureEnabled(true);

        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        // number of email changes = 99 and sent last verification email 1 minute ago (== last change one minute ago)
        personToBeChanged.setNumberOfEmailChanges(99);
        personToBeChanged.setLastSentTimeEmailVerification(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(1));
        th.personRepository.save(personToBeChanged);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .newEmailAddressVerified(false)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientPerson.email").value(newEmail))
                .andExpect(jsonPath("$.clientPerson.address.id").isNotEmpty())
                .andExpect(jsonPath("$.clientPerson.profilePicture.id").value(
                        personToBeChanged.getProfilePicture().getId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods",
                        hasSize(oAuthAccount.getAuthenticationMethods().size())))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods[0]").value(
                        oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blockReason").isEmpty())
                .andExpect(
                        jsonPath("$.clientPerson.oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.retrievalFailed").value(false))
                .andExpect(jsonPath("$.clientPerson.oauthAccount.failedReason").isEmpty())
                .andExpect(jsonPath("$.clientPerson.verificationStatuses").isEmpty());

        final Person personAfterChange = personRepository.findByEmail(newEmail).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
        assertEquals(99, personAfterChange.getNumberOfEmailChanges());

        // email sending is asynchronous
        waitForEventProcessing();

        // the verification mail was sent out
        assertThat(emailSenderService.getEmails())
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(newEmail);
    }

    @Test
    public void changeEmailAddressByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";

        assertFalse(personRepository.existsByEmailOrPendingNewEmail(newEmail));

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_SameEmail() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = personToBeChanged.getEmail();

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_EmailAlreadyRegistered() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = th.personRegularHorstiTenant3.getEmail();

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_PendingNewEmailEmailAlreadyRegistered() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final Person existingPerson = th.personRegularHorstiTenant3;
        final String newEmail = "new@mail.de";
        existingPerson.setPendingNewEmail(newEmail);
        personRepository.saveAndFlush(existingPerson);

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ALREADY_REGISTERED));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_InvalidOAuthId() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = th.personRegularKarl;
        final String newEmail = "new@mail.de";
        testOauthManagementService.deleteUser(personToBeChanged.getOauthId());

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_NoUsernamePasswordAccount() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = th.personRegularHorstiTenant3;
        final String newEmail = "new@mail.de";

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .newEmailAddress(newEmail)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE));

        final Person personAfterChange = personRepository.findByEmail(personToBeChanged.getEmail()).get();
        assertEquals(personToBeChanged.getId(), personAfterChange.getId());
    }

    @Test
    public void changeEmailAddressByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId("personId")
                        .newEmailAddress("email@mail.mail")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId("personId")
                        .newEmailAddress("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId("personId")
                        .newEmailAddress(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(th.personRegularAnna.getId())
                        .newEmailAddress("not_valid_mail")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ADDRESS_INVALID));

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId("")
                        .newEmailAddress("email@mail.mail")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId(null)
                        .newEmailAddress("email@mail.mail")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void changeEmailAddressByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                ClientPersonChangeEmailAddressByAdminRequest.builder()
                        .personId("id")
                        .newEmailAddress("mail")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personChangeEmailAddressByAdminRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/changeEmailAddressByAdminRequest")
                .contentType(contentType)
                .content(json(personChangeEmailAddressByAdminRequest)));
    }

    private void setEmailVerificationFeatureEnabled(boolean b) {
        final FeatureConfig featureConfig = FeatureConfig.builder()
                .enabled(b)
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .build();
        featureConfigRepository.save(featureConfig);
    }

    @Test
    public void changeNameByAdminRequest() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant1;
            final String newFirstName = "newFirst";
            final String newLastName = "newLast";

            final ClientPersonChangeNameByAdminRequest personChangeNameByAdminRequest =
                    ClientPersonChangeNameByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .firstName(newFirstName)
                            .lastName(newLastName)
                            .build();

            mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                            .headers(authHeadersFor(caller))
                            .contentType(contentType)
                            .content(json(personChangeNameByAdminRequest)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.clientPerson.firstName").value(newFirstName))
                    .andExpect(jsonPath("$.clientPerson.lastName").value(newLastName))
                    .andExpect(jsonPath("$.clientPerson.email").value(personToBeChanged.getEmail()))
                    .andExpect(jsonPath("$.clientPerson.address.id").isNotEmpty())
                    .andExpect(jsonPath("$.clientPerson.profilePicture.id").value(
                            personToBeChanged.getProfilePicture().getId()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.oauthId").value(oAuthAccount.getOauthId()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods",
                            hasSize(oAuthAccount.getAuthenticationMethods().size())))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.authenticationMethods[0]").value(
                            oAuthAccount.getAuthenticationMethods().iterator().next().name()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.blockReason").isEmpty())
                    .andExpect(
                            jsonPath("$.clientPerson.oauthAccount.loginCount").value(oAuthAccount.getLoginCount()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.blocked").value(oAuthAccount.isBlocked()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.retrievalFailed").value(false))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount.failedReason").isEmpty())
                    .andExpect(jsonPath("$.clientPerson.verificationStatuses[0]").value(
                            ClientPersonVerificationStatus.EMAIL_VERIFIED.name()));

            final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
            assertEquals(newFirstName, personAfterChange.getFirstName());
            assertEquals(newLastName, personAfterChange.getLastName());
        }
    }

    @Test
    public void changeNameByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        final String newFirstName = "newFirst";
        final String newLastName = "newLast";

        final ClientPersonChangeNameByAdminRequest personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .firstName(newFirstName)
                        .lastName(newLastName)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertEquals(personToBeChanged.getFirstName(), personAfterChange.getFirstName());
        assertEquals(personToBeChanged.getLastName(), personAfterChange.getLastName());
    }

    @Test
    public void changeNameByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonChangeNameByAdminRequest personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("personId")
                        .firstName("first")
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("personId")
                        .firstName("first")
                        .lastName("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("personId")
                        .firstName("first")
                        .lastName(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("personId")
                        .firstName("")
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("personId")
                        .firstName(null)
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("")
                        .firstName("first")
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId(null)
                        .firstName("first")
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void changeNameByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonChangeNameByAdminRequest personChangeNameByAdminRequest =
                ClientPersonChangeNameByAdminRequest.builder()
                        .personId("id")
                        .firstName("first")
                        .lastName("last")
                        .build();

        mockMvc.perform(post("/adminui/person/event/changeNameByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personChangeNameByAdminRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/changeNameByAdminRequest")
                .contentType(contentType)
                .content(json(personChangeNameByAdminRequest)));
    }

    @Test
    public void cancelChangeEmailAddressRequest() throws Exception {

        final Person caller = th.personSuperAdmin;
        Person personToBeChanged = th.personRegularHorstiTenant3;
        final String newEmail = "new@mail.de";
        personToBeChanged.setPendingNewEmail(newEmail);
        personToBeChanged = personRepository.saveAndFlush(personToBeChanged);

        assertThat(personToBeChanged.getPendingNewEmail()).isEqualTo(newEmail);

        mockMvc.perform(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                                .personId(personToBeChanged.getId())
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.person.id").value(personToBeChanged.getId()))
                .andExpect(jsonPath("$.person.pendingNewEmail").doesNotExist());

        final Person personAfterChange = personRepository.findById(personToBeChanged.getId()).get();
        assertThat(personAfterChange.getPendingNewEmail()).isNull();
    }

    @Test
    public void cancelChangeEmailAddressRequest_PersonNotFound() throws Exception {

        final Person caller = th.personSuperAdmin;

        mockMvc.perform(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                                .personId("invalidPersonId")
                                .build())))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void cancelChangeEmailAddressRequest_NoPendingNewEmail() throws Exception {

        final Person caller = th.personSuperAdmin;

        mockMvc.perform(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                                .personId(th.personRegularHorstiTenant3.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.PERSON_HAS_NO_PENDING_NEW_EMAIL));
    }

    @Test
    public void cancelChangeEmailAddressRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;

        mockMvc.perform(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                                .personId(personToBeChanged.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void cancelChangeEmailAddressRequest_Unauthorized() throws Exception {

        final Person caller = th.personRegularKarl;
        final Person personToBeChanged = personWithOauthAccountTenant1;

        mockMvc.perform(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                                .personId(personToBeChanged.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(post("/adminui/person/event/cancelChangeEmailAddressByAdminRequest")
                .contentType(contentType)
                .content(json(ClientPersonCancelChangeEmailAddressByAdminRequest.builder()
                        .personId(th.personRegularHorstiTenant3.getId())
                        .build())));
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeBlocked = personWithOauthAccountTenant1;

            final ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                    ClientPersonBlockLoginByAdminRequest.builder()
                            .personId(personToBeBlocked.getId())
                            .build();

            assertFalse(personToBeBlocked.isDeleted());
            assertFalse(personToBeBlocked.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
            assertFalse(testOauthManagementService.isBlocked(personToBeBlocked.getOauthId()));

            mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personBlockLoginRequest)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.clientPerson.status").value(ClientPersonStatus.REGISTERED.toString()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount").isNotEmpty());

            final Person personAfterBlock = personRepository.findById(personToBeBlocked.getId()).get();
            assertTrue(personAfterBlock.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
            assertTrue(testOauthManagementService.isBlocked(personAfterBlock.getOauthId()));

            final ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                    ClientPersonUnblockLoginByAdminRequest.builder()
                            .personId(personToBeBlocked.getId())
                            .build();

            mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personUnblockLoginRequest)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.clientPerson.status").value(ClientPersonStatus.REGISTERED.toString()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount").isNotEmpty());

            final Person personAfterUnblock = personRepository.findById(personToBeBlocked.getId()).get();
            assertFalse(personAfterUnblock.isDeleted());
            assertFalse(personAfterUnblock.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
            assertFalse(testOauthManagementService.isBlocked(personAfterUnblock.getOauthId()));
        }
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeBlocked = personWithOauthAccountTenant1;

        final ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId(personToBeBlocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personBlockLoginRequest)))
                .andExpect(isNotAuthorized());

        final Person personAfterBlock = personRepository.findById(personToBeBlocked.getId()).get();
        assertEquals(personToBeBlocked.getStatus(), personAfterBlock.getStatus());

        final ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId(personToBeBlocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)))
                .andExpect(isNotAuthorized());

        final Person personAfterUnblock = personRepository.findById(personToBeBlocked.getId()).get();
        assertEquals(personToBeBlocked.getStatus(), personAfterUnblock.getStatus());
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest_DeletedPerson() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeBlocked = th.personRegularAnna;
        String oauthId = personToBeBlocked.getOauthId();

        final OauthAccount oAuthAccountDeletedPerson = OauthAccount.builder()
                .oauthId(oauthId)
                .name(personToBeBlocked.getFullName())
                .authenticationMethods(Collections.singleton(OauthAccount.AuthenticationMethod.USERNAME_PASSWORD))
                .loginCount(3)
                .build();
        testOauthManagementService.addOAuthAccount(oAuthAccountDeletedPerson, personToBeBlocked.getEmail());

        assertFalse(personToBeBlocked.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
        assertTrue(testOauthManagementService.userWithOauthIdExists(oauthId));

        final ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId(personToBeBlocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personBlockLoginRequest)))
                .andExpect(status().isOk());

        final Person personAfterBlock = personRepository.findById(personToBeBlocked.getId()).get();
        assertTrue(personAfterBlock.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
        assertTrue(testOauthManagementService.userWithOauthIdExists(oauthId));

        //delete the person (in a very simple way, so that all entities are still there)
        personAfterBlock.setDeleted(true);
        th.personRepository.saveAndFlush(personAfterBlock);

        final ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId(personToBeBlocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.clientPerson.status").value(ClientPersonStatus.DELETED.toString()));

        final Person personAfterUnblock = personRepository.findById(personToBeBlocked.getId()).get();
        //the account should be deleted, so the the user can register again
        assertFalse(testOauthManagementService.userWithOauthIdExists(oauthId));
        //the oauth id should be set to something random
        assertNotEquals(oauthId, personAfterUnblock.getOauthId());
        assertTrue(personAfterUnblock.isDeleted());
        assertFalse(personAfterUnblock.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED));
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest_InvalidOAuthId() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeBlocked = th.personRegularKarl;
        testOauthManagementService.deleteUser(personToBeBlocked.getOauthId());

        final ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId(personToBeBlocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personBlockLoginRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));

        final Person personAfterBlock = personRepository.findById(personToBeBlocked.getId()).get();
        assertEquals(personToBeBlocked.getStatus(), personAfterBlock.getStatus());

        final ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                .personId(personToBeBlocked.getId())
                .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personUnblockLoginRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));

        final Person personAfterUnblock = personRepository.findById(personToBeBlocked.getId()).get();
        assertEquals(personToBeBlocked.getStatus(), personAfterUnblock.getStatus());
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personBlockLoginRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                .personId("")
                .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personBlockLoginRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personBlockLoginRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void blockAndUnblockPersonLoginByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonBlockLoginByAdminRequest personBlockLoginRequest =
                ClientPersonBlockLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/blockLoginByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personBlockLoginRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/blockLoginByAdminRequest")
                .contentType(contentType)
                .content(json(personBlockLoginRequest)));

        final ClientPersonUnblockLoginByAdminRequest personUnblockLoginRequest =
                ClientPersonUnblockLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockLoginByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personUnblockLoginRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/unblockLoginByAdminRequest")
                .contentType(contentType)
                .content(json(personUnblockLoginRequest)));
    }

    @Test
    public void unblockPersonAutomaticallyBlockedLoginByAdminRequest() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeUnblocked = personWithOauthAccountTenant1;

            testOauthManagementService.blockUser(personToBeUnblocked, OauthAccount.BlockReason.TOO_MANY_LOGIN_ATTEMPTS);

            assertTrue(testOauthManagementService.isBlocked(personToBeUnblocked.getOauthId()));

            final ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest personUnblockAutomaticallyBlockedLoginRequest =
                    ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                            .personId(personToBeUnblocked.getId())
                            .build();

            mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personUnblockAutomaticallyBlockedLoginRequest)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.clientPerson.id").value(personToBeUnblocked.getId()))
                    .andExpect(jsonPath("$.clientPerson.oauthAccount").isNotEmpty());

            assertFalse(testOauthManagementService.isBlocked(personToBeUnblocked.getOauthId()));
        }
    }

    @Test
    public void unblockPersonAutomaticallyBlockedLoginByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeUnblocked = personWithOauthAccountTenant1;

        testOauthManagementService.blockUser(personToBeUnblocked, OauthAccount.BlockReason.TOO_MANY_LOGIN_ATTEMPTS);

        assertTrue(testOauthManagementService.isBlocked(personToBeUnblocked.getOauthId()));

        final ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest personUnblockAutomaticallyBlockedLoginRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId(personToBeUnblocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginRequest)))
                .andExpect(isNotAuthorized());

        assertTrue(testOauthManagementService.isBlocked(personToBeUnblocked.getOauthId()));
    }

    @Test
    public void unblockPersonAutomaticallyBlockedLoginByAdminRequest_DeletedPerson() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeUnblocked = th.personDeleted;

        final ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest personUnblockAutomaticallyBlockedLoginRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId(personToBeUnblocked.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginRequest)))
                .andExpect(isException(ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND));
    }

    @Test
    public void unblockPersonAutomaticallyBlockedLoginByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest
                personUnblockAutomaticallyBlockedLoginByAdminRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginByAdminRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personUnblockAutomaticallyBlockedLoginByAdminRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personUnblockAutomaticallyBlockedLoginByAdminRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginByAdminRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void unblockPersonAutomaticallyBlockedLoginByAdminRequest_Unauthorized() throws Exception {

        final Person caller = th.personRegularKarl;

        final ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest
                personUnblockAutomaticallyBlockedLoginByAdminRequest =
                ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personUnblockAutomaticallyBlockedLoginByAdminRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/unblockAutomaticallyBlockedLoginByAdminRequest")
                .contentType(contentType)
                .content(json(personUnblockAutomaticallyBlockedLoginByAdminRequest)));
    }

    @Test
    public void setEmailVerifiedByAdminRequest_AddEmailAddressVerificationStatus() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant2;

            assertFalse(personToBeChanged.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                    "Person has verification status 'EMAIL_VERIFIED'");

            final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                    ClientPersonSetEmailVerifiedByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .emailAddressVerified(true)
                            .build();

            ClientPersonSetEmailVerifiedByAdminResponse clientPersonSetEmailVerifiedByAdminResponse =
                    toObject(mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                            .headers(authHeadersFor(caller))
                            .contentType(contentType)
                            .content(json(personSetEmailVerifiedRequest)))
                            .andExpect(status().isOk())
                            .andReturn(), ClientPersonSetEmailVerifiedByAdminResponse.class);

            Person updatedPerson = th.personRepository.findById(
                    clientPersonSetEmailVerifiedByAdminResponse.getUpdatedPerson().getId()).get();

            assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                    "Person has not verification status 'EMAIL_VERIFIED'");

            // Verify push message
            waitForEventProcessing();

            // Assert that pushToPersonInLastUsedAppVariantLoud was triggered
            final Pair<String, ClientPersonVerifyEmailAddressResponse> pushedEvent =
                    testClientPushService.getPushedEventToPersonInLastUsedAppVariantLoud(
                            updatedPerson,
                            ClientPersonVerifyEmailAddressResponse.class,
                            findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED));

            assertEquals(updatedPerson.getId(), pushedEvent.getRight().getPersonWithVerifiedEmail().getId());
            assertEquals("E-Mail-Adresse '" + updatedPerson.getEmail() + "' wurde erfolgreich verifiziert",
                    pushedEvent.getLeft());
            assertTrue(pushedEvent.getRight().isVerificationStatusChanged(),
                    "'EMAIL_VERIFIED' verification status was added to person");
            testClientPushService.assertNoMorePushedEvents();

            personWithOauthAccountTenant2.getVerificationStatuses().removeValue(
                    PersonVerificationStatus.EMAIL_VERIFIED);
            th.personRepository.save(personWithOauthAccountTenant2);
        }
    }

    @Test
    public void setEmailVerifiedByAdminRequest_RemoveEmailAddressVerificationStatus() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant1;

            assertTrue(personToBeChanged.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                    "Person has not verification status 'EMAIL_VERIFIED'");

            ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                    ClientPersonSetEmailVerifiedByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .emailAddressVerified(false)
                            .build();

            ClientPersonSetEmailVerifiedByAdminResponse clientPersonSetEmailVerifiedByAdminResponse =
                    toObject(mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                            .headers(authHeadersFor(caller))
                            .contentType(contentType)
                            .content(json(personSetEmailVerifiedRequest)))
                            .andExpect(status().isOk())
                            .andReturn(), ClientPersonSetEmailVerifiedByAdminResponse.class);

            Person updatedPerson = th.personRepository.findById(
                    clientPersonSetEmailVerifiedByAdminResponse.getUpdatedPerson().getId()).get();

            assertFalse(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                    "Person has not verification status 'EMAIL_VERIFIED'");

            waitForEventProcessing();

            testClientPushService.assertNoMorePushedEvents();
        }
    }

    @Test
    public void setEmailVerifiedByAdminRequest_DuplicatedVerified() throws Exception {

        final Person caller = th.personSuperAdmin;
        final Person personToBeChanged = personWithOauthAccountTenant1;
        th.personRepository.saveAndFlush(personToBeChanged);

        assertTrue(personToBeChanged.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertThat(personToBeChanged.getNumberOfEmailChanges()).isNull();

        final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .emailAddressVerified(true)
                        .build();

        final ClientPersonSetEmailVerifiedByAdminResponse clientPersonSetEmailVerifiedByAdminResponse =
                toObject(mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                        .andExpect(status().isOk())
                        .andReturn(), ClientPersonSetEmailVerifiedByAdminResponse.class);

        final Person updatedPerson = th.personRepository.findById(
                clientPersonSetEmailVerifiedByAdminResponse.getUpdatedPerson().getId()).get();

        assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertEquals(0, updatedPerson.getNumberOfEmailChanges());

        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void setEmailVerifiedByAdminRequest_PendingNewEmailAddEmailAddressVerificationStatus() throws Exception {

        final Person caller = th.personSuperAdmin;
        Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.setPendingNewEmail(newEmail);
        personToBeChanged.setNumberOfEmailChanges(5);
        th.personRepository.saveAndFlush(personToBeChanged);

        assertTrue(personToBeChanged.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");

        final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .emailAddressVerified(true)
                        .build();

        final ClientPersonSetEmailVerifiedByAdminResponse clientPersonSetEmailVerifiedByAdminResponse =
                toObject(mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                        .andExpect(status().isOk())
                        .andReturn(), ClientPersonSetEmailVerifiedByAdminResponse.class);

        final Person updatedPerson = th.personRepository.findById(
                clientPersonSetEmailVerifiedByAdminResponse.getUpdatedPerson().getId()).get();

        assertTrue(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertEquals(0, updatedPerson.getNumberOfEmailChanges());

        waitForEventProcessing();

        // Assert that pushToPersonInLastUsedAppVariantLoud was triggered
        final Pair<String, ClientPersonVerifyEmailAddressResponse> pushedEvent =
                testClientPushService.getPushedEventToPersonInLastUsedAppVariantLoud(
                        updatedPerson,
                        ClientPersonVerifyEmailAddressResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED));

        assertEquals(updatedPerson.getId(), pushedEvent.getRight().getPersonWithVerifiedEmail().getId());
        assertEquals("E-Mail-Adresse wurde erfolgreich zu '" + updatedPerson.getEmail() + "' geändert",
                pushedEvent.getLeft());
        assertFalse(pushedEvent.getRight().isVerificationStatusChanged(),
                "'EMAIL_VERIFIED' verification status was added to person");
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void setEmailVerifiedByAdminRequest_PendingNewEmailRemoveEmailAddressVerificationStatus() throws Exception {

        final Person caller = th.personSuperAdmin;
        Person personToBeChanged = personWithOauthAccountTenant1;
        final String newEmail = "new@mail.de";
        personToBeChanged.setPendingNewEmail(newEmail);
        personToBeChanged.setNumberOfEmailChanges(5);
        personToBeChanged = personRepository.saveAndFlush(personToBeChanged);

        assertTrue(personToBeChanged.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");

        final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .emailAddressVerified(false)
                        .build();

        final ClientPersonSetEmailVerifiedByAdminResponse clientPersonSetEmailVerifiedByAdminResponse =
                toObject(mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                        .andExpect(status().isOk())
                        .andReturn(), ClientPersonSetEmailVerifiedByAdminResponse.class);

        final Person updatedPerson = th.personRepository.findById(
                clientPersonSetEmailVerifiedByAdminResponse.getUpdatedPerson().getId()).get();

        assertFalse(updatedPerson.getVerificationStatuses().hasValue(PersonVerificationStatus.EMAIL_VERIFIED),
                "Person has not verification status 'EMAIL_VERIFIED'");
        assertEquals(5, updatedPerson.getNumberOfEmailChanges());

        waitForEventProcessing();

        // Assert that pushToPersonInLastUsedAppVariantLoud was triggered
        final Pair<String, ClientPersonVerifyEmailAddressResponse> pushedEvent =
                testClientPushService.getPushedEventToPersonInLastUsedAppVariantLoud(
                        updatedPerson,
                        ClientPersonVerifyEmailAddressResponse.class,
                        findPushCategoriesBySubject(ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED));

        assertEquals(updatedPerson.getId(), pushedEvent.getRight().getPersonWithVerifiedEmail().getId());
        assertEquals("E-Mail-Adresse wurde erfolgreich zu '" + updatedPerson.getEmail() + "' geändert",
                pushedEvent.getLeft());
        assertFalse(pushedEvent.getRight().isVerificationStatusChanged(),
                "'EMAIL_VERIFIED' verification status was added to person");
        testClientPushService.assertNoMorePushedEvents();

        personWithOauthAccountTenant2.getVerificationStatuses().removeValue(
                PersonVerificationStatus.EMAIL_VERIFIED);
        th.personRepository.save(personWithOauthAccountTenant2);
    }

    @Test
    public void setEmailVerifiedByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;

        final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .emailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void setEmailVerifiedByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId("personId")
                        .emailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId("")
                        .emailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId(null)
                        .emailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void setEmailVerifiedByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonSetEmailVerifiedByAdminRequest personSetEmailVerifiedRequest =
                ClientPersonSetEmailVerifiedByAdminRequest.builder()
                        .personId("personId")
                        .emailAddressVerified(true)
                        .build();

        mockMvc.perform(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personSetEmailVerifiedRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/setEmailVerifiedByAdminRequest")
                .contentType(contentType)
                .content(json(personSetEmailVerifiedRequest)));
    }

    @Test
    public void sendVerificationMailByAdminRequest() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );
        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant1;
            // Since the time restriction is neglected for admins, the email should be sent anyway
            personToBeChanged.setLastSentTimeEmailVerification(
                    timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(1));
            th.personRepository.save(personToBeChanged);

            ClientPersonSendVerificationMailByAdminRequest personSendVerificationMailRequest =
                    ClientPersonSendVerificationMailByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .build();

            mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personSendVerificationMailRequest)))
                    .andExpect(status().isOk());

            // Check if verification email was sent to person
            assertThat(emailSenderService.getEmails())
                    .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                    .contains(personToBeChanged.getEmail())
                    .doesNotContain(personToBeChanged.getPendingNewEmail());

            emailSenderService.clearMailList();
        }
    }

    @Test
    public void sendVerificationMailByAdminRequest_WithPendingNewEmail() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );
        final long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant1;
            // Since the time restriction is neglected for admins, the email should be sent anyway
            personToBeChanged.setLastSentTimeEmailVerification(
                    timeServiceMock.currentTimeMillisUTC() - TimeUnit.MINUTES.toMillis(1));
            personToBeChanged.setPendingNewEmail("new@mail.de");
            th.personRepository.save(personToBeChanged);

            ClientPersonSendVerificationMailByAdminRequest personSendVerificationMailRequest =
                    ClientPersonSendVerificationMailByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .build();

            mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personSendVerificationMailRequest)))
                    .andExpect(status().isOk());

            // Check if verification email was sent to person
            assertThat(emailSenderService.getEmails())
                    .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                    .doesNotContain(personToBeChanged.getEmail())
                    .contains(personToBeChanged.getPendingNewEmail());

            emailSenderService.clearMailList();
        }
    }

    @Test
    public void sendVerificationMailByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;

        final ClientPersonSendVerificationMailByAdminRequest personSendVerificationMailRequest =
                ClientPersonSendVerificationMailByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .build();

        mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSendVerificationMailRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void sendVerificationMailByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonSendVerificationMailByAdminRequest personSendVerificationMailRequest =
                ClientPersonSendVerificationMailByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSendVerificationMailRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personSendVerificationMailRequest =
                ClientPersonSendVerificationMailByAdminRequest.builder()
                        .personId("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSendVerificationMailRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personSendVerificationMailRequest =
                ClientPersonSendVerificationMailByAdminRequest.builder()
                        .personId(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personSendVerificationMailRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void sendVerificationMailByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonSendVerificationMailByAdminRequest personSendVerificationMailRequest =
                ClientPersonSendVerificationMailByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personSendVerificationMailRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/sendVerificationMailByAdminRequest")
                .contentType(contentType)
                .content(json(personSendVerificationMailRequest)));
    }

    @Test
    public void resetPasswordByAdminRequest() throws Exception {

        final List<Person> personsWithSuitableRoles = Arrays.asList(
                th.personSuperAdmin,
                personGlobalUserAdmin,
                personUserAdminTenant1
        );

        for (Person caller : personsWithSuitableRoles) {
            final Person personToBeChanged = personWithOauthAccountTenant1;

            final ClientPersonResetPasswordByAdminRequest personResetPasswordRequest =
                    ClientPersonResetPasswordByAdminRequest.builder()
                            .personId(personToBeChanged.getId())
                            .build();

            final String oldPassword = testOauthManagementService.getEmailToPasswordMap().get(personWithOauthAccountTenant1.getEmail());

            mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                    .headers(authHeadersFor(caller))
                    .contentType(contentType)
                    .content(json(personResetPasswordRequest)))
                    .andExpect(status().isOk());

            assertNotEquals(oldPassword, testOauthManagementService.getEmailToPasswordMap().get(personWithOauthAccountTenant1.getEmail()));
        }
    }

    @Test
    public void resetPasswordByAdminRequest_UserAdminWrongTenant() throws Exception {

        final Person caller = personUserAdminTenant2;
        final Person personToBeChanged = personWithOauthAccountTenant1;

        final ClientPersonResetPasswordByAdminRequest personResetPasswordRequest =
                ClientPersonResetPasswordByAdminRequest.builder()
                        .personId(personToBeChanged.getId())
                        .build();

        final String oldPassword =
                testOauthManagementService.getEmailToPasswordMap().get(personWithOauthAccountTenant1.getEmail());

        mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personResetPasswordRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertEquals(oldPassword,
                testOauthManagementService.getEmailToPasswordMap().get(personWithOauthAccountTenant1.getEmail()));
    }

    @Test
    public void resetPasswordByAdminRequest_InvalidRequest() throws Exception {

        final Person caller = th.personSuperAdmin;

        ClientPersonResetPasswordByAdminRequest personResetPasswordRequest =
                ClientPersonResetPasswordByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personResetPasswordRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        personResetPasswordRequest =
                ClientPersonResetPasswordByAdminRequest.builder()
                        .personId("")
                        .build();

        mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personResetPasswordRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        personResetPasswordRequest =
                ClientPersonResetPasswordByAdminRequest.builder()
                        .personId(null)
                        .build();

        mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(personResetPasswordRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void resetPasswordByAdminRequest_Unauthorized() throws Exception {

        final ClientPersonResetPasswordByAdminRequest personResetPasswordRequest =
                ClientPersonResetPasswordByAdminRequest.builder()
                        .personId("personId")
                        .build();

        mockMvc.perform(post("/adminui/person/event/resetPasswordByAdminRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(personResetPasswordRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/person/event/resetPasswordByAdminRequest")
                .contentType(contentType)
                .content(json(personResetPasswordRequest)));
    }

    @Test
    public void personChangeStatusByAdminRequest_EmptyStatuses() throws Exception {

        final Person person = th.personRegularAnna;
        person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
        th.personRepository.saveAndFlush(person);

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {

            assertThat(person.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.PENDING_DELETION);

            mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, person.getId(), Collections.emptySet(),
                    Collections.emptySet()))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedPerson.id").value(person.getId()))
                    .andExpect(jsonPath("$.updatedPerson.statuses", hasSize(1)))
                    .andExpect(jsonPath("$.updatedPerson.statuses",
                            containsInAnyOrder(ClientPersonStatusExtended.PENDING_DELETION.name())));

            final Person updatedPerson = th.personRepository.findById(person.getId()).get();
            assertThat(updatedPerson.getStatuses().getValues()).containsExactlyInAnyOrder(
                    PersonStatus.PENDING_DELETION);
        }
    }

    @Test
    public void personChangeStatusByAdminRequest_AddStatuses() throws Exception {

        final Set<ClientPersonStatusExtended> statusesToAdd =
                Stream.of(ClientPersonStatusExtended.LOGIN_BLOCKED,
                        ClientPersonStatusExtended.PENDING_DELETION,
                        ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT)
                        .collect(Collectors.toSet());

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {
            final Person person = th.personRegularAnna;
            person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
            th.personRepository.saveAndFlush(person);

            assertThat(person.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.PENDING_DELETION);

            mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, person.getId(), statusesToAdd,
                    Collections.emptySet()))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedPerson.id").value(person.getId()))
                    .andExpect(jsonPath("$.updatedPerson.statuses", hasSize(3)))
                    .andExpect(jsonPath("$.updatedPerson.statuses",
                            containsInAnyOrder(ClientPersonStatusExtended.LOGIN_BLOCKED.name(),
                                    ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT.name(),
                                    ClientPersonStatusExtended.PENDING_DELETION.name())));

            final Person updatedPerson = th.personRepository.findById(person.getId()).get();
            assertThat(updatedPerson.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.LOGIN_BLOCKED,
                    PersonStatus.PARALLEL_WORLD_INHABITANT, PersonStatus.PENDING_DELETION);

            // remove all statuses from person to get initial state
            updatedPerson.getStatuses().clear();
            th.personRepository.saveAndFlush(person);
        }
    }

    @Test
    public void personChangeStatusByAdminRequest_RemoveStatuses() throws Exception {

        final Set<ClientPersonStatusExtended> statusesToRemove =
                Stream.of(ClientPersonStatusExtended.PENDING_DELETION,
                        ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT)
                        .collect(Collectors.toSet());

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {
            final Person person = th.personRegularAnna;
            person.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
            person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
            th.personRepository.saveAndFlush(person);

            assertThat(person.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.LOGIN_BLOCKED,
                    PersonStatus.PENDING_DELETION);

            mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, person.getId(), Collections.emptySet(),
                    statusesToRemove))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedPerson.id").value(person.getId()))
                    .andExpect(jsonPath("$.updatedPerson.statuses", hasSize(1)))
                    .andExpect(jsonPath("$.updatedPerson.statuses",
                            containsInAnyOrder(ClientPersonStatusExtended.LOGIN_BLOCKED.name())));

            final Person updatedPerson = th.personRepository.findById(person.getId()).get();
            assertThat(updatedPerson.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.LOGIN_BLOCKED);

            // remove all statuses from person to get initial state
            updatedPerson.getStatuses().clear();
            th.personRepository.saveAndFlush(person);
        }
    }

    @Test
    public void personChangeStatusByAdminRequest_AddAndRemoveStatuses() throws Exception {

        final Set<ClientPersonStatusExtended> statusesToAdd =
                Stream.of(ClientPersonStatusExtended.LOGIN_BLOCKED,
                        ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT)
                        .collect(Collectors.toSet());
        final Set<ClientPersonStatusExtended> statusesToRemove =
                Collections.singleton(ClientPersonStatusExtended.PENDING_DELETION);

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalUserAdmin, personUserAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {
            final Person person = th.personRegularAnna;
            person.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
            person.getStatuses().addValue(PersonStatus.PENDING_DELETION);
            th.personRepository.saveAndFlush(person);

            assertThat(person.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.LOGIN_BLOCKED,
                    PersonStatus.PENDING_DELETION);

            mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, person.getId(), statusesToAdd,
                    statusesToRemove))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.updatedPerson.id").value(person.getId()))
                    .andExpect(jsonPath("$.updatedPerson.statuses", hasSize(2)))
                    .andExpect(jsonPath("$.updatedPerson.statuses",
                            containsInAnyOrder(ClientPersonStatusExtended.LOGIN_BLOCKED.name(),
                                    ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT.name())));

            final Person updatedPerson = th.personRepository.findById(person.getId()).get();
            assertThat(updatedPerson.getStatuses().getValues()).containsExactlyInAnyOrder(PersonStatus.LOGIN_BLOCKED,
                    PersonStatus.PARALLEL_WORLD_INHABITANT);

            // remove all statuses from person to get initial state
            updatedPerson.getStatuses().clear();
            th.personRepository.saveAndFlush(person);
        }
    }

    @Test
    public void personChangeStatusByAdminRequest_Unauthorized() throws Exception {

        final String personId = th.personRegularAnna.getId();

        assertOAuth2(
                buildPersonChangeStatusByAdminRequest(null, personId, Collections.emptySet(), Collections.emptySet()));

        // Caller does not have required role
        mockMvc.perform(buildPersonChangeStatusByAdminRequest(th.personRegularKarl, personId, Collections.emptySet(),
                Collections.emptySet()))
                .andExpect(isNotAuthorized());

        // Caller does have required role but for different tenant
        mockMvc.perform(buildPersonChangeStatusByAdminRequest(personUserAdminTenant2, personId, Collections.emptySet(),
                Collections.emptySet()))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void personChangeStatusByAdminRequest_InvalidAttributes() throws Exception {

        final Person caller = personGlobalUserAdmin;

        // personId NULL
        mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, null, Collections.emptySet(),
                Collections.emptySet()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // personId empty
        mockMvc.perform(buildPersonChangeStatusByAdminRequest(caller, "", Collections.emptySet(),
                Collections.emptySet()))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void personChangeStatusByAdminRequest_PersonNotFound() throws Exception {

        final Person caller = personGlobalUserAdmin;

        mockMvc.perform(
                buildPersonChangeStatusByAdminRequest(caller, "invalidPersonId", Collections.emptySet(),
                        Collections.emptySet()))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder buildPersonChangeStatusByAdminRequest(Person caller, String personId,
            Set<ClientPersonStatusExtended> statusesToAdd, Set<ClientPersonStatusExtended> statusesToRemove)
            throws IOException {
        MockHttpServletRequestBuilder builder = post("/adminui/person/event/personChangeStatusRequest")
                .contentType(contentType)
                .content(json(ClientPersonChangeStatusRequest.builder()
                        .personId(personId)
                        .statusesToAdd(statusesToAdd)
                        .statusesToRemove(statusesToRemove)
                        .build()));
        if (caller != null) {
            builder = builder.headers(authHeadersFor(caller));
        }
        return builder;
    }

}
