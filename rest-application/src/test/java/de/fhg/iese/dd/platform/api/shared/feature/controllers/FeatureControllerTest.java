/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Jannis von Albedyll, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.feature.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeature;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureServiceTest;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature1;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature2;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeature3;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestFeatureInvisibleForClients;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Tag("feature")
public class FeatureControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private FeatureConfigRepository featureConfigRepository;

    private AppVariant appVariant;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        appVariant = th.app1Variant1;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getFeatures() throws Exception {

        saveEnabledFeature1App1Variant1();
        // Disabled feature should also be found
        saveDisabledFeature2App1Variant1();

        List<ClientFeature> expectedFeatures =
                Arrays.asList(createClientFeature1Enabled(), createClientFeature2Disabled());

        mockMvc.perform(get("/feature/")
                        .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size()))) // Find disabled + enabled feature
                .andExpect(jsonEquals(expectedFeatures, "$[0].id", "$[1].id"));
    }

    @Test
    public void getFeatures_InvisibleForClients() throws Exception {

        saveEnabledFeature1App1Variant1();
        saveDisabledFeature2App1Variant1();

        //this should be invisible, as configured in the @Feature annotation
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeatureInvisibleForClients.class.getName())
                .enabled(true)
                .appVariant(appVariant)
                .build());

        List<ClientFeature> expectedFeatures =
                Arrays.asList(createClientFeature1Enabled(), createClientFeature2Disabled());

        mockMvc.perform(get("/feature/")
                        .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size()))) // Find disabled + enabled feature
                .andExpect(jsonEquals(expectedFeatures, "$[0].id", "$[1].id"));
    }

    @Test
    public void getFeature_Common() throws Exception {

        saveEnabledFeature1Common();

        List<ClientFeature> expectedFeatures = Collections.singletonList(createClientFeature1Enabled());

        mockMvc.perform(get("/feature/")
                        .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size()))) // Find tenant-specific feature only
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));
    }

    @Test
    public void getFeatures_InvalidStringValues() throws Exception {

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(false)
                .featureClass(TestFeature2.class.getName())
                .configValues("InvalidJSONString")
                .build());

        mockMvc.perform(get("/feature/")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getFeatures_WithoutIdentifier() throws Exception {

        //no feature identifier is set in the annotation of TestFeature3, so the class name is used as identifier
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(false)
                .featureClass(TestFeature3.class.getName())
                .configValues("{\"sampleStringValue\": \"\"}")
                .build());

        mockMvc.perform(get("/feature/")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].featureIdentifier").value(TestFeature3.class.getName()));
    }

    @Test
    public void getFeatures_InvalidAndValid() throws Exception {

        saveEnabledFeature1App1Variant1();
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .tenant(th.tenant1)
                .enabled(true)
                .configValues("InvalidJSONString")
                .build());

        List<ClientFeature> expectedFeatures = Collections.singletonList(createClientFeature1Enabled());

        mockMvc.perform(get("/feature/")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size())))
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));
    }

    @Test
    public void getFeatures_InvalidFeatureClasses() throws Exception {

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(false)
                .featureClass("de.fhg.iese.dd.platform.api.shared.controllers.FeatureControllerTest")
                .configValues("InvalidJSONString")
                .build());
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(false)
                .featureClass("")
                .configValues("InvalidJSONString")
                .build());

        mockMvc.perform(get("/feature/")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getFeatures_OnlyEnabled() throws Exception {

        saveEnabledFeature1App1Variant1();
        // Disabled feature should NOT be found
        saveDisabledFeature2App1Variant1();

        List<ClientFeature> expectedFeatures = Collections.singletonList(createClientFeature1Enabled());

        mockMvc.perform(get("/feature/")
                .param("onlyEnabled", "true")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size())))
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));// Find enabled only
    }

    @Test
    public void getFeatures_NoConfiguration() throws Exception {

        saveEnabledFeature1App1Variant1();

        AppVariant appVariant = th.app2Variant2; // Wrong appVariant --> expect empty response

        mockMvc.perform(get("/feature/")
                .headers(authHeadersFor(th.personRegularKarl, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getFeatures_OAuth2AppVariantRequired() throws Exception {

        assertOAuth2AppVariantRequired(get("/feature/"), appVariant, th.personRegularKarl);
    }

    @Test
    public void getFeatureDefault() throws Exception {

        // no tenant, no app variant
        saveEnabledFeature1Common();
        // no tenant, app 1 variant 1
        saveEnabledFeature1App1Variant1();

        List<ClientFeature> expectedFeatures = Collections.singletonList(createClientFeature1Enabled());

        mockMvc.perform(get("/feature/default")
                        .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                //only one feature, because the same feature was configured twice
                // - with the app variant
                // - without the app variant
                // -> Expected result: the config for the app variant
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));
    }

    @Test
    public void getFeatureDefault_GeoArea() throws Exception {

        final GeoArea geoArea = th.geoAreaEisenberg;
        // no tenant, no app variant
        saveEnabledFeature1Common();
        // no tenant, app 1 variant 1
        saveEnabledFeature1App1Variant1();
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .appVariant(appVariant)
                .geoAreasIncluded(Collections.singleton(geoArea))
                .configValues(FeatureServiceTest.GEOAREA1_VALUES)
                .build());

        List<ClientFeature> expectedFeatures = Collections.singletonList(ClientFeature.builder()
                .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                .enabled(true)
                .featureValues("{\n" +
                        "   \"sampleStringValue\" : \"geoarea1\"," + //overwritten from geo area config
                        "   \"sampleIntValue\" : 47112,\n" +
                        "   \"sampleBooleanValue\" : false,\n" +
                        "   \"sampleStringValues\" : [\"commonList1\",\"commonList2\"],\n" +
                        "   \"sampleEnum\" : \"POST\"\n" +
                        " }")
                .build());

        mockMvc.perform(get("/feature/default")
                        .param("homeAreaId", geoArea.getId())
                        .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                //only one feature, because the same feature was configured thrice
                // - with the app variant
                // - without the app variant
                // - with the app variant and geo area
                // -> Expected result: the config for the geo area wins and overwrites the others
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));
    }

    @Test
    public void getFeaturesDefault_OnlyEnabled() throws Exception {

        saveEnabledFeature1App1Variant1();
        // Disabled feature should NOT be found
        saveDisabledFeature2App1Variant1();

        List<ClientFeature> expectedFeatures = Collections.singletonList(createClientFeature1Enabled());

        mockMvc.perform(get("/feature/default")
                .param("onlyEnabled", "true")
                .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedFeatures.size())))
                .andExpect(jsonEquals(expectedFeatures, "$[0].id"));// Find enabled only
    }

    @Test
    public void getFeaturesDefault_InvalidStringValues() throws Exception {

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(false)
                .featureClass(TestFeature2.class.getName())
                .configValues("InvalidJSONString")
                .build());

        mockMvc.perform(get("/feature/default")
                .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getFeaturesDefault_NoConfiguration() throws Exception {

        saveEnabledFeature1App1Variant1();

        AppVariant appVariant = th.app2Variant2; // Wrong appVariant --> expect empty response

        mockMvc.perform(get("/feature/default")
                .headers(headersForAppVariant(appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getFeaturesDefaults_AppVariantRequired() throws Exception {

        mockMvc.perform(get("/feature/default"))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getFeaturesDefaults_InvalidGeoAreaId() throws Exception {

        mockMvc.perform(get("/feature/default")
                        .param("homeAreaId", UUID.randomUUID().toString()))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
    }

    private ClientFeature createClientFeature1Enabled() {
        return ClientFeature.builder()
                .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature1")
                .enabled(true)
                // this is a very lazy trick that only works since all feature values are defined in this config
                // for more complex configurations see FeatureServiceTest
                .featureValues(FeatureServiceTest.COMMON_VALUES)
                .build();
    }

    private ClientFeature createClientFeature2Disabled() {
        return ClientFeature.builder()
                .featureIdentifier("de.fhg.iese.dd.platform.test.TestFeature2")
                .enabled(false)
                // this is a very lazy trick that only works since all feature values are defined in this config
                // for more complex configurations see FeatureServiceTest
                .featureValues(FeatureServiceTest.COMMON_VALUES2)
                .build();
    }

    private void saveEnabledFeature1App1Variant1() {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .appVariant(appVariant)
                .configValues(FeatureServiceTest.COMMON_VALUES)
                .build());
    }

    private void saveEnabledFeature1Common() {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature1.class.getName())
                .enabled(true)
                .configValues(FeatureServiceTest.COMMON_VALUES)
                .build());
    }

    private void saveDisabledFeature2App1Variant1() {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestFeature2.class.getName())
                .enabled(false)
                .appVariant(appVariant)
                .configValues(FeatureServiceTest.COMMON_VALUES2)
                .build());
    }

}
