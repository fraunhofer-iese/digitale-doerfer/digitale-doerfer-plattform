/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.admintasks;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

public class OrphanMediaItemRemoverAdminTaskTest extends BaseServiceTest {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private MediaItemRepository mediaItemRepository;
    @Autowired
    private TestFileStorage fileStorage;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private OrphanMediaItemRemoverAdminTask orphanMediaItemRemoverAdminTask;
    @Autowired
    private IAdminTaskService adminTaskService;

    @Override
    public void createEntities() throws Exception { }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void removeOrphansTestDryRun() throws Exception {

        byte[] imageData = th.loadTestResource("testImage.jpg");
        MediaItem orphanMediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
        String mediaItemId = orphanMediaItem.getId();

        BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(true)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(orphanMediaItemRemoverAdminTask.getName(), adminTaskParameters);

        assertTrue(mediaItemRepository.existsById(mediaItemId));
    }

    @Test
    public void removeOrphansTest() throws Exception {

        byte[] imageData = th.loadTestResource("testImage.jpg");
        MediaItem orphanMediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.UNKNOWN);
        String fileName = orphanMediaItem.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName());
        String mediaItemId = orphanMediaItem.getId();

        assertTrue(fileStorage.fileExists(fileStorage.getInternalFileName(fileName)));

        BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(orphanMediaItemRemoverAdminTask.getName(), adminTaskParameters);

        waitForEventProcessing();

        assertFalse(mediaItemRepository.existsById(mediaItemId));

        assertFalse(fileStorage.fileExists(fileStorage.getInternalFileName(fileName)));
    }

}
