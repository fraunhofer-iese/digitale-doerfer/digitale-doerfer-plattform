/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.address.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.shared.address.exceptions.AddressInvalidException;
import de.fhg.iese.dd.platform.business.shared.geo.services.TestGoogleGeoService;
import de.fhg.iese.dd.platform.business.test.mocks.TestGeocodingService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

public class AddressServiceTest extends BaseServiceTest {

    /**
     * run the tests using real google lookup. This is only meant for manual testing.
     */
    private static final boolean RUN_WITH_GOOGLE_GEOCODING = false; //never commit when set to true!!!

    @Autowired
    private AddressService addressService;

    @Autowired
    private TestGeocodingService testGeocodingService;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ApplicationConfig applicationConfig;

    private static final String EXAMPLE_LOCATION_STRING = "Restaurant Da Nicola Eisenberg";

    /**
     * This is exactly what Google Maps returns when asked for EXAMPLE_LOCATION_STRING
     *
     * @return
     */
    private static final Address completeExampleAddress() {
        return Address.builder()
                .name("Restaurant Da Nicola")
                .street("Hauptstraße 111")
                .zip("67304")
                .city("Eisenberg (Pfalz)")
                .gpsLocation(new GPSLocation(49.55851999999999, 8.07485))
                .verified(true)
                .build();
    }

    @Override
    public void createEntities() throws Exception {
        if (RUN_WITH_GOOGLE_GEOCODING) {
            final Field geoService = AddressService.class.getDeclaredField("geoService");
            ReflectionUtils.makeAccessible(geoService);
            ReflectionUtils.setField(geoService, addressService, new TestGoogleGeoService(applicationConfig));
        }
    }

    @Override
    public void tearDown() throws Exception {
        testGeocodingService.clearLocationToAddressRegistration();
        addressRepository.deleteAll();
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameOnly_lookupWithResult() {

        //using EXAMPLE_LOCATION_STRING as name to make the tests also work with google geocoding
        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setName(EXAMPLE_LOCATION_STRING);
        testGeocodingService.registerAddress(EXAMPLE_LOCATION_STRING, expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(EXAMPLE_LOCATION_STRING)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameOnly_lookupWithoutResult() {

        final Address expectedAddress = Address.builder()
                .name("Sunken city Atlantis")
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_locationStringOnly_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setName(null);
        testGeocodingService.registerAddress(EXAMPLE_LOCATION_STRING, expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString(EXAMPLE_LOCATION_STRING)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_locationStringOnly_lookupWithoutResult() {

        final Address expectedAddress = Address.builder()
                .name("Sunken city Atlantis")
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString(expectedAddress.getName())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressNoGps_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setVerified(true);
        testGeocodingService.registerAddress(createAddressLookupString(expectedAddress), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressNoGps_lookupWithoutResult() {

        final Address expectedAddress = Address.builder()
                .name("Really not existing")
                .street("Really not existing")
                .zip("19991")
                .city("Really not existing")
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName("Really not existing")
                .addressStreet("Really not existing")
                .addressZip("19991")
                .addressCity("Really not existing")
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_gpsOnly_locationNearStreetAddress() {

        final Address expectedAddress = Address.builder()
                .street("Königsberger Straße 3")
                .zip("67659")
                .city("Kaiserslautern")
                .gpsLocation(new GPSLocation(49.449156, 7.739729))
                .verified(false)
                .build();
        testGeocodingService.registerGpsCoordinates(expectedAddress.getGpsLocation(), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_gpsOnly_locationTooFarFromStreetAddress() {

        //Google Maps returns "Königsberger Str. 3" at 49.44990090, 7.73875240
        final Address expectedAddress = Address.builder()
                .street(null)
                .zip("67659")
                .city("Kaiserslautern")
                .gpsLocation(new GPSLocation(49.451111, 7.735445))
                .verified(false)
                .build();
        //this is what Google Maps returns
        testGeocodingService.registerGpsCoordinates(expectedAddress.getGpsLocation(), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_gpsOnly_locationWithUnknownRoad() {

        //Google Maps returns Unnamed Road
        final Address expectedAddress = Address.builder()
                .street(null)
                .zip("67697")
                .city("Otterberg")
                .gpsLocation(new GPSLocation(49.534192, 7.786251))
                .verified(false)
                .build();
        testGeocodingService.registerGpsCoordinates(expectedAddress.getGpsLocation(), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_gpsOnly_locationNotResolvingToAddress() {

        //Google Maps returns no data
        final Address expectedAddress = Address.builder()
                .street(null)
                .zip(null)
                .city(null)
                .gpsLocation(new GPSLocation(49.874172, -34.404397))
                .verified(false)
                .build();
        testGeocodingService.registerGpsCoordinates(expectedAddress.getGpsLocation(), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameAndGps_locationFoundByGps() {

        final Address expectedAddress = Address.builder()
                .name("Bei Müllers")
                .street("Königsberger Straße 3")
                .zip("67659")
                .city("Kaiserslautern")
                .gpsLocation(new GPSLocation(49.449156, 7.739729))
                .verified(false)
                .build();
        testGeocodingService.registerGpsCoordinates(expectedAddress.getGpsLocation(), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    /**
     * This case looks up an address with street but no street number (also when run
     * with {@link AddressServiceTest#RUN_WITH_GOOGLE_GEOCODING}=true). It is expected that the street is contained
     * in the address without number.
     */
    @Test
    public void findOrCreateWithLocationDefinition_nameAndGps_partialLocationFoundByGps() {

        final GPSLocation gpsLocation = new GPSLocation(49.654670, 7.771685);
        final String name = "Somewhere on K11";
        final Address expectedAddress = Address.builder()
                .name(name)
                .street("K11")
                .zip("67808")
                .city("Ransweiler")
                .gpsLocation(gpsLocation)
                .verified(false)
                .build();

        testGeocodingService.registerGpsCoordinates(gpsLocation, expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(name)
                .gpsLocation(gpsLocation)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameAndGps_lookupWithoutResultFromGps() {

        final Address expectedAddress = Address.builder()
                .name(EXAMPLE_LOCATION_STRING) //this string should not be looked up!
                .gpsLocation(new GPSLocation(49.874172, -34.404397))
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_locationStringAndGps_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setName(null);
        expectedAddress.setVerified(false);
        testGeocodingService.registerAddress(EXAMPLE_LOCATION_STRING, expectedAddress);

        final GPSLocation slightlyDifferentLocation = expectedAddress.getGpsLocation();
        slightlyDifferentLocation.setLatitude(slightlyDifferentLocation.getLatitude() + 0.125);
        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString(EXAMPLE_LOCATION_STRING)
                .gpsLocation(slightlyDifferentLocation)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    /**
     * Tests the case that geo coding returns a result without street. (Also tests that DD-4760 has been fixed when
     * run with {@link AddressServiceTest#RUN_WITH_GOOGLE_GEOCODING}=true))
     */
    @Test
    public void findOrCreateWithLocationDefinition_locationStringAndGps_lookupWithPartialResult() {

        final String lookupString = "Talstraße 35-33, Sasbachwalden";
        final Address expectedAddress = Address.builder()
                .name(null)
                .street(null)
                .zip("77887")
                .city("Sasbachwalden")
                .gpsLocation(new GPSLocation(48.6177086, 8.127878100000002))
                .verified(false)
                .build();
        expectedAddress.setName(null);
        testGeocodingService.registerAddress(lookupString, expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString(lookupString)
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_locationStringAndGps_lookupWithoutResult() {

        final Address expectedAddress = Address.builder()
                .name("Some point somewhere")
                .gpsLocation(new GPSLocation(48.1111, 7.7778))
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString(expectedAddress.getName())
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameAndlocationStringAndGps_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setVerified(false);
        testGeocodingService.registerAddress(EXAMPLE_LOCATION_STRING, expectedAddress);

        final GPSLocation slightlyDifferentLocation = expectedAddress.getGpsLocation();
        slightlyDifferentLocation.setLatitude(slightlyDifferentLocation.getLatitude() + 0.125);
        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .locationLookupString(EXAMPLE_LOCATION_STRING)
                .gpsLocation(slightlyDifferentLocation)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_nameAndlocationStringAndGps_lookupWithoutResult() {

        final String name = "Gold";
        final String locationString = "Look behind the rainbow";
        final Address expectedAddress = Address.builder()
                .name(name + ", " + locationString)
                .gpsLocation(new GPSLocation(48.1111, 7.7778))
                .verified(false)
                .build();

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(name)
                .locationLookupString(locationString)
                .gpsLocation(expectedAddress.getGpsLocation())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressAndGps_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setVerified(false);
        testGeocodingService.registerAddress(createAddressLookupString(expectedAddress), expectedAddress);

        final GPSLocation slightlyDifferentLocation = expectedAddress.getGpsLocation();
        slightlyDifferentLocation.setLatitude(slightlyDifferentLocation.getLatitude() + 0.125);
        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .locationLookupString("does not matter")
                .gpsLocation(slightlyDifferentLocation)
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressAndGps_lookupWithoutResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setVerified(false);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .locationLookupString("does not matter")
                .gpsLocation(expectedAddress.getGpsLocation())
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), address.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressAndGpsNoName_lookupWithResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setName(null);
        expectedAddress.setVerified(false);
        testGeocodingService.registerAddress(createAddressLookupString(expectedAddress), expectedAddress);

        final GPSLocation slightlyDifferentLocation = expectedAddress.getGpsLocation();
        slightlyDifferentLocation.setLatitude(slightlyDifferentLocation.getLatitude() + 0.125);
        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString("does not matter")
                .gpsLocation(slightlyDifferentLocation)
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressAndGpsNoName_lookupWithoutResult() {

        final Address expectedAddress = completeExampleAddress();
        expectedAddress.setName(null);
        expectedAddress.setVerified(false);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationLookupString("does not matter")
                .gpsLocation(expectedAddress.getGpsLocation())
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertAddressContentsAreEqual(expectedAddress, addressFromRepo);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, false)
                .getId(), address.getId(), "With the second service call, the same address should be reused");
        assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(locationDefinition);
    }

    @Test
    public void findOrCreateWithLocationDefinition_fullAddressNoGps_reuseExistingVerifiedAddressFromDB() {

        Address expectedAddress = completeExampleAddress();
        expectedAddress.setVerified(true);
        expectedAddress = addressRepository.save(expectedAddress);
        testGeocodingService.registerAddress(createAddressLookupString(expectedAddress), expectedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(expectedAddress.getName())
                .addressStreet(expectedAddress.getStreet())
                .addressZip(expectedAddress.getZip())
                .addressCity(expectedAddress.getCity())
                .build();
        final Address foundAddress = addressService.findOrCreateAddress(locationDefinition, false);
        assertEquals(expectedAddress.getId(), foundAddress.getId(),
                "address should be the already existing address from the repo");
        assertAddressContentsAreEqual(expectedAddress, foundAddress);
        assertEquals(addressService.findOrCreateAddress(locationDefinition, true)
                        .getId(), foundAddress.getId(),
                "With the second service call, the same address should be reused and fullAddressRequired " +
                        "should not cause an error");
    }

    @Test
    public void findOrCreateWithLocationDefinition_resolvedVerifiedAddressUpdatesExistingAddressInDb() {

        //using EXAMPLE_LOCATION_STRING as name to make the tests also work with google geocoding
        final Address addressToResolveFromLocationString = completeExampleAddress();
        addressToResolveFromLocationString.setName(EXAMPLE_LOCATION_STRING);
        testGeocodingService.registerAddress(EXAMPLE_LOCATION_STRING, addressToResolveFromLocationString);

        Address existingUnverifiedAddress = completeExampleAddress();
        existingUnverifiedAddress.setName(EXAMPLE_LOCATION_STRING);
        existingUnverifiedAddress.setVerified(false);
        existingUnverifiedAddress = addressRepository.save(existingUnverifiedAddress);

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(EXAMPLE_LOCATION_STRING)
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        //this in particular checks that the returned address is verified
        assertAddressContentsAreEqual(addressToResolveFromLocationString, address);
        assertEquals(existingUnverifiedAddress.getId(), address.getId(), "existing address should be found");
        final Address addressFromRepo = addressRepository.findById(address.getId()).get();
        assertTrue(addressFromRepo.isVerified(), "verified status of address in repo should be updated");
    }

    @Test
    public void findOrCreateWithLocationDefinition_verifiedAddressInDbUpdatedUnresolvedAddress() {

        final Address verifiedAddressInDb = addressRepository.save(completeExampleAddress());

        final IAddressService.LocationDefinition locationDefinition = IAddressService.LocationDefinition.builder()
                .locationName(verifiedAddressInDb.getName())
                .locationLookupString("does not matter")
                .gpsLocation(verifiedAddressInDb.getGpsLocation())
                .addressStreet(verifiedAddressInDb.getStreet())
                .addressZip(verifiedAddressInDb.getZip())
                .addressCity(verifiedAddressInDb.getCity())
                .build();
        final Address address = addressService.findOrCreateAddress(locationDefinition, false);
        assertAddressContentsAreEqual(verifiedAddressInDb, address);
        assertEquals(verifiedAddressInDb.getId(), address.getId(), "address from repo should be used");
        assertTrue(address.isVerified(), "address should have verified=true");
    }

    private void assertAddressInvalidExceptionIsThrownWhenFullAddressIsRequired(
            IAddressService.LocationDefinition locationDefinition) {
        try {
            addressService.findOrCreateAddress(locationDefinition, true);
            fail("AddressInvalidException expected");
        } catch (AddressInvalidException e) {
            log.info("Expected error message: {}", e.getMessage());
            assertTrue(e.getMessage().startsWith("Full address required"));
        }
    }

    private static String createAddressLookupString(Address address) {
        return address.getStreet() + ", " + address.getZip() + " " + address.getCity();
    }

}
