/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxReadyForAllocationResponse;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxScannedReadyForAllocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPoolingStationBoxReadyForAllocationRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;

public class PoolingStationBoxScannedAllocationTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    private TransportAssignment transportAssignment;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception {

        receiver = th.personVgAdmin;
        carrier = th.personPoolingStationOp;

        pickupAddress = ClientAddressDefinition.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .build();

        deliveryPoolingStation = th.poolingStation1;
        deliveryPoolingStation.setStationType(PoolingStationType.AUTOMATIC);
        deliveryPoolingStation.setIsSelfScanningStation(true);
        deliveryPoolingStation = th.poolingStationRepository
                .save(deliveryPoolingStation);

        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        // first unused poolingStationBox is selected, therefore we can be sure that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox1;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);

        TransportAlternative transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStation();

        // TransportAlternativeSelectRequest
        transportAssignment = transportAlternativeSelectRequestFirstStep(
                    transportAlternativeToIntermediatePS.getId(), carrier, deliveryPoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrier, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryPoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode
    }

    @Test
    public void onPoolingStationBoxScannedReadyForAllocationRequest() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = th.poolingStation1.getId();
        ClientPoolingStationBoxScannedReadyForAllocationRequest request = new ClientPoolingStationBoxScannedReadyForAllocationRequest(poolingStationId, deliveryId);
        mockMvc.perform(post("/logistics/event/poolingStationBoxScannedReadyForAllocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void onPoolingStationBoxScannedReadyForAllocationRequestWrongPoolingStationId() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = th.poolingStation2.getId();
        ClientPoolingStationBoxScannedReadyForAllocationRequest request = new ClientPoolingStationBoxScannedReadyForAllocationRequest(poolingStationId, deliveryId);

        mockMvc.perform(post("/logistics/event/poolingStationBoxScannedReadyForAllocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_NOT_FOUND));
    }

    @Test
    public void onPoolingStationBoxScannedReadyForAllocationRequestDeliveryIdInvalid() throws Exception {

        String deliveryId = "invalidId";
        String poolingStationId = th.poolingStation1.getId();
        ClientPoolingStationBoxScannedReadyForAllocationRequest request = new ClientPoolingStationBoxScannedReadyForAllocationRequest(poolingStationId, deliveryId);

        mockMvc.perform(post("/logistics/event/poolingStationBoxScannedReadyForAllocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.DELIVERY_NOT_FOUND));
    }

    @Test
    public void onPoolingStationBoxScannedReadyForAllocationRequestPoolingStationIdInvalid() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = "invalidId";
        ClientPoolingStationBoxScannedReadyForAllocationRequest request = new ClientPoolingStationBoxScannedReadyForAllocationRequest(poolingStationId, deliveryId);

        mockMvc.perform(post("/logistics/event/poolingStationBoxScannedReadyForAllocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.POOLING_STATION_NOT_FOUND));
    }

    @Test
    public void onTransportPoolingStationBoxReadyForAllocationRequest() throws Exception {

        String poolingStationId = th.poolingStation1.getId();
        String deliveryTrackingCode = delivery.getTrackingCode();

        ClientTransportPoolingStationBoxReadyForAllocationRequest request =
                    new ClientTransportPoolingStationBoxReadyForAllocationRequest(poolingStationId, deliveryTrackingCode, transportAssignment.getId());

        MvcResult response = mockMvc.perform(
                        post("/logistics/event/transportPoolingStationBoxReadyForAllocationRequest")
                                .headers(authHeadersFor(carrier))
                                .contentType(contentType)
                                .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryTrackingCode").value(delivery.getTrackingCode()))
                .andReturn();

        ClientPoolingStationBoxReadyForAllocationResponse poolingStationBoxResponse = toObject(response.getResponse(),
                ClientPoolingStationBoxReadyForAllocationResponse.class);

        final PoolingStationBox boxToCompare =
                th.poolingStationBoxRepository.findById(poolingStationBoxResponse.getPoolingStationBoxId()).get();
        assertEquals(poolingStationId, boxToCompare.getPoolingStation().getId(),
                "Pooling station of pooling station box");
        assertEquals(boxToCompare.getName(), poolingStationBoxResponse.getPoolingStationBoxName(),
                "PoolingStationBoxName");
        assertEquals(boxToCompare.getConfiguration().getBoxType(), poolingStationBoxResponse.getPoolingStationBoxType(),
                "PoolingStationBoxTye");
        assertEquals(boxToCompare.getConfiguration().getTimeoutForDoorOpen(),
                poolingStationBoxResponse.getTimeoutForDoorOpen(), "PoolingStationBoxTimeoutForDoorOpen");
    }

    @Test
    public void onTransportPoolingStationBoxReadyForAllocationRequestUnauthorized() throws Exception {
        String poolingStationId = th.poolingStation1.getId();
        String deliveryTrackingCode = delivery.getTrackingCode();

        ClientTransportPoolingStationBoxReadyForAllocationRequest request =
                new ClientTransportPoolingStationBoxReadyForAllocationRequest(poolingStationId, deliveryTrackingCode, transportAssignment.getId());

        assertOAuth2(post("/logistics/event/transportPoolingStationBoxReadyForAllocationRequest")
                .content(json(request))
                .contentType(contentType));
    }

}
