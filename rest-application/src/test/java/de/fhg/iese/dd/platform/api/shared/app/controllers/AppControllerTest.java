/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AppControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private AppVariant appVariantWithApiKey1;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        final App apiKeyApp = th.appRepository.save(App.builder()
                .name("api key app")
                .appIdentifier("apikey.app")
                .build());

        appVariantWithApiKey1 = th.appVariantRepository.save(AppVariant.builder()
                .name("api key app variant 1")
                .appVariantIdentifier("apikey.app.variant.1")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .oauthClients(Collections.singleton(th.oauthClientRepository.save(OauthClient.builder()
                        .oauthClientIdentifier("apikey-oauthclient-identifier")
                        .build())))
                .build());
        th.mapGeoAreaToAppVariant(appVariantWithApiKey1, th.geoAreaTenant1, th.tenant1);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAppVariant_old() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        App app = th.app1Variant1.getApp();

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}", appVariant.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(appVariant.getId()))
                .andExpect(jsonPath("$.appVariantIdentifier").value(appVariant.getAppVariantIdentifier()))
                .andExpect(jsonPath("$.name").value(appVariant.getName()))
                .andExpect(jsonPath("$.logo.id").value(appVariant.getLogo().getId()))
                .andExpect(jsonPath("$.app.id").value(app.getId()))
                .andExpect(jsonPath("$.app.name").value(app.getName()))
                .andExpect(jsonPath("$.app.appIdentifier").value(app.getAppIdentifier()))
                .andExpect(jsonPath("$.app.logo.id").value(app.getLogo().getId()));
    }

    @Test
    public void getAppVariant_old_InvalidId() throws Exception {

        String invalidId = "de.fhg.iese.dd.wunderbar";

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}", invalidId))
                .andExpect(isException(invalidId, ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAppVariant() throws Exception {

        final AppVariant appVariant = appVariantWithApiKey1;
        final App app = appVariant.getApp();

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/appVariant/"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$.id").value(appVariant.getId()))
                        .andExpect(jsonPath("$.appVariantIdentifier").value(
                                appVariant.getAppVariantIdentifier()))
                        .andExpect(jsonPath("$.name").value(appVariant.getName()))
                        .andExpect(jsonPath("$.app.id").value(app.getId()))
                        .andExpect(jsonPath("$.app.name").value(app.getName()))
                        .andExpect(jsonPath("$.app.appIdentifier").value(app.getAppIdentifier())));
    }

    @Test
    public void getAppVariant_Deleted() throws Exception {

        final AppVariant appVariant = appVariantWithApiKey1;
        final App app = appVariant.getApp();

        appVariant.setDeleted(true);
        th.appVariantRepository.saveAndFlush(appVariant);

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/appVariant/"),
                r -> r.andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND)));
    }

    @Test
    public void getAppVariant_InvalidId() throws Exception {

        mockMvc.perform(get("/app/appVariant/")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier("my own identifier")
                        .build().withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAllTenantsForAppVariant() throws Exception {

        AppVariant appVariantSingle = th.app1Variant1;
        AppVariant appVariantMultiple = th.app2Variant1;
        List<Tenant> expectedTenantsSingle = Collections.singletonList(th.tenant1);
        List<Tenant> expectedTenantsMultiple = Arrays.asList(th.tenant1, th.tenant2);

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableCommunity",
                appVariantSingle.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTenantsSingle.size())))
                .andExpect(jsonPath("$[0].id").value(expectedTenantsSingle.get(0).getId()));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableCommunity",
                appVariantMultiple.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTenantsMultiple.size())))
                .andExpect(jsonPath("$[0].id").value(expectedTenantsMultiple.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedTenantsMultiple.get(1).getId()));
    }

    @Test
    public void getAllTenantsForAppVariant_InvalidId() throws Exception {

        //id is not the app variant identifier
        String invalidId = th.app1Variant1.getId();

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableCommunity", invalidId))
                .andExpect(isException(invalidId, ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAvailableTenantsForAppVariant() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final List<Tenant> expectedAvailableTenants = Collections.singletonList(th.tenant1);

        mockMvc.perform(get("/app/appVariant/availableTenants")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(appVariant.getApiKey1())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedAvailableTenants.size())))
                .andExpect(jsonPath("$[0].id").value(expectedAvailableTenants.get(0).getId()));
    }

    @Test
    public void getAvailableTenantsForAppVariant_WrongOrEmptyAPIKey() throws Exception {

        assertApiKeyRequired(get("/app/appVariant/availableTenants"));
    }

}
