/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class DeliveryToPoolingStationTest extends BaseLogisticsEventTest {

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    /*
     * Sequence of:
     * Purchase Order Received
     * Purchase Order Ready For Transport
     * Transport Alternative Selected
     * Transport Pickup
     * Delivery to manual pooling station
     * Receiver pickup from manual pooling station
     *
     */
    @Test
    public void deliveryToPoolingStation() throws Exception {

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);

        TransportAlternative transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment = transportAlternativeSelectRequestFirstStep(transportAlternativeToIntermediatePS.getId(), carrierFirstStep, deliveryPoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        Delivery delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrierFirstStep, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryPoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(deliveryPoolingStation.getId(), delivery.getTrackingCode(),
                transportAssignment.getId(), carrierFirstStep, deliveryPoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        transportDeliveredPoolingStationRequestFinal(
            deliveryPoolingStation, // poolingStation
            delivery.getTrackingCode(),// deliveryTrackingCode
            transportAssignment.getId(),// transportAssignmentId
            delivery.getId(),// deliveryId
            carrierFirstStep,// carrier
            pickupAddress);// pickupAddressTransport

        // ReceiverPickupPoolingStationBoxReadyForDeallocationRequest
        receiverPickupPoolingStationBoxReadyForDeallocationRequest(delivery.getId(), deliveryPoolingStation.getId(), deliveryPoolingStationBox);

        // ReceiverPickupReceivedPoolingStationRequest
        receiverPickupReceivedPoolingStationRequest(deliveryPoolingStation.getId(), delivery.getTrackingCode(),
                delivery.getId(), deliveryPoolingStationAddress);
    }

}
