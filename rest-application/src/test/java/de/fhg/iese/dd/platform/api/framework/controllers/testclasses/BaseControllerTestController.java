/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers.testclasses;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.shared.security.services.TestActionA;
import de.fhg.iese.dd.platform.business.shared.security.services.TestActionB;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestPersonVerificationStatusRestrictedFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.Getter;

@Getter
@RestController
@RequestMapping("/test")
public class BaseControllerTestController extends BaseController {

    private Person person;
    private AppVariant appVariant;
    private OauthClient oauthClient;

    @GetMapping("public")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    void testMethodPublic() {
        person = null;
        oauthClient = null;
        appVariant = getAppVariantNotNull();
    }

    @GetMapping("test")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    void testMethod() {
        person = getCurrentPersonNotNull();
        appVariant = getAppVariantNotNull();
        oauthClient = getOauthClientNotNull();
    }

    @GetMapping("testRestricted")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    void testMethodRestricted() {
        person = getCurrentPersonNotNull();
        appVariant = getAppVariantNotNull();
        oauthClient = getOauthClientNotNull();
        checkVerificationStatusRestriction(TestPersonVerificationStatusRestrictedFeature.class, person, appVariant);
    }

    @GetMapping("testMethodRoleRequiredActionDocumentation")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {TestActionA.class})
    void testMethodRoleRequiredActionDocumentation() {
        person = getCurrentPersonNotNull();
        appVariant = getAppVariantNotNull();
        oauthClient = getOauthClientNotNull();
    }

    @GetMapping("testMethodRoleOptionalActionDocumentation")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            optionalActions = {TestActionB.class})
    void testMethodRoleOptionalActionDocumentation() {
        person = getCurrentPersonNotNull();
        appVariant = getAppVariantNotNull();
        oauthClient = getOauthClientNotNull();
    }

    public void resetCapturedAuthorization() {
        person = null;
        appVariant = null;
        oauthClient = null;
    }

}
