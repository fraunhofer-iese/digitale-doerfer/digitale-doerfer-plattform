/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.legal.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

public class LegalServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private LegalTextService legalTextService;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createLegalEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();

    }

    @Test
    public void concurrentRequestHandlingDoesNotFail(){
        //this tests "simulates" concurrent requests, by calling an internal method two times that does not check if the acceptance already exists

        LegalTextAcceptance acceptance1 =
                legalTextService.createNewLegalTextAcceptance(th.personIeseAdmin, th.legalTextPlatformRequired,
                        th.nextTimeStamp());
        //this method has no check if the legal text acceptance already exists, so it needs to handle the unique constraint violation
        // in this case, the existing one needs to be returned
        LegalTextAcceptance acceptance2 =
                legalTextService.createNewLegalTextAcceptance(th.personIeseAdmin, th.legalTextPlatformRequired,
                        th.nextTimeStamp());
        assertEquals(acceptance1, acceptance2);
    }

}
