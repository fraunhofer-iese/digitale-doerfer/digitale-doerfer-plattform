/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Dominik Schnier, Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.PageBean;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroupExtended;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.GrapevineClientModelMapper;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalGroupAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GroupAdmin;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GroupAdminUiControllerTest extends BaseServiceTest {

    private static final Sort DEFAULT_MEMBERS_SORT = Sort.by("memberLastName");

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private GrapevineClientModelMapper grapevineModelMapper;

    private Person personGlobalGroupAdmin;
    private Person personGroupAdminTenant1;
    private Person personGroupAdminTenant2;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createGroupsAndGroupPostsWithComments();

        personGlobalGroupAdmin = th.createPersonWithDefaultAddress("personGlobalGroupAdmin", th.tenant1,
                "4cdd9123-2b47-421c-adf1-207f7c183529");
        th.assignRoleToPerson(personGlobalGroupAdmin, GlobalGroupAdmin.class, null);

        personGroupAdminTenant1 = th.createPersonWithDefaultAddress("personGroupAdmin", th.tenant1,
                "9b2f9a1b-39e6-4536-b0ca-8775909f0c6b");
        th.assignRoleToPerson(personGroupAdminTenant1, GroupAdmin.class, th.tenant1.getId());

        personGroupAdminTenant2 = th.createPersonWithDefaultAddress("personGroupAdminOtherTenant", th.tenant2,
                "aebd649f-144c-4dd9-829f-0e8183639820");
        th.assignRoleToPerson(personGroupAdminTenant2, GroupAdmin.class, th.tenant2.getId());

        Gossip gossipWithComment = postRepository.save(Gossip.builder()
                .created(th.nextTimeStamp())
                .tenant(th.tenant1)
                .group(th.groupSchachvereinAAA)
                .creator(th.personBenTenant1Dorf2)
                .geoAreas(Set.of(th.personBenTenant1Dorf2.getHomeArea()))
                .text("Endlich geschafft hier zu posten!")
                .build());

        commentRepository.save(Comment.builder()
                .post(gossipWithComment)
                .creator(th.personBenTenant1Dorf2)
                .deleted(false)
                .text("Mein erster Kommentar")
                .build()
                .withCreated(th.nextTimeStamp()));
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getGroups_WithoutTenantIdGlobalGroupAdmin() throws Exception {

        final List<Group> expectedGroups = th.groupRepository.findAll()
                .stream()
                .filter(Group::isNotDeleted)
                .sorted(Comparator.comparing(Group::getName))
                .collect(Collectors.toList());

        assertEquals(9, expectedGroups.size());
        int pageSize = 2;
        final Group firstGroup = expectedGroups.get(0);

        final MvcResult mvcResult = mockMvc.perform(buildGetGroupsRequest(personGlobalGroupAdmin, null, 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(firstGroup.getId()))
                .andExpect(jsonPath("$.content[0].name").value(firstGroup.getName()))
                .andExpect(jsonPath("$.content[0].shortName").value(firstGroup.getShortName()))
                .andExpect(jsonPath("$.content[0].logo.id").value(firstGroup.getLogo().getId()))
                .andExpect(jsonPath("$.content[0].visibility").value(firstGroup.getVisibility().name()))
                .andExpect(jsonPath("$.content[0].contentVisibility").value(firstGroup.getContentVisibility().name()))
                .andExpect(jsonPath("$.content[0].accessibility").value(firstGroup.getAccessibility().name()))
                .andExpect(jsonPath("$.content[0].memberCount").value(firstGroup.getMemberCount()))
                .andExpect(jsonPath("$.content[0].deleted").value(firstGroup.isDeleted()))
                .andExpect(jsonPath("$.content[0].tenantId").value(firstGroup.getTenant().getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedGroups.get(1).getId()))
                .andReturn();

        checkGeoAreas(expectedGroups, toPageOfType(mvcResult, ClientGroupExtended.class));

        // Next page
        mockMvc.perform(buildGetGroupsRequest(personGlobalGroupAdmin, null, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedGroups.get(2).getId()));
    }

    @Test
    public void getGroups_WithoutTenantIdGroupAdmin() throws Exception {

        final List<Group> expectedGroups = th.groupRepository.findAll()
                .stream()
                .filter(g -> g.getTenant().equals(th.tenant1) && g.isNotDeleted())
                .sorted(Comparator.comparing(Group::getName))
                .collect(Collectors.toList());

        assertEquals(7, expectedGroups.size());
        int pageSize = 2;
        final Group firstGroup = expectedGroups.get(0);

        final MvcResult mvcResult = mockMvc.perform(buildGetGroupsRequest(personGroupAdminTenant1, null, 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(firstGroup.getId()))
                .andExpect(jsonPath("$.content[0].name").value(firstGroup.getName()))
                .andExpect(jsonPath("$.content[0].shortName").value(firstGroup.getShortName()))
                .andExpect(jsonPath("$.content[0].logo.id").value(firstGroup.getLogo().getId()))
                .andExpect(jsonPath("$.content[0].visibility").value(firstGroup.getVisibility().name()))
                .andExpect(jsonPath("$.content[0].contentVisibility").value(firstGroup.getContentVisibility().name()))
                .andExpect(jsonPath("$.content[0].accessibility").value(firstGroup.getAccessibility().name()))
                .andExpect(jsonPath("$.content[0].memberCount").value(firstGroup.getMemberCount()))
                .andExpect(jsonPath("$.content[0].deleted").value(firstGroup.isDeleted()))
                .andExpect(jsonPath("$.content[0].tenantId").value(firstGroup.getTenant().getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedGroups.get(1).getId()))
                .andReturn();

        checkGeoAreas(expectedGroups, toPageOfType(mvcResult, ClientGroupExtended.class));

        // Next page
        mockMvc.perform(buildGetGroupsRequest(personGroupAdminTenant1, null, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedGroups.get(2).getId()));
    }

    @Test
    public void getGroups_WithTenantIdAllRoles() throws Exception {

        final Tenant tenant = th.tenant2;
        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant2);

        final List<Group> expectedGroups = th.groupRepository.findAll()
                .stream()
                .filter(g -> g.getTenant().equals(tenant) && g.isNotDeleted())
                .sorted(Comparator.comparing(Group::getName))
                .collect(Collectors.toList());

        assertEquals(1, expectedGroups.size());
        final Group firstGroup = expectedGroups.get(0);
        int pageSize = 2;

        for (final Person caller : personsWithSuitableRoles) {

            final MvcResult mvcResult = mockMvc.perform(buildGetGroupsRequest(caller, tenant.getId(), 0, pageSize))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(hasCorrectPageData(0, pageSize, expectedGroups.size()))
                    .andExpect(jsonPath("$.content[0].id").value(firstGroup.getId()))
                    .andExpect(jsonPath("$.content[0].name").value(firstGroup.getName()))
                    .andExpect(jsonPath("$.content[0].shortName").value(firstGroup.getShortName()))
                    .andExpect(jsonPath("$.content[0].logo").value(nullValue()))
                    .andExpect(jsonPath("$.content[0].visibility").value(firstGroup.getVisibility().name()))
                    .andExpect(
                            jsonPath("$.content[0].contentVisibility").value(firstGroup.getContentVisibility().name()))
                    .andExpect(jsonPath("$.content[0].accessibility").value(firstGroup.getAccessibility().name()))
                    .andExpect(jsonPath("$.content[0].memberCount").value(firstGroup.getMemberCount()))
                    .andExpect(jsonPath("$.content[0].deleted").value(firstGroup.isDeleted()))
                    .andExpect(jsonPath("$.content[0].tenantId").value(firstGroup.getTenant().getId()))
                    .andReturn();

            checkGeoAreas(expectedGroups, toPageOfType(mvcResult, ClientGroupExtended.class));
        }
    }

    @Test
    public void getGroups_GroupWithoutGeoAreas() throws Exception {

        Group groupWithoutGeoArea = th.groupRepository.save(Group.builder()
                .visibility(GroupVisibility.ANYONE)
                .contentVisibility(GroupContentVisibility.ANYONE)
                .accessibility(GroupAccessibility.PUBLIC)
                .deleted(false)
                .tenant(th.tenant1)
                .name("AAA Gruppe der Heimatlosen")
                .shortName("GH")
                .build()
                .withCreated(th.nextTimeStamp()));

        final List<Group> expectedGroups = th.groupRepository.findAll()
                .stream()
                .filter(Group::isNotDeleted)
                .sorted(Comparator.comparing(Group::getName))
                .collect(Collectors.toList());

        assertEquals(10, expectedGroups.size());
        int pageSize = 2;
        final Group firstGroup = expectedGroups.get(0);
        assertThat(firstGroup).isEqualTo(groupWithoutGeoArea);

        final MvcResult mvcResult = mockMvc.perform(buildGetGroupsRequest(personGlobalGroupAdmin, null, 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(0, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(firstGroup.getId()))
                .andExpect(jsonPath("$.content[0].name").value(firstGroup.getName()))
                .andExpect(jsonPath("$.content[0].shortName").value(firstGroup.getShortName()))
                .andExpect(jsonPath("$.content[0].visibility").value(firstGroup.getVisibility().name()))
                .andExpect(jsonPath("$.content[0].contentVisibility").value(firstGroup.getContentVisibility().name()))
                .andExpect(jsonPath("$.content[0].accessibility").value(firstGroup.getAccessibility().name()))
                .andExpect(jsonPath("$.content[0].memberCount").value(firstGroup.getMemberCount()))
                .andExpect(jsonPath("$.content[0].deleted").value(firstGroup.isDeleted()))
                .andExpect(jsonPath("$.content[0].tenantId").value(firstGroup.getTenant().getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedGroups.get(1).getId()))
                .andReturn();

        checkGeoAreas(expectedGroups, toPageOfType(mvcResult, ClientGroupExtended.class));

        // Next page
        mockMvc.perform(buildGetGroupsRequest(personGlobalGroupAdmin, null, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroups.size()))
                .andExpect(jsonPath("$.content[0].id").value(expectedGroups.get(2).getId()));
    }

    @Test
    public void getGroups_NotAuthorized() throws Exception {

        final Person caller = th.personRegularAnna;
        final String tenantId = th.tenant1.getId();
        final int page = 0;
        final int count = 1;

        assertOAuth2(buildGetGroupsRequest(caller, null, page, count));

        assertOAuth2(buildGetGroupsRequest(caller, tenantId, page, count));

        mockMvc.perform(buildGetGroupsRequest(caller, null, page, count))
                .andExpect(isNotAuthorized());

        mockMvc.perform(buildGetGroupsRequest(caller, tenantId, page, count))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGroups_PageOutOfRange() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final List<Group> expectedGroups = th.groupRepository.findAll()
                .stream()
                .filter(Group::isNotDeleted)
                .sorted(Comparator.comparing(Group::getName))
                .toList();

        assertEquals(9, expectedGroups.size());
        int pageSize = 9;

        mockMvc.perform(buildGetGroupsRequest(caller, null, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroups.size()));
    }

    @Test
    public void getGroups_PageParametersInvalid() throws Exception {

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {
            final String tenantId = th.tenant1.getId();

            // Maximum number of groups smaller than 1
            mockMvc.perform(buildGetGroupsRequest(caller, tenantId, 0, 0))
                    .andExpect(content().contentType(contentType))
                    .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

            // Page number smaller than 0
            mockMvc.perform(buildGetGroupsRequest(caller, tenantId, -1, 10))
                    .andExpect(content().contentType(contentType))
                    .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

            // Maximum number of groups smaller than 1 and page number smaller than 0
            mockMvc.perform(buildGetGroupsRequest(caller, tenantId, -1, 0))
                    .andExpect(content().contentType(contentType))
                    .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
        }
    }

    @Test
    public void getGroups_PermissionDenied() throws Exception {

        final String tenantId = th.tenant1.getId();
        final int page = 0;
        final int count = 1;

        // Caller does not have required role
        mockMvc.perform(buildGetGroupsRequest(th.personRegularAnna, null, page, count))
                .andExpect(isNotAuthorized());

        mockMvc.perform(buildGetGroupsRequest(th.personRegularAnna, tenantId, page, count))
                .andExpect(isNotAuthorized());

        // Caller does have required role but the tenantId belongs to different tenant
        mockMvc.perform(buildGetGroupsRequest(personGroupAdminTenant2, tenantId, page, count))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGroups_TenantNotFound() throws Exception {

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);

        for (final Person caller : personsWithSuitableRoles) {

            mockMvc.perform(buildGetGroupsRequest(caller, "invalidTenantId", 0, 1))
                    .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
        }
    }

    private MockHttpServletRequestBuilder buildGetGroupsRequest(Person caller, String tenantId, int page, int count) {
        MockHttpServletRequestBuilder builder = get("/adminui/group")
                .headers(authHeadersFor(caller))
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count));
        if (tenantId != null) {
            builder = builder.param("tenantId", tenantId);
        }
        return builder;
    }

    @Test
    public void getGroupById_allRoles() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Long lastPost = postRepository.findNewestPostCreatedTime(group);
        final Long lastCommentOfPost = postRepository.findNewestCommentCreatedTime(group);

        final List<Person> personsWithRolesToTest = Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);

        for (Person caller : personsWithRolesToTest) {
            log.info("Calling as {}", caller);
            mockMvc.perform(get("/adminui/group/{groupId}", group.getId())
                    .headers(authHeadersFor(caller)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.name").value(group.getName()))
                    .andExpect(jsonPath("$.shortName").value(group.getShortName()))
                    .andExpect(jsonPath("$.visibility").value(group.getVisibility().toString()))
                    .andExpect(jsonPath("$.contentVisibility").value(group.getContentVisibility().toString()))
                    .andExpect(jsonPath("$.accessibility").value(group.getAccessibility().toString()))
                    .andExpect(jsonPath("$.memberCount").value(group.getMemberCount()))
                    .andExpect(jsonPath("$.deleted").value(group.isDeleted()))
                    .andExpect(jsonPath("$.tenantId").value(group.getTenant().getId()))
                    .andExpect(jsonPath("$.groupMembershipAdmins").value(hasSize(1)))
                    .andExpect(jsonPath("$.postCount").value(2))
                    .andExpect(jsonPath("$.lastPost").value(lastPost))
                    .andExpect(jsonPath("$.lastCommentOfPost").value(lastCommentOfPost))
                    .andExpect(jsonPath("$.created").value(group.getCreated()));
        }
    }

    @Test
    public void getGroupById_deletedAllRoles() throws Exception {

        final Group deletedGroup = th.groupDeleted;
        final Group notDeletedGroup = th.groupSchachvereinAAA;
        final Long lastPost = postRepository.findNewestPostCreatedTime(deletedGroup);
        final Long lastCommentOfPost = postRepository.findNewestCommentCreatedTime(deletedGroup);

        final List<Person> personsWithRolesToTest = Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);

        for (Person caller : personsWithRolesToTest) {
            log.info("Calling as {}", caller);
            mockMvc.perform(get("/adminui/group/{groupId}", deletedGroup.getId())
                    .headers(authHeadersFor(caller)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.name").value(deletedGroup.getName()))
                    .andExpect(jsonPath("$.shortName").value(deletedGroup.getShortName()))
                    .andExpect(jsonPath("$.visibility").value(deletedGroup.getVisibility().toString()))
                    .andExpect(jsonPath("$.contentVisibility").value(deletedGroup.getContentVisibility().toString()))
                    .andExpect(jsonPath("$.accessibility").value(deletedGroup.getAccessibility().toString()))
                    .andExpect(jsonPath("$.memberCount").value(deletedGroup.getMemberCount()))
                    .andExpect(jsonPath("$.deleted").value(deletedGroup.isDeleted()))
                    .andExpect(jsonPath("$.tenantId").value(deletedGroup.getTenant().getId()))
                    .andExpect(jsonPath("$.groupMembershipAdmins").value(hasSize(0)))
                    .andExpect(jsonPath("$.postCount").value(1))
                    .andExpect(jsonPath("$.lastPost").value(lastPost))
                    .andExpect(jsonPath("$.lastCommentOfPost").value(lastCommentOfPost))
                    .andExpect(jsonPath("$.created").value(deletedGroup.getCreated()));

            // Make sure deleted group is not in regular list of tenant
            mockMvc.perform(buildGetGroupsRequest(caller, th.tenant1.getId(), 0, 10))
                    .andExpect(jsonPath("$.content[*].id", not(hasItem(deletedGroup.getId()))))
                    .andExpect(jsonPath("$.content[*].id", hasItem(notDeletedGroup.getId())));
        }
    }

    @Test
    public void getGroupById_NoPostInGroup() throws Exception {

        final Group group = th.groupDorfgeschichteSSA;
        final Person caller = personGroupAdminTenant1;

        log.info("Calling as {}", caller);
        mockMvc.perform(get("/adminui/group/{groupId}", group.getId())
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name").value(group.getName()))
                .andExpect(jsonPath("$.shortName").value(group.getShortName()))
                .andExpect(jsonPath("$.visibility").value(group.getVisibility().toString()))
                .andExpect(jsonPath("$.contentVisibility").value(group.getContentVisibility().toString()))
                .andExpect(jsonPath("$.accessibility").value(group.getAccessibility().toString()))
                .andExpect(jsonPath("$.memberCount").value(group.getMemberCount()))
                .andExpect(jsonPath("$.deleted").value(group.isDeleted()))
                .andExpect(jsonPath("$.tenantId").value(group.getTenant().getId()))
                .andExpect(jsonPath("$.groupMembershipAdmins").value(hasSize(0)))
                .andExpect(jsonPath("$.postCount").value(0))
                .andExpect(jsonPath("$.lastPost").value(nullValue()))
                .andExpect(jsonPath("$.lastCommentOfPost").value(nullValue()))
                .andExpect(jsonPath("$.created").value(group.getCreated()));
    }

    @Test
    public void getGroupById_GroupWithoutGeoAreas() throws Exception {

        Group groupWithoutGeoArea = th.groupRepository.save(Group.builder()
                .visibility(GroupVisibility.ANYONE)
                .contentVisibility(GroupContentVisibility.ANYONE)
                .accessibility(GroupAccessibility.PUBLIC)
                .deleted(false)
                .tenant(th.tenant1)
                .name("AAA Gruppe der Heimatlosen")
                .shortName("GH")
                .build()
                .withCreated(th.nextTimeStamp()));
        final Person caller = personGroupAdminTenant1;

        log.info("Calling as {}", caller);
        mockMvc.perform(get("/adminui/group/{groupId}", groupWithoutGeoArea.getId())
                        .headers(authHeadersFor(caller)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name").value(groupWithoutGeoArea.getName()))
                .andExpect(jsonPath("$.shortName").value(groupWithoutGeoArea.getShortName()))
                .andExpect(jsonPath("$.visibility").value(groupWithoutGeoArea.getVisibility().toString()))
                .andExpect(jsonPath("$.contentVisibility").value(groupWithoutGeoArea.getContentVisibility().toString()))
                .andExpect(jsonPath("$.accessibility").value(groupWithoutGeoArea.getAccessibility().toString()))
                .andExpect(jsonPath("$.memberCount").value(groupWithoutGeoArea.getMemberCount()))
                .andExpect(jsonPath("$.deleted").value(groupWithoutGeoArea.isDeleted()))
                .andExpect(jsonPath("$.tenantId").value(groupWithoutGeoArea.getTenant().getId()))
                .andExpect(jsonPath("$.groupMembershipAdmins").value(hasSize(0)))
                .andExpect(jsonPath("$.postCount").value(0))
                .andExpect(jsonPath("$.lastPost").value(nullValue()))
                .andExpect(jsonPath("$.lastCommentOfPost").value(nullValue()))
                .andExpect(jsonPath("$.created").value(groupWithoutGeoArea.getCreated()));
    }

    @Test
    public void getGroupById_groupNotFoundAllRoles() throws Exception {

        final List<Person> personsWithRolesToTest = Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);

        for (Person caller : personsWithRolesToTest) {
            log.info("Calling as {}", caller);
            mockMvc.perform(get("/adminui/group/{groupId}", "no-group")
                            .headers(authHeadersFor(caller)))
                    .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
        }
    }

    @Test
    public void getGroupById_Unauthorized() throws Exception {

        final Group group = th.groupSchachvereinAAA;

        assertOAuth2(get("/adminui/group/{groupId}", group.getId()));

        mockMvc.perform(get("/adminui/group/{groupId}", group.getId())
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGroupById_wrongTenant() throws Exception {

        final Group group = th.groupMuellAAP;
        final Person caller = personGroupAdminTenant1;

        log.info("Calling as {}", caller);
        mockMvc.perform(get("/adminui/group/{groupId}", group.getId())
                .headers(authHeadersFor(caller)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGroupMembers_AllRoles() throws Exception {

        final List<Person> personsWithSuitableRoles =
                Arrays.asList(personGlobalGroupAdmin, personGroupAdminTenant1);
        final String groupId = th.groupWaffenfreundeAMA.getId();
        final List<GroupMembership> expectedGroupMemberships = th.groupMembershipRepository.findAll().stream()
                .filter(gm -> gm.getGroup().getId().equals(groupId))
                .sorted(Comparator.comparing(gm -> gm.getMember().getLastName()))
                .collect(Collectors.toList());

        assertEquals(3, expectedGroupMemberships.size());

        for (Person caller : personsWithSuitableRoles) {

            mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, 0, 10))
                    .andExpect(content().contentType(contentType))
                    .andExpect(status().isOk())
                    .andExpect(jsonEquals(new PageImpl<>(
                            expectedGroupMemberships.stream()
                                    .map(gm -> grapevineModelMapper.toClientGroupMemberExtended(gm))
                                    .collect(Collectors.toList()),
                            PageRequest.of(0, 10, DEFAULT_MEMBERS_SORT),
                            3)));
        }
    }

    @Test
    public void getGroupMembers_Paged() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupWaffenfreundeAMA.getId();
        final List<GroupMembership> expectedGroupMemberships = th.groupMembershipRepository.findAll().stream()
                .filter(gm -> gm.getGroup().getId().equals(groupId))
                .sorted(Comparator.comparing(gm -> gm.getMember().getLastName()))
                .collect(Collectors.toList());

        assertEquals(3, expectedGroupMemberships.size());

        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, 0, 2))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedGroupMemberships.subList(0, 2).stream()
                                .map(gm -> grapevineModelMapper.toClientGroupMemberExtended(gm))
                                .collect(Collectors.toList()),
                        PageRequest.of(0, 2, DEFAULT_MEMBERS_SORT),
                        3)));

        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, 1, 2))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new PageImpl<>(
                        expectedGroupMemberships.subList(2, 3).stream()
                                .map(gm -> grapevineModelMapper.toClientGroupMemberExtended(gm))
                                .collect(Collectors.toList()),
                        PageRequest.of(1, 2, DEFAULT_MEMBERS_SORT),
                        3)));
    }

    @Test
    public void getGroupMembers_NotAuthorized() throws Exception {

        final Person caller = th.personRegularAnna;
        final String groupId = th.groupWaffenfreundeAMA.getId();
        final int page = 0;
        final int count = 1;

        assertOAuth2(buildGetGroupMembersRequest(null, groupId, page, count));

        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, page, count))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGroupMembers_OutOfRange() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupWaffenfreundeAMA.getId();
        final List<Person> expectedGroupMembers =
                Arrays.asList(th.personChloeTenant1Dorf1, th.personAnjaTenant1Dorf1, th.personBenTenant1Dorf2);
        int pageSize = 3;
        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroupMembers.size()));
    }

    @Test
    public void getGroupMembers_PageParametersInvalid() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final String groupId = th.groupWaffenfreundeAMA.getId();

        // Maximum number of groups smaller than 1
        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, 0, 0))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Page number smaller than 0
        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, -1, 10))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // Maximum number of groups smaller than 1 and page number smaller than 0
        mockMvc.perform(buildGetGroupMembersRequest(caller, groupId, -1, 0))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

    }

    @Test
    public void getGroupMembers_PermissionDenied() throws Exception {

        final String groupId = th.groupWaffenfreundeAMA.getId();
        final int page = 0;
        final int count = 1;

        // Caller does not have required role
        mockMvc.perform(
                buildGetGroupMembersRequest(th.personRegularKarl, groupId, page, count))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // Caller does have required role but the group has a different tenant
        mockMvc.perform(
                buildGetGroupMembersRequest(personGroupAdminTenant2, groupId, page, count))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void getGroupMembers_GroupNotFound() throws Exception {

        final Person caller = personGlobalGroupAdmin;
        final int page = 0;
        final int count = 1;

        mockMvc.perform(
                buildGetGroupMembersRequest(caller, BaseTestHelper.INVALID_UUID, page, count))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    private MockHttpServletRequestBuilder buildGetGroupMembersRequest(Person caller, String groupId, int page,
            int count) {
        MockHttpServletRequestBuilder builder = get("/adminui/group/{groupId}/members", groupId)
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count));
        if (caller != null) {
            builder = builder.headers(authHeadersFor(caller));
        }
        return builder;
    }

    private void checkGeoAreas(List<Group> expectedGroups, PageBean<ClientGroupExtended> result) {
        final Map<String, IGroupService.GroupGeoAreaIds> geoAreaIdsByGroups = th.findGeoAreaIdsByGroups(expectedGroups);
        for (ClientGroupExtended clientGroupExtended : result.getContent()) {
            final IGroupService.GroupGeoAreaIds groupGeoAreaIds = geoAreaIdsByGroups.get(clientGroupExtended.getId());
            if (groupGeoAreaIds == null) {
                assertThat(clientGroupExtended.getIncludedGeoAreas()).isEmpty();
                assertThat(clientGroupExtended.getExcludedGeoAreas()).isEmpty();
            } else {
                assertThat(clientGroupExtended.getIncludedGeoAreas()).map(ClientBaseEntity::getId)
                        .containsAll(groupGeoAreaIds.getIncludedGeoAreaIds());
                assertThat(clientGroupExtended.getExcludedGeoAreas()).map(ClientBaseEntity::getId)
                        .containsAll(groupGeoAreaIds.getExcludedGeoAreaIds());
            }
        }
    }

}
