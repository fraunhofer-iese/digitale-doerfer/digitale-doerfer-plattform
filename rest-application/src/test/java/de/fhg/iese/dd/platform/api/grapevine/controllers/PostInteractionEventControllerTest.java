/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Johannes Eveslage, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostsViewEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikePostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikePostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostView;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostInteraction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PostInteractionEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void likePostRequest_verifyingPushMessage() throws Exception {

        Gossip gossip = th.gossip4;
        Person liker = th.personSusanneChristmann;

        assertEquals(0, postRepository.findById(gossip.getId()).get().getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(gossip, liker));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(liker,
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPost().getGossip().getId()).isEqualTo(gossip.getId());
    }

    @Test
    public void likePostRequest_withDoubleLike() throws Exception {

        Gossip gossip = th.gossip4;
        Person liker = th.personSusanneChristmann;

        assertEquals(0, postRepository.findById(gossip.getId()).get().getLikeCount());
        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(gossip, liker));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        final Post postAfterLike = postRepository.findById(gossip.getId()).get();
        assertEquals(1, postAfterLike.getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postAfterLike,
                liker));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_ALREADY_LIKED));

        final Post postAfterSecondLike = postRepository.findById(gossip.getId()).get();
        assertEquals(1, postAfterSecondLike.getLikeCount());
    }

    @Test
    public void likePostRequest_relike() throws Exception {

        Gossip gossip = th.gossip4;
        Person liker = th.personSusanneChristmann;

        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .person(liker)
                .post(gossip)
                .liked(false)
                .build());

        gossip.setLikeCount(0);
        gossip = postRepository.saveAndFlush(gossip);

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(gossip, liker));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(liker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        final Post postFromRepoAfterLike = postRepository.findById(gossip.getId()).get();
        assertEquals(1, postFromRepoAfterLike.getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postFromRepoAfterLike, liker));
    }

    @Test
    public void likePostRequest_InvisiblePost() throws Exception {

        Gossip gossip = th.gossip7;
        gossip.setHiddenForOtherHomeAreas(true);
        gossip = postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void likePostRequest_wrongAppVariant() throws Exception {

        Gossip gossip = th.gossip4;
        gossip.setHiddenForOtherHomeAreas(true);
        gossip = postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant3Tenant3))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void likePostRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/likePostRequest")
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(th.gossip1withImages.getId()))),
                th.appVariant1Tenant1,
                th.personSusanneChristmann);
    }

    @Test
    public void likePostRequest_InvalidParameters() throws Exception {

        //invalid id
        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest("unknown-post"))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //deleted post
        Post post = th.gossip1withImages;
        post.setDeleted(true);
        th.postRepository.save(post);

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(post.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //missing event
        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann))
                        .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void unlikePostRequest_verifyingPushMessage() throws Exception {

        Gossip gossip = th.gossip4;
        Person unliker = th.personSusanneChristmann;

        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip)
                .person(unliker)
                .liked(true)
                .build());

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(0))
                .andExpect(jsonPath("$.post.gossip.liked").value(false));

        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(unliker,
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPost().getGossip().getId()).isEqualTo(gossip.getId());
    }

    @Test
    public void unlikePostRequest_withDoubleUnlike() throws Exception {

        Person unliker = th.personSusanneChristmann;
        Gossip gossip = th.gossip4;

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                        .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientLikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        assertEquals(1, postRepository.findById(gossip.getId()).get().getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(gossip, unliker));

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(gossip.getId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(0))
                .andExpect(jsonPath("$.post.gossip.liked").value(false));

        final Post postAfterUnlike = postRepository.findById(gossip.getId()).get();
        assertEquals(0, postAfterUnlike.getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postAfterUnlike,
                unliker));

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(unliker, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_ALREADY_UNLIKED));

        final Post postAfterSecondUnlike = postRepository.findById(gossip.getId()).get();
        assertEquals(0, postAfterSecondUnlike.getLikeCount());
    }

    @Test
    public void unlikePostRequest_likePostNotFound() throws Exception {

        final Post postToBeUnliked = th.gossip4;

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postToBeUnliked,
                th.personSusanneChristmann));

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(postToBeUnliked.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_LIKED));
    }

    @Test
    public void unlikePostRequest_wrongTenant() throws Exception {

        Gossip gossip = th.gossip7;
        gossip.setHiddenForOtherHomeAreas(true);
        gossip = postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant2Tenant1And2))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void unlikePostRequest_wrongAppVariant() throws Exception {

        Gossip gossip = th.gossip4;
        gossip.setHiddenForOtherHomeAreas(true);
        gossip = postRepository.saveAndFlush(gossip);

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant3Tenant3))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(gossip.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void unlikePostRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/unlikePostRequest")
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(th.gossip1withImages.getId()))),
                th.appVariant1Tenant1,
                th.personSusanneChristmann);
    }

    @Test
    public void unlikePostRequest_InvalidParameters() throws Exception {

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest("unknown-post"))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //deleted post
        Post post = th.gossip1withImages;
        post.setDeleted(true);
        th.postRepository.save(post);

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(new ClientUnlikePostRequest(post.getId()))))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
        //missing event
        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann))
                        .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void postsViewEvent() throws Exception {

        final Person person = th.personSusanneChristmann;
        final long timeA = timeServiceMock.currentTimeMillisUTC() - 10000;
        final long timeB = timeA - 42;

        Gossip gossip1 = th.gossip4;
        ClientPostView clientPostView1 = new ClientPostView(gossip1.getId(), timeA, timeA);
        ClientPostView clientPostView1_sameId = new ClientPostView(gossip1.getId(), timeB, timeB);

        Gossip gossip2 = th.gossip5;
        ClientPostView clientPostView2 = new ClientPostView(gossip2.getId(), timeA, timeA);

        Gossip gossip3 = th.gossip6;
        ClientPostView clientPostView3 = new ClientPostView(gossip3.getId(), timeA, null);

        Gossip gossip4 = th.gossip7;
        ClientPostView clientPostView4 = new ClientPostView(gossip4.getId(), null, timeA);

        //this should be ignored and no post interaction created
        Gossip gossip5 = th.gossip8With9Images;
        ClientPostView clientPostView5 = new ClientPostView(gossip4.getId(), null, null);

        //this should be ignored without exception
        ClientPostView clientPostViewInvalid = new ClientPostView("not-existing-id", timeA, timeA);

        // already created postInteraction for gossip1, but without view times
        // For all others new postInteractions are created
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(gossip1)
                .person(person)
                .build());

        mockMvc.perform(post("/grapevine/event/postsViewEvent")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(new ClientPostsViewEvent(
                                List.of(clientPostView1, clientPostView1_sameId, clientPostView2, clientPostView3,
                                        clientPostView4, clientPostView5, clientPostViewInvalid)))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        Optional<PostInteraction> updatedPostInteraction1 =
                postInteractionRepository.findByPostAndPerson(gossip1, person);
        assertThat(updatedPostInteraction1).isPresent();
        assertThat(updatedPostInteraction1.get().getViewedOverviewTime()).isEqualTo(timeA);
        assertThat(updatedPostInteraction1.get().getViewedDetailTime()).isEqualTo(timeA);

        Optional<PostInteraction> updatedPostInteraction2 =
                postInteractionRepository.findByPostAndPerson(gossip2, person);
        assertThat(updatedPostInteraction2).isPresent();
        assertThat(updatedPostInteraction2.get().getViewedOverviewTime()).isEqualTo(timeA);
        assertThat(updatedPostInteraction2.get().getViewedDetailTime()).isEqualTo(timeA);

        Optional<PostInteraction> updatedPostInteraction3 =
                postInteractionRepository.findByPostAndPerson(gossip3, person);
        assertThat(updatedPostInteraction3).isPresent();
        assertThat(updatedPostInteraction3.get().getViewedOverviewTime()).isNull();
        assertThat(updatedPostInteraction3.get().getViewedDetailTime()).isEqualTo(timeA);

        Optional<PostInteraction> updatedPostInteraction4 =
                postInteractionRepository.findByPostAndPerson(gossip4, person);
        assertThat(updatedPostInteraction4).isPresent();
        assertThat(updatedPostInteraction4.get().getViewedOverviewTime()).isEqualTo(timeA);
        assertThat(updatedPostInteraction4.get().getViewedDetailTime()).isNull();

        Optional<PostInteraction> updatedPostInteraction5 =
                postInteractionRepository.findByPostAndPerson(gossip5, person);
        assertThat(updatedPostInteraction5).isEmpty();
    }

    @Test
    public void postsViewEvent_Unauthorized() throws Exception {
        assertOAuth2(post("/grapevine/event/postsViewEvent")
                .contentType(contentType)
                .content(json(new ClientPostsViewEvent(List.of(new ClientPostView(th.gossip4.getId(), 1L, 1L))))));
    }

    @Test
    public void postsViewEvent_InvalidParameters() throws Exception {
        // missing event
        mockMvc.perform(post("/grapevine/event/postsViewEvent")
                        .headers(authHeadersFor(th.personSusanneChristmann))
                        .contentType(contentType))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void postsViewEvent_FutureTimestamps() throws Exception {

        Person person = th.personSusanneChristmann;
        // the current time is set to a fixed time
        long currentTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(currentTime);
        long futureTime = currentTime + TimeUnit.DAYS.toMillis(10);
        Gossip post = th.gossip4;
        ClientPostView clientPostView = new ClientPostView(post.getId(), futureTime, futureTime);
        mockMvc.perform(post("/grapevine/event/postsViewEvent")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(new ClientPostsViewEvent(List.of(clientPostView)))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        Optional<PostInteraction> updatedPostInteraction = postInteractionRepository.findByPostAndPerson(post, person);
        assertThat(updatedPostInteraction).isPresent();
        assertThat(updatedPostInteraction.get().getViewedOverviewTime()).isEqualTo(currentTime);
        assertThat(updatedPostInteraction.get().getViewedDetailTime()).isEqualTo(currentTime);
    }

    @Test
    public void postsViewEvent_PastTimestamps() throws Exception {

        Person person = th.personSusanneChristmann;
        // the current time is set to a fixed time
        long currentTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(currentTime);
        long timeSevenDaysBefore = currentTime - TimeUnit.DAYS.toMillis(7) - 1;
        long timeA = currentTime - TimeUnit.HOURS.toMillis(5);
        long timeB = currentTime - TimeUnit.HOURS.toMillis(6);

        Gossip post = th.gossip5;
        //the invalid times are ignored, the valid ones are used
        ClientPostView clientPostView1 = new ClientPostView(post.getId(), timeSevenDaysBefore, timeA);
        ClientPostView clientPostView2 = new ClientPostView(post.getId(), timeB, timeSevenDaysBefore);
        mockMvc.perform(post("/grapevine/event/postsViewEvent")
                        .headers(authHeadersFor(person))
                        .contentType(contentType)
                        .content(json(new ClientPostsViewEvent(List.of(clientPostView1, clientPostView2)))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        Optional<PostInteraction> updatedPostInteraction = postInteractionRepository.findByPostAndPerson(post, person);
        assertThat(updatedPostInteraction).isPresent();
        assertThat(updatedPostInteraction.get().getViewedOverviewTime()).isEqualTo(timeA);
        assertThat(updatedPostInteraction.get().getViewedDetailTime()).isEqualTo(timeB);
    }

}
