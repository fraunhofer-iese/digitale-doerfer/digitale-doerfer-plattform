/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.logistics.LogisticsConstants;
import de.fhg.iese.dd.platform.datamanagement.logistics.feature.LogisticsShopFeature;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxConfiguration;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationBoxType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxAllocationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxConfigurationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationBoxRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.PoolingStationRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAssignmentStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportConfirmationRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.MotivationConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.enums.AppAccountType;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.AccountEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.PersonAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.motivation.repos.TenantAccountRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHoursEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import de.fhg.iese.dd.platform.datamanagement.shopping.repos.PurchaseOrderRepository;

public abstract class BaseLogisticsTestHelper extends BaseSharedTestHelper {

    @Autowired
    public DeliveryRepository deliveryRepository;

    @Autowired
    public DeliveryStatusRecordRepository deliveryStatusRecordRepository;

    @Autowired
    public PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    public TransportAssignmentRepository transportAssignmentRepository;

    @Autowired
    public TransportAssignmentStatusRecordRepository transportAssignmentStatusRecordRepository;

    @Autowired
    public TransportAlternativeRepository transportAlternativeRepository;

    @Autowired
    public TransportAlternativeBundleRepository transportAlternativeBundleRepository;

    @Autowired
    public TransportAlternativeBundleStatusRecordRepository transportAlternativeBundleStatusRecordRepository;

    @Autowired
    public TransportConfirmationRepository transportConfirmationRepository;

    @Autowired
    public PoolingStationRepository poolingStationRepository;

    @Autowired
    public PoolingStationBoxRepository poolingStationBoxRepository;

    @Autowired
    public PoolingStationBoxAllocationRepository poolingStationBoxAllocationRepository;

    @Autowired
    public PoolingStationBoxConfigurationRepository poolingStationBoxConfigurationRepository;

    @Autowired
    protected AccountEntryRepository accountEntryRepository;

    @Autowired
    protected PersonAccountRepository personAccountRepository;

    @Autowired
    protected TenantAccountRepository tenantAccountRepository;

    @Autowired
    public ChatMessageRepository chatMessageRepository;

    @Autowired
    public ChatRepository chatRepository;

    @Autowired
    public IChatService chatService;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private PushCategoryRepository pushCategoryRepository;

    @Autowired
    private FeatureConfigRepository featureConfigRepository;

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IAppService appService;

    public PoolingStation poolingStation1;
    public PoolingStation poolingStation2;
    public PoolingStation poolingStation3;
    public PoolingStation poolingStation4;
    public List<PoolingStation> poolingStationList;

    public PoolingStationBox poolingStationBox1;
    public PoolingStationBox poolingStationBox2;
    public PoolingStationBox poolingStationBox3;
    public PoolingStationBox poolingStationBox4;
    public PoolingStationBox poolingStationBox5;
    public PoolingStationBox poolingStationBox6;
    public List<PoolingStationBox> poolingStationBoxList;

    public PoolingStationBoxAllocation poolingStationBoxAllocation1;
    public PoolingStationBoxAllocation poolingStationBoxAllocation2;

    public PoolingStationBoxConfiguration configuration1;

    protected List<PersonAccount> personAccountList;
    protected PersonAccount accountShopOwner;
    protected PersonAccount accountPersonVgAdmin;
    protected PersonAccount accountPersonIeseAdmin;
    protected PersonAccount accountPersonPoolingStationOp;
    protected PersonAccount accountPersonRegularAnna;
    protected PersonAccount accountPersonRegularKarl;

    protected List<TenantAccount> tenantAccountList;
    protected TenantAccount accountCommunity1;
    protected TenantAccount accountCommunity2;

    public App appLieferbar;
    public AppVariant appVariantLieferBar;
    private OauthClient oAuthClientLieferBar;

    public App appBestellBar;
    public AppVariant appVariantBestellBar1;
    public AppVariant appVariantBestellBar2;

    @Override
    protected Set<String> achievementRulesToCreate() {
        return new HashSet<>(Arrays.asList(
                "score.participants.created.1",
                "score.participants.registered.1",
                "shopping.score.purchaseOrder.created.1",
                "score.account.sent.1",
                "score.account.received.1",
                "logistics.score.transport.done.1"
        ));
    }

    public void createPoolingStations() {

        createPoolingStation1();

        createPoolingStation2();

        createPoolingStation3();

        createPoolingStation4();

        poolingStationList = Arrays.asList(
            poolingStation1,
            poolingStation2,
            poolingStation3,
            poolingStation4);

        configuration1 = PoolingStationBoxConfiguration.builder()
                .firesDoorOpenEvent(true)
                .firesDoorClosedEvent(true)
                .doorOpensInstantly(true)
                .timeoutForDoorOpen(0)
                .inactive(false)
                .boxType(PoolingStationBoxType.AUTOMATIC)
                .build();
        configuration1 = poolingStationBoxConfigurationRepository.save(configuration1);

        poolingStationBox1 = new PoolingStationBox("Fach 1", 1, configuration1, poolingStation1);
        poolingStationBox1.setCreated(nextTimeStamp());
        poolingStationBox1 = poolingStationBoxRepository.save(poolingStationBox1);

        poolingStationBox2 = new PoolingStationBox("Fach 2", 2, configuration1, poolingStation1);
        poolingStationBox2.setCreated(nextTimeStamp());
        poolingStationBox2 = poolingStationBoxRepository.save(poolingStationBox2);

        poolingStationBox3 = new PoolingStationBox("Fach 1", 1, configuration1, poolingStation2);
        poolingStationBox3.setCreated(nextTimeStamp());
        poolingStationBox3 = poolingStationBoxRepository.save(poolingStationBox3);

        poolingStationBox4 = new PoolingStationBox("Fach 2", 1, configuration1, poolingStation2);
        poolingStationBox4.setCreated(nextTimeStamp());
        poolingStationBox4 = poolingStationBoxRepository.save(poolingStationBox4);

        poolingStationBox5 = new PoolingStationBox("Fach 1", 1, configuration1, poolingStation3);
        poolingStationBox5.setCreated(nextTimeStamp());
        poolingStationBox5 = poolingStationBoxRepository.save(poolingStationBox5);

        poolingStationBox6 = new PoolingStationBox("Fach 1", 1, configuration1, poolingStation4);
        poolingStationBox6.setCreated(nextTimeStamp());
        poolingStationBox6 = poolingStationBoxRepository.save(poolingStationBox6);

        poolingStationBoxList = Arrays.asList(
                poolingStationBox1,
                poolingStationBox2,
                poolingStationBox3,
                poolingStationBox4,
                poolingStationBox5,
                poolingStationBox6);
    }

    public void createScoreEntities(){

        accountCommunity1 = new TenantAccount();
        accountCommunity1.setOwner(tenant1);
        accountCommunity1.setAppAccountType(AppAccountType.LOGISTICS);
        accountCommunity1.setBalance(100);
        accountCommunity1.setCreated(nextTimeStamp());
        accountCommunity1 = tenantAccountRepository.save(accountCommunity1);

        accountCommunity2 = new TenantAccount();
        accountCommunity2.setOwner(tenant2);
        accountCommunity2.setAppAccountType(AppAccountType.LOGISTICS);
        accountCommunity2.setBalance(100);
        accountCommunity2.setCreated(nextTimeStamp());
        accountCommunity2 = tenantAccountRepository.save(accountCommunity2);

        tenantAccountList = Arrays.asList(accountCommunity1, accountCommunity2);

        accountShopOwner = new PersonAccount();
        accountShopOwner.setOwner(personShopOwner);
        accountShopOwner.setBalance(60);
        accountShopOwner.setCreated(nextTimeStamp());
        accountShopOwner = personAccountRepository.save(accountShopOwner);

        accountPersonVgAdmin = new PersonAccount();
        accountPersonVgAdmin.setOwner(personVgAdmin);
        accountPersonVgAdmin.setBalance(60);
        accountPersonVgAdmin.setCreated(nextTimeStamp());
        accountPersonVgAdmin = personAccountRepository.save(accountPersonVgAdmin);

        accountPersonIeseAdmin = new PersonAccount();
        accountPersonIeseAdmin.setOwner(personIeseAdmin);
        accountPersonIeseAdmin.setBalance(50);
        accountPersonIeseAdmin.setCreated(nextTimeStamp());
        accountPersonIeseAdmin = personAccountRepository.save(accountPersonIeseAdmin);

        accountPersonPoolingStationOp = new PersonAccount();
        accountPersonPoolingStationOp.setOwner(personPoolingStationOp);
        accountPersonPoolingStationOp.setBalance(10);
        accountPersonPoolingStationOp.setCreated(nextTimeStamp());
        accountPersonPoolingStationOp = personAccountRepository.save(accountPersonPoolingStationOp);

        accountPersonRegularAnna = new PersonAccount();
        accountPersonRegularAnna.setOwner(personRegularAnna);
        accountPersonRegularAnna.setBalance(0);
        accountPersonRegularAnna.setCreated(nextTimeStamp());
        accountPersonRegularAnna = personAccountRepository.save(accountPersonRegularAnna);

        accountPersonRegularKarl = new PersonAccount();
        accountPersonRegularKarl.setOwner(personRegularKarl);
        accountPersonRegularKarl.setBalance(0);
        accountPersonRegularKarl.setCreated(nextTimeStamp());
        accountPersonRegularKarl = personAccountRepository.save(accountPersonRegularKarl);

        personAccountList = Arrays.asList(accountShopOwner, accountPersonVgAdmin, accountPersonIeseAdmin, accountPersonPoolingStationOp, accountPersonRegularAnna, accountPersonRegularKarl);
    }

    private void createPoolingStation1() {
        OpeningHours openingHoursPoolingStation1 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation1 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station Bahnhof")
                                .street("Zollamtstraße 8")
                                .zip(
                                        "67663")
                                .city("Kaiserslautern").gpsLocation(
                                new GPSLocation(49.435450, 7.769099)).build()),
                "D-Station Bahnhof", "42",
                mediaItemRepository.save(createMediaItem("imagePoolingStation1")),
                tenant2, openingHoursPoolingStation1, PoolingStationType.AUTOMATIC, true, true, false, true);
        poolingStation1.setCreated(nextTimeStamp());
        poolingStation1 = poolingStationRepository.save(poolingStation1);
    }

    private void createPoolingStation2() {

        OpeningHours openingHoursPoolingStation2 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation2 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station TU KL")
                                .street("Gottlieb-Daimler-Straße 49")
                                .zip("67663")
                                .city("Kaiserslautern")
                                .gpsLocation(new GPSLocation(49.423920, 7.755011))
                                .build()),
                "D-Station TU KL", "42",
                null,
                tenant1, openingHoursPoolingStation2, PoolingStationType.MANUAL, false, true, false, true);
        poolingStation2.setCreated(nextTimeStamp());
        poolingStation2 = poolingStationRepository.save(poolingStation2);
    }

    private void createPoolingStation3() {

        OpeningHours openingHoursPoolingStation3 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation3 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station Lidl")
                                .street("Königstraße 115")
                                .zip("67655")
                                .city("Kaiserslautern")
                                .gpsLocation(new GPSLocation(49.437537, 7.756299))
                                .build()),
                "D-Station Lidl", "42",
                null,
                tenant3, openingHoursPoolingStation3, PoolingStationType.AUTOMATIC, false, true, false, true);
        poolingStation3.setCreated(nextTimeStamp());
        poolingStation3 = poolingStationRepository.save(poolingStation3);
    }

    private void createPoolingStation4() {
        OpeningHours openingHoursPoolingStation4 = createOpeningHours(
                new OpeningHoursEntry(DayOfWeek.MONDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.TUESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.WEDNESDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.THURSDAY).withFromHours(9).withToHours(16),
                new OpeningHoursEntry(DayOfWeek.FRIDAY).withFromHours(9).withToHours(16));

        poolingStation4 = new PoolingStation(
                addressRepository.save(
                        Address.builder()
                                .name("D-Station ATU")
                                .street("Pariser Straße 196")
                                .zip("67663")
                                .city("Kaiserslautern")
                                .gpsLocation(new GPSLocation(49.441573, 7.743977))
                                .build()),
                "D-Station ATU", "42",
                null,
                tenant1, openingHoursPoolingStation4, PoolingStationType.MANUAL, false, false, true, true);
        poolingStation4.setCreated(nextTimeStamp());
        poolingStation4 = poolingStationRepository.save(poolingStation4);
    }

    public void createPushEntities() {

        appLieferbar = appRepository.save(App.builder()
                .id(LogisticsConstants.LIEFERBAR_APP_ID)
                .name("LieferBar")
                .appIdentifier("dd-logistics")
                .build());

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Neue Lieferungen verfügbar (direkt)")
                .description("Neue Lieferungen verfügbar, direkte Benachrichtigung")
                .defaultLoudPushEnabled(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID));

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Lieferstatus Empfänger")
                .description("Änderungen des Lieferstatus für Pakete, die ich erhalte")
                .defaultLoudPushEnabled(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID));

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Lieferstatus Lieferant")
                .description("Änderungen des Lieferstatus für Pakete, die ich transportiere")
                .defaultLoudPushEnabled(true)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID));

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Abholbenachrichtigung")
                .description("Benachrichtigung über abzuholende Pakete")
                .defaultLoudPushEnabled(true)
                .configurable(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID));

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Digitaler und Achievements")
                .description("Digitaler und Achievementes erhalten")
                .subject(MotivationConstants.PUSH_CATEGORY_SUBJECT)
                .defaultLoudPushEnabled(true)
                .configurable(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID));

        pushCategoryRepository.save(PushCategory.builder()
                .app(appLieferbar)
                .name("Chatnachrichten")
                .description("Neue Chatnachrichten von Empfänger und Besteller")
                .defaultLoudPushEnabled(true)
                .configurable(false)
                .build()
                .withId(LogisticsConstants.LIEFERBAR_PUSH_CATEGORY_CHAT_ID));

        appRepository.flush();
        pushCategoryRepository.flush();
    }

    public void createLieferBarAppVariant(){

        oAuthClientLieferBar = oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("oAuthClientLieferBar-identifier")
                .build());

        appVariantLieferBar = appVariantRepository.save(AppVariant.builder()
                .name("LieferBarAppVariant")
                .appVariantIdentifier("de.fhg.iese.dd.lieferbar")
                        .oauthClients(Collections.singleton(oAuthClientLieferBar))
                .app(appLieferbar)
                .updateUrlAndroid(UPDATE_URL_ANDROID)
                .updateUrlIOS(UPDATE_URL_IOS)
                .build());
        mapGeoAreaToAppVariant(appVariantLieferBar, geoAreaTenant1, tenant1);
        mapGeoAreaToAppVariant(appVariantLieferBar, geoAreaTenant2, tenant2);
        mapGeoAreaToAppVariant(appVariantLieferBar, geoAreaTenant3, tenant3);

        appVariantRepository.flush();
    }

    public void createLieferBarShopFeatures(){

        FeatureConfig featureConfig1 = FeatureConfig.builder()
                .featureClass(LogisticsShopFeature.class.getName())
                .enabled(true)
                .tenant(tenant1)
                .app(appLieferbar)
                .configValues("{\"shopUrl\": \"http://www.example.org/shop1/\"}")
                .build();
        featureConfig1.setCreated(timeService.currentTimeMillisUTC());
        featureConfigRepository.save(featureConfig1);

        FeatureConfig featureConfig2 = FeatureConfig.builder()
                .featureClass(LogisticsShopFeature.class.getName())
                .enabled(true)
                .tenant(tenant2)
                .app(appLieferbar)
                .configValues("{\"shopUrl\": \"http://www.example.org/shop2/\"}")
                .build();
        featureConfig2.setCreated(timeService.currentTimeMillisUTC());
        featureConfigRepository.save(featureConfig2);

        FeatureConfig featureConfig3 = FeatureConfig.builder()
                .featureClass(LogisticsShopFeature.class.getName())
                .enabled(true)
                .tenant(tenant3)
                .app(appLieferbar)
                .configValues("{\"shopUrl\": \"http://www.example.org/shop3/\"}")
                .build();
        featureConfig3.setCreated(timeService.currentTimeMillisUTC());
        featureConfigRepository.save(featureConfig3);

        featureConfigRepository.flush();
    }

    public void createBestellBarAppAndAppVariants() {

        appBestellBar = appRepository.save(App.builder()
                .id("0ed9eadc-4ab8-4e85-a2a2-72c4af215d0a") //ShoppingConstants.BESTELLBAR_APP_ID
                .name("BestellBar")
                .appIdentifier("dd-bestellbar")
                .build());

        appVariantBestellBar1 = appVariantRepository.save(AppVariant.builder()
                .name("BestellBarAppVariant1")
                .appVariantIdentifier("de.fhg.iese.dd.bestellbar.1")
                .app(appBestellBar)
                .apiKey1("de.fhg.iese.dd.bestellbar.1.apiKey1")
                .apiKey2("de.fhg.iese.dd.bestellbar.1.apiKey2")
                .build());

        appVariantBestellBar2 = appVariantRepository.save(AppVariant.builder()
                .name("BestellBarAppVariant2")
                .appVariantIdentifier("de.fhg.iese.dd.bestellbar.2")
                .app(appBestellBar)
                .apiKey1("de.fhg.iese.dd.bestellbar.2.apiKey1")
                .apiKey2("de.fhg.iese.dd.bestellbar.2.apiKey2")
                .build());

        appVariantRepository.flush();
    }

    public void createLieferBarGeoAreaSelections(){
        //personRegularAnna subscribed to geoAreaTenant1 (home) and geoAreaTenant2
        selectGeoAreasForAppVariant(appVariantLieferBar, personRegularAnna, geoAreaTenant1, geoAreaTenant2);

        //personRegularHorstiTenant3 subscribed to geoAreaTenant3 (home)
        selectGeoAreasForAppVariant(appVariantLieferBar, personRegularHorstiTenant3, geoAreaTenant3);

        //personVgAdmin subscribed to geoAreaTenant1 (home)
        selectGeoAreasForAppVariant(appVariantLieferBar, personVgAdmin, geoAreaTenant1);
    }

}
