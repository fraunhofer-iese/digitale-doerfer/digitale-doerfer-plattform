/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AppControllerAvailableGeoAreaTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private AppVariant appVariantNoGeoAreas;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        final App apiKeyApp = th.appRepository.save(App.builder()
                .name("api key app")
                .appIdentifier("apikey.app")
                .build());

        appVariantNoGeoAreas = th.appVariantRepository.save(AppVariant.builder()
                .name("app variant no tenant")
                .appVariantIdentifier("appvariant.no.tenant")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .oauthClients(Collections.singleton(th.oauthClientRepository.save(OauthClient.builder()
                        .oauthClientIdentifier("oauth-client-no-tenant")
                        .build())))
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAvailableGeoAreasForAppVariant() throws Exception {

        final List<ClientGeoArea> expectedResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult)));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                th.app1Variant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult));
    }

    @Test
    public void getAvailableGeoAreasForAppVariant_Exclusions() throws Exception {

        final AppVariant appVariant1 = th.app1Variant1;
        final AppVariant appVariant2 = th.app1Variant2;
        th.unmapAllGeoAreasFromAppVariant(appVariant1);
        th.unmapAllGeoAreasFromAppVariant(appVariant2);
        th.mapGeoAreaToAppVariant(appVariant1, th.geoAreaEisenberg, th.tenant1);
        th.excludeGeoAreaFromAppVariant(appVariant1, th.geoAreaDorf2InEisenberg, th.tenant1);
        th.mapGeoAreaToAppVariant(appVariant1, th.geoAreaKaiserslautern, th.tenant2);

        final List<ClientGeoArea> expectedResult1 = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant1,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult1)));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                appVariant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult1));

        //this is to test that the exclusion is relative to the tenant
        th.mapGeoAreaToAppVariant(appVariant2, th.geoAreaRheinlandPfalz, th.tenant1);
        th.excludeGeoAreaFromAppVariant(appVariant2, th.geoAreaEisenberg, th.tenant1);
        //this will be included since it is from a different tenant
        th.mapGeoAreaToAppVariant(appVariant2, th.geoAreaDorf1InEisenberg, th.tenant2);

        final List<ClientGeoArea> expectedResult2 = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                                th.toClientGeoArea(th.geoAreaMainz, 2, true))));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant2,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult2)));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                appVariant2.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult2));
    }

    @Test
    public void getAvailableGeoAreasForAppVariant_CacheRefresh() throws Exception {

        GeoArea changedGeoArea = th.geoAreaEisenberg;
        final List<ClientGeoArea> expectedOldResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(changedGeoArea, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));
        //ensure the cache is set
        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                th.app1Variant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedOldResult));
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedOldResult)));

        changedGeoArea.setName("Changed Name");
        th.geoAreaRepository.saveAndFlush(changedGeoArea);

        //the change is not yet reflected, since the reference data change log entry is not done
        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                th.app1Variant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedOldResult));
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedOldResult)));

        th.simulateReferenceDataChange();

        timeServiceMock.setOffset(20, ChronoUnit.MINUTES);

        final List<ClientGeoArea> expectedNewResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(changedGeoArea, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                th.app1Variant1.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedNewResult));
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedNewResult)));
    }

    @Test
    public void getAvailableGeoAreasForAppVariant_ParentNotInAppVariant() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        th.unmapAllGeoAreasFromAppVariant(appVariant);
        th.mapGeoAreaToAppVariant(appVariant, th.geoAreaDorf1InEisenberg, th.tenant1);

        assertThat(appService.getAvailableGeoAreasStrict(appVariant)).containsExactly(th.geoAreaDorf1InEisenberg);

        final List<ClientGeoArea> expectedResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,//this is important
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))));

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas",
                appVariant.getAppVariantIdentifier()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult));
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariant,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedResult)));
    }

    @Test
    public void getAvailableGeoAreasForAppVariant_InvalidAppVariant() throws Exception {

        mockMvc.perform(get("/app/appVariant/{appVariantIdentifier}/availableGeoAreas", BaseTestHelper.INVALID_UUID))
                .andExpect(isException(BaseTestHelper.INVALID_UUID, ClientExceptionType.APP_VARIANT_NOT_FOUND));
        mockMvc.perform(get("/app/appVariant/availableGeoAreas")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(BaseTestHelper.INVALID_UUID)
                        .build()
                        .withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAvailableGeoAreasForAppVariant_AppVariantWithoutAvailableGeoAreas() throws Exception {

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, appVariantNoGeoAreas,
                () -> get("/app/appVariant/availableGeoAreas"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonPath("$").isEmpty()));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren() throws Exception {

        final GeoArea rootArea = th.geoAreaRheinlandPfalz;

        // available geo areas for app1Variant1 (tenant1)
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                        th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))))));

        // available geo areas for app2Variant1 (tenant1, tenant2)
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app2Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                        th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                                        th.toClientGeoArea(th.geoAreaMainz, 2, true))))));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_GlobalRoot() throws Exception {

        // available geo areas for app1Variant1 (tenant1)
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true)))))));

        // available geo areas for app2Variant1 (tenant1, tenant2)
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app2Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                                                th.toClientGeoArea(th.geoAreaMainz, 2, true)))))));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_MultipleRoots() throws Exception {

        // available geo areas for app1Variant1 (tenant1) -> Mainz not available in app variant
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId(),
                                th.geoAreaMainz.getId())
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Arrays.asList(
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true)))));

        // available geo areas for app2Variant1 (tenant1, tenant2) -> Mainz available in app variant
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app2Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId(),
                                th.geoAreaMainz.getId())
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Arrays.asList(
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true),
                                th.toClientGeoArea(th.geoAreaMainz, 2, true)))));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_Depth() throws Exception {

        final GeoArea rootArea = th.geoAreaDeutschland;

        // default value 1
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", rootArea.getId()),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(rootArea, 0, false,
                                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false))))));

        // depth 2
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", rootArea.getId())
                        .param("depth", "2"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(rootArea, 0, false,
                                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false),
                                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true)))))));

        // depth 1
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "1"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true))))));

        // depth 0
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "0"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false)))));

        // depth invalid -> 0
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("geoAreaIds", th.geoAreaEisenberg.getId())
                        .param("depth", "-42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(Collections.singletonList(
                                th.toClientGeoArea(th.geoAreaEisenberg, 2, false)))));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_CacheRefresh() throws Exception {

        GeoArea changedGeoArea = th.geoAreaEisenberg;

        final List<ClientGeoArea> expectedOldResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(changedGeoArea, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));

        // ensure the cache is set
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedOldResult)));

        changedGeoArea.setName("Changed Name");
        th.geoAreaRepository.saveAndFlush(changedGeoArea);

        // the change is not yet reflected since the reference data change log entry is not done
        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedOldResult)));

        th.simulateReferenceDataChange();

        timeServiceMock.setOffset(15, ChronoUnit.MINUTES);

        final List<ClientGeoArea> expectedNewResult = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                        th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                th.toClientGeoArea(changedGeoArea, 2, false,
                                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true))));

        callPublicEndpointDifferentAppVariantIdentification(th.personRegularAnna, th.app1Variant1,
                () -> get("/app/appVariant/availableGeoAreas/children")
                        .param("depth", "42"),
                r -> r.andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(jsonEquals(expectedNewResult)));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_InvalidAppVariant() throws Exception {

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/children")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(BaseTestHelper.INVALID_UUID)
                        .build()
                        .withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantWithChildren_GeoAreaNotFound() throws Exception {

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/children")
                .param("geoAreaIds", th.geoAreaEisenberg.getId(), BaseTestHelper.INVALID_UUID)
                .param("depth", "42")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier(th.app1Variant1.getAppVariantIdentifier())
                        .build()
                        .withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.GEO_AREA_NOT_FOUND));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantByTenants() throws Exception {

        AppVariant appVariant1 = th.app1Variant1;
        AppVariant appVariant2 = th.app1Variant2;
        th.unmapAllGeoAreasFromAppVariant(appVariant1);
        th.unmapAllGeoAreasFromAppVariant(appVariant2);
        th.mapGeoAreaToAppVariant(appVariant1, th.geoAreaEisenberg, th.tenant1);
        th.excludeGeoAreaFromAppVariant(appVariant1, th.geoAreaDorf2InEisenberg, th.tenant1);
        th.mapGeoAreaToAppVariant(appVariant1, th.geoAreaKaiserslautern, th.tenant2);

        List<ClientGeoArea> expectedResult1 = Collections.singletonList(
                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/byTenants")
                        .param("relevantAppVariantIdentifier", appVariant1.getAppVariantIdentifier())
                        .param("tenantIds", th.tenant1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult1));

        List<ClientGeoArea> expectedResult2 = List.of(
                th.toClientGeoAreaWithBoundaryPoints(th.geoAreaEisenberg, 2, false,
                        th.toClientGeoAreaWithBoundaryPoints(th.geoAreaDorf1InEisenberg, 3, true)),
                th.toClientGeoAreaWithBoundaryPoints(th.geoAreaKaiserslautern, 2, true));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/byTenants")
                        .param("relevantAppVariantIdentifier", appVariant1.getAppVariantIdentifier())
                        .param("tenantIds", th.tenant1.getId(), th.tenant2.getId())
                        .param("includeBoundaryPoints", "true"))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(expectedResult2));
    }

    @Test
    public void getAvailableGeoAreasForAppVariantByTenants_InvalidParameter() throws Exception {

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/byTenants")
                        .param("relevantAppVariantIdentifier", "meine.eigene.app")
                        .param("tenantIds", th.tenant1.getId()))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));

        String invalidId = "4711";
        mockMvc.perform(get("/app/appVariant/availableGeoAreas/byTenants")
                        .param("relevantAppVariantIdentifier", th.app2Variant1.getAppVariantIdentifier())
                        .param("tenantIds", th.tenant1.getId(), invalidId)
                        .param("includeBoundaryPoints", "true"))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.TENANT_NOT_FOUND, invalidId));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/byTenants")
                        .param("relevantAppVariantIdentifier", th.app2Variant1.getAppVariantIdentifier()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

}
