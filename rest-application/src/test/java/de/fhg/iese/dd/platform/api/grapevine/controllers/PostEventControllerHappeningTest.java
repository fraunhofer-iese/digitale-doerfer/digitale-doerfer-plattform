/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningCreationByPersonFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostEventControllerHappeningTest extends BasePostEventControllerTest<Happening> {

    protected AppVariant appVariantContentCreationRestricted;

    protected AppVariant appVariantHappeningCreationEnabled;

    protected EnumSet<PersonVerificationStatus> allowedVerificationStatuses =
            EnumSet.of(PersonVerificationStatus.PHONE_NUMBER_VERIFIED, PersonVerificationStatus.EMAIL_VERIFIED);

    @Override
    protected Happening getTestPost1() {
        return th.happening1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personLaraSchaefer;
    }

    @Override
    protected Happening getTestPost2() {
        //gets an author for this test
        return th.happening2;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.offer1;
    }

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return ClientHappeningChangeRequest.builder()
                .build();
    }

    @Override
    public void createEntities() throws Exception {
        super.createEntities();
        th.happening2.setCreator(th.personFriedaFischer);
        th.happening2 = th.postRepository.saveAndFlush(th.happening2);
        createFeatureSuggestionCreationEnabled();
    }

    private void createFeatureSuggestionCreationEnabled() {
        appVariantHappeningCreationEnabled = th.appVariant4AllTenants;
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantHappeningCreationEnabled)
                .enabled(true)
                .featureClass(HappeningCreationByPersonFeature.class.getName())
                .configValues(null)
                .build());
    }

    private void createFeatureContentCreationRestriction() {
        appVariantContentCreationRestricted = th.appVariant1Tenant1;

        String allowedStatusArray = allowedVerificationStatuses.stream()
                .map(PersonVerificationStatus::name)
                .collect(Collectors.joining("\",\"", "[\"", "\"]"));

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantContentCreationRestricted)
                .enabled(true)
                .featureClass(ContentCreationRestrictionFeature.class.getName())
                .configValues(
                        "{\"allowedStatusesAnyOf\": " + allowedStatusArray + " }")
                .build());
        //the creation of happenings should be allowed
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariantContentCreationRestricted)
                .enabled(true)
                .featureClass(HappeningCreationByPersonFeature.class.getName())
                .configValues(null)
                .build());
    }

    @Test
    public void happeningCreateRequest_VerifyingPushMessage() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());
        String text = "Grillfest auf der Straße! Geröstetes Schwein am Spieß! 🐖";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3);
        long endTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(5);
        boolean allDayEvent = false;

        ClientLocationDefinition locationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;
        ClientHappeningCreateRequest happeningCreateRequest = ClientHappeningCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .customLocationDefinition(locationDefinition)
                .startTime(startTime)
                .endTime(endTime)
                .allDayEvent(allDayEvent)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.customAddress.name")
                        .value(locationDefinition.getLocationName()))
                .andExpect(jsonPath("$.post.happening.customAddress.street")
                        .value(locationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.post.happening.customAddress.zip")
                        .value(locationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.post.happening.customAddress.city")
                        .value(locationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.post.happening.customAddress.gpsLocation.latitude")
                        .value(locationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.post.happening.customAddress.gpsLocation.longitude")
                        .value(locationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        String postId = happeningCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.customAddress.name")
                        .value(locationDefinition.getLocationName()))
                .andExpect(jsonPath("$.happening.customAddress.street")
                        .value(locationDefinition.getAddressStreet()))
                .andExpect(jsonPath("$.happening.customAddress.zip")
                        .value(locationDefinition.getAddressZip()))
                .andExpect(jsonPath("$.happening.customAddress.city")
                        .value(locationDefinition.getAddressCity()))
                .andExpect(jsonPath("$.happening.customAddress.gpsLocation.latitude")
                        .value(locationDefinition.getGpsLocation().getLatitude()))
                .andExpect(jsonPath("$.happening.customAddress.gpsLocation.longitude")
                        .value(locationDefinition.getGpsLocation().getLongitude()))
                .andExpect(jsonPath("$.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.happening.allDayEvent").value(allDayEvent))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.happening.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.happening.commentsAllowed").value(true))
                .andExpect(jsonPath("$.happening.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the happening is also added to the repository
        Happening happeningFromRepo = (Happening) th.postRepository.findById(postId).get();

        assertThat(happeningFromRepo.getCreator()).isEqualTo(postCreator);
        assertThat(happeningFromRepo.getCreatedLocation().getLatitude()).isEqualTo(createdLocation.getLatitude());
        assertThat(happeningFromRepo.getCreatedLocation().getLongitude()).isEqualTo(createdLocation.getLongitude());
        assertThat(happeningFromRepo.getText()).isEqualTo(text);
        assertThat(happeningFromRepo.getCustomAddress().getName()).isEqualTo(locationDefinition.getLocationName());
        assertThat(happeningFromRepo.getCustomAddress().getStreet()).isEqualTo(locationDefinition.getAddressStreet());
        assertThat(happeningFromRepo.getCustomAddress().getZip()).isEqualTo(locationDefinition.getAddressZip());
        assertThat(happeningFromRepo.getCustomAddress().getCity()).isEqualTo(locationDefinition.getAddressCity());
        assertThat(happeningFromRepo.getStartTime()).isEqualTo(startTime);
        assertThat(happeningFromRepo.getEndTime()).isEqualTo(endTime);
        assertThat(happeningFromRepo.isAllDayEvent()).isEqualTo(allDayEvent);
        assertThat(happeningFromRepo.getTenant()).isEqualTo(postCreator.getTenant());
        assertThat(happeningFromRepo.getImages()).isEmpty();
        assertThat(happeningFromRepo.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertThat(happeningFromRepo.isHiddenForOtherHomeAreas()).isFalse();
        assertTrue(happeningFromRepo.isCommentsAllowed());
        assertThat(happeningFromRepo.isParallelWorld()).isFalse();

        // verify push message
        waitForEventProcessing();

        Pair<String, ClientPostCreateConfirmation> pushedEventAndMessage =
                testClientPushService.getPushedEventAndMessageToGeoAreasLoud(postCreator,
                        happeningFromRepo.getGeoAreas(),
                        false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);
        String message = pushedEventAndMessage.getLeft();
        assertThat(message).contains(postCreator.getFirstNameFirstCharLastDot());
        assertThat(message).contains("funkt in Events");

        ClientPostCreateConfirmation pushedEvent = pushedEventAndMessage.getRight();
        assertThat(pushedEvent.getPostId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getText()).isEqualTo(text);
        //check that the last name is cut
        assertThat(pushedEvent.getPost().getHappening().getCreator().getFirstName()).isEqualTo(
                postCreator.getFirstName());
        assertThat(pushedEvent.getPost().getHappening().getCreator().getLastName()).isEqualTo(
                BaseLastNameShorteningModifier.shorten(postCreator.getLastName()));

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningCreateRequest_AllDayEvent() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());
        String text = "Eintagsparty 🦟";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3);
        boolean allDayEvent = true;

        ClientHappeningCreateRequest happeningCreateRequest = ClientHappeningCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .startTime(startTime)
                .allDayEvent(allDayEvent)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(0))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        String postId = happeningCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.happening.endTime").value(0))
                .andExpect(jsonPath("$.happening.allDayEvent").value(allDayEvent))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.happening.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.happening.commentsAllowed").value(true))
                .andExpect(jsonPath("$.happening.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the happening is also added to the repository
        Happening happeningFromRepo = (Happening) th.postRepository.findById(postId).get();

        assertThat(happeningFromRepo.getCreator()).isEqualTo(postCreator);
        assertThat(happeningFromRepo.getCreatedLocation().getLatitude()).isEqualTo(createdLocation.getLatitude());
        assertThat(happeningFromRepo.getCreatedLocation().getLongitude()).isEqualTo(createdLocation.getLongitude());
        assertThat(happeningFromRepo.getText()).isEqualTo(text);
        assertThat(happeningFromRepo.getStartTime()).isEqualTo(startTime);
        assertThat(happeningFromRepo.getEndTime()).isEqualTo(0);
        assertThat(happeningFromRepo.isAllDayEvent()).isEqualTo(allDayEvent);
        assertThat(happeningFromRepo.getTenant()).isEqualTo(postCreator.getTenant());
        assertThat(happeningFromRepo.getImages()).isEmpty();
        assertThat(happeningFromRepo.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertThat(happeningFromRepo.isHiddenForOtherHomeAreas()).isFalse();
        assertTrue(happeningFromRepo.isCommentsAllowed());
        assertThat(happeningFromRepo.isParallelWorld()).isFalse();
    }

    @Test
    public void happeningCreateRequest_ContentCreationRestricted() throws Exception {

        //this is not tested using assertIsPersonVerificationStatusRestricted in order to test it in more detail

        createFeatureContentCreationRestriction();

        Person postCreator = th.personThomasBecker;

        assertTrue(postCreator.getVerificationStatuses().getValues().isEmpty(), "Post creator needs to be unverified");

        String text = "Party on!";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3);
        long endTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(5);
        boolean allDayEvent = false;

        ClientHappeningCreateRequest happeningCreateRequest = ClientHappeningCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .startTime(startTime)
                .endTime(endTime)
                .allDayEvent(allDayEvent)
                .build();

        //no verification at all
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //set the verification status to one that is not in the list of allowed statuses
        postCreator.getVerificationStatuses().addValue(
                EnumSet.complementOf(allowedVerificationStatuses).iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //one of the allowed verifications
        postCreator.getVerificationStatuses().addValue(allowedVerificationStatuses.iterator().next());
        postCreator = th.personRepository.saveAndFlush(postCreator);

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantContentCreationRestricted))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andReturn();

        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        String postId = happeningCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()));
    }

    @Test
    public void happeningCreateRequest_ParallelWorld() throws Exception {

        Person postCreator = th.personThomasBecker;
        postCreator.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(postCreator);

        String text = "AGGRO PARTY! BRINGT PFEFFERSPRAY MIT!!!!";
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(1);
        long endTime = 0;
        boolean allDayEvent = true;

        ClientHappeningCreateRequest happeningCreateRequest = ClientHappeningCreateRequest.builder()
                .text(text)
                .startTime(startTime)
                .endTime(endTime)
                .allDayEvent(allDayEvent)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the happening is also added to the repository
        Happening addedHappening =
                (Happening) th.postRepository.findById(happeningCreateConfirmation.getPost().getId()).get();

        assertThat(addedHappening.isParallelWorld()).isTrue();

        String postId = happeningCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()));
    }

    @Test
    public void happeningCreateRequest_TooBigPushMessage() throws Exception {

        Person postCreator = th.personThomasBecker;
        assertNotNull(postCreator.getHomeArea());
        assertNotEquals(postCreator.getHomeArea(), postCreator.getTenant().getRootArea());

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i < th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(th.createTemporaryMediaItemWithMaxUrls(postCreator, 0).getId());
        }

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Feuerwehrfest mit echtem Rauch 🔥")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(42))
                                .allDayEvent(true)
                                .temporaryMediaItemIds(temporaryIds)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Check whether the happening is also added to the repository
        String postId = happeningCreateConfirmation.getPost().getId();
        Happening addedHappening = (Happening) th.postRepository.findById(postId).get();

        assertNotNull(addedHappening);

        // verify push message
        waitForEventProcessing();

        //this event can only be pushed if it was minimized
        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(postCreator, addedHappening.getGeoAreas(), false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getId()).isEqualTo(postId);

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningCreateRequest_Multiple() throws Exception {

        //since there is a constraint on the external id of the post (see Post) we need to check that it's possible to create multiple posts
        Person postCreator = th.personThomasBecker;

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Serien-Party! Tag 1")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(42))
                                .allDayEvent(true)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertNotNull(th.postRepository.findById(happeningCreateConfirmation.getPost().getId()).get());

        MvcResult createRequestResult2 = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Serien-Party! Tag 2")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(43))
                                .allDayEvent(true)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation2 = toObject(createRequestResult2.getResponse(),
                ClientPostCreateConfirmation.class);
        assertNotNull(th.postRepository.findById(happeningCreateConfirmation2.getPost().getId()).get());

        MvcResult createRequestResult3 = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Serien-Party! Tag3")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(44))
                                .allDayEvent(true)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation3 = toObject(createRequestResult3.getResponse(),
                ClientPostCreateConfirmation.class);
        assertNotNull(th.postRepository.findById(happeningCreateConfirmation3.getPost().getId()).get());
    }

    @Test
    public void happeningCreateRequest_WithImagesAndDocumentsVerifyingPushMessage() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        assertNotNull(postCreator.getHomeArea());
        String text = "Dia-Show bei mir im Party-Keller! Karte zur Anreise anbei 🗺";
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3);
        long endTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(5);
        boolean allDayEvent = false;

        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images and documents
        ClientHappeningCreateRequest happeningCreateRequest = ClientHappeningCreateRequest.builder()
                .text(text)
                .startTime(startTime)
                .endTime(endTime)
                .allDayEvent(allDayEvent)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andReturn();
        ClientPostCreateConfirmation happeningCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(happeningCreateConfirmation.getPost().getHappening().getText()).isEqualTo(text);
        assertThat(happeningCreateConfirmation.getPost().getHappening().getImages()).hasSize(3);
        assertThat(happeningCreateConfirmation.getPost().getHappening().getAttachments()).hasSize(3);

        // Get the happening
        //check new entity uses images and documents
        String postId = happeningCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.happening.allDayEvent").value(allDayEvent))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.happening.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.happening.commentsAllowed").value(true))
                .andExpect(jsonPath("$.happening.images", hasSize(3)))
                .andExpect(jsonPath("$.happening.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.happening.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.happening.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.happening.attachments", hasSize(3)))
                .andExpect(jsonPath("$.happening.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.happening.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.happening.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.happening.hiddenForOtherHomeAreas").value(true));

        // Check whether the happening is also added to the repository
        Happening happeningFromRepo = (Happening) th.postRepository.findById(postId).get();

        assertThat(happeningFromRepo.getCreator()).isEqualTo(postCreator);
        assertThat(happeningFromRepo.getStartTime()).isEqualTo(startTime);
        assertThat(happeningFromRepo.getEndTime()).isEqualTo(endTime);
        assertThat(happeningFromRepo.isAllDayEvent()).isEqualTo(allDayEvent);
        assertThat(happeningFromRepo.getText()).isEqualTo(text);
        assertThat(happeningFromRepo.getTenant()).isEqualTo(postCreator.getTenant());
        assertThat(happeningFromRepo.getImages()).containsExactly(tempImage1.getMediaItem(), tempImage2.getMediaItem(),
                tempImage3.getMediaItem());
        assertThat(happeningFromRepo.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(happeningFromRepo.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        ;
        assertThat(happeningFromRepo.isHiddenForOtherHomeAreas()).isTrue();

        //check used temp images and documents are deleted
        assertThat(th.temporaryMediaItemRepository.count()).isEqualTo(1);
        assertThat(th.temporaryDocumentItemRepository.count()).isEqualTo(1);
        //check non used are still existing
        assertThat(th.temporaryMediaItemRepository.findAll().get(0)).isEqualTo(tempImageUnused);
        assertThat(th.temporaryDocumentItemRepository.findAll().get(0)).isEqualTo(tempDocumentUnused);

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent = testClientPushService.getPushedEventToGeoAreasLoud(postCreator,
                happeningFromRepo.getGeoAreas(),
                true,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getId()).isEqualTo(postId);

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningCreateRequest_InvalidImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Seminar: Bildbearbeitung")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .temporaryMediaItemIds(
                                        Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                                .build())))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertThat(th.temporaryMediaItemRepository.count()).isEqualTo(2);
    }

    @Test
    public void happeningCreateRequest_InvalidDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempDocumentItemId = "invalid id";

        //create request using temp documents and invalid id

        //check temp documents not found exception
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Kurs: Dokumentenmanagement")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .temporaryDocumentItemIds(
                                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidTempDocumentItemId))
                                .build())))
                .andExpect(isException("[" + invalidTempDocumentItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertThat(th.temporaryDocumentItemRepository.count()).isEqualTo(2);
    }

    @Test
    public void happeningCreateRequest_DuplicateImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Lesung aus dem Buch: Das doppelte Lottchen")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .temporaryMediaItemIds(
                                        Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage2.getId()))
                                .build())))
                .andExpect(isException("[" + tempImage2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp images are not deleted
        assertThat(th.temporaryMediaItemRepository.count()).isEqualTo(2);
    }

    @Test
    public void happeningCreateRequest_DuplicateDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Doppelgänger-Party 🙌";
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Outdoor-Kurs: Dokumente in Stein meißeln")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .temporaryDocumentItemIds(
                                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), tempDocument2.getId()))
                                .build())))
                .andExpect(isException("[" + tempDocument2.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp images are not deleted
        assertThat(th.temporaryDocumentItemRepository.count()).isEqualTo(2);
    }

    @Test
    public void happeningCreateRequest_ExceedingMaxTextLength() throws Exception {

        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(th.personThomasBecker, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text(text)
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningCreateRequest_ExceedingMaxNumberImages() throws Exception {

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberImagesPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(th.personThomasBecker, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Dia-Abend die ganze Nacht!")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .temporaryMediaItemIds(temporaryIds)
                                .build())))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningCreateRequest_ExceedingMaxNumberDocuments() throws Exception {

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(th.personThomasBecker, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Ganztages-Kurs: Dokumente shreddern")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .allDayEvent(true)
                                .temporaryDocumentItemIds(temporaryIds)
                                .build())))
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningCreateRequest_InvalidAttributes() throws Exception {

        Person person = th.personSusanneChristmann;

        // missing text
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(person, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(5))
                                .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing home area and no tenant
        person.setHomeArea(null);
        person = th.personRepository.save(person);
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(person, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Party für alle Neubürger")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(6))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(7))
                                .build())))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));

        person = th.personThomasBecker;

        //start after end
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(person, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Fachvortrag: Zeitreise ⏰")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(2))
                                .build())))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, "startTime",
                        "endTime"));

        //start equals end
        long startTime = timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3);
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(person, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Kurzvortrag: Zeitmanagement ⏰")
                                .startTime(startTime)
                                .endTime(startTime)
                                .build())))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, "startTime",
                        "endTime"));

        //start is zero
        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(person, appVariantHappeningCreationEnabled))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Fachvortrag: Wie war es 1970?s ⏰")
                                .startTime(0)
                                .allDayEvent(true)
                                .build())))
                .andExpect(isException("startTime", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningCreateRequest_FeatureNotEnabled() throws Exception {

        mockMvc.perform(post("/grapevine/event/happeningCreateRequest")
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant3Tenant3))
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Geheime Party im Bunker!")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .build())))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void happeningCreateRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/happeningCreateRequest")
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateRequest.builder()
                                .text("Geheime Party im Bunker!")
                                .startTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(3))
                                .endTime(timeServiceMock.currentTimeMillisUTC() + TimeUnit.DAYS.toMillis(4))
                                .build())),
                appVariantHappeningCreationEnabled,
                th.personSusanneChristmann);
    }

    /* ***** CHANGE OPERATIONS *******/

    @Test
    public void happeningChangeRequest_TextVerifyingPushMessage() throws Exception {

        Happening happeningToBeChanged = th.happening1;
        String postId = happeningToBeChanged.getId();
        Person postCreator = happeningToBeChanged.getCreator();
        assertThat(postCreator).isNotNull();
        String text = "Die Party findet nun doch drinnen statt!";
        long now = timeServiceMock.currentTimeMillisUTC();
        long startTime = now + TimeUnit.DAYS.toMillis(3);
        long endTime = now + TimeUnit.DAYS.toMillis(5);
        timeServiceMock.setConstantDateTime(now);
        boolean allDayEvent = false;
        List<MediaItem> existingImages = happeningToBeChanged.getImages();
        Set<DocumentItem> existingDocuments = happeningToBeChanged.getAttachments();
        assertThat(existingImages).isNotEmpty();
        assertThat(existingDocuments).isNotEmpty();

        ClientHappeningChangeRequest happeningChangeRequest = ClientHappeningChangeRequest.builder()
                .postId(happeningToBeChanged.getId())
                .startTime(startTime)
                .endTime(endTime)
                .allDayEvent(allDayEvent)
                .text(text)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/event/happeningChangeRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(happeningChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.post.happening.text").value(text))
                .andExpect(jsonPath("$.post.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.post.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.post.happening.allDayEvent").value(allDayEvent))
                .andExpect(jsonPath("$.post.happening.lastModified").value(now))
                .andReturn();

        mockMvc.perform(get("/grapevine/post/{postId}", postId)
                        .headers(authHeadersFor(postCreator, appVariantHappeningCreationEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(postId))
                .andExpect(jsonPath("$.happening.text").value(text))
                .andExpect(jsonPath("$.happening.startTime").value(startTime))
                .andExpect(jsonPath("$.happening.endTime").value(endTime))
                .andExpect(jsonPath("$.happening.allDayEvent").value(allDayEvent))
                .andExpect(jsonPath("$.happening.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.happening.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.happening.commentsAllowed").value(true))
                .andExpect(jsonPath("$.happening.lastModified").value(now))
                .andExpect(jsonPath("$.happening.hiddenForOtherHomeAreas").value(false));

        // Check whether the happening is also updated in the repository
        Happening updatedHappening = (Happening) th.postRepository.findById(postId).get();
        assertThat(updatedHappening.getText()).isEqualTo(text);
        assertThat(updatedHappening.getImages()).containsExactlyElementsOf(existingImages);
        assertThat(updatedHappening.getAttachments()).isEqualTo(existingDocuments);
        assertThat(updatedHappening.getStartTime()).isEqualTo(startTime);
        assertThat(updatedHappening.getEndTime()).isEqualTo(endTime);
        assertThat(updatedHappening.isAllDayEvent()).isEqualTo(allDayEvent);

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(happeningToBeChanged.getGeoAreas(),
                        happeningToBeChanged.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPostId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getId()).isEqualTo(postId);
        assertThat(pushedEvent.getPost().getHappening().getText()).isEqualTo(text);
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningChangeRequest_ImagesOnly() throws Exception {

        Happening happeningToBeChanged = th.happening2;
        Person creator = happeningToBeChanged.getCreator();
        assertThat(creator).isNotNull();
        List<MediaItem> existingImages = happeningToBeChanged.getImages();
        assertThat(existingImages).isNotEmpty();
        String existingText = happeningToBeChanged.getText();
        long existingStartTime = happeningToBeChanged.getStartTime();
        long existingEndTime = happeningToBeChanged.getEndTime();
        boolean existingAllDayEvent = happeningToBeChanged.isAllDayEvent();
        String happeningId = happeningToBeChanged.getId();

        //create additional temp image
        TemporaryMediaItem newTempMediaItem =
                th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        ClientHappeningChangeRequest happeningChangeRequest = ClientHappeningChangeRequest.builder()
                .postId(happeningId)
                .mediaItemPlaceHolders(Collections.singletonList(
                        ClientMediaItemPlaceHolder.builder()
                                .temporaryMediaItemId(newTempMediaItem.getId()).build()))
                .build();

        mockMvc.perform(post("/grapevine/event/happeningChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(happeningChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(happeningChangeRequest.getPostId()))
                .andExpect(jsonPath("$.post.happening.id").value(happeningChangeRequest.getPostId()))
                .andExpect(jsonPath("$.post.happening.images", hasSize(1)))
                .andExpect(jsonPath("$.post.happening.images[0].id").value(newTempMediaItem.getMediaItem().getId()));

        // Check whether only the changed fields are updated
        Happening updatedHappening = (Happening) th.postRepository.findById(happeningId).get();
        assertThat(updatedHappening.getImages()).contains(newTempMediaItem.getMediaItem());
        assertThat(updatedHappening.getText()).isEqualTo(existingText);
        assertThat(updatedHappening.getStartTime()).isEqualTo(existingStartTime);
        assertThat(updatedHappening.getEndTime()).isEqualTo(existingEndTime);
        assertThat(updatedHappening.isAllDayEvent()).isEqualTo(existingAllDayEvent);

        //check if the old images are deleted
        assertThat(th.mediaItemRepository.findAll()).doesNotContainAnyElementsOf(existingImages);
    }

    @Test
    public void happeningChangeRequest_InvalidAttributes() throws Exception {

        Happening happeningToBeChanged = th.happening2;
        Person creator = happeningToBeChanged.getCreator();
        assertThat(creator).isNotNull();
        long existingStartTime = happeningToBeChanged.getStartTime();
        long existingEndTime = happeningToBeChanged.getEndTime();
        String happeningId = happeningToBeChanged.getId();

        //new start time would be after existing end time
        mockMvc.perform(post("/grapevine/event/happeningChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(ClientHappeningChangeRequest.builder()
                                .postId(happeningId)
                                .startTime(existingEndTime + 5)
                                .build())))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, "startTime",
                        "endTime"));
        //end time zero but no all day event
        mockMvc.perform(post("/grapevine/event/happeningChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(ClientHappeningChangeRequest.builder()
                                .postId(happeningId)
                                .endTime(0L)
                                .allDayEvent(false)
                                .build())))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, "startTime",
                        "endTime"));
    }

    @Test
    public void happeningChangeRequest_NotOwner() throws Exception {

        Happening happeningToBeChanged = th.happening2;
        Person changingPerson = th.personLaraSchaefer;
        String existingText = happeningToBeChanged.getText();
        Person creator = happeningToBeChanged.getCreator();
        assertThat(creator).isNotNull();
        assertThat(creator).isNotEqualTo(changingPerson);

        List<MediaItem> existingImages = happeningToBeChanged.getImages();
        assertThat(existingImages).isNotEmpty();

        String text = "this is the new text for happening 4";

        ClientHappeningChangeRequest happeningChangeRequest = ClientHappeningChangeRequest.builder()
                .postId(happeningToBeChanged.getId())
                .startTime(5L)
                .allDayEvent(true)
                .text(text)
                .build();

        mockMvc.perform(post("/grapevine/event/happeningChangeRequest")
                        .headers(authHeadersFor(changingPerson, th.appVariant1Tenant1))
                        .contentType(contentType)
                        .content(json(happeningChangeRequest)))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));

        // Check whether the happening is not updated in the repository
        Happening happeningFromRepository = (Happening) th.postRepository.findById(happeningToBeChanged.getId()).get();
        assertThat(happeningFromRepository.getText()).isEqualTo(existingText);
        assertThat(happeningFromRepository.getImages()).containsExactlyInAnyOrderElementsOf(existingImages);
    }

}
