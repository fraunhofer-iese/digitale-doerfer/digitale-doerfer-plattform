/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.TransportAlternativeBundleRepository;

public class LogisticsAdminUIControllerTest extends BaseServiceTest {

    @Autowired
    private LogisticsAdministrationTestHelper th;

    @Autowired
    private TransportAlternativeBundleRepository transportAlternativeBundleRepository;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createShops();
        th.createPoolingStations();
        th.createLogisticScenario();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAllDeliveries() throws Exception {
        List<Delivery> expectedDeliveries = th.deliveryList.stream()
                .filter(i -> (i.getTenant().equals(th.tenant1) && i.getCreated() >= th.delivery2.getCreated()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedDeliveries.size());

        //VGadmin
        mockMvc.perform(get("/logistics/adminui/deliveries")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.delivery2.getCreated()))
                .param("toTime", Long.toString(th.delivery4.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedDeliveries.size())))
                .andExpect(jsonPath("$[0].deliveryId").value(expectedDeliveries.get(0).getId()))
                .andExpect(jsonPath("$[1].deliveryId").value(expectedDeliveries.get(1).getId()))
                .andExpect(jsonPath("$[2].deliveryId").value(expectedDeliveries.get(2).getId()))
                .andExpect(jsonPath("$[0].purchaseOrderItems").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].trackingCode").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].statusHistory").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].purchaseOrderId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].community").value(IsNull.nullValue()));

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/deliveries")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.delivery2.getCreated()))
                .param("toTime", Long.toString(th.delivery4.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedDeliveries.size())))
                .andExpect(jsonPath("$[0].deliveryId").value(expectedDeliveries.get(0).getId()))
                .andExpect(jsonPath("$[1].deliveryId").value(expectedDeliveries.get(1).getId()))
                .andExpect(jsonPath("$[2].deliveryId").value(expectedDeliveries.get(2).getId()))
                .andExpect(jsonPath("$[0].contentNotes").value(expectedDeliveries.get(0).getContentNotes()));
    }

    @Test
    public void getAllOpenTransportAlternativeBundles() throws Exception {

        List<TransportAlternativeBundle> expectedTransportAlternativeBundles = transportAlternativeBundleRepository
                .findAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThanOrderByCreatedDesc(th.tenant1.getId(),
                        th.transportAlternativeBundle1.getCreated());

        assertEquals(1, expectedTransportAlternativeBundles.size());

        //VGadmin
        mockMvc.perform(get("/logistics/adminui/transportAlternativeBundles")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.transportAlternativeBundle2.getCreated()))
                .param("toTime", Long.toString(th.transportAlternativeBundle4.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTransportAlternativeBundles.size())))
                .andExpect(jsonPath("$[0].transportAlternativeBundleId").value(
                        expectedTransportAlternativeBundles.get(0).getId()))
                .andExpect(jsonPath("$[0].purchaseOrderItems").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].statusHistory").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].purchaseOrderId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].senderShopId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].receiverId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].community").value(IsNull.nullValue()));

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/transportAlternativeBundles")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.transportAlternativeBundle2.getCreated()))
                .param("toTime", Long.toString(th.transportAlternativeBundle4.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTransportAlternativeBundles.size())))
                .andExpect(jsonPath("$[0].transportAlternativeBundleId").value(
                        expectedTransportAlternativeBundles.get(0).getId()));
    }

    @Test
    public void getAllTransportAssignments() throws Exception {
        List<TransportAssignment> expectedTransportAssignments = th.transportAssignmentList.stream()
                .filter(i -> i.getDelivery().getTenant().equals(th.tenant1)
                        && i.getCreated() >= th.transportAssignment2.getCreated())
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedTransportAssignments.size());

        //VGadmin
        mockMvc.perform(get("/logistics/adminui/transportAssignments")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.transportAssignment2.getCreated()))
                .param("toTime", Long.toString(th.transportAssignment3.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTransportAssignments.size())))
                .andExpect(jsonPath("$[0].transportAssignmentId").value(expectedTransportAssignments.get(0).getId()))
                .andExpect(jsonPath("$[1].transportAssignmentId").value(expectedTransportAssignments.get(1).getId()))
                .andExpect(jsonPath("$[1].purchaseOrderItems").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].statusHistory").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].purchaseOrderId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].senderShopId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].receiverId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].community").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].carrierId").value(IsNull.nullValue()));

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/transportAssignments")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", Long.toString(th.transportAssignment2.getCreated()))
                .param("toTime", Long.toString(th.transportAssignment3.getCreated())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedTransportAssignments.size())))
                .andExpect(jsonPath("$[0].transportAssignmentId").value(expectedTransportAssignments.get(0).getId()))
                .andExpect(jsonPath("$[1].transportAssignmentId").value(expectedTransportAssignments.get(1).getId()));
    }

    @Test
    public void getAllPoolingStationBoxesByCommunity() throws Exception {
        List<PoolingStationBox> expectedPoolingStationBoxes = th.poolingStationBoxList.stream()
                .filter(i -> i.getPoolingStation().getTenant().equals(th.tenant1))
                .sorted(Comparator.comparing(p -> ((PoolingStationBox) p).getPoolingStation().getName())
                        .thenComparing(p -> ((PoolingStationBox) p).getName()))
                .collect(Collectors.toList());

        assertEquals(4, expectedPoolingStationBoxes.size());

        //VGadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxes")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", th.tenant1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxes.size())))
                .andExpect(jsonPath("$[0].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[2].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[3].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].allocationId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].transportAssignmentInId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[0].community").value(IsNull.nullValue()));

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxes")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", ""))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxes.size())))
                .andExpect(jsonPath("$[0].poolingStationBoxId").value(expectedPoolingStationBoxes.get(0).getId()))
                .andExpect(jsonPath("$[1].poolingStationBoxId").value(expectedPoolingStationBoxes.get(1).getId()))
                .andExpect(jsonPath("$[2].poolingStationBoxId").value(expectedPoolingStationBoxes.get(2).getId()))
                .andExpect(jsonPath("$[3].poolingStationBoxId").value(expectedPoolingStationBoxes.get(3).getId()));
    }

    @Test
    public void getAllPoolingStationBoxesByCommunityAndPoolingStation() throws Exception {
        List<PoolingStationBox> expectedPoolingStationBoxes = th.poolingStationBoxList.stream()
                .filter(i -> i.getPoolingStation().getTenant().equals(th.tenant1)
                        && i.getPoolingStation().equals(th.poolingStation1))
                .sorted(Comparator.comparing(p -> ((PoolingStationBox) p).getPoolingStation().getName())
                        .thenComparing(p -> ((PoolingStationBox) p).getName()))
                .collect(Collectors.toList());

        assertEquals(2, expectedPoolingStationBoxes.size());

        //VGadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxes")
                .headers(authHeadersFor(th.personVgAdmin))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxes.size())))
                .andExpect(jsonPath("$[0].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].poolingStationBoxId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].allocationId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].transportAssignmentInId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].transportAssignmentOutId").value(IsNull.nullValue()))
                .andExpect(jsonPath("$[1].community").value(IsNull.nullValue()));

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxes")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxes.size())))
                .andExpect(jsonPath("$[0].poolingStationBoxId").value(expectedPoolingStationBoxes.get(0).getId()))
                .andExpect(jsonPath("$[1].poolingStationBoxId").value(expectedPoolingStationBoxes.get(1).getId()));
    }

    @Test
    public void getAllPoolingStationBoxAllocationsByCommunity() throws Exception {
        List<PoolingStationBoxAllocation> expectedPoolingStationBoxAllocations =
                th.poolingStationBoxAllocationList.stream()
                        .filter(i -> i.getDelivery().getTenant().equals(th.tenant1)
                                && i.getTimestamp() >= th.poolingStationBoxAllocation2.getTimestamp())
                        .sorted(Comparator.comparingLong(PoolingStationBoxAllocation::getTimestamp).reversed())
                        .collect(Collectors.toList());

        assertEquals(1, expectedPoolingStationBoxAllocations.size());

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxAllocations")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId()).param("poolingStationId", "")
                .param("fromTime", Long.toString(th.poolingStationBoxAllocation2.getTimestamp()))
                .param("toTime", Long.toString(th.poolingStationBoxAllocation2.getTimestamp())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxAllocations.size())))
                .andExpect(jsonPath("$[0].allocationId").value(expectedPoolingStationBoxAllocations.get(0).getId()));
    }

    @Test
    public void getAllPoolingStationBoxAllocationsByCommunityAndPoolingStation() throws Exception {
        List<PoolingStationBoxAllocation> expectedPoolingStationBoxAllocations =
                th.poolingStationBoxAllocationList.stream()
                        .filter(i -> i.getDelivery().getTenant().equals(th.tenant1)
                                && i.getPoolingStationBox().getPoolingStation().equals(th.poolingStation1))
                        .sorted(Comparator.comparingLong(PoolingStationBoxAllocation::getTimestamp).reversed())
                        .collect(Collectors.toList());

        assertEquals(2, expectedPoolingStationBoxAllocations.size());

        //IESEadmin
        mockMvc.perform(get("/logistics/adminui/poolingStationBoxAllocations")
                .headers(authHeadersFor(th.personIeseAdmin))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(th.poolingStationBoxAllocation2.getTimestamp())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStationBoxAllocations.size())))
                .andExpect(jsonPath("$[0].allocationId").value(expectedPoolingStationBoxAllocations.get(0).getId()))
                .andExpect(jsonPath("$[1].allocationId").value(expectedPoolingStationBoxAllocations.get(1).getId()));
    }

    @Test
    public void checkAllEndpoints_Unauthorized() throws Exception {

        // these tests are combined into one because the setup of the tests takes so long

        // adminui/deliveries

        assertOAuth2(get("/logistics/adminui/deliveries")
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .contentType(contentType));

        mockMvc.perform(get("/logistics/adminui/deliveries")
                .headers(authHeadersFor(th.personPoolingStationOp))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // adminui/transportalternativebundles

        assertOAuth2(get("/logistics/adminui/transportAlternativeBundles")
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .contentType(contentType));

        mockMvc.perform(get("/logistics/adminui/transportAlternativeBundles")
                .headers(authHeadersFor(th.personPoolingStationOp))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // adminui/transportassignments

        assertOAuth2(get("/logistics/adminui/transportAssignments")
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .contentType(contentType));

        mockMvc.perform(get("/logistics/adminui/transportAssignments")
                .headers(authHeadersFor(th.personPoolingStationOp))
                .param("communityId", th.tenant1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // adminui/poolingstationboxallocations

        assertOAuth2(get("/logistics/adminui/poolingStationBoxAllocations")
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId())
                .contentType(contentType));

        mockMvc.perform(get("/logistics/adminui/poolingStationBoxAllocations")
                .headers(authHeadersFor(th.personPoolingStationOp))
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId()))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        // adminui/poolingstationboxes

        assertOAuth2(get("/logistics/adminui/poolingStationBoxes")
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis()))
                .contentType(contentType));

        mockMvc.perform(get("/logistics/adminui/poolingStationBoxes")
                .headers(authHeadersFor(th.personPoolingStationOp))
                .param("communityId", th.tenant1.getId())
                .param("poolingStationId", th.poolingStation1.getId())
                .param("fromTime", "0")
                .param("toTime", Long.toString(System.currentTimeMillis())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
