/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.ElasticsearchTestUtil;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientExternalPostCreateOrUpdateByNewsSourceConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.newsitem.ClientNewsItemCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.datamanagement.ElasticsearchContainerProvider;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsItemPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.junit.ClassRule;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles({"test", "test-elasticsearch", "integration-test"})
@Tag("grapevine-search")
@ExtendWith(AssumeIntegrationTestExtension.class)
public class NewsItemEventControllerSearchTest extends BaseServiceTest {

    @ClassRule
    public static ElasticsearchContainer elasticsearchContainer = ElasticsearchContainerProvider.getInstance();

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private ElasticsearchTestUtil elasticsearchTestUtil;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createOrganizations();
        th.createNewsSources();
        createPostTypeFeatures();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        elasticsearchTestUtil.deleteAllEntitiesInIndex(SearchPost.class);
        elasticsearchTestUtil.reset();
    }

    private void createPostTypeFeatures() {

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(NewsItemPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Plausch\", \"itemName\": \"Plausch\", \"maxNumberCharsPerPost\": 255 }")
                .build()
                .withConstantId());
    }

    @Test
    public void newsItemPublishAndRepublishRequest_Search() throws Exception {

        String author = "Hans Dampf";
        GeoArea geoArea = th.subGeoArea1Tenant1;
        String text = "Langweiliger länglicher Text um ein Ereignis, das niemanden interessiert.";
        String categories = "Rhabarber";
        String externalId = "external id for published news";

        String newsURL = th.newsSourceDigitalbach.getSiteUrl() + "/rhabarber";
        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant4AllTenants;
        th.selectGeoAreasForAppVariant(appVariant, caller, geoArea);

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName(author)
                                .text(text)
                                .externalId(externalId)
                                .newsURL(newsURL)
                                .categories(categories)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation newsItemCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        waitForEventProcessingLong();

        final String postId = newsItemCreateConfirmation.getPostId();
        final SearchPost indexedPost =
                elasticsearchTestUtil.elasticsearchOperations.get(postId,
                        SearchPost.class);
        assertThat(indexedPost).isNotNull();
        assertThat(indexedPost.getText()).isEqualTo(text);

        mockMvc.perform(get("/grapevine/search")
                .param("search", "Ereignis")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.NEWS_ITEM.toString()))
                .andExpect(jsonPath("$.content[0].newsItem.id").value(postId))
                .andExpect(jsonPath("$.content[0].newsItem.text").value(text));

        String text2 = "Jetzt wird der Artikel doch interessant, es geht um Katzen!";

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName(author)
                                .text(text2)
                                .externalId(externalId)
                                .newsURL(newsURL)
                                .categories(categories)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        waitForEventProcessingLong();

        mockMvc.perform(get("/grapevine/search")
                .param("search", "interessant")
                .param("page", "0")
                .param("count", "1")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].type").value(ClientPostType.NEWS_ITEM.toString()))
                .andExpect(jsonPath("$.content[0].newsItem.id").value(postId))
                .andExpect(jsonPath("$.content[0].newsItem.text").value(text2));
    }

}
