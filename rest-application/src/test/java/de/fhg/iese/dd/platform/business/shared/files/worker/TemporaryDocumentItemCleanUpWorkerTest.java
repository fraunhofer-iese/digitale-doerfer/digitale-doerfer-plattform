/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryDocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;

public class TemporaryDocumentItemCleanUpWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TemporaryDocumentItemRepository temporaryDocumentItemRepository;
    @Autowired
    private IDocumentItemService documentItemService;
    @Autowired
    private TestTimeService testTimeService;

    @Autowired
    private TemporaryDocumentItemCleanUpWorker temporaryDocumentItemCleanUpWorker;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void cleanup_NoItems() throws Exception {

        List<TemporaryDocumentItem> temporaryDocumentItems =
                createTemporaryDocumentItems(testTimeService.currentTimeMillisUTC() + TimeUnit.SECONDS.toMillis(180));

        long sizeBeforeCleanUp = temporaryDocumentItems.size();
        long now = testTimeService.currentTimeMillisUTC();

        long expiredTemporaryDocumentItems = temporaryDocumentItems.stream()
                .filter(mi -> mi.getExpirationTime() < now).count();

        //no expired element
        assertEquals(0, expiredTemporaryDocumentItems);

        temporaryDocumentItemCleanUpWorker.run();

        long sizeAfterCleanUp = temporaryDocumentItemRepository.count();

        //no deletion was made
        assertEquals(sizeBeforeCleanUp, sizeAfterCleanUp);
    }

    @Test
    public void cleanup_SomeItems() throws Exception {

        List<TemporaryDocumentItem> temporaryDocumentItems =
                createTemporaryDocumentItems(testTimeService.currentTimeMillisUTC() + TimeUnit.SECONDS.toMillis(180));

        long numTemporaryDocumentItemsBeforeCleanup = temporaryDocumentItemRepository.count();
        assertEquals(temporaryDocumentItems.size(), numTemporaryDocumentItemsBeforeCleanup,
                "initialization of temp document items incorrect");
        int numTemporaryDocumentItemsToExpire = 5;

        assertTrue(numTemporaryDocumentItemsBeforeCleanup > numTemporaryDocumentItemsToExpire,
                "too less items to expire");

        //expire some of them
        long expiredTime = testTimeService.currentTimeMillisUTC() - TimeUnit.SECONDS.toMillis(30);
        List<TemporaryDocumentItem> temporaryDocumentItemsToExpire = new ArrayList<>(numTemporaryDocumentItemsToExpire);
        for (int i = 0; i < numTemporaryDocumentItemsToExpire; i++) {
            TemporaryDocumentItem temporaryDocumentItem = temporaryDocumentItems.get(i);
            temporaryDocumentItem.setExpirationTime(expiredTime);
            TemporaryDocumentItem savedDocumentItem = temporaryDocumentItemRepository.save(temporaryDocumentItem);
            temporaryDocumentItemsToExpire.add(savedDocumentItem);
            log.debug("expired {} to {}", savedDocumentItem, savedDocumentItem.getExpirationTime());
        }

        temporaryDocumentItemRepository.flush();

        log.debug("Cleanup should delete everything with expiration < {}", testTimeService.currentTimeMillisUTC());

        temporaryDocumentItemCleanUpWorker.run();

        //wait until all database triggers were run
        Thread.sleep(200);

        long numTemporaryDocumentItemsAfterCleanup = temporaryDocumentItemRepository.count();

        assertEquals(numTemporaryDocumentItemsBeforeCleanup - numTemporaryDocumentItemsToExpire,
                numTemporaryDocumentItemsAfterCleanup,
                "not all items have been deleted");

        for (TemporaryDocumentItem temporaryDocumentItem : temporaryDocumentItemsToExpire) {
            assertFalse(temporaryDocumentItemRepository.existsById(temporaryDocumentItem.getId()),
                    "item was not deleted");
        }
    }

    private List<TemporaryDocumentItem> createTemporaryDocumentItems(long expirationTime) throws Exception {

        List<TemporaryDocumentItem> createdDocumentItems = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {

            TemporaryDocumentItem temporaryDocumentItem =
                    documentItemService.createTemporaryDocumentItem(th.loadTestResource("document.pdf"),
                            "document.pdf", "Doku", null,
                            FileOwnership.of(th.personRegularAnna));
            temporaryDocumentItem.setExpirationTime(expirationTime);
            TemporaryDocumentItem savedTemporaryDocumentItem =
                    temporaryDocumentItemRepository.save(temporaryDocumentItem);
            log.debug("created {} with expiration time {}",
                    savedTemporaryDocumentItem.getId(), savedTemporaryDocumentItem.getExpirationTime());
            createdDocumentItems.add(savedTemporaryDocumentItem);
        }
        temporaryDocumentItemRepository.flush();
        return createdDocumentItems;
    }

}
