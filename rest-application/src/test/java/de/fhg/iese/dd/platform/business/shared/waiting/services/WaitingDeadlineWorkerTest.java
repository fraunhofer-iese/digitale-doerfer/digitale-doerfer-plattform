/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2021 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.waiting.services;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionExceptionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.shared.waiting.events.TestSharedExpiredEvent;
import de.fhg.iese.dd.platform.business.shared.waiting.worker.WaitingDeadlineWorker;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.model.WaitingDeadline;

public class WaitingDeadlineWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private IWaitingService waitingService;
    @Autowired
    private EventBusAccessor eventBusAccessor;

    @Autowired
    private WaitingDeadlineWorker waitingDeadlineWorker;

    private TestSharedExpiredEvent actualEvent;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();

        eventBusAccessor.registerEventConsumingFunction(TestSharedExpiredEvent.class, this::onTestSharedExpiredEvent,
                EventExecutionStrategy.SYNCHRONOUS_PREFERRED, EventExecutionExceptionStrategy.PROPAGATE_EXCEPTION);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void signalSharedWaitingExpiredEvent() throws Exception {

        TestSharedExpiredEvent expectedEvent = new TestSharedExpiredEvent(
                th.personIeseAdmin,
                th.tenant1,
                5,
                "Testing shared expired event"
        );
        expectedEvent.setDeadline(System.currentTimeMillis() - 1);

        WaitingDeadline waitingDeadline = waitingService.registerDeadline(expectedEvent);

        assertThat(waitingDeadline.getDeadline()).isEqualTo(expectedEvent.getDeadline());

        waitingDeadlineWorker.run();

        waitForEventProcessing();

        assertThat(actualEvent).isNotNull();
        assertThat(actualEvent.getPerson()).isEqualTo(expectedEvent.getPerson());
        assertThat(actualEvent.getTenant()).isEqualTo(expectedEvent.getTenant());
        assertThat(actualEvent.getValue()).isEqualTo(expectedEvent.getValue());
        assertThat(actualEvent.getString()).isEqualTo(expectedEvent.getString());
    }

    private void onTestSharedExpiredEvent(TestSharedExpiredEvent event, EventProcessingContext context) {
        actualEvent = event;
    }

}
