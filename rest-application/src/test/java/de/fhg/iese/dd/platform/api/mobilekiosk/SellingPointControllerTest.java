/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;

/**
 * Tests for the selling point controller.<br>
 * All tests that call {@link BaseMobileKioskTest#createAndConnectDeliveryWithSellingPoint(SellingPoint)} randomly
 * fail and, thus, are deactivated. Reactivate them for manual testing on changes to mobile-kiosk!
 */

public class SellingPointControllerTest extends BaseMobileKioskTest {

    //listActiveCurrentOrFutureSellingPoints

    @Test
    public void listOfAllActiveSellingPointsIsCorrect() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(7)))
            .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
            .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[2].id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[2].vehicleId", is(th.sp5.getVehicle().getId())))
            .andExpect(jsonPath("$[3].id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[3].location.id", is(th.sp6.getLocation().getId())))
            .andExpect(jsonPath("$[4].id", is(th.sp8.getId())))
            .andExpect(jsonPath("$[5].id", is(th.sp9.getId())))
            .andExpect(jsonPath("$[6].id", is(th.sp10.getId())));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
            .param("communityId", th.tenant1.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void listingOfActiveSellingPointsNotPossibleForNotExistingCommunity() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
            .param("communityId", "notEvenAnUUID")
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    //getSellingPointById

    @Test
    public void getSellingPointByIdWorksCorrectly() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/"+th.sp1.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id", is(th.sp1.getId())))
            .andExpect(jsonPath("$.location.id", is(th.sp1.getLocation().getId())))
            .andExpect(jsonPath("$.vehicleId", is(th.sp1.getVehicle().getId())))
            .andExpect(jsonPath("$.status", is(th.sp1.getStatus().toString())));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/"+th.sp7.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id", is(th.sp7.getId())))
            .andExpect(jsonPath("$.location.id", is(th.sp7.getLocation().getId())))
            .andExpect(jsonPath("$.vehicleId", is(th.sp7.getVehicle().getId())))
            .andExpect(jsonPath("$.status", is(th.sp7.getStatus().toString())));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/"+th.sp10.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id", is(th.sp10.getId())))
            .andExpect(jsonPath("$.location.id", is(th.sp10.getLocation().getId())))
            .andExpect(jsonPath("$.vehicleId", is(th.sp10.getVehicle().getId())))
            .andExpect(jsonPath("$.status", is(th.sp10.getStatus().toString())));
    }

    @Test
    public void getSellingPointByIdWithInvalidIdReturnsNotFound() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/notEvenAnUUID")
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    //listAvailableForOrderSellingPoints

    @Test
    public void listOfSellingPointsAvailableForOrderIsCorrect() throws Exception{

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(1).truncatedTo(ChronoUnit.DAYS));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/availableForOrder")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(6)))
            .andExpect(jsonPath("$[0].id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[1].id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].vehicleId", is(th.sp5.getVehicle().getId())))
            .andExpect(jsonPath("$[2].id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].location.id", is(th.sp6.getLocation().getId())))
            .andExpect(jsonPath("$[3].id", is(th.sp8.getId())))
            .andExpect(jsonPath("$[4].id", is(th.sp9.getId())))
            .andExpect(jsonPath("$[5].id", is(th.sp10.getId())));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/availableForOrder")
            .param("communityId", th.tenant1.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));

        timeServiceMock.resetOffset();
    }

    @Test
    public void listingOfSellingPointsAvailableForOrderNotPossibleForNonExistingCommunity() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/availableForOrder")
            .param("communityId", "notEvenAnUUID")
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    //listTodaysSellingPoints
    @Test
    public void listOfSellingPointsOfTodayIsCorrect() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/today")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].id", is(th.sp1.getId())))
            .andExpect(jsonPath("$[1].id", is(th.sp2.getId())))
            .andExpect(jsonPath("$[1].vehicleId", is(th.sp2.getVehicle().getId())))
            .andExpect(jsonPath("$[2].id", is(th.sp3.getId())))
            .andExpect(jsonPath("$[2].location.id", is(th.sp3.getLocation().getId())));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/today")
            .param("communityId", th.tenant1.getId())
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void listingOfSellingPointsOfTodayNotPossibleForNonExistingCommunity() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/today")
            .param("communityId", "notEvenAnUUID")
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    //listSellingPointsWithTransports
    @Disabled
    @Test
    public void listOfSellingPointsWithTransportsIsCorrectForVehicleDriverAndRegularPersonAndSuperAdmin()
            throws Exception {

        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        delivery2ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        deliveryForSP5 = createAndConnectDeliveryWithSellingPoint(th.sp5);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .headers(authHeadersFor(th.personTruckDriver))
                        .contentType(contentType))
                .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(6)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp3.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(0)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(2)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(1)))
            .andExpect(jsonPath("$[3].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[3].transports", hasSize(0)))
            .andExpect(jsonPath("$[4].sellingPoint.id", is(th.sp8.getId())))
            .andExpect(jsonPath("$[4].transports", hasSize(0)))
            .andExpect(jsonPath("$[5].sellingPoint.id", is(th.sp9.getId())))
            .andExpect(jsonPath("$[5].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
            .param("communityId", th.tenant1.getId())
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));

        //test that a non-vehicle-driver gets no information
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .headers(authHeadersFor(th.personRegularKarl))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        //tests that a super admin sees everything, also selling point 10, which is hit by another driver than personTruckDriver
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .headers(authHeadersFor(th.personSuperAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(7)))
                .andExpect(jsonPath("$[6].sellingPoint.id", is(th.sp10.getId())))
                .andExpect(jsonPath("$[6].transports", hasSize(0)));
    }

    @Disabled
    @Test
    public void listingOfSellingPointsWithTransportsNotPossibleForNonExistingCommunity() throws Exception{

        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
                .param("communityId", "notEvenAnUUID")
                .headers(authHeadersFor(th.personTruckDriver))
                .contentType(contentType))
                .andExpect(isException("notEvenAnUUID", ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Disabled
    @Test
    public void listingOfSellingPointsNotPossibleWhenUnauthenticated() throws Exception{

        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);

        assertOAuth2(get("/mobile-kiosk/sellingPoint/withTransports")
                .param("communityId", th.tenant.getId())
                .contentType(contentType));
    }

    //listSellingPointsWithTransportsOnRelativeDate
    @Disabled
    @Test
    public void listOfSellingPointsWithTransportsAndRelativeDateIsCorrectForVehicleDriverAndRegularPersonAndSuperAdmin()
            throws Exception {

        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        delivery2ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        deliveryForSP5 = createAndConnectDeliveryWithSellingPoint(th.sp5);
        deliveryForSP8 = createAndConnectDeliveryWithSellingPoint(th.sp8);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                        .headers(authHeadersFor(th.personTruckDriver))
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .param("relativeDate", "0")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp1.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(0)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp2.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(0)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp3.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "-1")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp11.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "-2")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "1")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(2)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(1)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "1")
            .param("days", "2")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(4)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(2)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(1)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)))
            .andExpect(jsonPath("$[3].sellingPoint.id", is(th.sp8.getId())))
            .andExpect(jsonPath("$[3].transports", hasSize(1)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "1")
            .param("days", "0")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(2)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(1)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "1")
            .param("days", "-1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(2)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(1)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personTruckDriver))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
            .param("relativeDate", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$[0].transports", hasSize(2)))
            .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$[1].transports", hasSize(1)))
            .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$[2].transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
            .headers(authHeadersFor(th.personTruckDriver))
            .param("communityId", th.tenant1.getId())
            .param("relativeDate", "0")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(0)));

        //test that a non-vehicle-driver gets no information
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("relativeDate", "1")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        //tests that also a REST can get the data
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("relativeDate", "1")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].sellingPoint.id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[0].transports", hasSize(2)))
                .andExpect(jsonPath("$[1].sellingPoint.id", is(th.sp5.getId())))
                .andExpect(jsonPath("$[1].transports", hasSize(1)))
                .andExpect(jsonPath("$[2].sellingPoint.id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[2].transports", hasSize(0)));
    }

    @Disabled
    @Test
    public void listingOfSellingPointsWithTransportsAndRelativeDateNotPossibleForNonExistingCommunity() throws Exception{
        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        delivery2ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        deliveryForSP5 = createAndConnectDeliveryWithSellingPoint(th.sp5);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports")
            .headers(authHeadersFor(th.personTruckDriver))
            .param("communityId", "notEvenAnUUID")
            .param("relativeDate", "0")
            .param("days", "1")
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    @Disabled
    @Test
    public void listingOfSellingPointsWithTransportsNotPossibleWhenUnauthenticatedOrWrongBearerToken() throws Exception {
        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("relativeDate", "1")
                .contentType(contentType))
                .andExpect(isUnauthorized());

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/relativeDate")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .param("relativeDate", "1")
                .contentType(contentType))
                .andExpect(isUnauthorized());
    }

    //getSellingPointWithTransportsById
    @Disabled
    @Test
    public void getSellingPointWithTransportsByIdReturnsCorrectSellingPoint() throws Exception{
        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        delivery2ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);
        deliveryForSP5 = createAndConnectDeliveryWithSellingPoint(th.sp5);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/"+th.sp4.getId())
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.sellingPoint.id", is(th.sp4.getId())))
            .andExpect(jsonPath("$.transports", hasSize(2)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/"+th.sp5.getId())
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.sellingPoint.id", is(th.sp5.getId())))
            .andExpect(jsonPath("$.transports", hasSize(1)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/"+th.sp6.getId())
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.sellingPoint.id", is(th.sp6.getId())))
            .andExpect(jsonPath("$.transports", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/"+th.sp1.getId())
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.sellingPoint.id", is(th.sp1.getId())))
            .andExpect(jsonPath("$.transports", hasSize(0)));
    }

    @Disabled
    @Test
    public void getSellingPointWorksForSuperAdmin() throws Exception {
        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/" + th.sp4.getId())
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.sellingPoint.id", is(th.sp4.getId())))
                .andExpect(jsonPath("$.transports", hasSize(1)));
    }

    @Test
    public void getSellingPointWithTransportsByIdWithInvalidIdReturnsNotFound() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/notEvenAnUUID")
            .headers(authHeadersFor(th.personTruckDriver))
            .contentType(contentType))
            .andExpect(status().isNotFound());
    }

    @Disabled
    @Test
    public void getSellingPointWithoutAuthorizationOrWithVehicleDriverRoleForWrongVehicleIsNowAllowed() throws Exception {
        delivery1ForSP4 = createAndConnectDeliveryWithSellingPoint(th.sp4);

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/" + th.sp4.getId())
                .contentType(contentType))
                .andExpect(status().isUnauthorized());

        mockMvc.perform(get("/mobile-kiosk/sellingPoint/withTransports/" + th.sp4.getId())
                        .headers(authHeadersFor(th.personCarDriver))
                        .contentType(contentType))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", containsString("super admin")))
                .andExpect(jsonPath("$.message", containsString("vehicle driver")));
    }

}
