/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;

public class LocationControllerTest extends BaseServiceTest {

    @Autowired
    private MobileKioskTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndVgAdmin();
        th.createPersons();
        th.createShops();
        th.createPushEntities();
        th.createScoreEntities();
        th.createAchievementRules();
        th.createVehiclesAndDrivers();
        th.createSellingPoints();
        th.createLocationHistory();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getRawLocationRecordByIdIsSuccessful() throws Exception{
        performGetRawLocationRecordByIdAndCompareToLocationRecord(th.truckLocationRecords.get(0));
        performGetRawLocationRecordByIdAndCompareToLocationRecord(th.truckLocationRecords.get(3));
        performGetRawLocationRecordByIdAndCompareToLocationRecord(th.truckLocationRecords.get(8));
    }

    private void performGetRawLocationRecordByIdAndCompareToLocationRecord(RawLocationRecord locationRecord) throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/" + locationRecord.getId())
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(locationRecord.getId())))
                .andExpect(jsonPath("$.latitude", is(locationRecord.getLocation().getLatitude())))
                .andExpect(jsonPath("$.longitude", is(locationRecord.getLocation().getLongitude())))
                .andExpect(jsonPath("$.sourceType", is(locationRecord.getSourceType().toString())))
                .andExpect(jsonPath("$.sourceId", is(locationRecord.getSourceId())))
                .andExpect(jsonPath("$.vehicleId", is(locationRecord.getVehicle().getId())))
                .andExpect(jsonPath("$.accuracy", is(locationRecord.getAccuracy())));
    }

    @Test
    public void getRawLocationRecordByIdDoesNotExist() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/notEvenAnUUID")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRawLocationRecordByIdWithUnauthorizedUserIsForbidden() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/"+th.truckLocationRecords.get(0).getId())
                .headers(authHeadersFor(th.personRegularKarl))
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getLatestLocationRecordForVehicleId() throws Exception{
        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp11.getPlannedStay().getStartTime()));
        ensureLatestLocationWorksCorrectly();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp11.getPlannedStay().getStartTime())
                .plus(59, ChronoUnit.MINUTES));
        ensureLatestLocationWorksCorrectly();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp11.getPlannedStay().getStartTime())
                .minus(59, ChronoUnit.MINUTES));
        ensureLatestLocationWorksCorrectly();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp9.getPlannedStay().getEndTime()));
        ensureLatestLocationWorksCorrectly();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp9.getPlannedStay().getEndTime())
                .plus(59, ChronoUnit.MINUTES));
        ensureLatestLocationWorksCorrectly();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp9.getPlannedStay().getEndTime())
                .minus(59, ChronoUnit.MINUTES));
        ensureLatestLocationWorksCorrectly();
    }

    private void ensureLatestLocationWorksCorrectly() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/latest")
                .param("vehicleId", th.truck.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id", is(th.truckLocationRecords.get(8).getId())))
                .andExpect(jsonPath("$.latitude", is(th.truckLocationRecords.get(8).getLocation().getLatitude())))
                .andExpect(jsonPath("$.longitude", is(th.truckLocationRecords.get(8).getLocation().getLongitude())))
                .andExpect(jsonPath("$.sourceType", is(th.truckLocationRecords.get(8).getSourceType().toString())))
                .andExpect(jsonPath("$.sourceId", is(th.truckLocationRecords.get(8).getSourceId())))
                .andExpect(jsonPath("$.vehicleId", is(th.truckLocationRecords.get(8).getVehicle().getId())))
                .andExpect(jsonPath("$.accuracy", is(th.truckLocationRecords.get(8).getAccuracy())));
    }

    @Test
    public void getLatestLocationRecordForVehicleIdFailsDueToOperationHours() throws Exception{

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp11.getPlannedStay().getStartTime())
                .minus(61, ChronoUnit.MINUTES));
        ensureLatestLocationFails();

        timeServiceMock.setOffsetToSetNow(timeServiceMock.toLocalTime(th.sp9.getPlannedStay().getEndTime())
                .plus(61, ChronoUnit.MINUTES));
        ensureLatestLocationFails();
    }

    private void ensureLatestLocationFails() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/latest")
                .param("vehicleId", th.truck.getId())
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getLatestLocationRecordForNonExistingVehicleIdThrowsNotFound() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/location/latest")
                .param("vehicleId", "notEvenAnUUID")
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllRawLocationRecordsIsSuccessful() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.truck.getId())
                        .param("start", "0")
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(9)))
                .andExpect(jsonPath("$[3].id", is(th.truckLocationRecords.get(3).getId())))
                .andExpect(jsonPath("$[3].latitude", is(th.truckLocationRecords.get(3).getLocation().getLatitude())))
                .andExpect(jsonPath("$[3].longitude", is(th.truckLocationRecords.get(3).getLocation().getLongitude())))
                .andExpect(jsonPath("$[3].sourceType", is(th.truckLocationRecords.get(3).getSourceType().toString())))
                .andExpect(jsonPath("$[3].sourceId", is(th.truckLocationRecords.get(3).getSourceId())))
                .andExpect(jsonPath("$[3].vehicleId", is(th.truckLocationRecords.get(3).getVehicle().getId())))
                .andExpect(jsonPath("$[3].accuracy", is(th.truckLocationRecords.get(3).getAccuracy())))
                .andExpect(jsonPath("$[8].id", is(th.truckLocationRecords.get(8).getId())))
                .andExpect(jsonPath("$[8].latitude", is(th.truckLocationRecords.get(8).getLocation().getLatitude())))
                .andExpect(jsonPath("$[8].longitude", is(th.truckLocationRecords.get(8).getLocation().getLongitude())))
                .andExpect(jsonPath("$[8].sourceType", is(th.truckLocationRecords.get(8).getSourceType().toString())))
                .andExpect(jsonPath("$[8].sourceId", is(th.truckLocationRecords.get(8).getSourceId())))
                .andExpect(jsonPath("$[8].vehicleId", is(th.truckLocationRecords.get(8).getVehicle().getId())))
                .andExpect(jsonPath("$[8].accuracy", is(th.truckLocationRecords.get(8).getAccuracy())))
                .andExpect(jsonPath("$[1].id", is(th.truckLocationRecords.get(1).getId())))
                .andExpect(jsonPath("$[1].latitude", is(th.truckLocationRecords.get(1).getLocation().getLatitude())))
                .andExpect(jsonPath("$[1].longitude", is(th.truckLocationRecords.get(1).getLocation().getLongitude())))
                .andExpect(jsonPath("$[1].sourceType", is(th.truckLocationRecords.get(1).getSourceType().toString())))
                .andExpect(jsonPath("$[1].sourceId", is(th.truckLocationRecords.get(1).getSourceId())))
                .andExpect(jsonPath("$[1].vehicleId", is(th.truckLocationRecords.get(1).getVehicle().getId())))
                .andExpect(jsonPath("$[1].accuracy", is(th.truckLocationRecords.get(1).getAccuracy())));

        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.truck.getId())
                        .param("start", String.valueOf(th.truckLocationRecords.get(5).getTimestamp()))
                        .param("end", String.valueOf(th.truckLocationRecords.get(8).getTimestamp()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id", is(th.truckLocationRecords.get(5).getId())))
                .andExpect(jsonPath("$[0].latitude", is(th.truckLocationRecords.get(5).getLocation().getLatitude())))
                .andExpect(jsonPath("$[0].longitude", is(th.truckLocationRecords.get(5).getLocation().getLongitude())))
                .andExpect(jsonPath("$[0].sourceType", is(th.truckLocationRecords.get(5).getSourceType().toString())))
                .andExpect(jsonPath("$[0].sourceId", is(th.truckLocationRecords.get(5).getSourceId())))
                .andExpect(jsonPath("$[0].vehicleId", is(th.truckLocationRecords.get(5).getVehicle().getId())))
                .andExpect(jsonPath("$[0].accuracy", is(th.truckLocationRecords.get(5).getAccuracy())))
                .andExpect(jsonPath("$[1].id", is(th.truckLocationRecords.get(6).getId())))
                .andExpect(jsonPath("$[1].latitude", is(th.truckLocationRecords.get(6).getLocation().getLatitude())))
                .andExpect(jsonPath("$[1].longitude", is(th.truckLocationRecords.get(6).getLocation().getLongitude())))
                .andExpect(jsonPath("$[1].sourceType", is(th.truckLocationRecords.get(6).getSourceType().toString())))
                .andExpect(jsonPath("$[1].sourceId", is(th.truckLocationRecords.get(6).getSourceId())))
                .andExpect(jsonPath("$[1].vehicleId", is(th.truckLocationRecords.get(6).getVehicle().getId())))
                .andExpect(jsonPath("$[1].accuracy", is(th.truckLocationRecords.get(6).getAccuracy())))
                .andExpect(jsonPath("$[2].id", is(th.truckLocationRecords.get(7).getId())))
                .andExpect(jsonPath("$[2].latitude", is(th.truckLocationRecords.get(7).getLocation().getLatitude())))
                .andExpect(jsonPath("$[2].longitude", is(th.truckLocationRecords.get(7).getLocation().getLongitude())))
                .andExpect(jsonPath("$[2].sourceType", is(th.truckLocationRecords.get(7).getSourceType().toString())))
                .andExpect(jsonPath("$[2].sourceId", is(th.truckLocationRecords.get(7).getSourceId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.truckLocationRecords.get(7).getVehicle().getId())))
                .andExpect(jsonPath("$[2].accuracy", is(th.truckLocationRecords.get(7).getAccuracy())))
                .andExpect(jsonPath("$[3].id", is(th.truckLocationRecords.get(8).getId())))
                .andExpect(jsonPath("$[3].latitude", is(th.truckLocationRecords.get(8).getLocation().getLatitude())))
                .andExpect(jsonPath("$[3].longitude", is(th.truckLocationRecords.get(8).getLocation().getLongitude())))
                .andExpect(jsonPath("$[3].sourceType", is(th.truckLocationRecords.get(8).getSourceType().toString())))
                .andExpect(jsonPath("$[3].sourceId", is(th.truckLocationRecords.get(8).getSourceId())))
                .andExpect(jsonPath("$[3].vehicleId", is(th.truckLocationRecords.get(8).getVehicle().getId())))
                .andExpect(jsonPath("$[3].accuracy", is(th.truckLocationRecords.get(8).getAccuracy())));
    }

    @Test
    public void getAllRawLocationRecordsWithUnauthorizedUserIsForbidden() throws Exception{
        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.truck.getId())
                        .param("start", "0")
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personRegularKarl))
                .contentType(contentType))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getAllRawLocationRecordsIsSuccessfulIfNoDataIsAvailable() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.car.getId())
                        .param("start", "0")
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));

        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", th.truck.getId())
                        .param("start", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAllRawLocationRecordsWithNotExistentVehicleIdThrowsNotFound() throws Exception {
        mockMvc.perform(get("/mobile-kiosk/location/")
                        .param("vehicleId", "notEvenAnUUID")
                        .param("start", "0")
                        .param("end", String.valueOf(timeServiceMock.currentTimeMillisUTC()))
                .headers(authHeadersFor(th.personVgAdminInTestTenant))
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

}
