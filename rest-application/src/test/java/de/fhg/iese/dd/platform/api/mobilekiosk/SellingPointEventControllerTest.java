/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointForDeliveryRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointsRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientUpdatedSellingPoint;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IDeliveryToSellingPointService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.html2pdf.services.IHtml2PdfService;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.DeliveryRepository;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;

/**
 * Tests for the selling point event controller.<br>
 * All tests that call {@link BaseMobileKioskTest#createAndConnectDeliveryWithSellingPoint(SellingPoint)} and
 * {@link BaseMobileKioskTest#markDeliveryAsReadyForTransport(Delivery)} randomly fail and, thus,
 * are deactivated. Reactivate them for manual testing on changes to mobile-kiosk!
 */
public class SellingPointEventControllerTest extends BaseMobileKioskTest {

    @Autowired
    private IDeliveryToSellingPointService deliveryToSellingPointService;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @SpyBean
    private IHtml2PdfService html2PdfServiceSpy;

    @Autowired
    private TestEmailSenderService testEmailSenderService;

    @Override
    public void createEntities() throws Exception {
        super.createEntities();
    }

    //=====================================================================================================
    //= TESTS USING API KEYS                                                                              =
    //=====================================================================================================

    //UpdateSellingPoints

    @Test
    public void sellingPointsAreUnchangedWhenSubmittedAgainExactlyTheSame() throws Exception {

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp8),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp9),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp10)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        assertThatSellingPointsAreUnchanged();
    }

    @Test
    public void updateOfOneNewSellingPointIsCorrect() throws Exception {

        final MockHttpServletRequestBuilder preparedRequestBuilder =
                post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID));

        waitForEventProcessing();

        assertThatUpdateOfOneNewSellingPointIsCorrect(preparedRequestBuilder);
    }

    @Test
    public void updateOfNewSellingPointWithProvidedGpsIsCorrect() throws Exception {

        TimeSpan plannedStay = th.getSellingPointStayOnDay(timeService.nowLocal(), 2, 2, 2);
        ClientUpdatedSellingPoint csp8 = ClientUpdatedSellingPoint.builder()
                .location(ClientAddress.builder()
                        .name("TestOrt")
                        .street("Unsinn-Straße 0")
                        .city("Quatschstadt")
                        .zip("00000")
                        .gpsLocation(new ClientGPSLocation(1, 2))
                        .build())
                .vehicleId(th.truck.getId())
                .vehicleName(th.truck.getName())
                .plannedArrival(plannedStay.getStartTime())
                .plannedDeparture(plannedStay.getEndTime())
                .build();

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        csp8))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[2].id", is(th.sp5.getId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.sp5.getVehicle().getId())))
                .andExpect(jsonPath("$[3].id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[3].location.id", is(th.sp6.getLocation().getId())))
                .andExpect(jsonPath("$[4].location.name", is(csp8.getLocation().getName())))
                .andExpect(jsonPath("$[4].location.street", is(csp8.getLocation().getStreet())))
                .andExpect(jsonPath("$[4].location.city", is(csp8.getLocation().getCity())))
                .andExpect(jsonPath("$[4].location.zip", is(csp8.getLocation().getZip())))
                .andExpect(jsonPath("$[4].location.gpsLocation.latitude",
                        is(csp8.getLocation().getGpsLocation().getLatitude())))
                .andExpect(jsonPath("$[4].location.gpsLocation.longitude",
                        is(csp8.getLocation().getGpsLocation().getLongitude())))
                .andExpect(jsonPath("$[4].vehicleId", is(csp8.getVehicleId())))
                .andExpect(jsonPath("$[4].plannedDeparture", is(csp8.getPlannedDeparture())))
                .andExpect(jsonPath("$[4].plannedArrival", is(csp8.getPlannedArrival())));
    }

    @Test
    public void reactivateAlreadyDeletedSellingPointByResubmittingIt() throws Exception {

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp7)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[2].id", is(th.sp5.getId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.sp5.getVehicle().getId())))
                .andExpect(jsonPath("$[3].id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[3].location.id", is(th.sp6.getLocation().getId())))
                .andExpect(jsonPath("$[4].id", is(th.sp7.getId())))
                .andExpect(jsonPath("$[4].location.id", is(th.sp7.getLocation().getId())))
                .andExpect(jsonPath("$[4].vehicleId", is(th.sp7.getVehicle().getId())))
                .andExpect(jsonPath("$[4].plannedDeparture", is(th.sp7.getPlannedStay().getEndTime())))
                .andExpect(jsonPath("$[4].plannedArrival", is(th.sp7.getPlannedStay().getStartTime())));
    }

    @Test
    public void deleteSellingPointByNotResubmittingIt() throws Exception {

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[1].vehicleId", is(th.sp4.getVehicle().getId())));
    }

    @Disabled
    @Test
    public void deleteSellingPointByNotResubmittingItAndExpectEmailToBeSent() throws Exception {

        testEmailSenderService.clearMailList();
        //prepare delivery for SP5
        deliveryForSP5 = createAndConnectDeliveryWithSellingPoint(th.sp5);

        //update without SP5 -> delete SP5
        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp8),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp9),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp10)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(6)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[2].id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[2].location.id", is(th.sp6.getLocation().getId())))
                .andExpect(jsonPath("$[3].id", is(th.sp8.getId())))
                .andExpect(jsonPath("$[4].id", is(th.sp9.getId())))
                .andExpect(jsonPath("$[5].id", is(th.sp10.getId())));

        assertEquals(1,
                testEmailSenderService.getEmails()
                        .stream()
                        .filter(testEmail -> testEmail.getRecipientEmailAddress().equals(
                                deliveryForSP5.getSender().getShop().getEmail()))
                        .count());

        assertEquals(1,
                testEmailSenderService.getEmails()
                        .stream()
                        .filter(testEmail ->
                                testEmail.getRecipientEmailAddress().equals(
                                        deliveryForSP5.getReceiver().getPerson().getEmail())
                                        && "Wichtiger Hinweis zu deiner Bestellung".equals(testEmail.getSubject())
                                        && testEmail.getText().contains(
                                        "Hallo " + deliveryForSP5.getReceiver().getPerson().getFirstName())
                                        && testEmail.getText().contains(
                                        "Dein Team vom " + deliveryForSP5.getSender().getShop().getName())
                                        && !testEmail.getText().contains("${")
                        )
                        .count());
    }

    @Test
    public void changeVehicleOfOneSellingPoint() throws Exception {

        ClientUpdatedSellingPoint csp4 = th.getClientUpdatedSellingPointForSellingPoint(th.sp4);
        csp4.setVehicleId("newVehicle");
        csp4.setVehicleName("kleiner LKW");

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        csp4,
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[1].vehicleId", is(csp4.getVehicleId())))
                .andExpect(jsonPath("$[1].vehicleName", is(csp4.getVehicleName())))
                .andExpect(jsonPath("$[2].id", is(th.sp5.getId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.sp5.getVehicle().getId())))
                .andExpect(jsonPath("$[3].id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[3].location.id", is(th.sp6.getLocation().getId())));
    }

    @Test
    public void updateSellingPointFailsWhenNotAuthenticated() throws Exception {

        assertThatSellingPointsAreUnchanged();

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Collections.singletonList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .header(HEADER_NAME_API_KEY, "wrongApiKey")
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isUnauthorized());

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, config.getApiKeyByTenantId().get("notEvenAnUUID"))
                        .content(json(request)))
                .andExpect(status().isUnauthorized());

        assertThatSellingPointsAreUnchanged();
    }

    @Test
    public void invalidSellingPointsCannotBeSubmitted() throws Exception {

        assertThatSellingPointsAreUnchanged();

        @SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4)))
                .build();

        request.getSellingPoints().get(0).setLocation(null);
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Location");
        assertThatSellingPointsAreUnchanged();

        request.getSellingPoints().set(0, th.getClientUpdatedSellingPointForSellingPoint(th.sp4));
        request.getSellingPoints().get(0).getLocation().setCity("");
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Location");
        assertThatSellingPointsAreUnchanged();

        request.getSellingPoints().set(0, th.getClientUpdatedSellingPointForSellingPoint(th.sp4));
        request.getSellingPoints().get(0).getLocation().setStreet(null);
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Location");
        assertThatSellingPointsAreUnchanged();

        request.getSellingPoints().set(0, th.getClientUpdatedSellingPointForSellingPoint(th.sp4));
        request.getSellingPoints().get(0).setVehicleId(null);
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Vehicle");
        assertThatSellingPointsAreUnchanged();

        request.getSellingPoints().set(0, th.getClientUpdatedSellingPointForSellingPoint(th.sp4));
        request.getSellingPoints().get(0).setVehicleId("invalidVehicle");
        request.getSellingPoints().get(0).setVehicleName(null);
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Vehicle");
        assertThatSellingPointsAreUnchanged();

        request.getSellingPoints().set(0, th.getClientUpdatedSellingPointForSellingPoint(th.sp4));
        request.getSellingPoints().get(0).setPlannedDeparture(
                request.getSellingPoints().get(0).getPlannedArrival() - 1);
        performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(request, "Arrival is after departure");
        assertThatSellingPointsAreUnchanged();
    }

    @Test
    public void sellingPointsCannotBeUpdatedForAnUnknownCommunity() throws Exception {

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId("notEvenAnUUID")
                .sellingPoints(Collections.singletonList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4)))
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, config.getApiKeyByTenantId().get("notEvenAnUUID"))
                        .content(json(request)))
                .andExpect(status().isNotFound());
    }

    //ConnectDeliveryWithSellingPoint
    @Disabled
    @Test
    public void connectionOfSellingPointWithDeliveryWorksCorrectly() throws Exception {

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);

        connectDeliveryWithSellingPoint(delivery1ForSP4, th.sp4);

        timeServiceMock.resetOffset();
    }

    @Disabled
    @Test
    public void connectionOfSellingPointWithDeliveryWorksCorrectlyAndGeneratesQRCodeLabel() throws Exception {

        //standard test case
        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);

        connectDeliveryWithSellingPoint(delivery1ForSP4, th.sp4);

        //check if qrcode label was generated and contains all necessary data
        final Delivery delivery = deliveryService.refresh(delivery1ForSP4);
        Mockito.verify(html2PdfServiceSpy).convert(ArgumentMatchers.argThat(argument ->
                        argument.contains(delivery.getTrackingCode())
                                && argument.contains(th.sp4.getLocation().getStreet())
                                && argument.contains(th.sp4.getLocation().getCity())
                                && argument.contains(delivery.getReceiver().getPerson().getFirstName())
                                && argument.contains(delivery.getReceiver().getPerson().getLastName())),
                ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt());

        timeServiceMock.resetOffset();
    }

    @Disabled
    @Test
    public void connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates()
            throws Exception {

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);

        connectDeliveryWithSellingPoint(delivery1ForSP4, th.sp4);

        markDeliveryAsReadyForTransport(delivery1ForSP4);

        assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery1ForSP4).getCarrier(),
                th.personTruckDriver);

        Delivery updatedDelivery = deliveryService.refresh(delivery1ForSP4);
        assertThatDeliveryHasTimeAndAddressFromSellingPoint(updatedDelivery, th.sp4);

        timeServiceMock.resetOffset();
    }

    @Disabled
    @Test
    public void connectionOfDeliveryWithSellingPointNotPossibleWhenTooLate() throws Exception {

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);

        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp4.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isBadRequest());

        assertFalse(deliveryToSellingPointService.existsForDelivery(delivery1ForSP4));
    }

    @Disabled
    @Test
    public void connectionOfDeliveryWithSellingPointNotPossibleWhenUnauthenticated() throws Exception {

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);
        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp4.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, "invalidApiKey")
                        .content(json(request)))
                .andExpect(status().isUnauthorized());

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isBadRequest());

        timeServiceMock.resetOffset();
    }

    @Test
    public void connectionOfDeliveryWithSellingPointNotPossibleWhenDeliveryNotExisting() throws Exception {

        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId("notEvenAnUUID")
                .sellingPointId(th.sp5.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isNotFound());
    }

    @Disabled
    @Test
    public void connectionOfDeliveryWithSellingPointNotPossibleWhenSellingPointNotExisting() throws Exception {

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);
        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId("notEvenAnUUID")
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isNotFound());
    }

    //UpdateSellingPointForDelivery

    @Disabled
    @Test
    public void deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToSameVehicleAndThusSameDriver()
            throws Exception {

        final MockHttpServletRequestBuilder preparedRequestBuilder =
                post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1());

        deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToSameVehicleAndThusSameDriver(preparedRequestBuilder);
    }

    private void deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToSameVehicleAndThusSameDriver(
            MockHttpServletRequestBuilder preparedRequestBuilder) throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        //same vehicle
        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp5.getId())
                .build();

        mockMvc.perform(preparedRequestBuilder
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId", is(delivery1ForSP4.getId())));

        waitForEventProcessing();

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery1ForSP4));
        assertEquals(deliveryToSellingPointService.findByDelivery(delivery1ForSP4).getSellingPoint(), th.sp5);

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, delivery1ForSP4);

        delivery1ForSP4 = deliveryService.refresh(delivery1ForSP4);

        assertThatDeliveryHasTimeAndAddressFromSellingPoint(delivery1ForSP4, th.sp5);

        assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery1ForSP4).getCarrier(),
                th.personTruckDriver);
    }

    @Disabled
    @Test
    public void deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToOtherVehicleAndThusOtherDriver()
            throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        //different vehicle
        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp10.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId", is(delivery1ForSP4.getId())));

        waitForEventProcessingLong();

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery1ForSP4));
        assertEquals(deliveryToSellingPointService.findByDelivery(delivery1ForSP4).getSellingPoint(), th.sp10);

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, delivery1ForSP4);

        delivery1ForSP4 = deliveryService.refresh(delivery1ForSP4);

        assertThatDeliveryHasTimeAndAddressFromSellingPoint(delivery1ForSP4, th.sp10);

        assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery1ForSP4).getCarrier(),
                th.personCarDriver);

        timeServiceMock.resetOffset();
    }

    @Disabled
    @Test
    public void sellingPointIsUpdatedForDeliveryBeforeReadyForTransportWithNoFurtherActions() throws Exception {

        connectionOfSellingPointWithDeliveryWorksCorrectly();
        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        //same vehicle
        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp5.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId", is(delivery1ForSP4.getId())));

        waitForEventProcessing();

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery1ForSP4));
        assertEquals(deliveryToSellingPointService.findByDelivery(delivery1ForSP4).getSellingPoint(), th.sp5);

        assertThatDeliveryIsInStatus(DeliveryStatus.CREATED, delivery1ForSP4);

        timeServiceMock.resetOffset();
    }

    @Disabled
    @Test
    public void sellingPointCannotBeUpdatedForDeliveryWhenTooLate() throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp5.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isBadRequest());
    }

    @Disabled
    @Test
    public void sellingPointCannotBeUpdatedForDeliveryWhenUnauthenticated() throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp4.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, "invalidApiKey")
                        .content(json(request)))
                .andExpect(status().isUnauthorized());

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isBadRequest());
    }

    @Disabled
    @Test
    public void sellingPointCannotBeUpdatedForDeliveryWhenDeliveryDoesNotExist() throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId("notEvenAnUUID")
                .sellingPointId(th.sp5.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isNotFound());

    }

    @Disabled
    @Test
    public void sellingPointCannotBeUpdatedForDeliveryWhenSellingPointDoesNotExist() throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId("notEvenAnUUID")
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointForDeliveryRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isNotFound());

    }

    @Disabled
    @Test
    public void markConnectedDeliveryAsReadyForTransportWithMultipleParcelsAndExpectAllDeliveriesToBeCreatedCorrectly()
            throws Exception {

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        delivery1ForSP4 = createDeliveryForSellingPoint(th.sp4);
        String shopOrderId = delivery1ForSP4.getCustomReferenceNumber();

        connectDeliveryWithSellingPoint(delivery1ForSP4, th.sp4);

        //check if qrcode label was generated and contains all necessary data
        final Delivery delivery = deliveryService.findDeliveryById(delivery1ForSP4.getId());
        Mockito.verify(html2PdfServiceSpy).convert(ArgumentMatchers.argThat(argument ->
                        argument.contains(delivery.getTrackingCode())
                                && argument.contains(th.sp4.getLocation().getStreet())
                                && argument.contains(th.sp4.getLocation().getCity())
                                && argument.contains(delivery.getReceiver().getPerson().getFirstName())
                                && argument.contains(delivery.getReceiver().getPerson().getLastName())),
                ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt());

        Mockito.reset(html2PdfServiceSpy);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest =
                sendPurchaseOrderReadyForTransportRequest(shopOrderId, th.sp4.getLocation(), 2);

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, delivery1ForSP4);

        assertEquals(transportAssignmentService.getLatestTransportAssignment(delivery1ForSP4).getCarrier(),
                th.personTruckDriver);

        Delivery updatedDelivery = deliveryService.refresh(delivery1ForSP4);
        assertThatDeliveryHasTimeAndAddressFromSellingPoint(updatedDelivery, th.sp4);

        List<Delivery> deliveries = th.deliveryRepository.findAllByCustomReferenceNumber(shopOrderId);
        assertEquals(3, deliveries.size());

        Optional<Delivery> additionalDelivery1 = deliveries.stream()
                .filter(d -> d.getContentNotes().equals(
                        purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(0).getContentNotes()))
                .findFirst();
        assertTrue(additionalDelivery1.isPresent());
        assertTrue(deliveryToSellingPointService.existsForDelivery(additionalDelivery1.get()));
        assertEquals(th.sp4, deliveryToSellingPointService.findByDelivery(additionalDelivery1.get()).getSellingPoint());

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, additionalDelivery1.get());

        assertEquals(
                transportAssignmentService.getLatestTransportAssignment(additionalDelivery1.get()).getCarrier(),
                th.personTruckDriver);

        Optional<Delivery> additionalDelivery2 = deliveries.stream()
                .filter(d -> d.getContentNotes().equals(
                        purchaseOrderReadyForTransportRequest.getAdditionalParcels().get(1).getContentNotes()))
                .findFirst();
        assertTrue(additionalDelivery2.isPresent());
        assertTrue(deliveryToSellingPointService.existsForDelivery(additionalDelivery2.get()));
        assertEquals(th.sp4, deliveryToSellingPointService.findByDelivery(additionalDelivery2.get()).getSellingPoint());

        assertThatDeliveryIsInStatus(DeliveryStatus.IN_DELIVERY, additionalDelivery2.get());

        assertEquals(transportAssignmentService.getLatestTransportAssignment(additionalDelivery2.get()).getCarrier(),
                th.personTruckDriver);

        Mockito.verify(html2PdfServiceSpy, times(2)).convert(ArgumentMatchers.argThat(
                argument -> {
                    Delivery delivery1 = deliveryService.findDeliveryById(additionalDelivery1.get().getId());
                    Delivery delivery2 = deliveryService.findDeliveryById(additionalDelivery2.get().getId());
                    if (argument.contains(delivery1.getTrackingCode())
                            && argument.contains(th.sp4.getLocation().getStreet())
                            && argument.contains(th.sp4.getLocation().getCity())
                            && argument.contains(delivery1ForSP4.getReceiver().getPerson().getFirstName())
                            && argument.contains(delivery1ForSP4.getReceiver().getPerson().getLastName())) {
                        return true;
                    }
                    return argument.contains(delivery2.getTrackingCode())
                            && argument.contains(th.sp4.getLocation().getStreet())
                            && argument.contains(th.sp4.getLocation().getCity())
                            && argument.contains(delivery1ForSP4.getReceiver().getPerson().getFirstName())
                            && argument.contains(delivery1ForSP4.getReceiver().getPerson().getLastName());

                }),
                ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt());

        timeServiceMock.resetOffset();
    }

    //=====================================================================================================
    //= TESTS USING PERSONS INSTEAD OF API KEYS                                                          =
    //=====================================================================================================

    @Test
    public void updateOfOneNewSellingPointIsCorrectWhenCalledByVgAdmin() throws Exception {

        final MockHttpServletRequestBuilder preparedRequestBuilder =
                post("/mobile-kiosk/event/updateSellingPointsRequest")
                        .headers(authHeadersFor(th.personVgAdminInTestTenant));

        waitForEventProcessing();

        assertThatUpdateOfOneNewSellingPointIsCorrect(preparedRequestBuilder);
    }

    @Disabled
    @Test
    public void updateSellingPointAsPersonFailsWhenNotAuthenticatedOrNotAuthorized() throws Exception {

        assertThatSellingPointsAreUnchanged();

        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Collections.singletonList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4)))
                .build();

        assertOAuth2(post("/mobile-kiosk/event/updateSellingPointsRequest")
                .contentType(contentType)
                .content(json(request)));

        mockMvc.perform(post("/mobile-kiosk/event/updateSellingPointsRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isForbidden());

        assertThatSellingPointsAreUnchanged();
    }

    @Disabled
    @Test
    public void deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToSameVehicleCalledByVgAdmin()
            throws Exception {

        final MockHttpServletRequestBuilder preparedRequestBuilder =
                post("/mobile-kiosk/event/updateSellingPointForDeliveryRequest")
                        .headers(authHeadersFor(th.personVgAdminInTestTenant));

        deliveryGetsNewSellingPointWorksCorrectlyWhenPointBelongsToSameVehicleAndThusSameDriver(preparedRequestBuilder);
    }

    @Disabled
    @Test
    public void sellingPointCannotBeUpdatedForDeliveryWhenUnauthenticatedOrUnauthorizedAsPersonOrDeliveryHasWrongCommunity()
            throws Exception {

        connectionOfSellingPointWithDeliveryReadyForTransportWorksCorrectlyAndTriggersDeliveryUpdates();

        timeServiceMock.setOffsetToSetNow(ZonedDateTime.now(timeService.getLocalTimeZone()).minusDays(10));

        ClientUpdateSellingPointForDeliveryRequest request = ClientUpdateSellingPointForDeliveryRequest.builder()
                .deliveryId(delivery1ForSP4.getId())
                .sellingPointId(th.sp4.getId())
                .build();

        assertOAuth2(post("/mobile-kiosk/event/updateSellingPointForDeliveryRequest")
                .contentType(contentType)
                .content(json(request)));

        mockMvc.perform(post("/mobile-kiosk/event/updateSellingPointForDeliveryRequest")
                        .headers(authHeadersFor(th.personRegularKarl))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isForbidden());

        final Delivery deliveryForOtherCommunity = deliveryRepository.findById(delivery1ForSP4.getId()).get();
        assertNotEquals(th.tenant, th.tenant1);
        deliveryForOtherCommunity.setTenant(th.tenant1);
        deliveryRepository.save(deliveryForOtherCommunity);

        ClientUpdateSellingPointForDeliveryRequest requestWithNotMatchingCommunity =
                ClientUpdateSellingPointForDeliveryRequest.builder()
                        .deliveryId(deliveryForOtherCommunity.getId())
                        .sellingPointId(th.sp4.getId())
                        .build();

        mockMvc.perform(post("/mobile-kiosk/event/updateSellingPointForDeliveryRequest")
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personVgAdminInTestTenant))
                        .content(json(requestWithNotMatchingCommunity)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", containsString("must belong to the same community")));
    }

    private void assertThatUpdateOfOneNewSellingPointIsCorrect(MockHttpServletRequestBuilder preparedRequestBuilder)
            throws Exception {

        TimeSpan plannedStay = th.getSellingPointStayOnDay(timeService.nowLocal(), 2, 2, 2);
        ClientUpdatedSellingPoint csp8 = ClientUpdatedSellingPoint.builder()
                .location(ClientAddress.builder()
                        .id(th.erbendorf.getId())
                        .name(th.erbendorf.getName())
                        .street(th.erbendorf.getStreet())
                        .zip(th.erbendorf.getZip())
                        .city(th.erbendorf.getCity())
                        .gpsLocation(new ClientGPSLocation(th.erbendorf.getGpsLocation().getLatitude(),
                                th.erbendorf.getGpsLocation().getLongitude()))
                        .build())
                .vehicleId(th.truck.getId())
                .vehicleName(th.truck.getName())
                .plannedArrival(plannedStay.getStartTime())
                .plannedDeparture(plannedStay.getEndTime())
                .build();
        ClientUpdateSellingPointsRequest request = ClientUpdateSellingPointsRequest.builder()
                .communityId(MobileKioskTestHelper.TEST_TENANT_ID)
                .sellingPoints(Arrays.asList(
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp1),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp2),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp3),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp4),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp5),
                        th.getClientUpdatedSellingPointForSellingPoint(th.sp6),
                        csp8))
                .build();

        mockMvc.perform(preparedRequestBuilder
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].id").value(th.sp3.getId()))
                .andExpect(jsonPath("$[1].id").value(th.sp4.getId()))
                .andExpect(jsonPath("$[2].id").value(th.sp5.getId()))
                .andExpect(jsonPath("$[2].vehicleId").value(th.sp5.getVehicle().getId()))
                .andExpect(jsonPath("$[3].id").value(th.sp6.getId()))
                .andExpect(jsonPath("$[3].location.id").value(th.sp6.getLocation().getId()))
                .andExpect(jsonPath("$[4].location.id").value(csp8.getLocation().getId()))
                .andExpect(jsonPath("$[4].vehicleId").value(csp8.getVehicleId()))
                .andExpect(jsonPath("$[4].plannedDeparture").value(csp8.getPlannedDeparture()))
                .andExpect(jsonPath("$[4].plannedArrival").value(csp8.getPlannedArrival()));
    }

    private void performUpdateSellingPointsRequestAndExpectBadRequestWithMessagePart(
            ClientUpdateSellingPointsRequest request, String messagePart) throws Exception {

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/updateSellingPointsRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY,
                                config.getApiKeyByTenantId().get(MobileKioskTestHelper.TEST_TENANT_ID))
                        .content(json(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", containsString(messagePart)));
    }

    private void assertThatSellingPointsAreUnchanged() throws Exception {

        mockMvc.perform(get("/mobile-kiosk/sellingPoint")
                .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(7)))
                .andExpect(jsonPath("$[0].id", is(th.sp3.getId())))
                .andExpect(jsonPath("$[1].id", is(th.sp4.getId())))
                .andExpect(jsonPath("$[2].id", is(th.sp5.getId())))
                .andExpect(jsonPath("$[2].vehicleId", is(th.sp5.getVehicle().getId())))
                .andExpect(jsonPath("$[3].id", is(th.sp6.getId())))
                .andExpect(jsonPath("$[3].location.id", is(th.sp6.getLocation().getId())))
                .andExpect(jsonPath("$[4].id", is(th.sp8.getId())))
                .andExpect(jsonPath("$[5].id", is(th.sp9.getId())))
                .andExpect(jsonPath("$[6].id", is(th.sp10.getId())));
    }

    private void assertThatDeliveryHasTimeAndAddressFromSellingPoint(Delivery delivery, SellingPoint sellingPoint) {

        Address deliveryAddress = delivery.getDeliveryAddress().getAddress();
        Address sellingPointLocation = sellingPoint.getLocation();

        assertThat(deliveryAddress.getName()).isEqualTo(sellingPointLocation.getName());
        assertThat(deliveryAddress.getStreet()).isEqualTo(sellingPointLocation.getStreet());
        assertThat(deliveryAddress.getCity()).isEqualTo(sellingPointLocation.getCity());
        assertThat(deliveryAddress.getZip()).isEqualTo(sellingPointLocation.getZip());
        assertThat(deliveryAddress.getGpsLocation()).isEqualTo(sellingPointLocation.getGpsLocation());

        assertThat(delivery.getDesiredDeliveryTime()).isEqualTo(sellingPoint.getPlannedStay());
    }

    private void connectDeliveryWithSellingPoint(Delivery delivery, SellingPoint sellingPoint) throws Exception {

        ClientConnectDeliveryWithSellingPointRequest request = ClientConnectDeliveryWithSellingPointRequest.builder()
                .deliveryId(delivery.getId())
                .sellingPointId(sellingPoint.getId())
                .build();

        mockMvc.perform(post("/mobile-kiosk/event/apiKey/connectDeliveryWithSellingPointRequest")
                        .param("communityId", MobileKioskTestHelper.TEST_TENANT_ID)
                        .contentType(contentType)
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId", is(delivery.getId())));

        waitForEventProcessingLong();

        assertTrue(deliveryToSellingPointService.existsForDelivery(delivery));

        assertThatDeliveryIsInStatus(DeliveryStatus.CREATED, delivery);
    }

}
