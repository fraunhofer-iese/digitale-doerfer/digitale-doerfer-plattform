/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class LastNameShorteningModifierTest {

    @Test
    public void trimToInitial() {
        assertThat(shorten(null)).isEqualTo(null);
        assertThat(shorten("")).isEqualTo("");
        assertThat(shorten(" ")).isEqualTo(" ");
        assertThat(shorten("a ")).isEqualTo("A.");
        assertThat(shorten(" a")).isEqualTo("A.");
        assertThat(shorten(" a ")).isEqualTo("A.");
        assertThat(shorten("Meyer")).isEqualTo("M.");
        assertThat(shorten(" Schmidt-Huber")).isEqualTo("S.");
        assertThat(shorten("Desch Pschorrn ")).isEqualTo("D.");
        assertThat(shorten("Hinterobermittelweitdörferhuberstädter")).isEqualTo("H.");
    }

}
