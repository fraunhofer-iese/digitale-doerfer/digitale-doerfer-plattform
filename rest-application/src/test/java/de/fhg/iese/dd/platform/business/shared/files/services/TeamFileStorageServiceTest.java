/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import com.github.sardine.Sardine;
import com.github.sardine.impl.SardineException;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.FileStorageException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.TeamFileStorageConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TeamFileNotFoundException;

@ActiveProfiles({"test", "TeamFileStorageServiceTest"})
public class TeamFileStorageServiceTest extends BaseServiceTest {

    @Autowired
    private TeamFileStorageService testService;

    @Autowired
    private TeamFileStorageConfig config;

    @Mock
    private Sardine sardineMock;

    @Override
    public void createEntities() throws Exception {
        testService.sardineSupplier = () -> sardineMock;
        //we want to always start with a fresh mock
        testService.sardine = null;
    }

    @Override
    public void tearDown() throws Exception {

    }

    @Test
    public void saveFile() throws Exception {

        byte[] bytes = "Test File Content".getBytes();
        String fileName = "testPlain.txt";
        String contentType = "text/plain";
        String url = config.getWebDavUrl() + "/" + fileName;

        testService.saveFile(bytes, contentType, fileName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).put(url, bytes, contentType);
    }

    @Test
    public void saveFile_NotExistingDirectory() throws Exception {

        String dir1Name = "folter1";
        String urlDir1 = config.getWebDavUrl() + "/" + dir1Name;

        byte[] bytes = "Test File Content".getBytes();
        String fileName = dir1Name + "/testPlain.txt";
        String contentType = "text/plain";
        String url = config.getWebDavUrl() + "/" + fileName;

        Mockito.doThrow(new SardineException("Folders need to be created separately", 409, "No convenience allowed"))
                .when(sardineMock).put(Mockito.eq(url), (byte[]) Mockito.any(), Mockito.any());

        Mockito.doAnswer(invocation -> {
            //when dir1 is created the file can be created without exception
            Mockito.doNothing().when(sardineMock).put(Mockito.eq(url), (byte[]) Mockito.any(), Mockito.any());
            return null;
        }).when(sardineMock).createDirectory(urlDir1);

        testService.saveFile(bytes, contentType, fileName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(2)).put(url, bytes, contentType);
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(urlDir1);
    }

    @Test
    public void saveFile_Exception() throws Exception {

        byte[] bytes = "Test File Content".getBytes();
        String fileName = "testPlain.txt";
        String contentType = "text/plain";
        String url = config.getWebDavUrl() + "/" + fileName;

        Mockito.doThrow(IOException.class).when(sardineMock).put(Mockito.any(), (byte[]) Mockito.any(), Mockito.any());

        try {
            testService.saveFile(bytes, contentType, fileName);
            fail("No exception thrown");
        } catch (FileStorageException e) {
            assertTrue(e.getMessage().contains(fileName));
            assertTrue(e.getMessage().contains(url));
        }

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).put(url, bytes, contentType);
    }

    @Test
    public void getFile() throws Exception {

        String fileName = "testPlain123.txt";
        String url = config.getWebDavUrl() + "/" + fileName;
        Mockito.when(sardineMock.get(url)).thenReturn(new ByteArrayInputStream("Test File Content".getBytes()));

        testService.getFile(fileName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).get(url);
    }

    @Test
    public void getFile_NotExistingFile() throws Exception {

        String fileName = "testPlain123.txt";
        String url = config.getWebDavUrl() + "/" + fileName;
        Mockito.doThrow(new SardineException("Not found file throws exceptions", 404, "Not Found"))
                .when(sardineMock).get(url);

        try {
            testService.getFile(fileName);
            fail("No exception thrown");
        } catch (TeamFileNotFoundException e) {
            assertTrue(e.getMessage().contains(fileName));
            assertTrue(e.getMessage().contains(url));
        }

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).get(url);
    }

    @Test
    public void getFile_Exception() throws Exception {

        String fileName = "testPlain123.txt";
        String url = config.getWebDavUrl() + "/" + fileName;
        Mockito.doThrow(new SardineException("No entry!", 402, "Insert coin to view file"))
                .when(sardineMock)
                .get(url);

        try {
            testService.getFile(fileName);
            fail("No exception thrown");
        } catch (FileStorageException e) {
            assertTrue(e.getMessage().contains(fileName));
            assertTrue(e.getMessage().contains(url));
        }

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).get(url);
    }

    @Test
    public void createDirectory() throws Exception {

        String directoryName = "folter";
        String url = config.getWebDavUrl() + "/" + directoryName;

        testService.createDirectory(directoryName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(url);
    }

    @Test
    public void createDirectory_Slash() throws Exception {

        String directoryName = "folter";
        String url = config.getWebDavUrl() + "/" + directoryName;

        testService.createDirectory(directoryName + "/");

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(url);
    }

    @Test
    public void createDirectory_Existing() throws Exception {

        String directoryName = "folter1";
        String url = config.getWebDavUrl() + "/" + directoryName;

        Mockito.doThrow(new SardineException("Already existing folders throw exceptions", 405, "Existing"))
                .when(sardineMock).createDirectory(url);

        testService.createDirectory(directoryName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(url);
    }

    @Test
    public void createDirectory_Multiple() throws Exception {

        String dir1Name = "folter1";
        String dir2Name = "folter2";
        String dir3Name = "folter3";
        String dir123Name = dir1Name + "/" + dir2Name + "/" + dir3Name;
        String urlDir1 = config.getWebDavUrl() + "/" + dir1Name;
        String urlDir2 = config.getWebDavUrl() + "/" + dir1Name + "/" + dir2Name;
        String urlDir3 = config.getWebDavUrl() + "/" + dir123Name;

        Mockito.doThrow(new SardineException("Folders need to be created one by one", 409, "No hierarchy allowed"))
                .when(sardineMock).createDirectory(urlDir3);
        Mockito.doThrow(new SardineException("Folders need to be created one by one", 409, "No hierarchy allowed"))
                .when(sardineMock).createDirectory(urlDir2);

        Mockito.doAnswer(invocation -> {
            //when dir1 is created dir2 can be created without exception
            Mockito.doAnswer(invocation2 -> {
                //when dir2 is created dir3 can be created without exception
                Mockito.doNothing().when(sardineMock).createDirectory(urlDir3);
                return null;
            }).when(sardineMock).createDirectory(urlDir2);
            return null;
        }).when(sardineMock).createDirectory(urlDir1);

        testService.createDirectory(dir123Name);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(urlDir1);
        Mockito.verify(sardineMock, Mockito.times(2)).createDirectory(urlDir2);
        Mockito.verify(sardineMock, Mockito.times(2)).createDirectory(urlDir3);
    }

    @Test
    public void createDirectory_MultipleExisting() throws Exception {

        String dir1Name = "folter1";
        String dir2Name = "folter2";
        String dir3Name = "folter3";
        String dir123Name = dir1Name + "/" + dir2Name + "/" + dir3Name;
        String urlDir1 = config.getWebDavUrl() + "/" + dir1Name;
        String urlDir2 = config.getWebDavUrl() + "/" + dir1Name + "/" + dir2Name;
        String urlDir3 = config.getWebDavUrl() + "/" + dir123Name;

        Mockito.doThrow(new SardineException("Already existing folders throw exceptions", 405, "Existing"))
                .when(sardineMock).createDirectory(urlDir1);
        Mockito.doThrow(new SardineException("Folders need to be created one by one", 409, "No hierarchy allowed"))
                .when(sardineMock).createDirectory(urlDir3);

        Mockito.doAnswer(invocation -> {
            //when dir2 is created dir3 can be created without exception
            Mockito.doNothing().when(sardineMock).createDirectory(urlDir3);
            return null;
        }).when(sardineMock).createDirectory(urlDir2);

        testService.createDirectory(dir123Name);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).createDirectory(urlDir2);
        Mockito.verify(sardineMock, Mockito.times(2)).createDirectory(urlDir3);
    }

    @Test
    public void fileExists() throws Exception {

        String fileNameExists = "feile.sharp";
        String urlExists = config.getWebDavUrl() + "/" + fileNameExists;
        String fileNameNotExists = "kratzbürste.pieks";
        String urlNotExists = config.getWebDavUrl() + "/" + fileNameNotExists;

        Mockito.when(sardineMock.exists(Mockito.any())).thenReturn(false);
        Mockito.when(sardineMock.exists(urlExists)).thenReturn(true);

        assertFalse(testService.fileExists(fileNameNotExists));
        assertTrue(testService.fileExists(fileNameExists));

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).exists(urlNotExists);
        Mockito.verify(sardineMock, Mockito.times(1)).exists(urlExists);
    }

    @Test
    public void delete() throws Exception {

        String fileName = "stirb.tot";
        String url = config.getWebDavUrl() + "/" + fileName;

        testService.delete(fileName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).delete(url);
    }

    @Test
    public void delete_NotExistingFile() throws Exception {

        String fileName = "zombie.tot";
        String url = config.getWebDavUrl() + "/" + fileName;

        //if the file is not existing we get a 404 exception
        Mockito.doThrow(new SardineException("Zombie is dead already!", 404, "Really really dead!"))
                .when(sardineMock).delete(url);

        //no exception should be thrown, because the file was not existing
        testService.delete(fileName);

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).delete(url);
    }

    @Test
    public void delete_Exception() throws Exception {

        String fileName = "undead.rip";
        String url = config.getWebDavUrl() + "/" + fileName;

        //unexpected exception
        Mockito.doThrow(new SardineException("No entry!", 402, "Insert coin to view file"))
                .when(sardineMock)
                .delete(url);

        try {
            testService.delete(fileName);
            fail("No exception thrown");
        } catch (FileStorageException e) {
            assertTrue(e.getMessage().contains(fileName));
            assertTrue(e.getMessage().contains(url));
        }

        assertCorrectInit();
        Mockito.verify(sardineMock, Mockito.times(1)).delete(url);
    }

    @Test
    public void delete_InternalException() throws Exception {

        String fileName = "undead.rip";
        String url = config.getWebDavUrl() + "/" + fileName;

        //unexpected exception
        Mockito.doThrow(new SardineException("Miep miep!", 500, "We screwed up again"))
                .when(sardineMock)
                .delete(url);

        try {
            testService.delete(fileName);
            fail("No exception thrown");
        } catch (FileStorageException e) {
            assertTrue(e.getMessage().contains(fileName));
            assertTrue(e.getMessage().contains(url));
        }

        assertCorrectInit();
        //we should have tried it several times before giving up
        Mockito.verify(sardineMock, Mockito.times(3)).delete(url);
    }

    private void assertCorrectInit() {
        Mockito.verify(sardineMock, Mockito.times(1)).setCredentials(config.getUsername(), config.getPassword());
        Mockito.verify(sardineMock, Mockito.times(1)).enableCompression();
    }

}

