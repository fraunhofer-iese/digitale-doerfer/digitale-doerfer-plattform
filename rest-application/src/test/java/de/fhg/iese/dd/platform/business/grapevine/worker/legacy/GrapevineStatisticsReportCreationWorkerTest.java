/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Danielle Korth, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker.legacy;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineChatService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.business.test.mocks.TestTeamFileStorageService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.config.StatisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.StatisticsCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GrapevineStatisticsReportCreationWorkerTest extends BaseServiceTest {

    private static final boolean SAVE_REPORTS = false;

    @Autowired
    private StatisticsConfig statisticsConfig;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private TestTeamFileStorageService testTeamFileStorageService;
    @Autowired
    public IGrapevineChatService grapevineChatService;
    @Autowired
    public IChatService chatService;

    @Autowired
    private GrapevineStatisticsReportFixedTimeCreationWorker statisticsReportFixedTimeCreationWorker;
    @Autowired
    private GrapevineStatisticsReportTemporaryCreationWorker statisticsReportTemporaryCreationWorker;

    private Tenant tenant1;
    private Tenant tenant2;
    private Tenant tenant3;

    @Override
    public void createEntities() throws IOException {

        th.createGrapevineScenario();
        createTenantsAndGeoAreas();
        createFeatureConfig();
    }

    public void createTenantsAndGeoAreas() {

        tenant1 = tenantRepository.save(Tenant.builder()
                .id("74dc2ee1-025e-4f5e-82bb-34b552e292e4")
                .name("VG 1")
                .build());

        tenant2 = tenantRepository.save(Tenant.builder()
                .id("c2c8af27-bedc-423a-a6ec-3e978180fd8f")
                .name("VG 2")
                .build());

        tenant3 = tenantRepository.save(Tenant.builder()
                .id("c2e6aa6c-2efe-4127-8009-152e2c47ed9c")
                .name("VG 3")
                .build());

        createGeoAreas();
    }

    private void createGeoAreas() {

        GeoArea geoAreaRheinlandPfalz = geoAreaRepository.save(GeoArea.builder()
                .id("76a2860b-a3ba-4299-955d-3859aedaa114")
                .name("Rheinland-Pfalz")
                .type(GeoAreaType.FEDERAL_STATE)
                .boundaryJson("[[[49.3,8.1], [49.3,8.4], [49.1,8.4], [49.1,8.1]]]")
                .center(new GPSLocation(49.2, 8.25))
                .build());

        GeoArea geoAreaTenant1 = geoAreaRepository.save(GeoArea.builder()
                .id("08964780-b3db-4988-b18f-3a2c643d8f1f")
                .tenant(tenant1)
                .name("Eisenbuckel")
                .type(GeoAreaType.ASSOCIATION_OF_MUNICIPALITIES)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson("[[[49.3,8.3], [49.3,8.4], [49.1,8.4], [49.1,8.3]]]")
                .center(new GPSLocation(49.2, 8.35))
                .build());
        tenant1.setRootArea(geoAreaTenant1);
        tenant1 = tenantRepository.save(tenant1);

        GeoArea geoAreaTenant2 = geoAreaRepository.save(GeoArea.builder()
                .id("b9b94a62-3a4e-4f74-baac-64492a9cf4d1")
                .tenant(tenant2)
                .name("Mainz")
                .type(GeoAreaType.CITY)
                .parentArea(geoAreaRheinlandPfalz)
                .boundaryJson("[[[49.3,8.2], [49.3,8.3], [49.1,8.3], [49.1,8.2]]]")
                .center(new GPSLocation(49.2, 8.25))
                .build());
        tenant2.setRootArea(geoAreaTenant2);
        tenant2 = tenantRepository.save(tenant2);

        GeoArea geoAreaTenant3 = geoAreaRepository.save(GeoArea.builder()
                .id("c22f2452-915d-4bb5-8328-15c450ca181f")
                .tenant(tenant3)
                .name("Geo Area 3")
                .type(GeoAreaType.CITY)
                .boundaryJson("[[[1,2]]]")
                .center(new GPSLocation(20.15, 18.35))
                .build());
        tenant3.setRootArea(geoAreaTenant3);
        tenant3 = tenantRepository.save(tenant3);
    }

    public void createFeatureConfig() throws IOException {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(StatisticsCreationFeature.class.getName())
                .geoAreasIncluded(Collections.singleton(th.geoAreaDeutschland))
                .enabled(true)
                .configValues(th.loadTextTestResource("StatisticsCreationFeatureConfig.json"))
                .build());
    }

    @Override
    public void tearDown() {
        th.deleteAllData();
    }

    @Test
    public void generateReports_FixedTime() throws IOException {

        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-10-04T04:01:34+01:00[Europe/Berlin]"));

        setupStatisticsScenario();

        statisticsReportFixedTimeCreationWorker.run();

        String expectedFileNameReportBeginningToPreviousMonthEnd =
                statisticsConfig.getTeamFileStorageFolderLongterm() + "/report_2018-01__2019-09.html";
        String expectedFileNameReportCurrentYearStartToPreviousMonthEnd =
                statisticsConfig.getTeamFileStorageFolderLongterm() + "/report_2019-01__2019-09.html";
        String expectedFileNameReportPreviousMonth =
                statisticsConfig.getTeamFileStorageFolderMonthly() + "/report_2019-09.html";
        String expectedFileNameReportPreviousWeek =
                statisticsConfig.getTeamFileStorageFolderWeekly() + "/report_2019-KW39.html";
        String expectedFileNameReportPreviousDay =
                statisticsConfig.getTeamFileStorageFolderDaily() + "/report_2019-10-03.html";

        assertStatisticsReportIsCreated(expectedFileNameReportBeginningToPreviousMonthEnd);
        assertStatisticsReportIsCreated(expectedFileNameReportCurrentYearStartToPreviousMonthEnd);
        assertStatisticsReportIsCreated(expectedFileNameReportPreviousMonth);
        assertStatisticsReportIsCreated(expectedFileNameReportPreviousWeek);
        assertStatisticsReportIsCreated(expectedFileNameReportPreviousDay);
        if (SAVE_REPORTS) {
            testTeamFileStorageService.saveFilesToTemp(getClass().getSimpleName());
        }
    }

    @Test
    public void generateReports_Temporary() throws IOException {

        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-10-04T04:01:34+01:00[Europe/Berlin]"));

        setupStatisticsScenario();

        statisticsReportTemporaryCreationWorker.run();

        String expectedFileNameReportCurrentYearStartToYesterdayEnd =
                statisticsConfig.getTeamFileStorageFolderTemporary() + "/report_2019-01__2019-10-03.html";
        String expectedFileNameReportCurrentMonthStartToYesterdayEnd =
                statisticsConfig.getTeamFileStorageFolderTemporary() + "/report_2019-10__2019-10-03.html";
        String expectedFileNameReportCurrentWeekStartToYesterdayEnd =
                statisticsConfig.getTeamFileStorageFolderTemporary() + "/report_2019-KW40__2019-10-03.html";

        assertStatisticsReportIsCreated(expectedFileNameReportCurrentYearStartToYesterdayEnd);
        assertStatisticsReportIsCreated(expectedFileNameReportCurrentMonthStartToYesterdayEnd);
        assertStatisticsReportIsCreated(expectedFileNameReportCurrentWeekStartToYesterdayEnd);
        if (SAVE_REPORTS) {
            testTeamFileStorageService.saveFilesToTemp(getClass().getSimpleName());
        }
    }

    private void setupStatisticsScenario() {
        //we want all persons to not have logged in recently, so that we can control it
        th.personRepository.findAll().stream()
                .peek(p -> p.setLastLoggedIn(0))
                .forEach(th.personRepository::save);

        th.personRegularAnna.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC());
        th.personRegularAnna.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(8));
        th.personRepository.save(th.personRegularAnna);

        th.personIeseAdmin.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(8));
        th.personIeseAdmin.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(10));
        th.personRepository.save(th.personIeseAdmin);

        th.personRegularKarl.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(3));
        th.personRegularKarl.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(15));
        th.personRegularKarl.setHomeArea(th.geoAreaDorf1InEisenberg);
        th.personRepository.save(th.personRegularKarl);

        th.gossip1withImages.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(10));
        th.postRepository.save(th.gossip1withImages);

        th.offer2.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(3));
        th.postRepository.save(th.offer2);

        th.newsItem1.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(40));
        th.postRepository.save(th.newsItem1);

        th.gossip2Comment1.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(10));
        th.commentRepository.save(th.gossip2Comment1);

        th.gossip2Comment3.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(40));
        th.commentRepository.save(th.gossip2Comment3);

        th.happening1Comment1.setCreated(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(3));
        th.commentRepository.save(th.happening1Comment1);

        th.selectGeoAreasForAppVariant(th.appVariant1Tenant1, th.personRegularKarl, th.geoAreaEisenberg,
                th.geoAreaMainz);
        th.selectGeoAreasForAppVariant(th.appVariant2Tenant1And2, th.personRegularKarl, th.geoAreaEisenberg,
                th.geoAreaMainz);
        th.selectGeoAreasForAppVariant(th.appVariant1Tenant1, th.personRegularAnna, th.geoAreaDorf1InEisenberg);

        Chat chat = grapevineChatService.findOrCreateChat(th.personSusanneChristmann, th.personThomasBecker,
                th.specialPost2Comment1);
        chat.setCreated(946684800000L);
        chatService.addParticipantsToChat(chat, th.personLaraSchaefer);
        chatService.addUserTextMessageToChat(chat, th.personSusanneChristmann, "Hallo, wie gehts?",
                System.currentTimeMillis() - 2000);
        chatService.addUserTextMessageToChat(chat, th.personThomasBecker, "Gut und euch?",
                System.currentTimeMillis() - 1000);
        chatService.addUserTextMessageToChat(chat, th.personLaraSchaefer, "Ja geht schon ;-)",
                System.currentTimeMillis());

        Group group1 = groupRepository.save(Group.builder()
                .name("Group1")
                .shortName("G1")
                .tenant(tenant1)
                .creator(th.personLaraSchaefer)
                .visibility(GroupVisibility.ANYONE)
                .accessibility(GroupAccessibility.PUBLIC)
                .contentVisibility(GroupContentVisibility.ANYONE)
                .deleted(false)
                .build());
        th.gossip1withImages.setGroup(group1);
        th.postRepository.save(th.gossip1withImages);

        Group group2 = groupRepository.save(Group.builder()
                .name("Group2")
                .shortName("G2")
                .tenant(tenant2)
                .creator(th.personThomasBecker)
                .visibility(GroupVisibility.ANYONE)
                .accessibility(GroupAccessibility.PUBLIC)
                .contentVisibility(GroupContentVisibility.ANYONE)
                .deleted(true)
                .build());
        th.happening2.setGroup(group2);
        th.postRepository.save(th.happening2);

        userGeneratedContentFlagService.create(th.gossip1withImages, th.gossip1withImages.getCreator(),
                th.gossip1withImages.getTenant(), th.personLaraSchaefer, "Die Bilder gefallen mir nicht");
        userGeneratedContentFlagService.create(th.gossip2Comment1, th.gossip2Comment1.getCreator(),
                th.gossip2WithComments.getTenant(), th.personSusanneChristmann, "Der Kommentar ergibt keinen Sinn!");
        userGeneratedContentFlagService.create(chat, th.personLaraSchaefer,
                th.personLaraSchaefer.getTenant(), th.personThomasBecker,
                "In dem Chat werden mir ständig Katzenfotos geschickt, obwohl ich lieber Hunde mag");
    }

    private void assertStatisticsReportIsCreated(String expectedReport) {
        assertStatisticsReportIsCreatedTenantSpecific(expectedReport, "text/html");
    }

    private void assertStatisticsReportIsCreatedTenantSpecific(String expectedReport, String fileType) {
        TestTeamFileStorageService.FileAttributes fileAttributes = testTeamFileStorageService
                .getFileAttributes(expectedReport);
        assertNotNull(fileAttributes, "Report " + expectedReport + " is not existing");
        assertEquals(fileType, fileAttributes.getMimeType());
        String reportText = new String(fileAttributes.getContent(), StandardCharsets.UTF_8);
        assertThat(reportText).contains(tenant1.getName());
        assertThat(reportText).contains(tenant2.getName());
        assertThat(reportText).contains(tenant3.getName());
    }

    private void assertGeoAreaReportIsCreated(String expectedReport, String fileType) {
        TestTeamFileStorageService.FileAttributes fileAttributes = testTeamFileStorageService
                .getFileAttributes(expectedReport);
        assertNotNull(fileAttributes, "Report " + expectedReport + " is not existing");
        assertEquals(fileType, fileAttributes.getMimeType());
        String reportText = new String(fileAttributes.getContent(), StandardCharsets.UTF_8);

        assertThat(reportText).contains(th.geoAreaEisenberg.getName());
        assertThat(reportText).contains(th.geoAreaDorf1InEisenberg.getName());
        assertThat(reportText).contains(th.geoAreaMainz.getName());
    }

    private void assertStatisticsReportMetadataIsCreated(String expectedReport) {
        TestTeamFileStorageService.FileAttributes fileAttributes = testTeamFileStorageService
                .getFileAttributes(expectedReport);
        assertNotNull(fileAttributes, "Report " + expectedReport + " is not existing");
        assertEquals("text/csv", fileAttributes.getMimeType());
        String reportText = new String(fileAttributes.getContent(), StandardCharsets.UTF_8);

        assertThat(reportText).contains("statistics-description");
    }

}
