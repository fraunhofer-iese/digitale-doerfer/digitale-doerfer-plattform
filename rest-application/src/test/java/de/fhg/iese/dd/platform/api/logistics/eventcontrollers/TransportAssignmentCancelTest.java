/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAlternativeSelectRequest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportAssignmentCancelRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class TransportAssignmentCancelTest extends BaseLogisticsEventTest {

    private TransportAssignment transportAssignment;

    private TransportAlternative transportAlternative;

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        String shopOrderId = purchaseOrderReceived();

        purchaseOrderReadyForTransport(shopOrderId);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        delivery = deliveries.get(0);

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();
        transportAlternative = transportAlternatives.get(0);

        transportAssignment = transportAlternativeSelectRequest(delivery, transportAlternative, carrier);

        assertNotEquals(receiver, carrier);
        assertNotEquals(receiver, carrierFirstStep);
        assertNotEquals(receiver, carrierLastStep);
        assertNotEquals(carrier, carrierFirstStep);
        assertNotEquals(carrier, carrierLastStep);
        assertNotEquals(carrierFirstStep, carrierLastStep);
    }

    @Test
    public void onTransportAssignmentCancelRequest() throws Exception {

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason );
        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignment.getId()));

        waitForEventProcessing();

        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_ASSIGNMENT.name()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()));
    }

    @Test
    public void onTransportAssignmentCancelRequestNotAssignable() throws Exception {

        //Carrier A takes over the transport
        //Carrier A cancels the assignment
        //Carrier B takes over the transport
        //Carrier A should not be able to get the Assignment

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason );
        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transportAssignmentId").value(transportAssignment.getId()));

        waitForEventProcessing();

        ClientTransportAlternativeSelectRequest transportAlternativeSelectRequest = new ClientTransportAlternativeSelectRequest(
            transportAlternative.getId());

        mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrierLastStep))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.accepted").value(true))
            .andExpect(jsonPath("$.transportAlternativeBundleToRemoveId").value(transportAlternative.getTransportAlternativeBundle().getId()))
            .andExpect(jsonPath("$.transportAlternativeId").value(transportAlternative.getId()));

        waitForEventProcessing();

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_PICKUP.name()));

        waitForEventProcessing();

        transportAlternativeSelectRequest = new ClientTransportAlternativeSelectRequest(transportAlternative.getId());

        mockMvc.perform(post("/logistics/event/transportAlternativeSelectRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(transportAlternativeSelectRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.accepted").value(false))
            .andExpect(jsonPath("$.transportAlternativeBundleToRemoveId").value(transportAlternative.getTransportAlternativeBundle().getId()))
            .andExpect(jsonPath("$.transportAlternativeId").value(transportAlternative.getId()));

        waitForEventProcessing();

        //--> Check Delivery (status) with service (receiver)
        mockMvc.perform(get("/logistics/delivery/")
            .headers(authHeadersFor(receiver)))
            .andExpect(content().contentType(contentType))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(delivery.getId()))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.WAITING_FOR_PICKUP.name()));
    }

    @Test
    public void onTransportAssignmentCancelRequestWrongCarrier() throws Exception {

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason);

        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrierLastStep))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(isException(ClientExceptionType.WRONG_CARRIER_CANCELLED));
    }

    @Test
    public void onTransportAssignmentCancelRequestAlreadyPickedUp() throws Exception {

        transportPickupRequest(
            carrier, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason );

        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_STATUS_INVALID));
    }

    @Test
    public void onTransportAssignmentCancelRequestTransportAssignmentIdNull() throws Exception {

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(null, cancelationReason );

        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportAssignmentCancelRequestTransportAssignmentIdIsInvalid() throws Exception {

        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest("", cancelationReason );

        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportAssignmentCancelRequestInvalidCancelationReason() throws Exception {

        final String cancelationReason = null;
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason );

        mockMvc.perform(post("/logistics/event/transportAssignmentCancelRequest")
                        .headers(authHeadersFor(carrier))
                        .contentType(contentType)
                        .content(json(clientTransportAssignmentCancelRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onTransportAssignmentCancelRequestUnauthorized() throws Exception {
        final String cancelationReason = "Just want to stop.";
        ClientTransportAssignmentCancelRequest clientTransportAssignmentCancelRequest =
                new ClientTransportAssignmentCancelRequest(transportAssignment.getId(), cancelationReason );

        assertOAuth2(post("/logistics/event/transportAssignmentCancelRequest")
                .content(json(clientTransportAssignmentCancelRequest))
                .contentType(contentType));

    }

}
