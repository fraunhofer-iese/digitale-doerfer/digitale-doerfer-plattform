/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.files.worker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryMediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

public class TemporaryMediaItemCleanUpWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TemporaryMediaItemRepository temporaryMediaItemRepository;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private TestTimeService testTimeService;

    @Autowired
    private TemporaryMediaItemCleanUpWorker temporaryMediaItemCleanUpWorker;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void cleanup_NoItems() throws Exception {

        List<TemporaryMediaItem> temporaryMediaItems =
                createTemporaryMediaItems(testTimeService.currentTimeMillisUTC() + TimeUnit.SECONDS.toMillis(180));

        long sizeBeforeCleanUp = temporaryMediaItems.size();
        long now = testTimeService.currentTimeMillisUTC();

        long expiredTemporaryMediaItems = temporaryMediaItems.stream()
                .filter(mi -> mi.getExpirationTime() < now).count();

        //no expired element
        assertEquals(0, expiredTemporaryMediaItems);

        temporaryMediaItemCleanUpWorker.run();

        long sizeAfterCleanUp = temporaryMediaItemRepository.count();

        //no deletion was made
        assertEquals(sizeBeforeCleanUp, sizeAfterCleanUp);
    }

    @Test
    public void cleanup_SomeItems() throws Exception {

        List<TemporaryMediaItem> temporaryMediaItems =
                createTemporaryMediaItems(testTimeService.currentTimeMillisUTC() + TimeUnit.SECONDS.toMillis(180));

        long numTemporaryMediaItemsBeforeCleanup = temporaryMediaItemRepository.count();
        assertEquals(temporaryMediaItems.size(), numTemporaryMediaItemsBeforeCleanup,
                "initialization of temp media items incorrect");
        int numTemporaryMediaItemsToExpire = 5;

        assertTrue(numTemporaryMediaItemsBeforeCleanup > numTemporaryMediaItemsToExpire, "too less items to expire");

        //expire some of them
        long expiredTime = testTimeService.currentTimeMillisUTC() - TimeUnit.SECONDS.toMillis(30);
        List<TemporaryMediaItem> temporaryMediaItemsToExpire = new ArrayList<>(numTemporaryMediaItemsToExpire);
        for (int i = 0; i < numTemporaryMediaItemsToExpire; i++) {
            TemporaryMediaItem temporaryMediaItem = temporaryMediaItems.get(i);
            temporaryMediaItem.setExpirationTime(expiredTime);
            TemporaryMediaItem savedMediaItem = temporaryMediaItemRepository.save(temporaryMediaItem);
            temporaryMediaItemsToExpire.add(savedMediaItem);
            log.debug("expired {} to {}", savedMediaItem, savedMediaItem.getExpirationTime());
        }

        temporaryMediaItemRepository.flush();

        log.debug("Cleanup should delete everything with expiration < {}", testTimeService.currentTimeMillisUTC());

        temporaryMediaItemCleanUpWorker.run();

        //wait until all database triggers were run
        Thread.sleep(200);

        long numTemporaryMediaItemsAfterCleanup = temporaryMediaItemRepository.count();

        assertEquals(numTemporaryMediaItemsBeforeCleanup - numTemporaryMediaItemsToExpire,
                numTemporaryMediaItemsAfterCleanup,
                "not all items have been deleted");

        for (TemporaryMediaItem temporaryMediaItem : temporaryMediaItemsToExpire) {
            assertFalse(temporaryMediaItemRepository.existsById(temporaryMediaItem.getId()), "item was not deleted");
        }
    }

    private List<TemporaryMediaItem> createTemporaryMediaItems(long expirationTime) throws Exception {

        List<TemporaryMediaItem> createdMediaItems = new ArrayList<>(10);

        for (int i = 0; i < 10; i++){

            TemporaryMediaItem temporaryMediaItem =
                    mediaItemService.createTemporaryMediaItem(th.loadTestResource("testImage.jpg"),
                            FileOwnership.of(th.personRegularAnna));
            temporaryMediaItem.setExpirationTime(expirationTime);
            TemporaryMediaItem savedTemporaryMediaItem = temporaryMediaItemRepository.save(temporaryMediaItem);
            log.debug("created {} with expiration time {}",
                    savedTemporaryMediaItem.getId(), savedTemporaryMediaItem.getExpirationTime());
            createdMediaItems.add(savedTemporaryMediaItem);
        }
        temporaryMediaItemRepository.flush();
        return createdMediaItems;
    }

}
