/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeStatusRequest;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonStatusExtended;
import de.fhg.iese.dd.platform.business.grapevine.services.PostSortCriteria;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostControllerBySelectedGeoAreasTest extends BasePostControllerTest {

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void postBySelectedGeoAreas_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(get("/grapevine/postBySelectedGeoAreas")
                .contentType(contentType), th.appVariant1Tenant1, th.personRegularKarl);
    }

    @Test
    public void postBySelectedGeoAreas_InvalidPageParameter() throws Exception {

        final Person person = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .headers(authHeadersFor(person, appVariant))
                .param("page", "-1"))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .headers(authHeadersFor(person, appVariant))
                .param("count", "-10"))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .headers(authHeadersFor(person, appVariant))
                .param("count", "0"))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void postBySelectedGeoAreas_DefaultParameterValues() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant1;
        final Person caller = th.withHomeArea(th.personSusanneChristmann, homeArea);
        final AppVariant appVariant = th.appVariant1Tenant1;
        final Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg, th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);

        assertNotNull(caller.getHomeArea(), "Home area should be defined for the person to test the default behavior");

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(
                GrapevineTestHelper.GEO_AREA_TENANT_1_POST_COUNT + GrapevineTestHelper.SUB_GEO_AREA_TENANT_1_POST_COUNT,
                expectedPosts.size());
        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(1, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(2, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 2, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(3, pageSize, expectedPosts.size()));
    }

    @Test
    public void postBySelectedGeoAreas_MultipleGeoAreas() throws Exception {

        GeoArea homeArea = th.geoAreaTenant1;
        Person caller = th.withHomeArea(th.personSusanneChristmann, homeArea);
        AppVariant appVariant = th.appVariant1Tenant1;
        Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg, th.geoAreaDorf2InEisenberg,
                        th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);
        //post is in two of the selected geo areas
        th.gossip1withImages.setGeoAreas(Set.of(th.geoAreaDorf1InEisenberg, th.geoAreaDorf2InEisenberg));
        th.gossip1withImages = th.postRepository.saveAndFlush(th.gossip1withImages);

        assertNotNull(caller.getHomeArea(), "Home area should be defined for the person to test the default behavior");

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(
                GrapevineTestHelper.GEO_AREA_TENANT_1_POST_COUNT + GrapevineTestHelper.SUB_GEO_AREA_TENANT_1_POST_COUNT,
                expectedPosts.size());
        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(1, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(2, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 2, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(3, pageSize, expectedPosts.size()));
    }

    @Test
    public void postBySelectedGeoAreas_TemplatedNewsSource() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant1;
        final Person person = th.withHomeArea(th.personSusanneChristmann, homeArea);
        final AppVariant appVariant = th.appVariant1Tenant1;
        final Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg, th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);

        th.selectGeoAreasForAppVariant(appVariant, person, selectedGeoAreas);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> post instanceof NewsItem)
                .filter(post -> ((NewsItem) post).isPublished())
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        NewsItem newsItem = (NewsItem) expectedPosts.get(0);
        final NewsSource newsSource = newsItem.getNewsSource();
        newsSource.setSiteName("Kram aus $post.geoArea.name$");
        th.newsSourceRepository.saveAndFlush(newsSource);
        String expectedSiteName = "Kram aus " + newsItem.getGeoAreas().stream()
                .min(Comparator.comparing(GeoArea::getId))
                .get()
                .getName();

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .param("type", ClientPostType.NEWS_ITEM.name())
                        .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, 10, expectedPosts.size()))
                .andExpect(jsonPath("content[0].newsItem.siteName").value(expectedSiteName));
    }

    @Test
    public void postBySelectedGeoAreas_ParallelWorldInhabitant() throws Exception {

        final GeoArea homeArea = th.geoAreaTenant1;
        final Person caller = th.withHomeArea(th.personSusanneChristmann, homeArea);
        final AppVariant appVariant = th.appVariant1Tenant1;
        final Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg, th.geoAreaRheinlandPfalz,
                        th.geoAreaDeutschland);
        //the likes are not required for the test
        th.postInteractionRepository.deleteAll();

        assertNotNull(caller.getHomeArea(), "Home area should be defined for the person to test the default behavior");

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        Person parallelWorldInhabitant = th.withHomeArea(th.gossip3WithComment.getCreator(), homeArea);
        th.selectGeoAreasForAppVariant(appVariant, parallelWorldInhabitant, selectedGeoAreas);
        assertNotEquals(caller, parallelWorldInhabitant);

        //this should hide the posts this person created
        mockMvc.perform(post("/adminui/person/event/personChangeStatusRequest")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType)
                        .content(json(ClientPersonChangeStatusRequest.builder()
                                .personId(parallelWorldInhabitant.getId())
                                .statusesToAdd(Set.of(ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT))
                                .build())))
                .andExpect(status().isOk());

        waitForEventProcessing();

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());
        assertEquals(
                GrapevineTestHelper.GEO_AREA_TENANT_1_POST_COUNT + GrapevineTestHelper.SUB_GEO_AREA_TENANT_1_POST_COUNT,
                expectedPosts.size());

        List<Post> filteredExpectedPosts = expectedPosts.stream()
                .filter(post -> !parallelWorldInhabitant.equals(post.getCreator()))
                .collect(Collectors.toList());

        assertThat(filteredExpectedPosts).hasSizeLessThan(expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, filteredExpectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, filteredExpectedPosts, 0, pageSize));

        //no filtering should happen inside the parallel world
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(parallelWorldInhabitant, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        //this should unhide the posts the person created
        mockMvc.perform(post("/adminui/person/event/personChangeStatusRequest")
                        .headers(authHeadersFor(th.personSuperAdmin))
                        .contentType(contentType)
                        .content(json(ClientPersonChangeStatusRequest.builder()
                                .personId(parallelWorldInhabitant.getId())
                                .statusesToRemove(Set.of(ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT))
                                .build())))
                .andExpect(status().isOk());

        waitForEventProcessing();

        //both are now normal callers
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(parallelWorldInhabitant, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_DefaultParameterValuesAndNoHomeArea() throws Exception {

        final Person caller = th.withoutHomeArea(th.personSusanneChristmann);
        final AppVariant appVariant = th.appVariant1Tenant1;
        final GeoArea selectedGeoArea = th.subGeoArea1Tenant1;

        assertNull(caller.getHomeArea(), "No home area should be defined for the person to test the default behavior");

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(GrapevineTestHelper.SUB_GEO_AREA_TENANT_1_POST_COUNT, expectedPosts.size());

        int pageSize = 10;
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(1, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(2, pageSize, expectedPosts.size()));
    }

    @Test
    public void postBySelectedGeoAreas_DefaultParameterValuesAndNoGeoAreaSelection() throws Exception {

        final GeoArea homeArea = th.subGeoArea1Tenant1;
        final Person caller = th.withHomeArea(th.personSusanneChristmann, homeArea);
        final AppVariant appVariant = th.appVariant1Tenant1;

        assertNotNull(caller.getHomeArea(),
                "Home area should be defined for the person to show that it does not matter");

        //no selection -> no results
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, 10, 0));
    }

    @Test
    public void postBySelectedGeoAreas_CountParameter() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;
        final GeoArea selectedGeoArea = th.geoAreaTenant1;

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        int pageSize = 5;

        // sort the expected posts by last activity (last comment) descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(Post::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(GrapevineTestHelper.GEO_AREA_TENANT_1_POST_COUNT, expectedPosts.size());

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("count", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(1, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_SortByLastActivityParameter() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;
        final GeoArea selectedGeoArea = th.geoAreaTenant1;

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        // sort the expected posts by last activity (last comment) descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .filter(post -> !(post instanceof ExternalPost && !((ExternalPost) post).isPublished()))
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(GrapevineTestHelper.GEO_AREA_TENANT_1_POST_COUNT, expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("sortBy", "LAST_ACTIVITY"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("page", "1")
                        .param("sortBy", "LAST_ACTIVITY"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(1, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 1, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_HappeningsWithStartAndEndTimeParameter() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;
        final Set<GeoArea> selectedGeoAreas = new HashSet<>();
        selectedGeoAreas.add(th.geoAreaTenant1);
        selectedGeoAreas.add(th.geoAreaTenant2);

        long currentTime = System.currentTimeMillis();

        long time1 = currentTime + TimeUnit.DAYS.toMillis(5);
        long time2 = currentTime + TimeUnit.DAYS.toMillis(7);
        long time3 = currentTime + TimeUnit.DAYS.toMillis(1);
        long time4 = currentTime + TimeUnit.DAYS.toMillis(3);

        th.happening1.setStartTime(time1); //relevant
        th.happening2.setStartTime(time2); //filtered out by geoarea
        th.happening3.setStartTime(time3); //filtered out by geoarea
        th.happening4.setStartTime(time4); //relevant

        th.postRepository.save(th.happening1);
        th.postRepository.save(th.happening2);
        th.postRepository.save(th.happening3);
        th.postRepository.save(th.happening4);

        final long startTime = time4;
        final long endTime = time1;
        final int expectedHappeningCount = 2;

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // sort the expected posts by start time ascending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> post instanceof Happening)
                .map(post -> (Happening) post)
                .filter(post -> (startTime <= post.getStartTime()) && (post.getStartTime() <= endTime))
                .sorted(Comparator.comparingLong(Happening::getStartTime))
                .collect(Collectors.toList());

        assertEquals(expectedHappeningCount, expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("sortBy", PostSortCriteria.HAPPENING_START.name())
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_HappeningsInPastAreFilteredOut() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;
        final Set<GeoArea> selectedGeoAreas = new HashSet<>();
        selectedGeoAreas.add(th.geoAreaTenant1);
        selectedGeoAreas.add(th.subGeoArea1Tenant1);

        long currentTime = System.currentTimeMillis();

        long time1 = currentTime + TimeUnit.DAYS.toMillis(1);
        long time3 = currentTime + TimeUnit.DAYS.toMillis(3);
        long time4 = currentTime + TimeUnit.DAYS.toMillis(4);
        long time5 = currentTime + TimeUnit.DAYS.toMillis(5);
        long time6 = currentTime + TimeUnit.DAYS.toMillis(6);
        long time7 = currentTime + TimeUnit.DAYS.toMillis(7);

        th.happening1.setStartTime(time6);
        th.happening1.setAllDayEvent(true);

        th.happening2.setStartTime(time5);
        th.happening2.setAllDayEvent(true);

        //start is out of the request range, but end is in!
        th.happening3.setStartTime(time1);
        th.happening3.setEndTime(time7);

        th.happening4.setStartTime(time3);
        th.happening4.setAllDayEvent(true);

        th.postRepository.save(th.happening1);
        th.postRepository.save(th.happening2);
        th.postRepository.save(th.happening3);
        th.postRepository.save(th.happening4);

        final long startTime = time4;
        final long endTime = time6;

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // sort the expected posts by start time ascending
        final List<Post> expectedPosts = Arrays.asList(th.happening3, th.happening2, th.happening1);

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("sortBy", PostSortCriteria.HAPPENING_START.name())
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_StartAndEndTimeParameter() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant3Tenant3;
        final GeoArea selectedGeoArea = th.geoAreaTenant3;
        final long startTime = 1300;
        final long endTime = 1500;

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> (startTime <= post.getCreated()) && (post.getCreated() <= endTime))
                .filter(post -> !(post instanceof Suggestion && ((Suggestion) post).isInternal()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_StartAndEndTimeAndSortByLastActivityParameter() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant3Tenant3;
        final GeoArea selectedGeoArea = th.geoAreaTenant3;
        final long startTime = 1300;
        final long endTime = 1500;

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        // sort the expected posts by last comment descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> (startTime <= post.getLastActivity()) && (post.getLastActivity() <= endTime))
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime))
                        .param("sortBy", "LAST_ACTIVITY"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_TypeParameter() throws Exception {

        final Person caller = th.withoutHomeArea(th.personSusanneChristmann);
        final AppVariant appVariant = th.appVariant1Tenant1;
        final GeoArea selectedGeoArea = th.subGeoArea1Tenant1;

        assertNull(caller.getHomeArea(), "No home area should be defined for the person to test the default behavior");

        th.selectGeoAreaForAppVariant(appVariant, caller, selectedGeoArea);

        // sort the expected posts by created descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> post.getGeoAreas() != null && post.getGeoAreas().contains(selectedGeoArea))
                .filter(post -> post.getPostType() == PostType.Gossip)
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedPosts.size());

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.GOSSIP.name()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_AllParameters() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant4AllTenants;
        final Collection<GeoArea> selectedGeoAreas =
                Arrays.asList(th.geoAreaTenant1, th.subGeoArea1Tenant1, th.geoAreaTenant3);
        final long startTime = 1300;
        final long endTime = 1500;
        final int pageNumber = 1;
        final int pageSize = 2;

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // sort the expected posts by last comment descending
        final List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .filter(post -> (startTime <= post.getLastActivity()) && (post.getLastActivity() <= endTime))
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING))
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.EMERGENCY_AID))
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedPosts.size());

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.GOSSIP.name())
                        .param("startTime", String.valueOf(startTime))
                        .param("endTime", String.valueOf(endTime))
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString())
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString())
                        .param("sortBy", "LAST_ACTIVITY")
                        .param("page", String.valueOf(pageNumber))
                        .param("count", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(pageNumber, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, pageNumber, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_InvalidSortByForType() throws Exception {

        final Person person = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .headers(authHeadersFor(person, appVariant))
                .param("sortBy", "HAPPENING_START"))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                .headers(authHeadersFor(person, appVariant))
                .param("type", ClientPostType.GOSSIP.name())
                .param("sortBy", "HAPPENING_START"))
                .andExpect(isException(ClientExceptionType.INVALID_SORTING_CRITERION));
    }

    @Test
    public void postBySelectedGeoAreas_HappeningsWithSortByStart() throws Exception {

        final Person person = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;

        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(person, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("sortBy", "HAPPENING_START"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void postBySelectedGeoAreas_HappeningsWithOrganizationTags() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant1Tenant1;
        final List<GeoArea> selectedGeoAreas = Arrays.asList(th.geoAreaTenant1, th.subGeoArea1Tenant1,
                th.geoAreaTenant3);
        final int pageSize = DEFAULT_PAGE_SIZE;
        final List<Happening> allHappenings = th.happeningRepository.findAll().stream()
                .filter(Post::isNotDeleted)
                .filter(Happening::isPublished)
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // -----------------------------------------
        // required organization tag only (-> Volunteering = organizationGartenbau and organizationFeuerwehr)
        List<Post> expectedPosts = allHappenings.stream()
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING))
                .collect(Collectors.toList());

        assertEquals(3, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // forbidden organization tag only (-> no Emergency Aid = organizationGartenbau + no organization)
        expectedPosts = allHappenings.stream()
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(2, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // both required and forbidden organization tags (-> Volunteering but no Emergency Aid = organizationGartenbau only)
        expectedPosts = allHappenings.stream()
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING))
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(1, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString())
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_WithOrganizationTags() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant3Tenant3;
        final List<GeoArea> selectedGeoAreas = Arrays.asList(th.geoAreaTenant1, th.subGeoArea1Tenant1,
                th.geoAreaTenant3);
        final List<Post> allPosts = th.postRepository.findAllFull().stream()
                .filter(Post::isNotDeleted)
                .filter(post -> !(post instanceof ExternalPost) || ((ExternalPost) post).isPublished())
                .filter(post -> CollectionUtils.containsAny(post.getGeoAreas(), selectedGeoAreas))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());
        int pageSize = DEFAULT_PAGE_SIZE;

        th.selectGeoAreasForAppVariant(appVariant, caller, selectedGeoAreas);

        // -----------------------------------------
        // required organization tag only (-> Volunteering = organizationGartenbau + organizationFeuerwehr)
        List<Post> expectedPosts = allPosts.stream()
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING))
                .collect(Collectors.toList());

        assertEquals(8, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // forbidden organization tag only (-> no Emergency Aid = organizationGartenbau only + no organization)
        expectedPosts = allPosts.stream()
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(37, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // both required and forbidden organization tags (-> Volunteering and no Emergency Aid = organizationGartenbau only)
        expectedPosts = allPosts.stream()
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING))
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(5, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString())
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // multiple organization tags (-> Volunteering or Emergency Aid = organizationFeuerwehr + organizationGartenbau)
        expectedPosts = allPosts.stream()
                .filter(post -> hasAnyRequiredOrganizationTag(post, OrganizationTag.VOLUNTEERING,
                        OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(8, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString(),
                                ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        // -----------------------------------------
        // multiple organization tags (-> no Volunteering and no Emergency Aid = no organization)
        expectedPosts = allPosts.stream()
                .filter(post -> hasNoForbiddenOrganizationTag(post, OrganizationTag.VOLUNTEERING,
                        OrganizationTag.EMERGENCY_AID))
                .collect(Collectors.toList());

        assertEquals(32, expectedPosts.size());
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString(),
                                ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectFakePageData(0, pageSize, expectedPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void postBySelectedGeoAreas_WithOrganizationTagsBadRequest() throws Exception {

        final Person caller = th.personSusanneChristmann;
        final AppVariant appVariant = th.appVariant3Tenant3;

        // ambiguous tag lists
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredOrganizationTags", ClientOrganizationTag.VOLUNTEERING.toString(),
                                ClientOrganizationTag.EMERGENCY_AID.toString())
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isBadRequest());

        // invalid tags
        mockMvc.perform(get("/grapevine/postBySelectedGeoAreas")
                        .headers(authHeadersFor(caller, appVariant))
                        .param("requiredOrganizationTags", "INVALID_TAG")
                        .param("forbiddenOrganizationTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isBadRequest());
    }

    private boolean hasAnyRequiredOrganizationTag(Post post, OrganizationTag... requiredTags) {

        if (post.getOrganization() == null) {
            return false;
        }
        for (OrganizationTag requiredTag : requiredTags) {
            if (post.getOrganization().getTags().hasValue(requiredTag)) {
                return true;
            }
        }
        return false;
    }

    private boolean hasNoForbiddenOrganizationTag(Post post, OrganizationTag... forbiddenTags) {

        if (post.getOrganization() == null) {
            return true;
        }
        for (OrganizationTag forbiddenTag : forbiddenTags) {
            if (post.getOrganization().getTags().hasValue(forbiddenTag)) {
                return false;
            }
        }
        return true;
    }

}
