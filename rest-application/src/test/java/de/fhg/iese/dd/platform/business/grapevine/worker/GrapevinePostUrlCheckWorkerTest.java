/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestParallelismService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import lombok.extern.log4j.Log4j2;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@Log4j2
public class GrapevinePostUrlCheckWorkerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private GrapevinePostUrlCheckWorker grapevinePostUrlCheckWorker;

    private static MockWebServer mockExternalSystem;
    private static final Map<String, MockResponse> mockedResponsesByPath = new HashMap<>();

    @BeforeAll
    public static void startMockServer() throws IOException {

        mockExternalSystem = new MockWebServer();
        mockExternalSystem.start(8080);
        Dispatcher dispatcher = new Dispatcher() {

            @NotNull
            @Override
            public MockResponse dispatch(@NotNull RecordedRequest request) throws InterruptedException {

                MockResponse response = mockedResponsesByPath.get(request.getPath());
                if (response == null) {
                    throw new IllegalStateException("No mock response is defined for " + request);
                }
                log.trace("Request: {}, Response: {}", request, response);
                return response;
            }
        };
        mockExternalSystem.setDispatcher(dispatcher);
    }

    @AfterAll
    public static void shutdownMockServer() throws Exception {

        mockExternalSystem.shutdown();
    }

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
        prepareNewsSource(th.newsSourceDigitalbach);
        prepareNewsSource(th.newsSourceAnalogheim);
        prepareNewsSource(th.newsSourceKatzenbach);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
        //we do not want the checks to run outside of the test
        testParallelismService.getScheduledTasks()
                .forEach(scheduledTask -> scheduledTask.scheduledFuture().cancel(true));
        mockedResponsesByPath.clear();
    }

    @Test
    public void checkPostUrls_Unpublish() throws Exception {

        //this is required to prevent the scheduled tasks from running immediately
        long now = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(2);
        timeServiceMock.setConstantDateTime(now);

        prepareMockResponse(th.newsItem1, th.newsSourceDigitalbach, 200);
        prepareMockResponse(th.newsItem2, th.newsSourceAnalogheim, 300);
        prepareMockResponse(th.newsItem3, th.newsSourceKatzenbach, 404);
        prepareMockResponse(th.newsItem4, th.newsSourceDigitalbach, 401);
        //deleted on our side
        prepareMockResponse(th.newsItem5, th.newsSourceAnalogheim, 200);
        //deleted on our side
        prepareMockResponse(th.newsItem6, th.newsSourceKatzenbach, 200);
        //not published
        prepareMockResponse(th.unpublishedNewsItem1, th.newsSourceKatzenbach, 200);
        prepareMockResponse(th.happening1, th.newsSourceDigitalbach, 200);
        prepareMockResponse(th.happening2, th.newsSourceAnalogheim, 201);
        prepareMockResponse(th.happening3, th.newsSourceKatzenbach, 403);
        prepareMockResponse(th.happening4, th.newsSourceDigitalbach, 500);
        //not published
        prepareMockResponse(th.unpublishedHappening1, th.newsSourceDigitalbach, 200);

        grapevinePostUrlCheckWorker.run();

        List<TestParallelismService.ScheduledTask> scheduledTasksFirstRun = testParallelismService.getScheduledTasks();
        assertThat(scheduledTasksFirstRun).hasSize(8);
        //we execute them right now, no one is started, since "now" is far in the future
        scheduledTasksFirstRun.forEach(scheduledTask -> scheduledTask.task().run());
        testParallelismService.reset();
        waitForEventProcessing();

        //200
        assertPostState(th.newsItem1, true, now);
        //300
        assertPostState(th.newsItem2, true, now);
        //404
        assertPostState(th.newsItem3, false, now);
        //401
        assertPostState(th.newsItem4, false, now);
        //not checked, deleted on our side
        assertPostState(th.newsItem5, true, null);
        //not checked, deleted on our side
        assertPostState(th.newsItem6, true, null);
        //not checked, not published
        assertPostState(th.unpublishedNewsItem1, false, null);
        //200
        assertPostState(th.happening1, true, now);
        //201
        assertPostState(th.happening2, true, now);
        //403
        assertPostState(th.happening3, false, now);
        //checked, but 500 is not considered valid check, so no check time is set
        assertPostState(th.happening4, true, null);
        //not checked, not published
        assertPostState(th.unpublishedHappening1, false, null);

        //we simulate that the external system is available now and run another check
        long nowLater = System.currentTimeMillis() + TimeUnit.HOURS.toMillis(3);
        timeServiceMock.setConstantDateTime(nowLater);
        prepareMockResponse(th.happening4, th.newsSourceDigitalbach, 200);
        grapevinePostUrlCheckWorker.run();

        List<TestParallelismService.ScheduledTask> scheduledTaskSecondRun = testParallelismService.getScheduledTasks();
        assertThat(scheduledTaskSecondRun).hasSize(1);
        //we execute them right now, no one is started, since "now" is far in the future
        scheduledTaskSecondRun.forEach(scheduledTask -> scheduledTask.task().run());
        testParallelismService.reset();
        waitForEventProcessing();

        //200
        assertPostState(th.happening4, true, nowLater);

        //no check should be done now
        grapevinePostUrlCheckWorker.run();

        List<TestParallelismService.ScheduledTask> scheduledTaskThirdRun = testParallelismService.getScheduledTasks();
        assertThat(scheduledTaskThirdRun).isEmpty();
    }

    @Test
    public void checkPostUrls_NothingToCheck() throws Exception {

        th.newsSourceAnalogheim.setCheckUrlOfPosts(false);
        th.newsSourceRepository.save(th.newsSourceAnalogheim);
        th.newsSourceDigitalbach.setCheckUrlOfPosts(false);
        th.newsSourceRepository.save(th.newsSourceDigitalbach);
        th.newsSourceKatzenbach.setCheckUrlOfPosts(false);
        th.newsSourceRepository.save(th.newsSourceKatzenbach);
        grapevinePostUrlCheckWorker.run();

        List<TestParallelismService.ScheduledTask> scheduledTasksFirstRun = testParallelismService.getScheduledTasks();
        assertThat(scheduledTasksFirstRun).isEmpty();
    }

    private void prepareNewsSource(NewsSource newsSource) {

        newsSource.setSiteUrl(mockExternalSystem.url("/" + newsSource.getId() + "/").toString());
        newsSource.setCheckUrlOfPosts(true);
        th.newsSourceRepository.save(newsSource);
    }

    private void prepareMockResponse(ExternalPost externalPost, NewsSource newsSource, int responseCode) {

        String path = "/" + newsSource.getId() + "/" + externalPost.getId();
        String url = mockExternalSystem.url(path).toString();
        externalPost.setNewsSource(newsSource);
        externalPost.setUrl(url);
        th.postRepository.save(externalPost);
        mockedResponsesByPath.put(path, new MockResponse().setResponseCode(responseCode));
    }

    private void assertPostState(ExternalPost post, boolean published, Long lastCheck) {

        ExternalPost updatedPost = th.externalPostRepository.findById(post.getId()).get();
        assertThat(updatedPost.isPublished()).as("published").isEqualTo(published);
        assertThat(updatedPost.getLastUrlCheckTime()).as("last check").isEqualTo(lastCheck);
    }

}
