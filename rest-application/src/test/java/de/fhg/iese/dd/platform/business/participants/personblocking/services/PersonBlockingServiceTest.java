/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.personblocking.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.participants.personblocking.exceptions.BlockedPersonsLimitExceededException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.config.PersonBlockingConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos.PersonBlockingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

public class PersonBlockingServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private PersonBlockingConfig personBlockingConfig;

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Autowired
    private PersonBlockingRepository personBlockingRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPersonBlockings();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void addNotExistingBlocking() {

        final Person blockingPerson = th.personRegularAnna;
        final Person blockedPerson = th.personRegularKarl;
        final App app = th.app2;

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                        blockingPerson, blockedPerson), "Wrong setup of test");

        personBlockingService.blockPerson(app, blockingPerson, blockedPerson);

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, blockedPerson));
        //test existsBlocking method in service, too
        assertTrue(personBlockingService.existsBlocking(app, blockingPerson, blockedPerson));
    }

    @Test
    public void addSelfBlocking() {

        final Person blockingPerson = th.personRegularAnna;
        final Person blockedPerson = th.personRegularAnna;
        final App app = th.app1;

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                        blockingPerson, blockedPerson), "Wrong setup of test");

        personBlockingService.blockPerson(app, blockingPerson, blockedPerson);

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, blockedPerson));
    }

    @Test
    public void addBlockingBeyondLimit() {

        final Person person = th.personRegularAnna;
        final App app = th.app1;

        assertEquals(2, personBlockingConfig.getMaxNumberBlockedPersons(), "Wrong setup of test");
        assertEquals(1, personBlockingRepository.countByAppAndBlockingPersonAndBlockedPerson_DeletedFalse(app, person),
                "Wrong setup of test");

        Assertions.assertThrows(BlockedPersonsLimitExceededException.class, () -> {
            personBlockingService.blockPerson(app, person, th.personRegularHorstiTenant3);
            personBlockingService.blockPerson(app, person, th.personRoleManagerTenant2);
        });
    }

    @Test
    public void addExistingBlocking() {

        final PersonBlocking blocking = th.annaBlocksKarlInApp1;

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                        blocking.getBlockingPerson(), blocking.getBlockedPerson()), "Wrong setup of test");

        personBlockingService.blockPerson(blocking.getApp(), blocking.getBlockingPerson(), blocking.getBlockedPerson());

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                blocking.getBlockingPerson(), blocking.getBlockedPerson()));
    }

    @Test
    public void removeNotExistingBlocking() {

        final Person blockingPerson = th.personRegularAnna;
        final Person blockedPerson = th.personRoleManagerTenant1;
        final App app = th.app1;

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                        blockingPerson, blockedPerson), "Wrong setup of test");

        personBlockingService.unblockPerson(app, blockingPerson, blockedPerson);

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, blockedPerson));
    }

    @Test
    public void removeExistingBlocking() {

        final PersonBlocking blocking = th.annaBlocksKarlInApp1;

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                        blocking.getBlockingPerson(), blocking.getBlockedPerson()), "Wrong setup of test");

        personBlockingService.unblockPerson(blocking.getApp(), blocking.getBlockingPerson(), blocking.getBlockedPerson());

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                blocking.getBlockingPerson(), blocking.getBlockedPerson()));
        //test existsBlocking method in service, too
        assertFalse(personBlockingService.existsBlocking(blocking.getApp(), blocking.getBlockingPerson(),
                blocking.getBlockedPerson()));
    }

    @Test
    public void getAllBlockings() {

        final Collection<Person> blockedPersons = personBlockingService.getBlockedPersons(th.app1, th.personRegularAnna);
        assertEquals(2, blockedPersons.size());
        assertThat(blockedPersons).containsExactlyInAnyOrder(th.personRegularKarl, th.personDeleted);
    }

    @Test
    public void getActiveBlockings() {

        final Collection<Person> blockedPersons = personBlockingService.getBlockedNotDeletedPersons(th.app1, th.personRegularAnna);
        assertEquals(1, blockedPersons.size());
        assertEquals(Collections.singletonList(th.personRegularKarl), blockedPersons);

        final Set<String> blockedPersonIds = personBlockingService.getBlockedNotDeletedPersonIds(th.app1, th.personRegularAnna);
        assertEquals(1, blockedPersons.size());
        assertEquals(th.personRegularKarl.getId(), blockedPersonIds.iterator().next());
    }

    @Test
    public void getApps() {

        final Collection<App> apps = personBlockingService.getBlockingApps(th.personRegularAnna);
        assertEquals(2, apps.size());
        assertThat(apps).containsExactlyInAnyOrder(th.app1, th.app2);
    }

    @Test
    public void getBlockingsOfPerson() {

        final Collection<PersonBlocking> personBlockings =
                personBlockingService.getBlockingsOfPerson(th.personRegularAnna);
        assertEquals(3, personBlockings.size());
        assertThat(personBlockings).containsExactlyInAnyOrder(th.annaBlocksKarlInApp1, th.annaBlocksDeletedInApp1,
                th.annaBlocksContactPersonCommunity1InApp2);

        assertTrue(personBlockingService.getBlockingsOfPerson(th.personRegularKarl).isEmpty());
    }

}
