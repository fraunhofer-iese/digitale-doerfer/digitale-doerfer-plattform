/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Dominik Schnier, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientDeliveryCreatedEvent;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientDimension;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientDeliveryStatus;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.api.shopping.ShoppingTestHelper;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReadyForTransportRequest;
import de.fhg.iese.dd.platform.api.shopping.clientevent.ClientPurchaseOrderReceivedEvent;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderItem;
import de.fhg.iese.dd.platform.api.shopping.clientmodel.ClientPurchaseOrderParcel;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DeliveryStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;

public class ShoppingEventControllerTest extends BaseServiceTest {

    @Autowired
    private ShoppingTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        th.createBestellBarAppAndAppVariants();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void purchaseOrderReceived() throws Exception {

        String shopOrderId = "shopOrderId" + UUID.randomUUID();
        String communityId = th.tenant1.getId();
        Shop sender = th.shop1;
        Person receiverPerson = th.personRegularAnna;

        ClientAddressDefinition deliveryAddress = ClientAddressDefinition.builder()
                .name("Balthasar Weitzel")
                .street("Pfaffenbärstraße 42")
                .zip("67663")
                .city("Kaiserslautern")
                .build();
        ClientAddressDefinition pickupAddress = ClientAddressDefinition.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .build();

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = ClientPurchaseOrderReceivedEvent.builder()
                .shopOrderId(shopOrderId)
                .communityId(communityId)
                .senderShopId(sender.getId())
                .purchaseOrderNotes("purchaseOrderNotes" + UUID.randomUUID())
                .pickupAddress(pickupAddress)
                .receiverPersonId(receiverPerson.getId())
                .deliveryAddress(deliveryAddress)
                .transportNotes("transportNotes" + UUID.randomUUID())
            .orderedItems(Arrays.asList(
                new ClientPurchaseOrderItem(2, "item", "unit"),
                new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")))
            .desiredDeliveryTime(new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + TimeUnit.HOURS.toMillis(3L)))
            .credits(8)
            .build();

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andReturn();

        // in some cases the event processing is not yet finished, so as a
        // workaround we wait
        waitForEventProcessing();

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of deliveries");
        Delivery delivery = deliveries.get(0);

        ParcelAddress actualPickupAddress = delivery.getPickupAddress();
        assertEquals(ParcelAddressType.SHOP, actualPickupAddress.getAddressType(), "Type of pickupAddress");
        assertEquals(th.shop1.getId(), actualPickupAddress.getShop().getId(), "Shop of pickupAddress");
        assertAddressEquals(pickupAddress, actualPickupAddress.getAddress(), "Details of pickupAddress");

        ParcelAddress actualDeliveryAddress = delivery.getDeliveryAddress();
        ClientAddressDefinition expectedAddress;

        //delivery to person
        expectedAddress = deliveryAddress;
        assertEquals(ParcelAddressType.PRIVATE, actualDeliveryAddress.getAddressType(), "Type of deliveryAddress");
        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(),
                actualDeliveryAddress.getPerson().getId(), "Person of deliveryAddress");
        assertAddressEquals(deliveryAddress, actualDeliveryAddress.getAddress(), "Details of deliveryAddress");

        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime(),
                delivery.getDesiredDeliveryTime().getStartTime(), "DesiredDeliveryTime start");
        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getEndTime(),
                delivery.getDesiredDeliveryTime().getEndTime(), "DesiredDeliveryTime end");

        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(),
                delivery.getReceiver().getPerson().getId(), "Receiver");
        assertEquals(LogisticsParticipantType.PRIVATE, delivery.getReceiver().getParticipantType(), "Receiver");
        assertEquals(purchaseOrderReceivedEvent.getSenderShopId(),
                delivery.getSender().getShop().getId(), "Sender");
        assertEquals(LogisticsParticipantType.SHOP, delivery.getSender().getParticipantType(), "Sender");

        assertEquals(purchaseOrderReceivedEvent.getTransportNotes(),
                delivery.getTransportNotes(), "TransportNotes");

        assertEquals(purchaseOrderReceivedEvent.getShopOrderId(),
                delivery.getCustomReferenceNumber(), "CustomReferenceNumber");

        // Check created purchase order
        List<PurchaseOrder> purchaseOrders = th.purchaseOrderRepository.findAll();
        assertEquals(1, purchaseOrders.size(), "Amount of purchaseOrders");
        PurchaseOrder purchaseOrder = purchaseOrders.get(0);

        assertEquals(purchaseOrderReceivedEvent.getPurchaseOrderNotes(),
                purchaseOrder.getPurchaseOrderNotes(), "PurchaseOrderNotes");

        // Check result
        ClientDeliveryCreatedEvent deliveryCreatedEvent = toObject(response.getResponse(),
                ClientDeliveryCreatedEvent.class);

        assertNotNull(deliveryCreatedEvent);
        assertEquals(delivery.getId(), deliveryCreatedEvent.getDeliveryId(), "DeliveryId");
        assertEquals(delivery.getTrackingCode(),
                deliveryCreatedEvent.getDeliveryTrackingCode(), "DeliveryTrackingCode");
        assertEquals(purchaseOrder.getId(), deliveryCreatedEvent.getPurchaseOrderId(), "PurchaseOrderId");

        // --> Check Delivery with service (receiver)
        mockMvc.perform(get("/logistics/delivery/").headers(authHeadersFor(receiverPerson)))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(delivery.getId()))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientDeliveryStatus.ORDERED.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiverPerson.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender.getId()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery.getTrackingCode()))

                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(expectedAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(expectedAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(expectedAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(expectedAddress.getCity()));
    }

    @Test
    public void purchaseOrderReceivedInvalidShopOrderId() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                null, // set shopOrderId to null!
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void purchaseOrderReceivedInvalidDeliveryAddress() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                null,// set deliveryAddress to null!
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void purchaseOrderReceivedInvalidPickupAddress() throws Exception {
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                null,// set pickupAddress to null!
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void purchaseOrderReceivedAuthentificationFail() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, "InvalidAPIKey")
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void purchaseOrderReceivedShopOrderIdAlreadyExist() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId",// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk());

        //PurchaseOrderReceived
        purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId",// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.PURCHASE_ORDER_INVALID));
    }

    @Test
    public void purchaseOrderReceivedDeliveryTimeEndBeforeStart() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() - 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.PURCHASE_ORDER_INVALID));
    }

    @Test
    public void purchaseOrderReceivedDeliveryTimeStartInPast() throws Exception{
        Person receiver = th.personVgAdmin;
        Shop sender = th.shop1;

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis() + 1000 * 60 * 60 * 3, System.currentTimeMillis()),
// TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(isException(ClientExceptionType.PURCHASE_ORDER_INVALID));
    }

    @Test
    public void purchaseOrderReceivedWithPoolingStation() throws Exception{
        Person receiver = th.personVgAdmin;

        Shop sender = th.shop1;

        assertNotNull(th.poolingStation1.getId());

        //PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                "shopOrderId" + UUID.randomUUID(),// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                sender.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                receiver.getId(),// String receiverPersonId;
                toClientAddressDefinition(th.poolingStation1.getAddress()),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                th.poolingStation1.getId(),// String deliveryPoolingStationId;
                42//credits
        );

        MvcResult response = mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(status().isOk()).andReturn();

        //Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        Delivery delivery = deliveries.get(0);

        ParcelAddress actualPickupAddress = delivery.getPickupAddress();
        assertEquals(ParcelAddressType.SHOP, actualPickupAddress.getAddressType(), "Type of pickupAddress");
        assertEquals(th.shop1.getId(), actualPickupAddress.getShop().getId(), "Shop of pickupAddress");
        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                actualPickupAddress.getAddress(), "Details of pickupAddress");

        ParcelAddress actualDeliveryAddress = delivery.getDeliveryAddress();
        assertEquals(ParcelAddressType.POOLING_STATION,
                actualDeliveryAddress.getAddressType(), "Type of deliveryAddress");
        assertNull(actualDeliveryAddress.getPerson());
        assertEquals(th.poolingStation1.getId(),
                actualDeliveryAddress.getPoolingStation().getId(), "Id of poolingstation");

        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                actualDeliveryAddress.getAddress(), "Details of deliveryAddress");

        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getStartTime(),
                delivery.getDesiredDeliveryTime().getStartTime(), "DesiredDeliveryTime start");
        assertEquals(purchaseOrderReceivedEvent.getDesiredDeliveryTime().getEndTime(),
                delivery.getDesiredDeliveryTime().getEndTime(), "DesiredDeliveryTime end");

        assertEquals(purchaseOrderReceivedEvent.getReceiverPersonId(),
                delivery.getReceiver().getPerson().getId(), "Receiver");
        assertEquals(LogisticsParticipantType.PRIVATE, delivery.getReceiver().getParticipantType(), "Receiver");
        assertEquals(purchaseOrderReceivedEvent.getSenderShopId(), delivery.getSender().getShop().getId(), "Sender");
        assertEquals(LogisticsParticipantType.SHOP, delivery.getSender().getParticipantType(), "Sender");
        assertEquals(10, delivery.getCredits(), "Credits Delivery");

        assertEquals(purchaseOrderReceivedEvent.getTransportNotes(), delivery.getTransportNotes(), "TransportNotes");

        //Check created purchase order
        List<PurchaseOrder> purchaseOrders = th.purchaseOrderRepository.findAll();
        assertEquals(1, purchaseOrders.size(), "Amount of purchaseOrders");
        PurchaseOrder purchaseOrder = purchaseOrders.get(0);

        assertEquals(purchaseOrderReceivedEvent.getPurchaseOrderNotes(),
                purchaseOrder.getPurchaseOrderNotes(), "PurchaseOrderNotes");
        assertEquals(purchaseOrderReceivedEvent.getCredits(), purchaseOrder.getCredits(), "Credits Purchase Order");

        //Check result
        ClientDeliveryCreatedEvent deliveryCreatedEvent =
                toObject(response.getResponse(), ClientDeliveryCreatedEvent.class);

        assertNotNull(deliveryCreatedEvent);
        assertEquals(delivery.getId(), deliveryCreatedEvent.getDeliveryId(), "DeliveryId");
        assertEquals(delivery.getTrackingCode(),
                deliveryCreatedEvent.getDeliveryTrackingCode(), "DeliveryTrackingCode");
        assertEquals(purchaseOrder.getId(), deliveryCreatedEvent.getPurchaseOrderId(), "PurchaseOrderId");
    }

    @Test
    public void purchaseOrderReadyForTransport() throws Exception{

        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk());

        List<Delivery> deliveries = th.deliveryRepository.findAllByCustomReferenceNumber(shopOrderId);
        assertEquals(1, deliveries.size());

        Optional<Delivery> originalDelivery =
                deliveries.stream().filter(d -> "contentNote".equals(d.getContentNotes())).findFirst();
        assertTrue(originalDelivery.isPresent());

        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                originalDelivery.get().getDeliveryAddress().getAddress(), "");

        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                originalDelivery.get().getPickupAddress().getAddress(), "");

        assertEquals(purchaseOrderReadyForTransportRequest.getSize().getWeight(),
                originalDelivery.get().getSize().getWeight(), 0.01);

        assertEquals(purchaseOrderReadyForTransportRequest.getPackagingType(),
                originalDelivery.get().getPackagingType());

        DeliveryStatus currentStatus =
                th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(originalDelivery.get())
                        .getStatus();
        assertEquals(DeliveryStatus.WAITING_FOR_ASSIGNMENT, currentStatus);
    }

    @Test
    public void purchaseOrderReadyForTransportDimensionsIncorrect() throws Exception{

        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(30.0)
                        .width(40.0)
                        .height(20.0)
                        .category(DimensionCategory.S)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
            .andExpect(status().isOk());

        waitForEventProcessing();
        Delivery delivery = th.deliveryRepository.findAll().get(0);
        Dimension actualDimension = delivery.getSize();

        assertEquals(40.0, actualDimension.getLength(), 0.0001d);
        assertEquals(30.0, actualDimension.getWidth(), 0.0001d);
        assertEquals(20.0, actualDimension.getHeight(), 0.0001d);
        assertEquals(DimensionCategory.M, actualDimension.getCategory());
        assertEquals(0.7, actualDimension.getWeight(), 0.0001d);
    }

    @Test
    public void purchaseOrderReadyForTransportDimensionsMissing() throws Exception{

        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                null, //size
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void purchaseOrderReadyForTransportNoPurchaseOrder() throws Exception{

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                null,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void purchaseOrderReadyForTransportNoType() throws Exception{
        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        createPurchaseOrderWithPoolingStationDeliveryAddress(shopOrderId);

        //PurchaseOrderReceived
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                null, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void purchaseOrderReadyForTransportAuthorizationFail() throws Exception{

        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        createPurchaseOrderWithPoolingStationDeliveryAddress(shopOrderId);

        //PurchaseOrderReceived
        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                2, // int numParcels;
                null
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, "InvalidAPIKey")
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void purchaseOrderReadyForTransportWithMultipleParcelsCreatesAllDeliveries() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                1, // int numParcels;
                null
        );

        List<ClientPurchaseOrderParcel> additionalParcels = new ArrayList<>();
        additionalParcels.add(ClientPurchaseOrderParcel.builder()
            .contentNotes("contentNote1")
            .packagingType(PackagingType.BAG)
            .size(ClientDimension.builder()
                .category(DimensionCategory.S)
                .build())
            .build());

        additionalParcels.add(ClientPurchaseOrderParcel.builder()
            .contentNotes("contentNote2")
            .packagingType(PackagingType.PARCEL)
            .size(ClientDimension.builder()
                .height(10.0)
                .width(20.0)
                .length(30.0)
                .weight(40.0)
                .build())
            .build());

        additionalParcels.add(ClientPurchaseOrderParcel.builder()
            .contentNotes("contentNote3")
            .packagingType(PackagingType.OTHER)
            .size(ClientDimension.builder()
                .category(DimensionCategory.L)
                .height(11.0)
                .width(22.0)
                .length(33.0)
                .weight(44.0)
                .build())
            .build());

        purchaseOrderReadyForTransportRequest.setAdditionalParcels(additionalParcels);

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        List<Delivery> deliveries = th.deliveryRepository.findAllByCustomReferenceNumber(shopOrderId);
        assertEquals(4, deliveries.size());

        Optional<Delivery> originalDelivery =
                deliveries.stream().filter(d -> "contentNote".equals(d.getContentNotes())).findFirst();
        assertTrue(originalDelivery.isPresent());
        Optional<Delivery> additionalDelivery1 = deliveries.stream().filter(d -> "contentNote1".equals(
                d.getContentNotes())).findFirst();
        assertTrue(additionalDelivery1.isPresent());
        Optional<Delivery> additionalDelivery2 = deliveries.stream().filter(d -> "contentNote2".equals(
                d.getContentNotes())).findFirst();
        assertTrue(additionalDelivery2.isPresent());
        Optional<Delivery> additionalDelivery3 = deliveries.stream().filter(d -> "contentNote3".equals(
                d.getContentNotes())).findFirst();
        assertTrue(additionalDelivery3.isPresent());

        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                originalDelivery.get().getDeliveryAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                additionalDelivery1.get().getDeliveryAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                additionalDelivery2.get().getDeliveryAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getDeliveryAddress(),
                additionalDelivery3.get().getDeliveryAddress().getAddress());

        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                originalDelivery.get().getPickupAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                additionalDelivery1.get().getPickupAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                additionalDelivery2.get().getPickupAddress().getAddress());
        assertAddressEquals(purchaseOrderReceivedEvent.getPickupAddress(),
                additionalDelivery3.get().getPickupAddress().getAddress());

        assertEquals(purchaseOrderReadyForTransportRequest.getSize().getWeight(),
                originalDelivery.get().getSize().getWeight(), 0.01);
        assertEquals(additionalParcels.get(0).getSize().getWeight(),
                additionalDelivery1.get().getSize().getWeight(), 0.01);
        assertEquals(additionalParcels.get(1).getSize().getWeight(),
                additionalDelivery2.get().getSize().getWeight(), 0.01);
        assertEquals(additionalParcels.get(2).getSize().getWeight(),
                additionalDelivery3.get().getSize().getWeight(), 0.01);

        assertEquals(purchaseOrderReadyForTransportRequest.getPackagingType(),
            originalDelivery.get().getPackagingType());
        assertEquals(additionalParcels.get(0).getPackagingType(),
            additionalDelivery1.get().getPackagingType());
        assertEquals(additionalParcels.get(1).getPackagingType(),
            additionalDelivery2.get().getPackagingType());
        assertEquals(additionalParcels.get(2).getPackagingType(),
            additionalDelivery3.get().getPackagingType());

        DeliveryStatus statusOriginalDelivery = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(originalDelivery.get()).getStatus();
        DeliveryStatus statusAdditionalDelivery1 = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(additionalDelivery1.get()).getStatus();
        DeliveryStatus statusAdditionalDelivery2 = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(additionalDelivery2.get()).getStatus();
        DeliveryStatus statusAdditionalDelivery3 = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(additionalDelivery3.get()).getStatus();

        assertEquals(DeliveryStatus.WAITING_FOR_ASSIGNMENT, statusOriginalDelivery);
        assertEquals(DeliveryStatus.WAITING_FOR_ASSIGNMENT, statusAdditionalDelivery1);
        assertEquals(DeliveryStatus.WAITING_FOR_ASSIGNMENT, statusAdditionalDelivery2);
        assertEquals(DeliveryStatus.WAITING_FOR_ASSIGNMENT, statusAdditionalDelivery3);
    }

    @Test
    public void purchaseOrderReadyForTransportWithMultipleParcelsDimensionsMissing() throws Exception {
        String shopOrderId = "shopOrderId" + UUID.randomUUID();

        createPurchaseOrderWithPersonDeliveryAddress(shopOrderId);

        ClientPurchaseOrderReadyForTransportRequest purchaseOrderReadyForTransportRequest = new ClientPurchaseOrderReadyForTransportRequest(
                shopOrderId,// String shopOrderId;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), //Address pickupAddress;
                ClientDimension.builder()
                        .length(40.0)
                        .width(30.0)
                        .height(20.0)
                        .category(DimensionCategory.M)
                        .weight(0.7)
                        .build(),
                "contentNote", // String contentNotes;
                PackagingType.PARCEL, //PackagingType packagingType;
                1, // int numParcels;
                null
        );

        List<ClientPurchaseOrderParcel> additionalParcels = new ArrayList<>();
        additionalParcels.add(ClientPurchaseOrderParcel.builder()
            .contentNotes("contentNote1")
            .packagingType(PackagingType.BAG)
            .build());

        purchaseOrderReadyForTransportRequest.setAdditionalParcels(additionalParcels);

        mockMvc.perform(post("/shopping/event/purchaseorderreadyfortransport")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReadyForTransportRequest)))
            .andExpect(status().isBadRequest());

        List<Delivery> deliveries = th.deliveryRepository.findAllByCustomReferenceNumber(shopOrderId);
        assertEquals(1, deliveries.size());

        DeliveryStatus currentStatus = th.deliveryStatusRecordRepository.findFirstByDeliveryOrderByTimeStampDesc(deliveries.get(0)).getStatus();
        assertEquals(DeliveryStatus.CREATED, currentStatus);
    }

    private ClientPurchaseOrderReceivedEvent createPurchaseOrderWithPersonDeliveryAddress(String shopOrderId) throws Exception {
        // PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                shopOrderId, // String shopOrderId;
                th.tenant1.getId(), // String communityId;
                th.shop1.getId(), // String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(), // String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), // Address pickupAddress;
                th.personVgAdmin.getId(), // String receiverPersonId;
                ClientAddressDefinition.builder()
                        .name("Balthasar Weitzel")
                        .street("Pfaffenbärstraße 42")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(), // Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(), // String transportNotes;
                Arrays.asList(new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
                // List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
                // TimeSpan desiredDeliveryTime;
                "",// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
            .andExpect(status().isOk()).andReturn();

        return purchaseOrderReceivedEvent;
    }

    private ClientPurchaseOrderReceivedEvent createPurchaseOrderWithPoolingStationDeliveryAddress(String shopOrderId) throws Exception {
        // PurchaseOrderReceived
        ClientPurchaseOrderReceivedEvent purchaseOrderReceivedEvent = new ClientPurchaseOrderReceivedEvent(
                shopOrderId,// String shopOrderId;
                th.tenant1.getId(),// String communityId;
                th.shop1.getId(),// String senderShopId;
                "purchaseOrderNotes" + UUID.randomUUID(),// String purchaseOrderNotes;
                ClientAddressDefinition.builder()
                        .name("Fraunhofer IESE")
                        .street("Fraunhofer-Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .build(),// Address pickupAddress;
                th.personVgAdmin.getId(),// String receiverPersonId;
                toClientAddressDefinition(th.poolingStation1.getAddress()),// Address deliveryAddress;
                "transportNotes" + UUID.randomUUID(),// String transportNotes;
                Arrays.asList(
                        new ClientPurchaseOrderItem(2, "item", "unit"),
                        new ClientPurchaseOrderItem(42, "Straußeneier", "große Stücke")),
// List<ClientPurchaseOrderItem> orderedItems;
                new TimeSpan(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 60 * 60 * 3),
// TimeSpan desiredDeliveryTime;
                th.poolingStation1.getId(),// String deliveryPoolingStationId;
                0//credits
        );

        mockMvc.perform(post("/shopping/event/purchaseorderreceived")
                        .header(HEADER_NAME_API_KEY, th.appVariantBestellBar1.getApiKey1())
                        .contentType(contentType)
                        .content(json(purchaseOrderReceivedEvent)))
                .andExpect(status().isOk()).andReturn();

        return purchaseOrderReceivedEvent;
    }

    protected ClientAddressDefinition toClientAddressDefinition(Address address) {
        return ClientAddressDefinition.builder()
                .name(address.getName())
                .street(address.getStreet())
                .zip(address.getZip())
                .city(address.getCity())
                .gpsLocation(address.getGpsLocation() == null ? null :
                        new ClientGPSLocation(address.getGpsLocation().getLatitude(),
                                address.getGpsLocation().getLongitude()))
                .build();
    }

}
