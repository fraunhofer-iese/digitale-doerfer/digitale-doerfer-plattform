/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class GrapevineUpdateParallelWorldStatusAdminTaskTest extends BaseServiceTest {

    @Autowired
    private IAdminTaskService adminTaskService;

    @Autowired
    private GrapevineUpdateParallelWorldStatusAdminTask grapevineUpdateParallelWorldStatusAdminTask;

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void runMigration() {

        Person creatorParallelWorld = th.personAnnikaSchneider;
        creatorParallelWorld.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(creatorParallelWorld);

        Person creatorRealWorld = th.personSusanneChristmann;
        creatorParallelWorld.getStatuses().removeValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(creatorRealWorld);

        //this post is in real world, but should be in parallel world
        Post postWrongInRealWorld = th.gossip1withImages;
        postWrongInRealWorld.setCreator(creatorParallelWorld);
        postWrongInRealWorld.setParallelWorld(false);
        th.postRepository.saveAndFlush(postWrongInRealWorld);

        //this post is in parallel world correctly
        Post postCorrectInParallelWorld = th.gossip2WithComments;
        postCorrectInParallelWorld.setCreator(creatorParallelWorld);
        postCorrectInParallelWorld.setParallelWorld(true);
        th.postRepository.saveAndFlush(postCorrectInParallelWorld);

        //this post is in parallel world, but should be in real world
        Post postWrongInParallelWorld = th.seeking1;
        postWrongInParallelWorld.setCreator(creatorRealWorld);
        postWrongInParallelWorld.setParallelWorld(true);
        th.postRepository.saveAndFlush(postWrongInParallelWorld);

        //this post is in real world correctly
        Post postCorrectInRealWorld = th.offer1;
        postCorrectInRealWorld.setCreator(creatorRealWorld);
        postCorrectInRealWorld.setParallelWorld(false);
        th.postRepository.saveAndFlush(postCorrectInRealWorld);

        assertThat(creatorRealWorld).isNotEqualTo(creatorParallelWorld);

        Post postUnchanged = th.gossip7;

        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskService.executeAdminTask(grapevineUpdateParallelWorldStatusAdminTask.getName(), adminTaskParameters);

        assertThat(th.postRepository.findById(postWrongInRealWorld.getId()).get().isParallelWorld()).isTrue();
        assertThat(th.postRepository.findById(postCorrectInParallelWorld.getId()).get().isParallelWorld()).isTrue();
        assertThat(th.postRepository.findById(postWrongInParallelWorld.getId()).get().isParallelWorld()).isFalse();
        assertThat(th.postRepository.findById(postCorrectInRealWorld.getId()).get().isParallelWorld()).isFalse();
        assertThat(th.postRepository.findById(postUnchanged.getId()).get().isParallelWorld()).isFalse();
    }

}
