/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shopping.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.shop.clientmodel.ClientOpeningHoursEntry;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shopping.ShoppingTestHelper;
import de.fhg.iese.dd.platform.api.shopping.clientevent.*;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.repos.ShopRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.mockito.stubbing.VoidAnswer1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MvcResult;

import java.net.URL;
import java.time.DayOfWeek;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ShopEventControllerTest extends BaseServiceTest {

    @MockBean
    protected IMediaItemService mediaItemServiceMock;

    @Autowired
    private ShoppingTestHelper th;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private ShopRepository shopRepository;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        initMediaItemServiceMock();
    }

    private void initMediaItemServiceMock() {
        Mockito.doAnswer(AdditionalAnswers.answerVoid(
                        (VoidAnswer1<MediaItem>) argument -> th.mediaItemRepository.deleteById(argument.getId())))
                .when(mediaItemServiceMock).deleteItem(Mockito.any());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void createShopWithMinimumData() throws Exception {

        String communityId = th.tenant1.getId();
        Person shopManager = th.personShopManager;
        String shopName = "Test-Shop";
        String shopEmail = "meister@test-shop.de";

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(communityId)
                .name(shopName)
                .email(shopEmail)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shop.id").isNotEmpty())
                .andReturn();

        ClientShopCreateConfirmation clientShopCreateConfirmation = toObject(createRequestResult.getResponse(), ClientShopCreateConfirmation.class);

        Optional<Shop> shopInRepository = th.shopRepository.findById(clientShopCreateConfirmation.getShop().getId());

        assertEquals(shopInRepository.get().getName(), shopName);
        assertEquals(shopInRepository.get().getEmail(), shopEmail);
        assertTrue(shopInRepository.get().getAddresses().isEmpty());
        assertNull(shopInRepository.get().getOpeningHours());
        assertNull(shopInRepository.get().getPhone());
        assertNull(shopInRepository.get().getProfilePicture());
        assertNull(shopInRepository.get().getWebShopURL());
    }

    @Test
    public void createShopWithCompleteData() throws Exception {

        String communityId = th.tenant1.getId();
        Person shopManager = th.personShopManager;
        String shopName = "Test-Shop";
        String shopEmail = "meister@test-shop.de";
        String shopPhone = "02741 123456";
        String shopWebShopURL = "https://test-shop.de";

        ClientAddressDefinition shopAddress = ClientAddressDefinition.builder()
                .name("Test-Shop")
                .street("Badstraße 11")
                .zip("67655")
                .city("Kaiserslautern")
                .build();

        ClientOpeningHoursEntry shopOpeningHoursMonday1 = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.MONDAY)
                .fromTime(900)
                .toTime(1200)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursMonday2 = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.MONDAY)
                .fromTime(1300)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursTuesday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.TUESDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursWednesday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.WEDNESDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursThursday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.THURSDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursFriday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.FRIDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        Set<ClientOpeningHoursEntry> shopOpeningHours = new HashSet<>(
                Arrays.asList(
                        shopOpeningHoursMonday1,
                        shopOpeningHoursMonday2,
                        shopOpeningHoursTuesday,
                        shopOpeningHoursWednesday,
                        shopOpeningHoursThursday,
                        shopOpeningHoursFriday
                )
        );

        MediaItem expectedImage = th.createMediaItem("shop-image.png");
        String shopProfilePictureURL = "http://example.org/shop-image.png";
        URL imageURL = new URL(shopProfilePictureURL);
        AppVariant appVariant = null;
        Mockito.when(mediaItemServiceMock.createMediaItem(imageURL,
                FileOwnership.of(shopManager, appVariant))).thenReturn(
                expectedImage);

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(communityId)
                .name(shopName)
                .email(shopEmail)
                .phone(shopPhone)
                .webShopURL(shopWebShopURL)
                .address(shopAddress)
                .profilePictureURL(shopProfilePictureURL)
                .openingHours(shopOpeningHours)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shop.id").isNotEmpty())
                .andReturn();

        ClientShopCreateConfirmation clientShopCreateConfirmation = toObject(createRequestResult.getResponse(), ClientShopCreateConfirmation.class);

        Optional<Shop> shopInRepository = th.shopRepository.findById(clientShopCreateConfirmation.getShop().getId());

        assertEquals(shopInRepository.get().getName(), shopName);
        assertEquals(shopInRepository.get().getEmail(), shopEmail);
        assertEquals(shopInRepository.get().getAddresses().size(), 1);
        assertEquals(shopInRepository.get().getOpeningHours().getEntries().size(), 6);
        assertEquals(shopInRepository.get().getPhone(), shopPhone);
        assertNotNull(shopInRepository.get().getProfilePicture());
        assertEquals(shopInRepository.get().getWebShopURL(), shopWebShopURL);
    }

    @Test
    public void shopCreateRequest_Unauthorized() throws Exception {

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(th.tenant1.getId())
                .name("Test-Shop")
                .email("meister@test-shop.de")
                .build();

        mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(post("/shop/event/shopCreateRequest")
                .contentType(contentType)
                .content(json(clientShopCreateRequest)));
    }

    @Test
    public void shopCreateRequest_ShopManagerWrongCommunity() throws Exception {

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(th.tenant2.getId())
                .name("Test-Shop")
                .email("meister@test-shop.de")
                .build();

        mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void shopCreateRequest_InvalidEmail() throws Exception {

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(th.tenant1.getId())
                .name("Test-Shop")
                .email("meister@test-shop.")
                .build();

        mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ADDRESS_INVALID));
    }

    @Test
    public void shopCreateRequest_TenantNotFound() throws Exception {

        ClientShopCreateRequest clientShopCreateRequest = ClientShopCreateRequest.builder()
                .tenantId(BaseTestHelper.INVALID_UUID)
                .name("Test-Shop")
                .email("meister@test-shop.de")
                .build();

        mockMvc.perform(post("/shop/event/shopCreateRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopCreateRequest)))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void shopUpdateRequest_MinimumDataShopManager() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;
        String shopName = "Updated Test-Shop";
        String shopEmail = "new_meister@test-shop.de";

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(shop.getId())
                .name(shopName)
                .email(shopEmail)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shop.id").value(shop.getId()))
                .andReturn();

        ClientShopUpdateConfirmation clientShopUpdateConfirmation = toObject(createRequestResult.getResponse(), ClientShopUpdateConfirmation.class);

        Optional<Shop> shopInRepository = th.shopRepository.findById(clientShopUpdateConfirmation.getShop().getId());

        assertEquals(shopInRepository.get().getName(), shopName);
        assertEquals(shopInRepository.get().getEmail(), shopEmail);
    }

    @Test
    public void shopUpdateRequest_MinimumDataShopOwner() throws Exception {

        Shop shop = th.shop1;
        Person shopOwner = shop.getOwner();
        String shopName = "Updated Test-Shop";
        String shopEmail = "new_meister@test-shop.de";

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(shop.getId())
                .name(shopName)
                .email(shopEmail)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(shopOwner))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shop.id").value(shop.getId()))
                .andReturn();

        ClientShopUpdateConfirmation clientShopUpdateConfirmation = toObject(createRequestResult.getResponse(), ClientShopUpdateConfirmation.class);

        Optional<Shop> shopInRepository = th.shopRepository.findById(clientShopUpdateConfirmation.getShop().getId());

        assertEquals(shopInRepository.get().getName(), shopName);
        assertEquals(shopInRepository.get().getEmail(), shopEmail);
        assertNotEquals(shopInRepository.get().getName(), shop.getName());
        assertNotEquals(shopInRepository.get().getEmail(), shop.getEmail());
    }

    @Test
    public void shopUpdateRequest_CompleteData() throws Exception {

        Shop oldShop = th.shop1;
        Person shopManager = th.personShopManager;
        String shopName = "Updated Test-Shop";
        String shopEmail = "new_meister@test-shop.de";
        String shopPhone = "02741 123456";
        String shopWebShopURL = "https://test-shop.de";

        ClientAddressDefinition shopAddress = ClientAddressDefinition.builder()
                .name("Test-Shop")
                .street("Badstraße 11")
                .zip("67655")
                .city("Kaiserslautern")
                .build();

        ClientOpeningHoursEntry shopOpeningHoursMonday1 = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.MONDAY)
                .fromTime(900)
                .toTime(1200)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursMonday2 = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.MONDAY)
                .fromTime(1300)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursTuesday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.TUESDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursWednesday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.WEDNESDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursThursday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.THURSDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        ClientOpeningHoursEntry shopOpeningHoursFriday = ClientOpeningHoursEntry.builder()
                .weekday(DayOfWeek.FRIDAY)
                .fromTime(900)
                .toTime(1700)
                .build();
        Set<ClientOpeningHoursEntry> shopOpeningHours = new HashSet<>(
                Arrays.asList(
                        shopOpeningHoursMonday1,
                        shopOpeningHoursMonday2,
                        shopOpeningHoursTuesday,
                        shopOpeningHoursWednesday,
                        shopOpeningHoursThursday,
                        shopOpeningHoursFriday
                )
        );

        MediaItem expectedImage = th.createMediaItem("shop-image.png");
        String shopProfilePictureURL = "http://example.org/shop-image.png";
        URL imageURL = new URL(shopProfilePictureURL);
        AppVariant appVariant = null;
        Mockito.when(
                mediaItemServiceMock.createMediaItem(imageURL,
                        FileOwnership.of(th.shop1.getOwner(), appVariant))).thenReturn(
                expectedImage);

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(oldShop.getId())
                .name(shopName)
                .email(shopEmail)
                .phone(shopPhone)
                .webShopURL(shopWebShopURL)
                .address(shopAddress)
                .profilePictureURL(shopProfilePictureURL)
                .openingHours(shopOpeningHours)
                .build();

        mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.shop.id").isNotEmpty());

        Optional<Shop> shopInRepository = th.shopRepository.findById(oldShop.getId());

        assertEquals(shopInRepository.get().getName(), shopName);
        assertEquals(shopInRepository.get().getEmail(), shopEmail);
        assertEquals(shopInRepository.get().getAddresses().size(), 1);
        assertEquals(shopInRepository.get().getOpeningHours().getEntries().size(), 6);
        assertEquals(shopInRepository.get().getPhone(), shopPhone);
        assertNotNull(shopInRepository.get().getProfilePicture());
        assertEquals(shopInRepository.get().getWebShopURL(), shopWebShopURL);

        assertNotEquals(shopInRepository.get().getName(), oldShop.getName());
        assertNotEquals(shopInRepository.get().getEmail(), oldShop.getEmail());
        assertNotEquals(shopInRepository.get().getAddresses(), oldShop.getAddresses());
        assertNotEquals(shopInRepository.get().getOpeningHours(), oldShop.getOpeningHours());
        assertNotEquals(shopInRepository.get().getPhone(), oldShop.getOpeningHours());
        assertNotEquals(shopInRepository.get().getProfilePicture(), oldShop.getProfilePicture());
        assertNotEquals(shopInRepository.get().getWebShopURL(), oldShop.getWebShopURL());
    }

    @Test
    public void shopUpdateRequest_NotFound() throws Exception {

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(BaseTestHelper.INVALID_UUID)
                .build();

        mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_FOUND));
    }

    @Test
    public void shopUpdateRequest_Unauthorized() throws Exception {

        Shop shop = th.shop1;
        Person personNotAuthorized = th.personRegularAnna;

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(shop.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(personNotAuthorized))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(post("/shop/event/shopUpdateRequest")
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)));
    }

    @Test
    public void shopUpdateRequest_ShopManagerWrongCommunity() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shopCommunity2 = th.shop3;

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(shopCommunity2.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopUpdateRequest")
                        .headers(authHeadersFor(shopManager))
                        .contentType(contentType)
                        .content(json(clientShopUpdateRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void shopUpdateRequest_InvalidEmail() throws Exception {

        ClientShopUpdateRequest clientShopUpdateRequest = ClientShopUpdateRequest.builder()
                .shopId(th.shop1.getId())
                .email("meister@test-shop.")
                .build();

        mockMvc.perform(post("/shop/event/shopUpdateRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopUpdateRequest)))
                .andExpect(isException(ClientExceptionType.EMAIL_ADDRESS_INVALID));
    }

    @Test
    public void shopSetOwnerRequest_ById() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;

        Person newOwner = th.personRegularAnna;

        List<Person> currentPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(shop.getId())
                .personId(newOwner.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.owner.firstName").value(newOwner.getFirstName()));

        List<Person> updatedPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        assertNotEquals(currentPersonsWithRoles, updatedPersonsWithRoles);
        assertThat(currentPersonsWithRoles).hasSize(1);
        assertThat(updatedPersonsWithRoles).hasSize(1);

        Optional<Shop> shopInRepository = shopRepository.findById(shop.getId());
        assertEquals(shopInRepository.get().getOwner().getFirstName(), newOwner.getFirstName());
    }

    @Test
    public void shopSetOwnerRequest_ByEmail() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;

        Person newOwner = th.personRegularAnna;

        List<Person> currentPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(shop.getId())
                .email(newOwner.getEmail())
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                        .headers(authHeadersFor(shopManager))
                        .contentType(contentType)
                        .content(json(clientShopSetOwnerRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.owner.firstName").value(newOwner.getFirstName()));

        List<Person> updatedPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        assertNotEquals(currentPersonsWithRoles, updatedPersonsWithRoles);
        assertThat(currentPersonsWithRoles).hasSize(1);
        assertThat(updatedPersonsWithRoles).hasSize(1);

        Optional<Shop> shopInRepository = shopRepository.findById(shop.getId());
        assertEquals(shopInRepository.get().getOwner().getFirstName(), newOwner.getFirstName());
    }

    @Test
    public void shopSetOwnerRequest_BothParams() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;

        Person newOwner = th.personRegularAnna;

        List<Person> currentPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(shop.getId())
                .personId(newOwner.getId())
                .email(newOwner.getEmail())
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                        .headers(authHeadersFor(shopManager))
                        .contentType(contentType)
                        .content(json(clientShopSetOwnerRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.owner.firstName").value(newOwner.getFirstName()));

        List<Person> updatedPersonsWithRoles = roleService.getAllPersonsWithRoleForEntity(ShopOwner.class, shop);

        assertNotEquals(currentPersonsWithRoles, updatedPersonsWithRoles);
        assertThat(currentPersonsWithRoles).hasSize(1);
        assertThat(updatedPersonsWithRoles).hasSize(1);

        Optional<Shop> shopInRepository = shopRepository.findById(shop.getId());
        assertEquals(shopInRepository.get().getOwner().getFirstName(), newOwner.getFirstName());
    }

    @Test
    public void shopSetOwnerRequest_NoIdOrEmail() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(shop.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shopSetOwnerRequest_IdNotFound() throws Exception {

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(th.shop1.getId())
                .personId(BaseTestHelper.INVALID_UUID)
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void shopSetOwnerRequest_EmailNotFound() throws Exception {

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(th.shop1.getId())
                .email("un@known.de")
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)))
                .andExpect(isException(ClientExceptionType.PERSON_WITH_EMAIL_NOT_FOUND));
    }

    @Test
    public void shopSetOwnerRequest_Unauthorized() throws Exception {

        ClientShopSetOwnerRequest clientShopSetOwnerRequest = ClientShopSetOwnerRequest.builder()
                .shopId(th.shop1.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopSetOwnerRequest")
                .headers(authHeadersFor(th.personRegularAnna))
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(post("/shop/event/shopSetOwnerRequest")
                .contentType(contentType)
                .content(json(clientShopSetOwnerRequest)));
    }

    @Test
    public void shopDeleteRequest() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;
        String shopId = shop.getId();

        ClientShopDeleteRequest clientShopDeleteRequest = ClientShopDeleteRequest.builder()
                .shopId(shop.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(jsonPath("$.shopId").value(shopId));

        mockMvc.perform(get("/shop/{shopId}", shopId)
                .headers(authHeadersFor(shopManager)))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_FOUND));

        assertFalse(th.shopRepository.existsById(shopId));
    }

    @Test
    public void shopDeleteRequest_ExistingOrders() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shop = th.shop1;
        th.createPurchaseOrder(shop, shopManager);

        ClientShopDeleteRequest clientShopDeleteRequest = ClientShopDeleteRequest.builder()
                .shopId(shop.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_DELETABLE));
    }

    @Test
    public void shopDeleteRequest_ShopNotFound() throws Exception {

        ClientShopDeleteRequest clientShopDeleteRequest = ClientShopDeleteRequest.builder()
                .shopId(BaseTestHelper.INVALID_UUID)
                .build();

        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(th.personShopManager))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(isException(ClientExceptionType.SHOP_NOT_FOUND));
    }

    @Test
    public void shopDeleteRequest_Unauthorized() throws Exception {

        Shop shop = th.shop1;
        Person shopOwner = shop.getOwner();
        Person personNotAuthorized = th.personRegularAnna;

        ClientShopDeleteRequest clientShopDeleteRequest = ClientShopDeleteRequest.builder()
                .shopId(shop.getId())
                .build();

        //the shop owner is not the shop manager
        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(shopOwner))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(personNotAuthorized))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        assertOAuth2(post("/shop/event/shopDeleteRequest")
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)));
    }

    @Test
    public void shopDeleteRequest_WrongTenant() throws Exception {

        Person shopManager = th.personShopManager;
        Shop shopCommunity2 = th.shop3;

        ClientShopDeleteRequest clientShopDeleteRequest = ClientShopDeleteRequest.builder()
                .shopId(shopCommunity2.getId())
                .build();

        mockMvc.perform(post("/shop/event/shopDeleteRequest")
                .headers(authHeadersFor(shopManager))
                .contentType(contentType)
                .content(json(clientShopDeleteRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

}
