/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import com.google.common.collect.Streams;
import de.fhg.iese.dd.platform.api.grapevine.ClientPostPageBean;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPost;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientPostType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PostControllerOwnTest extends BasePostControllerTest {

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllOwnPosts_SortedByLastActivity() throws Exception {

        Person caller = th.personLaraSchaefer;
        List<Post> expectedPosts = th.postRepository.findAllFull().stream()
                .filter(p -> p.isNotDeleted() && caller.equals(p.getCreator()))
                .sorted(Comparator.comparingLong(Post::getLastActivity).reversed())
                .collect(Collectors.toList());

        assertThat(expectedPosts.size()).isGreaterThanOrEqualTo(5);
        int pageSize = 10;

        AppVariant appVariant = th.appVariant4AllTenants;

        //TODO: remove the deprecated lastActivity parameter if all clients are updated
        mockMvc.perform(get("/grapevine/post/own")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("lastActivity", String.valueOf(true))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/own")
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .param("sortBy", "LAST_ACTIVITY")
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(assertPostsInOrder(caller, expectedPosts, 0, pageSize));
    }

    @Test
    public void getAllOwnPosts_Gossip() throws Exception {

        Person caller = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;

        // sort the expected gossips by created descending
        List<Gossip> expectedGossips = Streams.concat(th.gossipsTenant1.stream(), th.gossipsTenant2.stream())
                .filter(g -> caller.equals(g.getCreator()) && g.isNotDeleted())
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(4, expectedGossips.size());
        int pageSize = 2;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.GOSSIP.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedGossips.size()))
                .andExpect(assertPostsInOrder(caller, expectedGossips, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.GOSSIP.name())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(1, pageSize, expectedGossips.size()))
                .andExpect(assertPostsInOrder(caller, expectedGossips, 1, pageSize));
    }

    @Test
    public void getAllOwnPosts_OutOfRange() throws Exception {

        Person caller = th.personThomasBecker;

        // sort the expected gossips by created descending
        List<Gossip> expectedGossips = th.gossipsTenant1.stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedGossips.size());

        int pageSize = 2;

        AppVariant appVariant = th.appVariant4AllTenants;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.GOSSIP.name())
                        .param("page", "2")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(2, pageSize, expectedGossips.size()));
    }

    @Test
    public void getAllOwnPosts_Suggestion() throws Exception {

        Person caller = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;

        // sort the expected gossips by created descending
        List<Suggestion> expectedSuggestions = th.suggestionsTenant1.stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedSuggestions.size());
        int pageSize = 2;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SUGGESTION.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedSuggestions.size()))
                .andExpect(assertPostsInOrder(caller, expectedSuggestions, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SUGGESTION.name())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(1, pageSize, expectedSuggestions.size()))
                .andExpect(assertPostsInOrder(caller, expectedSuggestions, 1, pageSize));

        ClientPostPageBean allActualPosts = toObject(mockMvc.perform(get("/grapevine/post/own")
                                .param("page", "0")
                                .param("count", "100")
                                .headers(authHeadersFor(caller, appVariant)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andReturn(),
                ClientPostPageBean.class);

        assertThat(allActualPosts.getContent()).extracting(ClientPost::getId)
                .contains(expectedSuggestions.get(0).getId(),
                        expectedSuggestions.get(1).getId(),
                        expectedSuggestions.get(2).getId());
    }

    @Test
    public void getAllOwnPosts_InternalSuggestion() throws Exception {

        Person caller = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;

        List<Suggestion> ownSuggestions = th.suggestionsTenant1.stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        //internal suggestions should not be delivered at this endpoint
        assertEquals(3, ownSuggestions.size());
        final Suggestion internalSuggestion1 = ownSuggestions.get(0);
        final Suggestion internalSuggestion2 = ownSuggestions.get(1);
        internalSuggestion1.setInternal(true);
        internalSuggestion2.setInternal(true);
        th.postRepository.save(internalSuggestion1);
        th.postRepository.save(internalSuggestion2);

        Suggestion expectedSuggestion = ownSuggestions.get(2);
        int pageSize = 2;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SUGGESTION.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, 1))
                .andExpect(assertPostAttributes(caller, ".content[0]", expectedSuggestion));

        pageSize = 100;
        ClientPostPageBean allActualPosts = toObject(mockMvc.perform(get("/grapevine/post/own")
                                .param("page", "0")
                                .param("count", String.valueOf(pageSize))
                                .headers(authHeadersFor(caller, appVariant)))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(contentType))
                        .andExpect(hasCorrectPageData(0, pageSize, 11))
                        .andReturn(),
                ClientPostPageBean.class);

        assertThat(allActualPosts.getContent()).extracting(ClientPost::getId)
                .doesNotContain(internalSuggestion1.getId(), internalSuggestion2.getId());
    }

    @Test
    public void getAllOwnPosts_Seeking() throws Exception {

        Person caller = th.personSusanneChristmann;
        AppVariant appVariant = th.appVariant4AllTenants;

        // sort the expected gossips by created descending
        List<Seeking> expectedSeekings = th.seekingsTenant1
                .stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(3, expectedSeekings.size());
        int pageSize = 2;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SEEKING.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedSeekings.size()))
                .andExpect(assertPostsInOrder(caller, expectedSeekings, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SEEKING.name())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(1, pageSize, expectedSeekings.size()))
                .andExpect(assertPostsInOrder(caller, expectedSeekings, 1, pageSize));
    }

    @Test
    public void getAllOwnPosts_Offer() throws Exception {

        Person caller = th.personThomasBecker;
        AppVariant appVariant = th.appVariant4AllTenants;

        // sort the expected posts by created descending
        List<Offer> expectedOffers = th.offersTenant1
                .stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedOffers.size());
        int pageSize = 1;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.OFFER.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedOffers.size()))
                .andExpect(assertPostsInOrder(caller, expectedOffers, 0, pageSize));

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.OFFER.name())
                        .param("page", "1")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(1, pageSize, expectedOffers.size()))
                .andExpect(assertPostsInOrder(caller, expectedOffers, 1, pageSize));
    }

    @Test
    public void getAllOwnPosts_SpecialPost() throws Exception {

        Person caller = th.personThomasBecker;

        // sort the expected posts by created descending
        List<SpecialPost> expectedSpecialPosts = th.specialPostsTenant2
                .stream()
                .filter(g -> caller.equals(g.getCreator()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        assertEquals(2, expectedSpecialPosts.size());
        int pageSize = 4;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.SPECIAL_POST.name())
                        .param("page", "0")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedSpecialPosts.size()))
                .andExpect(assertPostsInOrder(caller, expectedSpecialPosts, 0, pageSize));
    }

    @Test
    public void getAllOwnPosts_Happening() throws Exception {

        Happening happening1 = th.happening1;
        Happening happening2 = th.happening2;
        Person caller = th.personAnnikaSchneider;
        happening1.setCreator(caller);
        happening2.setCreator(caller);
        List<Happening> expectedHappenings = Arrays.asList(happening2, happening1);
        th.postRepository.saveAll(expectedHappenings);
        int pageSize = 4;

        mockMvc.perform(get("/grapevine/post/own")
                        .param("type", ClientPostType.HAPPENING.name())
                        .param("sortBy", "HAPPENING_START")
                        .param("count", String.valueOf(pageSize))
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedHappenings.size()))
                .andExpect(assertPostsInOrder(caller, expectedHappenings, 0, pageSize));
    }

    @Test
    public void getAllOwnPosts_NewsItem() throws Exception {

        NewsItem newsItem1 = th.newsItem1;
        NewsItem newsItem2 = th.newsItem2;
        NewsItem newsItem3 = th.newsItem3;

        Person caller = th.personAnnikaSchneider;
        newsItem1.setCreator(caller);
        newsItem2.setCreator(caller);
        newsItem3.setCreator(caller);
        List<NewsItem> expectedNewsItems = Arrays.asList(newsItem1, newsItem2, newsItem3);
        th.postRepository.saveAll(expectedNewsItems);

        int pageSize = DEFAULT_PAGE_SIZE;

        mockMvc.perform(get("/grapevine/post/own")
                        .headers(authHeadersFor(caller, th.appVariant4AllTenants))
                        .param("type", ClientPostType.NEWS_ITEM.name())
                        .param("sortBy", "CREATED"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(hasCorrectPageData(0, pageSize, expectedNewsItems.size()))
                .andExpect(assertPostsInOrder(caller, expectedNewsItems, 0, pageSize));
    }

    @Test
    public void getAllOwnPosts_Unauthorized() throws Exception {

        assertOAuth2(get("/grapevine/post/own"));
    }

}
