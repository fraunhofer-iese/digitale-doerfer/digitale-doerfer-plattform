/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;

public class GrapevinePushCategoryMigrationAdminTaskTest extends BaseServiceTest {

    @Autowired
    private IAdminTaskService adminTaskExecutor;

    @Autowired
    private GrapevinePushCategoryMigrationAdminTask grapevinePushCategoryMigrationAdminTask;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;

    @Autowired
    private GrapevineTestHelper th;

    private AppVariant appVariant;

    private Person person;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();

        appVariant = th.appVariant1Tenant1;
        person = th.personSusanneChristmann;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void migrateGrapevinePushCategories() {

        // Setup test case
        final PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);

        //make one sub-push category invisible, this should be ignored
        final PushCategory pushCategoryGossip =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED);
        pushCategoryGossip.setVisible(false);
        pushCategoryRepository.save(pushCategoryGossip);

        assertEquals(0, pushCategoryUserSettingRepository.count());

        pushCategoryUserSettingRepository.save(PushCategoryUserSetting.builder()
                .appVariant(appVariant)
                .pushCategory(pushCategory)
                .person(person)
                .loudPushEnabled(false)
                .build());

        // Execute admin task, expect 6 newly created PushCategoryUserSettings
        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskExecutor.executeAdminTask(grapevinePushCategoryMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals(8, pushCategoryUserSettingRepository.count());

        // Execute admin task again, expect no newly created PushCategoryUserSettings
        adminTaskExecutor.executeAdminTask(grapevinePushCategoryMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals(8, pushCategoryUserSettingRepository.count());
    }

    @Test
    public void migrateGrapevinePushCategories_dryrun() {

        // Setup test case
        final PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);

        assertEquals(0, pushCategoryUserSettingRepository.count());

        pushCategoryUserSettingRepository.save(PushCategoryUserSetting.builder()
                .appVariant(appVariant)
                .pushCategory(pushCategory)
                .person(person)
                .loudPushEnabled(false)
                .build());

        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(true)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskExecutor.executeAdminTask(grapevinePushCategoryMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals(1, pushCategoryUserSettingRepository.count());
    }

    @Test
    public void migrateGrapevinePushCategories_noMigrationNeeded() {

        // Setup test case
        final PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);

        assertEquals(0, pushCategoryUserSettingRepository.count());

        // This entry should not exist, since the settings are the same as the default ones
        // Nevertheless there are currently two entries of this type on stage...
        pushCategoryUserSettingRepository.saveAndFlush(PushCategoryUserSetting.builder()
                .person(person)
                .pushCategory(pushCategory)
                .appVariant(appVariant)
                .loudPushEnabled(true)
                .build());

        assertEquals(1, pushCategoryUserSettingRepository.count());

        // Execute admin task, expect no newly created PushCategoryUserSettings
        final BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(false)
                .delayBetweenPagesMilliseconds(200)
                .parallelismPerPage(2)
                .pageTimeoutSeconds(2)
                .pageSize(100)
                .pageFrom(0)
                .pageTo(10)
                .build();

        adminTaskExecutor.executeAdminTask(grapevinePushCategoryMigrationAdminTask.getName(), adminTaskParameters);

        assertEquals(0, pushCategoryUserSettingRepository.count());
    }

}
