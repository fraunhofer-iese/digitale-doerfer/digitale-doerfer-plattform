/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.communication;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.communication.CommunicationTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public class ChatDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private CommunicationTestHelper communicationTestHelper;

    @Override
    public Person getPersonForPrivacyReport() {
        return communicationTestHelper.chat3Participant1.getPerson();
    }

    @Override
    public Person getPersonForDeletion() {
        return getPersonForPrivacyReport();
    }

    @Override
    public void createEntities() throws Exception {

        communicationTestHelper.createTenantsAndGeoAreas();
        communicationTestHelper.createPersons();
        communicationTestHelper.createAppEntities();
        communicationTestHelper.createChatEntities();

    }

    @Override
    public void tearDown() throws Exception {
        communicationTestHelper.deleteAllData();
    }

}
