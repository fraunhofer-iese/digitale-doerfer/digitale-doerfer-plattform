/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.controllers.testclasses.BaseControllerTestController;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.AppVariantOverrideFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BaseControllerAuthorizationInformationTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private BaseControllerTestController testController;

    private Person person;
    private AppVariant appVariantWithApiKey1;
    private AppVariant appVariantWithApiKey2;
    private AppVariant appVariantWithApiKey3;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        person = th.personRegularAnna;
        App apiKeyApp = th.appRepository.save(App.builder()
                .name("api key app")
                .appIdentifier("apikey.app")
                .build());
        appVariantWithApiKey1 = th.appVariantRepository.save(AppVariant.builder()
                .name("api key app variant 1")
                .appVariantIdentifier("apikey.app.variant.1")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .build());
        th.mapGeoAreaToAppVariant(appVariantWithApiKey1, th.geoAreaTenant1, th.tenant1);
        appVariantWithApiKey2 = th.appVariantRepository.save(AppVariant.builder()
                .name("api key app variant 2")
                .appVariantIdentifier("apikey.app.variant.2")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .build());
        th.mapGeoAreaToAppVariant(appVariantWithApiKey2, th.geoAreaTenant2, th.tenant2);
        appVariantWithApiKey3 = th.appVariantRepository.save(AppVariant.builder()
                .name("api key app variant 3")
                .appVariantIdentifier("apikey.app.variant.3")
                .apiKey1(UUID.randomUUID().toString())
                .apiKey2(UUID.randomUUID().toString())
                .app(apiKeyApp)
                .build());
        th.mapGeoAreaToAppVariant(appVariantWithApiKey3, th.geoAreaTenant3, th.tenant3);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
        testController.resetCapturedAuthorization();
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_OneAppVariant_AppVariantIdentifier_OverrideFeature()
            throws Exception {

        AppVariant appVariant = th.app1Variant1;
        AppVariant appVariantToOverwrite = th.app2Variant2;
        AppVariant appVariantOverwriteFails = th.app2Variant1;
        OauthClient oauthClient = th.app1Variant1OauthClient;

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(AppVariantOverrideFeature.class.getName())
                .configValues(
                        "{ \"allowedAppVariantIdentifiers\": [\"" +
                                appVariantToOverwrite.getAppVariantIdentifier() + "\"] }")
                .build());

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient))
                .collect(Collectors.toList())).containsExactly(appVariant);
        assertThat(appVariant.getOauthClients()).hasSize(1);

        callTestEndpoint(appVariant, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .oauthClient(oauthClient)
                .build());

        //the oauth client is only used by one app variant, so the app variant identifier is not required
        callTestEndpointWithToken(appVariant, oauthClient, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .build());

        //the app variant identifier overwrites the one of the oauth client because it is configured in the feature
        callTestEndpointWithToken(appVariantToOverwrite, oauthClient, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(appVariantToOverwrite)
                .build());

        //the app variant identifier fails to overwrites the one of the oauth client because it is not configured in the feature
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(appVariantOverwriteFails)
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_OneAppVariant_AppVariantIdentifier_OverrideFeature_Invalid()
            throws Exception {

        AppVariant appVariant = th.app1Variant1;
        OauthClient oauthClient = th.app1Variant1OauthClient;

        String invalidAppVariantIdentifier = "something.weird";
        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(AppVariantOverrideFeature.class.getName())
                .configValues(
                        "{ \"allowedAppVariantIdentifiers\": [\"" + invalidAppVariantIdentifier + "\"] }")
                .build());

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient))
                .collect(Collectors.toList())).containsExactly(appVariant);
        assertThat(appVariant.getOauthClients()).hasSize(1);

        //the app variant identifier overwrites the one of the oauth client because it is configured in the feature
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(th.app2Variant1)
                .build());

        //the app variant identifier overwrites the one of the oauth client, but there is no matching app variant
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariantIdentifier(invalidAppVariantIdentifier)
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_OneAppVariant_AppVariantIdentifier() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        OauthClient oauthClient = th.app1Variant1OauthClient;

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient))
                .collect(Collectors.toList())).containsExactly(appVariant);
        assertThat(appVariant.getOauthClients()).hasSize(1);

        callTestEndpoint(appVariant, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .oauthClient(oauthClient)
                .build());

        //the oauth client is only used by one app variant, so the app variant identifier is not required
        callTestEndpointWithToken(appVariant, oauthClient, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .build());

        //the app variant identifier does not match with the app variant found based on the oauth client
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(th.app2Variant1)
                .build());

        //the api key does not match with the app variant found based on the oauth client
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .apiKey("schicke ich einfach mal mit")
                .build());

        //the api key does not match with the app variant found based on the oauth client
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .apiKey("schicke ich einfach mal mit")
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_OneAppVariant_ApiKey() throws Exception {

        OauthClient oauthClient = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-oauthclient-identifier")
                .build());
        appVariantWithApiKey1.setOauthClients(Collections.singleton(oauthClient));
        th.appVariantRepository.save(appVariantWithApiKey1);

        callTestEndpoint(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey1())
                .oauthClient(oauthClient)
                .build());
        callTestEndpoint(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey2())
                .oauthClient(oauthClient)
                .build());

        //the oauth client is only used by one app variant, so the api key is not required
        callTestEndpointWithToken(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .build());

        //the api key does not match with the app variant found based on the oauth client
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .apiKey("super secret key")
                .build());
    }

    @Test
    public void getAuthorizationInformation_TwoOauthClients_OneAppVariant() throws Exception {

        //there are two oauth clients for the same app variant
        AppVariant appVariant = th.app1Variant2;
        OauthClient oauthClient1 = th.app1Variant2OauthClient1;
        OauthClient oauthClient2 = th.app1Variant2OauthClient2;

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient1) || a.getOauthClients().contains(oauthClient2))
                .collect(Collectors.toList())).containsExactly(appVariant);
        assertThat(appVariant.getOauthClients()).hasSize(2);

        callTestEndpoint(appVariant, oauthClient1, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .oauthClient(oauthClient1)
                .build());

        callTestEndpoint(appVariant, oauthClient2, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .oauthClient(oauthClient2)
                .build());

        //the oauth clients are only used by one app variant, so the app variant identifier is not required
        callTestEndpointWithToken(appVariant, oauthClient1, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient1)
                .build());

        callTestEndpointWithToken(appVariant, oauthClient2, AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient2)
                .build());

        //the app variant identifier does not match with the app variant found based on the oauth client
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient1)
                .appVariant(th.app2Variant1)
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_TwoAppVariants_AppVariantIdentifier() throws Exception {

        OauthClient oauthClient = th.app2OauthClient;
        AppVariant appVariant1 = th.app2Variant1;
        AppVariant appVariant2 = th.app2Variant2;

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient))
                .collect(Collectors.toList())).containsExactlyInAnyOrder(appVariant1, appVariant2);
        assertThat(appVariant1.getOauthClients()).hasSize(1);
        assertThat(appVariant1.getOauthClients()).hasSize(1);

        callTestEndpoint(appVariant1, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant1)
                .oauthClient(oauthClient)
                .build());

        callTestEndpoint(appVariant2, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant2)
                .oauthClient(oauthClient)
                .build());

        //the oauth client is used by two app variants, so the app variant identifier is required
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .build());

        //the app variant does not match with the oauth client
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(th.app1Variant1)
                .build());
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariantIdentifier("my own identifier")
                .build());
        //the api key does not match with the app variant found based on the oauth client and app variant identifier
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(appVariant1)
                .apiKey("schicke ich einfach mal mit")
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_TwoAppVariants_ApiKey() throws Exception {

        OauthClient oauthClient = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-oauthclient-identifier")
                .build());
        appVariantWithApiKey1.setOauthClients(Collections.singleton(oauthClient));
        appVariantWithApiKey2.setOauthClients(Collections.singleton(oauthClient));
        th.appVariantRepository.save(appVariantWithApiKey1);
        th.appVariantRepository.save(appVariantWithApiKey2);

        //match based on api key
        callTestEndpoint(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey1())
                .oauthClient(oauthClient)
                .build());
        callTestEndpoint(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey2())
                .oauthClient(oauthClient)
                .build());

        callTestEndpoint(appVariantWithApiKey2, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey1())
                .oauthClient(oauthClient)
                .build());
        callTestEndpoint(appVariantWithApiKey2, oauthClient, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey2())
                .oauthClient(oauthClient)
                .build());

        //match based on app variant identifier
        callTestEndpoint(appVariantWithApiKey2, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariantWithApiKey2)
                .oauthClient(oauthClient)
                .build());
        callTestEndpoint(appVariantWithApiKey1, oauthClient, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariantWithApiKey1)
                .oauthClient(oauthClient)
                .build());

        //the api key does not match with any of the app variants of the oauth client
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .apiKey(appVariantWithApiKey3.getApiKey1())
                .build());
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .apiKey("my own key")
                .build());

        //the api key does not match with the app variant that was identified by the app variant identifier
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(appVariantWithApiKey1)
                .apiKey("my own key")
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_TwoAppVariants_ApiKey_SameApiKey() throws Exception {

        OauthClient oauthClient = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-oauthclient-identifier")
                .build());
        appVariantWithApiKey1.setOauthClients(Collections.singleton(oauthClient));
        appVariantWithApiKey2.setOauthClients(Collections.singleton(oauthClient));
        appVariantWithApiKey2.setApiKey1(appVariantWithApiKey1.getApiKey1());
        appVariantWithApiKey2.setApiKey2(appVariantWithApiKey1.getApiKey2());
        th.appVariantRepository.save(appVariantWithApiKey1);
        th.appVariantRepository.save(appVariantWithApiKey2);

        //there are multiple matching app variants
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .apiKey(appVariantWithApiKey1.getApiKey1())
                .build());
    }

    @Test
    public void getAuthorizationInformation_TwoOauthClients_TwoAppVariants_AppVariantIdentifier() throws Exception {

        AppVariant appVariant1 = th.app2Variant1;
        AppVariant appVariant2 = th.app2Variant2;
        OauthClient oauthClient1 = th.app2OauthClient;
        OauthClient oauthClient2 = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("app2OauthClient2-identifier")
                .build());

        appVariant1.setOauthClients(new HashSet<>(Arrays.asList(oauthClient1, oauthClient2)));
        appVariant2.setOauthClients(new HashSet<>(Arrays.asList(oauthClient1, oauthClient2)));
        th.appVariantRepository.save(appVariant1);
        th.appVariantRepository.save(appVariant2);

        assertThat(th.appVariantRepository.findAllFullNotDeleted().stream()
                .filter(a -> a.getOauthClients() != null)
                .filter(a -> a.getOauthClients().contains(oauthClient1) || a.getOauthClients().contains(oauthClient2))
                .collect(Collectors.toList())).containsExactlyInAnyOrder(appVariant1, appVariant2);
        assertThat(appVariant1.getOauthClients()).hasSize(2);
        assertThat(appVariant2.getOauthClients()).hasSize(2);

        callTestEndpoint(appVariant1, oauthClient1, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant1)
                .oauthClient(oauthClient1)
                .build());

        callTestEndpoint(appVariant1, oauthClient2, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant1)
                .oauthClient(oauthClient2)
                .build());

        callTestEndpoint(appVariant2, oauthClient1, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant2)
                .oauthClient(oauthClient1)
                .build());

        callTestEndpoint(appVariant2, oauthClient2, AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant2)
                .oauthClient(oauthClient2)
                .build());

        //the oauth client is used by two app variants, so the app variant identifier is required
        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient1)
                .build());

        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient2)
                .build());
    }

    @Test
    public void getAuthorizationInformation_TwoOauthClients_TwoAppVariants_ApiKey() throws Exception {

        OauthClient oauthClient1 = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-ouathclient-identifier1")
                .build());
        OauthClient oauthClient2 = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("apikey-ouathclient-identifier2")
                .build());
        appVariantWithApiKey1.setOauthClients(new HashSet<>(Arrays.asList(oauthClient1, oauthClient2)));
        appVariantWithApiKey2.setOauthClients(new HashSet<>(Arrays.asList(oauthClient1, oauthClient2)));
        th.appVariantRepository.save(appVariantWithApiKey1);
        th.appVariantRepository.save(appVariantWithApiKey2);

        callTestEndpoint(appVariantWithApiKey1, oauthClient1, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey1())
                .oauthClient(oauthClient1)
                .build());
        callTestEndpoint(appVariantWithApiKey1, oauthClient1, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey2())
                .oauthClient(oauthClient1)
                .build());

        callTestEndpoint(appVariantWithApiKey1, oauthClient2, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey1())
                .oauthClient(oauthClient2)
                .build());
        callTestEndpoint(appVariantWithApiKey1, oauthClient2, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey1.getApiKey2())
                .oauthClient(oauthClient2)
                .build());

        callTestEndpoint(appVariantWithApiKey2, oauthClient1, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey1())
                .oauthClient(oauthClient1)
                .build());
        callTestEndpoint(appVariantWithApiKey2, oauthClient1, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey2())
                .oauthClient(oauthClient1)
                .build());

        callTestEndpoint(appVariantWithApiKey2, oauthClient2, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey1())
                .oauthClient(oauthClient2)
                .build());
        callTestEndpoint(appVariantWithApiKey2, oauthClient2, AuthorizationData.builder()
                .person(person)
                .apiKey(appVariantWithApiKey2.getApiKey2())
                .oauthClient(oauthClient2)
                .build());
    }

    @Test
    public void getAuthorizationInformation_OneOauthClient_NoAppVariant() throws Exception {

        OauthClient oauthClient = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier("secret-oauthclient-identifier")
                .build());

        callTestEndpointAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .build());

        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClient(oauthClient)
                .appVariant(th.app1Variant1)
                .build());
    }

    @Test
    public void getAuthorizationInformation_NoOauthClient_OneAppVariant() throws Exception {

        AppVariant appVariant = th.appVariantRepository.save(AppVariant.builder()
                .name("My secret app variant")
                .appVariantIdentifier("my.secret.app-variant")
                .app(th.app1)
                .build());
        th.mapGeoAreaToAppVariant(appVariant, th.geoAreaTenant1, th.tenant1);

        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .build());
    }

    @Test
    public void getAuthorizationInformation_InvalidParameters() throws Exception {

        String invalidOauthClientIdentifier = "my-fake-identifier";
        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClientIdentifier(invalidOauthClientIdentifier)
                .build());

        callTestEndpointWithTokenAndExpectNotFound(AuthorizationData.builder()
                .person(person)
                .oauthClientIdentifier(invalidOauthClientIdentifier)
                .appVariant(th.app1Variant1)
                .build());
    }

    private void callTestEndpoint(AppVariant expectedAppVariant, OauthClient expectedOauthClient,
            AuthorizationData authorizationData)
            throws Exception {
        callTestEndpointWithToken(expectedAppVariant, expectedOauthClient, authorizationData);
        callTestEndpointPublic(expectedAppVariant, authorizationData.withoutAccessToken());
    }

    private void callTestEndpointWithToken(AppVariant expectedAppVariant, OauthClient expectedOauthClient,
            AuthorizationData authorizationData)
            throws Exception {

        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(authorizationData)))
                .andExpect(status().isOk());

        assertEquals(person, testController.getPerson());
        assertEquals(expectedAppVariant, testController.getAppVariant());
        assertEquals(expectedOauthClient, testController.getOauthClient());
        testController.resetCapturedAuthorization();
    }

    private void callTestEndpointPublic(AppVariant expectedAppVariant, AuthorizationData authorizationData)
            throws Exception {

        mockMvc.perform(get("/test/public")
                .headers(authHeadersFor(authorizationData)))
                .andExpect(status().isOk());

        assertNull(testController.getPerson());
        assertNull(testController.getOauthClient());
        assertEquals(expectedAppVariant, testController.getAppVariant());
        testController.resetCapturedAuthorization();
    }

    private void callTestEndpointAndExpectNotFound(AuthorizationData authorizationData) throws Exception {

        callTestEndpointWithTokenAndExpectNotFound(authorizationData);

        callTestEndpointPublicAndExpectNotFound(authorizationData);
    }

    private void callTestEndpointWithTokenAndExpectNotFound(AuthorizationData authorizationData) throws Exception {

        if (authorizationData.getOauthClientIdentifier() != null) {
            mockMvc.perform(get("/test/test")
                    .headers(authHeadersFor(authorizationData)))
                    .andExpect(isExceptionWithMessageContains(ClientExceptionType.APP_VARIANT_NOT_FOUND,
                            authorizationData.getOauthClientIdentifier()));
        } else {
            mockMvc.perform(get("/test/test")
                    .headers(authHeadersFor(authorizationData)))
                    .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
        }
    }

    private void callTestEndpointPublicAndExpectNotFound(AuthorizationData authorizationData) throws Exception {
        mockMvc.perform(get("/test/public")
                .headers(authHeadersFor(authorizationData.withoutAccessToken())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

}
