/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Tahmid Ekram, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionStatusChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionWorkerAddConfirmation;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SuggestionEmailEventProcessorTest extends BaseServiceTest {

    //set to true if you want to inspect the actual email html. Then watch test case log for the output file name
    private static final boolean DUMP_MAILS_TO_TMP = false;

    @Autowired
    private SuggestionEmailEventProcessor grapevineEmailEventProcessor;

    @Autowired
    private TestEmailSenderService emailSenderService;

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private TestTimeService timeService;

    @Autowired
    private GrapevineConfig grapevineConfig;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IPushCategoryService pushCategoryService;

    //Delete all temp email files to have a clear start with every test run
    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_MAILS_TO_TMP) {
            deleteTempFilesStartingWithClassName(SuggestionEmailEventProcessorTest.class);
        }
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();
        th.createSuggestionCategories();
        th.createSuggestions();

        emailSenderService.clearMailList();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    // create email templates in temporary storage, check log for file name
    @AfterEach
    public void dumpEmailsToTmp(TestInfo testInfo) throws Exception {
        if (DUMP_MAILS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-" + testInfo.getDisplayName() + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile);
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Test
    public void newSuggestionMail() {
        Suggestion suggestion = th.suggestionIdea2;
        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));
        checkNumberOfSentEmailsToFirstRespondersAndWorkers(suggestion);

        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        checkBasicEmailBody(getMailTextForRecipient(suggestionFirstResponder),
                suggestion,
                suggestionFirstResponder.getFullName());

        Person suggestionWorker = th.personSuggestionWorkerTenant1Dieter;
        checkBasicEmailBody(getMailTextForRecipient(suggestionWorker),
                suggestion,
                suggestionWorker.getFullName());

        assertThatMailListContainsRecipient(suggestionFirstResponder.getEmail());
        assertThatMailListContainsRecipient(suggestionWorker.getEmail());
    }

    @Test
    public void newSuggestionMail_disabledEmailNotification() {
        Suggestion suggestion = th.suggestionIdea2;
        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_CREATED);
        AppVariant appVariant = th.appVariant1Tenant1;
        pushCategoryService.changePushCategoryUserSetting(pushCategory, suggestionFirstResponder, appVariant, false);

        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));

        assertThatMailListNotContainsRecipient(suggestionFirstResponder.getEmail());

        assertEquals(3, emailSenderService.getEmails().size());
    }

    @Test
    public void newSuggestionMail_ParallelWorldInhabitant() {
        Suggestion suggestion = th.suggestionIdea2;
        suggestion.getCreator().getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(suggestion.getCreator());
        suggestion.setParallelWorld(true);
        th.postRepository.save(suggestion);

        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));

        assertEquals(0, emailSenderService.getEmails().size());
    }

    @Test
    public void newSuggestionMail_WithOneImage() {
        Suggestion suggestion = th.suggestionIdea1;
        suggestion.getImages().remove(1);
        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));
        checkNumberOfSentEmailsToFirstRespondersAndWorkers(suggestion);

        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        checkBasicEmailBody(getMailTextForRecipient(suggestionFirstResponder),
                suggestion,
                suggestionFirstResponder.getFullName());

        Person suggestionWorker = th.personSuggestionWorkerTenant1Dieter;
        checkBasicEmailBody(getMailTextForRecipient(suggestionWorker),
                suggestion,
                suggestionWorker.getFullName());

        assertThatMailListContainsRecipient(suggestionFirstResponder.getEmail());
        assertThatMailListContainsRecipient(suggestionWorker.getEmail());
        assertThat(emailSenderService.getEmails().get(0).getText()).contains("ein Bild");
    }

    @Test
    public void newSuggestionMail_WithTwoImages() {
        Suggestion suggestion = th.suggestionIdea1;
        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));
        checkNumberOfSentEmailsToFirstRespondersAndWorkers(suggestion);

        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        checkBasicEmailBody(getMailTextForRecipient(suggestionFirstResponder),
                suggestion,
                suggestionFirstResponder.getFullName());

        Person suggestionWorker = th.personSuggestionWorkerTenant1Dieter;
        checkBasicEmailBody(getMailTextForRecipient(suggestionWorker),
                suggestion,
                suggestionWorker.getFullName());

        assertThatMailListContainsRecipient(suggestionFirstResponder.getEmail());
        assertThatMailListContainsRecipient(suggestionWorker.getEmail());
        assertThat(emailSenderService.getEmails().get(0).getText()).contains("2 Bilder");
    }

    @Test
    public void newSuggestionMail_WithCustomLocation() {
        Suggestion suggestion = th.suggestionIdea1;
        suggestion.setCustomAddress(Address.builder()
                .name("Musikwerkstatt Nachbarort")
                .city("Nachbardorf")
                .zip("12345")
                .street("Nachbarortstraße 5")
                .gpsLocation(new GPSLocation(49.43281378143463, 7.753112912177832))
                .build());
        grapevineEmailEventProcessor.handlePostCreateConfirmation(
                new PostCreateConfirmation<>(suggestion, th.app1Variant1, true));
        checkNumberOfSentEmailsToFirstRespondersAndWorkers(suggestion);

        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        checkBasicEmailBody(getMailTextForRecipient(suggestionFirstResponder),
                suggestion,
                suggestionFirstResponder.getFullName());

        Person suggestionWorker = th.personSuggestionWorkerTenant1Dieter;
        checkBasicEmailBody(getMailTextForRecipient(suggestionWorker),
                suggestion,
                suggestionWorker.getFullName());

        assertThatMailListContainsRecipient(suggestionFirstResponder.getEmail());
        assertThatMailListContainsRecipient(suggestionWorker.getEmail());
    }

    @Test
    public void newSuggestionCommentMail_BySuggestionCreator() {
        Suggestion suggestion = th.suggestionIdea1;
        Person commentCreator = suggestion.getCreator();
        Person involvedSuggestionWorker1 = th.personSuggestionFirstResponderTenant1Hilde;
        Person involvedSuggestionWorker2 = th.personSuggestionWorkerTenant1Horst;

        String text = "Kann sich mal jemand darum kümmern??";

        Comment comment = Comment.builder()
                .post(suggestion)
                .text(text)
                .creator(commentCreator)
                .build();

        CommentCreateConfirmation commentCreateConfirmation = new CommentCreateConfirmation(comment, th.app1Variant1);

        grapevineEmailEventProcessor.handleCommentCreateConfirmation(commentCreateConfirmation);

        checkCommentEmailBody(getMailTextForRecipient(involvedSuggestionWorker1),
                suggestion,
                comment,
                commentCreator.getFullName());

        checkCommentEmailBody(getMailTextForRecipient(involvedSuggestionWorker2),
                suggestion,
                comment,
                commentCreator.getFullName());

        assertThatMailListContainsRecipient(involvedSuggestionWorker1.getEmail());
        assertThatMailListContainsRecipient(involvedSuggestionWorker2.getEmail());
        // No mail to comment creator
        assertThatMailListNotContainsRecipient(commentCreator.getEmail());

        assertEquals(2, emailSenderService.getEmails().size());
    }

    @Test
    public void newSuggestionCommentMail_disabledEmailNotification() {
        Suggestion suggestion = th.suggestionIdea1;
        Person commentCreator = suggestion.getCreator();
        Person involvedSuggestionWorker1 = th.personSuggestionFirstResponderTenant1Hilde;
        Person involvedSuggestionWorker2 = th.personSuggestionWorkerTenant1Horst;

        PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(
                        LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_COMMENT_CREATED);
        AppVariant appVariant = th.appVariant1Tenant1;
        pushCategoryService.changePushCategoryUserSetting(pushCategory, involvedSuggestionWorker2, appVariant, false);

        String text = "Kann sich mal jemand darum kümmern??";

        Comment comment = Comment.builder()
                .post(suggestion)
                .text(text)
                .creator(commentCreator)
                .build();

        CommentCreateConfirmation commentCreateConfirmation = new CommentCreateConfirmation(comment, th.app1Variant1);

        grapevineEmailEventProcessor.handleCommentCreateConfirmation(commentCreateConfirmation);

        checkCommentEmailBody(getMailTextForRecipient(involvedSuggestionWorker1),
                suggestion,
                comment,
                commentCreator.getFullName());

        assertThatMailListContainsRecipient(involvedSuggestionWorker1.getEmail());
        assertThatMailListNotContainsRecipient(involvedSuggestionWorker2.getEmail());

        assertEquals(1, emailSenderService.getEmails().size());
    }

    @Test
    public void newSuggestionCommentMail_ParallelWorldInhabitant() {
        Suggestion suggestion = th.suggestionIdea2;
        Person parallelWorldInhabitant = th.personNinaBauer;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        assertNotEquals(suggestion.getCreator(), parallelWorldInhabitant);

        Comment comment = Comment.builder()
                .post(suggestion)
                .text("Du Koffer!")
                .creator(parallelWorldInhabitant)
                .build();

        CommentCreateConfirmation commentCreateConfirmation = new CommentCreateConfirmation(comment, th.app1Variant1);

        grapevineEmailEventProcessor.handleCommentCreateConfirmation(commentCreateConfirmation);

        assertEquals(0, emailSenderService.getEmails().size());
    }

    @Test
    public void newSuggestionCommentMail_BySuggestionWorker() {
        Suggestion suggestion = th.suggestionIdea1;
        Person commentCreator = th.personSuggestionFirstResponderTenant1Hilde;
        Person involvedSuggestionWorker = th.personSuggestionWorkerTenant1Horst;

        String text = "Ich habe die Anfrage soeben weitergeleitet.";

        Comment comment = Comment.builder()
                .post(suggestion)
                .text(text)
                .creator(commentCreator)
                .build();

        CommentCreateConfirmation commentCreateConfirmation = new CommentCreateConfirmation(comment, th.app1Variant1);

        grapevineEmailEventProcessor.handleCommentCreateConfirmation(commentCreateConfirmation);

        checkCommentEmailBody(getMailTextForRecipient(involvedSuggestionWorker),
                suggestion,
                comment,
                commentCreator.getFullName());

        assertThatMailListContainsRecipient(involvedSuggestionWorker.getEmail());
        // No mail to comment creator
        assertThatMailListNotContainsRecipient(commentCreator.getEmail());
        assertThatMailListNotContainsRecipient(suggestion.getCreator().getEmail());

        assertEquals(1, emailSenderService.getEmails().size());
    }

    @Test
    public void changeStatusMail() {
        Suggestion suggestion = th.suggestionIdea1;
        grapevineEmailEventProcessor.handleSuggestionStatusChangeConfirmation(
                new SuggestionStatusChangeConfirmation(suggestion));
        checkNumberOfSentEmailsToFirstResponders(suggestion);

        String emailBody = emailSenderService.getEmails().get(0).getText();

        checkBasicEmailBody(emailBody,
                suggestion,
                th.personSuggestionFirstResponderTenant1Hilde.getFullName());

        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(1)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getInitiator().getFullName()));

        emailBody = emailSenderService.getEmails().get(1).getText();

        checkBasicEmailBody(emailSenderService.getEmails().get(1).getText(),
                suggestion,
                th.personSuggestionWorkerTenant1Horst.getFullName());

        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(1)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getInitiator().getFullName()));

        assertThatMailListContainsRecipient(th.personSuggestionFirstResponderTenant1Hilde.getEmail());
        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Horst.getEmail());
        //Note that no mail is sent to th.personSuggestionWorkerTenant1Dieter since he is currently not working
        //on th.suggestionIdea1
    }

    @Test
    public void changeStatusMail_disabledEmailNotification() {
        Suggestion suggestion = th.suggestionIdea1;
        Person suggestionFirstResponder = th.personSuggestionFirstResponderTenant1Hilde;
        PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(
                        LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_STATUS_CHANGED);
        AppVariant appVariant = th.appVariant1Tenant1;
        pushCategoryService.changePushCategoryUserSetting(pushCategory, suggestionFirstResponder, appVariant, false);

        grapevineEmailEventProcessor.handleSuggestionStatusChangeConfirmation(
                new SuggestionStatusChangeConfirmation(suggestion));

        assertThatMailListNotContainsRecipient(suggestionFirstResponder.getEmail());
        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Horst.getEmail());

        assertEquals(1, emailSenderService.getEmails().size());
    }

    @Test
    public void changeStatusMail_NoInitiator() {
        Suggestion suggestion = th.suggestionIdea1;
        suggestion.getSuggestionStatusRecords().get(0).setInitiator(null);
        grapevineEmailEventProcessor.handleSuggestionStatusChangeConfirmation(
                new SuggestionStatusChangeConfirmation(suggestion));
        checkNumberOfSentEmailsToFirstResponders(suggestion);

        String emailBody = emailSenderService.getEmails().get(0).getText();

        checkBasicEmailBody(emailBody,
                suggestion,
                th.personSuggestionFirstResponderTenant1Hilde.getFullName());

        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(1)
                .getStatus().getDisplayName()));

        emailBody = emailSenderService.getEmails().get(1).getText();

        checkBasicEmailBody(emailBody, suggestion, th.personSuggestionWorkerTenant1Horst.getFullName());

        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(1)
                .getStatus().getDisplayName()));

        assertThatMailListContainsRecipient(th.personSuggestionFirstResponderTenant1Hilde.getEmail());
        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Horst.getEmail());
        //Note that no mail is sent to th.personSuggestionWorkerTenant1Dieter since he is currently not working
        //on th.suggestionIdea1
    }

    @Test
    public void changeStatusMail_NoMailToStatusTriggerer() {
        //prepare suggestion with a new most recent status record
        List<SuggestionStatusRecord> statusRecords = new ArrayList<>();
        Suggestion suggestion = th.suggestionIdea1;
        statusRecords.add(SuggestionStatusRecord.builder()
                .initiator(th.personSuggestionFirstResponderTenant1Hilde)
                .suggestion(suggestion)
                .status(SuggestionStatus.DONE)
                .build());
        statusRecords.addAll(suggestion.getSuggestionStatusRecords());
        suggestion.setSuggestionStatusRecords(statusRecords);

        grapevineEmailEventProcessor.handleSuggestionStatusChangeConfirmation(
                new SuggestionStatusChangeConfirmation(suggestion));
        assertEquals(1, emailSenderService.getEmails().size());

        String emailBody = emailSenderService.getEmails().get(0).getText();

        checkBasicEmailBody(emailBody, suggestion, th.personSuggestionWorkerTenant1Horst.getFullName());

        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(1)
                .getStatus().getDisplayName()));
        assertTrue(emailBody.contains(suggestion.getSuggestionStatusRecords().get(0)
                .getInitiator().getFullName()));

        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Horst.getEmail());
        //No mail to th.personSuggestionFirstResponderTenant1Hilde since she triggered the event
        //No mail to th.personSuggestionWorkerTenant1Dieter since he is currently not working
        //on th.suggestionIdea1
    }

    @Test
    public void suggestionWorkerAddMail() {
        grapevineEmailEventProcessor.handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation
                .builder()
                .worker(th.personSuggestionWorkerTenant1Dieter)
                .initiator(th.personSuggestionWorkerTenant1Dieter)
                .suggestion(th.suggestionIdea1)
                .build());
        //no mail when adding himself
        assertTrue(emailSenderService.getEmails().isEmpty());

        grapevineEmailEventProcessor.handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation
                .builder()
                .worker(th.personSuggestionWorkerTenant1Dieter)
                .initiator(th.personSuggestionFirstResponderTenant1Hilde)
                .suggestion(th.suggestionIdea1)
                .build());

        String emailBody = emailSenderService.getEmails().get(0).getText();

        checkBasicEmailBody(emailBody, th.suggestionIdea1, th.personSuggestionWorkerTenant1Dieter.getFullName());

        assertTrue(emailBody.contains(
                th.suggestionIdea1
                        .getSuggestionWorkerMappings().get(0)
                        .getWorker()
                        .getFullName())
        );

        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Dieter.getEmail());
    }

    @Test
    public void suggestionWorkerAddMail_disabledEmailNotification() {
        Suggestion suggestion = th.suggestionIdea1;
        Person suggestionFirstResponder = th.personSuggestionWorkerTenant1Dieter;
        PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(
                        LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_WORKER_ADDED);
        AppVariant appVariant = th.appVariant1Tenant1;
        pushCategoryService.changePushCategoryUserSetting(pushCategory, suggestionFirstResponder, appVariant, false);

        grapevineEmailEventProcessor.handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation
                .builder()
                .worker(suggestionFirstResponder)
                .initiator(suggestionFirstResponder)
                .suggestion(suggestion)
                .build());
        //no mail when adding himself anyway
        assertTrue(emailSenderService.getEmails().isEmpty());

        grapevineEmailEventProcessor.handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation
                .builder()
                .worker(suggestionFirstResponder)
                .initiator(th.personSuggestionFirstResponderTenant1Hilde)
                .suggestion(suggestion)
                .build());
        //no mail because of pushCategorySetting
        assertTrue(emailSenderService.getEmails().isEmpty());
    }

    @Test
    public void suggestionWorkerAddMail_InitiatorNull() {
        grapevineEmailEventProcessor.handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation
                .builder()
                .worker(th.personSuggestionWorkerTenant1Dieter)
                .initiator(null)
                .suggestion(th.suggestionIdea1)
                .build());

        checkBasicEmailBody(emailSenderService.getEmails().get(0).getText(), th.suggestionIdea1,
                th.personSuggestionWorkerTenant1Dieter.getFullName());

        assertThatMailListContainsRecipient(th.personSuggestionWorkerTenant1Dieter.getEmail());
    }

    private String getMailTextForRecipient(Person suggestionFirstResponder) {
        return emailSenderService.getEmails().stream().filter(
                testEmail -> suggestionFirstResponder.getEmail().equals(testEmail.getRecipientEmailAddress()))
                .findAny().get().getText();
    }

    private void assertThatMailListContainsRecipient(String recipient) {
        assertTrue(emailSenderService
                .getEmails()
                .stream()
                .anyMatch(email -> email.getRecipientEmailAddress().equals(recipient)));
    }

    private void assertThatMailListNotContainsRecipient(String recipient) {
        assertFalse(emailSenderService
                .getEmails()
                .stream()
                .anyMatch(email -> email.getRecipientEmailAddress().equals(recipient)));
    }

    private void checkNumberOfSentEmailsToFirstResponders(Suggestion suggestion) {
        // there are two suggestion first responders
        assertEquals(2, roleService.getAllPersonsWithRoleForEntity(SuggestionFirstResponder.class,
                suggestion.getTenant()).size());

        assertEquals(2, emailSenderService.getEmails().size());
    }

    private void checkNumberOfSentEmailsToFirstRespondersAndWorkers(Suggestion suggestion) {
        // there are two suggestion first responders
        assertEquals(2, roleService.getAllPersonsWithRoleForEntity(SuggestionFirstResponder.class,
                suggestion.getTenant()).size());
        // there are three suggestion workers
        assertEquals(3, roleService.getAllPersonsWithRoleForEntity(SuggestionWorker.class,
                suggestion.getTenant()).size());
        // we expect size of 4 for new suggestions and size of 2 for suggestion changes
        // one person has both roles, that's why 2 + 3 != 5 :-)
        assertEquals(4, emailSenderService.getEmails().size());
    }

    private void checkBasicEmailBody(String emailBody, Suggestion usedSuggestion, String recipientName) {

        assertTrue(emailBody.contains(usedSuggestion.getSuggestionCategory().getDisplayName()));
        assertTrue(emailBody.contains(recipientName));
        assertTrue(emailBody.contains(usedSuggestion.getText()));
        assertTrue(emailBody.contains(usedSuggestion.getCreator().getFullName()));

        assertTrue(emailBody.contains(timeService.toLocalTime(usedSuggestion.getCreated()).format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy 'um' HH:mm 'Uhr'"))));

        assertTrue(emailBody.contains(UriComponentsBuilder
                .fromHttpUrl(grapevineConfig.getLoesbarBaseUrl())
                .pathSegment("vorschlag", usedSuggestion.getId())
                .build().toUriString()));

        /*
        * Location checking in email body disabled for now.
        *
        assertTrue(emailBody.contains(String.format(Locale.US, "%.4f, %.4f",
                usedSuggestion.getCustomLocation().getLatitude(),
                th.suggestionIdea2.getCustomLocation().getLongitude())));

        assertTrue(emailBody.contains(String.format(Locale.US, "%f,%f",
                usedSuggestion.getCustomLocation().getLatitude(),
                usedSuggestion.getCustomLocation().getLongitude())));
        */
    }

    private void checkCommentEmailBody(String emailBody, Suggestion suggestion, Comment comment, String recipientName) {

        assertTrue(emailBody.contains(comment.getCreator().getFullName()));
        assertTrue(emailBody.contains(comment.getText()));
        assertTrue(emailBody.contains(recipientName));

        assertTrue(emailBody.contains(timeService.toLocalTime(comment.getCreated()).format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy 'um' HH:mm 'Uhr'"))));

        assertTrue(emailBody.contains(UriComponentsBuilder
            .fromHttpUrl(grapevineConfig.getLoesbarBaseUrl())
            .pathSegment("vorschlag", suggestion.getId())
            .build().toUriString()));
    }

}
