/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.geo.services;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.init.services.IDataInitializerService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class GeoAreaServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private IDataInitializerService dataInitializerService;

    @Autowired
    private ISpatialService spatialService;

    @Autowired
    private IGeoAreaService geoAreaService;

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    @ExtendWith(AssumeIntegrationTestExtension.class)
    public void loadGeoAreasFromDataInitAndTestIntegrity() {

        dataInitializerService.createInitialData("test", new HashSet<>(Arrays.asList("tenant", "geoarea")), null);

        List<GeoArea> allGeoAreas = geoAreaService.findAll();
        for (GeoArea geoArea : allGeoAreas) {
            Geometry geometry = spatialService.createGeometryFromGeoArea(geoArea);
            assertThat(spatialService.isWithinBoundary(geometry, geoArea.getCenter()))
                    .as("In " + geoArea +
                            ", the center point " + geoArea.getCenter()
                            + " should lie inside one of the boundary polygons").isTrue();
            assertThat(spatialService.isWithinBoundary(geometry, new GPSLocation(0, 0)))
                    .as("In " +
                                geoArea + ", the point 0,0"
                            + " should lie outside the boundary polygon")
                    .isFalse();
        }
    }

    @Test
    public void findIntersectedGeoAreas() {

        th.createTenantsAndGeoAreas();
        th.logGeoJsonViewerURL(th.geoAreaList);
        List<GeoArea> allGeoAreas = th.geoAreaList;

        Geometry nonIntersecting = spatialService.createGeometryFromGeoJson(
                "{\"coordinates\":[[[49.46,8.36],[49.53,8.33],[49.52,8.41],[49.46,8.36]]],\"type\":\"Polygon\"}");
        assertThat(geoAreaService.findIntersectedLeafGeoAreas(nonIntersecting, allGeoAreas)).isEmpty();

        Geometry twoIntersecting = spatialService.createGeometryFromGeoJson(
                "{\"coordinates\":[[[49.17,8.14],[49.23,8.11],[49.21,8.26],[49.17,8.14]]],\"type\":\"Polygon\"}");
        assertThat(geoAreaService.findIntersectedLeafGeoAreas(twoIntersecting, allGeoAreas))
                .containsExactly(th.geoAreaKaiserslautern, th.geoAreaMainz);

        Geometry multipleIntersecting = spatialService.createGeometryFromGeoJson(
                "{\"coordinates\":[[[49.07,8.41],[49.07,8.06],[49.32,8.06],[49.32,8.41],[49.07,8.41]]],\"type\":\"Polygon\"}");
        assertThat(geoAreaService.findIntersectedLeafGeoAreas(multipleIntersecting, allGeoAreas))
                .containsExactlyInAnyOrder(th.geoAreaDorf1InEisenberg, th.geoAreaDorf2InEisenberg,
                        th.geoAreaKaiserslautern, th.geoAreaMainz);

        Geometry multipleCrossing = spatialService.createGeometryFromGeoJson(
                "{\"coordinates\":[[[49.14,8.43],[49.14,7.99],[49.19,7.99],[49.19,8.43],[49.14,8.43]]],\"type\":\"Polygon\"}");
        assertThat(geoAreaService.findIntersectedLeafGeoAreas(multipleCrossing, allGeoAreas))
                .containsExactlyInAnyOrder(th.geoAreaDorf1InEisenberg,
                        th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz);
        assertThat(geoAreaService.findIntersectedLeafGeoAreas(multipleCrossing,
                Set.of(th.geoAreaDorf1InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz)))
                .containsExactlyInAnyOrder(th.geoAreaDorf1InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz);
    }

    @Test
    public void minimizeGeoAreas() {

        th.createTenantsAndGeoAreas();

        assertThat(geoAreaService.minimizeGeoAreas(
                Set.of(th.geoAreaDorf1InEisenberg, th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern,
                        th.geoAreaMainz),
                Set.of(th.geoAreaDeutschland, th.geoAreaRheinlandPfalz, th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg,
                        th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz)))
                .containsExactly(th.geoAreaDeutschland);

        assertThat(geoAreaService.minimizeGeoAreas(
                Set.of(th.geoAreaDorf1InEisenberg, th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern,
                        th.geoAreaMainz),
                Set.of(th.geoAreaRheinlandPfalz, th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg,
                        th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz)))
                .containsExactly(th.geoAreaRheinlandPfalz);

        assertThat(geoAreaService.minimizeGeoAreas(
                Set.of(th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz),
                Set.of(th.geoAreaDeutschland, th.geoAreaRheinlandPfalz, th.geoAreaEisenberg, th.geoAreaDorf1InEisenberg,
                        th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz)))
                .containsExactlyInAnyOrder(th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz);

        assertThat(geoAreaService.minimizeGeoAreas(
                Set.of(th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz),
                Set.of(th.geoAreaRheinlandPfalz, th.geoAreaEisenberg, th.geoAreaDorf2InEisenberg,
                        th.geoAreaKaiserslautern, th.geoAreaMainz)))
                .containsExactly(th.geoAreaRheinlandPfalz);

        //unrealistic input, with a "hole" within the available geo areas, since Eisenberg is not contained
        assertThat(geoAreaService.minimizeGeoAreas(
                Set.of(th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern, th.geoAreaMainz),
                Set.of(th.geoAreaRheinlandPfalz, th.geoAreaDorf2InEisenberg, th.geoAreaKaiserslautern,
                        th.geoAreaMainz)))
                .containsExactlyInAnyOrder(th.geoAreaDorf2InEisenberg, th.geoAreaRheinlandPfalz);
    }

}
