/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.statistics.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.business.test.mocks.TestTeamFileStorageService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.config.StatisticsConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.StatisticsCreationFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

public class StatisticsAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestTeamFileStorageService teamFileStorageService;
    @Autowired
    private IStatisticsService statisticsService;
    @Autowired
    private StatisticsConfig statisticsConfig;

    private List<StatisticsReportDefinition> createdStatisticsReports;
    private StatisticsReportDefinition createdStatisticsReportCsv;
    private StatisticsReportDefinition createdStatisticsReportText;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        createFeatureConfig();
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2019-07-01T00:01:34+02:00[Europe/Berlin]"));

        createdStatisticsReports = statisticsService.createConfiguredStatisticsReports();
        createdStatisticsReports.sort(Comparator.comparing(StatisticsReportDefinition::getName));
        createdStatisticsReportCsv = createdStatisticsReports.stream()
                .filter(r -> StringUtils.isNotEmpty(r.getFileNameCsv()))
                .findFirst().get();
        createdStatisticsReportText = createdStatisticsReports.stream()
                .filter(r -> StringUtils.isNotEmpty(r.getFileNameText()))
                .findFirst().get();
    }

    public void createFeatureConfig() throws IOException {
        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(StatisticsCreationFeature.class.getName())
                .geoAreasIncluded(Collections.singleton(th.geoAreaDeutschland))
                .enabled(true)
                .configValues(th.loadTextTestResource("StatisticsCreationFeatureConfig.json"))
                .build());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getStatisticsReportReferences() throws Exception {

        final int expectedNumberOfEntries = 2 * createdStatisticsReports.size();

        assertThat(expectedNumberOfEntries).isEqualTo(6);
        //only reports with offsetFromCurrentFullTimeUnit -1 are created, so we check only those in detail
        mockMvc.perform(get("/adminui/statistics/report")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedNumberOfEntries)))

                .andExpect(jsonPath("$[0].timeUnit").value(createdStatisticsReports.get(0).getTimeUnit().name()))
                .andExpect(jsonPath("$[0].offsetFromCurrentFullTimeUnit").value(-2))

                .andExpect(jsonPath("$[1].name").value(createdStatisticsReports.get(0).getName()))
                .andExpect(jsonPath("$[1].fileNameText").value(createdStatisticsReports.get(0).getFileNameText()))
                .andExpect(jsonPath("$[1].fileNameCsv").value(createdStatisticsReports.get(0).getFileNameCsv()))
                .andExpect(jsonPath("$[1].fileNameCsvMetadata").value(
                        createdStatisticsReports.get(0).getFileNameCsvMetadata()))
                .andExpect(jsonPath("$[1].timeUnit").value(createdStatisticsReports.get(0).getTimeUnit().name()))
                .andExpect(jsonPath("$[1].offsetFromCurrentFullTimeUnit").value(-1))

                .andExpect(jsonPath("$[2].timeUnit").value(createdStatisticsReports.get(1).getTimeUnit().name()))
                .andExpect(jsonPath("$[2].offsetFromCurrentFullTimeUnit").value(-2))

                .andExpect(jsonPath("$[3].name").value(createdStatisticsReports.get(1).getName()))
                .andExpect(jsonPath("$[3].fileNameText").value(createdStatisticsReports.get(1).getFileNameText()))
                .andExpect(jsonPath("$[3].fileNameCsv").value(createdStatisticsReports.get(1).getFileNameCsv()))
                .andExpect(jsonPath("$[3].fileNameCsvMetadata").value(
                        createdStatisticsReports.get(1).getFileNameCsvMetadata()))
                .andExpect(jsonPath("$[3].timeUnit").value(createdStatisticsReports.get(1).getTimeUnit().name()))
                .andExpect(jsonPath("$[3].offsetFromCurrentFullTimeUnit").value(-1))

                .andExpect(jsonPath("$[4].timeUnit").value(createdStatisticsReports.get(2).getTimeUnit().name()))
                .andExpect(jsonPath("$[4].offsetFromCurrentFullTimeUnit").value(-2))

                .andExpect(jsonPath("$[5].name").value(createdStatisticsReports.get(2).getName()))
                .andExpect(jsonPath("$[5].fileNameText").value(createdStatisticsReports.get(2).getFileNameText()))
                .andExpect(jsonPath("$[5].fileNameCsv").value(createdStatisticsReports.get(2).getFileNameCsv()))
                .andExpect(jsonPath("$[5].fileNameCsvMetadata").value(
                        createdStatisticsReports.get(2).getFileNameCsvMetadata()))
                .andExpect(jsonPath("$[5].timeUnit").value(createdStatisticsReports.get(2).getTimeUnit().name()))
                .andExpect(jsonPath("$[5].offsetFromCurrentFullTimeUnit").value(-1));
    }

    @Test
    public void getStatisticsReportReferences_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/statistics/report")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/statistics/report"));
    }

    @Test
    public void getStatisticsReport() throws Exception {

        mockMvc.perform(get("/adminui/statistics/report/content")
                        .param("reportFileName", createdStatisticsReportText.getFileNameText())
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType("text", "plain", StandardCharsets.UTF_8)))
                .andExpect(content().bytes(teamFileStorageService.getFile(
                        statisticsConfig.getTeamFileStorageFolder() + "/" +
                                createdStatisticsReportText.getFileNameText())));

        mockMvc.perform(get("/adminui/statistics/report/content")
                        .param("reportFileName", createdStatisticsReportCsv.getFileNameCsv())
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType("text", "csv", StandardCharsets.UTF_8)))
                .andExpect(content().bytes(teamFileStorageService.getFile(
                        statisticsConfig.getTeamFileStorageFolder() + "/" +
                                createdStatisticsReportText.getFileNameCsv())));

        mockMvc.perform(get("/adminui/statistics/report/content")
                        .param("reportFileName", createdStatisticsReportCsv.getFileNameCsvMetadata())
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(new MediaType("text", "csv", StandardCharsets.UTF_8)))
                .andExpect(content().bytes(teamFileStorageService.getFile(
                        statisticsConfig.getTeamFileStorageFolder() + "/" +
                                createdStatisticsReportCsv.getFileNameCsvMetadata())));
    }

    @Test
    public void getStatisticsReport_Unauthorized() throws Exception {

        mockMvc.perform(get("/adminui/statistics/report/content")
                        .param("reportFileName", createdStatisticsReportCsv.getFileNameCsv())
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());

        assertOAuth2(get("/adminui/statistics/report/content")
                .param("reportFileName", createdStatisticsReportCsv.getFileNameCsv()));
    }

    @Test
    public void getStatisticsReport_InvalidParams() throws Exception {

        // not found
        mockMvc.perform(get("/adminui/statistics/report/content")
                        .param("reportFileName", "datei")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin)))
                .andExpect(isException(ClientExceptionType.STATISTICS_REPORT_NOT_FOUND));
    }

}
