/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientBasePostChangeByPersonRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.offer.ClientOfferChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.offer.ClientOfferCreateRequest;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Offer;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PostEventControllerOfferTest extends BasePostEventControllerTest<Offer> {

    @Override
    protected ClientBasePostChangeByPersonRequest getPostChangeRequest() {
        return new ClientOfferChangeRequest();
    }

    @Override
    protected Offer getTestPost1() {
        return th.offer1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personLaraSchaefer;
    }

    @Override
    protected Offer getTestPost2() {
        return th.offer3;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.seeking1;
    }

    @Test
    public void offerCreateRequest_VerifyingPushMessage() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich habe ein paar Umzungkartons zuhause. Wer sie haben möchtet, einfach melden";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long date = System.currentTimeMillis();

        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .date(date)
                .createdLocation(createdLocation)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation tradingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the offer
        mockMvc.perform(get("/grapevine/post/" + tradingCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.offer.id").value(tradingCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.offer.id").value(tradingCreateConfirmation.getPost().getOffer().getId()))
                .andExpect(jsonPath("$.offer.commentsAllowed").value(true))
                .andExpect(jsonPath("$.offer.text").value(text))
                .andExpect(jsonPath("$.offer.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.offer.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.offer.date").value(date))
                .andExpect(jsonPath("$.offer.tradingCategoryEntity.id").value(th.tradingCategoryMisc.getId()))
                .andExpect(jsonPath("$.offer.tradingCategoryEntity.displayName").value(
                        th.tradingCategoryMisc.getDisplayName()))
                .andExpect(jsonPath("$.offer.tradingCategoryEntity.shortName").value(
                        th.tradingCategoryMisc.getShortName()))
                .andExpect(jsonPath("$.offer.hiddenForOtherHomeAreas").value(false)); //testing default setting

        // Check whether the offer is also added to the repository
        Offer addedOffer = (Offer) th.postRepository.findById(tradingCreateConfirmation.getPost().getId())
                .get();

        assertEquals(postCreator, addedOffer.getCreator());
        assertEquals(createdLocation.getLatitude(), addedOffer.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedOffer.getCreatedLocation().getLongitude());
        assertEquals(text, addedOffer.getText());
        assertEquals(th.tradingCategoryMisc, addedOffer.getTradingCategory());
        assertEquals(postCreator.getTenant(), addedOffer.getTenant());
        assertTrue(addedOffer.getImages() == null || addedOffer.getImages().isEmpty());
        assertThat(addedOffer.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertFalse(addedOffer.isHiddenForOtherHomeAreas());

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(
                        addedOffer.getCreator(),
                        addedOffer.getGeoAreas(),
                        false,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_OFFER_CREATED);

        assertEquals(tradingCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(tradingCreateConfirmation.getPostId(), pushedEvent.getPost().getOffer().getId());
        //check that the last name is cut
        assertEquals(postCreator.getFirstName(), pushedEvent.getPost().getOffer().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(postCreator.getLastName()),
                pushedEvent.getPost().getOffer().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void offerCreateRequest_ContentCreationRestricted() throws Exception {

        ClientOfferCreateRequest seekingCreateRequest = ClientOfferCreateRequest.builder()
                .text("text")
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .date(System.currentTimeMillis())
                .createdLocation(th.pointClosestToTenant1)
                .build();

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/event/offerCreateRequest")
                        .contentType(contentType)
                        .content(json(seekingCreateRequest)));
    }

    @Test
    public void offerCreateRequest_WithImagesAndDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich brauche eine Babysitterin für Samstag";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        long date = 1521302400000L;
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", postCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images and documents
        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                //.customLocation(customLocation)
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                //different order, sorting according to title
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument2.getId(), tempDocument1.getId(), tempDocument3.getId()))
                .tradingCategoryId(th.tradingCategoryService.getId())
                .date(date)
                .hiddenForOtherHomeAreas(true) //disallow visibility from other home areas
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientPostCreateConfirmation offerCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);
        assertThat(offerCreateConfirmation.getPost().getOffer().getText()).isEqualTo(text);
        assertThat(offerCreateConfirmation.getPost().getOffer().getImages()).hasSize(3);
        assertThat(offerCreateConfirmation.getPost().getOffer().getAttachments()).hasSize(3);

        // Get the offer
        //check new entity uses images and documents
        String postId = offerCreateConfirmation.getPost().getId();
        mockMvc.perform(get("/grapevine/post/" + postId)
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.offer.id").value(postId))
                .andExpect(jsonPath("$.offer.text").value(text))
                .andExpect(jsonPath("$.offer.creator.id").value(postCreator.getId()))
                .andExpect(jsonPath("$.offer.images", hasSize(3)))
                .andExpect(jsonPath("$.offer.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.offer.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.offer.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.offer.attachments[0].id")
                        .value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.offer.attachments[1].id")
                        .value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.offer.attachments[2].id")
                        .value(tempDocument3.getDocumentItem().getId()))
                .andExpect(jsonPath("$.offer.createdLocation").doesNotExist())
                .andExpect(jsonPath("$.offer.tradingCategoryEntity.id").value(th.tradingCategoryService.getId()))
                .andExpect(jsonPath("$.offer.date").value(String.valueOf(date)))
                .andExpect(jsonPath("$.offer.hiddenForOtherHomeAreas").value(true));

        // Check whether the offer is also added to the repository
        Offer addedOffer = (Offer) th.postRepository.findById(postId).get();

        assertEquals(postCreator, addedOffer.getCreator());
        assertEquals(createdLocation.getLatitude(), addedOffer.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), addedOffer.getCreatedLocation().getLongitude());
        assertEquals(text, addedOffer.getText());
        assertEquals(postCreator.getTenant(), addedOffer.getTenant());
        assertEquals(3, addedOffer.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedOffer.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedOffer.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedOffer.getImages().get(2));
        assertEquals(3, addedOffer.getAttachments().size());
        assertThat(addedOffer.getAttachments()).hasSize(3);
        assertThat(addedOffer.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());
        assertThat(addedOffer.getGeoAreas()).containsOnly(postCreator.getHomeArea());
        assertTrue(addedOffer.isHiddenForOtherHomeAreas());

        //check used temp images and documents are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        //check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));
    }

    @Test
    public void offerCreateRequest_InvalidImages() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich brauche eine Babysitterin für Samstag";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id
        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .tradingCategoryId(th.tradingCategoryService.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .build();

        //check temp image not found exception
        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void offerCreateRequest_InvalidDocuments() throws Exception {

        Person postCreator = th.personSusanneChristmann;
        String text = "Ich brauche eine Babysitterin für Sonntag";
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(postCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempDocumentItemId = "invalid id";

        //create request using temp documents and invalid id
        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .tradingCategoryId(th.tradingCategoryService.getId())
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidTempDocumentItemId))
                .build();

        //check temp document not found exception
        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(postCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("[" + invalidTempDocumentItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    @Test
    public void offerCreateRequest_ExceedingMaxNumImages() throws Exception {

        Person person = th.personThomasBecker;
        String text = "offer with too many images";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryMediaItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("temporaryMediaItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void offerCreateRequest_ExceedingMaxNumDocuments() throws Exception {

        Person person = th.personThomasBecker;
        String text = "offer with too many documents";

        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerPost(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(createdLocation)
                .temporaryDocumentItemIds(temporaryIds)
                .build();

        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void offerCreateRequest_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/event/offerCreateRequest")
                        .contentType(contentType)
                        .content(json(ClientOfferCreateRequest.builder()
                                .text("Free hugs!")
                                .tradingCategoryId(th.tradingCategoryMisc.getId())
                                .createdLocation(new ClientGPSLocation(42.0d, 47.11d))
                                .build())),
                th.appVariant1Tenant1,
                th.personAnnikaSchneider);
    }

    @Test
    public void offerCreateRequest_ExceedingMaxNumChars() throws Exception {

        Person person = th.personAnnikaSchneider;
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);
        ClientGPSLocation createdLocation = new ClientGPSLocation(42.0d, 47.11d);

        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryMisc.getId())
                .createdLocation(createdLocation)
                .build();

        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                .headers(authHeadersFor(person, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(offerCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void offerCreateRequest_MissingAttributes() throws Exception {

        Person person = th.personSusanneChristmann;
        String text = "offer with missing attributes";

        // missing text
        ClientOfferCreateRequest offerCreateRequest = ClientOfferCreateRequest.builder()
                .tradingCategoryId(th.tradingCategoryComputer.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // missing category -- the category is set to GENERAL
        offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        MvcResult createRequestResult = mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientPostCreateConfirmation tradingCreateConfirmation = toObject(createRequestResult.getResponse(),
                ClientPostCreateConfirmation.class);

        // Get the offer
        mockMvc.perform(get("/grapevine/post/" + tradingCreateConfirmation.getPost().getId())
                .headers(authHeadersFor(person, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.offer.id").value(tradingCreateConfirmation.getPost().getId()))
                .andExpect(jsonPath("$.offer.text").value(text))
                .andExpect(jsonPath("$.offer.tradingCategoryEntity.id").value(th.tradingCategoryMisc.getId()));

        // missing tenant and homeArea
        person.setHomeArea(null);
        person = th.personRepository.save(person);
        offerCreateRequest = ClientOfferCreateRequest.builder()
                .text(text)
                .tradingCategoryId(th.tradingCategoryComputer.getId())
                .createdLocation(new ClientGPSLocation(42.0, 42.0))
                .build();

        mockMvc.perform(post("/grapevine/event/offerCreateRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerCreateRequest)))
                .andExpect(isException("homeArea", ClientExceptionType.POST_COULD_NOT_BE_CREATED));
    }

    @Test
    public void offerChangeRequest_VerifyingPushMessage() throws Exception {

        Person person = th.personSusanneChristmann;
        Offer offerToBeChanged = th.offer1;

        TradingCategory newOfferCategory = th.tradingCategoryTools;

        ClientOfferChangeRequest offerChangeRequest = ClientOfferChangeRequest.builder()
                .postId(offerToBeChanged.getId())
                .tradingCategoryId(newOfferCategory.getId())
                .build();

        MvcResult tradingChangeResult = mockMvc.perform(post("/grapevine/event/offerChangeRequest")
                        .headers(authHeadersFor(person, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(offerChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(offerChangeRequest.getPostId()))
                .andExpect(jsonPath("$.post.offer.id").value(offerChangeRequest.getPostId()))
                .andReturn();

        ClientPostChangeConfirmation tradingChangeConfirmation = toObject(tradingChangeResult.getResponse(),
                ClientPostChangeConfirmation.class);

        // Check whether the offer is also updated in the repository
        Offer updatedOffer = (Offer) th.postRepository.findById(tradingChangeConfirmation.getPostId())
                .get();

        assertEquals(newOfferCategory, updatedOffer.getTradingCategory());

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(offerToBeChanged.getGeoAreas(),
                        offerToBeChanged.isHiddenForOtherHomeAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(tradingChangeConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(tradingChangeConfirmation.getPostId(), pushedEvent.getPost().getOffer().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void offerChangeRequest_NotOwner() throws Exception {

        Person person = th.personSebastianBauer;
        Offer offerToBeChanged = th.offer1;

        TradingStatus newOfferStatus = TradingStatus.DONE;

        ClientOfferChangeRequest offerChangeRequest = ClientOfferChangeRequest.builder()
                .postId(offerToBeChanged.getId())
                .tradingStatus(newOfferStatus)
                .build();

        mockMvc.perform(post("/grapevine/event/offerChangeRequest")
                .headers(authHeadersFor(person, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(offerChangeRequest)))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));
    }

}
