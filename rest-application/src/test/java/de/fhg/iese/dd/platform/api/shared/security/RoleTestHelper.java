/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.PoolingstationOperator;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.roles.ShopOwner;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressListEntryRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.repos.AddressRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.RoleAssignmentRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalUserAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RoleManager;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.UserAdmin;

@Component
public class RoleTestHelper extends BaseSharedTestHelper {

    public static final String GLOBAL_USER_ADMIN = "GLOBAL_USER_ADMIN";
    public static final String USER_ADMIN = "USER_ADMIN";
    public static final String SUPER_ADMIN = "SUPER_ADMIN";
    public static final String LOGISTICS_ADMIN_UI_ADMIN = "LOGISTICS_ADMIN_UI_ADMIN";
    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String SHOP_OWNER = "SHOP_OWNER";
    public static final String POOLINGSTATION_OPERATOR = "POOLINGSTATION_OPERATOR";
    public static final String LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN = "LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN";

    @Autowired
    public AddressRepository addressRepository;

    @Autowired
    public AddressListEntryRepository addressListEntryRepository;

    @Autowired
    public PersonRepository personRepository;

    @Autowired
    public RoleAssignmentRepository roleAssignmentRepository;

    @Autowired
    public List<BaseRole<?>> allRoles;

    public Person personWithManyRoles;
    public Person personRoleManagerTenant1And3;
    public Person personUserAdminTenant2;
    public Person personGlobalUserAdmin;
    public RoleAssignment roleAssignmentPersonWithManyRolesGlobalUserAdmin;

    /**
     * Must be called after shops are created
     */
    public void createAdditionalPersonsWithSpecialRoles() {
        long timestamp = System.currentTimeMillis();
        personWithManyRoles = createPersonWithManyRoles(timestamp++);
        personRoleManagerTenant1And3 = createRoleManagerTenant1And3(timestamp++);
        personUserAdminTenant2 = createPersonUserAdminTenant2();
        personGlobalUserAdmin = createPersonGlobalUserAdmin();
    }

    private Person createPersonUserAdminTenant2() {
        final Person person =
                createPerson("UserAdminTenant2", tenant2, addressPersonVgAdmin, "718c242d-02ca-420a-a110-5d8c4423f4ba");
        roleAssignmentRepository.save(new RoleAssignment(person, UserAdmin.class, tenant2.getId()));
        return person;
    }

    private Person createPersonGlobalUserAdmin() {
        final Person person =
                createPerson("AnotherGlobalUserAdmin", tenant2, addressPersonVgAdmin,
                        "99b6d190-adb3-4388-afa9-3030c74370c3");
        roleAssignmentRepository.save(new RoleAssignment(person, GlobalUserAdmin.class, null));
        return person;
    }

    private Person createPersonWithManyRoles(long created) {
        AddressListEntry addressListEntry = new AddressListEntry(
                addressRepository.save(
                        Address.builder()
                                .name("Mike Hopper")
                                .street("Am Harzhübel 115")
                                .zip("67358")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.4274535, 7.745186)).build()),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        Person person = Person.builder().firstName("Mike")
                .lastName("Hopper")
                .nickName("Mighty Mike")
                .phoneNumber("015324222")
                .email("digdorfdev+mikehopper@gmail.com")
                .accountType(AccountType.OAUTH)
                .addresses(Collections.singleton((addressListEntryRepository.save(addressListEntry))))
                .community(tenant2)
                .oauthId("mike-hopper")
                .build();
        person.setCreated(created);
        person = personRepository.save(person);
        roleAssignmentRepository.save(new RoleAssignment(person, LogisticsAdminUiAdmin.class, tenant1.getId()));
        roleAssignmentRepository.save(new RoleAssignment(person, LogisticsAdminUiAdmin.class, tenant2.getId()));
        roleAssignmentRepository.save(
                new RoleAssignment(person, ShopOwner.class, shop1.getId())); //belongs to tenant1 and 3
        roleAssignmentRepository.save(
                new RoleAssignment(person, LogisticsAdminUiRestrictedAdmin.class, tenant2.getId()));
        roleAssignmentRepository.save(new RoleAssignment(person, PoolingstationOperator.class,
                poolingStation0.getId())); //belongs to tenant2
        roleAssignmentRepository.save(new RoleAssignment(person, SuperAdmin.class, null));
        roleAssignmentPersonWithManyRolesGlobalUserAdmin =
                roleAssignmentRepository.save(new RoleAssignment(person, GlobalUserAdmin.class, null));
        return person;
    }

    private Person createRoleManagerTenant1And3(long created) {
        AddressListEntry addressListEntry = new AddressListEntry(
                addressRepository.save(
                        Address.builder()
                                .name("Mike Hopper")
                                .street("Am Harzhübel 115")
                                .zip("67358")
                                .city("Kaiserslautern").gpsLocation(new GPSLocation(49.4274535, 7.745186)).build()),
                IPersonService.DEFAULT_ADDRESS_LIST_ENTRY_NAME);
        Person person = Person.builder()
                .firstName("Sandra")
                .lastName("König")
                .nickName("Sandy")
                .phoneNumber("0125324222")
                .email("digdorfdev+sandrakoenig@gmail.com")
                .accountType(AccountType.OAUTH)
                .addresses(Collections.singleton((addressListEntryRepository.save(addressListEntry))))
                .community(tenant3)
                .oauthId("sandy")
                .build();
        person.setCreated(created);
        person = personRepository.save(person);
        roleAssignmentRepository.save(new RoleAssignment(person, RoleManager.class, tenant1.getId()));
        roleAssignmentRepository.save(new RoleAssignment(person, RoleManager.class, tenant3.getId()));
        return person;
    }

}
