/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInactivityDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;

@Tag("data-privacy")
@ActiveProfiles({"test", "data-privacy-service-test"})
public class InactivityDataPrivacyServiceTest extends BaseServiceTest {

    //set to true to inspect the actual email html. Then check test case log for the output file name
    private static final boolean DUMP_MAILS_TO_TMP = false;

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;
    @Autowired
    private IInactivityDataPrivacyService inactivityDataPrivacyService;
    @Autowired
    private TestEmailSenderService emailSenderService;

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_MAILS_TO_TMP) {
            deleteTempFilesStartingWithClassName(InactivityDataPrivacyServiceTest.class);
        }
    }

    @AfterEach
    public void dumpEmailsToTmp(TestInfo testInfo) throws Exception {
        if (DUMP_MAILS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-" + testInfo.getDisplayName() + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile);
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        //ensure that the last logged in time is recent
        List<Person> allPersons = th.personRepository.findAll();
        allPersons.forEach(p -> p.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC()));
        th.personRepository.saveAll(allPersons);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void sendWarningEmailToInactivePersons() {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int warningDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        Person personToWarnM1 = th.personRegularHorstiTenant3;
        Person personToWarn0 = th.personShopOwner;
        Person personToWarn1 = th.personRegularAnna;
        Person personToWarn2 = th.personRegularKarl;

        setVerificationStatus(personToWarnM1, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn0, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn1, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn2, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginDaysBack(personToWarnM1, warningDays - 1);
        setLastLoginDaysBack(personToWarn0, warningDays);
        setLastLoginDaysBack(personToWarn1, warningDays + 1);
        setLastLoginDaysBack(personToWarn2, warningDays + 2);

        List<Person> warnedPersons = inactivityDataPrivacyService.sendWarningEmailToInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn2, personToWarn1);

        assertPersonDoesNotHaveStatus(personToWarnM1, PersonStatus.PENDING_DELETION);
        assertPersonDoesNotHaveStatus(personToWarn0, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn1, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn2, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToWarnM1, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn0, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn1, nowTime);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn2, nowTime);
    }

    @Test
    public void setPendingDeletionOfUnverifiedEmailInactivePersons() {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        Person personToWarnM1 = th.personRegularHorstiTenant3;
        Person personToWarn0 = th.personShopOwner;
        Person personToWarn1 = th.personRegularAnna;
        Person personToWarn2 = th.personRegularKarl;

        removeVerificationStatus(personToWarnM1, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn0, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn1, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn2, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginDaysBack(personToWarnM1, inactiveForDays - 1);
        setLastLoginDaysBack(personToWarn0, inactiveForDays);
        setLastLoginDaysBack(personToWarn1, inactiveForDays + 1);
        setLastLoginDaysBack(personToWarn2, inactiveForDays + 2);

        List<Person> warnedPersons = inactivityDataPrivacyService.setPendingDeletionOfUnverifiedEmailInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn2, personToWarn1);

        assertPersonDoesNotHaveStatus(personToWarnM1, PersonStatus.PENDING_DELETION);
        assertPersonDoesNotHaveStatus(personToWarn0, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn1, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn2, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToWarnM1, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn0, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn1, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn2, null);
    }

    @Test
    public void sendWarningEmailToInactivePersons_WithExcludedPattern() {

        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToWarn = th.personRegularKarl;

        setVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn, PersonVerificationStatus.EMAIL_VERIFIED);

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);
        int warningDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        setLastLoginDaysBack(personToBeExcluded, warningDays + 2);
        setLastLoginDaysBack(personToWarn, warningDays + 2);

        List<Person> warnedPersons = inactivityDataPrivacyService.sendWarningEmailToInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn);

        // Person should not be warned due to email-address
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn, nowTime);
    }

    @Test
    public void setPendingDeletionOfUnverifiedEmailInactivePersons_WithExcludedPattern() {

        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToWarn = th.personRegularKarl;

        removeVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn, PersonVerificationStatus.EMAIL_VERIFIED);

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);
        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        setLastLoginDaysBack(personToBeExcluded, inactiveForDays + 2);
        setLastLoginDaysBack(personToWarn, inactiveForDays + 2);

        List<Person> warnedPersons = inactivityDataPrivacyService.setPendingDeletionOfUnverifiedEmailInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn);

        // Person should not be warned due to email-address
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn, null);
    }

    @Test
    public void sendWarningEmailToInactivePersons_WithUnverifiedEmail() {

        Person personToBeExcluded = th.personRegularAnna;
        Person personToWarn = th.personRegularKarl;

        removeVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn, PersonVerificationStatus.EMAIL_VERIFIED);

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);
        int warningDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        setLastLoginDaysBack(personToBeExcluded, warningDays + 2);
        setLastLoginDaysBack(personToWarn, warningDays + 2);

        List<Person> warnedPersons = inactivityDataPrivacyService.sendWarningEmailToInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn);

        // Person should not be warned due to email-address
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn, nowTime);
    }

    @Test
    public void setPendingDeletionOfUnverifiedEmailInactivePersons_WithVerifiedEmail() {

        Person personToSetPendingDeletion = th.personRegularAnna;
        Person personToBeExcluded = th.personRegularKarl;

        personToSetPendingDeletion.setVerificationStatus(0); //nothing verified, so no email should be send
        th.personRepository.saveAndFlush(personToSetPendingDeletion);
        setVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);
        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();

        setLastLoginDaysBack(personToSetPendingDeletion, inactiveForDays + 2);
        setLastLoginDaysBack(personToBeExcluded, inactiveForDays + 2);

        List<Person> pendingDeletionPersons =
                inactivityDataPrivacyService.setPendingDeletionOfUnverifiedEmailInactivePersons();

        assertThat(pendingDeletionPersons).containsExactly(personToSetPendingDeletion);

        // Person should not be warned due to email-address
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToSetPendingDeletion, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToSetPendingDeletion, null);
    }

    @Test
    public void sendWarningEmailToInactivePersons_MorePersonsExcludedThanBatchSize() {
        //this test ensures that the algorithm at PersonService.findPersonsNotLoggedInForDaysWithoutStatusAndNotDeleted works
        //multiple queries have to be issued, since the batch size is 3

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int warningDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        assertEquals(3, dataPrivacyConfig.getUserInactivityDeletionPreparation().getBatchSize());

        Person personToWarn1 = th.personRegularHorstiTenant3;
        Person personToWarn2 = th.personShopOwner;
        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToWarn4 = th.personRegularAnna;
        Person personToWarn5 = th.personRegularKarl;

        setVerificationStatus(personToWarn1, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn2, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn4, PersonVerificationStatus.EMAIL_VERIFIED);
        setVerificationStatus(personToWarn5, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginDaysBack(personToWarn1, warningDays + 1);
        setLastLoginDaysBack(personToWarn2, warningDays + 2);
        setLastLoginDaysBack(personToBeExcluded, warningDays + 3);
        setLastLoginDaysBack(personToWarn4, warningDays + 4);
        setLastLoginDaysBack(personToWarn5, warningDays + 5);

        List<Person> warnedPersons = inactivityDataPrivacyService.sendWarningEmailToInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn5, personToWarn4, personToWarn2);

        assertPersonHasStatus(personToWarn5, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn4, PersonStatus.PENDING_DELETION);
        //excluded
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn2, PersonStatus.PENDING_DELETION);
        //out of batch size
        assertPersonDoesNotHaveStatus(personToWarn1, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToWarn5, nowTime);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn4, nowTime);
        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn2, nowTime);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn1, null);
    }

    @Test
    public void setPendingDeletionOfUnverifiedEmailInactivePersons_MorePersonsExcludedThanBatchSize() {
        //this test ensures that the algorithm at PersonService.findPersonsNotLoggedInForDaysWithoutStatusAndNotDeleted works
        //multiple queries have to be issued, since the batch size is 3

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        assertEquals(3, dataPrivacyConfig.getUserInactivityDeletionPreparation().getBatchSize());

        Person personToWarn1 = th.personRegularHorstiTenant3;
        Person personToWarn2 = th.personShopOwner;
        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToWarn4 = th.personRegularAnna;
        Person personToWarn5 = th.personRegularKarl;

        removeVerificationStatus(personToWarn1, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn2, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn4, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToWarn5, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginDaysBack(personToWarn1, inactiveForDays + 1);
        setLastLoginDaysBack(personToWarn2, inactiveForDays + 2);
        setLastLoginDaysBack(personToBeExcluded, inactiveForDays + 3);
        setLastLoginDaysBack(personToWarn4, inactiveForDays + 4);
        setLastLoginDaysBack(personToWarn5, inactiveForDays + 5);

        List<Person> warnedPersons = inactivityDataPrivacyService.setPendingDeletionOfUnverifiedEmailInactivePersons();

        assertThat(warnedPersons).containsExactly(personToWarn5, personToWarn4, personToWarn2);

        assertPersonHasStatus(personToWarn5, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn4, PersonStatus.PENDING_DELETION);
        //excluded
        assertPersonDoesNotHaveStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonHasStatus(personToWarn2, PersonStatus.PENDING_DELETION);
        //out of batch size
        assertPersonDoesNotHaveStatus(personToWarn1, PersonStatus.PENDING_DELETION);

        assertPersonLastSentTimePendingDeletionWarning(personToWarn5, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn4, null);
        assertPersonLastSentTimePendingDeletionWarning(personToBeExcluded, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn2, null);
        assertPersonLastSentTimePendingDeletionWarning(personToWarn1, null);
    }

    @Test
    public void deletePendingDeletionWarnedPersons() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int warnedForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionAndWarnedForDays();

        //this person should receive a warning, but did not yet, so it can not be deleted
        Person personToWarnButNotWarned = th.personRegularAnna;
        Person personToDelete = th.personRegularHorstiTenant3;
        //this person received a warning, but the log in is more recent
        //actually this would be a bug in the update of the last login, but we should deal with it properly
        Person personToDeleteButLoginNotInDeletion = th.personRegularKarl;

        setLastLoginDaysBack(personToWarnButNotWarned, inactiveForDays + 1);
        setLastSentTimePendingDeletionWarningBack(personToDelete, warnedForDays + 1);
        setLastSentTimePendingDeletionWarningBack(personToDeleteButLoginNotInDeletion, 0);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionWarnedPersons();

        assertThat(deletedPersons).containsExactly(personToDelete);

        waitForEventProcessingLong();

        assertPersonIsDeleted(personToDelete);
        assertEquals(nowTime, th.personRepository.findById(personToDelete.getId()).get().getDeletionTime());
    }

    @Test
    public void deletePendingDeletionUnverifiedEmailPersons() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int pendingForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionUnverifiedEmailForDays();

        //this person should be set in pending deletion, but did not yet, so it can not be deleted
        Person personToDeleteButNotPending = th.personRegularAnna;
        Person personToDelete = th.personRegularHorstiTenant3;
        //this person is in pending deletion, but the log in is more recent
        //actually this would be a bug in the update of the last login, but we should deal with it properly
        Person personToDeleteButLoginNotInDeletion = th.personRegularKarl;

        removeVerificationStatus(personToDeleteButNotPending, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToDelete, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToDeleteButLoginNotInDeletion, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginDaysBack(personToDeleteButNotPending, inactiveForDays + pendingForDays + 1);
        setLastLoginBackAndPendingDeletion(personToDelete, inactiveForDays + pendingForDays + 1);
        setLastLoginBackAndPendingDeletion(personToDeleteButLoginNotInDeletion, 0);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionUnverifiedEmailPersons();

        assertThat(deletedPersons).containsExactly(personToDelete);

        waitForEventProcessingLong();

        assertPersonIsDeleted(personToDelete);
        assertPersonIsNotDeleted(personToDeleteButNotPending);
        assertPersonIsNotDeleted(personToDeleteButLoginNotInDeletion);
        assertEquals(nowTime, th.personRepository.findById(personToDelete.getId()).get().getDeletionTime());
    }

    @Test
    public void deletePendingDeletionWarnedPersons_WithExcludedPattern() throws Exception {

        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToDelete = th.personRegularHorstiTenant3;

        int warnedForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionAndWarnedForDays();

        setLastSentTimePendingDeletionWarningBack(personToBeExcluded, warnedForDays + 1);
        setLastSentTimePendingDeletionWarningBack(personToDelete, warnedForDays + 1);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionWarnedPersons();
        waitForEventProcessingLong();

        assertThat(deletedPersons).containsExactly(personToDelete);

        assertPersonHasStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonIsNotDeleted(personToBeExcluded); // Person should not be deleted due to email-address
    }

    @Test
    public void deletePendingDeletionUnverifiedEmailPersons_WithExcludedPattern() throws Exception {

        Person personToBeExcluded = createPersonWithExcludedEmail();
        Person personToDelete = th.personRegularHorstiTenant3;

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int pendingForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionUnverifiedEmailForDays();

        removeVerificationStatus(personToBeExcluded, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToDelete, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginBackAndPendingDeletion(personToBeExcluded, inactiveForDays + pendingForDays + 1);
        setLastLoginBackAndPendingDeletion(personToDelete, inactiveForDays + pendingForDays + 1);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionUnverifiedEmailPersons();
        waitForEventProcessingLong();

        assertThat(deletedPersons).containsExactly(personToDelete);

        assertPersonHasStatus(personToBeExcluded, PersonStatus.PENDING_DELETION);
        assertPersonIsNotDeleted(personToBeExcluded); // Person should not be deleted due to email-address
    }

    @Test
    public void deletePendingDeletionWarnedPersons_AlreadyDeletedPersons() throws Exception {

        Person personAlreadyDeleted = th.personDeleted;
        Person personToDelete = th.personRegularHorstiTenant3;

        int warnedForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionAndWarnedForDays();

        setLastSentTimePendingDeletionWarningBack(personAlreadyDeleted, warnedForDays + 1);
        setLastSentTimePendingDeletionWarningBack(personToDelete, warnedForDays + 1);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionWarnedPersons();
        waitForEventProcessingLong();

        assertThat(deletedPersons).containsExactly(personToDelete); //already deleted person should not be contained

        assertPersonHasStatus(personAlreadyDeleted,
                PersonStatus.PENDING_DELETION); //the already deleted person keeps the status
        assertPersonIsDeleted(personAlreadyDeleted);
    }

    @Test
    public void deletePendingDeletionUnverifiedEmailPersons_AlreadyDeletedPersons() throws Exception {

        Person personAlreadyDeleted = th.personDeleted;
        Person personToDelete = th.personRegularHorstiTenant3;

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int pendingForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionUnverifiedEmailForDays();

        removeVerificationStatus(personAlreadyDeleted, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToDelete, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginBackAndPendingDeletion(personAlreadyDeleted, inactiveForDays + pendingForDays + 1);
        setLastLoginBackAndPendingDeletion(personToDelete, inactiveForDays + pendingForDays + 1);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionUnverifiedEmailPersons();
        waitForEventProcessingLong();

        assertThat(deletedPersons).containsExactly(personToDelete); //already deleted person should not be contained

        assertPersonHasStatus(personAlreadyDeleted,
                PersonStatus.PENDING_DELETION); //the already deleted person keeps the status
        assertPersonIsDeleted(personAlreadyDeleted);
    }

    @Test
    public void deletePendingDeletionUnverifiedEmailPersons_WithVerifiedEmail() throws Exception {

        Person personToExclude = th.personRegularAnna;
        Person personToDelete = th.personRegularHorstiTenant3;

        int inactiveForDays = dataPrivacyConfig.getUserInactivityDeletionPreparation().getInactiveForDays();
        int pendingForDays = dataPrivacyConfig.getUserInactivityDeletion().getPendingDeletionUnverifiedEmailForDays();

        setVerificationStatus(personToExclude, PersonVerificationStatus.EMAIL_VERIFIED);
        removeVerificationStatus(personToDelete, PersonVerificationStatus.EMAIL_VERIFIED);

        setLastLoginBackAndPendingDeletion(personToExclude, inactiveForDays + pendingForDays + 1);
        setLastLoginBackAndPendingDeletion(personToDelete, inactiveForDays + pendingForDays + 1);

        List<Person> deletedPersons = inactivityDataPrivacyService.deletePendingDeletionUnverifiedEmailPersons();
        waitForEventProcessingLong();

        assertThat(deletedPersons).containsExactly(personToDelete);

        assertPersonIsDeleted(personToDelete);
        assertPersonIsNotDeleted(personToExclude);
    }

    private Person createPersonWithExcludedEmail() {
        String name = "WarnOrDeleteMe";
        return th.personRepository.save(Person.builder()
                .id("3e02972c-0d20-4b41-869b-88c0d2a0fcb8")
                .community(th.tenant1)
                .homeArea(th.geoAreaTenant1)
                .firstName("First " + name)
                .lastName("Last " + name)
                .nickName(name)
                .phoneNumber("0631 6800-" + (name.length() * 42))
                .email("testpattern+" + name + "@gmail.com") // matches test-pattern 'testpattern(.+)\@gmail\.com'
                .accountType(AccountType.OAUTH)
                .oauthId("test-oauth-id-" + name)
                .build());
    }

    private void setLastLoginDaysBack(Person person, int lastLoginInDays) {

        person.setLastLoggedIn(timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(lastLoginInDays));

        th.personRepository.saveAndFlush(person);
    }

    private void setVerificationStatus(Person person, PersonVerificationStatus verificationStatus) {
        person.getVerificationStatuses().addValue(verificationStatus);

        th.personRepository.saveAndFlush(person);
    }

    private void removeVerificationStatus(Person person, PersonVerificationStatus verificationStatus) {
        person.getVerificationStatuses().removeValue(verificationStatus);

        th.personRepository.saveAndFlush(person);
    }

    private void setLastSentTimePendingDeletionWarningBack(Person person, int pendingDeletionWarningDays) {

        person.setLastSentTimePendingDeletionWarning(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(pendingDeletionWarningDays));
        person.getStatuses().addValue(PersonStatus.PENDING_DELETION);

        th.personRepository.saveAndFlush(person);
    }

    private void setLastLoginBackAndPendingDeletion(Person person, int lastLoggedInDays) {

        person.setLastLoggedIn(
                timeServiceMock.currentTimeMillisUTC() - TimeUnit.DAYS.toMillis(lastLoggedInDays));
        person.getStatuses().addValue(PersonStatus.PENDING_DELETION);

        th.personRepository.saveAndFlush(person);
    }

    private void assertPersonHasStatus(Person person, PersonStatus personStatus) {
        assertThat(th.personRepository.findById(person.getId()).get().getStatuses().hasValue(personStatus))
                .isTrue();
    }

    private void assertPersonDoesNotHaveStatus(Person person, PersonStatus personStatus) {
        assertThat(th.personRepository.findById(person.getId()).get().getStatuses().hasValue(personStatus))
                .isFalse();
    }

    private void assertPersonIsDeleted(Person person) {
        assertThat(th.personRepository.findById(person.getId()).get().isDeleted())
                .isTrue();
    }

    private void assertPersonIsNotDeleted(Person person) {
        assertThat(th.personRepository.findById(person.getId()).get().isDeleted())
                .isFalse();
    }

    private void assertPersonLastSentTimePendingDeletionWarning(Person person,
            Long lastSentTimePendingDeletionWarning) {
        assertThat(th.personRepository.findById(person.getId()).get().getLastSentTimePendingDeletionWarning())
                .isEqualTo(lastSentTimePendingDeletionWarning);
    }

}
