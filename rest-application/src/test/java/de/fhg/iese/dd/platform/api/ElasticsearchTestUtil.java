/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import org.elasticsearch.index.query.QueryBuilders;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.ByQueryResponse;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ElasticsearchTestUtil {

    @SpyBean
    public ElasticsearchOperations elasticsearchOperations;

    public void reset() {
        Mockito.reset(elasticsearchOperations);
    }

    public void deleteAllEntitiesInIndex(Class<?> entityClass) {
        if (elasticsearchOperations.indexOps(entityClass).exists()) {
            final ByQueryResponse deleteResponse = elasticsearchOperations.delete(new NativeSearchQueryBuilder()
                            .withQuery(QueryBuilders.matchAllQuery())
                            .build(),
                    SearchPost.class,
                    elasticsearchOperations.getIndexCoordinatesFor(SearchPost.class));
            log.debug("Deleted {} entries from elasticsearch", deleteResponse.getDeleted());
        } else {
            log.debug("Deleted 0 entries from elasticsearch, index not existing");
        }
    }

    public void disableElasticsearch() {
        Mockito.doThrow(RuntimeException.class)
                .when(elasticsearchOperations)
                .index(Mockito.any(), Mockito.any());
        Mockito.doThrow(RuntimeException.class)
                .when(elasticsearchOperations)
                .search(Mockito.any(Query.class), Mockito.any(), Mockito.any());
        Mockito.doThrow(RuntimeException.class)
                .when(elasticsearchOperations)
                .delete(Mockito.any(Query.class), Mockito.any(), Mockito.any());
    }

    public void enableElasticsearch() {
        Mockito.reset(elasticsearchOperations);
    }

}
