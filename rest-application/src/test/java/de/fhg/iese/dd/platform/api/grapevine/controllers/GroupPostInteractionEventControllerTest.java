/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientLikePostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.like.ClientUnlikePostRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostInteraction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;

public class GroupPostInteractionEventControllerTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void likePostRequest_groupMember_verifyingPushMessage() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        final Post postToBeLiked = postRepository.findById(post.getId()).get();
        assertEquals(0, postToBeLiked.getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postToBeLiked,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(th.personAnjaTenant1Dorf1,
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPost().getGossip().getId()).isEqualTo(post.getId());
    }

    @Test
    public void likePostRequest_groupMember_withDoubleLike() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        final Post postToBeLiked = postRepository.findById(post.getId()).get();
        assertEquals(0, postToBeLiked.getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postToBeLiked,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        final Post postAfterLike = postRepository.findById(post.getId()).get();
        assertEquals(1, postAfterLike.getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postAfterLike,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(isException(ClientExceptionType.POST_ALREADY_LIKED));

        final Post postAfterSecondLike = postRepository.findById(post.getId()).get();
        assertEquals(1, postAfterSecondLike.getLikeCount());
    }

    @Test
    public void likePostRequest_groupMember_relike() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        final PostInteraction postInteraction = PostInteraction.builder()
                .person(th.personAnjaTenant1Dorf1)
                .post(post)
                .liked(false)
                .build();

        postInteractionRepository.saveAndFlush(postInteraction);

        final Post postToBeLiked = postRepository.findById(post.getId()).get();

        postToBeLiked.setLikeCount(0);
        postRepository.saveAndFlush(postToBeLiked);

        final Post postFromRepoAfterUpdate = postRepository.findById(post.getId()).get();
        assertEquals(0, postFromRepoAfterUpdate.getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postFromRepoAfterUpdate,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        final Post postFromRepoAfterLike = postRepository.findById(post.getId()).get();
        assertEquals(1, postFromRepoAfterLike.getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postFromRepoAfterLike,
                th.personAnjaTenant1Dorf1));
    }

    @Test
    public void likePostRequest_notGroupMember_publicGroup() throws Exception {

        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));
    }

    @Test
    public void likePostRequest_notGroupMember() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void unlikePostRequest_groupMember_verifyingPushMessage() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        // Setup liked state in repo
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(post)
                .person(th.personAnjaTenant1Dorf1)
                .liked(true)
                .build());

        final ClientUnlikePostRequest unlikePostRequest = new ClientUnlikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(0))
                .andExpect(jsonPath("$.post.gossip.liked").value(false));

        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToPersonSilent(th.personAnjaTenant1Dorf1,
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertThat(pushedEvent.getPost().getGossip().getId()).isEqualTo(post.getId());
    }

    @Test
    public void unlikePostRequest_groupMember_withDoubleUnlike() throws Exception {

        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        // Setup liked state in repo
        final ClientLikePostRequest likePostRequest = new ClientLikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/likePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(likePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(1))
                .andExpect(jsonPath("$.post.gossip.liked").value(true));

        final ClientUnlikePostRequest unlikePostRequest = new ClientUnlikePostRequest(post.getId());

        final Post postToBeUnliked = postRepository.findById(post.getId()).get();
        assertEquals(1, postToBeUnliked.getLikeCount());

        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postToBeUnliked,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(0))
                .andExpect(jsonPath("$.post.gossip.liked").value(false));

        final Post postAfterUnlike = postRepository.findById(post.getId()).get();
        assertEquals(0, postAfterUnlike.getLikeCount());

        assertFalse(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(postAfterUnlike,
                th.personAnjaTenant1Dorf1));

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikePostRequest)))
                .andExpect(isException(ClientExceptionType.POST_ALREADY_UNLIKED));

        final Post postAfterSecondUnlike = postRepository.findById(post.getId()).get();
        assertEquals(0, postAfterSecondUnlike.getLikeCount());
    }

    @Test
    public void unlikePostRequest_notGroupMember_publicGroup() throws Exception {

        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        // Setup liked state in repo
        postInteractionRepository.saveAndFlush(PostInteraction.builder()
                .post(post)
                .person(th.personFranziTenant1Dorf2NoMember)
                .liked(true)
                .build());

        final ClientUnlikePostRequest unlikePostRequest = new ClientUnlikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikePostRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.post.gossip.likeCount").value(0))
                .andExpect(jsonPath("$.post.gossip.liked").value(false));
    }

    @Test
    public void unlikePostRequest_notGroupMember() throws Exception {

        final Group group = th.groupMessdienerSMA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();

        final ClientUnlikePostRequest unlikePostRequest = new ClientUnlikePostRequest(post.getId());

        mockMvc.perform(post("/grapevine/event/unlikePostRequest")
                .headers(authHeadersFor(th.personFranziTenant1Dorf2NoMember, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(unlikePostRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

}
