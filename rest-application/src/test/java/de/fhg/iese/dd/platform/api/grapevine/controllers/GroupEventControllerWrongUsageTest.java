/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateInGroupRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupLeaveRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

/**
 * Tests event endpoints without OAuth or wrong OAuth ID, wrong and missing parameters.<br> This test is for
 * syntactically incorrect requests (semantically incorrect requests should be tested within the other tests that test
 * the business logic).<br> Only a minimal set of entities is created to speed up test execution.
 */
public class GroupEventControllerWrongUsageTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    private static final String TEST_INTRO_TEXT = "test";
    private static final String EMPTY_GROUP_ID = "";
    private static final String WRONG_GROUP_ID = "12345";

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void gossipCreateInGroupRequest_OAuth2AppVariantRequired() throws Exception {

        assertOAuth2AppVariantRequired(completeGossipCreateInGroupRequestBuilder(),
                th.appVariantKL_EB,
                th.personAnjaTenant1Dorf1);
    }

    @Test
    public void gossipCreateInGroupRequest_missingAttributes() throws Exception {

        mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(ClientGossipCreateInGroupRequest.builder()
                        //.groupId("someid")
                        .hiddenForOtherHomeAreas(false)
                        .text("This is a group post")
                        .build())))
                .andExpect(isException("groupId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                .headers(authHeadersFor(th.personAnjaTenant1Dorf1, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(ClientGossipCreateInGroupRequest.builder()
                        .groupId("someid")
                        .hiddenForOtherHomeAreas(false)
                        //.text("This is a group post")
                        .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void groupJoinRequestWithWrongOrEmptyAttributes() throws Exception {

        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                EMPTY_GROUP_ID,
                TEST_INTRO_TEXT,
                th.appVariantKL_EB))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                WRONG_GROUP_ID,
                TEST_INTRO_TEXT,
                th.appVariantKL_EB))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));

        String tooLongText = RandomStringUtils.randomPrint(th.grapevineConfig.getMaxNumberCharsPerComment() + 1);
        log.info("Using random text '{}'", tooLongText);

        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupAdfcAAP.getId(),
                tooLongText,
                th.appVariantKL_EB))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void groupJoinRequest_OAuth2AppVariantRequired() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/group/event/groupJoinRequest")
                        .contentType(contentType)
                        .content(json(ClientGroupJoinRequest.builder()
                                .groupId(th.groupAdfcAAP.getId())
                                .memberIntroductionText(TEST_INTRO_TEXT)
                                .build())),
                th.appVariantKL_EB,
                th.personAnjaTenant1Dorf1);
    }

    @Test
    public void groupLeaveRequestWithWrongOrEmptyAttributes() throws Exception {

        mockMvc.perform(buildLeaveRequest(th.personAnjaTenant1Dorf1,
                EMPTY_GROUP_ID,
                th.appVariantKL_EB))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(buildLeaveRequest(th.personAnjaTenant1Dorf1,
                WRONG_GROUP_ID,
                th.appVariantKL_EB))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void groupLeaveRequest_OAuth2AppVariantRequired() throws Exception {

        assertOAuth2AppVariantRequired(post("/grapevine/group/event/groupLeaveRequest")
                        .contentType(contentType)
                        .content(json(ClientGroupLeaveRequest.builder()
                                .groupId(th.groupAdfcAAP.getId())
                                .build())),
                th.appVariantKL_EB,
                th.personAnjaTenant1Dorf1);
    }

    @Test
    public void groupJoinRequest_PersonWithoutHomeAreaAndSelectedAreas() throws Exception {

        Person person = th.personAnjaTenant1Dorf1;
        AppVariant appVariant = th.appVariantKL_EB;
        person.setHomeArea(null);
        person = th.personRepository.saveAndFlush(person);
        th.unmapAllGeoAreasFromAppVariant(appVariant);
        th.appVariantUsageAreaSelectionRepository.deleteAll();

        mockMvc.perform(post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(th.groupSchachvereinAAA.getId())
                        .memberIntroductionText("Ich habe keine Heimat, alle anderen sind mir egal, mag aber Schach!")
                        .build())))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    private ClientGossipCreateInGroupRequest completeGossipCreateInGroupRequest() {
        return ClientGossipCreateInGroupRequest.builder()
                .groupId(th.groupAdfcAAP.getId())
                .hiddenForOtherHomeAreas(false)
                .text("This is a group post")
                .build();
    }

    private MockHttpServletRequestBuilder completeGossipCreateInGroupRequestBuilder() throws IOException {
        return post("/grapevine/event/gossipCreateInGroupRequest")
                .contentType(contentType)
                .content(json(completeGossipCreateInGroupRequest()));
    }

    private MockHttpServletRequestBuilder buildJoinRequest(Person person, String groupId, String introText,
            AppVariant appVariant) throws IOException {

        return post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(groupId)
                        .memberIntroductionText(introText)
                        .build()));
    }

    private MockHttpServletRequestBuilder buildLeaveRequest(Person person, String groupId, AppVariant appVariant)
            throws IOException {

        return post("/grapevine/group/event/groupLeaveRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupLeaveRequest.builder()
                        .groupId(groupId)
                        .build()));
    }

}
