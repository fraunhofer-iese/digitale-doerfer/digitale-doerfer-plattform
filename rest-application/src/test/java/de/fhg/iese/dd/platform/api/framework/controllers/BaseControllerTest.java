/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.EnumSet;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.featureTestValid.TestPersonVerificationStatusRestrictedFeature;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

public class BaseControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private ParticipantsConfig participantsConfig;

    private Person person;
    private Tenant tenantVerificationStatusRestrictedDisabled;
    protected AppVariant appVariantVerificationStatusRestrictedAnyOf;
    protected AppVariant appVariantVerificationStatusRestrictedAnyOfOrAllOf;
    protected EnumSet<PersonVerificationStatus> allowedVerificationStatusesAnyOf =
            EnumSet.of(PersonVerificationStatus.PHONE_NUMBER_VERIFIED, PersonVerificationStatus.EMAIL_VERIFIED);
    protected EnumSet<PersonVerificationStatus> allowedVerificationStatusesAllOf =
            EnumSet.of(PersonVerificationStatus.NAME_VERIFIED, PersonVerificationStatus.HOME_AREA_VERIFIED);

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        person = th.personRegularAnna;
        appVariantVerificationStatusRestrictedAnyOf = th.app2Variant1;
        appVariantVerificationStatusRestrictedAnyOfOrAllOf = th.app2Variant2;
        tenantVerificationStatusRestrictedDisabled = th.tenant3;

        String allowedStatusArrayAnyOf = allowedVerificationStatusesAnyOf.stream()
                .map(PersonVerificationStatus::name)
                .collect(Collectors.joining("\",\"", "[\"", "\"]"));

        String allowedStatusArrayAllOf = allowedVerificationStatusesAllOf.stream()
                .map(PersonVerificationStatus::name)
                .collect(Collectors.joining("\",\"", "[\"", "\"]"));

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestPersonVerificationStatusRestrictedFeature.class.getName())
                .appVariant(appVariantVerificationStatusRestrictedAnyOf)
                .enabled(true)
                .configValues(
                        "{ \"allowedStatusesAnyOf\": " + allowedStatusArrayAnyOf + " }")
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestPersonVerificationStatusRestrictedFeature.class.getName())
                .tenant(tenantVerificationStatusRestrictedDisabled)
                .enabled(false)
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(TestPersonVerificationStatusRestrictedFeature.class.getName())
                .appVariant(appVariantVerificationStatusRestrictedAnyOfOrAllOf)
                .enabled(true)
                .configValues(
                        "{ \"allowedStatusesAnyOf\": " + allowedStatusArrayAnyOf + "," +
                                "\"allowedStatusesAllOf\": " + allowedStatusArrayAllOf + " }")
                .build());
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void updateLastLogin() throws Exception {

        long lastLogin = 0L;
        person.setLastLoggedIn(lastLogin);
        person = th.personRepository.saveAndFlush(person);
        long nowTime = lastLogin + participantsConfig.getLastLoggedInUpdateThreshold().toMillis() + 1;

        assertTrue((nowTime - lastLogin) > participantsConfig.getLastLoggedInUpdateThreshold().toMillis(),
                "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(nowTime);

        person = callTestEndpointAndGetUpdatedPerson(person, null);

        assertEquals(nowTime, person.getLastLoggedIn());
    }

    @Test
    public void createAppVariantUsage() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        assertEquals(0, th.appVariantUsageRepository.count());

        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        assertEquals(1, th.appVariantUsageRepository.count());
        AppVariantUsage createdAppVariantUsage = th.appVariantUsageRepository.findAll().get(0);

        assertEquals(person, createdAppVariantUsage.getPerson());
        assertEquals(appVariant, createdAppVariantUsage.getAppVariant());
        assertEquals(nowTime, createdAppVariantUsage.getLastUsage());
    }

    @Test
    public void createAppVariantUsage_PublicEndpoints() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        assertEquals(0, th.appVariantUsageRepository.count());

        mockMvc.perform(get("/test/public")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        assertEquals(1, th.appVariantUsageRepository.count());
        AppVariantUsage createdAppVariantUsage = th.appVariantUsageRepository.findAll().get(0);

        assertEquals(person, createdAppVariantUsage.getPerson());
        assertEquals(appVariant, createdAppVariantUsage.getAppVariant());
        assertEquals(nowTime, createdAppVariantUsage.getLastUsage());
    }

    @Test
    public void updateAppVariantUsage() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        long lastUsage = 0L;
        long nowTime = lastUsage + IAppService.LAST_APP_VARIANT_USAGE_PERIOD + 1;

        assertTrue((nowTime - lastUsage) > IAppService.LAST_APP_VARIANT_USAGE_PERIOD, "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(nowTime);

        AppVariantUsage initialAppVariantUsage = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(appVariant)
                .person(person)
                .lastUsage(lastUsage)
                .build());

        assertEquals(1, th.appVariantUsageRepository.count());

        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        assertEquals(1, th.appVariantUsageRepository.count());
        AppVariantUsage updatedAppVariantUsage =
                th.appVariantUsageRepository.findById(initialAppVariantUsage.getId()).get();

        assertEquals(nowTime, updatedAppVariantUsage.getLastUsage());
    }

    @Test
    public void updateAppVariantUsage_PublicEndpoint() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        long lastUsage = 0L;
        long nowTime = lastUsage + IAppService.LAST_APP_VARIANT_USAGE_PERIOD + 1;

        assertTrue((nowTime - lastUsage) > IAppService.LAST_APP_VARIANT_USAGE_PERIOD, "Wrong setup of test!");

        timeServiceMock.setConstantDateTime(nowTime);

        AppVariantUsage initialAppVariantUsage = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(appVariant)
                .person(person)
                .lastUsage(lastUsage)
                .build());

        assertEquals(1, th.appVariantUsageRepository.count());

        mockMvc.perform(get("/test/public")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        assertEquals(1, th.appVariantUsageRepository.count());
        AppVariantUsage updatedAppVariantUsage =
                th.appVariantUsageRepository.findById(initialAppVariantUsage.getId()).get();

        assertEquals(nowTime, updatedAppVariantUsage.getLastUsage());
    }

    @Test
    public void deletedPersonCanNotUseEndpoints() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        person.setDeleted(true);
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void blockedPersonCanNotUseEndpoints() throws Exception {

        AppVariant appVariant = th.app1Variant1;
        Person person = th.personRegularAnna;
        person.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void checkVerificationStatusRestriction_AnyOf() throws Exception {

        assertVerificationStatusAnyOf(appVariantVerificationStatusRestrictedAnyOf);
    }

    @Test
    public void checkVerificationStatusRestriction_AnyOfOrAllOf() throws Exception {

        assertVerificationStatusAnyOf(appVariantVerificationStatusRestrictedAnyOfOrAllOf);
        assertVerificationStatusAllOf(appVariantVerificationStatusRestrictedAnyOfOrAllOf);
    }

    private void assertVerificationStatusAnyOf(AppVariant appVariant) throws Exception {
        EnumSet<PersonVerificationStatus> irrelevantStatus = EnumSet.complementOf(allowedVerificationStatusesAnyOf);

        // completely unverified person
        person.getVerificationStatuses().clear();
        person = th.personRepository.saveAndFlush(person);

        //unverified person, restricted app variant
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //unverified person, unrestricted app variant
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, th.app1Variant1)))
                .andExpect(status().isOk());

        //unverified person, restricted app variant, but unrestricted tenant
        person.setTenant(tenantVerificationStatusRestrictedDisabled);
        person = th.personRepository.saveAndFlush(person);
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        //set the verification status to one that is not in the list of allowed any of verifications
        person.getVerificationStatuses().addValue(irrelevantStatus.iterator().next());
        //set the tenant back
        person.setTenant(th.tenant1);
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //one of the allowed any of verifications
        person.getVerificationStatuses().clear();
        person.getVerificationStatuses().addValue(allowedVerificationStatusesAnyOf.iterator().next());
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        //one of the allowed and one of the not allowed any of verifications
        person.getVerificationStatuses().clear();
        person.getVerificationStatuses().addValue(allowedVerificationStatusesAnyOf.iterator().next());
        person.getVerificationStatuses().addValue(irrelevantStatus.iterator().next());
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());
    }

    private void assertVerificationStatusAllOf(AppVariant appVariant) throws Exception {

        EnumSet<PersonVerificationStatus> irrelevantStatus = EnumSet.complementOf(allowedVerificationStatusesAllOf);
        irrelevantStatus.removeAll(allowedVerificationStatusesAnyOf);

        // completely unverified person
        person.getVerificationStatuses().clear();
        person = th.personRepository.saveAndFlush(person);

        //unverified person, restricted app variant
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //unverified person, unrestricted app variant
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, th.app1Variant1)))
                .andExpect(status().isOk());

        //unverified person, restricted app variant, but unrestricted tenant
        person.setTenant(tenantVerificationStatusRestrictedDisabled);
        person = th.personRepository.saveAndFlush(person);
        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        //set the verification status to one that is not in the list of allowed all of verifications and not in the list of any of
        person.getVerificationStatuses().clear();
        person.getVerificationStatuses().addValue(irrelevantStatus.iterator().next());
        //set the tenant back
        person.setTenant(th.tenant1);
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //one of the allowed all of verifications
        person.getVerificationStatuses().clear();
        person.getVerificationStatuses().addValue(allowedVerificationStatusesAllOf.iterator().next());
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));

        //all of the allowed all of verifications
        person.getVerificationStatuses().setValues(allowedVerificationStatusesAllOf);
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());

        //all of the allowed all of and one of the not allowed all of verifications
        person.getVerificationStatuses().setValues(allowedVerificationStatusesAllOf);
        person.getVerificationStatuses().addValue(irrelevantStatus.iterator().next());
        person = th.personRepository.saveAndFlush(person);

        mockMvc.perform(get("/test/testRestricted")
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk());
    }

    private void setEmailAndVerificationStatus(Person person, String email, PersonVerificationStatus status) {
        person.setEmail(email);
        person.setVerificationStatus(0);
        if (status != null) {
            person.getVerificationStatuses().addValue(status);
        }
        th.personRepository.saveAndFlush(person);
    }

    private Person callTestEndpointAndGetUpdatedPerson(Person person, String verifiedEmail) throws Exception {
        mockMvc.perform(get("/test/test")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .appVariant(th.app1Variant2)
                        .verifiedEmail(verifiedEmail)
                        .build())))
                .andExpect(status().isOk());

        return th.personRepository.findById(person.getId()).get();
    }

}
