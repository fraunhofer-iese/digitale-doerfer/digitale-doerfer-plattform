/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.clientmodel.enums.ClientTransportStatus;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundleStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportAlternativeBundleStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;

public class TransportControllerTest extends BaseLogisticsEventTest {

    private Delivery delivery1;
    private String transportAlternativeBundleId1;
    private TransportAlternativeBundle transportAlternativeBundle1;
    private TransportAlternative transportAlternative1;

    private Delivery delivery2;
    private String transportAlternativeBundleId2;
    private TransportAlternativeBundle transportAlternativeBundle2;
    private TransportAlternative transportAlternative2;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createAchievementRules();
        th.createLieferBarAppVariant();
        th.createBestellBarAppAndAppVariants();
        th.createLieferBarGeoAreaSelections();

        init();
        specificInit();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    private void specificInit() throws Exception {

        //delivery1
        String shopOrderId1 = purchaseOrderReceived();

        waitForEventProcessing();

        delivery1 = purchaseOrderReadyForTransport(shopOrderId1);

        waitForEventProcessing();

        List<TransportAlternativeBundle> transportAlternativeBundles = th.transportAlternativeBundleRepository.findAll();

        assertEquals(1, transportAlternativeBundles.size(), "Amount of transportAlternativeBundles");

        transportAlternativeBundle1 = transportAlternativeBundles.get(0);

        transportAlternativeBundleId1 = transportAlternativeBundle1.getId();

        List<TransportAlternative> transportAlternatives = th.transportAlternativeRepository.findAll();

        assertEquals(1, transportAlternatives.size(), "Amount of transportAlternatives");

        transportAlternative1 = transportAlternatives.stream()
                .filter(ta -> ta.getKind() == TransportKind.RECEIVER).findFirst().orElse(null);

        assertNotNull(transportAlternative1);

        //delivery2
        String shopOrderId2 = purchaseOrderReceivedShop2();

        waitForEventProcessing();

        delivery2 = purchaseOrderReadyForTransport(shopOrderId2);

        waitForEventProcessing();

        transportAlternativeBundles = th.transportAlternativeBundleRepository.findAll();

        assertEquals(2, transportAlternativeBundles.size(), "Amount of transportAlternativeBundles");

        transportAlternativeBundle2 = transportAlternativeBundles
                .stream()
                .filter(tab -> Objects.equals(tab.getDelivery().getId(), delivery2.getId()))
                .findFirst()
                .orElse(null);

        assertNotNull(transportAlternativeBundle2);

        transportAlternativeBundleId2 = transportAlternativeBundle2.getId();

        transportAlternatives = th.transportAlternativeRepository.findAll();

        assertEquals(2, transportAlternatives.size(), "Amount of transportAlternatives");

        transportAlternative2 = transportAlternatives.stream()
                .filter(ta -> ta.getKind() == TransportKind.RECEIVER &&
                        Objects.equals(ta.getTransportAlternativeBundle().getId(), transportAlternativeBundleId2))
                .findFirst()
                .orElse(null);

        assertNotNull(transportAlternative2);
    }

    @Test
    public void getAllTransportsInGivenCommunity() throws Exception {

        mockMvc.perform(get("/logistics/transport/").headers(authHeadersFor(receiver))
                .param("communityId", tenant1Id))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId1))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery1.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));

        mockMvc.perform(get("/logistics/transport/").headers(authHeadersFor(receiver))
                .param("communityId", tenant2Id))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId2))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender2.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery2.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getAllTransportsForSelectedGeoAreas() throws Exception {

        mockMvc.perform(get("/logistics/transport/")
                        .headers(authHeadersFor(th.personRegularAnna, th.appVariantLieferBar)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId1))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery1.getTrackingCode()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()))
                .andExpect(jsonPath("$[1].id").value(transportAlternativeBundleId2))
                .andExpect(jsonPath("$[1].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
                .andExpect(jsonPath("$[1].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[1].sender.shop.id").value(sender2.getId()))
                .andExpect(jsonPath("$[1].trackingCode").value(delivery2.getTrackingCode()))
                .andExpect(jsonPath("$[1].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[1].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[1].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[1].deliveryAddress.address.city").value(deliveryAddress.getCity()));

        mockMvc.perform(get("/logistics/transport/")
                        .headers(authHeadersFor(th.personVgAdmin, th.appVariantLieferBar)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId1))
                .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
                .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
                .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
                .andExpect(jsonPath("$[0].trackingCode").value(delivery1.getTrackingCode()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.name").value(deliveryAddress.getName()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
                .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));

        mockMvc.perform(get("/logistics/transport/")
                        .headers(authHeadersFor(th.personRegularHorstiTenant3, th.appVariantLieferBar)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAllTransportsMissingParameters() throws Exception {

        //Missing appVariantIdentifier
        mockMvc.perform(get("/logistics/transport/")
                        .headers(authHeadersFor(th.personRegularAnna)))
                .andExpect(status().isNotFound());

        //Missing geo area selections
        mockMvc.perform(get("/logistics/transport/")
                        .headers(authHeadersFor(th.personRegularKarl, th.appVariantLieferBar)))
                .andExpect(status().isNotFound());

        //Missing communityId unauthorized
        mockMvc.perform(get("/logistics/transport/"))
                .andExpect(status().isNotFound());

    }

    @Test
    public void getAllTransportsInGivenCommunityUnauthorized() throws Exception {

        mockMvc.perform(get("/logistics/transport")
                .param("communityId", tenant1Id))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId1))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery1.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            //The address name must be censored
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()))
            //The person in deliveryAddress must be censored except for the id
            .andExpect(jsonPath("$[0].deliveryAddress.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.profilePicture").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.email").doesNotExist())
            //The person in receiver must be censored except for the id
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].receiver.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.profilePicture").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.email").doesNotExist())
            //The person in deliveryAddress of the transportAlternatives list must be censored except for the id
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.profilePircutre").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.email").doesNotExist())
            //The address name in the deliveryAddress of the transportAlternative list must be censored
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.name").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));

        mockMvc.perform(get("/logistics/transport/")
                .param("communityId", tenant2Id))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(transportAlternativeBundleId2))
            .andExpect(jsonPath("$[0].currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].sender.shop.id").value(sender2.getId()))
            .andExpect(jsonPath("$[0].trackingCode").value(delivery2.getTrackingCode()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            //The address name must be censored
            .andExpect(jsonPath("$[0].deliveryAddress.address.name").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].deliveryAddress.address.city").value(deliveryAddress.getCity()))
            //The person in deliveryAddress must be censored except for the id
            .andExpect(jsonPath("$[0].deliveryAddress.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].deliveryAddress.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.profilePicture").doesNotExist())
            .andExpect(jsonPath("$[0].deliveryAddress.person.email").doesNotExist())
            //The person in receiver must be censored except for the id
            .andExpect(jsonPath("$[0].receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].receiver.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.profilePicture").doesNotExist())
            .andExpect(jsonPath("$[0].receiver.person.email").doesNotExist())
            //The person in deliveryAddress of the transportAlternatives list must be censored except for the id
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.address").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.firstName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.lastName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.nickName").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.phoneNumber").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.profilePircutre").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.person.email").doesNotExist())
            //The address name in the deliveryAddress of the transportAlternative list must be censored
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.name").doesNotExist())
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$[0].transportAlternatives.[0].deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneTransportAlternativeBundle() throws Exception {

        String clientTransportId = transportAlternativeBundleId1;
        mockMvc.perform(get("/logistics/transport/" + clientTransportId)
            .headers(authHeadersFor(receiver)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(transportAlternativeBundleId1))
            .andExpect(jsonPath("$.currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$.receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$.sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$.trackingCode").value(delivery1.getTrackingCode()))
            .andExpect(jsonPath("$.deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$.deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$.deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$.deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneTransportAlternativeBundleUnauthorized() throws Exception {

        String clientTransportId = transportAlternativeBundleId1;
        mockMvc.perform(get("/logistics/transport/" + clientTransportId))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(transportAlternativeBundleId1))
            .andExpect(jsonPath("$.currentStatus.status").value(ClientTransportStatus.AVAILABLE.toString()))
            .andExpect(jsonPath("$.receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$.sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$.trackingCode").value(delivery1.getTrackingCode()))
            .andExpect(jsonPath("$.deliveryAddress.address.name").doesNotExist())
            .andExpect(jsonPath("$.deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$.deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$.deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneTransportAlternativeBundleExpired() throws Exception {

        TransportAlternativeBundleStatusRecord transportAlternativeBundleStatusRecord =
                TransportAlternativeBundleStatusRecord.builder()
                    .transportAlternativeBundle(transportAlternativeBundle1)
                    .incomingEvent("no event")
                    .status(TransportAlternativeBundleStatus.EXPIRED)
                    .parcelAlreadyAvailableForPickup(false)
                    .timestamp(System.currentTimeMillis())
                    .build();
        th.transportAlternativeBundleStatusRecordRepository.saveAndFlush(transportAlternativeBundleStatusRecord);

        //the expired alternative bundle should not be found anymore
        String clientTransportId = transportAlternativeBundleId1;
        mockMvc.perform(get("/logistics/transport/" + clientTransportId)
                .headers(authHeadersFor(receiver)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_OR_ALTERNATIVE_NOT_FOUND));
    }

    @Test
    public void getOneTransportAssignment() throws Exception {

        TransportAssignment transportAssignment = transportAlternativeSelectRequest(delivery1, transportAlternative1,
                carrier);

        waitForEventProcessing();

        mockMvc.perform(get("/logistics/transport/" + transportAssignment.getId())
            .headers(authHeadersFor(carrier)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(transportAssignment.getId()))
            .andExpect(jsonPath("$.currentStatus.status").value(ClientTransportStatus.ACCEPTED.toString()))
            .andExpect(jsonPath("$.receiver.person.id").value(receiver.getId()))
            .andExpect(jsonPath("$.sender.shop.id").value(sender1.getId()))
            .andExpect(jsonPath("$.trackingCode").value(delivery1.getTrackingCode()))
            .andExpect(jsonPath("$.deliveryAddress.address.name").value(deliveryAddress.getName()))
            .andExpect(jsonPath("$.deliveryAddress.address.street").value(deliveryAddress.getStreet()))
            .andExpect(jsonPath("$.deliveryAddress.address.zip").value(deliveryAddress.getZip()))
            .andExpect(jsonPath("$.deliveryAddress.address.city").value(deliveryAddress.getCity()));
    }

    @Test
    public void getOneTransportAssignmentNotCarrier() throws Exception {

        TransportAssignment transportAssignment =
                transportAlternativeSelectRequest(delivery1, transportAlternative1, carrier);
        mockMvc.perform(get("/logistics/transport/" + transportAssignment.getId())
                .headers(authHeadersFor(receiver)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_NOT_ASSIGNED_TO_CURRENT_PERSON));
    }

    @Test
    public void getOneTransportNotFound() throws Exception {

        mockMvc.perform(get("/logistics/transport/" + "unknown id")
                .headers(authHeadersFor(receiver)))
                .andExpect(isException(ClientExceptionType.TRANSPORT_ASSIGNMENT_OR_ALTERNATIVE_NOT_FOUND));
    }

}
