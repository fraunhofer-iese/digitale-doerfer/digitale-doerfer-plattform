/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddress;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.participants.ParticipantsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.enums.GeoAreaType;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ParticipantsTestHelper extends BaseSharedTestHelper {

    public Shop shop3;
    public Shop shop4;
    public Shop shop5;
    public List<Shop> shopList;

    public ClientAddress unnormalizedAddress1;
    public ClientAddressDefinition unnormalizedAddressDefinition1;
    public ClientAddress normalizedAddress1;
    public ClientAddressDefinition normalizedAddressDefinition1;

    public GeoArea geoAreaParent;
    public GeoArea geoAreaChild;

    @Override
    public void createShops() {

        super.createShops();

        shop3 = new Shop();
        shop3.setCreated(nextTimeStamp());
        shop3.setTenant((tenant1));
        shop3 = shopRepository.save(shop3);

        shop4 = new Shop();
        shop4.setCreated(nextTimeStamp());
        shop4.setTenant((tenant2));
        shop4 = shopRepository.save(shop4);

        shop5 = new Shop();
        shop5.setCreated(nextTimeStamp());
        shop5.setTenant((tenant3));
        shop5 = shopRepository.save(shop5);

        shopList = Arrays.asList(shop1, shop2, shop3, shop4, shop5);
    }

    public void createAddresses() {

        unnormalizedAddress1 = ClientAddress.builder()
                .name("   bei mir zu Hause  ")
                .street("    zur betrunkenen Traube 1 Strasse strasse str. Str.      ")
                .zip("  12345   ")
                .city("  kaiserslautern    ")
                .build();
        unnormalizedAddressDefinition1 = ClientAddressDefinition.builder()
                .name(unnormalizedAddress1.getName())
                .street(unnormalizedAddress1.getStreet())
                .zip(unnormalizedAddress1.getZip())
                .city(unnormalizedAddress1.getCity())
                .build();
        normalizedAddress1 = ClientAddress.builder()
                .name("Bei mir zu Hause")
                .street("Zur betrunkenen Traube 1 Straße straße straße Straße")
                .zip("12345")
                .city("Kaiserslautern")
                .build();
        normalizedAddressDefinition1 = ClientAddressDefinition.builder()
                .name(normalizedAddress1.getName())
                .street(normalizedAddress1.getStreet())
                .zip(normalizedAddress1.getZip())
                .city(normalizedAddress1.getCity())
                .build();

    }

    @Override
    public void createGeoAreas() {

        super.createGeoAreas();

        geoAreaParent = geoAreaRepository.save(
                GeoArea.builder()
                        .id("831e6f4f-76ad-4d19-8ff9-b8ec0f750dde")
                        .tenant(tenant3)
                        .name("Parent Geo Area")
                        .type(GeoAreaType.COUNTY)
                        .boundaryJson("{\"type\":\"Polygon\",\"coordinates\":[[[1,1],[2,1],[1,2],[1,1]]]}")
                        .center(DEFAULT_LOCATION)
                        .build());

        geoAreaChild = geoAreaRepository.save(
                GeoArea.builder()
                        .id("72eb1316-e18b-48da-bd35-d09c62b98e92")
                        .tenant(tenant3)
                        .parentArea(geoAreaParent)
                        .name("Child Geo Area")
                        .type(GeoAreaType.CITY)
                        .boundaryJson("{\"type\":\"Polygon\",\"coordinates\":[[[1,1],[2,1],[1,2],[1,1]]]}")
                        .center(DEFAULT_LOCATION)
                        .build());
    }

    public void createPushEntities() {
        String subjectOwnPersonChanges = ParticipantsConstants.PUSH_CATEGORY_SUBJECT_OWN_PERSON_CHANGES;
        String subjectEmailVerified = ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED;

        pushCategoryRepository.save(PushCategory.builder()
                .app(app1)
                .name("name-" + subjectOwnPersonChanges)
                .description("description-" + subjectOwnPersonChanges)
                .defaultLoudPushEnabled(true)
                .subject(subjectOwnPersonChanges) // important for search by subject
                .build());

        pushCategoryRepository.save(PushCategory.builder()
                .app(app1)
                .name("name-" + subjectEmailVerified)
                .description("description-" + subjectEmailVerified)
                .defaultLoudPushEnabled(true)
                .subject(subjectEmailVerified) // important for search by subject
                .build());
    }

    public void createAppVariantUsages(){
        //personRegularAnna subscribed to geoAreaTenant1 (home) and geoAreaTenant2
        selectGeoAreasForAppVariant(app1Variant1, personRegularAnna, geoAreaTenant1, geoAreaTenant2);

        //personRegularHorstiTenant3 subscribed to geoAreaTenant3 (home)
        selectGeoAreasForAppVariant(app1Variant1, personRegularHorstiTenant3, geoAreaTenant3);

        //personVgAdmin subscribed to geoAreaTenant1 (home)
        selectGeoAreasForAppVariant(app1Variant1, personVgAdmin, geoAreaTenant1);
    }

}
