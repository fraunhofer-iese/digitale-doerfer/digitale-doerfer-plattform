/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.business.motivation.framework.IAchievementRule;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;

public class AchievementEventProcessorTest extends BaseServiceTest {

    @Autowired
    private List<IAchievementRule<?>> achievementRules;

    @Autowired
    private MotivationTestHelper th;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPersons();
        th.createScoreEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void checkAchievement() {

        Map<String, String> achievementIds = new HashMap<>();
        Map<String, String> achievementLevelIds = new HashMap<>();
        Map<String, String> achievementNames = new HashMap<>();
        for (IAchievementRule<?> achievementRule : achievementRules) {
            log.info("Creating AchievementRule: {} ({})", achievementRule.getName(),
                    achievementRule.getClass().getName());

            //we add all names of the achievement rules and the classname of the rule, so that we can check if there are duplicates
            assertFalse(achievementNames.containsKey(achievementRule.getName()),
                    "Duplicate achievementRuleName " + achievementRule.getName() + " in AchievementRule \n" +
                            achievementRule.getClass().getName() + "\n and \n" +
                            achievementNames.get(achievementRule.getName()));

            achievementNames.put(achievementRule.getName(), achievementRule.getClass().getName());

            Collection<Achievement> createdAchievements = achievementRule.createOrUpdateRelevantAchievements();

            for (Achievement achievement : createdAchievements) {
                //we add all ids of the achievements and the classname of the rule, so that we can check if there are duplicates
                assertFalse(achievementIds.containsKey(achievement.getId()),
                        "Duplicate achievementId " + achievement.getId() + " in AchievementRule \n" +
                                achievementRule.getClass().getName() + "\n and \n" +
                                achievementIds.get(achievement.getId()));
                achievementIds.put(achievement.getId(), achievementRule.getClass().getName());

                //we add all ids of the achievement levels and the classname of the rule, so that we can check if there are duplicates
                for (AchievementLevel achievementLevel : achievement.getAchievementLevels()) {
                    assertFalse(achievementLevelIds.containsKey(achievementLevel.getId()),
                            "Duplicate achievementLevelId " +
                                    achievementLevel.getId() + " in AchievementRule " +
                                    achievementRule.getClass().getName() + " and " +
                                    achievementLevelIds.get(achievementLevel.getId()));
                    achievementLevelIds.put(achievementLevel.getId(), achievementRule.getClass().getName());
                }
            }
        }

        for (final IAchievementRule<?> achievementRule : achievementRules) {
            log.info("Checking AchievementRule: {}", achievementRule.getName());
            achievementRule.checkAchievement(th.personPoolingStationOp);
            achievementRule.checkAchievement(th.personRegularAnna);
            achievementRule.checkAchievement(th.personVgAdmin);
            achievementRule.checkAchievement(th.personIeseAdmin);
        }
        for (final IAchievementRule<?> achievementRule : achievementRules) {
            log.info("Dropping AchievementRule: {}", achievementRule.getName());
            achievementRule.dropRelevantAchievements();
        }
    }

}
