/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication;

import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientUserGeneratedContentFlagChat;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers.UserGeneratedContentFlagAdminUiControllerTest;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CommunicationUserGeneratedContentFlagAdminUIControllerTest extends UserGeneratedContentFlagAdminUiControllerTest {

    @Autowired
    private CommunicationTestHelper th;

    @Autowired
    private IChatService chatService;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAppEntities();
        th.createPushEntities();
        th.createPersons();
        th.createUserGeneratedContentAdmins();
        th.createChatEntities();
        super.createFlags();
    }

    @Override
    protected BaseSharedTestHelper getTestHelper() {
        return th;
    }

    @Override
    protected Chat getFlagEntityFlag1Tenant1() {
        return th.chat1;
    }

    @Override
    protected Chat getFlagEntityFlag2Tenant1() {
        return th.chat2;
    }

    @Override
    protected Chat getFlagEntityFlag3Tenant1() {
        return th.chat3;
    }

    @Override
    protected Collection<String> getSingleFlagUrlTemplates() {
        return List.of("/communication/adminui/flag/chat/{flagId}");
    }

    @Override
    @Test
    public void getFlagById_GlobalAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag1Tenant1;
        Chat flaggedChat = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedChat.getId(), expectedFlag.getEntityId());
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords().size()).isGreaterThan(0);

        assertFlagEquals(flaggedChat, 2, expectedFlag,
                toObject(mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagChat.class));
    }

    @Override
    @Test
    public void getFlagById_TenantAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag2Tenant1;
        Chat flaggedChat = getFlagEntityFlag2Tenant1();
        assertEquals(flaggedChat.getId(), expectedFlag.getEntityId());
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords().size()).isGreaterThan(0);

        assertFlagEquals(flaggedChat, 1, expectedFlag,
                toObject(mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                                .contentType(contentType)
                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagChat.class));
    }

    @Test
    public void getFlagById_WrongType() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag1Tenant2;
        GeoArea flaggedEntity = (GeoArea) getFlagEntityFlag1Tenant2();
        assertEquals(flaggedEntity.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant2)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagById_InvalidType() throws Exception {

        UserGeneratedContentFlag expectedFlag;
        expectedFlag = flag1Tenant1;
        expectedFlag.setEntityType("de.fhg.iese.dd.🐸");
        flagRepository.save(expectedFlag);
        Chat flaggedChat = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedChat.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));

        expectedFlag.setEntityType("");
        flagRepository.save(expectedFlag);

        mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagById_ChatNotFound() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag2Tenant1;
        Chat flaggedChat = getFlagEntityFlag2Tenant1();
        assertEquals(flaggedChat.getId(), expectedFlag.getEntityId());
        chatService.deleteChat(flaggedChat);

        mockMvc.perform(get("/communication/adminui/flag/chat/{flagId}", expectedFlag.getId())
                .contentType(contentType)
                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.CHAT_NOT_FOUND));
    }

    private void assertFlagEquals(Chat flaggedChat, long numberOfExpectedChatMessages,
            UserGeneratedContentFlag expectedChat, ClientUserGeneratedContentFlagChat actualFlag) {

        assertChatEquals(flaggedChat, numberOfExpectedChatMessages, actualFlag);
        super.assertFlagEquals(expectedChat, actualFlag);
    }

    private void assertChatEquals(Chat expectedChat, long numberOfExpectedChatMessages,
            ClientUserGeneratedContentFlagChat actualChat) {
        assertEquals(expectedChat.getTopic(), actualChat.getTopic());
        assertEquals(expectedChat.getCreated(), actualChat.getCreatedTimestamp());
        assertEquals(expectedChat.getLastMessageNotAutoGenerated().getSentTime(), actualChat.getLastMessageTimestamp());
        assertEquals(numberOfExpectedChatMessages, actualChat.getNumberOfChatMessages());
    }

}
