/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.personblocking.controllers;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.participants.personblocking.clientevent.ClientPersonBlockRequest;
import de.fhg.iese.dd.platform.api.participants.personblocking.clientevent.ClientPersonUnBlockRequest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.repos.PersonBlockingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class PersonBlockingEventControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private PersonBlockingRepository personBlockingRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPersonBlockings();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private AppVariant getAnyAppVariantFor(final App app) {
        if (app.equals(th.app1)) {
            return th.app1Variant1;
        } else if (app.equals(th.app2)) {
            return th.app2Variant1;
        }
        throw new IllegalArgumentException("Invalid app: " + app);
    }

    @Test
    public void personBlockRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personBlockRequest")
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(th.personRegularKarl.getId()))));
    }

    @Test
    public void personBlockRequest_InvalidPerson() throws Exception {

        mockMvc.perform(post("/person/event/personBlockRequest")
                .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(BaseTestHelper.INVALID_UUID))))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void personBlockRequest_NotExistingBlocking() throws Exception {

        final Person blockingPerson = th.personRegularAnna;
        final Person blockedPerson = th.personRegularKarl;
        final App app = th.app2;

        mockMvc.perform(post("/person/event/personBlockRequest")
                .headers(authHeadersFor(blockingPerson, getAnyAppVariantFor(app)))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(blockedPerson.getId()))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, blockedPerson));
    }

    @Test
    public void personBlockRequest_SelfBlocking() throws Exception {

        final Person blockingPerson = th.personRegularAnna;
        final Person blockedPerson = th.personRegularAnna;
        final App app = th.app1;

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                        blockingPerson, blockedPerson), "Wrong setup of test");

        mockMvc.perform(post("/person/event/personBlockRequest")
                .headers(authHeadersFor(blockingPerson, getAnyAppVariantFor(app)))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(blockedPerson.getId()))))
                .andExpect(status().isOk());

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, blockedPerson));
    }

    @Test
    public void personBlockRequest_ExistingBlocking() throws Exception {

        final PersonBlocking blocking = th.annaBlocksKarlInApp1;

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                        blocking.getBlockingPerson(), blocking.getBlockedPerson()), "Wrong setup of test");

        mockMvc.perform(post("/person/event/personBlockRequest")
                .headers(authHeadersFor(blocking.getBlockingPerson(), getAnyAppVariantFor(blocking.getApp())))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(blocking.getBlockedPerson().getId()))))
                .andExpect(status().isOk());

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                blocking.getBlockingPerson(), blocking.getBlockedPerson()));
    }

    @Test
    public void personUnBlockRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/person/event/personUnBlockRequest")
                .contentType(contentType)
                .content(json(new ClientPersonUnBlockRequest(th.personRegularKarl.getId()))));
    }

    @Test
    public void personUnBlockRequest_InvalidPerson() throws Exception {

        mockMvc.perform(post("/person/event/personUnBlockRequest")
                .headers(authHeadersFor(th.personRegularAnna, th.app1Variant1))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(BaseTestHelper.INVALID_UUID))))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));
    }

    @Test
    public void personUnBlockRequest_NotExistingBlocking() throws Exception {

        final Person blockingPerson = th.personRegularAnna;
        final Person notBlockedPerson = th.personRoleManagerTenant1;
        final App app = th.app1;

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                        blockingPerson, notBlockedPerson),
                "Wrong setup of test");

        mockMvc.perform(post("/person/event/personUnBlockRequest")
                .headers(authHeadersFor(blockingPerson, getAnyAppVariantFor(app)))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(notBlockedPerson.getId()))))
                .andExpect(status().isOk());

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(app,
                blockingPerson, notBlockedPerson));
    }

    @Test
    public void personUnBlockRequest_ExistingBlocking() throws Exception {

        final PersonBlocking blocking = th.annaBlocksKarlInApp1;

        assertTrue(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                        blocking.getBlockingPerson(), blocking.getBlockedPerson()), "Wrong setup of test");

        mockMvc.perform(post("/person/event/personUnBlockRequest")
                .headers(authHeadersFor(blocking.getBlockingPerson(), getAnyAppVariantFor(blocking.getApp())))
                .contentType(contentType)
                .content(json(new ClientPersonBlockRequest(blocking.getBlockedPerson().getId()))))
                .andExpect(status().isOk());

        assertFalse(personBlockingRepository.existsByAppAndBlockingPersonAndBlockedPerson(blocking.getApp(),
                blocking.getBlockingPerson(), blocking.getBlockedPerson()));
    }

}
