/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.happening.ClientHappeningCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.newsitem.ClientNewsItemCreateOrUpdateByNewsSourceRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientCroppedImageReference;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientLocationDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientAbsoluteImageCropDefinition;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientRelativeImageCropDefinition;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class HappeningEventByNewsSourceControllerTest extends BasePostEventByNewsSourceControllerTest<Happening> {

    @Autowired
    private HappeningRepository happeningRepository;
    @Autowired
    private HappeningParticipantRepository happeningParticipantRepository;

    private static final String AUTHOR_NAME = "Lara Schaefer";
    private static final String HAPPENING_TEXT = "Interessante Veranstaltung";
    private static final String CATEGORIES = "Veranstaltung";
    private static final String EXTERNAL_ID = "external id for published news";
    private static final String POST_URL = "https://dorfnews.digitale-doerfer.de/event/test_veranstaltung/";
    private static final String ORGANIZER = "FSV Digitalbach";
    private static final String VENUE = "Veranstaltungsort";
    private final long start = System.currentTimeMillis();
    private final long end = start + TimeUnit.DAYS.toMillis(1);
    private final boolean allDayEvent = true;

    @Override
    protected Happening getTestPost1() {
        return th.happening1;
    }

    @Override
    protected Person getTestPost1NotOwner() {
        return th.personThomasBecker;
    }

    @Override
    protected Happening getTestPost2() {
        return th.happening2;
    }

    @Override
    protected Post getTestPostDifferentType() {
        return th.suggestionIdea1;
    }

    @Test
    public void happeningPublishRequest_RequestAsJsonString() throws Exception {

        GeoArea geoArea = th.subGeoArea1Tenant1;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        // field publishImmediately is missing in json string but the default value is set to true -> post should be published
                        .content("{\n" +
                                "\"geoAreaId\": \"" + geoArea.getId() + "\",\n" +
                                "\"authorName\": \"" + AUTHOR_NAME + "\",\n" +
                                "\"text\": \"" + HAPPENING_TEXT + "\",\n" +
                                "\"externalId\": \"" + EXTERNAL_ID + "\",\n" +
                                "\"newsURL\": \"" + POST_URL + "\",\n" +
                                "\"categories\": \"" + CATEGORIES + "\",\n" +
                                "\"organizer\": \"" + ORGANIZER + "\",\n" +
                                "\"venue\": \"" + VENUE + "\",\n" +
                                "\"startTime\": \"" + start + "\",\n" +
                                "\"endTime\": \"" + end + "\",\n" +
                                "\"allDayEvent\": \"" + allDayEvent + "\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningCreateConfirmation.getPostId()).get();

        assertEquals(geoArea.getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsOnly(geoArea);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());
        assertTrue(publishedHappening.isCommentsAllowed());

        mockMvc.perform(get("/grapevine/post/{postId}", publishedHappening.getId())
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(publishedHappening.getId()))
                .andExpect(jsonPath("$.happening.venue").value(publishedHappening.getCustomAddress().getName()));

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedHappening.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);

        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningPublishRequest_CompleteVerifyingPushMessage() throws Exception {

        GeoArea geoArea = th.subGeoArea1Tenant1;
        MediaItem expectedImage1 = th.createMediaItem("blutspende");
        URL imageURL1 = new URL("http://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        Organization organization = th.organizationFeuerwehr;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .organizationId(organization.getId())
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .imageURLs(Collections.singletonList(imageURL1.toString()))
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .commentsDisallowed(true)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningCreateConfirmation.getPostId()).get();

        assertEquals(geoArea.getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsOnly(geoArea);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertThat(publishedHappening.getOrganization()).isEqualTo(organization);
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(Collections.singletonList(expectedImage1), publishedHappening.getImages());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());
        assertTrue(publishedHappening.isPublished());
        assertFalse(publishedHappening.isCommentsAllowed());

        mockMvc.perform(get("/grapevine/post/" + publishedHappening.getId())
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(publishedHappening.getId()))
                .andExpect(jsonPath("$.happening.venue").value(publishedHappening.getCustomAddress().getName()));

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedHappening.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED);

        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningPublishRequest_CompleteVerifyingPushMessage_MultiGeoArea() throws Exception {

        Set<GeoArea> geoAreas = Set.of(th.subGeoArea1Tenant1, th.subGeoArea2Tenant1, th.geoAreaKaiserslautern);
        List<String> geoAreaIds = geoAreas.stream()
                .map(GeoArea::getId)
                .toList();
        MediaItem expectedImage1 = th.createMediaItem("blutspende");
        URL imageURL1 = new URL("http://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        Organization organization = th.organizationFeuerwehr;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaIds(geoAreaIds)
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .organizationId(organization.getId())
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .imageURLs(Collections.singletonList(imageURL1.toString()))
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .commentsDisallowed(true)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningCreateConfirmation.getPostId()).get();

        assertEquals(geoAreas.stream().findFirst().get().getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsExactlyInAnyOrderElementsOf(geoAreas);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertThat(publishedHappening.getOrganization()).isEqualTo(organization);
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(Collections.singletonList(expectedImage1), publishedHappening.getImages());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());
        assertTrue(publishedHappening.isPublished());
        assertFalse(publishedHappening.isCommentsAllowed());

        mockMvc.perform(get("/grapevine/post/" + publishedHappening.getId())
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(publishedHappening.getId()))
                .andExpect(jsonPath("$.happening.venue").value(publishedHappening.getCustomAddress().getName()));

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedHappening.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED);

        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningPublishRequest_ImageOrder_Legacy() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem imageDownload1 = th.createMediaItem("thing1.jpg");
        MediaItem imageDownload2 = th.createMediaItem("thing2.jpg");
        testMediaItemService.addTestMediaItem(imageURL1, imageDownload1);
        testMediaItemService.addTestMediaItem(imageURL2, imageDownload2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .x(0).y(0)
                .height(900).width(1200)
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .x(300).y(400)
                .height(800).width(600)
                .build();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.geoAreaEisenberg.getId())
                                .authorName("Karl Napp")
                                .text("Super Veranstaltung")
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/meine-party")
                                .categories("Party")
                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                                .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening =
                (Happening) th.externalPostRepository.findById(happeningPublishConfirmation.getPostId()).get();

        assertThat(publishedHappening.getImages().size()).isEqualTo(4);

        assertThat(publishedHappening.getImages().get(0)).isEqualTo(imageDownload1);
        assertThat(publishedHappening.getImages().get(1)).isEqualTo(imageDownload2);
        MediaItem mediaItem2 = publishedHappening.getImages().get(2);
        assertThat(mediaItem2.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1.0);
        MediaItem mediaItem3 = publishedHappening.getImages().get(3);
        assertThat(mediaItem3.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.25);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5);
    }

    @Test
    public void happeningPublishRequest_ImageOrder() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        URL imageURL1 = new URL("https://example.org/image1");
        URL imageURL2 = new URL("https://example.org/image2");
        MediaItem imageDownload1 = th.createMediaItem("thing1.jpg");
        MediaItem imageDownload2 = th.createMediaItem("thing2.jpg");
        testMediaItemService.addTestMediaItem(imageURL1, imageDownload1);
        testMediaItemService.addTestMediaItem(imageURL2, imageDownload2);

        URL croppedImageURL1 = new URL("https://example.org/image3");
        URL croppedImageURL2 = new URL("https://example.org/image4");
        MockMultipartFile imageDownload3 = th.createTestImageFile("thing3.jpg");
        MockMultipartFile imageDownload4 = th.createTestImageFile("thing4.jpg");
        testMediaItemService.addTestImageToDownload(croppedImageURL1, imageDownload3.getBytes());
        testMediaItemService.addTestImageToDownload(croppedImageURL2, imageDownload4.getBytes());
        // original image width=1200, height=900
        ClientCroppedImageReference clientCroppedImageReference1 = ClientCroppedImageReference.builder()
                .url(croppedImageURL1.toString())
                .relativeCropDefinition(ClientRelativeImageCropDefinition.builder()
                        .relativeOffsetX(0d).relativeOffsetY(0d)
                        .relativeHeight(1d).relativeWidth(1d)
                        .build())
                .build();
        // original image width=1200, height=1600
        ClientCroppedImageReference clientCroppedImageReference2 = ClientCroppedImageReference.builder()
                .url(croppedImageURL2.toString())
                .absoluteCropDefinition(ClientAbsoluteImageCropDefinition.builder()
                        .absoluteOffsetX(300).absoluteOffsetY(400)
                        .absoluteHeight(800).absoluteWidth(600)
                        .build())
                .build();
        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.geoAreaEisenberg.getId())
                                .authorName("Karl Napp")
                                .text("Super Veranstaltung")
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .externalId("oi-die")
                                .newsURL(newsSource.getSiteUrl() + "/meine-party")
                                .categories("Party")
                                .imageURLs(Arrays.asList(imageURL1.toString(), imageURL2.toString()))
                                .croppedImages(List.of(clientCroppedImageReference1, clientCroppedImageReference2))
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening =
                (Happening) th.externalPostRepository.findById(happeningPublishConfirmation.getPostId()).get();

        assertThat(publishedHappening.getImages().size()).isEqualTo(4);

        assertThat(publishedHappening.getImages().get(0)).isEqualTo(imageDownload1);
        assertThat(publishedHappening.getImages().get(1)).isEqualTo(imageDownload2);
        MediaItem mediaItem2 = publishedHappening.getImages().get(2);
        assertThat(mediaItem2.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(1.0);
        assertThat(mediaItem2.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(1.0);
        MediaItem mediaItem3 = publishedHappening.getImages().get(3);
        assertThat(mediaItem3.getRelativeImageCropDefinition()).isNotNull();
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetX()).isEqualTo(0.25);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeOffsetY()).isEqualTo(0.25);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeWidth()).isEqualTo(0.5);
        assertThat(mediaItem3.getRelativeImageCropDefinition().getRelativeHeight()).isEqualTo(0.5);
    }

    @Test
    public void happeningPublishRequest_VenueButNoVenueAddress() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        GeoArea geoArea = th.subGeoArea1Tenant1;
        MediaItem expectedImage1 = th.createMediaItem("blutspende");
        URL imageURL1 = new URL("http://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .imageURLs(Collections.singletonList(imageURL1.toString()))
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .commentsDisallowed(false)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningPublishConfirmation.getPostId()).get();

        assertEquals(geoArea.getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsOnly(geoArea);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(Collections.singletonList(expectedImage1), publishedHappening.getImages());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEqualsAddressIgnoreGPSLocation(Address.builder()
                        .name(VENUE)
                        .build(),
                publishedHappening.getCustomAddress());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());
        assertTrue(publishedHappening.isCommentsAllowed());

        //call get endpoint to check if address name is delivered as venue
        mockMvc.perform(get("/grapevine/post/" + publishedHappening.getId())
                .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(publishedHappening.getId()))
                .andExpect(jsonPath("$.happening.venue").value(publishedHappening.getCustomAddress().getName()));
    }

    @Test
    public void happeningPublishRequest_VenueLocationDefinition() throws Exception {

        GeoArea geoArea = th.subGeoArea1Tenant1;
        MediaItem expectedImage1 = th.createMediaItem("blutspende");
        URL imageURL1 = new URL("http://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        ClientLocationDefinition locationDefinition = GrapevineTestHelper.locationDefinitionDaNicola;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(geoArea.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .imageURLs(Collections.singletonList(imageURL1.toString()))
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .customLocationDefinition(locationDefinition)
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningPublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningPublishConfirmation.getPostId()).get();

        assertEquals(geoArea.getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsOnly(geoArea);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(Collections.singletonList(expectedImage1), publishedHappening.getImages());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());
        assertEquals(locationDefinition.getLocationName(), publishedHappening.getCustomAddress().getName());
        assertEquals(locationDefinition.getAddressStreet(), publishedHappening.getCustomAddress().getStreet());
        assertEquals(locationDefinition.getAddressZip(), publishedHappening.getCustomAddress().getZip());
        assertEquals(locationDefinition.getAddressCity(), publishedHappening.getCustomAddress().getCity());
        assertEquals(locationDefinition.getGpsLocation().getLongitude(),
                publishedHappening.getCustomAddress().getGpsLocation().getLongitude());
        assertEquals(locationDefinition.getGpsLocation().getLatitude(),
                publishedHappening.getCustomAddress().getGpsLocation().getLatitude());
        assertTrue(publishedHappening.isCommentsAllowed());

        //call get endpoint to check if address name is delivered as venue
        mockMvc.perform(get("/grapevine/post/" + publishedHappening.getId())
                        .headers(authHeadersFor(th.personSusanneChristmann, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.happening.id").value(publishedHappening.getId()))
                .andExpect(jsonPath("$.happening.venue").value(publishedHappening.getCustomAddress().getName()));
    }

    @Test
    public void happeningPublishRequest_republish_CompleteVerifyingPushMessage() throws Exception {

        Happening happeningToBeRepublished = th.happening1;
        happeningToBeRepublished.setOrganization(th.organizationFeuerwehr);
        th.postRepository.save(happeningToBeRepublished);
        String newText = "NEW TEXT";
        MediaItem expectedImage1 = th.createMediaItem("blutspende");
        URL imageURL1 = new URL("http://example.org/image1");
        testMediaItemService.addTestMediaItem(imageURL1, expectedImage1);
        Organization newOrganization = th.organizationGartenbau;

        NewsSource newsSource = happeningToBeRepublished.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(happeningToBeRepublished.getGeoAreas().iterator().next().getId())
                                .authorName(happeningToBeRepublished.getAuthorName())
                                .text(newText)
                                .organizationId(newOrganization.getId())
                                .externalId(happeningToBeRepublished.getExternalId())
                                .newsURL(happeningToBeRepublished.getUrl())
                                .imageURLs(Collections.singletonList(imageURL1.toString()))
                                .categories(happeningToBeRepublished.getCategories())
                                .organizer(happeningToBeRepublished.getOrganizer())
                                .startTime(happeningToBeRepublished.getStartTime())
                                .endTime(happeningToBeRepublished.getEndTime())
                                .allDayEvent(happeningToBeRepublished.isAllDayEvent())
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningRepublishConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningRepublishConfirmation.getPostId()).get();
        assertEquals(happeningToBeRepublished.getTenant(), publishedHappening.getTenant());
        assertEquals(happeningToBeRepublished.getGeoAreas(), publishedHappening.getGeoAreas());
        assertEquals(happeningToBeRepublished.getAuthorName(), publishedHappening.getAuthorName());
        assertEquals(newText, publishedHappening.getText());
        assertThat(publishedHappening.getOrganization()).isEqualTo(newOrganization);
        assertEquals(happeningToBeRepublished.getExternalId(), publishedHappening.getExternalId());
        assertEquals(happeningToBeRepublished.getUrl(), publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(Collections.singletonList(expectedImage1), publishedHappening.getImages());
        assertEquals(happeningToBeRepublished.getCategories(), publishedHappening.getCategories());
        assertEquals(happeningToBeRepublished.getOrganizer(), publishedHappening.getOrganizer());
        assertEquals(happeningToBeRepublished.getStartTime(), publishedHappening.getStartTime());
        assertEquals(happeningToBeRepublished.getEndTime(), publishedHappening.getEndTime());
        assertEquals(happeningToBeRepublished.isAllDayEvent(), publishedHappening.isAllDayEvent());
        assertTrue(publishedHappening.isCommentsAllowed());

        // verify push message
        waitForEventProcessing();

        ClientPostChangeConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(publishedHappening.getGeoAreas(),
                        ClientPostChangeConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);

        assertEquals(happeningRepublishConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(happeningRepublishConfirmation.getPostId(), pushedEvent.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void happeningPublishRequest_HappeningWithSameExternalId() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        String externalId = "4711-2";

        mockMvc.perform(post("/grapevine/event/newsItemPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientNewsItemCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName("Susanne Christman")
                                .text("Text…")
                                .externalId(externalId)
                                .newsURL(newsSource.getSiteUrl() + "/url2")
                                .categories("Sport")
                                .build())))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName("Karl Napp")
                                .text("Text…")
                                .externalId(externalId)
                                .newsURL(newsSource.getSiteUrl() + "/url1")
                                .categories("Katze")
                                .organizer("Kater Carlos")
                                .venue("Veranstaltungsort")
                                .startTime(System.currentTimeMillis())
                                .endTime(System.currentTimeMillis() + 10)
                                .allDayEvent(false)
                                .build())))
                .andExpect(isException(ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED));
    }

    @Test
    public void happeningPublishRequest_MissingApiKey() throws Exception {

        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void happeningPublishRequest_WrongApiKey() throws Exception {

        String wrongApiKey = UUID.randomUUID().toString();
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, wrongApiKey)
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));
    }

    @Test
    public void happeningPublishRequest_InvalidAttributes() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        //missing external id
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException("externalId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //missing news url
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException("newsURL", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //missing start time
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException("startTime", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // end time 1 day before start time
        long end = start - TimeUnit.DAYS.toMillis(1);

        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(!allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //end time equal to start time
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(start)
                                .allDayEvent(!allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //text too long
        String text = RandomStringUtils.randomPrint(maxNumberCharsPerPost + 1);

        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .authorName(AUTHOR_NAME)
                                .text(text)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningPublishRequest_NoImages() throws Exception {

        GeoArea geoArea = th.subGeoArea1Tenant1;
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        ClientHappeningCreateOrUpdateByNewsSourceRequest happeningCreateRequest =
                ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                        .geoAreaId(geoArea.getId())
                        .authorName(AUTHOR_NAME)
                        .text(HAPPENING_TEXT)
                        .externalId(EXTERNAL_ID)
                        .newsURL(POST_URL)
                        .categories(CATEGORIES)
                        .organizer(ORGANIZER)
                        .venue(VENUE)
                        .startTime(start)
                        .endTime(end)
                        .allDayEvent(allDayEvent)
                        .build();

        MvcResult publishRequestResult = mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(happeningCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation happeningCreateConfirmation =
                toObject(publishRequestResult.getResponse(),
                        ClientExternalPostCreateOrUpdateByNewsSourceConfirmation.class);

        Happening publishedHappening = (Happening) th.externalPostRepository.findById(
                happeningCreateConfirmation.getPostId()).get();
        assertEquals(geoArea.getTenant(), publishedHappening.getTenant());
        assertThat(publishedHappening.getGeoAreas()).containsOnly(geoArea);
        assertEquals(AUTHOR_NAME, publishedHappening.getAuthorName());
        assertEquals(HAPPENING_TEXT, publishedHappening.getText());
        assertEquals(EXTERNAL_ID, publishedHappening.getExternalId());
        assertEquals(POST_URL, publishedHappening.getUrl());
        assertEquals(newsSource, publishedHappening.getNewsSource());
        assertEquals(CATEGORIES, publishedHappening.getCategories());
        assertEquals(ORGANIZER, publishedHappening.getOrganizer());
        assertEquals(start, publishedHappening.getStartTime());
        assertEquals(end, publishedHappening.getEndTime());
        assertEquals(allDayEvent, publishedHappening.isAllDayEvent());

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasLoud(publishedHappening.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);

        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPostId());
        assertEquals(happeningCreateConfirmation.getPostId(), pushedEvent.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }


    @Test
    public void happeningPublishRequest_GeoAreaIds() throws Exception {

        List<String> geoAreaIds = Stream.of(th.geoAreaEisenberg.getId(), th.geoAreaKaiserslautern.getId())
                .collect(Collectors.toList());
        NewsSource newsSource = th.newsSourceDigitalbach;
        assertThat(newsSource.getAppVariant()).isNotNull();

        // no id at all
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // single id and list of ids
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaId(th.subGeoArea1Tenant1.getId())
                                .geoAreaIds(geoAreaIds)
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // list of ids with duplicates
        geoAreaIds.add(th.geoAreaEisenberg.getId());
        mockMvc.perform(post("/grapevine/event/happeningPublishRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .contentType(contentType)
                        .content(json(ClientHappeningCreateOrUpdateByNewsSourceRequest.builder()
                                .geoAreaIds(geoAreaIds)
                                .authorName(AUTHOR_NAME)
                                .text(HAPPENING_TEXT)
                                .externalId(EXTERNAL_ID)
                                .newsURL(POST_URL)
                                .categories(CATEGORIES)
                                .organizer(ORGANIZER)
                                .venue(VENUE)
                                .startTime(start)
                                .endTime(end)
                                .allDayEvent(allDayEvent)
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void happeningDeleteRequest_WithHappeningParticipants() throws Exception {

        Happening happening = th.happening1;
        NewsSource newsSource = happening.getNewsSource();
        assertThat(newsSource.getAppVariant()).isNotNull();

        long happeningParticipantsCount = happeningParticipantRepository.countAllByHappening(happening);
        assertEquals(5, happeningParticipantsCount);

        mockMvc.perform(post("/grapevine/event/externalPostDeleteRequest")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .postId(happening.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.postId").value(happening.getId()));

        Happening deletedHappening = happeningRepository.findById(happening.getId()).get();
        assertTrue(deletedHappening.isDeleted());
        happeningParticipantsCount = happeningParticipantRepository.countAllByHappening(happening);
        assertEquals(0, happeningParticipantsCount);

        // verify push message
        waitForEventProcessing();

        ClientPostDeleteConfirmation pushedEvent =
                testClientPushService.getPushedEventToGeoAreasSilent(happening.getGeoAreas(),
                        ClientPostDeleteConfirmation.class, DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertEquals(happening.getId(), pushedEvent.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

    private static void assertEqualsAddressIgnoreGPSLocation(Address expected, Address actual) {
        if (expected == null) {
            assertNull(actual, "If expected address is null, actual should also be null");
        } else {
            if (actual == null) {
                fail("If expected address is not null, actual should also be not null");
            } else {
                assertTrue(
                        StringUtils.equals(expected.getName(), actual.getName()) &&
                                StringUtils.equals(expected.getStreet(), actual.getStreet()) &&
                                StringUtils.equals(expected.getZip(), actual.getZip()) &&
                                StringUtils.equals(expected.getCity(), actual.getCity()),
                        "Attributes of both not null addresses should equal. Got actual = " +
                                actual.toHumanReadableString(true) + " and expected = " +
                                expected.toHumanReadableString(true));
            }
        }
    }

}
