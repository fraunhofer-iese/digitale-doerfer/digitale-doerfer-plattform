/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.html2pdf.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;

public class Html2PdfServiceTest extends BaseServiceTest {

    //set to true to inspect the actual pdfs. Check the log for the output file name
    private static final boolean DUMP_PDF_TO_TMP = false;

    @Autowired
    private Html2PdfService html2PdfService;

    private final String HTML =
            "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "  <body>\n" +
                    "    <h1 style=\"text-align:center\">PDF-Test</h1>" +
                    "    <p style=\"color:blue; text-align:center\">Inhalt des Dokuments</p>\n" +
                    "  </body>\n" +
                    "</html>";

    private Map<String, byte[]> pdfContents;

    @Override
    public void createEntities() throws Exception {
        pdfContents = new HashMap<>();
    }

    @Override
    public void tearDown() throws Exception {

    }

    @Test
    public void convert(TestInfo testInfo) throws Exception {
        byte[] content = html2PdfService.convert(HTML, 150, 80);

        assertNotNull(content);
        assertEquals(content.getClass(), byte[].class);

        pdfContents.put(testInfo.getDisplayName(), content);
    }

    @Test
    public void convertA4(TestInfo testInfo) throws Exception {
        byte[] content = html2PdfService.convertA4(HTML);

        assertNotNull(content);
        assertEquals(content.getClass(), byte[].class);

        pdfContents.put(testInfo.getDisplayName(), content);
    }

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_PDF_TO_TMP) {
            deleteTempFilesStartingWithClassName(Html2PdfServiceTest.class);
        }
    }

    @AfterEach
    public void dumpPdfsToTmp(TestInfo testInfo) throws Exception {
        if (DUMP_PDF_TO_TMP) {
            final String displayName = testInfo.getDisplayName();
            final byte[] content = pdfContents.get(displayName);
            final Path tempFile =
                        Files.createTempFile(
                                getClass().getSimpleName() + "-" + displayName.replace("(TestInfo)", "") + "-",
                                ".pdf");
                log.info("Saving {} to {}", tempFile.getFileName().toString(), tempFile.toUri());
                Files.write(tempFile, content);

        }
    }

}
