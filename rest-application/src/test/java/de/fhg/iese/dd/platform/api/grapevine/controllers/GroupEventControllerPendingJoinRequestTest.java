/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupJoinRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class GroupEventControllerPendingJoinRequestTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    private AppVariant appVariant;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        th.createPushEntities();

        appVariant = th.appVariantKL_EB;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void groupPendingJoinAcceptRequest_withDoubleAcceptAndDeny() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA
        ))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeApproval = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeApproval.getStatus());

        // Accept pending join request. Ben has group admin rights
        mockMvc.perform(buildJoinAcceptRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(true));

        GroupMembership groupMembershipAfterApproval = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should now be approved in repository
        assertEquals(GroupMembershipStatus.APPROVED, groupMembershipAfterApproval.getStatus());

        // Accept join request again and expect status to not change
        mockMvc.perform(buildJoinAcceptRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // Now deny join request and expect status to not change
        mockMvc.perform(buildJoinDenyRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.APPROVED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonLoud with acceptance was triggered
        ClientGroupPendingJoinAcceptConfirmation clientGroupPendingJoinAcceptConfirmation =
                testClientPushService.getPushedEventToPersonLoud(th.personAnjaTenant1Dorf1,
                        ClientGroupPendingJoinAcceptConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED);

        assertEquals(clientGroupPendingJoinAcceptConfirmation.getGroupId(), th.groupSchachvereinAAA.getId());
    }

    @Test
    public void groupPendingJoinAcceptRequestUnauthorized() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA
        ))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeApproval = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeApproval.getStatus());

        // Try to accept pending join request. Chloe isn't even in the group
        mockMvc.perform(buildJoinAcceptRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterApproval = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterApproval.getStatus());
    }

    @Test
    public void groupPendingJoinDenyRequest_withDoubleDenyAndAccept() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA
        ))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeDenial = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeDenial.getStatus());

        // Deny pending join request. Ben has group admin rights
        mockMvc.perform(buildJoinDenyRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(true));

        GroupMembership groupMembershipAfterDenial = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should now be rejected in repository
        assertEquals(GroupMembershipStatus.REJECTED, groupMembershipAfterDenial.getStatus());

        // Deny join request again and expect status to not change
        mockMvc.perform(buildJoinDenyRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // Accept join request and expect status to not change
        mockMvc.perform(buildJoinAcceptRequest(th.personBenTenant1Dorf2, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.groupId").value(th.groupSchachvereinAAA.getId()))
                .andExpect(jsonPath("$.groupMembershipStatus").value(GroupMembershipStatus.REJECTED.toString()))
                .andExpect(jsonPath("$.groupMembershipStatusChanged").value(false));

        // verify push message
        waitForEventProcessing();

        // Assert that pushToPersonLoud for denial was triggered
        ClientGroupPendingJoinDenyConfirmation clientGroupPendingJoinDenyConfirmation =
                testClientPushService.getPushedEventToPersonLoud(th.personAnjaTenant1Dorf1,
                        ClientGroupPendingJoinDenyConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED);

        assertEquals(clientGroupPendingJoinDenyConfirmation.getGroupId(), th.groupSchachvereinAAA.getId());
    }

    @Test
    public void groupPendingJoinDenyUnauthorized() throws Exception {

        // Create a pending group join request
        mockMvc.perform(buildJoinRequest(th.personAnjaTenant1Dorf1,
                th.groupSchachvereinAAA
        ))
                .andExpect(status().isOk());

        GroupMembership groupMembershipBeforeDenial = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Make sure that membership status is still pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipBeforeDenial.getStatus());

        // Try to deny pending join request. Chloe isn't even in the group
        mockMvc.perform(buildJoinDenyRequest(th.personChloeTenant1Dorf1, th.groupSchachvereinAAA,
                th.personAnjaTenant1Dorf1))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        GroupMembership groupMembershipAfterDenial = th.groupMembershipRepository.findByGroupAndMember(
                th.groupSchachvereinAAA, th.personAnjaTenant1Dorf1).get();

        // Status should still be pending
        assertEquals(GroupMembershipStatus.PENDING, groupMembershipAfterDenial.getStatus());
    }

    private MockHttpServletRequestBuilder buildJoinRequest(Person caller, Group group) throws IOException {
        return post("/grapevine/group/event/groupJoinRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGroupJoinRequest.builder()
                        .groupId(group.getId())
                        .memberIntroductionText("Huhu")
                        .build()));
    }

    private MockHttpServletRequestBuilder buildJoinAcceptRequest(Person caller, Group group, Person personToAccept)
            throws IOException {
        return post("/grapevine/group/event/groupPendingJoinAcceptRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupPendingJoinAcceptRequest.builder()
                        .groupId(group.getId())
                        .personIdToAccept(personToAccept.getId())
                        .build()));
    }

    private MockHttpServletRequestBuilder buildJoinDenyRequest(Person caller, Group group, Person personToDeny)
            throws IOException {
        return post("/grapevine/group/event/groupPendingJoinDenyRequest")
                .headers(authHeadersFor(caller))
                .contentType(contentType)
                .content(json(ClientGroupPendingJoinDenyRequest.builder()
                        .groupId(group.getId())
                        .personIdToDeny(personToDeny.getId())
                        .build()));
    }

}
