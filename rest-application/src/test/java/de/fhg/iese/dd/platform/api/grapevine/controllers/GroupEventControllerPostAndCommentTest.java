/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.*;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipChangeRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.gossip.ClientGossipCreateInGroupRequest;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GossipPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for: Create gossip in groups (GossipCreateInGroupRequest), update and delete gossip, write comment on post in
 * group, update and delete comment
 */
public class GroupEventControllerPostAndCommentTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Autowired
    private IGroupService groupService;

    private AppVariant appVariant;
    private AppVariant otherAppVariant;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(GossipPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Plausch\", \"itemName\": \"Plausch\", \"maxNumberCharsPerPost\": 280 }")
                .build()
                .withConstantId());

        appVariant = th.appVariantKL_EB;
        otherAppVariant = th.appVariantMZ;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void gossipCreateInGroupRequest_minimalAttributes_verifyingPushMessage() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final String text = "Das Wetter ist so schön, hat jemand spontan Lust auf eine Tour?";
        final boolean hiddenForOtherHomeAreas = false;
        final List<Person> otherGroupMembers =
                th.groupMembershipRepository.findAllMembersByGroupAndMembershipStatus(group,
                        GroupMembershipStatus.APPROVED)
                        .stream()
                        .filter(person -> !person.equals(caller))
                        .collect(Collectors.toList());
        //designate blocking person and create blocking
        assertThat(otherGroupMembers.size()).isGreaterThan(
                1); //we should have at least 2 persons to have useful test cases
        final Person blockingPerson = otherGroupMembers.get(0);
        personBlockingService.blockPerson(th.app, blockingPerson, caller);

        final MvcResult result = mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipCreateInGroupRequest.builder()
                        .groupId(group.getId())
                        .hiddenForOtherHomeAreas(hiddenForOtherHomeAreas)
                        .text(text)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(result.getResponse(),
                ClientPostCreateConfirmation.class);

        Gossip gossipFromRepo = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();
        assertEquals(caller, gossipFromRepo.getCreator());
        assertEquals(group, gossipFromRepo.getGroup());
        assertEquals(text, gossipFromRepo.getText());
        assertThat(gossipFromRepo.getGeoAreas()).containsExactly(caller.getHomeArea());
        assertEquals(group.getTenant(), gossipFromRepo.getTenant());
        assertEquals(hiddenForOtherHomeAreas, gossipFromRepo.isHiddenForOtherHomeAreas());
        assertFalse(gossipFromRepo.isDeleted());
        assertTrue(gossipFromRepo.getImages() == null || gossipFromRepo.getImages().isEmpty());
        //if no custom location is provided, the center of the home area is taken
        assertEquals(caller.getHomeArea().getCenter(), gossipFromRepo.getCreatedLocation());

        // verify push message
        waitForEventProcessing();

        //verify silent push to post creator
        ClientPostCreateConfirmation pushedEvent = testClientPushService.getPushedEventToPersonSilent(caller,
                ClientPostCreateConfirmation.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED);

        assertThat(pushedEvent.getPostId()).isEqualTo(gossipCreateConfirmation.getPostId());

        //verify loud push to other group members except blocked one
        for (Person otherGroupMember : otherGroupMembers) {
            if (otherGroupMember.equals(blockingPerson)) {
                log.info("{} should get a silent push message (blocks the creator)", otherGroupMember);

                pushedEvent = testClientPushService.getPushedEventToPersonSilent(blockingPerson,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED);

            } else {
                log.info("{} should get a loud push message", otherGroupMember);

                pushedEvent = testClientPushService.getPushedEventToPersonLoud(caller,
                        otherGroupMember,
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_IN_GROUP_CREATED);

            }
            assertThat(pushedEvent.getPostId()).isEqualTo(gossipCreateConfirmation.getPostId());
            assertThat(pushedEvent.getPost().getGossip().getId())
                    .isEqualTo(gossipCreateConfirmation.getPost().getGossip().getId());
        }

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void gossipCreateInGroupRequest_ContentCreationRestricted() throws Exception {

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personAnjaTenant1Dorf1,
                appVariant,
                post("/grapevine/event/gossipCreateInGroupRequest")
                        .contentType(contentType)
                        .content(json(ClientGossipCreateInGroupRequest.builder()
                                .groupId(th.groupAdfcAAP.getId())
                                .hiddenForOtherHomeAreas(false)
                                .text("Du Koffer!")
                                .build())));
    }

    @Test
    public void gossipCreateInGroupRequest_allAttributes() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final String text = "Das Wetter ist so schön, hat jemand spontan Lust auf eine Tour?";
        final boolean hiddenForOtherHomeAreas = true;
        final ClientGPSLocation createdLocation = new ClientGPSLocation(13, 14);
        final TemporaryMediaItem mediaItem1 = th.createTemporaryMediaItem(caller, th.nextTimeStamp());
        final TemporaryMediaItem mediaItem2 = th.createTemporaryMediaItem(caller, th.nextTimeStamp());

        final MvcResult result = mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipCreateInGroupRequest.builder()
                        .groupId(group.getId())
                        .hiddenForOtherHomeAreas(hiddenForOtherHomeAreas)
                        .text(text)
                        .createdLocation(createdLocation)
                        .temporaryMediaItemIds(Arrays.asList(mediaItem1.getId(), mediaItem2.getId()))
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientPostCreateConfirmation gossipCreateConfirmation = toObject(result.getResponse(),
                ClientPostCreateConfirmation.class);

        Gossip gossipFromRepo = (Gossip) th.postRepository.findById(gossipCreateConfirmation.getPost().getId()).get();
        assertEquals(caller, gossipFromRepo.getCreator());
        assertEquals(group, gossipFromRepo.getGroup());
        assertEquals(text, gossipFromRepo.getText());
        assertThat(gossipFromRepo.getGeoAreas()).containsExactly(caller.getHomeArea());
        assertEquals(group.getTenant(), gossipFromRepo.getTenant());
        assertEquals(hiddenForOtherHomeAreas, gossipFromRepo.isHiddenForOtherHomeAreas());
        assertFalse(gossipFromRepo.isDeleted());
        assertEquals(createdLocation.getLatitude(), gossipFromRepo.getCreatedLocation().getLatitude());
        assertEquals(createdLocation.getLongitude(), gossipFromRepo.getCreatedLocation().getLongitude());
        assertEquals(mediaItem1.getMediaItem(), gossipFromRepo.getImages().get(0));
        assertEquals(mediaItem2.getMediaItem(), gossipFromRepo.getImages().get(1));
    }

    @Test
    public void gossipCreateInGroupRequest_notGroupMember() throws Exception {

        final Person caller = th.personEckhardTenant1Dorf1NoMember;
        final Group group = th.groupAdfcAAP;
        final String text = "Das Wetter ist so schön, hat jemand spontan Lust auf eine Tour?";
        final boolean hiddenForOtherHomeAreas = false;

        mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipCreateInGroupRequest.builder()
                        .groupId(group.getId())
                        .hiddenForOtherHomeAreas(hiddenForOtherHomeAreas)
                        .text(text)
                        .build())))
                .andExpect(isException(ClientExceptionType.NOT_MEMBER_OF_GROUP));

        //result should not change when caller has other group membership status
        th.iterateOverNonMemberGroupMembershipStatus(caller, group, membershipStatus ->
                mockMvc.perform(post("/grapevine/event/gossipCreateInGroupRequest")
                        .headers(authHeadersFor(caller, appVariant))
                        .contentType(contentType)
                        .content(json(ClientGossipCreateInGroupRequest.builder()
                                .groupId(group.getId())
                                .hiddenForOtherHomeAreas(hiddenForOtherHomeAreas)
                                .text(text)
                                .build())))
                        .andExpect(isException(ClientExceptionType.NOT_MEMBER_OF_GROUP))
        );
    }

    @Test
    public void gossipChangeRequest_changeText() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertTrue(post.getText().startsWith("Willkommen an alle ADFC")); //test data integrity
        final String newText = "Willkommen an alle ADFC-Mitglieder*innen und -Interessent*innen. Hier können " +
                "wir über Aktion*innen informieren und Vorschläge sammeln.";

        final MvcResult result = mockMvc.perform(post("/grapevine/event/gossipChangeRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientGossipChangeRequest.builder()
                        .postId(post.getId())
                        .text(newText)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientPostChangeConfirmation postChangeConfirmation = toObject(result.getResponse(),
                ClientPostChangeConfirmation.class);

        assertEquals(post.getId(), postChangeConfirmation.getPostId());
        Gossip gossipFromRepo = (Gossip) th.postRepository.findById(postChangeConfirmation.getPostId()).get();
        assertEquals(caller, gossipFromRepo.getCreator());
        assertEquals(group, gossipFromRepo.getGroup());
        assertEquals(newText, gossipFromRepo.getText());
        assertThat(gossipFromRepo.getGeoAreas()).containsExactly(caller.getHomeArea());
        assertEquals(group.getTenant(), gossipFromRepo.getTenant());
        assertFalse(gossipFromRepo.isDeleted());
    }

    @Test
    public void postDeleteRequest_deleteGroupPost() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertFalse(th.postRepository.findById(post.getId()).get().isDeleted()); //test data integrity

        final MvcResult result = mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .postId(post.getId())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientPostDeleteConfirmation postDeleteConfirmation = toObject(result.getResponse(),
                ClientPostDeleteConfirmation.class);

        assertEquals(post.getId(), postDeleteConfirmation.getPostId());
        final Post deletedPost = th.postRepository.findById(post.getId()).get();
        assertTrue(deletedPost.isDeleted());
        assertEquals(nowTime, deletedPost.getDeletionTime());
    }

    @Test
    public void commentCreateRequest_commentInGroup() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity

        assertCommentCreateRequestIsSuccessful(caller, post, "Wollt Ihr Euch alle mal vorstellen?", appVariant);
    }

    @Test
    public void commentCreateRequest_commentInGroup_noPushToPostCreatorNotInGroup() throws Exception {

        final Person caller = th.personBenTenant1Dorf2;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity

        // Post Creator leaves group
        groupService.leaveGroup(group, th.personAnjaTenant1Dorf1);

        assertCommentCreateRequestIsSuccessful(caller, post, "Wollt Ihr Euch alle mal ausziehen?", appVariant);

        //wait before testing push
        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void commentCreateRequest_replyToCommentInGroup_noPushToCommentCreatorNotInGroup() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        final Comment firstComment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(0);
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity

        // Comment Creator leaves group
        groupService.leaveGroup(group, th.personBenTenant1Dorf2);

        assertCommentCreateRequestIsSuccessful(caller, post, "Hallo Ben", firstComment.getId(), appVariant);

        //wait before testing push
        waitForEventProcessing();

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void commentCreateRequest_commentInGroup_appVariantOtherTenant() throws Exception {
        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupSchachvereinAAA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity

        assertCommentCreateRequestIsNotSuccessful(caller, post, "Ist das hier die Krabbelgruppe?", otherAppVariant);
    }

    @Test
    public void commentCreateRequest_commentInGroup_appVariantOtherTenant_member() throws Exception {
        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP; //caller is member
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity

        assertCommentCreateRequestIsSuccessful(caller, post, "😊🥰😍", otherAppVariant);
    }

    @Test
    public void commentCreateRequest_commentInPublicGroupNotAllowed_notMember() throws Exception {

        final Person caller = th.personEckhardTenant1Dorf1NoMember;
        final Group group = th.groupAdfcAAP;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity
        final String text = "Hallo Springkatze 🐈";

        assertCommentCreateRequestIsNotSuccessful(caller, post, text,
                post.getGroup().getId(), ClientExceptionType.NOT_MEMBER_OF_GROUP, appVariant);

        //result should not change when caller has other group membership status
        th.iterateOverNonMemberGroupMembershipStatus(caller, group, membershipStatus ->
                assertCommentCreateRequestIsNotSuccessful(caller, post, text,
                        post.getGroup().getId(), ClientExceptionType.NOT_MEMBER_OF_GROUP, appVariant)
        );
    }

    @Test
    public void commentCreateRequest_commentInRestrictedGroupNotAllowed_notMember() throws Exception {

        final Person caller = th.personEckhardTenant1Dorf1NoMember;
        final Group group = th.groupErfahrungMMA;
        final Post post = th.groupsToPostsAndComments.get(group).get(0).getLeft();
        assertThat(post.getGroup()).isEqualTo(group); //test data integrity
        final String text = "Wenn sich Leute nicht gegenseitig anpöbeln, ja!";

        assertCommentCreateRequestIsNotSuccessful(caller, post, text, appVariant);

        //result should not change when caller has other group membership status
        th.iterateOverNonMemberGroupMembershipStatus(caller, group, membershipStatus ->
                assertCommentCreateRequestIsNotSuccessful(caller, post, text, appVariant)
        );
    }

    private void assertCommentCreateRequestIsSuccessful(Person caller, Post post, String text, AppVariant appVariant)
            throws Exception {
        assertCommentCreateRequestIsSuccessful(caller, post, text, null, appVariant);
    }

    private void assertCommentCreateRequestIsSuccessful(Person caller, Post post, String text, String replyToCommentId,
            AppVariant appVariant)
            throws Exception {
        final MvcResult result = mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId(post.getId())
                        .text(text)
                        .replyToCommentId(replyToCommentId)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientCommentCreateConfirmation commentCreateConfirmation = toObject(result.getResponse(),
                ClientCommentCreateConfirmation.class);

        Comment commentFromRepo = th.commentRepository.findById(commentCreateConfirmation.getCommentId()).get();
        assertEquals(caller, commentFromRepo.getCreator());
        assertEquals(text, commentFromRepo.getText());
        assertFalse(commentFromRepo.isDeleted());
    }

    private void assertCommentCreateRequestIsNotSuccessful(Person caller, Post post, String text,
            AppVariant appVariant) throws Exception {
        assertCommentCreateRequestIsNotSuccessful(caller, post, text, post.getId(), ClientExceptionType.POST_NOT_FOUND,
                appVariant);
    }

    private void assertCommentCreateRequestIsNotSuccessful(Person caller, Post post, String text,
            String exceptionDetail, ClientExceptionType exceptionType, AppVariant appVariant) throws Exception {
        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId(post.getId())
                        .text(text)
                        .build())))
                .andExpect(isException(exceptionDetail, exceptionType));
    }

    @Test
    public void commentChangeRequest_changeText() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(1);
        assertEquals(caller, comment.getCreator()); //test data integrity
        assertThat(comment.getPost().getGroup()).isEqualTo(group); //test data integrity
        final String newText = "Moinsen!";

        final MvcResult result = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(ClientCommentChangeRequest.builder()
                        .commentId(comment.getId())
                        .text(newText)
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation = toObject(result.getResponse(),
                ClientCommentChangeConfirmation.class);

        assertEquals(comment.getId(), commentChangeConfirmation.getCommentId());
        Comment commentFromRepo = th.commentRepository.findById(commentChangeConfirmation.getCommentId()).get();
        assertEquals(caller, commentFromRepo.getCreator());
        assertEquals(newText, commentFromRepo.getText());
        assertFalse(commentFromRepo.isDeleted());
    }

    @Test
    public void commentDeleteRequest_deleteCommentOfGroupPost() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        final Person caller = th.personAnjaTenant1Dorf1;
        final Group group = th.groupAdfcAAP;
        final Comment comment = th.groupsToPostsAndComments.get(group).get(0).getRight().get(1);
        assertEquals(caller, comment.getCreator()); //test data integrity
        assertThat(comment.getPost().getGroup()).isEqualTo(group); //test data integrity
        assertFalse(th.commentRepository.findById(comment.getId()).get().isDeleted()); //test data integrity

        final MvcResult result = mockMvc.perform(post("/grapevine/comment/event/commentDeleteRequest")
                .headers(authHeadersFor(caller, appVariant))
                .contentType(contentType)
                .content(json(new ClientCommentDeleteRequest(comment.getId()))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn();

        ClientCommentDeleteConfirmation commentDeleteConfirmation = toObject(result.getResponse(),
                ClientCommentDeleteConfirmation.class);

        assertEquals(comment.getId(), commentDeleteConfirmation.getCommentId());
        assertTrue(th.commentRepository.findById(comment.getId()).get().isDeleted());
        final Comment deletedComment = th.commentRepository.findById(comment.getId()).get();
        assertTrue(deletedComment.isDeleted());
        assertEquals(nowTime, deletedComment.getDeletionTime());
    }

}
