/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostRequest;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GrapevineAppControllerTest extends BaseServiceTest {

    @Autowired
    private ConvenienceRepository convenienceRepository;
    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;
    @Autowired
    private GrapevineTestHelper th;

    private Convenience convenience;
    private GeoArea geoAreaConvenience;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
        createConveniences();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    private void createConveniences() {

        geoAreaConvenience = th.geoAreaDorf1InEisenberg;

        convenience = convenienceRepository.save(Convenience.builder()
                .name("Hirnspende")
                .description("Spende dein Hirn und tu etwas Gutes")
                .organizationName("Brain Inc.")
                .organizationLogo(th.createMediaItem("Hirn_Logo"))
                .url("https://www.hirnspendedienst.com")
                .urlLabel("Termin finden")
                .overviewImage(th.createMediaItem("Hirn_Overview"))
                .detailImages(Arrays.asList(th.createMediaItem("Hirn_Detail1"), th.createMediaItem("Hirn_Detail2")))
                .build());
        convenienceGeoAreaMappingRepository.save(ConvenienceGeoAreaMapping.builder()
                .convenience(convenience)
                .geoArea(geoAreaConvenience)
                .build()
                .withConstantId());
    }

    @Test
    public void getOnboardingStatus() throws Exception {

        Person person = th.personRegularKarl;
        AppVariant appVariant = th.appVariant4AllTenants;

        Person chatParticipant = th.personAnnikaSchneider;
        ClientChat expectedChat = createChat(person, chatParticipant);

        th.selectGeoAreaForAppVariant(appVariant, person, geoAreaConvenience);

        mockMvc.perform(get("/grapevine/onboardingStatus")
                        .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(person.getId()))
                .andExpect(jsonPath("$.ownChats", hasSize(1)))
                .andExpect(jsonPath("$.ownChats[0].lastMessage.id").value(expectedChat.getLastMessage().getId()))
                .andExpect(jsonPath("$.ownChats[0].lastMessage.chatId").value(expectedChat.getId()))
                .andExpect(jsonPath("$.ownChats[0].lastMessage.message")
                        .value(expectedChat.getLastMessage().getMessage()))
                .andExpect(jsonPath("$.ownChats[0].lastMessage.senderFirstName")
                        .value(expectedChat.getLastMessage().getSenderFirstName()))
                .andExpect(jsonPath("$.ownChats[0].lastMessage.senderLastName")
                        .value(expectedChat.getLastMessage().getSenderLastName()))
                .andExpect(jsonPath("$.ownChats[0].chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$.ownChats[0].chatParticipants[0].id")
                        .value(expectedChat.getChatParticipants().get(0).getId()))
                .andExpect(jsonPath("$.ownChats[0].chatParticipants[1].id")
                        .value(expectedChat.getChatParticipants().get(1).getId()))
                .andExpect(jsonPath("$.conveniences", hasSize(1)))
                .andExpect(jsonPath("$.conveniences[0].id").value(convenience.getId()))
                .andExpect(jsonPath("$.conveniences[0].name").value(convenience.getName()))
                .andExpect(jsonPath("$.conveniences[0].description").value(convenience.getDescription()))
                .andExpect(jsonPath("$.conveniences[0].organizationName")
                        .value(convenience.getOrganizationName()))
                .andExpect(jsonPath("$.conveniences[0].url").value(convenience.getUrl()))
                .andExpect(jsonPath("$.conveniences[0].urlLabel").value(convenience.getUrlLabel()))
                .andExpect(jsonPath("$.conveniences[0].organizationLogo.id")
                        .value(convenience.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$.conveniences[0].overviewImage.id")
                        .value(convenience.getOverviewImage().getId()))
                .andExpect(jsonPath("$.conveniences[0].detailImages[0].id")
                        .value(convenience.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$.suggestionCategories", hasSize(th.suggestionCategoriesInOrder.size())))
                .andExpect(jsonPath("$.suggestionCategories[0].id")
                        .value(th.suggestionCategoriesInOrder.get(0).getId()))
                .andExpect(jsonPath("$.suggestionCategories[0].orderValue")
                        .value(th.suggestionCategoriesInOrder.get(0).getOrderValue()))
                .andExpect(jsonPath("$.suggestionCategories[0].displayName")
                        .value(th.suggestionCategoriesInOrder.get(0).getDisplayName()))
                .andExpect(jsonPath("$.suggestionCategories[0].shortName")
                        .value(th.suggestionCategoriesInOrder.get(0).getShortName()));
    }

    @Test
    public void getOnboardingStatus_NoChats() throws Exception {

        Person person = th.personRegularKarl;
        AppVariant appVariant = th.appVariant4AllTenants;

        th.selectGeoAreaForAppVariant(appVariant, person, geoAreaConvenience);

        mockMvc.perform(get("/grapevine/onboardingStatus")
                        .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(person.getId()))
                .andExpect(jsonPath("$.ownChats", hasSize(0)))
                .andExpect(jsonPath("$.conveniences", hasSize(1)))
                .andExpect(jsonPath("$.conveniences[0].id").value(convenience.getId()))
                .andExpect(jsonPath("$.conveniences[0].name").value(convenience.getName()))
                .andExpect(jsonPath("$.conveniences[0].description").value(convenience.getDescription()))
                .andExpect(jsonPath("$.conveniences[0].organizationName")
                        .value(convenience.getOrganizationName()))
                .andExpect(jsonPath("$.conveniences[0].url").value(convenience.getUrl()))
                .andExpect(jsonPath("$.conveniences[0].urlLabel").value(convenience.getUrlLabel()))
                .andExpect(jsonPath("$.conveniences[0].organizationLogo.id")
                        .value(convenience.getOrganizationLogo().getId()))
                .andExpect(jsonPath("$.conveniences[0].overviewImage.id")
                        .value(convenience.getOverviewImage().getId()))
                .andExpect(jsonPath("$.conveniences[0].detailImages[0].id")
                        .value(convenience.getDetailImages().get(0).getId()))
                .andExpect(jsonPath("$.suggestionCategories", hasSize(th.suggestionCategoriesInOrder.size())))
                .andExpect(jsonPath("$.suggestionCategories[0].id")
                        .value(th.suggestionCategoriesInOrder.get(0).getId()))
                .andExpect(jsonPath("$.suggestionCategories[0].orderValue")
                        .value(th.suggestionCategoriesInOrder.get(0).getOrderValue()))
                .andExpect(jsonPath("$.suggestionCategories[0].displayName")
                        .value(th.suggestionCategoriesInOrder.get(0).getDisplayName()))
                .andExpect(jsonPath("$.suggestionCategories[0].shortName")
                        .value(th.suggestionCategoriesInOrder.get(0).getShortName()));

        th.unSelectAllGeoAreaForAppVariant(appVariant, person);
        th.selectGeoAreaForAppVariant(appVariant, person, th.geoAreaMainz);

        mockMvc.perform(get("/grapevine/onboardingStatus")
                        .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(person.getId()))
                .andExpect(jsonPath("$.ownChats", hasSize(0)))
                .andExpect(jsonPath("$.conveniences", hasSize(0)))
                .andExpect(jsonPath("$.suggestionCategories", hasSize(th.suggestionCategoriesInOrder.size())))
                .andExpect(jsonPath("$.suggestionCategories[0].id")
                        .value(th.suggestionCategoriesInOrder.get(0).getId()))
                .andExpect(jsonPath("$.suggestionCategories[0].orderValue")
                        .value(th.suggestionCategoriesInOrder.get(0).getOrderValue()))
                .andExpect(jsonPath("$.suggestionCategories[0].displayName")
                        .value(th.suggestionCategoriesInOrder.get(0).getDisplayName()))
                .andExpect(jsonPath("$.suggestionCategories[0].shortName")
                        .value(th.suggestionCategoriesInOrder.get(0).getShortName()));
    }

    @Test
    public void getOnboardingStatus_Unauthorized() throws Exception {

        assertOAuth2(get("/grapevine/onboardingStatus"));
    }

    private ClientChat createChat(Person chatParticipant1, Person chatParticipant2) throws Exception {

        Post post = th.gossip1withImages;
        post.setCreator(chatParticipant1);
        th.postRepository.saveAndFlush(post);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message("I have a question about your post! 💛")
                .sentTime(System.currentTimeMillis())
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                        .headers(authHeadersFor(chatParticipant2, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult, ClientChatStartOnPostConfirmation.class);

        return chatStartConfirmation.getChat();
    }

}
