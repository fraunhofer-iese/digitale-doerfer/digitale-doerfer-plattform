/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class GrapevinePostPublishWorkerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private IPostService postService;

    @Autowired
    private GrapevinePostPublishWorker grapevinePostPublishWorker;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void publishDueUnpublishedPosts() throws Exception {

        final long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);

        ExternalPost postNotToBePublished1 = th.unpublishedHappening1;
        ExternalPost postNotToBePublished2 = th.newsItem1;
        ExternalPost postNotToBePublished3 = th.newsItem2;
        ExternalPost newsItemToBePublished = th.unpublishedNewsItem1;
        ExternalPost happeningToBePublished = th.happening3;

        //publish time is not over -> not published
        postNotToBePublished1.setDesiredPublishTime(now + 1);
        postNotToBePublished1.setDesiredUnpublishTime(null);
        postNotToBePublished1.setPublished(false);

        //publish and unpublish time is over -> not published
        postNotToBePublished2.setDesiredPublishTime(now - 2);
        postNotToBePublished2.setDesiredUnpublishTime(now - 1);
        postNotToBePublished2.setPublished(false);

        //already published -> not published
        postNotToBePublished3.setDesiredPublishTime(now - 2);
        postNotToBePublished3.setDesiredUnpublishTime(now + 1);
        postNotToBePublished3.setPublished(true);

        //publish time is over, unpublish time not over -> published
        newsItemToBePublished.setDesiredPublishTime(now);
        newsItemToBePublished.setDesiredUnpublishTime(now + 1);
        newsItemToBePublished.setPublished(false);

        //publish time is over, unpublish time not set -> published
        happeningToBePublished.setDesiredPublishTime(now - 1);
        happeningToBePublished.setDesiredUnpublishTime(null);
        happeningToBePublished.setPublished(false);

        th.postRepository.saveAll(Arrays.asList(postNotToBePublished1, postNotToBePublished2, postNotToBePublished3,
                newsItemToBePublished, happeningToBePublished));

        grapevinePostPublishWorker.run();

        waitForEventProcessing();

        ExternalPost newsItemToBePublishedUpdated = postService.findExternalPostById(newsItemToBePublished.getId());
        ExternalPost happeningToBePublishedUpdated = postService.findExternalPostById(happeningToBePublished.getId());

        assertFalse(postService.findExternalPostById(postNotToBePublished1.getId()).isPublished());
        assertFalse(postService.findExternalPostById(postNotToBePublished2.getId()).isPublished());
        assertTrue(postService.findExternalPostById(postNotToBePublished3.getId()).isPublished());
        assertTrue(newsItemToBePublishedUpdated.isPublished());
        assertTrue(happeningToBePublishedUpdated.isPublished());

        // verify push message
        waitForEventProcessing();

        ClientPostCreateConfirmation pushedEventNewsItem =
                testClientPushService.getPushedEventToGeoAreasLoud(newsItemToBePublishedUpdated.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);

        assertEquals(newsItemToBePublishedUpdated.getId(), pushedEventNewsItem.getPostId());
        assertEquals(newsItemToBePublishedUpdated.getId(), pushedEventNewsItem.getPost().getNewsItem().getId());

        ClientPostCreateConfirmation pushedEventHappening =
                testClientPushService.getPushedEventToGeoAreasLoud(happeningToBePublishedUpdated.getGeoAreas(),
                        ClientPostCreateConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);

        assertEquals(happeningToBePublishedUpdated.getId(), pushedEventHappening.getPostId());
        assertEquals(happeningToBePublishedUpdated.getId(), pushedEventHappening.getPost().getHappening().getId());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void unpublishDuePublishedPosts() throws Exception {

        final long now = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(now);

        ExternalPost postNotToBeUnpublished1 = th.unpublishedHappening1;
        ExternalPost postNotToBeUnpublished2 = th.newsItem1;
        ExternalPost postNotToBeUnpublished3 = th.newsItem2;
        ExternalPost newsItemToBeUnpublished = th.unpublishedNewsItem1;
        ExternalPost happeningToBeUnpublished = th.happening3;

        //unpublish time is not over -> not unpublished
        postNotToBeUnpublished1.setDesiredPublishTime(now - 2);
        postNotToBeUnpublished1.setDesiredUnpublishTime(now + 1);
        postNotToBeUnpublished1.setPublished(true);

        //already not published -> not unpublished
        postNotToBeUnpublished2.setDesiredPublishTime(now - 2);
        postNotToBeUnpublished2.setDesiredUnpublishTime(now - 1);
        postNotToBeUnpublished2.setPublished(false);

        //unpublish time not set -> not unpublished
        postNotToBeUnpublished3.setDesiredPublishTime(now - 2);
        postNotToBeUnpublished3.setDesiredUnpublishTime(null);
        postNotToBeUnpublished3.setPublished(true);

        //unpublish time is over -> unpublished
        newsItemToBeUnpublished.setDesiredPublishTime(now);
        newsItemToBeUnpublished.setDesiredUnpublishTime(now - 1);
        newsItemToBeUnpublished.setPublished(true);

        //unpublish time is over -> unpublished
        happeningToBeUnpublished.setDesiredPublishTime(now - 2);
        happeningToBeUnpublished.setDesiredUnpublishTime(now - 1);
        happeningToBeUnpublished.setPublished(true);

        th.postRepository.saveAll(
                Arrays.asList(postNotToBeUnpublished1, postNotToBeUnpublished2, postNotToBeUnpublished3,
                        newsItemToBeUnpublished, happeningToBeUnpublished));

        grapevinePostPublishWorker.run();

        waitForEventProcessing();

        ExternalPost newsItemToBeUnpublishedUpdated = postService.findExternalPostById(newsItemToBeUnpublished.getId());
        ExternalPost happeningToBeUnpublishedUpdated =
                postService.findExternalPostById(happeningToBeUnpublished.getId());

        assertTrue(postService.findExternalPostById(postNotToBeUnpublished1.getId()).isPublished());
        assertFalse(postService.findExternalPostById(postNotToBeUnpublished2.getId()).isPublished());
        assertTrue(postService.findExternalPostById(postNotToBeUnpublished3.getId()).isPublished());
        assertFalse(newsItemToBeUnpublishedUpdated.isPublished());
        assertFalse(happeningToBeUnpublishedUpdated.isPublished());

        // verify push message
        waitForEventProcessing();

        ClientPostDeleteConfirmation pushedEventNewsItem =
                testClientPushService.getPushedEventToGeoAreasSilent(newsItemToBeUnpublishedUpdated.getGeoAreas(),
                        ClientPostDeleteConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertEquals(newsItemToBeUnpublishedUpdated.getId(), pushedEventNewsItem.getPostId());

        ClientPostDeleteConfirmation pushedEventHappening =
                testClientPushService.getPushedEventToGeoAreasSilent(happeningToBeUnpublishedUpdated.getGeoAreas(),
                        ClientPostDeleteConfirmation.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);

        assertEquals(happeningToBeUnpublishedUpdated.getId(), pushedEventHappening.getPostId());

        testClientPushService.assertNoMorePushedEvents();
    }

}
