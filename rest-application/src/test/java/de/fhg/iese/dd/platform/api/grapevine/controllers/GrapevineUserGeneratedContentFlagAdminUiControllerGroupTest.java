/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientGroup;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientUserGeneratedContentFlagGroup;
import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers.UserGeneratedContentFlagAdminUiControllerTest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GrapevineUserGeneratedContentFlagAdminUiControllerGroupTest extends UserGeneratedContentFlagAdminUiControllerTest {

    @Autowired
    private GroupTestHelper th;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createUserGeneratedContentAdmins();
        th.createGroupsAndGroupPostsWithComments();
        super.createFlags();
    }

    @Override
    protected BaseSharedTestHelper getTestHelper() {
        return th;
    }

    @Override
    protected Group getFlagEntityFlag1Tenant1() {
        return th.groupSchachvereinAAA;
    }

    @Override
    protected Person getFlagEntityFlag2Tenant1() {
        return th.personRegularAnna;
    }

    @Override
    protected Collection<String> getSingleFlagUrlTemplates() {
        return List.of("/grapevine/adminui/flag/group/{flagId}");
    }

    @Override
    public void getFlagById_GlobalAdmin() throws Exception {

        getFlagByIdGroup_GlobalAdmin();
    }

    private void getFlagByIdGroup_GlobalAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag1Tenant1;
        Group flaggedGroup = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedGroup.getId(), expectedFlag.getEntityId());
        assertThat(expectedFlag.getUserGeneratedContentFlagStatusRecords().size()).isGreaterThan(1);

        assertFlagEquals(flaggedGroup, expectedFlag,
                toObject(mockMvc.perform(
                                        get("/grapevine/adminui/flag/group/{flagId}", expectedFlag.getId())
                                                .contentType(contentType)
                                                .headers(authHeadersFor(th.personGlobalUserGeneratedContentAdmin)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagGroup.class));
    }

    @Override
    public void getFlagById_TenantAdmin() throws Exception {

        getFlagByIdGroup_TenantAdmin();
    }

    private void getFlagByIdGroup_TenantAdmin() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag1Tenant1;
        Group flaggedGroup = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedGroup.getId(), expectedFlag.getEntityId());

        assertFlagEquals(flaggedGroup, expectedFlag,
                toObject(mockMvc.perform(
                                        get("/grapevine/adminui/flag/group/{flagId}", expectedFlag.getId())
                                                .contentType(contentType)
                                                .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                                .andExpect(status().isOk())
                                .andReturn(),
                        ClientUserGeneratedContentFlagGroup.class));
    }

    @Test
    public void getFlagByIdGroup_WrongType() throws Exception {

        // person is not a group
        UserGeneratedContentFlag expectedFlag;
        expectedFlag = flag2Tenant1;
        Person flaggedPerson = getFlagEntityFlag2Tenant1();
        assertEquals(flaggedPerson.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/group/{flagId}", expectedFlag.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void getFlagByIdGroup_InvalidType() throws Exception {

        UserGeneratedContentFlag expectedFlag = flag1Tenant1;
        expectedFlag.setEntityType("de.fhg.iese.dd.🐛");
        flagRepository.save(expectedFlag);
        Group flaggedGroup = getFlagEntityFlag1Tenant1();
        assertEquals(flaggedGroup.getId(), expectedFlag.getEntityId());

        mockMvc.perform(get("/grapevine/adminui/flag/group/{flagId}", expectedFlag.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));

        expectedFlag.setEntityType("");
        flagRepository.save(expectedFlag);

        mockMvc.perform(get("/grapevine/adminui/flag/group/{flagId}", expectedFlag.getId())
                        .contentType(contentType)
                        .headers(authHeadersFor(th.personUserGeneratedContentAdminTenant1)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_HAS_DIFFERENT_TYPE));
    }

    @Test
    public void deleteFlagEntity_Group() throws Exception {

        UserGeneratedContentFlag flag = flag1Tenant1;
        Group flaggedEntity = getFlagEntityFlag1Tenant1();
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        Group changedGroup = th.groupRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedGroup.isDeleted()).as("Group should be deleted afterwards").isTrue();
        assertEquals(timeServiceMock.currentTimeMillisUTC(), changedGroup.getDeletionTime());
    }

    @Test
    public void deleteFlagEntity_DeletedGroup() throws Exception {

        UserGeneratedContentFlag flag = flag1Tenant1;
        Group flaggedEntity = getFlagEntityFlag1Tenant1();
        flaggedEntity.setDeleted(true);
        th.groupRepository.save(flaggedEntity);
        assertThat(flag.getEntityId()).isEqualTo(flaggedEntity.getId());

        deleteFlaggedEntity(flag);

        Group changedGroup = th.groupRepository.findById(flaggedEntity.getId()).get();
        assertThat(changedGroup.isDeleted()).as("Group should still be deleted afterwards").isTrue();
    }

    protected void assertFlagEquals(Group flaggedGroup, UserGeneratedContentFlag expectedFlag,
                                    ClientUserGeneratedContentFlagGroup actualFlag) {

        assertGroupEquals(flaggedGroup, actualFlag.getFlagGroup());

        super.assertFlagEquals(expectedFlag, actualFlag);
    }

    private void assertGroupEquals(Group expectedGroup, ClientGroup actualGroup) {
        //not tested in detail, just to ensure that the right entity is used
        assertEquals(expectedGroup.getId(), actualGroup.getId());
        assertEquals(expectedGroup.isDeleted(), actualGroup.isDeleted());
        //we also want the text, even if it is deleted
        assertEquals(expectedGroup.getName(), actualGroup.getName());
        assertEquals(expectedGroup.getDescription(), actualGroup.getDescription());
    }

}
