/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientPostDeleteRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public abstract class BaseAbstractPostEventControllerTest<T extends Post> extends BasePostControllerTest {

    protected final int maxNumberCharsPerPost = 280;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
        createPostTypeFeatures();
    }

    private void createPostTypeFeatures() {
        // DD-10993 Replaced max number of chars in config by additional attribute in feature
        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(GossipPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Plausch\", \"itemName\": \"Plausch\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(SpecialPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Dorfhelfer\", \"itemName\": \"Dorfhelfer\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + ", \"additionalCustomAttributes\": {\"specialPostType\": \"" + th.specialPostTypeFeatureValue +
                                "\"} }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(OfferPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Biete\", \"itemName\": \"Gebot\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(SeekingPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Suche\", \"itemName\": \"Gesuch\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(SuggestionPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Sag's uns\", \"itemName\": \"Meldung\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(NewsItemPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"News\", \"itemName\": \"News\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(HappeningPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Events\", \"itemName\": \"Event\", \"maxNumberCharsPerPost\": " +
                                maxNumberCharsPerPost + " }")
                .build()
                .withConstantId());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    protected abstract T getTestPost1();

    protected abstract Person getTestPost1NotOwner();

    protected abstract T getTestPost2();

    protected abstract Post getTestPostDifferentType();

    protected void verifyTestData() throws Exception {
        assertNotEquals(getTestPost1(), getTestPost2());
        assertNotEquals(getTestPost1().getPostType(), getTestPostDifferentType().getPostType());
        assertNotEquals(getTestPost2().getPostType(), getTestPostDifferentType().getPostType());
    }

    @Test
    public void postDeleteRequest_Unauthorized() throws Exception {
        Post postToBeDeleted = getTestPost2();

        ClientPostDeleteRequest postDeleteRequest = ClientPostDeleteRequest.builder()
                .postId(postToBeDeleted.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                .contentType(contentType)
                .content(json(postDeleteRequest)))
                .andExpect(isUnauthorized());

        assertPostIsNotDeleted(postToBeDeleted);

        assertOAuth2(post("/grapevine/event/postDeleteRequest")
                .contentType(contentType)
                .content(json(postDeleteRequest)));

        assertPostIsNotDeleted(postToBeDeleted);

        assertOAuth2(post("/grapevine/event/postDeleteRequest")
                .contentType(contentType)
                .content(json(ClientPostDeleteRequest.builder()
                        .postId("unknown id")
                        .build())));
    }

    @Test
    public void postDeleteRequest_NotOwner() throws Exception {

        Post postToBeDeleted = getTestPost1();
        Person caller = getTestPost1NotOwner();
        assertThat(postToBeDeleted.getCreator()).isNotEqualTo(caller);

        ClientPostDeleteRequest postDeleteRequest = ClientPostDeleteRequest.builder()
                .postId(postToBeDeleted.getId())
                .build();

        mockMvc.perform(post("/grapevine/event/postDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(postDeleteRequest)))
                .andExpect(isException(ClientExceptionType.POST_ACTION_NOT_ALLOWED));

        assertPostIsNotDeleted(postToBeDeleted);
    }

    protected void assertPostIsNotDeleted(Post postToBeDeleted) {
        Post postNotDeleted = th.postRepository.findById(postToBeDeleted.getId()).get();
        assertThat(postNotDeleted.isDeleted()).isFalse();
    }

    protected String getTypeName(Post post) {
        return StringUtils.uncapitalize(post.getPostType().name());
    }

}
