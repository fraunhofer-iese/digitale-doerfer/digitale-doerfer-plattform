/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.business.grapevine.services.PagedQuery;
import de.fhg.iese.dd.platform.business.grapevine.services.PostSortCriteria;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GroupListMembersFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Group and content visibility tests:
 * <ul>
 * <li>test all valid combinations from the table in https://ras.iese.de/confluence/x/eQDdAQ</li>
 * <li>Requests: Group(description), list of all gossips of a group, content of gossip (.../post/&lt;id&gt;),
 * list of all comments of group gossip(.../&lt;id&gt;/comments), content of comments of group
 * gossips (.../comment/&lt;id&gt;)</li>
 * </ul>
 */
public class GroupControllerVisibilityTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        th.printAvailableGroups();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void listGroupsAndContent_notGroupMember_homeInDorf1() throws Exception {

        //home: geoAreaDorf1InEisenberg
        //selected: geoAreaDorf1InEisenberg, geoAreaEisenberg
        Person caller = th.personEckhardTenant1Dorf1NoMember;

        th.printGroupSettings(caller);

        assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                th.groupAdfcAAP, //geoAreaEisenberg
                th.groupSchachvereinAAA, //geoAreaDorf1InEisenberg
                th.groupWaffenfreundeAMA, //geoAreaDorf1InEisenberg
                th.groupGlasfaserSSP, //geoAreaDorf1InEisenberg
                th.groupDorfgeschichteSSA); //geoAreaDorf1InEisenberg
        assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                th.groupMuellAAP); //geoAreaMZ

        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertContentIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB);
        assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller,
                th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //members only
        assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //same home geo area
        assertContentIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //same home geo area

        assertGroupIsAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantMZ); //same home geo area
        //assertContentIsAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content

        assertGroupIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB); //different home area
        assertGroupIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ); //different home area
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //members only
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //members only

        assertGroupIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB); //members only
        assertGroupIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //members only
        assertContentIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted

        assertGroupIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB);//geo area not in selected
        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected
        assertContentIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected

        assertGroupIsNotAccessible(th.groupKatzenAAP, caller,
                th.appVariantKL_EB); //available geo areas of app variant in excluded of group
        assertGroupIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //available geo areas of app variant in excluded of group
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
    }

    @Test
    public void listGroupsAndContent_notGroupMember_homeInDorf2() throws Exception {

        //home: geoAreaDorf2InEisenberg
        //selected: geoAreaDorf2InEisenberg, geoAreaEisenberg
        Person caller = th.personFranziTenant1Dorf2NoMember;

        th.printGroupSettings(caller);

        assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                th.groupAdfcAAP, //geoAreaEisenberg
                th.groupMessdienerSMA); //geoAreaDorf2InEisenberg
        assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                th.groupMuellAAP); //geoAreaMZ

        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertContentIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB); //other geo area
        assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area
        assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB); //other geo area
        assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //members only
        assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB); //other geo area
        assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //other geo area
        assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area
        assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area

        assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB); //other geo area
        assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantMZ); //other geo area
        //assertContentIsAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content

        assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB); //same home area
        assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ); //same home area
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //members only
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //members only

        assertGroupIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB); //members only
        assertGroupIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //members only
        assertContentIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //members only
        assertContentIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //members only

        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted

        assertGroupIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //geo area not in selected
        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected
        assertContentIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected

        assertGroupIsNotAccessible(th.groupKatzenAAP, caller,
                th.appVariantKL_EB); //available geo areas of app variant in excluded of group
        assertGroupIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //available geo areas of app variant in excluded of group
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
    }

    @Test
    public void listGroupsAndContent_otherGroupMemberStatus() throws Exception {

        //home: geoAreaDorf2InEisenberg
        //selected: geoAreaDorf2InEisenberg, geoAreaEisenberg
        final Person caller = th.personFranziTenant1Dorf2NoMember;
        final Group restrictedGroup = th.groupErfahrungMMA;

        th.iterateOverNonMemberGroupMembershipStatus(caller, restrictedGroup, membershipStatus -> {
            th.printGroupSettings(caller);

            switch (membershipStatus) {
                case PENDING:
                    assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                            th.groupAdfcAAP, //geoAreaEisenberg
                            th.groupMessdienerSMA, //geoAreaDorf2InEisenberg
                            restrictedGroup);
                    assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                            restrictedGroup, // geoAreaMainz
                            th.groupMuellAAP);
                    assertGroupIsAccessible(restrictedGroup, caller, th.appVariantKL_EB);
                    assertGroupIsAccessible(restrictedGroup, caller, th.appVariantMZ); //pending member
                    assertContentIsNotAccessible(restrictedGroup, caller, th.appVariantKL_EB,
                            ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE);
                    assertContentIsNotAccessible(restrictedGroup, caller, th.appVariantMZ,
                            ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE);
                    break;
                case APPROVED:
                    assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                            th.groupAdfcAAP, //geoAreaEisenberg
                            th.groupMessdienerSMA, //geoAreaDorf2InEisenberg
                            restrictedGroup);
                    assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                            restrictedGroup, // geoAreaMainz
                            th.groupMuellAAP);
                    assertGroupIsAccessible(restrictedGroup, caller, th.appVariantKL_EB);
                    assertGroupIsAccessible(restrictedGroup, caller, th.appVariantMZ);
                    assertContentIsAccessible(restrictedGroup, caller, th.appVariantKL_EB);
                    assertContentIsAccessible(restrictedGroup, caller, th.appVariantMZ);
                    break;
                case REJECTED:
                case NOT_MEMBER:
                    assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                            th.groupAdfcAAP, //geoAreaEisenberg
                            th.groupMessdienerSMA);//geoAreaDorf2InEisenberg
                    assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                            th.groupMuellAAP); // geoAreaMainz
                    assertGroupIsNotAccessible(restrictedGroup, caller, th.appVariantKL_EB);
                    assertContentIsNotAccessible(restrictedGroup, caller, th.appVariantKL_EB,
                            ClientExceptionType.GROUP_NOT_FOUND);
                    assertContentIsNotAccessible(restrictedGroup, caller, th.appVariantMZ,
                            ClientExceptionType.GROUP_NOT_FOUND);
                    break;
            }

            //the rest should be the same as in listGroupsAndContent_notGroupMember_homeInDorf2
            assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
            assertGroupIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //geo area not in selected
            assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
            assertContentIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

            assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB); //other geo area
            assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //geo area not in selected
            assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //other geo area
            assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

            assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB); //other geo area
            assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //geo area not in selected
            assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //members only
            assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

            assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB); //other geo area
            assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //other geo area
            assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //other geo area
            assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //other geo area

            assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB); //other geo area
            assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantMZ); //other geo area
            //assertContentIsAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content

            assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB); //same home area
            assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ); //same home area
            assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //members only
            assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //members only

            assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
            assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ); //deleted
            assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //deleted
            assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //deleted

            assertGroupIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //geo area not in selected
            assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected
            assertContentIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
            assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected

            assertGroupIsNotAccessible(th.groupKatzenAAP, caller,
                    th.appVariantKL_EB); //available geo areas of app variant in excluded of group
            assertGroupIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //geo area not in selected
            assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB,
                    ClientExceptionType.GROUP_NOT_FOUND); //available geo areas of app variant in excluded of group
            assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ,
                    ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
        });
    }

    @Test
    public void listGroupsAndContent_groupMember_homeInDorf1() throws Exception {

        //home: geoAreaDorf1InEisenberg
        //selected: geoAreaDorf1InEisenberg, geoAreaEisenberg
        //member: groupVgMMA, groupWaffenfreundeAMA, groupAdfcAAP
        Person caller = th.personAnjaTenant1Dorf1;

        th.printGroupSettings(caller);

        assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                th.groupAdfcAAP, //geoAreaEisenberg
                th.groupSchachvereinAAA, //geoAreaDorf1InEisenberg
                th.groupWaffenfreundeAMA, //geoAreaDorf1InEisenberg
                th.groupGlasfaserSSP, //geoAreaDorf1InEisenberg
                th.groupDorfgeschichteSSA, //geoAreaDorf1InEisenberg
                th.groupErfahrungMMA); //member

        assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                th.groupAdfcAAP, //member
                th.groupWaffenfreundeAMA, //member
                th.groupErfahrungMMA, //member
                th.groupMuellAAP); //geoAreaMZ

        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //member

        assertGroupIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB);
        assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected

        assertGroupIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //member

        assertGroupIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //same home geo area
        assertContentIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //same home geo area

        assertGroupIsAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantMZ); //same home geo area
        //assertContentIsAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content

        assertGroupIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB);
        assertGroupIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ); //different home geo area
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //members only
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //appvariant for other tenant

        assertGroupIsAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //member

        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted

        assertGroupIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //geo area not selected
        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area selected
        assertContentIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not selected
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area selected

        assertGroupIsNotAccessible(th.groupKatzenAAP, caller,
                th.appVariantKL_EB); //available geo areas of app variant in excluded of group
        assertGroupIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //available geo areas of app variant in excluded of group
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
    }

    @Test
    public void listGroupsAndContent_groupMember_homeInDorf2() throws Exception {

        //home: geoAreaDorf2InEisenberg
        //selected: geoAreaDorf2InEisenberg, geoAreaEisenberg
        //member: groupVgMMA, groupWaffenfreundeAMA, groupSchachvereinAAA, groupAdfcAAP
        Person caller = th.personBenTenant1Dorf2;

        th.printGroupSettings(caller);

        assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                th.groupAdfcAAP, //geoAreaEisenberg
                th.groupSchachvereinAAA, //geoAreaDorf1InEisenberg, member
                th.groupWaffenfreundeAMA, //geoAreaDorf1InEisenberg, member
                th.groupMessdienerSMA, //geoAreaDorf2InEisenberg
                th.groupErfahrungMMA); //member
        assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                th.groupAdfcAAP, //member
                th.groupSchachvereinAAA, //member
                th.groupWaffenfreundeAMA, //member
                th.groupErfahrungMMA, //member
                th.groupMuellAAP); //geoAreaMZ

        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupAdfcAAP, caller, th.appVariantMZ); //member

        assertGroupIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB); //member
        assertGroupIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB); //member
        assertContentIsAccessible(th.groupSchachvereinAAA, caller, th.appVariantMZ); //member

        assertGroupIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB); //member
        assertContentIsAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantMZ); //member

        assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB); //other home area
        assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ); //other home area
        assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other home area
        assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //other home area

        assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB); //other geo area
        //assertContentIsAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content

        assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ); //same home area
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //not a member
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_CONTENT_NOT_ACCESSIBLE); //not a member

        assertGroupIsAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB);
        assertGroupIsAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB);
        assertContentIsAccessible(th.groupErfahrungMMA, caller, th.appVariantMZ); //member

        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted

        assertGroupIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //other geo area
        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected
        assertContentIsNotAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ); //geo area in selected

        assertGroupIsNotAccessible(th.groupKatzenAAP, caller,
                th.appVariantKL_EB); //available geo areas of app variant in excluded of group
        assertGroupIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //geo area not in selected
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //available geo areas of app variant in excluded of group
        assertContentIsNotAccessible(th.groupKatzenAAP, caller, th.appVariantMZ,
                ClientExceptionType.GROUP_NOT_FOUND); //geo area not in selected
    }

    @Test
    public void listGroupsAndContent_otherTenant() throws Exception {

        Person caller = th.personDagmarTenant2Mainz;

        th.printGroupSettings(caller);

        assertCorrectGroupsAreListed(caller, th.appVariantKL_EB,
                th.groupMuellAAP, //member
                th.groupKatzenAAP); //member
        assertCorrectGroupsAreListed(caller, th.appVariantMZ,
                th.groupMuellAAP, //member
                th.groupKatzenAAP); //member

        assertGroupIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupAdfcAAP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupSchachvereinAAA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupWaffenfreundeAMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupGlasfaserSSP, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupDorfgeschichteSSA, caller, th.appVariantKL_EB); //other geo area and tenant
        //assertContentIsNotAccessible(th.groupDorfgeschichteSSA, caller); //this group has no content
        assertGroupIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupMessdienerSMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB); //other geo area and tenant
        assertContentIsNotAccessible(th.groupErfahrungMMA, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //other geo area and tenant
        assertGroupIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB); //deleted
        assertContentIsNotAccessible(th.groupDeleted, caller, th.appVariantKL_EB,
                ClientExceptionType.GROUP_NOT_FOUND); //deleted

        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //member
        assertGroupIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ);
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantKL_EB); //member
        assertContentIsAccessible(th.groupMuellAAP, caller, th.appVariantMZ);

        assertGroupIsAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB); //member
        assertGroupIsAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //member
        assertContentIsAccessible(th.groupKatzenAAP, caller, th.appVariantKL_EB); //member
        assertContentIsAccessible(th.groupKatzenAAP, caller, th.appVariantMZ); //member
    }

    @Test
    public void listGroupsAndContent_excludedGeoArea() throws Exception {

        Person caller = th.personDieterTenant2MainzNoMember;
        final Group group = th.groupKatzenAAP;
        final AppVariant appVariant = th.appVariantKL_EB;

        //excluded because also Eisenberg is available in the geo areas of the app variant
        th.printGroupSettings(caller);
        th.printAvailableGeoAreas(appVariant);

        assertCorrectGroupsAreListed(caller, appVariant);
        assertGroupIsNotAccessible(group, caller, appVariant);
        assertContentIsNotAccessible(group, caller, appVariant, ClientExceptionType.GROUP_NOT_FOUND);

        //included because Eisenberg is not available in the app variant anymore
        th.unmapAllGeoAreasFromAppVariant(appVariant);
        th.mapGeoAreaToAppVariant(appVariant, th.geoAreaKaiserslautern, th.tenant1);
        appService.invalidateCache();

        th.printGroupSettings(caller);
        th.printAvailableGeoAreas(appVariant);

        assertCorrectGroupsAreListed(caller, appVariant, group);
        assertGroupIsAccessible(group, caller, appVariant);
        assertContentIsAccessible(group, caller, appVariant);

        //excluded because Eisenberg is available in the app variant since it is under RLP
        th.unmapAllGeoAreasFromAppVariant(appVariant);
        th.mapGeoAreaToAppVariant(appVariant, th.geoAreaRheinlandPfalz, th.tenant1);
        appService.invalidateCache();

        th.printGroupSettings(caller);
        th.printAvailableGeoAreas(appVariant);

        assertCorrectGroupsAreListed(caller, appVariant);
        assertGroupIsNotAccessible(group, caller, appVariant);
        assertContentIsNotAccessible(group, caller, appVariant, ClientExceptionType.GROUP_NOT_FOUND);

        //included because Eisenberg is explicitly excluded in the app variant
        th.excludeGeoAreaFromAppVariant(appVariant, th.geoAreaEisenberg, th.tenant1);
        appService.invalidateCache();

        th.printGroupSettings(caller);
        th.printAvailableGeoAreas(appVariant);

        assertCorrectGroupsAreListed(caller, appVariant, group);
        assertGroupIsAccessible(group, caller, appVariant);
        assertContentIsAccessible(group, caller, appVariant);
    }

    @Test
    public void listGroupMembers() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupAdfcAAP;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        final List<Person> expectedGroupMembers =
                th.groupMembershipRepository.findAllMembersByGroupAndMembershipStatus(group,
                        GroupMembershipStatus.APPROVED).stream()
                        .sorted(Comparator.comparing(Person::getFirstName)
                                .thenComparing(Person::getLastName))
                        .collect(Collectors.toList());

        assertEquals(3, expectedGroupMembers.size());
        int pageSize = 2;

        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), 0, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(expectedGroupMembers.get(0).getId()))
                .andExpect(jsonPath("$.content[1].id").value(expectedGroupMembers.get(1).getId()))
                .andExpect(hasCorrectPageData(0, pageSize, expectedGroupMembers.size()));

        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), 1, pageSize))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id").value(expectedGroupMembers.get(2).getId()))
                .andExpect(hasCorrectPageData(1, pageSize, expectedGroupMembers.size()));
    }

    @Test
    public void listGroupMembers_NotAuthorized() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupErfahrungMMA;

        assertOAuth2AppVariantRequired(
                buildGetAllMembersByGroupId(null, null, group.getId(), 0, 10),
                appVariant, caller);
    }

    @Test
    public void listGroupMembers_PageParametersInvalid() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupErfahrungMMA;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        // only page negative
        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), -1, 10))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // only count negative
        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), 0, -1))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));

        // both parameters negative
        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), -1, -1))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.PAGE_PARAMETER_INVALID));
    }

    @Test
    public void listGroupMembers_GroupNotVisible() throws Exception {

        final Person caller = th.personEckhardTenant1Dorf1NoMember;
        final AppVariant appVariant = th.appVariantKL_EB;
        //this group is not visible to the caller, since it is in MZ, which is not in the geo areas of the app variant
        final Group group = th.groupMuellAAP;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), 0, 10))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void listGroupMembers_GroupNotFound() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, "invalidGroupId", 0, 10))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void listGroupMembers_FeatureNotEnabledForTenant() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupErfahrungMMA;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(false)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllMembersByGroupId(caller, appVariant, group.getId(), 0, 10))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    @Test
    public void listGroupAdmins() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupAdfcAAP;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        final List<Person> expectedGroupAdmins =
                th.roleAssignmentRepository.findByRoleAndRelatedEntityIdOrderByCreatedDesc(
                        GroupMembershipAdmin.class, group.getId()).stream()
                        .sorted(Comparator.comparing(Person::getFirstName)
                                .thenComparing(Person::getLastName))
                        .collect(Collectors.toList());

        assertEquals(2, expectedGroupAdmins.size());

        mockMvc.perform(buildGetAllAdminsByGroupId(caller, appVariant, group.getId()))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(expectedGroupAdmins.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedGroupAdmins.get(1).getId()))
                .andExpect(jsonPath("$", hasSize(expectedGroupAdmins.size())));
    }

    @Test
    public void listGroupAdmins_NotAuthorized() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupErfahrungMMA;

        assertOAuth2AppVariantRequired(
                buildGetAllAdminsByGroupId(null, null, group.getId()), appVariant, caller);
    }

    @Test
    public void listGroupAdmins_GroupNotVisible() throws Exception {

        final Person caller = th.personEckhardTenant1Dorf1NoMember;
        final AppVariant appVariant = th.appVariantKL_EB;
        //this group is not visible to the caller, since it is in MZ, which is not in the geo areas of the app variant
        final Group group = th.groupMuellAAP;
        assertThat(th.groupToGeoAreaMapping.get(group).getIncludedGeoAreas())
                .doesNotContain(th.geoAreaKaiserslautern, th.geoAreaEisenberg);

        th.printAvailableGeoAreas(appVariant);

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllAdminsByGroupId(caller, appVariant, group.getId()))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void listGroupAdmins_GroupNotFound() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(true)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllAdminsByGroupId(caller, appVariant, "invalidGroupId"))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void listGroupAdmins_FeatureNotEnabledForTenant() throws Exception {

        final Person caller = th.personAnjaTenant1Dorf1;
        final AppVariant appVariant = th.appVariantKL_EB;
        final Group group = th.groupErfahrungMMA;

        final FeatureConfig featureConfigTenant = FeatureConfig.builder()
                .featureClass(GroupListMembersFeature.class.getName())
                .enabled(false)
                .tenant(th.tenant1)
                .build();
        featureConfigRepository.saveAndFlush(featureConfigTenant);

        mockMvc.perform(buildGetAllAdminsByGroupId(caller, appVariant, group.getId()))
                .andExpect(content().contentType(contentType))
                .andExpect(isException(ClientExceptionType.FEATURE_NOT_ENABLED));
    }

    private void assertCorrectGroupsAreListed(Person caller, AppVariant appVariant, Group... expectedGroups)
            throws Exception {

        final List<Group> groups = Arrays.asList(expectedGroups);

        mockMvc.perform(get("/grapevine/group")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(groups.stream()
                        .map(group -> th.toClientGroup(group, caller))
                        .collect(Collectors.toList())));

        th.setGroupAccessibilityApprovalRequiredFeatureEnabled(appVariant, false);

        mockMvc.perform(get("/grapevine/group")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals("Group Accessibility Approval Required Feature not considered",
                        groups.stream()
                                .filter(group -> (group.getAccessibility() == GroupAccessibility.PUBLIC ||
                                        th.groupMembershipRepository.findByGroupAndMember(group, caller)
                                                .filter(gm -> gm.getStatus() == GroupMembershipStatus.APPROVED ||
                                                        gm.getStatus() == GroupMembershipStatus.PENDING).isPresent()))
                                .map(group -> th.toClientGroup(group, caller))
                                .collect(Collectors.toList())));

        th.setGroupAccessibilityApprovalRequiredFeatureEnabled(appVariant, true);
    }

    private void assertGroupIsAccessible(Group group, Person caller, AppVariant appVariant) throws Exception {

        mockMvc.perform(get("/grapevine/group/{groupId}", group.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(th.toClientGroup(group, caller)));
    }

    private void assertGroupIsNotAccessible(Group group, Person caller, AppVariant appVariant) throws Exception {

        mockMvc.perform(get("/grapevine/group/{groupId}", group.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    private void assertContentIsAccessible(Group group, Person caller, AppVariant appVariant) throws Exception {

        final List<Pair<Gossip, List<Comment>>> postsWithComments = th.groupsToPostsAndComments.get(group);

        //check list of posts via specific endpoint
        mockMvc.perform(get("/grapevine/group/{groupId}/post", group.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(jsonEquals(new PageImpl<>(postsWithComments
                        .stream()
                        .map(Pair::getLeft)
                        .filter(Gossip::isNotDeleted)
                        .sorted(Comparator.comparingLong(Gossip::getCreated).reversed())
                        .map(th::toCensoredClientPost)
                        .collect(Collectors.toList()),
                        PagedQuery.builder()
                                .elementsPerPage(10)
                                .pageNumber(0)
                                .sortCriteria(PostSortCriteria.CREATED)
                                .build().toPageRequest(),
                        0)));

        //check every single post and comment via generic endpoints
        for (Pair<Gossip, List<Comment>> postWithComments : postsWithComments) {

            if (postWithComments.getLeft().isDeleted()) {
                continue;
            }

            //check single post
            mockMvc.perform(get("/grapevine/post/{postId}", postWithComments.getLeft().getId())
                    .headers(authHeadersFor(caller, appVariant)))
                    .andExpect(status().isOk())
                    .andExpect(jsonEquals(th.toCensoredClientPost(postWithComments.getLeft())));
            //check list of comments
            mockMvc.perform(get("/grapevine/comment/")
                    .headers(authHeadersFor(caller, appVariant))
                    .param("postId", postWithComments.getLeft().getId()))
                    .andExpect(status().isOk())
                    .andExpect(jsonEquals(postWithComments.getRight()
                            .stream()
                            .sorted(Comparator.comparingLong(Comment::getCreated))
                            .map(th::toCensoredClientComment)
                            .collect(Collectors.toList())));
            //check every single comment
            for (Comment comment : postWithComments.getRight()) {
                mockMvc.perform(get("/grapevine/comment/{commentId}", comment.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                        .andExpect(status().isOk())
                        .andExpect(jsonEquals(th.toCensoredClientComment(comment)));
            }
        }
    }

    private void assertContentIsNotAccessible(Group group, Person caller, AppVariant appVariant, ClientExceptionType exceptionType)
            throws Exception {

        final List<Pair<Gossip, List<Comment>>> postsWithComments = th.groupsToPostsAndComments.get(group);

        //check list of posts via specific endpoint
        mockMvc.perform(get("/grapevine/group/{groupId}/post", group.getId())
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(isException(exceptionType));

        //check every single post and comment via generic endpoints
        for (Pair<Gossip, List<Comment>> postWithComments : postsWithComments) {
            mockMvc.perform(get("/grapevine/post/{postId}", postWithComments.getLeft().getId())
                    .headers(authHeadersFor(caller, appVariant)))
                    .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
            //check list of comments
            mockMvc.perform(get("/grapevine/comment/")
                    .headers(authHeadersFor(caller, appVariant))
                    .param("postId", postWithComments.getLeft().getId()))
                    .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

            //currently there is no check for access rights of single comments
            /*
            for (Comment comment : postWithComments.getRight()) {
                mockMvc.perform(get("/grapevine/comment/{commentId}", comment.getId())
                        .headers(authHeadersFor(caller, appVariant)))
                        .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));
            }
            */
        }
    }

    private MockHttpServletRequestBuilder buildGetAllMembersByGroupId(Person caller, AppVariant appVariant,
            String groupId, int page, int count) {
        MockHttpServletRequestBuilder builder = get("/grapevine/group/{groupId}/members", groupId)
                .param("page", String.valueOf(page))
                .param("count", String.valueOf(count));
        if (caller != null && appVariant != null) {
            builder = builder.headers(authHeadersFor(caller, appVariant));
        } else if (caller != null) {
            builder = builder.headers(authHeadersFor(caller));
        }
        return builder;
    }

    private MockHttpServletRequestBuilder buildGetAllAdminsByGroupId(Person caller, AppVariant appVariant,
            String groupId) {
        MockHttpServletRequestBuilder builder = get("/grapevine/group/{groupId}/admins", groupId);
        if (caller != null && appVariant != null) {
            builder = builder.headers(authHeadersFor(caller, appVariant));
        } else if (caller != null) {
            builder = builder.headers(authHeadersFor(caller));
        }
        return builder;
    }

}
