/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientComment;
import de.fhg.iese.dd.platform.business.grapevine.services.ICommentService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.List;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GrapevineClientEntitiesFilterTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Autowired
    private ICommentService commentService;

    @Autowired
    private IUserGeneratedContentFlagService flagService;

    private App app;
    private AppVariant appVariant;
    private Gossip gossipWithComments;
    private Person blockedPerson;
    private Comment comment1OfBlockedPerson_notFlagged;
    private Comment comment2OfBlockedPerson_flagged;
    private Comment commentOfGossipCreator;
    private Comment commentOfNonBlockedPerson_flagged;
    private Comment commentDeleted;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();

        appVariant = th.appVariant1Tenant1;
        app = appVariant.getApp();
        gossipWithComments = th.gossip2WithComments;
        comment1OfBlockedPerson_notFlagged = th.gossip2Comment1;
        comment2OfBlockedPerson_flagged = th.gossip2Comment4withImages;
        commentOfGossipCreator = th.gossip2Comment3;
        commentDeleted = th.gossip2Comment2Deleted;
        blockedPerson = comment1OfBlockedPerson_notFlagged.getCreator();

        commentOfNonBlockedPerson_flagged = Comment.builder()
                .post(gossipWithComments)
                .creator(th.personMonikaHess)
                .text("noch ein Kommentar")
                .build();
        commentOfNonBlockedPerson_flagged.setCreated(th.nextPostTimeStamp());
        commentService.store(commentOfNonBlockedPerson_flagged);

        personBlockingService.blockPerson(app, gossipWithComments.getCreator(), blockedPerson);
        flagService.create(comment2OfBlockedPerson_flagged, comment2OfBlockedPerson_flagged.getCreator(),
                th.tenant1, gossipWithComments.getCreator(), "geflaggt");
        flagService.create(commentOfNonBlockedPerson_flagged, commentOfNonBlockedPerson_flagged.getCreator(),
                th.tenant1, gossipWithComments.getCreator(), "geflaggt");

        assertThat(blockedPerson)
                .isNotEqualTo(gossipWithComments.getCreator());
        assertThat(commentOfNonBlockedPerson_flagged.getCreator())
                .isNotEqualTo(gossipWithComments.getCreator());
        assertThat(comment1OfBlockedPerson_notFlagged.getCreator())
                .isEqualTo(comment2OfBlockedPerson_flagged.getCreator());
        assertThat(commentOfGossipCreator.getCreator())
                .isEqualTo(gossipWithComments.getCreator());
        assertThat(personBlockingService.getBlockedPersons(app, gossipWithComments.getCreator()))
                .contains(blockedPerson);
        assertThat(personBlockingService.getBlockedPersons(app, gossipWithComments.getCreator()))
                .doesNotContain(commentOfNonBlockedPerson_flagged.getCreator());
        assertTrue(flagService.existsForPersonAndEntity(gossipWithComments.getCreator(),
                comment2OfBlockedPerson_flagged.getId(), comment2OfBlockedPerson_flagged.getClass().getName()));
        assertTrue(flagService.existsForPersonAndEntity(gossipWithComments.getCreator(),
                commentOfNonBlockedPerson_flagged.getId(), commentOfNonBlockedPerson_flagged.getClass().getName()));

    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void testFiltering() throws Exception {
        final MockHttpServletResponse response = mockMvc.perform(get("/grapevine/comment/")
                .headers(authHeadersFor(gossipWithComments.getCreator(), appVariant))
                .param("postId", gossipWithComments.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(5)))
                .andReturn()
                .getResponse();
        final List<ClientComment> returnedComments = toListOfType(response, ClientComment.class);

        final ClientComment clientCommentOfBlockedPerson_notFlagged = returnedComments.stream()
                .filter(c -> c.getId().equals(comment1OfBlockedPerson_notFlagged.getId()))
                .findAny()
                .get();
        assertThat(clientCommentOfBlockedPerson_notFlagged.getFilterStatus())
                .isEqualTo(ClientFilterStatus.BLOCKED);
        assertThat(clientCommentOfBlockedPerson_notFlagged.getCreator().getId())
                .isEqualTo(comment1OfBlockedPerson_notFlagged.getCreator().getId());
        assertThat(clientCommentOfBlockedPerson_notFlagged.getCreator().getFilterStatus())
                .isEqualTo(ClientFilterStatus.BLOCKED);
        assertThat(clientCommentOfBlockedPerson_notFlagged.getCreator().getProfilePicture())
                .isNull();
        assertThat(clientCommentOfBlockedPerson_notFlagged.getCreator().getFirstName())
                .isEqualTo(comment1OfBlockedPerson_notFlagged.getCreator().getFirstName());
        assertThat(clientCommentOfBlockedPerson_notFlagged.getCreator().getLastName())
                .isEqualTo(shorten(comment1OfBlockedPerson_notFlagged.getCreator().getLastName()));

        final ClientComment clientComment2OfBlockedPerson_flagged = returnedComments.stream()
                .filter(c -> c.getId().equals(comment2OfBlockedPerson_flagged.getId()))
                .findAny()
                .get();
        assertThat(clientComment2OfBlockedPerson_flagged.getFilterStatus())
                .isEqualTo(ClientFilterStatus.BLOCKED);
        assertThat(clientComment2OfBlockedPerson_flagged.getCreator().getId())
                .isEqualTo(comment2OfBlockedPerson_flagged.getCreator().getId());
        assertThat(clientComment2OfBlockedPerson_flagged.getCreator().getFilterStatus())
                .isEqualTo(ClientFilterStatus.BLOCKED);
        assertThat(clientComment2OfBlockedPerson_flagged.getCreator().getProfilePicture())
                .isNull();
        assertThat(clientComment2OfBlockedPerson_flagged.getCreator().getFirstName())
                .isEqualTo(comment2OfBlockedPerson_flagged.getCreator().getFirstName());
        assertThat(clientComment2OfBlockedPerson_flagged.getCreator().getLastName())
                .isEqualTo(shorten(comment2OfBlockedPerson_flagged.getCreator().getLastName()));

        final ClientComment clientCommentOfGossipCreator = returnedComments.stream()
                .filter(c -> c.getId().equals(commentOfGossipCreator.getId()))
                .findAny()
                .get();
        assertThat(clientCommentOfGossipCreator.getFilterStatus()).isNull();
        assertThat(clientCommentOfGossipCreator.getCreator().getId())
                .isEqualTo(commentOfGossipCreator.getCreator().getId());
        assertThat(clientCommentOfGossipCreator.getCreator().getFilterStatus())
                .isNull();
        assertThat(clientCommentOfGossipCreator.getCreator().getProfilePicture().getId())
                .isEqualTo(commentOfGossipCreator.getCreator().getProfilePicture().getId());
        assertThat(clientCommentOfGossipCreator.getCreator().getFirstName())
                .isEqualTo(commentOfGossipCreator.getCreator().getFirstName());
        assertThat(clientCommentOfGossipCreator.getCreator().getLastName())
                .isEqualTo(shorten(commentOfGossipCreator.getCreator().getLastName()));

        final ClientComment clientCommentOfNonBlockedPerson_flagged = returnedComments.stream()
                .filter(c -> c.getId().equals(commentOfNonBlockedPerson_flagged.getId()))
                .findAny()
                .get();
        assertThat(clientCommentOfNonBlockedPerson_flagged.getFilterStatus())
                .isEqualTo(ClientFilterStatus.FLAGGED);
        assertThat(clientCommentOfNonBlockedPerson_flagged.getCreator().getId())
                .isEqualTo(commentOfNonBlockedPerson_flagged.getCreator().getId());
        assertThat(clientCommentOfNonBlockedPerson_flagged.getCreator().getFilterStatus())
                .isNull();
        assertThat(clientCommentOfNonBlockedPerson_flagged.getCreator().getProfilePicture().getId())
                .isEqualTo(commentOfNonBlockedPerson_flagged.getCreator().getProfilePicture().getId());
        assertThat(clientCommentOfNonBlockedPerson_flagged.getCreator().getFirstName())
                .isEqualTo(commentOfNonBlockedPerson_flagged.getCreator().getFirstName());
        assertThat(clientCommentOfNonBlockedPerson_flagged.getCreator().getLastName())
                .isEqualTo(shorten(commentOfNonBlockedPerson_flagged.getCreator().getLastName()));

        final ClientComment clientCommentDeleted = returnedComments.stream()
                .filter(c -> c.getId().equals(commentDeleted.getId()))
                .findAny()
                .get();
        assertThat(clientCommentDeleted.getFilterStatus())
                .isNull();
        assertThat(clientCommentDeleted.getCreator().getId())
                .isEqualTo(clientCommentDeleted.getCreator().getId());
        assertThat(clientCommentDeleted.getCreator().getFilterStatus())
                .isNull();
        assertThat(clientCommentDeleted.getCreator().getProfilePicture().getId())
                .isEqualTo(clientCommentDeleted.getCreator().getProfilePicture().getId());
        assertThat(clientCommentDeleted.getCreator().getFirstName())
                .isEqualTo(clientCommentDeleted.getCreator().getFirstName());
        assertThat(clientCommentDeleted.getCreator().getLastName())
                .isEqualTo(shorten(clientCommentDeleted.getCreator().getLastName()));
        assertTrue(clientCommentDeleted.isDeleted());
    }

}
