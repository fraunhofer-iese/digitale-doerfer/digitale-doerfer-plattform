/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shopping;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.shopping.ShoppingTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReportFormat;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrder;
import de.fhg.iese.dd.platform.datamanagement.shopping.model.PurchaseOrderItem;

public class ShoppingDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private ShoppingTestHelper shoppingTestHelper;

    @Autowired
    private DataPrivacyDataCollectionRepository dataCollectionRepository;

    private PurchaseOrder purchaseOrder;

    @Override
    public void createEntities() throws Exception {
        shoppingTestHelper.createTenantsAndGeoAreas();
        shoppingTestHelper.createPersons();
        shoppingTestHelper.createShops();
        shoppingTestHelper.createPushEntities();
        shoppingTestHelper.createAchievementRules();

        purchaseOrder = shoppingTestHelper.createPurchaseOrder(shoppingTestHelper.shop1, getPersonForPrivacyReport());
    }

    @Override
    public void tearDown() throws Exception {
        shoppingTestHelper.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return shoppingTestHelper.personRegularAnna;
    }

    @Override
    public Person getPersonForDeletion() {
        return getPersonForPrivacyReport();
    }

    @Test
    public void dataPrivacyReportContainsPurchaseOrder() {

        assertTrue(StringUtils.isNotEmpty(purchaseOrder.getPurchaseOrderNotes()));
        assertTrue(StringUtils.isNotEmpty(purchaseOrder.getTransportNotes()));

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        Stream.of(DataPrivacyReportFormat.values()).forEach(format -> {
            //log which report is handled (for test debugging)
            log.info("Processing {} report", format);

            final String report = converterService.convert(dataPrivacyReport, format);

            assertFalse(report.isEmpty(), format + " report should not be empty");
            assertTrue(report.contains("Bestellung"));
            assertTrue(report.contains(purchaseOrder.getPurchaseOrderNotes()));
            assertTrue(report.contains(purchaseOrder.getTransportNotes()));
            assertTrue(report.contains(purchaseOrder.getSender().getName()));

            for (PurchaseOrderItem item : purchaseOrder.getItems()) {
                assertTrue(report.contains(item.getItemName()));
            }

        });
    }

    @Test
    public void purchaseOrdersAreDeleted() {

        IPersonDeletionReport personDeletionReport = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), personDeletionReport);

        assertThat(personDeletionReport).is(containsEntityWithOperation(purchaseOrder, DeleteOperation.DELETED));
    }

}
