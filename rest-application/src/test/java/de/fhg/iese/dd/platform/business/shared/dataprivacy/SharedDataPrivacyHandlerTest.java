/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.OauthUser;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.PersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsageAreaSelection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataDeletionAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataDeletionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.repos.NamedCounterRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.repos.OauthClientRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.repos.TeamNotificationChannelMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.repos.TeamNotificationConnectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.waiting.repos.WaitingDeadlineRepository;

@ActiveProfiles({"test", "data-privacy-service-test"})
public class SharedDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private IPersonService personService;

    private LegalTextAcceptance acceptance1DeletedPerson;
    private LegalTextAcceptance acceptance2DeletedPerson;
    private AppVariantUsage appVariantUsageDeletedPerson;
    private AppVariantUsageAreaSelection appVariantUsageAreaSelection1DeletedPerson;
    private AppVariantUsageAreaSelection appVariantUsageAreaSelection2DeletedPerson;
    private PushCategoryUserSetting pushCategoryUserSettingDeletedPerson;
    private TemporaryMediaItem temporaryMediaItemDeletedPerson;
    private TemporaryDocumentItem temporaryDocumentItemDeletedPerson;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createShops();
        th.createAppEntities();
        th.createPushEntities();
        th.createAchievementRules();
        th.createLegalEntities();
        createPersonData(getPersonForPrivacyReport());
        createPersonData(getPersonForDeletion());
    }

    private void createPersonData(Person person) {
        th.createUserDataEntities(person);

        acceptance1DeletedPerson =
                acceptLegalInDB(th.legalText1App1Variant1Required, person, timeServiceMock.currentTimeMillisUTC());
        acceptance2DeletedPerson =
                acceptLegalInDB(th.legalText2App1Variant1Optional, person, timeServiceMock.currentTimeMillisUTC());

        temporaryMediaItemDeletedPerson = th.createTemporaryMediaItem(person, Long.MAX_VALUE, 42L);
        temporaryDocumentItemDeletedPerson = th.createTemporaryDocumentItem(person, Long.MAX_VALUE, 42L);

        //app variant usage and area selections
        appVariantUsageDeletedPerson = th.appVariantUsageRepository.save(
                AppVariantUsage.builder()
                        .appVariant(th.app1Variant1)
                        .person(person)
                        .build());

        appVariantUsageAreaSelection1DeletedPerson = th.appVariantUsageAreaSelectionRepository.save(
                AppVariantUsageAreaSelection.builder()
                        .appVariantUsage(appVariantUsageDeletedPerson)
                        .selectedArea(th.geoAreaTenant1)
                        .build());

        appVariantUsageAreaSelection2DeletedPerson = th.appVariantUsageAreaSelectionRepository.save(
                AppVariantUsageAreaSelection.builder()
                        .appVariantUsage(appVariantUsageDeletedPerson)
                        .selectedArea(th.subGeoArea1Tenant1)
                        .build());

        //push settings
        pushCategoryService.changePushCategoryUserSetting(th.pushCategory1App1, person, th.app1Variant1,
                !th.pushCategory1App1.isDefaultLoudPushEnabled());

        List<PushCategoryUserSetting> userSettings =
                th.pushCategoryUserSettingRepository.findByPersonOrderByCreatedDesc(person);
        assertEquals(1, userSettings.size());
        pushCategoryUserSettingDeletedPerson = userSettings.get(0);

        //flagging of entities (not shown in report so far, but for completeness)
        userGeneratedContentFlagService.create(th.tenant1, th.personIeseAdmin, th.tenant2, person, "flagged");
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return th.personRegularKarl;
    }

    @Override
    public Person getPersonForDeletion() {
        return th.personIeseAdmin;
    }

    @Override
    public Collection<Class<? extends JpaRepository<?,?>>> getExcludedRepositories(){
        return Arrays.asList(
                //no relation to person for all repositories
                NamedCounterRepository.class,
                TeamNotificationChannelMappingRepository.class,
                TeamNotificationConnectionRepository.class,
                WaitingDeadlineRepository.class,
                FeatureConfigRepository.class,
                OauthClientRepository.class,
                //they store entities related to the data collection that are deleted after the data collection
                DataPrivacyWorkflowAppVariantRepository.class,
                DataPrivacyWorkflowAppVariantResponseRepository.class,
                DataPrivacyDataCollectionAppVariantResponseRepository.class,
                DataPrivacyWorkflowAppVariantStatusRecordRepository.class,
                DataPrivacyDataDeletionAppVariantResponseRepository.class,
                DataPrivacyWorkflowRepository.class,
                DataPrivacyDataCollectionRepository.class,
                DataPrivacyDataDeletionRepository.class);
    }

    @Test
    public void collectUserData_ReportContainsProfilePictureInMediaItems() {

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        assertThat(dataPrivacyReport.getMediaItems()).contains(getPersonForPrivacyReport().getProfilePicture());
    }

    @Test
    public void deletePerson_SharedDataEntities() {

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), report);

        //legal text acceptance
        assertThat(report).is(containsEntityWithOperation(acceptance1DeletedPerson, DeleteOperation.DELETED));
        assertFalse(th.legalTextAcceptanceRepository.existsById(acceptance1DeletedPerson.getId()));
        assertThat(report).is(containsEntityWithOperation(acceptance2DeletedPerson, DeleteOperation.DELETED));
        assertFalse(th.legalTextAcceptanceRepository.existsById(acceptance2DeletedPerson.getId()));

        //app variant usage and area selections
        assertThat(report).is(containsEntityWithOperation(appVariantUsageDeletedPerson, DeleteOperation.DELETED));
        assertFalse(th.appVariantUsageRepository.existsById(appVariantUsageDeletedPerson.getId()));
        assertFalse(th.appVariantUsageAreaSelectionRepository.existsById(
                appVariantUsageAreaSelection1DeletedPerson.getId()));
        assertFalse(th.appVariantUsageAreaSelectionRepository.existsById(
                appVariantUsageAreaSelection2DeletedPerson.getId()));

        //category 1 and values are deleted
        assertFalse(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry1Cat1.getId()));
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry1Cat1, DeleteOperation.DELETED));
        assertFalse(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry2Cat1.getId()));
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry2Cat1, DeleteOperation.DELETED));
        assertFalse(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry3Cat1.getId()));
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry3Cat1, DeleteOperation.DELETED));
        assertFalse(th.userDataCategoryRepository.existsById(th.userDataCategory1.getId()));
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory1, DeleteOperation.DELETED));

        //category 2 and values are deleted
        assertFalse(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry4Cat2.getId()));
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry4Cat2, DeleteOperation.DELETED));
        assertThat(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry5Cat2.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry5Cat2, DeleteOperation.DELETED));
        assertThat(th.userDataKeyValueEntryRepository.existsById(th.userDataKeyValueEntry6Cat2.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataKeyValueEntry6Cat2, DeleteOperation.DELETED));
        assertThat(th.userDataCategoryRepository.existsById(th.userDataCategory2.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory2, DeleteOperation.DELETED));

        //other categories are deleted
        assertThat(th.userDataCategoryRepository.existsById(th.userDataCategory3.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory3, DeleteOperation.DELETED));
        assertThat(th.userDataCategoryRepository.existsById(th.userDataCategory4.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory4, DeleteOperation.DELETED));
        assertThat(th.userDataCategoryRepository.existsById(th.userDataCategory5.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory5, DeleteOperation.DELETED));
        assertThat(th.userDataCategoryRepository.existsById(th.userDataCategory6.getId())).isFalse();
        assertThat(report).is(containsEntityWithOperation(th.userDataCategory6, DeleteOperation.DELETED));

        //push setting is deleted
        assertFalse(th.pushCategoryUserSettingRepository.existsById(pushCategoryUserSettingDeletedPerson.getId()));
        assertThat(report).is(
                containsEntityWithOperation(pushCategoryUserSettingDeletedPerson, DeleteOperation.DELETED));

        //temporary media item is deleted
        assertFalse(th.temporaryMediaItemRepository.existsById(temporaryMediaItemDeletedPerson.getId()));
        assertThat(report).is(containsEntityWithOperation(temporaryMediaItemDeletedPerson, DeleteOperation.DELETED));

        //temporary document item is deleted
        assertFalse(th.temporaryDocumentItemRepository.existsById(temporaryDocumentItemDeletedPerson.getId()));
        assertThat(report).is(containsEntityWithOperation(temporaryDocumentItemDeletedPerson, DeleteOperation.DELETED));

        //role assignment is deleted
        assertFalse(th.roleAssignmentRepository.existsById(th.roleAssignmentPersonIeseAdmin.getId()));
        assertThat(report).is(containsEntityWithOperation(th.roleAssignmentPersonIeseAdmin, DeleteOperation.DELETED));
    }

    @Test
    public void deletePerson_PersonBlocked() {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person personForDeletion = getPersonForDeletion();

        testOauthManagementService.blockUser(personForDeletion, OauthAccount.BlockReason.MANUALLY_BLOCKED);
        personForDeletion.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        personForDeletion = personService.store(personForDeletion);

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(personForDeletion, report);

        assertFalse(report.getDeletedEntities()
                        .stream()
                        .anyMatch(entry -> entry.getEntityClass().equals(OauthUser.class)),
                "Oauth user should not be deleted since the user is blocked in Oauth");
        final Person deletedPerson = personService.findPersonById(personForDeletion.getId());
        assertPersonHasStatus(deletedPerson, PersonStatus.LOGIN_BLOCKED);
        assertPersonIsDeleted(deletedPerson);
        assertEquals(nowTime, th.personRepository.findById(personForDeletion.getId()).get().getDeletionTime());
    }

    @Test
    public void deletePerson_NotExistingOAuthAccount() {

        testOauthManagementService.deleteUser(getPersonForDeletion().getOauthId());

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), report);

        //oauth account is not deleted
        assertFalse(report.getDeletedEntities()
                        .stream()
                        .anyMatch(entry -> entry.getEntityClass().equals(OauthUser.class)),
                "Oauth user should not be deleted since it was not existing anymore");
    }

    private LegalTextAcceptance acceptLegalInDB(LegalText legalText, Person person, long timestamp) {
        return th.legalTextAcceptanceRepository.saveAndFlush(
                LegalTextAcceptance.builder()
                        .legalText(legalText)
                        .person(person)
                        .timestamp(timestamp)
                        .build()
        );
    }

    private void assertPersonHasStatus(Person person, PersonStatus personStatus) {
        assertThat(th.personRepository.findById(person.getId()).get().getStatuses().hasValue(personStatus))
                .isTrue();
    }

    private void assertPersonIsDeleted(Person person) {
        assertThat(th.personRepository.findById(person.getId()).get().isDeleted())
                .isTrue();
    }

}
