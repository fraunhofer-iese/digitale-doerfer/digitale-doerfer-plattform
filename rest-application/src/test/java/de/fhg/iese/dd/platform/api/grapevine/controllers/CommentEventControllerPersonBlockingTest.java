/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentOnPostEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientReplyToCommentEvent;
import de.fhg.iese.dd.platform.business.shared.push.providers.TestExternalPushProvider;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CommentEventControllerPersonBlockingTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @SpyBean
    private TestExternalPushProvider testExternalPushProvider;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void commentCreateRequest_WithReplyToVerifyingLoudPushMessage() throws Exception {

        //person is not blocked by anyone
        doCommentCreateRequestAndVerifyPush(true, (gossipAuthor, replyToCommentAuthor) ->
                th.personLaraSchaefer
        );
    }

    @Test
    public void commentCreateRequest_WithReplyToVerifyingSilentPushMessageWhenAuthorIsBlocked() throws Exception {

        doCommentCreateRequestAndVerifyPush(false, (gossipAuthor, replyToCommentAuthor) -> {
            final Person annoyingGuy = th.personLaraSchaefer;
            th.personBlockingRepository.save(PersonBlocking.builder()
                    .app(th.appDorffunk)
                    .blockingPerson(gossipAuthor)
                    .blockedPerson(annoyingGuy)
                    .build());
            th.personBlockingRepository.save(PersonBlocking.builder()
                    .app(th.appDorffunk)
                    .blockingPerson(replyToCommentAuthor)
                    .blockedPerson(annoyingGuy)
                    .build());
            return annoyingGuy;
        });
    }

    @FunctionalInterface
    private interface CommentAuthorSupplier {
        Person getAuthor(Person gossipAuthor, Person replyToCommentAuthor);
    }

    private void doCommentCreateRequestAndVerifyPush(boolean loudPushExpected,
            CommentAuthorSupplier commentAuthorSupplier) throws Exception {

        final Gossip gossip = th.gossip2WithComments;
        final Person gossipAuthor = gossip.getCreator();
        assertEquals(th.personSusanneChristmann, gossipAuthor); //test data integrity

        final Comment replyToComment = th.gossip2Comment1;
        final Person replyToCommentAuthor = replyToComment.getCreator();
        assertEquals(th.personThomasBecker, replyToCommentAuthor); //test data integrity

        final Person author = commentAuthorSupplier.getAuthor(gossipAuthor, replyToCommentAuthor);

        final ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(gossip.getId())
                .text("grrrrrrrrr")
                .replyToCommentId(replyToComment.getId())
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                        .headers(authHeadersFor(author, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        // verify push message
        waitForEventProcessing();

        ClientReplyToCommentEvent replyToCommentEvent =
                testClientPushService.getPushedEventToPersonLoud(author, replyToCommentAuthor,
                        ClientReplyToCommentEvent.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_REPLY);
        ClientCommentOnPostEvent commentOnPostEvent =
                testClientPushService.getPushedEventToPersonLoud(author, gossipAuthor,
                        ClientCommentOnPostEvent.class,
                        DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST);

        if (loudPushExpected) {
            assertNull(replyToCommentEvent.getNewComment().getFilterStatus());
            assertNull(commentOnPostEvent.getComment().getFilterStatus());
        } else {
            assertEquals(ClientFilterStatus.BLOCKED, replyToCommentEvent.getNewComment().getFilterStatus());
            assertEquals(ClientFilterStatus.BLOCKED, commentOnPostEvent.getComment().getFilterStatus());
        }

        //test how the messages were pushed, since the silent/loud decision is taken at the push provider
        Mockito.verify(testExternalPushProvider).sendMessageToPerson(Mockito.anySet(), Mockito.eq(gossipAuthor),
                Mockito.any(), Mockito.eq(loudPushExpected));
        Mockito.verify(testExternalPushProvider).sendMessageToPerson(Mockito.anySet(), Mockito.eq(replyToCommentAuthor),
                Mockito.any(), Mockito.eq(loudPushExpected));

        testClientPushService.assertNoMorePushedEvents();
    }

}
