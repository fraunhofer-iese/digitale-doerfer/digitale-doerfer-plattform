/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommonHelpDesk;

public class CustomerSupportControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
    }

    private Person createCommonContactPerson() {
        Person contactPersonCommon = th.createPerson("Contactina", th.tenant1,
                Address.builder()
                        .name("Common Contactina Persona")
                        .street("Fraunhofer Platz 1")
                        .zip("67663")
                        .city("Kaiserslautern")
                        .gpsLocation(new GPSLocation(49.441864, 7.7652635)).build(),
                "730159c3-fd62-4adb-a0a6-77a74a1313e1");
        th.assignRoleToPerson(contactPersonCommon, CommonHelpDesk.class, null);
        return contactPersonCommon;
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();

    }

    @Test
    public void getContactPersonsForGeoArea() throws Exception {

        Person helplessPerson = th.personRegularAnna;
        Person expectedContactPerson = th.contactPersonTenant1;

        mockMvc.perform(get("/support/contacts")
                .headers(authHeadersFor(helplessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName").value(expectedContactPerson.getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(expectedContactPerson.getLastName()))
                .andExpect(jsonPath("$[0].email").value(expectedContactPerson.getEmail()))
                .andExpect(jsonPath("$[0].phoneNumber").value(expectedContactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$[0].profilePicture.id").value(expectedContactPerson.getProfilePicture().getId()));
    }

    @Test
    public void getContactPersonsCommon() throws Exception {

        Person helplessPerson = th.personRegularHorstiTenant3;
        Person expectedContactPerson = createCommonContactPerson();

        mockMvc.perform(get("/support/contacts")
                .headers(authHeadersFor(helplessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName").value(expectedContactPerson.getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(expectedContactPerson.getLastName()))
                .andExpect(jsonPath("$[0].email").value(expectedContactPerson.getEmail()))
                .andExpect(jsonPath("$[0].phoneNumber").value(expectedContactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$[0].profilePicture.id").value(expectedContactPerson.getProfilePicture().getId()));
    }

    @Test
    public void getDefaultContactForGeoArea() throws Exception {

        Person helplessPerson = th.personRegularAnna;
        Person contactPerson = th.contactPersonTenant1;

        mockMvc.perform(get("/support/contacts/default")
                .headers(authHeadersFor(helplessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(contactPerson.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(contactPerson.getLastName()))
                .andExpect(jsonPath("$.email").value(contactPerson.getEmail()))
                .andExpect(jsonPath("$.phoneNumber").value(contactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$.profilePicture.id").value(contactPerson.getProfilePicture().getId()));
    }

    @Test
    public void getDefaultContactCommon() throws Exception {

        Person helplessPerson = th.personRegularHorstiTenant3;
        Person expectedContactPerson = createCommonContactPerson();

        mockMvc.perform(get("/support/contacts/default")
                .headers(authHeadersFor(helplessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(expectedContactPerson.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedContactPerson.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedContactPerson.getEmail()))
                .andExpect(jsonPath("$.phoneNumber").value(expectedContactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$.profilePicture.id").value(expectedContactPerson.getProfilePicture().getId()));
    }

    @Test
    public void getDefaultContactForGeoAreaContactNotSet() throws Exception {

        Person authenticatedPerson = th.personRegularHorstiTenant3;

        mockMvc.perform(get("/support/contacts/default")
                .headers(authHeadersFor(authenticatedPerson))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.CONTACT_PERSON_NOT_FOUND));
    }

    @Test
    public void getContactPersonsForGeoAreaContactNotSet() throws Exception {

        Person authenticatedPerson = th.personRegularHorstiTenant3;

        mockMvc.perform(get("/support/contacts")
                .headers(authHeadersFor(authenticatedPerson))
                .contentType(contentType))
                .andExpect(isException(ClientExceptionType.CONTACT_PERSON_NOT_FOUND));
    }

    @Test
    public void getContactPersonsForGeoAreaHomeAreaNotSet() throws Exception {

        Person helpAndHomelessPerson = th.personRegularAnna;
        Person expectedContactPerson = th.contactPersonTenant1;
        helpAndHomelessPerson.setHomeArea(null);
        helpAndHomelessPerson = th.personRepository.save(helpAndHomelessPerson);

        mockMvc.perform(get("/support/contacts")
                .headers(authHeadersFor(helpAndHomelessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName").value(expectedContactPerson.getFirstName()))
                .andExpect(jsonPath("$[0].lastName").value(expectedContactPerson.getLastName()))
                .andExpect(jsonPath("$[0].email").value(expectedContactPerson.getEmail()))
                .andExpect(jsonPath("$[0].phoneNumber").value(expectedContactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$[0].profilePicture.id").value(expectedContactPerson.getProfilePicture().getId()));
    }

    @Test
    public void getDefaultContactPersonForGeoAreaHomeAreaNotSet() throws Exception {

        Person helpAndHomelessPerson = th.personRegularAnna;
        Person expectedContactPerson = th.contactPersonTenant1;
        helpAndHomelessPerson.setHomeArea(null);
        helpAndHomelessPerson = th.personRepository.save(helpAndHomelessPerson);

        mockMvc.perform(get("/support/contacts/default")
                .headers(authHeadersFor(helpAndHomelessPerson))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(expectedContactPerson.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(expectedContactPerson.getLastName()))
                .andExpect(jsonPath("$.email").value(expectedContactPerson.getEmail()))
                .andExpect(jsonPath("$.phoneNumber").value(expectedContactPerson.getPhoneNumber()))
                .andExpect(jsonPath("$.profilePicture.id").value(expectedContactPerson.getProfilePicture().getId()));
    }

}
