/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentOnPostEvent;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientReplyToCommentEvent;
import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentCreationRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CommentEventControllerCreateTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void commentCreateRequest_VerifyingPushMessage() throws Exception {

        Post postToComment = th.gossip1withImages;
        Person commentCreator = th.personLaraSchaefer;

        commentCreateRequest_VerifyingPushMessage(postToComment, commentCreator,
                "/grapevine/comment/event/commentCreateRequest", th.appVariant4AllTenants);
    }

    @Test
    public void commentCreateRequest_VerifyingPushMessage_Happening() throws Exception {

        Post postToComment = th.happening1;
        Person postCreator = postToComment.getCreator();
        Person commentCreator = th.personSebastianBauer;

        assertThat(postCreator).isNotNull();
        assertThat(postCreator).isNotEqualTo(commentCreator);

        commentCreateRequest_VerifyingPushMessage(postToComment, commentCreator,
                "/grapevine/comment/event/commentCreateRequest", th.appVariant4AllTenants);

        Post postToCommentNoCreator = th.happening2;
        assertThat(postToCommentNoCreator.getCreator()).isNull();

        commentCreateRequest_VerifyingPushMessage(postToCommentNoCreator, commentCreator,
                "/grapevine/comment/event/commentCreateRequest", th.appVariant4AllTenants);
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_VerifyingPushMessage() throws Exception {

        Post postToComment = th.suggestionWish1;
        //change the post, so that the suggestion worker would not see it and not be able to comment on it when using the other endpoint
        GeoArea geoArea = th.subGeoArea1Tenant1;
        postToComment.setGeoAreas(Set.of(geoArea));
        postToComment.setHiddenForOtherHomeAreas(true);
        postToComment = th.postRepository.save(postToComment);

        Person commentCreator = th.personSuggestionWorkerTenant1Dieter;
        commentCreator.setHomeArea(th.geoAreaTenant1);
        commentCreator = th.personRepository.save(commentCreator);

        commentCreateRequest_VerifyingPushMessage(postToComment, commentCreator,
                "/grapevine/comment/event/suggestion/commentCreateRequest", th.appVariant4AllTenants);
    }

    @Test
    public void commentCreateRequest_WithReplyToVerifyingPushMessage() throws Exception {

        commentCreateRequest_WithReplyToVerifyingPushMessage(th.gossip1withImages, th.personLaraSchaefer,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_WithReplyToVerifyingPushMessage() throws Exception {

        commentCreateRequest_WithReplyToVerifyingPushMessage(th.suggestionDefect1,
                th.personSuggestionFirstResponderTenant1Hilde,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    private void commentCreateRequest_WithReplyToVerifyingPushMessage(Post post, Person commentCreator,
            String path) throws Exception {

        String text = "Komm 🦆!";

        Comment replyToComment = th.gossip2Comment1;

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .text(text)
                .replyToCommentId(replyToComment.getId())
                .build();

        MvcResult createRequestResult = mockMvc.perform(post(path)
                .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientCommentCreateConfirmation commentCreateConfirmation =
                toObject(createRequestResult.getResponse(), ClientCommentCreateConfirmation.class);
        assertEquals(commentCreateRequest.getText(), commentCreateConfirmation.getComment().getText());
        assertEquals(commentCreator.getId(), commentCreateConfirmation.getComment().getCreator().getId());
        assertEquals(commentCreateRequest.getPostId(), commentCreateConfirmation.getComment().getPostId());
        assertEquals(replyToComment.getCreator().getId(), commentCreateConfirmation.getComment().getReplyTo().getId());

        // Check whether the comment is also added to the repository
        Comment addedComment = th.commentRepository.findById(commentCreateConfirmation.getCommentId()).get();

        assertEquals(post, addedComment.getPost());
        assertEquals(commentCreator, addedComment.getCreator());
        assertEquals(text, addedComment.getText());
        assertEquals(replyToComment, addedComment.getReplyTo());
        assertThat(addedComment.getImages()).isNullOrEmpty();
        assertThat(addedComment.getAttachments()).isNullOrEmpty();

        //verify push message
        waitForEventProcessing();

        ClientCommentOnPostEvent commentOnPostEvent = testClientPushService.getPushedEventToPersonLoud(commentCreator,
                post.getCreator(),
                ClientCommentOnPostEvent.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST);

        assertThat(commentOnPostEvent.getPostId()).isEqualTo(post.getId());
        assertThat(commentOnPostEvent.getCommentId()).isEqualTo(commentCreateConfirmation.getCommentId());
        assertThat(commentOnPostEvent.getComment().getId()).isEqualTo(commentCreateConfirmation.getComment().getId());

        ClientReplyToCommentEvent replyToCommentEvent = testClientPushService.getPushedEventToPersonLoud(commentCreator,
                replyToComment.getCreator(),
                ClientReplyToCommentEvent.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_REPLY);

        assertThat(replyToCommentEvent.getPostId()).isEqualTo(post.getId());
        assertThat(replyToCommentEvent.getNewCommentId()).isEqualTo(commentCreateConfirmation.getCommentId());
        assertThat(replyToCommentEvent.getReferencedCommentId()).isEqualTo(replyToComment.getId());
        //check that the last name is cut
        assertEquals(commentCreator.getFirstName(), replyToCommentEvent.getNewComment().getCreator().getFirstName());
        assertEquals(BaseLastNameShorteningModifier.shorten(commentCreator.getLastName()),
                replyToCommentEvent.getNewComment().getCreator().getLastName());

        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void commentCreateRequest_ContentCreationRestricted() throws Exception {

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/comment/event/commentCreateRequest")
                        .contentType(contentType)
                        .content(json(ClientCommentCreateRequest.builder()
                                .postId(th.gossip1withImages.getId())
                                .text("HAllO kAnn kAufen?!")
                                .build())));
    }

    @Test
    public void commentCreateRequest_InternalSuggestion() throws Exception {

        assertIsPersonVerificationStatusRestricted(ContentCreationRestrictionFeature.class,
                th.personSusanneChristmann,
                th.appVariant4AllTenants,
                post("/grapevine/comment/event/commentCreateRequest")
                        .contentType(contentType)
                        .content(json(ClientCommentCreateRequest.builder()
                                .postId(th.suggestionInternal.getId())
                                .text("Ja, nö! Das würde ich ja viel bessr machn!!11!")
                                .build())));
    }

    @Test
    public void commentCreateRequest_Suggestion_CommentsAllowedFalse() throws Exception {

        Person commentCreator = th.personSusanneChristmann;
        Suggestion suggestion = th.suggestionDefect1;
        suggestion.setCommentsAllowed(false);
        suggestion = th.postRepository.saveAndFlush(suggestion);
        AppVariant appVariant = th.appVariant4AllTenants;

        assertThat(suggestion.getCreator()).isNotEqualTo(commentCreator);

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(suggestion.getId())
                .text("Ich muss da jetzt mal eingreifen! 🤬")
                .build();

        //someone != suggestion creator can not comment
        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(commentCreator, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_CREATION_NOT_ALLOWED));

        //suggestion creator can comment
        commentCreateRequest_VerifyingPushMessage(suggestion, suggestion.getCreator(),
                "/grapevine/comment/event/commentCreateRequest",
                appVariant);

        //suggestion worker can comment
        commentCreateRequest_VerifyingPushMessage(suggestion, th.personSuggestionWorkerTenant1Dieter,
                "/grapevine/comment/event/suggestion/commentCreateRequest",
                appVariant);
    }

    @Test
    public void commentCreateRequest_InternalSuggestion_CommentsAllowedFalse() throws Exception {

        Suggestion suggestion = th.suggestionInternal;
        Person normalPerson = th.personSusanneChristmann;
        final Person suggestionWorker = th.personSuggestionWorkerTenant1Dieter;
        final Person suggestionCreator = suggestion.getCreator();
        suggestion.setCommentsAllowed(false);
        suggestion = th.postRepository.saveAndFlush(suggestion);
        AppVariant appVariant = th.appVariant4AllTenants;

        assertThat(suggestionCreator).isNotEqualTo(normalPerson);
        assertThat(suggestionWorker).isNotEqualTo(suggestionCreator);

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(suggestion.getId())
                .text("Ich geb mal meinen Senf dazu ab!")
                .build();

        //normal person can not comment
        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(normalPerson, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //suggestion worker and creator can not comment using the dorffunk endpoint
        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(suggestionCreator, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(suggestionWorker, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));

        //suggestion worker and creator can comment using the special suggestion endpoint
        commentCreateRequest_PushNotTested(suggestion, suggestionWorker,
                "/grapevine/comment/event/suggestion/commentCreateRequest",
                appVariant);
        commentCreateRequest_PushNotTested(suggestion, suggestionCreator,
                "/grapevine/comment/event/suggestion/commentCreateRequest",
                appVariant);

        //check that no push is sent
        waitForEventProcessing();
        testClientPushService.assertNoMorePushedEvents();
    }

    @Test
    public void commentCreateRequest_Gossip_CommentsAllowedFalse() throws Exception {

        Person commentCreator = th.personNinaBauer;
        Gossip gossip = th.gossip1withImages;
        gossip.setCommentsAllowed(false);
        gossip = th.postRepository.saveAndFlush(gossip);
        AppVariant appVariant = th.appVariant4AllTenants;

        assertThat(gossip.getCreator()).isNotEqualTo(commentCreator);

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(gossip.getId())
                .text("Hüpsches Pild !")
                .build();

        //someone != gossip creator can not comment
        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(commentCreator, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_CREATION_NOT_ALLOWED));

        //gossip creator can comment
        commentCreateRequest_VerifyingPushMessage(gossip, gossip.getCreator(),
                "/grapevine/comment/event/commentCreateRequest",
                appVariant);
    }

    @Test
    public void commentCreateRequest_CommentCountForSingleGossip() throws Exception {

        Map<String, Integer> gossipIdToCommentCount = th.gossipIdToCommentCount();

        AppVariant appVariant = th.appVariant4AllTenants;
        Person person = th.personSusanneChristmann;
        Gossip gossipWithComments = th.gossip2WithComments;

        String gossipId = gossipWithComments.getId();
        mockMvc.perform(get("/grapevine/post/" + gossipId)
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.commentCount").value(gossipIdToCommentCount.get(gossipId)));

        //Create a new comment
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(gossipId)
                .text("Adding a new comment")
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentCreateRequest")
                .headers(authHeadersFor(person, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(status().isOk());

        waitForEventProcessing();
        //Now checks that the commentCount was incremented by 1
        int newCommentCount = gossipIdToCommentCount.get(gossipId) + 1;
        mockMvc.perform(get("/grapevine/post/" + gossipId)
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.commentCount").value(newCommentCount));

        Post gossip = th.postRepository.findById(gossipId).get();
        assertEquals(newCommentCount, gossip.getCommentCount());
    }

    @Test
    public void commentCreateRequest_WithImagesAndDocuments() throws Exception {

        commentCreateRequest_WithImagesAndDocuments(th.gossip1withImages, th.personSusanneChristmann,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_WithImagesAndDocuments() throws Exception {

        commentCreateRequest_WithImagesAndDocuments(th.suggestionDefect1, th.personSuggestionWorkerTenant1Horst,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_ExceedingMaxTextLength() throws Exception {

        commentCreateRequest_ExceedingMaxTextLength(th.gossip6, th.personThomasBecker,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_ExceedingMaxTextLength() throws Exception {

        commentCreateRequest_ExceedingMaxTextLength(th.suggestionDefect1, th.personSuggestionWorkerTenant1Horst,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    private void commentCreateRequest_ExceedingMaxTextLength(Post post, Person commentCreator, String path)
            throws Exception {

        String text = RandomStringUtils.randomPrint(th.grapevineConfig.getMaxNumberCharsPerComment() + 1);
        log.info("Using random text '{}'", text);

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .text(text)
                .build();

        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type").value(ClientExceptionType.EVENT_ATTRIBUTE_INVALID.name()))
                .andExpect(jsonPath("$.detail").value("text"));
    }

    @Test
    public void commentCreateRequest_ExceedingMaxNumberDocuments() throws Exception {

        commentCreateRequest_ExceedingMaxNumberDocuments(th.gossip6, th.personThomasBecker,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_ExceedingMaxNumberDocuments() throws Exception {

        commentCreateRequest_ExceedingMaxNumberDocuments(th.suggestionDefect1, th.personSuggestionWorkerTenant1Horst,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_InvalidImages() throws Exception {

        commentCreateRequest_InvalidImages(th.gossip1withImages, th.personSusanneChristmann,
                "/grapevine/comment/event/commentCreateRequest");
        commentCreateRequest_DuplicateImages(th.gossip1withImages, th.personSusanneChristmann,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_InvalidImages() throws Exception {

        commentCreateRequest_InvalidImages(th.suggestionDefect2, th.personSuggestionFirstResponderTenant1Hilde,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
        commentCreateRequest_DuplicateImages(th.suggestionDefect2, th.personSuggestionFirstResponderTenant1Hilde,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    private void commentCreateRequest_InvalidImages(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "Kuck mal, wie ich im Buffett liege 🍴🍆!";

        th.temporaryMediaItemRepository.deleteAll();

        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidTempMediaItemId = "invalid id";

        //create request using temp images and invalid id
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), invalidTempMediaItemId))
                .text(text)
                .build();

        //check temp image not found exception
        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(isException("[" + invalidTempMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    private void commentCreateRequest_DuplicateImages(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "Mjam 🍴🍆!";

        th.temporaryMediaItemRepository.deleteAll();

        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using temp images twice
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage1.getId()))
                .text(text)
                .build();

        //check temp image duplicate exception
        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(isException("[" + tempImage1.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp images are not deleted
        assertEquals(2, th.temporaryMediaItemRepository.count());
    }

    @Test
    public void commentCreateRequest_InvalidDocuments() throws Exception {

        commentCreateRequest_InvalidDocuments(th.gossip1withImages, th.personSusanneChristmann,
                "/grapevine/comment/event/commentCreateRequest");
        commentCreateRequest_DuplicateDocuments(th.gossip1withImages, th.personSusanneChristmann,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_InvalidDocuments() throws Exception {

        commentCreateRequest_InvalidDocuments(th.suggestionDefect2, th.personSuggestionFirstResponderTenant1Hilde,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
        commentCreateRequest_DuplicateDocuments(th.suggestionDefect2, th.personSuggestionFirstResponderTenant1Hilde,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_OptionalAttributes() throws Exception {

        commentCreateRequest_OptionalAttributes(th.gossip3WithComment, th.personLaraSchaefer,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_OptionalAttributes() throws Exception {

        commentCreateRequest_OptionalAttributes(th.suggestionWish2, th.personSuggestionWorkerTenant1Dieter,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    private void commentCreateRequest_OptionalAttributes(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "¯\\_(ツ)_/¯ 🏢";
        Comment replyTo = th.gossip2Comment4withImages;

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .text(text)
                .replyToCommentId(replyTo.getId())
                .build();

        MvcResult createRequestResult = mockMvc.perform(post(path)
                .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientCommentCreateConfirmation gossipCreateConfirmation =
                toObject(createRequestResult.getResponse(), ClientCommentCreateConfirmation.class);

        // Check whether the comment is also added to the repository
        Comment addedComment = th.commentRepository.findById(gossipCreateConfirmation.getCommentId()).get();

        assertEquals(post, addedComment.getPost());
        assertEquals(commentCreator, addedComment.getCreator());
        assertEquals(text, addedComment.getText());
        assertEquals(replyTo, addedComment.getReplyTo());
        assertTrue(addedComment.getImages() == null || addedComment.getImages().isEmpty());
        assertTrue(addedComment.getAttachments() == null || addedComment.getAttachments().isEmpty());
    }

    @Test
    public void commentCreateRequest_Unauthorized() throws Exception {

        commentCreateRequest_Unauthorized(th.gossip3WithComment,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_Unauthorized() throws Exception {

        commentCreateRequest_Unauthorized(th.suggestionWish2,
                "/grapevine/comment/event/suggestion/commentCreateRequest");

        //caller has no roles
        mockMvc.perform(post("/grapevine/comment/event/suggestion/commentCreateRequest")
                .headers(authHeadersFor(th.personNinaBauer, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId(th.suggestionDefect1.getId())
                        .text("Mhm, sake! 🍶")
                        .build())))
                .andExpect(isNotAuthorized());

        //caller has roles for other tenant
        mockMvc.perform(post("/grapevine/comment/event/suggestion/commentCreateRequest")
                .headers(authHeadersFor(th.personSuggestionWorkerTenant2Heinz, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId(th.suggestionDefect1.getId())
                        .text("Freunde, wie wäre es mit Salat? 🥗")
                        .build())))
                .andExpect(isNotAuthorized());
    }

    private void commentCreateRequest_Unauthorized(Post post, String path) throws Exception {

        assertOAuth2(post(path).content(json(ClientCommentCreateRequest.builder()
                        .postId(post.getId())
                        .text("Komm ⛷️!")
                        .build()))
                .contentType(contentType));
    }

    @Test
    public void commentCreateRequest_UnknownPostId() throws Exception {

        commentCreateRequest_UnknownPostId(th.personLaraSchaefer,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_UnknownPostId() throws Exception {

        Person commentCreator = th.personSuggestionWorkerTenant1Horst;

        commentCreateRequest_UnknownPostId(commentCreator,
                "/grapevine/comment/event/suggestion/commentCreateRequest");

        //post is not a suggestion
        mockMvc.perform(post("/grapevine/comment/event/suggestion/commentCreateRequest")
                .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId(th.gossip1withImages.getId())
                        .text("Mjam 🍫!")
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    private void commentCreateRequest_UnknownPostId(Person commentCreator, String path) throws Exception {

        //missing post id
        mockMvc.perform(post(path)
                .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .text("Comment ohne postId")
                        .build())))
                .andExpect(isException("postId", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //invalid post id
        mockMvc.perform(post(path)
                .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(ClientCommentCreateRequest.builder()
                        .postId("not even an GUID")
                        .text("Komm 🦆!")
                        .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void commentCreateRequest_DeletedPost() throws Exception {

        Gossip gossip = th.gossip3WithComment;
        gossip.setDeleted(true);
        th.postRepository.save(gossip);

        commentCreateRequest_DeletedPost(gossip, th.personLaraSchaefer,
                "/grapevine/comment/event/commentCreateRequest");

        Suggestion suggestion = th.suggestionWish2;
        suggestion.setDeleted(true);
        suggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(suggestion);

        //a normal user should not be able to post on a withdrawn suggestion
        commentCreateRequest_DeletedPost(suggestion, th.personLaraSchaefer,
                "/grapevine/comment/event/commentCreateRequest");
    }

    @Test
    public void commentCreateRequest_SuggestionWorker_DeletedPost() throws Exception {

        Suggestion suggestion = th.suggestionWish2;
        suggestion.setDeleted(true);
        suggestion.setOverrideDeletedForSuggestionRoles(false);
        th.postRepository.save(suggestion);

        commentCreateRequest_DeletedPost(suggestion, th.personSuggestionWorkerTenant1Dieter,
                "/grapevine/comment/event/suggestion/commentCreateRequest");

        suggestion.setOverrideDeletedForSuggestionRoles(true);
        th.postRepository.save(suggestion);

        commentCreateRequest_DeletedPost(suggestion, th.personSuggestionWorkerTenant1Dieter,
                "/grapevine/comment/event/suggestion/commentCreateRequest");
    }

    private ClientCommentCreateConfirmation commentCreateRequest_PushNotTested(Post postToComment,
            Person commentCreator, String path, AppVariant appVariant) throws Exception {

        String commentText = "Yeah 🏳️‍🌈!";

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(postToComment.getId())
                .text(commentText)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post(path)
                .headers(authHeadersFor(commentCreator, appVariant))
                .contentType(contentType)
                .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientCommentCreateConfirmation commentCreateConfirmation =
                toObject(createRequestResult.getResponse(), ClientCommentCreateConfirmation.class);
        assertEquals(commentText, commentCreateConfirmation.getComment().getText());
        assertEquals(commentCreator.getId(), commentCreateConfirmation.getComment().getCreator().getId());
        assertEquals(postToComment.getId(), commentCreateConfirmation.getComment().getPostId());

        // Check whether the comment is also added to the repository
        Comment addedComment = th.commentRepository.findById(commentCreateConfirmation.getCommentId()).get();

        assertEquals(postToComment, addedComment.getPost());
        assertEquals(commentCreator, addedComment.getCreator());
        assertEquals(commentText, addedComment.getText());
        assertThat(addedComment.getImages()).isNullOrEmpty();
        assertThat(addedComment.getAttachments()).isNullOrEmpty();
        return commentCreateConfirmation;
    }

    private void commentCreateRequest_VerifyingPushMessage(Post postToComment, Person commentCreator,
            String path, AppVariant appVariant) throws Exception {

        ClientCommentCreateConfirmation commentCreateConfirmation =
                commentCreateRequest_PushNotTested(postToComment, commentCreator, path, appVariant);

        //authors of the post do not get a push message
        Person postCreator = postToComment.getCreator();
        if (postCreator != null && commentCreator != postCreator) {
            //verify push message
            waitForEventProcessing();

            ClientCommentOnPostEvent commentOnPostEvent =
                    testClientPushService.getPushedEventToPersonLoud(commentCreator,
                            postCreator,
                            ClientCommentOnPostEvent.class,
                            DorfFunkConstants.PUSH_CATEGORY_ID_COMMENT_ON_POST);

            assertThat(commentOnPostEvent.getPostId()).isEqualTo(postToComment.getId());
            assertThat(commentOnPostEvent.getCommentId()).isEqualTo(commentCreateConfirmation.getCommentId());
            assertThat(commentOnPostEvent.getComment().getId()).isEqualTo(
                    commentCreateConfirmation.getComment().getId());
            //check that the last name is cut
            assertEquals(commentCreator.getFirstName(), commentOnPostEvent.getComment().getCreator().getFirstName());
            assertEquals(BaseLastNameShorteningModifier.shorten(commentCreator.getLastName()),
                    commentOnPostEvent.getComment().getCreator().getLastName());

        }
        testClientPushService.assertNoMorePushedEvents();
    }

    private void commentCreateRequest_WithImagesAndDocuments(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "gothic text 😈";

        //create temp images
        TemporaryMediaItem tempImage1 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImageUnused =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage3 =
                th.createTemporaryMediaItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem("a.file", commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem("b.file", commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocumentUnused =
                th.createTemporaryDocumentItem("c.file", commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument3 =
                th.createTemporaryDocumentItem("d.file", commentCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using subset of temp images and documents
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .temporaryMediaItemIds(Arrays.asList(tempImage1.getId(), tempImage2.getId(), tempImage3.getId()))
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), tempDocument3.getId()))
                .text(text)
                .build();

        MvcResult createRequestResult = mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(status().isOk())
                .andReturn();
        ClientCommentCreateConfirmation commentCreateConfirmation =
                toObject(createRequestResult.getResponse(), ClientCommentCreateConfirmation.class);
        assertEquals(commentCreateRequest.getText(), commentCreateConfirmation.getComment().getText());
        assertEquals(commentCreator.getId(), commentCreateConfirmation.getComment().getCreator().getId());
        assertEquals(commentCreateRequest.getPostId(), commentCreateConfirmation.getComment().getPostId());
        assertEquals(commentCreateRequest.getTemporaryMediaItemIds().size(),
                commentCreateConfirmation.getComment().getImages().size());
        assertEquals(commentCreateRequest.getTemporaryDocumentItemIds().size(),
                commentCreateConfirmation.getComment().getAttachments().size());

        // Get the comment
        //check new entity uses images and documents
        mockMvc.perform(get("/grapevine/comment/" + commentCreateConfirmation.getCommentId())
                        .headers(authHeadersFor(commentCreator)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(commentCreateConfirmation.getCommentId()))
                .andExpect(jsonPath("$.creator.id").value(commentCreator.getId()))
                .andExpect(jsonPath("$.postId").value(post.getId()))
                .andExpect(jsonPath("$.text").value(text))
                .andExpect(jsonPath("$.images", hasSize(3)))
                .andExpect(jsonPath("$.images[0].id").value(tempImage1.getMediaItem().getId()))
                .andExpect(jsonPath("$.images[1].id").value(tempImage2.getMediaItem().getId()))
                .andExpect(jsonPath("$.images[2].id").value(tempImage3.getMediaItem().getId()))
                .andExpect(jsonPath("$.attachments", hasSize(3)))
                .andExpect(jsonPath("$.attachments[0].id").value(tempDocument1.getDocumentItem().getId()))
                .andExpect(jsonPath("$.attachments[1].id").value(tempDocument2.getDocumentItem().getId()))
                .andExpect(jsonPath("$.attachments[2].id").value(tempDocument3.getDocumentItem().getId()));

        // Check whether the comment is also added to the repository
        Comment addedComment = th.commentRepository.findById(commentCreateConfirmation.getCommentId()).get();

        assertEquals(post, addedComment.getPost());
        assertEquals(commentCreator, addedComment.getCreator());
        assertEquals(text, addedComment.getText());
        assertEquals(3, addedComment.getImages().size());
        assertEquals(tempImage1.getMediaItem(), addedComment.getImages().get(0));
        assertEquals(tempImage2.getMediaItem(), addedComment.getImages().get(1));
        assertEquals(tempImage3.getMediaItem(), addedComment.getImages().get(2));
        assertEquals(3, addedComment.getAttachments().size());
        assertThat(addedComment.getAttachments()).containsExactlyInAnyOrder(tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem(), tempDocument3.getDocumentItem());

        //check used temp images and documents are deleted
        assertEquals(1, th.temporaryMediaItemRepository.count());
        assertEquals(1, th.temporaryDocumentItemRepository.count());
        //check non used are still existing
        assertEquals(tempImageUnused, th.temporaryMediaItemRepository.findAll().get(0));
        assertEquals(tempDocumentUnused, th.temporaryDocumentItemRepository.findAll().get(0));
    }

    private void commentCreateRequest_DeletedPost(Post post, Person commentCreator, String path)
            throws Exception {

        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(ClientCommentCreateRequest.builder()
                                .postId(post.getId())
                                .text("Legga 🍓")
                                .build())))
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    private void commentCreateRequest_InvalidDocuments(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "Kuck mal, wie ich im Buffett liege 🍴🍆!";

        th.temporaryMediaItemRepository.deleteAll();

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        String invalidDocumentMediaItemId = "invalid id";

        //create request using temp documents and invalid id
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), invalidDocumentMediaItemId))
                .text(text)
                .build();

        //check temp documents not found exception
        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(isException("[" + invalidDocumentMediaItemId + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    private void commentCreateRequest_DuplicateDocuments(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "Mjam 🍴🍆!";

        th.temporaryDocumentItemRepository.deleteAll();

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(commentCreator, th.nextTimeStamp(), th.nextTimeStamp());

        //create request using temp documents twice
        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .temporaryDocumentItemIds(
                        Arrays.asList(tempDocument1.getId(), tempDocument2.getId(), tempDocument1.getId()))
                .text(text)
                .build();

        //check temp document duplicate exception
        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(isException("[" + tempDocument1.getId() + "]",
                        ClientExceptionType.TEMPORARY_FILE_ITEM_DUPLICATE_USAGE));

        //check unused temp documents are not deleted
        assertEquals(2, th.temporaryDocumentItemRepository.count());
    }

    private void commentCreateRequest_ExceedingMaxNumberDocuments(Post post, Person commentCreator, String path)
            throws Exception {

        String text = "comment with too many documents";
        List<String> temporaryIds = new ArrayList<>();
        for (int i = 0; i <= th.grapevineConfig.getMaxNumberAttachmentsPerComment(); i++) {
            temporaryIds.add(UUID.randomUUID().toString());
        }

        ClientCommentCreateRequest commentCreateRequest = ClientCommentCreateRequest.builder()
                .postId(post.getId())
                .text(text)
                .temporaryDocumentItemIds(temporaryIds)
                .build();

        mockMvc.perform(post(path)
                        .headers(authHeadersFor(commentCreator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentCreateRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(isException("temporaryDocumentItemIds", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

}
