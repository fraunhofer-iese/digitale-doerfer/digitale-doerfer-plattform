/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Ala Harirchi, Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LegacyGrapevineStatisticsAdminControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    private static final boolean SAVE_REPORTS = false;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getGrapevineReport_Unauthorized() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        assertOAuth2(get("/administration/grapevine/stats/globalReport")
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end)));

        mockMvc.perform(get("/administration/grapevine/stats/globalReport")
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end))
                .headers(authHeadersFor(th.personSusanneChristmann)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getGrapevineReport_NoException() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        String report = mockMvc.perform(get("/administration/grapevine/stats/globalReport")
                        .param("start", String.valueOf(start))
                        .param("end", String.valueOf(end))
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        if (SAVE_REPORTS) {
            final Path tempFile = Files.createTempFile(getClass().getSimpleName() + "-global-", ".html");
            log.info("Saving report to {}", tempFile.toUri());
            Files.write(tempFile, report.getBytes(StandardCharsets.UTF_8));
        }
    }

    @Test
    public void getGrapevineReport_EndTimeNotNow() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(3);
        long end = System.currentTimeMillis() - TimeUnit.HOURS.toMillis(3);

        String report = mockMvc.perform(get("/administration/grapevine/stats/globalReport")
                        .param("start", String.valueOf(start))
                        .param("end", String.valueOf(end))
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))))
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        if (SAVE_REPORTS) {
            final Path tempFile = Files.createTempFile(getClass().getSimpleName() + "-global_enddiff-", ".html");
            log.info("Saving report to {}", tempFile.toUri());
            Files.write(tempFile, report.getBytes(StandardCharsets.UTF_8));
        }
    }

    @Test
    public void getReportForTenantGroup_Unauthorized() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        assertOAuth2(get("/administration/grapevine/stats/tenantGroupReport")
                .param("tenantGroupTag", th.tenant1.getTag())
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end)));

        mockMvc.perform(get("/administration/grapevine/stats/tenantGroupReport")
                .param("tenantGroupTag", th.tenant1.getTag())
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end))
                .headers(authHeadersFor(th.personSusanneChristmann)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getReportForTenantGroup_NoException() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        mockMvc.perform(get("/administration/grapevine/stats/tenantGroupReport")
                        .param("tenantGroupTag", th.tenant1.getTag())
                        .param("start", String.valueOf(start))
                        .param("end", String.valueOf(end))
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))));
    }

    @Test
    public void getReportForTenant_Unauthorized() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        assertOAuth2(get("/administration/grapevine/stats/tenantReport")
                .param("tenantId", th.tenant1.getId())
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end)));

        mockMvc.perform(get("/administration/grapevine/stats/tenantReport")
                .param("tenantId", th.tenant1.getId())
                .param("start", String.valueOf(start))
                .param("end", String.valueOf(end))
                .headers(authHeadersFor(th.personSusanneChristmann)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getReportForTenant_NoException() throws Exception {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        mockMvc.perform(get("/administration/grapevine/stats/tenantReport")
                        .param("tenantId", th.tenant1.getId())
                        .param("start", String.valueOf(start))
                        .param("end", String.valueOf(end))
                        .headers(authHeadersFor(th.personSuperAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))));
    }

}
