/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.dataprivacy.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IInternalDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyDataCollectionRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

@Tag("data-privacy")
public class DataPrivacyAdminControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @SpyBean
    private IInternalDataPrivacyService internalDataPrivacyServiceSpy;

    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;

    @Autowired
    private IFileStorage fileStorage;

    @Autowired
    private DataPrivacyDataCollectionRepository dataCollectionRepository;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createUserDataEntities(th.personRegularAnna);
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();

    }

    @Test
    public void dataPrivacyReport_Unauthorized() throws Exception {

        assertOAuth2(get("/administration/dataprivacy/report")
                .param("personId", th.personRegularAnna.getId())
                .param("check", "i am sure to delete this person"));
    }

    @Test
    public void dataPrivacyReport_NoPermission() throws Exception {

        mockMvc.perform(get("/administration/dataprivacy/report")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("personId", th.personRegularAnna.getId())
                .param("html", "true"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void dataPrivacyReport_NoException() throws Exception {

        mockMvc.perform(get("/administration/dataprivacy/report")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", th.personRegularAnna.getId())
                .param("html", "true"))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))));

        waitForEventProcessing();

        mockMvc.perform(get("/administration/dataprivacy/report")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", th.personRegularAnna.getId())
                .param("html", "false"))
                .andExpect(status().isOk())
                .andExpect(content().string(not(is(emptyString()))));

        waitForEventProcessing();
    }

    @Test
    public void sendDataPrivacyReportEmail_Unauthorized() throws Exception {
        assertOAuth2(post("/administration/dataprivacy/report/mail")
                .param("personId", th.personRegularAnna.getId()));
    }

    @Test
    public void sendDataPrivacyReportEmail_NoPermission() throws Exception {
        mockMvc.perform(post("/administration/dataprivacy/report/mail")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("personId", th.personRegularAnna.getId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void sendDataPrivacyReportEmail_NoException() throws Exception {

        final Person person = th.personRegularAnna;

        mockMvc.perform(post("/administration/dataprivacy/report/mail")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", person.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyString())));

        waitForEventProcessing();

        //verify that IDataPrivacyService#collectUserDataInternal has been called for the correct person
        Mockito.verify(internalDataPrivacyServiceSpy).collectInternalUserData(ArgumentMatchers.eq(person),
                Mockito.any());

        final String fromEmailAddress = dataPrivacyConfig.getSenderEmailAddressAccount();
        assertThat(emailSenderServiceMock.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(1);

        TestEmailSenderService.TestEmail mail = emailSenderServiceMock.getEmails().get(0);
        assertThat(mail.getSubject()).isEqualTo("Deine angeforderten Daten");
        assertThat(mail.getText()).contains(th.personRegularAnna.getFirstName());
        assertThat(mail.isHtml()).isTrue();

        assertTrue(fileStorage
                        .findAll(ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER)
                        .stream()
                        .anyMatch(fileName -> fileName.endsWith("/Datenschutzbericht.zip")),
                "file storage should contain Datenschutzbericht.zip");
    }

    @Test
    public void personDeletion_NoException() throws Exception {

        mockMvc.perform(delete("/administration/dataprivacy/person")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", th.personRegularAnna.getId())
                .param("check", "Yes, do it!"))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyString())));

        waitForEventProcessing();

        //verify that IDataPrivacyService#deletePerson has been called for the correct person
        Mockito.verify(internalDataPrivacyServiceSpy).deleteInternalUserData(
                ArgumentMatchers.eq(th.personRegularAnna), Mockito.any());

        //verify that in this case (admin triggered deletion) not mail has been sent
        assertThat(emailSenderServiceMock.getEmails()).isEmpty();
    }

    @Test
    public void personDeletion_WrongCheck() throws Exception {

        mockMvc.perform(delete("/administration/dataprivacy/person")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", th.personRegularAnna.getId())
                .param("check", "i don't know the check sentence"))
                .andExpect(isException(ClientExceptionType.WRONG_CHECK));
    }

    @Test
    public void personDeletion_Unauthorized() throws Exception {

        assertOAuth2(delete("/administration/dataprivacy/person")
                .param("personId", th.personRegularAnna.getId())
                .param("check", "i am sure to delete this person"));
    }

    @Test
    public void personDeletion_NoPermission() throws Exception {

        mockMvc.perform(delete("/administration/dataprivacy/person")
                .headers(authHeadersFor(th.personRegularKarl))
                .param("personId", th.personRegularAnna.getId())
                .param("check", "i am sure to delete this person"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void personDeletion_MissingRequiredParameters() throws Exception {

        mockMvc.perform(delete("/administration/dataprivacy/person")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("check", "i am sure to delete this person"))
                .andExpect(status().isBadRequest());

        mockMvc.perform(delete("/administration/dataprivacy/person")
                        .headers(authHeadersFor(th.personSuperAdmin))
                .param("personId", th.personRegularAnna.getId()))
                .andExpect(status().isBadRequest());
    }

}
