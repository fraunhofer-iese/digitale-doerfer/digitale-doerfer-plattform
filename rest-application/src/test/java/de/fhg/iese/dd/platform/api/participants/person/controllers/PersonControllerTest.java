/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;

public class PersonControllerTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;
    @Autowired
    protected IFileStorage fileStorage;

    @Override
    public void createEntities() {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void getOwnPerson() throws Exception {

        Person person = th.personRegularKarl;
        person.setVerificationStatus(0);
        person.getVerificationStatuses().addValue(PersonVerificationStatus.PHONE_NUMBER_VERIFIED);
        person = th.personRepository.saveAndFlush(person);
        Address address = person.getAddresses().iterator().next().getAddress();

        mockMvc.perform(get("/person")
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(person.getId()))
                .andExpect(jsonPath("$.firstName").value(person.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(person.getLastName()))
                .andExpect(jsonPath("$.nickName").value(person.getNickName()))
                .andExpect(jsonPath("$.email").value(person.getEmail()))
                .andExpect(jsonPath("$.phoneNumber").value(person.getPhoneNumber()))
                .andExpect(jsonPath("$.address.name").value(address.getName()))
                .andExpect(jsonPath("$.address.street").value(address.getStreet()))
                .andExpect(jsonPath("$.address.zip").value(address.getZip()))
                .andExpect(jsonPath("$.address.city").value(address.getCity()))
                .andExpect(jsonPath("$.homeAreaId").value(person.getHomeArea().getId()))
                .andExpect(jsonPath("$.verificationStatuses").value(
                        ClientPersonVerificationStatus.PHONE_NUMBER_VERIFIED.name()))
                .andReturn();
    }

    @Test
    public void getOwnPerson_Unauthorized() throws Exception {

        assertOAuth2(get("/person"));
    }

    @Test
    public void uploadProfilePicture() throws Exception {

        mockMvc.perform(multipart("/person/picture")
                .file(createTestPictureFile())
                .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(status().isCreated());

        final Person person1 = personRepository.findById(th.personVgAdmin.getId()).get();

        assertNotNull(person1.getProfilePicture());

        assertAllImageFilesExist(person1.getProfilePicture());
    }

    @Test
    public void deleteProfilePicture() throws Exception {

        final Person person = th.personVgAdmin;

        mockMvc.perform(multipart("/person/picture")
            .file(createTestPictureFile())
            .headers(authHeadersFor(person)))
            .andExpect(status().isCreated());

        Person person1 = personRepository.findById(person.getId()).get();
        assertNotNull(person1.getProfilePicture());

        final String profilePictureId = person1.getProfilePicture().getId();

        mockMvc.perform(delete("/person/picture")
                .headers(authHeadersFor(person)))
                .andExpect(status().isOk());

        person1 = personRepository.findById(person.getId()).get();
        assertNull(person1.getProfilePicture());
        assertFalse(th.mediaItemRepository.existsById(profilePictureId));
    }

    @Test
    public void changeProfilePicture() throws Exception {

       mockMvc.perform(multipart("/person/picture")
            .file(createTestPictureFile())
            .headers(authHeadersFor(th.personVgAdmin)))
            .andExpect(status().isCreated());

        Person person1 = personRepository.findById(th.personVgAdmin.getId()).get();

        assertNotEquals(person1.getProfilePicture(), null);
        assertAllImageFilesExist(person1.getProfilePicture());

        final String initialPictureId = person1.getProfilePicture().getId();

        mockMvc.perform(multipart("/person/picture")
                .file(createTestPictureFile())
                .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(status().isCreated());

        person1 = personRepository.findById(th.personVgAdmin.getId()).get();

        assertNotEquals(person1.getProfilePicture(), null);
        assertNotEquals(initialPictureId, person1.getProfilePicture().getId());
        assertAllImageFilesExist(person1.getProfilePicture());

        assertFalse(th.mediaItemRepository.existsById(initialPictureId));
    }

    @Test
    @Disabled("can not be tested due to a bug in the file upload mock which does not check the file size")
    public void uploadProfilePictureTooBig() throws Exception {

        mockMvc.perform(multipart("/person/picture")
                        .file(createTestPictureTooBigFile())
                        .headers(authHeadersFor(th.personVgAdmin)))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_UPLOAD_FAILED));
    }

    @Test
    public void uploadProfilePicture_Unauthorized() throws Exception {

        assertOAuth2(multipart("/person/picture")
                .file(createTestPictureFile()));
    }

    @Test
    public void getTenants() throws Exception {

        final Person person = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;

        mockMvc.perform(get("/person/tenants")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .apiKey(appVariant.getApiKey1())
                        .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]").value(person.getTenant().getId()));
    }

    @Test
    public void getTenants_Unauthorized() throws Exception {
        assertOAuth2ApiKeyRequired(get("/person/tenants"), th.app1Variant1.getApiKey1(), th.personRegularAnna);
    }

    private MockMultipartFile createTestPictureFile() throws IOException {
        InputStream is = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("testImage.jpg");
        byte[] imageBytes = IOUtils.toByteArray(is);
        return new MockMultipartFile("file", "testImage.jpg", "image/jpg", imageBytes);
    }

    private MockMultipartFile createTestPictureTooBigFile() throws IOException {
        InputStream is = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("testImageTooBig.jpg");
        byte[] imageBytes = IOUtils.toByteArray(is);
        return new MockMultipartFile("file", "testImage.jpg", "image/jpg", imageBytes);
    }

    private void assertAllImageFilesExist(MediaItem mediaItem) {
        assertTrue(mediaItem.getUrls().values().stream()
                .map(fileStorage::getInternalFileName)
                .allMatch(fileStorage::fileExists), "not all files were created");
    }

}
