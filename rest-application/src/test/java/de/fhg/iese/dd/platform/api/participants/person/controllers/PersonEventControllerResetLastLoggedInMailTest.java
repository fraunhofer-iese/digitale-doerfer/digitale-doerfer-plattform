/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonResetLastLoggedInRequest;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonResetLastLoggedInRequest;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.config.ParticipantsConfig;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;

public class PersonEventControllerResetLastLoggedInMailTest extends BaseServiceTest {

    @Autowired
    private ParticipantsTestHelper th;

    @Autowired
    private IPersonService personService;

    @Autowired
    private IAuthorizationService authorizationService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ParticipantsConfig participantsConfig;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void personResetLastLoggedInRequestMail() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person personToReset = th.personRegularAnna;

        personToReset.getStatuses().addValue(PersonStatus.PENDING_DELETION);
        personToReset.setLastLoggedIn(nowTime - (participantsConfig.getLastLoggedInUpdateThreshold().toMillis() + 10L));
        personToReset = personRepository.saveAndFlush(personToReset);

        mockMvc.perform(buildResetRequestMail(personToReset, authorizationService.generateAuthToken(
                new PersonResetLastLoggedInRequest(personToReset),
                Duration.ofMinutes(5))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        final Person personAfterReset = th.personRepository.findById(personToReset.getId()).get();

        assertThat(personAfterReset.getStatuses().hasValue(PersonStatus.PENDING_DELETION)).isFalse();
        assertEquals(nowTime, personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_personDeleted() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person personToReset = th.personDeleted;
        personToReset.setLastLoggedIn(nowTime - (participantsConfig.getLastLoggedInUpdateThreshold().toMillis() + 10L));
        personToReset = personRepository.saveAndFlush(personToReset);

        mockMvc.perform(buildResetRequestMail(personToReset, authorizationService.generateAuthToken(
                new PersonResetLastLoggedInRequest(personToReset),
                Duration.ofMinutes(5))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        final Person personAfterReset = personService.findPersonById(personToReset.getId());

        // Request can be executed, but should never re-animate the person #NoZombies
        assertThat(personAfterReset.isDeleted()).isTrue();
        assertEquals(nowTime, personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_personBlocked() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person personToReset = th.personRegularAnna;

        personToReset.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        personToReset.setLastLoggedIn(nowTime - (participantsConfig.getLastLoggedInUpdateThreshold().toMillis() + 10L));
        personToReset = personRepository.saveAndFlush(personToReset);

        mockMvc.perform(buildResetRequestMail(personToReset, authorizationService.generateAuthToken(
                new PersonResetLastLoggedInRequest(personToReset),
                Duration.ofMinutes(5))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        final Person personAfterReset = personService.findPersonById(personToReset.getId());

        // Request can be executed, but should never unblock the person
        assertThat(personAfterReset.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED)).isTrue();
        assertEquals(nowTime, personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_personDeletedBlocked() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Person personToReset = th.personDeleted;

        personToReset.getStatuses().addValue(PersonStatus.LOGIN_BLOCKED);
        personToReset.setLastLoggedIn(nowTime - (participantsConfig.getLastLoggedInUpdateThreshold().toMillis() + 10L));
        personToReset = personRepository.saveAndFlush(personToReset);

        mockMvc.perform(buildResetRequestMail(personToReset, authorizationService.generateAuthToken(
                new PersonResetLastLoggedInRequest(personToReset),
                Duration.ofMinutes(5))))
                .andExpect(status().isOk());

        waitForEventProcessing();

        final Person personAfterReset = personService.findPersonById(personToReset.getId());

        // Request can be executed, but should never re-animate or unblock the person #NoZombies
        assertThat(personAfterReset.isDeleted()).isTrue();
        assertThat(personAfterReset.getStatuses().hasValue(PersonStatus.LOGIN_BLOCKED)).isTrue();
        assertEquals(nowTime, personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_wrongMailToken() throws Exception {

        final Person personToReset = th.personRegularAnna;

        mockMvc.perform(buildResetRequestMail(personToReset, "wrong-mailToken"))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        final Person personAfterReset = personService.findPersonById(personToReset.getId());

        assertEquals(personToReset.getLastLoggedIn(), personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_wrongMailTokenFromAnotherPerson() throws Exception {

        final Person personToReset = th.personRegularAnna;
        final Person personForMailToken = th.personRegularKarl;

        mockMvc.perform(buildResetRequestMail(personToReset, authorizationService.generateAuthToken(
                new PersonResetLastLoggedInRequest(personForMailToken),
                Duration.ofMinutes(5))))
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        final Person personAfterReset = personService.findPersonById(personToReset.getId());

        assertEquals(personToReset.getLastLoggedIn(), personAfterReset.getLastLoggedIn());
    }

    @Test
    public void personResetLastLoggedInRequestMail_wrongPersonId() throws Exception {

        final Person personToReset = th.personRegularAnna;

        mockMvc.perform(post("/person/event/personResetLastLoggedInRequest/mail")
                .contentType(contentType)
                .param("mailToken", authorizationService.generateAuthToken(
                        new PersonResetLastLoggedInRequest(personToReset),
                        Duration.ofMinutes(5)))
                .content(json(new ClientPersonResetLastLoggedInRequest("wrong-personId"))))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_FOUND));

        mockMvc.perform(post("/person/event/personResetLastLoggedInRequest/mail")
                .contentType(contentType)
                .param("mailToken", authorizationService.generateAuthToken(
                        new PersonResetLastLoggedInRequest(personToReset),
                        Duration.ofMinutes(5)))
                .content(json(new ClientPersonResetLastLoggedInRequest(null))))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        mockMvc.perform(post("/person/event/personResetLastLoggedInRequest/mail")
                .contentType(contentType)
                .param("mailToken", authorizationService.generateAuthToken(
                        new PersonResetLastLoggedInRequest(personToReset),
                        Duration.ofMinutes(5)))
                .content(json(new ClientPersonResetLastLoggedInRequest(""))))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    private MockHttpServletRequestBuilder buildResetRequestMail(Person person, String mailToken) throws IOException {
        return post("/person/event/personResetLastLoggedInRequest/mail")
                .contentType(contentType)
                .param("mailToken", mailToken)
                .content(json(new ClientPersonResetLastLoggedInRequest(person.getId())));
    }

}
