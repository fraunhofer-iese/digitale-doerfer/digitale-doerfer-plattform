/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.group.ClientGroupFlagRequest;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent.ClientUserGeneratedContentFlagResponse;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;

public class GroupEventControllerFlagTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private UserGeneratedContentFlagRepository flagRepository;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupsAndGroupPostsWithComments();
        th.createGroupFeatureConfiguration();
        th.printAvailableGroups();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void flagGroup() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        //this is the newest group admin
        Person expectedGroupAdmin = th.personChloeTenant1Dorf1;
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;
        th.assignRoleToPerson(expectedGroupAdmin, GroupMembershipAdmin.class, flaggedEntity.getId());

        th.printGroupSettings(flagCreator);

        MvcResult flagRequestResult = mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(ClientGroupFlagRequest.builder()
                                .groupId(flaggedEntity.getId())
                                .comment("Das ist aber eine hässliche Gruppe!")
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(flagRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedEntity.getTenant().getId());
        assertTrue(flagRepository.existsById(response.getUserGeneratedContentFlagId()));
        UserGeneratedContentFlag flag = flagRepository.findById(response.getUserGeneratedContentFlagId()).get();
        assertThat(flag.getEntityAuthor()).isEqualTo(expectedGroupAdmin);
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        mockMvc.perform(get("/grapevine/group/" + flaggedEntity.getId())
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagGroup_NoGroupAdmin() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;

        th.printGroupSettings(flagCreator);

        MvcResult flagRequestResult = mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(ClientGroupFlagRequest.builder()
                                .groupId(flaggedEntity.getId())
                                .comment("Das ist aber eine hässliche Gruppe!")
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(flagRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedEntity.getTenant().getId());
        assertTrue(flagRepository.existsById(response.getUserGeneratedContentFlagId()));
        UserGeneratedContentFlag flag = flagRepository.findById(response.getUserGeneratedContentFlagId()).get();
        assertThat(flag.getEntityAuthor()).isNull();
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        mockMvc.perform(get("/grapevine/group/" + flaggedEntity.getId())
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagGroup_NoComment() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;

        th.printGroupSettings(flagCreator);

        MvcResult flagRequestResult = mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(ClientGroupFlagRequest.builder()
                                .groupId(flaggedEntity.getId())
                                .build())))
                .andExpect(status().isOk())
                .andReturn();
        final ClientUserGeneratedContentFlagResponse response = toObject(flagRequestResult.getResponse(),
                ClientUserGeneratedContentFlagResponse.class);

        assertThat(response.getEntityId()).isEqualTo(flaggedEntity.getId());
        assertThat(response.getEntityType()).isEqualTo(flaggedEntity.getClass().getName());
        assertThat(response.getFlagCreatorId()).isEqualTo(flagCreator.getId());
        assertThat(response.getTenantId()).isEqualTo(flaggedEntity.getTenant().getId());
        assertTrue(flagRepository.existsById(response.getUserGeneratedContentFlagId()));
        UserGeneratedContentFlag flag = flagRepository.findById(response.getUserGeneratedContentFlagId()).get();
        assertThat(flag.getEntityAuthor()).isNull();
        //no further checks needed, since UserGeneratedContentFlagServiceTest tests correct functionality of service
        //and db constraints assure that there is only one entry in the UserGeneratedContentFlag flag table
        //with the given entity id, type and flag creator (namely the entry with response.getUserGeneratedContentFlagId())

        mockMvc.perform(get("/grapevine/group/" + flaggedEntity.getId())
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.FLAGGED.name()));
    }

    @Test
    public void flagGroup_InvisibleGroup() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personDagmarTenant2Mainz;

        mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(ClientGroupFlagRequest.builder()
                                .groupId(flaggedEntity.getId())
                                .comment("Das seh ich net!!!")
                                .build())))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void flagGroup_MultipleTimes() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;

        ClientGroupFlagRequest flagRequest = ClientGroupFlagRequest.builder()
                .groupId(flaggedEntity.getId())
                .comment("Das ist aber eine hässliche Gruppe!")
                .build();
        mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(status().isOk());

        mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(isException(ClientExceptionType.USER_GENERATED_CONTENT_FLAG_ALREADY_EXISTS));
    }

    @Test
    public void flagGroup_ThatHasBeenDeleted() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        flaggedEntity.setDeleted(true);
        th.groupRepository.saveAndFlush(flaggedEntity);
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;

        ClientGroupFlagRequest flagRequest = ClientGroupFlagRequest.builder()
                .groupId(flaggedEntity.getId())
                .comment("Die gibt es doch gar nimmmmmer!")
                .build();

        mockMvc.perform(post("/grapevine/group/event/groupFlagRequest")
                        .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                        .contentType(contentType)
                        .content(json(flagRequest)))
                .andExpect(isException(ClientExceptionType.GROUP_NOT_FOUND));
    }

    @Test
    public void flagGroup_Unauthorized() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personDagmarTenant2Mainz;

        assertOAuth2AppVariantRequired(post("/grapevine/group/event/groupFlagRequest")
                .contentType(contentType)
                .content(json(ClientGroupFlagRequest.builder()
                        .groupId(flaggedEntity.getId())
                        .comment("Das seh ich net!!!")
                        .build())), th.appVariantKL_EB, flagCreator);

    }

    @Test
    public void flagGroup_WrongOrEmptyEventAttributes() throws Exception {

        Group flaggedEntity = th.groupDorfgeschichteSSA;
        Person flagCreator = th.personEckhardTenant1Dorf1NoMember;

        ClientGroupFlagRequest flagRequest = ClientGroupFlagRequest.builder()
                .groupId(flaggedEntity.getId())
                .comment("Ich probier mal emojis 💥")
                .build();

        assertWrongOrEmptyEventAttributesAreInvalid(post("/grapevine/group/event/groupFlagRequest")
                .headers(authHeadersFor(flagCreator, th.appVariantKL_EB))
                .contentType(contentType)
                .content(json(flagRequest)), flagRequest);

    }

}
