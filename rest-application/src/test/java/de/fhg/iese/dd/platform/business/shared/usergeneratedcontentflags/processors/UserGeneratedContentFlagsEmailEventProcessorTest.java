/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.processors;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;

public class UserGeneratedContentFlagsEmailEventProcessorTest extends BaseServiceTest {

    //set to true if you want to inspect the actual email html. Then watch test case log for the output file name
    private static final boolean DUMP_MAILS_TO_TMP = false;

    @Autowired
    private UserGeneratedContentFlagsEmailEventProcessor userGeneratedContentFlagsEmailEventProcessor;

    @Autowired
    private TestEmailSenderService emailSenderService;

    @Autowired
    private ParticipantsTestHelper th;

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_MAILS_TO_TMP) {
            deleteTempFilesStartingWithClassName(UserGeneratedContentFlagsEmailEventProcessorTest.class);
        }
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();

        emailSenderService.clearMailList();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @AfterEach
    public void dumpEmailsToTemp(TestInfo testInfo) throws Exception {
        if (DUMP_MAILS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-" + testInfo.getDisplayName() + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile);
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Test
    public void onUserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation() {
        final Person author = th.personRegularKarl;
        final UserGeneratedContentFlag changedFlag = UserGeneratedContentFlag.builder()
                .entityId("ID")
                .entityType("Type")
                .entityAuthor(author)
                .flagCreator(th.personUserGeneratedContentAdminTenant1)
                .status(UserGeneratedContentFlagStatus.OPEN)
                .lastStatusUpdate(th.nextTimeStamp())
                .tenant(th.tenant1)
                .build();

        userGeneratedContentFlagsEmailEventProcessor.onUserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation(
                UserGeneratedContentFlagDeleteFlagEntityByAdminConfirmation.builder()
                        .changedFlag(changedFlag)
                        .flagReport(DeletedFlagReport.builder()
                                .appName("Lösbar")
                                .reportedText("Das ist ein böser Text, aber dieses Mal viel schlimmer als vorher!!! ⚗️")
                                .fromEmailAddressUserNotification("funk@digitale-doerfer.de")
                                .build())
                        .build());

        final String fromEmailAddress = "funk@digitale-doerfer.de";
        assertThat(emailSenderService.getMailsByFromAndToRecipient(fromEmailAddress, author.getEmail())).hasSize(1);
    }

}
