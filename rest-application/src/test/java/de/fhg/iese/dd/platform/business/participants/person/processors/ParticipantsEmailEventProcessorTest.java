/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.participants.person.processors;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.participants.ParticipantsTestHelper;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonCreateConfirmation;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonEmailVerificationFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;

public class ParticipantsEmailEventProcessorTest extends BaseServiceTest {

    //set to true if you want to inspect the actual email html. Then watch test case log for the output file name
    private static final boolean DUMP_MAILS_TO_TMP = false;

    @Autowired
    private ParticipantsEmailEventProcessor participantsEmailEventProcessor;
    @Autowired
    private TestEmailSenderService emailSenderService;
    @Autowired
    private ParticipantsTestHelper th;

    @BeforeAll
    public static void deleteTempFiles() throws IOException {
        if (DUMP_MAILS_TO_TMP) {
            deleteTempFilesStartingWithClassName(ParticipantsEmailEventProcessorTest.class);
        }
    }

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPersons();

        emailSenderService.clearMailList();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @AfterEach
    public void dumpEmailsToTemp(TestInfo testInfo) throws Exception {
        if (DUMP_MAILS_TO_TMP) {
            for (TestEmailSenderService.TestEmail email : emailSenderService.getEmails()) {
                final Path tempFile =
                        Files.createTempFile(getClass().getSimpleName() + "-" + testInfo.getDisplayName() + "-",
                                ".html");
                log.info("Saving {} to {}", email.toString(), tempFile);
                Files.write(tempFile, email.getText().getBytes(StandardCharsets.UTF_8));
            }
        }
    }

    @Test
    public void onPersonCreatConfirmation_FeatureEnabled() {

        final Person personEnabled = th.personRegularKarl;
        final Person personDisabled = th.personRegularAnna;
        final Tenant tenantEnabled = th.tenant1;
        final Tenant tenantDisabled = th.tenant2;

        personEnabled.setTenant(tenantEnabled);
        personDisabled.setTenant(tenantDisabled);

        th.personRepository.save(personEnabled);
        th.personRepository.save(personDisabled);

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .enabled(true)
                .build());

        featureConfigRepository.save(FeatureConfig.builder()
                .featureClass(PersonEmailVerificationFeature.class.getName())
                .tenant(tenantDisabled)
                .enabled(false)
                .build());

        participantsEmailEventProcessor.onPersonCreateConfirmation(new PersonCreateConfirmation(personEnabled));
        participantsEmailEventProcessor.onPersonCreateConfirmation(new PersonCreateConfirmation(personDisabled));

        final String fromEmailAddress = "zugang@digitale-doerfer.de";
        assertThat(emailSenderService.getMailsByFromAndToRecipient(fromEmailAddress, personEnabled.getEmail()))
                .hasSize(1)
                .extracting(TestEmailSenderService.TestEmail::getRecipientEmailAddress)
                .contains(personEnabled.getEmail());
    }

    @Test
    public void onPersonCreatConfirmation_FeatureDefaultDisabled() {
        final Person person = th.personRegularKarl;

        participantsEmailEventProcessor.onPersonCreateConfirmation(new PersonCreateConfirmation(person));

        final String fromEmailAddress = "zugang@digitale-doerfer.de";
        assertThat(emailSenderService.getMailsByFromAndToRecipient(fromEmailAddress, person.getEmail())).hasSize(0);
    }

}
