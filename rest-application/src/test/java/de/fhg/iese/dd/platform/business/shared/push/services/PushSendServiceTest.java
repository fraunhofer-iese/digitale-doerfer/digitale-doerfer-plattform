/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.push.services;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.business.shared.push.providers.TestExternalPushProvider;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.personblocking.model.PersonBlocking;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.push.config.PushConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PushSendServiceTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @SpyBean
    private TestExternalPushProvider externalPushProvider;

    @Autowired
    private IPushSendService pushSendService;

    @Autowired
    private PushConfig pushConfig;

    private Person personApp1Variant1and2;
    private AppVariantUsage personApp1Variant1and2_Usage1;
    private Person personApp1Variant1andApp2Variant1;

    private GeoArea selectedGeoArea;
    private GeoArea selectedGeoArea_WithoutTenant;

    private final PushMessage pushMessage = PushMessage.builder()
            .logMessage("test-push-message")
            .jsonMessageLoudFcm("{\"message\": \"loudFcm\"")
            .jsonMessageSilentFcm("{\"message\": \"silendFcm\"")
            .jsonMessageLoudApns("{\"message\": \"loudApns\"")
            .jsonMessageSilentApns("{\"message\": \"silentApns\"")
            .build();

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();

        selectedGeoArea = th.geoAreaEisenberg;
        selectedGeoArea_WithoutTenant = th.geoAreaDeutschland;
        Tenant tenant = th.tenant1;
        th.mapGeoAreaToAppVariant(th.app1Variant1, th.geoAreaTenant1, tenant);
        th.mapGeoAreaToAppVariant(th.app1Variant2, th.geoAreaTenant1, tenant);
        th.mapGeoAreaToAppVariant(th.app2Variant1, th.geoAreaTenant1, tenant);

        personApp1Variant1and2 = th.personRegularKarl;

        personApp1Variant1and2_Usage1 = th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(th.app1Variant1)
                .person(personApp1Variant1and2)
                .lastUsage(th.nextTimeStamp())
                .build());

        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(th.app1Variant2)
                .person(personApp1Variant1and2)
                .lastUsage(th.nextTimeStamp())
                .build());

        th.selectGeoAreasForAppVariant(th.app1Variant1, personApp1Variant1and2,
                selectedGeoArea, selectedGeoArea_WithoutTenant);
        th.selectGeoAreasForAppVariant(th.app1Variant2, personApp1Variant1and2,
                selectedGeoArea, selectedGeoArea_WithoutTenant);

        personApp1Variant1andApp2Variant1 = th.personRegularAnna;

        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(th.app1Variant1)
                .person(personApp1Variant1andApp2Variant1)
                .lastUsage(th.nextTimeStamp())
                .build());

        th.appVariantUsageRepository.saveAndFlush(AppVariantUsage.builder()
                .appVariant(th.app2Variant1)
                .person(personApp1Variant1andApp2Variant1)
                .lastUsage(th.nextTimeStamp())
                .build());

        th.selectGeoAreasForAppVariant(th.app1Variant1, personApp1Variant1andApp2Variant1,
                selectedGeoArea, selectedGeoArea_WithoutTenant);
        th.selectGeoAreasForAppVariant(th.app2Variant1, personApp1Variant1andApp2Variant1,
                selectedGeoArea, selectedGeoArea_WithoutTenant);

        appService.invalidateCache();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void pushMessageToPersonLoud_NoUserSettings() {

        //no user settings available
        assertThat(th.pushCategoryUserSettingRepository.count()).isZero();

        pushSendService.pushMessageToPersonLoud(personApp1Variant1and2, pushMessage, th.pushCategory1App1);

        Mockito.verify(externalPushProvider).sendMessageToPerson(
                new HashSet<>(Arrays.asList(th.app1Variant1, th.app1Variant2)),
                personApp1Variant1and2,
                pushMessage,
                true);
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.emptySet(),
                personApp1Variant1and2,
                pushMessage,
                false);
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToPersonLoud_WithUserSettings() {

        th.pushCategoryUserSettingRepository.save(PushCategoryUserSetting.builder()
                .loudPushEnabled(false)
                .person(personApp1Variant1and2)
                .pushCategory(th.pushCategory1App1)
                .appVariant(th.app1Variant2)
                .build());

        pushSendService.pushMessageToPersonLoud(personApp1Variant1and2, pushMessage, th.pushCategory1App1);

        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant1),
                personApp1Variant1and2,
                pushMessage,
                true);
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant2),
                personApp1Variant1and2,
                pushMessage,
                false);
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToPersonInLastUsedAppVariantLoud_NoUserSettings() {

        //no user settings available
        assertThat(th.pushCategoryUserSettingRepository.count()).isZero();

        pushSendService.pushMessageToPersonInLastUsedAppVariantLoud(personApp1Variant1and2, pushMessage,
                Collections.singleton(th.pushCategory1App1));

        //loud to more recently used app variant
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant2),
                personApp1Variant1and2,
                pushMessage,
                true);
        //silent to other app variant
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant1),
                personApp1Variant1and2,
                pushMessage,
                false);
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToPersonInLastUsedAppVariantLoud_WithUserSettings() {

        th.pushCategoryUserSettingRepository.save(PushCategoryUserSetting.builder()
                .loudPushEnabled(false)
                .person(personApp1Variant1and2)
                .pushCategory(th.pushCategory1App1)
                .appVariant(th.app1Variant2)
                .build());

        pushSendService.pushMessageToPersonInLastUsedAppVariantLoud(personApp1Variant1and2, pushMessage,
                Collections.singleton(th.pushCategory1App1));

        //loud to most recently used app variant with loud push configured
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant1),
                personApp1Variant1and2,
                pushMessage,
                true);
        //silent to other app variant
        Mockito.verify(externalPushProvider).sendMessageToPerson(
                Collections.singleton(th.app1Variant2),
                personApp1Variant1and2,
                pushMessage,
                false);
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToPersonSilent() {

        pushSendService.pushMessageToPersonSilent(personApp1Variant1andApp2Variant1, pushMessage,
                new HashSet<>(Arrays.asList(th.pushCategory1App1, th.pushCategory1App2)));

        Mockito.verify(externalPushProvider).sendMessageToPerson(
                new HashSet<>(Arrays.asList(th.app1Variant1, th.app2Variant1)),
                personApp1Variant1andApp2Variant1,
                pushMessage,
                false);
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaLoud_WithoutAuthor_WithoutTenant() {

        pushSendService.pushMessageToGeoAreasLoud(null, Set.of(selectedGeoArea_WithoutTenant), false, pushMessage,
                th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .loud(personApp1Variant1andApp2Variant1)
                .loud(personApp1Variant1and2)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .loud(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaLoud_WithoutAuthor() {

        pushSendService.pushMessageToGeoAreasLoud(null, Set.of(selectedGeoArea), false, pushMessage,
                th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .loud(personApp1Variant1andApp2Variant1)
                .loud(personApp1Variant1and2)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .loud(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaLoud_WithAuthor() {

        pushSendService.pushMessageToGeoAreasLoud(personApp1Variant1and2, Set.of(selectedGeoArea), false, pushMessage,
                th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .loud(personApp1Variant1andApp2Variant1)
                .silent(personApp1Variant1and2)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .silent(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaLoud_TooOldAppVariantUsage() {

        GeoArea geoArea = selectedGeoArea;

        //make the usage of app variant 1 older than the push last usage time
        personApp1Variant1and2_Usage1.setLastUsage(
                System.currentTimeMillis() - (pushConfig.getLastAppVariantUsageTime().toMillis() + 1));
        th.appVariantUsageRepository.saveAndFlush(personApp1Variant1and2_Usage1);

        pushSendService.pushMessageToGeoAreasLoud(null, Set.of(geoArea), false, pushMessage, th.pushCategory1App1);

        //the usage is too old, so the person should not get the push message
        assertPush().toAppVariant(th.app1Variant1)
                .loud(personApp1Variant1andApp2Variant1)
                .check();

        //the usage is fresh enough
        assertPush().toAppVariant(th.app1Variant2)
                .loud(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaSilent() {

        pushSendService.pushMessageToGeoAreasSilent(Set.of(selectedGeoArea), false, pushMessage, th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .silent(personApp1Variant1and2)
                .silent(personApp1Variant1andApp2Variant1)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .silent(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaSilent_WithoutTenant() {

        pushSendService.pushMessageToGeoAreasSilent(Set.of(selectedGeoArea_WithoutTenant), false, pushMessage,
                th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .silent(personApp1Variant1and2)
                .silent(personApp1Variant1andApp2Variant1)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .silent(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaSilent_OnlyForSameHomeArea_True() {

        personApp1Variant1and2.setHomeArea(selectedGeoArea);
        personApp1Variant1andApp2Variant1.setHomeArea(th.geoAreaKaiserslautern);
        th.personRepository.saveAndFlush(personApp1Variant1and2);
        th.personRepository.saveAndFlush(personApp1Variant1andApp2Variant1);
        assertThat(personApp1Variant1and2.getHomeArea()).isNotEqualTo(personApp1Variant1andApp2Variant1.getHomeArea());

        pushSendService.pushMessageToGeoAreasSilent(Set.of(selectedGeoArea), true, pushMessage, th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .silent(personApp1Variant1and2)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .silent(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoAreaSilent_OnlyForSameHomeArea_False() {

        pushSendService.pushMessageToGeoAreasSilent(Set.of(selectedGeoArea), false, pushMessage, th.pushCategory1App1);

        assertPush().toAppVariant(th.app1Variant1)
                .silent(personApp1Variant1and2)
                .silent(personApp1Variant1andApp2Variant1)
                .check();
        assertPush().toAppVariant(th.app1Variant2)
                .silent(personApp1Variant1and2)
                .check();
        assertNoMorePushes();
    }

    @Test
    public void pushMessageToGeoArea_MultipleCases() {

        //this test uses a different setup for historic reasons
        //since it is well understandable it will not be merged into the other test cases
        //the test cases below slightly overlap with the other test cases in this class
        final AppVariant appVariant = th.app1Variant1;
        th.unmapAllGeoAreasFromAppVariant(th.app1Variant2);
        th.unmapAllGeoAreasFromAppVariant(th.app2Variant1);
        th.unmapAllGeoAreasFromAppVariant(th.app2Variant2);

        final GeoArea areaOfInterest = th.geoAreaDorf2InEisenberg;
        final GeoArea areaOfNoInterest = th.geoAreaKaiserslautern;
        assertThat(areaOfInterest).isNotEqualTo(areaOfNoInterest);
        assertThat(areaOfInterest).isIn(appService.getAvailableGeoAreas(appVariant));
        assertThat(areaOfNoInterest).isIn(appService.getAvailableGeoAreas(appVariant));

        final PushCategory categoryLoudPush = th.pushCategory1App1;
        assertTrue(th.pushCategory1App1.isDefaultLoudPushEnabled());
        final PushCategory categorySilentPush = pushCategoryRepository.save(PushCategory.builder()
                .app(th.app1)
                .name("pushCategory_app1_4")
                .description("pushCategory_app1_4 description")
                .defaultLoudPushEnabled(false)
                .configurable(true)
                .build());
        assertFalse(categorySilentPush.isDefaultLoudPushEnabled());
        final Person receiverNoUserSettings = changeHomeArea(th.personRegularAnna, areaOfInterest);
        final Person receiverWithUserSettings = changeHomeArea(th.personVgAdmin, areaOfInterest);
        final Person receiverOtherHomeArea = changeHomeArea(th.personIeseAdmin, areaOfNoInterest);
        final Person otherPerson = th.personRegularKarl;
        assertThat(receiverNoUserSettings).isNotEqualTo(receiverWithUserSettings);
        assertThat(receiverOtherHomeArea).isNotEqualTo(receiverWithUserSettings);
        assertThat(receiverOtherHomeArea).isNotEqualTo(receiverNoUserSettings);
        assertThat(receiverOtherHomeArea).isNotEqualTo(receiverWithUserSettings);
        assertThat(otherPerson).isNotEqualTo(receiverNoUserSettings);
        assertThat(otherPerson).isNotEqualTo(receiverWithUserSettings);
        assertThat(otherPerson).isNotEqualTo(receiverOtherHomeArea);

        //select areaOfInterest for receivers
        appService.selectGeoAreas(appVariant, receiverNoUserSettings, Collections.singleton(areaOfInterest));
        appService.selectGeoAreas(appVariant, receiverWithUserSettings, Collections.singleton(areaOfInterest));
        appService.selectGeoAreas(appVariant, receiverOtherHomeArea, Collections.singleton(areaOfInterest));
        appService.selectGeoAreas(appVariant, otherPerson, Collections.singleton(areaOfNoInterest));

        //change category settings
        configurePushCategoryUserSetting(categoryLoudPush, receiverWithUserSettings, appVariant, false);
        configurePushCategoryUserSetting(categorySilentPush, receiverWithUserSettings, appVariant, true);

        //Note that in all tests, it is tested that otherPerson never gets push messages (which is correct
        //since this person has selected a different geo area)

        //====== TEST SET 0 ==========================================================================================

        //receiverNoUserSettings normally would get a loud push (see test set 1), but does not get one in this
        //case since this person is author
        pushSendService.pushMessageToGeoAreasLoud(receiverNoUserSettings, Set.of(areaOfInterest), false, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();

        pushSendService.pushMessageToGeoAreasLoud(receiverNoUserSettings, Set.of(areaOfInterest), true, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), true, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();

        //====== TEST SET 1 ==========================================================================================

        //category with default loud push, no blockings, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverNoUserSettings)
                .loud(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .check();

        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), true, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), true, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();

        //same test, but testing that author == null is allowed and produces correct sets
        pushSendService.pushMessageToGeoAreasLoud(null, Set.of(areaOfInterest), false, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverNoUserSettings)
                .loud(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverOtherHomeArea)
                .silent(receiverWithUserSettings)
                .check();

        pushSendService.pushMessageToGeoAreasLoud(null, Set.of(areaOfInterest), true, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), true, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();

        //category with default silent push, no blockings, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .silent(receiverOtherHomeArea)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .silent(receiverOtherHomeArea)
                .check();

        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), true, pushMessage,
                categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), true, pushMessage, categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();

        //====== TEST SET 2 =============================================================================
        //receiverOtherHomeArea now does not receive any push notifications anymore
        appService.selectGeoAreas(appVariant, receiverOtherHomeArea, Collections.singleton(areaOfNoInterest));
        //receiverNoUserSettings now blocks 'otherPerson'
        th.personBlockingRepository.save(PersonBlocking.builder()
                .app(appVariant.getApp())
                .blockingPerson(receiverNoUserSettings)
                .blockedPerson(otherPerson).build());

        //category with default loud push, receiverNoUserSettings block the author, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();

        //category with default silent push, receiverNoUserSettings block the author, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverWithUserSettings)
                .silent(receiverNoUserSettings)
                .check();

        //====== TEST SET 3 =============================================================================
        //receiverWithUserSettings now blocks 'otherPerson'
        th.personBlockingRepository.deleteAll();
        th.personBlockingRepository.save(PersonBlocking.builder()
                .app(appVariant.getApp())
                .blockingPerson(receiverWithUserSettings)
                .blockedPerson(otherPerson).build());

        //category with default loud push, receiverWithUserSettings blocks the author, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .loud(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categoryLoudPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();

        //category with default silent push, receiverWithUserSettings blocks the author, receivers are not the author
        pushSendService.pushMessageToGeoAreasLoud(otherPerson, Set.of(areaOfInterest), false, pushMessage,
                categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();
        pushSendService.pushMessageToGeoAreasSilent(Set.of(areaOfInterest), false, pushMessage, categorySilentPush);
        assertOnlyPush().toAppVariant(appVariant)
                .silent(receiverNoUserSettings)
                .silent(receiverWithUserSettings)
                .check();
    }

    private void assertNoMorePushes() {
        Mockito.verifyNoMoreInteractions(externalPushProvider);
    }

    private AssertPush.AssertPushBuilder assertPush() {
        return AssertPush.assertThat().pushProvider(externalPushProvider).pushes(pushMessage);
    }

    private AssertPush.AssertPushBuilder assertOnlyPush() {
        return AssertPush.assertThat().pushProvider(externalPushProvider).pushes(
                pushMessage).verifyNoMorePushesAndReset(true);
    }

    private static class AssertPush {

        @Builder(buildMethodName = "check", builderMethodName = "assertThat")
        public AssertPush(
                @NonNull TestExternalPushProvider pushProvider,
                @NonNull AppVariant toAppVariant,
                @Singular("loud") List<Person> loudPersons,
                @Singular("silent") List<Person> silentPersons,
                @NonNull PushMessage pushes,
                boolean verifyNoMorePushesAndReset) {
            Mockito.verify(pushProvider).sendMessageToPersons(
                    Mockito.eq(toAppVariant),
                    Mockito.argThat(new ContainsExactlyAnyOrder(loudPersons.stream()
                            .map(Person::getId)
                            .collect(Collectors.toList()))),
                    Mockito.argThat(new ContainsExactlyAnyOrder(silentPersons.stream()
                            .map(Person::getId)
                            .collect(Collectors.toList()))),
                    Mockito.eq(pushes));
            if (verifyNoMorePushesAndReset) {
                Mockito.verifyNoMoreInteractions(pushProvider);
                Mockito.reset(pushProvider);
            }
        }

    }

    private Person changeHomeArea(Person person, GeoArea newHomeArea) {
        person.setHomeArea(newHomeArea);
        return th.personRepository.save(person);
    }

    private void configurePushCategoryUserSetting(PushCategory category, Person person, AppVariant appVariant,
            boolean loudPush) {

        PushCategoryUserSetting userSetting = th.pushCategoryUserSettingRepository
                .findByPushCategoryAndPersonAndAppVariant(category, person, appVariant);
        if (userSetting == null) {
            userSetting = PushCategoryUserSetting.builder()
                    .person(person)
                    .pushCategory(category)
                    .appVariant(appVariant)
                    .build();
        }
        userSetting.setLoudPushEnabled(loudPush);
        th.pushCategoryUserSettingRepository.save(userSetting);
    }

    private static class ContainsExactlyAnyOrder implements ArgumentMatcher<Collection<String>> {

        private final Collection<String> expected;

        private ContainsExactlyAnyOrder(Collection<String> expected) {
            this.expected = expected;
        }

        @Override
        public boolean matches(Collection<String> argument) {
            return argument.size() == expected.size() && argument.containsAll(expected);
        }

        @Override
        public String toString() {
            return "contains exactly any order " + expected;
        }

    }

}
