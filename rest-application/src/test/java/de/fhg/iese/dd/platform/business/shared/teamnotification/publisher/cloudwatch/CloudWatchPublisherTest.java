/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.teamnotification.publisher.cloudwatch;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import de.fhg.iese.dd.platform.api.AssumeIntegrationTestExtension;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.AWSConfig;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.DeleteLogGroupRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetLogEventsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetLogEventsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.OutputLogEvent;

//the special profile is required to avoid having the cloud watch publisher running every time we test with aws enabled
@ActiveProfiles({"test", "test-aws", "integration-test", "CloudWatchPublisherTest"})
@ExtendWith(AssumeIntegrationTestExtension.class)
public class CloudWatchPublisherTest extends BaseServiceTest {

    @Autowired
    private AWSConfig awsConfig;
    @Autowired
    private IEnvironmentService environmentService;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private CloudWatchTeamNotificationPublisher cloudWatchTeamNotificationPublisher;

    private String logGroupName;
    private String logStreamName;
    private CloudWatchLogsClient cloudWatchClient;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();

        cloudWatchClient = createCloudWatchLogClient();

        logGroupName = awsConfig.getCloudWatch().getTeamNotification().getLogGroup();
        logStreamName = environmentService.getNodeIdentifier();
        //see tear down for an explanation
        cloudWatchTeamNotificationPublisher.setEnabled(true);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
        deleteTestLogGroupAndStream();
        //this is a dirty hack to avoid having the shutdown log message sent to the already deleted log stream
        cloudWatchTeamNotificationPublisher.setEnabled(false);
    }

    @Test
    public void sendTeamNotification_NewAndExistingLogStream() throws InterruptedException {

        //ensure the test log stream is not existing
        deleteTestLogGroupAndStream();

        String firstLogMessage = "first " + UUID.randomUUID();
        String secondLogMessage = "second " + UUID.randomUUID();
        String topic = "topiqué";
        Tenant tenant = th.tenant1;

        //message to not existing log group and stream
        cloudWatchTeamNotificationPublisher.sendTeamNotification(topic, tenant,
                TeamNotificationMessage.builder()
                        .text(firstLogMessage)
                        .created(timeServiceMock.currentTimeMillisUTC())
                        .build());
        //message to existing log group and stream
        cloudWatchTeamNotificationPublisher.sendTeamNotification(topic, tenant,
                TeamNotificationMessage.builder()
                        .text(secondLogMessage)
                        .created(timeServiceMock.currentTimeMillisUTC())
                        .build());

        GetLogEventsResponse logEvents = getLogEventsAndRetry(2, 4);

        assertThat(logEvents).isNotNull();
        assertThat(logEvents.events()).hasSize(2);
        assertThat(logEvents.events())
                .map(OutputLogEvent::message)
                .filteredOn(m -> m.contains(firstLogMessage))
                .hasSize(1);
        assertThat(logEvents.events())
                .map(OutputLogEvent::message)
                .filteredOn(m -> m.contains(secondLogMessage))
                .hasSize(1);
        assertThat(logEvents.events())
                .map(OutputLogEvent::message)
                .filteredOn(m -> m.contains(topic))
                .hasSize(2);
        assertThat(logEvents.events())
                .map(OutputLogEvent::message)
                .filteredOn(m -> m.contains(tenant.getName()))
                .hasSize(2);
        assertThat(logEvents.events())
                .map(OutputLogEvent::message)
                .filteredOn(m -> m.contains(tenant.getId()))
                .hasSize(2);
    }

    private GetLogEventsResponse getLogEventsAndRetry(int expectedSize, int numRetries) throws InterruptedException {
        for (int i = 0; i <= numRetries; i++) {
            GetLogEventsResponse logEvents =
                    cloudWatchClient.getLogEvents(GetLogEventsRequest.builder()
                            .logGroupName(logGroupName)
                            .logStreamName(logStreamName)
                            .build());
            if (logEvents.events().size() == expectedSize) {
                return logEvents;
            } else {
                log.info("Retrying {}/{} to get log events, size {} does not match expected size {}",
                        i, numRetries, logEvents.events().size(), expectedSize);
                // wait for cloudWatch to process the requests
                TimeUnit.MILLISECONDS.sleep(500);
            }
        }
        fail("Could not get events with the expected size");
        return null;
    }

    private CloudWatchLogsClient createCloudWatchLogClient() {

        return CloudWatchLogsClient.builder()
                .region(awsConfig.getAWSRegion())
                .credentialsProvider(awsConfig.getAWSCredentials())
                .overrideConfiguration(awsConfig.getDefaultServiceConfiguration())
                .build();
    }

    private void deleteTestLogGroupAndStream() {
        try {
            //cloudWatchClient.deleteLogStream(new DeleteLogStreamRequest(logGroupName, logStreamName));
            cloudWatchClient.deleteLogGroup(DeleteLogGroupRequest.builder()
                    .logGroupName(logGroupName)
                    .build());
        } catch (Exception e){
            //if it did not exist before (which is the default case) there will be an exception
        }
    }

}
