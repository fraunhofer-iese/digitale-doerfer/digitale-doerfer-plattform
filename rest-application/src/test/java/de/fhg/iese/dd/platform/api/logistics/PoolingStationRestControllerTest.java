/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientPoolingStation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStation;

public class PoolingStationRestControllerTest extends BaseServiceTest {

    @Autowired
    private LogisticsTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createPoolingStations();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

     @Test
    public void getAllPoolingStations() throws Exception{
        List<PoolingStation> expectedPoolingStations = th.poolingStationList.stream()
            .filter(i->i.getTenant().equals(th.tenant1))
            .sorted(Comparator.comparingLong(BaseEntity::getCreated))
            .collect(Collectors.toList());

         assertEquals(2, expectedPoolingStations.size());

        mockMvc.perform(get("/logistics/poolingstation")
                .param("communityId", th.tenant1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedPoolingStations.size())))
                .andExpect(jsonPath("$[0].id").value(expectedPoolingStations.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedPoolingStations.get(1).getId()));
    }

    @Test
    public void readPoolingStation() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/logistics/poolingstation/" + th.poolingStation1.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(th.poolingStation1.getId()))
                .andExpect(jsonPath("$.name").value(th.poolingStation1.getName()))
                .andExpect(jsonPath("$.stationType").value(th.poolingStation1.getStationType().toString()))
                .andExpect(jsonPath("$.verificationCode").value(th.poolingStation1.getVerificationCode()))
                .andReturn();

        ClientPoolingStation clientPoolingStation = toObject(mvcResult, ClientPoolingStation.class);
        assertEquals(th.poolingStation1.getIsSelfScanningStation(), clientPoolingStation.isSelfScanningStation());
        assertEquals(th.poolingStation1.getAddress().getId(), clientPoolingStation.getAddress().getId());
        assertEquals(th.poolingStation1.getProfilePicture().getId(), clientPoolingStation.getProfilePicture().getId());
        assertEquals(th.poolingStation1.getOpeningHours().getId(), clientPoolingStation.getOpeningHours().getId());
    }

    @Test
    public void getAllPoolingStationsNotFoundCommunity() throws Exception{
        String invalidId = UUID.randomUUID().toString();
        mockMvc.perform(get("/logistics/poolingstation")
                .param("communityId", invalidId))
                .andExpect(isException(ClientExceptionType.TENANT_NOT_FOUND));
    }

    @Test
    public void readPoolingStationNotFoundPoolingStation() throws Exception{
        String invalidId = UUID.randomUUID().toString();
        mockMvc.perform(get("/logistics/poolingstation/" + invalidId))
                .andExpect(isException(ClientExceptionType.POOLING_STATION_NOT_FOUND));
    }
    
}
