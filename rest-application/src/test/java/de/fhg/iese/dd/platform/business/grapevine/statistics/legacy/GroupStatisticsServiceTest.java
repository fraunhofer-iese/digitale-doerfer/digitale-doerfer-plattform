/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics.legacy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;

public class GroupStatisticsServiceTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private IGrapevineStatisticsService grapevineStatisticsService;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createGroupsAndGroupPostsWithComments();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getGroupStatsForTenant() {

        long start = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
        long end = System.currentTimeMillis();

        GroupStats community1GroupStats = grapevineStatisticsService.getGroupStatsPerTenant(th.tenant1, start, end);

        assertEquals(14, community1GroupStats.getGossipInGroupCount());

        assertEquals(3, community1GroupStats.getPublicGroupCount());

        assertEquals(6, community1GroupStats.getPublicGroupMemberCount());

        assertEquals(5, community1GroupStats.getPrivateGroupCount());

        assertEquals(8, community1GroupStats.getPrivateGroupMemberCount());
    }

}
