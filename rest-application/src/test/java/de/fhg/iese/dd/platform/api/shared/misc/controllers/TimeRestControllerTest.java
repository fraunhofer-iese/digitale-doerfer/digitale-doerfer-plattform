/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.test.web.servlet.MvcResult;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;

public class TimeRestControllerTest extends BaseServiceTest {

    private static final long PRECISION = 200L;
    private static final String TEST_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {}

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void currentTimeUTC() throws Exception{

        final ZoneId zoneId = ZoneId.of("UTC");
        final ZonedDateTime zonedDateTime = ZonedDateTime.now(Clock.system(zoneId));
        long expected = zonedDateTime.toInstant().toEpochMilli();

        MvcResult result = mockMvc
            .perform(get("/time")
            .param("inUtc", "true"))
            .andExpect(status().isOk())
            .andReturn();

        long actual = Long.parseLong(result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        long diff = Math.abs(actual - expected);

        assertTrue(diff <= PRECISION,
                "Expected " + expected + " actual " + actual + " diff " + diff + " > precision " + PRECISION);
    }

    @Test
    public void currentTimeLocal() throws Exception{

        final ZoneId zoneId = ZoneId.of(th.config.getLocalTimeZone());
        final ZonedDateTime zonedDateTime = ZonedDateTime.now(Clock.system(zoneId));
        long expected = zonedDateTime.toLocalDateTime().toInstant(zonedDateTime.getOffset()).toEpochMilli();

        MvcResult result = mockMvc
            .perform(get("/time")
            .param("inUtc", "false"))
            .andExpect(status().isOk())
            .andReturn();

        long actual = Long.parseLong(result.getResponse().getContentAsString(StandardCharsets.UTF_8));
        long diff = Math.abs(actual - expected);

        assertTrue(diff <= PRECISION,
                "Expected " + expected + " actual " + actual + " diff " + diff + " > precision " + PRECISION);
    }

    @Test
    public void formatTimeUTC() throws Exception{

        final Pair<String, String> expected = formatTime("UTC");

        MvcResult result = mockMvc
            .perform(get("/time/formatted")
            .param("inUtc", "true")
            .param("format", TEST_TIME_FORMAT))
            .andExpect(status().isOk())
            .andReturn();

        String actual = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        //it can happen that the result time from the server is one second later which then is the value of Pair.getSecond
        assertThat(actual).isIn(expected.getFirst(), expected.getSecond());
    }

    @Test
    public void formatTimeLocal() throws Exception{

        final String timeZone = th.config.getLocalTimeZone();
        final Pair<String, String> expected = formatTime(timeZone);

        MvcResult result = mockMvc
            .perform(get("/time/formatted")
            .param("inUtc", "false")
            .param("format", TEST_TIME_FORMAT))
            .andExpect(status().isOk())
            .andReturn();

        String actual = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
        //it can happen that the result time from the server is one second later which then is the value of Pair.getSecond
        assertThat(actual).isIn(expected.getFirst(), expected.getSecond());
    }

    //returns current time and current time one second later in the TEST_TIME_FORMAT
    private Pair<String, String> formatTime(String timeZone) {
        final ZoneId zoneId = ZoneId.of(timeZone);
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
                .ofPattern(TEST_TIME_FORMAT)
                .withZone(zoneId);
        final ZonedDateTime now = ZonedDateTime.now(Clock.system(zoneId));
        return Pair.of(dateTimeFormatter.format(now), dateTimeFormatter.format(now.plusSeconds(1)));
    }

}
