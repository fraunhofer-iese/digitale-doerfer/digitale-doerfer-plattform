/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest;
import de.fhg.iese.dd.platform.api.logistics.clientmodel.ClientTransportAlternative;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;

public class TransportPickupPoolingStationBoxScannedReadyForDeallocationRequest extends BaseLogisticsEventTest {

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createShops();
        th.createScoreEntities();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception{

        intermediatePoolingStation = th.poolingStation4;
        deliveryPoolingStation = th.poolingStation2;

        intermediatePoolingStationAddress = intermediatePoolingStation.getAddress();
        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        pickupAddress = ClientAddressDefinition.builder()
                .name("Gartenschau Kaiserslautern")
                .street("Lauterstraße 51")
                .zip("67659")
                .city("Kaiserslautern")
                .build();

        // th.poolingStationBox6 is assigned to th.poolingStation4
        intermediatePoolingStationBox = th.poolingStationBox6;

        // first unused poolingStationBox is selected, therefore we can be sure
        // that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox3;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);

        TransportAlternative transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStationViaIntermediatePoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment = transportAlternativeSelectRequestFirstStep(transportAlternativeToIntermediatePS.getId(),
                    carrierFirstStep, intermediatePoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrierFirstStep, //carrier
            transportAssignment.getId(), //transportAssignmentId
            intermediatePoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(intermediatePoolingStation.getId(),
                    delivery.getTrackingCode(), transportAssignment.getId(), carrierFirstStep,
                    intermediatePoolingStationBox);

        // TransportDeliveredPoolingStationRequest
        ClientTransportAlternative transportAlternativeToDeliveryPS;
        transportAlternativeToDeliveryPS = transportDeliveredPoolingStationRequestIntermediate(
                    intermediatePoolingStation.getId(), delivery.getTrackingCode(), transportAssignment.getId(),
                    carrierFirstStep, deliveryPoolingStation.getId(), delivery.getId());

        // TransportAlternativeSelectRequest
        // Attention new transportAssignment
        transportAssignment = transportAlternativeSelectRequestLastStep(transportAlternativeToDeliveryPS.getId(),
                    carrierLastStep, deliveryPoolingStationAddress);

        // TransportPickupPoolingStationBoxReadyForDeallocationRequest
        transportPickupPoolingStationBoxReadyForDeallocationRequest(delivery.getId(), transportAssignment.getId(),
                    intermediatePoolingStation, intermediatePoolingStationBox, carrierLastStep);
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = intermediatePoolingStation.getId();
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);
        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey()).contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
                .andExpect(jsonPath("$.poolingStationBoxId").value(intermediatePoolingStationBox.getId()))
                .andExpect(jsonPath("$.poolingStationBoxName").value(intermediatePoolingStationBox.getName()))
                .andExpect(jsonPath("$.poolingStationBoxType").value(intermediatePoolingStationBox.getConfiguration().getBoxType().toString()))
                .andExpect(jsonPath("$.timeoutForDoorOpen").value(intermediatePoolingStationBox.getConfiguration().getTimeoutForDoorOpen()));
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequestAlreadyPickedUp() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = intermediatePoolingStation.getId();
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);
        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey()).contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
                .andExpect(jsonPath("$.poolingStationBoxId").value(intermediatePoolingStationBox.getId()))
                .andExpect(jsonPath("$.poolingStationBoxName").value(intermediatePoolingStationBox.getName()))
                .andExpect(jsonPath("$.poolingStationBoxType").value(intermediatePoolingStationBox.getConfiguration().getBoxType().toString()))
                .andExpect(jsonPath("$.timeoutForDoorOpen").value(intermediatePoolingStationBox.getConfiguration().getTimeoutForDoorOpen()));

        request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);
        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey()).contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deliveryId").value(delivery.getId()))
                .andExpect(jsonPath("$.poolingStationBoxId").value(intermediatePoolingStationBox.getId()))
                .andExpect(jsonPath("$.poolingStationBoxName").value(intermediatePoolingStationBox.getName()))
                .andExpect(jsonPath("$.poolingStationBoxType").value(intermediatePoolingStationBox.getConfiguration().getBoxType().toString()))
                .andExpect(jsonPath("$.timeoutForDoorOpen").value(intermediatePoolingStationBox.getConfiguration().getTimeoutForDoorOpen()));
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequestWrongPoolingStation() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = deliveryPoolingStation.getId();
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);

        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.DELIVERY_PICKED_UP_AT_WRONG_LOCATION));
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequestDeliveryIdInvalid() throws Exception {

        String deliveryId = "invalid";
        String poolingStationId = intermediatePoolingStation.getId();
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);

        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.DELIVERY_NOT_FOUND));
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequestPoolingStationIdInvalid() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = "invalid";
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);

        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey()).contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.POOLING_STATION_NOT_FOUND));
    }

    @Test
    public void onTransportPickupPoolingStationBoxScannedReadyForDeallocationRequestApiKeyInvalid() throws Exception {

        String deliveryId = delivery.getId();
        String poolingStationId = intermediatePoolingStation.getId();
        ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest request = new ClientTransportPickupPoolingStationBoxScannedReadyForDeallocationRequest(
                deliveryId, poolingStationId);

        mockMvc.perform(post("/logistics/event/transportPickupPoolingStationBoxScannedReadyForDeallocationRequest")
                        .header(HEADER_NAME_API_KEY, "invalid").contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

}
