/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.shared.files.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.DocumentConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.FileStorageConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

@ActiveProfiles({"test", "DocumentItemServiceTest"})
public class DocumentItemServiceTest extends BaseServiceTest {

    @Autowired
    private IDocumentItemService documentItemService;
    @Autowired
    private TestFileStorage fileStorage;
    @Autowired
    private SharedTestHelper th;
    @Autowired
    private DocumentConfig documentConfig;
    @Autowired
    private FileStorageConfig fileStorageConfig;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void createDocumentItem() throws IOException {
        byte[] documentData = th.loadTestResource("document.pdf");
        final String fileName = "feil.pdf";
        final String title = "Mein Wichtiges Dokument 💎";
        final String description = "Beschreibung";

        DocumentItem documentItem = documentItemService.createDocumentItem(documentData, fileName, title, description,
                FileOwnership.UNKNOWN);
        log.debug(documentItem);

        assertThat(documentItem.getTitle()).isEqualTo(title);
        assertThat(documentItem.getMediaType()).isEqualTo(MediaType.APPLICATION_PDF_VALUE);
        assertThat(documentItem.getDescription()).isEqualTo(description);
        assertThat(documentItem.getUrl()).startsWith(fileStorageConfig.getExternalBaseUrl());
        assertThat(documentItem.getUrl()).endsWith("/" + fileName);
        assertThat(fileStorage.fileExists(fileStorage.getInternalFileName(documentItem.getUrl()))).isTrue();
    }

    @Test
    public void createDocumentItem_SanitizeName() throws IOException {
        byte[] documentData = th.loadTestResource("document.pdf");
        final String fileName = "Dätöíß😘 /.pDf";
        final String title = "Mein Einzigartiges Dokument 💎";
        final String description = "Beschreibung Ümläütße 😏";

        DocumentItem documentItem = documentItemService.createDocumentItem(documentData, fileName, title, description,
                FileOwnership.UNKNOWN);
        log.debug(documentItem);

        assertThat(documentItem.getTitle()).isEqualTo(title);
        assertThat(documentItem.getMediaType()).isEqualTo(MediaType.APPLICATION_PDF_VALUE);
        assertThat(documentItem.getDescription()).isEqualTo(description);
        assertThat(documentItem.getUrl()).startsWith(fileStorageConfig.getExternalBaseUrl());
        assertThat(documentItem.getUrl()).endsWith("/Datoi____.pdf");
        assertThat(fileStorage.fileExists(fileStorage.getInternalFileName(documentItem.getUrl()))).isTrue();
    }

    @Test
    public void createDocumentItem_FixExtension() throws IOException {
        byte[] documentData = th.loadTestResource("document.pdf");
        final String fileName = "Meine Datei.html";
        final String title = "Geheimplan! 😎";
        final String description = "Beschreibung";

        DocumentItem documentItem = documentItemService.createDocumentItem(documentData, fileName, title, description,
                FileOwnership.UNKNOWN);
        log.debug(documentItem);

        assertThat(documentItem.getTitle()).isEqualTo(title);
        assertThat(documentItem.getMediaType()).isEqualTo(MediaType.APPLICATION_PDF_VALUE);
        assertThat(documentItem.getDescription()).isEqualTo(description);
        assertThat(documentItem.getUrl()).startsWith(fileStorageConfig.getExternalBaseUrl());
        assertThat(documentItem.getUrl()).endsWith("/Meine_Datei.pdf");
        assertThat(fileStorage.fileExists(fileStorage.getInternalFileName(documentItem.getUrl()))).isTrue();
    }

    @Test
    public void createTemporaryDocumentItem() throws IOException {
        byte[] documentData = th.loadTestResource("document.pdf");
        final String fileName = "temp-feil.pdf";
        final String title = "Meine Schatzkarte";
        final String description = "Beschreibung Temporär";
        final Person owner = th.personRegularKarl;

        timeServiceMock.setConstantDateTime(0);

        TemporaryDocumentItem temporaryDocumentItem =
                documentItemService.createTemporaryDocumentItem(documentData, fileName, title, description,
                        FileOwnership.of(owner));
        log.debug(temporaryDocumentItem);

        final DocumentItem documentItem = temporaryDocumentItem.getDocumentItem();
        assertThat(temporaryDocumentItem.getId()).isNotEqualTo(documentItem.getId());
        assertThat(temporaryDocumentItem.getOwner()).isEqualTo(owner);
        assertThat(temporaryDocumentItem.getExpirationTime()).isEqualTo(
                documentConfig.getTemporaryItemExpiration().toMillis());
        assertThat(documentItem.getTitle()).isEqualTo(title);
        assertThat(documentItem.getMediaType()).isEqualTo(MediaType.APPLICATION_PDF_VALUE);
        assertThat(documentItem.getDescription()).isEqualTo(description);
        assertThat(documentItem.getUrl()).startsWith(fileStorageConfig.getExternalBaseUrl());
        assertThat(documentItem.getUrl()).endsWith("/" + fileName);
        assertThat(fileStorage.fileExists(fileStorage.getInternalFileName(documentItem.getUrl()))).isTrue();
    }

    @Test
    @Transactional
    public void copyDocumentItem() throws IOException {
        byte[] documentData = th.loadTestResource("document.pdf");
        final String fileName = "feil.pdf";
        final String title = "Mein Wichtiges Dokument 💎";
        final String description = "Beschreibung";

        DocumentItem documentItem = documentItemService.createDocumentItem(documentData, fileName, title, description,
                FileOwnership.of(th.personRegularKarl, th.app1Variant1));
        log.debug(documentItem);

        FileOwnership newOwnership = FileOwnership.of(th.personRegularKarl, th.app1Variant1);
        DocumentItem clonedItem = documentItemService.cloneItem(documentItem, newOwnership);

        assertThat(clonedItem.getId()).isNotEqualTo(documentItem.getId());
        assertThat(clonedItem.getTitle()).isEqualTo(documentItem.getTitle());
        assertThat(clonedItem.getMediaType()).isEqualTo(documentItem.getMediaType());
        assertThat(clonedItem.getDescription()).isEqualTo(documentItem.getDescription());
        assertThat(clonedItem.getApp()).isEqualTo(documentItem.getApp());
        assertThat(clonedItem.getAppVariant()).isEqualTo(documentItem.getAppVariant());
        assertThat(clonedItem.getOwner()).isEqualTo(documentItem.getOwner());
        assertThat(clonedItem.getUrl()).isNotEqualTo(documentItem.getUrl());
        assertThat(clonedItem.getUrl()).startsWith(fileStorageConfig.getExternalBaseUrl());
        assertThat(clonedItem.getUrl()).endsWith("/" + fileName);
        assertThat(fileStorage.fileExists(fileStorage.getInternalFileName(clonedItem.getUrl()))).isTrue();
    }

}
