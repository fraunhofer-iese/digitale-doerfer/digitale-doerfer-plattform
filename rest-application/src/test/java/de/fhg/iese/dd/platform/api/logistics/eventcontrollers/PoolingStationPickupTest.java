/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientReceiverPickupReceivedPoolingStationRequest;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;

public class PoolingStationPickupTest extends BaseLogisticsEventTest {

    private Delivery delivery;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception {

        pickupAddress = ClientAddressDefinition.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .build();

        deliveryPoolingStation = th.poolingStation1;
        deliveryPoolingStation.setStationType(PoolingStationType.AUTOMATIC);
        deliveryPoolingStation.setIsSelfScanningStation(true);
        deliveryPoolingStation = th.poolingStationRepository.save(deliveryPoolingStation);

        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        // first unused poolingStationBox is selected, therefore we can be sure that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox1;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);

        TransportAlternative transportAlternativeToIntermediatePS = null;
        transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment = null;
        transportAssignment = transportAlternativeSelectRequestFirstStep(
                    transportAlternativeToIntermediatePS.getId(), carrier, deliveryPoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrier, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryPoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(deliveryPoolingStation.getId(), delivery.getTrackingCode(),
                    transportAssignment.getId(), carrier, deliveryPoolingStationBox);

        // Check created delivery
        List<Delivery> deliveries2 = th.deliveryRepository.findAll();
        assertEquals(1, deliveries2.size(), "Amount of deliveries");
        delivery = deliveries.get(0);

        // TransportDeliveredPoolingStationRequest
        transportDeliveredPoolingStationRequestFinal(
            deliveryPoolingStation, // poolingStation
            delivery.getTrackingCode(),// deliveryTrackingCode
            transportAssignment.getId(),// transportAssignmentId
            delivery.getId(),// deliveryId
            carrier,// carrier
            pickupAddress);// pickupAddressTransport

        // ReceiverPickupPoolingStationBoxReadyForDeallocationRequest
        receiverPickupPoolingStationBoxReadyForDeallocationRequest(delivery.getId(), deliveryPoolingStation.getId(), deliveryPoolingStationBox);
    }

    @Test
    public void onReceiverPickupReceivedPoolingStationRequest() throws Exception {

        String poolingStationId = deliveryPoolingStation.getId();
        String deliveryTrackingCode = delivery.getTrackingCode();
        String deliveryId = delivery.getId();
        ClientReceiverPickupReceivedPoolingStationRequest request =
                new ClientReceiverPickupReceivedPoolingStationRequest(poolingStationId, deliveryTrackingCode, deliveryId);

        mockMvc.perform(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void onReceiverPickupReceivedPoolingStationRequestPoolingStationIdInvalid() throws Exception {

        String poolingStationId = "invalidid";
        String deliveryTrackingCode = delivery.getTrackingCode();
        String deliveryId = delivery.getId();
        ClientReceiverPickupReceivedPoolingStationRequest request =
                new ClientReceiverPickupReceivedPoolingStationRequest(poolingStationId, deliveryTrackingCode, deliveryId);

        mockMvc.perform(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.POOLING_STATION_NOT_FOUND));
    }

    @Test
    public void onReceiverPickupReceivedPoolingStationRequestDeliveryTrackingCodeInvalid() throws Exception {

        String poolingStationId = deliveryPoolingStation.getId();
        String deliveryTrackingCode = "invalidid";
        String deliveryId = delivery.getId();
        ClientReceiverPickupReceivedPoolingStationRequest request =
                new ClientReceiverPickupReceivedPoolingStationRequest(poolingStationId, deliveryTrackingCode, deliveryId);

        mockMvc.perform(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.WRONG_RECEIVER_RECEIVED));
    }

    @Test
    public void onReceiverPickupReceivedPoolingStationRequestDeliveryIdInvalid() throws Exception {

        String poolingStationId = deliveryPoolingStation.getId();
        String deliveryTrackingCode = delivery.getTrackingCode();
        String deliveryId = "invalidid";
        ClientReceiverPickupReceivedPoolingStationRequest request =
                new ClientReceiverPickupReceivedPoolingStationRequest(poolingStationId, deliveryTrackingCode, deliveryId);

        mockMvc.perform(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                        .headers(authHeadersFor(receiver))
                        .contentType(contentType)
                        .content(json(request)))
                .andExpect(isException(ClientExceptionType.DELIVERY_NOT_FOUND));
    }

    @Test
    public void onReceiverPickupReceivedPoolingStationRequestUnauthorized() throws Exception {
        String poolingStationId = deliveryPoolingStation.getId();
        String deliveryTrackingCode = delivery.getTrackingCode();
        String deliveryId = delivery.getId();
        ClientReceiverPickupReceivedPoolingStationRequest request =
                new ClientReceiverPickupReceivedPoolingStationRequest(poolingStationId, deliveryTrackingCode, deliveryId);

        assertOAuth2(post("/logistics/event/receiverPickupReceivedPoolingStationRequest")
                .content(json(request))
                .contentType(contentType));
    }

}
