/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.IsNot.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.push.clientevent.ClientPushCategoryUserSettingChangeResponse;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GossipPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

public class GrapevinePushCategoryCustomizationTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    private PushCategory pushCategoryWithFeature;
    private PushCategory pushCategoryNoFeatureFound;
    private PushCategory pushCategoryFeatureDisabled;
    private final String channelNameGossip = "Knatsch";
    private final String itemNameGossip = "Knatsch-Ding";

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();

        featureConfigRepository.save(FeatureConfig.builder()
                .enabled(true)
                .featureClass(GossipPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"" + channelNameGossip + "\", \"itemName\": \"" + itemNameGossip + "\" }")
                .build()
                .withConstantId());

        featureConfigRepository.save(FeatureConfig.builder()
                .enabled(false) //disabled, so the channel is not available and should not be visible
                .featureClass(SuggestionPostTypeFeature.class.getName())
                .configValues("{ \"channelName\": \"Wutbürger-Kanal" + "\", \"itemName\": \"Wutausbruch\" }")
                .build()
                .withConstantId());

        pushCategoryWithFeature = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .name("$channelName$")
                        .description("Ein neuer $itemName$ wurde erstellt")
                        .orderValue(1)
                        .defaultLoudPushEnabled(true)
                        .configurable(true)
                        .build(),
                th.nextTimeStamp())
                // required since the push category customization uses this constant to decide about it
                .withId(DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED));

        pushCategoryNoFeatureFound = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryWithFeature)
                        .name("$channelName$")
                        .description("Ein neuer $itemName$ wurde erstellt")
                        .orderValue(2)
                        .defaultLoudPushEnabled(false)
                        .configurable(true)
                        .visible(true)
                        .build(),
                th.nextTimeStamp())
                // required since the push category customization uses this constant to decide about it
                .withId(DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED));

        pushCategoryFeatureDisabled = pushCategoryRepository.save(withCreated(
                PushCategory.builder()
                        .app(th.app1)
                        .parentCategory(pushCategoryWithFeature)
                        .name("$channelName$")
                        .description("Ein neuer $itemName$ wurde erstellt")
                        .orderValue(2)
                        .defaultLoudPushEnabled(false)
                        .configurable(true)
                        .visible(true)
                        .build(),
                th.nextTimeStamp())
                // required since the push category customization uses this constant to decide about it
                .withId(DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED));

        th.pushCategoriesApp1.add(pushCategoryWithFeature);
        th.pushCategoriesApp1.add(pushCategoryNoFeatureFound);
        th.pushCategoriesApp1.add(pushCategoryFeatureDisabled);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getPushCategoryUserSettings_WithCustomization() throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        final Person caller = th.personRegularAnna;
        boolean newLoudPushEnabled = false;
        String expectedChangedPushCategoryDescription = "Ein neuer " + itemNameGossip + " wurde erstellt";
        String expectedChangedPushCategoryName = channelNameGossip;

        mockMvc.perform(get("/push/categorySetting")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].id").value(pushCategoryWithFeature.getId()))
                .andExpect(jsonPath("$[0].name").value(expectedChangedPushCategoryName))
                .andExpect(jsonPath("$[0].description").value(expectedChangedPushCategoryDescription))

                .andExpect(jsonPath("$[1].id").value(pushCategoryNoFeatureFound.getId()))
                .andExpect(jsonPath("$[1].name").value("$channelName$"))
                .andExpect(jsonPath("$[1].description").value("Ein neuer $itemName$ wurde erstellt"))

                .andExpect(jsonPath("$[*].id", not(hasItem(pushCategoryFeatureDisabled.getId()))));

        ClientPushCategoryUserSettingChangeResponse expectedResponse =
                ClientPushCategoryUserSettingChangeResponse.builder()
                        .changedPushCategoryUserSetting(ClientPushCategoryUserSetting.builder()
                                .id(pushCategoryWithFeature.getId())
                                .name(expectedChangedPushCategoryName)
                                .description(expectedChangedPushCategoryDescription)
                                .loudPushEnabled(newLoudPushEnabled)
                                .parentPushCategoryId(BaseEntity.getIdOf(pushCategoryWithFeature.getParentCategory()))
                                .orderValue(pushCategoryWithFeature.getOrderValue())
                                .build())
                        .changedDependentPushCategoryUserSettings(Collections.emptyList())
                        .build();

        mockMvc.perform(post("/push/categorySetting/{pushCategoryId}", pushCategoryWithFeature.getId())
                .headers(authHeadersFor(caller, appVariant))
                .param("loudPushEnabled", Boolean.toString(newLoudPushEnabled)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedResponse));

        mockMvc.perform(get("/push/categorySetting")
                .headers(authHeadersFor(caller, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].id").value(pushCategoryWithFeature.getId()))
                .andExpect(jsonPath("$[0].name").value(expectedChangedPushCategoryName))
                .andExpect(jsonPath("$[0].description").value(expectedChangedPushCategoryDescription))
                .andExpect(jsonPath("$[0].loudPushEnabled").value(newLoudPushEnabled))

                .andExpect(jsonPath("$[1].id").value(pushCategoryNoFeatureFound.getId()))
                .andExpect(jsonPath("$[1].name").value("$channelName$"))
                .andExpect(jsonPath("$[1].description").value("Ein neuer $itemName$ wurde erstellt"))

                .andExpect(jsonPath("$[*].id", not(hasItem(pushCategoryFeatureDisabled.getId()))));
    }

}
