/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.shared.statistics.IStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;

@Component
public class SharedTestHelper extends BaseSharedTestHelper {

    @Autowired
    public IChatService chatService;

    @Autowired
    private Set<IStatisticsProvider> statisticsProviders;

    @NotNull
    public List<StatisticsMetaData> getAllStatisticsMetaData() {
        return statisticsProviders.stream()
                .flatMap((IStatisticsProvider iStatisticsProvider) -> iStatisticsProvider.getAllMetaData().stream())
                .sorted(Comparator.comparing(StatisticsMetaData::getExpectedWidth))
                .collect(Collectors.toList());
    }

}
