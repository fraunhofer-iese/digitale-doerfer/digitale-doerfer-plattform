/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.shared.dataprivacy.worker;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollectionAppVariantResponse;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletionAppVariantResponse;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariantStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantResponseRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowAppVariantStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.repos.DataPrivacyWorkflowRepository;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

public class DataPrivacyWorkflowsCleanUpWorkerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestFileStorage testFileStorage;
    @Autowired
    private DataPrivacyConfig config;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private AppVariantRepository appVariantRepository;
    @Autowired
    private DataPrivacyWorkflowRepository dataPrivacyWorkflowRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantRepository dataPrivacyWorkflowAppVariantRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantStatusRecordRepository dataPrivacyWorkflowAppVariantStatusRecordRepository;
    @Autowired
    private DataPrivacyWorkflowAppVariantResponseRepository dataPrivacyWorkflowAppVariantResponseRepository;

    @Autowired
    private DataPrivacyWorkflowsCleanUpWorker dataPrivacyWorkflowsCleanUpWorker;

    @Override
    public void createEntities() throws Exception {
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void cleanup() {

        String testFileNameOld = ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER + "to-be-deleted.txt";
        String testFileNameNotOldEnough = ICommonDataPrivacyService.PRIVACY_REPORT_FOLDER + "to-survive.txt";
        String testFileNameNotRelevant = "any-path/to-survive.txt";

        byte[] bytes = "does not matter".getBytes();
        long retentionTimeMillis =
                Duration.ofDays(config.getMaxRetentionDataPrivacyReportsInDays()).toMillis();

        // data collections
        testFileStorage.addTestFile(testFileNameOld, bytes,
                System.currentTimeMillis() - retentionTimeMillis - 1);
        DataPrivacyDataCollection dataCollectionOld =
                createDataCollection(DataPrivacyWorkflowStatus.FINISHED,
                        System.currentTimeMillis() - retentionTimeMillis - 1);

        testFileStorage.addTestFile(testFileNameNotRelevant, bytes,
                System.currentTimeMillis() - retentionTimeMillis - 1);
        DataPrivacyDataCollection dataCollectionOldButNotFinished =
                createDataCollection(DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        System.currentTimeMillis() - retentionTimeMillis - 1);

        testFileStorage.addTestFile(testFileNameNotOldEnough, bytes,
                System.currentTimeMillis() - retentionTimeMillis + 15000);
        DataPrivacyDataCollection dataCollectionNotOldEnough =
                createDataCollection(DataPrivacyWorkflowStatus.FINISHED,
                        System.currentTimeMillis() - retentionTimeMillis + 15000);

        // data deletions
        DataPrivacyDataDeletion dataDeletionOld =
                createDataDeletion(DataPrivacyWorkflowStatus.FINISHED,
                        System.currentTimeMillis() - retentionTimeMillis - 1);

        DataPrivacyDataDeletion dataDeletionOldButNotFinished =
                createDataDeletion(DataPrivacyWorkflowStatus.WAITING_FOR_EXTERNALS,
                        System.currentTimeMillis() - retentionTimeMillis - 1);

        DataPrivacyDataDeletion dataDeletionNotOldEnough =
                createDataDeletion(DataPrivacyWorkflowStatus.FINISHED,
                        System.currentTimeMillis() - retentionTimeMillis + 15000);

        dataPrivacyWorkflowsCleanUpWorker.run();

        assertThat(testFileStorage.fileExists(testFileNameOld))
                .as("old files should be deleted").isFalse();
        assertThat(dataPrivacyWorkflowRepository.existsById(dataCollectionOld.getId()))
                .as("old data collections should be deleted").isFalse();
        assertThat(testFileStorage.fileExists(testFileNameNotOldEnough))
                .as("newer files should survive").isTrue();
        assertThat(dataPrivacyWorkflowRepository.existsById(dataCollectionNotOldEnough.getId()))
                .as("newer data collections should survive").isTrue();
        assertThat(testFileStorage.fileExists(testFileNameNotRelevant))
                .as("old, but not relevant files, should survive").isTrue();
        assertThat(dataPrivacyWorkflowRepository.existsById(dataCollectionOldButNotFinished.getId()))
                .as("old, but not finished data collections, should survive").isTrue();

        assertThat(dataPrivacyWorkflowRepository.existsById(dataDeletionOld.getId()))
                .as("old data deletions should be deleted").isFalse();
        assertThat(dataPrivacyWorkflowRepository.existsById(dataDeletionNotOldEnough.getId()))
                .as("newer data deletions should survive").isTrue();
        assertThat(dataPrivacyWorkflowRepository.existsById(dataDeletionOldButNotFinished.getId()))
                .as("old, but not finished data deletions, should survive").isTrue();
    }

    private DataPrivacyDataCollection createDataCollection(DataPrivacyWorkflowStatus statusDataCollection,
            long lastStatusChanged) {
        Person person = personRepository.saveAndFlush(Person.builder()
                .email(UUID.randomUUID().toString())
                .build());
        AppVariant appVariant = appVariantRepository.saveAndFlush(AppVariant.builder()
                .appVariantIdentifier(UUID.randomUUID().toString())
                .build());
        DataPrivacyDataCollection dataPrivacyDataCollection =
                dataPrivacyWorkflowRepository.saveAndFlush(DataPrivacyDataCollection.builder()
                        .workflowTrigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                        .internalFolderPath("/internal-path/")
                        .person(person)
                        .status(statusDataCollection)
                        .lastStatusChanged(lastStatusChanged)
                        .build());
        DataPrivacyWorkflowAppVariant dataPrivacyDataCollectionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.saveAndFlush(DataPrivacyWorkflowAppVariant.builder()
                        .appVariant(appVariant)
                        .dataPrivacyWorkflow(dataPrivacyDataCollection)
                        .build());
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAndFlush(
                DataPrivacyWorkflowAppVariantStatusRecord.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyDataCollectionAppVariant)
                        .status(DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS)
                        .build());
        dataPrivacyWorkflowAppVariantResponseRepository.saveAndFlush(
                DataPrivacyDataCollectionAppVariantResponse.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyDataCollectionAppVariant)
                        .description("blub")
                        .fileName("my-feil")
                        .build());
        return dataPrivacyDataCollection;
    }

    private DataPrivacyDataDeletion createDataDeletion(DataPrivacyWorkflowStatus statusDataDeletion,
            long lastStatusChanged) {
        Person person = personRepository.saveAndFlush(Person.builder()
                .email(UUID.randomUUID().toString())
                .build());
        AppVariant appVariant = appVariantRepository.saveAndFlush(AppVariant.builder()
                .appVariantIdentifier(UUID.randomUUID().toString())
                .build());
        DataPrivacyDataDeletion dataPrivacyDataDeletion =
                dataPrivacyWorkflowRepository.saveAndFlush(DataPrivacyDataDeletion.builder()
                        .workflowTrigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                        .person(person)
                        .status(statusDataDeletion)
                        .lastStatusChanged(lastStatusChanged)
                        .build());
        DataPrivacyWorkflowAppVariant dataPrivacyDataDeletionAppVariant =
                dataPrivacyWorkflowAppVariantRepository.saveAndFlush(DataPrivacyWorkflowAppVariant.builder()
                        .appVariant(appVariant)
                        .dataPrivacyWorkflow(dataPrivacyDataDeletion)
                        .build());
        dataPrivacyWorkflowAppVariantStatusRecordRepository.saveAndFlush(
                DataPrivacyWorkflowAppVariantStatusRecord.builder()
                        .dataPrivacyWorkflowAppVariant(dataPrivacyDataDeletionAppVariant)
                        .status(DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS)
                        .build());
        dataPrivacyWorkflowAppVariantResponseRepository.saveAndFlush(DataPrivacyDataDeletionAppVariantResponse.builder()
                .dataPrivacyWorkflowAppVariant(dataPrivacyDataDeletionAppVariant)
                .build());
        return dataPrivacyDataDeletion;
    }

}
