/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine;

import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class GroupDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private GroupMembershipRepository groupMembershipRepository;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createPushEntities();
        th.createGroupsAndGroupPostsWithComments();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return th.personAnjaTenant1Dorf1;
    }

    @Override
    public  Person getPersonForDeletion() { return getPersonForPrivacyReport(); }

    @Override
    public Collection<Class<? extends JpaRepository<?, ?>>> getExcludedRepositories() {
        //tested at GrapevineDataPrivacyServiceTest and can not be posted in a group
        return Arrays.asList(
                ExternalPostRepository.class,
                HappeningRepository.class,
                NewsSourceRepository.class,
                NewsSourceOrganizationMappingRepository.class,
                TradeRepository.class,
                SuggestionCategoryRepository.class,
                SuggestionRepository.class,
                SuggestionStatusRecordRepository.class,
                SuggestionWorkerMappingRepository.class,
                TradingCategoryRepository.class,
                PostInteractionRepository.class,
                LikeCommentRepository.class,
                HappeningParticipantRepository.class,
                NewsItemRepository.class,
                OfferRepository.class,
                SeekingRepository.class,
                SpecialPostRepository.class,
                //not connected to a person
                ConvenienceRepository.class,
                ConvenienceGeoAreaMappingRepository.class,
                OrganizationRepository.class,
                OrganizationPersonRepository.class,
                OrganizationGeoAreaMappingRepository.class);
    }

    @Test
    public void testGroupMembershipInPrivacyReport() {

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        assertThat(dataPrivacyReport).has(sectionExists("DorfFunk", dorfFunkSection -> {

            final List<IDataPrivacyReport.IDataPrivacyReportSubSection> subSections = dorfFunkSection.getSubSections();
            assertEquals(3, subSections.size());

            assertThat(dorfFunkSection).has(subSectionExists("Gruppen", groupSubSection -> {

                final List<IDataPrivacyReport.IDataPrivacyReportContent<?>> groupContents =
                        groupSubSection.getContents();
                assertEquals(4, groupContents.size());

                final List<String> groupNames =
                        Arrays.asList(th.groupAdfcAAP.getName(),
                                th.groupErfahrungMMA.getName(),
                                th.groupWaffenfreundeAMA.getName(),
                                th.groupDeleted.getName());

                groupNames.forEach(groupName -> {
                    assertTrue(groupContents.stream()
                            .anyMatch(content -> content.getDescription().contains(groupName)));
                });
            }));

        }));
    }

    @Test
    public void testOwnGroupsInPrivacyReport() {

        Group ownGroup = th.groupSchachvereinAAA;
        ownGroup.setCreator(getPersonForPrivacyReport());
        th.groupRepository.save(ownGroup);

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        assertThat(dataPrivacyReport).has(sectionExists("DorfFunk", dorfFunkSection -> {

            assertThat(dorfFunkSection).has(subSectionExists("Angelegte Gruppen", groupSubSection -> {

                final List<IDataPrivacyReport.IDataPrivacyReportContent<?>> groupContents =
                        groupSubSection.getContents();
                assertThat(groupContents).hasSize(2);
                assertThat(groupContents.get(0).getDescription()).contains("Gruppe").contains(ownGroup.getName());
                assertThat(groupContents.get(1).getDescription()).contains("Logo").contains(ownGroup.getName());
            }));

        }));
    }

    @Test
    public void deleteGroupMemberships() {

        final List<GroupMembership> groupMemberships =
                groupMembershipRepository.findAllByMemberOrderByCreatedDesc(getPersonForDeletion());

        assertEquals(4, groupMemberships.size());

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), report);

        for (GroupMembership groupMembership : groupMemberships) {
            assertThat(report).is(containsEntityWithOperation(groupMembership, DeleteOperation.DELETED));
            Optional<GroupMembership> groupMembershipFromRepo =
                    groupMembershipRepository.findById(groupMembership.getId());
            assertFalse(groupMembershipFromRepo.isPresent());
        }
    }

    @Test
    public void deleteOwnGroups() {

        Person person = getPersonForDeletion();
        Group ownGroup = th.groupSchachvereinAAA;
        ownGroup.setCreator(person);
        ownGroup.setLogo(th.createMediaItem("logo", FileOwnership.of(person)));
        th.groupRepository.save(ownGroup);

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(person, report);

        assertThat(report).is(containsEntityWithOperation(ownGroup, DeleteOperation.ERASED));
    }

}
