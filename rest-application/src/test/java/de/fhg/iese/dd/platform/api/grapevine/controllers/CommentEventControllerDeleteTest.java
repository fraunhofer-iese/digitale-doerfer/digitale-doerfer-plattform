/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Stefan Schweitzer, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentDeleteAllForPostRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentDeleteConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentDeleteRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CommentEventControllerDeleteTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void commentDeleteRequest() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Comment commentToBeDeleted = th.gossip2Comment1;
        Person creator = commentToBeDeleted.getCreator();

        ClientCommentDeleteRequest commentDeleteRequest = new ClientCommentDeleteRequest(commentToBeDeleted.getId());

        MvcResult createDeleteResult = mockMvc.perform(post("/grapevine/comment/event/commentDeleteRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentId").value(commentDeleteRequest.getCommentId()))
                .andReturn();

        ClientCommentDeleteConfirmation commentDeleteConfirmation
                = toObject(createDeleteResult.getResponse(), ClientCommentDeleteConfirmation.class);

        // Check whether the comment is also deleted from the repository
        Comment deleteComment = th.commentRepository.findById(commentDeleteConfirmation.getCommentId()).get();
        assertTrue(deleteComment.isDeleted());
        assertEquals(nowTime, deleteComment.getDeletionTime());
    }

    @Test
    public void commentDeleteRequest_MissingAttributes() throws Exception {
        Comment commentToBeDeleted = th.gossip2Comment3;
        Person creator = commentToBeDeleted.getCreator();

        ClientCommentDeleteRequest commentDeleteRequest = new ClientCommentDeleteRequest();

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteRequest)))
                .andExpect(status().isBadRequest())
                .andReturn();

        // Check whether the comment is also deleted from the repository
        Comment deleteComment = th.commentRepository.findById(commentToBeDeleted.getId()).get();
        assertFalse(deleteComment.isDeleted());
    }

    @Test
    public void commentDeleteRequest_Unauthorized() throws Exception {

        Comment commentToBeDeleted = th.gossip2Comment4withImages;

        assertOAuth2(post("/grapevine/comment/event/commentDeleteRequest")
                .contentType(contentType)
                .content(json(new ClientCommentDeleteRequest(commentToBeDeleted.getId()))));

        Comment commentNotDeleted = th.commentRepository.findById(commentToBeDeleted.getId()).get();
        assertFalse(commentNotDeleted.isDeleted());
    }

    @Test
    public void commentDeleteRequest_NotOwner() throws Exception {
        Comment commentToBeDeleted = th.gossip2Comment3;
        Person deletor = th.personSebastianBauer;
        // Make sure is not the owner
        assertNotSame(deletor, commentToBeDeleted.getCreator());

        ClientCommentDeleteRequest commentDeleteRequest = new ClientCommentDeleteRequest(commentToBeDeleted.getId());

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteRequest")
                        .headers(authHeadersFor(deletor))
                        .contentType(contentType)
                        .content(json(commentDeleteRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_ACTION_NOT_ALLOWED));

        Comment commentNotDeleted = th.commentRepository.findById(commentToBeDeleted.getId()).get();
        assertFalse(commentNotDeleted.isDeleted());
    }

    @Test
    public void commentDeleteRequest_DeletedComment() throws Exception {

        Comment commentToBeDeleted = th.gossip2Comment3;
        Person creator = commentToBeDeleted.getCreator();

        // Delete the comment
        commentToBeDeleted.setDeleted(true);
        commentToBeDeleted = th.commentRepository.saveAndFlush(commentToBeDeleted);

        ClientCommentDeleteRequest commentDeleteRequest = new ClientCommentDeleteRequest(commentToBeDeleted.getId());

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        Comment commentNotDeleted = th.commentRepository.findById(commentToBeDeleted.getId()).get();
        assertTrue(commentNotDeleted.isDeleted());
    }

    @Test
    public void commentDeleteAllForPostRequest() throws Exception {

        long nowTime = System.currentTimeMillis();
        timeServiceMock.setConstantDateTime(nowTime);

        Post postWithCommentsToBeDeleted = th.gossip2WithComments;
        Person creator = th.personThomasBecker;

        ClientCommentDeleteAllForPostRequest commentDeleteAllForPostRequest =
                ClientCommentDeleteAllForPostRequest.builder()
                        .postId(postWithCommentsToBeDeleted.getId())
                        .build();

        final List<Comment> comments = th.commentRepository.findAllByPostAndCreatorAndDeletedFalseOrderByCreatedDesc(
                postWithCommentsToBeDeleted, creator);
        assertEquals(2, comments.size());

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteAllForPostRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteAllForPostRequest)))
                .andExpect(status().isOk());

        // we need to wait, cause we're calling an endpoint which does not wait for the event processing before return
        waitForEventProcessing();

        final Comment deletedComment = th.commentRepository.findById(comments.get(0).getId()).get();
        assertTrue(deletedComment.isDeleted());
        assertEquals(nowTime, deletedComment.getDeletionTime());
        final Comment notDeletedComment = th.commentRepository.findById(th.gossip2Comment3.getId()).get();
        assertFalse(notDeletedComment.isDeleted());
        assertThat(notDeletedComment.getDeletionTime()).isNull();
    }

    @Test
    public void commentDeleteAllForPostRequest_NoCommentsToDelete() throws Exception {
        Post postWithoutComments = th.gossip1withImages;
        Person creator = th.personThomasBecker;

        ClientCommentDeleteAllForPostRequest commentDeleteAllForPostRequest =
                ClientCommentDeleteAllForPostRequest.builder()
                        .postId(postWithoutComments.getId())
                        .build();

        assertEquals(0, postWithoutComments.getCommentCount());

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteAllForPostRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteAllForPostRequest)))
                .andExpect(status().isOk());
    }

    @Test
    public void commentDeleteAllForPostRequest_MissingAttributes() throws Exception {
        Person creator = th.personFriedaFischer;

        ClientCommentDeleteAllForPostRequest commentDeleteAllForPostRequest = new ClientCommentDeleteAllForPostRequest();

        mockMvc.perform(post("/grapevine/comment/event/commentDeleteAllForPostRequest")
                        .headers(authHeadersFor(creator))
                        .contentType(contentType)
                        .content(json(commentDeleteAllForPostRequest)))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void commentDeleteAllForPostRequest_Unauthorized() throws Exception {
        final ClientCommentDeleteAllForPostRequest commentDeleteAllForPostRequest =
                ClientCommentDeleteAllForPostRequest.builder()
                        .postId(th.gossip2WithComments.getId())
                        .build();

        assertOAuth2(post("/grapevine/comment/event/commentDeleteAllForPostRequest")
                .contentType(contentType)
                .content(json(commentDeleteAllForPostRequest)));
    }

}
