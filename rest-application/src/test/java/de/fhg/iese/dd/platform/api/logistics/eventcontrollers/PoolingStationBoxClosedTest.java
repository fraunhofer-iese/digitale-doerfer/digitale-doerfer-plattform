/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.logistics.eventcontrollers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.fhg.iese.dd.platform.api.logistics.BaseLogisticsEventTest;
import de.fhg.iese.dd.platform.api.logistics.clientevent.ClientPoolingStationBoxClosedEvent;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternative;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public class PoolingStationBoxClosedTest extends BaseLogisticsEventTest {

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createPushEntities();
        th.createAchievementRules();
        th.createPersons();
        th.createScoreEntities();
        th.createShops();
        th.createPoolingStations();
        th.createBestellBarAppAndAppVariants();

        init();
        specificInit();
    }

    private void specificInit() throws Exception {

        receiver = th.personVgAdmin;
        Person carrier = th.personPoolingStationOp;

        pickupAddress = ClientAddressDefinition.builder()
                .name("Fraunhofer IESE")
                .street("Fraunhofer-Platz 1")
                .zip("67663")
                .city("Kaiserslautern")
                .build();

        deliveryPoolingStation = th.poolingStation1;
        deliveryPoolingStation.setStationType(PoolingStationType.AUTOMATIC);
        deliveryPoolingStation.setIsSelfScanningStation(true);
        deliveryPoolingStation = th.poolingStationRepository
                .save(deliveryPoolingStation);

        deliveryPoolingStationAddress = deliveryPoolingStation.getAddress();

        // first unused poolingStationBox is selected, therefore we can be sure that it is the right box
        deliveryPoolingStationBox = th.poolingStationBox1;

        // PurchaseOrderReceived
        String shopOrderId = purchaseOrderReceived(deliveryPoolingStation.getId(), receiver, pickupAddress);

        // PurchaseOrderReadyForTransport
        purchaseOrderReadyForTransport(shopOrderId);

        TransportAlternative transportAlternativeToIntermediatePS = checkDeliveryAndTransportToPoolingStation();

        // TransportAlternativeSelectRequest
        TransportAssignment transportAssignment;
        transportAssignment = transportAlternativeSelectRequestFirstStep(
                    transportAlternativeToIntermediatePS.getId(), carrier, deliveryPoolingStationAddress);

        // Check created delivery
        List<Delivery> deliveries = th.deliveryRepository.findAll();
        assertEquals(1, deliveries.size(), "Amount of delivieries");
        Delivery delivery = deliveries.get(0);

        // TransportPickupRequest
        transportPickupRequest(
            carrier, //carrier
            transportAssignment.getId(), //transportAssignmentId
            deliveryPoolingStationAddress, //deliveryAddress of transportAssignment
            receiver, //receiver
            delivery.getId(), //deliveryId
            deliveryPoolingStationAddress, //deliveryAddress of delivery
            delivery.getTrackingCode()); //deliveryTrackingCode

        // TransportPoolingStationBoxReadyForAllocationRequest
        transportPoolingStationBoxReadyForAllocationRequest(deliveryPoolingStation.getId(), delivery.getTrackingCode(),
                    transportAssignment.getId(), carrier, deliveryPoolingStationBox);

        //Since there is no boxAllocation specified in the test, the previous method will resulting on selecting a random box from poolingStation1 in MySQL (SQL server also most likely)
        //For H2 it is always the first box saved in the repo
        List<PoolingStationBoxAllocation> allAllocations = th.poolingStationBoxAllocationRepository.findAllByPoolingStationIdAndDeliveryId(deliveryPoolingStation.getId(), delivery.getId());
        assertEquals(1, allAllocations.size(),
                "Checking that there is only one allocation"); //If this fails the setup of the tests is wrong
        PoolingStationBoxAllocation allocation = allAllocations.get(0);
        deliveryPoolingStationBox = allocation.getPoolingStationBox();
    }
    @Test
    public void onPoolingStationBoxClosed() throws Exception {

        ClientPoolingStationBoxClosedEvent clientDeliveryReceivedRequest =
                new ClientPoolingStationBoxClosedEvent(deliveryPoolingStationBox.getId());

        mockMvc.perform(post("/logistics/event/poolingStationBoxClosed")
                .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                .contentType(contentType)
                .content(json(clientDeliveryReceivedRequest)))
                .andExpect(status().isOk());

        waitForEventProcessing();

        PoolingStationBoxAllocation boxAllocation = th.poolingStationBoxAllocationRepository.findFirstByPoolingStationBoxOrderByTimestampDesc(deliveryPoolingStationBox);
        Assertions.assertFalse(boxAllocation.isDoorOpen(), "PoolingStationBox should be closed");

    }

    @Test
    public void onPoolingStationBoxClosedInvalidPoolingStationBoxId() throws Exception {

        String poolingStationBoxId = "invalid";
        ClientPoolingStationBoxClosedEvent clientDeliveryReceivedRequest =
                new ClientPoolingStationBoxClosedEvent(poolingStationBoxId);

        mockMvc.perform(post("/logistics/event/poolingStationBoxClosed")
                .header(HEADER_NAME_API_KEY, config.getDStation().getApiKey())
                .contentType(contentType)
                .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.POOLING_STATION_BOX_NOT_FOUND));
    }

    @Test
    public void onPoolingStationBoxClosedApiKeyInvalid() throws Exception {

        String poolingStationBoxId = deliveryPoolingStationBox.getId();
        ClientPoolingStationBoxClosedEvent clientDeliveryReceivedRequest =
                new ClientPoolingStationBoxClosedEvent(poolingStationBoxId);

        mockMvc.perform(post("/logistics/event/poolingStationBoxClosed")
                .header(HEADER_NAME_API_KEY, "")
                .contentType(contentType)
                .content(json(clientDeliveryReceivedRequest)))
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

}
