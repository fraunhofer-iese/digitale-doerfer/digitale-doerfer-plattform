/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Alberto Lara, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.validation.MaxTextLength;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressDefinition;
import de.fhg.iese.dd.platform.api.shared.push.services.TestClientPushService;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.email.services.TestEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.test.mocks.TestDefaultTeamNotificationPublisher;
import de.fhg.iese.dd.platform.business.test.mocks.TestOauthManagementService;
import de.fhg.iese.dd.platform.business.test.mocks.TestTeamFileStorageService;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestParallelismService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.TestTimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonVerificationStatusRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.TestMediaItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.expression.StandardBeanExpressionResolver;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.http.client.MockClientHttpResponse;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ExtendWith(TestWatcherLoggingExtension.class)
@SpringBootTest(classes = RestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseServiceTest {

    @FunctionalInterface
    protected interface ThrowingSupplier<T, E extends Throwable> {

        T get() throws E;

    }

    @FunctionalInterface
    protected interface ThrowingConsumer<T, E extends Throwable> {

        void accept(T t) throws E;

    }

    public static final String FLAG_RUN_INTEGRATION_TESTS = "dd.test.integration.run";
    /**
     * Intentionally duplicate of {@link de.fhg.iese.dd.platform.api.framework.controllers.BaseController#HEADER_NAME_API_KEY}
     * to avoid unintentionally changing it there and breaking all clients.
     */

    private static final ObjectMapper OBJECT_MAPPER_IGNORE_NULL = initObjectMapper();
    private static final ObjectWriter OBJECT_WRITER_INGORE_NULL = OBJECT_MAPPER_IGNORE_NULL.writer();

    private static ObjectMapper initObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        //forces Jackson to use no args constructor when deserializing. In this constructor, default values can be applied.
        objectMapper.setVisibility(
                objectMapper.getVisibilityChecker().withCreatorVisibility(JsonAutoDetect.Visibility.PUBLIC_ONLY));
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return objectMapper;
    }

    public static void deleteTempFilesStartingWithClassName(Class<?> testClass) throws IOException {
        try (Stream<Path> files = Files.list(Paths.get(System.getProperty("java.io.tmpdir")))) {
            files.filter(p -> p.getFileName().toString().startsWith(testClass.getSimpleName()))
                    .forEach(path -> {
                        try {
                            Files.delete(path);
                            System.out.println("Deleted " + path);
                        } catch (IOException e) {
                            System.out.println("Could not delete " + path + ". Got " + e);
                        }
                    });
        }
    }

    protected static final String HEADER_NAME_API_KEY = "apiKey";

    protected final Logger log = LogManager.getLogger(this.getClass());

    protected MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    protected static final MediaType contentType = MediaType.APPLICATION_JSON;

    @Autowired
    protected MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    protected TestAccessTokenGeneratorService accessTokenGenerator;

    @Autowired
    public PushCategoryRepository pushCategoryRepository;
    @Autowired
    public FeatureConfigRepository featureConfigRepository;

    @Autowired
    public IAppService appService;
    @Autowired
    public TestTimeService timeServiceMock;
    @Autowired
    public TestParallelismService testParallelismService;
    @Autowired
    protected TestEmailSenderService emailSenderServiceMock;

    @Autowired(required = false) //on integration tests, we do not have the test file storage available
    protected TestFileStorage testFileStorage;

    @Autowired(required = false) //on integration tests, we do not have the test team file storage available
    private TestTeamFileStorageService testTeamFileStorageService;

    @Autowired(required = false)
    protected TestClientPushService testClientPushService;

    @Autowired(required = false)
    protected TestOauthManagementService testOauthManagementService;

    @Autowired(required = false)
    private TestDefaultTeamNotificationPublisher testDefaultTeamNotificationPublisher;

    @Autowired(required = false)
    private TestMediaItemService testMediaItemService;

    @Autowired
    private List<ICachingComponent> cachingComponents;

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @Autowired
    private ConfigurableBeanFactory beanFactory;

    protected void waitForEventProcessing() throws InterruptedException {

        waitForEventProcessingFinished(5L, 4, 8);
    }

    /**
     * Only use in rare cases! {@link #waitForEventProcessing()} is sufficient for almost all cases!
     *
     * @throws InterruptedException if the waiting was interrupted
     */
    protected void waitForEventProcessingLong() throws InterruptedException{
        waitForEventProcessingFinished(300L, 32, 60);
    }

    private void waitForEventProcessingFinished(long initialSleep, int waitIterations, int checkIterations) throws InterruptedException {
        Thread.sleep(initialSleep);
        for (int i = 0; i < checkIterations; i++) {
            if(eventBusAccessor.areEventsInProcessing()){
                Thread.sleep(100L);
                log.info("waitForEventProcessing: waiting for events to be processed {}", i);
            }
            if(i < waitIterations) {
                Thread.sleep(5L);
            }
        }
    }

    protected void waitForRequestProcessingFinished(BooleanSupplier check) throws InterruptedException {
        //wait for events to be on the event bus
        Thread.sleep(50L);
        //first check that there are no events waiting anymore
        waitForEventProcessing();
        //then wait for the asynchronous requests to be done
        Thread.sleep(50L);
        for (int i = 0; i < 30; i++) {
            if(check.getAsBoolean()){
                Thread.sleep(100L);
                log.info("waitForRequestProcessingFinished: waiting for requests to be processed {}", i);
            }
            Thread.sleep(5L);
        }
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    protected <T> T toObject(MvcResult result, Class<? extends T> clazz) throws IOException {
        return toObject(result.getResponse(), clazz);
    }

    @SuppressWarnings("unchecked")
    protected <T> T toObject(MockHttpServletResponse response, Class<? extends T> clazz) throws IOException {
        MockClientHttpResponse inputMessage = new MockClientHttpResponse(response.getContentAsByteArray(),
                HttpStatus.valueOf(response.getStatus()));
        return (T) mappingJackson2HttpMessageConverter.read(clazz, inputMessage);
    }

    protected <T> List<T> toListOfType(MockHttpServletResponse response, Class<? extends T> type) throws IOException {

        return JsonMapping.defaultJsonReader()
                .forType(TypeFactory.defaultInstance().constructCollectionType(List.class, type))
                .readValue(response.getContentAsString(StandardCharsets.UTF_8));
    }

    protected <T> PageBean<T> toPageOfType(MvcResult result, Class<? extends T> type) throws IOException {

        return JsonMapping.defaultJsonReader()
                .forType(TypeFactory.defaultInstance().constructParametricType(PageBean.class, type))
                .readValue(result.getResponse().getContentAsString(StandardCharsets.UTF_8));
    }

    @BeforeEach
    public void setup() throws Exception {

        assertNotNull(mappingJackson2HttpMessageConverter, "JSON message converter must not be null");

        this.mockMvc = webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();

        resetAllMocks();
        createEntities();
    }

    private void resetAllMocks() {

        emailSenderServiceMock.clearMailList();
        timeServiceMock.resetOffset();
        timeServiceMock.resetConstantDateTime();
        testParallelismService.reset();
        if (testFileStorage != null) {
            //on integration tests we do not have the test file storage available
            testFileStorage.reset();
        }
        if (testTeamFileStorageService != null) {
            //on integration tests we do not have the test team file storage available
            testTeamFileStorageService.reset();
        }
        if (testClientPushService != null) {
            //not available on integration tests
            testClientPushService.reset();
        }
        if (testOauthManagementService != null) {
            //not available on integration tests
            testOauthManagementService.reset();
        }
        if (testDefaultTeamNotificationPublisher != null) {
            //not available on integration tests
            testDefaultTeamNotificationPublisher.reset();
        }
        if (testMediaItemService != null) {
            //not available on integration tests
            testMediaItemService.reset();
        }

        //make sure all caches are reset
        cachingComponents.forEach(ICachingComponent::invalidateCache);
    }

    /**
     * Implement this method in subclasses calling the required createX()
     * methods from the domain specific {@link BaseTestHelper} which are needed for
     * running the Tests
     * <p>
     * i.e.: <code>th.createScoreEntities();</code>
     */
    public abstract void createEntities() throws Exception;

    @AfterEach
    public final void after() throws Exception {
        //wait for events to be processed, so that we do not get a lot of exceptions when deleting data while events are still processed
        waitForEventProcessing();
        tearDown();
    }

    /**
     * Implement this method in the following way:
     * <p>
     * For every test class create a variable 'th' whose type is the domain
     * specific {@link BaseTestHelper} and call {@link BaseTestHelper#deleteAllData()}
     *
     * <pre>
     * &#64;Override
     * public void tearDown() throws Exception {
     *
     *     th.deleteAllData();
     * }
     * </pre>
     *
     */
    public abstract void tearDown() throws Exception;

    protected HttpHeaders authHeadersFor(AuthorizationData authorizationData) {
        final HttpHeaders headers = new HttpHeaders();
        if (authorizationData.getAppVariantIdentifier() != null) {
            headers.add("appVariantIdentifier", authorizationData.getAppVariantIdentifier());
        }
        if (authorizationData.getApiKey() != null) {
            headers.add("apiKey", authorizationData.getApiKey());
        }
        if (!authorizationData.isNoAccessToken()) {
            headers.add("Authorization", "Bearer " + accessTokenGenerator.generateForOauthId(authorizationData));
        }
        return headers;
    }

    protected HttpHeaders authHeadersFor(Person person, AppVariant appVariant) {
        return authHeadersFor(AuthorizationData.builder()
                .person(person)
                .appVariant(appVariant)
                .build());
    }

    protected HttpHeaders authHeadersFor(Person person) {
        return authHeadersFor(AuthorizationData.builder()
                .person(person)
                .build());
    }

    protected HttpHeaders headersForAppVariant(AppVariant appVariant) {
        return headersForAppVariant(appVariant.getAppVariantIdentifier());
    }

    protected HttpHeaders headersForAppVariant(String appVariantIdentifier) {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("appVariantIdentifier", appVariantIdentifier);
        return headers;
    }

    protected <E> MockHttpServletRequestBuilder addRequestParameters(
            MockHttpServletRequestBuilder request,
            String parameterName, Collection<E> parameterValues) {
        if (parameterValues != null) {
            return request.param(parameterName, parameterValues.stream()
                    .map(Object::toString).toArray(String[]::new));
        } else {
            return request;
        }
    }

    protected static ResultMatcher isExceptionWithMessageContains(ClientExceptionType exceptionType,
            String... messageParts) {
        return mvcResult -> {
            isException(exceptionType).match(mvcResult);
            for (String messagePart : messageParts) {
                jsonPath("$.message", containsString(messagePart)).match(mvcResult);
            }
        };
    }

    protected static ResultMatcher isException(String detail, ClientExceptionType exceptionType) {
        return mvcResult -> {
            isException(exceptionType).match(mvcResult);
            if (detail == null) {
                jsonPath("$.detail").doesNotExist();
            } else {
                jsonPath("$.detail").value(detail).match(mvcResult);
            }
        };
    }

    protected static ResultMatcher isException(ClientExceptionType exceptionType) {
        return isExceptionWithAssertionMessage("Expected exception of type " + exceptionType, exceptionType);
    }

    private static ResultMatcher internalAssertException(ClientExceptionType exceptionType) {
        return mvcResult -> {
            AssertionError firstError = null;
            AssertionError secondError = null;
            try {
                jsonPath("$.type").value(exceptionType.name()).match(mvcResult);
            } catch (AssertionError e) {
                firstError = e;
            }
            try {
                status().is(exceptionType.getHttpStatus().value()).match(mvcResult);
            } catch (AssertionError e) {
                secondError = e;
            }
            if (firstError != null || secondError != null) {
                if (firstError == null) {
                    throw new AssertionError(secondError.getMessage());
                }
                if (secondError == null) {
                    throw new AssertionError(firstError.getMessage());
                }
                throw new AssertionError(firstError.getMessage() + "\n" + secondError.getMessage());
            }
        };
    }

    private static ResultMatcher isExceptionWithAssertionMessage(String assertionMessage,
            ClientExceptionType exceptionType) {
        return mvcResult -> {
            try {
                internalAssertException(exceptionType).match(mvcResult);
            } catch (AssertionError e) {
                throw new AssertionError(assertionMessage + "\ncaused by\n" + e.getMessage(), e);
            }
        };
    }

    /**
     * Use this check to ensure that an endpoint is authenticated.
     * <p/>
     * The endpoint should have @{@link de.fhg.iese.dd.platform.api.framework.ApiAuthentication} set to {@link
     * de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType#OAUTH2}. In this case, the Spring Security can
     * already block the request.
     * <p/>
     * The request should be done without providing a valid token as authentication header.
     */
    protected static ResultMatcher isUnauthorized(){
        return mvcResult -> {
            status().isUnauthorized().match(mvcResult);
            content().string("").match(mvcResult);
        };
    }

    private static ResultMatcher isUnauthorizedWithAssertionMessage(String assertionMessage){
        return mvcResult -> {
            try {
                isUnauthorized().match(mvcResult);
            } catch (AssertionError e) {
                throw new AssertionError(assertionMessage + "\ncaused by\n" + e.getMessage(), e);
            }
        };
    }

    /**
     * Use this check to ensure that an endpoint implementation checks the authorization of the requester.
     * <p/>
     * In case the @{@link de.fhg.iese.dd.platform.api.framework.ApiAuthentication} is set to {@link
     * de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType#OAUTH2} this is mainly done by using the {@link
     * IRoleService} to check if the requester has the according roles.
     * <p/>
     * In case the @{@link de.fhg.iese.dd.platform.api.framework.ApiAuthentication} is set to {@link
     * de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType#API_KEY} this is mainly done by comparing the
     * provided with the expected api key.
     */
    protected static ResultMatcher isNotAuthorized() {
        return mvcResult -> {
            isException(ClientExceptionType.NOT_AUTHORIZED).match(mvcResult);
        };
    }

    private static ResultMatcher isNotAuthorizedWithAssertionMessage(String assertionMessage) {
        return mvcResult -> {
            try {
                isNotAuthorized().match(mvcResult);
            } catch (AssertionError e) {
                throw new AssertionError(assertionMessage + "\ncaused by\n" + e.getMessage(), e);
            }
        };
    }

    protected static ResultMatcher isNotAuthenticated() {
        return mvcResult -> {
            isException(ClientExceptionType.NOT_AUTHENTICATED).match(mvcResult);
        };
    }

    private static ResultMatcher isNotAuthenticatedWithAssertionMessage(String assertionMessage) {
        return mvcResult -> {
            try {
                isNotAuthenticated().match(mvcResult);
            } catch (AssertionError e) {
                throw new AssertionError(assertionMessage + "\ncaused by\n" + e.getMessage(), e);
            }
        };
    }

    protected void assertIsPersonVerificationStatusRestricted(
            Class<? extends PersonVerificationStatusRestrictionFeature> feature, Person person, AppVariant appVariant,
            MockHttpServletRequestBuilder requestBuilder) throws Exception {

        EnumSet<PersonVerificationStatus> notAvailableStatuses = EnumSet.allOf(PersonVerificationStatus.class);
        notAvailableStatuses.removeAll(person.getVerificationStatuses().getValues());
        assertThat(notAvailableStatuses)
                .as("Person has all verification statuses, can't test restriction")
                .isNotEmpty();

        featureConfigRepository.save(FeatureConfig.builder()
                .appVariant(appVariant)
                .enabled(true)
                .featureClass(feature.getName())
                .configValues(
                        "{ \"allowedStatusesAnyOf\": [\"" + notAvailableStatuses.iterator().next().name() + "\"] }")
                .build());

        clearRequestHeaders(requestBuilder);

        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(isException(ClientExceptionType.PERSON_VERIFICATION_STATUS_INSUFFICIENT));
    }

    protected void assertOAuth2(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        assertNoAuthHeadersIsUnauthorized(requestBuilder);
        assertInvalidAuthHeadersIsNotAuthorized(requestBuilder);
        clearRequestHeaders(requestBuilder);
    }

    protected void assertOAuth2AppVariantRequired(MockHttpServletRequestBuilder requestBuilder, AppVariant appVariant,
            Person person) throws Exception {

        assertNoAuthHeadersIsUnauthorized(requestBuilder, appVariant);
        assertInvalidAuthHeadersIsNotAuthorized(requestBuilder, appVariant);
        assertAppVariantRequired(requestBuilder, person);
        clearRequestHeaders(requestBuilder);
    }

    protected void assertOAuth2ApiKeyRequired(MockHttpServletRequestBuilder requestBuilder, String apiKey,
            Person person) throws Exception {

        assertNoAuthHeadersIsNotAuthorized(requestBuilder, apiKey);
        assertInvalidAuthHeadersIsNotAuthorized(requestBuilder, apiKey);
        assertApiKeyRequired(requestBuilder, person);
        clearRequestHeaders(requestBuilder);
    }

    private void assertAppVariantRequired(MockHttpServletRequestBuilder requestBuilder, Person person)
            throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .appVariantIdentifier("invalid-app-variant-identifier")
                        .build())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .appVariantIdentifier("")
                        .build())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(person)))
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    protected void assertAppVariantRequired(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier("invalid-app-variant-identifier")
                        .build()
                        .withoutAccessToken())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariantIdentifier("")
                        .build()
                        .withoutAccessToken())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder)
                .andExpect(isExceptionWithAssertionMessage(
                        "getAppVariantNotNull() required for ApiAppVariantIdentifierUsage.REQUIRED, but not used",
                        ClientExceptionType.APP_VARIANT_NOT_FOUND));
    }

    private void assertApiKeyRequired(MockHttpServletRequestBuilder requestBuilder, Person person)
            throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .apiKey("invalid-app-key-identifier")
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(person)
                        .appVariantIdentifier("")
                        .build())))
                .andExpect(isNotAuthenticatedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(person)))
                .andExpect(isNotAuthenticatedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));
    }

    protected void assertApiKeyRequired(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey("invalid-app-key-identifier")
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey("")
                        .build())))
                .andExpect(isNotAuthenticatedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder)
                .andExpect(isNotAuthenticatedWithAssertionMessage(
                        "getAuthenticatedAppVariantNotNull() required for ApiAppVariantIdentification.AUTHENTICATION, but not used"));
    }

    private void assertInvalidAuthHeadersIsNotAuthorized(MockHttpServletRequestBuilder requestBuilder,
            AppVariant appVariant) throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("notexisting")
                        .appVariant(appVariant)
                        .build())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used",
                        ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId(null)
                        .appVariant(appVariant)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("")
                        .appVariant(appVariant)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used"));
    }

    private void assertInvalidAuthHeadersIsNotAuthorized(MockHttpServletRequestBuilder requestBuilder,
            String apiKey) throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("notexisting")
                        .apiKey(apiKey)
                        .build())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2_AND_API_KEY, but not used",
                        ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId(null)
                        .apiKey(apiKey)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2_AND_API_KEY, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("")
                        .apiKey(apiKey)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2_AND_API_KEY, but not used"));
    }

    private void assertInvalidAuthHeadersIsNotAuthorized(MockHttpServletRequestBuilder requestBuilder)
            throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("notexisting")
                        .build())))
                .andExpect(isExceptionWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used",
                        ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId(null)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .oauthUserId("")
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "getCurrentPersonNotNull() required for ApiAuthenticationType.OAUTH2, but not used"));
    }

    private void assertNoAuthHeadersIsUnauthorized(MockHttpServletRequestBuilder requestBuilder) throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder)
                .andExpect(isUnauthorizedWithAssertionMessage(
                        "ApiAuthenticationType.OAUTH2 expected but not set (or auto configuration in WebSecurityConfig failed)"));
    }

    private void assertNoAuthHeadersIsUnauthorized(MockHttpServletRequestBuilder requestBuilder, AppVariant appVariant)
            throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder)
                .andExpect(isUnauthorizedWithAssertionMessage(
                        "ApiAuthenticationType.OAUTH2 expected but not set (or auto configuration in WebSecurityConfig failed)"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(headersForAppVariant(appVariant.getAppVariantIdentifier())))
                .andExpect(isUnauthorizedWithAssertionMessage(
                        "ApiAuthenticationType.OAUTH2 expected but not set (or auto configuration in WebSecurityConfig failed)"));
    }

    private void assertNoAuthHeadersIsNotAuthorized(MockHttpServletRequestBuilder requestBuilder, String apiKey)
            throws Exception {

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder)
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "ApiAuthenticationType.OAUTH2_AND_API_KEY expected but not set (or auto configuration in WebSecurityConfig failed)"));

        clearRequestHeaders(requestBuilder);
        mockMvc.perform(requestBuilder
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(apiKey)
                        .build())))
                .andExpect(isNotAuthorizedWithAssertionMessage(
                        "ApiAuthenticationType.OAUTH2_AND_API_KEY expected but not set (or auto configuration in WebSecurityConfig failed)"));
    }

    /**
     * This is a dirty hack for resetting the headers of a request builder. Unfortunately this is required since there
     * is no other way to reuse a request builder.
     */
    @SuppressWarnings("unchecked")
    private void clearRequestHeaders(MockHttpServletRequestBuilder target) {

        Field field = ReflectionUtils.findField(target.getClass(), "headers");
        if (field == null) {
            throw new UnsupportedOperationException("Implementation of MockHttpServletRequestBuilder changed!");
        }
        ReflectionUtils.makeAccessible(field);
        MultiValueMap<String, Object> headers = (MultiValueMap<String, Object>) ReflectionUtils.getField(field, target);
        //noinspection ConstantConditions
        headers.clear();
    }

    protected PushCategory findPushCategory(PushCategoryId pushCategoryId) {
        PushCategory category = pushCategoryRepository.findById(pushCategoryId.getId()).orElse(null);
        assertNotNull(category);
        return category;
    }

    protected List<PushCategory> findPushCategoriesBySubject(String subject) {
        return pushCategoryRepository.findAllBySubjectOrderByCreatedDesc(subject);
    }

    protected static ResultMatcher hasCorrectPageData(int pageNumber, int pageSize, int totalElementCount) {

        //number of elements available for this page and the subsequent pages
        int elementCountAvailable = Math.max(totalElementCount - (pageNumber * pageSize), 0);
        //not more than page size elements on this page
        int numberOfElementsOnThisPage = Math.min(pageSize, elementCountAvailable);
        //formula from org.springframework.data.domain.PageImpl.getTotalPages
        int totalPages = pageSize == 0 ? 1 : (int) Math.ceil((double) totalElementCount / (double) pageSize);
        return mvcResult -> {
            jsonPath("$.number").value(pageNumber).match(mvcResult);
            jsonPath("$.size").value(pageSize).match(mvcResult);
            jsonPath("$.content", hasSize(numberOfElementsOnThisPage)).match(mvcResult);
            jsonPath("$.numberOfElements").value(numberOfElementsOnThisPage).match(mvcResult);
            jsonPath("$.totalElements").value(totalElementCount).match(mvcResult);
            jsonPath("$.totalPages").value(totalPages).match(mvcResult);
        };
    }

    protected static ResultMatcher hasCorrectFakePageData(int pageNumber, int pageSize, int totalElementCount) {

        int totalElementCountFullPreviousPage = pageNumber * pageSize;
        int elementCountThisPage =
                Math.max(Math.min(totalElementCount - totalElementCountFullPreviousPage, pageSize), 0);
        int fakeTotalElementCount = totalElementCountFullPreviousPage + elementCountThisPage;
        if (totalElementCountFullPreviousPage + pageSize < totalElementCount) {
            //there are more pages
            fakeTotalElementCount++;
        }
        return hasCorrectPageData(pageNumber, pageSize, fakeTotalElementCount);
    }

    protected static PageRequest simplePageRequest(int page, int pageSize) {
        //the sort direction and the sort properties are not returned in the json, so we can ignore it
        return PageRequest.of(page, pageSize, Sort.Direction.DESC, "?");
    }

    protected static ResultMatcher jsonEquals(String message, Object expected, String... jsonPathsToIgnore) {
        return mvcResult -> assertResultEquals(message, expected, mvcResult, false, jsonPathsToIgnore);
    }

    protected static ResultMatcher jsonEquals(Object expected, String... jsonPathsToIgnore) {
        return mvcResult -> assertResultEquals("", expected, mvcResult, false, jsonPathsToIgnore);
    }

    protected static ResultMatcher jsonEqualsIgnoreAdditionalValues(Object expected, String... jsonPathsToIgnore) {
        return mvcResult -> assertResultEquals("", expected, mvcResult, true, jsonPathsToIgnore);
    }

    private static void assertResultEquals(String message, Object expected, MvcResult actual,
            boolean ignoreAdditionalValues,
            String... jsonPathsToIgnore)
            throws JsonProcessingException, UnsupportedEncodingException, JSONException {

        String expectedJson;
        if (ignoreAdditionalValues) {
            expectedJson = OBJECT_WRITER_INGORE_NULL.writeValueAsString(expected);
        } else {
            expectedJson = defaultJsonWriter().writeValueAsString(expected);
        }
        String actualJson = actual.getResponse().getContentAsString(StandardCharsets.UTF_8);

        compareJsons(message, expectedJson, actualJson, jsonPathsToIgnore, ignoreAdditionalValues);
    }

    protected static void assertJsonEquals(Object expected, Object actual, String... jsonPathsToIgnore)
            throws Exception {

        String expectedJson = defaultJsonWriter().writeValueAsString(expected);
        String actualJson = defaultJsonWriter().writeValueAsString(actual);

        compareJsons("", expectedJson, actualJson, jsonPathsToIgnore);
    }

    protected static void assertJsonStringEquals(Object expected, String actualJson, String... jsonPathsToIgnore)
            throws Exception {

        String expectedJson = defaultJsonWriter().writeValueAsString(expected);

        compareJsons("", expectedJson, actualJson, jsonPathsToIgnore);
    }

    private static void compareJsons(String message, String expectedJson, String actualJson, String[] jsonPathsToIgnore)
            throws JSONException {
        compareJsons(message, expectedJson, actualJson, jsonPathsToIgnore, false);
    }

    private static void compareJsons(String message, String expectedJson, String actualJson, String[] jsonPathsToIgnore,
            boolean ignoreAdditionalValues) throws JSONException {
        if (jsonPathsToIgnore != null && jsonPathsToIgnore.length > 0) {

            Configuration jsonPathConfig = Configuration.defaultConfiguration().setOptions(Option.SUPPRESS_EXCEPTIONS);

            DocumentContext expectedJsonDocument = JsonPath.using(jsonPathConfig).parse(expectedJson);
            DocumentContext actualJsonDocument = JsonPath.using(jsonPathConfig).parse(actualJson);

            for (String jsonPathToIgnore : jsonPathsToIgnore) {
                expectedJsonDocument = expectedJsonDocument.delete(jsonPathToIgnore);
                actualJsonDocument = actualJsonDocument.delete(jsonPathToIgnore);
            }

            expectedJson = expectedJsonDocument.jsonString();
            actualJson = actualJsonDocument.jsonString();
        }

        try {
            JSONAssert.assertEquals(message, expectedJson, actualJson,
                    ignoreAdditionalValues ? JSONCompareMode.STRICT_ORDER : JSONCompareMode.STRICT);
        } catch (AssertionError e) {
            throw new AssertionError(
                    e.getMessage() + "\n" +
                            "expected:\n" + expectedJson + "\n" +
                            "actual:\n" + actualJson + "\n" +
                            "simple diff (actual - expected):\n" + StringUtils.difference(expectedJson, actualJson) +
                            "\n" +
                            "simple diff (expected - actual):\n" + StringUtils.difference(actualJson, expectedJson) +
                            "\n");
        }
    }

    public static void assertAddressContentsAreEqual(@Nullable Address expected, @Nullable Address actual) {

        if (expected == actual) {
            return;
        }
        if (expected == null) {
            //noinspection ConstantConditions
            assertNull(actual, "Expected address is null, but actual is not");
            return;
        }
        if (actual == null) {
            //noinspection ConstantConditions
            assertNull(expected, "Expected address is not null, but actual is");
            return;
        }
        assertThat(actual.isDeleted()).as("address.deleted").isEqualTo(expected.isDeleted());
        assertThat(actual.isVerified()).as("address.verified").isEqualTo(expected.isVerified());
        assertThat(actual.getName()).as("address.name").isEqualTo(expected.getName());
        assertThat(actual.getStreet()).as("address.street").isEqualTo(expected.getStreet());
        assertThat(actual.getZip()).as("address.zip").isEqualTo(expected.getZip());
        assertThat(actual.getCity()).as("address.city").isEqualTo(expected.getCity());
        assertThat(actual.getGpsLocation()).as("address.gpsLocation").isEqualTo(expected.getGpsLocation());
    }

    public void assertAddressEquals(ClientAddressDefinition expected, Address actual) {
        assertAddressEquals(expected, actual, "");
    }

    public void assertAddressEquals(ClientAddressDefinition expected, Address actual, String description) {
        assertThat(expected.getName()).as(description).isEqualTo(actual.getName());
        assertThat(expected.getStreet()).as(description).isEqualTo(actual.getStreet());
        assertThat(expected.getZip()).as(description).isEqualTo(actual.getZip());
        assertThat(expected.getCity()).as(description).isEqualTo(actual.getCity());
        if (expected.getGpsLocation() != null) {
            assertThat(expected.getGpsLocation().getLongitude()).as(description)
                    .isEqualTo(actual.getGpsLocation().getLongitude());
            assertThat(expected.getGpsLocation().getLatitude()).as(description)
                    .isEqualTo(actual.getGpsLocation().getLatitude());
        }
    }

    public static <T extends BaseEntity> T withCreated(T entity, long created) {
        entity.setCreated(created);
        return entity;
    }

    protected void callPublicEndpointDifferentAppVariantIdentification(
            Person person,
            AppVariant appVariant,
            Supplier<MockHttpServletRequestBuilder> request,
            ThrowingConsumer<ResultActions, Exception> expectation) throws Exception {

        //no access token, only appVariantIdentifier
        expectation.accept(mockMvc.perform(request.get()
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(appVariant)
                        .build()
                        .withoutAccessToken()))));

        //no access token, only apiKey
        expectation.accept(mockMvc.perform(request.get()
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(appVariant.getApiKey1())
                        .build()
                        .withoutAccessToken()))));

        //access token, oauthClientIdentifier and appVariantIdentifier
        expectation.accept(mockMvc.perform(request.get()
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(appVariant)
                        .person(person)
                        .build()))));
    }

    protected void callOauth2AppVariantIdentifierRequiredEndpointDifferentAppVariantIdentification(
            Person person,
            AppVariant appVariant,
            ThrowingSupplier<MockHttpServletRequestBuilder, Exception> request,
            ThrowingConsumer<ResultActions, Exception> expectation) throws Exception {

        //access token, only appVariantIdentifier
        expectation.accept(mockMvc.perform(request.get()
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(appVariant)
                        .person(person)
                        .build()))));

        //access token, only apiKey
        expectation.accept(mockMvc.perform(request.get()
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(appVariant.getApiKey1())
                        .oauthClient(appVariant.getOauthClients().iterator().next())
                        .person(person)
                        .build()))));
    }

    protected void assertWrongOrEmptyEventAttributesAreInvalid(MockHttpServletRequestBuilder requestBuilder,
            ClientBaseEvent event) throws Exception {

        for (Field field : event.getClass().getDeclaredFields()) {
            // required to modify event attributes
            ReflectionUtils.makeAccessible(field);
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation instanceof NotNull ||
                        annotation instanceof NotBlank ||
                        annotation instanceof NotEmpty ||
                        annotation instanceof MaxTextLength) {
                    ClientBaseEvent eventCopy = copy(event);
                    setEventAttributeInvalid(annotation, field, eventCopy);

                    mockMvc.perform(requestBuilder
                                    .content(json(eventCopy)))
                            .andExpect(isException(field.getName(), ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
                }
            }
        }
    }

    private <T extends ClientBaseEvent> T copy(T event)
            throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        @SuppressWarnings("unchecked")
        T newEvent = (T) event.getClass().getDeclaredConstructor().newInstance();
        BeanUtils.copyProperties(event, newEvent);
        return newEvent;
    }

    private void setEventAttributeInvalid(Annotation annotation, Field field,
            ClientBaseEvent event) throws IllegalAccessException {

        if (annotation instanceof NotNull) {
            // set field to NULL
            field.set(event, null);

        } else if (annotation instanceof NotBlank) {
            // set field to empty String
            field.set(event, "");

        } else if (annotation instanceof NotEmpty) {
            if (Collection.class.isAssignableFrom(field.getType())) {
                // set field to empty Collection
                field.set(event, Collections.emptyList());

            } else if (Array.class.isAssignableFrom(field.getType())) {
                // set field to empty Array
                field.set(event, ArrayUtils.toArray());

            } else if (Map.class.isAssignableFrom(field.getType())) {
                // set field to empty Map
                field.set(event, Collections.emptyMap());
            }

        } else if (annotation instanceof MaxTextLength) {
            final String valueExpression = ((MaxTextLength) annotation).max().value();
            final int maxValue = (int) getConfigValue(valueExpression);

            // set random text with length max value + 1
            field.set(event, RandomStringUtils.randomPrint(maxValue + 1));
        }
    }

    private Object getConfigValue(String expression) {

        BeanExpressionResolver resolver = beanFactory.getBeanExpressionResolver();
        if (resolver == null) {
            resolver = new StandardBeanExpressionResolver();
        }
        return resolver.evaluate(expression, new BeanExpressionContext(beanFactory, null));
    }

}
