/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public class AppControllerAvailableGeoAreaSearchTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void searchAvailableGeoAreasForAppVariant() throws Exception {

        th.geoAreaEisenberg.setName("Kupferberg"); //required to not match all sub-areas

        th.geoAreaRepository.saveAndFlush(th.geoAreaEisenberg);

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "Eisenbuckel")) //matches the two child areas
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "67304")) //matches only one child areas
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "6730")) //matches the two child areas
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(
                                th.app1Variant1) // Mainz belongs to tenant 2, which is not included in this appvariant
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "55129"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.emptyList()));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app2Variant1) // Should find Mainz by PLZ
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "55129"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaMainz, 2, true))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app2Variant1) // Should find Mainz
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "55129 Mai"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaMainz, 2, true))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app2Variant1) // Should find Mainz by PLZ, but name is wrong
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "55129 Kais"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.emptyList()));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "Kupferberg"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "DorfEins"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "2")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "DorfEins"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "3")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "Kupferberg"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.emptyList()));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "3")
                .param("includeAllChildrenOfMatchingAreas", "false")
                .param("search", "123456"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.emptyList()));
    }

    @Test
    public void searchAvailableGeoAreasForAppVariant_IncludingIntermediateChildren() throws Exception {

        th.geoAreaEisenberg.setName("Kupferberg"); //required to not match all sub-areas

        th.geoAreaRepository.saveAndFlush(th.geoAreaEisenberg);

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "Eisenbuckel")) //matches the two child areas
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "Kupferberg")) //matches the intermediate area, so all children are added
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "DorfEins"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "2")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "DorfEins"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "3")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "Eisenbuckel"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Arrays.asList(
                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true))));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(th.app1Variant1)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "67304")) //matches the intermediate area, so all children are added
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                                                th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)))))));
    }

    @Test
    public void searchAvailableGeoAreasForAppVariant_IncludingIntermediateChildren_UnavailableGeoAreas()
            throws Exception {

        final AppVariant appVariant = th.app1Variant1;
        th.geoAreaEisenberg.setName("Kupferberg"); //required to not match all sub-areas
        th.geoAreaRepository.saveAndFlush(th.geoAreaEisenberg);

        //not in available geo areas
        th.excludeGeoAreaFromAppVariant(appVariant, th.geoAreaDorf2InEisenberg, th.tenant1);

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .appVariant(appVariant)
                        .build()))
                .param("upToDepth", "0")
                .param("includeAllChildrenOfMatchingAreas", "true")
                .param("search", "Kupferberg")) //matches geoAreaEisenberg
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(Collections.singletonList(
                        th.toClientGeoArea(th.geoAreaDeutschland, 0, false,
                                th.toClientGeoArea(th.geoAreaRheinlandPfalz, 1, false,
                                        th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                                                th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true)))))));
    }

    @Test
    public void searchAvailableGeoAreasForAppVariant_InvalidAppVariant() throws Exception {

        assertAppVariantRequired(get("/app/appVariant/availableGeoAreas/search")
                .param("search", "miep"));
    }

    @Test
    public void searchAvailableGeoAreasForAppVariant_InvalidParameters() throws Exception {

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(headersForAppVariant(th.app1Variant1))
                .param("search", "12"))
                .andExpect(isException(ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(headersForAppVariant(th.app1Variant1))
                .param("upToDepth", "a")
                .param("search", "123"))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        mockMvc.perform(get("/app/appVariant/availableGeoAreas/search")
                .headers(headersForAppVariant(th.app1Variant1))
                .param("upToDepth", "-1")
                .param("search", "123"))
                .andExpect(isException(ClientExceptionType.QUERY_PARAMETER_INVALID));
    }

}
