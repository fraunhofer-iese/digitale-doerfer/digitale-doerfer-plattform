/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.api.AuthorizationData;
import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.files.clientevent.ClientMediaItemDeleteRequest;
import de.fhg.iese.dd.platform.api.shared.files.clientevent.ClientTemporaryMediaItemUseRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.MediaItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryMediaItemRepository;

public class MediaEventControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Autowired
    private TemporaryMediaItemRepository temporaryMediaItemRepository;

    @Autowired
    private MediaItemRepository mediaItemRepository;

    private MediaItem mediaItemWithoutOwner;
    private MediaItem mediaItemOwnerAppVariant1;
    private MediaItem mediaItemOwnerAppVariant1ReferencedByAnna;
    private MediaItem mediaItemOtherOwner;

    private TemporaryMediaItem temporaryMediaItemOwnerAnna;
    private TemporaryMediaItem temporaryMediaItemOwnerAnnaAndAppVariant1;
    private TemporaryMediaItem temporaryMediaItemOtherOwner;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();

        createMediaItemsAndTemporaryMediaItems();
    }

    private void createMediaItemsAndTemporaryMediaItems() {

        // media items
        mediaItemWithoutOwner = th.createMediaItem("unknown");
        mediaItemWithoutOwner.setFileOwnership(FileOwnership.UNKNOWN);
        mediaItemRepository.saveAndFlush(mediaItemWithoutOwner);

        mediaItemOwnerAppVariant1 = th.createMediaItem("vacation-anna");
        mediaItemOwnerAppVariant1.setFileOwnership(FileOwnership.of(th.app1Variant1));
        mediaItemRepository.saveAndFlush(mediaItemOwnerAppVariant1);

        mediaItemOwnerAppVariant1ReferencedByAnna = th.createMediaItem("profile-anna");
        mediaItemOwnerAppVariant1ReferencedByAnna.setFileOwnership(FileOwnership.of(th.app1Variant1));
        mediaItemRepository.saveAndFlush(mediaItemOwnerAppVariant1ReferencedByAnna);

        th.personRegularAnna.setProfilePicture(mediaItemOwnerAppVariant1ReferencedByAnna);
        th.personRepository.saveAndFlush(th.personRegularAnna);

        mediaItemOtherOwner = th.createMediaItem("vacation-karl");
        mediaItemOtherOwner.setFileOwnership(FileOwnership.of(th.app1Variant2));
        mediaItemRepository.saveAndFlush(mediaItemOtherOwner);

        // temporary media items
        temporaryMediaItemOwnerAnna =
                th.createTemporaryMediaItem(th.personRegularAnna, th.nextTimeStamp(), th.nextTimeStamp());
        MediaItem mediaItem = temporaryMediaItemOwnerAnna.getMediaItem();
        mediaItem.setFileOwnership(FileOwnership.of(th.personRegularAnna));
        mediaItemRepository.saveAndFlush(mediaItem);

        temporaryMediaItemOwnerAnnaAndAppVariant1 =
                th.createTemporaryMediaItem(th.personRegularAnna, th.nextTimeStamp(), th.nextTimeStamp());
        mediaItem = temporaryMediaItemOwnerAnnaAndAppVariant1.getMediaItem();
        mediaItem.setFileOwnership(FileOwnership.of(th.personRegularAnna, th.app1Variant1));
        mediaItemRepository.saveAndFlush(mediaItem);

        temporaryMediaItemOtherOwner =
                th.createTemporaryMediaItem(th.personRegularKarl, th.nextTimeStamp(), th.nextTimeStamp());
        mediaItem = temporaryMediaItemOtherOwner.getMediaItem();
        mediaItem.setFileOwnership(FileOwnership.of(th.personRegularKarl));
        mediaItemRepository.saveAndFlush(mediaItem);
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    @Transactional
    public void temporaryMediaItemUseRequest() throws Exception {

        final Person owner = th.personRegularAnna;
        final AppVariant appVariant = th.app1Variant1;
        final MediaItem expectedMediaItemOwnerAnna =
                mediaItemRepository.findById(temporaryMediaItemOwnerAnna.getMediaItem().getId()).get();
        final MediaItem expectedMediaItemOwnerAnnaAndAppVariant1 =
                mediaItemRepository.findById(temporaryMediaItemOwnerAnnaAndAppVariant1.getMediaItem().getId()).get();

        assertEquals(temporaryMediaItemOwnerAnna.getMediaItem(), expectedMediaItemOwnerAnna);
        assertEquals(owner, expectedMediaItemOwnerAnna.getOwner());
        assertThat(expectedMediaItemOwnerAnna.getAppVariant()).isNull();
        assertThat(expectedMediaItemOwnerAnna.getApp()).isNull();

        assertEquals(temporaryMediaItemOwnerAnnaAndAppVariant1.getMediaItem(),
                expectedMediaItemOwnerAnnaAndAppVariant1);
        assertEquals(owner, expectedMediaItemOwnerAnnaAndAppVariant1.getOwner());
        assertEquals(expectedMediaItemOwnerAnnaAndAppVariant1.getAppVariant(), th.app1Variant1);
        assertEquals(expectedMediaItemOwnerAnnaAndAppVariant1.getApp(), th.app1);

        mockMvc.perform(post("/media/event/temporaryMediaItemUseRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(owner)
                        .apiKey(appVariant.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientTemporaryMediaItemUseRequest.builder()
                        .temporaryMediaItemIds(Arrays.asList(temporaryMediaItemOwnerAnna.getId(),
                                temporaryMediaItemOwnerAnnaAndAppVariant1.getId()))
                        .build())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.clientMediaItems[0].id").value(expectedMediaItemOwnerAnna.getId()))
                .andExpect(
                        jsonPath("$.clientMediaItems[0].urls").value(expectedMediaItemOwnerAnna.getUrls()))
                .andExpect(jsonPath("$.clientMediaItems[1].id").value(expectedMediaItemOwnerAnnaAndAppVariant1.getId()))
                .andExpect(
                        jsonPath("$.clientMediaItems[1].urls").value(
                                expectedMediaItemOwnerAnnaAndAppVariant1.getUrls()));

        final MediaItem updatedMediaItemOwnerAnna =
                mediaItemRepository.findById(expectedMediaItemOwnerAnna.getId()).get();
        assertEquals(owner, updatedMediaItemOwnerAnna.getOwner());
        assertEquals(appVariant, updatedMediaItemOwnerAnna.getAppVariant());
        assertEquals(th.app1, updatedMediaItemOwnerAnna.getApp());
        // check if temporaryMediaItem is deleted
        assertFalse(temporaryMediaItemRepository.findById(temporaryMediaItemOwnerAnna.getId()).isPresent());

        final MediaItem updatedMediaItemOwnerAnnaAndAppVariant1 =
                mediaItemRepository.findById(expectedMediaItemOwnerAnnaAndAppVariant1.getId()).get();
        assertEquals(owner, updatedMediaItemOwnerAnnaAndAppVariant1.getOwner());
        assertEquals(appVariant, updatedMediaItemOwnerAnnaAndAppVariant1.getAppVariant());
        assertEquals(th.app1, updatedMediaItemOwnerAnnaAndAppVariant1.getApp());
        // check if temporaryMediaItem is deleted
        assertFalse(
                temporaryMediaItemRepository.findById(temporaryMediaItemOwnerAnnaAndAppVariant1.getId()).isPresent());
    }

    @Test
    public void temporaryMediaItemUseRequest_TemporaryMediaItemNotFound() throws Exception {

        // invalid TemporaryMediaItemId
        final String invalidTemporaryMediaItemId = UUID.randomUUID().toString();
        mockMvc.perform(post("/media/event/temporaryMediaItemUseRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .person(th.personRegularAnna)
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientTemporaryMediaItemUseRequest.builder()
                                .temporaryMediaItemIds(
                                        Arrays.asList(temporaryMediaItemOwnerAnna.getId(), invalidTemporaryMediaItemId))
                                .build())))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        // person not owner of TemporaryMediaItem
        final Person owner = th.personRegularAnna;
        mockMvc.perform(post("/media/event/temporaryMediaItemUseRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .person(owner)
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientTemporaryMediaItemUseRequest.builder()
                                .temporaryMediaItemIds(Arrays.asList(temporaryMediaItemOwnerAnna.getId(),
                                        temporaryMediaItemOtherOwner.getId()))
                                .build())))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void temporaryMediaItemUseRequest_InvalidParameters() throws Exception {

        // temporaryMediaItemIds NULL
        mockMvc.perform(post("/media/event/temporaryMediaItemUseRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(th.personRegularAnna)
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientTemporaryMediaItemUseRequest.builder()
                        .temporaryMediaItemIds(null)
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // temporaryMediaItemIds empty
        mockMvc.perform(post("/media/event/temporaryMediaItemUseRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .person(th.personRegularAnna)
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientTemporaryMediaItemUseRequest.builder()
                        .temporaryMediaItemIds(Collections.emptyList())
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void temporaryMediaItemUseRequest_Unauthorized() throws Exception {

        assertOAuth2ApiKeyRequired(post("/media/event/temporaryMediaItemUseRequest")
                .contentType(contentType)
                .content(json(ClientTemporaryMediaItemUseRequest.builder()
                        .temporaryMediaItemIds(Collections.singletonList(temporaryMediaItemOwnerAnna.getId()))
                        .build())), th.app1Variant1.getApiKey1(), th.personRegularAnna);
    }

    @Test
    @Transactional
    public void deleteMediaItemRequest() throws Exception {

        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientMediaItemDeleteRequest.builder()
                        .mediaItemIds(Collections.singletonList(mediaItemOwnerAppVariant1.getId()))
                        .build())))
                .andExpect(status().isOk());

        // check if mediaItemOwnerAppVariant1 is deleted
        assertFalse(mediaItemRepository.findById(mediaItemOwnerAppVariant1.getId()).isPresent());
    }

    @Test
    public void deleteMediaItemRequest_MediaItemNotFound() throws Exception {

        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientMediaItemDeleteRequest.builder()
                                .mediaItemIds(Arrays.asList(mediaItemOwnerAppVariant1.getId(), UUID.randomUUID().toString()))
                                .build())))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_NOT_FOUND));
    }

    @Test
    @Transactional
    public void deleteMediaItemRequest_MediaItemUsageNotAllowed() throws Exception {

        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientMediaItemDeleteRequest.builder()
                        .mediaItemIds(Arrays.asList(mediaItemOwnerAppVariant1.getId(), mediaItemOtherOwner.getId()))
                        .build())))
                .andExpect(isException(ClientExceptionType.MEDIA_ITEM_USAGE_NOT_ALLOWED));

        assertTrue(mediaItemRepository.findById(mediaItemOwnerAppVariant1.getId()).isPresent());
        assertTrue(mediaItemRepository.findById(mediaItemOtherOwner.getId()).isPresent());
    }

    // this test scenario was tested on a local system successfully since the behaviour of the test setup with
    // H2/Hibernate-Context differs and does not trigger the expected exception
    @Disabled
    @Test
    @Transactional
    public void deleteMediaItemRequest_MediaItemCannotBeDeleted() throws Exception {

        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                        .headers(authHeadersFor(AuthorizationData.builder()
                                .apiKey(th.app1Variant1.getApiKey1())
                                .build()))
                        .contentType(contentType)
                        .content(json(ClientMediaItemDeleteRequest.builder()
                                .mediaItemIds(
                                        Collections.singletonList(mediaItemOwnerAppVariant1ReferencedByAnna.getId()))
                                .build())))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_CANNOT_BE_DELETED));

        // check that media item is not deleted
        final Person personWithProfilePicture = th.personRepository.findById(th.personRegularAnna.getId()).get();
        assertEquals(mediaItemOwnerAppVariant1ReferencedByAnna, personWithProfilePicture.getProfilePicture());
    }

    @Test
    public void deleteMediaItemRequest_InvalidParameters() throws Exception {

        // mediaItemIds NULL
        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientMediaItemDeleteRequest.builder()
                        .mediaItemIds(null)
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // mediaItemIds empty
        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey(th.app1Variant1.getApiKey1())
                        .build()))
                .contentType(contentType)
                .content(json(ClientMediaItemDeleteRequest.builder()
                        .mediaItemIds(Collections.emptyList())
                        .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void deleteMediaItemRequest_Unauthorized() throws Exception {

        mockMvc.perform(post("/media/event/deleteMediaItemRequest")
                .headers(authHeadersFor(AuthorizationData.builder()
                        .apiKey("my own api key")
                        .build()))
                .contentType(contentType)
                .content(json(ClientMediaItemDeleteRequest.builder()
                        .mediaItemIds(Collections.singletonList(mediaItemOwnerAppVariant1.getId()))
                        .build())))
                .andExpect(isNotAuthorized());
    }

}
