/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.userdata.controllers;

import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.userdata.clientmodel.ClientUserDataKeyValueEntry;
import de.fhg.iese.dd.platform.api.shared.userdata.clientmodel.UserDataClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;

public class UserDataControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private UserDataClientModelMapper userDataClientModelMapper;

    @Override
    public void createEntities() throws Exception {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createUserDataEntities(th.personIeseAdmin);

    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getAllCategories() throws Exception {

        List<UserDataCategory> expectedCategories = th.userDataCategoryList.stream()
                .filter(i->i.getOwner().equals(th.personIeseAdmin))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        mockMvc.perform(get("/userdata/category")
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedCategories.size())))
                .andExpect(jsonPath("$[0]").value(expectedCategories.get(0).getName()))
                .andExpect(jsonPath("$[1]").value(expectedCategories.get(1).getName()))
                .andExpect(jsonPath("$[2]").value(expectedCategories.get(2).getName()));
    }

    @Test
    public void getAllCategoriesSince() throws Exception {

        List<UserDataCategory> expectedCategories = th.userDataCategoryList.stream()
                .filter(i -> i.getOwner().equals(th.personIeseAdmin)
                    && th.userDataCategory3.getLastChange() < i.getLastChange())
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        mockMvc.perform(get("/userdata/category")
                .param("since", Long.toString(th.userDataCategory3.getLastChange()))
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedCategories.size())))
                .andExpect(jsonPath("$[0]").value(expectedCategories.get(0).getName()))
                .andExpect(jsonPath("$[1]").value(expectedCategories.get(1).getName()))
                .andExpect(jsonPath("$[2]").value(expectedCategories.get(2).getName()));
    }

    @Test
    public void addCategory() throws Exception {

        mockMvc.perform(post("/userdata/category/" + "Sample Category")
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isCreated());

        // check that category was saved in repository
        assertNotNull(th.userDataCategoryRepository.findByNameAndOwner("Sample Category", th.personIeseAdmin));
    }

    @Test
    public void addCategoryNameAlreadyExisting() throws Exception {

        UserDataCategory oldCategory =
                th.userDataCategoryRepository.findByNameAndOwner(th.userDataCategory1.getName(), th.personIeseAdmin);

        mockMvc.perform(post("/userdata/category/" + th.userDataCategory1.getName())
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isCreated());

        UserDataCategory newCategory =
                th.userDataCategoryRepository.findByNameAndOwner(th.userDataCategory1.getName(), th.personIeseAdmin);

        assertEquals(oldCategory.getId(), newCategory.getId());
        assertEquals(oldCategory.getCreated(), newCategory.getCreated());
    }

    @Test
    public void addCategoryNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(post("/userdata/category/")
                        .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.type").value(ClientExceptionType.UNSPECIFIED_BAD_REQUEST.name()));
    }

    @Test
    public void removeCategory() throws Exception {

        mockMvc.perform(delete("/userdata/category/" + th.userDataCategory1.getName())
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isNoContent());

        // check that the category was actually deleted from the repository
        assertNull(
                th.userDataCategoryRepository.findByNameAndOwner(th.userDataCategory1.getName(), th.personIeseAdmin));
    }

    @Test
    public void removeCategoryNameNotExisting() throws Exception {

        mockMvc.perform(delete("/userdata/category/" + "Category X")
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isNoContent());

    }

    @Test
    public void removeCategoryNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(delete("/userdata/category/")
                        .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError())
            .andExpect(jsonPath("$.type").value(ClientExceptionType.UNSPECIFIED_BAD_REQUEST.name()));
    }

    @Test
    public void getKeyValueEntries() throws Exception {

        List<UserDataKeyValueEntry> expectedKeyValues = th.userDataKeyValueEntryList.stream()
                .filter(i-> i.getCategory().getOwner().equals(th.personIeseAdmin)
                                && i.getCategory().getName().equals(th.userDataCategory1.getName()))
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        // Controller method returns ClientUserDataKeyValueEntry type list
        List<ClientUserDataKeyValueEntry> expectedClientModelMap = expectedKeyValues.stream()
                .map(userDataClientModelMapper::toKeyValuePair)
                .collect(Collectors.toList());

        mockMvc.perform(get("/userdata/category/" + th.userDataCategory1.getName() + "/key")
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedClientModelMap.size())))
                .andExpect(jsonPath("$[0].key").value(expectedClientModelMap.get(0).getKey()))
                .andExpect(jsonPath("$[0].value").value(expectedClientModelMap.get(0).getValue()))
                .andExpect(jsonPath("$[1].key").value(expectedClientModelMap.get(1).getKey()))
                .andExpect(jsonPath("$[1].value").value(expectedClientModelMap.get(1).getValue()))
                .andExpect(jsonPath("$[2].key").value(expectedClientModelMap.get(2).getKey()))
                .andExpect(jsonPath("$[2].value").value(expectedClientModelMap.get(2).getValue()));
    }

    @Test
    public void getKeyValueEntriesCategoryNotExisting() throws Exception {

        mockMvc.perform(get("/userdata/category/" + "UnknownCategory" + "/key")
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().isOk())
            .andExpect(content().string("[]"));
    }

    @Test
    public void getKeyValueEntriesSince() throws Exception {

        List<UserDataKeyValueEntry> expectedKeyValues = th.userDataKeyValueEntryList.stream()
                .filter(i->i.getCategory().getOwner().equals(th.personIeseAdmin)
                        && i.getCategory().getName().equals(th.userDataCategory2.getName())
                        && th.userDataKeyValueEntry3Cat1.getLastChange() < i.getLastChange())
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .collect(Collectors.toList());

        // Controller method returns ClientUserDataKeyValueEntry type list
        List<ClientUserDataKeyValueEntry> expectedClientModelMap = expectedKeyValues.stream()
                .map(userDataClientModelMapper::toKeyValuePair)
                .collect(Collectors.toList());

        mockMvc.perform(get("/userdata/category/" + th.userDataCategory2.getName() + "/key")
                .param("since", Long.toString(th.userDataKeyValueEntry3Cat1.getLastChange()))
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedClientModelMap.size())))
                .andExpect(jsonPath("$[0].key").value(expectedClientModelMap.get(0).getKey()))
                .andExpect(jsonPath("$[0].value").value(expectedClientModelMap.get(0).getValue()))
                .andExpect(jsonPath("$[1].key").value(expectedClientModelMap.get(1).getKey()))
                .andExpect(jsonPath("$[1].value").value(expectedClientModelMap.get(1).getValue()))
                .andExpect(jsonPath("$[2].key").value(expectedClientModelMap.get(2).getKey()))
                .andExpect(jsonPath("$[2].value").value(expectedClientModelMap.get(2).getValue()));
    }

    @Test
    public void getKeyValueEntriesSinceCategoryNotExisting() throws Exception {

        mockMvc.perform(get("/userdata/category/" + "UnknownCategory" + "/key")
            .param("since", Long.toString(th.userDataKeyValueEntry3Cat1.getLastChange()))
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().isOk())
            .andExpect(content().string("[]"));

    }

    @Test
    public void setValueOfKey() throws Exception {

        String expectedValue = "newValue";

        mockMvc.perform(post(String.format("/userdata/category/%s/key/%s/value",
            th.userDataCategory1.getName(),
            th.userDataKeyValueEntry2Cat1.getUniqueKey()))
                .param("value", expectedValue)
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isCreated());

        String actualValue = th.userDataKeyValueEntryRepository.findByUniqueKeyAndCategory(
                th.userDataKeyValueEntry2Cat1.getUniqueKey(),
                th.userDataCategory1).getValueContent();

        // check that value was updated in repository
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void setValueOfKeyCategoryNameNotExisting() throws Exception {

        String expectedValue = "newValue";
        String newCategoryName = "Category New";
        String newKeyName = "Key New";

        mockMvc.perform(post(String.format("/userdata/category/%s/key/%s/value", newCategoryName, newKeyName))
                        .param("value", expectedValue)
                        .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isCreated());

        UserDataCategory newCategory =
                th.userDataCategoryRepository.findByNameAndOwner(newCategoryName, th.personIeseAdmin);
        assertNotNull(newCategory);

        String actualValue = th.userDataKeyValueEntryRepository.findByUniqueKeyAndCategory(newKeyName, newCategory)
                .getValueContent();

        // check that value was updated in repository
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void setValueOfKeyCategoryNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(post(String.format("/userdata/category/%s/key/%s/value", "", th.userDataKeyValueEntry2Cat1.getUniqueKey()))
            .param("value", "newValue")
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void getValue() throws Exception {

        mockMvc.perform(get(String.format("/userdata/category/%s/key/%s/value", th.userDataCategory1.getName(),
                        th.userDataKeyValueEntry2Cat1.getUniqueKey()))
                        .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.key").value(th.userDataKeyValueEntry2Cat1.getUniqueKey()))
                .andExpect(jsonPath("$.value").value(th.userDataKeyValueEntry2Cat1.getValueContent()));
    }

    @Test
    public void getValueCategoryNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(get(String.format("/userdata/category/%s/key/%s/value", "", th.userDataKeyValueEntry2Cat1.getUniqueKey()))
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void getValueCategoryNameNotExisting() throws Exception {

        mockMvc.perform(
                get("/userdata/category/{category}/key/{key}/value", "UnknownCategory",
                        th.userDataKeyValueEntry2Cat1.getUniqueKey())
                        .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyOrNullString())));
    }

    @Test
    public void getValueKeyNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(get(String.format("/userdata/category/%s/key/%s/value", th.userDataCategory1.getName(), ""))
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError())
            .andExpect(jsonPath("$.type").value(ClientExceptionType.UNSPECIFIED_BAD_REQUEST.name()));
    }

    @Test
    public void getValueKeyNameNotExisting() throws Exception {

        mockMvc.perform(
                get("/userdata/category/{category}/key/{key}/value", th.userDataCategory1.getName(), "UnknownKey")
                        .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isOk())
                .andExpect(content().string(is(emptyOrNullString())));
    }

    @Test
    public void removeKeyValue() throws Exception {

        mockMvc.perform(delete(String.format("/userdata/category/%s/key/%s",
            th.userDataCategory2.getName(),
            th.userDataKeyValueEntry5Cat2.getUniqueKey()))
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isNoContent());

        // check that key and value was deleted in repository
        assertNull(th.userDataKeyValueEntryRepository.findByUniqueKeyAndCategory(
                th.userDataKeyValueEntry5Cat2.getUniqueKey(), th.userDataCategory2));
    }

    @Test
    public void removeKeyValueCategoryNotExisting() throws Exception {

        mockMvc.perform(delete(String.format("/userdata/category/%s/key/%s", "Category Not Existing", th.userDataKeyValueEntry5Cat2.getUniqueKey()))
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isNoContent());

    }

    @Test
    public void removeKeyValueKeyNotExisting() throws Exception {

        mockMvc.perform(delete(String.format("/userdata/category/%s/key/%s", th.userDataCategory2.getName(), "NotExisting"))
                .headers(authHeadersFor(th.personIeseAdmin)))
                .andExpect(status().isNoContent());

    }

    @Test
    public void removeKeyValueCategoryNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(delete(String.format("/userdata/category/%s/key/%s", "", th.userDataKeyValueEntry4Cat2.getUniqueKey()))
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void removeKeyValueKeyNameEmpty() throws Exception {

        //it is not really possible to check for a sound exception here
        //since empty path parameters totally change the semantic of the call
        mockMvc.perform(delete(String.format("/userdata/category/%s/key/%s", th.userDataCategory2.getName(), ""))
            .headers(authHeadersFor(th.personIeseAdmin)))
            .andExpect(status().is4xxClientError())
            .andExpect(jsonPath("$.type").value(ClientExceptionType.UNSPECIFIED_BAD_REQUEST.name()));
    }

}
