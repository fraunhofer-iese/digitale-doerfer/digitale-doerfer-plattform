/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganization;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientOrganizationTag;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsItem;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostInteraction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class NewsSourceControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Override
    public void createEntities() throws Exception {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void checkNewsConfiguration_WrongOrEmptyAPIKey() throws Exception {
        assertNotEquals("Test setup wrong, same api key for news sources",
                th.newsSourceDigitalbach.getAppVariant().getApiKey1(),
                th.newsSourceAnalogheim.getAppVariant().getApiKey1());

        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, th.newsSourceAnalogheim.getAppVariant().getApiKey1())
                        .param("siteURL", th.newsSourceDigitalbach.getSiteUrl())
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("tenantId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("Wrong api key")))
                .andExpect(jsonPath("$").value(containsString(th.geoAreaTenant1.getName())))
                .andExpect(jsonPath("$").value(containsString(th.tenant1.getName())));

        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, "")
                        .param("siteURL", th.newsSourceDigitalbach.getSiteUrl())
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("tenantId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("Empty api key")))
                .andExpect(jsonPath("$").value(containsString(th.geoAreaTenant1.getName())))
                .andExpect(jsonPath("$").value(containsString(th.tenant1.getName())));

        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .param("siteURL", "wrong site url")
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("tenantId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("News source can not be found")))
                .andExpect(jsonPath("$").value(containsString(th.geoAreaTenant1.getName())))
                .andExpect(jsonPath("$").value(containsString(th.tenant1.getName())));

        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, "")
                        .param("siteURL", th.newsSourceDeleted.getSiteUrl())
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("tenantId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("deleted")));
    }

    @Test
    public void checkNewsConfiguration_InvalidGeoAreaOrTenant() throws Exception {
        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .param("siteURL", th.newsSourceDigitalbach.getSiteUrl())
                        .param("geoAreaId", "wrong id")
                        .param("tenantId", "wrong id"))
                .andExpect(jsonPath("$").value(containsString("Valid and unique api key")))
                .andExpect(jsonPath("$").value(containsString("GeoArea does not exist")))
                .andExpect(jsonPath("$").value(containsString("Tenant does not exist")));
    }

    @Test
    public void checkNewsConfiguration_ValidParams() throws Exception {
        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .param("siteURL", th.newsSourceDigitalbach.getSiteUrl())
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("tenantId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("Valid and unique api key")))
                .andExpect(jsonPath("$").value(containsString(th.geoAreaTenant1.getName())))
                .andExpect(jsonPath("$").value(containsString(th.tenant1.getName())));

        //remove after removing communityId as parameter
        mockMvc.perform(get("/grapevine/event/checkNewsConfiguration")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1())
                        .param("siteURL", th.newsSourceDigitalbach.getSiteUrl())
                        .param("geoAreaId", th.geoAreaTenant1.getId())
                        .param("communityId", th.tenant1.getId()))
                .andExpect(jsonPath("$").value(containsString("Valid and unique api key")))
                .andExpect(jsonPath("$").value(containsString(th.geoAreaTenant1.getName())))
                .andExpect(jsonPath("$").value(containsString(th.tenant1.getName())));
    }

    @Test
    public void getPostStatistics() throws Exception {

        long now = toMillisUTC("2025-04-20T02:00:00Z");
        timeServiceMock.setConstantDateTime(now);
        NewsItem newsItem = th.newsItem2;
        GeoArea geoArea1 = th.geoAreaDorf1InEisenberg;
        GeoArea geoArea2 = th.geoAreaKaiserslautern;
        Person person1 = th.personSusanneChristmann;
        Person person2 = th.personLaraSchaefer;
        Person person3 = th.personFriedaFischer;
        Person person4 = th.personNinaBauer;

        person1.setHomeArea(geoArea1);
        person2.setHomeArea(geoArea2);
        person3.setHomeArea(geoArea2);
        person4.setHomeArea(geoArea2);

        th.personRepository.saveAll(List.of(person1, person2, person3, person4));

        newsItem.setLikeCount(1);
        th.postRepository.save(newsItem);
        postInteractionRepository.save(PostInteraction.builder()
                .person(person1)
                .post(newsItem)
                .liked(true)
                .viewedOverviewTime(toMillisUTC("2025-04-20T00:30:00Z"))
                .viewedDetailTime(toMillisUTC("2025-04-20T01:30:00Z"))
                .build());

        postInteractionRepository.save(PostInteraction.builder()
                .person(person2)
                .post(newsItem)
                .liked(false)
                .viewedOverviewTime(toMillisUTC("2025-04-19T00:50:00Z"))
                .viewedDetailTime(toMillisUTC("2025-04-19T01:50:00Z"))
                .build());

        postInteractionRepository.save(PostInteraction.builder()
                .person(person3)
                .post(newsItem)
                .liked(false)
                .viewedOverviewTime(toMillisUTC("2025-04-20T12:30:00Z"))
                .viewedDetailTime(toMillisUTC("2025-04-20T13:30:00Z"))
                .build());

        postInteractionRepository.save(PostInteraction.builder()
                .person(person4)
                .post(newsItem)
                .liked(false)
                .viewedOverviewTime(toMillisUTC("2025-04-20T08:30:00Z"))
                .viewedDetailTime(null)
                .build());

        mockMvc.perform(get("/grapevine/statistics/post/{postId}", newsItem.getId())
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(newsItem.getId()))
                .andExpect(jsonPath("$.commentCount").value(4))
                .andExpect(jsonPath("$.likeCount").value(1))
                .andExpect(jsonPath("$.viewedOverviewCount").value(4))
                .andExpect(jsonPath("$.viewedDetailCount").value(3))
                .andExpect(jsonPath("$.dayTimeStatistics[0].startHour").value(0))
                .andExpect(jsonPath("$.dayTimeStatistics[0].endHour").value(1))
                .andExpect(jsonPath("$.dayTimeStatistics[0].viewedOverviewCount").value(2))
                .andExpect(jsonPath("$.dayTimeStatistics[0].viewedDetailCount").value(0))
                .andExpect(jsonPath("$.dayTimeStatistics[1].startHour").value(1))
                .andExpect(jsonPath("$.dayTimeStatistics[1].endHour").value(2))
                .andExpect(jsonPath("$.dayTimeStatistics[1].viewedOverviewCount").value(0))
                .andExpect(jsonPath("$.dayTimeStatistics[1].viewedDetailCount").value(2))
                .andExpect(jsonPath("$.dayTimeStatistics[12].startHour").value(12))
                .andExpect(jsonPath("$.dayTimeStatistics[12].endHour").value(13))
                .andExpect(jsonPath("$.dayTimeStatistics[12].viewedOverviewCount").value(1))
                .andExpect(jsonPath("$.dayTimeStatistics[12].viewedDetailCount").value(0))
                .andExpect(jsonPath("$.dayTimeStatistics[13].startHour").value(13))
                .andExpect(jsonPath("$.dayTimeStatistics[13].endHour").value(14))
                .andExpect(jsonPath("$.dayTimeStatistics[13].viewedOverviewCount").value(0))
                .andExpect(jsonPath("$.dayTimeStatistics[13].viewedDetailCount").value(1))
                .andExpect(jsonPath("$.dayTimeStatistics[25]").doesNotExist())
                .andExpect(jsonPath("$.geoAreaStatistics[0].geoArea.id").value(geoArea2.getId()))
                .andExpect(jsonPath("$.geoAreaStatistics[0].viewedOverviewCount").value(3))
                .andExpect(jsonPath("$.geoAreaStatistics[0].viewedDetailCount").value(2))
                .andExpect(jsonPath("$.geoAreaStatistics[1].geoArea.id").value(geoArea1.getId()))
                .andExpect(jsonPath("$.geoAreaStatistics[1].viewedOverviewCount").value(1))
                .andExpect(jsonPath("$.geoAreaStatistics[1].viewedDetailCount").value(1))
                .andExpect(jsonPath("$.geoAreaStatistics[2]").doesNotExist())
                .andExpect(jsonPath("$.pastDaysStatistics[0].startTime").value(toMillisUTC("2025-04-20T00:00:00Z")))
                .andExpect(jsonPath("$.pastDaysStatistics[0].endTime").value(now))
                .andExpect(jsonPath("$.pastDaysStatistics[0].viewedOverviewCount").value(3))
                .andExpect(jsonPath("$.pastDaysStatistics[0].viewedDetailCount").value(2))
                .andExpect(jsonPath("$.pastDaysStatistics[1].startTime").value(toMillisUTC("2025-04-19T00:00:00Z")))
                .andExpect(jsonPath("$.pastDaysStatistics[1].endTime").value(toMillisUTC("2025-04-20T00:00:00Z") - 1))
                .andExpect(jsonPath("$.pastDaysStatistics[1].viewedOverviewCount").value(1))
                .andExpect(jsonPath("$.pastDaysStatistics[1].viewedDetailCount").value(1))
                .andExpect(jsonPath("$.pastDaysStatistics[15]").doesNotExist());
    }

    @Test
    public void getPostStatistics_NoOrWrongOrEmptyAPIKey() throws Exception {
        assertNotEquals("Test setup wrong, same api key for news sources",
                th.newsSourceDigitalbach.getAppVariant().getApiKey1(),
                th.newsSourceAnalogheim.getAppVariant().getApiKey1());

        NewsItem newsItem = th.newsItem1;

        mockMvc.perform(get("/grapevine/statistics/post/{postId}", newsItem.getId()))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));

        mockMvc.perform(get("/grapevine/statistics/post/{postId}", newsItem.getId())
                        .header(HEADER_NAME_API_KEY, th.newsSourceAnalogheim.getAppVariant().getApiKey1()))
                .andExpect(status().isForbidden())
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        mockMvc.perform(get("/grapevine/statistics/post/{postId}", newsItem.getId())
                        .header(HEADER_NAME_API_KEY, ""))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void getPostStatistics_InvalidParameters() throws Exception {
        mockMvc.perform(get("/grapevine/statistics/post/{postId}", "invalid id")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1()))
                .andExpect(status().isNotFound())
                .andExpect(isException(ClientExceptionType.POST_NOT_FOUND));
    }

    @Test
    public void getAvailableGeoAreas() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        List<ClientGeoArea> expectedGeoAreas = List.of(
                th.toClientGeoArea(th.geoAreaEisenberg, 2, false,
                        th.toClientGeoArea(th.geoAreaDorf1InEisenberg, 3, true),
                        th.toClientGeoArea(th.geoAreaDorf2InEisenberg, 3, true)),
                th.toClientGeoArea(th.geoAreaKaiserslautern, 2, true));

        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(expectedGeoAreas.size())))
                .andExpect(jsonEquals(expectedGeoAreas));
    }

    @Test
    public void getAvailableGeoAreas_NoOrWrongOrEmptyAPIKey() throws Exception {

        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas"))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));

        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, "invalid api key"))
                .andExpect(status().isForbidden())
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, ""))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void getAvailableGeoAreas_NewsSourceNotFound() throws Exception {

        // deleted NewsSource
        th.newsSourceDigitalbach.setDeleted(true);
        th.newsSourceRepository.save(th.newsSourceDigitalbach);

        assertThat(th.newsSourceRepository.findByAppVariantAndDeletedFalse(th.newsSourceDigitalbach.getAppVariant())).isEmpty();
        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1()))
                .andExpect(status().isNotFound())
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));

        // AppVariant without NewsSource
        AppVariant appVariantNoNewsSource = th.appVariantRepository.save(AppVariant.builder()
                .app(th.appDorfNews)
                .oauthClients(Collections.singleton(th.appVariantWithApiKey1Tenant1OauthClient))
                .appVariantIdentifier("no-news-source")
                .apiKey1("newsTestApiKey")
                .apiKey2("secondTestApiKey")
                .callBackUrl("https://example.org")
                .name("AppVariant without NewsSource")
                .build());

        assertThat(th.newsSourceRepository.findByAppVariantAndDeletedFalse(appVariantNoNewsSource)).isEmpty();
        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, appVariantNoNewsSource.getApiKey1()))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));
    }

    @Test
    public void getAvailableOrganizations() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;
        List<ClientOrganization> allOrganizations = List.of(
                th.toClientOrganization(th.organizationFeuerwehr, List.of(th.geoAreaDorf1InEisenberg.getId())),
                th.toClientOrganization(th.organizationGartenbau,
                        List.of(th.geoAreaDorf1InEisenberg.getId(), th.geoAreaDorf2InEisenberg.getId()))
        );

        // all tags
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(allOrganizations.size())))
                .andExpect(jsonEquals(allOrganizations));

        // volunteering only
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .param("requiredTags", ClientOrganizationTag.VOLUNTEERING.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(allOrganizations.size())))
                .andExpect(jsonEquals(allOrganizations));

        // emergency aid only
        List<ClientOrganization> emergencyAidOrganizations = List.of(
                th.toClientOrganization(th.organizationFeuerwehr, List.of(th.geoAreaDorf1InEisenberg.getId())));
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .param("requiredTags", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonEquals(emergencyAidOrganizations));

        // news source without organizations
        NewsSource newsSourceNoOrganizations = th.newsSourceAnalogheim;
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSourceNoOrganizations.getAppVariant().getApiKey1()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAvailableOrganizations_InvalidParameters() throws Exception {

        NewsSource newsSource = th.newsSourceDigitalbach;

        //empty tag, should be ignored
        List<ClientOrganization> emergencyAidOrganizations = List.of(
                th.toClientOrganization(th.organizationFeuerwehr, List.of(th.geoAreaDorf1InEisenberg.getId())));
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .param("requiredTags", "", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonEquals(emergencyAidOrganizations));

        //invalid tag
        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, newsSource.getAppVariant().getApiKey1())
                        .param("requiredTags", "miau", ClientOrganizationTag.EMERGENCY_AID.toString()))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));
    }

    @Test
    public void getAvailableOrganizations_NoOrWrongOrEmptyAPIKey() throws Exception {

        mockMvc.perform(get("/grapevine/newssource/availableOrganizations"))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));

        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, "invalid api key"))
                .andExpect(status().isForbidden())
                .andExpect(isException(ClientExceptionType.NOT_AUTHORIZED));

        mockMvc.perform(get("/grapevine/newssource/availableOrganizations")
                        .header(HEADER_NAME_API_KEY, ""))
                .andExpect(status().isUnauthorized())
                .andExpect(isException(ClientExceptionType.NOT_AUTHENTICATED));
    }

    @Test
    public void getAvailableOrganizations_NewsSourceNotFound() throws Exception {

        // deleted NewsSource
        th.newsSourceDigitalbach.setDeleted(true);
        th.newsSourceRepository.save(th.newsSourceDigitalbach);

        assertThat(th.newsSourceRepository.findByAppVariantAndDeletedFalse(th.newsSourceDigitalbach.getAppVariant())).isEmpty();
        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, th.newsSourceDigitalbach.getAppVariant().getApiKey1()))
                .andExpect(status().isNotFound())
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));

        // AppVariant without NewsSource
        AppVariant appVariantNoNewsSource = th.appVariantRepository.save(AppVariant.builder()
                .app(th.appDorfNews)
                .oauthClients(Collections.singleton(th.appVariantWithApiKey1Tenant1OauthClient))
                .appVariantIdentifier("no-news-source")
                .apiKey1("newsTestApiKey")
                .apiKey2("secondTestApiKey")
                .callBackUrl("https://example.org")
                .name("AppVariant without NewsSource")
                .build());

        assertThat(th.newsSourceRepository.findByAppVariantAndDeletedFalse(appVariantNoNewsSource)).isEmpty();
        mockMvc.perform(get("/grapevine/newssource/availableGeoAreas")
                        .header(HEADER_NAME_API_KEY, appVariantNoNewsSource.getApiKey1()))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));
    }

    private long toMillisUTC(String isoDateTime) {
        return Instant.from(DateTimeFormatter.ISO_DATE_TIME.parse(isoDateTime)).toEpochMilli();
    }

}
