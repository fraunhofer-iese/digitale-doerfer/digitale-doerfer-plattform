/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import static de.fhg.iese.dd.platform.business.grapevine.statistics.GroupStatisticsProvider.STATISTICS_ID_GROUP_COUNT;
import static de.fhg.iese.dd.platform.business.grapevine.statistics.GroupStatisticsProvider.STATISTICS_ID_GROUP_COUNT_DELETED;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.NEW;
import static de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation.TOTAL;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GroupTestHelper;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReport;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public class GroupStatisticsProviderTest extends BaseServiceTest {

    @Autowired
    private GroupTestHelper th;

    @Autowired
    private IStatisticsService statisticsService;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createGroupFeatureConfiguration();
        th.createGroupsAndGroupPostsWithComments();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void generateStatisticsReport_Groups() {
        timeServiceMock.setConstantDateTime(ZonedDateTime.parse("2023-03-21T12:01:34+01:00[Europe/Berlin]"));
        long currentTime = timeServiceMock.currentTimeMillisUTC();

        th.groupAdfcAAP.setCreator(th.personAnjaTenant1Dorf1);
        th.groupAdfcAAP.setCreated(currentTime - TimeUnit.DAYS.toMillis(14));
        th.groupGlasfaserSSP.setCreator(th.personAnjaTenant1Dorf1);
        th.groupErfahrungMMA.setCreator(th.personDagmarTenant2Mainz);
        th.groupDorfgeschichteSSA.setCreator(th.personBenTenant1Dorf2);
        th.groupKatzenAAP.setCreator(th.personAnjaTenant1Dorf1);
        th.groupKatzenAAP.setDeleted(true);
        th.groupKatzenAAP.setDeletionTime(currentTime - TimeUnit.DAYS.toMillis(21));
        th.groupDeleted.setCreator(th.personFranziTenant1Dorf2NoMember);
        th.groupRepository.saveAll(List.of(th.groupAdfcAAP, th.groupGlasfaserSSP, th.groupErfahrungMMA,
                th.groupDorfgeschichteSSA, th.groupKatzenAAP, th.groupDeleted));

        StatisticsReportDefinition statisticsReportDefinition = StatisticsReportDefinition.builder()
                .statisticsId(STATISTICS_ID_GROUP_COUNT)
                .statisticsId(STATISTICS_ID_GROUP_COUNT_DELETED)
                .startTimeNew(currentTime - TimeUnit.DAYS.toMillis(7))
                .endTime(currentTime)
                .allGeoAreas(true)
                .build();
        StatisticsReport statisticsReport = statisticsService.generateStatisticsReport(statisticsReportDefinition);
        assertThat(statisticsReport.getTitle()).isEqualTo("Current statistics for test at 21.03.2023 12:01:34");
        assertThat(statisticsReport.getDefinition()).isEqualTo(statisticsReportDefinition);
        assertThat(statisticsReport.getCreationTime()).isEqualTo(currentTime);
        assertThat(statisticsReport.getGeoAreaStatistics()).hasSize(8);

        GeoAreaStatistics statisticsDeutschland = findStatisticsFor(statisticsReport, th.geoAreaDeutschland);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, TOTAL).getValue())
                .isEqualTo(6L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, NEW).getValue())
                .isEqualTo(5L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, TOTAL).getValue())
                .isEqualTo(2L);
        assertThat(statisticsDeutschland.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, NEW).getValue())
                .isEqualTo(1L);

        GeoAreaStatistics statisticsMainz = findStatisticsFor(statisticsReport, th.geoAreaMainz);
        assertThat(statisticsMainz.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsMainz.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, NEW).getValue())
                .isEqualTo(1L);
        assertThat(statisticsMainz.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, TOTAL).getValue())
                .isEqualTo(0L);
        assertThat(statisticsMainz.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, NEW).getValue())
                .isEqualTo(0L);

        GeoAreaStatistics statisticsDorf1 = findStatisticsFor(statisticsReport, th.geoAreaDorf1InEisenberg);
        assertThat(statisticsDorf1.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, TOTAL).getValue())
                .isEqualTo(3L);
        assertThat(statisticsDorf1.getStatisticsValue(STATISTICS_ID_GROUP_COUNT, NEW).getValue())
                .isEqualTo(2L);
        assertThat(statisticsDorf1.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, TOTAL).getValue())
                .isEqualTo(1L);
        assertThat(statisticsDorf1.getStatisticsValue(STATISTICS_ID_GROUP_COUNT_DELETED, NEW).getValue())
                .isEqualTo(0L);
    }

    private GeoAreaStatistics findStatisticsFor(StatisticsReport statisticsReport, GeoArea geoArea) {
        return statisticsReport.getGeoAreaStatistics().stream()
                .filter(geoAreaStatistics -> geoArea.equals(geoAreaStatistics.getGeoArea()))
                .findFirst()
                .get();
    }

}
