/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Dominik Schnier, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine;

import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.BaseDataPrivacyHandlerTest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class GrapevineDataPrivacyHandlerTest extends BaseDataPrivacyHandlerTest {

    @Autowired
    private GrapevineTestHelper grapevineTestHelper;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Autowired
    private LikeCommentRepository likeCommentRepository;

    @Override
    public void createEntities() throws Exception {

        grapevineTestHelper.createGrapevineScenario();
        grapevineTestHelper.createShops();
        grapevineTestHelper.createAchievementRules();
    }

    @Override
    public void tearDown() throws Exception {
        grapevineTestHelper.deleteAllData();
    }

    @Override
    public Person getPersonForPrivacyReport() {
        return grapevineTestHelper.personSusanneChristmann;
    }

    @Override
    public Person getPersonForDeletion() {
        return getPersonForPrivacyReport();
    }

    @Override
    public Collection<Class<? extends JpaRepository<?, ?>>> getExcludedRepositories() {
        return Arrays.asList(
                //tested at GroupDataPrivacyServiceTest
                GroupRepository.class,
                GroupGeoAreaMappingRepository.class,
                GroupMembershipRepository.class,
                //not connected to a person
                ConvenienceRepository.class,
                ConvenienceGeoAreaMappingRepository.class,
                OrganizationRepository.class,
                OrganizationPersonRepository.class,
                OrganizationGeoAreaMappingRepository.class);
    }

    @Test
    public void testMediaItemsInPrivacyReport() {

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);

        final List<String> filenames =
                Arrays.asList("profile-Susanne.jpg", "flyer1.jpg", "flyer2.jpg", "image1.jpg", "image2.jpg",
                        "image3.jpg",
                        "image4.jpg", "image5.jpg", "image6.jpg", "image7.jpg", "image8.jpg", "image9.jpg",
                        "Kleider.jpg", "Schuhe.jpg", "Sekt.jpg", "Wein.jpg", "Biergarnitur.jpg", "Kueche.jpg",
                        "gebuesch.jpg", "muell.jpg");

        filenames.forEach(filename -> assertThat(dataPrivacyReport).is(containsMediaItemWithFilename(filename)));
    }

    @Test
    public void testDocumentItemsInPrivacyReport() {

        final IDataPrivacyReport dataPrivacyReport = new DataPrivacyReport("");
        internalDataPrivacyService.collectInternalUserData(getPersonForPrivacyReport(), dataPrivacyReport);
        DataPrivacyDataCollection dataCollection =
                commonDataPrivacyService.startDataPrivacyDataCollection(getPersonForPrivacyReport(),
                        DataPrivacyWorkflowTrigger.USER_TRIGGERED);
        List<String> zippedFiles = testFileStorage.getZippedFiles(
                testFileStorage.appendFileName(dataCollection.getInternalFolderPath(), "Datenschutzbericht.zip"));

        final List<String> expectedDocumentTitles =
                Arrays.asList("Kleidung", "Kleidung-Text", "Bier", "Kaffee", "Anleitung",
                        "Arbeitsvertrag", "Kündigung");

        expectedDocumentTitles.forEach(title -> assertThat(dataPrivacyReport).is(containsDocumentItemWithTitle(title)));

        for (String expectedTitle : expectedDocumentTitles) {
            assertThat(zippedFiles)
                    .as("zip file should contain " + expectedTitle)
                    //we don't know the file extension, so we ignore it here
                    .anyMatch(zippedFile -> StringUtils.substringBeforeLast(zippedFile, ".").equals(expectedTitle));
        }
    }

    @Test
    public void deletePostsAndComments() {

        for (Post post : grapevineTestHelper.postsCreatedBySusanneChristmann) {
            final Post postFromRepo = postRepository.findById(post.getId()).get();
            assertThat(postFromRepo).as("Post with text %s should exist", post.getText()).isNotNull();
            assertThat(postFromRepo.getText()).isNotNull();
            assertThat(postFromRepo.getText()).isNot(erasedString());
        }
        final String commentFromRepo =
                commentRepository.findById(grapevineTestHelper.tenant2Gossip1Comment3.getId()).get().getText();
        assertThat(commentFromRepo).isNotNull();
        assertThat(commentFromRepo).isNot(erasedString());

        final PostInteraction gossip1Post1Interaction =
                postInteractionRepository.findById(grapevineTestHelper.gossip1Post1Interaction.getId()).get();
        final LikeComment gossip2Comment1LikeComment1 =
                likeCommentRepository.findById(grapevineTestHelper.gossip2Comment1LikeComment1.getId()).get();

        //check if likePost and likeComment of another person is existing
        assertTrue(postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(
                grapevineTestHelper.gossip1withImages,
                grapevineTestHelper.personThomasBecker));
        assertTrue(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(
                grapevineTestHelper.gossip2Comment3, grapevineTestHelper.personThomasBecker));

        // check if happeningParticipant in happening1 is existing, e.g. the person is participating in happening1
        final HappeningParticipant happeningParticipantHappening1 =
                grapevineTestHelper.happeningParticipantRepository.findByHappeningAndPerson(
                        grapevineTestHelper.happening1, getPersonForDeletion()).get();

        final IPersonDeletionReport report = new PersonDeletionReport();
        internalDataPrivacyService.deleteInternalUserData(getPersonForDeletion(), report);

        for (Post post : grapevineTestHelper.postsCreatedBySusanneChristmann) {
            assertThat(report).is(containsEntityWithOperation(post, DeleteOperation.ERASED));
            final Post postFromRepoAgain = postRepository.findById(post.getId()).get();
            assertThat(postFromRepoAgain.getText()).is(erasedString());
            assertThat(postFromRepoAgain.getImages()).isEmpty();
            assertThat(postFromRepoAgain.getAttachments()).isEmpty();
            assertThat(postFromRepoAgain.getCreatedLocation()).is(erasedGpsLocation());
            assertThat(postFromRepoAgain.getCustomAddress()).isNull();
        }
        final Comment community2Gossip1Comment3 =
                commentRepository.findById(grapevineTestHelper.tenant2Gossip1Comment3.getId()).get();
        assertThat(report).is(containsEntityWithOperation(community2Gossip1Comment3, DeleteOperation.ERASED));
        assertThat(community2Gossip1Comment3.getText()).is(erasedString());
        assertThat(community2Gossip1Comment3.getImages()).isEmpty();
        assertThat(community2Gossip1Comment3.getAttachments()).isEmpty();

        assertThat(report).is(containsEntityWithOperation(gossip1Post1Interaction, DeleteOperation.DELETED));
        assertThat(report).is(containsEntityWithOperation(gossip2Comment1LikeComment1, DeleteOperation.DELETED));

        //assert that likePosts and likeComments of other persons get deleted too when the post or comment gets deleted
        assertFalse(
                postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(
                        grapevineTestHelper.gossip1withImages,
                        grapevineTestHelper.personThomasBecker));
        assertFalse(likeCommentRepository.existsLikeCommentByCommentAndPersonAndLikedIsTrue(
                grapevineTestHelper.gossip2Comment3, grapevineTestHelper.personThomasBecker));

        assertThat(report).is(containsEntityWithOperation(happeningParticipantHappening1, DeleteOperation.DELETED));

        // assert that all happeningParticipants of the deleted person are deleted too
        assertEquals(0, grapevineTestHelper.happeningParticipantRepository.countAllByPerson(
                getPersonForDeletion()));

        //make sure that all posts and comments are deleted, i.e. have the deleted = true flag
        assertThat(postRepository.findAllByCreatorOrderByCreatedDesc(getPersonForDeletion())
                .stream().anyMatch(Post::isNotDeleted)).isFalse();
        assertThat(commentRepository.findAllByCreatorOrderByCreatedDesc(getPersonForDeletion())
                .stream().anyMatch(Comment::isNotDeleted)).isFalse();
        assertTrue(
                CollectionUtils.isEmpty(
                        postInteractionRepository.findAllByPersonOrderByCreatedAsc(getPersonForDeletion())));
        assertTrue(CollectionUtils.isEmpty(
                likeCommentRepository.findAllByPersonOrderByCreatedAsc(getPersonForDeletion())));
    }

}
