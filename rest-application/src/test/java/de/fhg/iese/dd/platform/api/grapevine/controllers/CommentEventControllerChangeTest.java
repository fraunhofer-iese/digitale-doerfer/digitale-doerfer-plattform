/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentChangeConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.comment.ClientCommentChangeRequest;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientDocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CommentEventControllerChangeTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    /*
                                                useEmptyFields  setText     setMediaItemPlaceHolders
    commentChangeRequest_Nothing                -               -           -
    commentChangeRequest_Images                 -               -           +
    commentChangeRequest_Text                   -               +           -
    commentChangeRequest_WithoutUseEmptyFields  -               +           +
    commentChangeRequest_Invalid                +               -           -
    commentChangeRequest_ImagesRemoveAll        +               +           -
    commentChangeRequest_WithUseEmptyFields     +               +           +
    */

    @Test
    public void commentChangeRequest_NoChanges() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment1;
        Person creator = commentToBeChanged.getCreator();

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .useEmptyFields(false)
                .build();

        MvcResult commentChangeResult = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation
                = toObject(commentChangeResult.getResponse(), ClientCommentChangeConfirmation.class);
        assertEquals(commentToBeChanged.getText(), commentChangeConfirmation.getComment().getText());
        assertEquals(creator.getId(), commentChangeConfirmation.getComment().getCreator().getId());
        assertEquals(commentToBeChanged.getPost().getId(), commentChangeConfirmation.getComment().getPostId());

        // Check whether the comment is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentChangeConfirmation.getCommentId()).get();
        assertEquals(commentToBeChanged.getText(), updatedComment.getText());
        assertEquals(commentToBeChanged.getImages(), updatedComment.getImages());
    }

    @Test
    public void commentChangeRequest_CommentCountForSingleGossip() throws Exception {

        Map<String, Integer> gossipIdToCommentCount = th.gossipIdToCommentCount();

        AppVariant appVariant = th.appVariant4AllTenants;
        Person person = th.personThomasBecker;
        Gossip gossipWithComments = th.gossip2WithComments;

        String gossipId = gossipWithComments.getId();
        mockMvc.perform(get("/grapevine/post/" + gossipId)
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.commentCount").value(gossipIdToCommentCount.get(gossipId)));

        Comment commentToBeUpdated = th.gossip2Comment1;
        String commentId = commentToBeUpdated.getId();

        //Create a new comment
        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentId)
                .text("This is the new text for comment 1")
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(person, appVariant))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(status().isOk())
                .andReturn();

        //wait for the change to be processed
        waitForEventProcessing();

        //Now checks that the commentCount has not changed
        mockMvc.perform(get("/grapevine/post/" + gossipId)
                .headers(authHeadersFor(person, appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.gossip.id").value(gossipId))
                .andExpect(jsonPath("$.gossip.commentCount").value(gossipIdToCommentCount.get(gossipId)));

        Post gossip = th.postRepository.findById(gossipId).get();
        assertEquals((long) gossipIdToCommentCount.get(gossipId), gossip.getCommentCount());
    }

    @Test
    public void commentChangeRequest_WithoutUseEmptyFields() throws Exception {
        changeComment(false);
    }

    @Test
    public void commentChangeRequest_WithUseEmptyFields() throws Exception {
        changeComment(true);
    }

    private void changeComment(boolean useEmptyFields) throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<MediaItem> currentImages = commentToBeChanged.getImages();
        List<DocumentItem> currentDocuments = new ArrayList<>(commentToBeChanged.getAttachments());
        Person creator = commentToBeChanged.getCreator();

        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are images to change
        assertTrue(currentImages.size() >= 2, "Comment has too less images, adjust test helper");

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(1).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are documents to change
        assertTrue(currentDocuments.size() >= 2, "Comment has too less documents, adjust test helper");

        List<ClientDocumentItemPlaceHolder> documentHolders = new ArrayList<>();
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(1).getId()).build());
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text("My new comment text")
                .mediaItemPlaceHolders(mediaHolders)
                .documentItemPlaceHolders(documentHolders)
                .useEmptyFields(useEmptyFields)
                .build();

        final MvcResult commentChangeResult = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andExpect(status().isOk())
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation
                = toObject(commentChangeResult.getResponse(), ClientCommentChangeConfirmation.class);
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getCommentId());
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getComment().getId());
        assertEquals(commentChangeRequest.getText(), commentChangeConfirmation.getComment().getText());
        assertEquals(creator.getId(), commentChangeConfirmation.getComment().getCreator().getId());
        assertEquals(commentToBeChanged.getPost().getId(), commentChangeConfirmation.getComment().getPostId());
        assertEquals(mediaHolders.size(), commentChangeConfirmation.getComment().getImages().size());
        assertEquals(documentHolders.size(), commentChangeConfirmation.getComment().getAttachments().size());

        // Check whether the comment is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentToBeChanged.getId()).get();
        List<MediaItem> updatedMediaItems = updatedComment.getImages();
        List<DocumentItem> updatedDocumentItems = new ArrayList<>(updatedComment.getAttachments());

        assertEquals(commentChangeRequest.getText(), updatedComment.getText());

        assertEquals(3, updatedMediaItems.size());
        assertEquals(currentImages.get(1), updatedMediaItems.get(0));
        assertEquals(tempImage1.getMediaItem(), updatedMediaItems.get(1));
        assertEquals(tempImage2.getMediaItem(), updatedMediaItems.get(2));

        assertEquals(3, updatedDocumentItems.size());
        assertThat(updatedComment.getAttachments()).containsExactlyInAnyOrder(currentDocuments.get(1),
                tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem());
    }

    @Test
    public void commentChangeRequest_Text() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment1;
        Person creator = commentToBeChanged.getCreator();

        String text = "this is the new text for comment 6";

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(text)
                .useEmptyFields(false)
                .build();

        MvcResult commentChangeResult = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation
                = toObject(commentChangeResult.getResponse(), ClientCommentChangeConfirmation.class);
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getCommentId());
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getComment().getId());
        assertEquals(commentChangeRequest.getText(), commentChangeConfirmation.getComment().getText());
        assertEquals(creator.getId(), commentChangeConfirmation.getComment().getCreator().getId());
        assertEquals(commentToBeChanged.getPost().getId(), commentChangeConfirmation.getComment().getPostId());

        // Check whether the comment is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentChangeConfirmation.getCommentId()).get();
        assertEquals(text, updatedComment.getText());
    }

    @Test
    public void commentChangeRequest_ExceedingMaxTextLength() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment1;
        Person creator = commentToBeChanged.getCreator();

        String text = RandomStringUtils.randomPrint(th.grapevineConfig.getMaxNumberCharsPerComment() + 1);
        log.info("Using random text '{}'", text);

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(text)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException("text", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void commentChangeRequest_Invalid() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        Person creator = commentToBeChanged.getCreator();

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(null)
                .mediaItemPlaceHolders(Collections.emptyList())
                .useEmptyFields(true)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException("text or mediaItemPlaceHolders", ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void commentChangeRequest_Images() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<MediaItem> currentImages = commentToBeChanged.getImages();
        Person creator = commentToBeChanged.getCreator();

        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are images to change
        assertTrue(currentImages.size() >= 2, "Comment has too less images, adjust test helper");

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(1).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .mediaItemPlaceHolders(mediaHolders)
                .useEmptyFields(false)
                .build();

        final MvcResult commentChangeResult = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andExpect(status().isOk())
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation
                = toObject(commentChangeResult.getResponse(), ClientCommentChangeConfirmation.class);
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getCommentId());
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getComment().getId());
        assertEquals(commentToBeChanged.getText(), commentToBeChanged.getText());
        assertEquals(creator.getId(), commentChangeConfirmation.getComment().getCreator().getId());
        assertEquals(commentToBeChanged.getPost().getId(), commentChangeConfirmation.getComment().getPostId());
        assertEquals(mediaHolders.size(), commentChangeConfirmation.getComment().getImages().size());

        // Check whether the post is also updated in the repository
        // Check whether the comment is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentToBeChanged.getId()).get();
        assertEquals(commentToBeChanged.getText(), updatedComment.getText());

        List<MediaItem> updatedMediaItems = updatedComment.getImages();

        assertEquals(3, updatedMediaItems.size());
        assertEquals(currentImages.get(1), updatedMediaItems.get(0));
        assertEquals(tempImage1.getMediaItem(), updatedMediaItems.get(1));
        assertEquals(tempImage2.getMediaItem(), updatedMediaItems.get(2));

        //Check that we do not produce orphans
        assertFalse(th.mediaItemRepository.existsById(currentImages.get(0).getId()));
    }

    @Test
    public void commentChangeRequest_ImagesRemoveAll() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<MediaItem> currentImages = commentToBeChanged.getImages();
        Person creator = commentToBeChanged.getCreator();

        // Make sure there are images to remove
        assertTrue(currentImages.size() >= 2, "Comment has too less images, adjust test helper");

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(commentToBeChanged.getText())
                .mediaItemPlaceHolders(Collections.emptyList())
                .useEmptyFields(true)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andExpect(status().isOk());

        //wait for the image deletion to be done
        waitForEventProcessing();

        // Check whether the post is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentToBeChanged.getId()).get();

        List<MediaItem> updatedMediaItems = updatedComment.getImages();

        assertTrue(CollectionUtils.isEmpty(updatedMediaItems));

        //Check that we do not produce orphans
        assertTrue(currentImages.stream()
                .filter(m -> th.mediaItemRepository.existsById(m.getId()))
                .collect(Collectors.toList())
                .isEmpty());
    }

    @Test
    public void commentChangeRequest_ImagesInvalid() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<MediaItem> currentImages = commentToBeChanged.getImages();
        Person creator = commentToBeChanged.getCreator();

        //create temp images
        TemporaryMediaItem tempImage1 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are images to change
        assertTrue(currentImages.size() >= 2, "Comment has too less images, adjust test helper");

        List<ClientMediaItemPlaceHolder> mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        //media item id instead of temp media item id
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getMediaItem().getId()).build());

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .mediaItemPlaceHolders(mediaHolders)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(tempImage1.getMediaItem().getId(),
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryMediaItemRepository.existsById(tempImage1.getId()));

        //create media item that does not belong to the post
        MediaItem otherMediaItem = th.createMediaItem("other media item");

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(currentImages.get(0).getId()).build());
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage1.getId()).build());
        //media item that does not belong to the post
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().mediaItemId(otherMediaItem.getId()).build());

        commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .mediaItemPlaceHolders(mediaHolders)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(otherMediaItem.getId(), ClientExceptionType.FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryMediaItemRepository.existsById(tempImage1.getId()));

        //invalid place holder

        //create additional temp image
        TemporaryMediaItem tempImage2 = th.createTemporaryMediaItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        mediaHolders = new ArrayList<>();
        mediaHolders.add(ClientMediaItemPlaceHolder.builder().temporaryMediaItemId(tempImage2.getId()).build());
        //invalid, because both IDs are set
        ClientMediaItemPlaceHolder invalidPlaceHolder = new ClientMediaItemPlaceHolder();
        invalidPlaceHolder.setMediaItemId(currentImages.get(0).getId());
        invalidPlaceHolder.setTemporaryMediaItemId(tempImage1.getId());

        mediaHolders.add(invalidPlaceHolder);

        commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .mediaItemPlaceHolders(mediaHolders)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(invalidPlaceHolder.toString(), ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryMediaItemRepository.existsById(tempImage1.getId()));
        assertTrue(th.temporaryMediaItemRepository.existsById(tempImage2.getId()));
    }

    @Test
    public void commentChangeRequest_Documents() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<DocumentItem> currentDocuments = new ArrayList<>(commentToBeChanged.getAttachments());
        Person creator = commentToBeChanged.getCreator();

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are documents to change
        assertTrue(currentDocuments.size() >= 2, "Comment has too less documents, adjust test helper");

        List<ClientDocumentItemPlaceHolder> documentHolders = new ArrayList<>();
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(1).getId()).build());
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .documentItemPlaceHolders(documentHolders)
                .useEmptyFields(false)
                .build();

        final MvcResult commentChangeResult = mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andExpect(status().isOk())
                .andReturn();

        ClientCommentChangeConfirmation commentChangeConfirmation
                = toObject(commentChangeResult.getResponse(), ClientCommentChangeConfirmation.class);
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getCommentId());
        assertEquals(commentToBeChanged.getId(), commentChangeConfirmation.getComment().getId());
        assertEquals(commentToBeChanged.getText(), commentChangeConfirmation.getComment().getText());
        assertEquals(creator.getId(), commentChangeConfirmation.getComment().getCreator().getId());
        assertEquals(commentToBeChanged.getPost().getId(), commentChangeConfirmation.getComment().getPostId());
        assertEquals(documentHolders.size(), commentChangeConfirmation.getComment().getAttachments().size());

        // Check whether the post is also updated in the repository
        // Check whether the comment is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentToBeChanged.getId()).get();
        assertEquals(commentToBeChanged.getText(), updatedComment.getText());

        List<DocumentItem> updatedDocumentItems = new ArrayList<>(updatedComment.getAttachments());

        assertEquals(3, updatedDocumentItems.size());
        assertThat(updatedComment.getAttachments()).containsExactlyInAnyOrder(currentDocuments.get(1),
                tempDocument1.getDocumentItem(),
                tempDocument2.getDocumentItem());

        //Check that we do not produce orphans
        assertFalse(th.documentItemRepository.existsById(currentDocuments.get(0).getId()));
    }

    @Test
    public void commentChangeRequest_DocumentsRemoveAll() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<DocumentItem> currentDocuments = new ArrayList<>(commentToBeChanged.getAttachments());
        Person creator = commentToBeChanged.getCreator();

        // Make sure there are documents to remove
        assertTrue(currentDocuments.size() >= 2, "Comment has too less documents, adjust test helper");

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(commentToBeChanged.getText())
                .documentItemPlaceHolders(Collections.emptyList())
                .useEmptyFields(true)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(jsonPath("$.commentId").value(commentChangeRequest.getCommentId()))
                .andExpect(status().isOk());

        //wait for the document deletion to be done
        waitForEventProcessing();

        // Check whether the post is also updated in the repository
        Comment updatedComment = th.commentRepository.findById(commentToBeChanged.getId()).get();

        List<DocumentItem> updatedDocumentItems = new ArrayList<>(updatedComment.getAttachments());

        assertTrue(CollectionUtils.isEmpty(updatedDocumentItems));

        //Check that we do not produce orphans
        assertThat(currentDocuments.stream()
                .filter(m -> th.documentItemRepository.existsById(m.getId()))
                .collect(Collectors.toList()))
                .isEmpty();
    }

    @Test
    public void commentChangeRequest_DocumentsInvalid() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        List<DocumentItem> currentDocuments = new ArrayList<>(commentToBeChanged.getAttachments());
        Person creator = commentToBeChanged.getCreator();

        //create temp documents
        TemporaryDocumentItem tempDocument1 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        // Make sure there are documents to change
        assertTrue(currentDocuments.size() >= 2, "Comment has too less documents, adjust test helper");

        List<ClientDocumentItemPlaceHolder> documentHolders = new ArrayList<>();
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        //document item id instead of temp document item id
        documentHolders.add(ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(
                tempDocument1.getDocumentItem().getId()).build());

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .documentItemPlaceHolders(documentHolders)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(tempDocument1.getDocumentItem().getId(),
                        ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId()));

        //create document item that does not belong to the post
        DocumentItem otherDocumentItem = th.createDocumentItem("other document item");

        documentHolders = new ArrayList<>();
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().documentItemId(currentDocuments.get(0).getId()).build());
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument1.getId()).build());
        //document item that does not belong to the post
        documentHolders.add(ClientDocumentItemPlaceHolder.builder().documentItemId(otherDocumentItem.getId()).build());

        commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .documentItemPlaceHolders(documentHolders)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(otherDocumentItem.getId(), ClientExceptionType.FILE_ITEM_NOT_FOUND));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId()));

        //invalid place holder

        //create additional temp document
        TemporaryDocumentItem tempDocument2 =
                th.createTemporaryDocumentItem(creator, th.nextTimeStamp(), th.nextTimeStamp());

        documentHolders = new ArrayList<>();
        documentHolders.add(
                ClientDocumentItemPlaceHolder.builder().temporaryDocumentItemId(tempDocument2.getId()).build());
        //invalid, because both IDs are set
        ClientDocumentItemPlaceHolder invalidPlaceHolder = new ClientDocumentItemPlaceHolder();
        invalidPlaceHolder.setDocumentItemId(currentDocuments.get(0).getId());
        invalidPlaceHolder.setTemporaryDocumentItemId(tempDocument1.getId());

        documentHolders.add(invalidPlaceHolder);

        commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .documentItemPlaceHolders(documentHolders)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(invalidPlaceHolder.toString(), ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        //check that the item is not deleted if the request fails
        assertTrue(th.temporaryDocumentItemRepository.existsById(tempDocument1.getId()));
        assertTrue(th.temporaryDocumentItemRepository.existsById(tempDocument2.getId()));
    }

    @Test
    public void commentChangeRequest_NotOwner() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        Person changingPerson = th.personLaraSchaefer;

        // Make sure that the changing person is not the owner
        assertNotSame(commentToBeChanged.getCreator(), changingPerson);

        String text = "this is the new text for comment";

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(text)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(changingPerson))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_ACTION_NOT_ALLOWED));

        // Check whether the comment is not updated in the repository
        Comment commentFromRepository = th.commentRepository.findById(commentToBeChanged.getId()).get();
        assertNotEquals(text, commentFromRepository.getText());
    }

    @Test
    public void commentChangeRequest_DeletedComment() throws Exception {

        Comment commentToBeChanged = th.gossip2Comment4withImages;
        Person creator = commentToBeChanged.getCreator();

        // Delete the comment
        commentToBeChanged.setDeleted(true);
        commentToBeChanged = th.commentRepository.saveAndFlush(commentToBeChanged);

        String text = "this is the new text for comment";

        ClientCommentChangeRequest commentChangeRequest = ClientCommentChangeRequest.builder()
                .commentId(commentToBeChanged.getId())
                .text(text)
                .useEmptyFields(false)
                .build();

        mockMvc.perform(post("/grapevine/comment/event/commentChangeRequest")
                        .headers(authHeadersFor(creator, th.appVariant4AllTenants))
                        .contentType(contentType)
                        .content(json(commentChangeRequest)))
                .andExpect(isException(ClientExceptionType.COMMENT_NOT_FOUND));

        // Check whether the comment is not updated in the repository
        Comment commentFromRepository = th.commentRepository.findById(commentToBeChanged.getId()).get();
        assertNotEquals(text, commentFromRepository.getText());
    }

    @Test
    public void commentChangeRequest_Unauthorized() throws Exception {

        assertOAuth2(post("/grapevine/comment/event/commentChangeRequest")
                .content(json(ClientCommentChangeRequest.builder()
                        .commentId(th.gossip2Comment1.getId())
                        .text("this is the new text for comment 6")
                        .useEmptyFields(false)
                        .build()))
                .contentType(contentType));
    }

}
