/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Danielle Korth, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.business.shared.security.ListActionConfigurationAction;

public class AuthorizationAdminUiControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getActionDocumentation() throws Exception {

        ListActionConfigurationAction expectedAction = new ListActionConfigurationAction();

        // using an endpoint that uses expectedAction as a required action
        String endpointPattern = "GET /adminui/authorization/action/documentation";

        mockMvc.perform(get("/adminui/authorization/action/documentation")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$..name")
                        .value(hasItems(containsString(expectedAction.getClass().getSimpleName()))))
                .andExpect(jsonPath("$..description")
                        .value(hasItem(expectedAction.getActionDescription())))
                .andExpect(jsonPath("$..endpointsActionRequired[*]")
                        .value(hasItem(endpointPattern)));
    }

    @Test
    public void getActionDocumentation_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/authorization/action/documentation"));

        mockMvc.perform(get("/adminui/authorization/action/documentation")
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());
    }

    @Test
    public void getRoleActionDocumentation() throws Exception {

        mockMvc.perform(get("/adminui/authorization/role/action/documentation")
                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.clientRoleInformation.key==\"TEST_ROLE_A\")]" +
                        ".actionsUnrestricted.[?(@.name==\"shared::TestActionA\")]" +
                        ".endpointsActionRequired[0]")
                        .value("GET /test/testMethodRoleRequiredActionDocumentation"))
                .andExpect(jsonPath("$[?(@.clientRoleInformation.key==\"TEST_ROLE_A\")]" +
                        ".actionsUnrestricted.[?(@.name==\"shared::TestActionA\")]" +
                        ".endpointsActionOptional..*")
                        .value(hasSize(0)))
                .andExpect(jsonPath("$[?(@.clientRoleInformation.key==\"TEST_ROLE_B\")]" +
                        ".actionsUnrestricted.[?(@.name==\"shared::TestActionB\")]" +
                        ".endpointsActionOptional[0]")
                        .value("GET /test/testMethodRoleOptionalActionDocumentation"))
                .andExpect(jsonPath("$[?(@.clientRoleInformation.key==\"TEST_ROLE_B\")]" +
                        ".actionsUnrestricted.[?(@.name==\"shared::TestActionB\")]" +
                        ".endpointsActionRequired..*")
                        .value(hasSize(0)))
                .andExpect(jsonPath("$[?(@.clientRoleInformation.key==\"TEST_ROLE_B\")]" +
                        ".actionsEntityRestricted.[?(@.name==\"shared::TestActionA\")]" +
                        ".endpointsActionRequired[0]")
                        .value("GET /test/testMethodRoleRequiredActionDocumentation"));
    }

    @Test
    public void getRoleActionDocumentation_Unauthorized() throws Exception {

        assertOAuth2(get("/adminui/authorization/role/action/documentation"));

        mockMvc.perform(get("/adminui/authorization/role/action/documentation")
                .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(isNotAuthorized());
    }

}
