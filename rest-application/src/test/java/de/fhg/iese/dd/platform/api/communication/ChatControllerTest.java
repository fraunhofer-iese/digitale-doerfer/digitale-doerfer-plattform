/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2019 Alberto Lara, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.communication;

import static de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier.shorten;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatMessage;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatParticipant;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;

public class ChatControllerTest extends BaseServiceTest {

    @Autowired
    private CommunicationTestHelper th;
    @Autowired
    private IPersonBlockingService personBlockingService;
    @Autowired
    private IChatService chatService;

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
        th.createChatEntities();
    }

    private static List<ChatParticipant> getParticipants(final Chat chat) {
        return chat.getParticipants().stream()
                .sorted(Comparator.comparing(participant -> participant.getPerson().getId()))
                .collect(Collectors.toList());
    }

    private static boolean containsParticipant(final Chat chat, final Person person) {
        return chat.getParticipants().stream()
                .map(ChatParticipant::getPerson)
                .anyMatch(p -> Objects.equals(p, person));
    }

    @Test
    public void getChat() throws Exception {

        String topic = "le topique";
        Person chatPerson1 = th.personRegularAnna;
        Person chatPerson2 = th.personRegularKarl;

        Chat chat = chatService.createChatWithSubjectsAndParticipants(topic, th.chat1SubjectPushCategory,
                Collections.singletonList(th.tenant1),
                Arrays.asList(chatPerson1, chatPerson2));

        ChatMessage chatMessage1 = chatService.addUserTextMessageToChat(chat, chatPerson1, "message1", 12L).getRight();
        ChatMessage chatMessage2 = chatService.addUserTextMessageToChat(chat, chatPerson2, "message2", 12L).getRight();

        List<ChatParticipant> sortedChatParticipants = getParticipants(chat);

        int numberUnreadMessages = 1;

        assertEquals(2, sortedChatParticipants.size());
        assertNull(chatMessage2.getMediaItem());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
                .headers(authHeadersFor(chatPerson1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(chat.getId()))
                .andExpect(jsonPath("$.chatTopic").value(topic))
                .andExpect(jsonPath("$.lastMessage.id").value(chatMessage2.getId()))
                .andExpect(jsonPath("$.lastMessage.chatId").value(chat.getId()))
                .andExpect(jsonPath("$.lastMessage.message").value(chatMessage2.getMessage()))
                .andExpect(jsonPath("$.lastMessage.mediaItem").doesNotExist())
                .andExpect(jsonPath("$.lastMessage.senderFirstName").value(chatMessage2.getSender().getFirstName()))
                .andExpect(jsonPath("$.lastMessage.senderLastName").value(
                        shorten(chatMessage2.getSender().getLastName())))
                .andExpect(jsonPath("$.chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$.chatParticipants[0].id").value(sortedChatParticipants.get(0).getId()))
                .andExpect(jsonPath("$.chatParticipants[0].person.id").value(sortedChatParticipants.get(0).getPerson().getId()))
                //check if the last name is shortened
                .andExpect(jsonPath("$.chatParticipants[0].person.lastName").value(
                        shorten(sortedChatParticipants.get(0).getPerson().getLastName())))
                .andExpect(jsonPath("$.chatParticipants[0].receivedTime").isEmpty())
                .andExpect(jsonPath("$.chatParticipants[0].readTime").isEmpty())
                .andExpect(jsonPath("$.chatParticipants[1].id").value(sortedChatParticipants.get(1).getId()))
                .andExpect(jsonPath("$.chatParticipants[1].person.id").value(sortedChatParticipants.get(1).getPerson().getId()))
                //check if the last name is shortened
                .andExpect(jsonPath("$.chatParticipants[1].person.lastName").value(
                        shorten(sortedChatParticipants.get(1).getPerson().getLastName())))
                .andExpect(jsonPath("$.chatParticipants[1].receivedTime").isEmpty())
                .andExpect(jsonPath("$.chatParticipants[1].readTime").isEmpty())
                .andExpect(jsonPath("$.unreadMessageCount").value(numberUnreadMessages));
    }

    @Test
    public void getChatBlockedPerson() throws Exception {

        Chat chat = th.chat1;
        ChatParticipant chatParticipant = th.chat1Participant1;
        ChatParticipant chatParticipantBlocked = th.chat1Participant2;
        App app = th.appRepository.save(App.builder().build());
        OauthClient oauthClient = th.oauthClientRepository.save(OauthClient.builder()
                .oauthClientIdentifier(UUID.randomUUID().toString())
                .build());
        AppVariant appVariant = th.appVariantRepository.save(AppVariant.builder()
                .app(app)
                .oauthClients(Collections.singleton(oauthClient))
                .appVariantIdentifier("myappvariant")
                .build());
        personBlockingService.blockPerson(app, chatParticipant.getPerson(), chatParticipantBlocked.getPerson());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
                .headers(authHeadersFor(chatParticipant.getPerson(), appVariant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.filterStatus").value(ClientFilterStatus.BLOCKED.toString()));
    }

    @Test
    public void getChatWithMediaItem() throws Exception {

        Chat chat = th.chat2;
        ChatParticipant chatParticipant = th.chat2Participant1;
        ChatMessage lastChatMessage = th.chat2Message1; //created timestamp is the most important!

        List<ChatParticipant> sortedChatParticipants = getParticipants(chat);
        int numberUnreadMessages = 1;

        assertEquals(2, sortedChatParticipants.size());
        assertTrue(chat.getParticipants().contains(chatParticipant));
        assertNotNull(lastChatMessage.getMediaItem());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
            .headers(authHeadersFor(chatParticipant.getPerson())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id").value(chat.getId()))
            .andExpect(jsonPath("$.lastMessage.id").value(lastChatMessage.getId()))
            .andExpect(jsonPath("$.lastMessage.chatId").value(chat.getId()))
            .andExpect(jsonPath("$.lastMessage.message").value(lastChatMessage.getMessage()))
            .andExpect(jsonPath("$.lastMessage.mediaItem.id").value(lastChatMessage.getMediaItem().getId()))
            .andExpect(jsonPath("$.lastMessage.senderFirstName").value(lastChatMessage.getSender().getFirstName()))
            .andExpect(jsonPath("$.lastMessage.senderLastName").value(
                    shorten(lastChatMessage.getSender().getLastName())))
            .andExpect(jsonPath("$.chatParticipants", hasSize(2)))
            .andExpect(jsonPath("$.chatParticipants[0].id").value(sortedChatParticipants.get(0).getId()))
            .andExpect(jsonPath("$.chatParticipants[0].person.id").value(sortedChatParticipants.get(0).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[0].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[0].readTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].id").value(sortedChatParticipants.get(1).getId()))
            .andExpect(jsonPath("$.chatParticipants[1].person.id").value(sortedChatParticipants.get(1).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[1].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].readTime").isEmpty())
            .andExpect(jsonPath("$.unreadMessageCount").value(numberUnreadMessages));
    }

    @Test
    public void getChatWithNoMessages() throws Exception {

        String topic = "le topique";
        Person chatPerson1 = th.personRegularAnna;
        Person chatPerson2 = th.personRegularKarl;

        Chat chat = chatService.createChatWithSubjectsAndParticipants(topic, th.chat1SubjectPushCategory,
                Collections.singletonList(th.tenant1),
                Arrays.asList(chatPerson1, chatPerson2));

        int expectedNumberOfUnreadMessages = 0;

        List<ChatParticipant> sortedChatParticipants = getParticipants(chat);

        assertEquals(2, sortedChatParticipants.size());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
            .headers(authHeadersFor(chatPerson1)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id").value(chat.getId()))
            .andExpect(jsonPath("$.lastMessage").isEmpty())
            .andExpect(jsonPath("$.chatParticipants", hasSize(2)))
            .andExpect(jsonPath("$.chatParticipants[0].id").value(sortedChatParticipants.get(0).getId()))
            .andExpect(jsonPath("$.chatParticipants[0].person.id").value(sortedChatParticipants.get(0).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[0].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[0].readTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].id").value(sortedChatParticipants.get(1).getId()))
            .andExpect(jsonPath("$.chatParticipants[1].person.id").value(sortedChatParticipants.get(1).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[1].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].readTime").isEmpty())
            .andExpect(jsonPath("$.unreadMessageCount").value(expectedNumberOfUnreadMessages));
    }

    @Test
    public void getChatWithOnlyAutogeneratedMessages() throws Exception {

        String topic = "le topique";
        Person chatPerson1 = th.personRegularAnna;
        Person chatPerson2 = th.personRegularKarl;

        Chat chat = chatService.createChatWithSubjectsAndParticipants(topic, th.chat1SubjectPushCategory,
                Collections.singletonList(th.tenant1),
                Arrays.asList(chatPerson1, chatPerson2));

        ChatMessage chatMessage1 = chatService.addAutoGeneratedMessageToChat(chat, "message1", 12L, true).getRight();
        ChatMessage chatMessage2 = chatService.addAutoGeneratedMessageToChat(chat, "message2", 12L, true).getRight();

        List<ChatParticipant> sortedChatParticipants = getParticipants(chat);

        assertEquals(2, sortedChatParticipants.size());
        // If not equals, verify CommunicationTestHelper
        assertEquals(2, th.chat1MessageList.size());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
            .headers(authHeadersFor(chatPerson2)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id").value(chat.getId()))
            .andExpect(jsonPath("$.lastMessage").isEmpty())
            .andExpect(jsonPath("$.chatParticipants", hasSize(2)))
            .andExpect(jsonPath("$.chatParticipants[0].id").value(sortedChatParticipants.get(0).getId()))
            .andExpect(jsonPath("$.chatParticipants[0].person.id").value(sortedChatParticipants.get(0).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[0].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[0].readTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].id").value(sortedChatParticipants.get(1).getId()))
            .andExpect(jsonPath("$.chatParticipants[1].person.id").value(sortedChatParticipants.get(1).getPerson().getId()))
            .andExpect(jsonPath("$.chatParticipants[1].receivedTime").isEmpty())
            .andExpect(jsonPath("$.chatParticipants[1].readTime").isEmpty())
            .andExpect(jsonPath("$.unreadMessageCount").value(2));
    }

    @Test
    public void getChatUnauthorizedAndNotParticipant() throws Exception {

        assertOAuth2(get("/communication/chat/" + th.chat1.getId()));

        //user is not participant
        Person notParticipant = th.personPoolingStationOp;
        assertFalse(containsParticipant(th.chat1, notParticipant));

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId())
                .headers(authHeadersFor(notParticipant)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT));
    }

    @Test
    public void getChatInvalidParameters() throws Exception {

        //invalid id
        mockMvc.perform(get("/communication/chat/4711")
                .headers(authHeadersFor(th.chat1Participant2.getPerson())))
                .andExpect(isException(ClientExceptionType.CHAT_NOT_FOUND));
    }

    @Test
    public void getChatMessage() throws Exception {

        Chat chat = th.chat1;
        ChatParticipant chatParticipant = th.chat1Participant1;
        ChatMessage chatMessage = th.chat1Message2;
        assertTrue(chat.getParticipants().contains(chatParticipant));
        assertNull(chatMessage.getMediaItem());

        mockMvc.perform(get("/communication/chat/message/" + chatMessage.getId())
            .headers(authHeadersFor(chatParticipant.getPerson())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id").value(chatMessage.getId()))
            .andExpect(jsonPath("$.chatId").value(chat.getId()))
            .andExpect(jsonPath("$.message").value(chatMessage.getMessage()))
            .andExpect(jsonPath("$.lastMessage.mediaItem").doesNotExist())
            .andExpect(jsonPath("$.senderFirstName").value(chatMessage.getSender().getFirstName()))
            .andExpect(jsonPath("$.senderLastName").value(
                    shorten(chatMessage.getSender().getLastName())));

    }

    @Test
    public void getChatMessageWithMediaItem() throws Exception {

        Chat chat = th.chat2;
        ChatParticipant chatParticipant = th.chat2Participant1;
        ChatMessage chatMessage = th.chat2Message1;

        assertNotNull(chatMessage.getMediaItem());
        assertTrue(chat.getParticipants().contains(chatParticipant));

        mockMvc.perform(get("/communication/chat/message/" + chatMessage.getId())
            .headers(authHeadersFor(chatParticipant.getPerson())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$.id").value(chatMessage.getId()))
            .andExpect(jsonPath("$.chatId").value(chat.getId()))
            .andExpect(jsonPath("$.message").value(chatMessage.getMessage()))
            .andExpect(jsonPath("$.mediaItem.id").value(chatMessage.getMediaItem().getId()))
            .andExpect(jsonPath("$.senderFirstName").value(chatMessage.getSender().getFirstName()))
            .andExpect(jsonPath("$.senderLastName").value(
                    shorten(chatMessage.getSender().getLastName())));

    }

    @Test
    public void getChatMessageUnauthorizedAndNotParticipant() throws Exception {

        ChatMessage chatMessage = th.chat1Message2;

        assertOAuth2(get("/communication/chat/message/" + chatMessage.getId()));

        //user is not participant
        Person notParticipant = th.personPoolingStationOp;
        assertFalse(containsParticipant(th.chat1, notParticipant));

        mockMvc.perform(get("/communication/chat/message/" + chatMessage.getId())
                .headers(authHeadersFor(notParticipant)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT));
    }

    @Test
    public void getChatMessageInvalidParameters() throws Exception {

        //invalid id
        mockMvc.perform(get("/communication/chat/message/4711")
                .headers(authHeadersFor(th.chat1Participant2.getPerson())))
                .andExpect(isException(ClientExceptionType.CHAT_NOT_FOUND));
    }

    @Test
    public void getChatMessages() throws Exception {

        ChatMessage chatMessage = th.chat1Message2;
        long fromTime = chatMessage.getCreated() - 1;

        ClientChatMessage expectedClientChatMessage = toClientChatMessage(chatMessage);

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message")
                        .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                        .param("from", Long.toString(fromTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonEquals(Collections.singletonList(expectedClientChatMessage)));
    }

    @Test
    public void getChatMessagesAndSetReceived() throws Exception {

        final Chat chat = th.chat1;
        final Person person = th.chat1Participant1.getPerson();
        Optional<ChatParticipant> participant;

        participant = th.chatParticipantRepository.findByChatAndPerson(chat, person);
        assertTrue(participant.isPresent());
        assertNull(participant.get().getReceivedTime());

        final long fromTime = th.chat1Message2.getCreated() - 1;

        mockMvc.perform(get("/communication/chat/" + chat.getId() + "/message")
                .headers(authHeadersFor(person))
                .param("from", Long.toString(fromTime)))
                .andExpect(status().isOk());

        participant = th.chatParticipantRepository.findByChatAndPerson(chat, person);
        assertTrue(participant.isPresent());
        assertNotNull(participant.get().getReceivedTime());
        assertEquals(th.chat1Message2.getCreated(), (long) participant.get().getReceivedTime());

        // test that the received flag can only be set further forward

        final long timeBefore = th.chat1Message1.getCreated() - 1;

        mockMvc.perform(get("/communication/chat/" + chat.getId() + "/message")
                .headers(authHeadersFor(person))
                .param("from", Long.toString(timeBefore)))
                .andExpect(status().isOk());

        participant = th.chatParticipantRepository.findByChatAndPerson(chat, person);
        assertTrue(participant.isPresent());
        assertNotNull(participant.get().getReceivedTime());
        assertEquals(th.chat1Message2.getCreated(), (long) participant.get().getReceivedTime());

        // test that the received flag is not set to null

        final long timeAfter = th.chat1Message2.getCreated()+1;

        mockMvc.perform(get("/communication/chat/" + chat.getId() + "/message")
                .headers(authHeadersFor(person))
                .param("from", Long.toString(timeAfter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());

        participant = th.chatParticipantRepository.findByChatAndPerson(chat, person);
        assertTrue(participant.isPresent());
        assertNotNull(participant.get().getReceivedTime());
        assertEquals(th.chat1Message2.getCreated(), (long) participant.get().getReceivedTime());

    }

    @Test
    public void getChatMessagesInvalidParameters() throws Exception {

        //user is not authorized
        assertOAuth2(get("/communication/chat/" + th.chat1.getId() + "/message")
                .param("from", Long.toString(th.chat1.getCreated())));

        //user is not participant
        Person notParticipant = th.personPoolingStationOp;
        assertFalse(containsParticipant(th.chat1, notParticipant));

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message")
                .headers(authHeadersFor(notParticipant))
                .param("from", Long.toString(th.chat1.getCreated())))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT));

        //not found chat
        mockMvc.perform(get("/communication/chat/notEvenAGuid/message")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("from", Long.toString(th.chat1.getCreated())))
                .andExpect(isException(ClientExceptionType.CHAT_NOT_FOUND));

    }

    @Test
    public void getChatWithAllMessagesRead() throws Exception {

        String topic = "le topique";
        Person chatPerson1 = th.personRegularAnna;
        Person chatPerson2 = th.personRegularKarl;

        Chat chat = chatService.createChatWithSubjectsAndParticipants(topic, th.chat1SubjectPushCategory,
                Collections.singletonList(th.tenant1),
                Arrays.asList(chatPerson1, chatPerson2));

        ChatMessage chatMessage1 = chatService.addUserTextMessageToChat(chat, chatPerson1, "message1", 12L).getRight();
        ChatMessage chatMessage2 = chatService.addUserTextMessageToChat(chat, chatPerson2, "message2", 12L).getRight();

        final long readTime = chatMessage2.getCreated() + 1;
        chatService.setChatReadTime(chat, chatPerson1, readTime);

        final List<ChatParticipant> sortedChatParticipants = getParticipants(chat);

        assertEquals(2, sortedChatParticipants.size());

        mockMvc.perform(get("/communication/chat/" + chat.getId())
                .headers(authHeadersFor(chatPerson1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(chat.getId()))
                .andExpect(jsonPath("$.lastMessage.id").value(chatMessage2.getId()))
                .andExpect(jsonPath("$.lastMessage.chatId").value(chat.getId()))
                .andExpect(jsonPath("$.lastMessage.message").value(chatMessage2.getMessage()))
                .andExpect(jsonPath("$.lastMessage.mediaItem").doesNotExist())
                .andExpect(jsonPath("$.lastMessage.senderFirstName").value(chatMessage2.getSender().getFirstName()))
                .andExpect(jsonPath("$.lastMessage.senderLastName").value(
                        shorten(chatMessage2.getSender().getLastName())))
                .andExpect(jsonPath("$.chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$.chatParticipants[0].id").value(sortedChatParticipants.get(0).getId()))
                .andExpect(jsonPath("$.chatParticipants[0].person.id").value(sortedChatParticipants.get(0).getPerson().getId()))
                .andExpect(jsonPath("$.chatParticipants[0].receivedTime").value(sortedChatParticipants.get(0).getReceivedTime()))
                .andExpect(jsonPath("$.chatParticipants[0].readTime").value(
                        Objects.equals(sortedChatParticipants.get(0).getPerson(), chatPerson1) ? readTime : null
                ))
                .andExpect(jsonPath("$.chatParticipants[1].id").value(sortedChatParticipants.get(1).getId()))
                .andExpect(jsonPath("$.chatParticipants[1].person.id").value(sortedChatParticipants.get(1).getPerson().getId()))
                .andExpect(jsonPath("$.chatParticipants[1].receivedTime").value(sortedChatParticipants.get(1).getReceivedTime()))
                .andExpect(jsonPath("$.chatParticipants[1].readTime").value(
                        Objects.equals(sortedChatParticipants.get(1).getPerson(), chatPerson1) ? readTime : null
                ))
                .andExpect(jsonPath("$.unreadMessageCount").value(0));
    }

    @Test
    public void getChatMessagesPaged() throws Exception {

        List<ClientChatMessage> expectedClientChatMessages = Arrays.asList(
                toClientChatMessage(th.chat1Message2),
                toClientChatMessage(th.chat1Message1)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(0, 10), expectedClientChatMessages.size());

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedWithoutPagination() throws Exception {

        List<ClientChatMessage> expectedClientChatMessages = Arrays.asList(
                toClientChatMessage(th.chat1Message2),
                toClientChatMessage(th.chat1Message1)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(0, Integer.MAX_VALUE), expectedClientChatMessages.size());

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("count", "0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedWithoutPaginationBadPageParameter() throws Exception {

        List<ClientChatMessage> expectedClientChatMessages = Arrays.asList(
                toClientChatMessage(th.chat1Message2),
                toClientChatMessage(th.chat1Message1)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(0, Integer.MAX_VALUE), expectedClientChatMessages.size());

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                // we expect page to be set to 0 automatically if count is = 0
                .param("page", "1")
                .param("count", "0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedWithFromTime() throws Exception {

        long fromTime = th.chat1Message2.getCreated() - 1;

        List<ClientChatMessage> expectedClientChatMessages = Collections.singletonList(
                toClientChatMessage(th.chat1Message2)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(0, 10), expectedClientChatMessages.size());

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("from", Long.toString(fromTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedWithCount() throws Exception {

        List<ClientChatMessage> expectedClientChatMessages = Collections.singletonList(
                toClientChatMessage(th.chat1Message2)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(0, 1), 2);

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("count", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedWithCountAndPage() throws Exception {

        List<ClientChatMessage> expectedClientChatMessages = Collections.singletonList(
                toClientChatMessage(th.chat1Message1)
        );

        Page<ClientChatMessage> expectedPage = new PageImpl<>(expectedClientChatMessages,
                simplePageRequest(1, 1), 2);

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("page", "1")
                .param("count", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonEquals(expectedPage));
    }

    @Test
    public void getChatMessagesPagedInvalidParameters() throws Exception {

        //user is not authorized
        assertOAuth2(get("/communication/chat/" + th.chat1.getId() + "/message/paged"));

        //user is not participant
        Person notParticipant = th.personPoolingStationOp;
        assertFalse(containsParticipant(th.chat1, notParticipant));

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(notParticipant)))
                .andExpect(isException(ClientExceptionType.PERSON_NOT_CHAT_PARTICIPANT));

        //not found chat
        mockMvc.perform(get("/communication/chat/notEvenAGuid/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson())))
                .andExpect(isException(ClientExceptionType.CHAT_NOT_FOUND));

        mockMvc.perform(get("/communication/chat/" + th.chat1.getId() + "/message/paged")
                .headers(authHeadersFor(th.chat1Participant1.getPerson()))
                .param("count", "-1"))
                .andExpect(status().isBadRequest());
    }

    private ClientChatMessage toClientChatMessage(ChatMessage chatMessage) {
        return ClientChatMessage.builder()
                .id(chatMessage.getId())
                .chatId(chatMessage.getChat().getId())
                .senderId(chatMessage.getSender().getId())
                .senderFirstName(chatMessage.getSender().getFirstName())
                .senderLastName(shorten(chatMessage.getSender().getLastName()))
                .message(chatMessage.getMessage())
                .mediaItem(th.toClientMediaItem(chatMessage.getMediaItem()))
                .sentTime(chatMessage.getSentTime())
                .created(chatMessage.getCreated())
                .autoGenerated(chatMessage.isAutoGenerated())
                .referencedEntityId(chatMessage.getReferencedEntityId())
                .referencedEntityName(chatMessage.getReferencedEntityName())
                .build();
    }

}
