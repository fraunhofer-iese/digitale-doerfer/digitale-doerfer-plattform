/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Alberto Lara
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.BaseServiceTest;

public class AchievementControllerTest  extends BaseServiceTest{

    @Autowired
    private MotivationTestHelper th;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createScoreEntities();
        th.createPushEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void getAvailableAndAchievedAchievements() throws Exception {

        th.createAchievementEntities();

        mockMvc.perform(
                get("/score/achievement/availableAndAchieved").headers(authHeadersFor(th.personPoolingStationOp)))
                .andExpect(status().isOk()).andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(MotivationTestHelper.demoAchievementName))
                .andExpect(jsonPath("$[0].descriptionText").value(MotivationTestHelper.demoAchievementDescription))
                .andExpect(jsonPath("$[0].category").value("Category1"))
                .andExpect(jsonPath("$[0].icon.id").value(th.achievementIcon.getId()))
                .andExpect(jsonPath("$[0].achievementLevels", hasSize(2)))
                .andExpect(jsonPath("$[0].achievementLevels[0].name").value(MotivationTestHelper.achievementLevel1Name))
                .andExpect(jsonPath("$[0].achievementLevels[0].descriptionText").value(MotivationTestHelper.achievementLevel1Description))
                .andExpect(jsonPath("$[0].achievementLevels[0].challengeDescriptionText").value(MotivationTestHelper.achievementLevel1ChallengeDescription))
                .andExpect(jsonPath("$[0].achievementLevels[0].icon.id").value(th.achievementIconAchieved.getId()))
                .andExpect(jsonPath("$[0].achievementLevels[0].achieved").value(true))
                .andExpect(jsonPath("$[0].achievementLevels[0].orderValue").value(1))
                .andExpect(jsonPath("$[0].achievementLevels[0].achievedTimestamp").value(th.achievedTimestamp))
                .andExpect(jsonPath("$[0].achievementLevels[1].name").value(MotivationTestHelper.achievementLevel2Name))
                .andExpect(jsonPath("$[0].achievementLevels[1].descriptionText").value(MotivationTestHelper.achievementLevel2Description))
                .andExpect(jsonPath("$[0].achievementLevels[1].challengeDescriptionText").value(MotivationTestHelper.achievementLevel2ChallengeDescription))
                .andExpect(jsonPath("$[0].achievementLevels[1].icon.id").value(th.achievementIconNotAchieved.getId()))
                .andExpect(jsonPath("$[0].achievementLevels[1].achieved").value(false))
                .andExpect(jsonPath("$[0].achievementLevels[1].orderValue").value(2))
                .andExpect(jsonPath("$[0].achievementLevels[1].achievedTimestamp").value(0));
    }

    @Test
    public void getAvailableAndAchievedAchievementsEmpty() throws Exception {

        mockMvc.perform(
                get("/score/achievement/availableAndAchieved").headers(authHeadersFor(th.personPoolingStationOp)))
                .andExpect(status().isOk()).andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

}
