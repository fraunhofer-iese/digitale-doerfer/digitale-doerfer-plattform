/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Steffen Hupp, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.contentintegration;

import de.fhg.iese.dd.platform.api.shared.BaseSharedTestHelper;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingAccessType;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.CrawlingConfig;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.model.KatwarnEventDetails;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.CrawlingConfigRepository;
import de.fhg.iese.dd.platform.datamanagement.contentintegration.repos.KatwarnEventDetailsRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.HappeningPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.NewsItemPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceOrganizationMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.FeatureConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.repos.FeatureConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

@Component
public class ContentIntegrationTestHelper extends BaseSharedTestHelper {

    @Autowired
    public NewsSourceRepository newsSourceRepository;
    @Autowired
    public PostRepository postRepository;
    @Autowired
    public CrawlingConfigRepository crawlingConfigRepository;
    @Autowired
    public FeatureConfigRepository featureConfigRepository;
    @Autowired
    public OrganizationRepository organizationRepository;
    @Autowired
    public NewsSourceOrganizationMappingRepository newsSourceOrganizationMappingRepository;
    @Autowired
    public KatwarnEventDetailsRepository katwarnEventDetailsRepository;

    public NewsSource newsSourceZukunftstheim;
    public NewsSource newsSourceDorfallerlei;

    public Organization organizationCrawlingConfigRSSNewsItemZukunftstheim;
    public Organization organizationCrawlingConfigRSSHappeningZukunftstheim;

    public CrawlingConfig crawlingConfigRSSNewsItemZukunftstheim;
    public CrawlingConfig crawlingConfigRSSHappeningZukunftstheim;
    public CrawlingConfig crawlingConfigAPINewsItemZukunftstheim;
    public CrawlingConfig crawlingConfigATOMNewsItemDorfallerlei;
    public CrawlingConfig crawlingConfigRSSWithFormatAndFixedValuesNewsItemZukunftsheim;
    public CrawlingConfig crawlingConfigRSSHappeningMixedRegexes;

    public KatwarnEventDetails katwarnEventDetailFire;
    public KatwarnEventDetails katwarnEventDetailBlizzard;
    public KatwarnEventDetails katwarnEventDetailLavaFlow;
    public KatwarnEventDetails katwarnEventDetailStorm;

    public void createOrganizations() {

        organizationCrawlingConfigRSSNewsItemZukunftstheim = organizationRepository.saveAndFlush(Organization.builder()
                .name("organizationCrawlingConfigRSSNewsItemZukunftstheim")
                .description("-")
                .locationDescription("-")
                .build());

        organizationCrawlingConfigRSSHappeningZukunftstheim = organizationRepository.saveAndFlush(Organization.builder()
                .name("organizationCrawlingConfigRSSHappeningZukunftstheim")
                .description("-")
                .locationDescription("-")
                .build());
    }

    private void createNewsSources() {

        newsSourceZukunftstheim = newsSourceRepository.save(NewsSource.builder()
                .siteUrl("https://zukunft.digitale-doerfer.de")
                .siteName("Zukunftsheim News")
                .siteLogo(createMediaItem("news-bild-zukunft"))
                .appVariant(app1Variant1)
                .build());

        newsSourceDorfallerlei = newsSourceRepository.save(NewsSource.builder()
                .siteUrl("https://allerlei.digitale-doerfer.de")
                .siteName("Dorfallerlei Aktuell")
                .siteLogo(createMediaItem("news-bild-allerlei"))
                .appVariant(app2Variant1)
                .build());
    }

    public void createNewsSourcesAndCrawlingConfigs() throws IOException {

        createOrganizations();

        createNewsSources();

        crawlingConfigRSSNewsItemZukunftstheim = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Config for RSS")
                .description("Additional crawling config for RSS feed")
                .newsSource(newsSourceZukunftstheim)
                .organization(organizationCrawlingConfigRSSNewsItemZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz, geoAreaKaiserslautern))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/rss.xml")
                .accessType(CrawlingAccessType.RSS)
                .externalPostType(PostType.NewsItem)
                .basicAuth("horst:A3ckel")
                .feedParametersJson(loadTextTestResource("contentintegration/crawlerConfig/feedParameterRSS.json"))
                .commentsDisallowed(true)
                .build());

        crawlingConfigAPINewsItemZukunftstheim = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Config for API")
                .newsSource(newsSourceZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz))
                .sourceUrl("https://zukunft.digitale-doerfer.de/api")
                .accessType(CrawlingAccessType.API)
                .externalPostType(PostType.NewsItem)
                .authenticationHeaderKey("authentication-header-key")
                .authenticationHeaderValue("authentication-header-value")
                .feedParametersJson("""
                        {
                                "text":[
                         {
                                    "tag":"description",
                                    "webFeedValueSources":[
                                        {
                                            "attribute":"",
                                            "type":"",
                                            "child":""
                                        }
                                    ]
                               }
                        ]
                            }""")
                .build());

        crawlingConfigATOMNewsItemDorfallerlei = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Config for ATOM")
                .newsSource(newsSourceDorfallerlei)
                .geoAreas(Set.of(geoAreaKaiserslautern))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/atom.xml")
                .accessType(CrawlingAccessType.ATOM)
                .externalPostType(PostType.NewsItem)
                .basicAuth("katze:hund")
                .feedParametersJson(loadTextTestResource("contentintegration/crawlerConfig/feedParameterATOM.json"))
                .build());

        crawlingConfigRSSHappeningZukunftstheim = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Happening Config for RSS")
                .newsSource(newsSourceZukunftstheim)
                .organization(organizationCrawlingConfigRSSHappeningZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/rss-happening.xml")
                .accessType(CrawlingAccessType.RSS)
                .externalPostType(PostType.Happening)
                .basicAuth("horst:A3ckel")
                .feedParametersJson(
                        loadTextTestResource("contentintegration/crawlerConfig/feedParameterRSSHappening.json"))
                .commentsDisallowed(false)
                .build());

        crawlingConfigRSSWithFormatAndFixedValuesNewsItemZukunftsheim = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Config for RSS Fixed Value")
                .description("Additional crawling config for RSS feed with fixed value")
                .newsSource(newsSourceZukunftstheim)
                .organization(organizationCrawlingConfigRSSNewsItemZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/rss.xml")
                .accessType(CrawlingAccessType.RSS)
                .externalPostType(PostType.NewsItem)
                .basicAuth("Karl:Napp")
                .feedParametersJson(
                        loadTextTestResource("contentintegration/crawlerConfig/feedParameterRSSFormatFixedValue.json"))
                .build());

        crawlingConfigRSSHappeningMixedRegexes = crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Happening Config for RSS with mixed regexes")
                .newsSource(newsSourceZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/rss-happening-mixed-regex.xml")
                .accessType(CrawlingAccessType.RSS)
                .externalPostType(PostType.Happening)
                .basicAuth("horst:A3ckel")
                .feedParametersJson(loadTextTestResource(
                        "contentintegration/crawlerConfig/feedParameterRSSHappeningMixedRegex.json"))
                .build());

        crawlingConfigRepository.save(CrawlingConfig.builder()
                .name("Config for RSS with composed text (title + description)")
                .newsSource(newsSourceZukunftstheim)
                .geoAreas(Set.of(geoAreaMainz))
                .sourceUrl("src/test/resources/contentintegration/crawlerFeeds/rss-composed-text.xml")
                .accessType(CrawlingAccessType.RSS)
                .externalPostType(PostType.NewsItem)
                .basicAuth("horst:A3ckel")
                .feedParametersJson(loadTextTestResource(
                        "contentintegration/crawlerConfig/feedParameterRSSComposedText.json"))
                .build());
    }

    public void updateCrawlingConfigs() {

        crawlingConfigRSSNewsItemZukunftstheim.setSourceUrl(
                "src/test/resources/contentintegration/crawlerFeeds/rss-updated.xml");
        crawlingConfigRepository.save(crawlingConfigRSSNewsItemZukunftstheim);
        crawlingConfigATOMNewsItemDorfallerlei.setSourceUrl(
                "src/test/resources/contentintegration/crawlerFeeds/atom-updated.xml");
        crawlingConfigRepository.save(crawlingConfigATOMNewsItemDorfallerlei);
    }

    public void createNewsItemTypeFeatures() {

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(NewsItemPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Nuews\", \"itemName\": \"NewsItem\", \"maxNumberCharsPerPost\": 360 }")
                .build()
                .withConstantId());
    }

    public void createHappeningTypeFeatures() {

        featureConfigRepository.saveAndFlush(FeatureConfig.builder()
                .enabled(true)
                .featureClass(HappeningPostTypeFeature.class.getName())
                .configValues(
                        "{ \"channelName\": \"Evunts\", \"itemName\": \"Happening\", \"maxNumberCharsPerPost\": 360 }")
                .build()
                .withConstantId());
    }

    public void createKatwarnEventDetails() {

        katwarnEventDetailFire = katwarnEventDetailsRepository.saveAndFlush(KatwarnEventDetails.builder()
                .eventCode("fire")
                .icon(createMediaItem("fire"))
                .build());
        katwarnEventDetailBlizzard = katwarnEventDetailsRepository.saveAndFlush(KatwarnEventDetails.builder()
                .eventCode("blizzard")
                .icon(createMediaItem("blizzard"))
                .build());
        katwarnEventDetailLavaFlow = katwarnEventDetailsRepository.saveAndFlush(KatwarnEventDetails.builder()
                .eventCode("lavaFlow")
                .icon(createMediaItem("lavaFlow"))
                .build());
        katwarnEventDetailStorm = katwarnEventDetailsRepository.saveAndFlush(KatwarnEventDetails.builder()
                .eventCode("storm")
                .icon(createMediaItem("storm"))
                .build());
    }

    @Override
    public void createPushEntities() {
        super.createPushEntities();
        createPushCategory(app1, DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        createPushCategory(app1, DorfFunkConstants.PUSH_CATEGORY_ID_POST_CHANGED);
        createPushCategory(app1, DorfFunkConstants.PUSH_CATEGORY_ID_POST_DELETED);
        createPushCategory(app1, DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);
        createPushCategory(app1, DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);
    }

}
