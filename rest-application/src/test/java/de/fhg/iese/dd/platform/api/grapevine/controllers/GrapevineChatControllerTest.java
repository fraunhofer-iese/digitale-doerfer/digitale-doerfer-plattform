/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChat;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatMessage;
import de.fhg.iese.dd.platform.api.communication.clientmodel.ClientChatParticipant;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.chat.ClientChatStartOnPostRequest;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatMessageRepository;
import de.fhg.iese.dd.platform.datamanagement.communication.repos.ChatRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GrapevineChatControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Override
    public void createEntities() {

        th.createGrapevineScenario();
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void getOwnChats() throws Exception {
        Person chatParticipant1 = th.personAnnikaSchneider;
        Person chatParticipant2 = th.personRegularKarl;

        ClientChat expectedChat = createChat(chatParticipant1, chatParticipant2);

        mockMvc.perform(get("/grapevine/chat/own")
            .headers(authHeadersFor(chatParticipant1)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(expectedChat.getId()))
            .andExpect(jsonPath("$[0].lastMessage.id").value(expectedChat.getLastMessage().getId()))
            .andExpect(jsonPath("$[0].lastMessage.chatId").value(expectedChat.getId()))
            .andExpect(jsonPath("$[0].lastMessage.message").value(expectedChat.getLastMessage().getMessage()))
            .andExpect(jsonPath("$[0].lastMessage.senderFirstName").value(expectedChat.getLastMessage().getSenderFirstName()))
            .andExpect(jsonPath("$[0].lastMessage.senderLastName").value(expectedChat.getLastMessage().getSenderLastName()))
            .andExpect(jsonPath("$[0].chatParticipants", hasSize(2)))
            .andExpect(jsonPath("$[0].chatParticipants[0].id").value(expectedChat.getChatParticipants().get(0).getId()))
            .andExpect(jsonPath("$[0].chatParticipants[1].id").value(expectedChat.getChatParticipants().get(1).getId()));

    }

    @Test
    public void getOwnChatsMultipleChats() throws Exception {
        Person chat1Participant1 = th.personAnnikaSchneider;
        Person chat1Participant2 = th.personRegularKarl;
        Person chat2Participant1 = chat1Participant1;
        Person chat2Participant2 = th.personRegularAnna;

        ClientChat expectedChat1 = createChat(chat1Participant1, chat1Participant2);

        //we just wait very shortly here, so that we can be sure that the secondly created chat is newer

        waitForEventProcessing();

        ClientChat expectedChat2 = createChat(chat2Participant1, chat2Participant2);

        ClientChatMessage chat1LastMessage = expectedChat1.getLastMessage();
        List<ClientChatParticipant> chat1Participants = expectedChat1.getChatParticipants();
        ClientChatMessage chat2LastMessage = expectedChat2.getLastMessage();
        List<ClientChatParticipant> chat2Participants = expectedChat2.getChatParticipants();
        mockMvc.perform(get("/grapevine/chat/own")
                .headers(authHeadersFor(chat1Participant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(expectedChat2.getId()))
                .andExpect(jsonPath("$[0].lastMessage.id").value(chat2LastMessage.getId()))
                .andExpect(jsonPath("$[0].lastMessage.chatId").value(expectedChat2.getId()))
                .andExpect(jsonPath("$[0].lastMessage.message").value(chat2LastMessage.getMessage()))
                .andExpect(jsonPath("$[0].lastMessage.senderFirstName").value(chat2LastMessage.getSenderFirstName()))
                .andExpect(jsonPath("$[0].lastMessage.senderLastName").value(chat2LastMessage.getSenderLastName()))
                .andExpect(jsonPath("$[0].chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$[0].chatParticipants[0].id").value(chat2Participants.get(0).getId()))
                .andExpect(jsonPath("$[0].chatParticipants[1].id").value(chat2Participants.get(1).getId()))
                .andExpect(jsonPath("$[1].id").value(expectedChat1.getId()))
                .andExpect(jsonPath("$[1].lastMessage.id").value(chat1LastMessage.getId()))
                .andExpect(jsonPath("$[1].lastMessage.chatId").value(expectedChat1.getId()))
                .andExpect(jsonPath("$[1].lastMessage.message").value(chat1LastMessage.getMessage()))
                .andExpect(jsonPath("$[1].lastMessage.senderFirstName").value(chat1LastMessage.getSenderFirstName()))
                .andExpect(jsonPath("$[1].lastMessage.senderLastName").value(chat1LastMessage.getSenderLastName()))
                .andExpect(jsonPath("$[1].chatParticipants", hasSize(2)))
                .andExpect(jsonPath("$[1].chatParticipants[0].id").value(chat1Participants.get(0).getId()))
                .andExpect(jsonPath("$[1].chatParticipants[1].id").value(chat1Participants.get(1).getId()));
    }

    @Test
    public void getOwnChats_ParallelWorldInhabitant() throws Exception {
        Person chat1Participant1 = th.personAnnikaSchneider;
        Person chat1Participant2 = th.personRegularKarl;
        Person chat2Participant1 = th.personAnnikaSchneider;
        Person chat2Participant2 = th.personRegularAnna;

        ClientChat expectedChat1 = createChat(chat1Participant1, chat1Participant2);
        ClientChat expectedChat2 = createChat(chat2Participant1, chat2Participant2);

        //this should hide the chats this person is part of
        Person parallelWorldInhabitant = th.personRegularKarl;
        parallelWorldInhabitant.getStatuses().addValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        th.personRepository.saveAndFlush(parallelWorldInhabitant);

        mockMvc.perform(get("/grapevine/chat/own")
                .headers(authHeadersFor(chat1Participant1)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedChat2.getId()));

        mockMvc.perform(get("/grapevine/chat/own")
                .headers(authHeadersFor(parallelWorldInhabitant)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(expectedChat1.getId()));
    }

    @Test
    public void getOwnChatsNoChats() throws Exception {

        Person person = th.personRegularAnna;
        assertThat(chatRepository.findByParticipantsContainingOrderByCreatedDesc(person)).isEmpty();

        mockMvc.perform(get("/grapevine/chat/own")
                        .headers(authHeadersFor(person)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getOwnChatsChatsWithNoMessages() throws Exception {
        Person chatParticipant1 = th.personNinaBauer;
        Person chatParticipant2 = th.personLaraSchaefer;

        ClientChat expectedChat = createChat(chatParticipant1, chatParticipant2);

        Chat chat = th.chatRepository.findById(expectedChat.getId()).get();
        chat.setLastMessageNotAutoGenerated(null);
        th.chatRepository.saveAndFlush(chat);

        chatMessageRepository.deleteAll();

        mockMvc.perform(get("/grapevine/chat/own")
            .headers(authHeadersFor(chatParticipant1)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].id").value(expectedChat.getId()))
            .andExpect(jsonPath("$[0].lastMessage").isEmpty())
            .andExpect(jsonPath("$[0].chatParticipants", hasSize(2)))
            .andExpect(jsonPath("$[0].chatParticipants[0].id").value(expectedChat.getChatParticipants().get(0).getId()))
            .andExpect(jsonPath("$[0].chatParticipants[1].id").value(expectedChat.getChatParticipants().get(1).getId()));
    }

    @Test
    public void getChatBlockedPerson() throws Exception {

        Person chatParticipant1 = th.personNinaBauer;
        Person chatParticipant2 = th.personLaraSchaefer;

        createChat(chatParticipant1, chatParticipant2);

        personBlockingService.blockPerson(th.appDorffunk, chatParticipant1, chatParticipant2);

        mockMvc.perform(get("/grapevine/chat/own")
                .headers(authHeadersFor(chatParticipant1, th.appVariant4AllTenants)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].filterStatus").value(ClientFilterStatus.BLOCKED.toString()));
    }

    @Test
    public void getOwnChatsUnauthorized() throws Exception {

        assertOAuth2(get("/grapevine/chat/own"));
    }

    private ClientChat createChat(Person chatParticipant1, Person chatParticipant2) throws Exception{

        Post post = th.gossip1withImages;
        post.setCreator(chatParticipant1);
        th.postRepository.saveAndFlush(post);

        ClientChatStartOnPostRequest chatStartOnPostRequest = ClientChatStartOnPostRequest.builder()
                .postId(post.getId())
                .message("I have a question about your post! 💛")
                .sentTime(System.currentTimeMillis())
                .build();

        MvcResult startChatResult = mockMvc.perform(post("/grapevine/chat/event/chatStartOnPostRequest")
                .headers(authHeadersFor(chatParticipant2, th.appVariant4AllTenants))
                .contentType(contentType)
                .content(json(chatStartOnPostRequest)))
                .andExpect(status().isOk())
                .andReturn();

        ClientChatStartOnPostConfirmation chatStartConfirmation =
                toObject(startChatResult, ClientChatStartOnPostConfirmation.class);

        return chatStartConfirmation.getChat();
    }

}
