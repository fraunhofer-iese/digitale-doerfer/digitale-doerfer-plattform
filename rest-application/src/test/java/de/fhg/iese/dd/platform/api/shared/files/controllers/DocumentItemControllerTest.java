/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.MultipartConfigElement;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.shared.SharedTestHelper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientTemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.DocumentConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.DocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.repos.TemporaryDocumentItemRepository;
import de.fhg.iese.dd.platform.datamanagement.test.mocks.TestFileStorage;

public class DocumentItemControllerTest extends BaseServiceTest {

    @Autowired
    private SharedTestHelper th;
    @Autowired
    private TestFileStorage fileStorage;
    @Autowired
    private TemporaryDocumentItemRepository temporaryDocumentItemRepository;
    @Autowired
    private DocumentItemRepository documentItemRepository;
    @Autowired
    protected TestRestTemplate testRestTemplate;
    @Autowired
    private DocumentConfig documentConfig;
    @Autowired
    private MultipartConfigElement multiPartConfiguration;

    @Override
    public void createEntities() {

        th.createTenantsAndGeoAreas();
        th.createAchievementRules();
        th.createPersons();
        th.createAppEntities();
    }

    @Override
    public void tearDown() throws Exception {

        th.deleteAllData();
    }

    @Test
    public void uploadTemporaryDocumentItem() throws Exception {

        Person uploader = th.personRegularAnna;
        AppVariant appVariant = th.app1Variant1;

        String fileName = "MyFile.pdf";
        String title = "Geheime Unterlagen 🥱";
        String description = "Nur für den Papierkorb";
        MockMultipartFile expectedFile =
                th.createTestMultipartFile("file", "document.pdf", fileName, "application/json");

        MvcResult result = mockMvc.perform(multipart("/document/temp")
                        .file(expectedFile)
                        .param("title", title)
                        .param("description", description)
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(status().isCreated())
                .andReturn();

        ClientTemporaryDocumentItem clientTempDocument =
                toObject(result.getResponse(), ClientTemporaryDocumentItem.class);

        assertEquals(1, temporaryDocumentItemRepository.count());

        TemporaryDocumentItem tempDocument = temporaryDocumentItemRepository.findByIdFull(clientTempDocument.getId());

        assertEquals(uploader.getId(), tempDocument.getOwner().getId());
        assertThat(tempDocument.getDocumentItem().getAppVariant()).isEqualTo(appVariant);
        assertThat(tempDocument.getDocumentItem().getMediaType()).isEqualTo("application/pdf");

        assertEquals(tempDocument.getDocumentItem().getId(), clientTempDocument.getDocumentItem().getId());
        assertEquals(title, clientTempDocument.getDocumentItem().getTitle());
        assertEquals(title, tempDocument.getDocumentItem().getTitle());
        assertEquals(description, clientTempDocument.getDocumentItem().getDescription());
        assertEquals(description, tempDocument.getDocumentItem().getDescription());
        assertEquals(tempDocument.getDocumentItem().getUrl(), clientTempDocument.getDocumentItem().getUrl());

        assertThat(fileStorage.fileExists(
                fileStorage.getInternalFileName(tempDocument.getDocumentItem().getUrl()))).isTrue();
    }

    @Test
    public void uploadTemporaryDocumentItem_NoTitle() throws Exception {

        Person uploader = th.personRegularAnna;
        AppVariant appVariant = th.app1Variant1;

        String fileName = "MyFile.pdf";
        MockMultipartFile expectedFile =
                th.createTestMultipartFile("file", "document.pdf", fileName, "application/json");

        MvcResult result = mockMvc.perform(multipart("/document/temp")
                        .file(expectedFile)
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(status().isCreated())
                .andReturn();

        ClientTemporaryDocumentItem clientTempDocument =
                toObject(result.getResponse(), ClientTemporaryDocumentItem.class);

        assertEquals(1, temporaryDocumentItemRepository.count());

        TemporaryDocumentItem tempDocument = temporaryDocumentItemRepository.findByIdFull(clientTempDocument.getId());

        assertEquals(uploader.getId(), tempDocument.getOwner().getId());
        assertThat(tempDocument.getDocumentItem().getAppVariant()).isEqualTo(appVariant);
        assertThat(tempDocument.getDocumentItem().getMediaType()).isEqualTo("application/pdf");

        assertEquals(tempDocument.getDocumentItem().getId(), clientTempDocument.getDocumentItem().getId());
        //title is generated from the filename
        assertEquals(fileName, clientTempDocument.getDocumentItem().getTitle());
        assertEquals(fileName, tempDocument.getDocumentItem().getTitle());
        assertThat(clientTempDocument.getDocumentItem().getDescription()).isNull();
        assertThat(tempDocument.getDocumentItem().getDescription()).isNull();
        assertEquals(tempDocument.getDocumentItem().getUrl(), clientTempDocument.getDocumentItem().getUrl());

        assertThat(fileStorage.fileExists(
                fileStorage.getInternalFileName(tempDocument.getDocumentItem().getUrl()))).isTrue();
    }
    
    @Test
    public void uploadTemporaryDocumentItem_TooBig() throws Exception {

        Person uploader = th.personRegularKarl;

        ClassPathResource expectedFileTooBig = th.createTestDocumentTooBigFile();

        assertThat(expectedFileTooBig.contentLength()).isGreaterThan(documentConfig.getMaxUploadFileSize().toBytes());

        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("file", expectedFileTooBig);

        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(parameters, authHeadersFor(uploader));
        try {
            ResponseEntity<String> result = testRestTemplate.postForEntity("/document/temp/", request, String.class);
            //when running this test with maven / surefire it will not throw an Exception, but return 400
            assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        } catch (Exception e) {
            //when running this test with JUnit it will throw this Exception
            assertThat(e).isInstanceOf(ResourceAccessException.class);
        }

        //this is the most important check
        assertEquals(0, temporaryDocumentItemRepository.count());
    }

    @Test
    public void uploadTemporaryDocumentItem_InvalidParameters() throws Exception {

        Person uploader = th.personRegularAnna;
        AppVariant appVariant = th.app1Variant1;

        //no document
        mockMvc.perform(multipart("/document/temp/")
                        .param("title", "Secret Plans")
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(isException(ClientExceptionType.UNSPECIFIED_BAD_REQUEST));

        //no valid document, images are not in the list of allowed extensions and media types
        MockMultipartFile expectedFileInvalid = th.createTestImageFile();

        mockMvc.perform(multipart("/document/temp/")
                        .file(expectedFileInvalid)
                        .param("title", "Meine Schatzkarte")
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(isException(ClientExceptionType.FILE_ITEM_UPLOAD_FAILED));

        //no file name in multipart file
        mockMvc.perform(multipart("/document/temp")
                        .file(th.createTestMultipartFile("file", "document.pdf", null, "application/json"))
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(isExceptionWithMessageContains(ClientExceptionType.FILE_ITEM_UPLOAD_FAILED, "name"));
    }

    @Test
    public void uploadTemporaryDocumentItem_LimitExceeded() throws Exception {

        Person uploader = th.personRegularKarl;
        AppVariant appVariant = th.app1Variant1;

        int maxNumberTempItems = documentConfig.getMaxNumberTemporaryItems();

        for (int i = 0; i < maxNumberTempItems - 1; i++) {
            th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());
        }

        assertEquals(maxNumberTempItems - 1, temporaryDocumentItemRepository.count());

        MockMultipartFile expectedFileMax =
                th.createTestMultipartFile("file", "example-qr-code-label.pdf", "Feil2.pdf", "application/json");

        MockMultipartFile expectedFileExceeding =
                th.createTestMultipartFile("file", "document.pdf", "Feil1.pdf", "application/json");

        mockMvc.perform(multipart("/document/temp/")
                        .file(expectedFileMax)
                        .param("title", "Formular Eins")
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(status().isCreated());

        mockMvc.perform(multipart("/document/temp/")
                        .file(expectedFileExceeding)
                        .param("title", "Zu viele Formulare")
                        .headers(authHeadersFor(uploader, appVariant)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_LIMIT_EXCEEDED));
    }

    @Test
    public void uploadTemporaryDocumentItem_Unauthorized() throws Exception {

        assertOAuth2AppVariantRequired(multipart("/document/temp/")
                        .file(th.createTestDocumentMultipartFile())
                        .param("title", "Kündigung"),
                th.app1Variant1, th.personRegularKarl);
    }

    @Test
    public void deleteTemporaryDocumentItem() throws Exception {

        Person uploader = th.personRegularKarl;

        mockMvc.perform(multipart("/document/temp/")
                        .file(th.createTestDocumentMultipartFile())
                        .param("title", "Formular Eins")
                        .headers(authHeadersFor(uploader, th.app1Variant1)))
                .andExpect(status().isCreated());

        TemporaryDocumentItem tempDocument = temporaryDocumentItemRepository.findAll().get(0);

        mockMvc.perform(delete("/document/temp/{tempId}", tempDocument.getId())
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isOk());

        assertThat(temporaryDocumentItemRepository.count()).isZero();
        assertThat(documentItemRepository.existsById(tempDocument.getDocumentItem().getId())).isFalse();

        //we need to wait a bit, since the deletion of the files is asynchronous
        waitForEventProcessing();

        String internalFileName = fileStorage.getInternalFileName(tempDocument.getDocumentItem().getUrl());
        assertThat(fileStorage.fileExists(internalFileName)).isFalse();
    }

    @Test
    public void deleteTemporaryDocumentItem_InvalidParameters() throws Exception {

        Person uploader = th.personRegularAnna;

        th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //invalid id
        mockMvc.perform(delete("/document/temp/4711")
                        .headers(authHeadersFor(uploader, th.app1Variant1)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void deleteTemporaryDocumentItem_Unauthorized() throws Exception {

        Person uploader = th.personRegularAnna;
        Person intruder = th.personRegularKarl;

        assertThat(uploader).isNotEqualTo(intruder);

        TemporaryDocumentItem expectedItem =
                th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //no authentication
        assertOAuth2(delete("/document/temp/{tempId}", expectedItem.getId()));

        //wrong person for valid id
        mockMvc.perform(delete("/document/temp/{tempId}", expectedItem.getId())
                        .headers(authHeadersFor(intruder)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getTemporaryDocumentItem() throws Exception {

        Person uploader = th.personRegularKarl;

        TemporaryDocumentItem expectedItem =
                th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(get("/document/temp/{tempId}", expectedItem.getId())
                        .headers(authHeadersFor(uploader, th.app1Variant2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(expectedItem.getId()))
                .andExpect(jsonPath("$.expirationTime").value(expectedItem.getExpirationTime()))
                .andExpect(jsonPath("$.documentItem.id").value(expectedItem.getDocumentItem().getId()))
                .andExpect(jsonPath("$.documentItem.title").value(expectedItem.getDocumentItem().getTitle()))
                .andExpect(
                        jsonPath("$.documentItem.description").value(expectedItem.getDocumentItem().getDescription()))
                .andExpect(jsonPath("$.documentItem.mediaType").value(expectedItem.getDocumentItem().getMediaType()))
                .andExpect(jsonPath("$.documentItem.url").value(expectedItem.getDocumentItem().getUrl()));
    }

    @Test
    public void getTemporaryDocumentItem_InvalidParameters() throws Exception {

        Person uploader = th.personRegularKarl;

        th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        mockMvc.perform(get("/document/temp/4711")
                        .headers(authHeadersFor(uploader)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getTemporaryDocumentItem_Unauthorized() throws Exception {

        Person uploader = th.personRegularAnna;
        Person intruder = th.personRegularKarl;

        assertThat(uploader).isNotEqualTo(intruder);

        TemporaryDocumentItem expectedItem =
                th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), th.nextTimeStamp());

        //no authentication
        assertOAuth2(get("/document/temp/{tempId}", expectedItem.getId()));

        //wrong person for valid id
        mockMvc.perform(get("/document/temp/{tempId}", expectedItem.getId())
                        .headers(authHeadersFor(intruder)))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void getAllTemporaryDocumentItems() throws Exception {

        Person uploader = th.personRegularKarl;

        TemporaryDocumentItem item4 = th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), 1);
        TemporaryDocumentItem item3 = th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), 2);
        TemporaryDocumentItem item2 = th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), 3);
        TemporaryDocumentItem item1 = th.createTemporaryDocumentItem(uploader, th.nextTimeStamp(), 4);
        //should not appear
        th.createTemporaryDocumentItem(th.personRegularAnna, th.nextTimeStamp(), 5);

        mockMvc.perform(get("/document/temp/")
                        .headers(authHeadersFor(uploader)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[0].id").value(item1.getId()))
                .andExpect(jsonPath("$[0].expirationTime").value(item1.getExpirationTime()))
                .andExpect(jsonPath("$[0].documentItem.id").value(item1.getDocumentItem().getId()))
                .andExpect(jsonPath("$[0].documentItem.title").value(item1.getDocumentItem().getTitle()))
                .andExpect(jsonPath("$[0].documentItem.description").value(item1.getDocumentItem().getDescription()))
                .andExpect(jsonPath("$[0].documentItem.mediaType").value(item1.getDocumentItem().getMediaType()))
                .andExpect(jsonPath("$[0].documentItem.url").value(item1.getDocumentItem().getUrl()))

                .andExpect(jsonPath("$[1].id").value(item2.getId()))
                .andExpect(jsonPath("$[1].expirationTime").value(item2.getExpirationTime()))
                .andExpect(jsonPath("$[1].documentItem.id").value(item2.getDocumentItem().getId()))
                .andExpect(jsonPath("$[1].documentItem.title").value(item2.getDocumentItem().getTitle()))
                .andExpect(jsonPath("$[1].documentItem.description").value(item2.getDocumentItem().getDescription()))
                .andExpect(jsonPath("$[1].documentItem.mediaType").value(item2.getDocumentItem().getMediaType()))
                .andExpect(jsonPath("$[1].documentItem.url").value(item2.getDocumentItem().getUrl()))

                .andExpect(jsonPath("$[2].id").value(item3.getId()))
                .andExpect(jsonPath("$[2].expirationTime").value(item3.getExpirationTime()))
                .andExpect(jsonPath("$[2].documentItem.id").value(item3.getDocumentItem().getId()))
                .andExpect(jsonPath("$[2].documentItem.title").value(item3.getDocumentItem().getTitle()))
                .andExpect(jsonPath("$[2].documentItem.description").value(item3.getDocumentItem().getDescription()))
                .andExpect(jsonPath("$[2].documentItem.mediaType").value(item3.getDocumentItem().getMediaType()))
                .andExpect(jsonPath("$[2].documentItem.url").value(item3.getDocumentItem().getUrl()))

                .andExpect(jsonPath("$[3].id").value(item4.getId()))
                .andExpect(jsonPath("$[3].expirationTime").value(item4.getExpirationTime()))
                .andExpect(jsonPath("$[3].documentItem.id").value(item4.getDocumentItem().getId()))
                .andExpect(jsonPath("$[3].documentItem.title").value(item4.getDocumentItem().getTitle()))
                .andExpect(jsonPath("$[3].documentItem.description").value(item4.getDocumentItem().getDescription()))
                .andExpect(jsonPath("$[3].documentItem.mediaType").value(item4.getDocumentItem().getMediaType()))
                .andExpect(jsonPath("$[3].documentItem.url").value(item4.getDocumentItem().getUrl()));
    }

    @Test
    public void getAllTemporaryDocumentItems_NoItems() throws Exception {

        mockMvc.perform(get("/document/temp/")
                        .headers(authHeadersFor(th.personRegularKarl)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void getAllTemporaryDocumentItems_Unauthorized() throws Exception {

        assertOAuth2(get("/document/temp"));
    }

}
