/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Benjamin Hassenfratz, Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.grapevine.controllers;

import de.fhg.iese.dd.platform.api.BaseServiceTest;
import de.fhg.iese.dd.platform.api.BaseTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.GrapevineTestHelper;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceCreateConfirmation;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceCreateRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientevent.ClientNewsSourceDeleteRequest;
import de.fhg.iese.dd.platform.api.grapevine.clientmodel.ClientNewsSource;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NewsSourceAdminUiEventControllerTest extends BaseServiceTest {

    @Autowired
    private GrapevineTestHelper th;
    @Autowired
    private NewsSourceRepository newsSourceRepository;

    private TemporaryMediaItem newsSourceLogo;

    @Override
    public void createEntities() throws Exception {
        th.createTenantsAndGeoAreas();
        th.createPersons();
        th.createAppEntities();
        th.createOrganizations();
        th.createNewsSources();

        newsSourceLogo = th.createTemporaryMediaItem(th.personGlobalConfigurationAdmin, th.nextTimeStamp());
    }

    @Override
    public void tearDown() throws Exception {
        th.deleteAllData();
    }

    @Test
    public void onNewsSourceCreateRequest() throws Exception {

        final ClientNewsSourceCreateRequest clientNewsSourceCreateRequest = ClientNewsSourceCreateRequest.builder()
                .appId(th.appDorfNews.getId())
                .siteUrl("https://www.buxtehude.de")
                .siteName("Buxtehude News")
                .temporaryMediaItemId(newsSourceLogo.getId())
                .changeNotes("New news source for Buxtehude")
                .build();

        assertTrue(th.temporaryMediaItemRepository.existsById(newsSourceLogo.getId()));

        final ClientNewsSource clientNewsSource =
                toObject(mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                                .contentType(contentType)
                                .content(json(clientNewsSourceCreateRequest)))
                        .andExpect(jsonPath("$.newsSource.siteUrl").value(clientNewsSourceCreateRequest.getSiteUrl()))
                        .andExpect(jsonPath("$.newsSource.siteName").value(clientNewsSourceCreateRequest.getSiteName()))
                        .andExpect(jsonPath("$.newsSource.apiManaged").value(true))
                        .andReturn(), ClientNewsSourceCreateConfirmation.class).getNewsSource();

        final String expectedAppVariantIdentifier = "de.fhg.iese.dd.dd.dorfnews.www.buxtehude.de";
        final AppVariant expectedAppVariant =
                th.appVariantRepository.findByIdFull(clientNewsSource.getAppVariantId()).get();
        assertEquals(clientNewsSourceCreateRequest.getAppId(), expectedAppVariant.getApp().getId());
        assertEquals(clientNewsSource.getAppVariantId(), expectedAppVariant.getId());
        assertEquals(expectedAppVariantIdentifier, expectedAppVariant.getAppVariantIdentifier());
        assertEquals(0, expectedAppVariant.getOauthClients().size());
        assertNull(expectedAppVariant.getExternalApiKey());
        assertNull(expectedAppVariant.getExternalBasicAuth());

        final NewsSource expectedNewsSource = th.newsSourceRepository.findById(clientNewsSource.getId()).get();
        assertEquals(clientNewsSource.getId(), expectedNewsSource.getId());
        assertEquals(clientNewsSource.getSiteUrl(), expectedNewsSource.getSiteUrl());
        assertEquals(clientNewsSource.getSiteName(), expectedNewsSource.getSiteName());
        assertEquals(newsSourceLogo.getMediaItem().getId(), expectedNewsSource.getSiteLogo().getId());
        assertTrue(expectedNewsSource.isApiManaged());
        assertFalse(th.temporaryMediaItemRepository.existsById(newsSourceLogo.getId()));
    }

    @Test
    public void onNewsSourceCreateRequest_WithAllEventAttributes() throws Exception {

        final ClientNewsSourceCreateRequest clientNewsSourceCreateRequest = ClientNewsSourceCreateRequest.builder()
                .appId(th.appDorfNews.getId())
                .callBackUrl("https://www.callback-buxtehude.de/")
                .oauthClientIdentifier(th.appVariant4AllTenantsOauthClient.getOauthClientIdentifier())
                .externalApiKey("external-api-key")
                .externalBasicAuth("user:password")
                .siteUrl("https://www.buxtehude.de")
                .siteName("Buxtehude News")
                .temporaryMediaItemId(newsSourceLogo.getId())
                .changeNotes("New news source for Buxtehude")
                .build();

        assertTrue(th.temporaryMediaItemRepository.existsById(newsSourceLogo.getId()));

        final ClientNewsSource clientNewsSource =
                toObject(mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                                .contentType(contentType)
                                .content(json(clientNewsSourceCreateRequest)))
                        .andExpect(jsonPath("$.newsSource.siteUrl").value(clientNewsSourceCreateRequest.getSiteUrl()))
                        .andExpect(jsonPath("$.newsSource.siteName").value(clientNewsSourceCreateRequest.getSiteName()))
                        .andExpect(jsonPath("$.newsSource.apiManaged").value(true))
                        .andReturn(), ClientNewsSourceCreateConfirmation.class).getNewsSource();

        final String expectedAppVariantIdentifier = "de.fhg.iese.dd.dd.dorfnews.www.buxtehude.de";
        final AppVariant expectedAppVariant =
                th.appVariantRepository.findByIdFull(clientNewsSource.getAppVariantId()).get();
        assertEquals(clientNewsSourceCreateRequest.getAppId(), expectedAppVariant.getApp().getId());
        assertEquals(clientNewsSource.getAppVariantId(), expectedAppVariant.getId());
        assertEquals(expectedAppVariantIdentifier, expectedAppVariant.getAppVariantIdentifier());
        assertEquals(1, expectedAppVariant.getOauthClients().size());
        assertEquals(clientNewsSourceCreateRequest.getCallBackUrl(), expectedAppVariant.getCallBackUrl());
        assertEquals(clientNewsSourceCreateRequest.getExternalApiKey(), expectedAppVariant.getExternalApiKey());
        assertEquals(clientNewsSourceCreateRequest.getExternalBasicAuth(), expectedAppVariant.getExternalBasicAuth());

        final NewsSource expectedNewsSource = th.newsSourceRepository.findById(clientNewsSource.getId()).get();
        assertEquals(clientNewsSource.getId(), expectedNewsSource.getId());
        assertEquals(clientNewsSource.getSiteUrl(), expectedNewsSource.getSiteUrl());
        assertEquals(clientNewsSource.getSiteName(), expectedNewsSource.getSiteName());
        assertEquals(newsSourceLogo.getMediaItem().getId(), expectedNewsSource.getSiteLogo().getId());
        assertTrue(expectedNewsSource.isApiManaged());
        assertFalse(th.temporaryMediaItemRepository.existsById(newsSourceLogo.getId()));
    }

    @Test
    public void onNewsSourceCreateRequest_NormalizedUrl() throws Exception {

        // with site url https://www.buxtehude.de/
        String siteUrl = "https://www.buxtehude.de";
        ClientNewsSourceCreateRequest clientNewsSourceCreateRequest = ClientNewsSourceCreateRequest.builder()
                .appId(th.appDorfNews.getId())
                .siteUrl(siteUrl + "/")
                .siteName("Buxtehude Aktuell")
                .temporaryMediaItemId(newsSourceLogo.getId())
                .changeNotes("New news source for Buxtehude Aktuell")
                .build();

        ClientNewsSource clientNewsSource =
                toObject(mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                                .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                                .contentType(contentType)
                                .content(json(clientNewsSourceCreateRequest)))
                        .andExpect(jsonPath("$.newsSource.siteUrl").value(siteUrl))
                        .andReturn(), ClientNewsSourceCreateConfirmation.class).getNewsSource();

        NewsSource expectedNewsSource = th.newsSourceRepository.findById(clientNewsSource.getId()).get();
        assertEquals(clientNewsSource.getSiteUrl(), expectedNewsSource.getSiteUrl());

        // with site url https://www.buxtehude-news.de/blub
        siteUrl = "https://www.buxtehude-news.de";
        clientNewsSourceCreateRequest = ClientNewsSourceCreateRequest.builder()
                .appId(th.appDorfNews.getId())
                .siteUrl(siteUrl + "/blub")
                .siteName("Buxtehude News")
                .temporaryMediaItemId(th.createTemporaryMediaItem(th.personGlobalConfigurationAdmin, th.nextTimeStamp()).getId())
                .changeNotes("New news source for Buxtehude News")
                .build();

        clientNewsSource = toObject(mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(clientNewsSourceCreateRequest)))
                .andExpect(jsonPath("$.newsSource.siteUrl").value(siteUrl))
                .andReturn(), ClientNewsSourceCreateConfirmation.class).getNewsSource();

        expectedNewsSource = th.newsSourceRepository.findById(clientNewsSource.getId()).get();
        assertEquals(clientNewsSource.getSiteUrl(), expectedNewsSource.getSiteUrl());
    }

    @Test
    public void onNewsSourceCreateRequest_InvalidAttributes() throws Exception {

        // appId null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // appId blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId("")
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // siteUrl null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // siteUrl blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // siteName null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // siteName blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // temporaryMediaItemId null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // temporaryMediaItemId blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId("")
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // changeNotes null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // changeNotes blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onNewsSourceCreateRequest_AppNotFound() throws Exception {

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(BaseTestHelper.INVALID_UUID)
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.APP_NOT_FOUND));
    }

    @Test
    public void onNewsSourceCreateRequest_OAuthClientNotFound() throws Exception {

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .oauthClientIdentifier(BaseTestHelper.INVALID_UUID)
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.OAUTH_CLIENT_NOT_FOUND));
    }

    @Test
    public void onNewsSourceCreateRequest_TemporaryMediaItemNotFound() throws Exception {

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(BaseTestHelper.INVALID_UUID)
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND));
    }

    @Test
    public void onNewsSourceCreateRequest_AppVariantAlreadyExists() throws Exception {

        th.appVariantRepository.saveAndFlush(AppVariant.builder()
                        .app(th.appDorfNews)
                        .appVariantIdentifier("de.fhg.iese.dd.dd.dorfnews.www.buxtehude.de")
                .build());

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorfNews.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.APP_VARIANT_ALREADY_EXISTS));
    }

    @Test
    public void onNewsSourceCreateRequest_NewsSourceAlreadyExists() throws Exception {

        th.newsSourceRepository.saveAndFlush(NewsSource.builder()
                .siteUrl("https://www.buxtehude.de")
                .siteName("Buxtehude News")
                .siteLogo(th.createMediaItem("news-bild-buxtehude"))
                .appVariant(th.appVariant4AllTenants)
                .build());

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                        .headers(authHeadersFor(th.personGlobalConfigurationAdmin))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceCreateRequest.builder()
                                .appId(th.appDorffunk.getId())
                                .siteUrl("https://www.buxtehude.de")
                                .siteName("Buxtehude News")
                                .temporaryMediaItemId(newsSourceLogo.getId())
                                .changeNotes("New news source for Buxtehude")
                                .build())))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_ALREADY_EXISTS));
    }

    @Test
    public void onNewsSourceCreateRequest_Unauthorized() throws Exception {

        final ClientNewsSourceCreateRequest newsSourceCreateRequest = ClientNewsSourceCreateRequest.builder()
                .appId(th.appDorfNews.getId())
                .siteUrl("https://www.buxtehude.de")
                .siteName("Buxtehude News")
                .temporaryMediaItemId(newsSourceLogo.getId())
                .changeNotes("New news source for Buxtehude")
                .build();

        mockMvc.perform(post("/adminui/newssource/event/newsSourceCreateRequest")
                .headers(authHeadersFor(th.personRegularAnna))
                        .contentType(contentType)
                        .content(json(newsSourceCreateRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/newssource/event/newsSourceCreateRequest")
                .contentType(contentType)
                .content(json(newsSourceCreateRequest)));
    }

    @Test
    public void onNewsSourceDeleteRequest() throws Exception {

        Person caller = th.personGlobalConfigurationAdmin;
        NewsSource newsSource = th.newsSourceDigitalbach;

        assertThat(newsSource.isDeleted()).isFalse();

        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId(newsSource.getId())
                                .changeNotes("Will ich los werden!")
                                .build())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("newsSourceId").value(newsSource.getId()))
                .andExpect(jsonPath("message").value(containsString(newsSource.getAppVariant().getId())));

        NewsSource deletedNewsSource = newsSourceRepository.findById(newsSource.getId()).get();
        assertThat(deletedNewsSource.isDeleted()).isTrue();
    }

    @Test
    public void onNewsSourceDeleteRequest_InvalidAttributes() throws Exception {

        Person caller = th.personGlobalConfigurationAdmin;

        // newsSourceId null
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId(null)
                                .changeNotes("Leer")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // newsSourceId blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId(" ")
                                .changeNotes("Blank")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));

        // change notes blank
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId(th.newsSourceDigitalbach.getId())
                                .changeNotes("")
                                .build())))
                .andExpect(isException(ClientExceptionType.EVENT_ATTRIBUTE_INVALID));
    }

    @Test
    public void onNewsSourceDeleteRequest_NewsSourceNotFound() throws Exception {

        Person caller = th.personGlobalConfigurationAdmin;

        // newsSource does not exist
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId("abc")
                                .changeNotes("def")
                                .build())))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));

        // newsSource is already deleted
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(ClientNewsSourceDeleteRequest.builder()
                                .newsSourceId(th.newsSourceDeleted.getId())
                                .changeNotes("Doppelt tot")
                                .build())))
                .andExpect(isException(ClientExceptionType.NEWS_SOURCE_NOT_FOUND));
    }

    @Test
    public void onNewsSourceDeleteRequest_Unauthorized() throws Exception {

        Person caller = th.personRegularAnna;
        NewsSource newsSource = th.newsSourceDigitalbach;

        assertThat(newsSource.isDeleted()).isFalse();

        ClientNewsSourceDeleteRequest newsSourceDeleteRequest = ClientNewsSourceDeleteRequest.builder()
                .newsSourceId(newsSource.getId())
                .changeNotes("Ich versuche es mal")
                .build();
        mockMvc.perform(post("/adminui/newssource/event/newsSourceDeleteRequest")
                        .headers(authHeadersFor(caller))
                        .contentType(contentType)
                        .content(json(newsSourceDeleteRequest)))
                .andExpect(isNotAuthorized());

        assertOAuth2(post("/adminui/newssource/event/newsSourceDeleteRequest")
                .contentType(contentType)
                .content(json(newsSourceDeleteRequest)));
    }

}
