/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.fhg.iese.dd.platform.api.framework.swagger.SwaggerEnvironmentFilter;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;

@Configuration
public class WebConfiguration {

    @Bean
    public FilterRegistrationBean<SwaggerEnvironmentFilter> swaggerEnvironmentFilter(IEnvironmentService environmentService) {
        final FilterRegistrationBean<SwaggerEnvironmentFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new SwaggerEnvironmentFilter(environmentService));
        registration.setDispatcherTypes(EnumSet.allOf(DispatcherType.class));
        registration.addUrlPatterns("/webjars/springfox-swagger-ui/swagger-ui.css");
        registration.addUrlPatterns("/swagger-ui.html");
        return registration;
    }

}
