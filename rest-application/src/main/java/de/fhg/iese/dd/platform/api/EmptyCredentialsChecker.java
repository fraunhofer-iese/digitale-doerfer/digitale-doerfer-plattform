/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DataInitConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DeveloperAccountConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ManualTestConfig;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 * Checks if some of the required credentials are not configured. This can easily happen when migrating to the
 * new secret mechanisms. Not using CommandLineRunner because they are
 * executed very late (after everything else is initiated). Using @PostConstruct instead
 */
@Component
@Log4j2
class EmptyCredentialsChecker {

    private static final Set<String> REMOTE_PROFILES = Set.of("dev", "stage", "prod");

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private Environment environment;

    @Autowired
    private DeveloperAccountConfig developerAccountConfig;

    @Autowired
    private ManualTestConfig manualTestConfig;

    @Autowired
    private DataInitConfig dataInitConfig;

    @AllArgsConstructor
    @Getter
    private class SecretProperty {
        String key;
        Supplier<String> valueSupplier;

        /**
         * Only use this constructor for properties that are not available in some configuration
         * beans (as e.g. {@link ApplicationConfig}).
         *
         * @param key
         */
        public SecretProperty(String key) {
            this.key = key;
            this.valueSupplier = () -> environment.getProperty(key);
        }
    }

    //IMPORTANT: Do NOT use method reference operator (::) here in order to prevent NPEs during init of this list!
    private final List<SecretProperty> SECRET_PROPERTIES = Arrays.asList(
            new SecretProperty("spring.datasource.username"),
            new SecretProperty("spring.datasource.password"),
            new SecretProperty("spring.mail.username"),
            new SecretProperty("spring.mail.password"),
            new SecretProperty("dd-platform.aws.access-key", () -> config.getAws().getAccessKey()),
            new SecretProperty("dd-platform.aws.secret-key", () -> config.getAws().getSecretKey()),
            new SecretProperty("dd-platform.oauth.api-key-for-person-linking", () -> config.getOauth().getApiKeyForPersonLinking()),
            new SecretProperty("dd-platform.oauth.api-key-for-registration-token",
                    () -> config.getOauth().getApiKeyForRegistrationToken()),
            new SecretProperty("api-key-for-pending-accounts:",
                    () -> config.getOauth().getApiKeyForPendingAccounts()),
            new SecretProperty("dd-platform.oauth-management.client-secret", () -> config.getOauthManagement().getClientSecret()),
            new SecretProperty("dd-platform.twilio-sms.auth-token", () -> config.getTwilioSms().getAuthToken()),
            new SecretProperty("dd-platform.google.api-key", () -> config.getGoogle().getApiKey()),
            new SecretProperty("dd-platform.d-station.api-key", () -> config.getDStation().getApiKey()),
            new SecretProperty("dd-platform.developeraccounts.developer-user.password",
                    () -> developerAccountConfig.getDeveloperUser().getPassword()),
            new SecretProperty("dd-platform.developeraccounts.admin-user.password",
                    () -> developerAccountConfig.getAdminUser().getPassword()),
            new SecretProperty("dd-platform.manual-test.api-key", () -> manualTestConfig.getApiKey()),
            new SecretProperty("dd-platform.default-super-admin-user.email", () -> config.getDefaultSuperAdminUser().getEmail()),
            new SecretProperty("dd-platform.default-super-admin-user.password",
                    () -> config.getDefaultSuperAdminUser().getPassword()),
            new SecretProperty("dd-platform.data-init.password", () -> dataInitConfig.getPassword())
            //due to mysterious reasons, config.getDataInitConfig() returns null
    );

    @PostConstruct
    @SuppressFBWarnings(value = "DCN_NULLPOINTER_EXCEPTION",
            justification = "The NPE check is reasonable, since the specific value suppliers might throw exceptions. " +
                    "There is no elegant way to do it different.")
    public void doTheCheck() {

        final List<String> missingConfiguration = new ArrayList<>();

        for (SecretProperty secretProperty : SECRET_PROPERTIES) {
            String secret;
            try {
                secret = secretProperty.getValueSupplier().get();
            } catch (NullPointerException e) {
                //happens if some parts of the config tree are missing
                secret = null;
            }
            if (StringUtils.isEmpty(secret) || "undefined".equalsIgnoreCase(secret)) {
                missingConfiguration.add(secretProperty.getKey());
            }
        }

        if (!missingConfiguration.isEmpty()) {
            if (Arrays.stream(environment.getActiveProfiles()).anyMatch(REMOTE_PROFILES::contains)) {
                throw new IllegalStateException("Missing required configuration value(s): " + missingConfiguration);
            } else {
                log.warn("MISSING IMPORTANT CONFIGURATION VALUE(S): {}", missingConfiguration);
            }
        }
    }

}
