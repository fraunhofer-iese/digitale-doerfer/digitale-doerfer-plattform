/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import com.fasterxml.classmate.TypeResolver;
import de.fhg.iese.dd.platform.api.framework.ClientExceptionEntity;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IEnvironmentService;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * Central Application starting the Rest API.
 *
 */
@EnableJpaRepositories(basePackages = "de.fhg.iese.dd.platform.datamanagement.**.repos",
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = JpaRepository.class))
@EntityScan(basePackages = "de.fhg.iese.dd.platform.datamanagement")
@ComponentScan({"de.fhg.iese.dd.platform.datamanagement", "de.fhg.iese.dd.platform.api","de.fhg.iese.dd.platform.business"})
@EnableSwagger2
@EnableScheduling
@SpringBootApplication
public class RestApplication extends SpringBootServletInitializer {

    @Autowired
    private TypeResolver typeResolver;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private IEnvironmentService environmentService;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return configureInternal(application);
    }

    private static SpringApplicationBuilder configureInternal(SpringApplicationBuilder application){
        return application
                .sources(RestApplication.class)
                .main(RestApplication.class)
                .properties("application.name:Digitale Dörfer Plattform REST API");
    }

    public static void main(String[] args) {
        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder();
        applicationBuilder = configureInternal(applicationBuilder);
        applicationBuilder.run(args);
    }

    /**
     * Configures the swagger documentation, available at /swagger-ui.html
     *
     * @return
     *
     * @see "http://springfox.github.io/springfox/docs/current/"
     */
    @Bean
    public Docket swaggerConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                                typeResolver.resolve(DeferredResult.class,
                                        typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)),
                        newRule(Object.class, Map.class),
                        newRule(char[].class, String.class))
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .globalResponseMessage(RequestMethod.GET, newArrayList(defaultErrorResponse()))
                .globalResponseMessage(RequestMethod.POST, newArrayList(defaultErrorResponse()))
                .globalResponseMessage(RequestMethod.PUT, newArrayList(defaultErrorResponse()))
                .globalResponseMessage(RequestMethod.DELETE, newArrayList(defaultErrorResponse()))
                .additionalModels(typeResolver.resolve(ClientExceptionEntity.class))
                .tags(new Tag("admin", "Administrative Services"))
                .securitySchemes(newArrayList(bearerTokenScheme(), oauthScheme()))
                .securityContexts(securityContext())
                .globalOperationParameters(newArrayList(new ParameterBuilder()
                        .name(BaseController.HEADER_NAME_APP_VARIANT_IDENTIFIER)
                        .description("Header for **optional** identification of calling app variant. " +
                                "The header is only required if the oauth client identifier in the access token is ambiguous. " +
                                "In this case only one additional identification is necessary, not both.")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .build()
                ))
                .globalOperationParameters(newArrayList(new ParameterBuilder()
                        .name(BaseController.HEADER_NAME_API_KEY)
                        .description("Header for **optional** identification of calling app variant. " +
                                "The header is only required if the oauth client identifier in the access token is ambiguous. " +
                                "In this case only one additional identification is necessary, not both.")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .build()));
    }

    //Security scheme constants

    final static String BEARER_TOKEN_SCHEMA_NAME = "Bearer Token";

    final static String OAUTH2_SCHEMA_NAME = "Auth0";

    private static final AuthorizationScope[] OAUTH2_SCOPES = new AuthorizationScope[]{
            new AuthorizationScope("openid", "the OpenID Connect standard claim")
            //further scopes can be added here
    };

    /**
     * BearerTokenScheme uses bearer token directly for authentication
     */
    private ApiKey bearerTokenScheme() {
        return new ApiKey(BEARER_TOKEN_SCHEMA_NAME, "Authorization", "header");
    }

    /**
     * OauthScheme implements implicit auth0 workflow
     */
    private OAuth oauthScheme() {
        String authorizationURL = applicationConfig.getOauth().getIssuer() + "authorize";

        final LoginEndpoint loginEndpoint = new LoginEndpoint(authorizationURL);
        final GrantType grantType = new ImplicitGrant(loginEndpoint, "access_token");

        return new OAuthBuilder()
                .name(OAUTH2_SCHEMA_NAME)
                .grantTypes(newArrayList(grantType))
                .scopes(newArrayList(OAUTH2_SCOPES))
                .build();
    }

    /**
     * SecurityReferences map the schemes defined above to specific authorization scopes
     */
    private List<SecurityReference> securityReferences() {
        //bearer token must have a (dummy) scope in order to work
        final AuthorizationScope[] globalScope = {new AuthorizationScope("global", "accessEverything")};
        return newArrayList(new SecurityReference(BEARER_TOKEN_SCHEMA_NAME, globalScope),
                new SecurityReference(OAUTH2_SCHEMA_NAME, OAUTH2_SCOPES));
    }

    /**
     * Security Context used to assign security references to all paths/APIs
     */
    private List<SecurityContext> securityContext() {
        return newArrayList(SecurityContext.builder()
                .securityReferences(securityReferences())
                .forPaths(PathSelectors.any())
                .build());
    }

    /**
     * Parameter configuration for auth0 authentication URL
     */
    @Bean
    SecurityConfiguration security() {
        Map<String, Object> audience = new HashMap<>();
        audience.put("audience", applicationConfig.getOauth().getAudience());

        return SecurityConfigurationBuilder.builder()
                .clientId(applicationConfig.getOauth().getSwaggerClientId())
                .appName("Swagger Client")
                .scopeSeparator(" ")
                .additionalQueryStringParams(audience)
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }

    private ResponseMessage defaultErrorResponse(){
        return new ResponseMessageBuilder()
            .code(400)
                .message("""
                        Possible error response codes are 4** if there was an error in the request or 5** if something went wrong internally.

                        All potential exceptions and their error codes are listed above.""")
            .responseModel(new ModelRef("ClientExceptionEntity"))
            .build();
    }

    private ApiInfo apiInfo(){
        return new ApiInfo(
                environmentService.getEnvironmentFullName() + " API of Digitale Dörfer Plattform",//title,
                String.format("This is the OpenAPI documentation of the Digitale Dörfer Plattform. " +
                                "Be aware that this is *just a static html page with javascript*, rendering the [OpenAPI  description](/v2/api-docs) provided by the platform.%n" +
                                "All operations the platform offers are listed and can be executed directly. " +
                                "Each operation comes with a short description, which helps to understand its behaviour. " +
                                "Nevertheless this description is not comprehensive and is **not** meant to replace communication with the developers. " +
                                "When in doubt about something please feel free to come to the *architecture / backend team* and ask for clarification." +
                                "%n%n" +
                                "#### Authorization %n" +
                                "Operations tagged with 🔒 *oauth2* require a valid access token in the `Authorization` header parameter. " +
                                "You can log in by hitting the green *Authorize* button. It redirects you to a login page." +
                                "%n%n" +
                                "#### Calling app variants %n" +
                                "An app variant is a concrete instantiation of an app, e.g. a web app at a specific URL or a mobile app that can be downloaded and installed. " +
                                "Since there are several configurations done on the level of app variants, it is important to know which one is using the API. " +
                                "Thus all endpoints try to identify the calling app variant. For some endpoints this identification is even required, that is mentioned in the documentation of this endpoint.%n" +
                                "The identification of the calling app variant follows this approach: %n" +
                                "1. Oauth client identifier in the access token (claim `azp` = authorized party) restricts the " +
                                "matching app variants to those that are configured to use this oauth client. " +
                                "If this is a one to one mapping, this app variant is chosen and no more identification is required.%n" +
                                "2. If there are multiple app variants using the same oauth client additional identification is required:%n" +
                                "  a. Header `appVariantIdentifier` in the request is used to match one of the app variants (using this oauth client) by identifier.%n" +
                                "*or*%n" +
                                "  b. Header `apiKey` in the request is used to match one of the app variants (using this oauth client) by api key.%n%n" +
                                "Option a and b are both valid, usually web or mobile clients use a, backends use b.%n" +
                                "In case the endpoint is a public endpoint and not called with an access token, step 1 can not be applied, so only 2 is relevant. " +
                                "In this case one of the headers needs to be set if the identification of the app variant is mandatory for this endpoint. %n" +
                                "If you want to use this swagger client to call 'in the name of' another app variant, " +
                                "you can do this by providing the according app variant identifier of this app variant in the header, as described above. " +
                                "This identifier has to be configured in the list of 'allowed override' in the  feature " +
                                "configuration of the swagger client at init-shared/feature/AppVariantOverrideFeature.json." +
                                "%n%n" +
                                "#### Environment '%s'%n" +
                                "%s" +
                                "#### Configuration%n" +
                                "**Version of the platform**: `%s` " +
                                "%n%n" +
                                "**Active Profiles**: `%s`" +
                                "%n%n" +
                                "**Environment Identifier**: `%s`" +
                                "%n%n",
                        environmentService.getEnvironmentFullName(),
                        environmentService.getEnvironmentDescription(),
                        applicationConfig.getVersionInfo().getPlatformVersion(),
                        environmentService.getActiveProfileInfo(false),
                        environmentService.getEnvironmentIdentifier()),
                applicationConfig.getVersionInfo().getPlatformVersion(),//version,
                "https://www.iese.fraunhofer.de",//termsOfServiceUrl,
                new Contact("Fraunhofer IESE - Digitale Dörfer Team", "https://www.digitale-doerfer.de",
                        "support@digitale-doerfer.de"),//contact,
                "Imprint",//license,
                "https://www.digitale-doerfer.de/impressum/",//licenseUrl
                Collections.emptyList()); //vendorExtension
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelsExpandDepth(2)
                .defaultModelExpandDepth(2)
                .defaultModelRendering(ModelRendering.MODEL)
                .displayRequestDuration(true)
                .docExpansion(DocExpansion.NONE)
                .filter(false)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA)
                .validatorUrl(null)
                .build();
    }

}
