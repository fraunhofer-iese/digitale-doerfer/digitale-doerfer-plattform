/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2022 Steffen Hupp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DeveloperAccountConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthConfig;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.log4j.Log4j2;

@Configuration
@EnableWebSecurity
@Log4j2
public class WebSecurityConfig {

    public static final List<String> ADMINISTRATION_URL_PATTERNS =
            List.of("/administration", "/administration/**");

    private static final String SWAGGER_UI_ENTRY_URL = "/swagger-ui.html";
    private static final String SWAGGER_UI_OAUTH_CALLBACK = "/webjars/springfox-swagger-ui/oauth2-redirect.html";
    public static final List<String> SWAGGER_UI_URL_PATTERNS =
            List.of(SWAGGER_UI_ENTRY_URL, "/v2/api-docs", "/swagger-resources", "/swagger-resources/**", "/webjars",
                    "/webjars/**");

    @Autowired
    private DeveloperAccountConfig developerAccountConfig;

    @Autowired
    private OauthConfig oauthConfig;

    @Autowired
    private List<BaseController> allControllers;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        final JwtWebSecurityConfigurer configurer = getConfigurer();
        final ExceptionHandlingConfigurer<HttpSecurity> chainedConfigurer = configurer
                .configure(http)
                .exceptionHandling();//override setting in JwtSecurityConfigurer

        http.authorizeRequests()
                //allow CORS preflight requests
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                //This is required to enable health checks by elastic beanstalk
                .requestMatchers(EndpointRequest.to("health")).permitAll();
        configureEndpointSecurity(http);

        if (developerAccountConfig.isPublicAccess()) {
            //Swagger
            http.authorizeRequests().antMatchers(SWAGGER_UI_URL_PATTERNS.toArray(new String[0]))
                    .permitAll();

            // /manage/** endpoints (except "/manage/health")
            http.authorizeRequests().antMatchers("/manage/**")
                    .permitAll();
            http.authorizeRequests().antMatchers("/h2-console/**")
                    .permitAll();
        } else {
            //Swagger
            chainedConfigurer.authenticationEntryPoint(
                    (HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) -> {
                        if (SWAGGER_UI_ENTRY_URL.equals(request.getRequestURI())) {
                            response.addHeader("WWW-Authenticate",
                                    "Basic"); //otherwise, no basic auth login is triggered
                        }
                        //Unfortunately, authException.getMessage() does not provide any more detailed reason, and
                        //also authException.getCause() is null, so just returning "Unauthorized" is fine
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
                    });

            http.authorizeRequests().antMatchers(SWAGGER_UI_OAUTH_CALLBACK).permitAll();
            http.authorizeRequests().antMatchers(SWAGGER_UI_URL_PATTERNS.toArray(new String[0]))
                    .hasAnyRole("ADMIN", "DEVELOPER").and()
                    .httpBasic();

            // /manage/** endpoints (except "/manage/health")
            http.authorizeRequests().antMatchers("/manage/**")
                    .hasAnyRole("ADMIN").and()
                    .httpBasic();
        }
        //Note that mixing basic auth. and OAuth2 does not work very well together, since the .authenticated()-
        //configuration on REST resources is also fulfilled when they are accessed via basic auth, which is highly undesired.
        //However, it is checked in de.fhg.iese.dd.platform.api.framework.controllers.BaseController#getOAuthUserId
        //that the correct authorization method is used. Another solution would be to use a reverse proxy (nginx)
        //to restrict certain paths with basic auth and remove the config from the platform.

        http.headers().frameOptions().sameOrigin();

        //important! by default everything is authenticated, if no public pattern matches first
        http.authorizeRequests().anyRequest().authenticated();
        return http.build();
    }

    private void configureEndpointSecurity(HttpSecurity httpSecurity) throws Exception {
        //sort the controllers to have consistent logs
        allControllers.sort(Comparator.comparing(c -> c.getClass().getName()));
        Multimap<HttpMethod, String> publicEndpoints = ArrayListMultimap.create();
        Multimap<HttpMethod, String> authenticatedEndpoints = ArrayListMultimap.create();
        StringBuilder authInfoLog = new StringBuilder("Exposing these endpoints:");
        for (BaseController controller : allControllers) {
            //get the base request mapping of the controller
            RequestMapping controllerMapping =
                    AnnotatedElementUtils.findMergedAnnotation(controller.getClass(), RequestMapping.class);
            if (controllerMapping != null) {
                if (controllerMapping.path().length == 1) {
                    //the base path of the controller should start with / and not end with /
                    String controllerPath = StringUtils.prependIfMissing(
                            StringUtils.removeEnd(controllerMapping.path()[0], "/"),
                            "/");
                    Method[] methods = ReflectionUtils.getAllDeclaredMethods(controller.getClass());
                    for (Method method : methods) {
                        //check if we have an actual endpoint
                        //synthesizes annotations that use the @AliasFor annotation (see @GetMapping, @PostMapping, ...)
                        RequestMapping requestMapping =
                                AnnotatedElementUtils.findMergedAnnotation(method, RequestMapping.class);
                        if (requestMapping != null) {
                            //if no method is defined, this mapping is used for all http methods
                            RequestMethod[] requestMethods = requestMapping.method().length != 0 ?
                                    requestMapping.method() :
                                    RequestMethod.values();
                            //we have an actual method that defines an endpoint
                            for (RequestMethod requestMethod : requestMethods) {
                                if (requestMapping.path().length <= 1) {
                                    ApiAuthentication apiAuthentication =
                                            AnnotatedElementUtils.findMergedAnnotation(method, ApiAuthentication.class);
                                    if (apiAuthentication != null) {
                                        //get the path of the endpoint
                                        String requestPattern;
                                        if (requestMapping.path().length == 1) {
                                            //the endpoint defines an own path, ensure that it starts with /
                                            String methodPath =
                                                    StringUtils.prependIfMissing(requestMapping.path()[0], "/");
                                            //the ant pattern uses * for path variables, that are captured with \{[^\{]*\}
                                            requestPattern = controllerPath +
                                                    RegExUtils.replaceAll(methodPath, "\\{[^\\{]*\\}", "*");
                                        } else {
                                            //the endpoint does not define an own path, the ant pattern is just the controller path
                                            requestPattern = controllerPath;
                                        }
                                        //unfortunately the enum in the annotation needs to be converted
                                        HttpMethod httpMethod = toHttpMethod(requestMethod);
                                        //the modifier is for better logging
                                        String modifier;
                                        switch (apiAuthentication.value()) {
                                            case OAUTH2:
                                                //authenticated
                                                authenticatedEndpoints.put(httpMethod, requestPattern);
                                                modifier = "-";
                                                break;
                                            case OAUTH2_OPTIONAL:
                                                //needs to be public, so that anonymous requests are possible
                                            case PUBLIC:
                                                //public
                                                publicEndpoints.put(httpMethod, requestPattern);
                                                modifier = "+";
                                                break;
                                            case API_KEY:
                                            case ONE_TIME_TOKEN:
                                            case OAUTH2_AND_API_KEY:
                                                //needs to be public, api key/one time token is checked in actual method implementation
                                                publicEndpoints.put(httpMethod, requestPattern);
                                                modifier = "~";
                                                break;
                                            default:
                                                throw new IllegalStateException(
                                                        "Unknown ApiAuthenticationType, missing implementation");
                                        }
                                        //log what we did by using the modifier
                                        authInfoLog.append(String.format("%n    %s %-11.10s%-8s%-60s\t[%s#%s]",
                                                modifier,
                                                apiAuthentication.value().toString(),
                                                httpMethod.toString(),
                                                requestPattern,
                                                controller.getClass().getName(),
                                                method.getName()
                                        ));
                                    } else {
                                        //we want all of our own methods to define @ApiAuthentication
                                        log.warn("Method {}#{} did not define @ApiAuthentication!",
                                                controller.getClass().getName(),
                                                method.getName());
                                    }
                                } else {
                                    //this is a valid spring configuration, currently we do not use it
                                    log.warn("Method {}#{} defines multiple request mappings!",
                                            controller.getClass().getName(), method.getName());
                                }
                            }
                        }
                    }
                } else {
                    //this is a valid spring configuration, currently we do not use it
                    log.warn("Controller {} defines multiple request mappings!", controller.getClass().getName());
                }
            } else {
                //we expect all of our controllers to have a kind of base url
                log.warn("Controller {} did not define @RequestMapping!", controller.getClass().getName());
            }
        }
        log.info(authInfoLog.toString());
        //we check first if there are public endpoints that would match an authenticated one
        AntPathMatcher matcher = new AntPathMatcher();
        for (HttpMethod httpMethod : HttpMethod.values()) {
            for (String authenticatedEndpoint : authenticatedEndpoints.get(httpMethod)) {
                //check all public endpoints if they match the current authenticated
                List<String> publicEndpointsMatchingAuthenticatedEndpoints = publicEndpoints.get(httpMethod).stream()
                        .filter(publicEndpoint -> matcher.match(publicEndpoint, authenticatedEndpoint))
                        .collect(Collectors.toList());
                if (!publicEndpointsMatchingAuthenticatedEndpoints.isEmpty()) {
                    //at least one public endpoint matches the current authenticated
                    if (log.isTraceEnabled()) {
                        log.trace("Public endpoints {} matches authenticated endpoint {}, " +
                                        "explicitly adding authenticated pattern first",
                                publicEndpointsMatchingAuthenticatedEndpoints, authenticatedEndpoint);
                    }
                    //we add the authenticated pattern FIRST, so that it matches before the public pattern
                    httpSecurity.authorizeRequests().antMatchers(httpMethod, authenticatedEndpoint).authenticated();
                }
            }
        }
        //now we can add all public pattern
        publicEndpoints.forEach((method, publicEndpoint) -> {
            try {
                httpSecurity.authorizeRequests().antMatchers(method, publicEndpoint).permitAll();
            } catch (Exception e) {
                //ugly workaround, since lambdas can not throw exceptions
                log.error("Failed to configure http security", e);
                throw new RuntimeException(e);
            }
        });
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
    private HttpMethod toHttpMethod(RequestMethod requestMethod){
        return Objects.requireNonNull(HttpMethod.resolve(requestMethod.name()));
    }

    private JwtWebSecurityConfigurer getConfigurer() {
        if (oauthConfig.getMethod() == OauthConfig.SignatureMethod.RS256) {
            return JwtWebSecurityConfigurer
                    .forRS256(oauthConfig.getAudience(), oauthConfig.getIssuers());
        }
        if (oauthConfig.getMethod() == OauthConfig.SignatureMethod.HS256) {
            if (StringUtils.isEmpty(oauthConfig.getSecretBase64())) {
                throw new IllegalArgumentException("oauth.secretBase64 must not be empty when using HS256");
            }
            return JwtWebSecurityConfigurer
                    .forHS256WithBase64Secret(oauthConfig.getAudience(), oauthConfig.getIssuers(),
                            oauthConfig.getSecretBase64());
        }
        throw new IllegalStateException("Unsupported oauth.method configured. Supported methods: "
                + Arrays.asList(OauthConfig.SignatureMethod.values()));
    }

    /**
     * configures users for basic authentication
     */
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        if (developerAccountConfig.isPublicAccess()) {
            log.warn("Swagger endpoints are open to the public. This should only be done in local. " +
                    "Configured via 'developeraccounts' in application.yml");
            return new InMemoryUserDetailsManager();
        } else {
            UserDetails developer = User.builder()
                    .username(developerAccountConfig.getDeveloperUser().getUsername())
                    .password("{noop}" + developerAccountConfig.getDeveloperUser().getPassword())
                    .roles("DEVELOPER")
                    .build();
            UserDetails admin = User.builder()
                    .username(developerAccountConfig.getAdminUser().getUsername())
                    .password("{noop}" + developerAccountConfig.getAdminUser().getPassword())
                    .roles("ADMIN")
                    .build();
            return new InMemoryUserDetailsManager(developer, admin);
        }
    }

}
