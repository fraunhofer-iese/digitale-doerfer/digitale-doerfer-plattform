/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2022 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.AccountType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.repos.TenantRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.config.DefaultSuperAdminUserConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import lombok.extern.log4j.Log4j2;

/**
 * Checks if at least one user with the role {@link SuperAdmin} is existent and creates a default one, if not.
 */
@Profile("!integration-test")
@Component
@Log4j2
@Order(0)
public class SuperAdminChecker implements CommandLineRunner {

    private static final String SUPER_ADMIN_TENANT_NAME = "Super Admin Tenant -DO NOT USE FOR ANYTHING-";
    public static final String SUPER_ADMIN_TENANT_ID = "195b67bf-d24c-4474-af89-987985bf35b8";

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IPersonService personService;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private DefaultSuperAdminUserConfig defaultSuperAdminUserConfig;
    @Autowired
    private IOauthManagementService oauthManagementService;

    @Override
    @Transactional
    public void run(String... args) {
        try {
            createDefaultSuperAdminUser();
        } catch (Exception e) {
            log.error("Failed to create default super admin: " + e, e);
        }
    }

    private void createDefaultSuperAdminUser() {
        //Simple check for default admin user
        if (personService.existsPersonByEmail(defaultSuperAdminUserConfig.getEmail())) {
            log.info("Found user with email {}. Assuming that Super admin rights are given.",
                    defaultSuperAdminUserConfig.getEmail());
            return;
        }
        //check if there is at least one user with admin role
        if (roleService.existsPersonWithRole(SuperAdmin.class)) {
            log.info("Found at least one super admin, skipping creation of default admin user.");
            return;
        }

        //no super admin exists, create default one, first in auth0
        String oauthId = oauthManagementService.getOauthIdForCustomUser(defaultSuperAdminUserConfig.getEmail());
        if (oauthId == null) {
            oauthId = oauthManagementService.createUser(defaultSuperAdminUserConfig.getEmail(),
                    defaultSuperAdminUserConfig.getPassword(), true);
        }

        //then in DB
        final Tenant tenant = tenantRepository.findById(SUPER_ADMIN_TENANT_ID).orElseGet(() -> {
            Tenant newTenant = new Tenant();
            newTenant.setName(SUPER_ADMIN_TENANT_NAME);
            newTenant.setId(SUPER_ADMIN_TENANT_ID);
            return tenantRepository.saveAndFlush(newTenant);
        });
        //update person's oauthId only if the person is already present in the database
        Person person = personRepository.findByEmail(defaultSuperAdminUserConfig.getEmail()).orElseGet(() ->
                Person.builder()
                        .accountType(AccountType.OAUTH)
                        .community(tenant)
                        .email(defaultSuperAdminUserConfig.getEmail())
                        .nickName("digdorfdev")
                        .firstName("Digitale")
                        .lastName("Dörfer")
                        .phoneNumber("0631-6800-0")
                        .build());
        person.setOauthId(oauthId);
        person = personService.store(person);
        roleService.assignRoleToPerson(person, SuperAdmin.class, (String) null);
        log.info("Created default super admin user with email {} and oauth id {}.",
                defaultSuperAdminUserConfig.getEmail(),
                oauthId);
    }

}
