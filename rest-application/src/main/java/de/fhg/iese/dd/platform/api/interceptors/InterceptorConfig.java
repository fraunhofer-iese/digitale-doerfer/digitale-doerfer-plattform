/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.interceptors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import de.fhg.iese.dd.platform.api.framework.WebSecurityConfig;
import de.fhg.iese.dd.platform.api.framework.controllers.CheckSuperAdminActionInterceptor;
import de.fhg.iese.dd.platform.api.framework.controllers.LogRequestsInterceptor;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //ensure that all admin endpoints can only be called by an admin
        registry.addInterceptor(new CheckSuperAdminActionInterceptor())
                .addPathPatterns(WebSecurityConfig.ADMINISTRATION_URL_PATTERNS.toArray(new String[0]));

        //do detailed logging of calls (except for swagger ui resources)
        registry.addInterceptor(new LogRequestsInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(WebSecurityConfig.SWAGGER_UI_URL_PATTERNS.toArray(new String[0]));
    }

}
