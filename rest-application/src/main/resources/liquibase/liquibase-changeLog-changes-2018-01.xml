<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  ~ Digitale Dörfer Plattform
  ~ Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel, Johannes Schneider, Adeline Silva Schäfer
  ~
  ~ SPDX-License-Identifier: Apache-2.0
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
    http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.4.xsd">

    <!-- all changesets here are only run on MySQL -->

    <!--
        Do not add anything here! These changes only apply for mysql since the datatypes for mssql are different.
        All upcomming changes are put into a new changeLog-changes-file.
     -->

    <changeSet id="2018-01-23_17-19" author="schneider" dbms="mysql">
        <comment>DD-1274 enhance query interface and other grapevine updates</comment>
        <renameColumn columnDataType="varchar(255)"
                      oldColumnName="type"
                      newColumnName="news_item_type"
                      remarks="type is too generic"
                      tableName="grapevine_post"/>
        <addColumn tableName="grapevine_post">
            <column name="last_comment" type="bigint"/>
        </addColumn>
    </changeSet>

    <changeSet id="2018-01-23_13-16" author="schneider" dbms="mysql">
        <comment>new column last_comment is null per default. Fill with correct value</comment>
        <sql>
            update grapevine_post p
            set p.last_comment = (select max(c.created) from grapevine_comment c where c.post_id = p.id)
            where p.last_comment is null;

            update grapevine_post p
            set p.last_comment = p.created
            where p.last_comment is null;
        </sql>
    </changeSet>

    <changeSet id="2018-01-23_17-30" author="weitzel" dbms="mysql">
        <comment>DD-1776 Enhance chat with references to entities</comment>
        <addColumn tableName="chat_message">
            <column name="referenced_entity_id" type="varchar(36)"/>
            <column name="referenced_entity_name" type="varchar(255)"/>
        </addColumn>
    </changeSet>

    <changeSet id="2018-01-30_11-20" author="schneider" dbms="mysql">
        <comment>
            DD-1779 Split DKB community into Eisenberg and Goellheim.

            Important: Rerun data-initializer for community, person, shop, app, push and waitingevent to fill the new
            column boundary_json and setup the other base data!
        </comment>
        <addColumn tableName="community">
            <column name="boundary_json" type="TEXT"/>
        </addColumn>
        <dropColumn columnName="ll_latitude" tableName="community"/>
        <dropColumn columnName="ll_longitude" tableName="community"/>
        <dropColumn columnName="lr_latitude" tableName="community"/>
        <dropColumn columnName="lr_longitude" tableName="community"/>
        <dropColumn columnName="ul_latitude" tableName="community"/>
        <dropColumn columnName="ul_longitude" tableName="community"/>
        <dropColumn columnName="ur_latitude" tableName="community"/>
        <dropColumn columnName="ur_longitude" tableName="community"/>
    </changeSet>

    <changeSet id="2018-01-31_16-10" author="silva" dbms="mysql">
        <comment>DD-1813 Add DorfNews name in news item</comment>
        <addColumn tableName="grapevine_post">
            <column name="site_name" type="varchar(255)"/>
        </addColumn>
    </changeSet>

    <changeSet id="2018-02-06__15-02" author="silva" dbms="mysql">
        <comment>DD-1848 added new suggestion class</comment>
        <addColumn tableName="grapevine_post">
            <column name="suggestion_status" type="varchar(50)"/>
            <column name="suggestion_type" type="varchar(50)"/>
        </addColumn>
    </changeSet>

    <changeSet id="2018-02-15__15-07" author="silva" dbms="mysql">
        <addColumn tableName="grapevine_post">
            <column name="comment_count" type="bigint"/>
        </addColumn>
    </changeSet>

    <changeSet id="2018-02-15_17-31" author="silva" dbms="mysql">
        <comment>new column comment_count is null per default. Fill with correct value</comment>
        <sql>
            update grapevine_post p
            set p.comment_count = (select count(c.post_id) from grapevine_comment c where p.id = c.post_id);
        </sql>
    </changeSet>

    <changeSet author="silva" id="2018-02-28_16-31" dbms="mysql">
        <comment>DD-1966 added new seeking class</comment>
        <addColumn tableName="grapevine_post">
            <column name="date" type="bigint"/>
            <column name="seeking_category" type="varchar(50)"/>
            <column name="seeking_status" type="varchar(50)"/>
        </addColumn>
    </changeSet>

    <changeSet author="silva" id="2018-03-05_08-53-20" dbms="mysql">
        <renameColumn tableName="grapevine_post"
                      oldColumnName="seeking_category"
                      columnDataType="varchar(50)"
                      newColumnName="trading_category"
                      remarks="added new superclass and change the name to be more generic"/>

        <renameColumn tableName="grapevine_post"
                      oldColumnName="seeking_status"
                      columnDataType="varchar(50)"
                      newColumnName="trading_status"
                      remarks="added new superclass and change the name to be more generic"/>
    </changeSet>

    <changeSet id="2018-03-03_17-00" author="weitzel" dbms="mysql">
        <comment>DD-1984 Temporary images for user uploads</comment>

        <createTable tableName="temporary_media_item">
            <column name="id" type="VARCHAR(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="owner_id" type="VARCHAR(36)">
                <constraints nullable="false"/>
            </column>
            <column name="media_item_id" type="VARCHAR(36)">
                <constraints nullable="true"/>
            </column>
            <column name="expiration_time" type="BIGINT"/>
        </createTable>

        <addPrimaryKey columnNames="id" constraintName="PRIMARY" tableName="temporary_media_item"/>
        <addForeignKeyConstraint baseColumnNames="media_item_id"
                                 baseTableName="temporary_media_item"
                                 constraintName="FKTemporaryMediaItem_MediaItem_MediaItem_gb7d6pf1by"
                                 deferrable="false"
                                 initiallyDeferred="false"
                                 referencedColumnNames="id"
                                 referencedTableName="media_item"/>
        <addForeignKeyConstraint baseColumnNames="owner_id"
                                 baseTableName="temporary_media_item"
                                 constraintName="FKTemporaryMediaItem_Owner_Person_oxqhmqwrn7"
                                 deferrable="false"
                                 initiallyDeferred="false"
                                 referencedColumnNames="id"
                                 referencedTableName="person"/>
    </changeSet>

    <changeSet id="2018-03-08_00-05" author="weitzel" dbms="mysql">
        <comment>DD-1999 Prevent duplicate push endpoints</comment>

        <addUniqueConstraint columnNames="endpoint_identifier"
                             constraintName="UKPushEndpoint_EndpointIdentifier_25ksa3wpuk"
                             tableName="push_endpoint"/>
    </changeSet>

    <changeSet id="2018-03-15_15-41" author="schneider" dbms="mysql">
        <comment>
            DD-1912 SQL server support: We now specify the column length for text columns instead of specifying
            the raw SQL type (in particular, "TEXT" was used a lot). This leads to slightly changed data types also in
            MySQL.
            This change set adapts the data types accordingly.
        </comment>
        <modifyDataType tableName="achievement" columnName="description" newDataType="${varchar.type}(1022)" />
        <modifyDataType tableName="achievement_level" columnName="challenge_description" newDataType="${varchar.type}(1022)" />
        <modifyDataType tableName="achievement_level" columnName="description" newDataType="${varchar.type}(1022)" />
        <modifyDataType tableName="chat_message" columnName="message" newDataType="${clob.type}" />
        <modifyDataType tableName="community" columnName="boundary_json" newDataType="${clob.type}"/>
        <modifyDataType tableName="conditional_rule" columnName="parameters" newDataType="${clob.type}" />
        <modifyDataType tableName="coupon" columnName="description" newDataType="${varchar.type}(1022)" />
        <modifyDataType tableName="delivery" columnName="content_notes" newDataType="${clob.type}" />
        <modifyDataType tableName="delivery" columnName="transport_notes" newDataType="${clob.type}" />
        <modifyDataType tableName="favor" columnName="text" newDataType="${clob.type}" />
        <modifyDataType tableName="favor_rating" columnName="text" newDataType="${clob.type}" />
        <modifyDataType tableName="grapevine_comment" columnName="text" newDataType="${clob.type}" />
        <modifyDataType tableName="grapevine_post" columnName="text" newDataType="${clob.type}" />
        <modifyDataType tableName="transport_confirmation" columnName="notes" newDataType="${clob.type}" />
        <modifyDataType tableName="purchase_order" columnName="content_notes" newDataType="${clob.type}" />
        <modifyDataType tableName="purchase_order" columnName="purchase_order_notes" newDataType="${clob.type}" />
        <modifyDataType tableName="purchase_order" columnName="transport_notes" newDataType="${clob.type}" />
        <modifyDataType tableName="push_category" columnName="description" newDataType="${varchar.type}(1022)" />
        <modifyDataType tableName="user_data_key_value_entry" columnName="value" newDataType="${clob.type}" />
        <modifyDataType tableName="waiting_deadline" columnName="event_attributes" newDataType="${clob.type}" />
    </changeSet>

</databaseChangeLog>