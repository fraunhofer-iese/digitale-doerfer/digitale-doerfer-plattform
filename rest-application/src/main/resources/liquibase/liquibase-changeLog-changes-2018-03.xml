<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!--
  ~ Digitale Dörfer Plattform
  ~ Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel, Johannes Schneider, Adeline Silva Schäfer, Steffen Hupp, Stefan Schweitzer
  ~
  ~ SPDX-License-Identifier: Apache-2.0
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
    http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.4.xsd">

    <!--
        How to append a change here:

        Id of the change set: timestamp when doing the change YYYY-MM-DD_HH-MM
        Author: your git / teamchat id
        Comment: id of the task, short explanation (more or less the first line of the git commit message)

        There can be several changes in one changeset, it should contain all changes that are required for the task

        Always think about existing data! Prefer rename over drop / add, try to migrate existing data, examples:
        - rename columns (push_endpoint.endpoint_arn to push_endpoint.endpoint_identifier)
        - convert data (timestamp to long)
        - move data (session.last_logged_in to person.last_logged_in)

        <changeSet id="2018-01-11_09-18" author="le mie" dbms="mysql,mssql">
            <comment>DD-4711 Added new flag for handling exchange of digitaler to bitcoin</comment>
            <addColumn tableName="bitcoin">
                <column name="exchanged" type="boolean"/>
            </addColumn>
        </changeSet>

        ! IMPORTANT !

        To ensure support for all dbms, we introduced placeholders for string datatypes.
        Please use these placeholders that are defined in liquibase-changeLog.xml
     -->

    <changeSet id="2018-03-17_16-00" author="weitzel" dbms="mysql,mssql">
        <comment>DD-1969 Images in chat messages</comment>

        <addColumn tableName="chat_message">
            <column name="media_item_id" type="${varchar.type}(36)"/>
        </addColumn>

        <addForeignKeyConstraint
                baseColumnNames="media_item_id"
                baseTableName="chat_message"
                constraintName="FKChatMessage_MediaItem_MediaItem_nv0oq2x6p0"
                deferrable="false"
                initiallyDeferred="false"
                referencedColumnNames="id"
                referencedTableName="media_item"/>
    </changeSet>

    <changeSet author="silva" id="2018-03-20_10-30-37" dbms="mysql,mssql">
        <comment>DD-2051 Multiple image resolutions. Added multiple paths to MediaItem table and removed the
            external located property. Now all media items are stored locally
        </comment>
        <addColumn tableName="media_item">
            <column name="size_version" type="int"/>
        </addColumn>
        <addColumn tableName="media_item">
            <column name="urls_json" type="${clob.type}"/>
        </addColumn>
        <dropColumn columnName="type" tableName="media_item"/>
    </changeSet>

    <changeSet id="2018-04-05_23-41" author="weitzel" dbms="mysql,mssql">
        <comment>new column size_version is null per default. Fill with correct value</comment>
        <sql>
            update media_item set size_version = -1;
        </sql>
    </changeSet>

    <changeSet id="2018-05-07_10-20" author="weitzel" dbms="mysql,mssql">

        <comment>DD-2258 Storage of legal texts in platform, user acceptance of legal texts stored in entities instead
            of user data
        </comment>

        <createTable tableName="shared_legal_text">
            <column name="id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="date_active" type="BIGINT">
                <constraints nullable="false"/>
            </column>
            <column name="date_inactive" type="BIGINT"/>
            <column name="legal_text_identifier" type="${varchar.type}(255)">
                <constraints nullable="false"/>
            </column>
            <column name="name" type="${varchar.type}(255)">
                <constraints nullable="false"/>
            </column>
            <column name="legal_text_type" type="${varchar.type}(255)"/>
            <column name="order_value" type="INT"/>
            <column name="required" type="BIT">
                <constraints nullable="false"/>
            </column>
            <column name="url_text" type="${varchar.type}(1022)">
                <constraints nullable="false"/>
            </column>
            <column name="app_variant_id" type="${varchar.type}(36)"/>
        </createTable>

        <createTable tableName="shared_legal_text_acceptance">
            <column name="id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="timestamp" type="BIGINT">
                <constraints nullable="false"/>
            </column>
            <column name="legal_text_id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
            <column name="person_id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
        </createTable>

        <addPrimaryKey columnNames="id"
                       constraintName="shared_legal_textPK"
                       tableName="shared_legal_text"/>
        <addPrimaryKey columnNames="id"
                       constraintName="shared_legal_text_acceptancePK"
                       tableName="shared_legal_text_acceptance"/>
        <addUniqueConstraint columnNames="legal_text_identifier"
                             constraintName="UKSharedLegalText_LegalTextIdentifier_5mbx8nrr5m"
                             tableName="shared_legal_text"/>
        <addForeignKeyConstraint baseColumnNames="legal_text_id"
                                 baseTableName="shared_legal_text_acceptance"
                                 constraintName="FKSharedLegalTextAcceptance_LegalText_SharedLegalText_kyvvbev265"
                                 deferrable="false"
                                 initiallyDeferred="false"
                                 referencedColumnNames="id"
                                 referencedTableName="shared_legal_text"/>
        <addForeignKeyConstraint baseColumnNames="person_id"
                                 baseTableName="shared_legal_text_acceptance"
                                 constraintName="FKSharedLegalTextAcceptance_Person_Person_5is5dassls"
                                 deferrable="false"
                                 initiallyDeferred="false"
                                 referencedColumnNames="id"
                                 referencedTableName="person"/>
        <addForeignKeyConstraint baseColumnNames="app_variant_id"
                                 baseTableName="shared_legal_text"
                                 constraintName="FKSharedLegalText_AppVariant_AppVariant_453xvruca7"
                                 deferrable="false"
                                 initiallyDeferred="false"
                                 referencedColumnNames="id"
                                 referencedTableName="app_variant"/>
    </changeSet>

    <changeSet author="schneider" id="2018-05-15_17-10-00" dbms="mysql,mssql">

        <comment>
            DD-2276 Implement basic structure for person deletion (+ some lombok refactoring)

            - tables address and person get flags to indicate entity deletion
            - during lombok refactoring, a column in pooling_station_box_configuration was renamed
        </comment>

        <addColumn tableName="person">
            <column name="status" type="${varchar.type}(20)" value="REGISTERED">
                <constraints nullable="false"/>
            </column>
        </addColumn>

        <addColumn tableName="address">
            <column name="deleted" type="bit(1)" valueBoolean="false">
                <constraints nullable="false"/>
            </column>
        </addColumn>

        <renameColumn tableName="pooling_station_box_configuration"
                      oldColumnName="is_inactive"
                      newColumnName="inactive"
                      columnDataType="bit(1)"/>
        <addNotNullConstraint tableName="pooling_station_box_configuration"
                              columnName="inactive"
                              columnDataType="bit(1)"/>
    </changeSet>

    <changeSet id="2018-05-17_15-00" author="weitzel" dbms="mysql,mssql">

        <comment>DD-2270 Removal of all favor data</comment>

        <sql dbms="mysql">SET AUTOCOMMIT=0;</sql>

        <dropTable tableName="favor_media_items" cascadeConstraints="true"/>
        <dropTable tableName="favor_inquiry_status_record" cascadeConstraints="true"/>
        <dropTable tableName="favor_inquiry" cascadeConstraints="true"/>
        <dropTable tableName="favor_rating" cascadeConstraints="true"/>
        <dropTable tableName="favor" cascadeConstraints="true"/>
        <dropTable tableName="car_unity_car_offer" cascadeConstraints="true"/>

    </changeSet>

    <changeSet id="2018-06-11_11-00" author="hupp" dbms="mysql,mssql">
        <comment>BAY-125 location processing</comment>

        <createTable tableName="mobilekiosk_raw_location_record">
            <column name="id" type="VARCHAR(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="accuracy" type="DOUBLE"/>
            <column name="location_lat" type="DOUBLE">
                <constraints nullable="false"/>
            </column>
            <column name="location_long" type="DOUBLE">
                <constraints nullable="false"/>
            </column>
            <column name="source_id" type="VARCHAR(255)"/>
            <column name="source_type" type="INT"/>
            <column name="timestamp" type="BIGINT"/>
            <column name="vehicle_id" type="VARCHAR(36)"/>
        </createTable>

        <addPrimaryKey columnNames="id" constraintName="mobilekiosk_raw_location_recordPK"
                       tableName="mobilekiosk_raw_location_record"/>

        <addForeignKeyConstraint baseColumnNames="vehicle_id" baseTableName="mobilekiosk_raw_location_record"
                                 constraintName="FKMobilekiRawLocaReco_Vehicle_MobileSellVehi_larv2luuwo"
                                 deferrable="false" initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="mobilekiosk_selling_vehicle"/>

        <createIndex indexName="IXDelivery_CustomReferenceNumber_53kqurx0nk" tableName="delivery">
            <column name="custom_reference_number"/>
        </createIndex>
    </changeSet>


    <changeSet id="2018-06-04_16-00" author="schweitzer" dbms="mysql,mssql">

        <comment>New entity class ChatParticipant (preliminary work for DD-1783 Read flag for chat messages)</comment>

        <createTable tableName="chat_participant">
            <column name="id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="chat_id" type="${varchar.type}(36)"/>
            <column name="person_id" type="${varchar.type}(36)"/>
        </createTable>
        <addPrimaryKey columnNames="id" constraintName="chat_participantPK" tableName="chat_participant"/>
        <addForeignKeyConstraint baseColumnNames="chat_id" baseTableName="chat_participant"
                                 constraintName="FKChatParticipant_Chat_Chat_ssgnv5c57d" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="chat"/>
        <addForeignKeyConstraint baseColumnNames="person_id" baseTableName="chat_participant"
                                 constraintName="FKChatParticipant_Person_Person_hbtq2wvl36" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="person"/>
        <dropForeignKeyConstraint baseTableName="chat_participants"
                                  constraintName="FKChatParticipants_Chat_Chat_44t5cpu2fe"/>
        <dropForeignKeyConstraint baseTableName="chat_participants"
                                  constraintName="FKChatParticipants_Participants_Person_jt6owvey0b"/>

        <sql>
            insert into chat_participant(id, created, chat_id, person_id)
            select UUID(), c.created, p.chat_id, p.participants_id
            from chat_participants as p
            inner join chat as c on c.id = p.chat_id
        </sql>

        <!-- for safety reasons the old chat_participants table is not yet dropped <dropTable tableName="chat_participants"/> -->
    </changeSet>

    <changeSet id="2018-06-07_15-00" author="schweitzer" dbms="mysql,mssql">

        <comment>DD-1783: Implement read flag for chat messages on platform</comment>

        <addNotNullConstraint columnDataType="varchar(36)" columnName="chat_id" tableName="chat_participant"/>
        <addNotNullConstraint columnDataType="varchar(36)" columnName="person_id" tableName="chat_participant"/>
        <addUniqueConstraint columnNames="chat_id, person_id" constraintName="UKChatParticipant_Chat_Perso_bquclam7fy"
                             tableName="chat_participant"/>

        <addColumn tableName="chat_participant">
            <column name="read_time" type="bigint"/>
        </addColumn>

    </changeSet>

    <changeSet id="2018-06-19_14-00" author="schweitzer" dbms="mysql,mssql">

        <comment>DD-2363: Implement storage of received flag for chat message</comment>

        <addColumn tableName="chat_participant">
            <column name="received_time" type="bigint"/>
        </addColumn>

    </changeSet>

    <changeSet author="silva" id="2018-06-21__11-14" dbms="mysql,mssql">
        <comment>
            DD-2280-Happenings
            Added new table to store the DorfNews sources
        </comment>
        <createTable tableName="grapevine_news_source">

            <column name="id" type="${varchar.type}(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>

            <column name="site_name" type="${varchar.type}(255)"/>
            <column name="site_url" type="${varchar.type}(255)"/>
            <column name="site_logo_id" type="${varchar.type}(36)"/>
        </createTable>
        <addPrimaryKey columnNames="id" constraintName="grapevine_news_sourcePK" tableName="grapevine_news_source"/>
        <addForeignKeyConstraint baseColumnNames="site_logo_id" baseTableName="grapevine_news_source"
                                 constraintName="FKGrapevineNewsSource_SiteLogo_MediaItem_a8e1bwd8se" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="media_item"/>
    </changeSet>


    <changeSet author="silva" id="2018-06-21__11-15" dbms="mysql,mssql">
        <comment>
            DD-2280-Happenings
            Adapted the grapevine_post to support happenings
        </comment>
        <addColumn tableName="grapevine_post">
            <column name="all_day_event" type="bit"/>
        </addColumn>
        <addColumn tableName="grapevine_post">
            <column name="end_time" type="bigint"/>
        </addColumn>
        <addColumn tableName="grapevine_post">
            <column name="news_source_id" type="${varchar.type}(36)"/>
        </addColumn>
        <addColumn tableName="grapevine_post">
            <column name="organizer" type="${varchar.type}(255)"/>
        </addColumn>
        <addColumn tableName="grapevine_post">
            <column name="start_time" type="bigint"/>
        </addColumn>
        <addColumn tableName="grapevine_post">
            <column name="venue" type="${varchar.type}(255)"/>
        </addColumn>

        <addForeignKeyConstraint baseColumnNames="news_source_id" baseTableName="grapevine_post"
                                 constraintName="FKGrapevinePost_NewsSource_GrapevineNewsSource_jfts10kftq"
                                 deferrable="false" initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="grapevine_news_source"/>
    </changeSet>

    <changeSet id="2018-06-25_11-30" author="hupp" dbms="mysql,mssql">
        <comment>BAY-176: Counter for rolling parcel codes</comment>
        <createTable tableName="named_counter">
            <column name="id" type="VARCHAR(36)">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="name" type="VARCHAR(255)">
                <constraints nullable="false"/>
            </column>
            <column name="value" type="BIGINT"/>
        </createTable>
        <addPrimaryKey columnNames="id" constraintName="named_counterPK" tableName="named_counter"/>
        <addUniqueConstraint columnNames="name" constraintName="UKNamedCounter_Name_qppuo7cjgs"
                             tableName="named_counter"/>
    </changeSet>

    <changeSet id="2018-07-11_16-06" author="silva" dbms="mysql,mssql">
        <comment>
            DD-2280 Removed a column from NewsItem that is not necessary anymore and rename another column to be more
            generic.
        </comment>
        <renameColumn tableName="grapevine_post"
                      oldColumnName="news_item_type"
                      newColumnName="external_post_type"
                      columnDataType="varchar(255)"/>
        <dropColumn columnName="site_name" tableName="grapevine_post"/>
    </changeSet>

    <changeSet id="2018-07-23_10-54" author="weitzel" dbms="mysql,mssql">
        <comment>
            Keeping schema in sync with code:
            - Removed dead table, new one is chat_participant
            - Removed accidentally added remarks
            - Not null constraint for enum column
        </comment>

        <dropTable tableName="chat_participants"/>

        <renameColumn tableName="grapevine_post" oldColumnName="trading_category" newColumnName="trading_category"
                      columnDataType="${varchar.type}(255)" remarks=""/>
        <renameColumn tableName="grapevine_post" oldColumnName="trading_status" newColumnName="trading_status"
                      columnDataType="${varchar.type}(255)" remarks=""/>

        <addNotNullConstraint columnDataType="${varchar.type}(255)" columnName="legal_text_type"
                              tableName="shared_legal_text"/>

    </changeSet>

    <changeSet id="2018-07-23_10-57" author="weitzel" dbms="mysql,mssql">
        <comment>
            Removed unused coupon feature and according tables
        </comment>

        <dropForeignKeyConstraint baseTableName="coupon_person_pairing"
                                  constraintName="FKCouponPersonPairing_Coupon_Coupon_nvrrkfjvjx"/>
        <dropForeignKeyConstraint baseTableName="coupon_person_pairing"
                                  constraintName="FKCouponPersonPairing_Person_Person_eoashgacpk"/>
        <dropForeignKeyConstraint baseTableName="coupon" constraintName="FKCoupon_Community_Community_ei5flssp32"/>
        <dropForeignKeyConstraint baseTableName="coupon" constraintName="FKCoupon_Picture_MediaItem_2cq830hp05"/>
        <dropForeignKeyConstraint baseTableName="coupon" constraintName="FKCoupon_Shop_Shop_7qkmykcubu"/>
        <dropForeignKeyConstraint baseTableName="coupon"
                                  constraintName="FKCoupon_UnlockCondition_ConditionalRule_rgu8a618sg"/>
        <dropTable tableName="conditional_rule"/>
        <dropTable tableName="coupon"/>
        <dropTable tableName="coupon_person_pairing"/>

    </changeSet>

    <changeSet id="2018-07-18_14-08" author="schweitzer" dbms="mysql,mssql">
        <comment>
            DD-2503: Implement new data model for tenants and geographic areas
        </comment>

        <!-- Entity GeoArea -->
        <createTable tableName="participants_geo_area">
            <column name="id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="boundary_json" type="${clob.type}"/>
            <column name="center_latitude" type="DOUBLE"/>
            <column name="center_longitude" type="DOUBLE"/>
            <column name="name" type="${varchar.type}(255)">
                <constraints nullable="false"/>
            </column>
            <column name="type" type="${varchar.type}(255)"/>
            <column name="logo_id" type="${id.type}"/>
            <column name="parent_area_id" type="${id.type}"/>
            <column name="tenant_id" type="${id.type}"/>
        </createTable>
        <addPrimaryKey columnNames="id" constraintName="participants_geo_areaPK" tableName="participants_geo_area"/>
        <addForeignKeyConstraint baseColumnNames="logo_id" baseTableName="participants_geo_area"
                                 constraintName="FKParticipantsGeoArea_Logo_MediaItem_lawap46i12" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="media_item"/>
        <addForeignKeyConstraint baseColumnNames="parent_area_id" baseTableName="participants_geo_area"
                                 constraintName="FKParticipantsGeoArea_ParentArea_ParticipantsGeoArea_55n2d4iree"
                                 deferrable="false" initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="participants_geo_area"/>
        <addForeignKeyConstraint baseColumnNames="tenant_id" baseTableName="participants_geo_area"
                                 constraintName="FKParticipantsGeoArea_Tenant_Community_eedit7f555" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="community"/>

        <!-- Entity PersonHomeAreaSelection -->
        <createTable tableName="participants_person_home_area_selection">
            <column name="id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="status" type="${enum.type}"/>
            <column name="home_area_id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
            <column name="person_id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <addPrimaryKey columnNames="id" constraintName="participants_person_home_area_selectionPK"
                       tableName="participants_person_home_area_selection"/>
        <addUniqueConstraint columnNames="person_id, home_area_id"
                             constraintName="UKParticipantsPersonHomeAreaSelection_HomeAre_Person_9y9vd1pv95"
                             tableName="participants_person_home_area_selection"/>
        <addForeignKeyConstraint baseColumnNames="home_area_id" baseTableName="participants_person_home_area_selection"
                                 constraintName="FKParticiPerHomAreSel_HomeArea_ParticipantsGeoArea_6qgfiaegme"
                                 deferrable="false" initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="participants_geo_area"/>
        <addForeignKeyConstraint baseColumnNames="person_id" baseTableName="participants_person_home_area_selection"
                                 constraintName="FKParticipantsPersonHomeAreaSelection_Person_Person_o404lbs7e0"
                                 deferrable="false" initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="person"/>

        <!-- Attribute homeArea of Person -->
        <addColumn tableName="person">
            <column name="home_area_id" type="${id.type}"/>
        </addColumn>
        <addForeignKeyConstraint baseColumnNames="home_area_id" baseTableName="person"
                                 constraintName="FKPerson_HomeArea_ParticipantsGeoArea_6yxe5yfs1a" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="participants_geo_area"/>

        <!-- Attribute rootArea of Community -->
        <addColumn tableName="community">
            <column name="root_area_id" type="${id.type}"/>
        </addColumn>
        <addForeignKeyConstraint baseColumnNames="root_area_id" baseTableName="community"
                                 constraintName="FKCommunity_RootArea_ParticipantsGeoArea_f4f6nl9xn5" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id"
                                 referencedTableName="participants_geo_area"/>

    </changeSet>

    <changeSet id="2018-07-27_11-00" author="schweitz" dbms="mysql,mssql">
        <comment>
            DD-2525: Enable selection of geographic areas as filter in app variant instead of communities
        </comment>

        <createTable tableName="shared_app_variant_usage_area_selection">
            <column name="id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
            <column name="created" type="BIGINT"/>
            <column name="status" type="${enum.type}"/>
            <column name="app_variant_usage_id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
            <column name="selected_area_id" type="${id.type}">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <addPrimaryKey columnNames="id"
                       constraintName="shared_app_variant_usage_area_selectionPK"
                       tableName="shared_app_variant_usage_area_selection"/>
        <addUniqueConstraint columnNames="app_variant_usage_id, selected_area_id"
                             constraintName="UKSharAppVarUsaAreSel_AppVariantUsag_Selected_area_lffdwecvjr"
                             tableName="shared_app_variant_usage_area_selection"/>
        <addForeignKeyConstraint baseColumnNames="app_variant_usage_id"
                                 baseTableName="shared_app_variant_usage_area_selection"
                                 constraintName="FKSharAppVarUsaAreSel_AppVariantUsage_AppVariantUsage_2qbhi91rmu"
                                 deferrable="false" initiallyDeferred="false"
                                 referencedColumnNames="id" referencedTableName="app_variant_usage"/>
        <addForeignKeyConstraint baseColumnNames="selected_area_id"
                                 baseTableName="shared_app_variant_usage_area_selection"
                                 constraintName="FKSharAppVarUsaAreSel_SelectedArea_ParticiGeoArea_fd273uyfsi"
                                 deferrable="false" initiallyDeferred="false"
                                 referencedColumnNames="id" referencedTableName="participants_geo_area"/>
    </changeSet>

    <changeSet id="2018-07-27_13-48" author="schweitzer" dbms="mysql,mssql">
        <comment>
            DD-2523: Implement relationship between posts and geographic areas
        </comment>

        <addColumn tableName="grapevine_post">
            <column name="geo_area_id" type="${id.type}"/>
        </addColumn>
        <addForeignKeyConstraint baseColumnNames="geo_area_id" baseTableName="grapevine_post"
                                 constraintName="FKGrapevinePost_GeoArea_ParticipantsGeoArea_j305uaphah"
                                 deferrable="false" initiallyDeferred="false"
                                 referencedColumnNames="id" referencedTableName="participants_geo_area"/>

    </changeSet>

    <changeSet author="weitzel" id="2018-08-16_21-06" dbms="mysql,mssql">
        <comment>
            DD-2692: Constraint for AppVariantUsages to avoid duplicates instead of removing them
        </comment>

        <addUniqueConstraint columnNames="app_variant_id, person_id"
                             constraintName="UKAppVariantUsage_AppVarian_Person_fyok1b5w2d"
                             tableName="app_variant_usage"/>
    </changeSet>


    <changeSet author="silva" id="2018-08-21_10-00" dbms="mysql,mssql">
        <comment>DD-2757: Added an extra attribute for the venue address</comment>
        <addColumn tableName="grapevine_post">
            <column name="venue_address_id" type="varchar(36)"/>
        </addColumn>
        <addForeignKeyConstraint baseColumnNames="venue_address_id" baseTableName="grapevine_post"
                                 constraintName="FKGrapevinePost_VenueAddress_Address_32igi8efbr" deferrable="false"
                                 initiallyDeferred="false" referencedColumnNames="id" referencedTableName="address"/>
    </changeSet>

</databaseChangeLog>