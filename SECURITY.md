# Security Policy

We continuously release new versions to fix known security vulnerabilities and update our dependencies.

## Reporting a Vulnerability

Please report (suspected) security vulnerabilities to
**[support@digitale-doerfer.de](mailto:support@digitale-doerfer.de)**. You will receive a response from
us within two working days. If the issue is confirmed, we will release a patch as soon as possible depending on
complexity.