<#if dependencyMap?size == 0>
    The project has no dependencies.
<#else>
    Lists of ${dependencyMap?size} third-party dependencies.

    <#list dependencyMap as e>
        <#assign p = e.getKey()/>
        <#assign licenses = e.getValue()/>
        <#if p.name?index_of('Unnamed') &gt; -1>
            <#assign name = p.artifactId/>
        <#else>
            <#assign name = p.name/>
        </#if><#-- @formatter:off -->
* [${p.artifactId}](${p.url!"no url defined"})
    * `${p.groupId}:${p.artifactId}`
    * ${p.description?replace("\n"," ")?replace("\\s+"," ","r")!"-"}
        <#list licenses as license>
    * Published under ${license}
        </#list>
    </#list><#-- @formatter:on -->
</#if>
