/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ExternalPostRepository;

@Component
public class GrapevineExternalIdMigrationAdminTask extends BaseAdminTask {

    private static final Pattern PATTERN = Pattern.compile("^(.*[\\D])([\\d]+)$");

    private final ExternalPostRepository externalPostRepository;

    @Autowired
    public GrapevineExternalIdMigrationAdminTask(final ExternalPostRepository externalPostRepository) {
        this.externalPostRepository = externalPostRepository;
    }

    @Override
    public String getName() {
        return "GrapevineExternalIdMigration";
    }

    @Override
    public String getDescription() {
        return "Migrates the external ID of external posts from a URL to a numeric format.";
    }

    private void migrateExternalPost(final ExternalPost externalPost, final LogSummary logSummary, final AdminTaskParameters parameters) {
        final String externalId = externalPost.getExternalId();
        final Matcher matcher = PATTERN.matcher(externalId);
        if (matcher.matches()) {
            final String migratedId = matcher.group(2);
            logSummary.info("Migrating external ID from {} to {}", externalId, migratedId);
            if (!parameters.isDryRun()) {
                externalPost.setExternalId(migratedId);
                externalPostRepository.save(externalPost);
            }
        } else {
            logSummary.warn("External ID doesn't matches the expected format: {}", externalId);
        }
    }

    @Override
    public void execute(final LogSummary logSummary, final AdminTaskParameters parameters) throws Exception {

        processPages(logSummary, parameters,
                externalPostRepository::findAllByOrderByCreated,
                externalPosts -> {

                    processParallel(
                            externalPost -> migrateExternalPost(externalPost, logSummary, parameters),
                            externalPosts.getContent(),
                            logSummary, parameters);

                    if (!parameters.isDryRun()) {
                        externalPostRepository.flush();
                    }
                });

    }

}
