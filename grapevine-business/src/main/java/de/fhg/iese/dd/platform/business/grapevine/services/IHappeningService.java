/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.HappeningParticipant;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IHappeningService extends IEntityService<Happening> {

    Happening findById(String happeningId) throws PostNotFoundException;

    Page<Person> findAllParticipantsByHappening(Happening happening, Person caller, Pageable pageable);

    List<HappeningParticipant> findAllHappeningParticipationsByPerson(Person person);

    void setParticipated(Person person, Collection<Post> posts);

    Happening updateParticipationCount(Happening happening);

    Happening confirmParticipation(Happening happening, Person person);

    Happening revokeParticipation(Happening happening, Person person);

    void deleteAllHappeningParticipations(Happening happening);

}
