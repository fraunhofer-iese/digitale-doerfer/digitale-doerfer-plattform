/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.events;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryDocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
public abstract class BasePostCreateRequest extends BaseEvent {

    @Nullable
    private Person creator;
    private AppVariant appVariant;
    private GPSLocation createdLocation;
    private Address customAddress;
    @NonNull
    private String text;
    private Organization organization;
    private List<TemporaryMediaItem> temporaryMediaItemsToAdd;
    private Set<TemporaryDocumentItem> temporaryDocumentItemsToAdd;
    private boolean hiddenForOtherHomeAreas;
    @Builder.Default
    private boolean commentsDisallowed = false;
    private Map<String, Object> customAttributes;
    @Nullable
    private String pushMessage;
    private Boolean pushMessageLoud;
    private PushCategory.PushCategoryId pushCategoryId;

}
