/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Steffen Hupp, Benjamin Hassenfratz, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.*;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipCreateInGroupRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.gossip.GossipCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.newsitem.NewsItemCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.offer.OfferCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.seeking.SeekingCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.SuggestionCannotBeDeletedException;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeStatusConfirmation;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@EventProcessor
class PostEventProcessor extends BasePostEventProcessor {

    @Autowired
    private IPostInteractionService postInteractionService;

    @EventProcessing
    private PostCreateConfirmation<Gossip> handleGossipCreateRequest(GossipCreateRequest request) {

        return handleBasePostCreateRequest(request, new Gossip());
    }

    @EventProcessing
    private PostCreateConfirmation<Gossip> handleGossipCreateInGroupRequest(GossipCreateInGroupRequest request) {

        return handleBasePostCreateRequest(request, Gossip.builder()
                .group(request.getGroup())
                .tenant(request.getGroup().getTenant())
                .build());
    }

    @EventProcessing
    private PostCreateConfirmation<Seeking> handleSeekingCreateRequest(SeekingCreateRequest request) {

        return handleTradeCreateRequest(request, new Seeking());
    }

    @EventProcessing
    private PostCreateConfirmation<Offer> handleOfferCreateRequest(OfferCreateRequest request) {

        return handleTradeCreateRequest(request, new Offer());
    }

    private <T extends Trade> PostCreateConfirmation<T> handleTradeCreateRequest(BaseTradeCreateRequest request,
            T trade) {

        trade.setTradingStatus(TradingStatus.OPEN);
        trade.setTradingCategory(request.getTradingCategory());
        trade.setCustomAddress(request.getCustomAddress());
        trade.setDate(request.getDate());

        return handleBasePostCreateRequest(request, trade);
    }

    @EventProcessing
    private PostDeleteConfirmation handlePostDeleteRequest(PostDeleteRequest request)
            throws SuggestionCannotBeDeletedException {

        Post post = request.getPost();

        if (post instanceof Suggestion suggestion) {
            throw new SuggestionCannotBeDeletedException(suggestion);
        }

        post = postService.deletePost(post);
        return new PostDeleteConfirmation(post);
    }

    @EventProcessing
    private PostChangeConfirmation<Gossip> handleGossipChangeRequest(GossipChangeRequest request) {

        return changePost(request, request.getPost());
    }

    @EventProcessing(includeSubtypesOfEvent = true)
    private <T extends Trade> PostChangeConfirmation<T> handleTradeChangeRequest(BaseTradeChangeRequest<T> request) {

        T tradeToBeChanged = request.getPost();

        if (request.getTradingStatus() != null) {
            tradeToBeChanged.setTradingStatus(request.getTradingStatus());
        }
        if (request.getTradingCategory() != null) {
            tradeToBeChanged.setTradingCategory(request.getTradingCategory());
        }

        if (request.getDate() != 0) {
            tradeToBeChanged.setDate(request.getDate());
        }

        return changePost(request, tradeToBeChanged);
    }

    @EventProcessing
    private PostCreateConfirmation<NewsItem> handleNewsItemCreateByNewsSourceRequest(
            NewsItemCreateByNewsSourceRequest request) {

        return handleExternalPostCreateByNewsSourceRequest(request, new NewsItem());
    }

    @EventProcessing
    private List<BaseEvent> handleNewsItemChangeByNewsSourceRequest(NewsItemChangeByNewsSourceRequest request) {

        return handleExternalPostChangeByNewsSourceRequest(request, request.getPost());
    }

    @EventProcessing
    private void updatePostOnCommentCreation(CommentCreateConfirmation confirmation) {
        final Comment comment = confirmation.getComment();
        final Post post = comment.getPost();
        post.setLastActivity(comment.getLastModified());
        post.setCommentCount(post.getCommentCount() + 1);
        postService.store(post);
    }

    @EventProcessing
    private void updatePostOnCommentChange(CommentChangeConfirmation confirmation) {
        final Comment comment = confirmation.getComment();
        final Post post = comment.getPost();
        post.setLastActivity(comment.getLastModified());
        postService.store(post);
    }

    @EventProcessing
    private ExternalPostPublishConfirmation handleExternalPostPublishRequest(ExternalPostPublishRequest request) {
        ExternalPost post = request.getPost();

        post.setPublished(true);
        ExternalPost storedPost = postService.storePost(post);
        log.debug("published post {}", post.getId());

        return new ExternalPostPublishConfirmation(storedPost);
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
    private ExternalPostUnpublishConfirmation handleExternalPostUnpublishRequest(ExternalPostUnpublishRequest request) {
        ExternalPost post = request.getPost();

        post.setPublished(false);
        ExternalPost storedPost = postService.storePost(post);
        log.debug("unpublished post {}", post.getId());

        return new ExternalPostUnpublishConfirmation(storedPost, storedPost.getGeoAreas());
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
    private void handlePostsViewEvent(PostsViewEvent event) {

        final Person person = event.getViewer();
        final List<PostView> postViews = event.getPostViews();

        postInteractionService.updatePostViews(person, postViews);
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
    private void handlePersonChangeStatusConfirmation(PersonChangeStatusConfirmation event) {

        Person person = event.getUpdatedPerson();
        if (event.getStatusesAdded().contains(PersonStatus.PARALLEL_WORLD_INHABITANT)) {
            postService.movePostsToParallelWorld(person);
        }
        if (event.getStatusesRemoved().contains(PersonStatus.PARALLEL_WORLD_INHABITANT)) {
            postService.movePostsToRealWorld(person);
        }
    }

}
