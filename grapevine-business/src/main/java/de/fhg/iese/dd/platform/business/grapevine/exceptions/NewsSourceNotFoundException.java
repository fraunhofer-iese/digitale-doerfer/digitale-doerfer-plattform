/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;

public class NewsSourceNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 8455589620821515897L;

    private NewsSourceNotFoundException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static NewsSourceNotFoundException forId(String id) {
        return new NewsSourceNotFoundException("Could not find newsSource with id '{}'.", id);
    }

    public static NewsSourceNotFoundException forIdDeleted(String id) {

        return new NewsSourceNotFoundException("Could not find newsSource with id '{}' because it is deleted.", id);
    }

    public static NewsSourceNotFoundException forSiteUrl(String siteUrl) {
        return new NewsSourceNotFoundException("Could not find newsSource with siteUrl '{}'.", siteUrl);
    }

    public static NewsSourceNotFoundException forSiteUrlDeleted(String siteUrl) {

        return new NewsSourceNotFoundException("Could not find newsSource with siteUrl '{}' because it is deleted.",
                siteUrl);
    }

    public static NewsSourceNotFoundException forAppVariant(String appVariantIdentifier) {
        return new NewsSourceNotFoundException(
                "Could not find newsSource with appVariantIdentifier '{}'.", appVariantIdentifier);
    }

    public static NewsSourceNotFoundException forAppVariantDeleted(String appVariantIdentifier) {

        return new NewsSourceNotFoundException(
                "Could not find newsSource with appVariantIdentifier '{}' because it is deleted.",
                appVariantIdentifier);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.NEWS_SOURCE_NOT_FOUND;
    }

}
