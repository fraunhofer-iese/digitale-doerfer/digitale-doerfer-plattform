/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.TradingCategoryRepository;

@Component
public class GrapevineCategoryDataInitializer extends BaseDataInitializer {

    public static final String SUGGESTION_CATEGORY_MISC_ID = "26d80fef-47d5-43de-af33-1eb9c744e651";
    public static final String TRADING_CATEGORY_MISC_ID = "3412fd58-ed42-4973-9b8d-0e6d489ca0fa";
    public static final String TOPIC = "post-category";

    @Autowired
    private SuggestionCategoryRepository suggestionCategoryRepository;
    @Autowired
    private TradingCategoryRepository tradingCategoryRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                .processedEntity(SuggestionCategory.class)
                .processedEntity(TradingCategory.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {
        logSummary.indent();
        try {
            createSuggestionCategories(logSummary);
            createTradingCategories(logSummary);
        } finally {
            logSummary.outdent();
        }
    }

    private void createSuggestionCategories(LogSummary logSummary) {
        final List<SuggestionCategory> suggestionCategories = loadAllEntities(SuggestionCategory.class);

        logSummary.info("Creating {} suggestion categories", suggestionCategories.size());
        int orderValue = 1;
        for (SuggestionCategory suggestionCategory : suggestionCategories) {
            suggestionCategoryRepository.save(suggestionCategory
                    .withId(checkOrLookupId(suggestionCategory.getId()))
                    .withOrderValue(orderValue++));
            logSummary.info("created {}", suggestionCategory);
        }
        suggestionCategoryRepository.flush();
    }

    private void createTradingCategories(LogSummary logSummary) {
        final List<TradingCategory> tradingCategories = loadAllEntities(TradingCategory.class);

        logSummary.info("Creating {} trading categories", tradingCategories.size());
        int orderValue = 1;
        for (TradingCategory tradingCategory : tradingCategories) {
            tradingCategoryRepository.save(tradingCategory
                    .withId(checkOrLookupId(tradingCategory.getId()))
                    .withOrderValue(orderValue++));
            logSummary.info("created {}", tradingCategory);
        }
        tradingCategoryRepository.flush();
    }

}
