/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.*;
import de.fhg.iese.dd.platform.business.grapevine.services.ICommentService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@EventProcessor
class CommentEventProcessor extends BaseEventProcessor {

    @Autowired
    private ICommentService commentService;

    @Autowired
    private IMediaItemService mediaItemService;

    @Autowired
    private IDocumentItemService documentItemService;

    @EventProcessing
    private CommentCreateConfirmation handleCommentCreateRequest(CommentCreateRequest request) {
        Comment comment = Comment.builder()
                .post(request.getPost())
                .creator(request.getCreator())
                //we "free" the temporary media items and add them to the new entity
                //if none are added we get an empty list for no cost
                .images(mediaItemService.useAllItems(request.getTemporaryMediaItemsToAdd(),
                        FileOwnership.of(request.getCreator(), request.getAppVariant())))
                .attachments(documentItemService.useAllItemsUnordered(request.getTemporaryDocumentItemsToAdd(),
                        FileOwnership.of(request.getCreator(), request.getAppVariant())))
                .text(request.getText())
                .lastModified(timeService.currentTimeMillisUTC())
                .replyTo(request.getReplyTo())
                .build();
        comment = commentService.store(comment);
        return new CommentCreateConfirmation(comment, request.getAppVariant());
    }

    @EventProcessing
    private void handleCommentChangeRequest(CommentChangeRequest request, final EventProcessingContext context) {

        Comment comment = request.getComment();
        if (StringUtils.isNotEmpty(request.getText()) || request.isUseEmptyFields()) {
            comment.setText(request.getText());
        }

        final List<MediaItem> oldImages = comment.getImages();

        List<MediaItem> newImages;
        if (CollectionUtils.isEmpty(request.getImageHolders())) {
            if (request.isUseEmptyFields()) {
                newImages = Collections.emptyList();
            } else {
                newImages = oldImages;
            }
        } else {
            //use the temporary and existing images
            newImages =
                    mediaItemService.useTemporaryAndExistingMediaItems(request.getImageHolders(),
                            FileOwnership.of(comment.getCreator()));
        }
        final Runnable orphanMediaItemsDeletion = mediaItemService.getOrphanItemDeletion(oldImages, newImages);

        comment.setImages(newImages);

        final Set<DocumentItem> oldAttachments = comment.getAttachments();

        Set<DocumentItem> newAttachments;
        if (CollectionUtils.isEmpty(request.getDocumentHolders())) {
            if (request.isUseEmptyFields()) {
                newAttachments = Collections.emptySet();
            } else {
                newAttachments = oldAttachments;
            }
        } else {
            //use the temporary and existing documents
            newAttachments = documentItemService.useTemporaryAndExistingDocumentItems(request.getDocumentHolders(),
                    FileOwnership.of(comment.getCreator()));
        }
        final Runnable orphanDocumentItemsDeletion =
                documentItemService.getOrphanItemDeletion(oldAttachments, newAttachments);

        comment.setAttachments(newAttachments);

        comment.setLastModified(timeService.currentTimeMillisUTC());
        comment = commentService.store(comment);
        notify(new CommentChangeConfirmation(comment, request.getAppVariant()), context);

        orphanMediaItemsDeletion.run();
        orphanDocumentItemsDeletion.run();
    }

    @EventProcessing
    private CommentDeleteConfirmation handleCommentDeleteRequest(CommentDeleteRequest request) {
        Comment deletedComment = commentService.deleteComment(request.getComment());
        return new CommentDeleteConfirmation(deletedComment);
    }

    @EventProcessing
    private List<CommentDeleteRequest> handleCommentDeleteAllForPostRequest(CommentDeleteAllForPostRequest request) {

        final List<Comment> comments = commentService
                .findAllByPostAndCreatorNotDeleted(request.getPost(), request.getCreator());

        return comments.stream()
                .map(CommentDeleteRequest::new)
                .collect(Collectors.toList());
    }

}
