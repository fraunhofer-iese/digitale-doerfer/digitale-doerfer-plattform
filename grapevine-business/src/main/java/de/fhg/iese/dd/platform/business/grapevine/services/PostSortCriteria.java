/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Adeline Silva Schäfer, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import org.springframework.data.domain.Sort;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PostSortCriteria {

    CREATED(Post.class, "created", Sort.Direction.DESC, 0, Long.MAX_VALUE),
    LAST_MODIFIED(Post.class, "lastModified", Sort.Direction.DESC, 0, Long.MAX_VALUE),
    LAST_ACTIVITY(Post.class, "lastActivity", Sort.Direction.DESC, 0, Long.MAX_VALUE),
    LAST_SUGGESTION_ACTIVITY(Suggestion.class, "lastSuggestionActivity", Sort.Direction.DESC, 0, Long.MAX_VALUE),
    HAPPENING_START(Happening.class, "startTime", Sort.Direction.ASC, 0, Long.MAX_VALUE);

    private final Class<? extends Post> postType;
    private final String column;
    private final Sort.Direction direction;
    private final long defaultStart;
    private final long defaultEnd;

    public boolean isAvailableFor(final Class<? extends Post> postType) {
        return this.postType.isAssignableFrom(postType);
    }

}
