/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.template;

import org.apache.logging.log4j.message.ParameterizedMessage;

import de.fhg.iese.dd.platform.business.shared.template.TemplateLocation;

public class GrapevineTemplate extends TemplateLocation {

    public static final GrapevineTemplate NEW_SUGGESTION_EMAIL = new GrapevineTemplate("NewSuggestionEmail.ftl");
    public static final String NEW_SUGGESTION_EMAIL_SIMPLE = "[LösBar] Neuer Fall";
    public static final String NEW_SUGGESTION_EMAIL_CATEGORY = "[LösBar] Neuer Fall \"{}\"";
    public static final GrapevineTemplate SUGGESTION_STATUS_CHANGE =
            new GrapevineTemplate("SuggestionStatusChangeMail.ftl");
    public static final String SUGGESTION_STATUS_CHANGE_SUBJECT = "[LösBar] Statusänderung";
    public static final GrapevineTemplate SUGGESTION_WORKER_ADD_EMAIL =
            new GrapevineTemplate("SuggestionWorkerAddMail.ftl");
    public static final String SUGGESTION_WORKER_ADD_SUBJECT = "[LösBar] Deine Hilfe ist gefragt";
    public static final GrapevineTemplate NEW_SUGGESTION_COMMENT_EMAIL =
            new GrapevineTemplate("NewSuggestionCommentEmail.ftl");
    public static final String NEW_SUGGESTION_COMMENT = "[LösBar] Neuer Kommentar zu einem deiner Fälle";

    public static final GrapevineTemplate NEW_GROUP_JOIN_REQUEST_EMAIL =
            new GrapevineTemplate("NewGroupJoinRequest.ftl");
    public static final String NEW_GROUP_JOIN_REQUEST_SUBJECT =
            "[DorfFunk] Neue Beitrittsanfrage in deiner Gruppe \"{}\"";

    public static final GrapevineTemplate GRAPEVINE_STATISTICS_REPORT_HTML =
            new GrapevineTemplate("GrapevineStatisticsReportHTML.ftl");
    public static final GrapevineTemplate TENANT_GROUP_STATISTICS_REPORT_HTML =
            new GrapevineTemplate("TenantGroupStatisticsReportHTML.ftl");
    public static final GrapevineTemplate TENANT_STATISTICS_REPORT_HTML =
            new GrapevineTemplate("TenantStatisticsReportHTML.ftl");

    public static String formattedString(String message, Object... arguments) {
        return new ParameterizedMessage(message, arguments).getFormattedMessage();
    }

    private GrapevineTemplate(String name) {
        super(name);
    }

}
