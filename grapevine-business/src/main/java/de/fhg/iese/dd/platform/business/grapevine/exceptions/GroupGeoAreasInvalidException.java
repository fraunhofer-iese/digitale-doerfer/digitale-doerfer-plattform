/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import java.util.Collection;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

public class GroupGeoAreasInvalidException extends BadRequestException {

    private static final long serialVersionUID = -7218863502429713539L;

    private GroupGeoAreasInvalidException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static GroupGeoAreasInvalidException forMainGeoAreaNotInIncluded(GeoArea mainGeoArea,
            Collection<GeoArea> includedGeoAreas) {
        return new GroupGeoAreasInvalidException("Main geo area {} is not in the included geo areas {}",
                BaseEntity.getIdOf(mainGeoArea), BaseEntity.getIdsOf(includedGeoAreas));
    }

    public static GroupGeoAreasInvalidException forExcludedGeoAreaInIncluded(Collection<GeoArea> includedGeoAreas,
            Collection<GeoArea> excludedGeoAreas) {
        return new GroupGeoAreasInvalidException("The excluded geo areas {} contain an included geo area {}",
                BaseEntity.getIdsOf(includedGeoAreas), BaseEntity.getIdsOf(excludedGeoAreas));
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.GROUP_GEO_AREAS_INVALID;
    }

}
