/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;

@Component
public class GrapevineUpdateIndexAdminTask extends BaseAdminTask {

    @Autowired
    private IGrapevineSearchService grapevineSearchService;

    @Override
    public String getName() {
        return "GrapevineUpdateSearchIndex";
    }

    @Override
    public String getDescription() {
        return "Checks or updates the search index with new settings and mappings. " +
                "When updating the index is closed first and opened afterwards.\n" +
                "To check, use 'dryRun'.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {

        if (parameters.isDryRun()) {
            grapevineSearchService.checkIndex(logSummary);
        } else {
            grapevineSearchService.updateIndex(logSummary);
        }
    }

}
