/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.events;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.CroppedImageReference;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.lang.Nullable;

import java.net.URL;
import java.util.List;

import java.net.URI;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
public abstract class BaseExternalPostChangeByNewsSourceRequest<T extends ExternalPost> extends BasePostChangeRequest<T> {

    private Set<GeoArea> geoAreas;
    private String postURL;

    private String authorName;
    private String categories;
    private List<URL> imageURLs;
    private List<CroppedImageReference> croppedImageReferences;
    private MediaItem imageUploaded;

    private Long desiredPublishTime;
    private Long desiredUnpublishTime;
    @Builder.Default
    private boolean publishImmediately = true;
    @Builder.Default
    private boolean commentsDisallowed = false;
    
}
