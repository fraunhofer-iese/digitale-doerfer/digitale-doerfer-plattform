/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.EventProcessingContext;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.BasePostCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnPostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupLeaveConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.business.shared.log.processors.BaseLogEventProcessor;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.events.UserGeneratedContentFlagConfirmation;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
class GrapevineLogEventProcessor extends BaseLogEventProcessor {

    @EventProcessing(relevantEvents = {
            BasePostCreateRequest.class,
            PostCreateConfirmation.class,
            PostChangeConfirmation.class,
            PostDeleteConfirmation.class,
            CommentCreateConfirmation.class,
            CommentChangeConfirmation.class,
            CommentDeleteConfirmation.class,
            UserGeneratedContentFlagConfirmation.class,
            ChatStartOnPostConfirmation.class,
            ChatStartOnCommentConfirmation.class,
            GroupJoinConfirmation.class,
            GroupLeaveConfirmation.class,
            GroupPendingJoinDenyConfirmation.class,
            GroupPendingJoinAcceptConfirmation.class},
            includeSubtypesOfEvent = true
    )
    protected void logEvent(BaseEvent event, EventProcessingContext context) {
        super.logEvent(event, context);
    }

}
