/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;

public interface ICommentService extends IEntityService<Comment> {

    /**
     * also includes deleted comments, so check for {@link Comment#isDeleted()}!
     *
     * @param commentId
     *
     * @return
     *
     * @throws CommentNotFoundException
     */
    Comment findCommentByIdIncludingDeleted(String commentId) throws CommentNotFoundException;

    /**
     * All comments of the post.
     * <p>
     * Also includes deleted comments, so check for {@link Comment#isDeleted()}!
     *
     * @param post
     *
     * @return
     */
    List<Comment> findAllByPostOrderByCreatedAsc(Post post);

    /**
     * All comments of the post, excluding those from creators with status {@link PersonStatus#PARALLEL_WORLD_INHABITANT}.
     * <p>
     * Also includes deleted comments, so check for {@link Comment#isDeleted()}!
     *
     * @param caller the caller that wants to see the comments
     * @param post   the post of the comments
     *
     * @return
     */
    List<Comment> findAllByPostFilteredOrderByCreatedAsc(Person caller, Post post);

    /**
     * Finds all comments a person created.
     * <p/>
     * Also includes deleted comments, so check for {@link Comment#isDeleted()}!
     *
     * @param creator
     *         the person that created the comment
     *
     * @return
     */
    List<Comment> findAllByCreatorOrderByCreatedAsc(Person creator);

    /**
     * Finds all comments in a given post a person created, which are not deleted.
     *
     * @param post
     * @param creator
     *          the person that created the comment
     * @return
     */
    List<Comment> findAllByPostAndCreatorNotDeleted(Post post, Person creator);

    /**
     * Only includes comments that exists and have not been deleted (hence {@link Comment#isDeleted()} is false)
     *
     * @param commentId
     * @return
     * @throws CommentNotFoundException
     */
    Comment findCommentByIdNotDeleted(String commentId) throws CommentNotFoundException;

    /**
     * Deletes all comments (including images) of a post. Only use for demo data. For real data use {@link
     * #deleteComment(Comment)}
     *
     * @param post
     */
    void deleteCommentsForDemoPost(Post post);

    /**
     * Deletes a comment
     *
     * @param comment the comment to delete
     *
     * @return the deleted comment
     */
    Comment deleteComment(Comment comment);

}
