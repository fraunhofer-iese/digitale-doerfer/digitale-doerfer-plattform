/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.specialpost.SpecialPostChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.specialpost.SpecialPostCreateRequest;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SpecialPost;

@EventProcessor
class SpecialPostEventProcessor extends BasePostEventProcessor {

    @EventProcessing
    private PostCreateConfirmation<SpecialPost> handleSpecialPostCreateRequest(SpecialPostCreateRequest request) {

        SpecialPost specialPost = new SpecialPost();

        return handleBasePostCreateRequest(request, specialPost);
    }

    @EventProcessing
    private PostChangeConfirmation<SpecialPost> handleSpecialPostChangeRequest(SpecialPostChangeRequest request) {

        SpecialPost specialPost = request.getPost();

        return changePost(request, specialPost);
    }

}
