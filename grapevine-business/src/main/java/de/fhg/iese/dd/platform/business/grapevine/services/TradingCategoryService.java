/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.DefaultTradingCategoryMustNotBeDeletedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.TradingCategoryNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.init.GrapevineCategoryDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.TradeRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.TradingCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class TradingCategoryService extends BaseEntityService<TradingCategory> implements ITradingCategoryService {

    @Autowired
    private TradingCategoryRepository tradingCategoryRepository;
    @Autowired
    private TradeRepository tradeRepository;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    private Map<String, TradingCategory> tradingCategoryById;

    @PostConstruct
    private void initialize() {
        //this cache is replaced with a new map after it got cleared
        tradingCategoryById = Collections.emptyMap();
    }

    @Override
    public CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(TradingCategory.class)
                .build();
    }

    @Override
    public TradingCategory findTradingCategoryById(String tradingCategoryId) throws TradingCategoryNotFoundException {

        checkAndRefreshCache();
        TradingCategory tradingCategory = tradingCategoryById.get(tradingCategoryId);
        if (tradingCategory == null) {
            throw new TradingCategoryNotFoundException(tradingCategoryId);
        }
        return tradingCategory;
    }

    @Override
    public List<TradingCategory> findAllTradingCategoriesInDataInitOrder() {

        checkAndRefreshCache();
        return tradingCategoryById.values().stream()
                .sorted(Comparator.comparingInt(TradingCategory::getOrderValue))
                .collect(Collectors.toList());
    }

    @Override
    public TradingCategory getDefaultTradingCategory() {

        checkAndRefreshCache();
        TradingCategory defaultTradingCategory = tradingCategoryById.get(
                GrapevineCategoryDataInitializer.TRADING_CATEGORY_MISC_ID);
        if (defaultTradingCategory == null) {
            throw new IllegalStateException("Data init for topic 'post-category' has not been run");
        }
        return defaultTradingCategory;
    }

    @Override
    @Transactional
    public void deleteTradingCategory(TradingCategory tradingCategory, Person deleter) throws
            DefaultTradingCategoryMustNotBeDeletedException {

        Objects.requireNonNull(tradingCategory);
        final TradingCategory defaultCategory = getDefaultTradingCategory();
        if (tradingCategory.equals(defaultCategory)) {
            throw new DefaultTradingCategoryMustNotBeDeletedException();
        }
        ReferenceDataChangeLogEntry referenceDataChangeLogEntry =
                referenceDataChangeService.acquireReferenceDataChangeLock(deleter,
                        "Delete " + tradingCategory + " via API", false);
        try {
            tradeRepository.updateTradingCategory(tradingCategory, defaultCategory);
            tradingCategoryRepository.delete(tradingCategory);
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, true);

        } catch (Exception ex) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, false);
            throw ex;
        }
    }

    @Override
    public void invalidateCache() {

        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {
        // if the cache is stale or no fetch ever happened
        // get all trading categories fresh from the database
        cacheChecker.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();

            tradingCategoryById = query(() -> tradingCategoryRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, TradingCategory.class))
                    .collect(Collectors.toMap(TradingCategory::getId, Function.identity())));
            log.info("Refreshed {} cached trading categories in {} ms", tradingCategoryById.size(),
                    System.currentTimeMillis() - start);
        });
    }

}
