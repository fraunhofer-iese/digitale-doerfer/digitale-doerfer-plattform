/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

@Component
public class GrapevineSetGroupMainGeoAreaAdminTask extends BaseAdminTask {

    @Autowired
    private IGroupService groupService;
    @Autowired
    private GroupRepository groupRepository;

    @Override
    public String getName() {
        return "GrapevineSetGroupMainGeoArea";
    }

    @Override
    public String getDescription() {
        return "Sets the main geo area for groups that do not have a main geo area defined.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        
        processPages(logSummary, parameters,
                page -> groupRepository.findGroupsWithoutMainGeoArea(page),
                groupPage -> {
                    for (Group group : groupPage) {

                        IGroupService.GroupGeoAreas groupGeoAreas = groupService.findGeoAreasForGroup(group);
                        GeoArea mainGeoArea = groupService.guessMainGeoArea(groupGeoAreas.getIncludedGeoAreas());
                        if (!parameters.isDryRun()) {
                            groupService.setGroupGeoAreaMappings(group,
                                    mainGeoArea,
                                    groupGeoAreas.getIncludedGeoAreas(),
                                    groupGeoAreas.getExcludedGeoAreas());
                        }
                        logSummary.info("{}Processed '{}' with geo areas [{}], main geo area: '{}'",
                                groupGeoAreas.getIncludedGeoAreas().size() > 1 ? "!CHECK! " : "",
                                group.getName(),
                                groupGeoAreas.getIncludedGeoAreas().stream()
                                        .map(g -> "'" + g.getName() + "'")
                                        .collect(Collectors.joining(", ")),
                                mainGeoArea != null ? mainGeoArea.getName() : "-");
                    }
                });
    }

}
