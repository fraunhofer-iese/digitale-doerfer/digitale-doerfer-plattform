/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import static java.util.Map.entry;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.LikeCommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.LikeCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.PostCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.ViewCountByGeoAreaId;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;

@Component
public class GrapevineStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_POST_COUNT_GOSSIP = "grapevine.post.count.gossip";
    public static final String STATISTICS_ID_POST_COUNT_SUGGESTION = "grapevine.post.count.suggestion";
    public static final String STATISTICS_ID_POST_COUNT_OFFER = "grapevine.post.count.offer";
    public static final String STATISTICS_ID_POST_COUNT_SEEKING = "grapevine.post.count.seeking";
    public static final String STATISTICS_ID_POST_COUNT_HAPPENING = "grapevine.post.count.happening";
    public static final String STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED =
            "grapevine.post.count.happening.person";
    public static final String STATISTICS_ID_POST_COUNT_NEWSITEM = "grapevine.post.count.newsitem";
    public static final String STATISTICS_ID_POST_COUNT_SPECIALPOST = "grapevine.post.count.specialpost";
    public static final String STATISTICS_ID_POST_COUNT_WITH_IMAGE = "grapevine.post.count.withimage";
    public static final String STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT = "grapevine.post.count.withattachment";
    public static final String STATISTICS_ID_POST_COUNT_IN_GROUP = "grapevine.post.count.group";
    public static final String STATISTICS_ID_LIKE_COUNT_POST = "grapevine.like.count.post";
    public static final String STATISTICS_ID_LIKE_COUNT_COMMENT = "grapevine.like.count.comment";
    public static final String STATISTICS_ID_PERSON_COUNT_SELECTED_GEOAREA = "grapevine.dorffunk.geoarea.selected";
    public static final String STATISTICS_ID_POST_VIEW_DETAIL_COUNT = "grapevine.post.view.detail.count";
    public static final String STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT = "grapevine.post.view.overview.count";

    @Autowired
    private IAppService appService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private LikeCommentRepository likeCommentRepository;
    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_GOSSIP)
                .machineName("gossip-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_SUGGESTION)
                .machineName("suggestion-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_OFFER)
                .machineName("offer-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_SEEKING)
                .machineName("seeking-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_HAPPENING)
                .machineName("happening-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED)
                .machineName("happening-count-person")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_NEWSITEM)
                .machineName("newsitem-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_SPECIALPOST)
                .machineName("specialpost-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_WITH_IMAGE)
                .machineName("post-count-image")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT)
                .machineName("post-count-attachment")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_COUNT_IN_GROUP)
                .machineName("post-count-group")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_LIKE_COUNT_COMMENT)
                .machineName("like-count-comment")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_LIKE_COUNT_POST)
                .machineName("like-count-post")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_PERSON_COUNT_SELECTED_GEOAREA)
                .machineName("person-count-selected-geoarea")
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_VIEW_DETAIL_COUNT)
                .machineName("view-detail-count-post")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT)
                .machineName("view-overview-count-post")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        calculatePostStatistics(geoAreaInfos, definition);
        calculateLikeStatistics(geoAreaInfos, definition);
        calculateSelectedGeoAreaStatistics(geoAreaInfos, definition);
        calculateViewStatistics(geoAreaInfos, definition);
    }

    private void calculateViewStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(postInteractionRepository::countViewsByGeoAreaId, Map.of(
                        STATISTICS_ID_POST_VIEW_DETAIL_COUNT, ViewCountByGeoAreaId::getViewedDetailCount,
                        STATISTICS_ID_POST_VIEW_OVERVIEW_COUNT, ViewCountByGeoAreaId::getViewedOverviewCount),
                geoAreaInfos, definition);
    }

    private void calculatePostStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(postRepository::countByGeoAreaId,
                Map.ofEntries(
                        entry(STATISTICS_ID_POST_COUNT_GOSSIP, PostCountByGeoAreaId::getPostCountGossip),
                        entry(STATISTICS_ID_POST_COUNT_SUGGESTION, PostCountByGeoAreaId::getPostCountSuggestion),
                        entry(STATISTICS_ID_POST_COUNT_OFFER, PostCountByGeoAreaId::getPostCountOffer),
                        entry(STATISTICS_ID_POST_COUNT_SEEKING, PostCountByGeoAreaId::getPostCountSeeking),
                        entry(STATISTICS_ID_POST_COUNT_HAPPENING, PostCountByGeoAreaId::getPostCountHappening),
                        entry(STATISTICS_ID_POST_COUNT_HAPPENING_PERSON_CREATED,
                                PostCountByGeoAreaId::getPostCountHappeningPerson),
                        entry(STATISTICS_ID_POST_COUNT_NEWSITEM, PostCountByGeoAreaId::getPostCountNewsItem),
                        entry(STATISTICS_ID_POST_COUNT_SPECIALPOST, PostCountByGeoAreaId::getPostCountSpecialPost),
                        entry(STATISTICS_ID_POST_COUNT_IN_GROUP, PostCountByGeoAreaId::getPostCountInGroup),
                        entry(STATISTICS_ID_POST_COUNT_WITH_IMAGE, PostCountByGeoAreaId::getPostCountWithImage),
                        entry(STATISTICS_ID_POST_COUNT_WITH_ATTACHMENT,
                                PostCountByGeoAreaId::getPostCountWithAttachment)),
                geoAreaInfos, definition);
    }

    private void calculateLikeStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(likeCommentRepository::countLikeByGeoAreaId,
                Map.of(STATISTICS_ID_LIKE_COUNT_COMMENT, LikeCountByGeoAreaId::getLikeCount),
                geoAreaInfos, definition);
        querySummingStatistics(postInteractionRepository::countLikeByGeoAreaId,
                Map.of(STATISTICS_ID_LIKE_COUNT_POST, LikeCountByGeoAreaId::getLikeCount),
                geoAreaInfos, definition);
    }

    private void calculateSelectedGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        if (!definition.getStatisticsIds().contains(STATISTICS_ID_PERSON_COUNT_SELECTED_GEOAREA)) {
            return;
        }

        final App dorfFunkApp = appService.findById(DorfFunkConstants.APP_ID);
        Map<String, Long> personCountByGeoAreaId = appService.getPersonCountBySelectedGeoAreaId(dorfFunkApp);

        geoAreaInfos.forEach(
                geoAreaInfo -> geoAreaInfo.setStatisticsValueLong(STATISTICS_ID_PERSON_COUNT_SELECTED_GEOAREA,
                        StatisticsTimeRelation.TOTAL,
                        personCountByGeoAreaId.getOrDefault(geoAreaInfo.getGeoArea().getId(), 0L)));
    }

}
