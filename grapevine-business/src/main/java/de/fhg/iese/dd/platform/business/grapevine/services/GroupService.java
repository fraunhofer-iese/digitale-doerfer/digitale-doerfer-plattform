/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.*;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ConcurrentRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.GroupAccessibilityApprovalRequiredFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupMembershipRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupIdMetaData;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.GroupMetaData;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nullable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
class GroupService extends BaseEntityService<Group> implements IGroupService {

    private static final Set<GroupAccessibility> ALLOWED_GROUP_ACCESSIBILITY_ALL =
            new HashSet<>(Arrays.asList(GroupAccessibility.values()));
    private static final Set<GroupAccessibility> ALLOWED_GROUP_ACCESSIBILITY_PUBLIC =
            Collections.singleton(GroupAccessibility.PUBLIC);
    private static final GroupGeoAreaIds EMPTY_GROUP_GEO_AREA_IDS =
            new GroupGeoAreaIds("", Collections.emptySet(), Collections.emptySet());

    private static final Pattern SHORT_NAME_PATTERN = Pattern.compile("\\b([a-zA-Z])");

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private GroupGeoAreaMappingRepository groupGeoAreaMappingRepository;
    @Autowired
    private GroupMembershipRepository groupMembershipRepository;
    @Autowired
    private IAppService appService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IRoleService roleService;

    @Override
    public Group findGroupById(String groupId) throws GroupNotFoundException {
        return groupRepository.findByIdAndDeletedFalse(groupId)
                .orElseThrow(() -> new GroupNotFoundException(groupId));
    }

    @Override
    public Group findGroupByIdIncludingDeleted(String groupId) throws GroupNotFoundException {
        return groupRepository.findById(groupId)
                .orElseThrow(() -> new GroupNotFoundException(groupId));
    }

    @Override
    public Page<Group> findAllNotDeletedGroups(Pageable pageable) {
        return groupRepository.findByDeletedFalseOrderByNameAsc(pageable);
    }

    @Override
    public List<Group> findAllGroupsCreatedByPerson(Person person) {
        return groupRepository.findByCreatorOrderByCreatedDesc(person);
    }

    @Override
    public Page<Group> findAllNotDeletedGroupsBelongingToTenant(Set<String> tenantIds, Pageable pageable) {
        if (CollectionUtils.isEmpty(tenantIds)) {
            return Page.empty();
        }
        return groupRepository.findByTenantIdInAndDeletedFalseOrderByNameAsc(tenantIds, pageable);
    }

    @Override
    public List<Group> findAllAvailableGroupsFilteredByGroupSettingsAndFeatureConfig(Person caller,
            AppVariant callingAppVariant) {

        Set<String> selectedGeoAreaIds =
                BaseEntity.toInSafeIdSet(appService.getSelectedGeoAreaIds(callingAppVariant, caller));
        final Set<String> availableGeoAreaIds = appService.getAvailableGeoAreas(callingAppVariant).stream()
                .map(GeoArea::getId)
                .collect(Collectors.toSet());
        String homeAreaId = getHomeAreaId(caller);

        Set<GroupAccessibility> allowedAccessibility;
        if (featureService.isFeatureEnabled(GroupAccessibilityApprovalRequiredFeature.class,
                FeatureTarget.of(caller, callingAppVariant))) {
            allowedAccessibility = ALLOWED_GROUP_ACCESSIBILITY_ALL;
        } else {
            allowedAccessibility = ALLOWED_GROUP_ACCESSIBILITY_PUBLIC;
        }

        final Set<Group> memberGroups = groupRepository.findAllByMembership(caller);

        final List<GroupMetaData> groupsAndMetadata =
                groupRepository.findAllAvailableGroupsAndMetadata(selectedGeoAreaIds);
        final Set<Group> visibleGroups = groupsAndMetadata.stream()
                .collect(Collectors.groupingBy(GroupMetaData::getGroup))
                .entrySet().stream()
                .filter(e -> allowedAccessibility.contains(e.getKey().getAccessibility()))
                .filter(e -> isGroupVisible(e.getKey(), e.getValue(), homeAreaId, selectedGeoAreaIds,
                        availableGeoAreaIds))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
        visibleGroups.addAll(memberGroups);
        return visibleGroups.stream()
                .sorted(Comparator.comparingLong(Group::getCreated))
                .collect(Collectors.toList());
    }

    @Override
    public Group findGroupByIdFilteredByGroupSettings(String groupId, Person caller, AppVariant callingAppVariant)
            throws GroupNotFoundException {

        //check if it is a group with membership, then it can be directly returned
        Group memberGroup = groupRepository.findByIdAndMembership(groupId, caller);
        if (memberGroup != null) {
            return memberGroup;
        }

        Set<String> selectedGeoAreaIds = appService.getSelectedGeoAreaIds(callingAppVariant, caller);
        final Set<String> availableGeoAreaIds = appService.getAvailableGeoAreas(callingAppVariant).stream()
                .map(GeoArea::getId)
                .collect(Collectors.toSet());
        String homeAreaId = getHomeAreaId(caller);
        final List<GroupMetaData> groupAndMetadata = groupRepository.findGroupAndMetadata(groupId);
        final Optional<Group> groupOptional = groupAndMetadata.stream()
                .collect(Collectors.groupingBy(GroupMetaData::getGroup))
                .entrySet().stream()
                .filter(e -> isGroupVisible(e.getKey(), e.getValue(), homeAreaId, selectedGeoAreaIds,
                        availableGeoAreaIds))
                .map(Map.Entry::getKey)
                //there is only one
                .findFirst();
        return groupOptional.orElseThrow(() -> new GroupNotFoundException(groupId));
    }

    private boolean isGroupVisible(Group group, List<GroupMetaData> groupMetaData, String homeAreaId,
            Set<String> selectedGeoAreaIds, Set<String> availableGeoAreaIds) {

        if (group.isDeleted()) {
            return false;
        }
        //exclusion is relative to the available geo areas
        if (groupMetaData.stream()
                .filter(GroupMetaData::isGeoAreaExcluded)
                .map(GroupMetaData::getGeoAreaId)
                .anyMatch(availableGeoAreaIds::contains)) {
            return false;
        }
        switch (group.getVisibility()) {
            case ANYONE:
                //if it is visible for anyone it still has to be in the selected geo areas
                return groupMetaData.stream()
                        .filter(gm -> !gm.isGeoAreaExcluded())
                        .map(GroupMetaData::getGeoAreaId)
                        .anyMatch(selectedGeoAreaIds::contains);
            case SAME_HOME_AREA:
                //if it is visible for the same home area the caller has to have the same home area
                return groupMetaData.stream()
                        .filter(gm -> !gm.isGeoAreaExcluded())
                        .map(GroupMetaData::getGeoAreaId)
                        .anyMatch(homeAreaId::equals);
            default:
                return false; //member case is already checked before
        }
    }

    private String getHomeAreaId(Person caller) {
        String homeAreaId;
        if (caller.getHomeArea() == null) {
            homeAreaId = "xxx"; //this is just an id that will not map to anything
        } else {
            homeAreaId = caller.getHomeArea().getId();
        }
        return homeAreaId;
    }

    @Override
    public Map<String, GroupGeoAreaIds> findGeoAreaIdsByGroups(Collection<Group> groups) {
        Set<Group> groupSet = new HashSet<>(groups);
        final Map<String, GroupGeoAreaIds> groupGeoAreaIdsByGroupIds =
                groupGeoAreaMappingRepository.findAllGeoAreaIdsByGroupIds(Group.toInSafeEntitySet(groupSet)).stream()
                        .collect(Collectors.groupingBy(GroupIdMetaData::getGroupId,
                                Collectors.collectingAndThen(Collectors.toList(), this::toGroupGeoAreaIds)));
        groups.stream()
                .map(Group::getId)
                .forEach(groupId -> groupGeoAreaIdsByGroupIds.putIfAbsent(groupId, EMPTY_GROUP_GEO_AREA_IDS));
        return groupGeoAreaIdsByGroupIds;
    }

    private GroupGeoAreaIds toGroupGeoAreaIds(List<GroupIdMetaData> mappings) {

        final Map<Boolean, Set<String>> geoAreaIdsByExcluded = mappings.stream()
                .collect(Collectors.partitioningBy(GroupIdMetaData::isGeoAreaExcluded,
                        Collectors.mapping(GroupIdMetaData::getGeoAreaId, Collectors.toSet())));
        final String mainGeoAreaId = mappings.stream()
                .filter(GroupIdMetaData::isMainGeoArea)
                .map(GroupIdMetaData::getGeoAreaId)
                .sorted()
                .findFirst()
                .orElse(null);
        return GroupGeoAreaIds.builder()
                .mainGeoAreaId(mainGeoAreaId)
                .includedGeoAreaIds(geoAreaIdsByExcluded.get(false))
                .excludedGeoAreaIds(geoAreaIdsByExcluded.get(true))
                .build();
    }

    @Override
    public Map<String, GroupGeoAreas> findGeoAreasByGroups(Collection<Group> groups) {

        return findGeoAreaIdsByGroups(groups).entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> groupGeoAreaIdsToGroupGeoAreas(e.getValue())));
    }

    @Override
    public GroupGeoAreaIds findGeoAreaIdsForGroup(Group group) {

        return findGeoAreaIdsByGroups(Collections.singleton(group)).values().iterator().next();
    }

    @Override
    public GroupGeoAreas findGeoAreasForGroup(Group group) {
        final GroupGeoAreaIds geoAreaIdsForGroup = findGeoAreaIdsForGroup(group);
        return groupGeoAreaIdsToGroupGeoAreas(geoAreaIdsForGroup);
    }

    private GroupGeoAreas groupGeoAreaIdsToGroupGeoAreas(GroupGeoAreaIds geoAreaIdsForGroup) {
        if (StringUtils.isNotEmpty(geoAreaIdsForGroup.getMainGeoAreaId())) {
            return GroupGeoAreas.builder()
                    .mainGeoArea(geoAreaService.findGeoAreaById(geoAreaIdsForGroup.getMainGeoAreaId()))
                    .includedGeoAreas(geoAreaService.findAllById(geoAreaIdsForGroup.getIncludedGeoAreaIds()))
                    .excludedGeoAreas(geoAreaService.findAllById(geoAreaIdsForGroup.getExcludedGeoAreaIds()))
                    .build();
        } else {
            return GroupGeoAreas.builder()
                    .includedGeoAreas(geoAreaService.findAllById(geoAreaIdsForGroup.getIncludedGeoAreaIds()))
                    .excludedGeoAreas(geoAreaService.findAllById(geoAreaIdsForGroup.getExcludedGeoAreaIds()))
                    .build();
        }
    }

    @Override
    public Set<String> findAllMemberGroupIds(Person caller) {
        return groupMembershipRepository.findAllMemberGroupIds(caller, GroupMembershipStatus.APPROVED);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public GroupMembership joinGroupByAdmin(Group group, Person personToJoin, String memberIntroductionText) {

        Optional<GroupMembership> existingMembershipOpt =
                groupMembershipRepository.findByGroupAndMember(group, personToJoin);
        GroupMembership newOrUpdatedMembership;

        newOrUpdatedMembership = existingMembershipOpt.orElseGet(() -> GroupMembership.builder()
                .group(group)
                .member(personToJoin)
                .memberIntroductionText(memberIntroductionText)
                .build());
        // When requested by admin, the GroupMembershipStatus will always be set to APPROVED
        newOrUpdatedMembership.setStatus(GroupMembershipStatus.APPROVED);
        return updateMembership(newOrUpdatedMembership);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public GroupMembership joinGroupOrRequestToJoin(Group group, Person personToJoin, String memberIntroductionText) {

        Optional<GroupMembership> existingMembershipOpt =
                groupMembershipRepository.findByGroupAndMember(group, personToJoin);
        GroupMembership newOrUpdatedMembership;

        if (existingMembershipOpt.isPresent()) {
            GroupMembership existingMembership = existingMembershipOpt.get();
            switch (existingMembership.getStatus()) {
                case PENDING:
                    throw new GroupMembershipAlreadyPendingException(group.getId(), personToJoin.getId());
                case APPROVED:
                    //nothing to do, just return the same membership
                    break;
                case REJECTED:
                    //nothing to do so far, just return the same membership
                    //maybe the person can request membership again in the future
                    break;
                case NOT_MEMBER:
                    //this actually should never happen, since memberships for non-members get deleted
                    log.warn("Group membership {} with status NOT_MEMBER found, reusing it.",
                            existingMembership.getId());
                    //if it happens, we just re-use the membership
                    updateMembershipStatusAfterJoin(group, existingMembership);
                    break;
            }
            newOrUpdatedMembership = existingMembership;
        } else {
            newOrUpdatedMembership = GroupMembership.builder()
                    .group(group)
                    .member(personToJoin)
                    .memberIntroductionText(memberIntroductionText)
                    .build();
            updateMembershipStatusAfterJoin(group, newOrUpdatedMembership);
        }
        return updateMembership(newOrUpdatedMembership);
    }

    private GroupMembership updateMembership(GroupMembership newOrUpdatedMembership) {

        final Group group = newOrUpdatedMembership.getGroup();
        final Person personToJoin = newOrUpdatedMembership.getMember();

        GroupMembership updatedMembership = saveAndRetryOnDataIntegrityViolation(
                () -> groupMembershipRepository.saveAndFlush(newOrUpdatedMembership),
                () -> groupMembershipRepository.findByGroupAndMember(group, personToJoin).orElseThrow(
                        () -> new ConcurrentRequestException("Concurrent request failed, could not join group"))
        );

        final Group updatedGroup = updateGroupMemberCount(group);
        updatedMembership.setGroup(updatedGroup);
        return updatedMembership;
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Group addGroupMembershipAdminToGroup(Group group, Person adminToAdd) {

        if (!isGroupMember(group, adminToAdd)) {
            final GroupMembership groupMembership = joinGroupByAdmin(group, adminToAdd, "Added by admin");
            group = groupMembership.getGroup();
        }
        roleService.assignRoleToPerson(adminToAdd, GroupMembershipAdmin.class, group);
        return group;
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Group removeGroupMembershipAdminFromGroup(Group group, Person adminToRemove) {

        checkIfGroupMembershipAdminRemovalIsAllowed(group, adminToRemove);
        final Optional<RoleAssignment> roleAssignmentOpt =
                roleService.getCurrentRoleAssignmentForRoleAndEntity(adminToRemove, GroupMembershipAdmin.class, group);
        roleAssignmentOpt.ifPresent(roleAssignment -> roleService.removeRoleAssignment(roleAssignment));
        return group;
    }

    private void checkIfGroupMembershipAdminRemovalIsAllowed(Group group, Person adminToRemove) {

        final List<Person> groupMembershipAdmins =
                roleService.getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class, group);
        final long countOtherGroupMembershipAdmins = groupMembershipAdmins.stream()
                .filter(p -> !p.getId().equals(adminToRemove.getId()))
                .count();
        if (countOtherGroupMembershipAdmins == 0) {
            throw GroupMembershipAdminRemovalNotPossibleException.forRemovalNotPossible(group);
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Pair<GroupMembership, Boolean> acceptPendingGroupJoinRequest(Group group, Person personToAccept) {

        Optional<GroupMembership> existingMembershipOpt =
                groupMembershipRepository.findByGroupAndMember(group, personToAccept);
        if (existingMembershipOpt.isPresent()) {
            GroupMembership groupMembership = existingMembershipOpt.get();
            boolean groupMembershipStatusChanged = false;
            //we expect the groupMembershipStatus to always be pending here
            //to avoid unwanted states, we also handle all other possible values
            switch (groupMembership.getStatus()) {
                case PENDING:
                    //normal case, accept it
                    groupMembership.setStatus(GroupMembershipStatus.APPROVED);
                    groupMembershipStatusChanged = true;
                    break;
                case APPROVED:
                    //stays approved
                    break;
                case REJECTED:
                    //stays rejected
                    break;
                case NOT_MEMBER:
                    //this actually should never happen, since memberships for non-members get deleted
                    log.warn("Group membership {} with status NOT_MEMBER found", groupMembership.getId());
                    throw new NotMemberOfGroupException(group.getId(), personToAccept.getId());
            }
            GroupMembership updatedMembership = groupMembership;
            if (groupMembershipStatusChanged) {
                updatedMembership = saveAndRetryOnDataIntegrityViolation(
                        () -> groupMembershipRepository.saveAndFlush(groupMembership),
                        () -> groupMembershipRepository.findByGroupAndMember(group, personToAccept).orElseThrow(
                                () -> new ConcurrentRequestException(
                                        "Concurrent request failed, could not accept pending group join request"))
                );
                Group updatedGroup = updateGroupMemberCount(group);
                updatedMembership.setGroup(updatedGroup);
            }
            return Pair.of(updatedMembership, groupMembershipStatusChanged);
        } else {
            throw new NotMemberOfGroupException(group.getId(), personToAccept.getId());
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Pair<GroupMembership, Boolean> rejectPendingGroupJoinRequest(Group group, Person personToDeny) {

        Optional<GroupMembership> existingMembershipOpt =
                groupMembershipRepository.findByGroupAndMember(group, personToDeny);
        if (existingMembershipOpt.isPresent()) {
            GroupMembership groupMembership = existingMembershipOpt.get();
            boolean groupMembershipStatusChanged = false;
            //we expect the groupMembershipStatus to always be pending here
            //to avoid unwanted states, we also handle all other possible values
            switch (groupMembership.getStatus()) {
                case PENDING:
                    //normal case, deny it
                    groupMembership.setStatus(GroupMembershipStatus.REJECTED);
                    groupMembershipStatusChanged = true;
                    break;
                case APPROVED:
                    //should not happen, for now staying accepted
                    break;
                case REJECTED:
                    //stays rejected
                    break;
                case NOT_MEMBER:
                    //this actually should never happen, since memberships for non-members get deleted
                    log.warn("Group membership {} with status NOT_MEMBER found", groupMembership.getId());
                    throw new NotMemberOfGroupException(group.getId(), personToDeny.getId());
            }
            GroupMembership updatedMembership = groupMembership;
            if (groupMembershipStatusChanged) {
                updatedMembership = saveAndRetryOnDataIntegrityViolation(
                        () -> groupMembershipRepository.saveAndFlush(groupMembership),
                        () -> groupMembershipRepository.findByGroupAndMember(group, personToDeny).orElseThrow(
                                () -> new ConcurrentRequestException(
                                        "Concurrent request failed, could not deny pending group join request"))
                );
            }
            return Pair.of(updatedMembership, groupMembershipStatusChanged);
        } else {
            throw new NotMemberOfGroupException(group.getId(), personToDeny.getId());
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Group updateGroupMemberCount(Group group) {
        group.setMemberCount(groupMembershipRepository.countByGroupAndStatus(group, GroupMembershipStatus.APPROVED));
        return saveAndRetryOnDataIntegrityViolation(
                () -> groupRepository.saveAndFlush(group),
                () -> {
                    log.warn("Concurrent request failed, could not update group membership count");
                    return groupRepository.findById(group.getId())
                            .orElseThrow(() -> new ConcurrentRequestException("Failed to get original group {}",
                                    group.getId()));
                });
    }

    private void updateMembershipStatusAfterJoin(Group group, GroupMembership newOrUpdatedMembership) {
        switch (group.getAccessibility()) {
            case PUBLIC:
                newOrUpdatedMembership.setStatus(GroupMembershipStatus.APPROVED);
                break;
            case APPROVAL_REQUIRED:
                newOrUpdatedMembership.setStatus(GroupMembershipStatus.PENDING);
                break;
            default:
                newOrUpdatedMembership.setStatus(GroupMembershipStatus.REJECTED);
                log.error("Unknown accessibility setting {}, rejecting membership", group.getAccessibility());
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Group leaveGroup(Group group, Person personToLeave) {

        Optional<GroupMembership> existingMembershipOpt =
                groupMembershipRepository.findByGroupAndMember(group, personToLeave);
        //we might do checks in the future to see if this is the last group admin
        if (existingMembershipOpt.isPresent()) {
            GroupMembership existingMembership = existingMembershipOpt.get();
            switch (existingMembership.getStatus()) {
                case PENDING:
                    //currently we just delete it, needs to be re-thought with private groups
                    break;
                case APPROVED:
                    //normal case, just delete it
                    break;
                case REJECTED:
                    //rejected stays rejected
                    throw new NotMemberOfGroupException(group.getId(), personToLeave.getId());
                case NOT_MEMBER:
                    //this actually should never happen, since memberships for non-members get deleted
                    log.warn("Group membership {} with status NOT_MEMBER found", existingMembership.getId());
                    throw new NotMemberOfGroupException(group.getId(), personToLeave.getId());
            }
            groupMembershipRepository.delete(existingMembership);
        } else {
            throw new NotMemberOfGroupException(group.getId(), personToLeave.getId());
        }
        return updateGroupMemberCount(group);
    }

    @Override
    public List<Person> findAllApprovedMembers(Group group) {
        return groupMembershipRepository.findAllMembersByGroupAndMembershipStatus(group,
                GroupMembershipStatus.APPROVED);
    }

    @Override
    public Page<Person> findAllApprovedMembers(Group group, Pageable pageable) {
        return groupMembershipRepository.findAllMembersByGroupAndMembershipStatusPaged(group,
                GroupMembershipStatus.APPROVED, pageable);
    }

    @Override
    public Map<Group, GroupMembershipStatus> findGroupMembershipStatusByGroup(Collection<Group> groups, Person person) {
        return groupMembershipRepository.findAllByGroupInAndMemberOrderByCreatedDesc(groups, person).stream()
                .collect(Collectors.toMap(GroupMembership::getGroup, GroupMembership::getStatus));
    }

    @Override
    public GroupMembershipStatus findGroupMembershipStatusForGroup(Group group, Person person) {
        return groupMembershipRepository.findByGroupAndMember(group, person)
                .map(GroupMembership::getStatus)
                .orElse(GroupMembershipStatus.NOT_MEMBER);
    }

    @Override
    public void checkIsGroupContentVisible(Group group, Person caller, AppVariant callingAppVariant)
            throws GroupContentNotAccessibleException {

        if (group.isDeleted()) {
            //deleted groups are never accessible
            throw new GroupNotFoundException(group.getId());
        }
        //group membership overrules every setting
        if (isGroupMember(group, caller)) {
            return;
        }
        switch (group.getContentVisibility()) {
            case ANYONE:
                final GroupGeoAreaIds geoAreaIdsForGroup = findGeoAreaIdsForGroup(group);
                Set<String> selectedGeoAreaIds = appService.getSelectedGeoAreaIds(callingAppVariant, caller);
                if (!Collections.disjoint(selectedGeoAreaIds, geoAreaIdsForGroup.getIncludedGeoAreaIds())
                        &&
                        Collections.disjoint(appService.getAvailableGeoAreaIds(callingAppVariant),
                                geoAreaIdsForGroup.getExcludedGeoAreaIds())
                ) {
                    return;
                }
                break;
            case SAME_HOME_AREA:
                final GroupGeoAreaIds geoAreaIdsForGroupSame = findGeoAreaIdsForGroup(group);
                final String homeAreaId = getHomeAreaId(caller);
                if (geoAreaIdsForGroupSame.getIncludedGeoAreaIds().contains(homeAreaId)
                        &&
                        Collections.disjoint(appService.getAvailableGeoAreaIds(callingAppVariant),
                                geoAreaIdsForGroupSame.getExcludedGeoAreaIds())
                ) {
                    return;
                }
                break;
            case MEMBERS:
                break;
        }
        throw new GroupContentNotAccessibleException(group.getId());
    }

    @Override
    public void checkIsGroupMember(Group group, Person person) throws NotMemberOfGroupException {
        if (!isGroupMember(group, person)) {
            throw new NotMemberOfGroupException(group.getId(), person.getId());
        }
    }

    @Override
    public void checkGroupMembershipAdmin(Person person, Group group) {
        if (!roleService.hasRoleForEntity(person, GroupMembershipAdmin.class, group)) {
            throw new NotAuthorizedException(
                    "This action is only allowed for person with role GroupMembershipAdmin for group {}.",
                    group.getId());
        }
    }

    @Override
    public void checkIfGroupCanBeDeleted(Group group, boolean forceDeleteNonEmptyGroup)
            throws GroupCannotBeDeletedException {
        if (forceDeleteNonEmptyGroup) {
            return;
        }
        if (group.getMemberCount() > 3) {
            throw GroupCannotBeDeletedException.forTooManyMembers(group.getId(), group.getMemberCount());
        }
        if (postRepository.countAllByGroup(group) > 0) {
            throw GroupCannotBeDeletedException.forContainingPosts(group.getId());
        }
    }

    @Override
    public boolean isGroupMember(Group group, Person person) {

        return group.isNotDeleted() && groupMembershipRepository
                .existsByGroupAndMemberAndStatus(group, person, GroupMembershipStatus.APPROVED);
    }

    @Override
    public Page<Post> findAllPostsInGroup(Group group, @Nullable final PostType type, @Nullable final Long rawStartTime,
            @Nullable final Long rawEndTime, final PagedQuery pagedQuery) {

        final long startTime = (rawStartTime == null) ? pagedQuery.sortCriteria.getDefaultStart() : rawStartTime;
        final long endTime = (rawEndTime == null) ? pagedQuery.sortCriteria.getDefaultEnd() : rawEndTime;

        switch (pagedQuery.getSortCriteria()) {
            case LAST_ACTIVITY:
                return (type == null)
                        ? postRepository.findAllByGroupAndLastActivityBetween(group, startTime, endTime,
                        pagedQuery.toPageRequest())
                        : postRepository.findAllByGroupAndTypeAndLastActivityBetween(group, type, startTime, endTime,
                        pagedQuery.toPageRequest());
            case CREATED:
                return (type == null)
                        ? postRepository.findAllByGroupAndCreatedBetween(group, startTime, endTime,
                        pagedQuery.toPageRequest())
                        : postRepository.findAllByGroupAndTypeAndCreatedBetween(group, type, startTime, endTime,
                        pagedQuery.toPageRequest());
            default:
                throw new InvalidSortingCriterionException();
        }
    }

    @Override
    public Page<Post> findAllPostsInMemberGroups(Person person, @Nullable final PostType type,
            @Nullable final Long rawStartTime, @Nullable final Long rawEndTime,
            final PagedQuery pagedQuery) {

        final Set<String> memberGroupIds = findAllMemberGroupIds(person);
        if (CollectionUtils.isEmpty(memberGroupIds)) {
            return Page.empty();
        }
        
        final long startTime = (rawStartTime == null) ? pagedQuery.sortCriteria.getDefaultStart() : rawStartTime;
        final long endTime = (rawEndTime == null) ? pagedQuery.sortCriteria.getDefaultEnd() : rawEndTime;

        switch (pagedQuery.getSortCriteria()) {
            case LAST_ACTIVITY:
                return (type == null)
                        ? postRepository.findAllByGroupIdsAndLastActivityBetween(memberGroupIds, startTime, endTime,
                        pagedQuery.toPageRequest())
                        : postRepository.findAllByGroupIdsAndTypeAndLastActivityBetween(memberGroupIds, type, startTime,
                        endTime,
                        pagedQuery.toPageRequest());
            case CREATED:
                return (type == null)
                        ? postRepository.findAllByGroupIdsAndCreatedBetween(memberGroupIds, startTime, endTime,
                        pagedQuery.toPageRequest())
                        : postRepository.findAllByGroupIdsAndTypeAndCreatedBetween(memberGroupIds, type, startTime,
                        endTime,
                        pagedQuery.toPageRequest());
            default:
                throw new InvalidSortingCriterionException();
        }
    }

    @Override
    public Page<GroupMembership> findAllGroupMembershipsByGroup(Group group, Pageable pageable) {
        return groupMembershipRepository.findAllByGroup(group, pageable);
    }

    @Override
    public List<GroupMembership> findAllGroupMembershipsByPerson(Person person) {
        return groupMembershipRepository.findAllByMemberOrderByCreatedDesc(person);
    }

    @Override
    public void checkGeoAreaConsistency(@Nullable GeoArea mainGeoArea, Set<GeoArea> includedGeoAreas,
            Set<GeoArea> excludedGeoAreas) throws GroupGeoAreasInvalidException {
        if (!CollectionUtils.isEmpty(excludedGeoAreas) && excludedGeoAreas.stream()
                .anyMatch(includedGeoAreas::contains)) {
            throw GroupGeoAreasInvalidException.forExcludedGeoAreaInIncluded(includedGeoAreas, excludedGeoAreas);
        }
        if (mainGeoArea != null && !includedGeoAreas.contains(mainGeoArea)) {
            throw GroupGeoAreasInvalidException.forMainGeoAreaNotInIncluded(mainGeoArea, includedGeoAreas);
        }
    }

    @Override
    @Transactional
    public GroupGeoAreas setGroupGeoAreaMappings(Group group,
            @Nullable GeoArea mainGeoArea,
            Set<GeoArea> includedGeoAreas,
            Set<GeoArea> excludedGeoAreas) throws GroupGeoAreasInvalidException {

        checkGeoAreaConsistency(mainGeoArea, includedGeoAreas, excludedGeoAreas);

        //the mappings get the same id for group and geo areas
        final Set<GroupGeoAreaMapping> newOrUpdatedMappings = Stream.concat(
                        includedGeoAreas.stream()
                                .map(geoArea -> GroupGeoAreaMapping.builder()
                                        .geoArea(geoArea)
                                        .group(group)
                                        .excluded(false)
                                        .mainGeoArea(geoArea.equals(mainGeoArea))
                                        .build()
                                        .withConstantId()),
                        excludedGeoAreas.stream()
                                .map(geoArea -> GroupGeoAreaMapping.builder()
                                        .geoArea(geoArea)
                                        .group(group)
                                        .excluded(true)
                                        .build()
                                        .withConstantId()))
                .collect(Collectors.toSet());
        final Set<String> newOrUpdateMappingIds = newOrUpdatedMappings.stream()
                .map(GroupGeoAreaMapping::getId)
                .collect(Collectors.toSet());

        groupGeoAreaMappingRepository.deleteByGroupAndIdNotIn(group, BaseEntity.toInSafeIdSet(newOrUpdateMappingIds));
        groupGeoAreaMappingRepository.saveAll(newOrUpdatedMappings);
        groupGeoAreaMappingRepository.flush();
        return new GroupGeoAreas(mainGeoArea, includedGeoAreas, excludedGeoAreas);
    }

    @Override
    public long getPostCountOfGroup(Group group) {
        return postRepository.countAllByGroup(group);
    }

    @Override
    public Long findNewestPostCreatedTime(Group group) {
        return postRepository.findNewestPostCreatedTime(group);
    }

    @Override
    public Long findNewestCommentCreatedTime(Group group) {
        return postRepository.findNewestCommentCreatedTime(group);
    }

    @Override
    public String generateShortName(String groupName) {
        String firstLetter = "";
        String secondLetter = "";
        if (StringUtils.isNotBlank(groupName)) {
            Matcher matcher = SHORT_NAME_PATTERN.matcher(groupName);
            if (matcher.find()) {
                firstLetter = StringUtils.defaultString(matcher.group(1)).toUpperCase();
                if (matcher.find()) {
                    secondLetter = StringUtils.defaultString(matcher.group(1)).toUpperCase();
                }
            }
        }
        return firstLetter + secondLetter;
    }

    @Override
    public @Nullable GeoArea guessMainGeoArea(Collection<GeoArea> includedGeoAreas) {
        if (CollectionUtils.isEmpty(includedGeoAreas)) {
            return null;
        }
        return includedGeoAreas.stream()
                .min(Comparator.comparing(GeoArea::getName))
                .orElse(null);
    }

    @Override
    public Group deleteGroup(Group group) {

        groupGeoAreaMappingRepository.deleteByGroup(group);
        groupMembershipRepository.deleteByGroup(group);
        roleService.deleteAllRoleAssignmentsForRoleAndEntity(GroupMembershipAdmin.class, group);
        group.setDeleted(true);
        group.setDeletionTime(timeService.currentTimeMillisUTC());
        group.setMemberCount(0L);

        return store(group);
    }

}
