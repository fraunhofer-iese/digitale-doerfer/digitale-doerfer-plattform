/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class GrapevineSearchReindexWorker implements IWorkerTask {

    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private ITimeService timeService;

    @Override
    public Trigger getTaskTrigger() {
        return new CronTrigger("11 11 * * * *", timeService.getLocalTimeZone());//every hour at xx:11:11
    }

    @Override
    public void run() {
        final long start = System.currentTimeMillis();
        log.info("re-indexing failed search entries");
        final long reIndexedEntries = grapevineSearchService.reIndexFailedEntries();
        log.info("re-indexed {} failed search entries finished in {} ms", reIndexedEntries,
                System.currentTimeMillis() - start);
    }

}
