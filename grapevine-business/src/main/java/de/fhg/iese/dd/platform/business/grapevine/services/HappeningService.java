/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.HappeningParticipant;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningParticipantRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.HappeningRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class HappeningService extends BasePostService<Happening> implements IHappeningService {

    @Autowired
    private HappeningRepository happeningRepository;
    @Autowired
    private HappeningParticipantRepository happeningParticipantRepository;

    @Override
    public Happening findById(String happeningId) throws PostNotFoundException {
        return happeningRepository.findByIdAndDeletedFalse(happeningId)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(happeningId));
    }

    @Override
    public Page<Person> findAllParticipantsByHappening(Happening happening, Person caller, Pageable pageable) {

        final int hiddenStatusBitMask = caller.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT) ?
                0 : PersonStatus.PARALLEL_WORLD_INHABITANT.getBitMaskValue();
        return happeningParticipantRepository.findAllParticipantsByHappening(happening, hiddenStatusBitMask,
                pageable);
    }

    @Override
    public List<HappeningParticipant> findAllHappeningParticipationsByPerson(Person person) {
        return happeningParticipantRepository.findAllByPerson(person);
    }

    @Override
    public void setParticipated(Person person, Collection<Post> posts) {

        Set<Happening> happenings = posts.stream()
                .filter(p -> p instanceof Happening)
                .map(p -> (Happening) p)
                .collect(Collectors.toSet());
        // only set participated when happenings exist
        if (!happenings.isEmpty()) {
            Set<String> participatingHappeningIds =
                    happeningParticipantRepository.findAllHappeningIdByPersonAndHappeningIn(person, happenings);
            happenings.forEach(h -> h.setParticipated(participatingHappeningIds.contains(h.getId())));
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Happening updateParticipationCount(Happening happening) {
        final long participantCount = happeningParticipantRepository.countAllByHappening(happening);
        happening.setParticipantCount(participantCount);
        return useCachedEntities(happeningRepository.saveAndFlush(happening));
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Happening confirmParticipation(Happening happening, Person person) {

        happening.setParticipated(true);
        if (!happeningParticipantRepository.existsByHappeningAndPerson(happening, person)) {
            saveAndIgnoreIntegrityViolation(
                    () -> happeningParticipantRepository.saveAndFlush(new HappeningParticipant(happening, person)),
                    () -> null
            );
            Happening updatedHappening = updateParticipationCount(happening);
            updatedHappening.setParticipated(true);
            return updatedHappening;
        }
        return useCachedEntities(happening);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Happening revokeParticipation(Happening happening, Person person) {

        final Optional<HappeningParticipant> happeningParticipant =
                happeningParticipantRepository.findByHappeningAndPerson(happening, person);

        happening.setParticipated(false);
        if (happeningParticipant.isPresent()) {
            saveAndIgnoreIntegrityViolation(
                    () -> {
                        happeningParticipantRepository.delete(happeningParticipant.get());
                        return null;
                    },
                    () -> null
            );
            Happening updatedHappening = updateParticipationCount(happening);
            updatedHappening.setParticipated(false);
            return updatedHappening;
        }
        return useCachedEntities(happening);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void deleteAllHappeningParticipations(Happening happening) {
        happeningParticipantRepository.deleteAllByHappening(happening);
        updateParticipationCount(happening);
    }

}
