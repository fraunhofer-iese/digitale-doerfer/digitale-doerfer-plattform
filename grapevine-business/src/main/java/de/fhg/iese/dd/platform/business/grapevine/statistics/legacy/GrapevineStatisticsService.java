/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Adeline Silva Schäfer, Ala Harirchi, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics.legacy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.GrapevineGeoAreaInfo;
import de.fhg.iese.dd.platform.business.grapevine.template.GrapevineTemplate;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.files.services.ITeamFileStorageService;
import de.fhg.iese.dd.platform.business.shared.statistics.services.IStatisticsService;
import de.fhg.iese.dd.platform.business.shared.template.services.ITemplateService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.PersonRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.StatisticsConfig;
import freemarker.template.TemplateException;

@Service
class GrapevineStatisticsService extends BaseService implements IGrapevineStatisticsService {

    private static final ZonedDateTime STATISTICS_BEGINNING =
            ZonedDateTime.parse("2018-01-01T00:00:00+01:00[Europe/Berlin]");

    private static final long END_EQUALS_NOW_PRECISION = TimeUnit.MINUTES.toMillis(10);

    private String teamFileStorageFolderWeekly;
    private String teamFileStorageFolderDaily;
    private String teamFileStorageFolderMonthly;
    private String teamFileStorageFolderLongTerm;
    private String teamFileStorageFolderTemporary;

    @Autowired
    private StatisticsConfig statisticsConfig;
    @Autowired
    private ITemplateService templateService;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private ITeamFileStorageService teamFileStorageService;
    @Autowired
    private IStatisticsService statisticsService;

    @PostConstruct
    private void initialize() {
        teamFileStorageFolderWeekly =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolderWeekly(), "/");
        teamFileStorageFolderDaily =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolderDaily(), "/");
        teamFileStorageFolderMonthly =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolderMonthly(), "/");
        teamFileStorageFolderLongTerm =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolderLongterm(), "/");
        teamFileStorageFolderTemporary =
                StringUtils.appendIfMissing(statisticsConfig.getTeamFileStorageFolderTemporary(), "/");
    }

    @Override
    public void createStatisticsReportsFixedTime() {

        teamFileStorageService.createDirectory(teamFileStorageFolderLongTerm);
        createReportBeginningToPreviousMonthEnd();
        createReportCurrentYearStartToPreviousMonthEnd();
        teamFileStorageService.createDirectory(teamFileStorageFolderMonthly);
        createReportPreviousMonth();
        teamFileStorageService.createDirectory(teamFileStorageFolderWeekly);
        createReportPreviousWeek();
        teamFileStorageService.createDirectory(teamFileStorageFolderDaily);
        createReportPreviousDay();
    }

    @Override
    public void createStatisticsReportsTemporary() {
        //the temporary files are always created freshly
        //since this method is expected to be called every night the reports there are always until yesterday
        teamFileStorageService.delete(teamFileStorageFolderTemporary);
        teamFileStorageService.createDirectory(teamFileStorageFolderTemporary);
        createReportBeginningToToYesterdayEnd();
        createReportCurrentYearStartToYesterdayEnd();
        createReportCurrentMonthStartToYesterdayEnd();
        createReportCurrentWeekStartToYesterdayEnd();
    }

    private void createReportBeginningToPreviousMonthEnd() {
        ZonedDateTime previousMonthEnd = timeService.getPreviousMonthEnd(timeService.nowLocal());
        String reportFileName =
                teamFileStorageFolderLongTerm + reportNameMonthToMonth(STATISTICS_BEGINNING, previousMonthEnd) +
                        ".html";
        createAndSaveHtmlReport(reportFileName, STATISTICS_BEGINNING, previousMonthEnd);
    }

    private void createReportCurrentYearStartToPreviousMonthEnd() {
        ZonedDateTime previousMonthEnd = timeService.getPreviousMonthEnd(timeService.nowLocal());
        //this needs to be done relative to the previous month end, so that we do not get wrong dates on year change
        ZonedDateTime currentYearStart = timeService.getSameYearStart(previousMonthEnd);
        String reportFileName =
                teamFileStorageFolderLongTerm + reportNameMonthToMonth(currentYearStart, previousMonthEnd) + ".html";
        createAndSaveHtmlReport(reportFileName, currentYearStart, previousMonthEnd);
    }

    private void createReportPreviousMonth() {
        ZonedDateTime previousMonthStart = timeService.getPreviousMonthStart(timeService.nowLocal());
        ZonedDateTime previousMonthEnd = timeService.getPreviousMonthEnd(timeService.nowLocal());
        String statisticsName = statisticsService.getReportName("report", -1, ChronoUnit.MONTHS);
        String reportFileName = teamFileStorageFolderMonthly + statisticsName + ".html";
        createAndSaveHtmlReport(reportFileName, previousMonthStart, previousMonthEnd);
    }

    private void createReportPreviousWeek() {
        ZonedDateTime previousWeekStart = timeService.getPreviousWeekStart(timeService.nowLocal());
        ZonedDateTime previousWeekEnd = timeService.getPreviousWeekEnd(timeService.nowLocal());
        String reportFileName = teamFileStorageFolderWeekly +
                statisticsService.getReportName("report", -1, ChronoUnit.WEEKS) + ".html";
        createAndSaveHtmlReport(reportFileName, previousWeekStart, previousWeekEnd);
    }

    private void createReportPreviousDay() {
        ZonedDateTime previousDayStart = timeService.getPreviousDayStart(timeService.nowLocal());
        ZonedDateTime previousDayEnd = timeService.getPreviousDayEnd(timeService.nowLocal());
        String reportFileName = teamFileStorageFolderDaily +
                statisticsService.getReportName("report", -1, ChronoUnit.DAYS) + ".html";
        createAndSaveHtmlReport(reportFileName, previousDayStart, previousDayEnd);
    }

    private void createReportCurrentMonthStartToYesterdayEnd() {
        ZonedDateTime currentMonthStart = timeService.getSameMonthStart(timeService.nowLocal());
        ZonedDateTime yesterdayEnd = timeService.getPreviousDayEnd(timeService.nowLocal());
        String reportFileName =
                teamFileStorageFolderTemporary + reportNameMonthToDay(currentMonthStart, yesterdayEnd) + ".html";
        createAndSaveHtmlReport(reportFileName, currentMonthStart, yesterdayEnd);
    }

    private void createReportCurrentWeekStartToYesterdayEnd() {
        ZonedDateTime currentWeekStart = timeService.getSameWeekStart(timeService.nowLocal());
        ZonedDateTime yesterdayEnd = timeService.getPreviousDayEnd(timeService.nowLocal());
        String reportFileName =
                teamFileStorageFolderTemporary + reportNameWeekToDay(currentWeekStart, yesterdayEnd) + ".html";
        createAndSaveHtmlReport(reportFileName, currentWeekStart, yesterdayEnd);
    }

    private void createReportCurrentYearStartToYesterdayEnd() {
        ZonedDateTime currentYearStart = timeService.getSameYearStart(timeService.nowLocal());
        ZonedDateTime yesterdayEnd = timeService.getPreviousDayEnd(timeService.nowLocal());
        String reportFileName =
                teamFileStorageFolderTemporary + reportNameMonthToDay(currentYearStart, yesterdayEnd) + ".html";
        createAndSaveHtmlReport(reportFileName, currentYearStart, yesterdayEnd);
    }

    private void createReportBeginningToToYesterdayEnd() {
        ZonedDateTime yesterdayEnd = timeService.getPreviousDayEnd(timeService.nowLocal());
        String reportFileName =
                teamFileStorageFolderTemporary + reportNameMonthToDay(STATISTICS_BEGINNING, yesterdayEnd) + ".html";
        createAndSaveHtmlReport(reportFileName, STATISTICS_BEGINNING, yesterdayEnd);
    }

    private void createAndSaveHtmlReport(String reportFileName, ZonedDateTime start, ZonedDateTime end) {
        if (!teamFileStorageService.fileExists(reportFileName)) {
            try {
                long startMillis = start.toInstant().toEpochMilli();
                long endMillis = end.toInstant().toEpochMilli();
                GrapevineStats grapevineStats = getGrapevineStats(startMillis, endMillis);
                String report = getGrapevineStatsReport(startMillis, endMillis, grapevineStats);
                teamFileStorageService.saveFile(report.getBytes(StandardCharsets.UTF_8), "text/html", reportFileName);
            } catch (IOException | TemplateException e) {
                //we only log the error here, since we do not want to prevent the other reports from getting created
                log.error("Failed to create report", e);
            }
        }
    }

    private String reportNameMonthToMonth(ZonedDateTime start, ZonedDateTime end) {
        return String.format("report_%1$tY-%1$tm__%2$tY-%2$tm", start, end);
    }

    private String reportNameMonthToDay(ZonedDateTime start, ZonedDateTime end) {
        return String.format("report_%1$tY-%1$tm__%2$tY-%2$tm-%2$td", start, end);
    }

    private String reportNameWeekToDay(ZonedDateTime start, ZonedDateTime end) {

        return String.format("report_%1$04d-KW%2$02d__%3$tY-%3$tm-%3$td",
                //this is not just the year, since around year change this value differs!
                //https://en.wikipedia.org/wiki/ISO_week_date
                start.get(IsoFields.WEEK_BASED_YEAR),
                start.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR),
                end);
    }

    @Override
    public GrapevineStats getGrapevineStats(long start, long end) {

        List<Tenant> tenants = tenantService.findAllOrderedByNameAsc();
        List<String> tenantTags = tenantService.findAllTenantTags();

        List<TenantGroupStats> tenantGroupStats = tenantTags.stream()
                .map(t -> getTenantGroupStats(t, start, end))
                .collect(Collectors.toList());
        List<TenantStats> tenantStats = tenants.stream()
                .map(t -> getTenantStats(t, start, end))
                .collect(Collectors.toList());

        PostStats postStats = getPostStats(start, end);
        UserStats userStats = getUserStats(start, end);
        GroupStats groupStats = getGroupStats(start, end);

        return GrapevineStats.builder()
                .tenantGroupStats(tenantGroupStats)
                .tenantStats(tenantStats)
                .postStats(postStats)
                .userStats(userStats)
                .groupStats(groupStats)
                .build();
    }

    @Override
    public PostStats getPostStats(long start, long end) {

        long gossipCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.Gossip, start, end);
        long newsItemCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.NewsItem, start, end);
        long suggestionCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.Suggestion, start, end);
        long seekingCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.Seeking, start, end);
        long offerCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.Offer, start, end);
        long happeningCount =
                postRepository.countAllByPostTypeAndCreatedBetween(PostType.Happening, start, end);

        long postCount = gossipCount + newsItemCount + suggestionCount + seekingCount + offerCount + happeningCount;

        long commentCount = commentRepository.countAllByCreatedBetweenAndDeletedFalse(start, end);

        return PostStats.builder()
                .gossipCount(gossipCount)
                .newsItemCount(newsItemCount)
                .suggestionCount(suggestionCount)
                .seekingCount(seekingCount)
                .offerCount(offerCount)
                .happeningCount(happeningCount)
                .postCount(postCount)
                .commentCount(commentCount)
                .build();
    }

    @Override
    public PostStats getPostStatsPerTenantGroup(String tag, long start, long end) {

        long gossipCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.Gossip, start, end);
        long newsItemCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.NewsItem, start, end);
        long suggestionCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.Suggestion, start, end);
        long seekingCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.Seeking, start, end);
        long offerCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.Offer, start, end);
        long happeningCount = postRepository.countAllByTenantTagAndPostTypeAndCreatedBetween(tag,
                PostType.Happening, start, end);

        long postCount = gossipCount + newsItemCount + suggestionCount + seekingCount + offerCount + happeningCount;

        long commentCount = commentRepository.countAllByPostTenantTagAndCreatedBetweenAndDeletedFalse(tag,
                start, end);

        return PostStats.builder()
                .gossipCount(gossipCount)
                .newsItemCount(newsItemCount)
                .suggestionCount(suggestionCount)
                .seekingCount(seekingCount)
                .offerCount(offerCount)
                .postCount(postCount)
                .happeningCount(happeningCount)
                .commentCount(commentCount)
                .build();
    }

    @Override
    public PostStats getPostStatsPerTenant(Tenant tenant, long start, long end) {

        long gossipCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.Gossip, start, end);
        long newsItemCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.NewsItem, start, end);
        long suggestionCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.Suggestion, start, end);
        long seekingCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.Seeking, start, end);
        long offerCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.Offer, start, end);
        long happeningCount = postRepository.countAllByTenantAndPostTypeAndCreatedBetween(tenant,
                PostType.Happening, start, end);

        long postCount = gossipCount + newsItemCount + suggestionCount + seekingCount + offerCount + happeningCount;

        long commentCount = commentRepository.countAllByPostTenantAndCreatedBetweenAndDeletedFalse(tenant,
                start, end);

        return PostStats.builder()
                .gossipCount(gossipCount)
                .newsItemCount(newsItemCount)
                .suggestionCount(suggestionCount)
                .seekingCount(seekingCount)
                .offerCount(offerCount)
                .postCount(postCount)
                .happeningCount(happeningCount)
                .commentCount(commentCount)
                .build();
    }

    @Override
    public UserStats getUserStats(long start, long end) {

        long createdCount = personRepository.countAllByCreatedBetween(start, end);
        long deletedCount = personRepository.countAllByDeletionTimeBetweenAndDeletedTrue(start, end);
        long activeCount = personRepository.countAllByLastLoggedInGreaterThan(start);

        return UserStats.builder()
                .createdUsers(createdCount)
                .deletedUsers(deletedCount)
                .activeUsersSinceReportStart(activeCount)
                .build();
    }

    @Override
    public UserStats getUserStatsPerTenantGroup(String tag, long start, long end) {

        long createdCount = personRepository.countAllByCreatedBetweenAndCommunityTag(start, end, tag);
        long deletedCount =
                personRepository.countAllByDeletionTimeBetweenAndDeletedTrueAndCommunityTag(start, end, tag);
        long activeCount = personRepository.countAllByLastLoggedInGreaterThanAndCommunityTag(start, tag);

        return UserStats.builder()
                .createdUsers(createdCount)
                .deletedUsers(deletedCount)
                .activeUsersSinceReportStart(activeCount)
                .build();
    }

    @Override
    public UserStats getUserStatsPerTenant(Tenant tenant, long start, long end) {

        long createdCount = personRepository.countAllByCreatedBetweenAndCommunity(start, end, tenant);
        long deletedCount =
                personRepository.countAllByDeletionTimeBetweenAndDeletedTrueAndCommunity(start, end, tenant);
        long activeCount = personRepository.countAllByLastLoggedInGreaterThanAndCommunity(start, tenant);

        return UserStats.builder()
                .createdUsers(createdCount)
                .deletedUsers(deletedCount)
                .activeUsersSinceReportStart(activeCount)
                .build();
    }

    @Override
    public GroupStats getGroupStats(long start, long end) {

        long gossipInGroupCount =
                postRepository.countAllByPostTypeAndGroupIsNotNullAndCreatedBetween(
                        PostType.Gossip, start, end);
        long publicGroupCount =
                groupRepository.countAllByAccessibilityAndCreatedBetween(GroupAccessibility.PUBLIC, start, end);
        long publicGroupMemberCount =
                groupRepository.countGroupMembersByAccessibilityAndJoinedBetween(GroupAccessibility.PUBLIC, start,
                        end).orElse(0L);
        long privateGroupCount =
                groupRepository.countAllByAccessibilityAndCreatedBetween(GroupAccessibility.APPROVAL_REQUIRED, start,
                        end);
        long privateGroupMemberCount =
                groupRepository.countGroupMembersByAccessibilityAndJoinedBetween(GroupAccessibility.APPROVAL_REQUIRED,
                        start, end).orElse(0L);

        return GroupStats.builder()
                .gossipInGroupCount(gossipInGroupCount)
                .publicGroupCount(publicGroupCount)
                .publicGroupMemberCount(publicGroupMemberCount)
                .privateGroupCount(privateGroupCount)
                .privateGroupMemberCount(privateGroupMemberCount)
                .build();
    }

    @Override
    public GroupStats getGroupStatsPerTenantGroup(String tag, long start, long end) {

        long gossipInGroupCount =
                postRepository.countAllByTenantTagAndPostTypeAndGroupIsNotNullAndCreatedBetween(
                        tag, PostType.Gossip, start, end);
        long publicGroupCount =
                groupRepository.countAllByTenantTagAndAccessibilityAndCreatedBetween(tag, GroupAccessibility.PUBLIC,
                        start, end);
        long publicGroupMemberCount =
                groupRepository.countGroupMembersByTenantTagAndAccessibilityAndJoinedBetween(tag,
                        GroupAccessibility.PUBLIC, start, end).orElse(0L);
        long privateGroupCount =
                groupRepository.countAllByTenantTagAndAccessibilityAndCreatedBetween(tag,
                        GroupAccessibility.APPROVAL_REQUIRED, start, end);
        long privateGroupMemberCount =
                groupRepository.countGroupMembersByTenantTagAndAccessibilityAndJoinedBetween(tag,
                        GroupAccessibility.APPROVAL_REQUIRED, start, end).orElse(0L);

        return GroupStats.builder()
                .gossipInGroupCount(gossipInGroupCount)
                .publicGroupCount(publicGroupCount)
                .publicGroupMemberCount(publicGroupMemberCount)
                .privateGroupCount(privateGroupCount)
                .privateGroupMemberCount(privateGroupMemberCount)
                .build();
    }

    @Override
    public GroupStats getGroupStatsPerTenant(Tenant tenant, long start, long end) {

        long gossipInGroupCount =
                postRepository.countAllByTenantAndPostTypeAndGroupIsNotNullAndCreatedBetween(
                        tenant, PostType.Gossip, start, end);
        long publicGroupCount =
                groupRepository.countAllByTenantAndAccessibilityAndCreatedBetween(tenant, GroupAccessibility.PUBLIC,
                        start, end);
        long publicGroupMemberCount =
                groupRepository.countGroupMembersByTenantAndAccessibilityAndJoinedBetween(tenant,
                        GroupAccessibility.PUBLIC, start, end).orElse(0L);
        long privateGroupCount =
                groupRepository.countAllByTenantAndAccessibilityAndCreatedBetween(tenant,
                        GroupAccessibility.APPROVAL_REQUIRED, start, end);
        long privateGroupMemberCount =
                groupRepository.countGroupMembersByTenantAndAccessibilityAndJoinedBetween(tenant,
                        GroupAccessibility.APPROVAL_REQUIRED, start, end).orElse(0L);

        return GroupStats.builder()
                .gossipInGroupCount(gossipInGroupCount)
                .publicGroupCount(publicGroupCount)
                .publicGroupMemberCount(publicGroupMemberCount)
                .privateGroupCount(privateGroupCount)
                .privateGroupMemberCount(privateGroupMemberCount)
                .build();
    }

    @Override
    public TenantGroupStats getTenantGroupStats(String tag, long start, long end) {

        PostStats postStats = getPostStatsPerTenantGroup(tag, start, end);
        UserStats userStats = getUserStatsPerTenantGroup(tag, start, end);
        GroupStats groupStats = getGroupStatsPerTenantGroup(tag, start, end);
        ActivityStats activityStats = getActivityStatsPerTenantGroup(tag, start, end);

        return TenantGroupStats.builder()
                .tenantGroupName(tag)
                .postStats(postStats)
                .userStats(userStats)
                .groupStats(groupStats)
                .activityStats(activityStats)
                .build();
    }

    @Override
    public TenantStats getTenantStats(Tenant tenant, long start, long end) {

        PostStats postStats = getPostStatsPerTenant(tenant, start, end);
        UserStats userStats = getUserStatsPerTenant(tenant, start, end);
        GroupStats groupStats = getGroupStatsPerTenant(tenant, start, end);
        ActivityStats activityStats = getActivityStatsPerTenant(tenant, start, end);

        return TenantStats.builder()
                .tenantName(tenant.getName())
                .postStats(postStats)
                .userStats(userStats)
                .groupStats(groupStats)
                .activityStats(activityStats)
                .build();
    }

    @Override
    public ActivityStats getActivityStatsPerTenantGroup(String tag, long start, long end) {

        Set<String> uniqueInternalPostCreatorIds =
                postRepository.distinctCreatorIdsByTenantTag(tag, start, end);

        Set<String> uniqueCommentCreatorIds = commentRepository.distinctCreatorIdsByTenantTag(tag, start, end);

        List<Long> activeUsersInternalPostCount =
                postRepository.activeUsersInternalPostCount(tag, start, end);

        List<Long> postCountByAmount = postRepository.postCountByAmount(tag, start, end);

        // Either post or comment
        Set<String> uniqueInternalPostOrCommentCreatorIds = new HashSet<>();
        uniqueInternalPostOrCommentCreatorIds.addAll(uniqueInternalPostCreatorIds);
        uniqueInternalPostOrCommentCreatorIds.addAll(uniqueCommentCreatorIds);

        // Sum all posts made by users (excludes news and happenings)
        long totalPosts = postCountByAmount.stream().mapToLong(Long::longValue).sum();
        long sumOfPostCounts = 0L;
        long usersWith80PercentPosts = 0L;
        float percentUsersWith80PercentPosts = 0f;

        if (totalPosts != 0) {
            for (int i = 0; (float) sumOfPostCounts / (float) totalPosts < 0.8; i++) {
                // <List>postCountByAmount is ordered by amount of made posts in decreasing order.
                // To get the percent-value, we add posts of users, until the sum exceeds 80% of total posts.
                sumOfPostCounts += postCountByAmount.get(i);
                usersWith80PercentPosts++;
            }
            percentUsersWith80PercentPosts = (float) usersWith80PercentPosts / (float) postCountByAmount.size() * 100;
        }

        return ActivityStats.builder()
                .tenantName(tag)
                .usersPosted(uniqueInternalPostCreatorIds.size())
                .usersCommented(uniqueCommentCreatorIds.size())
                .usersPostedOrCommented(uniqueInternalPostOrCommentCreatorIds.size())
                .activeUsers(activeUsersInternalPostCount.size())
                .percentUsersWith80PercentPosts(percentUsersWith80PercentPosts)
                .usersWith80PercentPosts(usersWith80PercentPosts)
                .build();
    }

    @Override
    public ActivityStats getActivityStatsPerTenant(Tenant tenant, long start, long end) {

        Set<String> uniqueInternalPostCreatorIds =
                postRepository.distinctCreatorIdsByTenant(tenant, start, end);

        Set<String> uniqueCommentCreatorIds = commentRepository.distinctCreatorIdsByTenant(tenant, start, end);

        List<Long> activeUsersInternalPostCount =
                postRepository.activeUsersInternalPostCount(tenant, start, end);

        List<Long> postCountByAmount = postRepository.postCountByAmount(tenant, start, end);

        // Either post or comment
        Set<String> uniqueInternalPostOrCommentCreatorIds = new HashSet<>();
        uniqueInternalPostOrCommentCreatorIds.addAll(uniqueInternalPostCreatorIds);
        uniqueInternalPostOrCommentCreatorIds.addAll(uniqueCommentCreatorIds);

        // Sum all posts made by users (excludes news and happenings)
        long totalPosts = postCountByAmount.stream().mapToLong(Long::longValue).sum();
        long sumOfPostCounts = 0L;
        long usersWith80PercentPosts = 0L;
        float percentUsersWith80PercentPosts = 0f;

        if (totalPosts != 0) {
            for (int i = 0; (float) sumOfPostCounts / (float) totalPosts < 0.8; i++) {
                // <List>postCountByAmount is ordered by amount of made posts in decreasing order.
                // To get the percent-value, we add posts of users, until the sum exceeds 80% of total posts.
                    sumOfPostCounts += postCountByAmount.get(i);
                    usersWith80PercentPosts++;
            }
            percentUsersWith80PercentPosts = (float) usersWith80PercentPosts / (float) postCountByAmount.size() * 100;
        }

        return ActivityStats.builder()
                .tenantName(tenant.getName())
                .usersPosted(uniqueInternalPostCreatorIds.size())
                .usersCommented(uniqueCommentCreatorIds.size())
                .usersPostedOrCommented(uniqueInternalPostOrCommentCreatorIds.size())
                .activeUsers(activeUsersInternalPostCount.size())
                .percentUsersWith80PercentPosts(percentUsersWith80PercentPosts)
                .usersWith80PercentPosts(usersWith80PercentPosts)
                .build();
    }

    @Override
    public String getGrapevineStatsReport(long start, long end, GrapevineStats grapevineStats)
            throws IOException, TemplateException {

        GrapevineStatsReport grapevineStatsReport = GrapevineStatsReport.builder()
                .startDate(timeService.toLocalTimeHumanReadable(start))
                .endDate(timeService.toLocalTimeHumanReadable(end))
                .nowDate(getNowDateOrNull(end))
                .grapevineStats(grapevineStats)
                .build();
        return templateService.createTextWithTemplateObjectModel(
                GrapevineTemplate.GRAPEVINE_STATISTICS_REPORT_HTML, grapevineStatsReport);
    }

    @Override
    public String getTenantGroupStatsReport(long start, long end, TenantGroupStats tenantGroupStats)
            throws IOException, TemplateException {

        TenantGroupStatsReport tenantGroupStatsReport = TenantGroupStatsReport.builder()
                .startDate(timeService.toLocalTimeHumanReadable(start))
                .endDate(timeService.toLocalTimeHumanReadable(end))
                .nowDate(getNowDateOrNull(end))
                .tenantGroupStats(tenantGroupStats)
                .build();

        return templateService.createTextWithTemplateObjectModel(
                GrapevineTemplate.TENANT_GROUP_STATISTICS_REPORT_HTML, tenantGroupStatsReport);
    }

    @Override
    public String getTenantStatsReport(long start, long end, TenantStats tenantStats)
            throws IOException, TemplateException {

        TenantStatsReport tenantStatsReport = TenantStatsReport.builder()
                .startDate(timeService.toLocalTimeHumanReadable(start))
                .endDate(timeService.toLocalTimeHumanReadable(end))
                .nowDate(getNowDateOrNull(end))
                .tenantStats(tenantStats)
                .build();

        return templateService.createTextWithTemplateObjectModel(
                GrapevineTemplate.TENANT_STATISTICS_REPORT_HTML, tenantStatsReport);
    }

    @Nullable
    private String getNowDateOrNull(long end) {
        long now = timeService.currentTimeMillisUTC();
        String nowDate;
        //this is only required for the active users count, since it can not be queried to a specific point in time
        if ((now - end) >= END_EQUALS_NOW_PRECISION) {
            nowDate = timeService.toLocalTimeHumanReadable(now);
        } else {
            nowDate = null;
        }
        return nowDate;
    }

    @Override
    public GrapevineGeoAreaInfo getGrapevineGeoAreaInfo(GeoArea geoArea) {

        long personCount = personRepository.countByHomeAreaAndDeletedFalse(geoArea);
        long postCount = postRepository.countAllByGeoAreaAndDeletedFalse(geoArea);
        long commentCount = commentRepository.countAllByPostGeoAreaAndDeletedFalse(geoArea);

        return GrapevineGeoAreaInfo.builder()
                .geoArea(geoArea)
                .personCount(personCount)
                .postCount(postCount)
                .commentCount(commentCount)
                .build();
    }

}
