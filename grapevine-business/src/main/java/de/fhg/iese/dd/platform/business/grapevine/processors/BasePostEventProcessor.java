/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.*;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostCouldNotBeCreatedException;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.PostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.CroppedImageReference;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IDocumentItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Marker;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.net.URL;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

abstract class BasePostEventProcessor extends BaseEventProcessor {

    @Autowired
    protected IMediaItemService mediaItemService;
    @Autowired
    protected IParallelismService parallelismService;
    @Autowired
    protected IDocumentItemService documentItemService;
    @Autowired
    protected IPostService postService;
    @Autowired
    protected IAddressService addressService;
    @Autowired
    private IFeatureService featureService;

    protected <T extends Post> PostCreateConfirmation<T> handleBasePostCreateRequest(BasePostCreateRequest request,
            T post) {

        Person creator = request.getCreator();
        Set<GeoArea> geoAreas = post.getGeoAreas();
        if (CollectionUtils.isEmpty(geoAreas)) {
            if (creator == null) {
                throw new PostCouldNotBeCreatedException(
                        "No geo areas provided and no creator to take the home area, so the post can not be created.");
            } else {
                GeoArea homeArea = creator.getHomeArea();
                if (homeArea == null) {
                    throw new PostCouldNotBeCreatedException(
                            "Creator does not have homeArea set, so the post can not be created.")
                            .withDetail("homeArea");
                }
                geoAreas = Set.of(homeArea);
            }
        }
        long currentTime = timeService.currentTimeMillisUTC();
        post.setCreated(currentTime);
        post.setLastModified(currentTime);
        post.setLastActivity(currentTime);
        if (post.getTenant() == null) {
            //groups have the tenant already set
            //this is not precise, but good enough to have a valid tenant
            Tenant tenant = geoAreas.stream()
                    .map(GeoArea::getTenant)
                    .filter(Objects::nonNull)
                    .findAny()
                    .orElse(null);
            post.setTenant(tenant);
        }
        post.setGeoAreas(geoAreas);
        post.setText(request.getText());
        post.setCreator(creator);
        post.setOrganization(request.getOrganization());
        // we "free" the temporary media items and add them to the new entity
        // if none are added we get an empty list for no cost
        List<MediaItem> newImages = mediaItemService.useAllItems(request.getTemporaryMediaItemsToAdd(),
                //owner can be null
                FileOwnership.of(creator, request.getAppVariant()));
        if (post.getImages() != null) {
            //if there are already images set we do not want to overwrite them
            post.getImages().addAll(newImages);
        } else {
            post.setImages(newImages);
        }
        Set<DocumentItem> newAttachments =
                documentItemService.useAllItemsUnordered(request.getTemporaryDocumentItemsToAdd(),
                        //owner can be null
                        FileOwnership.of(creator, request.getAppVariant()));
        if (post.getAttachments() != null) {
            //if there are already attachments set we do not want to overwrite them
            post.getAttachments().addAll(newAttachments);
        } else {
            post.setAttachments(newAttachments);
        }
        post.setCommentsAllowed(!request.isCommentsDisallowed());
        post.setCreatedLocation(request.getCreatedLocation());
        post.setHiddenForOtherHomeAreas(request.isHiddenForOtherHomeAreas());
        post.setCustomAddress(findOrCreateAddress(request.getCustomAddress()));
        post.setDeleted(false);
        post.setCustomAttributes(
                addFeatureDefinedCustomAttributes(post, request.getCustomAttributes(), request.getCreator(),
                request.getAppVariant()));
        if (creator != null) {
            boolean creatorIsParallelWorldInhabitant =
                    creator.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
            post.setParallelWorld(creatorIsParallelWorldInhabitant);
        }
        T storedPost = postService.storePost(post);
        return PostCreateConfirmation.<T>builder()
                .post(storedPost)
                .appVariant(request.getAppVariant())
                .published(true)
                .pushMessage(request.getPushMessage())
                .pushMessageLoud(request.getPushMessageLoud())
                .pushCategoryId(request.getPushCategoryId())
                .build();
    }

    protected <T extends Post> PostChangeConfirmation<T> changePost(BasePostChangeRequest<T> request, T post) {

        if (StringUtils.isNotEmpty(request.getText()) || request.isUseEmptyFields()) {
            post.setText(request.getText());
        }
        post.setOrganization(request.getOrganization());
        if (request.isRemoveCustomAddress()) {
            post.setCustomAddress(null);
        } else {
            post.setCustomAddress(request.getCustomAddress());
        }
        final List<MediaItem> oldImages = post.getImages();

        List<MediaItem> newImages;
        if (CollectionUtils.isEmpty(request.getImageHolders())) {
            if (request.isUseEmptyFields()) {
                newImages = Collections.emptyList();
            } else {
                newImages = oldImages;
            }
        } else {
            //use the temporary and existing images
            newImages = mediaItemService.useTemporaryAndExistingMediaItems(request.getImageHolders(),
                    FileOwnership.of(request.getCreator(), request.getAppVariant()));
        }
        final Runnable orphanMediaItemsDeletion = mediaItemService.getOrphanItemDeletion(oldImages, newImages);

        post.setImages(newImages);

        final Set<DocumentItem> oldAttachments = post.getAttachments();

        Set<DocumentItem> newAttachments;
        if (CollectionUtils.isEmpty(request.getDocumentHolders())) {
            if (request.isUseEmptyFields()) {
                newAttachments = Collections.emptySet();
            } else {
                newAttachments = oldAttachments;
            }
        } else {
            //use the temporary and existing documents
            newAttachments = documentItemService.useTemporaryAndExistingDocumentItems(request.getDocumentHolders(),
                    FileOwnership.of(request.getCreator(), request.getAppVariant()));
        }
        final Runnable orphanDocumentItemsDeletion =
                documentItemService.getOrphanItemDeletion(oldAttachments, newAttachments);

        post.setAttachments(newAttachments);
        post.setCustomAttributes(
                addFeatureDefinedCustomAttributes(post, request.getCustomAttributes(), request.getCreator(),
                request.getAppVariant()));

        Boolean commentsDisallowed = request.getCommentsDisallowed();
        if (commentsDisallowed != null) {
            post.setCommentsAllowed(!commentsDisallowed);
        }
        long now = timeService.currentTimeMillisUTC();
        post.setLastModified(now);
        if (request.isUpdateLastActivity()) {
            post.setLastActivity(now);
        }
        T storedPost = postService.storePost(post);

        parallelismService.getBlockableExecutor().submit(orphanMediaItemsDeletion);
        parallelismService.getBlockableExecutor().submit(orphanDocumentItemsDeletion);

        return PostChangeConfirmation.<T>builder()
                .post(storedPost)
                .appVariant(request.getAppVariant())
                .published(true)
                .pushMessage(request.getPushMessage())
                .pushMessageLoud(request.getPushMessageLoud())
                .pushCategoryId(request.getPushCategoryId())
                .build();
    }

    protected <T extends ExternalPost> PostCreateConfirmation<T> handleExternalPostCreateByNewsSourceRequest(
            BaseExternalPostCreateByNewsSourceRequest request, T post) {

        post.setGeoAreas(request.getGeoAreas());
        post.setExternalId(request.getExternalId());
        post.setNewsSource(request.getNewsSource());
        post.setAuthorName(StringUtils.defaultIfBlank(request.getAuthorName(), null));
        post.setCategories(request.getCategories());
        post.setUrl(request.getPostURL());
        post.setDesiredPublishTime(request.getDesiredPublishTime());
        post.setDesiredUnpublishTime(request.getDesiredUnpublishTime());
        post.setPublished(request.isPublishImmediately());
        post.setImages(getImages(request.getImageUploaded(), request.getImageURLs(),
                request.getCroppedImageReferences(), request.getAppVariant(), request.getLogMarker()));
        PostCreateConfirmation<T> postCreateConfirmation = handleBasePostCreateRequest(request, post);
        postCreateConfirmation.setPublished(request.isPublishImmediately());
        return postCreateConfirmation;
    }

    protected <T extends ExternalPost> List<BaseEvent> handleExternalPostChangeByNewsSourceRequest(
            BaseExternalPostChangeByNewsSourceRequest<T> request, T post) {

        boolean changedGeoAreaPublishedPost = false;
        Set<GeoArea> newGeoAreas = request.getGeoAreas();
        Set<GeoArea> oldGeoAreas = post.getGeoAreas();
        if (!CollectionUtils.isEmpty(newGeoAreas)) {
            if (post.isPublished() && !newGeoAreas.equals(oldGeoAreas)) {
                changedGeoAreaPublishedPost = true;
            }
            post.setGeoAreas(newGeoAreas);
        }
        post.setDeleted(false);
        post.setDeletionTime(null);
        post.setAuthorName(StringUtils.defaultIfBlank(request.getAuthorName(), null));
        post.setCategories(request.getCategories());
        post.setUrl(request.getPostURL());
        post.setDesiredPublishTime(request.getDesiredPublishTime());
        post.setDesiredUnpublishTime(request.getDesiredUnpublishTime());
        post.setLastModified(timeService.currentTimeMillisUTC());
        List<MediaItem> oldImages = post.getImages();
        List<MediaItem> newImages = getImages(request.getImageUploaded(), request.getImageURLs(),
                request.getCroppedImageReferences(), request.getAppVariant(), request.getLogMarker());
        post.setImages(newImages);
        final Runnable orphanMediaItemsDeletion = mediaItemService.getOrphanItemDeletion(oldImages, newImages);

        boolean changedToPublished = false;

        if (!post.isPublished() && request.isPublishImmediately()) {
            post.setPublished(true);
            changedToPublished = true;
        }

        PostChangeConfirmation<T> postChangeConfirmation = changePost(request, post);
        parallelismService.getBlockableExecutor().submit(orphanMediaItemsDeletion);

        if (changedGeoAreaPublishedPost) {
            //the post is already published, but the geo area changed, so it needs to be unpublished in the previous geo area
            //to avoid multiple pushes the change confirmation is set to not published
            postChangeConfirmation.setPublished(false);
            Set<GeoArea> geoAreasToUnpublish = oldGeoAreas.stream()
                    .filter(o -> !newGeoAreas.contains(o))
                    .collect(Collectors.toSet());
            List<BaseEvent> events = new ArrayList<>(3);
            events.add(postChangeConfirmation);
            //and published in the new geo area
            events.add(ExternalPostPublishConfirmation.builder()
                    .externalPost(postChangeConfirmation.getPost())
                    .pushMessage(postChangeConfirmation.getPushMessage())
                    .pushMessageLoud(postChangeConfirmation.getPushMessageLoud())
                    .pushCategoryId(postChangeConfirmation.getPushCategoryId())
                    .build());
            if (!geoAreasToUnpublish.isEmpty()) {
                events.add(new ExternalPostUnpublishConfirmation(post, geoAreasToUnpublish));
            }
            return events;
        } else {
            if (changedToPublished) {
                //in this case the post existed before and got published in the same geo area
                //to avoid multiple pushes the change confirmation is set to not published
                postChangeConfirmation.setPublished(false);
                return List.of(postChangeConfirmation,
                        new ExternalPostPublishConfirmation(postChangeConfirmation.getPost()));
            } else {
                postChangeConfirmation.setPublished(post.isPublished());
                return List.of(postChangeConfirmation);
            }
        }
    }

    private List<MediaItem> getImages(MediaItem imageUploaded, List<URL> imageURLs,
                                      List<CroppedImageReference> croppedImageReferences, AppVariant appVariant,
                                      Marker logMarker) {

        List<MediaItem> newImages = new ArrayList<>();
        if (imageUploaded != null) {
            //the request supports one directly uploaded image
            newImages.add(imageUploaded);
        }
        if (!CollectionUtils.isEmpty(imageURLs)) {
            //these images are referenced by URL
            Collection<MediaItem> downloadedMediaItems = downloadMediaItems(imageURLs, appVariant, logMarker);
            newImages.addAll(downloadedMediaItems);
        }
        if (!CollectionUtils.isEmpty(croppedImageReferences)) {
            //these images are referenced by URL and also have crop information
            Collection<MediaItem> downloadedMediaItems = downloadCroppedMediaItems(croppedImageReferences, appVariant,
                    logMarker);
            newImages.addAll(downloadedMediaItems);
        }
        return newImages;
    }

    private Collection<MediaItem> downloadMediaItems(List<URL> imageURLs, AppVariant appVariant, Marker logMarker) {

        return parallelizeAndHandleExceptions(imageURLs, appVariant, mediaItemService::createMediaItem, logMarker);
    }

    private Collection<MediaItem> downloadCroppedMediaItems(List<CroppedImageReference> images, AppVariant appVariant, Marker logMarker) {

        return parallelizeAndHandleExceptions(images, appVariant, mediaItemService::createMediaItem, logMarker);
    }

    @Nullable
    private Map<String, Object> addFeatureDefinedCustomAttributes(Post post, Map<String, Object> requestAttributes, Person caller, AppVariant appVariant) {

        Class<? extends PostTypeFeature<Post>> postTypeFeature = PostTypeFeature.getSpecificPostTypeFeature(post);
        Map<String, Object> featureDefinedAttributes = featureService.getFeatureOptional(postTypeFeature,
                        FeatureTarget.of(caller, appVariant))
                .map(PostTypeFeature::getAdditionalCustomAttributes)
                .orElse(Collections.emptyMap());
        if (CollectionUtils.isEmpty(requestAttributes)) {
            if (featureDefinedAttributes.isEmpty()) {
                return null;
            } else {
                return new HashMap<>(featureDefinedAttributes);
            }
        } else {
            Map<String, Object> customAttributes;
            customAttributes = new HashMap<>(requestAttributes);
            customAttributes.putAll(featureDefinedAttributes);
            return customAttributes;
        }
    }

    @SneakyThrows
    private <T> Collection<MediaItem> parallelizeAndHandleExceptions(List<T> images, AppVariant appVariant,
                                                                     BiFunction<T, FileOwnership, MediaItem> mediaItemFunction,
                                                                     Marker logMarker) {

        IParallelismService.ComputationOutcomes<MediaItem> mediaItemsAndExceptions = parallelismService.submitToBlockableExecutorAndWaitForComputationOutcome(
                images.stream()
                        .map(image -> (Callable<MediaItem>) (() -> mediaItemFunction.apply(image,
                                FileOwnership.of(appVariant))))
                        .collect(Collectors.toList()),
                Duration.ofMinutes(1));
        List<Throwable> exceptions = mediaItemsAndExceptions.exceptions();
        if (!CollectionUtils.isEmpty(exceptions)) {
            String exceptionMessages = exceptions.stream()
                    .map(Throwable::toString)
                    .collect(Collectors.joining("\n"));
            long exceptionTypes = exceptions.stream()
                    .map(Throwable::getClass)
                    .distinct()
                    .count();
            if (logMarker != null) {
                log.debug(logMarker, "Failed to download {} images, {} different exceptions:\n{}",
                        exceptions.size(), exceptionTypes, exceptionMessages);
            }
            Throwable exceptionToThrow = exceptions.iterator().next();
            for (int i = 1; i < exceptions.size(); i++) {
                Throwable suppressedException = exceptions.get(i);
                exceptionToThrow.addSuppressed(suppressedException);
            }
            throw exceptionToThrow;
        }
        return mediaItemsAndExceptions.results();
    }

    protected Address findOrCreateAddress(Address customAddress) {
        if (customAddress != null) {
            return addressService.findOrCreateAddress(
                    IAddressService.AddressDefinition.fromAddress(customAddress),
                    IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY_GPS,
                    IAddressService.GPSResolutionStrategy.PROVIDED_LOOKEDUP_NONE,
                    false);
        }
        return null;
    }

}
