/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.participants.person.init.OauthAccount;
import de.fhg.iese.dd.platform.business.participants.person.init.PersonBaseDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

/**
 * Creates additional demo persons and assigns grapevine-specific roles. This cannot be done in shared-business because
 * these roles are not known there.
 */
@Component
public class GrapevineDemoPersonDataInitializer extends PersonBaseDataInitializer {

    protected static final String DATA_JSON_SECTION_SUGGESTION_FIRST_RESPONDER = "suggestion_first_responder";
    protected static final String DATA_JSON_SECTION_SUGGESTION_WORKER = "suggestion_worker";

    @Override
    public String getTopic() {
        return TOPIC_DEMO;
    }

    @Override
    protected void createAdditionalPersons(Tenant tenant, Map<String, List<Person>> entities,
            List<OauthAccount> oauthAccounts, LogSummary logSummary) {

        List<Person> firstResponders = entities.get(DATA_JSON_SECTION_SUGGESTION_FIRST_RESPONDER);
        if (!CollectionUtils.isEmpty(firstResponders)) {
            for (Person firstResponder : firstResponders) {
                firstResponder = createPerson(tenant, firstResponder, oauthAccounts, logSummary);
                roleService.assignRoleToPerson(firstResponder, SuggestionFirstResponder.class, tenant);
                logSummary.info("Assigned {} role SUGGESTION_FIRST_RESPONDER for {}", firstResponder.toString(),
                        tenant.toString());
            }
        }

        List<Person> suggestionWorkers = entities.get(DATA_JSON_SECTION_SUGGESTION_WORKER);
        if (!CollectionUtils.isEmpty(suggestionWorkers)) {
            for (Person suggestionWorker : suggestionWorkers) {
                suggestionWorker = createPerson(tenant, suggestionWorker, oauthAccounts, logSummary);
                roleService.assignRoleToPerson(suggestionWorker, SuggestionWorker.class, tenant);
                logSummary.info("Assigned {} role SUGGESTION_WORKER for {}", suggestionWorker.toString(),
                        tenant.toString());
            }
        }
    }

}
