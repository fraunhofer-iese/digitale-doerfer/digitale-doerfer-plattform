/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;
import de.fhg.iese.dd.platform.datamanagement.shared.push.repos.PushCategoryUserSettingRepository;

@Component
public class GrapevinePushCategoryMigrationAdminTask extends BaseAdminTask {

    @Autowired
    private PushCategoryUserSettingRepository pushCategoryUserSettingRepository;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Override
    public String getName() {
        return "GrapevinePushCategoryMigration";
    }

    @Override
    public String getDescription() {
        return "Migrates the user setting for 'DORFFUNK_PUSH_CATEGORY_ID_POST_CREATED' to all of the newly created push categories.";
    }

    @Override
    public void execute(final LogSummary logSummary, final AdminTaskParameters parameters) {

        final PushCategory parentPushCategory =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_POST_CREATED);
        final List<PushCategoryUserSetting> pushCategoryUserSettings =
                pushCategoryUserSettingRepository.findAllByPushCategoryOrderByCreatedDesc(parentPushCategory);

        for (PushCategoryUserSetting pushCategoryUserSetting : pushCategoryUserSettings) {

            Person person = pushCategoryUserSetting.getPerson();
            try {
                if (parameters.isDryRun()) {
                    logSummary.info("Migration of push settings for person '{}' would be done", person.getId());
                } else {
                    //changing the settings of the parent category will set all the child categories to the same value
                    pushCategoryService.changePushCategoryUserSetting(parentPushCategory,
                            person,
                            pushCategoryUserSetting.getAppVariant(),
                            pushCategoryUserSetting.isLoudPushEnabled());
                    logSummary.info("Migrated push settings for person '{}'", person.getId());
                }
            } catch (Exception e) {
                logSummary.error("Error while migrating PushCategoryUserSetting '{}' for person '{}'", e,
                        pushCategoryUserSetting.getId(), person.getId());
            }
        }
    }

}
