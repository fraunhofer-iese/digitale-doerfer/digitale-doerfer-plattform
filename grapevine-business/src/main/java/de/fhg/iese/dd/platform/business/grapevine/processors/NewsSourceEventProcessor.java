/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.NewsSourceDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

@EventProcessor
class NewsSourceEventProcessor extends BasePostEventProcessor {

    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IAppService appService;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;

    @EventProcessing
    private NewsSourceCreateConfirmation handleNewsSourceCreateRequest(NewsSourceCreateRequest request) {

        URL siteUrl;
        String normalizedSiteUrl;
        try {
            siteUrl = new URL(request.getSiteUrl());
            normalizedSiteUrl = siteUrl.getProtocol() + "://" + siteUrl.getHost();
        } catch (MalformedURLException ex) {
            throw new BadRequestException("Site url '{}' is no valid URL", request.getSiteUrl());
        }

        ReferenceDataChangeLogEntry referenceDataChangeLogEntry =
                referenceDataChangeService.acquireReferenceDataChangeLock(request.getCreator(),
                        "Create NewsSource (" + normalizedSiteUrl + ") via AdminUi", false);
        try {
            AppVariant appVariant = AppVariant.builder()
                    .app(request.getApp())
                    .oauthClients(request.getOauthClient() != null ?
                            Collections.singleton(request.getOauthClient()) : null)
                    .callBackUrl(request.getCallbackUrl())
                    .externalBasicAuth(request.getExternalBasicAuth())
                    .externalApiKey(request.getExternalApiKey())
                    .build();
            appVariant = appService.createAppVariant(appVariant, siteUrl.getHost());

            NewsSource newsSource = NewsSource.builder()
                    .appVariant(appVariant)
                    .siteUrl(normalizedSiteUrl)
                    .siteName(request.getSiteName())
                    .build();

            newsSourceService.checkNewsSourceExists(newsSource.getSiteUrl());
            newsSource.setSiteLogo(
                    mediaItemService.useItem(request.getLogo(), FileOwnership.of(newsSource.getAppVariant())));
            newsSource.setApiManaged(true);
            newsSource = newsSourceService.store(newsSource);

            referenceDataChangeService.setDataChangeVersionInfo(referenceDataChangeLogEntry, request.getChangeNotes());
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, true);

            return new NewsSourceCreateConfirmation(newsSource);
        } catch (Exception ex) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, false);
            throw ex;
        }
    }

    @EventProcessing
    private NewsSourceDeleteConfirmation handleNewsSourceDeleteRequest(NewsSourceDeleteRequest request) {

        LogSummary logSummary = new LogSummary();
        NewsSource newsSourceToDelete = request.getNewsSource();
        AppVariant appVariant = newsSourceToDelete.getAppVariant();
        ReferenceDataChangeLogEntry referenceDataChangeLogEntry =
                referenceDataChangeService.acquireReferenceDataChangeLock(request.getDeleter(),
                        "Soft delete NewsSource [" + newsSourceToDelete.getId() + "](" + newsSourceToDelete.getSiteUrl() + ") via AdminUi",
                        false);
        try {
            logSummary.info("Deleting {}", newsSourceToDelete.toString());
            newsSourceToDelete.setDeleted(true);
            newsSourceToDelete.setDeletionTime(timeService.currentTimeMillisUTC());
            NewsSource deletedNewsSource = newsSourceService.store(newsSourceToDelete);

            if (appVariant != null) {
                appService.deleteAppVariant(appVariant);
                logSummary.info("Soft deleted app variant {} linked to news source", appVariant.getId());
            } else {
                logSummary.info("News Source has no app variant, nothing to delete");
            }
            logSummary.info("Deleted {}", newsSourceToDelete.toString());

            referenceDataChangeService.setDataChangeVersionInfo(referenceDataChangeLogEntry, request.getChangeNotes());
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, true);

            return NewsSourceDeleteConfirmation.builder()
                    .deletedNewsSource(deletedNewsSource)
                    .message(logSummary.toString())
                    .build();
        } catch (Exception ex) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, false);
            throw ex;
        }
    }

}
