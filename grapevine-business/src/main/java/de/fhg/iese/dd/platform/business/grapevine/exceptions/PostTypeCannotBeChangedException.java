/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;

public class PostTypeCannotBeChangedException extends BadRequestException {

    private static final long serialVersionUID = 7928776698127542052L;

    public PostTypeCannotBeChangedException(ExternalPost externalPost) {
        super("A post of type {} with id {} and external id {} already exists for this news source. " +
                        "Changing it to a different type is not possible.",
                externalPost.getPostType(), externalPost.getId(), externalPost.getExternalId());
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.POST_TYPE_CANNOT_BE_CHANGED;
    }

}
