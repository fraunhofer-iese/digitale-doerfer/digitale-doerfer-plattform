/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

@Component
public class ConvenienceDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        .referencedEntity(AppVariant.class)
                        .changedOrDeletedEntity(Convenience.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        long impactedMappings = convenienceGeoAreaMappingRepository.countByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} conveniences will move to new geo area", impactedMappings);
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        Set<ConvenienceGeoAreaMapping> mappingsToBeChanged =
                convenienceGeoAreaMappingRepository.findMappingsToGeoAreaAAndNotToGeoAreaB(geoAreaToBeDeleted,
                        geoAreaToBeUsedInstead);

        for (ConvenienceGeoAreaMapping convenienceGeoAreaMapping : mappingsToBeChanged) {
            convenienceGeoAreaMapping.setGeoArea(geoAreaToBeUsedInstead);
        }
        convenienceGeoAreaMappingRepository.saveAll(mappingsToBeChanged);
        int deletedMappings = convenienceGeoAreaMappingRepository.deleteByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} conveniences moved to new geo area, {} conveniences were already present",
                mappingsToBeChanged.size(), deletedMappings);
    }

}
