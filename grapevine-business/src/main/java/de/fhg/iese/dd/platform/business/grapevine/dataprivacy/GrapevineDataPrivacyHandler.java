/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.dataprivacy;

import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.handlers.BaseDataPrivacyHandler;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IDataPrivacyReport;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.IPersonDeletionReport;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GrapevineDataPrivacyHandler extends BaseDataPrivacyHandler {

    private static final Collection<Class<? extends BaseEntity>> REQUIRED_ENTITIES =
            unmodifiableList(Person.class, App.class, MediaItem.class, DocumentItem.class);

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES =
            unmodifiableList(Post.class, Comment.class);

    @Autowired
    private IPostService postService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IGrapevineDataDeletionService grapevineDataDeletionService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private ILikeCommentService likeCommentService;
    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IFeatureService featureService;

    //TODO remove when clients have implemented entity deletion
    @Autowired
    private EventBusAccessor eventBusAccessor;

    @PostConstruct
    private void initialize() {
        eventBusAccessor.setOwner(this);
    }
    //END remove

    @Override
    public Collection<Class<? extends BaseEntity>> getRequiredEntities() {
        return REQUIRED_ENTITIES;
    }

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public void collectUserData(Person person, IDataPrivacyReport dataPrivacyReport) {

        IDataPrivacyReport.IDataPrivacyReportSection section =
                dataPrivacyReport.newSection("DorfFunk", "Daten aus DorfFunk");

        collectOwnPosts(person, section);

        collectOwnLikePosts(person, section);

        collectOwnComments(person, section);

        collectOwnLikeComments(person, section);

        collectOwnPostViews(person, section);

        collectOwnHappeningParticipants(person, section);

        collectOwnGroups(person, section);

        collectOwnGroupMemberships(person, section);

        if (section.isEmpty()) {
            dataPrivacyReport.removeSection(section);
        }
    }

    private void collectOwnPosts(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<Post> ownPosts = postService.findAllByCreatorIncludingDeleted(person);
        if (!CollectionUtils.isEmpty(ownPosts)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Beiträge", "Eigene Beiträge");

            for (Post ownPost : ownPosts) {

                String postContent = ownPost.isDeleted()
                        ? "Gelöscht am " + timeService.toLocalTimeHumanReadable(ownPost.getLastModified())
                        : ownPost.getText();

                String geoAreaNames = ownPost.getGeoAreas().stream()
                        .map(GeoArea::getName)
                        .collect(Collectors.joining(", "));
                subSection.newContent(getPostName(ownPost),
                        "Erstellt am " + timeService.toLocalTimeHumanReadable(ownPost.getCreated()) +
                                " in " + geoAreaNames,
                        postContent);
                if (ownPost.getCreatedLocation() != null) {
                    subSection.newLocationContent("Erstellungsort ", null, ownPost.getCreatedLocation());
                }
                if (ownPost.getCustomAddress() != null) {
                    subSection.newContent("Angefügte Adresse ", ownPost.getCustomAddress().toHumanReadableString(true));
                }

                attachImages(subSection, ownPost.getImages());
                attachDocuments(subSection, ownPost.getAttachments());
            }
        }
    }

    private void collectOwnLikePosts(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<PostInteraction> postInteractions = postInteractionService.findAllByPersonOrderByCreatedAsc(person);
        if (!CollectionUtils.isEmpty(postInteractions)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Beiträge mit Like", "Beiträge, denen du ein Like gegeben hast");

            for (PostInteraction postInteraction : postInteractions) {
                subSection.newContent(postInteraction.isLiked() ? "Like" : "Like und später Unlike",
                        "Um " + timeService.toLocalTimeHumanReadable(postInteraction.getCreated()),
                        getPostReference(postInteraction.getPost()));
            }
        }
    }

    private void collectOwnComments(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<Comment> ownComments = commentService.findAllByCreatorOrderByCreatedAsc(person);
        if (!CollectionUtils.isEmpty(ownComments)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Kommentare", "Kommentare zu Beiträgen");

            for (Comment ownComment : ownComments) {
                String commentContent = ownComment.isDeleted()
                        ? "Gelöscht am " + timeService.toLocalTimeHumanReadable(ownComment.getLastModified())
                        : ownComment.getText();
                subSection.newContent("Kommentar",
                        "Erstellt am " + timeService.toLocalTimeHumanReadable(ownComment.getCreated()) + " zu " +
                                getPostReference(ownComment.getPost()),
                        commentContent);
                attachImages(subSection, ownComment.getImages());
                attachDocuments(subSection, ownComment.getAttachments());
            }
        }
    }

    private void collectOwnLikeComments(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<LikeComment> likeComments = likeCommentService.findAllByPersonOrderByCreatedAsc(person);
        if (!CollectionUtils.isEmpty(likeComments)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Kommentare mit Like", "Kommentare, denen du ein Like gegeben hast");

            for (LikeComment likeComment : likeComments) {
                subSection.newContent(likeComment.isLiked() ? "Like" : "Like und später Unlike",
                        "Um " + timeService.toLocalTimeHumanReadable(likeComment.getCreated()),
                        getCommentReference(likeComment.getComment()));
            }
        }
    }
    
    private void collectOwnPostViews(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<PostInteraction> viewedPosts = postInteractionService.findAllByPersonOrderByCreatedAsc(person);
        if (!CollectionUtils.isEmpty(viewedPosts)) {
            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Angesehene Beiträge",
                            "Beiträge, die du gesehen oder im Detail gelesen hast");

            for (PostInteraction postInteraction : viewedPosts) {
                if (postInteraction.getViewedOverviewTime() != null) {
                    subSection.newContent("Beitrag gesehen",
                            "Um " + timeService.toLocalTimeHumanReadable(postInteraction.getViewedOverviewTime()),
                            getPostReference(postInteraction.getPost()));
                }
                if (postInteraction.getViewedDetailTime() != null) {
                    subSection.newContent("Beitrag im Detail gelesen",
                            "Um " + timeService.toLocalTimeHumanReadable(postInteraction.getViewedDetailTime()),
                            getPostReference(postInteraction.getPost()));
                }
            }
        }
    }

    private void collectOwnHappeningParticipants(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final Collection<HappeningParticipant> happeningParticipants =
                happeningService.findAllHappeningParticipationsByPerson(person);
        if (!CollectionUtils.isEmpty(happeningParticipants)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Veranstaltungen", "Veranstaltungen, denen du zugesagt hast");

            for (HappeningParticipant happeningParticipant : happeningParticipants) {
                subSection.newContent(getPostName(happeningParticipant.getHappening()),
                        "Zugesagt am " + timeService.toLocalTimeHumanReadable(happeningParticipant.getCreated()),
                        getPostReference(happeningParticipant.getHappening()));
            }
        }
    }

    private void collectOwnGroups(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        List<Group> groups = groupService.findAllGroupsCreatedByPerson(person);
        if (!CollectionUtils.isEmpty(groups)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Angelegte Gruppen", "Gruppen, die du angelegt hast");

            for (Group group : groups) {
                subSection.newContent("Gruppe",
                        "Gruppe \"" + group.getName() + "\" (" + group.getShortName() + ") angelegt am " +
                                timeService.toLocalTimeHumanReadable(group.getCreated()) +
                                (group.isDeleted() ? " gelöscht am " +
                                        timeService.toLocalTimeHumanReadable(group.getDeletionTime()) : ""),
                        "Beschreibung: \"" + group.getDescription() + "\"");
                subSection.newImageContent("Logo", "Logo der Gruppe \"" + group.getName() + "\"", group.getLogo());
            }
        }
    }

    private void collectOwnGroupMemberships(Person person, IDataPrivacyReport.IDataPrivacyReportSection section) {

        final List<GroupMembership> groupMemberships = groupService.findAllGroupMembershipsByPerson(person);
        if (!CollectionUtils.isEmpty(groupMemberships)) {

            IDataPrivacyReport.IDataPrivacyReportSubSection subSection =
                    section.newSubSection("Gruppen", "Gruppen, in denen du Mitglied bist");

            for (GroupMembership groupMembership : groupMemberships) {
                Group group = groupMembership.getGroup();
                subSection.newContent("Gruppe",
                        "Mitglied in \"" + group.getName() + "\" seit " +
                                timeService.toLocalTimeHumanReadable(groupMembership.getCreated()),
                        // Content is not added if last parameter is "" or null
                        (StringUtils.isEmpty(groupMembership.getMemberIntroductionText())) ?
                                "- Kein Vorstellungstext -" : groupMembership.getMemberIntroductionText());
            }
        }
    }

    private String getCommentReference(Comment comment) {
        String commentTextTeaser =
                StringUtils.abbreviate(RegExUtils.replaceAll(comment.getText(), "[\r\n]+", " "), 40);
        return String.format("Kommentar \"%s\" von \"%s\" zu %s",
                commentTextTeaser,
                comment.getCreator().getFirstNameFirstCharLastDot(),
                getPostReference(comment.getPost()));
    }

    private String getPostReference(Post post) {

        String postTextTeaser =
                StringUtils.abbreviate(RegExUtils.replaceAll(post.getText(), "[\r\n]+", " "), 40);

        String postAuthor = getPostAuthor(post);
        return String.format("%s \"%s\" von \"%s\"",
                getPostName(post),
                postTextTeaser,
                postAuthor);
    }

    private String getPostAuthor(Post post) {
        Person creator = post.getCreator();
        if (creator != null) {
            return creator.getFirstNameFirstCharLastDot();
        } else {
            if (post instanceof ExternalPost externalPost) {
                NewsSource newsSource = externalPost.getNewsSource();
                if (newsSource != null) {
                    return newsSource.getSiteUrl();
                } else {
                    return externalPost.getAuthorName();
                }
            } else {
                return post.getId();
            }
        }
    }

    private String getPostName(Post post) {
        return post.accept(new Post.Visitor<>() {
            @Override
            public String whenGossip(Gossip gossip) {
                return getItemName(GossipPostTypeFeature.class, gossip, "Plausch");
            }

            @Override
            public String whenSuggestion(Suggestion suggestion) {
                return getItemName(SuggestionPostTypeFeature.class, suggestion, "\"Sag's uns\"-Beitrag") +
                        "(" +
                        (suggestion.getSuggestionCategory() != null
                                ? suggestion.getSuggestionCategory().getDisplayName()
                                : "keine Kategorie")
                        + ")";
            }

            @Override
            public String whenSeeking(Seeking seeking) {
                return getItemName(SeekingPostTypeFeature.class, seeking, "Gesuch") +
                        "(" +
                        (seeking.getTradingCategory() != null
                                ? seeking.getTradingCategory().getDisplayName()
                                : "keine Kategorie")
                        + ")";
            }

            @Override
            public String whenOffer(Offer offer) {
                return getItemName(OfferPostTypeFeature.class, offer, "Angebot") +
                        "(" +
                        (offer.getTradingCategory() != null
                                ? offer.getTradingCategory().getDisplayName()
                                : "keine Kategorie")
                        + ")";
            }

            @Override
            public String whenNewsItem(NewsItem newsItem) {
                return getItemName(NewsItemPostTypeFeature.class, newsItem, "News-Beitrag");
            }

            @Override
            public String whenHappening(Happening happening) {
                return getItemName(HappeningPostTypeFeature.class, happening, "Veranstaltung");
            }

            @Override
            public String whenSpecialPost(SpecialPost specialPost) {
                return getItemName(SpecialPostTypeFeature.class, specialPost, "Dorfhelfer-Beitrag");
            }
        });
    }

    private <P extends Post> String getItemName(Class<? extends PostTypeFeature<P>> feature, P post,
            String defaultName) {
        try {
            //post.tenant can be null, so the default config for the feature is taken
            //after adjusting the feature service we can use the dorffunk app here, instead of relying on a common feature
            PostTypeFeature<?> featureValue = featureService.getFeature(feature, FeatureTarget.of(post.getTenant()));
            if (StringUtils.isNotEmpty(featureValue.getItemName())) {
                return featureValue.getItemName();
            }
        } catch (FeatureNotFoundException e) {
            //nothing to do, the feature has not been configured properly, so we return the default name
        }
        return defaultName;
    }

    private void attachImages(IDataPrivacyReport.IDataPrivacyReportSubSection subSection, List<MediaItem> mediaItems) {
        for (MediaItem mediaItem : mediaItems) {
            subSection.newImageContent("Angehängtes Bild", mediaItem);
        }
    }

    private void attachDocuments(IDataPrivacyReport.IDataPrivacyReportSubSection subSection,
            Set<DocumentItem> attachments) {
        for (DocumentItem documentItem : attachments) {
            subSection.newDocumentContent("Angehängtes Dokument", null, documentItem);
        }
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void deleteUserData(Person person, IPersonDeletionReport personDeletionReport) {

        //we need to delete own postInteractions first, otherwise they would already be deleted and not be added to the data report
        final List<PostInteraction> postInteractions = postInteractionService.findAllByPersonOrderByCreatedAsc(person);
        for (PostInteraction postInteraction : postInteractions) {
            personDeletionReport.addEntry(postInteraction,
                    grapevineDataDeletionService.deletePostInteraction(postInteraction));
        }

        final List<HappeningParticipant> happeningParticipants =
                happeningService.findAllHappeningParticipationsByPerson(person);
        for (HappeningParticipant happeningParticipant : happeningParticipants) {
            personDeletionReport.addEntry(happeningParticipant,
                    grapevineDataDeletionService.deleteHappeningParticipant(happeningParticipant));
        }

        final List<Post> posts = postService.findAllByCreatorIncludingDeleted(person);
        for (Post post : posts) {
            final List<MediaItem> images = post.getImages();
            final Set<DocumentItem> documents = post.getAttachments();
            personDeletionReport.addEntry(post, grapevineDataDeletionService.deletePost(post));
            //will be deleted by orphan removal
            personDeletionReport.addEntries(images, DeleteOperation.DELETED);
            personDeletionReport.addEntries(documents, DeleteOperation.DELETED);

            //TODO remove when clients have implemented entity deletion
            eventBusAccessor.notifyTrigger(new PostDeleteConfirmation(post));
        }

        //we need to delete own likeComments first, otherwise they would already be deleted and not be added to the data report
        final List<LikeComment> likeComments = likeCommentService.findAllByPersonOrderByCreatedAsc(person);
        for (LikeComment likeComment : likeComments) {
            personDeletionReport.addEntry(likeComment, grapevineDataDeletionService.deleteLikeComment(likeComment));
        }

        final List<Comment> comments = commentService.findAllByCreatorOrderByCreatedAsc(person);
        for (Comment comment : comments) {
            final List<MediaItem> images = comment.getImages();
            final Set<DocumentItem> documents = comment.getAttachments();
            personDeletionReport.addEntry(comment, grapevineDataDeletionService.deleteComment(comment));
            //will be deleted by orphan removal
            personDeletionReport.addEntries(images, DeleteOperation.DELETED);
            personDeletionReport.addEntries(documents, DeleteOperation.DELETED);

            //TODO remove when clients have implemented entity deletion
            eventBusAccessor.notifyTrigger(new CommentDeleteConfirmation(comment));
        }

        final List<GroupMembership> groupMemberships = groupService.findAllGroupMembershipsByPerson(person);
        for (GroupMembership groupMembership : groupMemberships) {
            personDeletionReport.addEntry(groupMembership,
                    grapevineDataDeletionService.deleteGroupMembership(groupMembership));
        }

        List<Group> createdGroups = groupService.findAllGroupsCreatedByPerson(person);
        for (Group createdGroup : createdGroups) {
            personDeletionReport.addEntry(createdGroup, grapevineDataDeletionService.deleteGroup(createdGroup));
        }
    }

}
