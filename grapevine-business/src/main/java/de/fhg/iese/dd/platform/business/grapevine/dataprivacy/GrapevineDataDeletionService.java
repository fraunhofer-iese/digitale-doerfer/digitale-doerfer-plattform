/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Dominik Schnier, Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.dataprivacy;

import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.reports.DeleteOperation;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.BaseDataDeletionService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
class GrapevineDataDeletionService extends BaseDataDeletionService implements IGrapevineDataDeletionService {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private GroupMembershipRepository groupMembershipRepository;
    @Autowired
    private PostInteractionRepository postInteractionRepository;
    @Autowired
    private LikeCommentRepository likeCommentRepository;
    @Autowired
    private HappeningParticipantRepository happeningParticipantRepository;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private ILikeCommentService likeCommentService;
    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private ITimeService timeService;
    @Autowired
    private IGrapevineSearchService grapevineSearchService;

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public DeleteOperation deletePost(Post post) {

        post.setText(erasedNonNullString());
        post.setCreated(erasedLongValue());
        //we do not update last modified value since otherwise some of the sort orders are affected
        post.setLastActivity(erasedLongValue());
        post.setDeleted(true);
        post.setImages(null);
        post.setAttachments(null);
        post.setCustomAddress(null);
        post.setLikeCount(0);
        post.setDeletionTime(timeService.currentTimeMillisUTC());
        post.setCreatedLocation(erasedGPSLocation());

        post.accept(new Post.Visitor<Void>() {
            @Override
            public Void whenGossip(Gossip gossip) {
                return null;
            }

            @Override
            public Void whenNewsItem(NewsItem newsItem) {
                handleExternalPost(newsItem);
                return null;
            }

            @Override
            public Void whenSuggestion(Suggestion suggestion) {
                //we want to keep the empty suggestion, so that the internal chats of suggestion workers are kept
                suggestion.setOverrideDeletedForSuggestionRoles(true);
                return null;
            }

            @Override
            public Void whenSeeking(Seeking seeking) {
                handleTrade(seeking);
                return null;
            }

            @Override
            public Void whenOffer(Offer offer) {
                handleTrade(offer);
                return null;
            }

            @Override
            public Void whenHappening(Happening happening) {
                handleExternalPost(happening);
                happening.setOrganizer(erasedNonNullString());
                happening.setStartTime(erasedLongValue());
                happening.setEndTime(erasedLongValue());
                return null;
            }

            private void handleExternalPost(ExternalPost externalPost) {
                externalPost.setAuthorName(erasedNonNullString());
                externalPost.setCategories(erasedNonNullString());
                externalPost.setExternalId(erasedNonNullString());
                externalPost.setNewsSource(null);
            }

            @Override
            public Void whenSpecialPost(SpecialPost specialPost) {
                return null;
            }

            private void handleTrade(Trade trade) {
                trade.setDate(erasedLongValue());
                trade.setTradingStatus(TradingStatus.DONE);
            }
        });

        postRepository.saveAndFlush(post);
        postInteractionRepository.deleteAllByPost(post);
        grapevineSearchService.indexOrUpdateIndexedPost(post);
        return DeleteOperation.ERASED;
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public DeleteOperation deleteComment(Comment comment) {

        comment.setImages(null);
        comment.setAttachments(null);
        comment.setDeleted(true);
        //we do not update last modified value since otherwise some of the sort orders are affected
        comment.setText(erasedNonNullString());
        comment.setLikeCount(0);
        comment.setDeletionTime(timeService.currentTimeMillisUTC());

        commentRepository.saveAndFlush(comment);
        likeCommentRepository.deleteAllByComment(comment);
        grapevineSearchService.indexOrUpdateIndexedComment(comment);
        return DeleteOperation.ERASED;
    }

    @Override
    public DeleteOperation deleteGroupMembership(GroupMembership groupMembership) {

        groupMembershipRepository.delete(groupMembership);
        //we update the member count to keep it consistent, but we want to do it with a fresh group
        groupService.updateGroupMemberCount(groupService.refresh(groupMembership.getGroup()));
        return DeleteOperation.DELETED;
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public DeleteOperation deletePostInteraction(PostInteraction postInteraction) {

        postInteractionRepository.delete(postInteraction);
        postInteractionService.updatePostLikeCount(postInteraction.getPost());
        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteLikeComment(LikeComment likeComment) {

        likeCommentRepository.delete(likeComment);
        likeCommentService.updateCommentLikeCount(likeComment.getComment());
        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteHappeningParticipant(HappeningParticipant happeningParticipant) {

        happeningParticipantRepository.delete(happeningParticipant);
        happeningService.updateParticipationCount(happeningParticipant.getHappening());
        return DeleteOperation.DELETED;
    }

    @Override
    public DeleteOperation deleteGroup(Group group) {

        groupService.deleteGroup(group);
        group.setName("-");
        group.setLogo(null);
        group.setDescription(null);
        group.setShortName("-");
        groupService.store(group);
        return DeleteOperation.ERASED;
    }

}
