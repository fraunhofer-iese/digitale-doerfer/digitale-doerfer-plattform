/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Service
class CommentService extends BaseEntityService<Comment> implements ICommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private IMediaItemService mediaItemService;

    @Autowired
    private ILikeCommentService likeCommentService;

    @Override
    public List<Comment> findAllByPostOrderByCreatedAsc(Post post) {
        return commentRepository.findAllByPostOrderByCreatedAsc(post);
    }

    @Override
    public List<Comment> findAllByPostFilteredOrderByCreatedAsc(Person caller, Post post) {
        int hiddenCreatorStatusBitMask = caller.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT) ?
                0 : PersonStatus.PARALLEL_WORLD_INHABITANT.getBitMaskValue();

        return commentRepository.findAllByPostAndNotCreatorStatusOrderByCreatedAsc(post, hiddenCreatorStatusBitMask);
    }

    @Override
    public List<Comment> findAllByCreatorOrderByCreatedAsc(Person creator) {
        return commentRepository.findAllByCreatorOrderByCreatedDesc(creator);
    }

    @Override
    public List<Comment> findAllByPostAndCreatorNotDeleted(Post post, Person creator) {
        return commentRepository.findAllByPostAndCreatorAndDeletedFalseOrderByCreatedDesc(post, creator);
    }

    @Override
    public Comment findCommentByIdIncludingDeleted(String commentId) throws CommentNotFoundException {
        return commentRepository.findById(commentId)
                .orElseThrow(() -> new CommentNotFoundException(commentId));
    }

    @Override
    public Comment findCommentByIdNotDeleted(String commentId) throws CommentNotFoundException {
        return commentRepository.findByIdAndDeletedFalse(commentId)
                .orElseThrow(() -> new CommentNotFoundException(commentId));
    }

    @Override
    public void deleteCommentsForDemoPost(Post post) {
        final List<Comment> comments = commentRepository.findAllByPostOrderByCreatedAsc(post);
        for (Comment comment : comments) {
            final List<MediaItem> images = comment.getImages();
            if (images != null) {
                comment.setImages(null);
                commentRepository.save(comment);
                for (MediaItem image : images) {
                    mediaItemService.deleteItem(image);
                }
            }
            commentRepository.delete(comment);
        }
        commentRepository.flush();
    }

    @Override
    public Comment deleteComment(Comment comment) {

        final long currentTime = timeService.currentTimeMillisUTC();
        comment.setDeleted(true);
        comment.setDeletionTime(currentTime);
        comment.setLastModified(currentTime);
        comment.setLikeCount(0);
        comment = store(comment);
        likeCommentService.deleteAllByComment(comment);
        return comment;
    }

}
