/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionStatusChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionWorkerAddConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionService;
import de.fhg.iese.dd.platform.business.grapevine.template.GrapevineTemplate;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class SuggestionEmailEventProcessor extends BaseEventProcessor {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IAppService appService;
    @Autowired
    private IEmailSenderService emailSenderService;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private ISuggestionService suggestionService;

    @EventProcessing
    void handlePostCreateConfirmation(PostCreateConfirmation<?> event) {

        Post post = event.getPost();
        //it is *not* possible to register for a parameterized event class: PostCreateConfirmation<Suggestion> will not work
        if (!(post instanceof Suggestion suggestion)) {
            return;
        }
        if (suggestion.isParallelWorld()) {
            return;
        }

        Tenant tenant = suggestion.getTenant();
        final Set<Person> potentialRecipients = new HashSet<>(roleService.getAllPersonsWithRoleForEntity(
                Arrays.asList(SuggestionFirstResponder.class, SuggestionWorker.class),
                tenant));
        PushCategory category =
                pushCategoryService.findPushCategoryById(LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_CREATED);
        Set<AppVariant> appVariants = appService.findAllAppVariants(category.getApp());
        Collection<Person> recipients = pushCategoryService
                .filterPersonsWithPushCategoryUserSetting(category, potentialRecipients, appVariants)
                //true = person activated loud push = email in this case
                .get(true);
        if (CollectionUtils.isEmpty(recipients)) {
            log.info("Suggestion {} has been created, {} workers are potential recipients " +
                            "(roles SuggestionFirstResponder or SuggestionWorker for tenant), " +
                            "but all have switched off notifications (PUSH_CATEGORY_ID_EMAIL_SUGGESTION_CREATED)",
                    suggestion.getId(), potentialRecipients.size());
            return;
        }

        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressLoesbar();
        final String subject = suggestion.getSuggestionCategory() == null ?
                GrapevineTemplate.NEW_SUGGESTION_EMAIL_SIMPLE :
                GrapevineTemplate.formattedString(GrapevineTemplate.NEW_SUGGESTION_EMAIL_CATEGORY,
                        suggestion.getSuggestionCategory().getDisplayName());

        emailSenderService.sendHtmlEmailToPersons(fromEmailAddress,
                recipients,
                GrapevineTemplate.NEW_SUGGESTION_EMAIL,
                receiver -> buildCommonTemplateModel(receiver, suggestion),
                subject);
    }

    @EventProcessing
    void handleCommentCreateConfirmation(CommentCreateConfirmation event) {

        final Comment comment = event.getComment();
        if (!(comment.getPost() instanceof Suggestion suggestion)) {
            return;
        }
        if (comment.getCreator().getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT)) {
            return;
        }

        final Set<Person> potentialRecipients = suggestionService.findSuggestionWorkers(suggestion, false)
                .stream()
                .map(SuggestionWorkerMapping::getWorker)
                .filter(worker -> worker != null && !worker.equals(comment.getCreator()))
                .collect(Collectors.toSet());
        PushCategory category = pushCategoryService.findPushCategoryById(
                LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_COMMENT_CREATED);
        Set<AppVariant> appVariants = appService.findAllAppVariants(category.getApp());
        Collection<Person> recipients = pushCategoryService
                .filterPersonsWithPushCategoryUserSetting(category, potentialRecipients, appVariants)
                //true = person activated loud push = email in this case
                .get(true);
        if (CollectionUtils.isEmpty(recipients)) {
            log.info("Comment on suggestion {} has been created, {} workers are potential recipients " +
                            "(assigned to this suggestion), " +
                            "but all have switched off notifications (PUSH_CATEGORY_ID_EMAIL_SUGGESTION_COMMENT_CREATED)",
                    suggestion.getId(), potentialRecipients.size());
            return;
        }

        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressLoesbar();

        emailSenderService.sendHtmlEmailToPersons(fromEmailAddress,
                recipients,
                GrapevineTemplate.NEW_SUGGESTION_COMMENT_EMAIL,
                receiver -> {
                    final Map<String, Object> templateModel = buildCommonTemplateModel(receiver, suggestion);
                    templateModel.put("commentCreator", comment.getCreator().getFullName());
                    templateModel.put("commentText", comment.getText());
                    templateModel.put("commentCreatedTime", timeService.toLocalTime(comment.getCreated()).format(
                            DateTimeFormatter.ofPattern("dd.MM.yyyy 'um' HH:mm 'Uhr'")));
                    return templateModel;
                },
                GrapevineTemplate.NEW_SUGGESTION_COMMENT);
    }

    @EventProcessing
    void handleSuggestionStatusChangeConfirmation(SuggestionStatusChangeConfirmation event) {

        final Suggestion suggestion = event.getChangedSuggestion();
        final List<SuggestionWorkerMapping> suggestionWorkerMappings =
                nullToEmptyList(suggestion.getSuggestionWorkerMappings());
        final List<SuggestionStatusRecord> suggestionStatusRecords =
                nullToEmptyList(suggestion.getSuggestionStatusRecords());

        final SuggestionStatus oldStatus, newStatus;
        if (suggestionStatusRecords.isEmpty()) {
            log.error("Status of suggestion {} changed, but status record list is empty!", suggestion.getId());
            oldStatus = null;
            newStatus = null;
        } else if (suggestionStatusRecords.size() == 1) {
            log.error("Status of suggestion {} changed, but status record list has only one entry!",
                    suggestion.getId());
            oldStatus = null;
            newStatus = suggestionStatusRecords.get(0).getStatus();
        } else {
            oldStatus = suggestionStatusRecords.get(1).getStatus();
            newStatus = suggestionStatusRecords.get(0).getStatus();
        }
        final Person statusChangeInitiator = suggestionStatusRecords.stream()
                .findFirst()
                .map(SuggestionStatusRecord::getInitiator)
                .orElse(null);

        final Set<Person> potentialRecipients = suggestionWorkerMappings.stream()
                .map(SuggestionWorkerMapping::getWorker)
                .filter(worker -> worker != null && !worker.equals(statusChangeInitiator))
                .collect(Collectors.toSet());
        PushCategory category = pushCategoryService.findPushCategoryById(
                LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_STATUS_CHANGED);
        Set<AppVariant> appVariants = appService.findAllAppVariants(category.getApp());
        Collection<Person> recipients = pushCategoryService
                .filterPersonsWithPushCategoryUserSetting(category, potentialRecipients, appVariants)
                //true = person activated loud push = email in this case
                .get(true);
        if (CollectionUtils.isEmpty(recipients)) {
            log.info("Status of suggestion {} has been changed, {} workers are potential recipients " +
                            "(assigned to this suggestion), " +
                            "but all have switched off notifications (PUSH_CATEGORY_ID_EMAIL_SUGGESTION_STATUS_CHANGED)",
                    suggestion.getId(), potentialRecipients.size());
            return;
        }

        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressLoesbar();

        emailSenderService.sendHtmlEmailToPersons(fromEmailAddress,
                recipients,
                GrapevineTemplate.SUGGESTION_STATUS_CHANGE,
                receiver -> {
                    final Map<String, Object> templateModel = buildCommonTemplateModel(receiver, suggestion);
                    templateModel.put("oldStatus", oldStatus);
                    templateModel.put("newStatus", newStatus);
                    if (statusChangeInitiator != null) {
                        templateModel.put("statusChangerName", statusChangeInitiator.getFullName());
                    }
                    return templateModel;
                },
                GrapevineTemplate.SUGGESTION_STATUS_CHANGE_SUBJECT);
    }

    @EventProcessing
    void handleSuggestionWorkerAddConfirmation(SuggestionWorkerAddConfirmation event) {

        final Suggestion suggestion = event.getSuggestion();
        final Person worker = event.getWorker();
        final Person initiator = event.getInitiator();

        if (worker.equals(initiator)) {
            //do not send a mail if worker added him-/herself
            return;
        }

        PushCategory category = pushCategoryService.findPushCategoryById(
                LoesBarConstants.PUSH_CATEGORY_ID_EMAIL_SUGGESTION_WORKER_ADDED);
        Set<AppVariant> appVariants = appService.findAllAppVariants(category.getApp());
        Collection<Person> recipients = pushCategoryService
                .filterPersonsWithPushCategoryUserSetting(category, Collections.singleton(worker), appVariants)
                //true = person activated loud push = email in this case
                .get(true);

        if (CollectionUtils.isEmpty(recipients)) {
            log.info("One worker has been added to suggestion {}, who also is potential recipient " +
                            "(assigned to this suggestion), " +
                            "but has switched off notifications (PUSH_CATEGORY_ID_EMAIL_SUGGESTION_WORKER_ADDED)",
                    suggestion.getId());
            return;
        }

        final String fromEmailAddress = grapevineConfig.getSenderEmailAddressLoesbar();

        emailSenderService.sendHtmlEmailToPersons(fromEmailAddress,
                recipients,
                GrapevineTemplate.SUGGESTION_WORKER_ADD_EMAIL,
                receiver -> {
                    final Map<String, Object> templateModel = buildCommonTemplateModel(receiver, suggestion);
                    if (initiator != null) {
                        templateModel.put("initiatorName", initiator.getFullName());
                    }
                    return templateModel;
                },
                GrapevineTemplate.SUGGESTION_WORKER_ADD_SUBJECT);
    }

    private Map<String, Object> buildCommonTemplateModel(Person receiver, Suggestion suggestion) {

        Person creator = suggestion.getCreator();
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiverName", receiver.getFullName());
        templateModel.put("creatorName", creator == null
                ? "-"
                : creator.getFullName());
        templateModel.put("createdTime", timeService.toLocalTime(suggestion.getCreated()).format(
                DateTimeFormatter.ofPattern("dd.MM.yyyy 'um' HH:mm 'Uhr'")));
        if (suggestion.getCustomAddress() != null) {
            //trimmed location for display
            String location = suggestion.getCustomAddress().getName();
            if (StringUtils.isNotEmpty(location)) {
                templateModel.put("location", location);
            }
            if (suggestion.getCustomAddress().getGpsLocation() != null) {
                if (StringUtils.isEmpty(location)) {
                    //if location has no name, but GPS coordinates, a link to the coordinates with label "Unbekannt"
                    //will be included in the mails
                    templateModel.put("location", "Unbekannt");
                }
                //exact location for maps link
                templateModel.put("locationLatLng", String.format(Locale.US, "%f,%f",
                        suggestion.getCustomAddress().getGpsLocation().getLatitude(),
                        suggestion.getCustomAddress().getGpsLocation().getLongitude()));
            }
        }
        if (suggestion.getSuggestionCategory() != null) {
            templateModel.put("suggestionCategory", suggestion.getSuggestionCategory().getDisplayName());
        }
        templateModel.put("suggestionText", suggestion.getText());
        if (suggestion.getImages() != null && !suggestion.getImages().isEmpty()) {
            templateModel.put("nrOfImages", suggestion.getImages().size());
        }
        if (!StringUtils.isEmpty(grapevineConfig.getLoesbarBaseUrl())) {
            templateModel.put("suggestionLink", UriComponentsBuilder
                    .fromHttpUrl(grapevineConfig.getLoesbarBaseUrl())
                    .pathSegment("vorschlag", suggestion.getId())
                    .build());
        }
        templateModel.put("contactPerson", roleService.getDefaultContactPersonForTenant(suggestion.getTenant()));
        return templateModel;
    }

    private <T> List<T> nullToEmptyList(List<T> list) {
        return list == null ? Collections.emptyList() : list;
    }

}
