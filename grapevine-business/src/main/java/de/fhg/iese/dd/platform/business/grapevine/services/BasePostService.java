/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;

abstract class BasePostService<P extends Post> extends BaseEntityService<P> {

    @Autowired
    protected INewsSourceService newsSourceService;
    @Autowired
    protected IGeoAreaService geoAreaService;
    @Autowired
    protected ITenantService tenantService;
    @Autowired
    protected ITradingCategoryService tradingCategoryService;
    @Autowired
    protected ISuggestionCategoryService suggestionCategoryService;

    protected <X extends Post> X useCachedEntities(X post) {
        //noinspection ConstantConditions
        if (post == null) {
            return null;
        }
        post.setGeoAreas(geoAreaService.forceCached(post.getGeoAreas()));
        post.setTenant(tenantService.forceCached(post.getTenant()));
        Person creator = post.getCreator();
        if (creator != null) {
            creator.setHomeArea(geoAreaService.forceCached(creator.getHomeArea()));
        }
        if (post instanceof ExternalPost externalPost) {
            NewsSource newsSource = externalPost.getNewsSource();
            if (newsSource != null) {
                externalPost.setNewsSource(newsSourceService.findNewsSourceByIdIncludingDeleted(newsSource.getId()));
            }
        }
        if (post instanceof Trade trade) {
            TradingCategory tradingCategory = trade.getTradingCategory();
            if (tradingCategory != null) {
                trade.setTradingCategory(tradingCategoryService.findTradingCategoryById(tradingCategory.getId()));
            }
        }
        if (post instanceof Suggestion suggestion) {
            SuggestionCategory suggestionCategory = suggestion.getSuggestionCategory();
            if (suggestionCategory != null) {
                suggestion.setSuggestionCategory(
                        suggestionCategoryService.findSuggestionCategoryById(suggestionCategory.getId()));
            }
        }
        return post;
    }

}
