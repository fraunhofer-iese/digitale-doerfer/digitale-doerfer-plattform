/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.security;

import java.util.Collection;
import java.util.Collections;

import de.fhg.iese.dd.platform.business.participants.tenant.security.ActionTenantRestricted;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GlobalGroupAdmin;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.GroupAdmin;

public class AddPersonToGroupAction extends ActionTenantRestricted {

    private static final Collection<Class<? extends BaseRole<?>>> ROLES_ALL_TENANTS_ALLOWED =
            Collections.singleton(GlobalGroupAdmin.class);
    private static final Collection<Class<? extends BaseRole<?>>> ROLES_SPECIFIC_TENANTS_ALLOWED =
            Collections.singleton(GroupAdmin.class);

    @Override
    public String getActionDescription() {
        return "Adds a person to an existing group in backend";
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesAllTenantsAllowed() {
        return ROLES_ALL_TENANTS_ALLOWED;
    }

    @Override
    protected Collection<Class<? extends BaseRole<?>>> rolesSpecificTenantsAllowed() {
        return ROLES_SPECIFIC_TENANTS_ALLOWED;
    }

}
