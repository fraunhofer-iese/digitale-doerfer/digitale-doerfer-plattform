/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.callback.providers.IWebClientProvider;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ExternalException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;

@Log4j2
@Service
class GrapevineTextClassificationService extends BaseService implements IGrapevineTextClassificationService {

    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private IWebClientProvider webClientProvider;

    private String lastExceptionMessage = null;

    private WebClient webClient;

    private WebClient getWebClient() {

        if (webClient == null) {
            webClient = webClientProvider.createWebClient(grapevineConfig.getClassificationService().getBaseUrl());
        }
        return webClient;
    }

    @Override
    @Nullable
    public Double classifyText(String text) {

        try {
            ClassificationRequest request = ClassificationRequest.builder()
                    .texts(List.of(text))
                    .apiKey(grapevineConfig.getClassificationService().getApiKey())
                    .build();
            Double value = extractResultValue(getWebClient().post()
                    .uri(grapevineConfig.getClassificationService().getClassificationQuery())
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .bodyValue(request)
                    .retrieve()
                    .bodyToMono(ClassificationResponse.class)
                    .block());

            lastExceptionMessage = null;
            return value;
        } catch (WebClientException | ExternalException e) {
            if (!Objects.equals(e.getMessage(), lastExceptionMessage)) {
                log.error("Failed to classify text", e);
                lastExceptionMessage = e.getMessage();
            }
            return null;
        }
    }

    private double extractResultValue(@Nullable ClassificationResponse response) {

        if (response == null) {
            throw new ExternalException("Classification response was null");
        }
        if (CollectionUtils.isEmpty(response.getResult())) {
            throw new ExternalException("Classification response did not contain result");
        }
        List<Double> result = response.getResult();
        if (result.size() != 1) {
            throw new ExternalException("Classification response did contained wrong number of results {}",
                    result.size());
        }
        Double value = result.get(0);
        if (value == null) {
            throw new ExternalException("Classification response contained a null value");
        }

        return value;
    }

    @Builder
    @Data
    private static class ClassificationRequest {

        @JsonProperty("api_key")
        private String apiKey;

        private List<String> texts;

    }

    @Data
    private static class ClassificationResponse {

        private List<Double> result;

    }

}
