/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.shared.search.exceptions.SearchNotAvailableException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;

public interface IGrapevineSearchService extends IService {

    void ensureIndexExists(@Nullable LogSummary logSummary);

    void checkIndex(LogSummary logSummary);

    void updateIndex(LogSummary logSummary);

    void indexOrUpdateIndexedPost(Post post);

    void indexOrUpdateIndexedComment(Comment comment);

    Page<Post> simpleSearch(String searchTerm, String creatorId, String homeAreaId, Set<String> geoAreaIds,
            Set<String> groupIds, Pageable pageable) throws SearchNotAvailableException;

    long deleteOldEntries();

    long reIndexFailedEntries();

}
