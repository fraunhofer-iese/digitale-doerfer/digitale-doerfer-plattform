/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class NewsSourceAlreadyExistsException extends BadRequestException {

    private static final long serialVersionUID = 4681249990441134547L;

    private NewsSourceAlreadyExistsException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static NewsSourceAlreadyExistsException forAlreadyExists(String siteUrl) {
        return new NewsSourceAlreadyExistsException("News source with site url '{}' already exists", siteUrl);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.NEWS_SOURCE_ALREADY_EXISTS;
    }

}
