/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Steffen Hupp, Benjamin Hassenfratz, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.grapevine.events.ExternalPostPublishRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.ExternalPostUnpublishRequest;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.callback.exceptions.ExternalSystemCallFailedException;
import de.fhg.iese.dd.platform.business.shared.callback.services.ExternalSystemResponse;
import de.fhg.iese.dd.platform.business.shared.callback.services.IExternalSystemCallbackService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ExternalPostRepository;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
class PostPublishQueueService extends BaseService implements IPostPublishQueueService {

    private static final Marker LOG_MARKER_POST_URL_CHECK = MarkerManager.getMarker("POST_URL_CHECK");

    @Autowired
    private ExternalPostRepository externalPostRepository;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IExternalSystemCallbackService externalSystemCallbackService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private GrapevineConfig grapevineConfig;

    @Override
    public void publishToBePublishedPosts() {

        final long currentTime = timeService.currentTimeMillisUTC();
        externalPostRepository.findAllPublishedFalseAndWithinDesiredPublishTime(currentTime).stream()
                .map(this::useCachedGeoArea)
                .forEach(post -> notify(new ExternalPostPublishRequest(post)));
    }

    @Override
    public void unpublishToBeUnpublishedPosts() {

        final long currentTime = timeService.currentTimeMillisUTC();
        externalPostRepository.findAllPublishedTrueAndAfterDesiredUnpublishTime(currentTime).stream()
                .map(this::useCachedGeoArea)
                .forEach(post -> notify(new ExternalPostUnpublishRequest(post)));
    }

    @Override
    public void unpublishInvalidUrlPosts() {

        GrapevineConfig.ExternalPostUrlCheckConfig externalPostUrlCheckConfig = grapevineConfig.getExternalPostUrlCheck();
        long currentTime = timeService.currentTimeMillisUTC();
        long minLastModified = currentTime - externalPostUrlCheckConfig.getPostMaxAge().toMillis();
        long maxLastCheck = currentTime - externalPostUrlCheckConfig.getCheckIntervalPerPost().toMillis();
        //these are all posts that are published, not deleted, the news source has check url of posts true,
        // and they are within the time boundaries
        List<ExternalPost> postsToCheck = externalPostRepository
                .findAllPublishedAndLastModifiedAfterAndLastCheckBefore(minLastModified, maxLastCheck,
                        Pageable.ofSize(externalPostUrlCheckConfig.getMaxNumPostsPerCheckRun()));
        int numPostsToCheck = postsToCheck.size();
        Map<String, Deque<ExternalPost>> postsToBeCheckedByNewsSourceSiteUrl = postsToCheck.stream()
                .filter(post -> post.getNewsSource() != null)
                .collect(Collectors.groupingBy(post -> post.getNewsSource().getSiteUrl(),
                        Collectors.toCollection(ArrayDeque::new)));
        if (postsToBeCheckedByNewsSourceSiteUrl.isEmpty()) {
            //if there are no posts to check we skip immediately
            return;
        }
        int numDifferentSourceSiteUrl = postsToBeCheckedByNewsSourceSiteUrl.size();
        int maxNumPostsSameNewsSourceSiteUrl = postsToBeCheckedByNewsSourceSiteUrl.values().stream()
                .mapToInt(Deque::size)
                .max()
                .orElseThrow();
        //we only have a limited amount of time to complete all check iterations
        long maxCheckRuntimeMillis = externalPostUrlCheckConfig.getMaxCheckRunDuration().toMillis();
        //we only have a limited amount of checks per iteration
        int maxNumChecksPerIteration = externalPostUrlCheckConfig.getMaxNumPostsPerIteration();
        //the max check run time and the minimum wait between each iteration constrains the number of iterations
        int maxAllowedNumberOfIterations = (int) (maxCheckRuntimeMillis /
                externalPostUrlCheckConfig.getMinWaitTimeBetweenIterations().toMillis());
        //the maximum possible iterations for the given posts is either
        // - the maximum of posts with the same site urls (one of them in every iteration)
        // or
        // - the maximum of different site urls divided by the max num of parallel checks (every iteration would be full of posts with different site urls)
        int maxPossibleNumberOfIterations = Math.max(maxNumPostsSameNewsSourceSiteUrl,
                numDifferentSourceSiteUrl / maxNumChecksPerIteration);
        int numberOfIterations = Math.min(maxAllowedNumberOfIterations, maxPossibleNumberOfIterations);

        //the pause is constrained by the maximum wait time
        long pauseBetweenIterations = Math.min(
                externalPostUrlCheckConfig.getMaxWaitTimeBetweenIterations().toMillis(),
                maxCheckRuntimeMillis / numberOfIterations);

        long pauseBetweenSingleChecks = externalPostUrlCheckConfig.getWaitTimeWithinIteration().toMillis();
        for (int i = 0; i < numberOfIterations; i++) {

            //this is a single iteration, where we only check posts of different news sources
            Set<String> siteUrlsInIteration = new HashSet<>(postsToBeCheckedByNewsSourceSiteUrl.size());
            for (int j = 0; j < maxNumChecksPerIteration; j++) {
                //this is a single check within a single iteration
                Map.Entry<String, Deque<ExternalPost>> siteUrlAndPosts = postsToBeCheckedByNewsSourceSiteUrl.entrySet()
                        .stream()
                        // if there are no posts anymore for this site url it can be ignored
                        .filter(e -> !e.getValue().isEmpty())
                        // we don't want the same site url multiple times in the iteration
                        .filter(e -> !siteUrlsInIteration.contains(e.getKey()))
                        // from the remaining posts, we want the newest one
                        .max(Comparator.comparingLong(
                                (Map.Entry<String, Deque<ExternalPost>> e) -> e.getValue().peekFirst().getCreated()))
                        .orElse(null);
                if (siteUrlAndPosts == null || siteUrlAndPosts.getValue().isEmpty()) {
                    // if there is no available post from a different site url the iteration is done
                    break;
                }
                siteUrlsInIteration.add(siteUrlAndPosts.getKey());
                ExternalPost postToBeChecked = siteUrlAndPosts.getValue().pollFirst();
                long taskStartTime = currentTime + i * pauseBetweenIterations + j * pauseBetweenSingleChecks;
                Instant startTimeInstant = Instant.ofEpochMilli(taskStartTime);
                if (log.isTraceEnabled()) {
                    log.trace("{} | {} {}  {}", i, j, startTimeInstant, postToBeChecked.getUrl());
                }
                parallelismService.scheduleOneTimeTask(() -> checkPostUrlAndUnpublish(postToBeChecked),
                        startTimeInstant);
            }
        }
        int numLeftOverPosts = postsToBeCheckedByNewsSourceSiteUrl.values().stream()
                .mapToInt(Deque::size)
                .sum();
        log.debug(LOG_MARKER_POST_URL_CHECK,
                "Scheduled check of {} posts with {} different site urls, with pause of {} between {} iterations, {} posts did not fit into check run",
                numPostsToCheck - numLeftOverPosts,
                postsToBeCheckedByNewsSourceSiteUrl.size(),
                DurationFormatUtils.formatDuration(pauseBetweenIterations, "mm:ss.SSS", true),
                numberOfIterations,
                numLeftOverPosts);
    }

    private void checkPostUrlAndUnpublish(ExternalPost post) {

        try {
            ExternalSystemResponse<String> response = externalSystemCallbackService
                    .callExternalSystem(
                            post.getNewsSource().getAppVariant(),
                            HttpMethod.GET,
                            URI.create(post.getUrl()),
                            null,
                            String.class);
            ResponseEntity<String> responseEntity = response.getResponse();
            if (responseEntity.getStatusCode().is5xxServerError()) {
                //the check is only successful if the external system responded != 5xx
                return;
            }
            post.setLastUrlCheckTime(timeService.currentTimeMillisUTC());
            ExternalPost savedPost = useCachedGeoArea(externalPostRepository.save(post));
            if (responseEntity.getStatusCode().is4xxClientError()) {
                //currently we assume that we get 4xx if the post is not available anymore
                notify(new ExternalPostUnpublishRequest(savedPost));
                log.debug(LOG_MARKER_POST_URL_CHECK,
                        "Check of post {} returned status code {} -> Post will be unpublished. Checked url '{}'",
                        post.getId(),
                        responseEntity.getStatusCode().toString(),
                        post.getUrl());
            } else {
                if (log.isTraceEnabled()) {
                    log.trace(LOG_MARKER_POST_URL_CHECK,
                            "Check of post {} returned status code {}. Checked url '{}'",
                            post.getId(),
                            responseEntity.getStatusCode().toString(),
                            post.getUrl());
                }
            }
        } catch (ExternalSystemCallFailedException ignored) {
            //if the call failed completely we retry next time
        }
    }

    private <P extends Post> P useCachedGeoArea(P post) {
        //noinspection ConstantConditions
        if (post != null) {
            post.setGeoAreas(geoAreaService.forceCached(post.getGeoAreas()));
        }
        return post;
    }

}
