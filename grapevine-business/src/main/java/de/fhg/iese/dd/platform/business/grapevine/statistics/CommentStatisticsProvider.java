/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.CommentCountByGeoAreaId;

@Component
public class CommentStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_COMMENT_COUNT = "grapevine.comment.count";
    public static final String STATISTICS_ID_COMMENT_COUNT_WITH_IMAGE = "grapevine.comment.count.withimage";
    public static final String STATISTICS_ID_COMMENT_COUNT_WITH_ATTACHMENT = "grapevine.comment.count.withattachment";

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(CommentStatisticsProvider.STATISTICS_ID_COMMENT_COUNT)
                .machineName("comment-count")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(CommentStatisticsProvider.STATISTICS_ID_COMMENT_COUNT_WITH_IMAGE)
                .machineName("comment-count-image")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(CommentStatisticsProvider.STATISTICS_ID_COMMENT_COUNT_WITH_ATTACHMENT)
                .machineName("comment-count-attachment")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(commentRepository::countByGeoAreaId, Map.of(
                        STATISTICS_ID_COMMENT_COUNT, CommentCountByGeoAreaId::getCommentCount,
                        STATISTICS_ID_COMMENT_COUNT_WITH_IMAGE, CommentCountByGeoAreaId::getCommentCountWithImage,
                        STATISTICS_ID_COMMENT_COUNT_WITH_ATTACHMENT,
                        CommentCountByGeoAreaId::getCommentCountWithAttachment),
                geoAreaInfos, definition);
    }

}
