/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikeCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.LikePostRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikeCommentConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikeCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikePostConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.like.UnlikePostRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.ILikeCommentService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostInteractionService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor
class LikeEventProcessor extends BasePostEventProcessor {

    @Autowired
    private IPostInteractionService postInteractionService;

    @Autowired
    private ILikeCommentService likeCommentService;

    @EventProcessing
    private LikePostConfirmation handleLikePostRequest(LikePostRequest request) {

        final Post post = request.getPost();
        final Person liker = request.getLiker();

        return new LikePostConfirmation(postInteractionService.likePost(post, liker), liker);
    }

    @EventProcessing
    private UnlikePostConfirmation handleUnlikePostRequest(UnlikePostRequest request) {

        final Post post = request.getPost();
        final Person unliker = request.getUnliker();

        return new UnlikePostConfirmation(postInteractionService.unlikePost(post, unliker), unliker);
    }

    @EventProcessing
    private LikeCommentConfirmation handleLikeCommentRequest(LikeCommentRequest request) {

        final Comment comment = request.getComment();
        final Person liker = request.getLiker();

        return new LikeCommentConfirmation(likeCommentService.likeComment(comment, liker), liker);
    }

    @EventProcessing
    private UnlikeCommentConfirmation handleUnlikeCommentRequest(UnlikeCommentRequest request) {

        final Comment comment = request.getComment();
        final Person unliker = request.getUnliker();

        return new UnlikeCommentConfirmation(likeCommentService.unlikeComment(comment, unliker), unliker);
    }

}
