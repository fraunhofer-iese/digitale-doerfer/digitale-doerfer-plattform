/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.search.exceptions.SearchNotAvailableException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.search.BaseSearchEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.search.SearchPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import lombok.AccessLevel;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.open.OpenIndexRequest;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CloseIndexRequest;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.Operator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.index.Settings;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * This is the actually working implementation of the service, which uses elasticsearch for search
 */
@Service
@ConditionalOnProperty(
        value = "dd-platform.grapevine.search-activated",
        havingValue = "true")
class GrapevineSearchActivatedService extends BaseService implements IGrapevineSearchService {

    private static final int REINDEX_BATCH_SIZE = 500;
    private static final int REINDEX_PAGE_SIZE = 10;

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private ElasticsearchOperations elasticsearchOperations;
    @Autowired
    private RestHighLevelClient elasticsearchClient;
    @Autowired
    private IParallelismService parallelismService;

    private IndexCoordinates searchPostIndex;
    @Setter(AccessLevel.PROTECTED) //required for testing
    private boolean indexChecked = false;

    @Override
    public void ensureIndexExists(@Nullable LogSummary logSummary) {

        if (!callElasticsearch(e -> e.indexOps(getSearchPostIndex()).exists())) {
            log.info("No index found, creating new index {}", getSearchPostIndex().getIndexName());
            if (logSummary != null) {
                logSummary.info("No index found, creating new index {}", getSearchPostIndex().getIndexName());
            }
            callElasticsearch(e -> e.indexOps(SearchPost.class).createWithMapping());
        }
        indexChecked = true;
    }

    @Override
    public void checkIndex(LogSummary logSummary) {

        logSummary.info("Index {}", getSearchPostIndex().getIndexName());

        final Map<String, Object> existingMapping =
                callElasticsearch(e -> e.indexOps(SearchPost.class).getMapping());
        final Document newMapping =
                callElasticsearch(e -> e.indexOps(SearchPost.class).createMapping(SearchPost.class));
        final Map<String, Object> existingSettings =
                callElasticsearch(e -> e.indexOps(SearchPost.class).getSettings());
        final Map<String, Object> newSettings =
                callElasticsearch(e -> e.indexOps(SearchPost.class).createSettings(SearchPost.class));

        logSummary.info("Mapping on search server:\n{}", printPrettyJson(existingMapping));
        logSummary.info("Mapping defined by software:\n{}", printPrettyJson(newMapping));
        logSummary.info("Settings on search server:\n{}", printPrettyJson(existingSettings));
        logSummary.info("Settings defined by software:\n{}", printPrettyJson(newSettings));
    }

    @Override
    public void updateIndex(LogSummary logSummary) {

        final String indexName = getSearchPostIndex().getIndexName();
        final Document newMapping =
                callElasticsearch(e -> e.indexOps(SearchPost.class).createMapping(SearchPost.class));
        final Settings newSettings =
                callElasticsearch(e -> e.indexOps(SearchPost.class).createSettings(SearchPost.class));

        logSummary.info("Working on index {}", indexName);
        logSummary.info("Steps:\n" +
                "- Closing index so that updates on the settings and mappings can be done" +
                "- Updating the settings that are referenced by the mappings" +
                "- Updating the mappings (this can only add or change fields, not remove any)" +
                "- Opening the index again so that the search server can re-index all existing documents with the new settings again");
        final CloseIndexRequest closeIndexRequest = new CloseIndexRequest(indexName);
        try {
            logSummary.info("Closing index");
            elasticsearchClient.indices().close(closeIndexRequest, RequestOptions.DEFAULT);
            logSummary.info("Closed index");
        } catch (IOException e) {
            logSummary.error("Failed closing index: {}", e);
            return;
        }

        final org.elasticsearch.common.settings.Settings convertedSettings =
                org.elasticsearch.common.settings.Settings.builder()
                        .loadFromMap(newSettings)
                        .build();
        UpdateSettingsRequest updateSettingsRequest = new UpdateSettingsRequest(convertedSettings, indexName);
        try {
            logSummary.info("Updating settings");
            elasticsearchClient.indices().putSettings(updateSettingsRequest, RequestOptions.DEFAULT);
            logSummary.info("Updated settings");
        } catch (IOException e) {
            logSummary.error("Failed to update settings: {}", e);
        }

        PutMappingRequest putMappingRequest = new PutMappingRequest(indexName);
        putMappingRequest.source(newMapping);
        try {
            logSummary.info("Updating mappings");
            elasticsearchClient.indices().putMapping(putMappingRequest, RequestOptions.DEFAULT);
            logSummary.info("Updated mappings");
        } catch (Exception e) {
            logSummary.error("Failed to update mappings: {}", e);
        }

        final OpenIndexRequest openIndexRequest = new OpenIndexRequest(indexName);
        try {
            logSummary.info("Opening index {}", indexName);
            elasticsearchClient.indices().open(openIndexRequest, RequestOptions.DEFAULT);
            logSummary.info("Opened index {}", indexName);
        } catch (IOException e) {
            logSummary.error("Failed opening index: {}", e);
        }
    }

    private String printPrettyJson(Object object) {
        final ObjectMapper mapper = JsonMapper.builder()
                .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
                .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
                .build();

        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return "Could not print: " + e;
        }
    }

    private void checkIndex() {
        if (!indexChecked) {
            ensureIndexExists(null);
        }
    }

    @Override
    public void indexOrUpdateIndexedPost(Post post) {

        checkIndex();
        if (post.isDeleted() ||
                // external posts that are not published should not be searchable
                (post instanceof ExternalPost && !((ExternalPost) post).isPublished())) {
            callElasticsearchAndUpdatePost(e -> e.delete(post.getId(), getSearchPostIndex()), post);
        } else {
            final SearchPost searchPost = postToSearchPost(post);
            if (searchPost != null) {
                final IndexQuery indexQuery = new IndexQueryBuilder()
                        .withId(searchPost.getId())
                        .withObject(searchPost)
                        .build();
                final String id = callElasticsearchAndUpdatePost(e -> e.index(indexQuery, getSearchPostIndex()), post);
                if (log.isTraceEnabled()) {
                    log.trace("Saved {} to {} with id {}", searchPost.toString(), getSearchPostIndex(), id);
                }
            } else {
                //the search post is null if it should not be indexed
                //in this case we try to delete it, in case it was indexed already before
                callElasticsearchAndUpdatePost(e -> e.delete(post.getId(), getSearchPostIndex()), post);
            }
        }
    }

    @Override
    public void indexOrUpdateIndexedComment(Comment comment) {
        indexOrUpdateIndexedPost(comment.getPost());
    }

    @Override
    public Page<Post> simpleSearch(String searchTerm, String creatorId, String homeAreaId, Set<String> geoAreaIds,
            Set<String> groupIds, Pageable pageable) throws SearchNotAvailableException {

        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQuery()
                        .must(multiMatchQuery(searchTerm, "text", "commentTexts", "address")
                                //see https://www.elastic.co/guide/en/elasticsearch/reference/7.15/common-options.html#fuzziness
                                // defines how many edits are allowed
                                // 0..3 exact match
                                // 4..7 one edit
                                // > 8 two edits
                                .fuzziness(Fuzziness.build("auto:4,8"))
                                //how many characters need to match at the beginning
                                .prefixLength(2)
                                .operator(Operator.AND))
                        .filter(boolQuery()
                                .should(termsQuery("geoAreaIds", geoAreaIds))
                                .should(termsQuery("groupId", groupIds))
                                .should(termQuery("homeGeoAreaIds", homeAreaId))
                                .should(termQuery("creatorId", creatorId))))
                .withPageable(pageable)
                .withFields("id")
                .build();
        final SearchHits<BaseSearchEntity> searchHits =
                callElasticsearch(e -> e.search(searchQuery, BaseSearchEntity.class, getSearchPostIndex()));
        if (log.isTraceEnabled()) {
            log.trace("Searching with term '{}' geoAreaId {} groupId {} homeGeoAreaId {} creatorId {}: {}",
                    searchTerm, geoAreaIds, groupIds, homeAreaId, creatorId, searchHits);
        }
        final List<String> foundPostIds = searchHits.stream()
                .map(SearchHit::getId)
                .collect(Collectors.toList());
        final List<Post> foundPostsVisible = postRepository.findAllByIdIn(foundPostIds).stream()
                //this filtering should never filter out posts if they have been properly indexed and the search query is correct
                .filter(post -> checkPostVisible(post, creatorId, homeAreaId, geoAreaIds, groupIds))
                .sorted(Comparator.comparingInt(o -> foundPostIds.indexOf(o.getId())))
                .collect(Collectors.toList());

        int numInvisiblePosts = foundPostIds.size() - foundPostsVisible.size();
        if (numInvisiblePosts != 0) {

            final Set<String> foundPostIdsVisible = foundPostsVisible.stream()
                    .map(Post::getId)
                    .collect(Collectors.toSet());

            final Set<String> foundPostIdsInvisible = foundPostIds.stream()
                    .filter(id -> !foundPostIdsVisible.contains(id))
                    .collect(Collectors.toSet());

            for (String foundPostIdInvisible : foundPostIdsInvisible) {
                parallelismService.getBlockableExecutor().submit(() -> {
                    Optional<Post> foundPostInvisibleOpt = postRepository.findById(foundPostIdInvisible);
                    if (foundPostInvisibleOpt.isPresent()) {
                        //we reindex the invalid ones, so that they get updated and do not show up anymore
                        indexOrUpdateIndexedPost(foundPostInvisibleOpt.get());
                    } else {
                        //if the post could not be found we delete it from the index
                        log.warn("Invalid search result: Post {} is not found in DB", foundPostIdInvisible);
                        callElasticsearch(e -> e.delete(foundPostIdInvisible, getSearchPostIndex()));
                    }
                });
            }
            log.warn("Search found {} invisible posts: {}", numInvisiblePosts, foundPostIdsInvisible);
        }

        return new PageImpl<>(foundPostsVisible, pageable, searchHits.getTotalHits() - numInvisiblePosts);
    }

    @Override
    public long deleteOldEntries() {

        long oldestToKeepTimestamp =
                timeService.currentTimeMillisUTC() - grapevineConfig.getSearchablePostTime().toMillis();
        log.debug("Deleting search posts older than {}", timeService.toLocalTimeHumanReadable(oldestToKeepTimestamp));
        final NativeSearchQuery deleteQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQuery()
                        .filter(rangeQuery("lastModified").lt(oldestToKeepTimestamp))
                        .filter(rangeQuery("lastCommented").lt(oldestToKeepTimestamp)))
                .build();
        final ByQueryResponse response =
                callElasticsearch(e -> e.delete(deleteQuery, SearchPost.class, getSearchPostIndex()));
        return response.getDeleted();
    }

    @Override
    public long reIndexFailedEntries() {

        //this is to get a ceil div result
        int maxIterations = (REINDEX_BATCH_SIZE + REINDEX_PAGE_SIZE - 1) / REINDEX_PAGE_SIZE;
        long syncedPosts = 0;
        for (int i = 1; i <= maxIterations; i++) {

            //all posts that have failed in the past
            //we always stay on the first page, since re-indexing them will make them disappear from the page
            final Page<Post> postsToSync = postRepository
                    .findAllBySearchIndexFailedTrueOrderByCreatedDesc(PageRequest.of(0, REINDEX_PAGE_SIZE));
            //this will also reset the failed flag
            postsToSync.forEach(this::indexOrUpdateIndexedPost);
            //we need to get this number on the first run, since otherwise the already re-indexed ones are subtracted
            if (i == 1) {
                syncedPosts = postsToSync.getTotalElements();
            }
            if (i == maxIterations) {
                log.warn("Not all posts where indexing failed could be indexed, found {} entries and processed {}",
                        syncedPosts,
                        REINDEX_BATCH_SIZE);
            }
            if (postsToSync.isEmpty()) {
                break;
            }
        }
        return syncedPosts;
    }

    private boolean checkPostVisible(Post post, String callerId, String homeAreaId, Set<String> geoAreaIds,
            Set<String> groupIds) {

        if (post.isDeleted()) {
            log.warn("Invalid search result: Post {} is deleted",
                    BaseEntity.getIdOf(post));
            return false;
        }
        if (Objects.equals(callerId, BaseEntity.getIdOf(post.getCreator()))) {
            //creator can always see its posts
            return true;
        }
        if (post.getGroup() != null) {
            //group post
            if (!groupIds.contains(post.getGroup().getId())) {
                //caller is not member of the group
                log.warn("Invalid search result: Caller {} is not member of group {} but {} is a group post",
                        callerId, post.getGroup().getId(), BaseEntity.getIdOf(post));
                return false;
            }
        } else {
            //non-group post
            if (!CollectionUtils.containsAny(geoAreaIds, BaseEntity.getIdsOf(post.getGeoAreas()))) {
                //post in not selected geo area
                log.warn("Invalid search result: Post {} is not in selected geo areas of caller {}",
                        BaseEntity.getIdOf(post), callerId);
                return false;
            }
        }
        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            //internal suggestion
            log.warn("Invalid search result: Post {} is an internal suggestion",
                    BaseEntity.getIdOf(post));
            return false;
        }
        if (post.isHiddenForOtherHomeAreas() && post.getGeoAreas().stream()
                .map(BaseEntity::getId)
                .noneMatch(geoAreaId -> StringUtils.equals(geoAreaId, homeAreaId))) {
            //hidden for other home areas and caller has a different home area
            log.warn(
                    "Invalid search result: Post {} is hidden for other home areas except {} and caller {} has home area {}",
                    BaseEntity.getIdOf(post), BaseEntity.getIdsOf(post.getGeoAreas()), callerId, homeAreaId);
            return false;
        }
        if (post instanceof ExternalPost && !((ExternalPost) post).isPublished()) {
            //unpublished post
            log.warn("Invalid search result: Post {} is an external post and not published",
                    BaseEntity.getIdOf(post));
            return false;
        }
        //no additional check
        return true;
    }

    private @Nullable
    SearchPost postToSearchPost(Post post) {

        if (post.isDeleted()) {
            return null;
        }

        final List<String> commentTexts = commentRepository.findAllCommentTextsByPostId(post.getId());

        final SearchPost searchPost = SearchPost.builder()
                .id(post.getId())
                .postType(post.getPostType())
                .text(post.getText())
                .lastModified(post.getLastModified())
                .lastCommented(post.getLastActivity())
                .address(post.getCustomAddress() == null ? null : post.getCustomAddress().toHumanReadableString(true))
                .commentTexts(commentTexts)
                .build();
        if (post.getCreator() != null) {
            searchPost.setCreatorId(BaseEntity.getIdOf(post.getCreator()));
        }
        if (post.getGroup() != null) {
            //group post, only visible to group members
            searchPost.setGroupId(post.getGroup().getId());
            return searchPost;
        }

        if (post instanceof Suggestion && ((Suggestion) post).isInternal()) {
            //we do not index internal suggestions
            return null;
        }
        if (post.isHiddenForOtherHomeAreas()) {
            //only visible to persons with the same home geo area id
            searchPost.setHomeGeoAreaIds(post.getGeoAreas().stream()
                    .map(BaseEntity::getId)
                    .toList());
            return searchPost;
        }

        //normal public post
        searchPost.setGeoAreaIds(post.getGeoAreas().stream()
                .map(BaseEntity::getId)
                .toList());
        return searchPost;
    }

    private IndexCoordinates getSearchPostIndex() {
        if (searchPostIndex == null) {
            searchPostIndex = callElasticsearch(e -> e.getIndexCoordinatesFor(SearchPost.class));
        }
        return searchPostIndex;
    }

    private <R> R callElasticsearchAndUpdatePost(Function<ElasticsearchOperations, R> function, Post post)
            throws SearchNotAvailableException {
        try {
            R result = callElasticsearch(function);
            if (Boolean.TRUE.equals(post.getSearchIndexFailed())) {
                //it is not set to null so that we can trace if a post was re-indexed
                post.setSearchIndexFailed(false);
                postRepository.save(post);
            }
            return result;
        } catch (Exception e) {
            post.setSearchIndexFailed(true);
            postRepository.save(post);
            throw e;
        }
    }

    private <R> R callElasticsearch(Function<ElasticsearchOperations, R> function) throws SearchNotAvailableException {
        try {
            return function.apply(elasticsearchOperations);
        } catch (Exception e) {
            log.error("Failed to call elasticsearch: {}", e.toString());
            throw new SearchNotAvailableException("Search is currently not available");
        }
    }

}
