/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentAlreadyLikedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentAlreadyUnlikedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.CommentNotLikedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ConcurrentRequestException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.LikeComment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.CommentRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.LikeCommentRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@Service
class LikeCommentService extends BaseEntityService<LikeComment> implements ILikeCommentService {

    @Autowired
    private LikeCommentRepository likeCommentRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public List<LikeComment> findAllByPersonOrderByCreatedAsc(Person person) {
        return likeCommentRepository.findAllByPersonOrderByCreatedAsc(person);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Comment updateCommentLikeCount(Comment comment) {
        final long newLikeCount = likeCommentRepository.countAllByCommentAndLikedTrue(comment);
        comment.setLikeCount(newLikeCount);
        commentRepository.updateLikeCount(comment, newLikeCount);
        return comment;
    }

    @Override
    public Set<Comment> filterLikedComments(Person person, Collection<Comment> commentsToCheck) {

        Set<Comment> commentsToCheckSet = new HashSet<>(commentsToCheck);

        Set<String> likedCommentIds = likeCommentRepository.findCommentIdByPersonAndPostInAndLikedTrue(person,
                Comment.toInSafeEntitySet(commentsToCheckSet));

        return commentsToCheck.stream()
                .filter(comment -> likedCommentIds.contains(comment.getId()))
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Comment likeComment(Comment comment, Person liker) {
        Optional<LikeComment> existingLikeComment = likeCommentRepository.findByCommentAndPerson(comment, liker);
        LikeComment likeComment;
        if (existingLikeComment.isPresent()) {
            likeComment = existingLikeComment.get();
            if (likeComment.isLiked()) {
                throw new CommentAlreadyLikedException("Comment '{}' already liked by user '{}'", comment.getId(), liker.getId());
            } else {
                likeComment.setLiked(true);
            }
        } else {
            likeComment = LikeComment.builder()
                    .comment(comment)
                    .person(liker)
                    .liked(true)
                    .build();
        }
        saveAndRetryOnDataIntegrityViolation(
                () -> likeCommentRepository.saveAndFlush(likeComment),
                () -> likeCommentRepository.findByCommentAndPersonAndLiked(comment, liker, true).orElseThrow(
                        () -> new ConcurrentRequestException("Concurrent request failed, could not like comment"))
        );
        return updateCommentLikeCount(comment);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Comment unlikeComment(Comment comment, Person unliker) {
        Optional<LikeComment> existingLikeComment = likeCommentRepository.findByCommentAndPerson(comment, unliker);
        final LikeComment likeComment = existingLikeComment.orElseThrow(() ->
                new CommentNotLikedException("Comment '{}' has not been liked by user '{}' before", comment.getId(),
                        unliker.getId()));
        if (!likeComment.isLiked()) {
            throw new CommentAlreadyUnlikedException("Comment '{}' already unliked by user '{}'", comment.getId(), unliker.getId());
        }
        likeComment.setLiked(false);
        saveAndRetryOnDataIntegrityViolation(
                () -> likeCommentRepository.saveAndFlush(likeComment),
                () -> likeCommentRepository.findByCommentAndPersonAndLiked(comment, unliker, false).orElseThrow(
                        () -> new ConcurrentRequestException("Concurrent request failed, could not unlike comment"))
        );
        return updateCommentLikeCount(comment);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void deleteAllByComment(Comment comment) {
        likeCommentRepository.deleteAllByComment(comment);
    }

}
