/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;

@Component
public class GrapevineUpdateParallelWorldStatusAdminTask extends BaseAdminTask {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private ITimeService timeService;

    @Override
    public String getName() {
        return "GrapevineUpdateParallelWorldStatus";
    }

    @Override
    public String getDescription() {
        return "Updates the parallel world status of all posts according to the status of their creator.";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {

        processPages(logSummary, parameters,
                page -> postRepository.findAllWithIncorrectParallelWorldStatus(
                        PersonStatus.PARALLEL_WORLD_INHABITANT.getBitMaskValue(), page),
                postPage -> {
                    for (Post post : postPage) {
                        if (post.getCreator() == null) {
                            continue;
                        }
                        boolean creatorIsInParallelWorld =
                                post.getCreator().getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
                        if (creatorIsInParallelWorld != post.isParallelWorld()) {
                            logSummary.info("Moved {} created {} to {}",
                                    StringUtils.rightPad(post.getPostType().toString(), 10),
                                    timeService.toLocalTimeHumanReadable(post.getCreated()),
                                    creatorIsInParallelWorld ? "parallel world" : "real world");
                            if (!parameters.isDryRun()) {
                                post.setParallelWorld(creatorIsInParallelWorld);
                                postRepository.save(post);
                            }
                        }
                    }
                });
    }

}
