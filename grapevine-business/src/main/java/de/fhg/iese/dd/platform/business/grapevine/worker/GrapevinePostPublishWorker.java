/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostPublishQueueService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
public class GrapevinePostPublishWorker  implements IWorkerTask {

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IPostPublishQueueService postPublishQueueService;

    @Override
    public Trigger getTaskTrigger() {
        //every 5th minute at 25s
        return new CronTrigger("25 */5 * * * *", timeService.getLocalTimeZone());
    }

    @Override
    public void run() {
        postPublishQueueService.publishToBePublishedPosts();
        postPublishQueueService.unpublishToBeUnpublishedPosts();
    }

}
