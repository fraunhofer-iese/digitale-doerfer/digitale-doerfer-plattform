/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.NewsSourceAlreadyExistsException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.NewsSourceNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.OrganizationNotAvailableException;
import de.fhg.iese.dd.platform.business.participants.geoarea.exceptions.GeoAreaNotFoundException;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceOrganizationMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class NewsSourceService extends BaseEntityService<NewsSource> implements INewsSourceService {

    @Autowired
    private NewsSourceRepository newsSourceRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private NewsSourceOrganizationMappingRepository newsSourceOrganizationMappingRepository;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAppService appService;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    private Map<String, NewsSource> newsSourceById;

    @PostConstruct
    private void initialize() {
        //this cache is replaced with a new map after it got cleared
        newsSourceById = Collections.emptyMap();
        cacheChecker.addCheckQuery(appService::checkCacheAppsAndAppVariants);
    }

    @Override
    public CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(NewsSource.class)
                .usedCache(IAppService.class)
                .build();
    }

    @Override
    public Page<NewsSource> findAllNotDeletedNewsSources(Pageable pageable) {

        checkAndRefreshCache();
        List<NewsSource> newsSourceSorted = newsSourceById.values().stream()
                .filter(NewsSource::isNotDeleted)
                .sorted(Comparator.comparing(NewsSource::getId))
                .toList();
        return toPage(newsSourceSorted, pageable);
    }


    @Override
    public NewsSource findNewsSourceById(String id) throws NewsSourceNotFoundException {

        NewsSource newsSource = findNewsSourceByIdIncludingDeleted(id);
        if (newsSource.isDeleted()) {
            throw NewsSourceNotFoundException.forIdDeleted(id);
        }
        return newsSource;
    }

    @Override
    public NewsSource findNewsSourceByIdIncludingDeleted(String id) throws NewsSourceNotFoundException {

        checkAndRefreshCache();
        NewsSource newsSource = newsSourceById.get(id);
        if (newsSource == null) {
            throw NewsSourceNotFoundException.forId(id);
        }
        return newsSource;
    }

    @Override
    public NewsSource findNewsSourceBySiteUrl(String siteUrl) throws NewsSourceNotFoundException {

        NewsSource newsSource = findNewsSourceBySiteUrlIncludingDeleted(siteUrl);
        if (newsSource.isDeleted()) {
            throw NewsSourceNotFoundException.forSiteUrlDeleted(siteUrl);
        }
        return newsSource;
    }

    private NewsSource findNewsSourceBySiteUrlIncludingDeleted(String siteUrl) throws NewsSourceNotFoundException {

        checkAndRefreshCache();
        String normalizedSiteUrl = normalizeSiteUrl(siteUrl);
        return newsSourceById.values().stream()
                .filter(newsSource -> normalizedSiteUrl.equals(newsSource.getSiteUrl()))
                .findFirst()
                .orElseThrow(() -> NewsSourceNotFoundException.forSiteUrl(normalizedSiteUrl));
    }

    @Override
    public NewsSource findNewsSourceByAppVariant(AppVariant appVariant) throws NewsSourceNotFoundException {

        NewsSource newsSource = findNewsSourceByAppVariantIncludingDeleted(appVariant);
        if (newsSource.isDeleted()) {
            throw NewsSourceNotFoundException.forAppVariantDeleted(appVariant.getAppVariantIdentifier());
        }
        return newsSource;
    }

    @Override
    public NewsSource findNewsSourceByAppVariantIncludingDeleted(AppVariant appVariant) throws NewsSourceNotFoundException {

        checkAndRefreshCache();
        return newsSourceById.values().stream()
                .filter(newsSource -> appVariant.equals(newsSource.getAppVariant()))
                .findFirst()
                .orElseThrow(() -> NewsSourceNotFoundException.forAppVariant(appVariant.getAppVariantIdentifier()));
    }

    @Override
    public List<Organization> findAllAvailableOrganizationsByNewsSource(NewsSource newsSource,
                                                                        Set<OrganizationTag> tags) {

        int tagBitMask = StorableEnumSet.toBitMaskValue(tags);
        return organizationRepository.findByNewsSourceOrderByName(newsSource, tagBitMask);
    }

    @Override
    public void checkOrganizationAvailable(NewsSource newsSource, Organization organization) throws OrganizationNotAvailableException {

        if (!newsSourceOrganizationMappingRepository.existsByNewsSourceAndOrganization(newsSource, organization)) {
            throw new OrganizationNotAvailableException("The organization '{}' is not available for news source '{}'",
                    organization.getId(), newsSource.getId());
        }
    }

    @Override
    public String checkConfiguration(String geoAreaId, String tenantId, String siteURL, String apiKey) {

        StringBuilder summary = new StringBuilder();
        AppVariant appVariant = null;
        try {
            NewsSource newsSource = findNewsSourceBySiteUrlIncludingDeleted(siteURL);
            if (newsSource.isDeleted()) {
                summary.append("❌ News source is already deleted and can't be used!\n");
            }
            if (StringUtils.isEmpty(apiKey)) {
                summary.append("❌ Empty api key, please add!\n");
            } else {
                appVariant = newsSource.getAppVariant();
                if (appVariant == null) {
                    summary.append("❌ News source has no app variant, " +
                            "please check if the news source is configured properly!\n");
                } else {
                    if (!StringUtils.equalsAny(apiKey, appVariant.getApiKey1(), appVariant.getApiKey2())) {
                        summary.append("❌ Wrong api key, please change!\n");
                    } else {
                        try {
                            appService.findAppVariantByApiKey(apiKey);
                            summary.append("✅ Valid and unique api key\n");
                        } catch (AppVariantNotFoundException e) {
                            summary.append("⚠️ Valid, but not unique api key, please change!\n");
                        }
                    }
                }
            }
        } catch (NewsSourceNotFoundException e) {
            summary.append("❌ News source can not be found: \"");
            summary.append(e.getMessage());
            summary.append("\", please check if it was created!\n");
        }
        if (appVariant != null) {
            summary.append("❓ Identified app variant, check if this matches: ");
            summary.append(appVariant.getAppVariantIdentifier());
            summary.append("\n");
        }

        try {
            GeoArea geoArea = geoAreaService.findGeoAreaById(geoAreaId);
            summary.append("❓ GeoArea found, check if this matches: ");
            summary.append(geoArea.getName());
            summary.append("\n");
            if (appVariant != null) {
                if (appService.getAvailableGeoAreasStrict(appVariant).contains(geoArea)) {
                    summary.append("✅ GeoArea is allowed for this news source\n");
                } else {
                    summary.append("⚠️ GeoArea is not allowed for this news source, " +
                            "please check if the news source is configured properly.\n");
                }
            }
        } catch (GeoAreaNotFoundException e) {
            summary.append("❌ GeoArea does not exist, please change!\n");
        }

        try {
            Tenant tenant = tenantService.findTenantById(tenantId);
            summary.append("❓ Tenant found, check if this matches: ");
            summary.append(tenant.getName());
            summary.append("\n");
            if (appVariant != null) {
                if (appService.getAvailableTenants(appVariant).contains(tenant)) {
                    summary.append("✅ Tenant is allowed for this news source\n");
                } else {
                    summary.append("⚠️ Tenant is not allowed for this news source, " +
                            "please check if the news source is configured properly.\n");
                }
            }
        } catch (TenantNotFoundException e) {
            summary.append("❌ Tenant does not exist, please change!\n");
        }
        return summary.toString();
    }

    @Override
    public void checkNewsSourceExists(String siteUrl) {

        checkAndRefreshCache();
        String normalizedSiteUrl = normalizeSiteUrl(siteUrl);
        Optional<NewsSource> existingNewsSource = newsSourceById.values().stream()
                .filter(newsSource -> !newsSource.isDeleted())
                .filter(newsSource -> normalizedSiteUrl.equals(newsSource.getSiteUrl()))
                .findFirst();

        if (existingNewsSource.isPresent()) {
            throw NewsSourceAlreadyExistsException.forAlreadyExists(siteUrl);
        }
    }

    @Override
    public String getExpandedNewsSourceSiteName(ExternalPost externalPost) {

        NewsSource newsSource = externalPost.getNewsSource();
        if (newsSource == null) {
            return null;
        }
        if (CollectionUtils.isEmpty(externalPost.getGeoAreas())) {
            return StringUtils.replaceEach(newsSource.getSiteName(),
                    new String[]{INewsSourceService.NEWS_SOURCE_VARIABLE_GEO_AREA_NAME},
                    new String[]{"-"});
        } else {
            String firstGeoAreaName = externalPost.getGeoAreas().stream()
                    .min(Comparator.comparing(GeoArea::getId))
                    .map(GeoArea::getName)
                    .orElse("-");
            return StringUtils.replaceEach(newsSource.getSiteName(),
                    new String[]{INewsSourceService.NEWS_SOURCE_VARIABLE_GEO_AREA_NAME},
                    new String[]{firstGeoAreaName});
        }
    }

    @Nonnull
    private String normalizeSiteUrl(String siteUrl) {

        return StringUtils.removeEnd(siteUrl, "/");
    }

    @Override
    public void invalidateCache() {

        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {
        // if the cache is stale or no fetch ever happened
        // get all news sources fresh from the database
        cacheChecker.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();

            newsSourceById = query(() -> newsSourceRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, NewsSource.class))
                    .map(this::useCachedAppVariant)
                    .collect(Collectors.toMap(NewsSource::getId, Function.identity())));
            log.info("Refreshed {} cached news sources in {} ms", newsSourceById.size(),
                    System.currentTimeMillis() - start);
        });
    }

    private NewsSource useCachedAppVariant(NewsSource newsSource) {
        //noinspection ConstantValue
        if (newsSource != null) {
            AppVariant appVariant = newsSource.getAppVariant();
            if (appVariant != null) {
                String appVariantId = appVariant.getId();
                try {
                    AppVariant cachedAppVariant = appService.findAppVariantByIdIncludingDeleted(appVariantId);
                    newsSource.setAppVariant(cachedAppVariant);
                } catch (AppVariantNotFoundException e) {
                    log.error("Could not find app variant '{}' of news source '{}'", appVariantId, newsSource.getId());
                }
            }
        }
        return newsSource;
    }

}
