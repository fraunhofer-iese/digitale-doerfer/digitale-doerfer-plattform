/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.ConvenienceNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

public interface IConvenienceService extends IEntityService<Convenience> {

    /**
     * Finds a convenience by id and configures it for the caller.
     *
     * @param convenienceId the id of the convenience
     * @param caller        the caller to configure the convenience for
     * @param appVariant    currently unused, but could be used to configure the convenience
     *
     * @return the configured convenience, if it exists
     *
     * @throws ConvenienceNotFoundException if there was no convenience with this id
     */
    Convenience findConvenienceById(String convenienceId, Person caller, AppVariant appVariant)
            throws ConvenienceNotFoundException;

    /**
     * Finds all conveniences for the user. These are all conveniences where one of selected geo areas of the user is in
     * the geo areas of the convenience. The resulting conveniences are configured for the caller.
     *
     * @param caller            the caller to find and configure the conveniences
     * @param callingAppVariant the app variant of the caller, used to find the selected geo areas
     *
     * @return all available and configured conveniences, ordered by name, never null.
     */
    List<Convenience> findAllAvailableConveniences(Person caller, AppVariant callingAppVariant);

    Convenience saveConvenience(Convenience convenience, Collection<GeoArea> geoAreas);

}
