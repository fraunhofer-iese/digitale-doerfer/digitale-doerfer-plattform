/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.DefaultTradingCategoryMustNotBeDeletedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.TradingCategoryNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.TradingCategory;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

import java.util.List;

public interface ITradingCategoryService extends IEntityService<TradingCategory>, ICachingComponent {

    TradingCategory findTradingCategoryById(String tradingCategoryId) throws TradingCategoryNotFoundException;

    List<TradingCategory> findAllTradingCategoriesInDataInitOrder();

    TradingCategory getDefaultTradingCategory();

    /**
     * Deletes the given category and updates all offers and seekings that referenced the given category to the default
     * category
     *
     * @param tradingCategory the category to delete
     * @param deleter the person starting the deletion
     * @throws DefaultTradingCategoryMustNotBeDeletedException when called with the default trading category
     */
    void deleteTradingCategory(TradingCategory tradingCategory, Person deleter);

}
