/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collections;
import java.util.Set;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;

/**
 * This is fake search service that does not do anything. It only returns a fake post with a hint that the search is
 * deactivated.
 */
@Service
@ConditionalOnProperty(
        value = "dd-platform.grapevine.search-activated",
        havingValue = "false",
        matchIfMissing = true)
class GrapevineSearchDeactivatedService extends BaseService implements IGrapevineSearchService {

    @Override
    public void ensureIndexExists(@Nullable LogSummary logSummary) {
        if (logSummary != null) {
            logSummary.warn("Search is deactivated");
        }
    }

    @Override
    public void checkIndex(LogSummary logSummary) {
        logSummary.warn("Search is deactivated");
    }

    @Override
    public void updateIndex(LogSummary logSummary) {
        logSummary.warn("Search is deactivated");
    }

    @Override
    public void indexOrUpdateIndexedPost(Post post) {
    }

    @Override
    public void indexOrUpdateIndexedComment(Comment comment) {
    }

    @Override
    public Page<Post> simpleSearch(String searchTerm, String creatorId, String homeAreaId, Set<String> geoAreaIds,
            Set<String> groupIds, Pageable pageable) {

        return new PageImpl<>(Collections.singletonList(Gossip.builder()
                .text("Search is deactivated")
                .build()),
                pageable, 1);
    }

    @Override
    public long deleteOldEntries() {
        return 0;
    }

    @Override
    public long reIndexFailedEntries() {
        return 0;
    }

}
