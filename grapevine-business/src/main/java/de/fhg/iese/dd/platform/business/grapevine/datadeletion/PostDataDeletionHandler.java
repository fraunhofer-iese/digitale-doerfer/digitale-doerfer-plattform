/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.datadeletion;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
public class PostDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private PostRepository postRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        //authors of posts
                        .referencedEntity(Person.class)
                        //groups of posts
                        .referencedEntity(Group.class)
                        .changedOrDeletedEntity(Post.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        //authors of posts
                        .referencedEntity(Person.class)
                        //groups of posts
                        .referencedEntity(Group.class)
                        .changedOrDeletedEntity(Post.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        long impactedPosts = postRepository.countByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} posts will move to new geo area", impactedPosts);
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {

        PageRequest pageRequest = PageRequest.of(0, 250);
        Page<? extends Post> page;
        do {
            page = postRepository.findAllByGeoAreasContainsOrderByCreatedDesc(
                    geoAreaToBeDeleted, pageRequest);
            for (Post post : page) {
                post.getGeoAreas().remove(geoAreaToBeDeleted);
                post.getGeoAreas().add(geoAreaToBeUsedInstead);
                post.setTenant(geoAreaToBeUsedInstead.getTenant());
            }
            postRepository.saveAll(page);
            pageRequest = pageRequest.next();
        } while (page.hasNext());
        logSummary.info("{} posts moved to new geo area and tenant", page.getTotalElements());
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        long impactedPosts = postRepository.countByTenant(tenantToBeDeleted);
        logSummary.info("{} posts will move to new tenant", impactedPosts);
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {
        int updatedPosts = postRepository.updateTenant(tenantToBeDeleted, tenantToBeUsedInstead);
        logSummary.info("{} posts moved to new tenant", updatedPosts);
    }

}
