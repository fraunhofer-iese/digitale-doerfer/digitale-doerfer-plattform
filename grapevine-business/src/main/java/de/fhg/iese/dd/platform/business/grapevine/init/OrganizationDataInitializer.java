/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import de.fhg.iese.dd.platform.business.grapevine.services.IOrganizationService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.DataInitKey;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationFact;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationPerson;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.repos.AppRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class OrganizationDataInitializer extends BaseDataInitializer {

    public static final String TOPIC = "organization";
    @Autowired
    private AppRepository appRepository;
    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private OrganizationRepository organizationRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                .requiredEntity(App.class)
                .requiredEntity(GeoArea.class)
                .processedEntity(Organization.class)
                .processedEntity(OrganizationPerson.class)
                .processedEntity(OrganizationGeoAreaMapping.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {
        final Set<Organization> predefinedOrganizations = createPredefinedOrganizations(logSummary);
        Set<Organization> createdOrganizations = new HashSet<>(predefinedOrganizations);
        checkOrphanOrganizations(createdOrganizations, logSummary);
    }

    private Set<Organization> createPredefinedOrganizations(LogSummary logSummary) {

        App dorfFunkApp = appRepository.findById(DorfFunkConstants.APP_ID).orElseThrow(
                () -> new DataInitializationException("Could not find DorfFunk app {}", DorfFunkConstants.APP_ID));
        final FileOwnership fileOwnership = FileOwnership.of(dorfFunkApp);
        Map<DataInitKey, List<Organization>> organizationsByDataInitKey =
                loadAllDataInitKeysAndEntities(Organization.class);
        DuplicateIdChecker<Organization> organizationDuplicateIdChecker = new DuplicateIdChecker<>();

        int numOrganizations = (int) organizationsByDataInitKey.values().stream()
                .collect(Collectors.summarizingInt(List::size)).getSum();
        logSummary.info("Creating {} organizations", numOrganizations);
        Set<Organization> createdOrganizations = new HashSet<>(numOrganizations);

        for (Map.Entry<DataInitKey, List<Organization>> organizations : organizationsByDataInitKey.entrySet()) {

            for (Organization organization : organizations.getValue()) {
                organization = checkId(organization);
                organizationDuplicateIdChecker.checkId(organization);
                organization = adjustCreated(organization);
                checkFieldNotNull(organization, Organization::getName, "name");
                checkFieldNotNull(organization, Organization::getLocationDescription, "locationDescription");
                checkFieldNotNull(organization, Organization::getDescription, "description");
                createImage(Organization::getOverviewImage, Organization::setOverviewImage, organization, fileOwnership,
                        logSummary, true);
                createImage(Organization::getLogoImage, Organization::setLogoImage, organization, fileOwnership,
                        logSummary, false);
                createImages(Organization::getDetailImages, Organization::setDetailImages, organization, fileOwnership,
                        logSummary, false);

                if (!CollectionUtils.isEmpty(organization.getOrganizationPersons())) {
                    for (OrganizationPerson organizationPerson : organization.getOrganizationPersons()) {
                        //this reference is not set in the data init
                        organizationPerson.setOrganization(organization);
                        //the id will be overwritten later, but we need it for a proper warning in the logs if the image is not found
                        organizationPerson.setId(organization.getId());
                        createImage(OrganizationPerson::getProfilePicture, OrganizationPerson::setProfilePicture,
                                organizationPerson, fileOwnership, logSummary, true);
                        //the id is not set in the data init and should not change every time
                        organizationPerson.withConstantId();
                    }
                }

                for (OrganizationFact fact : organization.getFacts()) {
                    if (StringUtils.isBlank(fact.getName()) || StringUtils.isBlank(fact.getValue())) {
                        throw new DataInitializationException(
                                "Fact of organization {} with name '{}' defines fact without name or value",
                                organization.getId(),
                                organization.getName());
                    }
                }

                if (organization.getAddress() != null) {
                    Address address = addressService.findOrCreateAddress(
                            IAddressService.AddressDefinition.fromAddress(organization.getAddress()),
                            IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY,
                            IAddressService.GPSResolutionStrategy.LOOKEDUP_NONE, false);
                    organization.setAddress(address);
                }

                Set<GeoArea> geoAreas = organizations.getKey().getGeoAreasIncluded();
                if (CollectionUtils.isEmpty(geoAreas)) {
                    throw new DataInitializationException("No geo areas defined for organization {} with name '{}'",
                            organization.getId(),
                            organization.getName());
                }
                if (CollectionUtils.isEmpty(organization.getOrganizationPersons())) {
                    organization.setOrganizationPersons(Collections.emptyList());
                }

                Organization createdOrganization;
                try {
                    //this also deletes old geo area mappings
                    createdOrganization = organizationService.saveOrganization(organization, geoAreas);
                } catch (Exception e) {
                    throw new DataInitializationException(
                            "Failed to save organization with id '" + organization.getId() + "'", e);
                }
                createdOrganizations.add(createdOrganization);
                logSummary.info("Created organization {} with name '{}', tags {} and geo areas '{}'",
                        organization.getId(),
                        organization.getName(),
                        organization.getTags().toString(),
                        geoAreas.stream()
                                .map(GeoArea::getName)
                                .collect(Collectors.joining(", ")));
            }
        }

        return createdOrganizations;
    }

    private void checkOrphanOrganizations(Set<Organization> createdOrganizations, LogSummary logSummary) {
        final List<Organization> orphanOrganizations =
                organizationRepository.findAllNotInOrderByName(Organization.toInSafeEntitySet(createdOrganizations));

        if (!CollectionUtils.isEmpty(orphanOrganizations)) {
            orphanOrganizations.forEach(c -> logSummary.warn(
                    "Organization {} with name '{}' was created before and not deleted, but does not exist in data init anymore",
                    c.getId(), c.getName()));
        }
    }

}
