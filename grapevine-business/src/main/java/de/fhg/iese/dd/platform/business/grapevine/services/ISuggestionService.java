/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.*;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionWorkerMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Set;

public interface ISuggestionService extends IEntityService<Suggestion> {

    List<Class<? extends BaseRole<Tenant>>> ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER =
            List.of(SuggestionFirstResponder.class, SuggestionWorker.class);

    /**
     * Finds a suggestion by id if a suggestion with this id exists and {@link Suggestion#isDeleted()} is false.
     *
     * @param suggestionId
     *         id of the suggestion to fetch
     * @param fetchExtendedSuggestion
     *         if {@link Suggestion#getSuggestionStatusRecords()} and
     *         {@link Suggestion#setSuggestionWorkerMappings(List)} should be set
     *
     * @return the suggestion with the given id
     *
     * @throws PostNotFoundException
     *         if no suggestion with that id exists or {@link Suggestion#isDeleted()} is true
     */
    Suggestion findSuggestionById(String suggestionId, boolean fetchExtendedSuggestion) throws PostNotFoundException;

    /**
     * Finds a suggestion by id if a suggestion with this id exists and {@link Suggestion#isDeleted()} is false or
     * {@link Suggestion#isOverrideDeletedForSuggestionRoles()} is true.
     *
     * @param suggestionId
     *         id of the suggestion to fetch
     * @param fetchExtendedSuggestion
     *         if {@link Suggestion#getSuggestionStatusRecords()} and {@link Suggestion#setSuggestionWorkerMappings(List)}
     *         should be set
     *
     * @return the suggestion with the given id
     *
     * @throws PostNotFoundException
     *         if no suggestion with that id exists or {@link Suggestion#isDeleted()} is true and
     *         {@link Suggestion#isOverrideDeletedForSuggestionRoles()} is false
     */
    Suggestion findSuggestionByIdIncludingOverrideDeleted(String suggestionId, boolean fetchExtendedSuggestion) throws PostNotFoundException;

    /**
     * Finds all suggestions in the given tenants where {@link Suggestion#isDeleted()} is false or
     * {@link Suggestion#isOverrideDeletedForSuggestionRoles()} is true.
     *
     * @param tenantIds         the tenants to find the suggestions for
     * @param startTime         all suggestions need to be created after that time
     * @param endTime           all suggestions need to be created before that time
     * @param includedStatus    all returned suggestions need to have one of these status. If empty, all status are
     *                          included.
     * @param excludedStatus    all returned suggestions can't have any of these statuses.
     * @param containedWorkerId optional id of the worker that needs to be assigned
     * @param unassigned        if true also unassigned suggestions are included
     * @param pagedQuery        controls the pagination and sorting
     *
     * @return all suggestions matching the criterion above in a paged way
     *
     * @throws InvalidSortingCriterionException if the page query was wrong
     */
    Page<Suggestion> findAllSuggestionsInTenantsIncludingOverrideDeleted(
            Set<String> tenantIds,
            long startTime,
            long endTime,
            Set<SuggestionStatus> includedStatus,
            Set<SuggestionStatus> excludedStatus,
            @Nullable String containedWorkerId,
            boolean unassigned,
            PagedQuery pagedQuery)
            throws InvalidSortingCriterionException;

    /**
     * Finds a suggestion by internal chat if this suggestion exists and {@link Suggestion#isDeleted()} is false.
     *
     * @param chat
     *         chat of the suggestion to fetch
     * @param fetchExtendedSuggestion
     *         if {@link Suggestion#getSuggestionStatusRecords()} and
     *         {@link Suggestion#setSuggestionWorkerMappings(List)} should be set
     *
     * @return the suggestion related with the given chat
     *
     * @throws PostNotFoundException
     *         if no suggestion with that chat exists or {@link Suggestion#isDeleted()} is true
     */
    Suggestion findSuggestionByInternalWorkerChatIncludingOverrideDeleted(Chat chat, boolean fetchExtendedSuggestion) throws PostNotFoundException;

    /**
     * Find all workers of a suggestion
     *
     * @param suggestion
     *         the suggestion to find the workers for
     *
     * @param includeInactive if true, workers with {@link SuggestionWorkerMapping#isActive()} == false are included
     * @return list of suggestion workers ordered by creation date asc or an empty list if there are none
     */
    List<SuggestionWorkerMapping> findSuggestionWorkers(Suggestion suggestion, boolean includeInactive);

    /**
     * Find all status records of a suggestion
     *
     * @param suggestion
     *         the suggestion to find the status records for
     *
     * @return list of suggestion status records ordered by creation date desc or an empty list if there are none
     */
    List<SuggestionStatusRecord> findSuggestionStatusRecords(Suggestion suggestion);

    /**
     * Add a worker to a suggestion
     *
     * @param suggestion
     *         the suggestion to add the worker to
     * @param worker
     *         the worker to add
     * @param initiator
     *         the person that wants to add the worker
     *
     * @return the updated suggestion
     *
     * @throws SuggestionWorkerAlreadyWorkingOnSuggestionException
     *         if the worker is already working on the suggestion
     */
    Suggestion addSuggestionWorker(Suggestion suggestion, Person worker, @Nullable Person initiator)
            throws SuggestionWorkerAlreadyWorkingOnSuggestionException;

    /**
     * Remove a worker from a suggestion
     *
     * @param suggestion
     *         the suggestion to remove the worker from
     * @param worker
     *         the worker to remove
     *
     * @return the updated suggestion
     *
     * @throws SuggestionWorkerNotWorkingOnSuggestionException
     *         if the worker is not working on the suggestion
     */
    Suggestion removeSuggestionWorker(Suggestion suggestion, Person worker)
            throws SuggestionWorkerNotWorkingOnSuggestionException;

    /**
     * Change the status of a suggestion. The current status {@link Suggestion#getSuggestionStatus()} is set, and the
     * new status is added as {@link SuggestionStatusRecord} to {@link Suggestion#getSuggestionStatusRecords()}.
     *
     * @param suggestion
     *         the suggestion to change the status of
     * @param newStatus
     *         the new status
     * @param initiator
     *         the person that wants to change the status
     *
     * @return the updated suggestion
     *
     * @throws SuggestionStatusChangeInvalidException
     *         if the status transition is not allowed
     */
    Suggestion changeSuggestionStatus(Suggestion suggestion, SuggestionStatus newStatus,
            @Nullable Person initiator) throws SuggestionStatusChangeInvalidException;

    /**
     * Deletes all suggestion status entries and worker mappings and sets the suggestion status to open.
     * Saves the suggestion and returns the updated entity.
     *
     * @param suggestion
     * @return
     * @throws SuggestionStatusChangeInvalidException
     */
    @NonNull
    Suggestion resetStatusAndWorkerForDemoSuggestion(Suggestion suggestion)
            throws SuggestionStatusChangeInvalidException;

    /**
     * Checks if the person has roles {@link SuggestionWorker} or {@link SuggestionFirstResponder} for the given tenant
     *
     * @param person the person to check
     * @param tenant the tenant for which the person should have the roles
     *
     * @throws NotAuthorizedException if the given person does not have one of the required roles, see {@link
     *                                ISuggestionService#ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER}
     */
    void checkSuggestionRoles(Person person, Tenant tenant) throws NotAuthorizedException;

    /**
     * Checks if the person has roles {@link SuggestionWorker} or {@link SuggestionFirstResponder} for any tenant
     *
     * @param person the person to check
     *
     * @return all role assignments for the roles {@link ISuggestionService#ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER}
     *         of the person
     *
     * @throws NotAuthorizedException if the given person does not have one of the required roles, see {@link
     *                                ISuggestionService#ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER}
     */
    Set<String> checkSuggestionRolesAndReturnTenantIds(Person person) throws NotAuthorizedException;

    /**
     * Checks if the person has roles {@link SuggestionWorker} or {@link SuggestionFirstResponder} for any tenant
     *
     * @param person the person to check
     *
     * @throws NotAuthorizedException if the given person does not have one of the required roles, see {@link
     *                                ISuggestionService#ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER}
     */
    void checkSuggestionRoles(Person person) throws NotAuthorizedException;

    /**
     * Checks if the given person can be added to a suggestion as a worker
     *
     * @param newWorker
     *         the person to check
     * @param tenant
     *         the tenant for which the person should have the roles
     *
     * @throws NotAuthorizedException
     *         if the given person does not have one of the required roles, see {@link
     *         ISuggestionService#ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER}
     */
    void checkNewWorker(Person newWorker, Tenant tenant) throws NotAuthorizedException;

}
