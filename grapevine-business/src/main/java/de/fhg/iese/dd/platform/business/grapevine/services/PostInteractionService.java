/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Dominik Schnier, Johannes Eveslage, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostAlreadyLikedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostAlreadyUnlikedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotLikedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ConcurrentRequestException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostInteraction;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostView;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
class PostInteractionService extends BaseEntityService<PostInteraction> implements IPostInteractionService {

    @Autowired
    private PostInteractionRepository postInteractionRepository;
    @Autowired
    private PostRepository postRepository;

    @Override
    public List<PostInteraction> findAllByPersonOrderByCreatedAsc(Person person) {
        return postInteractionRepository.findAllByPersonOrderByCreatedAsc(person);
    }

    @Override
    public Post updatePostLikeCount(Post post) {
        final long newLikeCount = postInteractionRepository.countAllByPostAndLikedTrue(post);
        post.setLikeCount(newLikeCount);
        postRepository.updateLikeCount(post, newLikeCount);
        return post;
    }

    @Override
    public boolean isPostLiked(@Nullable Person person, @Nullable Post post) {

        if (person == null || post == null) {
            return false;
        } else {
            return postInteractionRepository.existsLikePostByPostAndPersonAndLikedIsTrue(post, person);
        }
    }

    @Override
    public Set<Post> filterLikedPosts(@Nullable Person person, Collection<? extends Post> postsToCheck) {

        if (person == null) {
            return Collections.emptySet();
        }

        Set<Post> postsToCheckSet = new HashSet<>(postsToCheck);

        Set<String> likedPostIds = postInteractionRepository.findPostIdByPersonAndPostInAndLikedTrue(person,
                Post.toInSafeEntitySet(postsToCheckSet));

        return postsToCheck.stream()
                .filter(post -> likedPostIds.contains(post.getId()))
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Post likePost(Post post, Person liker) {

        Optional<PostInteraction> existingPostInteraction = postInteractionRepository.findByPostAndPerson(post, liker);
        PostInteraction postInteraction;
        if (existingPostInteraction.isPresent()) {
            postInteraction = existingPostInteraction.get();
            if (postInteraction.isLiked()) {
                throw new PostAlreadyLikedException("Post '{}' already liked by user '{}'", post.getId(),
                        liker.getId());
            } else {
                postInteraction.setLiked(true);
            }
        } else {
            postInteraction = PostInteraction.builder()
                    .post(post)
                    .person(liker)
                    .liked(true)
                    .build();
        }
        saveAndRetryOnDataIntegrityViolation(
                () -> postInteractionRepository.saveAndFlush(postInteraction),
                () -> postInteractionRepository.findByPostAndPersonAndLiked(post, liker, true).orElseThrow(
                        () -> new ConcurrentRequestException("Concurrent request failed, could not like post"))
        );
        return updatePostLikeCount(post);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Post unlikePost(Post post, Person unliker) {
        Optional<PostInteraction> existingPostInteraction =
                postInteractionRepository.findByPostAndPerson(post, unliker);
        final PostInteraction postInteraction = existingPostInteraction.orElseThrow(() ->
                new PostNotLikedException("Post '{}' has not been liked by user '{}' before", post.getId(),
                        unliker.getId()));
        if (!postInteraction.isLiked()) {
            throw new PostAlreadyUnlikedException("Post '{}' already unliked by user '{}'", post.getId(),
                    unliker.getId());
        }
        postInteraction.setLiked(false);
        saveAndRetryOnDataIntegrityViolation(
                () -> postInteractionRepository.saveAndFlush(postInteraction),
                () -> postInteractionRepository.findByPostAndPersonAndLiked(post, unliker, false).orElseThrow(
                        () -> new ConcurrentRequestException("Concurrent request failed, could not unlike post"))
        );
        return updatePostLikeCount(post);
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void deleteAllByPost(Post post) {
        postInteractionRepository.deleteAllByPost(post);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void updatePostViews(Person viewer, List<PostView> postViews) {
        for (PostView postView : postViews) {
            final Optional<PostInteraction> existingPostInteractionOpt =
                    postInteractionRepository.findByPostAndPerson(postView.getPost(), viewer);
            final PostInteraction updatedOrNewPostInteraction;
            if (existingPostInteractionOpt.isPresent()) {
                updatedOrNewPostInteraction = existingPostInteractionOpt.get();
                if (updatedOrNewPostInteraction.getViewedDetailTime() == null) {
                    updatedOrNewPostInteraction.setViewedDetailTime(postView.getViewedDetailTime());
                }
                if (updatedOrNewPostInteraction.getViewedOverviewTime() == null) {
                    updatedOrNewPostInteraction.setViewedOverviewTime(postView.getViewedOverviewTime());
                }
            } else {
                updatedOrNewPostInteraction = PostInteraction.builder()
                        .post(postView.getPost())
                        .person(viewer)
                        .viewedDetailTime(postView.getViewedDetailTime())
                        .viewedOverviewTime(postView.getViewedOverviewTime())
                        .build();
            }
            saveAndIgnoreIntegrityViolation(() -> postInteractionRepository.saveAndFlush(updatedOrNewPostInteraction),
                    () -> null);
        }
    }

}
