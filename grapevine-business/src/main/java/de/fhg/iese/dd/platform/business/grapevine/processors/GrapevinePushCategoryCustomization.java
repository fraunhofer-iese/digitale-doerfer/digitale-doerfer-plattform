/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Dominik Schnier, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.push.plugin.IPushCategoryCustomization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.*;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
class GrapevinePushCategoryCustomization implements IPushCategoryCustomization {

    @Autowired
    private IFeatureService featureService;

    @Override
    public List<PushCategory> customizePushCategories(List<PushCategory> pushCategories, Person requester,
            AppVariant appVariant) {

        FeatureTarget featureTarget = FeatureTarget.of(requester, appVariant);

        removePushCategoryIfFeatureDisabled(pushCategories, featureTarget,
                SuggestionCreationFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED);
        removePushCategoryIfFeatureDisabled(pushCategories, featureTarget,
                GroupFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_GROUP_MEMBER_STATUS_CHANGED);
        removePushCategoryIfFeatureDisabled(pushCategories, featureTarget,
                EmergencyAidOrganizationFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_EMERGENCY_AID_POST_CREATED);
        removePushCategoryIfFeatureDisabled(pushCategories, featureTarget,
                CivilProtectionOrganizationFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_CIVIL_PROTECTION_POST_CREATED_OR_UPDATED);

        customizePushCategory(pushCategories, featureTarget,
                GossipPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_GOSSIP_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                HappeningPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_HAPPENING_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                NewsItemPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_NEWSITEM_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                OfferPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_OFFER_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                SeekingPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SEEKING_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                SuggestionPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_CREATED);
        customizePushCategory(pushCategories, featureTarget,
                SuggestionPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SUGGESTION_STATUS_CHANGED);
        customizePushCategory(pushCategories, featureTarget,
                SpecialPostTypeFeature.class,
                DorfFunkConstants.PUSH_CATEGORY_ID_SPECIALPOST_CREATED);

        return pushCategories;
    }

    private void removePushCategoryIfFeatureDisabled(List<PushCategory> pushCategories, FeatureTarget featureTarget, Class<? extends BaseFeature> featureClass, PushCategory.PushCategoryId pushCategoryId) {

        if (!featureService.isFeatureEnabled(featureClass, featureTarget)) {
            pushCategories.removeIf(pushCategoryId::hasSameId);
        }
    }

    private void customizePushCategory(List<PushCategory> pushCategories, FeatureTarget featureTarget, Class<? extends PostTypeFeature<?>> postTypeFeatureClass,
            PushCategory.PushCategoryId pushCategoryId) {

        // do not execute if push category not in list
        PushCategory pushCategory = getPushCategoryById(pushCategories, pushCategoryId);
        if (pushCategory == null) {
            return;
        }
        
        try {
            PostTypeFeature<?> featureForPerson =
                    featureService.getFeature(postTypeFeatureClass, featureTarget);
            if (!featureForPerson.isEnabled()) {
                pushCategories.removeIf(pushCategoryId::hasSameId);
            } else {
                pushCategory.setName(setPlaceholder(pushCategory.getName(), featureForPerson));
                pushCategory.setDescription(setPlaceholder(pushCategory.getDescription(), featureForPerson));
            }
        } catch (FeatureNotFoundException e) {
            log.warn("Feature for customizing push category " + pushCategoryId + " not configured: {}", e.toString());
        }
    }

    private PushCategory getPushCategoryById(List<PushCategory> pushCategories, PushCategory.PushCategoryId pushCategoryId) {
        for (PushCategory pushCategory : pushCategories) {
            if (pushCategoryId.hasSameId(pushCategory)) {
                return pushCategory;
            }
        }
        return null;
    }

    private String setPlaceholder(String rawMessage, PostTypeFeature<?> postTypeFeature) {
        return StringUtils.replaceEach(rawMessage,
                new String[]{"$channelName$", "$itemName$"},
                new String[]{postTypeFeature.getChannelName(), postTypeFeature.getItemName()});
    }

}
