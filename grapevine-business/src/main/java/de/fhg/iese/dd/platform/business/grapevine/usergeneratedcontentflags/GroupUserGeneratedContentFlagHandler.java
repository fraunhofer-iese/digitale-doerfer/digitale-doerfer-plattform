/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Stefan Schweitzer
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.usergeneratedcontentflags;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.handlers.BaseUserGeneratedContentFlagHandler;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService.DeletedFlagReport;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;

@Component
public class GroupUserGeneratedContentFlagHandler extends BaseUserGeneratedContentFlagHandler {

    private static final Collection<Class<? extends BaseEntity>> PROCESSED_ENTITIES = unmodifiableList(Group.class);

    @Autowired
    protected IGroupService groupService;

    @Autowired
    private IAppService appService;

    @Autowired
    private GrapevineConfig grapevineConfig;

    @Override
    public Collection<Class<? extends BaseEntity>> getProcessedEntities() {
        return PROCESSED_ENTITIES;
    }

    @Override
    public DeletedFlagReport deleteFlaggedEntity(UserGeneratedContentFlag flag) {
        userGeneratedContentFlagService.checkEntityTypeOrSubtype(flag, Group.class);
        Group group = groupService.findGroupByIdIncludingDeleted(flag.getEntityId());
        Group deletedGroup = groupService.deleteGroup(group);
        notify(new GroupDeleteConfirmation(deletedGroup.getId()));

        return DeletedFlagReport.builder()
                .appName(appService.findById(DorfFunkConstants.APP_ID).getName())
                .reportedText(group.getName())
                .fromEmailAddressUserNotification(grapevineConfig.getSenderEmailAddressDorffunk())
                .build();
    }

    @Override
    public String getAdminUiDetailPagePath(UserGeneratedContentFlag flag) {
        return "flagged-contents/details/group/" + flag.getId();
    }

}
