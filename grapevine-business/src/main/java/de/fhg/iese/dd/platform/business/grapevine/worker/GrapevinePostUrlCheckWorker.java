/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostPublishQueueService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class GrapevinePostUrlCheckWorker implements IWorkerTask {

    private static final Marker LOG_MARKER_POST_URL_CHECK = MarkerManager.getMarker("POST_URL_CHECK");

    @Autowired
    private ITimeService timeService;

    @Autowired
    private IPostPublishQueueService postPublishQueueService;

    @Override
    public Trigger getTaskTrigger() {
        //every 21st minute at 33s
        return new CronTrigger("33 */21 * * * *", timeService.getLocalTimeZone());
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();
        log.debug(LOG_MARKER_POST_URL_CHECK, "checking post urls");
        postPublishQueueService.unpublishInvalidUrlPosts();
        log.debug(LOG_MARKER_POST_URL_CHECK, "finished triggering post url checks in {} ms",
                System.currentTimeMillis() - start);
    }

}
