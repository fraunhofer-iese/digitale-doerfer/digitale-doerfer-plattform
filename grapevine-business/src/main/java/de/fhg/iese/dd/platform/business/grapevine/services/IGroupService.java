/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupCannotBeDeletedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupContentNotAccessibleException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupGeoAreasInvalidException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupMembershipAdminRemovalNotPossibleException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupMembershipAlreadyPendingException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.NotMemberOfGroupException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import lombok.Builder;
import lombok.Value;

public interface IGroupService extends IEntityService<Group> {

    @Value
    @Builder
    class GroupGeoAreas {

        @Nullable
        GeoArea mainGeoArea;
        Set<GeoArea> includedGeoAreas;
        Set<GeoArea> excludedGeoAreas;

    }

    @Value
    @Builder
    class GroupGeoAreaIds {

        String mainGeoAreaId;
        Set<String> includedGeoAreaIds;
        Set<String> excludedGeoAreaIds;

    }

    Group findGroupById(String groupId) throws GroupNotFoundException;

    Group findGroupByIdIncludingDeleted(String groupId) throws GroupNotFoundException;

    Page<Group> findAllNotDeletedGroups(Pageable pageable);

    List<Group> findAllGroupsCreatedByPerson(Person person);

    Page<Group> findAllNotDeletedGroupsBelongingToTenant(Set<String> tenantIds, Pageable pageable);

    List<Group> findAllAvailableGroupsFilteredByGroupSettingsAndFeatureConfig(Person caller,
            AppVariant callingAppVariant);

    Group findGroupByIdFilteredByGroupSettings(String groupId, Person caller, AppVariant callingAppVariant)
            throws GroupNotFoundException;

    Page<Post> findAllPostsInGroup(Group group, PostType toPostType, Long startTime, Long endTime,
            PagedQuery pagedQuery);

    Page<Post> findAllPostsInMemberGroups(Person person, PostType toPostType, Long startTime, Long endTime,
            PagedQuery pagedQuery);

    Page<GroupMembership> findAllGroupMembershipsByGroup(Group group, Pageable pageable);

    List<GroupMembership> findAllGroupMembershipsByPerson(Person person);

    void checkGeoAreaConsistency(@Nullable GeoArea mainGeoArea, Set<GeoArea> includedGeoAreas,
            Set<GeoArea> excludedGeoAreas) throws GroupGeoAreasInvalidException;

    GroupGeoAreas setGroupGeoAreaMappings(Group group, @Nullable GeoArea mainGeoArea, Set<GeoArea> includedGeoAreas,
            Set<GeoArea> excludedGeoAreas) throws GroupGeoAreasInvalidException;

    Map<Group, GroupMembershipStatus> findGroupMembershipStatusByGroup(Collection<Group> groups, Person person);

    GroupMembershipStatus findGroupMembershipStatusForGroup(Group group, Person person);

    void checkIsGroupContentVisible(Group group, Person caller, AppVariant callingAppVariant)
            throws GroupContentNotAccessibleException;

    void checkIsGroupMember(Group group, Person person) throws NotMemberOfGroupException;

    void checkGroupMembershipAdmin(Person person, Group group) throws NotAuthorizedException;

    void checkIfGroupCanBeDeleted(Group group, boolean forceDeleteNonEmptyGroup) throws GroupCannotBeDeletedException;

    boolean isGroupMember(Group group, Person person);

    Map<String, GroupGeoAreaIds> findGeoAreaIdsByGroups(Collection<Group> groups);

    Map<String, GroupGeoAreas> findGeoAreasByGroups(Collection<Group> groups);

    GroupGeoAreaIds findGeoAreaIdsForGroup(Group group);

    GroupGeoAreas findGeoAreasForGroup(Group group);

    /**
     * Finds all group ids of the groups where the caller is accepted member of the group. If the result is empty a set
     * with a fake id is returned. See GroupService.emptyMemberGroupIdsToFixHQLBugOnEmptySet for details why this is
     * required
     */
    Set<String> findAllMemberGroupIds(Person caller);

    /**
     * Is only used by admins and for that reason, the person will be added to group without checking possible existing
     * groupMembershipStatus.
     */
    GroupMembership joinGroupByAdmin(Group group, Person personToJoin, String memberIntroductionText);

    GroupMembership joinGroupOrRequestToJoin(Group group, Person personToJoin, String memberIntroductionText)
            throws GroupMembershipAlreadyPendingException;

    Group addGroupMembershipAdminToGroup(Group group, Person adminToAdd);

    Group removeGroupMembershipAdminFromGroup(Group group, Person adminToRemove) throws
            GroupMembershipAdminRemovalNotPossibleException;

    Pair<GroupMembership, Boolean> acceptPendingGroupJoinRequest(Group group, Person personToAccept)
            throws NotMemberOfGroupException;

    Pair<GroupMembership, Boolean> rejectPendingGroupJoinRequest(Group group, Person personToDeny)
            throws NotMemberOfGroupException;

    Group updateGroupMemberCount(Group group);

    Group leaveGroup(Group group, Person personToLeave) throws NotMemberOfGroupException;

    /**
     * returns list of approved group members
     *
     * @param group
     *
     * @return
     */
    List<Person> findAllApprovedMembers(Group group);

    Page<Person> findAllApprovedMembers(Group group, Pageable pageable);

    long getPostCountOfGroup(Group group);

    Long findNewestPostCreatedTime(Group group);

    Long findNewestCommentCreatedTime(Group group);

    String generateShortName(String groupName);

    @Nullable
    GeoArea guessMainGeoArea(Collection<GeoArea> includedGeoAreas);

    Group deleteGroup(Group group);

}
