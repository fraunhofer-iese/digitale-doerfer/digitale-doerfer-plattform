/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import java.util.List;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class PostStatistics {

    private Post post;

    private long calculationTime;
    private int viewedOverviewCount;
    private int viewedDetailCount;
    private int likeCount;
    private int commentCount;
    private List<PostTimeStatistics> pastDaysStatistics;
    private List<PostDayTimeStatistics> dayTimeStatistics;
    private List<PostGeoAreaStatistics> geoAreaStatistics;

}
