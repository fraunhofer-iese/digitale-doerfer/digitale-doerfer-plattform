/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.events;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.DocumentItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@SuperBuilder
public abstract class BasePostChangeRequest<T extends Post> extends BaseEvent {

    @NonNull
    T post;

    private Person creator;
    private AppVariant appVariant;
    private String text;
    private Organization organization;
    private Address customAddress;
    private boolean removeCustomAddress;
    private List<MediaItemPlaceHolder> imageHolders;
    private List<DocumentItemPlaceHolder> documentHolders;
    /**
     * Null = do not change the current value
     */
    @Builder.Default
    private Boolean commentsDisallowed = null;
    private Map<String, Object> customAttributes;
    private boolean useEmptyFields;
    @Builder.Default
    private boolean updateLastActivity = false;
    @Nullable
    private String pushMessage;
    @Nullable
    private Boolean pushMessageLoud;
    @Nullable
    private PushCategory.PushCategoryId pushCategoryId;

}
