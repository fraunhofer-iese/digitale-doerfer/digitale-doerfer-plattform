/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.NewsSourceNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.OrganizationNotAvailableException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ExternalPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

public interface INewsSourceService extends IEntityService<NewsSource>, ICachingComponent {

    String NEWS_SOURCE_VARIABLE_GEO_AREA_NAME = "$post.geoArea.name$";

    Page<NewsSource> findAllNotDeletedNewsSources(Pageable pageable);

    NewsSource findNewsSourceById(String sourceId) throws NewsSourceNotFoundException;

    NewsSource findNewsSourceByIdIncludingDeleted(String id) throws NewsSourceNotFoundException;

    NewsSource findNewsSourceBySiteUrl(String siteUrl) throws NewsSourceNotFoundException;

    NewsSource findNewsSourceByAppVariant(AppVariant appVariant) throws NewsSourceNotFoundException;

    NewsSource findNewsSourceByAppVariantIncludingDeleted(AppVariant appVariant) throws NewsSourceNotFoundException;

    /**
     * Finds all organizations for the news source.
     *
     * @param newsSource the news source to find and configure the organizations
     * @param tags       the required tags the organizations need to have (any of)
     * @return all available and configured organizations with any of the tags, ordered by name, never null.
     */
    List<Organization> findAllAvailableOrganizationsByNewsSource(NewsSource newsSource, Set<OrganizationTag> tags);

    void checkOrganizationAvailable(NewsSource newsSource, Organization organization) throws OrganizationNotAvailableException;

    String checkConfiguration(String geoAreaId, String tenantId, String siteURL, String apiKey);

    void checkNewsSourceExists(String siteUrl);

    @Nullable
    String getExpandedNewsSourceSiteName(ExternalPost externalPost);

}
