/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningChangeByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningChangeByPersonRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningCreateByNewsSourceRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningCreateByPersonRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationConfirmConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationConfirmRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationRevokeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.happening.HappeningParticipationRevokeRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.IHappeningService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;

@EventProcessor
class HappeningEventProcessor extends BasePostEventProcessor {

    @Autowired
    private IHappeningService happeningService;

    @EventProcessing
    private PostCreateConfirmation<Happening> handleHappeningCreateByPersonRequest(
            HappeningCreateByPersonRequest request) {

        return handleBasePostCreateRequest(request, Happening.builder()
                .startTime(request.getStartTime())
                .endTime(request.getEndTime())
                .allDayEvent(request.isAllDayEvent())
                .build());
    }

    @EventProcessing
    private PostChangeConfirmation<Happening> handleHappeningChangeRequest(HappeningChangeByPersonRequest request) {

        Happening post = request.getPost();

        if (request.getStartTime() != null) {
            post.setStartTime(request.getStartTime());
        }
        if (request.getEndTime() != null) {
            post.setEndTime(request.getEndTime());
        }
        if (request.getAllDayEvent() != null) {
            post.setAllDayEvent(request.getAllDayEvent());
        }
        return changePost(request, post);
    }

    @EventProcessing
    private PostCreateConfirmation<Happening> handleHappeningCreateByNewsSourceRequest(
            HappeningCreateByNewsSourceRequest request) {

        return handleExternalPostCreateByNewsSourceRequest(request, Happening.builder()
                .organizer(request.getOrganizer())
                .startTime(request.getStartTime())
                .endTime(request.getEndTime())
                .allDayEvent(request.isAllDayEvent())
                .build());
    }

    @EventProcessing
    private List<BaseEvent> handleHappeningChangeByNewsSourceRequest(
            HappeningChangeByNewsSourceRequest request) {

        Happening post = request.getPost();
        post.setOrganizer(request.getOrganizer());
        post.setStartTime(request.getStartTime());
        post.setEndTime(request.getEndTime());
        post.setAllDayEvent(request.isAllDayEvent());
        return handleExternalPostChangeByNewsSourceRequest(request, post);
    }

    @EventProcessing
    private HappeningParticipationConfirmConfirmation handleHappeningParticipationConfirmRequest(
            HappeningParticipationConfirmRequest request) {

        final Happening updatedHappening =
                happeningService.confirmParticipation(request.getHappening(), request.getPersonToParticipate());
        return HappeningParticipationConfirmConfirmation.builder()
                .updatedHappening(updatedHappening)
                .build();
    }

    @EventProcessing
    private HappeningParticipationRevokeConfirmation handleHappeningParticipationRevokeRequest(
            HappeningParticipationRevokeRequest request) {

        final Happening updatedHappening =
                happeningService.revokeParticipation(request.getHappening(), request.getPersonToCancel());
        return HappeningParticipationRevokeConfirmation.builder()
                .updatedHappening(updatedHappening)
                .build();
    }

}
