/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.StatusConflictException;

public class GroupMembershipAlreadyPendingException extends StatusConflictException {

    private static final long serialVersionUID = 4867544550243636931L;

    public GroupMembershipAlreadyPendingException(String groupId, String personId) {
        super("Group {} is already in membership status PENDING for person {}", groupId, personId);
    }

    @Override
    public ClientExceptionType getClientExceptionType(){
        return ClientExceptionType.GROUP_MEMBERSHIP_ALREADY_PENDING;
    }

}
