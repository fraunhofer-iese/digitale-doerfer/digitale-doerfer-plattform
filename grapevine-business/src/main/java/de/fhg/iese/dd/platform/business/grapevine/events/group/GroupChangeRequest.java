/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.events.group;

import java.util.Set;

import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupAccessibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupContentVisibility;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupVisibility;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemPlaceHolder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class GroupChangeRequest extends BaseEvent {

    private Group group;
    private Person changingPerson;
    private String newName;
    private String newShortName;
    private String newDescription;
    private GroupVisibility newGroupVisibility;
    private GroupAccessibility newGroupAccessibility;
    private GroupContentVisibility newGroupContentVisibility;
    private GeoArea newMainGeoArea;
    private Set<GeoArea> newIncludedGeoAreas;
    private Set<GeoArea> newExcludedGeoAreas;
    private MediaItemPlaceHolder logoPlaceHolder;

}
