/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.OrganizationNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class OrganizationService extends BaseEntityService<Organization> implements IOrganizationService {

    @Autowired
    private IAppService appService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private OrganizationGeoAreaMappingRepository organizationGeoAreaMappingRepository;

    @Override
    public Collection<String> getRelatedGeoAreaIds(Organization organization) {

        return organizationGeoAreaMappingRepository.findGeoAreaIdsByOrganization(organization);
    }

    @Override
    public Organization findOrganizationById(String organizationId) throws OrganizationNotFoundException {

        return organizationRepository.findByIdAndDeletedFalse(organizationId)
                .orElseThrow(() -> new OrganizationNotFoundException(organizationId));
    }

    @Override
    public List<Organization> findAllAvailableOrganizations(Person caller, AppVariant callingAppVariant,
                                                            Set<OrganizationTag> tags) {

        Set<String> selectedGeoAreaIds =
                BaseEntity.toInSafeIdSet(appService.getSelectedGeoAreaIds(callingAppVariant, caller));
        int tagBitMask = StorableEnumSet.toBitMaskValue(tags);
        return organizationRepository.findByGeoAreaInOrderByName(selectedGeoAreaIds, tagBitMask);
    }

    @Override
    @Transactional
    public Organization saveOrganization(Organization organization, Collection<GeoArea> geoAreas) {

        //ensure all the contained persons reference the right entity
        if (!CollectionUtils.isEmpty(organization.getOrganizationPersons())) {
            organization.getOrganizationPersons().forEach(vp -> vp.setOrganization(organization));
        }

        //we need to delete existing detail images before we save the new ones, since they are not under orphan control
        final Runnable orphanMediaItemDeletion = mediaItemService
                .getOrphanItemDeletion(
                        organizationRepository.findById(organization.getId())
                                .map(Organization::getDetailImages)
                                .orElse(null),
                        organization.getDetailImages());

        final Organization savedOrganization = organizationRepository.saveAndFlush(organization);

        //these are old ones that are not needed anymore
        organizationGeoAreaMappingRepository.deleteAllByOrganizationAndGeoAreaNotIn(organization,
                GeoArea.toInSafeEntityCollection(geoAreas));

        //these are new ones that might overwrite existing ones
        final Set<OrganizationGeoAreaMapping> geoAreaMappings = geoAreas.stream()
                //they get the same id, so that existing ones get overwritten
                .map(g -> OrganizationGeoAreaMapping.builder()
                        .organization(organization)
                        .geoArea(g)
                        .build()
                        .withConstantId())
                .collect(Collectors.toSet());
        organizationGeoAreaMappingRepository.saveAll(geoAreaMappings);
        organizationGeoAreaMappingRepository.flush();

        //we delete the orphans at the end
        orphanMediaItemDeletion.run();
        return savedOrganization;
    }

    @Override
    @Transactional
    public Organization store(Organization entity) {
        return saveOrganization(entity, Collections.emptyList());
    }

}
