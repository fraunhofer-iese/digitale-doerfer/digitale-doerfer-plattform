/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2022 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupAddGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupAddGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeNameByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeNameByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeVisibilityAndAccessibilityByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeVisibilityAndAccessibilityByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupLeaveConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupLeaveRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupModifyGeoAreasByAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupModifyGeoAreasByAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupRemoveGroupMembershipAdminConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupRemoveGroupMembershipAdminRequest;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.grapevine.services.IPostService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@EventProcessor
class GroupEventProcessor extends BaseEventProcessor {

    @Autowired
    private IGroupService groupService;
    @Autowired
    private IPostService postService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IAppService appService;

    @Autowired
    private IRoleService roleService;

    @EventProcessing
    private GroupCreateByAdminConfirmation handleGroupCreateByAdminRequest(
            GroupCreateByAdminRequest groupCreateByAdminRequest) {

        final Tenant tenant = groupCreateByAdminRequest.getTenant();
        Group group = Group.builder()
                .name(groupCreateByAdminRequest.getName())
                .shortName(groupCreateByAdminRequest.getShortName())
                .tenant(tenant)
                .visibility(groupCreateByAdminRequest.getGroupVisibility())
                .accessibility(groupCreateByAdminRequest.getGroupAccessibility())
                .contentVisibility(groupCreateByAdminRequest.getGroupContentVisibility())
                .deleted(false)
                .memberCount(0)
                .build();
        final Group createdGroup = groupService.store(group);

        // assign group admin role and add to group
        final List<Person> groupMembershipAdmins = groupCreateByAdminRequest.getGroupMembershipAdminsToJoin().stream()
                .peek(p -> groupService.addGroupMembershipAdminToGroup(createdGroup, p))
                .collect(Collectors.toList());

        final IGroupService.GroupGeoAreas groupGeoAreas =
                groupService.setGroupGeoAreaMappings(createdGroup, null,
                        groupCreateByAdminRequest.getIncludedGeoAreas(),
                        groupCreateByAdminRequest.getExcludedGeoAreas());
        final Group updatedGroupWithMembers = groupService.findGroupById(createdGroup.getId());

        return GroupCreateByAdminConfirmation.builder()
                .createdGroup(updatedGroupWithMembers)
                .groupGeoAreas(groupGeoAreas)
                .groupMembershipAdmins(groupMembershipAdmins)
                .build();
    }

    @EventProcessing
    private GroupDeleteByAdminConfirmation handleGroupDeleteByAdminRequest(
            GroupDeleteByAdminRequest groupDeleteByAdminRequest) {

        Group groupToDelete = groupDeleteByAdminRequest.getGroup();

        groupService.checkIfGroupCanBeDeleted(groupToDelete, groupDeleteByAdminRequest.isForceDeleteNonEmptyGroup());

        postService.deleteAllPostsOfGroup(groupToDelete);
        final Group deletedGroup = groupService.deleteGroup(groupToDelete);

        return GroupDeleteByAdminConfirmation.builder()
                .deletedGroupId(deletedGroup.getId())
                .build();
    }

    @EventProcessing
    private GroupCreateConfirmation handleGroupCreateRequest(GroupCreateRequest groupCreateRequest) {

        Group group = Group.builder()
                .creator(groupCreateRequest.getCreator())
                .name(groupCreateRequest.getName())
                .description(groupCreateRequest.getDescription())
                .tenant(groupCreateRequest.getTenant())
                .visibility(groupCreateRequest.getGroupVisibility())
                .accessibility(groupCreateRequest.getGroupAccessibility())
                .contentVisibility(groupCreateRequest.getGroupContentVisibility())
                .deleted(false)
                .memberCount(0)
                .build();
        if (groupCreateRequest.getLogo() != null) {
            group.setLogo(mediaItemService.useItem(groupCreateRequest.getLogo(),
                    FileOwnership.of(appService.findById(DorfFunkConstants.APP_ID))));
        }
        if (StringUtils.isBlank(groupCreateRequest.getShortName())) {
            group.setShortName(groupService.generateShortName(groupCreateRequest.getName()));
        } else {
            group.setShortName(groupCreateRequest.getShortName());
        }

        final Group createdGroup = groupService.store(group);

        // assign group admin role
        final GroupMembership adminMembership =
                groupService.joinGroupByAdmin(createdGroup, groupCreateRequest.getCreator(), "Group creator");
        roleService.assignRoleToPerson(groupCreateRequest.getCreator(), GroupMembershipAdmin.class, createdGroup);

        final IGroupService.GroupGeoAreas groupGeoAreas = groupService.setGroupGeoAreaMappings(createdGroup,
                groupCreateRequest.getMainGeoArea(),
                groupCreateRequest.getIncludedGeoAreas(),
                groupCreateRequest.getExcludedGeoAreas());
        final Group updatedGroupWithMembers = groupService.findGroupById(createdGroup.getId());

        return GroupCreateConfirmation.builder()
                .createdGroup(updatedGroupWithMembers)
                .groupMembershipStatus(adminMembership.getStatus())
                .groupGeoAreas(groupGeoAreas)
                .build();
    }

    @EventProcessing
    private GroupDeleteConfirmation handleGroupDeleteRequest(GroupDeleteRequest groupDeleteRequest) {

        Group groupToDelete = groupDeleteRequest.getGroup();

        postService.deleteAllPostsOfGroup(groupToDelete);
        final Group deletedGroup = groupService.deleteGroup(groupToDelete);

        return new GroupDeleteConfirmation(deletedGroup.getId());
    }

    @EventProcessing
    private GroupChangeConfirmation handleGroupChangeRequest(GroupChangeRequest groupChangeRequest) {

        Group group = groupChangeRequest.getGroup();
        group.setName(groupChangeRequest.getNewName());
        if (StringUtils.isBlank(groupChangeRequest.getNewShortName())) {
            group.setShortName(groupService.generateShortName(groupChangeRequest.getNewName()));
        } else {
            group.setShortName(groupChangeRequest.getNewShortName());
        }
        group.setDescription(groupChangeRequest.getNewDescription());
        group.setVisibility(groupChangeRequest.getNewGroupVisibility());
        group.setAccessibility(groupChangeRequest.getNewGroupAccessibility());
        group.setContentVisibility(groupChangeRequest.getNewGroupContentVisibility());

        MediaItem logo = null;
        MediaItem oldLogo = group.getLogo();
        boolean changedLogo = true;
        MediaItemPlaceHolder logoPlaceholder = groupChangeRequest.getLogoPlaceHolder();
        if (logoPlaceholder != null) {
            // logo stays the same
            if (logoPlaceholder.getMediaItem() != null) {
                logo = logoPlaceholder.getMediaItem();
                changedLogo = false;
            } else {    // change logo
                logo = mediaItemService.useItem(logoPlaceholder.getTemporaryMediaItem(),
                        FileOwnership.of(appService.findById(DorfFunkConstants.APP_ID)));
            }
        }

        group.setLogo(logo);
        group = groupService.store(group);

        if (changedLogo && oldLogo != null) {
            mediaItemService.deleteItem(oldLogo);
        }

        final IGroupService.GroupGeoAreas groupGeoAreas = groupService.setGroupGeoAreaMappings(group,
                groupChangeRequest.getNewMainGeoArea(),
                groupChangeRequest.getNewIncludedGeoAreas(),
                groupChangeRequest.getNewExcludedGeoAreas());

        return GroupChangeConfirmation.builder()
                .updatedGroup(group)
                .groupGeoAreas(groupGeoAreas)
                .build();
    }

    @EventProcessing(includeSubtypesOfEvent = true)
    private GroupJoinConfirmation handleGroupJoinRequest(GroupJoinRequest groupJoinRequest) {

        final Group group = groupJoinRequest.getGroup();
        final Person personToJoin = groupJoinRequest.getPersonToJoin();
        final String memberIntroductionText = groupJoinRequest.getMemberIntroductionText();

        GroupMembership groupMembership;
        if (groupJoinRequest instanceof GroupJoinByAdminRequest) {
            groupMembership = groupService.joinGroupByAdmin(group, personToJoin, memberIntroductionText);
        } else {
            groupMembership = groupService.joinGroupOrRequestToJoin(group, personToJoin, memberIntroductionText);
        }

        return GroupJoinConfirmation.builder()
                .membership(groupMembership)
                .build();
    }

    @EventProcessing
    private GroupLeaveConfirmation handleGroupLeaveRequest(GroupLeaveRequest groupLeaveRequest) {

        Group group = groupLeaveRequest.getGroup();
        Person personToLeave = groupLeaveRequest.getPersonToLeave();

        Group updatedGroup = groupService.leaveGroup(group, personToLeave);

        return GroupLeaveConfirmation.builder()
                .group(updatedGroup)
                .membershipStatus(GroupMembershipStatus.NOT_MEMBER)
                .personLeft(personToLeave)
                .build();
    }

    @EventProcessing
    private GroupPendingJoinAcceptConfirmation handleGroupPendingJoinAcceptRequest(
            GroupPendingJoinAcceptRequest groupPendingJoinAcceptRequest) {

        Group group = groupPendingJoinAcceptRequest.getGroup();
        Person personToAccept = groupPendingJoinAcceptRequest.getPersonToAccept();

        Pair<GroupMembership, Boolean>
                groupMembershipAndStatusChanged = groupService.acceptPendingGroupJoinRequest(group, personToAccept);

        return GroupPendingJoinAcceptConfirmation.builder()
                .group(groupMembershipAndStatusChanged.getLeft().getGroup())
                .acceptingPerson(groupPendingJoinAcceptRequest.getAcceptingPerson())
                .personToAccept(groupPendingJoinAcceptRequest.getPersonToAccept())
                .groupMembershipStatus(groupMembershipAndStatusChanged.getLeft().getStatus())
                .groupMembershipStatusChanged(groupMembershipAndStatusChanged.getRight())
                .build();
    }

    @EventProcessing
    private GroupPendingJoinDenyConfirmation handleGroupPendingJoinDenyRequest(
            GroupPendingJoinDenyRequest groupPendingJoinDenyRequest) {

        Group group = groupPendingJoinDenyRequest.getGroup();
        Person personToDeny = groupPendingJoinDenyRequest.getPersonToDeny();

        Pair<GroupMembership, Boolean> groupMembershipAndStatusChanged =
                groupService.rejectPendingGroupJoinRequest(group, personToDeny);

        return GroupPendingJoinDenyConfirmation.builder()
                .group(groupMembershipAndStatusChanged.getLeft().getGroup())
                .denyingPerson(groupPendingJoinDenyRequest.getDenyingPerson())
                .personToDeny(groupPendingJoinDenyRequest.getPersonToDeny())
                .groupMembershipStatus(groupMembershipAndStatusChanged.getLeft().getStatus())
                .groupMembershipStatusChanged(groupMembershipAndStatusChanged.getRight())
                .build();
    }

    @EventProcessing
    private GroupAddGroupMembershipAdminConfirmation handleGroupAddGroupMembershipAdminRequest(
            GroupAddGroupMembershipAdminRequest groupAddGroupMembershipAdminRequest) {

        final Group group = groupAddGroupMembershipAdminRequest.getGroup();
        final Person membershipAdminToAdd = groupAddGroupMembershipAdminRequest.getMembershipAdminToAdd();

        final Group updatedGroup = groupService.addGroupMembershipAdminToGroup(group, membershipAdminToAdd);
        final List<Person> groupMembershipAdmins =
                roleService.getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class, group);

        return GroupAddGroupMembershipAdminConfirmation.builder()
                .updatedGroup(updatedGroup)
                .groupMembershipAdmins(groupMembershipAdmins)
                .build();
    }

    @EventProcessing
    private GroupRemoveGroupMembershipAdminConfirmation handleGroupRemoveGroupMembershipAdminRequest(
            GroupRemoveGroupMembershipAdminRequest groupRemoveGroupMembershipAdminRequest) {

        final Group group = groupRemoveGroupMembershipAdminRequest.getGroup();
        final Person membershipAdminToRemove = groupRemoveGroupMembershipAdminRequest.getMembershipAdminToRemove();

        final Group updatedGroup = groupService.removeGroupMembershipAdminFromGroup(group, membershipAdminToRemove);
        final List<Person> groupMembershipAdmins =
                roleService.getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class, group);

        return GroupRemoveGroupMembershipAdminConfirmation.builder()
                .updatedGroup(updatedGroup)
                .groupMembershipAdmins(groupMembershipAdmins)
                .build();
    }

    @EventProcessing
    private GroupModifyGeoAreasByAdminConfirmation handleGroupModifyGeoAreasByAdminRequest(
            GroupModifyGeoAreasByAdminRequest groupModifyGeoAreasByAdminRequest) {

        final Group group = groupModifyGeoAreasByAdminRequest.getGroup();
        final Set<GeoArea> includedGeoAreas = groupModifyGeoAreasByAdminRequest.getIncludedGeoAreas();
        final Set<GeoArea> excludedGeoAreas = groupModifyGeoAreasByAdminRequest.getExcludedGeoAreas();

        final IGroupService.GroupGeoAreas groupGeoAreas =
                groupService.setGroupGeoAreaMappings(group, null, includedGeoAreas, excludedGeoAreas);

        return GroupModifyGeoAreasByAdminConfirmation.builder()
                .group(group)
                .groupGeoAreas(groupGeoAreas)
                .build();
    }

    @EventProcessing
    private GroupChangeNameByAdminConfirmation handleGroupChangeNameByAdminRequest(
            GroupChangeNameByAdminRequest groupChangeNameByAdminRequest) {

        Group group = groupChangeNameByAdminRequest.getGroup();
        group.setName(groupChangeNameByAdminRequest.getNewName());
        group.setShortName(groupChangeNameByAdminRequest.getNewShortName());
        group = groupService.store(group);

        return GroupChangeNameByAdminConfirmation.builder()
                .updatedGroup(group)
                .build();
    }

    @EventProcessing
    private GroupChangeVisibilityAndAccessibilityByAdminConfirmation handleGroupChangeVisibilityAndAccessibilityByAdminRequest(
            GroupChangeVisibilityAndAccessibilityByAdminRequest groupChangeVisibilityAndAccessibilityByAdminRequest) {

        Group group = groupChangeVisibilityAndAccessibilityByAdminRequest.getGroup();
        group.setVisibility(groupChangeVisibilityAndAccessibilityByAdminRequest.getGroupVisibility());
        group.setAccessibility(groupChangeVisibilityAndAccessibilityByAdminRequest.getGroupAccessibility());
        group.setContentVisibility(groupChangeVisibilityAndAccessibilityByAdminRequest.getGroupContentVisibility());
        group = groupService.store(group);

        return GroupChangeVisibilityAndAccessibilityByAdminConfirmation.builder()
                .updatedGroup(group)
                .build();
    }

}
