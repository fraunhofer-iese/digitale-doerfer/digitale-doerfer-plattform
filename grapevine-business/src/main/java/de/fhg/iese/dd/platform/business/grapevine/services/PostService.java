/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Johannes Schneider, Balthasar Weitzel, Adeline Silva Schäfer, Stefan Schweitzer, Dominik Schnier, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupContentNotAccessibleException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.GroupNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.InvalidSortingCriterionException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.enums.StorableEnumSet;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

@Service
class PostService extends BasePostService<Post> implements IPostService {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private GossipRepository gossipRepository;
    @Autowired
    private SeekingRepository seekingRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private SpecialPostRepository specialPostRepository;
    @Autowired
    private ExternalPostRepository externalPostRepository;
    @Autowired
    private NewsItemRepository newsItemRepository;
    @Autowired
    private HappeningRepository happeningRepository;

    @Autowired
    private IGroupService groupService;
    @Autowired
    private IPostInteractionService postInteractionService;
    @Autowired
    private IHappeningService happeningService;
    @Autowired
    private IGrapevineSearchService grapevineSearchService;

    @Override
    public Post findPostByIdNotDeleted(String postId) throws PostNotFoundException {
        return postRepository.findByIdAndDeletedFalse(postId)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Post findPostByIdIncludingDeleted(String postId) throws PostNotFoundException {
        return postRepository.findById(postId)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Post findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(String postId, Person caller,
            AppVariant callingAppVariant) throws PostNotFoundException {
        Post post = postRepository.findByIdAndNotDeletedFilteredByHiddenForOtherHomeAreas(postId, caller,
                        caller.getHomeArea())
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
        //if the post is returned and the group is set we need to check if the caller is allowed to access it
        if (post.getGroup() != null) {
            try {
                groupService.checkIsGroupContentVisible(post.getGroup(), caller, callingAppVariant);
            } catch (GroupContentNotAccessibleException | GroupNotFoundException e) {
                //we do not want to show that the post actually exists
                throw PostNotFoundException.noPostWithId(postId);
            }
        }
        return post;
    }

    @Override
    public ExternalPost findExternalPostById(String postId) throws PostNotFoundException {
        return externalPostRepository.findById(postId)
                .filter(ExternalPost::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Gossip findGossipById(String postId) throws PostNotFoundException {
        return gossipRepository.findById(postId)
                .filter(Gossip::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Seeking findSeekingById(String postId) throws PostNotFoundException {
        return seekingRepository.findById(postId)
                .filter(Seeking::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Offer findOfferById(String postId) throws PostNotFoundException {
        return offerRepository.findById(postId)
                .filter(Offer::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public SpecialPost findSpecialPostById(String postId) throws PostNotFoundException {
        return specialPostRepository.findById(postId)
                .filter(SpecialPost::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public NewsItem findNewsItemById(String postId) throws PostNotFoundException {
        return newsItemRepository.findById(postId)
                .filter(NewsItem::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public Happening findHappeningById(String postId) throws PostNotFoundException {
        return happeningRepository.findById(postId)
                .filter(Happening::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(postId));
    }

    @Override
    public ExternalPost findExternalPostByNewsSourceAndExternalIdNotDeleted(NewsSource newsSource, String externalId)
            throws PostNotFoundException {
        return externalPostRepository.findByNewsSourceAndExternalId(newsSource, externalId)
                .filter(ExternalPost::isNotDeleted)
                .map(this::useCachedEntities)
                .orElseThrow(() -> PostNotFoundException.noPostWithExternalId(externalId));
    }

    @Override
    public Optional<ExternalPost> findExternalPostByNewsSourceAndExternalIgnoringDeleted(NewsSource newsSource,
            String externalId) {
        return externalPostRepository.findByNewsSourceAndExternalId(newsSource, externalId)
                .map(this::useCachedEntities);
    }

    @Override
    public Page<Post> findAllByCreator(Person currentUser, PagedQuery pagedQuery) {
        return postRepository.findAllByCreatorAndDeletedFalse(currentUser, pagedQuery.toPageRequest())
                .map(this::useCachedEntities);
    }

    @Override
    public List<Post> findAllByCreatorIncludingDeleted(Person currentUser) {
        return postRepository.findAllByCreatorOrderByCreatedDesc(currentUser).stream()
                .map(this::useCachedEntities)
                .collect(Collectors.toList());
    }

    @Override
    public Page<Post> findAllByCreatorAndPostType(Person creator, PostType type, PagedQuery pagedQuery) {

        return postRepository.findAllByCreatorAndPostTypeAndDeletedFalse(creator, type,
                        pagedQuery.toPageRequest())
                .map(this::useCachedEntities);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <P extends Post> P storePost(P entity) {

        return useCachedEntities((P) super.store(entity));
    }

    @Override
    public Page<Post> findAllByActivity(Person person, PagedQuery request) {

        return postRepository.findAllPostsCommentedAndHappeningsParticipatedByPerson(person.getId(),
                        request.toPageRequest())
                .map(this::useCachedEntities);
    }

    /**
     * No @Transactional annotation! Otherwise, hibernate tries to "save" the "modified" posts because we use cached entities in them.
     */
    @Override
    public Slice<Post> findAllPosts(final Person caller,
            final Set<String> geoAreaIds, @Nullable final PostType type, @Nullable final Long rawStartTime,
            @Nullable final Long rawEndTime, final Set<OrganizationTag> requiredOrganizationTags,
            final Set<OrganizationTag> forbiddenOrganizationTags, final PagedQuery pagedQuery) {

        final long startTime = (rawStartTime == null)
                ? pagedQuery.sortCriteria.getDefaultStart()
                : rawStartTime;
        final long endTime = (rawEndTime == null)
                ? pagedQuery.sortCriteria.getDefaultEnd()
                : rawEndTime;

        int requiredOrganizationTagsBitMask = StorableEnumSet.toBitMaskValue(requiredOrganizationTags);
        int forbiddenOrganizationTagsBitMask = StorableEnumSet.toBitMaskValue(forbiddenOrganizationTags);

        final Set<String> memberGroupIds = BaseEntity.toInSafeIdSet(groupService.findAllMemberGroupIds(caller));

        boolean callerInParallelWorld = caller.getStatuses().hasValue(PersonStatus.PARALLEL_WORLD_INHABITANT);
        //we get the ids first in a paged way
        Slice<String> idSlice = switch (pagedQuery.getSortCriteria()) {
            case LAST_ACTIVITY -> (type == null)
                    ?
                    postRepository.findAllByGeoAreaAndLastlastActivityBetween(caller, callerInParallelWorld,
                            caller.getHomeArea(),
                            BaseEntity.toInSafeIdSet(geoAreaIds), memberGroupIds, startTime, endTime,
                            requiredOrganizationTagsBitMask,
                            forbiddenOrganizationTagsBitMask,
                            pagedQuery.toPageRequest())
                    : postRepository.findAllByGeoAreaAndTypeAndLastlastActivityBetween(caller,
                            callerInParallelWorld,
                            caller.getHomeArea(),
                            BaseEntity.toInSafeIdSet(geoAreaIds), memberGroupIds, type, startTime, endTime,
                            requiredOrganizationTagsBitMask,
                            forbiddenOrganizationTagsBitMask,
                            pagedQuery.toPageRequest());
            case CREATED -> (type == null)
                    ?
                    postRepository.findAllByGeoAreaAndCreatedBetween(caller, callerInParallelWorld,
                            caller.getHomeArea(),
                            BaseEntity.toInSafeIdSet(geoAreaIds), memberGroupIds, startTime, endTime,
                            requiredOrganizationTagsBitMask,
                            forbiddenOrganizationTagsBitMask,
                            pagedQuery.toPageRequest())
                    : postRepository.findAllByGeoAreaAndTypeAndCreatedBetween(caller, callerInParallelWorld,
                            caller.getHomeArea(),
                            BaseEntity.toInSafeIdSet(geoAreaIds), memberGroupIds, type, startTime, endTime,
                            requiredOrganizationTagsBitMask,
                            forbiddenOrganizationTagsBitMask,
                            pagedQuery.toPageRequest());
            case HAPPENING_START ->
                    happeningRepository.findAllByGeoAreaAndStartTimeBetween(caller, caller.getHomeArea(),
                            BaseEntity.toInSafeIdSet(geoAreaIds), memberGroupIds, startTime, endTime,
                            requiredOrganizationTagsBitMask,
                            forbiddenOrganizationTagsBitMask,
                            pagedQuery.toPageRequest());
            default -> throw new InvalidSortingCriterionException();
        };
        List<String> idsInOrder = idSlice.getContent();
        //since paging can not join we use the ids to query once with a join
        List<Post> posts = postRepository.findAllByIdIn(idsInOrder).stream()
                .map(this::useCachedEntities)
                .sorted(Comparator.comparing(p -> idsInOrder.indexOf(p.getId())))
                .collect(Collectors.toList());
        return new SliceImpl<>(posts, idSlice.getPageable(), idSlice.hasNext());
    }

    @Override
    public Page<Post> searchPosts(String search, Person caller, Set<String> geoAreaIds,
            PageRequest pageRequest) {

        final Set<String> memberGroupIds = groupService.findAllMemberGroupIds(caller);
        return grapevineSearchService.simpleSearch(search, caller.getId(), BaseEntity.getIdOf(caller.getHomeArea()),
                        geoAreaIds,
                        memberGroupIds, pageRequest)
                .map(this::useCachedEntities);
    }

    @Override
    public Page<? extends ExternalPost> findAllExternalPosts(Collection<String> geoAreaIds, Collection<PostType> types,
            PagedQuery pagedQuery) {
        switch (pagedQuery.getSortCriteria()) {
            case CREATED:
            case LAST_MODIFIED:
                return externalPostRepository.findAllByGeoAreaIdInAndPostTypeInAndDeletedFalseAndPublishedTrue(
                                geoAreaIds, types,
                                pagedQuery.toPageRequest())
                        .map(this::useCachedEntities);
            case HAPPENING_START:
                return happeningRepository.findAllByGeoAreaIdInAndDeletedFalseAndPublishedTrue(geoAreaIds,
                                pagedQuery.toPageRequest())
                        .map(this::useCachedEntities);
            default:
                throw new InvalidSortingCriterionException();
        }
    }

    @Override
    public Page<? extends ExternalPost> findAllExternalPostsByNewsSource(String newsSourceId, Collection<PostType> types,
            PagedQuery pagedQuery) {
        switch (pagedQuery.getSortCriteria()) {
            case CREATED:
            case LAST_MODIFIED:
                return externalPostRepository.findAllByNewsSourceIdAndPostTypeInAndDeletedFalseAndPublishedTrue(
                                newsSourceId, types,
                                pagedQuery.toPageRequest())
                        .map(this::useCachedEntities);
            case HAPPENING_START:
                return happeningRepository.findAllByNewsSourceIdAndDeletedFalseAndPublishedTrue(newsSourceId,
                                pagedQuery.toPageRequest())
                        .map(this::useCachedEntities);
            default:
                throw new InvalidSortingCriterionException();
        }
    }

    @Override
    public Page<Post> findAllByUserGeneratedContentFlagStatusNotInAndCreatedBetween(
            Collection<UserGeneratedContentFlagStatus> forbiddenFlagStatuses, long start, long end,
            PageRequest pageRequest) {
        return postRepository.findAllByUserGeneratedContentFlagStatusNotInAndCreatedBetween(
                        forbiddenFlagStatuses, start, end, pageRequest)
                .map(this::useCachedEntities);
    }

    @Override
    public Post deletePost(Post post) {

        final long currentTime = timeService.currentTimeMillisUTC();
        post.setDeleted(true);
        post.setDeletionTime(currentTime);
        post.setLastModified(currentTime);
        post.setLikeCount(0);
        post = store(post);

        postInteractionService.deleteAllByPost(post);
        if (post instanceof Happening) {
            happeningService.deleteAllHappeningParticipations((Happening) post);
        }
        return post;
    }

    @Override
    public void deleteAllPostsOfGroup(Group group) {
        List<Post> postsToDelete = postRepository.findAllByGroupAndDeletedFalse(group);
        postsToDelete.forEach(this::deletePost);
    }

    @Override
    public void deleteDemoPosts(GeoArea geoArea, PostType postType, Set<? extends Post> postsToNotDelete, LogSummary logSummary) {

        Page<Post> posts = postRepository.findAllByGeoAreaAndPostType(geoArea, postType, Post.toInSafeEntitySet(postsToNotDelete), PageRequest.of(0, 10));
        for (Post post : posts) {
            deletePost(post);
        }
        logSummary.info("Deleted {} of {} existing posts in {}", posts.getNumberOfElements(), posts.getTotalElements(), geoArea);
    }

    @Override
    public void movePostsToParallelWorld(Person creator) {

        setParallelWorldByCreator(creator, true);
    }

    @Override
    public void movePostsToRealWorld(Person creator) {

        setParallelWorldByCreator(creator, false);
    }

    private void setParallelWorldByCreator(Person creator, boolean parallelWorld) {
        List<Post> changedPosts = postRepository.findAllByCreatorOrderByCreatedDesc(creator).stream()
                .peek(p -> p.setParallelWorld(parallelWorld))
                .collect(Collectors.toList());
        postRepository.saveAll(changedPosts);
        postRepository.flush();
    }

}
