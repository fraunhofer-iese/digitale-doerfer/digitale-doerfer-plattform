/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import de.fhg.iese.dd.platform.business.grapevine.exceptions.NewsSourceNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.IConvenienceService;
import de.fhg.iese.dd.platform.business.grapevine.services.INewsSourceService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.DataInitKey;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.DataBindingMethodResolver;
import org.springframework.expression.spel.support.SimpleEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ConvenienceDataInitializer extends BaseDataInitializer {

    @Autowired
    private IConvenienceService convenienceService;
    @Autowired
    private IAppService appService;
    @Autowired
    private INewsSourceService newsSourceService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private ConvenienceRepository convenienceRepository;

    /**
     * Wrapper for evaluation of the variables in templates
     */
    @Value
    private static class TemplateVariableEvaluationContext {

        AppVariant appVariant;
        NewsSource newsSource;

    }

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("convenience")
                .requiredEntity(App.class)
                .requiredEntity(GeoArea.class)
                .processedEntity(Convenience.class)
                .processedEntity(ConvenienceGeoAreaMapping.class)
                .build();
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {
        final Set<Convenience> predefinedConveniences = createPredefinedConveniences(logSummary);
        final Set<Convenience> templatedConveniences = createTemplatedConveniences(predefinedConveniences, logSummary);
        Set<Convenience> createdConveniences = new HashSet<>(predefinedConveniences);
        createdConveniences.addAll(templatedConveniences);
        checkOrphanConveniences(createdConveniences, logSummary);
    }

    /**
     * Predefined = defined explicitly, not auto generated
     */
    private Set<Convenience> createPredefinedConveniences(LogSummary logSummary) {

        final App dorfFunkApp = appService.findById(DorfFunkConstants.APP_ID);
        final FileOwnership fileOwnership = FileOwnership.of(dorfFunkApp);
        Map<DataInitKey, List<Convenience>> keyToEntities = loadAllDataInitKeysAndEntities(Convenience.class);
        DuplicateIdChecker<Convenience> convenienceDuplicateIdChecker = new DuplicateIdChecker<>();

        final int numConveniences = (int) keyToEntities.values().stream()
                .collect(Collectors.summarizingInt(List::size)).getSum();
        logSummary.info("Creating {} predefined conveniences", numConveniences);
        Set<Convenience> createdConveniences = new HashSet<>(numConveniences);
        for (Map.Entry<DataInitKey, List<Convenience>> keyAndEntities : keyToEntities.entrySet()) {

            AppVariant appVariant = keyAndEntities.getKey().getAppVariant();
            Set<GeoArea> geoAreas = keyAndEntities.getKey().getGeoAreasIncluded();
            final List<Convenience> conveniences = keyAndEntities.getValue();

            for (Convenience convenience : conveniences) {
                convenience = checkId(convenience);
                convenienceDuplicateIdChecker.checkId(convenience);
                convenience = adjustCreated(convenience);
                checkFieldNotNull(convenience, Convenience::getName, "name");
                checkFieldNotNull(convenience, Convenience::getDescription, "description");
                convenience.setOrganizationName(Objects.toString(convenience.getOrganizationName(), null));
                convenience.setUrl(Objects.toString(convenience.getUrl(), null));
                convenience.setUrlLabel(Objects.toString(convenience.getUrlLabel(), null));
                convenience.setRelatedAppVariant(appVariant);
                createImage(Convenience::getOverviewImage, Convenience::setOverviewImage, convenience,
                        fileOwnership, logSummary, true);
                createImage(Convenience::getOrganizationLogo, Convenience::setOrganizationLogo, convenience,
                        fileOwnership, logSummary, false);
                createImages(Convenience::getDetailImages, Convenience::setDetailImages, convenience,
                        fileOwnership, logSummary, false);

                if (CollectionUtils.isEmpty(geoAreas)) {
                    logSummary.error("Skipping creation of convenience {} because it does not define any geo area id",
                            convenience.getId());
                    continue;
                }
                //this also deletes old geo area mappings
                final Convenience createdConvenience = convenienceService.saveConvenience(convenience, geoAreas);
                createdConveniences.add(createdConvenience);
                logSummary.info("Created convenience {} with name '{}' and geo areas '{}'", convenience.getId(),
                        convenience.getName(), geoAreas.stream()
                                .map(GeoArea::getName)
                                .collect(Collectors.joining(", ")));
            }
        }
        return createdConveniences;
    }

    /**
     * templated = auto generated for all app variants of an app based on a template
     */
    private Set<Convenience> createTemplatedConveniences(Set<Convenience> predefinedConveniences,
            LogSummary logSummary) {
        final App dorfFunkApp = appService.findById(DorfFunkConstants.APP_ID);
        final FileOwnership fileOwnership = FileOwnership.of(dorfFunkApp);
        Map<DataInitKey, List<ConvenienceTemplateInit>> keyToEntities =
                loadAllDataInitKeysAndEntities(ConvenienceTemplateInit.class);

        Set<Convenience> deletedConveniences = new HashSet<>();
        DuplicateIdChecker<Convenience> templatedConvenienceDuplicateIdChecker = new DuplicateIdChecker<>();
        ExpressionParser expressionParser = new SpelExpressionParser();

        final int numConvenienceTemplates = (int) keyToEntities.values().stream()
                .collect(Collectors.summarizingInt(List::size)).getSum();
        logSummary.info("Found {} templates for conveniences", numConvenienceTemplates);
        Set<Convenience> createdTemplatedConveniences = new HashSet<>(numConvenienceTemplates);
        for (Map.Entry<DataInitKey, List<ConvenienceTemplateInit>> keyAndEntities : keyToEntities.entrySet()) {

            App app = keyAndEntities.getKey().getApp();
            final List<ConvenienceTemplateInit> convenienceTemplates = keyAndEntities.getValue();

            if (app == null) {
                logSummary.error("No app defined for templates {}, skipping",
                        convenienceTemplates.stream()
                                .map(ConvenienceTemplateInit::getId)
                                .collect(Collectors.joining(", ")));
                continue;
            }

            final Set<AppVariant> appVariants = appService.findAllAppVariants(app);
            logSummary.info("Creating {} templated conveniences for app {}", appVariants.size(),
                    app.getAppIdentifier());
            for (AppVariant appVariant : appVariants) {
                //check if a predefined convenience for this app variant already exists, if yes, skip it
                if (predefinedConveniences.stream().anyMatch(c -> appVariant.equals(c.getRelatedAppVariant()))) {
                    logSummary.info("Found predefined convenience for {}, skipping generation",
                            appVariant.getAppVariantIdentifier());
                    continue;
                }
                if (appVariant.isDeleted()) {
                    //the app variant is deleted, so we want to get rid of the convenience, too
                    convenienceRepository.findByRelatedAppVariant(appVariant).ifPresent(deletedConveniences::add);
                    //and we do not want to create it
                    continue;
                }

                NewsSource newsSource;
                try {
                    newsSource = newsSourceService.findNewsSourceByAppVariantIncludingDeleted(appVariant);
                } catch (NewsSourceNotFoundException e) {
                    newsSource = null;
                }

                for (ConvenienceTemplateInit convenienceTemplateInit : convenienceTemplates) {
                    Set<String> excludedNewsSourceIds = convenienceTemplateInit.getExcludedNewsSourceIds();
                    if (!CollectionUtils.isEmpty(excludedNewsSourceIds) && newsSource != null) {
                        if (excludedNewsSourceIds.contains(newsSource.getId())) {
                            logSummary.info("Skipping news source {}", newsSource.getId());
                            continue;
                        }
                    }
                    convenienceTemplateInit = checkId(convenienceTemplateInit);
                    checkFieldNotNull(convenienceTemplateInit, Convenience::getName, "name");
                    checkFieldNotNull(convenienceTemplateInit, Convenience::getDescription, "description");
                    //the convenienceTemplateInit is just a container for values, we have to copy them to a real convenience
                    Convenience convenience = Convenience.builder()
                            .name(expandTemplateVariables(convenienceTemplateInit.getName(),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource)))
                            .description(expandTemplateVariables(convenienceTemplateInit.getDescription(),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource)))
                            .organizationName(expandTemplateVariables(
                                    Objects.toString(convenienceTemplateInit.getOrganizationName(), null),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource)))
                            .url(expandTemplateVariables(
                                    Objects.toString(convenienceTemplateInit.getUrl(), null),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource)))
                            .urlLabel(expandTemplateVariables(
                                    Objects.toString(convenienceTemplateInit.getUrlLabel(), null),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource)))
                            .overviewImage(getReferencedOrDefaultImage(convenienceTemplateInit, fileOwnership,
                                    convenienceTemplateInit.getOverviewImage(),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource),
                                    logSummary))
                            .organizationLogo(getReferencedOrDefaultImage(convenienceTemplateInit, fileOwnership,
                                    convenienceTemplateInit.getOrganizationLogo(),
                                    expressionParser, new TemplateVariableEvaluationContext(appVariant, newsSource),
                                    logSummary))
                            .relatedAppVariant(appVariant)
                            .build();
                    convenience.generateConstantId(convenienceTemplateInit.getId(), appVariant.getId());
                    templatedConvenienceDuplicateIdChecker.checkId(convenience);
                    convenience.setCreated(timeService.currentTimeMillisUTC());

                    if (!CollectionUtils.isEmpty(convenienceTemplateInit.getDetailImages())) {
                        NewsSource finalNewsSource = newsSource;
                        convenience.setDetailImages(convenienceTemplateInit.getDetailImages().stream()
                                .map(mediaItem -> getReferencedOrDefaultImage(convenience, fileOwnership, mediaItem,
                                        expressionParser,
                                        new TemplateVariableEvaluationContext(appVariant, finalNewsSource), logSummary))
                                .collect(Collectors.toList()));
                    }

                    final Collection<GeoArea> geoAreas = appService.getConfiguredAvailableRootGeoAreas(appVariant);

                    //this also deletes old geo area mappings
                    final Convenience createdConvenience = convenienceService.saveConvenience(convenience, geoAreas);
                    createdTemplatedConveniences.add(createdConvenience);
                    logSummary.info("Created templated convenience {} with name '{}' and geo areas '{}'",
                            convenience.getId(),
                            convenience.getName(), geoAreas.stream()
                                    .map(GeoArea::getName)
                                    .collect(Collectors.joining(", ")));
                }
            }
        }
        logSummary.info("Deleting {} conveniences of deleted app variants", deletedConveniences.size());
        convenienceRepository.deleteAll(deletedConveniences);
        return createdTemplatedConveniences;
    }

    private MediaItem getReferencedOrDefaultImage(Convenience convenience, FileOwnership fileOwnership,
            MediaItem templatedMediaItem, ExpressionParser expressionParser, TemplateVariableEvaluationContext context,
            LogSummary logSummary) {

        if (templatedMediaItem == null) {
            return null;
        }
        String templateMediaItemId = templatedMediaItem.getId();
        if (StringUtils.isEmpty(templateMediaItemId)) {
            return null;
        }
        if (isTemplateString(templateMediaItemId)) {
            String templatedImageId =
                    expandTemplateVariables(templateMediaItemId, expressionParser, context);

            if (StringUtils.isNotEmpty(templatedImageId)) {
                MediaItem originalMediaItem = mediaItemService.findItemById(templatedImageId);
                return mediaItemService.cloneItem(originalMediaItem, fileOwnership);
            } else {
                return null;
            }
        } else {
            return createMediaItemFromLocation(convenience, templateMediaItemId, fileOwnership, logSummary);
        }
    }

    private String expandTemplateVariables(String templateString, ExpressionParser expressionParser,
            TemplateVariableEvaluationContext evaluationContext) {

        if (templateString == null) {
            return null;
        }

        if (!isTemplateString(templateString)) {
            return templateString;
        }

        final String expressionString = StringUtils.removeStart(StringUtils.removeEnd(templateString, "}"), "#{");
        final SpelExpression expression = (SpelExpression) expressionParser.parseExpression(expressionString);
        expression.setEvaluationContext(SimpleEvaluationContext.forReadOnlyDataBinding()
                .withMethodResolvers(DataBindingMethodResolver.forInstanceMethodInvocation())
                .withRootObject(evaluationContext)
                .build());

        String expandedValue = expression.getValue(String.class);
        return expandNewsSourceNameVariable(expandedValue, evaluationContext.getAppVariant());
    }

    private String expandNewsSourceNameVariable(String newsSourceName, AppVariant appVariant) {
        if (StringUtils.contains(newsSourceName, INewsSourceService.NEWS_SOURCE_VARIABLE_GEO_AREA_NAME)) {
            Set<GeoArea> rootGeoAreasAppVariant =
                    geoAreaService.getRootGeoAreas(appService.getAvailableGeoAreasStrict(appVariant));
            String rootGeoAreasNames = rootGeoAreasAppVariant.stream()
                    .map(GeoArea::getName)
                    .sorted()
                    .collect(Collectors.joining(", "));
            String rootGeoAreasNamesShort = StringUtils.abbreviate(rootGeoAreasNames, 120);
            return StringUtils.replaceEach(newsSourceName,
                    new String[]{INewsSourceService.NEWS_SOURCE_VARIABLE_GEO_AREA_NAME},
                    new String[]{rootGeoAreasNamesShort});
        } else {
            return newsSourceName;
        }
    }

    private boolean isTemplateString(String templateString) {
        return StringUtils.startsWith(templateString, "#{");
    }

    private void checkOrphanConveniences(Set<Convenience> createdConveniences, LogSummary logSummary) {
        final List<Convenience> orphanConveniences =
                convenienceRepository.findAllNotInOrderByName(Convenience.toInSafeEntitySet(createdConveniences));

        if (!CollectionUtils.isEmpty(orphanConveniences)) {
            orphanConveniences.forEach(c -> logSummary.warn(
                    "Convenience {} with name '{}' was created by a template before and not deleted, " +
                            "but does not exist in data init template anymore",
                    c.getId(), c.getName()));
        }
    }

}
