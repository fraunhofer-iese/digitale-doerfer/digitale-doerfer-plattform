/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostInteractionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.results.ViewCountByGeoAreaId;

@Service
class PostStatisticsService extends BaseService implements IPostStatisticsService {

    public static final int PAST_DAYS = 14;
    @Autowired
    private IGeoAreaService geoAreaService;

    @Autowired
    private PostInteractionRepository postInteractionRepository;

    @Override
    public PostStatistics calculatePostStatistics(Post post) {

        long now = timeService.currentTimeMillisUTC();

        List<ViewCountByGeoAreaId> viewCountByGeoAreaIds = postInteractionRepository.viewCountByGeoAreaId(post);
        List<Long> viewedOverviewTimes = postInteractionRepository.viewedOverviewTimeByPost(post);
        List<Long> viewedDetailTimes = postInteractionRepository.viewedDetailTimeByPost(post);

        return PostStatistics.builder()
                .post(post)
                .calculationTime(now)
                .viewedOverviewCount(viewedOverviewTimes.size())
                .viewedDetailCount(viewedDetailTimes.size())
                .likeCount(Math.toIntExact(post.getLikeCount()))
                .commentCount(Math.toIntExact(post.getCommentCount()))
                .pastDaysStatistics(calculatePastDaysStatistics(now, viewedOverviewTimes, viewedDetailTimes))
                .dayTimeStatistics(calculateDayTimeStatistics(viewedOverviewTimes, viewedDetailTimes))
                .geoAreaStatistics(calculateGeoAreaStatistics(viewCountByGeoAreaIds))
                .build();
    }

    private List<PostGeoAreaStatistics> calculateGeoAreaStatistics(List<ViewCountByGeoAreaId> viewCountByGeoAreaIds) {
        return viewCountByGeoAreaIds.stream()
                .filter(vc -> StringUtils.isNotEmpty(vc.getGeoAreaId()))
                .map(vc -> PostGeoAreaStatistics.builder()
                        .geoArea(geoAreaService.findGeoAreaById(vc.getGeoAreaId()))
                        .viewedOverviewCount(Math.toIntExact(vc.getViewedOverviewCount()))
                        .viewedDetailCount(Math.toIntExact(vc.getViewedDetailCount()))
                        .build())
                .collect(Collectors.toList());
    }

    private List<PostDayTimeStatistics> calculateDayTimeStatistics(List<Long> viewedOverviewTimes,
            List<Long> viewedDetailTimes) {

        //the views are grouped by the hour of the day (0-23)
        Map<Byte, Long> hourOfDayToViewedOverviewCount = viewedOverviewTimes.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(this::millisUTCToHourOfDay, Collectors.counting()));
        Map<Byte, Long> hourOfDayToViewedDetailCount = viewedDetailTimes.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(this::millisUTCToHourOfDay, Collectors.counting()));

        List<PostDayTimeStatistics> dayTimeStatistics = new ArrayList<>(24);
        for (byte hourOfDay = 0; hourOfDay < 24; hourOfDay++) {
            long viewedOverviewCount = hourOfDayToViewedOverviewCount.getOrDefault(hourOfDay, 0L);
            long viewedDetailCount = hourOfDayToViewedDetailCount.getOrDefault(hourOfDay, 0L);
            dayTimeStatistics.add(PostDayTimeStatistics.builder()
                    .startHour(hourOfDay)
                    .endHour((byte) (hourOfDay + 1))
                    .viewedOverviewCount(Math.toIntExact(viewedOverviewCount))
                    .viewedDetailCount(Math.toIntExact(viewedDetailCount))
                    .build());
        }
        return dayTimeStatistics;
    }

    private List<PostTimeStatistics> calculatePastDaysStatistics(long now, List<Long> viewedOverviewTimes,
            List<Long> viewedDetailTimes) {

        //the views are grouped by the days since 01.01.1970
        Map<Integer, Long> hourOfDayToViewedOverviewCount = viewedOverviewTimes.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(this::millisUTCToDays, Collectors.counting()));
        Map<Integer, Long> hourOfDayToViewedDetailCount = viewedDetailTimes.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(this::millisUTCToDays, Collectors.counting()));

        List<PostTimeStatistics> pastDaysStatistics = new ArrayList<>(PAST_DAYS);
        int nowDay = millisUTCToDays(now);
        for (int pastDay = 0; pastDay < PAST_DAYS; pastDay++) {
            int currentDay = nowDay - pastDay;
            Long viewedOverviewCount = hourOfDayToViewedOverviewCount.getOrDefault(currentDay, 0L);
            Long viewedDetailCount = hourOfDayToViewedDetailCount.getOrDefault(currentDay, 0L);
            pastDaysStatistics.add(PostTimeStatistics.builder()
                    .startTime(daysToStartOfDayMillisUTC(currentDay))
                    //the current day is not yet over, so we need to take now
                    .endTime(pastDay == 0 ? now : (daysToStartOfDayMillisUTC(currentDay + 1) - 1))
                    .viewedOverviewCount(Math.toIntExact(viewedOverviewCount))
                    .viewedDetailCount(Math.toIntExact(viewedDetailCount))
                    .build());
        }
        return pastDaysStatistics;
    }

    private byte millisUTCToHourOfDay(long timeMillis) {
        return (byte) Math.floorMod(Math.floorDiv(timeMillis, TimeUnit.HOURS.toMillis(1)), 24L);
    }

    private int millisUTCToDays(long timeMillis) {
        return Math.toIntExact(Math.floorDiv(timeMillis, TimeUnit.DAYS.toMillis(1)));
    }

    private long daysToStartOfDayMillisUTC(int days) {
        return days * TimeUnit.DAYS.toMillis(1);
    }

}
