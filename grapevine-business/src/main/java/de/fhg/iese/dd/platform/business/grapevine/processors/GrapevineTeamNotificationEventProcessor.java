/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.ExternalPostPublishConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnCommentRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.chat.ChatStartOnPostRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentDeleteRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGroupService;
import de.fhg.iese.dd.platform.business.shared.teamnotification.TeamNotificationMessage;
import de.fhg.iese.dd.platform.business.shared.teamnotification.services.ITeamNotificationService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.teamnotification.model.enums.TeamNotificationPriority;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.config.UserGeneratedContentFlagConfig;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class GrapevineTeamNotificationEventProcessor extends BaseEventProcessor {

    private static final String TEAM_NOTIFICATION_TOPIC = "grapevine";

    private static final String TEAM_NOTIFICATION_TOPIC_GROUP = "grapevine-group";

    private static final int MAX_TEXT_LENGTH_ORIGINAL_POST_TEXT = 80;

    @Autowired
    private ITeamNotificationService teamNotificationService;

    @Autowired
    private UserGeneratedContentFlagConfig userGeneratedContentFlagConfig;

    @EventProcessing
    private void handlePostCreateConfirmation(PostCreateConfirmation<?> event) {
        Post post = event.getPost();
        Pair<String, String> pictureCountMessageAndLinks = getPictureCountMessageAndLinks(post.getImages());

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "{} created by {} in {}{}{}: ```{}```{}{} ({})",
                post.getPostType(),
                getCreatorReference(post),
                tenantAndGeoArea(post),
                groupInfo(post),
                pictureCountMessageAndLinks.getLeft(),
                nice(post.getText()),
                getUrl(post),
                pictureCountMessageAndLinks.getRight(),
                event.isPublished() ? "published" : "not published");
    }

    @EventProcessing
    private void handleExternalPostPublishConfirmation(ExternalPostPublishConfirmation event) {
        ExternalPost externalPost = event.getExternalPost();
        Pair<String, String> pictureCountMessageAndLinks = getPictureCountMessageAndLinks(externalPost.getImages());

        teamNotificationService.sendTeamNotification(
                externalPost.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "{} published by {} in {}{}{}: ```{}```{}{}",
                externalPost.getPostType(),
                getCreatorReference(externalPost),
                tenantAndGeoArea(externalPost),
                groupInfo(externalPost),
                pictureCountMessageAndLinks.getLeft(),
                nice(externalPost.getText()),
                externalPost.getUrl(),
                pictureCountMessageAndLinks.getRight());
    }

    @EventProcessing
    private void handlePostChangeConfirmation(PostChangeConfirmation<?> event) {

        Post post = event.getPost();

        //Changes in news items and happenings occur seldom, but it is important to get a team
        //notification about this. This change is also triggered when they are "undeleted"
        post.accept(new Post.Visitor<Void>() {
            @Override
            public Void whenGossip(Gossip gossip) {
                return null;
            }

            @Override
            public Void whenNewsItem(NewsItem newsItem) {
                if (newsItem.getNewsSource() != null) {
                    sendNotification(newsItem);
                }
                return null;
            }

            @Override
            public Void whenSuggestion(Suggestion suggestion) {
                return null;
            }

            @Override
            public Void whenSeeking(Seeking seeking) {
                return null;
            }

            @Override
            public Void whenOffer(Offer offer) {
                return null;
            }

            @Override
            public Void whenHappening(Happening happening) {
                if (happening.getNewsSource() != null) {
                    sendNotification(happening);
                }
                return null;
            }

            @Override
            public Void whenSpecialPost(SpecialPost specialPost) {
                return null;
            }

            private void sendNotification(Post post) {

                Pair<String, String> pictureCountMessageAndLinks = getPictureCountMessageAndLinks(post.getImages());

                teamNotificationService.sendTeamNotification(
                        post.getTenant(),
                        TEAM_NOTIFICATION_TOPIC,
                        TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                        "{} updated or undeleted by {} in {}{}{}: ```{}```{}{}",
                        post.getPostType(),
                        getCreatorReference(post),
                        tenantAndGeoArea(post),
                        groupInfo(post),
                        pictureCountMessageAndLinks.getLeft(),
                        nice(post.getText()),
                        getUrl(post),
                        pictureCountMessageAndLinks.getRight());
            }
        });
    }

    @EventProcessing
    private void handleCommentCreateConfirmation(CommentCreateConfirmation event) {
        Comment comment = event.getComment();
        Post post = comment.getPost();
        Pair<String, String> pictureCountMessageAndLinks = getPictureCountMessageAndLinks(comment.getImages());

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Comment added to {} `{}` by {} in {}{}{}: ```{}```{}",
                post.getPostType(),
                referenceToOriginalText(post.getText()),
                comment.getCreator().getFirstNameFirstCharLastDot(),
                tenantAndGeoArea(post),
                groupInfo(post),
                pictureCountMessageAndLinks.getLeft(),
                nice(comment.getText()),
                pictureCountMessageAndLinks.getRight());
    }

    @EventProcessing
    private void handlePostDeleteRequest(PostDeleteRequest event) {
        Post post = event.getPost();

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "{} deleted by {} in {}{}: ```{}```",
                post.getPostType(),
                getCreatorReference(post),
                tenantAndGeoArea(post),
                groupInfo(post),
                nice(post.getText()));
    }

    @EventProcessing
    private void handleCommentDeleteRequest(CommentDeleteRequest event) {
        Comment comment = event.getComment();
        Post post = comment.getPost();

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS,
                "Comment deleted from {} `{}` by {} in {}{}: ```{}```",
                post.getPostType(),
                referenceToOriginalText(post.getText()),
                comment.getCreator().getFirstNameFirstCharLastDot(),
                tenantAndGeoArea(post),
                groupInfo(post),
                nice(comment.getText()));
    }

    @EventProcessing
    private void handleChatStartOnPostRequest(ChatStartOnPostRequest event) {
        Post post = event.getPost();
        Person sender = event.getSender();

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "Chat started by {} on {} `{}` by {}",
                sender.getFirstNameFirstCharLastDot(),
                post.getPostType(),
                referenceToOriginalText(post.getText()),
                getCreatorReference(post));
    }

    @EventProcessing
    private void handleChatStartOnCommentRequest(ChatStartOnCommentRequest event) {
        Comment comment = event.getComment();
        Post post = comment.getPost();
        Person sender = event.getSender();

        teamNotificationService.sendTeamNotification(
                post.getTenant(),
                TEAM_NOTIFICATION_TOPIC,
                TeamNotificationPriority.INFO_APPLICATION_SINGLE_USER,
                "Chat started by {} on Comment `{}` by {} on {} `{}`",
                sender.getFirstNameFirstCharLastDot(),
                referenceToOriginalText(comment.getText()),
                comment.getCreator().getFirstNameFirstCharLastDot(),
                post.getPostType(),
                referenceToOriginalText(post.getText()));
    }

    @EventProcessing
    private void handleGroupCreateConfirmation(GroupCreateConfirmation event) {
        Group group = event.getCreatedGroup();
        IGroupService.GroupGeoAreas groupGeoAreas = event.getGroupGeoAreas();

        teamNotificationService.sendTeamNotification(
                group.getTenant(),
                TEAM_NOTIFICATION_TOPIC_GROUP,
                TeamNotificationMessage.builder()
                        .priority(TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS)
                        .message(
                                "{} created {} group {} '{}' for [{}]",
                                group.getCreator() != null ? group.getCreator().getFirstNameFirstCharLastDot() : "-",
                                group.getAccessibility() == GroupAccessibility.APPROVAL_REQUIRED ? "private" : "public",
                                group.getId(),
                                group.getName(),
                                groupGeoAreas.getIncludedGeoAreas().stream()
                                        .map(g -> "'" + g.getName() + "'")
                                        .collect(Collectors.joining(", ")))
                        .link(Pair.of("AdministrierBar for details about group",
                                UriComponentsBuilder.fromHttpUrl(
                                                userGeneratedContentFlagConfig.getAdministrierbarBaseUrl())
                                        .pathSegment("groups", "details", "{id}")
                                        .build(group.getId())
                                        .toString()))
                        .build()
        );
    }

    @EventProcessing
    private void handleGroupChangeConfirmation(GroupChangeConfirmation event) {
        Group group = event.getUpdatedGroup();
        IGroupService.GroupGeoAreas groupGeoAreas = event.getGroupGeoAreas();

        teamNotificationService.sendTeamNotification(
                group.getTenant(),
                TEAM_NOTIFICATION_TOPIC_GROUP,
                TeamNotificationMessage.builder()
                        .priority(TeamNotificationPriority.INFO_APPLICATION_MULTIPLE_USERS)
                        .message(
                                "{} updated {} group {} '{}' for [{}]",
                                group.getCreator() != null ? group.getCreator().getFirstNameFirstCharLastDot() : "-",
                                group.getAccessibility() == GroupAccessibility.APPROVAL_REQUIRED ? "private" : "public",
                                group.getId(),
                                group.getName(),
                                groupGeoAreas.getIncludedGeoAreas().stream()
                                        .map(g -> "'" + g.getName() + "'")
                                        .collect(Collectors.joining(", ")))
                        .link(Pair.of("AdministrierBar for details about group",
                                UriComponentsBuilder.fromHttpUrl(
                                                userGeneratedContentFlagConfig.getAdministrierbarBaseUrl())
                                        .pathSegment("groups", "details", "{id}")
                                        .build(group.getId())
                                        .toString()))
                        .build()
        );
    }

    private String getUrl(Post post) {
        String url;
        if (post instanceof ExternalPost externalPost) {
            url = StringUtils.defaultString(externalPost.getUrl());
        } else {
            url = "";
        }
        return url;
    }

    private String getCreatorReference(Post post) {
        Person creator = post.getCreator();
        if (creator != null) {
            return creator.getFullName();
        } else {
            if (post instanceof ExternalPost externalPost) {
                NewsSource newsSource = externalPost.getNewsSource();
                if (newsSource != null) {
                    return newsSource.getSiteUrl();
                } else {
                    return externalPost.getAuthorName();
                }
            } else {
                return post.getId();
            }
        }
    }

    private Pair<String, String> getPictureCountMessageAndLinks(List<MediaItem> images) {
        String pictureCountMessage = "";
        String pictureLinks = "";
        if (!CollectionUtils.isEmpty(images)) {
            if (images.size() == 1) {
                pictureCountMessage = " with one picture";
            } else {
                pictureCountMessage = " with " + images.size() + " pictures";
            }
            pictureLinks = images.stream()
                    .map(m -> m.getUrls().get(MediaItemSize.ORIGINAL_SIZE.getName()))
                    .collect(Collectors.joining("\n", "\n", ""));
        }
        return Pair.of(pictureCountMessage, pictureLinks);
    }

    private String groupInfo(Post post) {
        if (post.getGroup() != null) {
            return String.format(" in group `%s`", post.getGroup().getName());
        } else {
            return "";
        }
    }

    private String tenantAndGeoArea(Post post){
        return String.format("`%s` `[%s]`",
                CollectionUtils.isEmpty(post.getGeoAreas())
                        ? "unknown"
                        : post.getGeoAreas().stream()
                                .map(GeoArea::getName)
                                .collect(Collectors.joining(", ")),
                post.getTenant() == null
                        ? "unknown"
                        : post.getTenant().getName());
    }

    private String referenceToOriginalText(String ugly){
        return StringUtils.abbreviate(
                RegExUtils.replaceAll(ugly, "[\r\n]+", " "),
                MAX_TEXT_LENGTH_ORIGINAL_POST_TEXT);
    }

    private String nice(String ugly){
        return RegExUtils.replaceAll(ugly, "[\r\n]+", "\n");
    }

}
