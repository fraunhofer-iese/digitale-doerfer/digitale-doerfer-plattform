/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2021 Adeline Silva Schäfer, Ala Harirchi, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics.legacy;

import java.io.IOException;

import de.fhg.iese.dd.platform.business.framework.services.IService;
import de.fhg.iese.dd.platform.business.grapevine.statistics.GrapevineGeoAreaInfo;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import freemarker.template.TemplateException;

public interface IGrapevineStatisticsService extends IService {

    void createStatisticsReportsFixedTime();

    void createStatisticsReportsTemporary();

    GrapevineStats getGrapevineStats(long start, long end);

    PostStats getPostStats(long start, long end);

    PostStats getPostStatsPerTenantGroup(String tag, long start, long end);

    PostStats getPostStatsPerTenant(Tenant tenant, long start, long end);

    /**
     * Number of created/logged users in the specified period (start, end)
     *
     * @param start
     * @param end
     *
     * @return
     */
    UserStats getUserStats(long start, long end);

    UserStats getUserStatsPerTenantGroup(String tag, long start, long end);

    UserStats getUserStatsPerTenant(Tenant tenant, long start, long end);

    GroupStats getGroupStats(long start, long end);

    GroupStats getGroupStatsPerTenantGroup(String tag, long start, long end);

    GroupStats getGroupStatsPerTenant(Tenant tenant, long start, long end);

    TenantGroupStats getTenantGroupStats(String tag, long start, long end);

    TenantStats getTenantStats(Tenant tenant, long start, long end);

    ActivityStats getActivityStatsPerTenantGroup(String tag, long start, long end);

    ActivityStats getActivityStatsPerTenant(Tenant tenant, long start, long end);

    String getGrapevineStatsReport(long start, long end, GrapevineStats grapevineStats)
            throws IOException, TemplateException;

    String getTenantGroupStatsReport(long start, long end, TenantGroupStats tenantGroupStats)
            throws IOException, TemplateException;

    String getTenantStatsReport(long start, long end, TenantStats tenantStats)
            throws IOException, TemplateException;

    GrapevineGeoAreaInfo getGrapevineGeoAreaInfo(GeoArea geoArea);
}
