/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Group;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.GroupRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;

@Component
public class GroupDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupGeoAreaMappingRepository groupGeoAreaMappingRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        //members of groups
                        .referencedEntity(Person.class)
                        .changedOrDeletedEntity(Group.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build(),
                SwapReferencesDeletionStrategy.forTriggeringEntity(Tenant.class)
                        //members of groups
                        .referencedEntity(Person.class)
                        .changedOrDeletedEntity(Group.class)
                        .checkImpact(this::checkImpactTenant)
                        .swapData(this::swapDataTenant)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        long impactedMappings = groupGeoAreaMappingRepository.countByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} groups will move to new geo area", impactedMappings);
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        Set<GroupGeoAreaMapping> mappingsToBeChanged =
                groupGeoAreaMappingRepository.findMappingsToGeoAreaAAndNotToGeoAreaB(geoAreaToBeDeleted,
                        geoAreaToBeUsedInstead);

        for (GroupGeoAreaMapping groupGeoAreaMapping : mappingsToBeChanged) {
            groupGeoAreaMapping.setGeoArea(geoAreaToBeUsedInstead);
        }
        groupGeoAreaMappingRepository.saveAll(mappingsToBeChanged);
        int deletedMappings = groupGeoAreaMappingRepository.deleteByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} groups moved to new geo area, {} groups were already present", mappingsToBeChanged.size(),
                deletedMappings);
    }

    private void checkImpactTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead,
            LogSummary logSummary) {
        long impactedGroups = groupRepository.countByTenant(tenantToBeDeleted);
        logSummary.info("{} groups will move to new tenant", impactedGroups);
    }

    private void swapDataTenant(Tenant tenantToBeDeleted, Tenant tenantToBeUsedInstead, LogSummary logSummary) {
        int updatedGroups = groupRepository.updateTenant(tenantToBeDeleted, tenantToBeUsedInstead);
        logSummary.info("{} groups moved to new tenant", updatedGroups);
    }

}
