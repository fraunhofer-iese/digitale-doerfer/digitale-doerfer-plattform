/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2020 Balthasar Weitzel, Adeline Silva Schäfer, Stefan Schweitzer, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import lombok.NonNull;

@Service
class GrapevineChatService extends BaseService implements IGrapevineChatService {

    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IChatService chatService;

    private static final Set<String> CHAT_TOPICS_DORFFUNK_AND_SUGGESTION_INTERNAL_WORKER =
            Set.of(DorfFunkConstants.CHAT_TOPIC, LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC);

    @Override
    public Chat findOrCreateChat(@NonNull Person participantOne, @NonNull Person participantTwo, @NonNull BaseEntity subject) {
        Set<Person> participants = new HashSet<>();
        participants.add(participantOne);
        participants.add(participantTwo);
        Collection<Chat> chats =
                chatService.findChatsByTopicAndParticipants(DorfFunkConstants.CHAT_TOPIC, participants);
        if (chats != null && !chats.isEmpty()) {
            if (chats.size() > 1) {
                log.warn("More than one chat for topic {} and participants {} and {}",
                        DorfFunkConstants.CHAT_TOPIC,
                        participantOne, participantTwo);
            }
            return chats.iterator().next();
        }
        PushCategory pushCategory =
                pushCategoryService.findPushCategoryById(DorfFunkConstants.PUSH_CATEGORY_ID_CHAT_MESSAGE_RECEIVED);
        return chatService.createChatWithSubjectsAndParticipants(
                DorfFunkConstants.CHAT_TOPIC,
                pushCategory,
                Collections.singleton(subject),
                participants);
    }

    @Override
    public Chat createSuggestionInternalWorkerChat(@NonNull Suggestion suggestion) {

        PushCategory pushCategory = pushCategoryService
                .findPushCategoryById(LoesBarConstants.PUSH_CATEGORY_ID_INTERNAL_WORKER_CHAT_MESSAGE_RECEIVED);
        return chatService.createChatWithSubjects(
                LoesBarConstants.SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC,
                pushCategory,
                suggestion);
    }

    @Override
    public Chat addSuggestionWorkerToSuggestionInternalWorkerChat(@NonNull Chat suggestionInternalWorkerChat,
            @NonNull Person newWorker) {
        return chatService.addParticipantsToChat(suggestionInternalWorkerChat, newWorker);
    }

    @Override
    public Chat removeSuggestionWorkerFromSuggestionInternalWorkerChat(
            @NonNull Chat suggestionInternalWorkerChat, @NonNull Person workerToRemove) {
        return chatService.removeParticipantFromChat(suggestionInternalWorkerChat, workerToRemove);
    }

    @Override
    public Pair<Chat, ChatMessage> addPostReferenceToChat(@NonNull Chat chat, @NonNull Person sender, @NonNull Post post, long sentTime){
        return chatService.addReferenceToEntityMessageToChat(chat, sender, null, post, sentTime, false);
    }

    @Override
    public Pair<Chat, ChatMessage> addCommentReferenceToChat(@NonNull Chat chat, @NonNull Person sender, @NonNull Comment comment, long sentTime){
        return chatService.addReferenceToEntityMessageToChat(chat, sender, null, comment, sentTime, false);
    }

    @Override
    public List<Chat> findOwnChats(@NonNull Person person) {
        return chatService.findChatsByTopicAndParticipant(person, DorfFunkConstants.CHAT_TOPIC, person);
    }

    @Override
    public List<Chat> findOwnChatsAndSuggestionInternalWorkerChats(@NonNull Person person) {
        return chatService.findChatsByTopicsAndParticipant(CHAT_TOPICS_DORFFUNK_AND_SUGGESTION_INTERNAL_WORKER, person);
    }

}
