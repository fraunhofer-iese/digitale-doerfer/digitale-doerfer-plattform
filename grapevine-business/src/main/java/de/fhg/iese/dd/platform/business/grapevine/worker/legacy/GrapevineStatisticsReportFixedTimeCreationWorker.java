/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.worker.legacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.environment.model.IWorkerTask;
import de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.IGrapevineStatisticsService;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class GrapevineStatisticsReportFixedTimeCreationWorker implements IWorkerTask {

    @Autowired
    private IGrapevineStatisticsService grapevineStatisticsService;
    @Autowired
    private ITimeService timeService;

    @Override
    public String getName() {
        return "StatisticsReportFixedTimeCreation";
    }

    @Override
    public Trigger getTaskTrigger() {
        return new CronTrigger("30 7 0 * * *", timeService.getLocalTimeZone());//every day at 00:07:30
    }

    @Override
    public void run() {
        final long start = System.currentTimeMillis();
        log.info("creating statistics reports fixed time triggered");
        grapevineStatisticsService.createStatisticsReportsFixedTime();
        log.info("creating statistics reports fixed time finished in {} ms", System.currentTimeMillis() - start);
    }

}
