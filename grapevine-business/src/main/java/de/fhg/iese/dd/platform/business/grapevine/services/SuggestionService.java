/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import com.google.common.collect.*;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.*;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatusRecord;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionWorkerMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionStatusRecordRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionWorkerMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionFirstResponder;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.SuggestionWorker;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.util.Streamable;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
class SuggestionService extends BasePostService<Suggestion> implements ISuggestionService {
    
    private static final List<Class<? extends BaseRole<?>>> ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER_UNTYPED =
            List.of(SuggestionFirstResponder.class, SuggestionWorker.class);

    private static final List<SuggestionStatus> ALL_SUGGESTION_STATUS = Arrays.asList(SuggestionStatus.values());

    /**
     * Mapping between old status and allowed new status
     * <p>
     * If you adjust it, also adjust the swagger documentation!
     */
    private final static Multimap<SuggestionStatus, SuggestionStatus> ALLOWED_SUGGESTION_STATUS_TRANSITIONS =
            new ImmutableMultimap.Builder<SuggestionStatus, SuggestionStatus>()
                    .putAll(SuggestionStatus.OPEN, SuggestionStatus.IN_PROGRESS, SuggestionStatus.ON_HOLD, SuggestionStatus.DONE)
                    .putAll(SuggestionStatus.IN_PROGRESS, SuggestionStatus.ON_HOLD, SuggestionStatus.DONE)
                    .putAll(SuggestionStatus.ON_HOLD, SuggestionStatus.IN_PROGRESS, SuggestionStatus.DONE)
                    .putAll(SuggestionStatus.DONE, SuggestionStatus.IN_PROGRESS)
                    .build();

    @Autowired
    private SuggestionRepository suggestionRepository;
    @Autowired
    private SuggestionWorkerMappingRepository suggestionWorkerMappingRepository;
    @Autowired
    private SuggestionStatusRecordRepository suggestionStatusRecordRepository;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private IGrapevineChatService grapevineChatService;

    @Override
    public Suggestion findSuggestionById(String suggestionId, boolean fetchExtendedSuggestion)
            throws PostNotFoundException {
        Suggestion suggestion = suggestionRepository.findById(suggestionId)
                .map(this::useCachedEntities)
                .filter(Suggestion::isNotDeleted)
                .orElseThrow(() -> PostNotFoundException.noPostWithId(suggestionId));
        if (fetchExtendedSuggestion) {
            suggestion.setSuggestionWorkerMappings(findSuggestionWorkers(suggestion, false));
            suggestion.setSuggestionStatusRecords(findSuggestionStatusRecords(suggestion));
        }
        return suggestion;
    }

    @Override
    public Suggestion findSuggestionByIdIncludingOverrideDeleted(String suggestionId, boolean fetchExtendedSuggestion) throws PostNotFoundException {
        Suggestion suggestion = suggestionRepository.findById(suggestionId)
                .map(this::useCachedEntities)
                .filter(s -> s.isNotDeleted() || s.isOverrideDeletedForSuggestionRoles())
                .orElseThrow(() -> PostNotFoundException.noPostWithId(suggestionId));
        if (fetchExtendedSuggestion) {
            suggestion.setSuggestionWorkerMappings(findSuggestionWorkers(suggestion, false));
            suggestion.setSuggestionStatusRecords(findSuggestionStatusRecords(suggestion));
        }
        return suggestion;
    }

    @Override
    public Page<Suggestion> findAllSuggestionsInTenantsIncludingOverrideDeleted(
            Set<String> tenantIds,
            long startTime,
            long endTime,
            Set<SuggestionStatus> includedStatus,
            Set<SuggestionStatus> excludedStatus,
            @Nullable String assignedWorkerId,
            boolean unassigned,
            PagedQuery pagedQuery)
            throws InvalidSortingCriterionException {

        final long end = (endTime == 0) ? timeService.currentTimeMillisUTC() : endTime;

        final Set<SuggestionStatus> combinedIncludedStatus;
        if (CollectionUtils.isEmpty(includedStatus)) {
            //implicitly include all
            combinedIncludedStatus = new HashSet<>(ALL_SUGGESTION_STATUS);
        } else {
            //new copy, might be that the the set we get from the request is unmodifiable
            combinedIncludedStatus = new HashSet<>(includedStatus);
        }
        if (!CollectionUtils.isEmpty(excludedStatus)) {
            //can be null, so only remove when necessary
            combinedIncludedStatus.removeAll(excludedStatus);
        }
        Page<Suggestion> suggestions;
        if (CollectionUtils.isEmpty(combinedIncludedStatus)) {
            throw new BadRequestException("Combination of included and excluded status results in " +
                    "filter not matching any suggestions.").withDetail("excludedStatus");
        }
        //we assume that this is never called by a parallel world inhabitant

        if (pagedQuery.getSortCriteria() == PostSortCriteria.LAST_ACTIVITY) {
            if (assignedWorkerId != null) {
                suggestions =
                        suggestionRepository.findAllByTenantInAndLastActivityBetweenIncludingOverrideDeletedAndWorkerAssigned(
                                        tenantIds, startTime, end, combinedIncludedStatus, assignedWorkerId,
                                        pagedQuery.toPageRequest())
                                .map(this::useCachedEntities);
            } else {
                if (unassigned) {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndLastActivityBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                } else {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndLastActivityBetweenIncludingOverrideDeleted(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                }
            }
        } else if (pagedQuery.getSortCriteria() == PostSortCriteria.CREATED) {
            if (assignedWorkerId != null) {
                suggestions =
                        suggestionRepository.findAllByTenantInAndCreatedBetweenIncludingOverrideDeletedAndWorkerAssigned(
                                        tenantIds, startTime, end, combinedIncludedStatus, assignedWorkerId,
                                        pagedQuery.toPageRequest())
                                .map(this::useCachedEntities);
            } else {
                if (unassigned) {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndCreatedBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                } else {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndCreatedBetweenIncludingOverrideDeleted(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                }
            }
        } else if (pagedQuery.getSortCriteria() == PostSortCriteria.LAST_SUGGESTION_ACTIVITY) {
            if (assignedWorkerId != null) {
                suggestions =
                        suggestionRepository.findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeletedAndWorkerAssigned(
                                        tenantIds, startTime, end, combinedIncludedStatus, assignedWorkerId,
                                        pagedQuery.toPageRequest())
                                .map(this::useCachedEntities);
            } else {
                if (unassigned) {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeletedAndNoWorkerAssigned(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                } else {
                    suggestions =
                            suggestionRepository.findAllByTenantInAndLastSuggestionActivityBetweenIncludingOverrideDeleted(
                                            tenantIds, startTime, end, combinedIncludedStatus,
                                            pagedQuery.toPageRequest())
                                    .map(this::useCachedEntities);
                }
            }
        } else {
            throw new InvalidSortingCriterionException();
        }
        fetchSuggestionWorkers(suggestions);
        fetchSuggestionStatusRecords(suggestions);
        return suggestions;
    }

    @Override
    public Suggestion findSuggestionByInternalWorkerChatIncludingOverrideDeleted(Chat chat, boolean fetchExtendedSuggestion) throws PostNotFoundException {
        Suggestion suggestion = suggestionRepository.findByInternalWorkerChat(chat)
                .map(this::useCachedEntities)
                .filter(s -> s.isNotDeleted() || s.isOverrideDeletedForSuggestionRoles())
                .orElseThrow(() -> new PostNotFoundException("No post found for chat {}", chat.getId()));

        if (fetchExtendedSuggestion) {
            suggestion.setSuggestionWorkerMappings(findSuggestionWorkers(suggestion, false));
            suggestion.setSuggestionStatusRecords(findSuggestionStatusRecords(suggestion));
        }
        return suggestion;
    }

    private void fetchSuggestionWorkers(Streamable<Suggestion> suggestions){
        //we get all the mappings for all suggestions, so that we only have one request
        List<SuggestionWorkerMapping> allWorkerMappings = suggestionWorkerMappingRepository
                .findBySuggestionIdInAndInactivatedIsNullOrderByCreatedAsc(suggestions.stream()
                        .map(BaseEntity::getId)
                        .collect(Collectors.toList()));

        //transform it into a multimap suggestion -> list<WorkerMappings> which preserves the order of the mappings
        ImmutableListMultimap<Suggestion, SuggestionWorkerMapping> suggestionToWorkers =
                Multimaps.index(allWorkerMappings, SuggestionWorkerMapping::getSuggestion);

        suggestions.forEach(suggestion -> {
            ImmutableList<SuggestionWorkerMapping> workerMappings = suggestionToWorkers.get(suggestion);
            if (!CollectionUtils.isEmpty(workerMappings)) {
                //we clone the list, so that it becomes modifiable
                suggestion.setSuggestionWorkerMappings(new ArrayList<>(workerMappings));
            }
        });
    }

    private void fetchSuggestionStatusRecords(Streamable<Suggestion> suggestions){
        //we get all the status records for all suggestions, so that we only have one request
        List<SuggestionStatusRecord> allStatusRecords = suggestionStatusRecordRepository
                .findBySuggestionIdInOrderByCreatedDesc(suggestions.stream()
                        .map(BaseEntity::getId)
                        .collect(Collectors.toList()));

        //transform it into a multimap suggestion -> list<SuggestionStatusRecord> which preserves the order of the records
        ImmutableListMultimap<Suggestion, SuggestionStatusRecord> suggestionToStatus =
                Multimaps.index(allStatusRecords, SuggestionStatusRecord::getSuggestion);

        suggestions.forEach(suggestion -> {
            ImmutableList<SuggestionStatusRecord> statusRecords = suggestionToStatus.get(suggestion);
            if (!CollectionUtils.isEmpty(statusRecords)) {
                //we clone the list, so that it becomes modifiable
                suggestion.setSuggestionStatusRecords(new ArrayList<>(statusRecords));
            }
        });
    }

    @Override
    @NonNull
    public List<SuggestionWorkerMapping> findSuggestionWorkers(@lombok.NonNull @NonNull Suggestion suggestion, boolean includeInactive) {
        if (includeInactive) {
            return suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion);
        } else {
            return suggestionWorkerMappingRepository.findBySuggestionAndInactivatedIsNullOrderByCreatedAsc(suggestion);
        }
    }

    @Override
    @NonNull
    public List<SuggestionStatusRecord> findSuggestionStatusRecords(@lombok.NonNull @NonNull Suggestion suggestion) {
        return suggestionStatusRecordRepository.findBySuggestionOrderByCreatedDesc(suggestion);
    }

    @Override
    @NonNull
    public Suggestion addSuggestionWorker(@lombok.NonNull @NonNull Suggestion suggestion,
            @lombok.NonNull @NonNull Person worker, @Nullable Person initiator)
    throws SuggestionWorkerAlreadyWorkingOnSuggestionException {

        List<SuggestionWorkerMapping> existingSuggestionWorkers = findSuggestionWorkers(suggestion, false);

        if (existingSuggestionWorkers.stream().anyMatch(s -> worker.equals(s.getWorker()))) {
            throw new SuggestionWorkerAlreadyWorkingOnSuggestionException(
                    "Suggestion {} already contains suggestion worker {}", suggestion.getId(), worker.getId());
        }

        SuggestionWorkerMapping newWorkerMapping = SuggestionWorkerMapping.builder()
                .suggestion(suggestion)
                .worker(worker)
                .initiator(initiator)
                .inactivated(null) //specified to show the importance, only mappings with inactivated == null are active
                .build();
        //we use the time service here to set created, so that we can control that time in tests
        timeService.setCreatedToNow(newWorkerMapping);
        SuggestionWorkerMapping savedWorkerMapping = saveAndRetryOnDataIntegrityViolation(
                () -> suggestionWorkerMappingRepository.saveAndFlush(newWorkerMapping),
                () -> suggestionWorkerMappingRepository.findBySuggestionAndWorkerAndInactivatedIsNull(suggestion, worker)
        );
        List<SuggestionWorkerMapping> newSuggestionWorkers = new ArrayList<>(existingSuggestionWorkers);
        newSuggestionWorkers.add(savedWorkerMapping);
        suggestion.setSuggestionWorkerMappings(newSuggestionWorkers);
        //add the worker also to the internal chat
        //this null check is only required for existing suggestions that do not have a chat
        if (suggestion.getInternalWorkerChat() != null) {
            suggestion.setInternalWorkerChat(grapevineChatService.addSuggestionWorkerToSuggestionInternalWorkerChat(
                    suggestion.getInternalWorkerChat(), worker));
        }
        suggestion.setLastSuggestionActivity(timeService.currentTimeMillisUTC());
        return store(suggestion);
    }

    @Override
    public Suggestion removeSuggestionWorker(Suggestion suggestion, Person worker)
            throws SuggestionWorkerNotWorkingOnSuggestionException {

        List<SuggestionWorkerMapping> existingSuggestionWorkers = findSuggestionWorkers(suggestion, false);

        SuggestionWorkerMapping mappingToInactivate = existingSuggestionWorkers.stream()
                .filter(s -> worker.equals(s.getWorker()))
                .findFirst().orElseThrow(() -> new SuggestionWorkerNotWorkingOnSuggestionException(
                        "Suggestion {} does not contain suggestion worker {}", suggestion.getId(), worker.getId()));

        mappingToInactivate.setInactivated(timeService.currentTimeMillisUTC());

        SuggestionWorkerMapping savedWorkerMapping = saveAndRetryOnDataIntegrityViolation(
                () -> suggestionWorkerMappingRepository.saveAndFlush(mappingToInactivate),
                //if it could not be saved, try to get the one that was saved in parallel, otherwise return the same
                () -> suggestionWorkerMappingRepository.findById(mappingToInactivate.getId()).orElse(mappingToInactivate)
        );

        List<SuggestionWorkerMapping> newSuggestionWorkers = new ArrayList<>(existingSuggestionWorkers);
        newSuggestionWorkers.remove(savedWorkerMapping);
        suggestion.setSuggestionWorkerMappings(newSuggestionWorkers);
        //remove the worker from the internal chat
        //this null check is only required for existing suggestions that do not have a chat
        if (suggestion.getInternalWorkerChat() != null) {
            suggestion.setInternalWorkerChat(
                    grapevineChatService.removeSuggestionWorkerFromSuggestionInternalWorkerChat(
                            suggestion.getInternalWorkerChat(), worker));
        }
        suggestion.setLastSuggestionActivity(timeService.currentTimeMillisUTC());
        return store(suggestion);
    }

    @Override
    @NonNull
    public Suggestion changeSuggestionStatus(@lombok.NonNull @NonNull Suggestion suggestion,
            @lombok.NonNull @NonNull SuggestionStatus newStatus, @Nullable Person initiator)
            throws SuggestionStatusChangeInvalidException {

        SuggestionStatus oldStatus = suggestion.getSuggestionStatus();
        //the old status can be null if it is a newly created suggestion
        if (oldStatus != null && !ALLOWED_SUGGESTION_STATUS_TRANSITIONS.containsEntry(oldStatus, newStatus)) {
            throw new SuggestionStatusChangeInvalidException(oldStatus, newStatus);
        }

        List<SuggestionStatusRecord> suggestionStatusRecords = findSuggestionStatusRecords(suggestion);
        //we need to check that the new status record does not get the same timestamp
        // since this would cause confusion when ordering them
        long timestamp = timeService.currentTimeMillisUTC();
        if (!suggestionStatusRecords.isEmpty()) {
            SuggestionStatusRecord currentStatusRecord = suggestionStatusRecords.get(0);
            if (timestamp <= currentStatusRecord.getCreated()) {
                timestamp++;
                if (timestamp <= currentStatusRecord.getCreated()) {
                    log.warn("Events from the past for suggestion {}, " +
                                    "last status record timestamp is {}, current time is {}, taking last timestamp + 1",
                            suggestion.getId(),
                            currentStatusRecord.getCreated(), timestamp);
                    timestamp = currentStatusRecord.getCreated() + 1;
                }
            }
        }

        SuggestionStatusRecord statusRecord = SuggestionStatusRecord.builder()
                .suggestion(suggestion)
                .status(newStatus)
                .initiator(initiator)
                .build();
        statusRecord.setCreated(timestamp);
        SuggestionStatusRecord savedStatusRecord = saveAndRetryOnDataIntegrityViolation(
                () -> suggestionStatusRecordRepository.saveAndFlush(statusRecord),
                () -> suggestionStatusRecordRepository.findFirstBySuggestionOrderByCreatedDesc(suggestion));
        suggestionStatusRecords.add(0, savedStatusRecord);
        suggestion.setSuggestionStatusRecords(suggestionStatusRecords);
        suggestion.setSuggestionStatus(newStatus);
        suggestion.setLastSuggestionActivity(timeService.currentTimeMillisUTC());
        return store(suggestion);
    }

    @Override
    @NonNull
    public Suggestion resetStatusAndWorkerForDemoSuggestion(@lombok.NonNull @NonNull Suggestion suggestion)
            throws SuggestionStatusChangeInvalidException {

        final List<SuggestionStatusRecord> statusRecords =
                suggestionStatusRecordRepository.findBySuggestionOrderByCreatedDesc(suggestion);
        suggestionStatusRecordRepository.deleteAll(statusRecords);
        suggestion.setSuggestionStatus(null);
        suggestion = changeSuggestionStatus(suggestion, SuggestionStatus.OPEN, suggestion.getCreator());

        final List<SuggestionWorkerMapping> workerMappings =
                suggestionWorkerMappingRepository.findBySuggestionOrderByCreatedAsc(suggestion);
        suggestionWorkerMappingRepository.deleteAll(workerMappings);

        return useCachedEntities(suggestion);
    }

    @Override
    public Suggestion store(Suggestion entity) {
        Suggestion savedSuggestion = suggestionRepository.saveAndFlush(entity);
        //we need to add these after saving, since the volatile fields are deleted when saving them
        savedSuggestion.setSuggestionStatusRecords(entity.getSuggestionStatusRecords());
        savedSuggestion.setSuggestionWorkerMappings(entity.getSuggestionWorkerMappings());
        return useCachedEntities(savedSuggestion);
    }

    @Override
    public void checkSuggestionRoles(Person person, Tenant tenant) throws NotAuthorizedException {
        if (!roleService.hasRoleForEntity(person, ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER, tenant)) {
            throw newNotAuthorizedExceptionForSuggestionRoles();
        }
    }

    @Override
    public Set<String> checkSuggestionRolesAndReturnTenantIds(Person person) throws NotAuthorizedException {
        final Set<String> assignedTenantIds =
                roleService.getRelatedEntityIdsOfRoleAssignments(person,
                        ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER_UNTYPED);
        if (assignedTenantIds.isEmpty()) {
            throw newNotAuthorizedExceptionForSuggestionRoles();
        }
        return assignedTenantIds;
    }

    @Override
    public void checkSuggestionRoles(Person person) throws NotAuthorizedException {
        if (!roleService.hasRoleForAnyEntity(person, ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER_UNTYPED)) {
            throw newNotAuthorizedExceptionForSuggestionRoles();
        }
    }

    @Override
    public void checkNewWorker(Person newWorker, Tenant tenant) throws NotAuthorizedException {
        if (!roleService.hasRoleForEntity(newWorker, ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER, tenant)) {
            throw new NotAuthorizedException("Only persons with the following roles can be added to a suggestion: "
                    + buildRolesString());
        }
    }

    private NotAuthorizedException newNotAuthorizedExceptionForSuggestionRoles() {

        return new NotAuthorizedException("This operation is only allowed for persons having one of the following " +
                "roles: " + buildRolesString());
    }

    private String buildRolesString() {
        return ROLES_SUGGESTION_FIRST_RESPONDER_AND_WORKER
                .stream()
                .map(roleClass -> roleService.getRole(roleClass))
                .map(role -> role.getKey() + " (" + role.getDescription() + ")")
                .collect(Collectors.joining(", "));
    }

}
