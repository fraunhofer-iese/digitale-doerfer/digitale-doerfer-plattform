/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.communication.model.ChatMessage;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.DorfFunkConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.LoesBarConstants;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

public interface IGrapevineChatService {

    /**
     * Find all chats the person is a participant.
     * <p/>
     * Only chats that are related to grapevine are returned. These are all chats with topic {@link
     * DorfFunkConstants#CHAT_TOPIC}.
     *
     * @param person the person to find the chats for
     *
     * @return all chats of the person or an empty list if there are none
     */
    List<Chat> findOwnChats(Person person);

    /**
     * Find all chats and suggestions the person is a participant.
     * <p/>
     *
     * @param person
     *         the person to find the chats for
     *
     * @return all chats with suggestions of the person or an empty list if there are none
     */
    List<Chat> findOwnChatsAndSuggestionInternalWorkerChats(Person person);

    /**
     * Find a chat with exactly these two persons as participants. The order of participants is not relevant.
     * <p/>
     * Only chats that are related to grapevine are returned. These are all chats with topic {@link
     * DorfFunkConstants#CHAT_TOPIC}.
     *
     * @param participantOne a participant of the chat
     * @param participantTwo a participant of the chat
     *
     * @return the existing chat between these two persons or a newly created one
     */
    Chat findOrCreateChat(Person participantOne, Person participantTwo, BaseEntity subject);

    /**
     * Create an internal worker chat for a suggestion. This chat gets the topic {@link
     * LoesBarConstants#SUGGESTION_INTERNAL_WORKER_CHAT_TOPIC}.
     *
     * @param suggestion the suggestion to create the chat for
     *
     * @return the newly created chat
     */
    Chat createSuggestionInternalWorkerChat(Suggestion suggestion);

    /**
     * Add a suggestion worker to an internal chat. The worker can read all messages in that chat, also old ones.
     *
     * @param suggestionInternalWorkerChat
     *         the chat to add the worker to
     * @param newWorker
     *         the worker to add
     *
     * @return the modified chat
     */
    Chat addSuggestionWorkerToSuggestionInternalWorkerChat(Chat suggestionInternalWorkerChat, Person newWorker);

    /**
     * Remove a suggestion worker from an internal chat. The worker can not read any messages of this chat afterwards.
     *
     * @param suggestionInternalWorkerChat
     *         the chat to remove the worker from
     * @param workerToRemove
     *         the worker to remove
     *
     * @return the modified chat
     */
    Chat removeSuggestionWorkerFromSuggestionInternalWorkerChat(Chat suggestionInternalWorkerChat,
            Person workerToRemove);

    /**
     * Add a special message to the chat that references a post.
     *
     * @param chat
     * @param sender
     * @param post
     * @param sentTime
     * @return
     */
    Pair<Chat, ChatMessage> addPostReferenceToChat(Chat chat, Person sender, Post post, long sentTime);

    /**
     * Add a special message to the chat that references a comment.
     *
     * @param chat
     * @param sender
     * @param comment
     * @param sentTime
     * @return
     */
    Pair<Chat, ChatMessage> addCommentReferenceToChat(Chat chat, Person sender, Comment comment, long sentTime);

}
