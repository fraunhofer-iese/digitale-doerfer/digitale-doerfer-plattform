/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.ExternalPostPublishConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.ExternalPostUnpublishConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.SuggestionWithdrawConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class GrapevineSearchEventProcessor extends BaseEventProcessor {

    @Autowired
    private IGrapevineSearchService grapevineSearchService;

    @EventProcessing
    private void handlePostCreateConfirmation(PostCreateConfirmation<?> event) {
        // not published posts should not be indexed
        if (!event.isPublished()) {
            return;
        }
        grapevineSearchService.indexOrUpdateIndexedPost(event.getPost());
    }

    @EventProcessing
    private void handleExternalPostPublishConfirmation(ExternalPostPublishConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedPost(event.getExternalPost());
    }
    
    @EventProcessing
    private void handleExternalPostUnpublishConfirmation(ExternalPostUnpublishConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedPost(event.getExternalPost());
    }

    @EventProcessing
    private void handlePostChangeConfirmation(PostChangeConfirmation<?> event) {
        grapevineSearchService.indexOrUpdateIndexedPost(event.getPost());
    }

    @EventProcessing
    private void handlePostDeleteConfirmation(PostDeleteConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedPost(event.getPost());
    }

    @EventProcessing
    private void handleSuggestionWithdrawConfirmation(SuggestionWithdrawConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedPost(event.getSuggestion());
    }

    @EventProcessing
    private void handleCommentCreateConfirmation(CommentCreateConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedComment(event.getComment());
    }

    @EventProcessing
    private void handleCommentChangeConfirmation(CommentChangeConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedComment(event.getComment());
    }

    @EventProcessing
    private void handleCommentDeleteConfirmation(CommentDeleteConfirmation event) {
        grapevineSearchService.indexOrUpdateIndexedComment(event.getComment());
    }

}
