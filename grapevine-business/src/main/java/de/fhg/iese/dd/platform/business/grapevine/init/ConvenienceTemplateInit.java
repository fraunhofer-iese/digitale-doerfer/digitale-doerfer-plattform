/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import java.util.Set;

import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Not persisted, only used for init of a template for conveniences that are created for app variants and news sources
 */
@Getter
@NoArgsConstructor
public class ConvenienceTemplateInit extends Convenience {

    @SuppressWarnings("unused")
    private Set<String> excludedNewsSourceIds;

}
