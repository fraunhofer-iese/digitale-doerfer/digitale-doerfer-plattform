/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupJoinConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinAcceptRequest;
import de.fhg.iese.dd.platform.business.grapevine.events.group.GroupPendingJoinDenyRequest;
import de.fhg.iese.dd.platform.business.grapevine.template.GrapevineTemplate;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembership;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.GroupMembershipStatus;
import de.fhg.iese.dd.platform.datamanagement.grapevine.roles.GroupMembershipAdmin;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
class GroupEmailEventProcessor extends BaseEventProcessor {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private IAuthorizationService authorizationService;

    @Autowired
    private GrapevineConfig grapevineConfig;

    @EventProcessing
    void handleGroupJoinConfirmation(GroupJoinConfirmation event) {

        if (event.getMembership().getStatus() != GroupMembershipStatus.PENDING) {
            return;
        }

        final GroupMembership groupMembership = event.getMembership();

        final List<Person> groupMembershipAdmins =
                roleService.getAllPersonsWithRoleForEntity(GroupMembershipAdmin.class,
                        groupMembership.getGroup());

        if (groupMembershipAdmins.isEmpty()) {
            log.warn("Person {} wants to join group {}, but no group membership admins are available for notification",
                    groupMembership.getMember().getId(), groupMembership.getGroup().getId());
            return;
        }

        String fromEmailAddress = grapevineConfig.getSenderEmailAddressDorffunk();
        emailSenderService.sendHtmlEmailToPersons(fromEmailAddress,
                groupMembershipAdmins,
                GrapevineTemplate.NEW_GROUP_JOIN_REQUEST_EMAIL,
                receiver -> buildTemplateModel(receiver, groupMembership),
                GrapevineTemplate.formattedString(GrapevineTemplate.NEW_GROUP_JOIN_REQUEST_SUBJECT,
                        groupMembership.getGroup().getName())
        );
    }

    private Map<String, Object> buildTemplateModel(Person receiver, GroupMembership groupMembership) {
        final Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("receiverName", receiver.getFullName());
        templateModel.put("personToAccept", groupMembership.getMember().getFirstNameFirstCharLastDot());
        templateModel.put("groupName", groupMembership.getGroup().getName());
        templateModel.put("introductionText", groupMembership.getMemberIntroductionText());
        templateModel.put("groupJoinAcceptLink", UriComponentsBuilder
                .fromHttpUrl(grapevineConfig.getDorffunkBaseUrl())
                .queryParam("type", "accept")
                .queryParam("group", groupMembership.getGroup().getId())
                .queryParam("admin", receiver.getId())
                .queryParam("person", groupMembership.getMember().getId())
                .queryParam("token", authorizationService.generateAuthToken(
                        GroupPendingJoinAcceptRequest.builder()
                                .group(groupMembership.getGroup())
                                .acceptingPerson(receiver)
                                .personToAccept(groupMembership.getMember())
                                .build(),
                        Duration.ofDays(120)))
                .build());
        templateModel.put("groupJoinDenyLink", UriComponentsBuilder
                .fromHttpUrl(grapevineConfig.getDorffunkBaseUrl())
                .queryParam("type", "deny")
                .queryParam("group", groupMembership.getGroup().getId())
                .queryParam("admin", receiver.getId())
                .queryParam("person", groupMembership.getMember().getId())
                .queryParam("token", authorizationService.generateAuthToken(
                        GroupPendingJoinDenyRequest.builder()
                                .group(groupMembership.getGroup())
                                .denyingPerson(receiver)
                                .personToDeny(groupMembership.getMember())
                                .build(),
                        Duration.ofDays(120)))
                .build());
        return templateModel;
    }

}
