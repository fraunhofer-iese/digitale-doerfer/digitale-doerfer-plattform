/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentDeleteConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.suggestion.*;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineChatService;
import de.fhg.iese.dd.platform.business.grapevine.services.ISuggestionService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.SuggestionPostTypeFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.beans.factory.annotation.Autowired;

@EventProcessor
class SuggestionEventProcessor extends BasePostEventProcessor {

    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private IGrapevineChatService grapevineChatService;
    @Autowired
    private IFeatureService featureService;

    @EventProcessing
    private PostCreateConfirmation<Suggestion> handleSuggestionCreateRequest(SuggestionCreateRequest request) {

        Person creator = request.getCreator();
        AppVariant appVariant = request.getAppVariant();

        final boolean publicCommentCreation = featureService
                .getFeatureOptional(SuggestionPostTypeFeature.class, FeatureTarget.of(creator, appVariant))
                .map(SuggestionPostTypeFeature::isPublicCommentCreation)
                //in case there is no config, we allow it
                .orElse(true);

        request.setCommentsDisallowed(!publicCommentCreation);
        Suggestion suggestion = handleBasePostCreateRequest(request, Suggestion.builder()
                .suggestionCategory(request.getSuggestionCategory())
                //there is no initial status, so that the changeSuggestionStatus later on works
                .suggestionStatus(null)
                .commentsAllowed(publicCommentCreation)
                .internal(request.isInternal())
                //we set it to enable correct sorting
                .lastSuggestionActivity(timeService.currentTimeMillisUTC())
                .build())
                .getPost();
        //we can only create the chat after the suggestion has been saved, since it is referenced in the chat as subject
        suggestion.setInternalWorkerChat(grapevineChatService.createSuggestionInternalWorkerChat(suggestion));
        suggestion = suggestionService.store(suggestion);
        //we use the service to "change" the status, so that an initial status record is created
        suggestion = suggestionService.changeSuggestionStatus(suggestion, SuggestionStatus.OPEN, creator);
        return PostCreateConfirmation.<Suggestion>builder()
                .post(suggestion)
                .appVariant(request.getAppVariant())
                .published(true)
                .build();
    }

    @EventProcessing
    private PostChangeConfirmation<Suggestion> handleSuggestionChangeRequest(SuggestionChangeRequest request) {

        Suggestion suggestionToBeChanged = request.getPost();
        if (request.getSuggestionCategory() != null) {
            suggestionToBeChanged.setSuggestionCategory(request.getSuggestionCategory());
        }
        return changePost(request, suggestionToBeChanged);
    }

    @EventProcessing
    private SuggestionWithdrawConfirmation handleSuggestionWithdrawRequest(SuggestionWithdrawRequest request) {

        final long currentTime = timeService.currentTimeMillisUTC();
        Suggestion suggestion = request.getSuggestion();
        suggestion.setDeleted(true);
        suggestion.setDeletionTime(currentTime);
        suggestion.setOverrideDeletedForSuggestionRoles(true);
        suggestion.setWithdrawReason(request.getReason());
        suggestion.setWithdrawn(currentTime);

        Suggestion savedSuggestion = suggestionService.store(suggestion);
        return new SuggestionWithdrawConfirmation(savedSuggestion);
    }

    @EventProcessing
    private SuggestionStatusChangeConfirmation handleSuggestionStatusChangeRequest(
            SuggestionStatusChangeRequest request) {

        Suggestion changedSuggestion = suggestionService.changeSuggestionStatus(request.getSuggestion(),
                request.getNewStatus(), request.getSuggestionStatusChangeInitiator());

        return new SuggestionStatusChangeConfirmation(changedSuggestion);
    }

    @EventProcessing
    private SuggestionCategoryChangeConfirmation handleSuggestionCategoryChangeRequest(
            SuggestionCategoryChangeRequest request) {

        Suggestion suggestion = request.getSuggestion();

        suggestion.setSuggestionCategory(request.getNewSuggestionCategory());

        Suggestion savedSuggestion = suggestionService.store(suggestion);
        return new SuggestionCategoryChangeConfirmation(savedSuggestion);
    }

    @EventProcessing
    private SuggestionWorkerAddConfirmation handleSuggestionWorkerAddRequest(SuggestionWorkerAddRequest request) {

        Suggestion suggestion = request.getSuggestion();
        Person newWorker = request.getWorker();
        Person initiator = request.getSuggestionWorkerAddInitiator();

        Suggestion updatedSuggestion = suggestionService.addSuggestionWorker(suggestion, newWorker, initiator);

        return SuggestionWorkerAddConfirmation.builder()
                .suggestion(updatedSuggestion)
                .initiator(initiator)
                .worker(newWorker)
                .build();
    }

    @EventProcessing
    private SuggestionWorkerRemoveConfirmation handleSuggestionWorkerRemoveRequest(SuggestionWorkerRemoveRequest request) {

        Suggestion suggestion = request.getSuggestion();
        Person workerToRemove = request.getWorkerToRemove();

        Suggestion updatedSuggestion = suggestionService.removeSuggestionWorker(suggestion, workerToRemove);

        return SuggestionWorkerRemoveConfirmation.builder()
                .suggestion(updatedSuggestion)
                .removedWorker(workerToRemove)
                .build();
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
    private void updateSuggestionOnCommentCreation(CommentCreateConfirmation event) {

        final Comment comment = event.getComment();
        if (!(comment.getPost() instanceof Suggestion suggestion)) {
            return;
        }
        suggestion.setLastSuggestionActivity(comment.getLastModified());
        suggestionService.store(suggestion);
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
    private void updateSuggestionOnCommentChange(CommentChangeConfirmation event) {

        final Comment comment = event.getComment();
        if (!(comment.getPost() instanceof Suggestion suggestion)) {
            return;
        }
        suggestion.setLastSuggestionActivity(comment.getLastModified());
        suggestionService.store(suggestion);
    }

    @EventProcessing(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_PREFERRED)
    private void updateSuggestionOnCommentDelete(CommentDeleteConfirmation event) {

        final Comment comment = event.getComment();
        if (!(comment.getPost() instanceof Suggestion suggestion)) {
            return;
        }
        suggestion.setLastSuggestionActivity(comment.getLastModified());
        suggestionService.store(suggestion);
    }

}
