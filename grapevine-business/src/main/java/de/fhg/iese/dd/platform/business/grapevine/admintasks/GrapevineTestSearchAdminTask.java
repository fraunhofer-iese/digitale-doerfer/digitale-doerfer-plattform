/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchProperties;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class GrapevineTestSearchAdminTask extends BaseAdminTask {

    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private ElasticsearchOperations elasticsearchOperations;
    @Autowired
    private ElasticsearchProperties config;

    @Override
    public String getName() {
        return "GrapevineTestSearch";
    }

    @Override
    public String getDescription() {
        return "Tests if the grapevine search is configured well. Tests for all permissions of the technical user, also tests if they are restrictive enough";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {

        /*
        These are the permissions that should be given to the user. It might be that less are still okay.
        The cluster permissions for index operations are required to make the index permissions work.

            Cluster permissions
                cluster_monitor
                indices_monitor
                crud
                cluster_composite_ops
            Index permissions
                prefix*
                    unlimited
         */

        logSummary.info("Checking user {} at {}", config.getUsername(), config.getUris());
        Gossip fakePost = Gossip.builder()
                .text("Fake post")
                .creator(Person.builder().build())
                .geoAreas(Set.of(GeoArea.builder().build()))
                .build();

        grapevineSearchService.ensureIndexExists(logSummary);
        logSummary.info("✅ index check ok");
        grapevineSearchService.indexOrUpdateIndexedPost(fakePost);
        logSummary.info("✅ update ok");
        fakePost.setDeleted(true);
        grapevineSearchService.indexOrUpdateIndexedPost(fakePost);
        logSummary.info("✅ delete ok");

        try {
            final IndexCoordinates notAccessibleIndex = IndexCoordinates.of("private-index");
            elasticsearchOperations.indexOps(notAccessibleIndex).exists();
            logSummary.error("❌ exists on not accessible index");
            elasticsearchOperations.indexOps(notAccessibleIndex).create();
            logSummary.error("❌ create on not accessible index");
            elasticsearchOperations.indexOps(notAccessibleIndex).delete();
            logSummary.error("❌ delete on not accessible index");
        } catch (Exception e) {
            logSummary.info("✅ not accessible index ok");
        }
    }

}
