/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import de.fhg.iese.dd.platform.business.communication.services.IChatService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.services.*;
import de.fhg.iese.dd.platform.business.participants.person.init.PersonDataInitializer;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.init.BaseDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.DataInitKey;
import de.fhg.iese.dd.platform.business.shared.init.IManualTestDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.communication.model.Chat;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.repos.GeoAreaRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Consumer;

@Component
public class GrapevineDemoDataInitializer extends BaseDataInitializer implements IManualTestDataInitializer {

    private static final long VIRTUAL_TODAY_START =
            ZonedDateTime.of(2031, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toInstant().toEpochMilli();

    @Autowired
    private GeoAreaRepository geoAreaRepository;
    @Autowired
    private NewsSourceRepository newsSourceRepository;

    @Autowired
    private IPersonService personService;
    @Autowired
    private IPostService postService;
    @Autowired
    private ITradingCategoryService tradingCategoryService;
    @Autowired
    private ISuggestionService suggestionService;
    @Autowired
    private ISuggestionCategoryService suggestionCategoryService;
    @Autowired
    private IGrapevineChatService grapevineChatService;
    @Autowired
    private IChatService chatService;
    @Autowired
    private ICommentService commentService;

    @Override
    public DataInitializerConfiguration getConfiguration() {
        return DataInitializerConfiguration.builder()
                .topic("post-demo")
                .requiredEntity(Tenant.class)
                .requiredEntity(Person.class)
                .requiredEntity(Organization.class)
                .requiredEntity(NewsSource.class)
                .requiredEntity(TradingCategory.class)
                .requiredEntity(SuggestionCategory.class)
                .requiredEntity(PushCategory.class)
                .processedEntity(Gossip.class)
                .processedEntity(NewsItem.class)
                .processedEntity(Seeking.class)
                .processedEntity(Offer.class)
                .processedEntity(Suggestion.class)
                .processedEntity(Happening.class)
                .processedEntity(Comment.class)
                .build();
    }

    @Override
    public String getScenarioId() {
        return "grapevine-demo-1";
    }

    @Override
    public Collection<String> getDependentTopics() {
        return unmodifiableList(PersonDataInitializer.TOPIC_DEMO,
                OrganizationDataInitializer.TOPIC,
                NewsSourceDataInitializer.TOPIC,
                GrapevineCategoryDataInitializer.TOPIC,
                GrapevinePushDataInitializer.TOPIC);
    }

    @Override
    protected void createInitialDataSimple(LogSummary logSummary) {

        loadPosts(Gossip.class, PostType.Gossip, gossip -> createGossip(gossip, logSummary), logSummary);
        loadPosts(Seeking.class, PostType.Seeking, seeking -> createSeeking(seeking, logSummary), logSummary);
        loadPosts(Offer.class, PostType.Offer, offer -> createOffer(offer, logSummary), logSummary);
        loadPosts(Suggestion.class, PostType.Suggestion, suggestion -> createSuggestion(suggestion, logSummary), logSummary);
        loadPosts(NewsItem.class, PostType.NewsItem, newsItem -> createNewsItem(newsItem, logSummary), logSummary);
        loadPosts(Happening.class, PostType.Happening, happening -> createHappening(happening, logSummary), logSummary);
        //in all create methods, existing comments have been deleted
        createComments(logSummary);
    }

    private <T extends Post> void loadPosts(Class<T> postClass, PostType postType, Consumer<T> postConsumer, LogSummary logSummary) {

        Map<DataInitKey, List<T>> keysAndEntities = loadAllDataInitKeysAndEntities(postClass);
        for (Map.Entry<DataInitKey, List<T>> keyAndEntities : keysAndEntities.entrySet()) {
            Tenant tenant = keyAndEntities.getKey().getTenant();
            if (tenant == null) {
                throw new DataInitializationException("No tenant provided at {}",
                        keyAndEntities.getKey().getFileNames());
            }
            Set<GeoArea> geoAreasIncluded = keyAndEntities.getKey().getGeoAreasIncluded();
            if (CollectionUtils.isEmpty(geoAreasIncluded)) {
                throw new DataInitializationException("No geo areas provided at {}",
                        keyAndEntities.getKey().getFileNames());
            }


            List<T> posts = keyAndEntities.getValue();
            for (GeoArea geoArea : geoAreasIncluded) {
                //all posts that are not part of the demo are deleted, with a maximum of 10
                postService.deleteDemoPosts(geoArea, postType, new HashSet<>(posts), logSummary);
            }
            for (T post : posts) {
                post.setTenant(tenant);
                post.setGeoAreas(geoAreasIncluded);
                postConsumer.accept(post);
            }
        }
    }

    private void createGossip(Gossip gossip, LogSummary logSummary) {

        gossip = createPost(gossip, logSummary);
        gossip = postService.storePost(gossip);
        logSummary.info("Created Gossip '{}' in {}",
                StringUtils.abbreviate(gossip.getText(), 25), gossip.getGeoAreas());
    }

    private void createNewsItem(NewsItem newsItem, LogSummary logSummary) {

        newsItem = createExternalPost(newsItem, logSummary);
        newsItem = postService.storePost(newsItem);
        logSummary.info("Created NewsItem '{}' in {}",
                StringUtils.abbreviate(newsItem.getText(), 25), newsItem.getGeoAreas());
    }

    private void createHappening(Happening happening, LogSummary logSummary) {

        happening = createExternalPost(happening, logSummary);
        happening.setStartTime(shiftVirtualTimeToRealTime(happening.getStartTime()));
        happening.setEndTime(shiftVirtualTimeToRealTime(happening.getEndTime()));
        happening = postService.storePost(happening);
        logSummary.info("Created Happening '{}' in {}",
                StringUtils.abbreviate(happening.getText(), 25), happening.getGeoAreas());
    }

    private void createSuggestion(Suggestion suggestion, LogSummary logSummary) {

        //check suggestion status
        if (suggestion.getSuggestionStatus() != SuggestionStatus.OPEN) {
            throw new DataInitializationException("Only SuggestionStatus.OPEN is currently supported. " +
                    "Suggestion {} uses {}.", suggestion.getId(), suggestion.getSuggestionStatus());
        }

        //storing old chat to be deleted later
        Chat oldInternalChat;
        try {
            final Suggestion existingSuggestion = suggestionService.findSuggestionById(suggestion.getId(), false);
            oldInternalChat = existingSuggestion.getInternalWorkerChat();
            if (oldInternalChat != null) {
                log.debug("Found existing internal worker chat with id {}", oldInternalChat.getId());
            }
        } catch (PostNotFoundException e) {
            oldInternalChat = null;
        }

        suggestion = createPost(suggestion, logSummary);
        suggestion = fetchSuggestionCategoryFromDatabaseAndUpdateSuggestion(suggestion);
        suggestion = postService.storePost(suggestion);
        log.debug("initial suggestion with id {} stored", suggestion.getId());

        //reset worker mapping and status
        suggestionService.resetStatusAndWorkerForDemoSuggestion(suggestion);
        //create "fresh" chat for the suggestion
        suggestion.setInternalWorkerChat(grapevineChatService.createSuggestionInternalWorkerChat(suggestion));
        suggestion = suggestionService.store(suggestion);
        log.debug("added new worker chat to suggestion with id {}", suggestion.getId());

        if (oldInternalChat != null) {
            chatService.deleteChat(oldInternalChat);
            log.debug("Deleted old chat with id {}", oldInternalChat.getId());
        }

        logSummary.info("Created Suggestion '{}' in {}",
                StringUtils.abbreviate(suggestion.getText(), 25), suggestion.getGeoAreas());
    }

    private void createSeeking(Seeking seeking, LogSummary logSummary) {

        seeking = fetchTradingCategoryFromDatabaseAndUpdateTrade(seeking);
        seeking = createPost(seeking, logSummary);
        seeking = postService.storePost(seeking);
        logSummary.info("Created Seeking '{}' in {}",
                StringUtils.abbreviate(seeking.getText(), 25), seeking.getGeoAreas());

    }

    private void createOffer(Offer offer, LogSummary logSummary) {

        offer = fetchTradingCategoryFromDatabaseAndUpdateTrade(offer);
        offer = createPost(offer, logSummary);
        offer = postService.storePost(offer);
        logSummary.info("Created Offer '{}' in {}",
                StringUtils.abbreviate(offer.getText(), 25), offer.getGeoAreas());
    }

    private <T extends Trade> T fetchTradingCategoryFromDatabaseAndUpdateTrade(T trade) {

        final TradingCategory tradingCategory = tradingCategoryService
                .findTradingCategoryById(checkOrLookupId(trade.getTradingCategory().getId()));
        trade.setTradingCategory(tradingCategory);
        return trade;
    }

    private Suggestion fetchSuggestionCategoryFromDatabaseAndUpdateSuggestion(Suggestion suggestion) {

        final SuggestionCategory suggestionCategory = suggestionCategoryService
                .findSuggestionCategoryById(checkOrLookupId(suggestion.getSuggestionCategory().getId()));
        suggestion.setSuggestionCategory(suggestionCategory);
        return suggestion;
    }

    private <P extends ExternalPost> P createExternalPost(P dataJsonPost, LogSummary logSummary) {

        String newsSourceId = checkOrLookupId(dataJsonPost.getNewsSource().getId());
        P createdPost = createPost(dataJsonPost, logSummary);
        String postId = createdPost.getId();
        NewsSource newsSource = newsSourceRepository.findById(newsSourceId)
                .orElseThrow(
                        () -> new DataInitializationException("Could not find news source {} referenced by post {}",
                                newsSourceId, postId));
        createdPost.setNewsSource(newsSource);
        return dataJsonPost;
    }

    private <P extends Post> P createPost(P dataJsonPost, LogSummary logSummary) {

        checkId(dataJsonPost);
        dataJsonPost.setDeleted(false);
        dataJsonPost.setDeletionTime(null);
        dataJsonPost.setCreated(shiftVirtualTimeToRealTime(dataJsonPost.getCreated()));
        if (dataJsonPost.getLastActivity() == 0) {
            dataJsonPost.setLastActivity(dataJsonPost.getCreated());
        } else {
            dataJsonPost.setLastActivity(shiftVirtualTimeToRealTime(dataJsonPost.getLastActivity()));
        }
        if (dataJsonPost.getLastModified() == 0) {
            dataJsonPost.setLastModified(dataJsonPost.getCreated());
        } else {
            dataJsonPost.setLastModified(shiftVirtualTimeToRealTime(dataJsonPost.getLastModified()));
        }
        if (dataJsonPost.getCreator() != null) {
            String creatorId = checkOrLookupId(dataJsonPost.getCreator().getId());
            Person creator = personService.findPersonById(creatorId);
            dataJsonPost.setCreator(creator);
        }

        Set<GeoArea> dataJsonPostGeoAreas = dataJsonPost.getGeoAreas();
        if (CollectionUtils.isEmpty(dataJsonPostGeoAreas)) {
            throw new DataInitializationException("Post {} does not define any geo areas", dataJsonPost.getId());
        }
        Set<GeoArea> geoAreas = new HashSet<>();

        for (GeoArea dataJsonPostGeoArea : dataJsonPostGeoAreas) {
            String geoAreaId = checkOrLookupId(dataJsonPostGeoArea.getId());
            GeoArea geoArea = geoAreaRepository.findById(geoAreaId)
                    .orElseThrow(
                            () -> new DataInitializationException("Could not find geo area {} referenced by post {}",
                                    geoAreaId,
                                    dataJsonPost.getId()));
            geoAreas.add(geoArea);
        }

        dataJsonPost.setGeoAreas(geoAreas);

        if (dataJsonPost.getImages() != null) {
            createImages(Post::getImages, Post::setImages, dataJsonPost, FileOwnership.UNKNOWN, logSummary, false);
        }
        if (dataJsonPost.getCustomAddress() != null) {
            createAddress(Post::getCustomAddress, Post::setCustomAddress, dataJsonPost, logSummary);
        }

        commentService.deleteCommentsForDemoPost(dataJsonPost);
        return dataJsonPost;
    }

    private void createComments(LogSummary logSummary) {
        Map<DataInitKey, List<CommentDemo>> keysAndEntities = loadAllDataInitKeysAndEntities(CommentDemo.class);
        for (Map.Entry<DataInitKey, List<CommentDemo>> keyAndEntities : keysAndEntities.entrySet()) {
            Tenant tenant = keyAndEntities.getKey().getTenant();
            if (tenant == null) {
                throw new DataInitializationException("No tenant provided at {}",
                        keyAndEntities.getKey().getFileNames());
            }
            for (CommentDemo commentDemo : keyAndEntities.getValue()) {

                checkId(commentDemo);
                Post post = postService.findPostByIdIncludingDeleted(commentDemo.getPostId());
                String creatorId = checkOrLookupId(commentDemo.getCreator().getId());
                Person creator = personService.findPersonById(creatorId);

                commentDemo.setCreator(creator);
                commentDemo.setPost(post);
                commentDemo.setCreated(shiftVirtualTimeToRealTime(commentDemo.getCreated()));

                if (commentDemo.getImages() != null) {
                    createImages(Comment::getImages, Comment::setImages, commentDemo, FileOwnership.UNKNOWN, logSummary,
                            false);
                }

                Comment comment = new Comment();
                BeanUtils.copyProperties(commentDemo, comment);
                comment = commentService.store(comment);
                post.setCommentCount(post.getCommentCount() + 1);
                postService.storePost(post);
                logSummary.info("Created Comment '{}' for Post '{}'",
                        StringUtils.abbreviate(comment.getText(), 25), post.getId());
            }
        }
    }

    protected long shiftVirtualTimeToRealTime(long virtualTimeStampYYYYMMDDhhmm) {

        long virtualTime = timestampYYYYMMDDhhmmtoMillisUTC(virtualTimeStampYYYYMMDDhhmm);
        long offsetVirtualTimeToVirtualTodayStart = VIRTUAL_TODAY_START - virtualTime;
        long realStartOfDay = timeService.getSameDayStart(timeService.nowLocal()).toInstant().toEpochMilli();
        return realStartOfDay - offsetVirtualTimeToVirtualTodayStart;
    }

    @Override
    protected void dropDataSimple(LogSummary logSummary) {
        logSummary.info("Demo posts are not dropped");
    }

}
