/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.admintasks;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineSearchService;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.grapevine.config.GrapevineConfig;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.PostRepository;

@Component
public class GrapevineReindexPostsAdminTask extends BaseAdminTask {

    @Autowired
    private IGrapevineSearchService grapevineSearchService;
    @Autowired
    private GrapevineConfig grapevineConfig;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private ITimeService timeService;

    @Override
    public String getName() {
        return "GrapevineReindexPosts";
    }

    @Override
    public String getDescription() {
        return "Re-indexes all posts newer than " + grapevineConfig.getSearchablePostTime().toDays() + " days";
    }

    @Override
    public void execute(LogSummary logSummary, AdminTaskParameters parameters) throws Exception {
        long startInterval = timeService.currentTimeMillisUTC() - grapevineConfig.getSearchablePostTime().toMillis();
        long endInterval = timeService.currentTimeMillisUTC();

        processPages(logSummary, parameters,
                page -> postRepository.findAllByDeletedFalseAndLastModifiedBetweenOrderByCreated(startInterval,
                        endInterval, page),
                postPage -> {
                    for (Post post : postPage) {
                        if (!parameters.isDryRun()) {
                            grapevineSearchService.indexOrUpdateIndexedPost(post);
                        }
                        logSummary.info("Processed {} created {}",
                                StringUtils.rightPad(post.getPostType().toString(), 10),
                                timeService.toLocalTimeHumanReadable(post.getCreated()));
                    }
                });
    }

}
