/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerAtOnceCache;
import de.fhg.iese.dd.platform.business.framework.referencedata.change.IReferenceDataChangeService;
import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.DefaultSuggestionCategoryMustNotBeDeletedException;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.SuggestionCategoryNotFoundException;
import de.fhg.iese.dd.platform.business.grapevine.init.GrapevineCategoryDataInitializer;
import de.fhg.iese.dd.platform.datamanagement.framework.referencedata.model.ReferenceDataChangeLogEntry;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SuggestionCategory;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionCategoryRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.SuggestionRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class SuggestionCategoryService extends BaseEntityService<SuggestionCategory> implements ISuggestionCategoryService {

    @Autowired
    private SuggestionCategoryRepository suggestionCategoryRepository;
    @Autowired
    private SuggestionRepository suggestionRepository;
    @Autowired
    private IReferenceDataChangeService referenceDataChangeService;
    @Autowired
    private CacheCheckerAtOnceCache cacheChecker;

    private Map<String, SuggestionCategory> suggestionCategoryById;

    @PostConstruct
    private void initialize() {
        //this cache is replaced with a new map after it got cleared
        suggestionCategoryById = Collections.emptyMap();
    }

    @Override
    public CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .cachedEntity(SuggestionCategory.class)
                .build();
    }

    @Override
    public SuggestionCategory findSuggestionCategoryById(String suggestionCategoryId) throws SuggestionCategoryNotFoundException {

        checkAndRefreshCache();
        SuggestionCategory suggestionCategory = suggestionCategoryById.get(suggestionCategoryId);
        if (suggestionCategory == null) {
            throw new SuggestionCategoryNotFoundException(suggestionCategoryId);
        }
        return suggestionCategory;
    }

    @Override
    public List<SuggestionCategory> findAllSuggestionCategoriesInDataInitOrder() {

        checkAndRefreshCache();
        return suggestionCategoryById.values().stream()
                .sorted(Comparator.comparingInt(SuggestionCategory::getOrderValue))
                .collect(Collectors.toList());
    }

    @Override
    public SuggestionCategory getDefaultSuggestionCategory() {

        checkAndRefreshCache();
        SuggestionCategory defaultSuggestionCategory = suggestionCategoryById.get(
                GrapevineCategoryDataInitializer.SUGGESTION_CATEGORY_MISC_ID);
        if (defaultSuggestionCategory == null) {
            throw new IllegalStateException("Data init for topic 'post-category' has not been run");
        }
        return defaultSuggestionCategory;
    }

    @Override
    @Transactional
    public void deleteSuggestionCategory(SuggestionCategory suggestionCategory, Person deleter) throws
            DefaultSuggestionCategoryMustNotBeDeletedException {

        Objects.requireNonNull(suggestionCategory);
        final SuggestionCategory defaultCategory = getDefaultSuggestionCategory();
        if (suggestionCategory.equals(defaultCategory)) {
            throw new DefaultSuggestionCategoryMustNotBeDeletedException();
        }
        ReferenceDataChangeLogEntry referenceDataChangeLogEntry =
                referenceDataChangeService.acquireReferenceDataChangeLock(deleter,
                        "Delete " + suggestionCategory + " via API", false);
        try {
            suggestionRepository.updateSuggestionCategory(suggestionCategory, defaultCategory);
            suggestionCategoryRepository.delete(suggestionCategory);
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, true);

        } catch (Exception ex) {
            referenceDataChangeService.releaseReferenceDataChangeLock(referenceDataChangeLogEntry, false);
            throw ex;
        }
    }

    @Override
    public void invalidateCache() {

        cacheChecker.resetCacheChecks();
    }

    private void checkAndRefreshCache() {
        // if the cache is stale or no fetch ever happened
        // get all suggestion categories fresh from the database
        cacheChecker.refreshCacheIfStale(() -> {
            long start = System.currentTimeMillis();

            suggestionCategoryById = query(() -> suggestionCategoryRepository.findAll().stream()
                    .map(proxy -> Hibernate.unproxy(proxy, SuggestionCategory.class))
                    .collect(Collectors.toMap(SuggestionCategory::getId, Function.identity())));
            log.info("Refreshed {} cached suggestion categories in {} ms", suggestionCategoryById.size(),
                    System.currentTimeMillis() - start);
        });
    }

}
