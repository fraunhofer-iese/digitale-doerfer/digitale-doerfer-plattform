/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.datadeletion;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.BaseReferenceDataDeletionHandler;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.DeleteDependentEntitiesDeletionStrategy;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.DataDeletionStrategy.SwapReferencesDeletionStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

@Component
public class OrganizationDataDeletionHandler extends BaseReferenceDataDeletionHandler {

    @Autowired
    private OrganizationGeoAreaMappingRepository organizationGeoAreaMappingRepository;

    @Override
    protected Collection<SwapReferencesDeletionStrategy<?>> registerSwapReferencesDeletionStrategies() {
        return unmodifiableList(
                SwapReferencesDeletionStrategy.forTriggeringEntity(GeoArea.class)
                        .changedOrDeletedEntity(Organization.class)
                        .checkImpact(this::checkImpactGeoArea)
                        .swapData(this::swapDataGeoArea)
                        .build()
        );
    }

    @Override
    protected Collection<DeleteDependentEntitiesDeletionStrategy<?>> registerDeleteDependentEntitiesDeletionStrategies() {
        return Collections.emptyList();
    }

    private void checkImpactGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        long impactedMappings = organizationGeoAreaMappingRepository.countByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} organizations will move to new geo area", impactedMappings);
    }

    private void swapDataGeoArea(GeoArea geoAreaToBeDeleted, GeoArea geoAreaToBeUsedInstead, LogSummary logSummary) {
        Set<OrganizationGeoAreaMapping> mappingsToBeChanged =
                organizationGeoAreaMappingRepository.findMappingsToGeoAreaAAndNotToGeoAreaB(geoAreaToBeDeleted,
                        geoAreaToBeUsedInstead);

        for (OrganizationGeoAreaMapping organizationGeoAreaMapping : mappingsToBeChanged) {
            organizationGeoAreaMapping.setGeoArea(geoAreaToBeUsedInstead);
        }
        organizationGeoAreaMappingRepository.saveAll(mappingsToBeChanged);
        int deletedMappings = organizationGeoAreaMappingRepository.deleteByGeoArea(geoAreaToBeDeleted);
        logSummary.info("{} organizations moved to new geo area, {} organizations were already present",
                mappingsToBeChanged.size(), deletedMappings);
    }

}
