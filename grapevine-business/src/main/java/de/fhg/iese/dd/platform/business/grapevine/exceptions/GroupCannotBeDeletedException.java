/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class GroupCannotBeDeletedException extends BadRequestException {

    private static final long serialVersionUID = -7423050515415692333L;

    private GroupCannotBeDeletedException(String message, Object... arguments) {
        super(message, arguments);
    }

    public static GroupCannotBeDeletedException forTooManyMembers(String groupId, long memberCount) {
        return new GroupCannotBeDeletedException("Group '{}' has {} members but only a maximum of 3 members is allowed",
                groupId, memberCount);
    }
    
    public static GroupCannotBeDeletedException forContainingPosts(String groupId) {
        return new GroupCannotBeDeletedException("Group '{}' contains at least one post", groupId);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.GROUP_CANNOT_BE_DELETED;
    }

}
