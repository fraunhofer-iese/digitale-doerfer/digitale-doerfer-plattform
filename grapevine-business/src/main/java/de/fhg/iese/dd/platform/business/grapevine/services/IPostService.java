/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2024 Adeline Silva Schäfer, Balthasar Weitzel, Johannes Schneider, Stefan Schweitzer, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.PostNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.*;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface IPostService extends IEntityService<Post> {

    /**
     * Gets the post if
     * <p/>
     * it is not deleted
     * <p/>
     * or the post has hidden for other home areas true and the caller has the same home area as the post's geo area
     * <p/>
     * or if the caller is the creator of the post
     * <p/>
     * or it's an external post
     * <p>
     * or it's a group post and the caller has access to the content of the group
     *
     * @param postId
     *         the id of the post to find
     * @param caller
     *         the requesting person
     * @param callingAppVariant
     *         the app variant of the requesting person
     *
     * @return the post with the postId
     *
     * @throws PostNotFoundException
     *         if there is no post with the id or the visibility of the post does not allow the caller to see it
     */
    Post findByIdFilteredByHiddenForOtherHomeAreasAndGroupContentVisibility(String postId, Person caller,
            AppVariant callingAppVariant) throws PostNotFoundException;

    ExternalPost findExternalPostById(String postId) throws PostNotFoundException;

    Post findPostByIdNotDeleted(String postId) throws PostNotFoundException;

    Post findPostByIdIncludingDeleted(String postId) throws PostNotFoundException;

    Gossip findGossipById(String postId) throws PostNotFoundException;

    Seeking findSeekingById(String postId) throws PostNotFoundException;

    Offer findOfferById(String postId) throws PostNotFoundException;

    SpecialPost findSpecialPostById(String postId) throws PostNotFoundException;

    NewsItem findNewsItemById(String postId) throws PostNotFoundException;

    Happening findHappeningById(String postId) throws PostNotFoundException;

    ExternalPost findExternalPostByNewsSourceAndExternalIdNotDeleted(NewsSource newsSource, String externalId)
            throws PostNotFoundException;

    Optional<ExternalPost> findExternalPostByNewsSourceAndExternalIgnoringDeleted(NewsSource newsSource,
            String externalId);

    Page<Post> findAllByCreator(Person creator, PagedQuery pagedQuery);

    Page<Post> findAllByCreatorAndPostType(Person creator, PostType type, PagedQuery pagedQuery);

    List<Post> findAllByCreatorIncludingDeleted(Person currentUser);

    /**
     * Shortcut method for {@link IEntityService#store(BaseEntity)} to avoid casting.
     *
     * @param <P>
     * @param entity
     * @return
     */
    <P extends Post> P storePost(P entity);

    /**
     * Lists all posts created by another person where the given person has commented. Sorted by last comment creation
     * timestamp.
     *
     * @param person
     * @param pagedQuery
     *
     * @return
     */
    Page<Post> findAllByActivity(Person person, PagedQuery pagedQuery);

    /**
     * Lists all posts where the caller has the same home area as the post's geo area or where the caller is the creator
     * of the post. Sorts by the criteria given by {@link PagedQuery#getSortCriteria()}. The startTime and endTime are
     * dependent on these criteria.</br>
     * Organization tags filter the returned posts. If {@code requiredOrganizationTags}
     * and {@code forbiddenOrganizationTags} are empty, the filters are not used.
     *
     * @param caller                    the person to which the posts are listed
     * @param geoAreaIds                the geo areas in which the posts should be
     * @param type                      If set to {@code null}, posts of all types are returned, otherwise only posts of the given
     *                                  type.
     * @param startTime                 start of the time range, to be interpreted by the sort criteria
     * @param endTime                   end of the time range, to be interpreted by the sort criteria
     * @param requiredOrganizationTags  if not empty only posts that have an organization and the organization has any of the required tags are returned
     * @param forbiddenOrganizationTags if not empty only posts that have no organization or the organization has none of the forbidden tags are returned
     * @param pagedQuery                for paging and sorting
     * @return paged posts matching the criteria
     */
    Slice<Post> findAllPosts(Person caller, Set<String> geoAreaIds, PostType type,
            Long startTime, Long endTime, Set<OrganizationTag> requiredOrganizationTags,
            Set<OrganizationTag> forbiddenOrganizationTags, PagedQuery pagedQuery);

    Page<Post> searchPosts(String search, Person caller, Set<String> geoAreaIds,
            PageRequest pageRequest);

    /**
     * Lists all external posts of the given types and geo areas.
     *
     * @param geoAreaIds the ids of geo areas that have to match post.geoArea
     * @param types      the types of posts, has to be NewsItem or Happening.
     * @param pagedQuery for paging and sorting
     *
     * @return paged posts matching the criteria
     */
    Page<? extends ExternalPost> findAllExternalPosts(Collection<String> geoAreaIds, Collection<PostType> types,
            PagedQuery pagedQuery);

    /**
     * Lists all external posts of the given types and news source.
     *
     * @param newsSourceId the id of the news source that have to match externalPost.newsSource
     * @param types        the types of posts, has to be NewsItem or Happening.
     * @param pagedQuery   for paging and sorting
     *
     * @return paged posts matching the criteria
     */
    Page<? extends ExternalPost> findAllExternalPostsByNewsSource(String newsSourceId, Collection<PostType> types,
            PagedQuery pagedQuery);

    /**
     * Lists all posts that are not flagged or whose flag has a status other than {@code forbiddenflagstatuses}
     *
     * @param forbiddenFlagStatuses the statuses of the posts flags that should not be returned
     * @param pageRequest           for paging
     *
     * @return paged posts matching the criteria
     */
    Page<Post> findAllByUserGeneratedContentFlagStatusNotInAndCreatedBetween(
            Collection<UserGeneratedContentFlagStatus> forbiddenFlagStatuses, long start, long end,
            PageRequest pageRequest);

    /**
     * Deletes a post
     *
     * @param post the post to delete
     *
     * @return the deleted post
     */
    Post deletePost(Post post);

    void deleteAllPostsOfGroup(Group group);

    void deleteDemoPosts(GeoArea geoArea, PostType postType, Set<? extends Post> postsToNotDelete, LogSummary logSummary);

    void movePostsToParallelWorld(Person creator);

    void movePostsToRealWorld(Person creator);

}
