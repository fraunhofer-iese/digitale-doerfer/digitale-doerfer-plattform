/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.statistics;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.business.shared.statistics.BaseStatisticsProvider;
import de.fhg.iese.dd.platform.business.shared.statistics.GeoAreaStatistics;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsMetaData;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsReportDefinition;
import de.fhg.iese.dd.platform.business.shared.statistics.StatisticsTimeRelation;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Gossip;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Happening;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsItem;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Offer;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Seeking;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.SpecialPost;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Suggestion;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.UserGeneratedContentFlagRepository;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.repos.results.UserGeneratedContentFlagExtendedCountByGeoArea;

@Component
public class UserGeneratedContentFlagStatisticsProvider extends BaseStatisticsProvider {

    public static final String STATISTICS_ID_FLAGGED_CONTENT_COUNT_POST = "grapevine.flagged-content.count.post";
    public static final String STATISTICS_ID_FLAGGED_CONTENT_COUNT_COMMENT = "grapevine.flagged-content.count.comment";

    private static final Set<String> POST_ENTITY_TYPES =
            Set.of(Gossip.class.getName(), Happening.class.getName(), NewsItem.class.getName(), Offer.class.getName(),
                    Seeking.class.getName(), SpecialPost.class.getName(), Suggestion.class.getName());
    private static final String COMMENT_ENTITY_TYPE = Comment.class.getName();

    @Autowired
    private UserGeneratedContentFlagRepository userGeneratedContentFlagRepository;

    @Override
    public Collection<StatisticsMetaData> getAllMetaData() {
        Set<StatisticsMetaData> metaData = new HashSet<>();
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_FLAGGED_CONTENT_COUNT_POST)
                .machineName("flagged-content-count-post")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        metaData.add(StatisticsMetaData.builder()
                .id(STATISTICS_ID_FLAGGED_CONTENT_COUNT_COMMENT)
                .machineName("flagged-content-count-comment")
                .timeRelation(StatisticsTimeRelation.NEW)
                .timeRelation(StatisticsTimeRelation.TOTAL)
                .expectedWidth(6)
                .valueFormat("%d")
                .build());
        return metaData;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
    public void calculateGeoAreaStatistics(Collection<GeoAreaStatistics> geoAreaInfos,
            StatisticsReportDefinition definition) {

        querySummingStatistics(t -> userGeneratedContentFlagRepository.countByEntityTypesByGeoAreaId(POST_ENTITY_TYPES,
                        COMMENT_ENTITY_TYPE, t),
                Map.of(STATISTICS_ID_FLAGGED_CONTENT_COUNT_POST,
                        UserGeneratedContentFlagExtendedCountByGeoArea::getUserGeneratedContentFlagCountEntityTypes,
                        STATISTICS_ID_FLAGGED_CONTENT_COUNT_COMMENT,
                        UserGeneratedContentFlagExtendedCountByGeoArea::getUserGeneratedContentFlagCountEntityType),
                geoAreaInfos, definition);
    }

}
