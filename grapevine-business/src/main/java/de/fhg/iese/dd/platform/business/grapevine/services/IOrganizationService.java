/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import de.fhg.iese.dd.platform.business.framework.services.IEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.OrganizationNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.OrganizationTag;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IOrganizationService extends IEntityService<Organization> {

    Collection<String> getRelatedGeoAreaIds(Organization organization);

    /**
     * Finds a organization by id if it is not deleted.
     *
     * @param organizationId the id of the organization
     * @return the organization, if it exists and is not deleted
     * @throws OrganizationNotFoundException if there was no organization with this id
     */
    Organization findOrganizationById(String organizationId) throws OrganizationNotFoundException;

    /**
     * Finds all organizations for the user. These are all organizations where one of selected geo areas of the user is
     * in the geo areas of the organization. The resulting organizations are configured for the caller.
     *
     * @param caller            the caller to find and configure the organizations
     * @param callingAppVariant the app variant of the caller, used to find the selected geo areas
     * @param tags              the required tags the organizations need to have (any of)
     *
     * @return all available and configured organizations with any of the tags, ordered by name, never null.
     */
    List<Organization> findAllAvailableOrganizations(Person caller, AppVariant callingAppVariant,
            Set<OrganizationTag> tags);

    Organization saveOrganization(Organization organization, Collection<GeoArea> geoAreas);

}
