/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriTemplate;

import de.fhg.iese.dd.platform.business.framework.services.BaseEntityService;
import de.fhg.iese.dd.platform.business.grapevine.exceptions.ConvenienceNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Convenience;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.ConvenienceGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceGeoAreaMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.ConvenienceRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Service
class ConvenienceService extends BaseEntityService<Convenience> implements IConvenienceService {

    private static final String URI_VARIABLE_HOME_AREA_NAME = "homeArea.name";
    private static final String URI_VARIABLE_HOME_AREA_ZIP_CODES = "homeArea.zipCodes";
    private static final Set<String> URI_VARIABLES =
            new HashSet<>(Arrays.asList(URI_VARIABLE_HOME_AREA_NAME, URI_VARIABLE_HOME_AREA_ZIP_CODES));

    @Autowired
    private IAppService appService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private ConvenienceRepository convenienceRepository;
    @Autowired
    private ConvenienceGeoAreaMappingRepository convenienceGeoAreaMappingRepository;

    @Override
    public Convenience findConvenienceById(String convenienceId, Person caller, AppVariant appVariant)
            throws ConvenienceNotFoundException {

        final Convenience convenience = convenienceRepository.findByIdAndDeletedFalse(convenienceId)
                .orElseThrow(() -> new ConvenienceNotFoundException(convenienceId));
        return configureForPerson(convenience, caller);
    }

    @Override
    public List<Convenience> findAllAvailableConveniences(Person caller, AppVariant callingAppVariant) {

        Set<String> selectedGeoAreaIds =
                BaseEntity.toInSafeIdSet(appService.getSelectedGeoAreaIds(callingAppVariant, caller));
        return convenienceRepository.findByGeoAreaInOrderByName(selectedGeoAreaIds).stream()
                .map(c -> configureForPerson(c, caller))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Convenience saveConvenience(Convenience convenience, Collection<GeoArea> geoAreas) {

        //we need to delete existing detail images before we save the new ones, since they are not under orphan control
        final Runnable orphanMediaItemDeletion = mediaItemService
                .getOrphanItemDeletion(
                        convenienceRepository.findById(convenience.getId())
                                .map(Convenience::getDetailImages)
                                .orElse(null),
                        convenience.getDetailImages());

        final Convenience savedConvenience = convenienceRepository.saveAndFlush(convenience);

        //these are old ones that are not needed anymore
        convenienceGeoAreaMappingRepository.deleteAllByConvenienceAndGeoAreaNotIn(convenience,
                GeoArea.toInSafeEntityCollection(geoAreas));

        //these are new ones that might overwrite existing ones
        final Set<ConvenienceGeoAreaMapping> geoAreaMappings = geoAreas.stream()
                //they get the same id, so that existing ones get overwritten
                .map(g -> ConvenienceGeoAreaMapping.builder()
                        .convenience(convenience)
                        .geoArea(g)
                        .build()
                        .withConstantId())
                .collect(Collectors.toSet());
        convenienceGeoAreaMappingRepository.saveAll(geoAreaMappings);
        convenienceGeoAreaMappingRepository.flush();

        //we delete the orphans at the end
        orphanMediaItemDeletion.run();
        return savedConvenience;
    }

    @Override
    @Transactional
    public Convenience store(Convenience entity) {
        return saveConvenience(entity, Collections.emptyList());
    }

    private Convenience configureForPerson(Convenience convenience, Person caller) {

        if (StringUtils.isBlank(convenience.getUrl())) {
            return convenience;
        }
        Map<String, String> uriVariables = new HashMap<>();
        final GeoArea homeArea = caller.getHomeArea();
        if (homeArea != null) {
            uriVariables.put(URI_VARIABLE_HOME_AREA_NAME, homeArea.getName());
            uriVariables.put(URI_VARIABLE_HOME_AREA_ZIP_CODES, homeArea.getZip());
        } else {
            //if the person has no home area we set empty strings, so that the expansion does not fail
            uriVariables.put(URI_VARIABLE_HOME_AREA_NAME, "");
            uriVariables.put(URI_VARIABLE_HOME_AREA_ZIP_CODES, "");
        }
        final UriTemplate uriTemplate = new UriTemplate(convenience.getUrl());
        try {
            convenience.setUrl(uriTemplate.expand(uriVariables).toString());
        } catch (IllegalArgumentException e) {
            //in this case not all variables could be filled, so we log a warning and add them with empty strings
            String unknownVariables = uriTemplate.getVariableNames().stream()
                    .filter(uv -> !URI_VARIABLES.contains(uv))
                    .peek(uv -> uriVariables.put(uv, ""))
                    .collect(Collectors.joining(", "));
            log.warn("Convenience {} contains unknown variables '{}' in url",
                    convenience.getId(),
                    unknownVariables);
            convenience.setUrl(uriTemplate.expand(uriVariables).toString());
        }
        return convenience;
    }

}
