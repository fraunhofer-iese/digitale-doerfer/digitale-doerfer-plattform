/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 - 2024 Stefan Schweitzer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.processors;

import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.grapevine.events.PostChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.PostCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentChangeConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.events.comment.CommentCreateConfirmation;
import de.fhg.iese.dd.platform.business.grapevine.services.IGrapevineTextClassificationService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.feature.ContentClassificationFeature;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Comment;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Post;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Set;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class GrapevineTextClassificationEventProcessor extends BasePostEventProcessor {

    @Autowired
    private IGrapevineTextClassificationService grapevineTextClassificationService;

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @Autowired
    private IFeatureService featureService;

    @EventProcessing
    void handlePostCreateConfirmation(PostCreateConfirmation<?> event) {
        // not published posts should not be classified
        if (!event.isPublished()) {
            return;
        }

        flagIfInappropriate(event.getPost(), event.getAppVariant());
    }

    @EventProcessing
    void handlePostChangeConfirmation(PostChangeConfirmation<?> event) {

        flagIfInappropriate(event.getPost(), event.getAppVariant());
    }

    @EventProcessing
    void handleCommentCreateConfirmation(CommentCreateConfirmation event) {

        flagIfInappropriate(event.getComment(), event.getAppVariant());
    }

    @EventProcessing
    void handleCommentChangeConfirmation(CommentChangeConfirmation event) {

        flagIfInappropriate(event.getComment(), event.getAppVariant());
    }

    private void flagIfInappropriate(Comment comment, AppVariant appVariant) {

        Post post = comment.getPost();
        flagIfInappropriate(comment, appVariant, post.getTenant(), post.getGeoAreas(),
                comment.getCreator(), comment.getText());
    }

    private void flagIfInappropriate(Post post, AppVariant appVariant) {

        flagIfInappropriate(post, appVariant, post.getTenant(), post.getGeoAreas(), post.getCreator(), post.getText());
    }

    private void flagIfInappropriate(BaseEntity entity, AppVariant appVariant, Tenant tenant, Set<GeoArea> geoAreas, Person creator, String text) {

        GeoArea geoArea;
        if (!CollectionUtils.isEmpty(geoAreas)) {
            geoArea = geoAreas.iterator().next();
        } else {
            geoArea = null;
        }
        ContentClassificationFeature contentClassificationFeature = featureService.getFeatureOrNull(
                ContentClassificationFeature.class,
                FeatureTarget.of(geoArea, tenant, appVariant));

        if (contentClassificationFeature == null || !contentClassificationFeature.isEnabled()) {
            return;
        }

        Double classificationValue = grapevineTextClassificationService.classifyText(text);

        double thresholdValue = contentClassificationFeature.getThresholdValue();
        boolean isInappropriate = (classificationValue != null) && (classificationValue > thresholdValue);

        if (isInappropriate) {
            userGeneratedContentFlagService.createOrUpdateStatus(entity, creator, tenant, null,
                    "⚠ Autogeneriert ⚠ Wert: " + classificationValue + " (>" + thresholdValue + ")");
        }
    }

}
