/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Adeline Silva Schäfer, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.business.grapevine.init;

import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.init.BaseAppDataInitializer;
import de.fhg.iese.dd.platform.business.shared.init.DataInitKey;
import de.fhg.iese.dd.platform.datamanagement.framework.LogSummary;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.DataInitializationException;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSourceOrganizationMapping;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceOrganizationMappingRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.NewsSourceRepository;
import de.fhg.iese.dd.platform.datamanagement.grapevine.repos.OrganizationRepository;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class NewsSourceDataInitializer extends BaseAppDataInitializer {

    public static final String TOPIC = "news";
    private static final Pattern URL_PROTOCOL_PATTERN = Pattern.compile("http(s)?://");

    @Autowired
    private IAppService appService;
    @Autowired
    private NewsSourceRepository newsSourceRepository;
    @Autowired
    private NewsSourceOrganizationMappingRepository newsSourceOrganizationMappingRepository;
    @Autowired
    private OrganizationRepository organizationRepository;

    @Override
    public DataInitializerConfiguration getConfiguration() {

        return DataInitializerConfiguration.builder()
                .topic(TOPIC)
                .requiredEntity(App.class)
                .requiredEntity(Tenant.class)
                .requiredEntity(OauthClient.class)
                .requiredEntity(Organization.class)
                .processedEntity(AppVariant.class)
                .processedEntity(NewsSource.class)
                .build();
    }

    @Override
    public void createInitialDataSimple(LogSummary logSummary) {

        List<String> apiManagedNewsSourceIds = newsSourceRepository.getIdsByApiManagedTrue();
        logSummary.info("Found {} API-managed NewsSources.", apiManagedNewsSourceIds.size());

        DuplicateIdChecker<NewsSource> newsSourceDuplicateIdChecker = new DuplicateIdChecker<>();

        List<NewsSource> createdNewsSources = new ArrayList<>();
        List<AppVariant> createdAppVariants = new ArrayList<>();
        List<AppVariant> deletedAppVariants = new ArrayList<>();
        Map<NewsSource, Set<Organization>> newsSourceToOrganizations = new HashMap<>();

        Map<DataInitKey, List<NewsSourceConfiguration>> keyToNewsSources = loadAllDataInitKeysAndEntities("NewsSource", NewsSourceConfiguration.class);

        for (Map.Entry<DataInitKey, List<NewsSourceConfiguration>> entry : keyToNewsSources.entrySet()) {
            DataInitKey dataInitKey = entry.getKey();
            List<NewsSourceConfiguration> newsSourceConfigurations = entry.getValue();
            App app = dataInitKey.getApp();
            if (app == null) {
                throw new DataInitializationException("Missing app for news source at {}", dataInitKey.getFileNames());
            }

            for (NewsSourceConfiguration newsSourceConfiguration : newsSourceConfigurations) {
                checkId(newsSourceConfiguration);
                newsSourceDuplicateIdChecker.checkId(newsSourceConfiguration);
                if (apiManagedNewsSourceIds.contains(newsSourceConfiguration.getId())) {
                    logSummary.warn("NewsSource with id '{}' is managed by the API and cannot be changed by " +
                            "data init, skipping it. Adjust data init to not include it.", newsSourceConfiguration.getId());
                    continue;
                }
                adjustCreated(newsSourceConfiguration);
                createImage(NewsSource::getSiteLogo, NewsSource::setSiteLogo, newsSourceConfiguration, FileOwnership.of(app),
                        logSummary, true);
                if (StringUtils.endsWith(newsSourceConfiguration.getSiteUrl(), "/")) {
                    String newSiteUrl = StringUtils.removeEnd(newsSourceConfiguration.getSiteUrl(), "/");
                    logSummary.warn("NewsSource site url '{}' for '{}' ends with '/', truncating it to '{}'. " +
                                    "Adjust data init accordingly!",
                            newsSourceConfiguration.getSiteUrl(), newsSourceConfiguration.getSiteName(), newSiteUrl);
                    newsSourceConfiguration.setSiteUrl(newSiteUrl);
                }
                List<String> organizationIds = newsSourceConfiguration.getOrganizationIds();
                NewsSource newsSource = new NewsSource();
                BeanUtils.copyProperties(newsSourceConfiguration, newsSource);

                AppVariant appVariant = createOrUpdateAppVariant(newsSource, app, logSummary);
                newsSource.setAppVariant(appVariant);
                createdAppVariants.add(appVariant);
                if (newsSource.isDeleted()) {
                    deletedAppVariants.add(appVariant);
                }

                int numOrganizations = 0;
                if (!CollectionUtils.isEmpty(organizationIds)) {
                    Set<Organization> organizations = organizationIds.stream()
                            .map(organizationId -> organizationRepository.findByIdAndDeletedFalse(organizationId)
                                    .orElseThrow(() ->
                                            new DataInitializationException(
                                                    "Could not find organization '{}' referenced by news source '{}' at {}",
                                                    organizationId, newsSourceConfiguration.getId(),
                                                    dataInitKey.getFileNames())))
                            .collect(Collectors.toSet());
                    newsSourceToOrganizations.put(newsSource, organizations);
                    numOrganizations = organizations.size();
                }
                createdNewsSources.add(newsSource);
                logSummary.info("created NewsSource '{}' with {} organization mappings", newsSource.getSiteName(), numOrganizations);
            }
        }

        mapOauthClientsToAppVariants(createdAppVariants, logSummary);

        // we store them all at once to for faster data init
        appVariantRepository.saveAll(createdAppVariants);
        appVariantRepository.flush();
        checkDuplicateApiKeys(createdAppVariants, logSummary);

        Pair<Set<AppVariantTenantContract>, Set<AppVariantGeoAreaMapping>> contractsAndMappingsNew =
                super.mapGeoAreasToAppVariants(logSummary);

        //the contracts need to be stored first, since they are referenced by the mappings
        logSummary.info("stored {} app variant tenant contracts", contractsAndMappingsNew.getFirst().size());
        appVariantTenantContractRepository.saveAll(contractsAndMappingsNew.getFirst());
        appVariantTenantContractRepository.flush();

        storeAppVariantGeoAreaMappings(contractsAndMappingsNew.getFirst(), contractsAndMappingsNew.getSecond(),
                logSummary);
        newsSourceRepository.saveAll(createdNewsSources);
        newsSourceRepository.flush();

        deletedAppVariants.forEach(deletedAppVariant -> appService.deleteAppVariant(deletedAppVariant));

        //storing the mapping of news sources to organizations
        Set<NewsSourceOrganizationMapping> organizationMappings = newsSourceToOrganizations.entrySet().stream()
                .flatMap(entry -> entry.getValue().stream()
                        .map(organization -> NewsSourceOrganizationMapping.builder()
                                .newsSource(entry.getKey())
                                .organization(organization)
                                .build()
                                .withConstantId()))
                .collect(Collectors.toSet());
        newsSourceOrganizationMappingRepository.saveAll(organizationMappings);
        int deletedOldMappings = newsSourceOrganizationMappingRepository.deleteByNotIn(
                NewsSourceOrganizationMapping.toInSafeEntitySet(organizationMappings));
        newsSourceOrganizationMappingRepository.flush();
        logSummary.info("stored {} news source to organization mappings and deleted {} old ones",
                organizationMappings.size(), deletedOldMappings);
    }

    private AppVariant createOrUpdateAppVariant(NewsSource newsSource, App app, LogSummary logSummary) {

        AppVariant jsonAppVariant = newsSource.getAppVariant();
        if (jsonAppVariant == null) {
            throw new DataInitializationException("No app variant defined for {}", newsSource.toString());
        }
        if (StringUtils.isNotEmpty(jsonAppVariant.getAppVariantIdentifier())) {
            AppVariant referencedAppVariant = appVariantRepository
                    .findByAppVariantIdentifier(jsonAppVariant.getAppVariantIdentifier())
                    .orElseThrow(() -> new DataInitializationException(
                            "AppVariant '{}' referenced in '{}' could not be found",
                            jsonAppVariant.getAppVariantIdentifier(), newsSource.toString()));
            logSummary.info("using referenced app variant {} for {}", referencedAppVariant, newsSource);
            return referencedAppVariant;
        }

        String siteUrl = StringUtils.removeEnd(newsSource.getSiteUrl(), "/");
        String siteUrlWithoutProtocol = RegExUtils.removeFirst(siteUrl, URL_PROTOCOL_PATTERN);
        String adjustedAppIdentifier = app.getAppIdentifier().replace("-", ".");
        String appVariantIdentifier = "de.fhg.iese." + adjustedAppIdentifier + "." + siteUrlWithoutProtocol;

        jsonAppVariant.setAppVariantIdentifier(appVariantIdentifier);
        jsonAppVariant.setName(app.getName() + " " + siteUrlWithoutProtocol);
        jsonAppVariant.setApp(app);
        String jsonAppVariantCallBackUrl = StringUtils.removeEnd(jsonAppVariant.getCallBackUrl(), "/");
        if (StringUtils.isNotBlank(jsonAppVariantCallBackUrl)) {
            //a specific callback url is defined in the json app variant, so it differs from the site url
            if (StringUtils.startsWith(jsonAppVariantCallBackUrl, "/")) {
                //if the callback url starts with a / it is a relative one, so we append it to the existing site url
                jsonAppVariant.setCallBackUrl(siteUrl + jsonAppVariantCallBackUrl);
            } else {
                //the callback url is an absolute one, so we take it completely
                jsonAppVariant.setCallBackUrl(jsonAppVariantCallBackUrl);
            }
        } else {
            //no specific callback url was defined, we take the site url
            jsonAppVariant.setCallBackUrl(siteUrl);
        }
        //we re-use the existing app variant of this news source, if it is available, to avoid generating duplicates
        Optional<AppVariant> existingAppVariant =
                newsSourceRepository.findById(newsSource.getId()).map(NewsSource::getAppVariant);
        if (existingAppVariant.isPresent()) {
            //if there is an existing app variant for this news source, re-use it!
            jsonAppVariant.setId(existingAppVariant.get().getId());
        } else {
            //if there is no existing app variant, base the id of the app variant on the news source id
            jsonAppVariant.withConstantId(newsSource);
        }

        return super.setExistingAppVariantValues(jsonAppVariant, logSummary);
    }

    private void mapOauthClientsToAppVariants(List<AppVariant> appVariants, LogSummary logSummary)
            throws DataInitializationException {

        Map<String, OauthClient> oauthClientByIdentifier = oauthClientRepository.findAll().stream()
                .collect(Collectors.toMap(OauthClient::getOauthClientIdentifier, Function.identity()));

        for (AppVariant appVariant : appVariants) {

            Set<OauthClient> jsonOauthClients = appVariant.getOauthClients();
            // Currently not helpful to warn if there are no oauth clients, since the DorfPages do not use oauth
            if (CollectionUtils.isEmpty(jsonOauthClients)) {
                continue;
            }

            Set<OauthClient> existingOauthClients = new HashSet<>();
            for (final OauthClient jsonOauthClient : jsonOauthClients) {
                OauthClient existingOauthClient = oauthClientByIdentifier.get(jsonOauthClient.getOauthClientIdentifier());
                if (existingOauthClient == null) {
                    throw new DataInitializationException("OauthClient '{}' could not be found",
                            jsonOauthClient.getOauthClientIdentifier());
                }
                existingOauthClients.add(existingOauthClient);
            }
            appVariant.setOauthClients(existingOauthClients);
            logSummary.info("added {} oauth clients to app variant '{}':\n{}",
                    existingOauthClients.size(),
                    appVariant.getAppVariantIdentifier(),
                    existingOauthClients.stream()
                            .map(OauthClient::toString)
                            .collect(Collectors.joining("\n")));
        }
    }

}
