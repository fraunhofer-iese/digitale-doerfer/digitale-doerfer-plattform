<#ftl output_format="HTML">
<#-- @ftlvariable name="" type="de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.GrapevineStatsReport" -->
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Statistiken ${startDate} - ${endDate}</title>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            margin-right: 0;
        }

        h1 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }

        h2 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }

        h3 {
            color: black;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        h4 {
            color: black;
            margin-top: 10px;
            margin-bottom: 0;
        }

        p {
            color: #222;
            margin-top: 0;
            margin-bottom: 5px;
            display: inline;
            font-weight: normal
        }

        a {
            color: #dd3c5d;
        }

        .level-top {
            margin-left: 0;
        }

        .level-sub-1 {
            margin-left: 8px;
        }

        .level-sub-2 {
            margin-left: 16px;
        }

        .level-sub-3 {
            margin-left: 32px;
        }

        .level-sub-4 {
            margin-left: 48px;
        }

        .red-alert {
            position: relative;
        }

        .red-alert::before {
            /* Highlight color */
            background-color: rgba(229, 197, 11, 0.8);

            content: "";
            position: absolute;
            width: calc(100% + 4px);
            height: 60%;
            left: -2px;
            bottom: 0;
            z-index: -1;
            transform: rotate(-2deg);
        }

        .toc-table {
            width: 50%;
            border-collapse: collapse;
        }

        .toc-table td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        .toc-table tr:nth-child(odd) {
            background-color: #f0f0f0;
        }

        .toc-table tr:hover {
            background-color: #e6e6e6;
        }

        .toc-table th {
            background-color: #dddddd;
        }

        .data-table {
            width: 50%;
            border-collapse: collapse;
            table-layout: fixed
        }

        .data-table td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        .data-table tr:nth-child(odd) {
            background-color: #f0f0f0;
        }

        .data-table th {
            background-color: #e6e6e6;
        }

        .topButton {
            display: block;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: #dd3c5d;
            color: white;
            cursor: pointer;
            padding: 13px;
            border-radius: 4px;
        }

        .topButton:hover {
            background-color: #555;
        }
    </style>
</head>
<body>

<button class="topButton" onclick="window.location.href='#pageBeginning'">Seitenanfang</button>
<h1 class="red-alert">Diese Statistiken sind ungenau und nicht mehr empfohlen zu verwenden!
    Bitte die CSV-Statistiken verwenden, die auch in der AdminUI heruntergeladen werden können.</h1>
<h1 class="level-top" id="pageBeginning">Statistiken für DorfFunk:</h1>
<h2 class="level-sub-1">Statistiken für den Zeitraum: </h2>
<h3 class="level-sub-2"><p>${startDate} - ${endDate}</p></h3>
<#if nowDate?has_content>
    <h4 class="level-sub-2">
        <p id="activeExplanation"> ⚠ Zeitraum für aktive Nutzer weicht ab:<br/>
            ${startDate} - ${nowDate}<br/>
            Dies ist technisch bedingt, da nur der letzte Login-Zeitpunkt eines Nutzers bekannt ist.</p>
    </h4>
</#if>
<h3 class="level-sub-2">Anzahl neu angelegter Nutzer: <p>${grapevineStats.userStats.createdUsers}</p></h3>
<h3 class="level-sub-2">Anzahl gelöschter Nutzer: <p>${grapevineStats.userStats.deletedUsers}</p></h3>
<h3 class="level-sub-2">Anzahl aktiver (letzter Login im Zeitraum, inklusive gelöschte) Nutzer<#if nowDate?has_content>
    <a href="#activeExplanation">(Zeitraum ⚠)</a></#if>: <p>${grapevineStats.userStats.activeUsersSinceReportStart}</p>
</h3>

<h2 class="level-sub-1">Gruppen: </h2>
<h3 class="level-sub-2">Anzahl neuer Plausch in Gruppen: <p>${grapevineStats.groupStats.gossipInGroupCount}</p></h3>

<h3 class="level-sub-2">Anzahl neue öffentliche Gruppen: <p>${grapevineStats.groupStats.publicGroupCount}</p></h3>
<h3 class="level-sub-2">Anzahl neue Mitglieder in öffentlichen Gruppen:
    <p>${grapevineStats.groupStats.publicGroupMemberCount}</p></h3>

<h3 class="level-sub-2">Anzahl neue private Gruppen: <p>${grapevineStats.groupStats.privateGroupCount}</p></h3>
<h3 class="level-sub-2">Anzahl neue Mitglieder in privaten Gruppen:
    <p>${grapevineStats.groupStats.privateGroupMemberCount}</p></h3>

<h2 class="level-sub-1">Aktuelle Mandanten-Gruppen:</h2>
<div>
    Mandanten-Gruppen sind technische Zusammenfassungen von Mandanten, die initial gleich konfiguriert wurden.
    Eine genaue Abbildung auf Bundesländer ist hier nicht gegeben und kann auch nicht hergestellt werden.
    Die Gruppe "-" enthält alle Mandanten, die keiner Gruppe angehören.
</div>
<table class="toc-table level-sub-2">
    <tr>
        <th>Mandanten-Gruppe</th>
        <th style="width: 130px">Neue Nutzer</th>
        <th style="width: 130px">Aktive Nutzer
            <#if nowDate?has_content><a href="#activeExplanation">(Zeitraum ⚠)</a></#if></th>
        <th style="width: 130px">Gesamte Posts</th>
    </tr>
    <#list grapevineStats.tenantGroupStats as tenantGroupStats>
        <tr>
            <td><a class="content-link"
                   href="#${tenantGroupStats.tenantGroupName!"-"}">${tenantGroupStats.tenantGroupName!"-"}</a></td>
            <td>${tenantGroupStats.userStats.createdUsers}</td>
            <td>${tenantGroupStats.userStats.activeUsersSinceReportStart}</td>
            <td>${tenantGroupStats.postStats.postCount}</td>
        </tr>
    </#list>
</table>

<h2 class="level-sub-1">Aktuelle Mandanten:</h2>
<table class="toc-table level-sub-2">
    <tr>
        <th>Mandant</th>
        <th style="width: 130px">Neue Nutzer</th>
        <th style="width: 130px">Aktive Nutzer
            <#if nowDate?has_content><a href="#activeExplanation">(Zeitraum ⚠)</a></#if></th>
        <th style="width: 130px">Gesamte Posts</th>
    </tr>
    <#list grapevineStats.tenantStats as tenantStats>
        <tr>
            <td><a class="content-link" href="#${tenantStats.tenantName}">${tenantStats.tenantName}</a></td>
            <td>${tenantStats.userStats.createdUsers}</td>
            <td>${tenantStats.userStats.activeUsersSinceReportStart}</td>
            <td>${tenantStats.postStats.postCount}</td>
        </tr>
    </#list>
</table>

<h2 class="level-sub-1">Statistiken pro Mandanten-Gruppe:</h2>
<div>
    Mandanten-Gruppen sind technische Zusammenfassungen von Mandanten, die initial gleich konfiguriert wurden.
    Eine genaue Abbildung auf Bundesländer ist hier nicht gegeben und kann auch nicht hergestellt werden.
    Die Gruppe "-" enthält alle Mandanten, die keiner Gruppe angehören.
</div>
<#list grapevineStats.tenantGroupStats as tenantGroupStats>
    <div id="${tenantGroupStats.tenantGroupName!"-"}">
        <h3 class="level-sub-2">Mandanten-Gruppe: <p>${tenantGroupStats.tenantGroupName!"-"}</p></h3>
        <h4 class="level-sub-3">Anzahl neu angelegter Nutzer: <p>${tenantGroupStats.userStats.createdUsers}</p></h4>
        <h4 class="level-sub-3">Anzahl gelöschter Nutzer: <p>${tenantGroupStats.userStats.deletedUsers}</p></h4>
        <h4 class="level-sub-3">Anzahl aktiver Nutzer (letzter Login im Zeitraum, inklusive
            gelöschte)<#if nowDate?has_content><a href="#activeExplanation"> (Zeitraum ⚠)</a></#if>:
            <p>${tenantGroupStats.userStats.activeUsersSinceReportStart}</p></h4>
        <h4 class="level-sub-3">Anzahl Posts:</h4>
        <table class="data-table level-sub-4">
            <tr>
                <th>Plausch</th>
                <th>Suche</th>
                <th>Biete</th>
                <th>Sag's uns</th>
                <th>News</th>
                <th>Events</th>
                <th>←∑ Posts</th>
                <th>Kommentare</th>
            </tr>
            <tr>
                <td>${tenantGroupStats.postStats.gossipCount}</td>
                <td>${tenantGroupStats.postStats.seekingCount}</td>
                <td>${tenantGroupStats.postStats.offerCount}</td>
                <td>${tenantGroupStats.postStats.suggestionCount}</td>
                <td>${tenantGroupStats.postStats.newsItemCount}</td>
                <td>${tenantGroupStats.postStats.happeningCount}</td>
                <td>${tenantGroupStats.postStats.postCount}</td>
                <td>${tenantGroupStats.postStats.commentCount}</td>
            </tr>
        </table>

        <h4 class="level-sub-3">Gruppen: </h4>
        <h4 class="level-sub-4">Anzahl neuer Plausch in Gruppen:
            <p>${tenantGroupStats.groupStats.gossipInGroupCount}</p>
        </h4>
        <h4 class="level-sub-4">Anzahl neue öffentliche Gruppen: <p>${tenantGroupStats.groupStats.publicGroupCount}</p>
        </h4>
        <h4 class="level-sub-4">Anzahl neue Mitglieder in öffentlichen Gruppen:
            <p>${tenantGroupStats.groupStats.publicGroupMemberCount}</p></h4>
        <h4 class="level-sub-4">Anzahl neue private Gruppen: <p>${tenantGroupStats.groupStats.privateGroupCount}</p>
        </h4>
        <h4 class="level-sub-4">Anzahl neue Mitglieder in privaten Gruppen:
            <p>${tenantGroupStats.groupStats.privateGroupMemberCount}</p></h4>

        <h4 class="level-sub-3">Aktivität:</h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die etwas gepostet haben:
            <p>${tenantGroupStats.activityStats.usersPosted}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die etwas kommentiert haben:
            <p>${tenantGroupStats.activityStats.usersCommented}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die entweder etwas gepostet oder kommentiert haben:
            <p>${tenantGroupStats.activityStats.usersPostedOrCommented}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die mehr als zehn mal gepostet haben:
            <p>${tenantGroupStats.activityStats.activeUsers}</p></h4>
        <h4 class="level-sub-4">Tatsächliche Anzahl der Nutzer, welche 80% der Posts erstellt haben:
            <p>${tenantGroupStats.activityStats.usersWith80PercentPosts}</p></h4>
        <h4 class="level-sub-4">Anteil der Postersteller, die mehr als 80% der Posts erstellt haben:
            <p>${tenantGroupStats.activityStats.percentUsersWith80PercentPosts}%</p></h4>
        <br>
    </div>
</#list>

<h2 class="level-sub-1">Statistiken pro Mandant:</h2>
<#list grapevineStats.tenantStats as tenantStats>
    <div id="${tenantStats.tenantName}">
        <h3 class="level-sub-2">Mandant: <p>${tenantStats.tenantName}</p></h3>
        <h4 class="level-sub-3">Anzahl neu angelegter Nutzer: <p>${tenantStats.userStats.createdUsers}</p></h4>
        <h4 class="level-sub-3">Anzahl gelöschter Nutzer: <p>${tenantStats.userStats.deletedUsers}</p></h4>
        <h4 class="level-sub-3">Anzahl aktiver Nutzer (letzter Login im Zeitraum, inklusive
            gelöschte)<#if nowDate?has_content><a href="#activeExplanation"> (Zeitraum ⚠)</a></#if>:
            <p>${tenantStats.userStats.activeUsersSinceReportStart}</p></h4>
        <h4 class="level-sub-3">Anzahl Posts:</h4>
        <table class="data-table level-sub-4">
            <tr>
                <th>Plausch</th>
                <th>Suche</th>
                <th>Biete</th>
                <th>Sag's uns</th>
                <th>News</th>
                <th>Events</th>
                <th>←∑ Posts</th>
                <th>Kommentare</th>
            </tr>
            <tr>
                <td>${tenantStats.postStats.gossipCount}</td>
                <td>${tenantStats.postStats.seekingCount}</td>
                <td>${tenantStats.postStats.offerCount}</td>
                <td>${tenantStats.postStats.suggestionCount}</td>
                <td>${tenantStats.postStats.newsItemCount}</td>
                <td>${tenantStats.postStats.happeningCount}</td>
                <td>${tenantStats.postStats.postCount}</td>
                <td>${tenantStats.postStats.commentCount}</td>
            </tr>
        </table>

        <h4 class="level-sub-3">Gruppen: </h4>
        <h4 class="level-sub-4">Anzahl neuer Plausch in Gruppen: <p>${tenantStats.groupStats.gossipInGroupCount}</p>
        </h4>
        <h4 class="level-sub-4">Anzahl neue öffentliche Gruppen: <p>${tenantStats.groupStats.publicGroupCount}</p></h4>
        <h4 class="level-sub-4">Anzahl neue Mitglieder in öffentlichen Gruppen:
            <p>${tenantStats.groupStats.publicGroupMemberCount}</p></h4>
        <h4 class="level-sub-4">Anzahl neue private Gruppen: <p>${tenantStats.groupStats.privateGroupCount}</p></h4>
        <h4 class="level-sub-4">Anzahl neue Mitglieder in privaten Gruppen:
            <p>${tenantStats.groupStats.privateGroupMemberCount}</p></h4>

        <h4 class="level-sub-3">Aktivität:</h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die etwas gepostet haben:
            <p>${tenantStats.activityStats.usersPosted}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die etwas kommentiert haben:
            <p>${tenantStats.activityStats.usersCommented}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die entweder etwas gepostet oder kommentiert haben:
            <p>${tenantStats.activityStats.usersPostedOrCommented}</p></h4>
        <h4 class="level-sub-4">Anzahl der Nutzer, die mehr als zehn mal gepostet haben:
            <p>${tenantStats.activityStats.activeUsers}</p></h4>
        <h4 class="level-sub-4">Tatsächliche Anzahl der Nutzer, welche 80% der Posts erstellt haben:
            <p>${tenantStats.activityStats.usersWith80PercentPosts}</p></h4>
        <h4 class="level-sub-4">Anteil der Postersteller, die mehr als 80% der Posts erstellt haben:
            <p>${tenantStats.activityStats.percentUsersWith80PercentPosts}%</p></h4>
        <br>
    </div>
</#list>
</body>
</html>
