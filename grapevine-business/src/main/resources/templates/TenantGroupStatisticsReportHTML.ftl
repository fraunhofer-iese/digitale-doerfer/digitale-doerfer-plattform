<#ftl output_format="HTML">
<#-- @ftlvariable name="" type="de.fhg.iese.dd.platform.business.grapevine.statistics.legacy.TenantStatsReport" -->
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Tenant Stats</title>
    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            margin-right: 0px;
        }

        h1 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }

        h2 {
            color: #dd3c5d;
            margin-top: 20px;
            margin-bottom: 5px;
        }

        h3 {
            color: black;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        h4 {
            color: black;
            margin-top: 10px;
            margin-bottom: 0px;
        }

        p {
            color: #222;
            margin-top: 0;
            margin-bottom: 5px;
            display: inline;
            font-weight: normal
        }

        a {
            color: #dd3c5d;
        }

        .level-top {
            margin-left: 0px;
        }

        .level-sub-1 {
            margin-left: 8px;
        }

        .level-sub-2 {
            margin-left: 16px;
        }

        .level-sub-3 {
            margin-left: 32px;
        }

        .level-sub-4 {
            margin-left: 48px;
        }

        .red-alert {
            position: relative;
        }

        .red-alert::before {
            /* Highlight color */
            background-color: rgba(229, 197, 11, 0.8);

            content: "";
            position: absolute;
            width: calc(100% + 4px);
            height: 60%;
            left: -2px;
            bottom: 0;
            z-index: -1;
            transform: rotate(-2deg);
        }

    </style>
</head>
<body>

<h1 class="red-alert">Diese Statistiken sind ungenau und nicht mehr empfohlen zu verwenden!
    Bitte die CSV-Statistiken verwenden, die auch in der AdminUI heruntergeladen werden können.</h1>
<h1 class="level-top">Mandanten-Gruppe: <p>${tenantGroupStats.tenantGroupName!"-"}</p></h1>
<h2 class="level-sub-1">Statistiken für den Zeitraum: </h2>
<h3 class="level-sub-2"><p>${startDate} - ${endDate} </p></h3>
<h3 class="level-sub-2">Anzahl neu angelegter Nutzer: <p>${tenantGroupStats.userStats.createdUsers}</p></h3>
<h3 class="level-sub-2">Anzahl gelöschter Nutzer: <p>${tenantGroupStats.userStats.deletedUsers}</p></h3>

<h3 class="level-sub-2">Anzahl Posts:</h3>
<h4 class="level-sub-4">Plausch: <p>${tenantGroupStats.postStats.gossipCount}</p></h4>
<h4 class="level-sub-4">News: <p>${tenantGroupStats.postStats.newsItemCount}</p></h4>
<h4 class="level-sub-4">Suche: <p>${tenantGroupStats.postStats.seekingCount}</p></h4>
<h4 class="level-sub-4">Biete: <p>${tenantGroupStats.postStats.offerCount}</p></h4>
<h4 class="level-sub-4">Sag's uns-Einträge: <p>${tenantGroupStats.postStats.suggestionCount}</p></h4>
<h4 class="level-sub-4">Events: <p>${tenantGroupStats.postStats.happeningCount}</p></h4>

<h4 class="level-sub-4">Kommentare: <p>${tenantGroupStats.postStats.commentCount}</p></h4>
<h4 class="level-sub-4">Gesamte Posts: <p>${tenantGroupStats.postStats.postCount}</p></h4>

<h3 class="level-sub-2">Gruppen:</h3>
<h4 class="level-sub-4">Anzahl neuer Plausch in Gruppen: <p>${tenantGroupStats.groupStats.gossipInGroupCount}</p></h4>
<h4 class="level-sub-4">Anzahl neue öffentliche Gruppen: <p>${tenantGroupStats.groupStats.publicGroupCount}</p></h4>
<h4 class="level-sub-4">Anzahl neue Mitglieder in öffentlichen Gruppen:
    <p>${tenantGroupStats.groupStats.publicGroupMemberCount}</p></h4>
<h4 class="level-sub-4">Anzahl neue private Gruppen: <p>${tenantGroupStats.groupStats.privateGroupCount}</p></h4>
<h4 class="level-sub-4">Anzahl neue Mitglieder in privaten Gruppen:
    <p>${tenantGroupStats.groupStats.privateGroupMemberCount}</p></h4>

<h3 class="level-sub-2">Aktivität:</h3>
<h4 class="level-sub-4">Anzahl der Nutzer, die etwas gepostet haben:
    <p>${tenantGroupStats.activityStats.usersPosted}</p></h4>
<h4 class="level-sub-4">Anzahl der Nutzer, die etwas kommentiert haben:
    <p>${tenantGroupStats.activityStats.usersCommented}</p></h4>
<h4 class="level-sub-4">Anzahl der Nutzer, die entweder etwas gepostet oder kommentiert haben:
    <p>${tenantGroupStats.activityStats.usersPostedOrCommented}</p></h4>
<h4 class="level-sub-4">Anzahl der Nutzer, die mehr als zehn mal gepostet haben:
    <p>${tenantGroupStats.activityStats.activeUsers}</p></h4>
<h4 class="level-sub-4">Tatsächliche Anzahl der Nutzer, welche 80% der Posts erstellt haben:
    <p>${tenantGroupStats.activityStats.usersWith80PercentPosts}</p></h4>
<h4 class="level-sub-4">Anteil der Postersteller, die mehr als 80% der Posts erstellt haben:
    <p>${tenantGroupStats.activityStats.percentUsersWith80PercentPosts}%</p></h4>
<br>
</body>
</html>
