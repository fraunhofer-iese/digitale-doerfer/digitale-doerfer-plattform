<#include "/mailHeaderShared.ftl">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;">

                        <h1 class="null" style="text-align: center;">
                            <span style="font-family:lato,helvetica neue,helvetica,arial,sans-serif">
                                <span style="color:#696969">
                                    <span style="font-size:12px">Hinweis aus der LösBar</span>
                                </span>
                                <br>
                                <strong>
                                    <span style="color:#DE3B5D">
                                        Neuer Kommentar
                                    </span>
                                </strong>
                            </span>
                        </h1>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
    <tbody class="mcnDividerBlockOuter">
    <tr>
        <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 27px;">
            <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="min-width: 100%;border-top-width: 2px;border-top-style: solid;border-top-color: #EAEAEA;">
                <tbody>
                <tr>
                    <td>
                        <span></span>
                    </td>
                </tr>
                </tbody>
            </table>

        </td>
    </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
    <tr>
        <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
            <!--[if mso]>
            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
            <![endif]-->

            <!--[if mso]>
            <td valign="top" width="600" style="width:600px;">
            <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;"
                   width="100%" class="mcnTextContentContainer">
                <tbody>
                <tr>

                    <td valign="top" class="mcnTextContent"
                        style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                        <h2 class="null" style="margin-bottom: 25px; text-align: left;">Hallo ${receiverName}!</h2>
                        <p style="text-align: left; margin-bottom: 0px">
                            ${commentCreator} hat am <strong>${commentCreatedTime}</strong>
                            folgenden Kommentar zu einem deiner Fälle in der Kategorie <strong>${suggestionCategory}</strong> erstellt:
                        </p>
                        <table align="center" border="0" cellpadding="10%" cellspacing="0"
                               style="max-width:75%; min-width:75%; margin: 10px 0 0 0;" width="100%" class="mcnTextContentContainer">
                            <tr>
                                <td style="vertical-align: top; font-size: 60px; padding-top: 24px; color: #66BFAC; padding: 25px 0; width: 5%;">“</td>
                                <td style="font-style:italic; vertical-align: text-top; padding: 12px 10px 10px 5px; color: #696969;">${commentText}</td>
                            </tr>
                        </table>
                        <p style="text-align: left; margin-bottom: 0px;">
                            Melde dich bei der Lösbar an, um darauf zu antworten!
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if mso]>
            </td>
            <![endif]-->

            <!--[if mso]>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<#if suggestionLink??>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock"
           style="margin-top: 15px; min-width:100%;">
        <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" valign="top"
                align="center" class="mcnButtonBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer"
                       style="border-collapse: separate !important;border-top-left-radius: 3px;border-top-right-radius: 3px;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;background-color: #DE3B5D;">
                    <tbody>
                    <tr>
                        <td align="center" valign="middle" class="mcnButtonContent"
                            style="font-family: Lato, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; padding: 15px;">
                            <a class="mcnButton " title="Zur Meldung" href="${suggestionLink}" target="_blank"
                               style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Jetzt
                                Fall anschauen</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</#if>
<#include "/mailFooterShared.ftl">
