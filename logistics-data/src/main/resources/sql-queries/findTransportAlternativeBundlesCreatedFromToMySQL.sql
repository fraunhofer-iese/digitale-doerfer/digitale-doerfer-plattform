select alternative_bundle.created           as created,
       participants_shop.name               as shopName,
       concat(pickup_address.name, ', ', pickup_address.street, ', ', pickup_address.zip, ' ',
              pickup_address.city)          as pickupAddress,
       concat(receiver.first_name, ' ', receiver.last_name, ' (', receiver.email,
              ') ')                         as receiver,
       concat(delivery_address.name, ', ', delivery_address.street, ', ', delivery_address.zip, ' ',
              delivery_address.city)        as deliveryAddress,
       delivery.desired_delivery_time_start as desiredDeliveryTimeStart,
       delivery.desired_delivery_time_end   as desiredDeliveryTimeEnd,
       (
           select group_concat(concat(purchase_order_item.amount, ' ', purchase_order_item.unit, ' ',
                                      purchase_order_item.item_name) separator '; ')
           from purchase_order_item
           where purchase_order_item.purchase_order_id = purchase_order.id
           group by purchase_order_item.purchase_order_id
       )                                    as purchaseOrderItems,
       concat(delivery.size_length, ' * ', delivery.size_width, ' * ',
              delivery.size_height)         as size,
       delivery.size_weight                 as weight,
       purchase_order.purchase_order_notes  as orderNotes,
       delivery.transport_notes             as transportNotes,
       delivery.content_notes               as contentNotes,
       purchase_order.created               as ordered,
       (
           select group_concat(
                          concat(status_record.status, ' (', status_record.timestamp, ')')
                          order by status_record.timestamp desc
                          separator '; ')
           from transport_alternative_bundle_status_record as status_record
           where status_record.transport_alternative_bundle_id = alternative_bundle.id
           group by status_record.transport_alternative_bundle_id
       )                                    as statusHistory,
       alternative_bundle.id                as transportAlternativeBundleId,
       delivery.id                          as deliveryId,
       purchase_order.id                    as purchaseOrderId,
       purchase_order.shop_order_id         as shopOrderId,
       purchase_order.sender_id             as senderShopId,
       community.name                       as community,
       receiver.id                          as receiverId

from transport_alternative_bundle as alternative_bundle

         left join delivery
                   on alternative_bundle.delivery_id = delivery.id

         left join community
                   on delivery.community_id = community.id

         left join purchase_order
                   on purchase_order.delivery_id = delivery.id

         left join participants_shop
                   on purchase_order.sender_id = participants_shop.id

         left join parcel_address as pickup_p_address
                   on alternative_bundle.pickup_address_id = pickup_p_address.id

         left join parcel_address as delivery_p_address
                   on delivery.delivery_address_id = delivery_p_address.id

         left join address as pickup_address
                   on pickup_p_address.address_id = pickup_address.id

         left join address as delivery_address
                   on delivery_p_address.address_id = delivery_address.id

         left join logistics_participant as receiverLP
                   on delivery.receiver_id = receiverLP.id

         left join person as receiver
                   on receiverLP.person_id = receiver.id
where community.id = :communityId
  and alternative_bundle.created >= :fromTime
  and :toTime >= alternative_bundle.created
  and true =
      (select (status_record.status = 'WAITING_FOR_ASSIGNMENT')
       from transport_alternative_bundle_status_record as status_record
       where status_record.transport_alternative_bundle_id = alternative_bundle.id
       order by status_record.timestamp desc
       limit 1)
order by alternative_bundle.created desc