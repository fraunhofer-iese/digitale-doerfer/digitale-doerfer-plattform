select purchase_order.created               as ordered,
       (
           select status_record.status
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
           order by status_record.time_stamp desc
           limit 1
       )                                    as currentStatus,
       (
           select status_record.time_stamp
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
           order by status_record.time_stamp desc
           limit 1
       )                                    as lastStatusUpdate,
       participants_shop.name               as shopName,
       concat(pickup_address.name, ', ', pickup_address.street, ', ', pickup_address.zip, ' ',
              pickup_address.city)          as pickupAddress,
       concat(receiver.first_name, ' ', receiver.last_name, ' (', receiver.email,
              ') ')                         as receiver,
       (
           select concat(carrier.first_name, ' ', carrier.last_name, ' (', carrier.email, ') ')
           from transport_assignment,
                person as carrier
           where transport_assignment.delivery_id = delivery.id
             and carrier.id = transport_assignment.carrier_id
             and true =
                 (select (status_record.status != 'CANCELLED')
                  from transport_assignment_status_record as status_record
                  where status_record.transport_assignment_id = transport_assignment.id
                  order by status_record.timestamp desc
                  limit 1)
           limit 1
       )                                    as carrier,
       concat(delivery_address.name, ', ', delivery_address.street, ', ', delivery_address.zip, ' ',
              delivery_address.city)        as deliveryAddress,
       delivery.desired_delivery_time_start as desiredDeliveryTimeStart,
       delivery.desired_delivery_time_end   as desiredDeliveryTimeEnd,
       (
           select group_concat(concat(purchase_order_item.amount, ' ', purchase_order_item.unit, ' ',
                                      purchase_order_item.item_name) separator '; ')
           from purchase_order_item
           where purchase_order_item.purchase_order_id = purchase_order.id
           group by purchase_order_item.purchase_order_id
       )                                    as purchaseOrderItems,
       delivery.tracking_code               as trackingCode,
       concat(delivery.size_length, ' * ', delivery.size_width, ' * ',
              delivery.size_height)         as size,
       delivery.size_weight                 as weight,
       purchase_order.purchase_order_notes  as orderNotes,
       delivery.transport_notes             as transportNotes,
       delivery.content_notes               as contentNotes,
       (
           select status_record.time_stamp
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
             and status_record.status = 'WAITING_FOR_ASSIGNMENT'
           order by status_record.time_stamp asc
           limit 1
       )                                    as packed,
       (
           select status_record.time_stamp
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
             and status_record.status = 'IN_DELIVERY'
           order by status_record.time_stamp asc
           limit 1
       )                                    as pickedUp,
       (
           select status_record.time_stamp
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
             and status_record.status = 'DELIVERED'
           order by status_record.time_stamp asc
           limit 1
       )                                    as delivered,
       (
           select status_record.time_stamp
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
             and status_record.status = 'RECEIVED'
           order by status_record.time_stamp asc
           limit 1
       )                                    as received,
       (
           select group_concat(
                          concat(status_record.status, ' (', status_record.time_stamp, ')')
                          order by status_record.time_stamp desc
                          separator '; ')
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
           group by status_record.delivery_id
       )                                    as statusHistory,
       delivery.id                          as deliveryId,
       purchase_order.id                    as purchaseOrderId,
       purchase_order.shop_order_id         as shopOrderId,
       participants_shop.id                 as senderShopId,
       community.name                       as community,
       receiver.id                          as receiverId

from delivery

         left join community
                   on delivery.community_id = community.id

         left join purchase_order
                   on purchase_order.delivery_id = delivery.id

         left join participants_shop
                   on purchase_order.sender_id = participants_shop.id

         left join parcel_address as pickup_p_address
                   on delivery.pickup_address_id = pickup_p_address.id

         left join parcel_address as delivery_p_address
                   on delivery.delivery_address_id = delivery_p_address.id

         left join address as pickup_address
                   on pickup_p_address.address_id = pickup_address.id

         left join address as delivery_address
                   on delivery_p_address.address_id = delivery_address.id

         left join logistics_participant as receiverLP
                   on delivery.receiver_id = receiverLP.id

         left join person as receiver
                   on receiverLP.person_id = receiver.id
where community.id = :communityId
  and delivery.created >= :fromTime
  and :toTime >= delivery.created
order by delivery.created desc