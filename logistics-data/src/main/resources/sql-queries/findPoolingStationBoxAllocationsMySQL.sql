select pooling_station.name                        as poolingStationName,
       pooling_station_box.name                    as poolingStationBoxName,
       pooling_station_box_allocation.status       as currentStatus,
       pooling_station_box_allocation.timestamp    as lastStatusUpdate,
       pooling_station_box_allocation.in_time      as inTime,
       pooling_station_box_allocation.out_time     as outTime,
       pooling_station_box_allocation.is_door_open as isDoorOpen,
       participants_shop.name                      as shopName,
       concat(pickup_address.name, ', ', pickup_address.street, ', ', pickup_address.zip, ' ',
              pickup_address.city)                 as pickupAddress,
       concat(receiver.first_name, ' ', receiver.last_name, ' (', receiver.email,
              ') ')                                as receiver,
       concat(carrier_in.first_name, ' ', carrier_in.last_name, ' (', carrier_in.email,
              ') ')                                as inCarrier,
       concat(carrier_out.first_name, ' ', carrier_out.last_name, ' (', carrier_out.email,
              ') ')                                as outCarrier,
       concat(delivery_address.name, ', ', delivery_address.street, ', ', delivery_address.zip, ' ',
              delivery_address.city)               as deliveryAddress,
       (
           select status_record.status
           from delivery_status_record as status_record
           where status_record.delivery_id = delivery.id
           order by status_record.time_stamp desc
           limit 1
       )                                           as deliveryStatus,
       delivery.id                                 as deliveryId,
       pooling_station_box.id                      as poolingStationBoxId,
       community.name                              as community,
       pooling_station_box_allocation.id           as allocationId,
       assignment_in.id                            as transportAssignmentInId,
       assignment_out.id                           as transportAssignmentOutId

from pooling_station_box_allocation

         left join pooling_station_box
                   on pooling_station_box_allocation.pooling_station_box_id = pooling_station_box.id

         left join pooling_station
                   on pooling_station_box.pooling_station_id = pooling_station.id

         left join delivery
                   on pooling_station_box_allocation.delivery_id = delivery.id

         left join purchase_order
                   on purchase_order.delivery_id = delivery.id

         left join participants_shop
                   on purchase_order.sender_id = participants_shop.id

         left join parcel_address as pickup_p_address
                   on delivery.pickup_address_id = pickup_p_address.id

         left join parcel_address as delivery_p_address
                   on delivery.delivery_address_id = delivery_p_address.id

         left join address as pickup_address
                   on pickup_p_address.address_id = pickup_address.id

         left join address as delivery_address
                   on delivery_p_address.address_id = delivery_address.id

         left join logistics_participant as receiverLP
                   on delivery.receiver_id = receiverLP.id

         left join person as receiver
                   on receiverLP.person_id = receiver.id

         left join community
                   on pooling_station.community_id = community.id

         left join transport_assignment as assignment_in
         left join parcel_address as assignment_in_addr
                   on assignment_in.delivery_address_id = assignment_in_addr.id
                   on pooling_station_box_allocation.delivery_id = assignment_in.delivery_id
                       and assignment_in_addr.pooling_station_id = pooling_station.id

         left join transport_assignment as assignment_out
         left join parcel_address as assignment_out_addr
                   on assignment_out.pickup_address_id = assignment_out_addr.id
                   on pooling_station_box_allocation.delivery_id = assignment_out.delivery_id
                       and assignment_out_addr.pooling_station_id = pooling_station.id

         left join person as carrier_in
                   on assignment_in.carrier_id = carrier_in.id

         left join person as carrier_out
                   on assignment_out.carrier_id = carrier_out.id

where community.id = :communityId
  and pooling_station.id like :poolingStationId
  and pooling_station_box_allocation.timestamp >= :fromTime
  and :toTime >= pooling_station_box_allocation.timestamp

order by pooling_station_box_allocation.timestamp desc