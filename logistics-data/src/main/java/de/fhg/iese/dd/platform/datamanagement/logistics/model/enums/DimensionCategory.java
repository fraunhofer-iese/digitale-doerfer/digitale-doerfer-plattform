/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model.enums;

import java.util.Arrays;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Dimension;

/**
 * Category of the box according to size.
 *
 * see {@link DimensionCategory#calculateCategory(Dimension)} for the definition of the sizes.
 */
public enum DimensionCategory {
    UNDERSIZED,
    S,
    M,
    L,
    OVERSIZED;

    /**
     * Calculate the category according to the size (length, width, height).
     * <p/>
     * < 20 x < 10 x < 5: UNDERSIZED
     * <p/>
     * 30 x 20 x 10: S
     * <p/>
     * 40 x 30 x 20: M
     * <p/>
     * 60 x 40 x 30: L
     * <p/>
     * > 60 x > 40 x > 30: OVERSIZED

     * @param dimension
     * @return
     */
    public static DimensionCategory calculateCategory(Dimension dimension){
        double[] dimensions = {dimension.getLength(), dimension.getWidth(), dimension.getHeight()};
        Arrays.sort(dimensions);
        double height = dimensions[0];
        double width  = dimensions[1];
        double length = dimensions[2];

        if(length < 20d || width < 10d || height < 5d){
            return DimensionCategory.UNDERSIZED;
        }
        if(length <= 30d && width <= 20d && height <= 10d){
            return DimensionCategory.S;
        }
        if(length <= 40d && width <= 30d && height <= 20d){
            return DimensionCategory.M;
        }
        if(length <= 60d && width <= 40d && height <= 30d){
            return DimensionCategory.L;
        }
        return DimensionCategory.OVERSIZED;
    }

}
