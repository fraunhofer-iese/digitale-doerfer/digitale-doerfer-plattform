/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SqlResultSetMapping;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAlternativeBundleAdminUIRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraph(name = "TransportAlternativeBundle.reduced",
        includeAllAttributes = false,
        attributeNodes = {
                @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.reduced"),
                @NamedAttributeNode(value = "delivery", subgraph = "Delivery.reduced")
        })
@SqlResultSetMapping(
        name = "TransportAlternativeBundleAdminUIRowMapping",
        classes = @ConstructorResult(
                targetClass = TransportAlternativeBundleAdminUIRow.class,
                columns = {
                        @ColumnResult(name = "created", type = Long.class),
                        @ColumnResult(name = "shopName"),
                        @ColumnResult(name = "pickupAddress"),
                        @ColumnResult(name = "receiver"),
                        @ColumnResult(name = "deliveryAddress"),
                        @ColumnResult(name = "desiredDeliveryTimeStart", type = Long.class),
                        @ColumnResult(name = "desiredDeliveryTimeEnd", type = Long.class),
                        @ColumnResult(name = "purchaseOrderItems"),
                        @ColumnResult(name = "size"),
                        @ColumnResult(name = "weight", type = Double.class),
                        @ColumnResult(name = "orderNotes", type = String.class),
                        @ColumnResult(name = "transportNotes", type = String.class),
                        @ColumnResult(name = "contentNotes", type = String.class),
                        @ColumnResult(name = "ordered", type = Long.class),
                        @ColumnResult(name = "statusHistory"),
                        @ColumnResult(name = "transportAlternativeBundleId"),
                        @ColumnResult(name = "deliveryId"),
                        @ColumnResult(name = "purchaseOrderId"),
                        @ColumnResult(name = "shopOrderId"),
                        @ColumnResult(name = "senderShopId"),
                        @ColumnResult(name = "community"),
                        @ColumnResult(name = "receiverId")
                }))
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportAlternativeBundle extends BaseEntity {

    @ManyToOne
    private ParcelAddress pickupAddress;

    @ManyToOne
    private Delivery delivery;

    public TransportAlternativeBundle withConstantId(){
        generateConstantId(delivery, pickupAddress);
        return this;
    }

    @Override
    public String toString() {
        return "TransportAlternativeBundle [" +
                "id='" + id + "', " +
                "pickupAddress='" + pickupAddress + "', " +
                "delivery='" + delivery + "', " +
                ']';
    }

}
