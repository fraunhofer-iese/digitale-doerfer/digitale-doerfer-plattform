/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Central repository for all entities of type PoolingStationBox.
 *
 * Basic methods for finding a single record, all records, paginated records, create/update, and delete are
 * automatically provided through JpaRepository and its super-classes, while specific methods are explicitly declared
 * in this class.
 *
 * A concrete implementation of this interface is automatically injected by Spring @ runtime.
 */
@Transactional(readOnly = true)
public interface PoolingStationBoxRepository extends JpaRepository<PoolingStationBox, String> {

    String QUERY_findAvailableByPoolingStationId =
            """
                     select
                            (select pooling_station_box_allocation.timestamp
                            from\s
                            pooling_station_box_allocation
                            where
                            pooling_station_box_allocation.pooling_station_box_id = pooling_station_box.id
                            order by pooling_station_box_allocation.timestamp desc
                            limit 1) as lru,

                    pooling_station_box.*

                    from\s
                        pooling_station_box,
                        pooling_station_box_configuration
                    where
                        pooling_station_box.pooling_station_id = ?1\s
                    and
                        pooling_station_box_configuration.id = pooling_station_box.configuration_id
                    and
                        pooling_station_box_configuration.inactive = false
                    and
                        not exists(
                            select 1\s
                            from\s
                            pooling_station_box_allocation
                            where
                            pooling_station_box_allocation.pooling_station_box_id = pooling_station_box.id
                            and
                            pooling_station_box_allocation.status in ('RESERVATION', 'PENDING_IN', 'ACTIVE', 'PENDING_OUT'))

                    order by lru asc
                    limit 1;""";

    @Query(value = QUERY_findAvailableByPoolingStationId, nativeQuery = true)
    PoolingStationBox findAvailableByPoolingStationId(String id);

}
