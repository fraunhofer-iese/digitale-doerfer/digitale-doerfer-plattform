/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.CreatorType;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.HandoverType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "TransportConfirmation.reduced",
                includeAllAttributes = false
        ),
        @NamedEntityGraph(name = "TransportConfirmation.full",
                includeAllAttributes = true
        ),
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TransportConfirmation extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column
    private CreatorType creator;

    @Enumerated(EnumType.STRING)
    @Column
    private HandoverType handover;

    @Column
    private String acceptorName;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String notes;

    @Column
    private long timestamp;

    @ManyToOne
    private TransportAssignment transportAssignmentPickedUp;

    @ManyToOne
    private TransportAssignment transportAssignmentDelivered;

    @ManyToOne
    private TransportAssignment transportAssignmentReceived;

    @ManyToOne
    private ReceiverPickupAssignment receiverPickupAssignmentPickedUp;

    public TransportConfirmation withConstantId(){
        generateConstantId(transportAssignmentPickedUp, transportAssignmentDelivered, receiverPickupAssignmentPickedUp);
        return this;
    }

}
