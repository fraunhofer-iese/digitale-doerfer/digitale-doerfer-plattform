/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos.results;

import de.fhg.iese.dd.platform.datamanagement.shared.misc.repos.results.BaseAdminUIRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryAdminUIRow extends BaseAdminUIRow {

    private Long ordered;//Bestellt: Timestamp (long)
    private String currentStatus;//Status: String
    private Long lastStatusUpdate;//Letztes Statusupdate: Timestamp (long)
    private String shopName;//Shopname: String
    private String pickupAddress;//Abholadresse: String
    private String receiver;//Besteller: String
    private String carrier;//Lieferant: String
    private String deliveryAddress;//Lieferadresse: String
    private Long desiredDeliveryTimeStart;//Gewünschter Lieferzeitraum Start: Timestamp (long)
    private Long desiredDeliveryTimeEnd;//Gewünschter Lieferzeitraum Ende: Timestamp (long)
    private String purchaseOrderItems;//Bestellinhalt: String
    private String trackingCode;//TrackingCode: String
    private String size;//Größe: String (x*y*z)
    private Double weight;//Gewicht: Double
    private String orderNotes;//Bestellnotizen: String
    private String transportNotes;//Transportnotizen: String
    private String contentNotes;//Inhaltsnotizen: String
    private Long packed;//Verpackt: Timestamp (long)
    private Long pickedUp;//Abgeholt: Timestamp (long)
    private Long delivered;//Abgeliefert: Timestamp (long)
    private Long received;//Empfangen: Timestamp (long)
    private String statusHistory;//Status-Verlauf: String
    private String deliveryId;//DeliveryId: String
    private String purchaseOrderId;//PurchaseOrderId: String
    private String shopOrderId;//ShopOrderId: String
    private String senderShopId;//SenderId: String
    private String community;//Community: String
    private String receiverId;//ReceiverId: String

}
