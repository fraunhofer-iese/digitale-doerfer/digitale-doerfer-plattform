/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos.results;

import de.fhg.iese.dd.platform.datamanagement.shared.misc.repos.results.BaseAdminUIRow;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PoolingStationBoxAdminUIRow extends BaseAdminUIRow {

    private String poolingStationName; //Packstations-Name: String
    private String poolingStationBoxName; //Box-Name: String
    private String currentStatus;//Status: String
    private Long lastStatusUpdate;//Letztes Statusupdate: Timestamp (long)
    private Long inTime;//Eingelegt: Timestamp (long)
    private Boolean isDoorOpen;//Tür Offen: boolean
    private String shopName;//Shopname: String
    private String pickupAddress; //Abholadresse: String
    private String receiver;//Empfänger: String
    private String inCarrier;//Lieferant einliefern: String
    private String outCarrier;//Lieferant abholen: String
    private String deliveryAddress;//Lieferadresse: String
    private String deliveryStatus;//Status Bestellung: String
    private String deliveryId;//DeliveryId: String
    private String poolingStationBoxId;//Box-Id: String
    private String community;//Community: String
    private String allocationId;//Allocation-Id: String
    private String transportAssignmentInId;  //TransportAssignmentInId: String
    private String transportAssignmentOutId; //TransportAssignmentOutId: String

}
