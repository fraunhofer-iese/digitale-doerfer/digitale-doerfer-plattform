/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.LogisticsParticipantType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "LogisticsParticipant.reduced",
                attributeNodes = {
                        @NamedAttributeNode(value = "person", subgraph = "Person.reduced"),
                        @NamedAttributeNode(value = "shop", subgraph = "Shop.reduced")
                }),
        @NamedEntityGraph(name = "LogisticsParticipant.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "person", subgraph = "Person.full"),
                        @NamedAttributeNode(value = "shop", subgraph = "Shop.full")
                }),
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogisticsParticipant extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column
    private LogisticsParticipantType participantType;

    @OneToOne
    private Person person;

    @OneToOne
    private Shop shop;

    public LogisticsParticipant(Person person) {
        super();
        this.person = person;
        this.participantType = LogisticsParticipantType.PRIVATE;
    }

    public LogisticsParticipant(Shop shop) {
        super();
        this.shop = shop;
        this.participantType = LogisticsParticipantType.SHOP;
    }

    @JsonIgnore
    @Transient
    public String getFullName() {
        if(participantType == LogisticsParticipantType.SHOP){
            return shop.getName();
        }
        if(participantType == LogisticsParticipantType.PRIVATE){
            return person.getFullName();
        }
        return "unknown";
    }

}
