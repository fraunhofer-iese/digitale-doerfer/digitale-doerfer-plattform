/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToOne;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.ParcelAddressType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.Shop;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "ParcelAddress.reduced",
                attributeNodes = {
                        @NamedAttributeNode(value = "address", subgraph = "Address.reduced"),
                        @NamedAttributeNode(value = "poolingStation", subgraph = "PoolingStation.reduced"),
                        @NamedAttributeNode(value = "poolingStationBox", subgraph = "PoolingStationBox.reduced"),
                        @NamedAttributeNode(value = "person", subgraph = "Person.reduced"),
                        @NamedAttributeNode(value = "shop", subgraph = "Shop.reduced"),
                }),
        @NamedEntityGraph(name = "ParcelAddress.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "address", subgraph = "Address.full"),
                        @NamedAttributeNode(value = "poolingStation", subgraph = "PoolingStation.full"),
                        @NamedAttributeNode(value = "poolingStationBox", subgraph = "PoolingStationBox.full"),
                        @NamedAttributeNode(value = "person", subgraph = "Person.full"),
                        @NamedAttributeNode(value = "shop", subgraph = "Shop.full"),
                }),
})
@Getter
@Setter
@NoArgsConstructor
public class ParcelAddress extends BaseEntity {

    @ManyToOne
    private Address address;

    @Enumerated(EnumType.STRING)
    @Column
    private ParcelAddressType addressType;

    /**
     * poolingStationBox: optional (only if PoolingBoxStation)
     */
    @OneToOne
    private PoolingStationBox poolingStationBox;

    @ManyToOne
    private PoolingStation poolingStation;

    @Column
    private String notes;

    @ManyToOne
    private Person person;

    @ManyToOne
    private Shop shop;

    public ParcelAddress(Address address, Shop shop) {
        super();
        this.address = address;
        this.shop = shop;
        this.addressType = ParcelAddressType.SHOP;
    }

    public ParcelAddress(Address address, PoolingStationBox poolingStationBox, PoolingStation poolingStation) {
        super();
        this.address = address;
        this.poolingStationBox = poolingStationBox;
        this.poolingStation = poolingStation;
        this.addressType = ParcelAddressType.POOLING_STATION;
    }

    public ParcelAddress(Address address, Person person) {
        super();
        this.address = address;
        this.person = person;
        this.addressType = ParcelAddressType.PRIVATE;
    }

    public ParcelAddress(Address address, PoolingStation poolingStation) {
        super();
        this.address = address;
        this.poolingStation = poolingStation;
        this.addressType = ParcelAddressType.POOLING_STATION;
    }

    public ParcelAddress withConstantId(){
        generateConstantId(address, person, shop, poolingStation, poolingStationBox);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ParcelAddress [");
        if (id != null) {
            builder.append("id=");
            builder.append(id);
            builder.append(", ");
        }
        if (address != null) {
            builder.append("address=");
            builder.append(address);
            builder.append(", ");
        }
        if (addressType != null) {
            builder.append("addressType=");
            builder.append(addressType);
            builder.append(", ");
        }
        if (poolingStationBox != null) {
            builder.append("poolingStationBox=");
            builder.append(poolingStationBox);
            builder.append(", ");
        }
        if (poolingStation != null) {
            builder.append("poolingStation=");
            builder.append(poolingStation);
            builder.append(", ");
        }
        if (notes != null) {
            builder.append("notes=");
            builder.append(notes);
            builder.append(", ");
        }
        if (person != null) {
            builder.append("person=");
            builder.append(person);
            builder.append(", ");
        }
        if (shop != null) {
            builder.append("shop=");
            builder.append(shop);
        }
        builder.append("]");
        return builder.toString();
    }

}
