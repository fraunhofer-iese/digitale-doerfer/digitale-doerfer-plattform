/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import java.util.List;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.repos.BaseCustomSQLQueryRepository;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.DeliveryAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.PoolingStationBoxAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.PoolingStationBoxAllocationAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAlternativeBundleAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAssignmentAdminUIRow;

/**
 * All queries are defined in src/main/resources/sql-queries/
 */
@Component
class LogisticsAdminMySQLRepository extends BaseCustomSQLQueryRepository implements LogisticsAdminRepository {

    @Override
    @SuppressWarnings("unchecked")
    public List<DeliveryAdminUIRow> findDeliveriesInCommunityCreatedFromToOrderByCreatedDesc(String communityId,
            long from, long to) {
        return getQueryFromResource("findDeliveriesCreatedFromToMySQL", "DeliveryAdminUIRowMapping")
                .setParameter("communityId", communityId)
                .setParameter("fromTime", from)
                .setParameter("toTime", to)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TransportAlternativeBundleAdminUIRow> findTransportAlternativeBundlesCreatedFromToOrderByCreatedDesc(String communityId, long from, long to){
        return getQueryFromResource("findTransportAlternativeBundlesCreatedFromToMySQL", "TransportAlternativeBundleAdminUIRowMapping")
                .setParameter("communityId", communityId)
                .setParameter("fromTime", from)
                .setParameter("toTime", to)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TransportAssignmentAdminUIRow> findTransportAssignmentsCreatedFromToOrderByCreatedDesc(String communityId, long from, long to){
        return getQueryFromResource("findTransportAssignmentsCreatedFromToMySQL", "TransportAssignmentAdminUIRowMapping")
                .setParameter("communityId", communityId)
                .setParameter("fromTime", from)
                .setParameter("toTime", to)
                .getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PoolingStationBoxAdminUIRow> findPoolingStationBoxesOrderByPoolingStationNameAndBoxName(String communityId, String poolingStationId){
        return getQueryFromResource("findPoolingStationBoxesMySQL", "PoolingStationBoxAdminUIRowMapping").
                setParameter("communityId", communityId).
                setParameter("poolingStationId", poolingStationId).
                getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PoolingStationBoxAllocationAdminUIRow> findPoolingStationBoxAllocationsOrderByTimestampDesc(String communityId, String poolingStationId, long from, long to){
        return getQueryFromResource("findPoolingStationBoxAllocationsMySQL", "PoolingStationBoxAllocationAdminUIRowMapping").
                setParameter("communityId", communityId).
                setParameter("poolingStationId", poolingStationId).
                setParameter("fromTime", from).
                setParameter("toTime", to).
                getResultList();
    }

}
