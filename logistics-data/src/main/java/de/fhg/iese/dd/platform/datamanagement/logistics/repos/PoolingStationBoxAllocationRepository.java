/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBox;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.PoolingStationBoxAllocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface PoolingStationBoxAllocationRepository extends JpaRepository<PoolingStationBoxAllocation, String> {

    String QUERY_findAllByPoolingStationIdAndDeliveryId =
            """
                    select
                        pooling_station_box_allocation.*
                    from\s
                        pooling_station_box_allocation,
                        pooling_station_box
                    where
                        pooling_station_box.pooling_station_id = ?1\s
                        and
                        pooling_station_box_allocation.delivery_id = ?2\s
                        and
                        pooling_station_box_allocation.pooling_station_box_id = pooling_station_box.id

                    order by pooling_station_box_allocation.timestamp desc;""";

    PoolingStationBoxAllocation findFirstByPoolingStationBoxOrderByTimestampDesc(PoolingStationBox poolingStationBox);

    @Query(value = QUERY_findAllByPoolingStationIdAndDeliveryId, nativeQuery = true)
    List<PoolingStationBoxAllocation> findAllByPoolingStationIdAndDeliveryId(String poolingStationId,
            String deliveryId);

    @Transactional(readOnly = false)
    void deleteAllByDeliveryId(String deliveryId);

    List<PoolingStationBoxAllocation> findAllByDeliveryOrderByCreatedDesc(Delivery delivery);

}
