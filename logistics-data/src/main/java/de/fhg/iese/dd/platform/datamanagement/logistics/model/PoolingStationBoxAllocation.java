/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.BoxAllocationStatus;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.PoolingStationBoxAllocationAdminUIRow;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@Table(indexes = {
        @Index(columnList = "inTime"),
        @Index(columnList = "outTime"),
        @Index(columnList = "timestamp"),
        @Index(columnList = "status"),
        @Index(columnList = "delivery_id, pooling_station_box_id"),
})
@NamedEntityGraph(name = "PoolingStationBoxAllocation.reduced",
        includeAllAttributes = false,
        attributeNodes = {
                @NamedAttributeNode(value = "poolingStationBox", subgraph = "PoolingStationBox.reduced"),
        })
@SqlResultSetMapping(
        name = "PoolingStationBoxAllocationAdminUIRowMapping",
        classes = @ConstructorResult(
                targetClass = PoolingStationBoxAllocationAdminUIRow.class,
                columns = {
                        @ColumnResult(name = "poolingStationName"),
                        @ColumnResult(name = "poolingStationBoxName"),
                        @ColumnResult(name = "currentStatus"),
                        @ColumnResult(name = "lastStatusUpdate", type = Long.class),
                        @ColumnResult(name = "inTime", type = Long.class),
                        @ColumnResult(name = "outTime", type = Long.class),
                        @ColumnResult(name = "isDoorOpen", type = Boolean.class),
                        @ColumnResult(name = "shopName"),
                        @ColumnResult(name = "pickupAddress"),
                        @ColumnResult(name = "receiver"),
                        @ColumnResult(name = "inCarrier"),
                        @ColumnResult(name = "outCarrier"),
                        @ColumnResult(name = "deliveryAddress"),
                        @ColumnResult(name = "deliveryStatus"),
                        @ColumnResult(name = "deliveryId"),
                        @ColumnResult(name = "poolingStationBoxId"),
                        @ColumnResult(name = "community"),
                        @ColumnResult(name = "allocationId"),
                        @ColumnResult(name = "transportAssignmentInId"),
                        @ColumnResult(name = "transportAssignmentOutId")}))
@Getter
@SuperBuilder
@NoArgsConstructor
public class PoolingStationBoxAllocation extends BaseEntity {

    @ManyToOne
    private PoolingStationBox poolingStationBox;

    @ManyToOne
    private Delivery delivery;

    @Column(nullable = true)
    private long inTime;

    @Column(nullable = true)
    private long outTime;

    /**
     * Updated whenever a value changes. It is required to find the newest
     * {@link PoolingStationBoxAllocation}
     */
    @Column
    private long timestamp;

    @Enumerated(EnumType.STRING)
    @Column
    private BoxAllocationStatus status;

    @Column(nullable=false)
    @Builder.Default
    private boolean isDoorOpen = false;
    
    public PoolingStationBoxAllocation setDelivery(Delivery delivery) {
        this.delivery = delivery;
        return this;
    }

    public PoolingStationBoxAllocation setInTime(long inTime) {
        this.inTime = inTime;
        return this;
    }

    public PoolingStationBoxAllocation setOutTime(long outTime) {
        this.outTime = outTime;
        return this;
    }

    public PoolingStationBoxAllocation setStatus(BoxAllocationStatus status) {
        this.status = status;
        return this;
    }

    public PoolingStationBoxAllocation setDoorOpen(boolean isDoorOpen) {
        this.isDoorOpen = isDoorOpen;
        return this;
    }

    public PoolingStationBoxAllocation setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

}
