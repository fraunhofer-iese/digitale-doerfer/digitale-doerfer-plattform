/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAlternativeBundle;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface TransportAlternativeBundleRepository extends JpaRepository<TransportAlternativeBundle, String> {

    String QUERY_countAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThan =
            """
                    select\s
                        count(*)\s
                     from\s
                        transport_alternative_bundle,\s
                         delivery\s
                     where\s
                         transport_alternative_bundle.delivery_id = delivery.id\s
                     and\s
                         delivery.community_id = ?1\s
                     and\s
                         'WAITING_FOR_ASSIGNMENT' =\s
                            (select status_record.status\s
                             from transport_alternative_bundle_status_record as status_record\s
                             where\s
                             status_record.transport_alternative_bundle_id = transport_alternative_bundle.id\s
                             and\s
                             status_record.timestamp > ?2\s
                             order by status_record.timestamp desc\s
                             limit 1)
                    """;

    String QUERY_findAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThan =
            """
                    select\s
                        transport_alternative_bundle.*\s
                     from\s
                        transport_alternative_bundle,\s
                         delivery\s
                     where\s
                         transport_alternative_bundle.delivery_id = delivery.id\s
                     and\s
                         delivery.community_id = ?1\s
                     and\s
                         'WAITING_FOR_ASSIGNMENT' =\s
                            (select status_record.status\s
                             from transport_alternative_bundle_status_record as status_record\s
                             where\s
                             status_record.transport_alternative_bundle_id = transport_alternative_bundle.id\s
                             and\s
                             status_record.timestamp > ?2\s
                             order by status_record.timestamp desc\s
                             limit 1)\s
                     order by transport_alternative_bundle.created desc\s
                    \s""";

    String QUERY_findAllWaitingForAssignmentByCommunityIdsNotExpiredSince =
            """
                    select\s
                        transport_alternative_bundle.*\s
                     from\s
                        transport_alternative_bundle,\s
                         delivery\s
                     where\s
                         transport_alternative_bundle.delivery_id = delivery.id\s
                     and\s
                         delivery.community_id in (:communityIds)\s
                     and\s
                         'WAITING_FOR_ASSIGNMENT' =\s
                            (select status_record.status\s
                             from transport_alternative_bundle_status_record as status_record\s
                             where\s
                             status_record.transport_alternative_bundle_id = transport_alternative_bundle.id\s
                             order by status_record.timestamp desc\s
                             limit 1)\s
                     and\s
                         delivery.desired_delivery_time_end > :expiredTimestamp\s
                     order by transport_alternative_bundle.created desc\s
                    \s""";

    String QUERY_findFirstWaitingForAssignmentByCommunityIdAndTimestampGreaterThan =
            """
                    select\s
                        transport_alternative_bundle.*\s
                     from\s
                        transport_alternative_bundle,\s
                         delivery\s
                     where\s
                         transport_alternative_bundle.delivery_id = delivery.id\s
                     and\s
                         delivery.community_id = ?1\s
                     and\s
                         'WAITING_FOR_ASSIGNMENT' =\s
                            (select status_record.status\s
                             from transport_alternative_bundle_status_record as status_record\s
                             where\s
                             status_record.transport_alternative_bundle_id = transport_alternative_bundle.id\s
                             and\s
                             status_record.timestamp > ?2\s
                             order by status_record.timestamp desc\s
                             limit 1)\s
                     order by transport_alternative_bundle.created desc\s
                     limit 1""";

    @EntityGraph(value = "TransportAlternativeBundle.reduced", type = EntityGraphType.LOAD)
    List<TransportAlternativeBundle> findAllByDeliveryOrderByCreatedDesc(Delivery delivery);

    @Query(value = QUERY_findAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThan, nativeQuery = true)
    List<TransportAlternativeBundle> findAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThanOrderByCreatedDesc(
            String communityId, long timestamp);

    @Query(value = QUERY_findAllWaitingForAssignmentByCommunityIdsNotExpiredSince, nativeQuery = true)
    List<TransportAlternativeBundle> findAllWaitingForAssignmentByCommunityIdsNotExpiredSinceOrderByCreatedDesc(
            List<String> communityIds, long expiredTimestamp);

    @Query(value = QUERY_findFirstWaitingForAssignmentByCommunityIdAndTimestampGreaterThan, nativeQuery = true)
    TransportAlternativeBundle findFirstWaitingForAssignmentByCommunityIdAndTimestampGreaterThan(String communityId,
            long timestamp);

    @Query(value = QUERY_countAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThan, nativeQuery = true)
    long countAllWaitingForAssignmentByCommunityIdAndTimestampGreaterThan(String communityId, long timestamp);

}
