/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.DimensionCategory;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Immutable dimension with a calculated {@link DimensionCategory}. <br/>
 * (see {@link DimensionCategory#calculateCategory(Dimension)} for the rules)
 * <p/>
 * This invariant should be met when creating dimensions: <br/>
 * <code>length >= width >= height</code> <br/>
 * Nevertheless it is not enforced to support custom dimensions when the
 * according parcel can not be tilted. To get a normalized dimension use {@link #toNormalizedDimension()}.
 *
 */
@Getter
@Embeddable
@NoArgsConstructor
public class Dimension {

    /**
     * Length of the box in cm, largest number
     * <p>
     * length >= width >= height
     */
    @Column
    private double length;

    /**
     * Width of the box in cm
     * <p>
     * length >= width >= height
     */
    @Column
    private double width;

    /**
     * Height of the box in cm, smallest number
     * <p>
     * length >= width >= height
     */
    @Column
    private double height;

    /**
     * Category of the box according to the size (length, width, height).
     *
     */
    @Enumerated(EnumType.STRING)
    @Column
    private DimensionCategory category;

    /**
     * Weight of the box in kg
     */
    @Column
    private double weight;

    /**
     * Create a new dimension with this condition met: <br/>
     * <code>length >= width >= height</code> <br/>
     *
     * @return a new dimension with swapped length, width, height.
     */
    public Dimension toNormalizedDimension(){
        double[] dimensions = {length, width, height};
        Arrays.sort(dimensions);
        return new Dimension(dimensions[2], dimensions[1], dimensions[0], weight);
    }

    /**
     * Create a new entity and calculate the category acccording to  {@link DimensionCategory#calculateCategory(Dimension)}.
     * <p>
     * No normalization is taking place, use {@link #toNormalizedDimension()} for this purpose.
     *
     * @param length
     * @param width
     * @param height
     * @param weight
     */
    @Builder
    public Dimension(double length, double width, double height, double weight) {
        super();
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
        this.category = DimensionCategory.calculateCategory(this);
    }

}
