/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Axel Wickenkamp, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.ParcelAddress;

/**
 * Central repository for all entities of type ParcelAddress.
 *
 * Basic methods for finding a single record, all records, paginated records, create/update, and delete are
 * automatically provided through JpaRepository and its super-classes, while specific methods are explicitly declared
 * in this class.
 *
 * A concrete implementation of this interface is automatically injected by Spring @ runtime.
 *
 */
@Transactional(readOnly = true)
public interface ParcelAddressRepository extends JpaRepository<ParcelAddress, String> {

}
