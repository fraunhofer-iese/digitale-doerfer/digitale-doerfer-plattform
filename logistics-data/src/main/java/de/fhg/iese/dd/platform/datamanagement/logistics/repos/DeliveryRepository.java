/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface DeliveryRepository extends JpaRepository<Delivery, String> {

    @EntityGraph(value = "Delivery.full", type = EntityGraphType.LOAD)
    List<Delivery> findAllByReceiverPersonOrderByCreatedDesc(Person receiver);

    @EntityGraph(value = "Delivery.full", type = EntityGraphType.LOAD)
    List<Delivery> findAllByCommunityId(String communityId);

    String FIND_ALL_DELIVERIES_BY_RECEIVER_SINCE_AND_ALL_NOT_RECEIVED_QUERY =
            """
                    select
                        delivery.*
                    from
                        delivery

                        left join logistics_participant as receiverLP
                        on delivery.receiver_id = receiverLP.id

                        left join person as receiver
                        on receiverLP.person_id = receiver.id
                    where
                        receiver.id = :receiverId
                    and
                        (
                            delivery.created >= :sinceTimestamp
                        or
                            not exists(select 1
                                       from delivery_status_record
                                       where delivery_status_record.delivery_id = delivery.id
                                       and delivery_status_record.status = 'RECEIVED')
                        )
                    order by delivery.created desc""";

    @Query(value = FIND_ALL_DELIVERIES_BY_RECEIVER_SINCE_AND_ALL_NOT_RECEIVED_QUERY, nativeQuery = true)
    List<Delivery> findAllDeliveriesByReceiverSinceAndAllNotReceivedOrderByCreatedDesc(
            @Param("receiverId")String receiverId,
            @Param("sinceTimestamp")long sinceTimestamp);

    @Query(value = "select distinct(tracking_code) from delivery where created > ?1 AND tracking_code is not null",
            nativeQuery = true)
    List<String> findAllTrackingCodesSince(long since);

    @EntityGraph(value = "Delivery.full", type = EntityGraphType.LOAD)
    List<Delivery> findAllByCustomReferenceNumber(String referenceNumber);
}
