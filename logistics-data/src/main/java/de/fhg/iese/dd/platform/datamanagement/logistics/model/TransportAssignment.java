/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.TransportAssignmentAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.repos.results.PersonAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "TransportAssignment.reduced",
                attributeNodes = {
                        @NamedAttributeNode(value = "carrier", subgraph = "Person.reduced"),
                        @NamedAttributeNode(value = "delivery", subgraph = "Delivery.reduced"),
                        @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.reduced"),
                        @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.reduced"),
                        @NamedAttributeNode(value = "transportAlternative", subgraph = "TransportAlternative.reduced"),
                }),
        @NamedEntityGraph(name = "TransportAssignment.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "carrier", subgraph = "Person.full"),
                        @NamedAttributeNode(value = "delivery", subgraph = "Delivery.full"),
                        @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.full"),
                        @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.full"),
                        @NamedAttributeNode(value = "transportAlternative", subgraph = "TransportAlternative.full"),
                })
})
@SqlResultSetMappings({
        @SqlResultSetMapping(
                name = "TransportAssignmentAdminUIRowMapping",
                classes = @ConstructorResult(
                        targetClass = TransportAssignmentAdminUIRow.class,
                        columns = {
                                @ColumnResult(name = "assigned", type = Long.class),
                                @ColumnResult(name = "currentStatus"),
                                @ColumnResult(name = "lastStatusUpdate", type = Long.class),
                                @ColumnResult(name = "shopName"),
                                @ColumnResult(name = "pickupAddress"),
                                @ColumnResult(name = "receiver"),
                                @ColumnResult(name = "carrier"),
                                @ColumnResult(name = "deliveryAddress"),
                                @ColumnResult(name = "desiredDeliveryTimeStart", type = Long.class),
                                @ColumnResult(name = "desiredDeliveryTimeEnd", type = Long.class),
                                @ColumnResult(name = "purchaseOrderItems"),
                                @ColumnResult(name = "size"),
                                @ColumnResult(name = "weight", type = Double.class),
                                @ColumnResult(name = "orderNotes", type = String.class),
                                @ColumnResult(name = "transportNotes", type = String.class),
                                @ColumnResult(name = "contentNotes", type = String.class),
                                @ColumnResult(name = "ordered", type = Long.class),
                                @ColumnResult(name = "packed", type = Long.class),
                                @ColumnResult(name = "available", type = Long.class),
                                @ColumnResult(name = "pickedUp", type = Long.class),
                                @ColumnResult(name = "delivered", type = Long.class),
                                @ColumnResult(name = "received", type = Long.class),
                                @ColumnResult(name = "statusHistory"),
                                @ColumnResult(name = "transportAssignmentId"),
                                @ColumnResult(name = "deliveryId"),
                                @ColumnResult(name = "purchaseOrderId"),
                                @ColumnResult(name = "shopOrderId"),
                                @ColumnResult(name = "senderShopId"),
                                @ColumnResult(name = "community"),
                                @ColumnResult(name = "receiverId"),
                                @ColumnResult(name = "carrierId")
                        })),
        @SqlResultSetMapping(
                name = "PersonAdminUIRowMapping",
                classes = @ConstructorResult(
                        targetClass = PersonAdminUIRow.class,
                        columns = {
                                @ColumnResult(name="name"),
                                @ColumnResult(name="address"),
                                @ColumnResult(name="phoneNumber"),
                                @ColumnResult(name="email"),
                                @ColumnResult(name="created", type = Long.class),
                                @ColumnResult(name="lastLoggedIn", type = Long.class),
                                @ColumnResult(name="profilePictureUrl"),
                                @ColumnResult(name="accountType"),
                                @ColumnResult(name="countPurchaseOrder", type = Integer.class),
                                @ColumnResult(name="countTransportAssignment", type = Integer.class),
                                @ColumnResult(name="countCancelledTransportAssignment", type = Integer.class),
                                @ColumnResult(name="community"),
                                @ColumnResult(name="personId")
                        }))
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class TransportAssignment extends BaseEntity {

    @ManyToOne
    private Person carrier;

    @Column
    private int credits;

    @ManyToOne
    private ParcelAddress deliveryAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredDeliveryTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredDeliveryTimeEnd")),
    })
    private TimeSpan desiredDeliveryTime;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredPickupTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredPickupTimeEnd")),
    })
    private TimeSpan desiredPickupTime;

    @ManyToOne
    private ParcelAddress pickupAddress;

    @Column
    private long latestDeliveryTime;

    @Column
    private long latestPickupTime;

    @ManyToOne
    @JoinColumn(name="delivery_id")
    private Delivery delivery;

    @ManyToOne
    @JoinColumn(name="transport_alternative_id")
    private TransportAlternative transportAlternative;

    @Enumerated(EnumType.STRING)
    @Column
    private TransportKind kind;

    public TransportAssignment withConstantId(){
        generateConstantId(delivery, carrier, pickupAddress, deliveryAddress);
        return this;
    }

}
