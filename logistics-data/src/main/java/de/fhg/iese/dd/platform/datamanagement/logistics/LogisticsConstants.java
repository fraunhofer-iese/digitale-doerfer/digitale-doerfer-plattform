/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics;

import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;

public final class LogisticsConstants {

    public static final String TENANT_ID_DEMO = "ceb17000-36e0-4ca4-841f-e0d1dac8dd83";

    public static final String LOGISTICS_DELIVERY_TRANSPORT_CHAT_ID = "delivery-transport-chat";

    public static final String LIEFERBAR_APP_ID = "4137a5e3-65b1-4efb-9fd1-c4b65dd42755";

    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_ID =
            new PushCategoryId("7e8f50fd-3ff3-44a0-aab9-53e2db7ad343");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_NEW_TRANSPORTS_PERIODIC_ID =
            new PushCategoryId("e770b36d-fd14-41b4-ae20-6c8e6db3f1c9");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_DELIVERY_STATUS_RECEIVER_ID =
            new PushCategoryId("b3fe5107-7399-40ff-8d49-7fa97ebb981b");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_TRANSPORT_STATUS_CARRIER_ID =
            new PushCategoryId("84d7087f-f49e-4cd4-9b20-1bd8f82b0861");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_PICKUP_ASSIGNMENT_ID =
            new PushCategoryId("da9f497e-146c-4e80-a335-0bc8ed918e6c");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_MOTIVATION_ID =
            new PushCategoryId("440ce7f9-341d-48d7-8bc9-62a98b919fb0");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_OWN_PERSON_CHANGES_ID =
            new PushCategoryId("3121d9b4-5fcd-4e45-ae42-d9ae2a097e33");
    public static final PushCategoryId LIEFERBAR_PUSH_CATEGORY_CHAT_ID =
            new PushCategoryId("3653d8f9-8528-434d-8014-532c2d193c1d");

}
