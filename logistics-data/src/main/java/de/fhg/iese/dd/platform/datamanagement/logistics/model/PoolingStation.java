/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PoolingStationType;
import de.fhg.iese.dd.platform.datamanagement.participants.shop.model.OpeningHours;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "PoolingStation.reduced",
                attributeNodes = {
                        @NamedAttributeNode(value = "address", subgraph = "Address.reduced")
                }),
        @NamedEntityGraph(name = "PoolingStation.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "address", subgraph = "Address.full")
                }),
})
@Table(indexes = {
        @Index(columnList = "name"),
        @Index(columnList = "stationType"),
})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PoolingStation extends BaseEntity implements NamedEntity {

    @OneToOne
    private Address address;

    @Column
    private String name;

    @Column
    private String verificationCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem profilePicture;

    @ManyToOne
    @Getter(AccessLevel.PRIVATE)
    private Tenant community;

    @OneToOne
    private OpeningHours openingHours;

    @Enumerated(EnumType.STRING)
    @Column
    private PoolingStationType stationType;

    @Column
    private Boolean isSelfScanningStation=false;

    @Column(nullable=false)
    private boolean isFinalStation=false;

    @Column(nullable=false)
    private boolean isIntermediateStation=false;

    @Column(nullable=false)
    private boolean isMainHub=false;

    public PoolingStation(String id, Address address, String name, String verificationCode, MediaItem profilePicture,
            Tenant tenant, OpeningHours openingHours, PoolingStationType stationType,
            boolean isFinalStation, boolean isIntermediateStation, boolean isMainHub,
            boolean isSelfScanningStation) {
        super();
        this.id = id;
        this.address = address;
        this.name = name;
        this.verificationCode = verificationCode;
        this.profilePicture = profilePicture;
        this.community = tenant;
        this.openingHours = openingHours;
        this.stationType = stationType;
        this.isSelfScanningStation = isSelfScanningStation;
        this.isFinalStation = isFinalStation;
        this.isIntermediateStation = isIntermediateStation;
        this.isMainHub = isMainHub;
    }

    public boolean isSelfScanningStation() {
        if(isSelfScanningStation==null) isSelfScanningStation=false;
        return isSelfScanningStation;
    }

    @Override
    public String toString() {
        return "PoolingStation [id=" +
                id +
                ", name=" +
                name +
                ", address=" +
                address +
                ", stationType=" +
                stationType +
                "]";
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
