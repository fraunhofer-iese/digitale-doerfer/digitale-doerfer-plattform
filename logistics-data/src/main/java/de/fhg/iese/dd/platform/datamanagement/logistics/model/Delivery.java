/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2019 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.PackagingType;
import de.fhg.iese.dd.platform.datamanagement.logistics.repos.results.DeliveryAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraphs({
        @NamedEntityGraph(name = "Delivery.reduced",
                includeAllAttributes = false,
                attributeNodes = {
                        @NamedAttributeNode(value = "sender", subgraph = "LogisticsParticipant.reduced"),
                        @NamedAttributeNode(value = "receiver", subgraph = "LogisticsParticipant.reduced"),
                        @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.reduced"),
                        @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.reduced"),
                }),
        @NamedEntityGraph(name = "Delivery.full",
                includeAllAttributes = true,
                attributeNodes = {
                        @NamedAttributeNode(value = "sender", subgraph = "LogisticsParticipant.full"),
                        @NamedAttributeNode(value = "receiver", subgraph = "LogisticsParticipant.full"),
                        @NamedAttributeNode(value = "pickupAddress", subgraph = "ParcelAddress.full"),
                        @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.full"),
                }),
})
@Table(indexes={
        @Index(columnList="trackingCode"),
        @Index(columnList="customReferenceNumber")
        })
@SqlResultSetMapping(
        name = "DeliveryAdminUIRowMapping",
        classes = @ConstructorResult(
                targetClass = DeliveryAdminUIRow.class,
                columns = {
                        @ColumnResult(name = "ordered", type = Long.class),
                        @ColumnResult(name = "currentStatus"),
                        @ColumnResult(name = "lastStatusUpdate", type = Long.class),
                        @ColumnResult(name = "shopName"),
                        @ColumnResult(name = "pickupAddress"),
                        @ColumnResult(name = "receiver"),
                        @ColumnResult(name = "carrier"),
                        @ColumnResult(name = "deliveryAddress"),
                        @ColumnResult(name = "desiredDeliveryTimeStart", type = Long.class),
                        @ColumnResult(name = "desiredDeliveryTimeEnd", type = Long.class),
                        @ColumnResult(name = "purchaseOrderItems"),
                        @ColumnResult(name = "trackingCode"),
                        @ColumnResult(name = "size"),
                        @ColumnResult(name = "weight", type = Double.class),
                        @ColumnResult(name = "orderNotes", type = String.class),
                        @ColumnResult(name = "transportNotes", type = String.class),
                        @ColumnResult(name = "contentNotes", type = String.class),
                        @ColumnResult(name = "packed", type = Long.class),
                        @ColumnResult(name = "pickedUp", type = Long.class),
                        @ColumnResult(name = "delivered", type = Long.class),
                        @ColumnResult(name = "received", type = Long.class),
                        @ColumnResult(name = "statusHistory"),
                        @ColumnResult(name = "deliveryId"),
                        @ColumnResult(name = "purchaseOrderId"),
                        @ColumnResult(name = "shopOrderId"),
                        @ColumnResult(name = "senderShopId"),
                        @ColumnResult(name = "community"),
                        @ColumnResult(name = "receiverId")}))
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class Delivery extends BaseEntity {

    @Column
    private String trackingCode;

    @Column
    private String trackingCodeLabelURL;

    @Column
    private String trackingCodeLabelA4URL;

    @ManyToOne
    private Tenant community;

    @ManyToOne
    private LogisticsParticipant sender;

    @ManyToOne
    private ParcelAddress pickupAddress;

    @ManyToOne
    private LogisticsParticipant receiver;

    @ManyToOne
    private ParcelAddress deliveryAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredDeliveryTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredDeliveryTimeEnd")),
    })
    private TimeSpan desiredDeliveryTime;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "estimatedDeliveryTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "estimatedDeliveryTimeEnd")),
    })
    private TimeSpan estimatedDeliveryTime;

    @Column
    private long actualDeliveryTime;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String transportNotes;

    @Column
    private int credits;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name="width",column=@Column(name="size_width")),
        @AttributeOverride(name="length",column=@Column(name="size_length")),
        @AttributeOverride(name="height",column=@Column(name="size_height")),
        @AttributeOverride(name="category",column=@Column(name="size_category")),
        @AttributeOverride(name="weight",column=@Column(name="size_weight")),
    })
    private Dimension size;

    @Enumerated(EnumType.STRING)
    @Column
    private PackagingType packagingType;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String contentNotes;

    @Column
    @Builder.Default
    private Integer parcelCount = 1;

    @Column
    private String customReferenceNumber;

    @Override
    public String toString() {
        return "Delivery [id=" +id +
                ", trackingCode=" + trackingCode +
                ", customReferenceNumber=" + customReferenceNumber +
                ", community=" + community +
                ", sender=" + sender +
                ", pickupAddress=" + pickupAddress +
                ", receiver=" + receiver +
                ", deliveryAddress=" + deliveryAddress +
                "]";
    }

    /**
     * Use {@link #getTenant()} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Tenant getCommunity() {
        return community;
    }

    /**
     * Use {@link #setTenant(Tenant)} instead
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void setCommunity(Tenant community) {
        this.community = community;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @param tenant
     */
    public void setTenant(Tenant tenant) {
        this.community = tenant;
    }

    /**
     * Intermediate method until all occurrences of Community are renamed to Tenant
     *
     * @return the tenant of the person
     */
    public Tenant getTenant() {
        return community;
    }

}
