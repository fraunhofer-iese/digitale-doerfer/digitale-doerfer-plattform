/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.repos;

import de.fhg.iese.dd.platform.datamanagement.logistics.model.TransportAssignment;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Transactional(readOnly = true)
public interface TransportAssignmentRepository extends JpaRepository<TransportAssignment, String> {

    String QUERY_findAllNonCancelledByCarrierId =
            """
                    select\s
                        transport_assignment.*
                    from\s
                        transport_assignment
                    where
                        transport_assignment.carrier_id = ?1\s
                    and
                        true =
                            (select (status_record.status != 'CANCELLED' and status_record.timestamp > ?2 )
                            from transport_assignment_status_record as status_record
                            where
                            status_record.transport_assignment_id = transport_assignment.id
                            order by status_record.timestamp desc
                            limit 1)
                    order by transport_assignment.created desc;""";

    String NON_CANCELLED_ASSIGNMENTS_DELIVERY_COUNT_QUERY =
            """
                    select\s
                        count(*)
                    from\s
                        transport_assignment\s
                    where\s
                        transport_assignment.carrier_id = :personId\s
                    and\s
                        transport_assignment.delivery_id = :deliveryId\s
                    and\s
                        true =
                            (select status_record.status != 'CANCELLED'
                            from transport_assignment_status_record as status_record
                            where
                            status_record.transport_assignment_id = transport_assignment.id
                            order by status_record.timestamp desc
                            limit 1)""";

    String COMPLETED_ASSIGNMENTS_FIRST_QUERY =
            "select transport_assignment.* "
                    + "from transport_assignment "
                    + "inner join transport_assignment_status_record "
                    + "on transport_assignment.id = transport_assignment_status_record.transport_assignment_id "
                    + "where transport_assignment_status_record.status = 'DELIVERED' "
                    + "and transport_assignment.carrier_id = :personId "
                    + "order by transport_assignment.created desc "
                    + "limit 1;";

    String COMPLETED_ASSIGNMENTS_COUNT_QUERY =
            "select count(*) "
                    + "from transport_assignment "
                    + "inner join transport_assignment_status_record "
                    + "on transport_assignment.id = transport_assignment_status_record.transport_assignment_id "
                    + "where transport_assignment_status_record.status = 'DELIVERED' "
                    + "and transport_assignment.carrier_id = :personId ;";

    String COMPLETED_ASSIGNMENTS_COUNT_DISTINCT_PERSON_ADDRESS_QUERY =
            "select count(distinct transport_assignment.delivery_address_id) "
                    + "from transport_assignment, transport_assignment_status_record, parcel_address "
                    + "where transport_assignment_status_record.status = 'DELIVERED' "
                    + "and transport_assignment.id = transport_assignment_status_record.transport_assignment_id "
                    + "and transport_assignment.carrier_id = :personId "
                    + "and transport_assignment.delivery_address_id = parcel_address.id "
                    + "and parcel_address.address_type = 'PRIVATE' ;";

    String COMPLETED_ASSIGNMENTS_COUNT_PERSON_ADDRESS_QUERY =
            "select count(*)"
                    + "from transport_assignment, transport_assignment_status_record, parcel_address "
                    + "where transport_assignment_status_record.status = 'DELIVERED' "
                    + "and transport_assignment.id = transport_assignment_status_record.transport_assignment_id "
                    + "and transport_assignment.carrier_id = :personId "
                    + "and transport_assignment.delivery_address_id = parcel_address.id "
                    + "and parcel_address.address_type = 'PRIVATE' "
                    + "and parcel_address.person_id = :receiverId ;";

    //FIXME strange way of counting, why not using the delivery.receiver
    String COMPLETED_ASSIGNMENTS_MAX_COUNT_PERSON_ADDRESS_QUERY =
            "select max(n) FROM ( "
                    + "select transport_assignment.delivery_address_id, count(*) AS n "
                    + "from transport_assignment, transport_assignment_status_record, parcel_address "
                    + "where transport_assignment_status_record.status = 'DELIVERED' "
                    + "and transport_assignment.id = transport_assignment_status_record.transport_assignment_id "
                    + "and transport_assignment.delivery_address_id = parcel_address.id "
                    + "and parcel_address.address_type = 'PRIVATE' "
                    + "and transport_assignment.carrier_id = :personId "
                    + "group by transport_assignment.delivery_address_id) as t;";

    @Query(value = QUERY_findAllNonCancelledByCarrierId, nativeQuery = true)
    List<TransportAssignment> findAllNonCancelledByCarrierIdOrderByCreatedDesc(String carrierId, long sinceTimestamp);

    @EntityGraph(value = "TransportAssignment.reduced", type = EntityGraphType.LOAD)
    List<TransportAssignment> findAllByDeliveryIdOrderByCreatedDesc(String deliveryId);

    @EntityGraph(value = "TransportAssignment.reduced", type = EntityGraphType.LOAD)
    TransportAssignment findFirstByDeliveryIdOrderByCreatedDesc(String deliveryId);

    List<TransportAssignment> findAllByCarrierOrderByCreatedDesc(Person carrier);

    @Query(value = COMPLETED_ASSIGNMENTS_FIRST_QUERY, nativeQuery = true)
    TransportAssignment findFirstByCarrierIdOrderByCreatedDesc(@Param("personId") String personId);

    @EntityGraph(value = "TransportAssignment.reduced", type = EntityGraphType.LOAD)
    List<TransportAssignment> findAllByTransportAlternativeIdAndCarrierIdOrderByCreatedDesc(String transportAlternativeId, String carrierId);

    @Query(value = COMPLETED_ASSIGNMENTS_COUNT_QUERY, nativeQuery = true)
    int getNumberOfCompletedTransportAssignmentsForPerson(@Param("personId") String personId);

    @Query(value = NON_CANCELLED_ASSIGNMENTS_DELIVERY_COUNT_QUERY, nativeQuery = true)
    int getNumberOfNonCancelledTransportAssignmentsForPersonAndDelivery(@Param("personId") String personId, @Param("deliveryId") String deliveryId);

    @Query(value = COMPLETED_ASSIGNMENTS_COUNT_DISTINCT_PERSON_ADDRESS_QUERY, nativeQuery = true)
    int getNumberOfDistinctDeliveryAddressesOFCompletedTransportAssignmentsForPerson(@Param("personId") String personId);

    @Query(value = COMPLETED_ASSIGNMENTS_COUNT_PERSON_ADDRESS_QUERY, nativeQuery = true)
    int getNumberOfCompletedTransportAssignmentsForPersonAndReceiverPerson(@Param("personId") String personId, @Param("receiverId") String receiverId);

    @Query(value = COMPLETED_ASSIGNMENTS_MAX_COUNT_PERSON_ADDRESS_QUERY, nativeQuery = true)
    Optional<BigInteger> getMaxNumberOfCompletedTransportAssignmentsToSameReceiverPerson(@Param("personId") String personId);

    Set<TransportAssignment> findAllByDeliveryIdIn(Set<String> deliveryIds);

}
