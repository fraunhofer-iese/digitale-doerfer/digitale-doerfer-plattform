/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2016 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;

import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategy;
import de.fhg.iese.dd.platform.datamanagement.framework.NamingStrategyName;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.enums.TransportKind;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NamingStrategy(strategy = NamingStrategyName.CLASSNAME)
@NamedEntityGraph(name = "TransportAlternative.reduced",
        includeAllAttributes = false,
        attributeNodes = {
                @NamedAttributeNode(value = "deliveryAddress", subgraph = "ParcelAddress.reduced"),
                @NamedAttributeNode(value = "transportAlternativeBundle",
                        subgraph = "TransportAlternativeBundle.reduced")
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportAlternative extends BaseEntity {

    @Column
    private int credits;

    @ManyToOne
    private ParcelAddress deliveryAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredDeliveryTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredDeliveryTimeEnd")),
    })
    private TimeSpan desiredDeliveryTime;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "startTime", column = @Column(name = "desiredPickupTimeStart")),
            @AttributeOverride(name = "endTime", column = @Column(name = "desiredPickupTimeEnd")),
    })
    private TimeSpan desiredPickupTime;

    @Enumerated(EnumType.STRING)
    @Column
    private TransportKind kind;

    @Column
    private long latestPickupTime;

    @Column
    private long latestDeliveryTime;

    @ManyToOne
    @JoinColumn(name="transport_alternative_bundle_id")
    private TransportAlternativeBundle transportAlternativeBundle;

    public TransportAlternative withConstantId(){
        generateConstantId(transportAlternativeBundle, deliveryAddress);
        return this;
    }

}
