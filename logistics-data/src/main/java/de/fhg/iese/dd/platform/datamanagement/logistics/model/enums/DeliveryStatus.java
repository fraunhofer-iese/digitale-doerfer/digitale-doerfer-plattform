/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model.enums;

public enum DeliveryStatus {
    CREATED("Lieferung erstellt"),
    WAITING_FOR_ASSIGNMENT("Lieferung wartet auf Übernahme durch Lieferanten"),
    WAITING_FOR_PICKUP("Lieferung wartet auf Abholung"),
    IN_DELIVERY("Lieferung in Zustellung"),
    IN_DELIVERY_POOLING_STATION("Lieferung in Zustellung (Zwischenstation)"),
    POOLING_STATION_WAITING_FOR_ASSIGNMENT("Lieferung in Zustellung (Zwischenstation)"),
    POOLING_STATION_WAITING_FOR_PICKUP("Lieferung in Zustellung (Zwischenstation)"),
    DELIVERED("Lieferung zugestellt"),
    DELIVERED_POOLING_STATION("Lieferung zur Abholung in Station"),
    RECEIVED("Lieferung empfangen"),
    CANCELLED("Lieferung abgebrochen");

    private final String humanReadableString;

    DeliveryStatus(final String humanReadableString) {
        this.humanReadableString = humanReadableString;
    }

    public String getFriendlyHumanReadableString() {
        return humanReadableString;
    }

}
