/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.logistics.model.enums;

public enum BoxAllocationStatus {

    /**
     * The box has been reserved for putting a delivery in soon.
     * <p/>
     * The door is closed.
     */
    RESERVATION,

    /**
     * The delivery should be put in, we do not know if this has been done already.
     * <p/>
     * The door is open or going to be opened or has just been closed.
     */
    PENDING_IN,

    /**
     * The delivery was put in.
     * <p/>
     * The door is closed.
     */
    ACTIVE,

    /**
     * The delivery has been in and should be got out, we do not know if this has been done already.
     * <p/>
     * The door is open or going to be opened or has just been closed.
     */
    PENDING_OUT,

    /**
     * The delivery was in and is now taken out.
     * <p/>
     * The door is closed.
     */
    PAST
}
