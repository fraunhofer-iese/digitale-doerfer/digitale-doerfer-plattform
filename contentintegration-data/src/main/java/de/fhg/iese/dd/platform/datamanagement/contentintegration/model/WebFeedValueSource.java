/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Mher Ter-Tovmasyan, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.contentintegration.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
@ToString
public class WebFeedValueSource {

    /**
     * Name of the attribute that should be used
     */
    private String attribute;
    /**
     * Attribute with name "type" has to have this value
     */
    private String type;
    /**
     * Name of the child element that should be used
     */
    private String child;
    /**
     * Optional format string applied to the scraped raw value
     */
    private String format;
    /**
     * Optional list of regexes to extract the value from the raw value. First match wins. Is applied before format.
     */
    private List<FormattedRegex> regexes;

}
