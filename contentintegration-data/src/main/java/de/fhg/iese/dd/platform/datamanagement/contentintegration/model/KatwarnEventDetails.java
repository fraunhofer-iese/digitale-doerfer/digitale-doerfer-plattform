/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.contentintegration.model;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(indexes = {
        @Index(columnList = "eventCode", unique = true)
})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class KatwarnEventDetails extends BaseEntity {

    @Column(nullable = false)
    private String eventCode;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private MediaItem icon;

    public KatwarnEventDetails withConstantId() {

        super.generateConstantId(eventCode);
        return this;
    }

    @Override
    public String toString() {

        return "KatwarnEventDetails [" +
                "id='" + id + "', " +
                "eventCode='" + eventCode + "'" +
                ']';
    }

}
