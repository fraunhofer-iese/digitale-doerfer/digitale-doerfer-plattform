/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2021 - 2024 Steffen Hupp, Balthasar Weitzel, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.contentintegration.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.NewsSource;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.Organization;
import de.fhg.iese.dd.platform.datamanagement.grapevine.model.PostType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.encryption.EncryptionConverter;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonReader;
import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class CrawlingConfig extends BaseEntity {

    private static final ObjectWriter MAP_ENTRY_JSON_WRITER = defaultJsonWriter();
    private static final TypeReference<Map<String, List<WebFeedParameter>>> JSON_TYPEREF_NAME_TO_URL =
            new TypeReference<>() {
            };
    private static final ObjectReader STRING_MAP_READER = defaultJsonReader().forType(JSON_TYPEREF_NAME_TO_URL);

    @Column(nullable = false)
    private String name;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String description;

    @ManyToOne
    private NewsSource newsSource;

    @Column(length = 500, nullable = false)
    private String sourceUrl;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<GeoArea> geoAreas;

    @ManyToOne
    private Organization organization;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CrawlingAccessType accessType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PostType externalPostType;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String authenticationHeaderKey;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String authenticationHeaderValue;

    @Column
    @Convert(converter = EncryptionConverter.class)
    private String basicAuth;

    @Column(length = BaseEntity.MAXIMAL_TEXT_LENGTH)
    private String feedParametersJson;

    @Column(nullable = false)
    @Builder.Default
    private boolean commentsDisallowed = false;

    /**
     * If true, it was created by the api or was created by data-init and later modified via the api.
     */
    @Column(nullable = false)
    @Builder.Default
    private boolean apiManaged = false;

    public Map<String, List<WebFeedParameter>> getWebFeedParameters() {
        try {
            if (StringUtils.isNotEmpty(feedParametersJson)) {
                return STRING_MAP_READER.readValue(feedParametersJson);
            } else {
                return Collections.emptyMap();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    //only required for data init, not for the API
    @JsonProperty
    public void setWebFeedParameters(Map<String, List<WebFeedParameter>> parameters) {
        if (!CollectionUtils.isEmpty(parameters)) {
            try {
                feedParametersJson = MAP_ENTRY_JSON_WRITER.writeValueAsString(parameters);
            } catch (JsonProcessingException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    public String getBasicAuthUsername() {
        if (StringUtils.isNotBlank(basicAuth)) {
            return StringUtils.substringBefore(basicAuth, ":");
        }
        return "";
    }

    public String getBasicAuthPassword() {
        if (StringUtils.isNotBlank(basicAuth)) {
            return StringUtils.substringAfter(basicAuth, ":");
        }
        return "";
    }

    @Override
    public String toString() {
        return "CrawlingConfig [" +
                "id='" + id + "', " +
                "sourceUrl='" + sourceUrl + "', " +
                "newsSource='" + newsSource.getId() + "'" +
                "parametersJson='" + feedParametersJson + "'" +
                ']';
    }

}
