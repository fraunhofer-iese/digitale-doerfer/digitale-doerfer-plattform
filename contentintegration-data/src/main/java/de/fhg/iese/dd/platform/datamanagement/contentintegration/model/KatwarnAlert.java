/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.datamanagement.contentintegration.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@ToString
public class KatwarnAlert {

    @NotBlank
    private String version;
    @NotBlank
    private String id;
    @NotBlank
    private String incident;
    @NotBlank
    private String providerId;
    private String senderId;
    private List<String> references;
    @NotNull
    private KatwarnContentType contentType;
    @NotBlank
    private String eventCode;
    @NotNull
    private KatwarnSeverity severity;
    @NotNull
    private KatwarnMessageType msgType;
    private boolean notifiable;
    private String geometry;
    @NotNull
    private Long sent;
    @NotNull
    private KatwarnStatus status;
    private String note;
    @NotNull
    private Long effective;
    private Long expires;
    @NotBlank
    private String defaultLanguage;
    @NotNull
    private Map<String, KatwarnLanguageBlock> languages;

    @Getter
    @Setter
    @SuperBuilder
    @NoArgsConstructor
    @ToString
    public static class KatwarnLanguageBlock {

        @NotBlank
        private String issuer;
        @NotBlank
        private String subject;
        @NotBlank
        private String headline;
        private String eventType;
        private String description;
        private List<String> instruction;
        private String web;
        private String contact;
        private KatwarnLocality locality;

        @Getter
        @Setter
        @SuperBuilder
        @NoArgsConstructor
        @ToString
        public static class KatwarnLocality {

            private String prefix;
            private String suffix;
            @NotNull
            private List<String> names;

        }

    }

    public enum KatwarnContentType {
        WARNING,
        INFO,
        CLEAR
    }

    public enum KatwarnSeverity {
        MINOR,
        MODERATE,
        SEVERE,
        EXTREME
    }

    public enum KatwarnMessageType {
        ALERT,
        UPDATE,
        CANCEL,
        ACK,
        ERROR
    }

    public enum KatwarnStatus {
        ACTUAL,
        EXERCISE,
        SYSTEM,
        TEST
    }


}
