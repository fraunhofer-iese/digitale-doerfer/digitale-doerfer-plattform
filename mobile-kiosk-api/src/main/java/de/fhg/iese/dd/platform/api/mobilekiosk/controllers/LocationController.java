/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientRawLocationRecord;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.MobileKioskClientModelMapper;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.IRawLocationRecordService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingVehicleService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"mobile-kiosk/location"})
@Api(tags = {"mobile-kiosk.location"})
public class LocationController extends BaseController {

    @Autowired
    private IRawLocationRecordService rawLocationRecordService;
    @Autowired
    private MobileKioskClientModelMapper mobileKioskClientModelMapper;
    @Autowired
    private IRoleService roleService;
    @Autowired
    private ISellingVehicleService sellingVehicleService;

    @ApiOperation(value = "Get one raw location record",
            notes = "Identified by the location record id")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHORIZED,
            ClientExceptionType.UNSPECIFIED_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {LogisticsAdminUiRestrictedAdmin.class})
    @GetMapping("{id}")
    public ClientRawLocationRecord getRawLocationRecordById(
            @PathVariable String id) {

        Person person = getCurrentPersonNotNull();

        RawLocationRecord rawLocationRecord = rawLocationRecordService.findById(id);

        if (!roleService.hasRoleForEntity(person, LogisticsAdminUiRestrictedAdmin.class,
                rawLocationRecord.getVehicle().getTenant())) {
            throw new NotAuthorizedException();
        }

        return mobileKioskClientModelMapper.toClientRawLocationRecord(rawLocationRecord);
    }

    @ApiOperation(value = "Get all raw location records for the given vehicle id between the start and end timestamps",
            notes = "The start and end timestamps are inclusive")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHORIZED,
            ClientExceptionType.SELLING_VEHICLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {LogisticsAdminUiRestrictedAdmin.class})
    @GetMapping
    public List<ClientRawLocationRecord> getRawLocationRecords(
            @RequestParam String vehicleId,
            @RequestParam long start,
            @RequestParam long end) {

        Person person = getCurrentPersonNotNull();

        SellingVehicle vehicle = sellingVehicleService.findById(vehicleId);

        if (!roleService.hasRoleForEntity(person, LogisticsAdminUiRestrictedAdmin.class, vehicle.getTenant())) {
            throw new NotAuthorizedException();
        }

        return rawLocationRecordService.findAllForVehicleIdAndTimestampBetween(vehicleId, start, end)
                .stream()
                .map(mobileKioskClientModelMapper::toClientRawLocationRecord)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Get the latest raw location record for the given vehicle id")
    @ApiExceptions({
            ClientExceptionType.VEHICLE_NOT_IN_OPERATING_HOURS,
            ClientExceptionType.SELLING_VEHICLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("latest")
    public ClientRawLocationRecord getLatestRawLocationRecord(
            @RequestParam String vehicleId) {

        SellingVehicle vehicle = sellingVehicleService.findById(vehicleId);

        return mobileKioskClientModelMapper
                .toClientRawLocationRecord(rawLocationRecordService.findLatestByVehicleId(vehicle.getId()));
    }

}
