/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.logistics.controllers.LogisticsAdminUIController;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiAdmin;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.MobileKioskAdminRepository;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.repos.results.DeliveryAndSellingPointAdminUIRow;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"mobile-kiosk/adminui"})
@Api(tags = {"mobile-kiosk.adminui"})
@IgnoreArchitectureViolation(
        value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
        reason = "Legacy implementation")
public class MobileKioskAdminUIController extends BaseController {

    @Autowired
    private ITenantService tenantService;

    @Autowired
    private MobileKioskAdminRepository mobileKioskAdminRepository;

    @ApiOperation(value = "Gets all open deliveries with their selling point (if available) for a given tenant " +
            "within a specific time range",
            notes = "Newest delivery is listed first")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {LogisticsAdminUiRestrictedAdmin.class, LogisticsAdminUiAdmin.class},
            notes = "VG admins get a censored view on the data")
    @GetMapping("/delivery")
    public List<DeliveryAndSellingPointAdminUIRow> getAllDeliveries(
            @RequestParam String communityId,
            @RequestParam long fromTime,
            @RequestParam long toTime
    ) {

        Person currentPerson = getCurrentPersonNotNull();
        Tenant tenant = tenantService.findTenantById(communityId);

        boolean authorizedCensored =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiRestrictedAdmin.class, tenant);
        boolean authorizedDetail =
                getRoleService().hasRoleForEntity(currentPerson, LogisticsAdminUiAdmin.class, tenant);

        if (!authorizedCensored && !authorizedDetail) {
            throw new NotAuthorizedException("Person must be VG admin or IESE admin of the requested " +
                    "community to call this endpoint");
        }

        List<DeliveryAndSellingPointAdminUIRow> result = mobileKioskAdminRepository
                .findDeliveriesAndSellingPointsInCommunityCreatedFromToOrderByCreatedDesc(tenant.getId(),
                        fromTime, toTime);

        if (!authorizedDetail) {
            result.forEach(LogisticsAdminUIController::censorDeliveryAdminUIRow);
        }

        return result;
    }

}
