/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientLocationRecordsReceivedEvent;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.LocationRecordsReceivedConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.LocationRecordsReceivedEvent;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingVehicleService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.enums.LocationSourceType;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping({"mobile-kiosk/location/event"})
@Api(tags = {"mobile-kiosk.location.events"})
public class LocationEventController extends BaseController {

    @Autowired
    private ISellingVehicleService sellingVehicleService;
    @Autowired
    private IRoleService roleService;

    @ApiOperation(value = "Receives a ClientLocationRecordsReceivedEvent from a driver",
            notes = """
                    only drivers for the given vehicle id can send the request.
                    All invalid location records will be ignored.
                    Required fields in the event:\s
                    vehicleId, locationRecords.timestamp, locationRecords.location
                    Accuracy is an optional double between 0 and 1""")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHORIZED,
            ClientExceptionType.SELLING_VEHICLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {SellingVehicleDriver.class})
    @PostMapping("locationRecordsReceivedEvent")
    public void onClientLocationRecordsReceivedEvent(
            @Valid @RequestBody ClientLocationRecordsReceivedEvent clientEvent) {

        Person person = getCurrentPersonNotNull();

        SellingVehicle vehicle = sellingVehicleService.findById(clientEvent.getVehicleId());
        if (!roleService.hasRoleForEntity(person, SellingVehicleDriver.class, vehicle)) {
            throw new NotAuthorizedException();
        }

        List<RawLocationRecord> locationRecords = clientEvent.getLocationRecords()
                .stream()
                .filter(lr -> lr.getLocation() != null && lr.getTimestamp() > 0)
                .map(lr -> RawLocationRecord
                        .builder()
                        .accuracy(lr.getAccuracy())
                        .timestamp(lr.getTimestamp())
                        .vehicle(vehicle)
                        .location(lr.getLocation() == null ? null :
                                new GPSLocation(lr.getLocation().getLatitude(),
                                        lr.getLocation().getLongitude()))
                        .sourceType(LocationSourceType.DRIVER_APP)
                        .sourceId(person.getId())
                        .build())
                .collect(Collectors.toList());

        LocationRecordsReceivedEvent event = LocationRecordsReceivedEvent.builder()
                .locationRecords(locationRecords)
                .build();
        notifyAndWaitForReply(LocationRecordsReceivedConfirmation.class, event);
    }

}
