/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2018 Balthasar Weitzel, Johannes Schneider, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientSellingPoint;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientSellingPointWithTransports;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.MobileKioskClientModelMapper;
import de.fhg.iese.dd.platform.business.mobilekiosk.model.SellingPointWithTransportAssignments;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingPointService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingVehicle;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.roles.SellingVehicleDriver;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"mobile-kiosk/sellingPoint"})
@Api(tags = {"mobile-kiosk.sellingpoint"})
public class SellingPointController extends BaseController {

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private ISellingPointService sellingPointService;
    @Autowired
    private MobileKioskClientModelMapper mobileKioskClientModelMapper;

    @ApiOperation(value = "Gets all available selling points now or in the future",
            notes = "Ordered by arrival time")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping
    public List<ClientSellingPoint> listActiveCurrentOrFutureSellingPoints(
            @RequestParam String communityId) {

        final Tenant tenant = tenantService.findTenantById(communityId);

        return sellingPointService.findAllActiveFutureOrCurrentSellingPointsOrderedByPlannedArrivalAsc(tenant)
                .stream()
                .map(mobileKioskClientModelMapper::toClientSellingPoint)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Gets one selling point",
            notes = "Identified by the selling point id")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value="/{sellingPointId}")
    public ClientSellingPoint getSellingPointById(
            @PathVariable String sellingPointId) {

        return mobileKioskClientModelMapper.toClientSellingPoint(sellingPointService.findById(sellingPointId));
    }

    @ApiOperation(value = "Gets all selling points available for order",
            notes = "Ordered by arrival time")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/availableForOrder")
    public List<ClientSellingPoint> listAvailableForOrderSellingPoints(
            @RequestParam String communityId) {

        final Tenant tenant = tenantService.findTenantById(communityId);

        return sellingPointService.getSellingPointsAvailableForOrder(tenant)
                .stream()
                .map(mobileKioskClientModelMapper::toClientSellingPoint)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Gets all selling points today (including already past ones)",
            notes = "Ordered by arrival time")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/today")
    public List<ClientSellingPoint> listTodaysSellingPoints(
            @RequestParam String communityId) {

        final Tenant tenant = tenantService.findTenantById(communityId);

        return sellingPointService.getTodaysSellingPoints(tenant)
                .stream()
                .map(mobileKioskClientModelMapper::toClientSellingPoint)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Gets all future selling points with their respective transports",
            notes = "Ordered by arrival time. Only shows a selling point if it is hit by a selling vehicle which" +
                    "is driven by the person making this request. Shows all selling points with transports for super admins.")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class, SellingVehicleDriver.class},
            notes = "Selling vehicle drivers get a censored view on the data")
    @GetMapping("/withTransports")
    public List<ClientSellingPointWithTransports> listSellingPointsWithTransports(
            @RequestParam String communityId) {

        final Person person = getCurrentPersonNotNull();
        final Tenant tenant = tenantService.findTenantById(communityId);

        return sellingPointService.getSellingPointsWithTransportAssignments(tenant)
                .stream()
                .filter(allForSuperAdminOnlyAssignedVehiclesForDriver(tenant, person))
                .map(mobileKioskClientModelMapper::toClientSellingPointWithTransports)
                .collect(Collectors.toList());
    }

    @ApiOperation(
            value = "Gets all selling points on the given relative day and the following days with their respective transports",
            notes = "Ordered by arrival time. Only shows a selling point if it is hit by a selling vehicle which " +
                    "is driven by the person making this request.\nRelativeDate: 0 is today, -1 yesterday, 1 tomorrow etc. " +
                    "A value days = 1 means only the specified day, everything above also the respective following days. " +
                    "Shows all selling points with transports for super admins.")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class, SellingVehicleDriver.class},
            notes = "Selling vehicle drivers get a censored view on the data")
    @GetMapping("/withTransports/relativeDate")
    public List<ClientSellingPointWithTransports> listSellingPointsWithTransportsOnRelativeDate(
            @RequestParam String communityId,
            @RequestParam(required = false, defaultValue = "0") int relativeDate,
            @RequestParam(required = false, defaultValue = "1") int days) {

        final Person person = getCurrentPersonNotNull();
        final Tenant tenant = tenantService.findTenantById(communityId);

        if (days < 1) {
            days = 1;
        }

        return sellingPointService.getSellingPointsWithTransportAssignments(tenant, relativeDate, days)
                .stream()
                .filter(allForSuperAdminOnlyAssignedVehiclesForDriver(tenant, person))
                .map(mobileKioskClientModelMapper::toClientSellingPointWithTransports)
                .collect(Collectors.toList());
    }

    private Predicate<SellingPointWithTransportAssignments> allForSuperAdminOnlyAssignedVehiclesForDriver(Tenant tenant,
            Person person) {
        Predicate<SellingPointWithTransportAssignments> filter;
        if (getRoleService().hasRoleForAnyEntity(person, Collections.singleton(SuperAdmin.class))) {
            filter = spta -> true;
        } else {
            final Set<String> vehicleIds = getRoleService().getCurrentExtendedRoleAssignments(person, tenant)
                    .stream()
                    .filter(ra -> ra.getRole().equals(SellingVehicleDriver.class))
                    .map(RoleAssignment::getRelatedEntityId)
                    .collect(Collectors.toSet());

            filter = spta -> vehicleIds.contains(spta.getSellingPoint().getVehicle().getId());
        }
        return filter;
    }

    @ApiOperation(value = "Gets a selling point with its respective transports",
            notes = "Must be called by the driver of the vehicle delivering the selling point or a super admin.")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHORIZED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class, SellingVehicleDriver.class},
            notes = "Selling vehicle drivers get a censored view on the data")
    @GetMapping("/withTransports/{id}")
    public ClientSellingPointWithTransports getSellingPointWithTransportsById(
            @PathVariable String id) {

        final Person person = getCurrentPersonNotNull();
        final SellingPointWithTransportAssignments sellingPointWithTransportAssignment =
                sellingPointService.getSellingPointWithTransportAssignmentsById(id);
        final SellingVehicle vehicle = sellingPointWithTransportAssignment.getSellingPoint().getVehicle();
        if (!getRoleService().hasRoleForAnyEntity(person, Collections.singleton(SuperAdmin.class))
                || !getRoleService().hasRoleForEntity(person, SellingVehicleDriver.class, vehicle)) {
            throw new NotAuthorizedException("Person must be super admin or vehicle driver to this selling point");
        }

        return mobileKioskClientModelMapper.toClientSellingPointWithTransports(sellingPointWithTransportAssignment);
    }

}
