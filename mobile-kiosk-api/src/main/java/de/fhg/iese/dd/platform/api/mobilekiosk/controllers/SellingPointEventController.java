/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientConnectDeliveryWithSellingPointResponse;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointForDeliveryRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointForDeliveryResponse;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointsRequest;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientevent.ClientUpdateSellingPointsResponse;
import de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel.ClientUpdatedSellingPoint;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.business.logistics.services.IDeliveryService;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.ConnectDeliveryWithSellingPointRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.DeliveryConnectedWithSellingPointConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointForDeliveryRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointsConfirmation;
import de.fhg.iese.dd.platform.business.mobilekiosk.events.UpdateSellingPointsRequest;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingPointService;
import de.fhg.iese.dd.platform.business.mobilekiosk.services.ISellingVehicleService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthenticatedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.logistics.model.Delivery;
import de.fhg.iese.dd.platform.datamanagement.logistics.roles.LogisticsAdminUiRestrictedAdmin;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.config.MobileKioskConfig;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPointStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.TimeSpan;
import de.fhg.iese.dd.platform.datamanagement.shopping.ShoppingConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"mobile-kiosk/event"})
@Api(tags = {"mobile-kiosk.events"})
public class SellingPointEventController extends BaseController {

    @FunctionalInterface
    private interface AuthorisationChecker {

        void check(Tenant tenant) throws NotAuthenticatedException, NotAuthorizedException;

    }

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private ISellingPointService sellingPointService;
    @Autowired
    private ISellingVehicleService sellingVehicleService;
    @Autowired
    private IDeliveryService deliveryService;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    @Autowired
    private MobileKioskConfig config;

    @ApiOperation(value = "Receives a ClientUpdateSellingPointsRequest.")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.SELLING_VEHICLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {LogisticsAdminUiRestrictedAdmin.class})
    @PostMapping("/updateSellingPointsRequest")
    public ClientUpdateSellingPointsResponse updateSellingPoints(
            @Valid @RequestBody ClientUpdateSellingPointsRequest clientUpdateSellingPointsRequest) {

        return doUpdate(clientUpdateSellingPointsRequest, this::checkThatCurrentPersonIsVgAdminForTenant);
    }

    @ApiOperation(value = "Receives a ClientUpdateSellingPointsRequest.",
            notes = "Caller must authenticate via apiKey.")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.SELLING_VEHICLE_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            notes = "API key is configured per tenant in mobilekiosk.api-key-by-tenant-id")
    @PostMapping("/apiKey/updateSellingPointsRequest")
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Legacy code")
    public ClientUpdateSellingPointsResponse updateSellingPointsApiKey(
            @RequestHeader(name = HEADER_NAME_API_KEY) String apiKey,
            @Valid @RequestBody ClientUpdateSellingPointsRequest clientUpdateSellingPointsRequest) {

        return doUpdate(clientUpdateSellingPointsRequest, tenant -> {
            if (!config.getApiKeyByTenantId().get(clientUpdateSellingPointsRequest.getCommunityId()).equals(
                    apiKey)) {
                throw new NotAuthenticatedException();
            }
        });
    }

    private ClientUpdateSellingPointsResponse doUpdate(
            ClientUpdateSellingPointsRequest clientUpdateSellingPointsRequest, AuthorisationChecker checker) {

        Tenant tenant = tenantService.findTenantById(clientUpdateSellingPointsRequest.getCommunityId());
        checker.check(tenant);

        List<SellingPoint> sellingPoints = new ArrayList<>();
        for (ClientUpdatedSellingPoint clientUpdatedSellingPoint : clientUpdateSellingPointsRequest.getSellingPoints()) {
            checkValidSellingPoint(clientUpdatedSellingPoint);
            sellingPoints.add(SellingPoint.builder()
                    .community(tenant)
                    .vehicle(sellingVehicleService.findAndUpdateOrCreate(clientUpdatedSellingPoint.getVehicleId(),
                            clientUpdatedSellingPoint.getVehicleName(), tenant))
                    .plannedStay(new TimeSpan(clientUpdatedSellingPoint.getPlannedArrival(),
                            clientUpdatedSellingPoint.getPlannedDeparture()))
                    .location(addressService.findOrCreateAddress(
                            addressClientModelMapper.toAddressDefinition(clientUpdatedSellingPoint.getLocation()),
                            IAddressService.AddressFindStrategy.NAME_STREET_ZIP_CITY_GPS,
                            IAddressService.GPSResolutionStrategy.PROVIDED_LOOKEDUP_NONE,
                            false))
                    .status(SellingPointStatus.ACTIVE)
                    .build());
        }

        UpdateSellingPointsRequest request = new UpdateSellingPointsRequest(tenant, sellingPoints);
        notifyAndWaitForReply(UpdateSellingPointsConfirmation.class, request);
        return new ClientUpdateSellingPointsResponse(true);
    }

    private void checkValidSellingPoint(ClientUpdatedSellingPoint sellingPoint) {

        if (sellingPoint.getPlannedArrival() >= sellingPoint.getPlannedDeparture()) {
            throw new InvalidEventAttributeException("Arrival is after departure");
        }
        if (sellingPoint.getLocation() == null ||
                Strings.isNullOrEmpty(sellingPoint.getLocation().getCity()) ||
                Strings.isNullOrEmpty(sellingPoint.getLocation().getZip()) ||
                Strings.isNullOrEmpty(sellingPoint.getLocation().getStreet()) ||
                Strings.isNullOrEmpty(sellingPoint.getLocation().getName())) {
            throw new InvalidEventAttributeException("Location is invalid");
        }
        if (Strings.isNullOrEmpty(sellingPoint.getVehicleId())) {
            throw new InvalidEventAttributeException("Vehicle is invalid");
        }
    }

    @ApiOperation(value = "Receives a ClientConnectDeliveryWithSellingPointRequest.")
    @ApiExceptions({
            ClientExceptionType.SELLING_POINT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "API key is configured in BestellBar AppVariant")
    @PostMapping("/apiKey/connectDeliveryWithSellingPointRequest")
    public ClientConnectDeliveryWithSellingPointResponse connectDeliveryWithSellingPointApiKey(
            @Valid @RequestBody
                    ClientConnectDeliveryWithSellingPointRequest clientConnectDeliveryWithSellingPointRequest) {

        Delivery delivery =
                deliveryService.findDeliveryById(clientConnectDeliveryWithSellingPointRequest.getDeliveryId());

        validateAuthenticatedAppVariantAgainstAppAndTenant(
                getAppService().findById(ShoppingConstants.BESTELLBAR_APP_ID),
                delivery.getTenant());

        SellingPoint sellingPoint =
                sellingPointService.findById(clientConnectDeliveryWithSellingPointRequest.getSellingPointId());

        ConnectDeliveryWithSellingPointRequest request = ConnectDeliveryWithSellingPointRequest.builder()
                .delivery(delivery)
                .sellingPoint(sellingPoint)
                .build();
        notifyAndWaitForReply(DeliveryConnectedWithSellingPointConfirmation.class, request);
        return ClientConnectDeliveryWithSellingPointResponse.builder()
                .deliveryId(clientConnectDeliveryWithSellingPointRequest.getDeliveryId())
                .sellingPointId(clientConnectDeliveryWithSellingPointRequest.getSellingPointId())
                .build();
    }

    @ApiOperation(value = "Receives a ClientUpdateSellingPointForDeliveryRequest.",
            notes = "Calling person must have role LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN for the corresponding tenant.")
    @ApiExceptions({
            ClientExceptionType.SELLING_POINT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {LogisticsAdminUiRestrictedAdmin.class})
    @PostMapping("/updateSellingPointForDeliveryRequest")
    public ClientUpdateSellingPointForDeliveryResponse updateSellingPointForDelivery(
            @Valid @RequestBody ClientUpdateSellingPointForDeliveryRequest clientUpdateSellingPointForDeliveryRequest) {

        return doUpdateSellingPointForDelivery(clientUpdateSellingPointForDeliveryRequest,
                this::checkThatCurrentPersonIsVgAdminForTenant, getCurrentPersonNotNull());
    }

    @ApiOperation(value = "Receives a ClientUpdateSellingPointForDeliveryRequest.",
            notes = "Caller must authenticate via apiKey.")
    @ApiExceptions({
            ClientExceptionType.SELLING_POINT_NOT_FOUND,
            ClientExceptionType.DELIVERY_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION,
            notes = "API key is configured in BestellBar AppVariant")
    @PostMapping("/apiKey/updateSellingPointForDeliveryRequest")
    public ClientUpdateSellingPointForDeliveryResponse updateSellingPointForDeliveryApiKey(
            @Valid @RequestBody ClientUpdateSellingPointForDeliveryRequest clientUpdateSellingPointForDeliveryRequest) {

        return doUpdateSellingPointForDelivery(clientUpdateSellingPointForDeliveryRequest, community ->
                validateAuthenticatedAppVariantAgainstAppAndTenant(
                        getAppService().findById(ShoppingConstants.BESTELLBAR_APP_ID),
                        deliveryService.findDeliveryById(clientUpdateSellingPointForDeliveryRequest.getDeliveryId())
                                .getTenant()), null);
    }

    private ClientUpdateSellingPointForDeliveryResponse doUpdateSellingPointForDelivery(
            ClientUpdateSellingPointForDeliveryRequest clientUpdateSellingPointForDeliveryRequest,
            AuthorisationChecker checker,
            Person admin) {

        SellingPoint sellingPoint =
                sellingPointService.findById(clientUpdateSellingPointForDeliveryRequest.getSellingPointId());
        Delivery delivery =
                deliveryService.findDeliveryById(clientUpdateSellingPointForDeliveryRequest.getDeliveryId());

        if (!sellingPoint.getTenant().equals(delivery.getTenant())) {
            throw new InvalidEventAttributeException("Selling point and delivery must belong to the same tenant.");
        }
        checker.check(sellingPoint.getTenant());

        UpdateSellingPointForDeliveryRequest request = UpdateSellingPointForDeliveryRequest.builder()
                .delivery(delivery)
                .sellingPoint(sellingPoint)
                .admin(admin)
                .build();
        notifyAndWaitForReply(DeliveryConnectedWithSellingPointConfirmation.class, request);
        return ClientUpdateSellingPointForDeliveryResponse.builder()
                .deliveryId(clientUpdateSellingPointForDeliveryRequest.getDeliveryId())
                .sellingPointId(clientUpdateSellingPointForDeliveryRequest.getSellingPointId())
                .build();
    }

    private void checkThatCurrentPersonIsVgAdminForTenant(Tenant tenant) {
        if (!getRoleService().hasRoleForEntity(getCurrentPersonNotNull(), LogisticsAdminUiRestrictedAdmin.class,
                tenant)) {
            throw new NotAuthorizedException(
                    "Person must be LOGISTICS_ADMIN_UI_RESTRICTED_ADMIN for tenant " + tenant.getName()
                            + " to call this method");
        }
    }

}
