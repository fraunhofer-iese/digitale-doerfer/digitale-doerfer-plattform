/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.mobilekiosk.clientmodel;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.logistics.clientmodel.LogisticsClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.business.mobilekiosk.model.SellingPointWithTransportAssignments;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.RawLocationRecord;
import de.fhg.iese.dd.platform.datamanagement.mobilekiosk.model.SellingPoint;

@Component
public class MobileKioskClientModelMapper {

    @Autowired
    private LogisticsClientModelMapper logisticsClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    public ClientSellingPoint toClientSellingPoint(SellingPoint sellingPoint) {
        if (sellingPoint == null) {
            return null;
        }
        return ClientSellingPoint.builder()
                .id(sellingPoint.getId())
                .communityId(sellingPoint.getTenant().getId())
                .vehicleId(sellingPoint.getVehicle().getId())
                .vehicleName(sellingPoint.getVehicle().getName())
                .location(addressClientModelMapper.toClientAddress(sellingPoint.getLocation()))
                .plannedArrival(sellingPoint.getPlannedStay().getStartTime())
                .plannedDeparture(sellingPoint.getPlannedStay().getEndTime())
                .status(sellingPoint.getStatus())
                .build();
    }
    
    public ClientSellingPointWithTransports toClientSellingPointWithTransports(SellingPointWithTransportAssignments sellingPointWithTransportAssignments) {
        return ClientSellingPointWithTransports.builder()
                .id(sellingPointWithTransportAssignments.getSellingPoint().getId())
                .sellingPoint(toClientSellingPoint(sellingPointWithTransportAssignments.getSellingPoint()))
                .transports(sellingPointWithTransportAssignments.getTransportAssignments()
                        .stream()
                        .map(logisticsClientModelMapper::createClientTransportFromTransportAssignment)
                        .collect(Collectors.toSet()))
                .build();
    }

    public ClientRawLocationRecord toClientRawLocationRecord(RawLocationRecord rawLocationRecord){
        if(rawLocationRecord == null) return null;
        return ClientRawLocationRecord.builder()
                .id(rawLocationRecord.getId())
                .accuracy(rawLocationRecord.getAccuracy())
                //@NonNull location -> cannot be null
                .latitude(rawLocationRecord.getLocation().getLatitude())
                .longitude(rawLocationRecord.getLocation().getLongitude())
                .vehicleId(rawLocationRecord.getVehicle().getId())
                .timestamp(rawLocationRecord.getTimestamp())
                .sourceType(rawLocationRecord.getSourceType())
                .sourceId(rawLocationRecord.getSourceId())
                .build();
    }

}
