/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.processors;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.motivation.clientevent.ClientAchievementAchievedEvent;
import de.fhg.iese.dd.platform.api.motivation.clientevent.ClientMultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.api.motivation.clientevent.ClientScoreChangedEvent;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.ClientAccountEntry;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.ClientAchievementLevel;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.MotivationClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.MultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.ScoreChangedEvent;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.motivation.MotivationConstants;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.TenantAccount;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class MotivationPushEventProcessor extends BaseEventProcessor {

    @Autowired
    private IClientPushService clientPushService;

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Autowired
    private MotivationClientModelMapper modelMapper;

    @PushEvent(clientEvent = ClientAchievementAchievedEvent.class,
            categorySubject = MotivationConstants.PUSH_CATEGORY_SUBJECT,
            notes = "Sent to the person that received the achievement, if the achievement was not part of a multi achievement.")
    @PushEvent(clientEvent = ClientAchievementAchievedEvent.class,
            categorySubject = MotivationConstants.PUSH_CATEGORY_SUBJECT,
            loud = false,
            notes = "Sent to the person that received the achievement if the achievement was not part of a multi achievement.")
    @EventProcessing
    private void handleAchievementAchievedEvent(AchievementAchievedEvent event) {
        AchievementPersonPairing achievedLevel = event.getAchievedLevel();
        AchievementLevel achievementLevel = achievedLevel.getAchievementLevel();
        Achievement achievement = achievementLevel.getAchievement();
        Person achiever = achievedLevel.getPerson();

        ClientAchievementLevel clientAchievementLevel = modelMapper.toClientAchievementLevel(
                achievementLevel,
                true,
                achievedLevel.getTimestamp());

        ClientAchievementAchievedEvent clientAchievementAchievedEvent = new ClientAchievementAchievedEvent(
                clientAchievementLevel,
                achievement.getId());

        String message = "Hallo "+achiever.getFirstName()+", du hast das Achievement \""+achievementLevel.getName()+"\" erhalten!";

        //get the push category of the achievement, if it is specific for an app, otherwise it is null
        PushCategory pushCategory = achievement.getPushCategory();

        if(event.isPartOfMultiAchievement()){
            //do not push the single achievements loud if multiple were achieved
            if (pushCategory != null) {
                clientPushService.pushToPersonSilent(achiever, clientAchievementAchievedEvent, pushCategory);
            } else {
                clientPushService.pushToPersonSilent(achiever, clientAchievementAchievedEvent,
                        getAllMotivationCategories());
            }
        } else {
            pushEvent(achiever, clientAchievementAchievedEvent, message, pushCategory);
        }
    }

    @PushEvent(clientEvent = ClientMultiAchievementAchievedEvent.class,
            categorySubject = MotivationConstants.PUSH_CATEGORY_SUBJECT,
            notes = "Sent to the person that received several achievement at ones, a so called multi achievement.")
    @EventProcessing
    private void handleMultiAchievementAchievedEvent(MultiAchievementAchievedEvent event) {

        Collection<AchievementPersonPairing> achievedLevels = event.getAchievedLevels();
        Person achiever = event.getAchiever();

        List<ClientAchievementLevel> clientAchievementLevels = achievedLevels.stream()
                .filter(Objects::nonNull)
                .map(achievedLevel -> modelMapper.toClientAchievementLevel(
                        achievedLevel.getAchievementLevel(),
                        true,
                        achievedLevel.getTimestamp()))
                .collect(Collectors.toList());

        Collection<PushCategory> pushCategories = achievedLevels.stream()
                .filter(Objects::nonNull)
                .map(a -> a.getAchievementLevel().getAchievement())
                .map(Achievement::getPushCategory)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        //just take any of the categories and push the event there
        //normally it should be all the same category
        PushCategory pushCategory = pushCategories.stream()
                .findAny()
                //if there are no categories, use null, so that the event gets pushed to the last recently used app variant
                .orElse(null);

        ClientMultiAchievementAchievedEvent clientMultiAchievementAchievedEvent =
                new ClientMultiAchievementAchievedEvent(clientAchievementLevels);

        String message = String.format("Hallo %s, du hast %d Achievements erhalten!", achiever.getFirstName(),
                clientAchievementLevels.size());

        pushEvent(achiever, clientMultiAchievementAchievedEvent, message, pushCategory);
    }

    @PushEvent(clientEvent = ClientScoreChangedEvent.class,
            categorySubject = MotivationConstants.PUSH_CATEGORY_SUBJECT,
            notes = "Sent to the person that got DigiTaler added or removed from their account.")
    @PushEvent(clientEvent = ClientScoreChangedEvent.class,
            categorySubject = MotivationConstants.PUSH_CATEGORY_SUBJECT,
            loud = false,
            notes = "Sent to the person that got DigiTaler reserved from their account.")
    @EventProcessing
    private void handleScoreChangedEvent(ScoreChangedEvent event) {
        Account account = event.getAccount();
        Person accountOwner = null;
        if (account instanceof PersonAccount) {
            accountOwner = ((PersonAccount) account).getOwner();
        }
        if (account instanceof TenantAccount) {
            //currently we do not know to whom to push to. Might be a group of persons in the future
            return;
        }
        if (accountOwner == null) {
            log.warn("Could not determine account owner of account {} ", account);
            return;
        }
        AccountEntry accountEntry = event.getAccountEntry();
        int amount = accountEntry.getAmount();
        PushCategory pushCategory = event.getPushCategory();

        ClientAccountEntry clientAccountEntry = modelMapper.toClientAccountEntry(accountEntry);
        ClientScoreChangedEvent
                clientScoreChangedEvent = new ClientScoreChangedEvent(accountOwner.getId(), clientAccountEntry, account.getBalance());

        switch (accountEntry.getAccountEntryType()) {
            case MINUS: {
                String message = String.format("Hallo %s, Dir wurden soeben %d DigiTaler abgebucht.",
                        accountOwner.getFirstName(), amount);
                pushEvent(accountOwner, clientScoreChangedEvent, message, pushCategory);
                break;
            }
            case PLUS: {
                String message = String.format("Herzlichen Glückwunsch %s, Du hast soeben %d DigiTaler erhalten.",
                        accountOwner.getFirstName(), amount);
                pushEvent(accountOwner, clientScoreChangedEvent, message, pushCategory);
                break;
            }
            case RESERVATION_MINUS:
            case RESERVATION_PLUS:
                clientPushService.pushToPersonSilent(accountOwner, clientScoreChangedEvent, pushCategory);
                break;
            default:
                throw new RuntimeException("Unknown Account Entry Type!");
        }

    }

    private void pushEvent(Person person, ClientBaseEvent pushEvent, String message, PushCategory pushCategory){
        if(pushCategory != null) {
            //if we have a valid category, push it to that category loud and to all others silent
            Set<PushCategory> silentCategories = getAllMotivationCategories().stream()
                    .filter(category -> !pushCategory.equals(category))
                    .collect(Collectors.toSet());
            clientPushService.pushToPersonLoud(person, pushEvent, message, pushCategory);
            clientPushService.pushToPersonSilent(person, pushEvent, silentCategories);

        }else{
            //the provided category is not valid, push it to all motivation categories
            clientPushService.pushToPersonInLastUsedAppVariantLoud(person,
                    IClientPushService.PushSendRequestMultipleCategories.builder()
                            .event(pushEvent)
                            .message(message)
                            .categories(getAllMotivationCategories())
                            .build());
        }
    }

    private Collection<PushCategory> getAllMotivationCategories(){
        return pushCategoryService.findPushCategoriesBySubject(MotivationConstants.PUSH_CATEGORY_SUBJECT);
    }

}
