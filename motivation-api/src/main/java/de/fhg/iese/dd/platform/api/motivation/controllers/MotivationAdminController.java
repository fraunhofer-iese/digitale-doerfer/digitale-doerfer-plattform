/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.motivation.events.AchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.events.CheckAchievementAchievedRequest;
import de.fhg.iese.dd.platform.business.motivation.events.MultiAchievementAchievedEvent;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IAchievementService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/administration/motivation")
@Api(tags = {"admin","motivation.admin"})
public class MotivationAdminController extends BaseController {

    @Autowired
    private IPersonService personService;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IAchievementService achievementService;

    @ApiOperation(value = "Check Achievements",
            notes = "Check if achievements have been achieved. "
                    +
                    "This is only required if new achievements have been added and the events that trigger these achievements will not occur anymore.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("achievement/checkAchievements")
    public void checkAchievements(
            @RequestParam(required = false) List<String> personIdsToCheck,
            @RequestParam(defaultValue="false") boolean checkAllPersons,
            @RequestParam(required = false) List<String> achievementRulesToCheck,
            @RequestParam(defaultValue="true") boolean checkAllAchievementRules) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        List<Person> personsToCheck = null;
        if(personIdsToCheck != null){
           personsToCheck = personService.findAllPersonsById(personIdsToCheck);
        }
        Set<String> achievementRulesToCheckSet = null;
        if(achievementRulesToCheck != null){
            achievementRulesToCheckSet = new HashSet<>(achievementRulesToCheck);
        }
        notify(new CheckAchievementAchievedRequest(personsToCheck,checkAllPersons, achievementRulesToCheckSet, checkAllAchievementRules));
    }

    @ApiOperation(value = "Achieve all Achievements",
            notes = "Achieve all achievements for the given person")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("achievement/achieveAll")
    public void achieveAllAchievements(
            @RequestParam String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        Person person = personService.findPersonById(personId);

        List<AchievementPersonPairing> achievedLevels = achievementService.achieveAllLevels(person);

        boolean isMultiAchievement = achievedLevels.size() > 1;
        achievedLevels
            .forEach(achievedAchievementLevel -> notify(new AchievementAchievedEvent(achievedAchievementLevel, isMultiAchievement)));
        if(isMultiAchievement){
            notify(new MultiAchievementAchievedEvent(person, achievedLevels));
        }
        log.info("achieved all achievements for person {}", personId);
    }

    @ApiOperation(value = "Remove all Achievements",
            notes = "Unachive all achievements of a given person")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping("achievement/unachieveAll")
    public void unachieveAllAchievements(
            @RequestParam String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        Person person = personService.findPersonById(personId);

        achievementService.unachieveAllLevels(person);
        log.info("unachieved all achievements for person {}", personId);
    }
    
    @ApiOperation(value = "Check all account balances for inconsistencies with their entries",
            notes = "Recalculates all balances and logs inconsistencies without modifying anything.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping("account/check")
    public String checkScores() {
        
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        int difference = accountService.recalculateAllAccountBalances(false);
        if (difference == 0) {
            return "All account balances are valid";
        } else {
            return "There are inconsistencies regarding the accounts balance, see logs for details.";
        }
    }

    @ApiOperation(value = "Recalculates all account balances",
            notes = "Recalculates all balances, updates the balances and logs inconsistencies.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping("account/recalculate")
    public String recalculateScores() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        int difference = accountService.recalculateAllAccountBalances(true);
        if (difference == 0) {
            return "All account balances are valid";
        } else {
            return "There are inconsistencies regarding the accounts balance, see logs for details. " +
                    "These inconsistencies were fixed with a summed up absolute balance change of " + difference;
        }
    }

}
