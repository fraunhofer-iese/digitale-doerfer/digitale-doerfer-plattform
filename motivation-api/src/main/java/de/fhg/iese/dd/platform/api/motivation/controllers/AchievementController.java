/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.ClientAchievement;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.ClientAchievementLevel;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.MotivationClientModelMapper;
import de.fhg.iese.dd.platform.business.motivation.services.IAchievementService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementPersonPairing;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"score/achievement"})
@Api(tags = {"motivation.achievement"})
public class AchievementController extends BaseController {

    @Autowired
    private MotivationClientModelMapper motivationClientModelMapper;

    @Autowired
    private IAchievementService achievementService;

    @ApiOperation(value = "Get all available and achieved achievements",
            notes = "All available achievements for the user and those that are achieved are marked as achieved.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("availableAndAchieved")
    public List<ClientAchievement> getAvailableAndAchievedAchievements() {

        Person currentUser = getCurrentPersonNotNull();

        //all achievements with all levels
        List<Achievement> achievements = achievementService.getAchievements();

        //the levels that the user achieved
        List<AchievementPersonPairing> achievedLevels = achievementService.getAchievedLevels(currentUser);

        //map the level ids to the achieved levels
        Map<String, AchievementPersonPairing> achievedLevelIds = new HashMap<>(achievedLevels.size());
        for(AchievementPersonPairing achievedLevel : achievedLevels){
            achievedLevelIds.put(achievedLevel.getAchievementLevel().getId(), achievedLevel);
        }

        List<ClientAchievement> clientAchievements = new ArrayList<>(achievements.size());

        //transform all achievements to client achievements
        for(Achievement achievement : achievements){
            ClientAchievement clientAchievement = motivationClientModelMapper.toClientAchievement(achievement);
            //transform all levels of that achievement to client achievement levels
            List<AchievementLevel> sortedAchievementLevels = achievement
                    .getAchievementLevels()
                    .stream()
                    .sorted(Comparator.comparing(AchievementLevel::getOrderValue))
                    .collect(Collectors.toList());
            for(AchievementLevel achievementLevel : sortedAchievementLevels){
                //check if this level was achieved
                AchievementPersonPairing achievedLevel = achievedLevelIds.get(achievementLevel.getId());
                ClientAchievementLevel clientAchievementLevel;
                if (achievedLevel != null) {
                    //the level was achieved by the user
                    clientAchievementLevel =
                            motivationClientModelMapper.toClientAchievementLevel(achievementLevel, true,
                                    achievedLevel.getTimestamp());
                } else {
                    clientAchievementLevel =
                            motivationClientModelMapper.toClientAchievementLevel(achievementLevel, false, 0);
                }
                clientAchievement.addAchievementLevel(clientAchievementLevel);
            }
            clientAchievements.add(clientAchievement);
        }
        addAdditionalLogDetails("Returned {} achieved of {} available achievement levels", achievedLevelIds.size(),
                clientAchievements.size());
        return clientAchievements;
    }

}
