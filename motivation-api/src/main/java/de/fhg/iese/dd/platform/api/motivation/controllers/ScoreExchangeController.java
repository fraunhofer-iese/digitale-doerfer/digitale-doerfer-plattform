/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Matthias Gerbershagen, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.controllers;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.motivation.exceptions.ScoreExchangeAmountInvalidException;
import de.fhg.iese.dd.platform.business.motivation.services.IAccountService;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping({"score"})
@Api(tags = {"score.exchange"})
public class ScoreExchangeController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IPersonAccountService personAccountService;

    @ApiOperation(value = "Send DT to another user")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("exchange/{personId}")
    public void exchangeScore(
            @ApiParam(value = "Receiver of the DTs", required = true) @PathVariable String personId,
            @RequestParam int amount,
            @RequestParam String postingText) {

        long start = System.nanoTime();
        Person sender = getCurrentPersonNotNull();
        if (amount < 0) {
            throw new ScoreExchangeAmountInvalidException("Negative score amount can not be exchanged").withDetail(
                    String.valueOf(amount));
        }
        Account senderAccount = personAccountService.getAccountForOwner(sender);
        Person receiver = personService.findPersonById(personId);
        Account receiverAccount = personAccountService.getAccountForOwner(receiver);
        accountService.bookScore(senderAccount, receiverAccount, postingText, amount, sender, "exchange", null);
        log.info("Exchanged {} DTs between {} and {} in {} ms", amount, sender.getId(), receiver.getId(), TimeUnit.NANOSECONDS.toMillis(System.nanoTime()-start));
    }

}
