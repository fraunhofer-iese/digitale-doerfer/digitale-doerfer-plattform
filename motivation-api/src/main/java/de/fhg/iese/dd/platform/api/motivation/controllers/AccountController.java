/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Matthias Gerbershagen, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.ClientAccount;
import de.fhg.iese.dd.platform.api.motivation.clientmodel.MotivationClientModelMapper;
import de.fhg.iese.dd.platform.business.motivation.services.IPersonAccountService;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Account;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("score/account")
@Api(tags = {"score.account"})
public class AccountController extends BaseController {

    @Autowired
    private IPersonAccountService personAccountService;
    @Autowired
    private MotivationClientModelMapper motivationClientModelMapper;

    @ApiOperation(value = "Get the current account balance",
            notes = "Returns the current balance of the current user")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("balance")
    public int getBalanceForPerson() {

        Person person = getCurrentPersonNotNull();
        Account account = personAccountService.getAccountForOwner(person);
        return account.getBalance();
    }

    @ApiOperation(value = "Get the account including account entries",
            notes = "Returns the account of the current user")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping()
    public ClientAccount getAccount() {

        Person person = getCurrentPersonNotNull();
        PersonAccount account = personAccountService.getAccountForOwner(person);
        ClientAccount clientAccount = motivationClientModelMapper.toClientAccount(account);

        //these log details are used if the request is slow or fails
        addAdditionalLogDetails("#account.entries: {}",
                (clientAccount == null || clientAccount.getEntries() == null) ? 0 : clientAccount.getEntries().size());
        return clientAccount;
    }

}
