/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.motivation.clientmodel;

import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AccountEntry;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.Achievement;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.AchievementLevel;
import de.fhg.iese.dd.platform.datamanagement.motivation.model.PersonAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class MotivationClientModelMapper {

    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    public ClientAccount toClientAccount(PersonAccount account) {
        if (account == null) {
            return null;
        }
        return ClientAccount.builder()
                .id(account.getId())
                .balance(account.getBalance())
                .entries(account.getEntries().stream()
                        .map(this::toClientAccountEntry)
                        .collect(Collectors.toList()))
                .build();
    }

    public ClientAccountEntry toClientAccountEntry(AccountEntry accountEntry) {
        if (accountEntry == null) {
            return null;
        }
        ClientAccountEntry clientAccountEntry = ClientAccountEntry.builder()
                .id(accountEntry.getId())
                .accountEntryType(accountEntry.getAccountEntryType())
                .amount(accountEntry.getAmount())
                .timestamp(accountEntry.getTimestamp())
                .build();
        if (accountEntry.getPartner() instanceof PersonAccount account) {
            clientAccountEntry.setPartner(
                    personClientModelMapper.createClientPersonReference(account.getOwner()));
            clientAccountEntry.setSystemGenerated(false);
        } else {
            clientAccountEntry.setSystemGenerated(true);
        }
        return clientAccountEntry;
    }

    public ClientAchievement toClientAchievement(Achievement achievement){
        if(achievement == null){
            return null;
        }
        return ClientAchievement.builder()
                .id(achievement.getId())
                .name(achievement.getName())
                .descriptionText(achievement.getDescription())
                .category(achievement.getCategory())
                .icon(fileClientModelMapper.toClientMediaItem(achievement.getIcon()))
                .build();
    }

    public ClientAchievementLevel toClientAchievementLevel(AchievementLevel achievementLevel, boolean achieved, long achievedTimestamp) {
        if (achievementLevel == null) {
            return null;
        }
        ClientAchievementLevel clientAchievementLevel = ClientAchievementLevel.builder()
                .id(achievementLevel.getId())
                .name(achievementLevel.getName())
                .descriptionText(achievementLevel.getDescription())
                .challengeDescriptionText(achievementLevel.getChallengeDescription())
                .orderValue(achievementLevel.getOrderValue())
                .achieved(achieved)
                .achievedTimestamp(achievedTimestamp)
                .build();

        if (achieved) {
            clientAchievementLevel.setIcon(fileClientModelMapper.toClientMediaItem(achievementLevel.getIcon()));
        } else {
            clientAchievementLevel.setIcon(
                    fileClientModelMapper.toClientMediaItem(achievementLevel.getIconNotAchieved()));
        }

        return clientAchievementLevel;
    }

}
