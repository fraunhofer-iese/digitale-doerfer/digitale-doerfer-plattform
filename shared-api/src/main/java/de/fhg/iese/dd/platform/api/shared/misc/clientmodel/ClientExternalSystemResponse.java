/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.clientmodel;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;

import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientExternalSystemResponse {

    private HttpMethod requestMethod;
    private String requestURI;
    private Map<String, List<String>> requestHeaders;
    private String requestBody;

    private String responseStatus;
    private Map<String, List<String>> responseHeaders;
    private String responseBodyString;
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "only used in admin endpoint, object is a wrapper for any external response")
    private Map<String, Object> responseBodyJson;

}
