/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.clientmodel;

import de.fhg.iese.dd.platform.api.participants.person.controllers.modifiers.BaseLastNameShorteningModifier;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientOauthAccount;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.SecurityClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonStatus;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.enums.PersonVerificationStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Log4j2
public class PersonClientModelMapper {

    @Autowired
    private IPersonService personService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private SecurityClientModelMapper securityClientModelMapper;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    /**
     * Persons are additionally censored by {@link BaseLastNameShorteningModifier} if they are instantiated for the
     * package the controller is located in.
     */
    public ClientPerson createClientPerson(Person person) {
        if (person == null) {
            return null;
        }
        ClientPerson cp = new ClientPerson();
        copyPersonValues(person, cp);
        return cp;
    }

    public ClientPersonOwn createClientPersonOwn(Person person) {
        if (person == null) {
            return null;
        }
        ClientPersonOwn clientPersonOwn = new ClientPersonOwn();
        copyPersonValues(person, clientPersonOwn);
        clientPersonOwn.setVerificationStatuses(
                toClientPersonVerificationStatuses(person.getVerificationStatuses().getValues()));
        return clientPersonOwn;
    }

    private void copyPersonValues(Person from, ClientPerson to) {
        to.setId(from.getId());
        AddressListEntry defaultAddressListEntry = personService.getDefaultAddressListEntry(from);
        if (defaultAddressListEntry != null) {
            to.setAddress(addressClientModelMapper.toClientAddress(defaultAddressListEntry.getAddress()));
        }
        to.setEmail(from.getEmail());
        to.setPendingNewEmail(from.getPendingNewEmail());
        to.setFirstName(from.getFirstName());
        to.setLastName(from.getLastName());
        to.setNickName(from.getNickName());
        to.setPhoneNumber(from.getPhoneNumber());
        to.setProfilePicture(fileClientModelMapper.toClientMediaItem(from.getProfilePicture()));
        to.setAccountType(from.getAccountType());
        final GeoArea homeArea = from.getHomeArea();
        if (homeArea != null) {
            to.setHomeAreaId(homeArea.getId());
        }
        to.setStatus(toClientPersonStatus(from));
        to.setDeleted(from.isDeleted());
    }

    /**
     * Persons are additionally censored by {@link BaseLastNameShorteningModifier} if they are instantiated for the
     * package the controller is located in.
     */
    public ClientPerson createCensoredClientPerson(Person person) {
        if (person == null) {
            return null;
        }
        return ClientPerson.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .nickName(person.getNickName())
                .profilePicture(fileClientModelMapper.toClientMediaItem(person.getProfilePicture()))
                .status(toClientPersonStatus(person))
                .deleted(person.isDeleted())
                .build();
    }

    /**
     * Creates a ClientPersonExtended from a person and the oAuthAccount information. If this information is null, the
     * returned entity will have a ClientOAuthAccount attribute with just the oauth id from the person table and
     * retrievalFailed = true and failedReason with the given reason.
     */
    public ClientPersonExtended createClientPersonExtended(Person person, OauthAccount oauthAccount,
            String failedReason) {

        final ClientOauthAccount clientOauthAccount;
        if (oauthAccount != null) {
            clientOauthAccount = securityClientModelMapper.toClientOauthAccount(oauthAccount);
        } else {
            clientOauthAccount = ClientOauthAccount.builder()
                    .oauthId(person.getOauthId())
                    .failedReason(failedReason)
                    .retrievalFailed(true)
                    .build();
        }

        final ClientPersonExtended clientPersonExtended = createClientPersonExtended(person);
        clientPersonExtended.setOauthAccount(clientOauthAccount);
        return clientPersonExtended;
    }

    /**
     * Creates a ClientPersonExtended with {@link ClientPersonExtended#getOauthAccount()} == null.
     */
    public ClientPersonExtended createClientPersonExtended(Person person) {

        final ClientPersonExtended clientPersonExtended = ClientPersonExtended.builder()
                .id(person.getId())
                .created(person.getCreated())
                .status(ClientPersonStatus.DELETED)
                .deleted(person.isDeleted())
                .verificationStatuses(toClientPersonVerificationStatuses(person.getVerificationStatuses().getValues()))
                .statuses(toClientPersonStatusesExtended(person.getStatuses().getValues()))
                .build();
        if (person.isNotDeleted()) {
            copyPersonValues(person, clientPersonExtended);
            clientPersonExtended.setLastLoggedIn(person.getLastLoggedIn());
            if (person.getTenant() != null) {
                clientPersonExtended.setHomeCommunityId(person.getTenant().getId());
                clientPersonExtended.setHomeCommunityName(person.getTenant().getName());
            }
            if (person.getHomeArea() != null) {
                clientPersonExtended.setHomeAreaId(person.getHomeArea().getId());
                clientPersonExtended.setHomeAreaName(person.getHomeArea().getName());
                if (person.getHomeArea().getTenant() != null) {
                    clientPersonExtended.setHomeAreaTenantId(person.getHomeArea().getTenant().getId());
                    clientPersonExtended.setHomeAreaTenantName(person.getHomeArea().getTenant().getName());
                }
            }
        }
        return clientPersonExtended;
    }

    public ClientPersonExtended createClientPersonExtendedWithLimitedAttributes(Person person) {
        if (person == null) {
            return null;
        }
        final ClientPersonExtended.ClientPersonExtendedBuilder<?, ?> personBuilder = ClientPersonExtended.builder()
                .id(person.getId())
                .status(toClientPersonStatus(person))
                .deleted(person.isDeleted());
        if (person.isNotDeleted()) {
            personBuilder.firstName(person.getFirstName());
            personBuilder.lastName(person.getLastName());
            personBuilder.email(person.getEmail());
            personBuilder.profilePicture(fileClientModelMapper.toClientMediaItem(person.getProfilePicture()));
        }
        return personBuilder.build();
    }

    /**
     * Persons are additionally censored by {@link BaseLastNameShorteningModifier} if they are instantiated for the
     * package the controller is located in.
     */
    public ClientPerson createFullCensoredClientPerson(Person person) {
        if (person == null) {
            return null;
        }
        return ClientPerson.builder()
                .id(person.getId())
                .status(toClientPersonStatus(person))
                .deleted(person.isDeleted())
                .build();
    }

    /**
     * Persons are additionally censored by {@link BaseLastNameShorteningModifier} if they are instantiated for the
     * package the controller is located in.
     */
    public ClientPersonReference createClientPersonReference(Person person) {
        if (person == null) {
            return null;
        }
        return ClientPersonReference.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .profilePicture(fileClientModelMapper.toClientMediaItem(person.getProfilePicture()))
                .status(toClientPersonStatus(person))
                .deleted(person.isDeleted())
                .build();
    }

    /**
     * Persons are additionally censored by {@link BaseLastNameShorteningModifier} if they are instantiated for the
     * package the controller is located in.
     */
    public ClientPersonReferenceWithEmail createClientPersonReferenceWithEmail(Person person) {
        if (person == null) {
            return null;
        }
        return ClientPersonReferenceWithEmail.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .email(person.getEmail())
                .profilePicture(fileClientModelMapper.toClientMediaItem(person.getProfilePicture()))
                .status(toClientPersonStatus(person))
                .deleted(person.isDeleted())
                .build();
    }

    /**
     * ClientContactPerson are delivered will full last name, since they are meant to be displayed in the application.
     */
    public ClientContactPerson createClientContactPerson(Person person) {
        if (person == null) {
            return null;
        }
        return ClientContactPerson.builder()
                .id(person.getId())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .email(person.getEmail())
                .phoneNumber(person.getPhoneNumber())
                .profilePicture(fileClientModelMapper.toClientMediaItem(person.getProfilePicture()))
                .build();
    }

    private ClientPersonStatus toClientPersonStatus(Person person) {
        if (person.isDeleted()) {
            return ClientPersonStatus.DELETED;
        } else {
            return ClientPersonStatus.REGISTERED;
        }
    }

    private Set<ClientPersonStatusExtended> toClientPersonStatusesExtended(Set<PersonStatus> personStatuses) {

        Set<ClientPersonStatusExtended> result = EnumSet.noneOf(ClientPersonStatusExtended.class);

        for (PersonStatus personStatus : personStatuses) {
            switch (personStatus) {
                case LOGIN_BLOCKED:
                    result.add(ClientPersonStatusExtended.LOGIN_BLOCKED);
                    break;
                case PENDING_DELETION:
                    result.add(ClientPersonStatusExtended.PENDING_DELETION);
                    break;
                case PARALLEL_WORLD_INHABITANT:
                    result.add(ClientPersonStatusExtended.PARALLEL_WORLD_INHABITANT);
                    break;
                default:
                    log.warn("unknown person status {}", personStatus);
            }
        }
        return result;
    }

    private Set<ClientPersonVerificationStatus> toClientPersonVerificationStatuses(
            Set<PersonVerificationStatus> personVerificationStatuses) {

        Set<ClientPersonVerificationStatus> result = EnumSet.noneOf(ClientPersonVerificationStatus.class);

        for (PersonVerificationStatus personVerificationStatus : personVerificationStatuses) {
            switch (personVerificationStatus) {
                case PHONE_NUMBER_VERIFIED:
                    result.add(ClientPersonVerificationStatus.PHONE_NUMBER_VERIFIED);
                    break;
                case EMAIL_VERIFIED:
                    result.add(ClientPersonVerificationStatus.EMAIL_VERIFIED);
                    break;
                case NAME_VERIFIED:
                    result.add(ClientPersonVerificationStatus.NAME_VERIFIED);
                    break;
                case HOME_AREA_VERIFIED:
                    result.add(ClientPersonVerificationStatus.HOME_AREA_VERIFIED);
                    break;
                case ADDRESS_VERIFIED:
                    result.add(ClientPersonVerificationStatus.ADDRESS_VERIFIED);
                    break;
                default:
                    log.warn("unknown person verification status {}", personVerificationStatus);
            }
        }
        return result;
    }

    public Set<PersonStatus> toPersonStatuses(Set<ClientPersonStatusExtended> clientPersonStatuses) {
        if (CollectionUtils.isEmpty(clientPersonStatuses)) {
            return Collections.emptySet();
        }
        return clientPersonStatuses.stream()
                .map(clientStatus -> PersonStatus.valueOf(clientStatus.name()))
                .collect(Collectors.toSet());
    }

}
