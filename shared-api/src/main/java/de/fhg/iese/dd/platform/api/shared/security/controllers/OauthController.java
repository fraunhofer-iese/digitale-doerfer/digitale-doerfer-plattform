/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidQueryParameterException;
import de.fhg.iese.dd.platform.api.shared.security.exceptions.OauthIdRegistrationAlreadyExistsException;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthManagementException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthManagementService;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthRegistrationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.security.config.OauthConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthRegistration;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oauth")
@Api(tags = {"shared.oauth"})
public class OauthController extends BaseController {

    @Autowired
    private OauthConfig oauthConfig;

    @Autowired
    private IOauthManagementService oauthManagementService;

    @Autowired
    private IOauthRegistrationService oauthRegistrationService;

    @ApiOperation(value = "Revokes all refresh tokens of a user",
            notes = """
                    The user is identified by the provided access token.

                    Revocation applies to all applications and implies that, after access token expiration, the user has to log in with username and password again.""")
    @ApiExceptions({ClientExceptionType.UNSPECIFIED_EXTERNAL_ERROR})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "/revokeAllRefreshTokens")
    public void revokeAllRefreshTokens() throws OauthManagementException {
        final String oauthId = getOAuthUserIdNotNull();
        oauthManagementService.revokeAllRefreshTokens(oauthId);
    }

    @ApiOperation(value = "Registers an OAuthId with an email address",
            notes = "Registers an OAuthId with an email address. " +
                    "The registration is used to delete unused OAuthIds when no person was created.")
    @ApiExceptions({
            ClientExceptionType.NOT_AUTHORIZED,
            ClientExceptionType.QUERY_PARAMETER_INVALID,
            ClientExceptionType.OAUTH_ID_REGISTRATION_ALREADY_EXISTS
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY)
    @PostMapping("/registration")
    public void registerOauthUser(
            @RequestParam
            @ApiParam(value = "Email address of the user to register")
                    String email,

            @RequestParam
            @ApiParam(value = "OAuthId of the user to register")
                    String oauthId
    ) {

        if (StringUtils.isEmpty(oauthId)) {
            throw new InvalidQueryParameterException("oauthId", "Must not be empty");
        }

        if (StringUtils.isEmpty(email)) {
            throw new InvalidQueryParameterException("email", "Must not be empty");
        }

        validateApiKey(oauthConfig::getApiKeyForPendingAccounts, getApiKeyNotNull(),
                "oauth.api-key-for-pending-accounts");

        if (oauthRegistrationService.registrationWithOauthIdExists(oauthId)) {
            throw new OauthIdRegistrationAlreadyExistsException(oauthId);
        }

        OauthRegistration oauthRegistration = OauthRegistration.builder()
                .email(email)
                .oauthId(oauthId)
                .build();

        oauthRegistrationService.store(oauthRegistration);
    }

}
