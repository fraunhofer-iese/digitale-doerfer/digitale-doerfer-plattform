/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2020 Jannis von Albedyll, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.processors;

import org.springframework.beans.factory.annotation.Autowired;

import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonChangeHomeAreaResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.ClientPersonVerifyEmailAddressResponse;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.framework.events.EventExecutionStrategy;
import de.fhg.iese.dd.platform.business.framework.events.processing.BaseEventProcessor;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessing;
import de.fhg.iese.dd.platform.business.framework.events.processing.EventProcessor;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeEmailAddressVerificationStatusConfirmation;
import de.fhg.iese.dd.platform.business.participants.person.events.PersonChangeHomeAreaConfirmation;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushException;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.participants.ParticipantsConstants;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;

@EventProcessor(executionStrategy = EventExecutionStrategy.ASYNCHRONOUS_REQUIRED)
public class PersonPushEventProcessor extends BaseEventProcessor {

    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private IClientPushService clientPushService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @PushEvent(clientEvent = ClientPersonChangeHomeAreaResponse.class,
            categorySubject = ParticipantsConstants.PUSH_CATEGORY_SUBJECT_OWN_PERSON_CHANGES,
            loud = false,
            notes = "Sent to the person that changed the home area.")
    @EventProcessing
    private void handlePersonChangeHomeAreaConfirmation(
            PersonChangeHomeAreaConfirmation personChangeHomeAreaConfirmation) {
        Person person = personChangeHomeAreaConfirmation.getPerson();
        GeoArea newHomeArea = personChangeHomeAreaConfirmation.getNewHomeArea();

        try {
            clientPushService.pushToPersonSilent(
                    person,
                    ClientPersonChangeHomeAreaResponse.builder()
                            .personId(person.getId())
                            .homeAreaId(newHomeArea.getId())
                            .build(),
                    pushCategoryService.findPushCategoriesBySubject(
                            ParticipantsConstants.PUSH_CATEGORY_SUBJECT_OWN_PERSON_CHANGES)
            );
        } catch (PushException ex) {
            log.error("Failed to push " + personChangeHomeAreaConfirmation.getLogMessage(), ex);
        }
    }

    @PushEvent(clientEvent = ClientPersonVerifyEmailAddressResponse.class,
            categorySubject = ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED,
            loud = true,
            notes = "Sent to the person that verified the email address.")
    @EventProcessing
    private void handlePersonChangeEmailAddressVerificationStatusConfirmation(
            PersonChangeEmailAddressVerificationStatusConfirmation personChangeEmailAddressVerificationStatusConfirmation) {

        if (!personChangeEmailAddressVerificationStatusConfirmation.isVerificationStatusChanged() &&
                !personChangeEmailAddressVerificationStatusConfirmation.isEmailAddressChanged()) {
            return;
        }

        final Person person = personChangeEmailAddressVerificationStatusConfirmation.getPerson();
        final ClientPersonVerifyEmailAddressResponse clientPersonVerifyEmailAddressResponse =
                ClientPersonVerifyEmailAddressResponse.builder()
                        .personWithVerifiedEmail(personClientModelMapper.createClientPersonReferenceWithEmail(person))
                        .verificationStatusChanged(
                                personChangeEmailAddressVerificationStatusConfirmation.isVerificationStatusChanged())
                        .build();
        final String message = personChangeEmailAddressVerificationStatusConfirmation.isEmailAddressChanged() ?
                "E-Mail-Adresse wurde erfolgreich zu '" + person.getEmail() + "' geändert" :
                "E-Mail-Adresse '" + person.getEmail() + "' wurde erfolgreich verifiziert";

        try {
            clientPushService.pushToPersonInLastUsedAppVariantLoud(
                    person,
                    IClientPushService.PushSendRequestMultipleCategories.builder()
                            .event(clientPersonVerifyEmailAddressResponse)
                            .message(message)
                            .categories(pushCategoryService.findPushCategoriesBySubject(
                                    ParticipantsConstants.PUSH_CATEGORY_SUBJECT_EMAIL_VERIFIED))
                            .build());
        } catch (PushException ex) {
            log.error("Failed to push " + personChangeEmailAddressVerificationStatusConfirmation.getLogMessage(), ex);
        }
    }

}
