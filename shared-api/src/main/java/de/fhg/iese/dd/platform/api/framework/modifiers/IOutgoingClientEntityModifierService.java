/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.business.framework.services.IService;

public interface IOutgoingClientEntityModifierService extends IService {

    /**
     * Apply all {@link IOutgoingClientEntityModifier}s that match the given request path.
     *
     * @param rootEntity            the root of the graph that is traversed to apply the modifiers
     * @param requestPath           the current REST request path
     * @param contentFilterSettings the filter settings for the current user that did the request
     *
     * @return number of entities that have been traversed
     *
     * @see IOutgoingClientEntityModifier#getPathPatterns()
     * @see IOutgoingClientEntityModifier#getExcludedPathPatterns()
     */
    int modifyOutgoingClientEntitiesForRequest(Object rootEntity, String requestPath,
            IContentFilterSettings contentFilterSettings);

    /**
     * Apply all {@link IOutgoingClientEntityModifier}s that match the given event package name.
     *
     * @param eventToPush           the event that is pushed and traversed to apply the modifiers
     * @param contentFilterSettings the filter settings for the current user that receives the push message
     *
     * @return number of entities that have been traversed
     *
     * @see IOutgoingClientEntityModifier#getPushEventPackagePrefixes()
     */
    int modifyOutgoingClientEntitiesForPush(ClientBaseEvent eventToPush, IContentFilterSettings contentFilterSettings);

}
