/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2021 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.sample.controllers;

import java.util.Arrays;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.sample.clientevent.ClientSampleEvent;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleResultDEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleResultEEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleSpecialEvent;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleTrigger1Event;
import de.fhg.iese.dd.platform.business.shared.sample.events.SampleTrigger2Event;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Profile({"!prod"})
@RestController
@RequestMapping("sample/event")
@Api(tags = { "sample" })
public class SampleController extends BaseController {

    @ApiOperation(value = "Just an example event processing implementation, not for production.",
            notes = "Illustrates notifyOneAndWaitForAnyReply")
    @ApiAuthentication(ApiAuthenticationType.PUBLIC)
    @PostMapping("/notifyOneAndWaitForAnyReply")
    public String exampleNotifyOneAndWaitForAnyReply(@RequestBody ClientSampleEvent clientEvent) {

        SampleTrigger1Event sampleTrigger1Event =
                new SampleTrigger1Event("\"" + clientEvent.getPayload() + "\"->SampleController");
        BaseEvent resultEvent =
                notifyAndWaitForAnyReply(sampleTrigger1Event, SampleResultDEvent.class, SampleResultEEvent.class);

        if (resultEvent instanceof SampleResultDEvent) {
            return "D: " + ((SampleResultDEvent) resultEvent).payload;

        }
        if (resultEvent instanceof SampleResultEEvent) {
            return "E: " + ((SampleResultEEvent) resultEvent).payload;

        }
        return "none: " + resultEvent;
    }

    @ApiOperation(value = "Just an example event processing implementation, not for production.",
            notes = "Illustrates notifyMultipleAndWaitForAnyReply")
    @ApiAuthentication(ApiAuthenticationType.PUBLIC)
    @PostMapping("/notifyMultipleAndWaitForAnyReply")
    public String exampleNotifyMultipleAndWaitForAnyReply(@RequestBody ClientSampleEvent clientEvent) {

        SampleTrigger1Event sampleTrigger1Event =
                new SampleTrigger1Event("\"" + clientEvent.getPayload() + "\"->SampleController");
        SampleTrigger2Event sampleTrigger2Event =
                new SampleTrigger2Event("\"" + clientEvent.getPayload() + "\"->SampleController");
        BaseEvent resultEvent = notifyAndWaitForAnyReply(Arrays.asList(sampleTrigger1Event, sampleTrigger2Event),
                SampleResultDEvent.class, SampleResultEEvent.class);

        if (resultEvent instanceof SampleResultDEvent) {
            return "D: " + ((SampleResultDEvent) resultEvent).payload;

        }
        if (resultEvent instanceof SampleResultEEvent) {
            return "E: " + ((SampleResultEEvent) resultEvent).payload;

        }
        return "unspecific: " + resultEvent;
    }

    @ApiOperation(value = "Just an example event processing implementation, not for production.",
            notes = "Illustrates notify")
    @ApiAuthentication(ApiAuthenticationType.PUBLIC)
    @PostMapping("/notify")
    public String exampleNotify(@RequestBody ClientSampleEvent clientEvent) {

        SampleTrigger1Event sampleTrigger1Event =
                new SampleTrigger1Event("\"" + clientEvent.getPayload() + "\"->SampleController");
        SampleTrigger2Event sampleTrigger2Event =
                new SampleTrigger2Event("\"" + clientEvent.getPayload() + "\"->SampleController");

        notify(sampleTrigger1Event);
        notify(sampleTrigger2Event);

        return "no result";
    }

    @ApiOperation(value = "Just an example event processing implementation, not for production.",
            notes = "Illustrates logging")
    @ApiAuthentication(ApiAuthenticationType.OAUTH2)
    @PostMapping("/logMe")
    public void exampleSpecial() {

        notify(SampleSpecialEvent.builder()
                .appVariants(Arrays.asList(getAppVariantOrNull(), getAppVariantOrNull()))
                .person(getCurrentPersonNotNull())
                .status(HttpStatus.ACCEPTED)
                .build());
    }

}
