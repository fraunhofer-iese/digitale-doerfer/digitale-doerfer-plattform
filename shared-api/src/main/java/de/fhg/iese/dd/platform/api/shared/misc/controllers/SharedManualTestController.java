/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2024 Alberto Lara, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer.DataInitializerResult;
import de.fhg.iese.dd.platform.business.shared.init.services.IDataInitializerService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthenticatedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ManualTestConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manualtest")
@Api(tags = {"shared.manualtest"})
public class SharedManualTestController extends BaseController {

    public static final String HEADER_NAME_MANUAL_TEST_API_KEY = "manualTestApiKey";

    @Autowired
    private IDataInitializerService dataInitializerService;

    @Autowired
    private ManualTestConfig manualTestConfig;

    @ApiOperation(value = "Start a specific manual test scenario in the demo tenant",
            notes = "Start a manual test scenario by setting the data to a specific state.\n"
                    + "If clean=true all other data in that domain of the scenario is deleted.")
    @ApiExceptions({ClientExceptionType.UNSPECIFIED_NOT_FOUND, ClientExceptionType.NOT_AUTHENTICATED})
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY, notes = "API Key is defined in manual-test.api-key")
    @PutMapping(value = "start")
    public ResponseEntity<String> startManualTestScenario(
            @RequestHeader(HEADER_NAME_MANUAL_TEST_API_KEY) String manualTestApiKey,
            @RequestParam("scenarioId") String scenarioId,
            @RequestParam(value = "clean", defaultValue = "false", required = false) boolean clean
    ) {

        validateApiKey(manualTestConfig::getApiKey, manualTestApiKey, "manual-test.api-key");
        if (!StringUtils.equals(manualTestApiKey, manualTestConfig.getApiKey())) {
            throw new NotAuthenticatedException(
                    "The manualTestApiKey does not match the api key in config: manual-test.api-key");
        }

        if (!dataInitializerService.getAllManualTestScenarioIds().contains(scenarioId)) {
            throw new NotFoundException("No scenario with id {} found", scenarioId);
        }

        DataInitializerResult dataInitializerResult = dataInitializerService.startManualTestScenario(scenarioId, clean);

        if (dataInitializerResult.wasSuccessful()) {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Get all available manual test scenario ids",
            notes = "These ids can be used to start a specific manual test scenario")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping
    public List<String> getManualTestScenarioIds() {

        return dataInitializerService.getAllManualTestScenarioIds();
    }

}
