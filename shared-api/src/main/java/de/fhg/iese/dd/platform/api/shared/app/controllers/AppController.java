/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidQueryParameterException;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientCommunity;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.TenantClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.*;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeature;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.FeatureClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalText;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextStatus;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.LegalClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.PushClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.services.IClientAppService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantVersionService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantVersionService.CheckVersionResult;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.legal.services.ILegalTextService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BaseRuntimeException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IParallelismService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.enums.AppPlatform;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/app")
@Api(tags = {"shared.app"})
class AppController extends BaseController {

    @Autowired
    private IAppService appService;
    @Autowired
    private IAppVariantVersionService appVariantVersionService;
    @Autowired
    private IClientAppService clientAppService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private ITenantService tenantService;
    @Autowired
    private ILegalTextService legalTextService;
    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IParallelismService parallelismService;
    @Autowired
    private IPushCategoryService pushCategoryService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private AppClientModelMapper appClientModelMapper;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;
    @Autowired
    private TenantClientModelMapper tenantClientModelMapper;
    @Autowired
    private LegalClientModelMapper legalClientModelMapper;
    @Autowired
    private FeatureClientModelMapper featureClientModelMapper;
    @Autowired
    private PushClientModelMapper pushClientModelMapper;

    @ApiOperation(value = "Checks if the app version is supported by this version of the API",
            notes = "The apps are requesting this method during startup. "
                    + "If they do not get a response with supported = true, they display the update information text")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "checkversion")
    public ClientAppVariantVersionCheckResult checkVersion(
            @RequestParam
            @ApiParam(value = "version of the app variant", required = true)
                    String appVariantVersionIdentifier,
            @RequestParam
            @ApiParam(value = "platform of the client (ANDROID|IOS|WEB)", required = true)
                    AppPlatform platform,
            @RequestParam(required = false)
            @ApiParam("OS version running on the device")
                    String osversion,
            @RequestParam(required = false)
            @ApiParam("information on the device (model etc.)")
                    String device) {

        final AppVariant appVariant = getAppVariantNotNull();

        CheckVersionResult checkVersionResult = appVariantVersionService
                .checkAppVariantVersionSupport(appVariant.getAppVariantIdentifier(), appVariantVersionIdentifier,
                        platform, osversion, device);
        addAdditionalLogDetails(checkVersionResult.getLogMessage());
        return appClientModelMapper.toClientAppVariantVersionCheckResult(checkVersionResult);
    }

    @ApiOperation(value = "Returns the details of the onboarding status of the current user",
            notes = "The home geo area and selected areas of the user are checked if they are valid, e.g., the home geo area " +
                    "must be a leaf geo area and contained in available geo areas. Furthermore, the user needs to select " +
                    "at least one geo area and all selected geo areas must be contained in the available geo areas. " +
                    "If one requirement is not fulfilled, an onboarding is required")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "onboardingStatus")
    public ClientOnboardingStatus getOnboardingStatus(
            @RequestParam(required = false)//only for backward compatibility
            @ApiParam(value = "version of the app variant")
                    String appVariantVersionIdentifier,
            @RequestParam(required = false)//only for backward compatibility
            @ApiParam(value = "platform of the client (ANDROID|IOS|WEB)")
                    AppPlatform platform,
            @RequestParam(required = false)
            @ApiParam("OS version running on the device")
                    String osversion,
            @RequestParam(required = false)
            @ApiParam("information on the device (model etc.)")
                    String device
    ) {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();

        final CompletableFuture<ClientLegalTextStatus> legalTextStatusFuture =
                CompletableFuture.supplyAsync(() ->
                                legalClientModelMapper.toClientLegalTextStatus(
                                        legalTextService.getCurrentLegalTextStatus(appVariant, person)),
                        parallelismService.getBlockableExecutor());

        final CompletableFuture<CheckVersionResult> checkVersionFuture;
        if (StringUtils.isNotEmpty(appVariantVersionIdentifier) && platform != null) {
            checkVersionFuture = CompletableFuture.supplyAsync(() ->
                            appVariantVersionService.checkAppVariantVersionSupport(appVariant.getAppVariantIdentifier(),
                                    appVariantVersionIdentifier, platform, osversion, device),
                    parallelismService.getBlockableExecutor());
        } else {
            checkVersionFuture = CompletableFuture.completedFuture(null);
        }

        final CompletableFuture<List<ClientPushCategoryUserSetting>> pushFuture = CompletableFuture.supplyAsync(() ->
                        pushCategoryService.getConfigurableAndVisiblePushCategoryUserSettings(person, appVariant)
                                .stream()
                                .map(pushClientModelMapper::toClientPushCategoryUserSetting)
                                .collect(Collectors.toList()),
                parallelismService.getBlockableExecutor());

        final CompletableFuture<List<ClientFeature>> featureFuture = CompletableFuture.supplyAsync(() ->
                        featureClientModelMapper.toClientFeatures(featureService.getAllFeaturesForTarget(
                                        FeatureTarget.of(person, appVariant),
                                        false)
                                .stream()
                                .filter(BaseFeature::isVisibleForClient)
                                .collect(Collectors.toList())),
                parallelismService.getBlockableExecutor());

        final Set<GeoArea> availableGeoAreas = appService.getAvailableGeoAreas(appVariant);
        final Set<GeoArea> selectedGeoAreas = appService.getSelectedGeoAreas(appVariant, person);
        //we refresh the geo area with a cached one
        GeoArea homeGeoArea = geoAreaService.ensureCached(person.getHomeArea());

        ClientGeoAreaSelectionStatus homeGeoAreaSelectionStatus;
        if (homeGeoArea == null) {
            homeGeoAreaSelectionStatus = ClientGeoAreaSelectionStatus.INVALID_NO_SELECTION;
        } else if (!homeGeoArea.isLeaf()) {
            homeGeoAreaSelectionStatus = ClientGeoAreaSelectionStatus.INVALID_NOT_LEAF;
        } else if (!availableGeoAreas.contains(homeGeoArea)) {
            homeGeoAreaSelectionStatus = ClientGeoAreaSelectionStatus.INVALID_NOT_AVAILABLE;
        } else {
            homeGeoAreaSelectionStatus = ClientGeoAreaSelectionStatus.VALID;
        }

        final Map<String, ClientGeoAreaSelectionStatus> selectedGeoAreaIdsInvalid = selectedGeoAreas.stream()
                .filter(geoArea -> !availableGeoAreas.contains(geoArea))
                .collect(Collectors.toMap(GeoArea::getId,
                        geoArea -> ClientGeoAreaSelectionStatus.INVALID_NOT_AVAILABLE));
        final long availableLeafGeoAreasCount = availableGeoAreas.stream()
                .filter(GeoArea::isLeaf)
                .count();

        final boolean onboardingRequired = homeGeoAreaSelectionStatus != ClientGeoAreaSelectionStatus.VALID ||
                CollectionUtils.isEmpty(selectedGeoAreas) ||
                !CollectionUtils.isEmpty(selectedGeoAreaIdsInvalid);

        try {
            final CheckVersionResult checkVersionResult = checkVersionFuture.get();
            addAdditionalLogDetails(
                    checkVersionResult == null ? "no version check" : checkVersionResult.getLogMessage());

            if (checkVersionResult == null || checkVersionResult.isSupported()) {
                // check version is null if no version check info was provided
                return ClientOnboardingStatus.builder()
                        .id(person.getId())
                        .ownPerson(personClientModelMapper.createClientPersonOwn(person))
                        .versionCheckResult(
                                appClientModelMapper.toClientAppVariantVersionCheckResult(checkVersionResult))
                        .features(featureFuture.get())
                        .legalTextStatus(legalTextStatusFuture.get())
                        .pushCategorySettings(pushFuture.get())
                        .homeGeoArea(geoAreaClientModelMapper.toClientGeoArea(homeGeoArea, false))
                        .homeGeoAreaSelectionStatus(homeGeoAreaSelectionStatus)
                        .selectedGeoAreas(geoAreaClientModelMapper.toClientGeoAreas(selectedGeoAreas, false))
                        .selectedGeoAreaIdsInvalid(selectedGeoAreaIdsInvalid)
                        .availableLeafGeoAreasCount(availableLeafGeoAreasCount)
                        .onboardingRequired(onboardingRequired)
                        .build();
            } else {
                return ClientOnboardingStatus.builder()
                        .id(person.getId())
                        .versionCheckResult(
                                appClientModelMapper.toClientAppVariantVersionCheckResult(checkVersionResult))
                        .onboardingRequired(true)
                        .build();
            }
        } catch (InterruptedException e) {
            throw new InternalServerErrorException(e);
        } catch (ExecutionException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof BaseRuntimeException) {
                throw (BaseRuntimeException) cause;
            } else {
                throw new InternalServerErrorException(cause);
            }
        }
    }

    @ApiOperation(value = "Onboarding status for a new user",
            notes = "Initial onboarding status for a new user before registration or login")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @ApiExceptions(ClientExceptionType.GEO_AREA_NOT_FOUND)
    @GetMapping(value = "onboardingStatus/initial")
    public ClientInitialOnboardingStatus getOnboardingStatusInitial(
            @RequestParam
            @ApiParam(value = "version of the app variant")
                    String appVariantVersionIdentifier,
            @RequestParam
            @ApiParam(value = "platform of the client (ANDROID|IOS|WEB)")
                    AppPlatform platform,
            @RequestParam(required = false)
            @ApiParam("OS version running on the device")
                    String osversion,
            @RequestParam(required = false)
            @ApiParam("information on the device (model etc.)")
                    String device,
            @RequestParam(required = false)
            @ApiParam("The home area the potential new user already selected (influences the features")
                    String homeAreaId

    ) {
        final AppVariant appVariant = getAppVariantNotNull();

        final CompletableFuture<List<ClientLegalText>> legalTextFuture = CompletableFuture.supplyAsync(() ->
                        legalClientModelMapper.toClientLegalTexts(
                                legalTextService.getCurrentLegalTexts(appVariant)),
                parallelismService.getBlockableExecutor());

        final CompletableFuture<CheckVersionResult> checkVersionFuture = CompletableFuture.supplyAsync(() ->
                        appVariantVersionService.checkAppVariantVersionSupport(appVariant.getAppVariantIdentifier(),
                                appVariantVersionIdentifier, platform, osversion, device),
                parallelismService.getBlockableExecutor());

        final CompletableFuture<List<ClientFeature>> featureFuture = CompletableFuture.supplyAsync(() -> {
            FeatureTarget featureTarget;
            if (StringUtils.isNotEmpty(homeAreaId)) {
                final GeoArea geoArea = geoAreaService.findGeoAreaById(homeAreaId);
                featureTarget = FeatureTarget.of(geoArea, appVariant);
            } else {
                featureTarget = FeatureTarget.of(appVariant);
            }
            final List<BaseFeature> featuresVisibleForClient =
                    featureService.getAllFeaturesForTarget(featureTarget, false)
                            .stream()
                            .filter(BaseFeature::isVisibleForClient)
                            .collect(Collectors.toList());
            return featureClientModelMapper.toClientFeatures(featuresVisibleForClient);

        }, parallelismService.getBlockableExecutor());

        try {
            final CheckVersionResult checkVersionResult = checkVersionFuture.get();
            addAdditionalLogDetails(checkVersionResult.getLogMessage());

            if (checkVersionResult.isSupported()) {
                return ClientInitialOnboardingStatus.builder()
                        .id(appVariant.getId())
                        .versionCheckResult(
                                appClientModelMapper.toClientAppVariantVersionCheckResult(checkVersionResult))
                        .features(featureFuture.get())
                        .legalTexts(legalTextFuture.get())
                        .build();
            } else {
                return ClientInitialOnboardingStatus.builder()
                        .id(appVariant.getId())
                        .versionCheckResult(
                                appClientModelMapper.toClientAppVariantVersionCheckResult(checkVersionResult))
                        .build();
            }
        } catch (InterruptedException e) {
            throw new InternalServerErrorException(e);
        } catch (ExecutionException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof BaseRuntimeException) {
                throw (BaseRuntimeException) cause;
            } else {
                throw new InternalServerErrorException(cause);
            }
        }
    }

    @ApiOperation(value = "Use /appVariant instead")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}")
    @Deprecated
    public ClientAppVariant getAppVariant_old(
            @PathVariable String appVariantIdentifier) {

        AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);

        return appClientModelMapper.toClientAppVariant(appVariant);
    }

    @ApiOperation(value = "Returns the details of the current app variant",
            notes = "Details of the current used app variant.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant")
    public ClientAppVariant getAppVariant() {

        final AppVariant appVariant = getAppVariantNotNull();
        return appClientModelMapper.toClientAppVariant(appVariant);
    }

    @Deprecated
    @ApiOperation(value = "Returns all available tenants for an app variant",
            notes = "All tenants that are available in an app variant. The tenants are sorted by name, ascending.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}/availableCommunity")
    public List<ClientCommunity> getAllTenantsForAppVariant(
            @PathVariable String appVariantIdentifier) {

        AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);
        final Set<Tenant> availableTenants = appService.getAvailableTenants(appVariant);
        return availableTenants.stream()
                .sorted(Comparator.comparing(Tenant::getName))
                .map(c -> tenantClientModelMapper.createClientCommunity(c))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns all available tenants for an external app variant",
            notes = "All tenants that are available in an external app variant. The tenants are sorted by name, ascending.")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping(value = "/appVariant/availableTenants")
    public List<ClientCommunity> getAvailableTenantsForAppVariant() {

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        final Set<Tenant> availableTenants = appService.getAvailableTenants(appVariant);
        return availableTenants.stream()
                .sorted(Comparator.comparing(Tenant::getName))
                .map(c -> tenantClientModelMapper.createClientCommunity(c))
                .collect(Collectors.toList());
    }

    @Deprecated
    @ApiOperation(value = "Returns all selected tenants for an app variant",
            notes = "All tenants that are selected in an app variant by the current user. The tenants are sorted by name, ascending.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}/selectedCommunity")
    public List<ClientCommunity> getAllSelectedTenantsForAppVariant(
            @PathVariable String appVariantIdentifier) {

        return appService.getSelectedGeoAreas(appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier),
                        getCurrentPersonNotNull()).stream()
                .map(GeoArea::getTenant)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(Tenant::getName))
                .map(c -> tenantClientModelMapper.createClientCommunity(c))
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Use /appVariant/availableGeoAreas instead")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}/availableGeoAreas")
    @Deprecated
    public List<ClientGeoArea> getAvailableGeoAreasForAppVariant_old(
            @PathVariable String appVariantIdentifier) {

        final AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);
        return clientAppService.getAvailableClientGeoAreas(appVariant);
    }

    @ApiOperation(value = "Returns all available geo areas for an app variant",
            notes = "All geo areas that are available in an app variant, including the parents " +
                    "(also if they have a tenant that is not available in the app variant). " +
                    "The geo areas are sorted by name, ascending.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/availableGeoAreas")
    public List<ClientGeoArea> getAvailableGeoAreasForAppVariant() {

        return clientAppService.getAvailableClientGeoAreas(getAppVariantNotNull());
    }

    @ApiOperation(value = "Returns the children of given geo areas available for an app variant",
            notes = "Returns the geo areas with given ids that are available in an app variant and all children identified." +
                    "The depth of the tree is restricted by a depth parameter. " +
                    "Additionally all the visible geo areas can be included.")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/availableGeoAreas/children")
    public List<ClientGeoArea> getAvailableGeoAreasForAppVariantWithChildren(
            @RequestParam(required = false)
            @ApiParam(value = "The root areas of the tree. If empty, the global root areas are taken.")
                    List<String> geoAreaIds,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all visible geo areas, relative to the given geo area ids. " +
                    "Visible = sibling or sibling of all parents up to the root areas.",
                    defaultValue = "false")
                    boolean revealVisibleGeoAreas,

            @RequestParam(required = false, defaultValue = "1")
            @ApiParam(value = "Depth of the tree relative to the given root areas. 0 = only the root areas.",
                    defaultValue = "1")
                    int depth
    ) {

        final AppVariant appVariant = getAppVariantNotNull();

        Set<GeoArea> availableGeoAreas = appService.getAvailableGeoAreas(appVariant);
        Set<GeoArea> rootGeoAreas = geoAreaService.findAllById(geoAreaIds).stream()
                .filter(availableGeoAreas::contains)
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(rootGeoAreas)) {
            rootGeoAreas = appService.getAvailableRootGeoAreas(appVariant);
        }

        return geoAreaClientModelMapper.toClientGeoAreas(
                geoAreaService.addChildAreas(rootGeoAreas, availableGeoAreas::contains, depth, revealVisibleGeoAreas),
                false);
    }

    @ApiOperation(value = "Returns the geo areas available for an app variant and tenants",
            notes = "Returns the geo areas for the given app variant and tenants. A geo area is returned if there is a " +
                    "contract by one of the tenants for the app variant that includes this geo area. " +
                    "Additionally the children of these geo areas are returned. " +
                    "The parent geo areas can be included.")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "/appVariant/availableGeoAreas/byTenants")
    public List<ClientGeoArea> getAvailableGeoAreasForAppVariantByTenants(
            @RequestParam
            @ApiParam(value = "The ids of tenants that need to have contracts for the app variant in the geo areas returned")
            @NotEmpty
            List<String> tenantIds,

            @RequestParam
            @ApiParam(value = "The app variant identifier of the relevant app variant")
            String relevantAppVariantIdentifier,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes the shapes of the geo areas. High amount of data!",
                    defaultValue = "false")
            boolean includeBoundaryPoints,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all parent geo areas",
                    defaultValue = "false")
            boolean revealParentGeoAreas) {

        AppVariant relevantAppVariant = appService.findAppVariantByAppVariantIdentifier(relevantAppVariantIdentifier);
        Map<GeoArea, Set<AppVariantGeoAreaMapping>> availableGeoAreasWithMappings = appService
                .getAvailableGeoAreasWithMappings(relevantAppVariant);

        Set<String> tenantIdSet = new HashSet<>(tenantIds);

        tenantService.checkTenantsByIdExists(tenantIdSet);

        Set<GeoArea> availableGeoAreas = availableGeoAreasWithMappings.entrySet().stream()
                .filter(e -> e.getValue().stream()
                        .anyMatch(m ->
                                !m.isExcluded()
                                        && m.getContract() != null
                                        && m.getContract().getTenant() != null
                                        && tenantIdSet.contains(m.getContract().getTenant().getId())))
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());

        if (revealParentGeoAreas) {
            availableGeoAreas.addAll(geoAreaService.addParentGeoAreas(availableGeoAreas, g -> true));
        }
        return geoAreaClientModelMapper.toClientGeoAreas(availableGeoAreas, includeBoundaryPoints);
    }

    @ApiOperation(value = "Returns all nearest available geo areas for an app variant",
            notes = "All geo areas that are available in an app variant and whose center point is located within given " +
                    "radius from current position. If `onlyLeaves` is true only leaf geo areas are returned.")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/availableGeoAreas/nearest")
    public List<ClientGeoArea> getNearestAvailableGeoAreasForAppVariant(
            @RequestParam double latitude,
            @RequestParam double longitude,
            @RequestParam(defaultValue = "10", required = false) double radiusKM,
            @RequestParam(defaultValue = "true", required = false) boolean onlyLeaves
    ) {
        if (radiusKM < 0) {
            throw new InvalidQueryParameterException("radiusKM", "only positive allowed");
        }

        return geoAreaClientModelMapper.toClientGeoAreas(
                appService.getNearestAvailableGeoAreas(getAppVariantNotNull(),
                        new GPSLocation(latitude, longitude), radiusKM, onlyLeaves), false);
    }

    @ApiOperation(value = "Searches in available geo areas for an app variant",
            notes = """
                    Searches in all geo areas that are available in an app variant, including the parents (also if they have a tenant that is not available in the app variant). The search considers the name and/or the zip codes of the geo area. For all alpha search terms a name search is done. For the first numeric search term (it might also contain spaces) a zip search is conducted. Every search term is connected with 'and'. For the name search a normalization is done for both the search term and the names of the geo areas:
                    - No umlauts and ß (äöüß → aous)
                    - No double letters (Be**ll**ingen → Be**l**ingen, Zell am See → Zel am Se)
                    - No abbreviated titles (', St', ', M', '(VGem)', ...)
                    - No non-alpha characters ('(', '-', ...)

                    The resulting words are matched with 'starts with', ignoring their order, but all search terms have to match ('Frankfurt am Main' is matched by 'Main Fra' but not by 'Frankfurt an der Oder').

                    Cuts the tree from the top using the parameter `upToDepth`. If `includeAllChildrenOfMatchingAreas` is true all children of a matching area are added if they are in the available geo areas for this app variant.""")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT,
            ClientExceptionType.QUERY_PARAMETER_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/availableGeoAreas/search")
    public List<ClientGeoArea> searchAvailableGeoAreasForAppVariant(
            @RequestParam(defaultValue = "0", required = false)
            @ApiParam(value = "Filter to only include areas >= this depth, 0 = full tree. Must be positive. " +
                    "If you cut too deep no tree is left.")
                    int upToDepth,
            @RequestParam(defaultValue = "true", required = false)
            @ApiParam(value = "If true all children of matching areas are included")
                    boolean includeAllChildrenOfMatchingAreas,
            @RequestParam
            @ApiParam(value = "Search term, minimum length 3", required = true)
                    String search
    ) {

        String trimmedSearch = StringUtils.trim(search);
        if (StringUtils.length(trimmedSearch) < 3) {
            throw new SearchParameterTooShortException("search", 3);
        }
        if (upToDepth < 0) {
            throw new InvalidQueryParameterException("upToDepth", "only positive allowed");
        }
        return geoAreaClientModelMapper.toClientGeoAreas(
                appService.searchAvailableGeoAreas(getAppVariantNotNull(), upToDepth, includeAllChildrenOfMatchingAreas,
                        trimmedSearch), false);
    }

    @ApiOperation(value = "Use /appVariant/selectedGeoAreas instead")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}/selectedGeoAreas")
    @Deprecated
    public Set<String> getSelectedGeoAreasForAppVariant_old(
            @PathVariable String appVariantIdentifier) {

        final AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);
        return appService.getSelectedGeoAreaIds(appVariant, getCurrentPersonNotNull());
    }

    @ApiOperation(value = "Returns all selected geo areas for an app variant",
            notes = "All geo areas that are selected in an app variant by the current user.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/selectedGeoAreas")
    public Set<String> getSelectedGeoAreasForAppVariant() {

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant = getAppVariantNotNull();
        return appService.getSelectedGeoAreaIds(appVariant, person);
    }

    @ApiOperation(value = "Use /appVariant/nearestGeoArea instead")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/{appVariantIdentifier}/nearestGeoArea")
    @Deprecated
    public ClientGeoArea getNearestGeoAreaForAppVariant_old(
            @PathVariable String appVariantIdentifier,
            @RequestParam double latitude,
            @RequestParam double longitude) {

        final AppVariant appVariant = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifier);
        final Set<GeoArea> availableLeafGeoAreas = appService.getAvailableLeafGeoAreas(appVariant);
        final GPSLocation currentLocation = new GPSLocation(latitude, longitude);

        final Pair<GeoArea, IGeoAreaService.GeoAreaMatch>
                nearestGeoAreaMatch = geoAreaService.findNearestGeoArea(availableLeafGeoAreas, currentLocation);
        GeoArea nearestGeoArea = nearestGeoAreaMatch.getLeft();
        // log details are added to the json log, so that we can analyze the precision of the matching
        addAdditionalLogDetails("({}, {}) -> {} -> {}",
                latitude, longitude,
                nearestGeoAreaMatch.getRight(),
                BaseEntity.getIdOf(nearestGeoArea));
        return geoAreaClientModelMapper.toClientGeoArea(nearestGeoArea, false);
    }

    @ApiOperation(value = "Returns the nearest geo area for an app variant and a given location",
            notes = "Geo area is a leaf in the the geo area tree. Only geo areas within the configured geo area lookup " +
                    "radius are considered. If no geo ares in this radius is found, null is returned.")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/appVariant/nearestGeoArea")
    public ClientGeoArea getNearestGeoAreaForAppVariant(
            @RequestParam double latitude,
            @RequestParam double longitude) {

        final AppVariant appVariant = getAppVariantNotNull();
        final Set<GeoArea> availableLeafGeoAreas = appService.getAvailableLeafGeoAreas(appVariant);
        final GPSLocation currentLocation = new GPSLocation(latitude, longitude);

        final Pair<GeoArea, IGeoAreaService.GeoAreaMatch>
                nearestGeoAreaMatch = geoAreaService.findNearestGeoArea(availableLeafGeoAreas, currentLocation);
        GeoArea nearestGeoArea = nearestGeoAreaMatch.getLeft();
        // log details are added to the json log, so that we can analyze the precision of the matching
        addAdditionalLogDetails("({}, {}) -> {} -> {}",
                latitude, longitude,
                nearestGeoAreaMatch.getRight(),
                BaseEntity.getIdOf(nearestGeoArea));
        return geoAreaClientModelMapper.toClientGeoArea(nearestGeoArea, false);
    }

}
