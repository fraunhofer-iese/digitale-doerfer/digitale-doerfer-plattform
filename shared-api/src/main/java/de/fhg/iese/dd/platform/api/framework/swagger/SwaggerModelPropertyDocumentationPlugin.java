/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import static springfox.documentation.schema.Annotations.findPropertyAnnotation;

import java.lang.annotation.Annotation;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanExpressionContext;
import org.springframework.beans.factory.config.BeanExpressionResolver;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.expression.StandardBeanExpressionResolver;
import org.springframework.stereotype.Component;

import com.google.common.base.Optional;

import de.fhg.iese.dd.platform.api.framework.validation.MaxElements;
import de.fhg.iese.dd.platform.api.framework.validation.MaxTextLength;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.log4j.Log4j2;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@Component
@Log4j2
public class SwaggerModelPropertyDocumentationPlugin implements ModelPropertyBuilderPlugin {

    @Autowired
    private ConfigurableBeanFactory beanfactory;

    private static final List<Class<? extends Annotation>> propertyAnnotationClasses =
            List.of(ApiModelProperty.class, NotBlank.class, NotEmpty.class, NotNull.class, MaxTextLength.class,
                    MaxElements.class);

    @Override
    public boolean supports(DocumentationType delimiter) {
        return SwaggerPluginSupport.pluginDoesApply(delimiter);
    }

    @Override
    public void apply(ModelPropertyContext context) {

        if (context.getBeanPropertyDefinition().isPresent()) {

            final StringBuilder descriptionBuilder = new StringBuilder();
            for (Class<? extends Annotation> propertyAnnotationClass : propertyAnnotationClasses) {

                Optional<? extends Annotation> annotation =
                        findPropertyAnnotation(context.getBeanPropertyDefinition().get(), propertyAnnotationClass);

                if (annotation.isPresent()) {
                    final Annotation currentAnnotation = annotation.get();
                    final String description = extendPropertyDescription(currentAnnotation);
                    descriptionBuilder.append(description);

                    // property is set to required if it has NotBlank, NotEmpty or NotNull annotation
                    if (currentAnnotation instanceof NotBlank ||
                            currentAnnotation instanceof NotEmpty ||
                            currentAnnotation instanceof NotNull) {
                        context.getBuilder().required(true);
                    }
                }

            }
            context.getBuilder().description(descriptionBuilder.toString());
        }
    }

    private String extendPropertyDescription(Annotation annotation) {

        if (annotation instanceof ApiModelProperty) {
            final String notes = ((ApiModelProperty) annotation).notes();
            if (StringUtils.isEmpty(notes)) {
                return StringUtils.EMPTY;
            }
            return notes + ".<br />";
        }

        if (annotation instanceof NotBlank) {
            return "Must not be null or whitespace.<br />";
        }

        if (annotation instanceof NotEmpty) {
            return "Must not be null or empty.<br />";
        }

        if (annotation instanceof NotNull) {
            return "Must not be null.<br />";
        }

        if (annotation instanceof MaxTextLength) {
            final String valueExpression = ((MaxTextLength) annotation).max().value();
            final Object maxValue = getConfigValue(valueExpression);
            return String.format("Text must not have more than %s characters (%s).<br />", maxValue.toString(),
                    valueExpression.replaceAll("([#{}])", ""));
        }

        if (annotation instanceof MaxElements) {
            final String valueExpression = ((MaxElements) annotation).max().value();
            final Object maxValue = getConfigValue(valueExpression);
            return String.format("Collection must not have more than %s elements (%s).<br />", maxValue.toString(),
                    valueExpression.replaceAll("([#{}])", ""));
        }
        return StringUtils.EMPTY;
    }

    private Object getConfigValue(String expression) {
        BeanExpressionResolver resolver = beanfactory.getBeanExpressionResolver();
        if (resolver == null) {
            resolver = new StandardBeanExpressionResolver();
        }
        return resolver.evaluate(expression, new BeanExpressionContext(beanfactory, null));
    }

}
