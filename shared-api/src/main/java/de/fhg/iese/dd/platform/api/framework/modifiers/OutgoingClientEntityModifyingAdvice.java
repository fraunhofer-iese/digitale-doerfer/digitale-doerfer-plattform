/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import de.fhg.iese.dd.platform.api.framework.ClientExceptionHandler;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.controllers.RequestContext;
import lombok.extern.log4j.Log4j2;

/**
 * This controller advice scans through all outgoing client entities and applies the
 * {@link OutgoingClientEntityModifyingAdvice}s if they match to the type and controller package.
 */
@Log4j2
@ControllerAdvice
class OutgoingClientEntityModifyingAdvice implements ResponseBodyAdvice<Object> {

    @Autowired
    private IOutgoingClientEntityModifierService outgoingClientEntityModifierService;

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> clazz) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType,
            Class<? extends HttpMessageConverter<?>> clazz, ServerHttpRequest serverHttpRequest,
            ServerHttpResponse serverHttpResponse) {

        final long startTime = System.nanoTime();
        final Class<?> controllerClass = methodParameter.getDeclaringClass();
        final String requestPath = serverHttpRequest.getURI().getPath();

        if (!BaseController.class.isAssignableFrom(controllerClass)) {
            if (log.isTraceEnabled()) {
                if (ClientExceptionHandler.class != controllerClass && BasicErrorController.class != controllerClass) {
                    log.trace("Current controller {} is not extending BaseController, skipping.",
                            controllerClass.getName());
                }
            }
            return body;
        }

        final IContentFilterSettings contentFilterSettings = getRequestContext();
        if (contentFilterSettings == null) {
            log.warn("No RequestContext found in controller {}.", controllerClass.getName());
            return body;
        }

        int nrOfInspectedObjects =
                outgoingClientEntityModifierService.modifyOutgoingClientEntitiesForRequest(body, requestPath,
                        getRequestContext());

        if (log.isTraceEnabled()) {
            log.trace("beforeBodyWrite took {} ms, inspected {} objects",
                    TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime), nrOfInspectedObjects);
        }
        return body;
    }

    private RequestContext getRequestContext() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            Object requestContext = request.getAttribute(BaseController.REQUEST_ATTRIBUTE_NAME_REQUEST_CONTEXT);
            if (requestContext != null) {
                if (requestContext instanceof RequestContext) {
                    return (RequestContext) requestContext;
                }
            }
        }
        return null;
    }

}
