/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.dataprivacy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.WrongCheckException;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.config.DataPrivacyConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/administration/dataprivacy")
@Api(tags = {"admin", "shared.admin.dataprivacy"})
public class DataPrivacyAdminController extends BaseController {

    @Autowired
    private ICommonDataPrivacyService commonDataPrivacyService;

    @Autowired
    private IPersonService personService;

    @Autowired
    private IFileStorage fileStorage;

    @Autowired
    private DataPrivacyConfig dataPrivacyConfig;

    @ApiOperation(value = "Data privacy report",
            notes = "Returns the zip file location of the data privacy report for a single user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "report")
    public String startDataPrivacyCollection(@RequestParam String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        final Person person = personService.findPersonById(personId);
        final DataPrivacyDataCollection dataCollection =
                commonDataPrivacyService.startDataPrivacyDataCollection(person,
                        DataPrivacyWorkflowTrigger.ADMIN_TRIGGERED_REPORT_ONLY);

        return fileStorage.getExternalUrl(dataCollection.getInternalFolderPath());
    }

    @ApiOperation(value = "Send data privacy report to the given person",
            notes = "Collects the data privacy report for the given person and sends it via email")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "report/mail")
    public void sendDataPrivacyReportEmail(@RequestParam String personId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        final Person person = personService.findPersonById(personId);

        notify(DataPrivacyReportRequest.builder()
                .person(person)
                .trigger(DataPrivacyWorkflowTrigger.ADMIN_TRIGGERED_MAIL_TO_USER)
                .build());
    }

    @ApiOperation(value = "Deletes the given person",
            notes = "Returns OK when the action could be started successfully. Runs the delete process asynchronously.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "person")
    public void deletePerson(
            @RequestParam String personId,
            @RequestParam String check) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        if (!dataPrivacyConfig.getUserDeletionCheck().equals(check)) {
            throw new WrongCheckException();
        }

        final Person person = personService.findPersonById(personId);
        notify(PersonDeleteRequest.builder()
                .person(person)
                .trigger(DataPrivacyWorkflowTrigger.ADMIN_TRIGGERED)
                .build());
    }

}
