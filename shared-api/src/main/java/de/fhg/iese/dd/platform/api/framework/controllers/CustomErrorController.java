/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ClientExceptionEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthenticatedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;

@Controller
public class CustomErrorController extends BasicErrorController {

    @Autowired
    private ApplicationConfig applicationConfig;

    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes, new ErrorProperties());
    }

    @Override
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = super.errorHtml(request, response);
        modelAndView.addObject("version", applicationConfig.getVersionInfo().getPlatformVersion());
        return modelAndView;
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
    @ApiAuthentication(ApiAuthenticationType.PUBLIC)
    @ResponseBody
    public ResponseEntity<ClientExceptionEntity> jsonError(HttpServletRequest httpRequest) {

        final String path = httpRequest.getAttribute(RequestDispatcher.ERROR_REQUEST_URI).toString();
        Object statusCodeString = httpRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        HttpStatus status = null;
        if (statusCodeString instanceof Integer) {
            status = HttpStatus.resolve((Integer) statusCodeString);
        }
        if (status == null || status.is5xxServerError()) {
            status = HttpStatus.UNAUTHORIZED;
        }
        if (status == HttpStatus.UNAUTHORIZED) {
            return new ResponseEntity<>(ClientExceptionEntity.builder()
                    .type(ClientExceptionType.NOT_AUTHENTICATED)
                    .exception(NotAuthenticatedException.class.getName())
                    .message("No or incorrect authentication provided")
                    .path(path)
                    .timestamp(System.currentTimeMillis())
                    .status(HttpStatus.UNAUTHORIZED)
                    .build(),
                    HttpStatus.UNAUTHORIZED);
        }
        if (status == HttpStatus.FORBIDDEN) {
            return new ResponseEntity<>(ClientExceptionEntity.builder()
                    .type(ClientExceptionType.NOT_AUTHORIZED)
                    .exception(NotAuthorizedException.class.getName())
                    .message("Not authorized to do this action or view the data")
                    .path(path)
                    .timestamp(System.currentTimeMillis())
                    .status(HttpStatus.FORBIDDEN)
                    .build(),
                    HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(ClientExceptionEntity.builder()
                .type(ClientExceptionType.UNSPECIFIED_NOT_FOUND)
                .exception(NotFoundException.class.getName())
                .message("No endpoint found")
                .path(path)
                .timestamp(System.currentTimeMillis())
                .status(HttpStatus.NOT_FOUND)
                .build(),
                HttpStatus.NOT_FOUND);
    }

}
