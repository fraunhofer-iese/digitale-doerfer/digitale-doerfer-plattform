/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.files.clientevent.ClientMediaItemDeleteRequest;
import de.fhg.iese.dd.platform.api.shared.files.clientevent.ClientTemporaryMediaItemUseRequest;
import de.fhg.iese.dd.platform.api.shared.files.clientevent.ClientTemporaryMediaItemUseResponse;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.files.events.MediaItemDeleteConfirmation;
import de.fhg.iese.dd.platform.business.shared.files.events.MediaItemDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.files.events.TemporaryMediaItemUseConfirmation;
import de.fhg.iese.dd.platform.business.shared.files.events.TemporaryMediaItemUseRequest;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/media/event")
@Api(tags = {"shared.media.events"})
public class MediaItemEventController extends BaseController {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    @ApiOperation(value = "Uses temporary media items",
            notes = "Uses temporary media items that the user uploaded before and persists them. Temporary media items " +
                    "get automatically deleted after a short time, when it is used it gets transformed into a media item " +
                    "that is only deleted explicitly")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2_AND_API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping("temporaryMediaItemUseRequest")
    public ClientTemporaryMediaItemUseResponse onTemporaryMediaItemUseRequest(
            @Valid @RequestBody
            @ApiParam(required = true) final ClientTemporaryMediaItemUseRequest request
    ) {

        final Person owner = getCurrentPersonNotNull();
        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        final List<TemporaryMediaItem> temporaryMediaItems =
                mediaItemService.findTemporaryItemsById(owner, request.getTemporaryMediaItemIds());

        final TemporaryMediaItemUseRequest temporaryMediaItemUseRequest = TemporaryMediaItemUseRequest.builder()
                .temporaryMediaItems(temporaryMediaItems)
                .newFileOwnership(FileOwnership.of(owner, appVariant))
                .build();
        final TemporaryMediaItemUseConfirmation confirmation =
                notifyAndWaitForReply(TemporaryMediaItemUseConfirmation.class, temporaryMediaItemUseRequest);

        return ClientTemporaryMediaItemUseResponse.builder()
                .clientMediaItems(fileClientModelMapper.toClientMediaItems(confirmation.getMediaItems()))
                .build();
    }

    @ApiOperation(value = "Deletes media files",
            notes = "Deletes media files that are owned by the app variant.")
    @ApiExceptions({
            ClientExceptionType.FILE_ITEM_NOT_FOUND,
            ClientExceptionType.MEDIA_ITEM_USAGE_NOT_ALLOWED,
            ClientExceptionType.FILE_ITEM_CANNOT_BE_DELETED
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @PostMapping("/deleteMediaItemRequest")
    public void onDeleteMediaItemRequest(
            @Valid @RequestBody ClientMediaItemDeleteRequest request
    ) {

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        // only media items that are owned by this app variant are returned, otherwise a not found exception is thrown
        List<MediaItem> mediaItems =
                mediaItemService.findItemsByIdOwnedByAppVariant(appVariant, request.getMediaItemIds());

        final MediaItemDeleteRequest event = MediaItemDeleteRequest.builder()
                .mediaItemsToDelete(mediaItems)
                .build();
        // we wait for the event to get the exceptions that occur while processing it
        notifyAndWaitForReply(MediaItemDeleteConfirmation.class, event);
    }

}
