/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.clientmodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientTenantAndAppVariantEntry {

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appId = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appIdentifier = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appVariantId = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appVariantIdentifier = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String appVariantName = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String tenantId = "";

    @Builder.Default
    @ApiModelProperty(required = true)
    private String tenantName = "";

}
