/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlag;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagDetail;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.UserGeneratedContentFlagClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.security.ListUserGeneratedContentFlagsAction;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.enums.UserGeneratedContentFlagStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;

@RestController
@RequestMapping("/adminui/flag")
@Api(tags = {"shared.adminui.flag"})
public class UserGeneratedContentFlagAdminUiController extends BaseAdminUiController {

    @Getter
    @AllArgsConstructor
    public enum FlagSortColumn {

        FLAG_CREATOR_LAST_NAME("flagCreator.lastName"),
        FLAG_CREATOR_FIRST_NAME("flagCreator.firstName"),
        ENTITY_AUTHOR_FIRST_NAME("entityAuthor.firstName"),
        ENTITY_AUTHOR_LAST_NAME("entityAuthor.lastName"),
        ENTITY_TYPE("entityType"),
        TENANT_NAME("tenant.name"),
        STATUS("status"),
        LAST_STATUS_UPDATE("lastStatusUpdate");

        private final String column;

    }

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private UserGeneratedContentFlagClientModelMapper userGeneratedContentFlagClientModelMapper;

    @ApiOperation(value = "Lists all user generated content flags",
            notes = "The flags can be filtered by tenant and multiple status")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping
    public Page<ClientUserGeneratedContentFlag> listFlags(
            @ApiParam("number of pages to retrieve. First page has number 0")
            @RequestParam(required = false, defaultValue = "0")
                    int page,

            @ApiParam("number of elements per page")
            @RequestParam(required = false, defaultValue = "10")
                    int count,

            @RequestParam(required = false, defaultValue = "LAST_STATUS_UPDATE")
                    FlagSortColumn sortColumn,

            @RequestParam(required = false, defaultValue = "DESC")
                    Sort.Direction sortDirection,

            @ApiParam("tenant for which the flags should be listed")
            @RequestParam(required = false)
                    String tenantId,

            @ApiParam("only list entries in these status (if empty, all entries are listed)")
            @RequestParam(required = false)
                    Set<UserGeneratedContentFlagStatus> includedStatus) {

        checkPageAndCountValues(page, count);

        final Person currentPerson = getCurrentPersonNotNull();

        if (StringUtils.isNotEmpty(tenantId)) {
            //looking up the tenant to ensure that the tenant id is valid
            tenantService.checkTenantByIdExists(tenantId);
        }

        PermissionTenantRestricted listFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        final PageRequest pageRequest = PageRequest.of(page, count, Sort.by(sortDirection, sortColumn.getColumn()));

        if (StringUtils.isNotEmpty(tenantId)) {
            if (listFlagsPermission.isTenantAllowed(tenantId)) {
                return userGeneratedContentFlagService.findAllByTenantInAndStatusIn(
                                Collections.singleton(tenantId), includedStatus, pageRequest)
                        .map(userGeneratedContentFlagClientModelMapper::toClientUserGeneratedContentFlag);
            } else {
                //the permission to view this tenant is not given
                throw new NotAuthorizedException("Insufficient privileges to list flags of tenant {}", tenantId);
            }
        } else {
            if (listFlagsPermission.isAllTenantsAllowed()) {
                return userGeneratedContentFlagService.findAllByStatusIn(includedStatus, pageRequest)
                        .map(userGeneratedContentFlagClientModelMapper::toClientUserGeneratedContentFlag);
            } else {
                return userGeneratedContentFlagService.findAllByTenantInAndStatusIn(
                                listFlagsPermission.getAllowedTenantIds(), includedStatus, pageRequest)
                        .map(userGeneratedContentFlagClientModelMapper::toClientUserGeneratedContentFlag);
            }
        }
    }

    @ApiOperation(value = "Returns details of a user generated content flag")
    @ApiExceptions({
            ClientExceptionType.USER_GENERATED_CONTENT_FLAG_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListUserGeneratedContentFlagsAction.class})
    @GetMapping("{flagId}")
    public ClientUserGeneratedContentFlagDetail getFlagById(@PathVariable String flagId) {

        final Person currentPerson = getCurrentPersonNotNull();
        PermissionTenantRestricted listFlagsPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListUserGeneratedContentFlagsAction.class, currentPerson);

        UserGeneratedContentFlag userGeneratedContentFlag =
                userGeneratedContentFlagService.findByIdWithStatusRecords(flagId);

        if (listFlagsPermission.isTenantDenied(userGeneratedContentFlag.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to list flag {}", flagId);
        }

        return userGeneratedContentFlagClientModelMapper.toClientUserGeneratedContentFlagDetail(
                userGeneratedContentFlag);
    }

}
