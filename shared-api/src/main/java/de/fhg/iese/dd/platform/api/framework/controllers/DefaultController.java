/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/")
@Api(tags = {"shared.api-info"})
public class DefaultController extends BaseController {

    @Autowired
    private ApplicationConfig applicationConfig;

    @ApiAuthentication(ApiAuthenticationType.PUBLIC)
    @GetMapping
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Special endpoint for rendering a fallback html page")
    public ModelAndView renderApiInfoPage(HttpServletRequest httpRequest) {

        // view apiInfo.html located in shared-business/src/main/resources/templates
        ModelAndView modelAndView = new ModelAndView("apiInfo");
        modelAndView.addObject("version", applicationConfig.getVersionInfo().getPlatformVersion());

        return modelAndView;
    }

}
