/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2021 Johannes Schneider, Balthasar Weitzel, Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.clientmodel;

import java.util.List;
import java.util.Set;

import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientAppVariantUsageExtended;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientOauthAccount;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleAssignment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Only returned to admins
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientPersonExtended extends ClientPerson {

    private String homeAreaName;
    private String homeAreaTenantId;
    private String homeAreaTenantName;
    private String homeCommunityId;
    private String homeCommunityName;
    private Long created;
    private Long lastLoggedIn;

    private ClientOauthAccount oauthAccount;
    private List<ClientRoleAssignment> roleAssignments;
    private Set<ClientPersonVerificationStatus> verificationStatuses;
    private Set<ClientPersonStatusExtended> statuses;
    private List<ClientAppVariantUsageExtended> appVariantUsages;

    public ClientPersonExtended withRoleAssignments(List<ClientRoleAssignment> roleAssignments) {
        this.roleAssignments = roleAssignments;
        return this;
    }

    public ClientPersonExtended withAppVariantUsages(List<ClientAppVariantUsageExtended> appVariantUsages) {
        this.appVariantUsages = appVariantUsages;
        return this;
    }

}
