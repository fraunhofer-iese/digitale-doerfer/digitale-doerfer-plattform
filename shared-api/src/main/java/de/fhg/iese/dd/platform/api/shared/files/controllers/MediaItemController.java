/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItemSize;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientTemporaryMediaItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.config.MediaConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemProcessingException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemSize;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/media")
@Api(tags = {"shared.media"})
class MediaItemController extends BaseController {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private MediaConfig mediaConfig;

    @ApiOperation(value = "Uploads a temporary media file",
            notes = """
                    The uploaded file can be used by this user to create entities with media items. Use the returned id (NOT the id of the media item) in create requests.

                    The temporary files have an expiration date and are removed automatically afterwards""")
    @ApiExceptions({
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED,
            ClientExceptionType.TEMPORARY_FILE_ITEM_LIMIT_EXCEEDED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/temp")
    public ClientTemporaryMediaItem uploadTemporaryMediaItem(@RequestParam MultipartFile file) {

        if (file.getSize() >= mediaConfig.getMaxUploadFileSize().toBytes()) {
            throw new FileItemUploadException("File size limit of {}MB for upload exceeded",
                    mediaConfig.getMaxUploadFileSize().toMegabytes());
        }
        final Person owner = getCurrentPersonNotNull();
        //TODO replace with AppVariantNotNull after DorfFunk fixed missing app variant identifier
        final AppVariant appVariant = getAppVariantOrNull();
        byte[] imageData;
        try {
            imageData = file.getBytes();
        } catch (IOException e) {
            throw new FileItemUploadException(e);
        }
        try {
            TemporaryMediaItem temporaryMediaItem =
                    mediaItemService.createTemporaryMediaItem(imageData, FileOwnership.of(owner, appVariant));
            return fileClientModelMapper.toClientTemporaryMediaItem(temporaryMediaItem);
        } catch (FileItemProcessingException e) {
            throw new FileItemUploadException(e);
        }
    }

    @ApiOperation(value = "Uploads a media file of an app variant",
            notes = "The uploaded file is *only* owned by the app variant and *not* by the user.")
    @ApiExceptions({
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/appVariant")
    public ClientMediaItem uploadMediaItem(@RequestParam MultipartFile file) {

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        byte[] imageData;
        try {
            imageData = file.getBytes();
        } catch (IOException e) {
            throw new FileItemUploadException(e);
        }
        try {
            final MediaItem mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.of(appVariant));
            return fileClientModelMapper.toClientMediaItem(mediaItem);
        } catch (FileItemProcessingException e) {
            throw new FileItemUploadException(e);
        }
    }

    @ApiOperation(value = "Deletes a temporary media file",
            notes = "Deletes a temporary file that was uploaded by the user before.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping("/temp/{temporaryMediaItemId}")
    public void deleteTemporaryMediaItem(@PathVariable String temporaryMediaItemId) {

        Person owner = getCurrentPersonNotNull();
        mediaItemService.deleteTemporaryItemById(owner, temporaryMediaItemId);
    }

    @ApiOperation(value = "Gets a temporary media file",
            notes = "Gets a temporary file that was uploaded by the user before.")
    @ApiExceptions({
            ClientExceptionType.TEMPORARY_FILE_ITEM_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/temp/{temporaryMediaItemId}")
    public ClientTemporaryMediaItem getTemporaryMediaItem(@PathVariable String temporaryMediaItemId) {

        Person owner = getCurrentPersonNotNull();
        TemporaryMediaItem temporaryMediaItem =
                mediaItemService.findTemporaryItemById(owner, temporaryMediaItemId);

        return fileClientModelMapper.toClientTemporaryMediaItem(temporaryMediaItem);
    }

    @ApiOperation(value = "Gets all temporary media files",
            notes = "Gets all temporary files that were uploaded by the user before.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/temp")
    public List<ClientTemporaryMediaItem> getAllTemporaryMediaItems() {

        Person owner = getCurrentPersonNotNull();

        return mediaItemService.findAllTemporaryItemsByOwner(owner).stream()
                .map(fileClientModelMapper::toClientTemporaryMediaItem)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Gets all media files",
            notes = "Gets all media files that were uploaded by the app variant before. By default only media items " +
                    "owned exclusively by the app variant are returned. If `userOwned` is true, also media items " +
                    "owned by the app variant and a person are returned.")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping("/appVariant")
    public Page<ClientMediaItem> getAllMediaItems(
            @RequestParam(defaultValue = "false")
            @ApiParam(value = "return also media items owned by a user",
                    defaultValue = "false")
                    boolean userOwned,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of media items",
                    defaultValue = "0")
                    int page,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of media items returned in a page",
                    defaultValue = "10")
                    int count
    ) {

        checkPageAndCountValues(page, count);

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        final Pageable pageable = PageRequest.of(page, count);
        Page<MediaItem> mediaItems =
                mediaItemService.findAllItemsOwnedByAppVariant(appVariant, userOwned, pageable);

        return mediaItems.map(fileClientModelMapper::toClientMediaItem);
    }

    @ApiOperation(value = "Get the current defined media item sizes",
            notes = "Gets the current defined media item sizes with their maximum size.")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/size")
    public List<ClientMediaItemSize> getDefinedSizes() {

        List<MediaItemSize> sizes = mediaItemService.getDefinedSizesCurrentSizeVersion();
        int currentSizeVersion = mediaItemService.getCurrentSizeVersion();

        return sizes.stream()
                .map(s -> fileClientModelMapper.toClientMediaItemSize(s, currentSizeVersion))
                .collect(Collectors.toList());
    }

}
