/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Stefan Schweitzer, Danielle Korth, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.controllers;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoAreaExtended;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.security.ListGeoAreasExtendedAction;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/geoArea")
@Api(tags = {"participants.adminui.geoarea"})
public class GeoAreaAdminUiController extends BaseController {

    private static final int MIN_SEARCH_LENGTH = 3;

    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;

    @ApiOperation(value = "Returns all geo areas",
            notes = "Returns all geo areas as a tree. " +
                    "This endpoint has a bad performance because it returns a lot of data. " +
                    "Use /adminui/geoArea/children instead, which supports a kind of pagination.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGeoAreasExtendedAction.class},
            notes = "If callers are restricted to some tenants only geo areas with no tenant or these tenants are returned. " +
                    "The resulting tree might look distorted.")
    @GetMapping
    public List<ClientGeoAreaExtended> getAllGeoAreas(
            @RequestParam(
                    required = false,
                    defaultValue = "false")
                    boolean includeBoundaryPoints) {

        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListGeoAreasExtendedAction.class,
                        getCurrentPersonNotNull());

        Collection<GeoArea> geoAreas = filterByPermission(permission, geoAreaService.findAll());

        return geoAreaClientModelMapper.toClientGeoAreasExtended(geoAreas, includeBoundaryPoints);
    }

    @ApiOperation(value = "Returns the children of the given geo areas",
            notes = "Returns the geo areas with the given ids and all children identified. " +
                    "The depth of the tree is restricted by a depth parameter. " +
                    "Additionally all the visible geo areas can be included.")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGeoAreasExtendedAction.class},
            notes = "If callers are restricted to some tenants only geo areas with no tenant or these tenants are returned. " +
                    "The resulting tree might look distorted.")
    @GetMapping(value = "/children")
    public List<ClientGeoAreaExtended> getGeoAreaWithChildren(
            @RequestParam(required = false)
            @ApiParam(value = "The root areas of the tree. If empty the global root areas are taken.")
                    List<String> geoAreaIds,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes the shapes of the geo areas. High amount of data!",
                    defaultValue = "false")
                    boolean includeBoundaryPoints,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all visible geo areas, relative to the given geo area ids. " +
                    "Visible = sibling or sibling of all parents up to the root areas.",
                    defaultValue = "false")
                    boolean revealVisibleGeoAreas,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all parent geo areas, relative to the given geo area ids. " +
                    "This is a subset of the geo areas returned by revealVisibleGeoAreas = true.",
                    defaultValue = "false")
                    boolean revealParentGeoAreas,

            @RequestParam(required = false, defaultValue = "1")
            @ApiParam(value = "Depth of the tree relative to the given root areas. 0 = only the given geo area ids.",
                    defaultValue = "1")
                    int depth) {

        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListGeoAreasExtendedAction.class,
                        getCurrentPersonNotNull());

        Collection<GeoArea> foundAreas = geoAreaService.findAllById(geoAreaIds);

        if (CollectionUtils.isEmpty(foundAreas)) {
            foundAreas = geoAreaService.findAllRootAreas();
        }

        final Set<GeoArea> result = geoAreaService.addChildAreas(foundAreas, g -> true, depth, revealVisibleGeoAreas);
        if (revealParentGeoAreas) {
            result.addAll(geoAreaService.addParentGeoAreas(result, g -> true));
        }

        return geoAreaClientModelMapper.toClientGeoAreasExtended(filterByPermission(permission, result),
                includeBoundaryPoints);
    }

    @ApiOperation(value = "Returns the children of searched geo areas",
            notes = "Returns the geo areas matching the search parameter (case insensitive `id` or `name`) and their " +
                    "children up to the given depth. " +
                    "The depth of the tree is restricted by a depth parameter.  " +
                    "Additionally all the visible geo areas can be included.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListGeoAreasExtendedAction.class},
            notes = "If callers are restricted to some tenants only geo areas with no tenant or these tenants are returned. " +
                    "The resulting tree might look distorted.")
    @GetMapping(value = "/search")
    public List<ClientGeoAreaExtended> getGeoAreaWithChildrenSearched(
            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes the shapes of the geo areas. High amount of data!",
                    defaultValue = "false")
                    boolean includeBoundaryPoints,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all visible geo areas, relative to the given geo area ids. " +
                    "Visible = sibling or sibling of all parents up to the root areas.",
                    defaultValue = "false")
                    boolean revealVisibleGeoAreas,

            @RequestParam(required = false, defaultValue = "false")
            @ApiParam(value = "Includes all parent geo areas, relative to the given geo area ids. " +
                    "This is a subset of the geo areas returned by revealVisibleGeoAreas = true.",
                    defaultValue = "false")
                    boolean revealParentGeoAreas,

            @RequestParam(required = false, defaultValue = "1")
            @ApiParam(value = "Depth of the tree relative to the areas matching the search term. 0 = only the areas " +
                    "matching the search term.",
                    defaultValue = "1")
                    int depth,

            @RequestParam
            @ApiParam("Root areas of the returned tree have to match the search term with id or name.")
                    String search) {

        PermissionTenantRestricted permission =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListGeoAreasExtendedAction.class,
                        getCurrentPersonNotNull());

        final String searchString = checkAndParseSearchParameter(search);
        if (StringUtils.isEmpty(searchString)) {
            return Collections.emptyList();
        }

        Collection<GeoArea> foundAreas = geoAreaService.findAllByIdOrNameInfixSearch(searchString);
        if (CollectionUtils.isEmpty(foundAreas)) {
            return Collections.emptyList();
        }

        final Set<GeoArea> result = geoAreaService.addChildAreas(foundAreas, g -> true, depth, revealVisibleGeoAreas);
        if (revealParentGeoAreas) {
            result.addAll(geoAreaService.addParentGeoAreas(result, g -> true));
        }

        return geoAreaClientModelMapper.toClientGeoAreasExtended(filterByPermission(permission, result),
                includeBoundaryPoints);
    }

    private Collection<GeoArea> filterByPermission(PermissionTenantRestricted permission,
            Collection<GeoArea> geoAreas) {
        if (permission.isAllTenantsAllowed()) {
            return geoAreas;
        } else {
            return geoAreas.stream()
                    //geo areas without tenant can be viewed by anyone, too
                    .filter(g -> g.getTenant() == null || permission.isTenantAllowed(g.getTenant()))
                    .collect(Collectors.toSet());
        }
    }

    private String checkAndParseSearchParameter(String search) {
        final String trimmedSearchString = search.trim().toLowerCase();
        if (trimmedSearchString.length() < MIN_SEARCH_LENGTH) {
            throw new SearchParameterTooShortException("search", MIN_SEARCH_LENGTH);
        }
        return trimmedSearchString;
    }

}
