/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import java.util.List;
import java.util.stream.Collectors;

@Service
class OutgoingClientEntityModifierService extends BaseService implements IOutgoingClientEntityModifierService {

    @Autowired
    private List<IOutgoingClientEntityModifier<? extends ClientBaseEntity>> outgoingClientEntityModifiers;

    @Override
    public int modifyOutgoingClientEntitiesForRequest(Object rootEntity, String requestPath,
            IContentFilterSettings contentFilterSettings) {

        //find all matching modifiers by using the ant matcher on the request path
        final AntPathMatcher antPathMatcher = new AntPathMatcher();
        List<IOutgoingClientEntityModifier<? extends ClientBaseEntity>> modifiers =
                outgoingClientEntityModifiers.stream()
                        //included
                        .filter(modifier -> modifier.getPathPatterns().stream()
                                .anyMatch(pattern -> antPathMatcher.match(pattern, requestPath)))
                        //excluded
                        .filter(modifier -> modifier.getExcludedPathPatterns().isEmpty()
                                || modifier.getExcludedPathPatterns().stream()
                                .noneMatch(pattern -> antPathMatcher.match(pattern, requestPath)))
                        .collect(Collectors.toList());

        return applyModifiers(rootEntity, contentFilterSettings, modifiers);
    }

    @Override
    public int modifyOutgoingClientEntitiesForPush(ClientBaseEvent eventToPush,
            IContentFilterSettings contentFilterSettings) {

        //find all matching modifiers by using the package prefix of the event class
        final String className = eventToPush.getClass().getName();
        List<IOutgoingClientEntityModifier<? extends ClientBaseEntity>> modifiers =
                outgoingClientEntityModifiers.stream()
                        //whitelist
                        .filter(modifier -> modifier.getPushEventPackagePrefixes().stream()
                                .anyMatch(prefix -> StringUtils.startsWith(className, prefix)))
                        .collect(Collectors.toList());
        return applyModifiers(eventToPush, contentFilterSettings, modifiers);
    }

    private int applyModifiers(Object rootEntity, IContentFilterSettings contentFilterSettings,
            List<IOutgoingClientEntityModifier<? extends ClientBaseEntity>> modifiers) {
        //walk through all client entities
        return ObjectGraphWalker
                .startAt(rootEntity)
                .walkAndCall(ClientBaseEntity.class, clientEntity -> {
                    for (IOutgoingClientEntityModifier<? extends ClientBaseEntity> modifier : modifiers) {
                        if (modifier.getType().isInstance(clientEntity)) {
                            //clientEntity.getClass() is a subtype of the modifiers type
                            if (!applyModifierToClientEntity(modifier, clientEntity, contentFilterSettings)) {
                                //modifier.modify has returned false -> no other modifiers for clientEntity.getClass() and supertypes are called
                                break;
                            }
                        }
                    }
                    return true; //continue walk on attributes of clientEntity
                });
    }

    //this helper method is needed since the wildcard type of IOutgoingClientEntityModifier cannot be inferred directly
    @SuppressWarnings("unchecked")
    private <E extends ClientBaseEntity> boolean applyModifierToClientEntity(
            IOutgoingClientEntityModifier<E> modifier, ClientBaseEntity clientEntity, IContentFilterSettings context) {
        //safe cast since E extends ClientBaseEntity
        return modifier.modify((E) clientEntity, context);
    }

}
