/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantTenantContractChangeAdditionalNotesRequest;
import de.fhg.iese.dd.platform.api.shared.app.clientevent.ClientAppVariantTenantContractChangeAdditionalNotesResponse;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.AppClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantTenantContractChangeAdditionalNotesConfirmation;
import de.fhg.iese.dd.platform.business.shared.app.events.AppVariantTenantContractChangeAdditionalNotesRequest;
import de.fhg.iese.dd.platform.business.shared.app.security.UpdateAppVariantTenantContractAction;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantTenantContractService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/app")
@Api(tags = {"shared.adminui.app"})
public class AppAdminUiEventController extends BaseController {

    @Autowired
    private IAppVariantTenantContractService appVariantTenantContractService;

    @Autowired
    private AppClientModelMapper appClientModelMapper;

    @ApiOperation(value = "Change additional notes of an AppVariantTenantContract")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_TENANT_CONTRACT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {UpdateAppVariantTenantContractAction.class})
    @PostMapping("/appVariantTenantContractChangeAdditionalNotesRequest")
    public ClientAppVariantTenantContractChangeAdditionalNotesResponse onChangeAdditionalNotesRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientAppVariantTenantContractChangeAdditionalNotesRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        getRoleService().decidePermissionAndThrowNotAuthorized(UpdateAppVariantTenantContractAction.class,
                currentPerson);

        AppVariantTenantContract contract =
                appVariantTenantContractService.findAppVariantTenantContractById(request.getContractId());
        AppVariantTenantContractChangeAdditionalNotesRequest changeRequest =
                AppVariantTenantContractChangeAdditionalNotesRequest.builder()
                        .appVariantTenantContract(contract)
                        .changedNotes(request.getChangedNotes())
                        .build();

        AppVariantTenantContractChangeAdditionalNotesConfirmation confirmation =
                notifyAndWaitForReply(AppVariantTenantContractChangeAdditionalNotesConfirmation.class, changeRequest);

        return ClientAppVariantTenantContractChangeAdditionalNotesResponse.builder()
                .updatedAppVariantTenantContract(
                        appClientModelMapper.toClientAppVariantTenantContract(
                                confirmation.getAppVariantTenantContract()))
                .build();
    }

}
