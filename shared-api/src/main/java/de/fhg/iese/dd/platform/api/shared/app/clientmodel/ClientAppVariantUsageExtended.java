/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Tahmid Ekram
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.clientmodel;

import java.util.List;

import javax.validation.constraints.NotNull;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientAppVariantUsageExtended {

    @NotNull
    private ClientAppVariant appVariant;
    private long lastUsage;
    @NotNull
    private List<ClientGeoArea> selectedGeoAreas;

}
