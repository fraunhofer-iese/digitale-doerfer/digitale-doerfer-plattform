/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2017 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.clientmodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.datamanagement.framework.IgnoreArchitectureViolation;

@JsonInclude(Include.NON_NULL)
@IgnoreArchitectureViolation(
        value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
        reason = "Only used for technical communication with push services, not with clients directly")
public class FCMPush {

    @JsonProperty("data")
    private FCMPushContent data;

    public FCMPushContent getData() {
        return data;
    }

    public void setData(FCMPushContent data) {
        this.data = data;
    }

    public FCMPush(ClientBaseEvent event, String message) {
        super();
        this.data = new FCMPushContent(event, message);
    }

    @JsonInclude(Include.NON_NULL)
    @IgnoreArchitectureViolation(
            value = IgnoreArchitectureViolation.ArchitectureRule.API_MODEL,
            reason = "Only used for technical communication with push services, not with clients directly")
    public static class FCMPushContent {

        //general
        @JsonProperty("event")
        private ClientBaseEvent event;

        @JsonProperty("eventName")
        private String eventName;

        //Android-"specific"
        @JsonProperty("message")
        private String message;

        FCMPushContent(ClientBaseEvent event, String message) {
            super();
            this.setEvent(event);
            this.message = message;
        }

        public ClientBaseEvent getEvent() {
            return event;
        }

        public void setEvent(ClientBaseEvent event) {
            this.event = event;
            this.eventName = event.getClass().getSimpleName();
        }

        public String getEventName() {
            return eventName;
        }

        public String getMessage() {
            return message;
        }
        
        public void setMessage(String message) {
            this.message = message;
        }
    }

}
