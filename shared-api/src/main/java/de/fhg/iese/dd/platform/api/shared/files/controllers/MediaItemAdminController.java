/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientTemporaryMediaItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryMediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IFileDownloadService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.net.URL;

@RestController
@RequestMapping("/administration/media/")
@Api(tags = {"admin", "shared.media.admin"})
public class MediaItemAdminController extends BaseController {

    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private IFileDownloadService fileDownloadService;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    @ApiOperation(value = "Downloads an image and creates a media item",
            notes = "Downloads an image and creates a media item for test purposes. " +
                    "Not intended to be used in production, since we can not check if the user owns the rights of the image.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/download")
    ClientTemporaryMediaItem downloadMediaItem(@RequestParam String url) {

        Person currentPerson = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, currentPerson);

        URL checkedUrl = fileDownloadService.createUrlAndCheck(url);

        TemporaryMediaItem temporaryMediaItem = mediaItemService.createTemporaryMediaItem(checkedUrl,
                FileOwnership.of(currentPerson, getAppVariantOrNull()));
        return fileClientModelMapper.toClientTemporaryMediaItem(temporaryMediaItem);
    }

}
