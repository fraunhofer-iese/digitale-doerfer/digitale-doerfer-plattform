/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import de.fhg.iese.dd.platform.business.shared.security.services.Action;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiAuthentication {

    ApiAuthenticationType value() default ApiAuthenticationType.OAUTH2;

    ApiAppVariantIdentification appVariantIdentification() default ApiAppVariantIdentification.NONE;

    /**
     * Roles that are required for this endpoint. If more then one role is given, the calling person must have
     * <b>at least one</b> role (OR-logic).
     * The roles from {@link #requiredActions()} and {@link #optionalActions()} are automatically listed and
     * do not need to be added here.
     * <h3>IMPORTANT</h3>
     * This check is not done automatically and needs to be implemented in the method!
     * The listed roles are only for documentation.
     */
    Class<? extends BaseRole<?>>[] requiredRoles() default {};

    /**
     * Actions that are required for this endpoint. If more then one action is given, the calling person must have the
     * permission for <b>all</b> actions (AND-logic).
     * <h3>IMPORTANT</h3>
     * This check is not done automatically and needs to be implemented in the method! The listed actions are only for
     * documentation.
     */
    Class<? extends Action<?>>[] requiredActions() default {};

    /**
     * Actions that are optional for this endpoint but change the result and behaviour.
     * <h3>IMPORTANT</h3>
     * This check is not done automatically and needs to be implemented in the method! The listed actions are only for
     * documentation.
     */
    Class<? extends Action<?>>[] optionalActions() default {};

    String notes() default "";

}
