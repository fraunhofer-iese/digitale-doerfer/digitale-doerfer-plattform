/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.ClientMinimizableEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvent;
import de.fhg.iese.dd.platform.api.framework.clientevent.PushEvents;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifier;
import de.fhg.iese.dd.platform.api.framework.modifiers.IOutgoingClientEntityModifierService;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.APNSPush;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushEventDocumentation;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.FCMPush;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.reflection.exceptions.ReflectionException;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushCategoryNotFoundException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushException;
import de.fhg.iese.dd.platform.business.shared.push.exceptions.PushMessageInvalidException;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessage;
import de.fhg.iese.dd.platform.business.shared.push.model.PushMessageMinimizable;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushCategoryService;
import de.fhg.iese.dd.platform.business.shared.push.services.IPushSendService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory.PushCategoryId;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ConfigurationBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Profile({"!test"})
@Service
class ClientPushService extends BaseService implements IClientPushService {

    @Autowired
    private IPushCategoryService pushCategoryService;

    @Autowired
    private IPushSendService pushSendService;

    @Autowired
    private IPersonBlockingService personBlockingService;

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @Autowired
    private IOutgoingClientEntityModifierService outgoingClientEntityModifierService;

    @Autowired
    private IReflectionService reflectionService;

    @Getter
    @Builder
    private static class ContentFilterSettings implements IContentFilterSettings {

        private final String callingPersonId;
        private final Set<String> blockedPersonIds;
        private final Set<String> flaggedEntityIds;

    }

    @Getter
    @Builder
    private static class PushEventDocumentation {

        private final Class<? extends ClientBaseEvent> clientEvent;
        private final Class<? extends BaseEvent> event;
        private final String categoryInternalName;
        private final PushCategory category;
        private final boolean loud;
        private final String notes;

    }

    @Override
    public void pushToPersonLoud(Person receiver, PushSendRequest pushSendRequest) throws PushException {

        lookupPushCategory(pushSendRequest);
        pushSendService.pushMessageToPersonLoud(receiver,
                toPushMessage(pushSendRequest, getContentFilterSettings(pushSendRequest.getCategory(), receiver)),
                pushSendRequest.getCategory());
    }

    @Override
    public void pushToPersonLoud(Person receiver, ClientBaseEvent event, String message, PushCategory category)
            throws PushException {

        pushToPersonLoud(receiver, PushSendRequest.builder()
                .event(event)
                .message(message)
                .category(category)
                .build());
    }

    @Override
    public void pushToPersonLoud(Person author, Person receiver, PushSendRequest pushSendRequest)
            throws PushException {

        lookupPushCategory(pushSendRequest);
        pushSendService.pushMessageToPersonLoud(author, receiver,
                toPushMessage(pushSendRequest, getContentFilterSettings(pushSendRequest.getCategory(), receiver)),
                pushSendRequest.getCategory());
    }

    @Override
    public void pushToPersonLoud(Person author, Person receiver, ClientBaseEvent event, String message,
            PushCategory category) throws PushException {

        pushToPersonLoud(author, receiver, PushSendRequest.builder()
                .event(event)
                .message(message)
                .category(category)
                .build());
    }

    @Override
    public void pushToPersonInLastUsedAppVariantLoud(Person receiver,
            PushSendRequestMultipleCategories pushSendRequest) throws PushException {

        pushSendService.pushMessageToPersonInLastUsedAppVariantLoud(receiver,
                toPushMessage(pushSendRequest, getContentFilterSettings(pushSendRequest.getCategories(), receiver)),
                pushSendRequest.getCategories());
    }

    @Override
    public void pushToPersonSilent(Person receiver, PushSendRequest pushSendRequest) throws PushException {

        lookupPushCategory(pushSendRequest);
        pushSendService.pushMessageToPersonSilent(receiver,
                toPushMessage(pushSendRequest, getContentFilterSettings(pushSendRequest.getCategory(), receiver)),
                Collections.singleton(pushSendRequest.getCategory()));
    }

    @Override
    public void pushToPersonSilent(Person receiver, ClientBaseEvent event, PushCategory category) throws PushException {

        pushToPersonSilent(receiver, PushSendRequest.builder()
                .event(event)
                .category(category)
                .build());
    }

    @Override
    public void pushToPersonSilent(Person receiver, PushSendRequestMultipleCategories pushSendRequest)
            throws PushException {

        pushSendService.pushMessageToPersonSilent(receiver,
                toPushMessage(pushSendRequest, getContentFilterSettings(pushSendRequest.getCategories(), receiver)),
                pushSendRequest.getCategories());
    }

    @Override
    public void pushToPersonSilent(Person receiver, ClientBaseEvent event, Collection<PushCategory> categories)
            throws PushException {

        pushToPersonSilent(receiver, PushSendRequestMultipleCategories.builder()
                .event(event)
                .categories(categories)
                .build());
    }

    @Override
    public void pushToGeoAreasLoud(@Nullable Person author, Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest) throws PushException {

        lookupPushCategory(pushSendRequest);
        pushSendService.pushMessageToGeoAreasLoud(author, geoAreas, onlyForSameHomeArea,
                //currently we can not determine the settings for all receivers in the geo area
                toPushMessage(pushSendRequest, IContentFilterSettings.EMPTY_SETTINGS), pushSendRequest.getCategory());
    }

    @Override
    public void pushToGeoAreaLoud(Person author, GeoArea geoArea, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest) throws PushException {

        pushToGeoAreasLoud(author, Set.of(geoArea), onlyForSameHomeArea, pushSendRequest);
    }

    @Override
    public void pushToGeoAreaLoud(GeoArea geoArea, PushSendRequest pushSendRequest) throws PushException {

        pushToGeoAreasLoud(null, Set.of(geoArea), false, pushSendRequest);
    }

    @Override
    public void pushToGeoAreaLoud(GeoArea geoArea, ClientBaseEvent event, String message, PushCategory category) throws PushException {

        pushToGeoAreasLoud(null, Set.of(geoArea), false, PushSendRequest.builder()
                .event(event)
                .message(message)
                .category(category)
                .build());
    }

    @Override
    public void pushToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest)
            throws PushException {

        lookupPushCategory(pushSendRequest);
        pushSendService.pushMessageToGeoAreasSilent(geoAreas, onlyForSameHomeArea,
                //currently we can not determine the settings for all receivers in the geo area
                toPushMessage(pushSendRequest, IContentFilterSettings.EMPTY_SETTINGS), pushSendRequest.getCategory());
    }

    @Override
    public void pushToGeoAreasSilent(Set<GeoArea> geoAreas, boolean onlyForSameHomeArea, ClientBaseEvent event, PushCategory category) throws PushException {

        pushToGeoAreasSilent(geoAreas, onlyForSameHomeArea, PushSendRequest.builder()
                .event(event)
                .category(category)
                .build());
    }

    @Override
    public void pushToGeoAreaSilent(GeoArea geoArea, PushSendRequest pushSendRequest) throws PushException {

        pushToGeoAreasSilent(Set.of(geoArea), false, pushSendRequest);
    }

    @Override
    public void pushToGeoAreaSilent(GeoArea geoArea, ClientBaseEvent event, PushCategory category)
            throws PushException {

        pushToGeoAreasSilent(Set.of(geoArea), false, PushSendRequest.builder()
                .event(event)
                .category(category)
                .build());
    }

    @Override
    public void pushToGeoAreaSilent(GeoArea geoArea, boolean onlyForSameHomeArea, PushSendRequest pushSendRequest)
            throws PushException {

        pushToGeoAreasSilent(Set.of(geoArea), onlyForSameHomeArea, pushSendRequest);
    }

    @Override
    public PushCategory findCategory(PushCategoryId pushCategoryId) {
        return pushCategoryService.findPushCategoryById(pushCategoryId);
    }

    @Override
    public List<ClientPushEventDocumentation> getPushEventDocumentation() {

        return getPushedEventDocumentations().stream()
                .map(this::toClientPushedEventDocumentation)
                .sorted(Comparator.comparing(ClientPushEventDocumentation::getClientEventName))
                .collect(Collectors.toList());
    }

    @Override
    public String getPushEventDocumentationPlain() {

        List<PushEventDocumentation> eventDocus = getPushedEventDocumentations();

        TreeMap<App, List<PushEventDocumentation>> pushedEventsByApp = eventDocus.stream()
                .filter(docu -> docu.getCategory() != null)
                //we group the documentation by app
                .collect(Collectors.groupingBy(docu -> docu.getCategory().getApp(),
                        //we want the tree map with sorted keys by app name
                        () -> new TreeMap<>(Comparator.comparing(App::getName)),
                        //the documentation per app should be sorted
                        Collectors.collectingAndThen(
                                Collectors.toList(), ClientPushService::toSortedDocuList)));

        //these are the documentations without app
        List<PushEventDocumentation> orphanPushEvents = eventDocus.stream()
                .filter(docu -> docu.getCategory() == null)
                .collect(Collectors.collectingAndThen(
                        Collectors.toList(), ClientPushService::toSortedDocuList));

        String horizontalLine = StringUtils.repeat("⎯", 173) + "\n";
        String lightHorizontalLine = StringUtils.repeat("⎯ ", 87) + "\n";
        StringBuilder summary = new StringBuilder();

        //print the documentations per app
        for (Map.Entry<App, List<PushEventDocumentation>> appToPushedEvents : pushedEventsByApp.entrySet()) {
            App app = appToPushedEvents.getKey();
            List<PushEventDocumentation> pushEvents = appToPushedEvents.getValue();
            summary.append(horizontalLine);
            summary.append(" ").append(app.getName()).append("\n");
            printDocu(summary, "Event", "Category", "loud", "Notes");
            summary.append(horizontalLine);

            for (Iterator<PushEventDocumentation> iterator = pushEvents.iterator(); iterator.hasNext(); ) {
                PushEventDocumentation docu = iterator.next();
                printDocu(summary,
                        String.format("%s%n->(%s)",
                                docu.getClientEvent().getSimpleName(),
                                docu.getEvent().getSimpleName()),
                        String.format("%s%n%s%n%s",
                                docu.getCategory().getName(),
                                docu.getCategoryInternalName(),
                                docu.getCategory().getId()),
                        docu.isLoud() ? "loud" : " --",
                        docu.getNotes());
                if (iterator.hasNext()) {
                    summary.append(lightHorizontalLine);
                }
            }
            summary.append(horizontalLine);
            summary.append("\n\n");
        }

        //and afterwards those without app
        if (!CollectionUtils.isEmpty(orphanPushEvents)) {
            summary.append(horizontalLine);
            summary.append(" Unknown app").append("\n");
            printDocu(summary, "Event", "Category", "loud", "Notes");
            summary.append(horizontalLine);

            for (Iterator<PushEventDocumentation> iterator = orphanPushEvents.iterator(); iterator.hasNext(); ) {
                PushEventDocumentation docu = iterator.next();
                printDocu(summary,
                        String.format("%s%n->(%s)",
                                docu.getClientEvent().getSimpleName(),
                                docu.getEvent().getSimpleName()),
                        docu.getCategoryInternalName(),
                        docu.isLoud() ? "loud" : " --",
                        docu.getNotes());
                if (iterator.hasNext()) {
                    summary.append(lightHorizontalLine);
                }
            }
            summary.append(horizontalLine);
        }
        return summary.toString();
    }

    private List<PushEventDocumentation> getPushedEventDocumentations() {
        //only scan the api layer for annotated methods
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .forPackage("de.fhg.iese.dd.platform.api")
                .setScanners(Scanners.SubTypes, Scanners.MethodsAnnotated));
        //these methods are annotated with a single documentation annotation
        Set<Method> methodsSingleAnnotated = reflections.getMethodsAnnotatedWith(PushEvent.class);
        //these are either "auto-wrapped" due to @Repeatable or explicitly wrapped
        //Reflections would not find them by just looking for the wrapped ones
        Set<Method> methodsRepeatableAnnotated = reflections.getMethodsAnnotatedWith(PushEvents.class);

        Set<Method> methodsAnnotated = new HashSet<>(methodsSingleAnnotated);
        methodsAnnotated.addAll(methodsRepeatableAnnotated);

        List<PushEventDocumentation> eventDocus = new ArrayList<>(methodsAnnotated.size());

        for (Method method : methodsAnnotated) {
            PushEvent[] pushEvents = method.getAnnotationsByType(PushEvent.class);
            for (PushEvent pushEvent : pushEvents) {
                eventDocus.addAll(toDocu(method, pushEvent));
            }
        }
        return eventDocus;
    }

    private void printDocu(StringBuilder summary, String name, String category, String loud, String notes) {

        List<String> nameSplit = splitLines(name, 40);
        List<String> categorySplit = splitLines(category, 45);
        List<String> notesSplit = splitLines(notes, 65);

        for (int i = 0; i == 0 || categorySplit.size() > i || notesSplit.size() > i || nameSplit.size() > i; i++) {

            summary.append(String.format(" %-40.40s │ %-45.45s │ %-4.4s │ %-65.65s%n",
                    nameSplit.size() > i ? nameSplit.get(i) : "",
                    categorySplit.size() > i ? categorySplit.get(i) : "",
                    i == 0 ? loud : "",
                    notesSplit.size() > i ? notesSplit.get(i) : ""));
        }
    }

    /**
     * Splits a text into lines of the given max length while trying to preserve the words.
     */
    private List<String> splitLines(String text, int maxLength) {

        // the first part should match trying to preserve the words, the second one when the words are too long
        Pattern wordWrapPattern =
                Pattern.compile("((\\S.{1," + (maxLength - 1) + "})(?:\\s|$))|(.{1," + maxLength + "})",
                        Pattern.MULTILINE);

        Matcher categoryMatcher = wordWrapPattern.matcher(text);
        List<String> split = new ArrayList<>();
        while (categoryMatcher.find()) {
            // the trim is a hack, because I am not very good at regex ;-)
            split.add(categoryMatcher.group().trim());
        }
        return split;
    }

    private static List<PushEventDocumentation> toSortedDocuList(List<PushEventDocumentation> unsortedList) {
        unsortedList.sort(
                Comparator.comparing((PushEventDocumentation docu) -> docu.getClientEvent().getSimpleName())
                        .thenComparing((PushEventDocumentation docu) -> docu.getEvent().getSimpleName()));
        return unsortedList;
    }

    private Collection<PushEventDocumentation> toDocu(Method method, PushEvent pushEvent) {

        Class<?> declaringClass = method.getDeclaringClass();

        final Class<? extends BaseEvent> eventClass;
        // BaseEvent is the default value for the annotation value
        if (pushEvent.event() != BaseEvent.class) {
            eventClass = pushEvent.event();
        } else {
            // since the annotation is typically on an event handling function there should be only one parameter
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length > 0) {
                Class<?> parameterType = parameterTypes[0];
                if (BaseEvent.class.isAssignableFrom(parameterType)) {
                    @SuppressWarnings("unchecked")
                    Class<? extends BaseEvent> castedParameterType = (Class<? extends BaseEvent>) parameterType;
                    eventClass = castedParameterType;
                } else {
                    eventClass = BaseEvent.class;
                }
            } else {
                eventClass = BaseEvent.class;
            }
        }
        Collection<PushCategory> categories;
        String categoryInternalName;
        if (StringUtils.isNotEmpty(pushEvent.categorySubject())) {
            categories = pushCategoryService.findPushCategoriesBySubject(pushEvent.categorySubject());
            categoryInternalName = "(subject: " + pushEvent.categorySubject() + ")";
        } else {
            PushCategory pushCategory = categoryIdNameToPushCategory(declaringClass, pushEvent.categoryIdName());
            categoryInternalName = StringUtils.substringAfterLast(pushEvent.categoryIdName(), ".");
            if (pushCategory != null) {
                categories = Collections.singleton(pushCategory);
            } else {
                categories = Collections.emptyList();
            }
        }

        if (CollectionUtils.isEmpty(categories)) {
            return Collections.singleton(PushEventDocumentation.builder()
                    .clientEvent(pushEvent.clientEvent())
                    .event(eventClass)
                    .categoryInternalName(categoryInternalName)
                    .loud(pushEvent.loud())
                    .notes(pushEvent.notes())
                    .build());
        } else {
            return categories.stream()
                    .map(category -> PushEventDocumentation.builder()
                            .clientEvent(pushEvent.clientEvent())
                            .event(eventClass)
                            .category(category)
                            .categoryInternalName(categoryInternalName)
                            .loud(pushEvent.loud())
                            .notes(pushEvent.notes())
                            .build()).collect(Collectors.toList());
        }
    }

    private PushCategory categoryIdNameToPushCategory(Class<?> declaringClass, String categoryIdName) {

        if (StringUtils.isEmpty(categoryIdName)) {
            return null;
        }
        //the assumption is that the categories are defined in the datamanagement layer
        String moduleName = reflectionService.getModuleName(declaringClass);
        String fullyQualifiedCategoryIdName =
                "de.fhg.iese.dd.platform.datamanagement." + moduleName + "." + categoryIdName;
        try {
            PushCategoryId categoryId =
                    reflectionService.findConstantByFullyQualifiedName(fullyQualifiedCategoryIdName,
                            PushCategoryId.class);

            return pushCategoryService.findPushCategoryById(categoryId);
        } catch (ReflectionException e) {
            log.error("Failed to get push category: {}", e.getMessage());
            return null;
        } catch (PushCategoryNotFoundException e) {
            log.error("Failed to find push category: {}", e.getMessage());
            return null;
        }
    }

    /**
     * Gets the filter settings for the receiver of the push message for applying {@link
     * IOutgoingClientEntityModifier#modify(ClientBaseEntity, IContentFilterSettings)}s.
     */
    private IContentFilterSettings getContentFilterSettings(PushCategory category, Person receiver) {

        return ContentFilterSettings.builder()
                .callingPersonId(BaseEntity.getIdOf(receiver))
                .blockedPersonIds(personBlockingService.getBlockedNotDeletedPersonIds(category.getApp(), receiver))
                .flaggedEntityIds(userGeneratedContentFlagService.findFlaggedEntityIdsByFlagCreator(receiver))
                .build();
    }

    private IContentFilterSettings getContentFilterSettings(Collection<PushCategory> categories, Person receiver) {

        return ContentFilterSettings.builder()
                .callingPersonId(BaseEntity.getIdOf(receiver))
                .blockedPersonIds(categories.stream()
                        //it might be multiple apps in these categories
                        .map(PushCategory::getApp)
                        //every app needs to be handled only once
                        .distinct()
                        //we just combine all the blocked person ids
                        .flatMap(app -> personBlockingService.getBlockedNotDeletedPersonIds(app, receiver).stream())
                        .collect(Collectors.toSet()))
                .flaggedEntityIds(userGeneratedContentFlagService.findFlaggedEntityIdsByFlagCreator(receiver))
                .build();
    }

    private PushMessage toPushMessage(BasePushSendRequest pushSendRequest, IContentFilterSettings contentFilterSettings)
            throws PushMessageInvalidException {

        ClientBaseEvent event = pushSendRequest.getEvent();
        PushMessage pushMessage;
        if (event instanceof ClientMinimizableEvent) {
            pushMessage = new PushMessageMinimizable(
                    // we can not call toPushMessage here, since we might have infinite recursion
                    // if the minimized event is also a minimizable event
                    () -> toPushMessageInternal(new PushMessage(),
                            pushSendRequest.toBuilder()
                                    .event(((ClientMinimizableEvent) event).toMinimizedEvent())
                                    .build(),
                            contentFilterSettings));
        } else {
            pushMessage = new PushMessage();
        }
        return toPushMessageInternal(pushMessage, pushSendRequest, contentFilterSettings);
    }

    private void lookupPushCategory(PushSendRequest pushSendRequest) {
        if (pushSendRequest.getCategoryId() != null) {
            pushSendRequest.setCategory(findCategory(pushSendRequest.getCategoryId()));
        }
    }

    private PushMessage toPushMessageInternal(PushMessage pushMessage, BasePushSendRequest pushSendRequest,
            IContentFilterSettings contentFilterSettings) {

        ClientBaseEvent event = pushSendRequest.getEvent();
        String message = pushSendRequest.getMessage();

        applyOutgoingClientEntityModifiers(event, contentFilterSettings);

        try {
            pushMessage.setJsonMessageLoudApns(
                    defaultJsonWriter().writeValueAsString(
                            new APNSPush(false, message, event, pushSendRequest.getBadgeCount())));
            pushMessage.setJsonMessageSilentApns(
                    defaultJsonWriter().writeValueAsString(
                            new APNSPush(true, null, event, pushSendRequest.getBadgeCount())));
            pushMessage.setJsonMessageLoudFcm(
                    defaultJsonWriter().writeValueAsString(new FCMPush(event, message)));
            pushMessage.setJsonMessageSilentFcm(
                    defaultJsonWriter().writeValueAsString(new FCMPush(event, null)));
            pushMessage.setLogMessage(toLogMessage(event));
            return pushMessage;
        } catch (JsonProcessingException e) {
            throw new PushMessageInvalidException("Push message for event " + event + " was invalid.", e);
        }
    }

    /**
     * Applies all {@link IOutgoingClientEntityModifier#modify(ClientBaseEntity, IContentFilterSettings)}s that match
     * with the package name of the event.
     */
    private void applyOutgoingClientEntityModifiers(ClientBaseEvent event,
            IContentFilterSettings contentFilterSettings) {

        long startTime = 0L;
        if (log.isTraceEnabled()) {
            startTime = System.nanoTime();
        }

        int inspectedObjectCount =
                outgoingClientEntityModifierService.modifyOutgoingClientEntitiesForPush(event, contentFilterSettings);

        if (log.isTraceEnabled()) {
            log.trace("modifying push event took {} ms, inspected {} objects",
                    TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime), inspectedObjectCount);
        }
    }

    private String toLogMessage(ClientBaseEvent event) {
        return StringUtils.remove(StringUtils.removeStart(
                event.getClass().getName(), "de.fhg.iese.dd.platform.api."), "clientevent.");
    }

    private ClientPushEventDocumentation toClientPushedEventDocumentation(PushEventDocumentation docu) {
        // special requirement for AdministrierBar: No null-fields
        final ClientPushEventDocumentation.ClientPushEventDocumentationBuilder clientDocuBuilder =
                ClientPushEventDocumentation.builder()
                        .eventName(StringUtils.defaultString(docu.getEvent().getSimpleName()))
                        .clientEventName(StringUtils.defaultString(docu.getClientEvent().getSimpleName()))
                        .categoryInternalName(StringUtils.defaultString(docu.getCategoryInternalName()))
                        .loud(docu.isLoud())
                        .notes(StringUtils.defaultString(docu.getNotes()));

        if (docu.getCategory() != null) {
            clientDocuBuilder.categoryId(StringUtils.defaultString(docu.getCategory().getId()))
                    .categoryName(StringUtils.defaultString(docu.getCategory().getName()))
                    .categoryDescription(StringUtils.defaultString(docu.getCategory().getDescription()))
                    .categorySubject(StringUtils.defaultString(docu.getCategory().getSubject()))
                    .appId(StringUtils.defaultString(docu.getCategory().getApp().getId()))
                    .appIdentifier(StringUtils.defaultString(docu.getCategory().getApp().getAppIdentifier()))
                    .appName(StringUtils.defaultString(docu.getCategory().getApp().getName()));
        }

        return clientDocuBuilder.build();
    }

}
