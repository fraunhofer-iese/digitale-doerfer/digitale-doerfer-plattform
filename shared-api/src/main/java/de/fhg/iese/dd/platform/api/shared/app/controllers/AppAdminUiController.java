/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2023 Johannes Schneider, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooLongException;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientTenant;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.TenantClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.*;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantTenantContractService;
import de.fhg.iese.dd.platform.business.shared.security.ListAppConfigurationAction;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/adminui/app")
@Api(tags = {"shared.adminui.app"})
public class AppAdminUiController extends BaseController {

    @Autowired
    private IAppService appService;
    @Autowired
    private IAppVariantTenantContractService appVariantTenantContractService;
    @Autowired
    private AppClientModelMapper appClientModelMapper;
    @Autowired
    private TenantClientModelMapper tenantClientModelMapper;

    @ApiOperation(value = "App variant details")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping(value = "/appVariant/{appVariantId}")
    public ClientAppVariant getAppVariantById(
            @PathVariable String appVariantId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        AppVariant appVariant = appService.findAppVariantById(appVariantId);

        return appClientModelMapper.toClientAppVariant(appVariant);
    }

    @ApiOperation(value = "App variant details with secrets",
            notes = "Returns an app variant with its api keys and basic auth information")
    @ApiExceptions({ClientExceptionType.APP_VARIANT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping(value = "/appVariant/{appVariantId}/secrets")
    public ClientAppVariantWithSecrets getAppVariantWithSecretsById(
            @PathVariable String appVariantId) {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        AppVariant appVariant = appService.findAppVariantById(appVariantId);

        return appClientModelMapper.toClientAppVariantWithSecrets(appVariant);
    }

    @Deprecated
    @ApiOperation(value = "App variants by tenant",
            notes = "This is a legacy endpoint, it does not reflect the complexity of the current configuration")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping("appVariantByTenant")
    public List<ClientTenantAndAppVariantEntry> appVariantByTenant() {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        //build list
        final List<ClientTenantAndAppVariantEntry> clientTenantAndAppVariantEntries = new ArrayList<>();
        final List<AppVariant> appVariants = appService.findAllAppVariants();
        for (AppVariant appVariant : appVariants) {
            final Set<Tenant> availableTenants = appService.getAvailableTenants(appVariant);
            for (Tenant tenant : availableTenants) {
                clientTenantAndAppVariantEntries.add(
                        appClientModelMapper.toClientTenantAndAppVariantEntry(appVariant, tenant));
            }
        }

        //sort list (German sorting!)
        final Collator germanCollator = Collator.getInstance(Locale.GERMAN);
        clientTenantAndAppVariantEntries.sort(
                Comparator.comparing(ClientTenantAndAppVariantEntry::getTenantName, germanCollator)
                        .thenComparing(ClientTenantAndAppVariantEntry::getAppVariantName, germanCollator));

        return clientTenantAndAppVariantEntries;
    }

    @ApiOperation(value = "Tenants related to app variants and contracts",
            notes = """
                    Get all tenants with optional filtering, the filter are connected this way:
                    (appVariantIds || contractIds) && search
                    The ids are not checked, not existing ids are ignored""")
    @ApiExceptions({
            ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT,
            ClientExceptionType.SEARCH_PARAMETER_TOO_LONG
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping("/tenant")
    public Page<ClientTenant> getTenantsByContractOrAppVariant(
            @RequestParam(required = false)
            @ApiParam(value = "there must be a contract for the tenant for one of the app variants")
                    List<String> appVariantIds,
            @RequestParam(required = false)
            @ApiParam(value = "there must be a contract for the tenant")
                    List<String> contractIds,
            @RequestParam(required = false)
            @ApiParam(value = "name of the tenant")
                    String search,
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of entities",
                    defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of entities returned in a page",
                    defaultValue = "10") int count
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        checkPageAndCountValues(page, count);

        return appVariantTenantContractService.getTenantsByContractOrAppVariant(
                        filterIds(contractIds),
                        filterIds(appVariantIds),
                        trimAndCheckOptionalSearch(search),
                        PageRequest.of(page, count, Sort.Direction.ASC, "name"))
                .map(tenantClientModelMapper::toClientTenant);
    }

    @ApiOperation(value = "App variants related to tenants and contracts",
            notes = """
                    Get all app variants with optional filtering, the filter are connected this way:
                    (tenantIds || contractIds) && search
                    The ids are not checked, not existing ids are ignored""")
    @ApiExceptions({
            ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT,
            ClientExceptionType.SEARCH_PARAMETER_TOO_LONG
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping("/appVariant")
    public Page<ClientAppVariant> getAppVariantsByTenantOrContract(
            @RequestParam(required = false)
            @ApiParam(value = "there must be a contract for the app variant with one of the tenants")
                    List<String> tenantIds,
            @RequestParam(required = false)
            @ApiParam(value = "there must be a contract for the app variant")
                    List<String> contractIds,
            @RequestParam(required = false)
            @ApiParam(value = "name or app variant identifier")
                    String search,
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of entities",
                    defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of entities returned in a page",
                    defaultValue = "10") int count
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        return appVariantTenantContractService.getAppVariantsByTenantOrContract(
                        filterIds(tenantIds),
                        filterIds(contractIds),
                        trimAndCheckOptionalSearch(search),
                        PageRequest.of(page, count, Sort.Direction.ASC, "appVariantIdentifier"))
                .map(appClientModelMapper::toClientAppVariant);
    }

    @ApiOperation(value = "Contract related to tenants and app variants",
            notes = """
                    Get all contracts with optional filtering, the filter are connected this way:
                    (tenantIds || appVariantIds) && search
                    The ids are not checked, not existing ids are ignored""")
    @ApiExceptions({
            ClientExceptionType.SEARCH_PARAMETER_TOO_SHORT,
            ClientExceptionType.SEARCH_PARAMETER_TOO_LONG
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListAppConfigurationAction.class)
    @GetMapping("/contract")
    public Page<ClientAppVariantTenantContract> getContractsByTenantOrAppVariant(
            @RequestParam(required = false)
            @ApiParam(value = "the contract must be with one of the tenants")
                    List<String> tenantIds,
            @RequestParam(required = false)
            @ApiParam(value = "the contract must be for one the app variants")
                    List<String> appVariantIds,
            @RequestParam(required = false)
            @ApiParam(value = "notes")
                    String search,
            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "page number of the paged list of entities",
                    defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "maximum number of entities returned in a page",
                    defaultValue = "10") int count
    ) {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListAppConfigurationAction.class,
                getCurrentPersonNotNull());

        return appVariantTenantContractService.getContractsByTenantOrAppVariant(
                        filterIds(tenantIds),
                        filterIds(appVariantIds),
                        trimAndCheckOptionalSearch(search),
                        PageRequest.of(page, count, Sort.Direction.ASC, "created", "id"))
                .map(appClientModelMapper::toClientAppVariantTenantContract);
    }

    @Nullable
    private String trimAndCheckOptionalSearch(String search) {
        String trimmedSearch = StringUtils.trim(search);
        if (StringUtils.isNotEmpty(search)) {
            if (StringUtils.length(trimmedSearch) < 3) {
                throw new SearchParameterTooShortException("search", 3);
            }
            if (StringUtils.length(trimmedSearch) > 100) {
                throw new SearchParameterTooLongException("search", 100);
            }
        }
        return trimmedSearch;
    }

    private Set<String> filterIds(List<String> rawIds) {
        if (CollectionUtils.isEmpty(rawIds)) {
            return Collections.emptySet();
        }
        return rawIds.stream()
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toSet());
    }

}
