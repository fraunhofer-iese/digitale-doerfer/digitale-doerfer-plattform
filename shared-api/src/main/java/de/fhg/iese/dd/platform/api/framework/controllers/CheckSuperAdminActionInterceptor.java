/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Checks if the person calling the endpoint is allowed to execute a {@link SuperAdminAction}.
 * <p>
 * It is registered at InterceptorConfig at the rest-application.
 */
public class CheckSuperAdminActionInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws IOException {
        //We let OPTIONS requests pass in order to enable CORS request of web clients
        if (!"OPTIONS".equalsIgnoreCase(request.getMethod())) {
            if (handler instanceof HandlerMethod handlerMethod) {
                if (handlerMethod.getBean() instanceof BaseController controller) {
                    //throwing an exception here is okay, this gets translated into an exception entity
                    controller.getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class,
                            controller.getCurrentPersonNotNull());
                    return true;
                }
            }
            //somehow we can not just throw an exception here, since it causes 500
            //it seems to be that the custom error handler is not used here
            response.sendError(403, "Not authorized");
            return false;
        }
        return true;
    }

}
