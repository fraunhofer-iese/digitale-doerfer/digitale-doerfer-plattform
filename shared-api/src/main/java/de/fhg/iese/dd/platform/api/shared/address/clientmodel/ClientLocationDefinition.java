/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.address.clientmodel;

import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@ApiModel(description = "When using this type, fill at least one of the location fields or all the address fields. " +
        "The data provided here is looked up by a geocoding service to convert it to an " +
        "address. You can either provide a locationLookupString (which is looked up) or a full address (with all " +
        "address* fields being present). If no locationLookupString and address is provided, but a name, the name is " +
        "used for look up. If you provide GPS coordinates, these coordinates will be used, even if the look up " +
        "finds other coordinates.")
public class ClientLocationDefinition {

    private String locationName;
    private String locationLookupString;
    private ClientGPSLocation gpsLocation;
    private String addressStreet;
    private String addressZip;
    private String addressCity;

}
