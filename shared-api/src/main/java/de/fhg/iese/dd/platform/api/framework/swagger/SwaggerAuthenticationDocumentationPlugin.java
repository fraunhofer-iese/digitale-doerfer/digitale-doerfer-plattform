/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import com.google.common.base.Optional;
import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.business.shared.security.services.Action;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.RelatedEntityIsNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.service.contexts.OperationContext;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Extends the swagger documentation with the exceptions documented in the {@link ApiExceptions} annotations.
 * <p>
 * Unfortunately the plugin system of springfox is rather weak. Thus a small portion of code was copied from {@link
 * springfox.documentation.swagger.readers.operation.OperationNotesReader} because otherwise the notes would be
 * overwritten by this plugin.
 */
@Component
@Order(1)
public class SwaggerAuthenticationDocumentationPlugin extends BaseSwaggerPlugin {

    private static final String DESCRIPTION_OPTIONAL_APP_VARIANT_HEADER =
            "Header for **strongly suggested** identification of the calling app variant.\n" +
                    "The header is only required if the mapping of the oauth client identifier in the access token " +
                    "to an app variant is ambiguous. " +
                    "In this case only one additional identification is necessary, not both.";
    private static final String DESCRIPTION_ONE_OF_REQUIRED_APP_VARIANT_HEADER =
            "Header for **required** identification of the calling app variant.\n" +
                    "The header is only required if the mapping of the oauth client identifier in the access token " +
                    "to an app variant is ambiguous. " +
                    "In this case only one additional identification is necessary, not both.";
    private static final String DESCRIPTION_REQUIRED_API_KEY =
            "Header for **required** identification of the calling app variant.\n" +
                    "The header is required for a strong authentication of the app variant.";

    private static final ApiAuthentication DEFAULT_API_AUTHENTICATION = new ApiAuthentication() {

        @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
        //rawtypes is required to prevent the compiler at jenkins to generate warnings
        private final Class<? extends BaseRole<?>>[] DEFAULT_REQUIRED_ROLES = new Class[0];

        @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
        //rawtypes is required to prevent the compiler at jenkins to generate warnings
        private final Class<? extends Action<?>>[] DEFAULT_ACTIONS = new Class[0];

        @Override
        public Class<? extends Annotation> annotationType() {
            return ApiAuthentication.class;
        }

        @Override
        public ApiAuthenticationType value() {
            return ApiAuthenticationType.OAUTH2;
        }

        @Override
        public ApiAppVariantIdentification appVariantIdentification() {
            return ApiAppVariantIdentification.NONE;
        }

        @Override
        public Class<? extends BaseRole<?>>[] requiredRoles() {
            return DEFAULT_REQUIRED_ROLES;
        }

        @Override
        public Class<? extends Action<?>>[] requiredActions() {
            return DEFAULT_ACTIONS;
        }

        @Override
        public Class<? extends Action<?>>[] optionalActions() {
            return DEFAULT_ACTIONS;
        }

        @Override
        public String notes() {
            return null;
        }

    };

    @Autowired
    private IRoleService roleService;

    @Override
    public void extendDocumentation(OperationContext context) {
        if(isNotDDClass(context)){
            return;
        }
        ApiAuthentication apiAuthentication = getApiAuthentication(context);
        String autShortInfo;
        StringBuilder autLongInfo = new StringBuilder("\n\n---\n\n## ");
        switch (apiAuthentication.value()) {
            case OAUTH2:
                autShortInfo = "🔏 oauth2";
                autLongInfo.append("Authenticated via OAuth 2.0");
                break;
            case OAUTH2_OPTIONAL:
                autShortInfo = "🔏 / 🆓 oauth2 or public";
                autLongInfo.append("Authenticated via OAuth 2.0 or public");
                autLongInfo.append("\n\n");
                autLongInfo.append("Can be called authenticated or anonymous, which changes the actual result. ");
                autLongInfo.append("Deprecated and will be replaced with an always authenticated endpoint.");
                break;
            case API_KEY:
                setParameterRequired(context, "apiKey", true, DESCRIPTION_REQUIRED_API_KEY);
                autShortInfo = "🔐 api key";
                autLongInfo.append("Authenticated via API key");
                break;
            case OAUTH2_AND_API_KEY:
                setParameterRequired(context, "apiKey", true, DESCRIPTION_REQUIRED_API_KEY);
                autShortInfo = "🔏➕🔐 oauth2 and api key";
                autLongInfo.append("Authenticated via API key");
                break;
            case ONE_TIME_TOKEN:
                autShortInfo = "🔑 one time token";
                autLongInfo.append("Authenticated via one time token");
                break;
            case PUBLIC:
                autShortInfo = "🆓 public";
                autLongInfo.append("No authentication required");
                break;
            default:
                autShortInfo = "\u2000\uD83D\uDD0F unknown";
                autLongInfo.append("Unknown, implement at ").append(this.getClass().getName());
                break;
        }
        String appVariantIdentifierInfo = calculateAppVariantInfo(context, apiAuthentication);
        List<Class<? extends BaseRole<?>>> actionRoles = calculateActionsRoles(apiAuthentication);
        String actionsInfo = calculateActionsInfo(apiAuthentication);
        String rolesInfo = calculateRolesInfo(apiAuthentication, actionRoles);
        if (StringUtils.isNotEmpty(appVariantIdentifierInfo)) {
            autLongInfo.append("\n\n");
            autLongInfo.append(appVariantIdentifierInfo);
        }
        if (StringUtils.isNotEmpty(apiAuthentication.notes())) {
            autLongInfo.append("\n\n");
            autLongInfo.append(apiAuthentication.notes());
        }
        if (StringUtils.isNotEmpty(actionsInfo)) {
            autLongInfo.append("\n\n");
            autLongInfo.append(actionsInfo);
        }
        if (StringUtils.isNotEmpty(rolesInfo)) {
            autLongInfo.append("\n\n");
            autLongInfo.append(rolesInfo);
        }
        appendToOperationField(context, "summary", "\u2000" + autShortInfo);
        appendToOperationField(context, "notes", autLongInfo.toString());
    }

    private String calculateAppVariantInfo(OperationContext context, ApiAuthentication apiAuthentication) {
        switch (apiAuthentication.appVariantIdentification()) {
            case NONE:
                setParameterRequired(context, "appVariantIdentifier", false,
                        DESCRIPTION_OPTIONAL_APP_VARIANT_HEADER);
                setParameterRequired(context, "apiKey", false,
                        DESCRIPTION_OPTIONAL_APP_VARIANT_HEADER);
                return "Parameter **`appVariantIdentifier`** or **`apiKey`** should be provided, " +
                        "if oauth client in access token does not uniquely identify the calling app variant.";
            case IDENTIFICATION_ONLY:
                setParameterRequired(context, "appVariantIdentifier", false,
                        DESCRIPTION_ONE_OF_REQUIRED_APP_VARIANT_HEADER);
                setParameterRequired(context, "apiKey", false,
                        DESCRIPTION_ONE_OF_REQUIRED_APP_VARIANT_HEADER);
                return "One of the parameters **`appVariantIdentifier`** or **`apiKey`** is required, " +
                        "if oauth client in access token does not uniquely identify the calling app variant.";
            case AUTHENTICATION:
                removeParameter(context, "appVariantIdentifier");
                setParameterRequired(context, "apiKey", true, DESCRIPTION_REQUIRED_API_KEY);
                return "Parameter **`apiKey`** is required to authenticate the calling app variant.";
            default:
                return "Unknown, implement at " + this.getClass().getName();
        }
    }

    private String calculateRolesInfo(ApiAuthentication apiAuthentication,
            List<Class<? extends BaseRole<?>>> actionRoles) {
        if (apiAuthentication.requiredRoles().length > 0 || !actionRoles.isEmpty()) {
            return Stream.concat(Arrays.stream(apiAuthentication.requiredRoles()), actionRoles.stream())
                    .distinct()
                    .map(role -> roleService.getRole(role))
                    .map(role -> String.format("|`%s`|`%s`|%s|%s|",
                            role.getKey(),
                            RelatedEntityIsNull.class.isAssignableFrom(role.getRelatedEntityClass()) ? "-" :
                                    role.getRelatedEntityClass().getSimpleName(),
                            role.getDisplayName(),
                            role.getDescription()))
                    .collect(Collectors.joining("\n",
                            """


                                    ### Required Roles
                                    |Key|Related Entity Class|Name|Description|
                                    | ---- | ---- | ---- | ---- |
                                    """, ""));
        } else {
            return null;
        }
    }

    private List<Class<? extends BaseRole<?>>> calculateActionsRoles(ApiAuthentication apiAuthentication) {
        return Stream.concat(Arrays.stream(apiAuthentication.requiredActions()),
                Arrays.stream(apiAuthentication.optionalActions()))
                .map(action -> roleService.getAction(action))
                .flatMap(action -> action.getPermittedRoles().stream())
                .distinct()
                .collect(Collectors.toList());
    }

    private String calculateActionsInfo(ApiAuthentication apiAuthentication) {
        StringBuilder actionsInfo = new StringBuilder();
        if (apiAuthentication.requiredActions().length > 0 || apiAuthentication.optionalActions().length > 0) {
            actionsInfo.append("\n\n### Actions\n");
            actionsInfo.append("|Action|Required|Description|Permission Description|\n");
            actionsInfo.append("| ---- | :--: | ---- | ---- |\n");
        }
        addActionsInfo(actionsInfo, apiAuthentication.requiredActions(), true);
        addActionsInfo(actionsInfo, apiAuthentication.optionalActions(), false);
        if (actionsInfo.length() > 0) {
            return actionsInfo.toString();
        } else {
            return null;
        }
    }

    private void addActionsInfo(StringBuilder actionsInfo, Class<? extends Action<?>>[] actions, boolean required) {
        if (actions.length > 0) {
            actionsInfo.append(Arrays.stream(actions)
                    .map(action -> roleService.getAction(action))
                    .map(action -> String.format("|`%s`|%s|%s|%s|",
                            action.getClass().getSimpleName(),
                            required
                                    ? "yes"
                                    : "-",
                            Objects.toString(action.getActionDescription(),
                                            "no description")
                                    .replace("\n", "<br>"),
                            Objects.toString(action.getPermissionDescription(roleService::getRole),
                                            "no description")
                                    .replace("\n", "<br>")))
                    .collect(Collectors.joining("\n")))
                    .append("\n");
        }
    }

    static ApiAuthentication getApiAuthentication(OperationContext context) {
        Optional<ApiAuthentication> apiAuthenticationOptional = context.findAnnotation(ApiAuthentication.class);
        ApiAuthentication apiAuthentication;
        if (apiAuthenticationOptional.isPresent()) {
            apiAuthentication = apiAuthenticationOptional.get();
        } else {
            apiAuthentication = DEFAULT_API_AUTHENTICATION;
        }
        return apiAuthentication;
    }

}
