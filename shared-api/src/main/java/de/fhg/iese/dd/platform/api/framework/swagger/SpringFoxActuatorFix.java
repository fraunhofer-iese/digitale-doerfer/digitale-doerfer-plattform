/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2023 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import static springfox.documentation.spi.service.contexts.Orderings.byPatternsCondition;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.mvc.condition.PathPatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;

import lombok.extern.log4j.Log4j2;
import springfox.documentation.spi.service.RequestHandlerProvider;
import springfox.documentation.spring.web.WebMvcRequestHandler;
import springfox.documentation.spring.web.plugins.DocumentationPluginsBootstrapper;
import springfox.documentation.spring.web.plugins.WebMvcRequestHandlerProvider;
import springfox.documentation.spring.web.readers.operation.HandlerMethodResolver;

/**
 * Fixes SpringFox to work with PathPatterns for Actuator Endpoints
 * <p/>
 * Code mainly copied from https://github.com/springfox/springfox/issues/3462
 */
@Component
@Log4j2
class SpringFoxActuatorFix {

    @Bean
    public InitializingBean removeSpringfoxHandlerProvider(DocumentationPluginsBootstrapper bootstrapper) {
        return () -> {
            //the field to remove the faulty handler is not accessible, so we need to work with reflection magic
            List<?> handlerProviders = getValue(bootstrapper, "handlerProviders", List.class);
            if (handlerProviders != null) {
                handlerProviders.removeIf(WebMvcRequestHandlerProvider.class::isInstance);
            }
        };
    }

    @Bean
    public RequestHandlerProvider customRequestHandlerProvider(Optional<ServletContext> servletContext,
            HandlerMethodResolver methodResolver, List<RequestMappingInfoHandlerMapping> handlerMappings) {

        return () -> handlerMappings.stream()
                .filter(mapping -> !"IntegrationRequestMappingHandlerMapping"
                        .equals(mapping.getClass()
                                .getSimpleName()))
                .map(mapping -> mapping.getHandlerMethods().entrySet())
                .flatMap(Set::stream)
                .map(entry -> new WebMvcRequestHandler(methodResolver, tweakInfo(entry.getKey()), entry.getValue()))
                .sorted(byPatternsCondition())
                .collect(Collectors.toList());
    }

    private RequestMappingInfo tweakInfo(RequestMappingInfo info) {
        PathPatternsRequestCondition pathPatternsCondition = info.getPathPatternsCondition();
        if (pathPatternsCondition == null) {
            return info;
        }
        Set<String> patternValues = pathPatternsCondition.getPatternValues();
        if (CollectionUtils.isEmpty(patternValues)) {
            return info;
        }
        return info.mutate()
                .options(new RequestMappingInfo.BuilderConfiguration())
                .paths(patternValues.toArray(new String[]{}))
                .build();
    }

    private <T> T getValue(Object object, String fieldName, Class<T> expectedType) {
        Field field = org.springframework.util.ReflectionUtils.findField(object.getClass(), fieldName, expectedType);
        if (field != null) {
            org.springframework.util.ReflectionUtils.makeAccessible(field);
            Object fieldContent = org.springframework.util.ReflectionUtils.getField(field, object);
            if (fieldContent == null) {
                return null;
            }
            if (expectedType.isAssignableFrom(fieldContent.getClass())) {
                @SuppressWarnings("unchecked")
                T result = (T) fieldContent;
                return result;
            } else {
                log.error("Field {} is of wrong type", fieldName);
            }
        } else {
            log.error("Could not get field {}", fieldName);
        }
        return null;
    }

}
