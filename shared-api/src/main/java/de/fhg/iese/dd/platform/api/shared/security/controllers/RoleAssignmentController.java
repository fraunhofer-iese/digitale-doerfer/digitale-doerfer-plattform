/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleAssignment;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleInformation;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.RoleClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.security.ListRolesAction;
import de.fhg.iese.dd.platform.business.shared.security.services.Permission;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/roleAssignment")
@Api(tags = {"participants.roleAssignment"})
public class RoleAssignmentController extends BaseController {

    @Autowired
    private RoleClientModelMapper roleClientModelMapper;

    @ApiOperation(value = "Get all available roles",
            notes = """
                    If the current user is SUPER_ADMIN, GLOBAL_USER_ADMIN, USER_ADMIN OR ROLE_MANAGER, all available roles are returned.
                    If the current user has none of the above, all roles which are currently assigned to the user are returned.
                    Returns an empty list if no role is assigned to the user.""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            optionalActions = {ListRolesAction.class}
    )
    @GetMapping("/roles")
    public List<ClientRoleInformation> getAllAvailableRoles() {

        Person currentPerson = getCurrentPersonNotNull();
        Permission listRolesPermission = getRoleService().decidePermission(ListRolesAction.class, currentPerson);
        if (listRolesPermission.isAllowed()) {
            return getRoleService().getAllRoles()
                    .stream()
                    .map(roleClientModelMapper::toClientRoleInformation)
                    .sorted(Comparator.comparing(ClientRoleInformation::getKey))
                    .collect(Collectors.toList());
        } else {
            return getRoleService().getCurrentExtendedRoleAssignments(currentPerson)
                    .stream()
                    .map(roleClientModelMapper::toClientRoleInformation)
                    .sorted(Comparator.comparing(ClientRoleInformation::getKey))
                    .collect(Collectors.toList());
        }
    }

    @ApiOperation(value = "Get all role assignments of the current logged in user",
            notes = "Note that some role assignments have relatedEntityId==null. The other assignments have the " +
                    "entity name and tenant filled.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("own")
    public List<ClientRoleAssignment> getOwnRoleAssignments() {

        final Person currentPerson = getCurrentPersonNotNull();
        return getRoleService().getCurrentExtendedRoleAssignments(currentPerson)
                .stream()
                .map(roleClientModelMapper::toClientRoleAssignment)
                .sorted(Comparator.comparing(ClientRoleAssignment::getRoleKey).thenComparing(
                        ClientRoleAssignment::getRelatedEntityId))
                .collect(Collectors.toList());
    }

}
