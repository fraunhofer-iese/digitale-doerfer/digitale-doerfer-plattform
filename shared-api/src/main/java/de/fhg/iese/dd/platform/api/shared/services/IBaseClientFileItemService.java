/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.services;

import java.util.Collection;
import java.util.List;

import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.IClientFileItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.TemporaryFileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItemPlaceHolder;

interface IBaseClientFileItemService<F extends BaseFileItem, P extends BaseFileItemPlaceHolder<F>> {

    /**
     * Converts a list of {@link IClientFileItemPlaceHolder}s that reference either an existing file item or an uploaded
     * temporary file item to a list of {@link BaseFileItemPlaceHolder}s that contain the actual file items and
     * temporary file items.
     *
     * @param clientItemPlaceHolders placeholders that reference either an existing file item or a temporary file item
     * @param existingItems          file items that can be referenced in the placeholders
     * @param temporaryItemOwner     person that owns the temporary file items
     *
     * @return list of placeholders for file items with either an existing file item or a temporary file item
     *
     * @throws InvalidEventAttributeException     if no or both ids were used in a {@link IClientFileItemPlaceHolder}
     * @throws FileItemNotFoundException          if an id of a file item was used that is not in the list of
     *                                            existingItems
     * @throws TemporaryFileItemNotFoundException if the id of a temporary file item was used that is not existing (for
     *                                            this temporaryItemOwner)
     */
    List<P> toItemPlaceHolders(List<? extends IClientFileItemPlaceHolder> clientItemPlaceHolders,
            Collection<F> existingItems,
            Person temporaryItemOwner);

    P toItemPlaceHolder(IClientFileItemPlaceHolder clientItemPlaceHolder, F existingItem, Person temporaryItemOwner);

}
