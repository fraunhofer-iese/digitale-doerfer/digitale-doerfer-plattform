/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientevent;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.usergeneratedcontentflags.model.UserGeneratedContentFlag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientUserGeneratedContentFlagResponse extends ClientBaseEvent {

    @NotBlank
    private String userGeneratedContentFlagId;

    @NotBlank
    private String entityId;

    @NotBlank
    private String entityType;

    @Nullable
    private String flagCreatorId;

    @NotBlank
    private String tenantId;

    public ClientUserGeneratedContentFlagResponse(UserGeneratedContentFlag createdContentFlag) {
        this.userGeneratedContentFlagId = createdContentFlag.getId();
        this.entityId = createdContentFlag.getEntityId();
        this.entityType = createdContentFlag.getEntityType();
        this.flagCreatorId = BaseEntity.getIdOf(createdContentFlag.getFlagCreator());
        this.tenantId = BaseEntity.getIdOf(createdContentFlag.getTenant());
    }

}
