/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.feature.clientmodel;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;

@Component
public class FeatureClientModelMapper {
    
    public List<ClientFeature> toClientFeatures(List<BaseFeature> features) {
        if (CollectionUtils.isEmpty(features)) {
            return Collections.emptyList();
        } else {
            return features.stream()
                    .map(f -> toClientFeature(ClientFeature.builder(), f))
                    .collect(Collectors.toList());
        }
    }

    public List<ClientFeatureExtended> toClientFeaturesExtended(List<BaseFeature> features) {
        if (CollectionUtils.isEmpty(features)) {
            return Collections.emptyList();
        } else {
            return features.stream()
                    .map(f -> {
                        ClientFeatureExtended cfe = toClientFeature(ClientFeatureExtended.builder(), f);
                        cfe.setVisibleForClient(f.isVisibleForClient());
                        return cfe;
                    })
                    .collect(Collectors.toList());
        }
    }

    private <F extends ClientFeature> F toClientFeature(ClientFeature.ClientFeatureBuilder<F, ?> builder,
            BaseFeature feature) {
        if (feature == null) {
            return null;
        }
        return builder
                .id(UUID.nameUUIDFromBytes(
                        feature.getFeatureIdentifier().getBytes(StandardCharsets.US_ASCII)).toString())
                .featureIdentifier(feature.getFeatureIdentifier())
                .enabled(feature.isEnabled())
                .featureValues(feature.getFeatureValuesAsJson())
                .build();
    }

}
