/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

/**
 * Documents the types of exceptions that can be thrown by this endpoint. For documenting the reason why a specific
 * exception is thrown use @{@link ApiException}.
 * <p>
 * These types are automatically added, depending on the configuration of the endpoint:
 * <p>
 * {@link ApiAuthenticationType#OAUTH2} or {@link ApiAuthenticationType#OAUTH2_OPTIONAL}
 * <ul>
 *      <li>{@link ClientExceptionType#PERSON_WITH_OAUTH_ID_NOT_FOUND}</li>
 *      <li>{@link ClientExceptionType#NOT_AUTHORIZED}</li>
 * </ul>
 * <p>
 * {@link ApiAuthenticationType#API_KEY}
 * <ul>
 *      <li>{@link ClientExceptionType#NOT_AUTHENTICATED}</li>
 *      <li>{@link ClientExceptionType#NOT_AUTHORIZED}</li>
 * </ul>
 * <p>
 * {@link ApiAppVariantIdentification#IDENTIFICATION_ONLY}
 * <ul>
 *      <li>{@link ClientExceptionType#APP_VARIANT_NOT_FOUND}</li>
 * </ul>
 * <p>
 * *&#47;event&#47;*
 * <ul>
 *      <li>{@link ClientExceptionType#EVENT_ATTRIBUTE_INVALID}</li>
 * </ul>
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface ApiExceptions {

    ClientExceptionType[] value();

}
