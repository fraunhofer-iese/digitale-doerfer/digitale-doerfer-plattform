/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptionContainer;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import springfox.documentation.spi.service.contexts.OperationContext;

/**
 * Extends the swagger documentation with the exceptions documented in the {@link ApiExceptions} annotations.
 */
@Component
@Order(2)
public class SwaggerExceptionDocumentationPlugin extends BaseSwaggerPlugin {

    @Override
    public void extendDocumentation(OperationContext context) {
        if (isNotDDClass(context)) {
            return;
        }
        final List<Pair<ClientExceptionType, String>> definedExceptionsAndReason = new ArrayList<>();

        // a single exception with a reason
        Optional<ApiException> apiExceptionOptional =
                context.findAnnotation(ApiException.class).toJavaUtil();
        if (apiExceptionOptional.isPresent()) {
            ApiException apiException = apiExceptionOptional.get();

            definedExceptionsAndReason.add(Pair.of(apiException.value(), apiException.reason()));
        }

        // multiple exceptions with a reason, automatically wrapped into the container
        Optional<ApiExceptionContainer> apiExceptionContainerOptional =
                context.findAnnotation(ApiExceptionContainer.class).toJavaUtil();
        if (apiExceptionContainerOptional.isPresent()) {
            ApiExceptionContainer apiExceptionContainer = apiExceptionContainerOptional.get();
            //it is possible to have the same exception type with multiple reasons
            Stream.of(apiExceptionContainer.value())
                    .map(ex -> Pair.of(ex.value(), ex.reason()))
                    .forEach(definedExceptionsAndReason::add);
        }

        // list of exceptions without reason
        Optional<ApiExceptions> apiExceptionsOptional =
                context.findAnnotation(ApiExceptions.class).toJavaUtil();
        if (apiExceptionsOptional.isPresent()) {
            ApiExceptions apiExceptions = apiExceptionsOptional.get();

            Stream.of(apiExceptions.value())
                    //we do not want to have the same exception type without reason multiple times
                    .distinct()
                    //and we do not want to have an exception type without reason if there is one with reason
                    .filter(ex -> definedExceptionsAndReason.stream()
                            .noneMatch(exWithReason -> exWithReason.getLeft() == ex))
                    .map(ex -> Pair.of(ex, (String) null))
                    .forEach(definedExceptionsAndReason::add);
        }

        //we use markdown syntax here
        //https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
        StringBuilder exceptionListing = new StringBuilder();
        exceptionListing
                .append("\n\n")
                .append("## Potential Exceptions\n\n")
                .append("|Type|Status Code|Description|\n")
                .append("| ---- | ---- | ---- |\n")
                .append(definedExceptionsAndReason.stream()
                        .sorted(exceptionComparator())
                        .map(SwaggerExceptionDocumentationPlugin::toExceptionDocumentationLine)
                        .collect(Collectors.joining("\n")));

        // separates the "standard" exceptions below from the specific ones above
        if (definedExceptionsAndReason.size() > 0) {
            exceptionListing.append("\n| ― | ― | ― |\n");
        }

        exceptionListing.append(defaultExceptions(context).stream()
                //we don't want to add the "standard" exceptions if they are already mentioned
                .filter(defaultEx -> definedExceptionsAndReason.stream()
                        .noneMatch(definedEx -> definedEx.getLeft() == defaultEx.getLeft()))
                .sorted(exceptionComparator())
                .map(SwaggerExceptionDocumentationPlugin::toExceptionDocumentationLine)
                .collect(Collectors.joining("\n")));

        appendToOperationField(context, "notes", exceptionListing.toString());
    }

    private static Comparator<Pair<ClientExceptionType, String>> exceptionComparator() {
        //we sort the potential exceptions by status and name
        return Comparator.comparing((Pair<ClientExceptionType, String> ex) -> ex.getLeft().getHttpStatus())
                .thenComparing((Pair<ClientExceptionType, String> ex) -> ex.getLeft().name());
    }

    private Collection<Pair<ClientExceptionType, String>> defaultExceptions(OperationContext context) {
        Set<Pair<ClientExceptionType, String>> exceptionTypes = new HashSet<>();
        ApiAuthentication authentication = SwaggerAuthenticationDocumentationPlugin.getApiAuthentication(context);
        switch (authentication.value()) {
            case OAUTH2:
                exceptionTypes.add(Pair.of(ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND, null));
                exceptionTypes.add(Pair.of(ClientExceptionType.NOT_AUTHORIZED, null));
                break;
            case OAUTH2_OPTIONAL:
                exceptionTypes.add(Pair.of(ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND, null));
                break;
            case API_KEY:
                exceptionTypes.add(Pair.of(ClientExceptionType.NOT_AUTHENTICATED, "No API key was provided"));
                exceptionTypes.add(Pair.of(ClientExceptionType.NOT_AUTHORIZED, "The provided API key is invalid"));
                break;
            case PUBLIC:
                break;
        }
        switch (authentication.appVariantIdentification()) {
            case NONE:
            case AUTHENTICATION:
                break;
            case IDENTIFICATION_ONLY:
                exceptionTypes.add(Pair.of(ClientExceptionType.APP_VARIANT_NOT_FOUND, null));
                break;
        }
        if (StringUtils.contains(context.requestMappingPattern(), "/event/")) {
            exceptionTypes.add(Pair.of(ClientExceptionType.EVENT_ATTRIBUTE_INVALID, null));
        }
        return exceptionTypes;
    }

    private static String toExceptionDocumentationLine(Pair<ClientExceptionType, String> ex) {
        ClientExceptionType exceptionType = ex.getLeft();
        String exceptionReason = ex.getRight();
        if (StringUtils.isNotEmpty(exceptionReason)) {
            return String.format("|`%s`|%s|%s <br/> (%s)|", exceptionType.name(),
                    exceptionType.getHttpStatus().toString(),
                    exceptionReason, exceptionType.getExplanation());

        } else {
            return String.format("|`%s`|%s|%s|", exceptionType.name(), exceptionType.getHttpStatus().toString(),
                    exceptionType.getExplanation());
        }
    }

}
