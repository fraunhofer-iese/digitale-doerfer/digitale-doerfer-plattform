/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import lombok.AllArgsConstructor;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

/**
 * A walker for the in-memory object graph spanned by a root object. The walker recursively descends into attributes of
 * the given object and also walks through iterables, arrays and map values.<br>
 * Due to performance reasons, the current implementation only descends into an attribute that is no iterable/array/map
 * if its type is in package {@link ObjectGraphWalker#PACKAGE_PREFIX} or a subpackage of it. Furthermore,
 * static attributes, primitive attributes and java.lang-attributes are ignored.
 */
@AllArgsConstructor(staticName = "startAt")
public class ObjectGraphWalker {

    /**
     * Only attributes of types in this package (and subpackages) are walked through
     */
    public final String PACKAGE_PREFIX = "de.fhg.iese";

    private final Object rootObject;

    /**
     * Walk through the object graph spanned by the given root object. The callback is executed whenever an object
     * is found with {@code type.isInstance(object)}. If the callback returns false, the walker does not walk into the
     * attributes of the callback function parameter.
     *
     * @param type must not be {@code null}
     * @param callback must not be {@code null}
     * @param <T>
     * @return the number of inspected objects
     */
    public <T> int walkAndCall(Class<T> type, Function<T, Boolean> callback) {
        Objects.requireNonNull(type);
        Objects.requireNonNull(callback);

        //store already visited objects to avoid infinite loops (cycles) and duplicate object checking
        final Set<Object> inspectedObjects = new HashSet<>();
        inspect(rootObject, inspectedObjects, type, callback);
        return inspectedObjects.size();
    }

    private <T> void inspect(Object object, Set<Object> inspectedObjects, Class<T> type,
            Function<T, Boolean> baseEntityCallback) {
        if (object == null) {
            return;
        }
        if (object instanceof byte[]) {
            return;
        }
        inspectedObjects.add(object);
        //check if callback should be applied
        if (type.isInstance(object)) {
            final Boolean result = baseEntityCallback.apply(type.cast(object));
            if (Boolean.FALSE.equals(result)) {
                //do not recursively inspect attributes
                return;
            }
        }
        if (object instanceof Iterable<?> iterable) {
            iterable.forEach(o -> inspect(o, inspectedObjects, type, baseEntityCallback));
            return;
        }
        if (object instanceof Object[] array) {
            for (Object element : array) {
                inspect(element, inspectedObjects, type, baseEntityCallback);
            }
            return;
        }
        if (object instanceof Map<?, ?> map) {
            //in case of a map, inspect only the values since modifying the keys (which contain values expected by the
            // client) does not make sense
            inspect(map.values(), inspectedObjects, type, baseEntityCallback);
            return;
        }
        if (!object.getClass().getPackage().getName().startsWith(PACKAGE_PREFIX)) {
            return;
        }
        ReflectionUtils.doWithFields(object.getClass(), field -> {
            ReflectionUtils.makeAccessible(field);
            final Object value = ReflectionUtils.getField(field, object);
            if (value != null && !inspectedObjects.contains(value)) {
                inspect(value, inspectedObjects, type, baseEntityCallback);
            }
        }, field -> !Modifier.isStatic(field.getModifiers()) //skipping static fields (are not serialized)
                && !field.getType().isPrimitive() //skipping primitive fields
                && !"java.lang".equals(
                field.getType().getPackage().getName())); //skipping standard java datatypes (as String, Long, ...)
    }

}
