/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2022 Torsten Lenhart, Steffen Hupp, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.tenant.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPerson;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientCommunity;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.TenantClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.participants.tenant.exceptions.TenantNotFoundException;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.CommunityHelpDesk;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/community")
@Api(tags = {"participants.community"})
class TenantController extends BaseController {

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private IAppService appService;
    @Autowired
    private TenantClientModelMapper tenantClientModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @Deprecated
    @ApiOperation(value = "Returns all tenants",
            notes = "This endpoint is deprecated. Please use /adminui/tenant/ instead")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping
    public List<ClientCommunity> getAllTenants() {

        return tenantService.findAllOrderedByNameAsc().stream()
                .map(c -> tenantClientModelMapper.createClientCommunity(c))
                .collect(Collectors.toList());
    }

    @Deprecated
    @ApiOperation(value = "Returns the home tenant of the current user",
            notes = "This endpoint is deprecated. Please use geo areas instead")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/own")
    public ClientCommunity getHomeTenant() {

        Person currentUser = getCurrentPersonNotNull();
        //the additional lookup is to ensure that it gets freshly loaded
        final Tenant tenant = tenantService.findTenantById(currentUser.getTenant().getId());
        return tenantClientModelMapper.createClientCommunity(tenant);
    }

    @Deprecated
    @ApiOperation(value = "Returns a tenant by id",
            notes = "This endpoint is deprecated. Please use /adminui/tenant/{tenantId} instead")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @GetMapping("/{communityId}")
    public ClientCommunity getTenant(
            @PathVariable String communityId) {

        return tenantClientModelMapper.createClientCommunity(tenantService.findTenantById(communityId));
    }

    @Deprecated
    @ApiOperation(value = "Returns the nearest tenant based on the given location",
            notes = "This endpoint is deprecated. Please use geo areas instead")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND,
            ClientExceptionType.APP_VARIANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping("/nearestCommunity/{latitude}/{longitude}")
    public ClientCommunity findNearestTenant(
            @PathVariable double latitude,
            @PathVariable double longitude) {

        AppVariant appVariant = getAppVariantNotNull();

        GPSLocation location = new GPSLocation(latitude, longitude);
        Pair<GeoArea, IGeoAreaService.GeoAreaMatch> nearestGeoAreaMatch =
                geoAreaService.findNearestGeoArea(appService.getAvailableLeafGeoAreas(appVariant), location);
        if (nearestGeoAreaMatch.getRight() == IGeoAreaService.GeoAreaMatch.NONE ||
                nearestGeoAreaMatch.getLeft().getTenant() == null) {
            throw new TenantNotFoundException("No tenant found for location {}", location);
        }
        return tenantClientModelMapper.createClientCommunity(nearestGeoAreaMatch.getLeft().getTenant());
    }

    @Deprecated
    @ApiOperation(value = "Returns the list of contact persons for the given tenant.",
            notes = "This endpoint is deprecated. Please use /support/contacts instead")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/{communityId}/contacts")
    public List<ClientPerson> getContactPersons(
            @PathVariable String communityId) {

        Tenant tenant = tenantService.findTenantById(communityId);
        List<Person> persons = getRoleService().getAllPersonsWithRoleForEntity(CommunityHelpDesk.class, tenant);
        return persons.stream()
                .map(personClientModelMapper::createClientPerson)
                .collect(Collectors.toList());
    }

    @Deprecated
    @ApiOperation(value = "Return the e-mail of the contact person for the given tenant",
            notes = "This endpoint is deprecated. Please use /support/contacts/default instead")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping("/{communityId}/contactEmail")
    public String getContactPersonEmail(
            @PathVariable String communityId) {

        Tenant tenant = tenantService.findTenantById(communityId);
        String email = null;
        Person person = getRoleService().getDefaultContactPersonForTenant(tenant);
        if (person != null) {
            email = person.getEmail();
        }
        return email;
    }

}
