/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2023 Steffen Hupp, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

import static de.fhg.iese.dd.platform.api.framework.controllers.LogRequestsInterceptor.REQUEST_MESSAGE_MARKER;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.catalina.connector.ClientAbortException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.fhg.iese.dd.platform.api.framework.controllers.LogRequestsInterceptor;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingTimeoutException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BaseRuntimeException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import lombok.extern.log4j.Log4j2;

@ControllerAdvice
@Log4j2
public class ClientExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EventProcessingTimeoutException.class)
    protected ResponseEntity<?> handleEventProcessingTimeoutException(EventProcessingTimeoutException occurredException,
            WebRequest request) {

        ClientExceptionEntity clientException =
                createClientException(occurredException, ClientExceptionType.REQUEST_PROCESSING_TIMEOUT, request);

        logException(occurredException, clientException, request);
        return toResponseEntity(clientException);
    }

    @ExceptionHandler(EventProcessingException.class)
    protected ResponseEntity<?> handleEventProcessingException(EventProcessingException occurredException,
            WebRequest request) {

        Exception containedException = (Exception) occurredException.getCause();

        ClientExceptionEntity clientException;
        if (containedException instanceof BaseRuntimeException) {
            clientException = createClientException((BaseRuntimeException) containedException, request);
        } else {
            clientException =
                    createClientException(containedException, ClientExceptionType.UNSPECIFIED_SERVER_ERROR, request);
        }
        logException(containedException, clientException, request);
        return toResponseEntity(clientException);
    }

    @ExceptionHandler(BaseRuntimeException.class)
    protected ResponseEntity<?> handleBaseRuntimeExceptions(BaseRuntimeException occurredException,
            WebRequest request) {

        ClientExceptionEntity clientException = createClientException(occurredException, request);

        logException(occurredException, clientException, request);
        return toResponseEntity(clientException);
    }

    @ExceptionHandler({MultipartException.class})
    protected ResponseEntity<?> handleFileUploadException(MultipartException occurredException, WebRequest request) {

        ClientExceptionEntity clientException =
                createClientException(new FileItemUploadException("File size for upload exceeded"), request);

        logException(occurredException, clientException, request);
        if (occurredException.getCause() instanceof ClientAbortException) {
            return null;
        } else {
            return toResponseEntity(clientException);
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<?> handleConstraintViolation(ConstraintViolationException occurredException,
            WebRequest request) {

        final Set<ConstraintViolation<?>> constraintViolations = occurredException.getConstraintViolations();
        StringBuilder errorList = new StringBuilder();
        StringBuilder detail = new StringBuilder();
        if (!CollectionUtils.isEmpty(constraintViolations)) {
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                if (errorList.length() > 0) {
                    errorList.append(", ");
                }
                if (detail.length() > 0) {
                    detail.append(", ");
                }
                errorList.append(constraintViolation.getPropertyPath())
                        .append(": ")
                        .append(constraintViolation.getMessage());
                detail.append(constraintViolation.getPropertyPath());
            }
        } else {
            errorList.append(occurredException.getMessage());
            if (occurredException.getCause() != null) {
                detail.append(occurredException.getCause().getMessage());
            }
        }
        ClientExceptionEntity clientException = ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(occurredException.getClass().getName())
                .message(errorList.toString())
                .detail(detail.toString())
                .build()
                .withType(ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        logException(occurredException, clientException, request);
        return toResponseEntity(clientException);
    }

    @ExceptionHandler(ClientAbortException.class)
    protected void handleClientAbortException(ClientAbortException occurredException, WebRequest request) {

        ClientExceptionEntity clientException =
                createClientException(occurredException, ClientExceptionType.CLIENT_ABORTED_CONNECTION, request);

        logException(occurredException, clientException, request);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<?> handleAnyException(Exception occurredException, WebRequest request) {

        ClientExceptionEntity clientException =
                createClientException(occurredException, ClientExceptionType.UNSPECIFIED_SERVER_ERROR, request);

        logException(occurredException, clientException, request);
        return toResponseEntity(clientException);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        StringBuilder errorList = new StringBuilder();
        StringBuilder detail = new StringBuilder();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            if (errorList.length() > 0) {
                errorList.append(", ");
            }
            if (detail.length() > 0) {
                detail.append(", ");
            }
            errorList.append(error.getField()).append(": ").append(error.getDefaultMessage());
            detail.append(error.getField());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            if (errorList.length() > 0) {
                errorList.append(", ");
            }
            errorList.append(error.getObjectName()).append(": ").append(error.getDefaultMessage());
        }
        ClientExceptionEntity clientException = ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(ex.getClass().getName())
                .message(errorList.toString())
                .detail(detail.toString())
                .build()
                .withType(ClientExceptionType.EVENT_ATTRIBUTE_INVALID);

        logException(ex, clientException, request);
        return toResponseEntity(clientException);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ClientExceptionEntity clientException = ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(ex.getClass().getName())
                .message(ex.getMessage())
                .build()
                .withType(ClientExceptionType.UNSPECIFIED_BAD_REQUEST);
        logException(ex, clientException, request);
        return toResponseEntity(clientException);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ClientExceptionEntity clientException = ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(ex.getClass().getName())
                .message(ex.getMessage())
                .type(ClientExceptionType.UNSPECIFIED_BAD_REQUEST)
                .detail(ex.getParameterName() + ":" + ex.getParameterType())
                .build()
                .withType(ClientExceptionType.UNSPECIFIED_BAD_REQUEST);
        logException(ex, clientException, request);
        return toResponseEntity(clientException);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ClientExceptionEntity clientException = ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(ex.getClass().getName())
                .message(ex.getMessage())
                .detail(body != null ? body.toString() : null)
                .build()
                .withType(ClientExceptionType.UNSPECIFIED_BAD_REQUEST);
        logException(ex, clientException, request);
        return toResponseEntity(clientException);
    }

    private void logException(Exception occurredException, ClientExceptionEntity clientException,
            RequestAttributes request) {
        if (clientException.getStatus().is5xxServerError()) {
            // this is required to get the stack trace
            // the according request can be found with the log of the LogRequestsInterceptor
            log.error(clientException.toString(), occurredException);
        } else {
            // this should not be included in the json log, since it does not contain any additional information
            log.debug(REQUEST_MESSAGE_MARKER, clientException.toString());
        }
        request.setAttribute(LogRequestsInterceptor.REQUEST_ATTRIBUTE_EXCEPTION_ENTITY, clientException,
                RequestAttributes.SCOPE_REQUEST);
    }

    private ResponseEntity<Object> toResponseEntity(ClientExceptionEntity clientException) {

        return ResponseEntity
                .status(clientException.getStatus())
                .contentType(MediaType.APPLICATION_JSON)
                .body(clientException);
    }

    private String getPath(WebRequest request) {
        if (request instanceof ServletWebRequest) {
            HttpServletRequest httpRequest = ((ServletWebRequest) request).getNativeRequest(HttpServletRequest.class);
            if (httpRequest != null) {
                return httpRequest.getRequestURI();
            }
        }
        return "";
    }

    private ClientExceptionEntity createClientException(BaseRuntimeException occurredException,
            WebRequest request) {
        return ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(occurredException.getClass().getName())
                .message(occurredException.getMessage())
                .detail(occurredException.getDetail())
                .build()
                .withType(occurredException.getClientExceptionType());
    }

    private ClientExceptionEntity createClientException(Exception occurredException,
            ClientExceptionType clientExceptionType, WebRequest request) {
        return ClientExceptionEntity.builder()
                .path(getPath(request))
                .timestamp(System.currentTimeMillis())
                .exception(occurredException.getClass().getName())
                .message(occurredException.getMessage())
                .detail(occurredException.getCause() != null ? occurredException.getCause().getMessage() : null)
                .build()
                .withType(clientExceptionType);
    }

}
