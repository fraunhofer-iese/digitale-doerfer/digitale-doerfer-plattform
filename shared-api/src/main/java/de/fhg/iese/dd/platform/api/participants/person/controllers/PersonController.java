/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2020 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonOwn;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.ClientMediaItem;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.SharedConstants;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemProcessingException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemUploadException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.FileOwnership;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/person")
@Api(tags = {"participants.person"})
class PersonController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private IMediaItemService mediaItemService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;
    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    @ApiOperation(value = "Get own person")
    @ApiExceptions({
            ClientExceptionType.PERSON_WITH_OAUTH_ID_NOT_FOUND,
            ClientExceptionType.NOT_AUTHORIZED,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping
    public ClientPersonOwn getPerson() {

        return personClientModelMapper.createClientPersonOwn(getCurrentPersonNotNull());
    }

    @ApiOperation(value = "Use /person/event/personProfilePictureChangeRequest instead")
    @ApiExceptions({
            ClientExceptionType.FILE_ITEM_UPLOAD_FAILED
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/picture")
    @ResponseStatus(HttpStatus.CREATED)
    @Deprecated
    public ClientMediaItem uploadProfilePicture(
            @RequestParam("file") MultipartFile image) {

        if (image.isEmpty()) {
            throw new FileItemUploadException("Image is empty");
        }

        final Person person = getCurrentPersonNotNull();
        final AppVariant appVariant =
                getAppService().findAppVariantByAppVariantIdentifier(SharedConstants.PLATFORM_APP_VARIANT_IDENTIFIER);
        byte[] imageData;
        try {
            imageData = image.getBytes();
        } catch (IOException e) {
            throw new FileItemUploadException(e);
        }
        MediaItem mediaItem;
        try {
            mediaItem = mediaItemService.createMediaItem(imageData, FileOwnership.of(person, appVariant));
        } catch (FileItemProcessingException e) {
            throw new FileItemUploadException(e);
        }
        personService.updateProfilePicture(person, mediaItem);
        return fileClientModelMapper.toClientMediaItem(mediaItem);
    }

    @ApiOperation(value = "Use /person/event/personProfilePictureDeleteRequest instead")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping(value = "/picture")
    @Deprecated
    public void deleteProfilePicture() {

        Person person = getCurrentPersonNotNull();

        personService.updateProfilePicture(person, null);
    }

    @ApiOperation(value = "Returns all tenants of a user")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2_AND_API_KEY,
            appVariantIdentification = ApiAppVariantIdentification.AUTHENTICATION)
    @GetMapping(value = "/tenants")
    public List<String> getTenants() {

        final Person person = getCurrentPersonNotNull();
        getAuthenticatedAppVariantNotNull();

        return Collections.singletonList(person.getTenant().getId());
    }

}
