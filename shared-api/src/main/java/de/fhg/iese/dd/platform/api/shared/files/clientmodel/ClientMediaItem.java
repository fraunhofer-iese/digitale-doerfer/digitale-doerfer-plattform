/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Matthias Gerbershagen, Balthasar Weitzel, Adeline Silva Schäfer, Johannes Eveslage
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Map;

import static de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping.defaultJsonWriter;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class ClientMediaItem extends ClientBaseEntity {

    @JsonRawValue
    @ApiModelProperty(dataType = "java.util.Map")
    private String urls;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ClientRelativeImageCropDefinition relativeImageCropDefinition;

    /**
     * Only required for deserialization in tests
     *
     * @param urlsMap
     */
    @JsonSetter("urls")
    private void setUrlsMap(Map<String, String> urlsMap) {
        try {
            urls = defaultJsonWriter().writeValueAsString(urlsMap);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    public void setUrls(String urls) {
        this.urls = urls;
    }

}
