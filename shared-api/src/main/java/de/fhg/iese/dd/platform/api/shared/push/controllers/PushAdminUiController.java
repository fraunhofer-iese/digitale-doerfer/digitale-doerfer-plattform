/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushEventDocumentation;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.shared.security.ListPushConfigurationAction;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/adminui/push")
@Api(tags = {"shared.adminui.push"})
public class PushAdminUiController extends BaseController {

    @Autowired
    private IClientPushService clientPushService;

    @ApiOperation(value = "Push events overview",
            notes = "Overview of all pushed events and a brief documentation when they are sent.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = ListPushConfigurationAction.class)
    @GetMapping(value = "pushEvents", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClientPushEventDocumentation> getPushEventOverviewJson() {

        getRoleService().decidePermissionAndThrowNotAuthorized(ListPushConfigurationAction.class,
                getCurrentPersonNotNull());

        return clientPushService.getPushEventDocumentation();
    }

}
