/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2021 Benjamin Hassenfratz, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientExternalSystemResponse;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.callback.services.ExternalSystemResponse;
import de.fhg.iese.dd.platform.business.shared.callback.services.IExternalSystemCallbackService;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/administration/external")
@Api(tags = {"admin", "shared.admin.external"})
public class ExternalSystemCallbackAdminController extends BaseController {

    private static final TypeReference<Map<String, Object>> MAP_STRING_OBJECT_TYPE_REF =
            new TypeReference<>() {
            };

    @Autowired
    private IAppService appService;
    @Autowired
    private IExternalSystemCallbackService externalSystemCallbackService;

    @ApiOperation(value = "Calls an external system",
            notes = "Calls an external system based on the callback url of the corresponding app variant identified " +
                    "by the given app variant identifier. The url suffix is added to the callback url (baseUrl) and " +
                    "the response of the external system is returned.")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.EXTERNAL_SYSTEM_CALL_FAILED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class})
    @GetMapping
    public ClientExternalSystemResponse callExternalSystem(
            @RequestParam String appVariantIdentifierToCall,
            @RequestParam String urlSuffix
    ) {
        final AppVariant appVariantToCall = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifierToCall);

        if (!appVariantToCall.hasExternalSystem()) {
            throw new BadRequestException("App variant with identifier '{}' has no callback url",
                    appVariantIdentifierToCall);
        }
        final ExternalSystemResponse<String> response = externalSystemCallbackService
                .callExternalSystem(appVariantToCall, HttpMethod.GET, urlSuffix, String.class);
        return createClientExternalSystemResponse(response);
    }

    @ApiOperation(value = "Calls an external system",
            notes = "Calls an external system based on the callback url of the corresponding app variant identified " +
                    "by the given app variant identifier. The url suffix is added to the callback url (baseUrl) and " +
                    "additionally, a payload is sent with the request. The response of the external system is returned.")
    @ApiExceptions({
            ClientExceptionType.APP_VARIANT_NOT_FOUND,
            ClientExceptionType.EXTERNAL_SYSTEM_CALL_FAILED})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredRoles = {SuperAdmin.class})
    @RequestMapping(method = {
            RequestMethod.POST,
            RequestMethod.PUT,
            RequestMethod.DELETE
    })
    public ClientExternalSystemResponse callExternalSystem(
            @RequestParam String appVariantIdentifierToCall,
            @RequestParam String urlSuffix,
            @RequestBody(required = false) String requestBody,
            HttpServletRequest request
    ) {
        final HttpMethod method = HttpMethod.resolve(request.getMethod());
        final AppVariant appVariantToCall = appService.findAppVariantByAppVariantIdentifier(appVariantIdentifierToCall);

        if (!appVariantToCall.hasExternalSystem()) {
            throw new BadRequestException("App variant with identifier '{}' has no callback url",
                    appVariantIdentifierToCall);
        }
        final ExternalSystemResponse<String> response = externalSystemCallbackService
                .callExternalSystem(appVariantToCall, method, urlSuffix, requestBody, String.class);
        return createClientExternalSystemResponse(response);
    }

    protected ClientExternalSystemResponse createClientExternalSystemResponse(ExternalSystemResponse<String> response) {
        final ResponseEntity<String> clientResponse = response.getResponse();

        ClientExternalSystemResponse.ClientExternalSystemResponseBuilder<?, ?> responseBuilder =
                ClientExternalSystemResponse.builder()
                        .requestMethod(response.getRequestMethod())
                        .requestURI(response.getRequestURI().toString())
                        .requestHeaders(hideSensitiveInformation(response.getRequestHeaders()))
                        .responseHeaders(hideSensitiveInformation(clientResponse.getHeaders()))
                        .responseStatus(clientResponse.getStatusCode().toString());
        try {
            responseBuilder.responseBodyJson(
                    JsonMapping.defaultJsonReader().forType(MAP_STRING_OBJECT_TYPE_REF)
                            .readValue(clientResponse.getBody()));
        } catch (JsonProcessingException e) {
            //ignore the exception, we always try to transform it into json
        }
        responseBuilder.responseBodyString(clientResponse.getBody());
        return responseBuilder.build();
    }

    private Map<String, List<String>> hideSensitiveInformation(Map<String, List<String>> map) {
        if (map == null) {
            return Collections.emptyMap();
        }
        return map.entrySet().stream()
                .map(this::hideSensitiveInformation)
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    private Pair<String, List<String>> hideSensitiveInformation(Map.Entry<String, List<String>> entry) {
        final String key = entry.getKey();
        if (StringUtils.containsIgnoreCase(key, "key") ||
                StringUtils.containsIgnoreCase(key, "secret") ||
                StringUtils.containsIgnoreCase(key, "authorization") ||
                StringUtils.containsIgnoreCase(key, "password")) {
            return Pair.of(key, entry.getValue().stream()
                    .map(ExternalSystemCallbackAdminController::maskValue)
                    .collect(Collectors.toList()));
        } else {
            return Pair.of(key, entry.getValue());
        }
    }

    private static String maskValue(String s) {
        //the first 5 chars are kept, the rest is masked
        return StringUtils.truncate(s, 5) + StringUtils.repeat("*", StringUtils.length(s) - 5);
    }

}
