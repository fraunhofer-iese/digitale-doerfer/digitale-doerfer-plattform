/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.clientmodel;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.push.model.PushCategoryUserSetting;

@Component
public class PushClientModelMapper {
    
    public List<ClientPushCategoryUserSetting> toClientPushCategoryUserSetting(
            List<PushCategoryUserSetting> pushCategoryUserSettings) {
        return pushCategoryUserSettings.stream()
                .map(this::toClientPushCategoryUserSetting)
                .collect(Collectors.toList());
    }

    public ClientPushCategoryUserSetting toClientPushCategoryUserSetting(
            PushCategoryUserSetting pushCategoryUserSetting) {
        if (pushCategoryUserSetting == null) {
            return null;
        }
        PushCategory pushCategory = pushCategoryUserSetting.getPushCategory();
        if (pushCategory == null) {
            return null;
        }
        return ClientPushCategoryUserSetting.builder()
                .id(pushCategory.getId())
                .name(pushCategory.getName())
                .description(pushCategory.getDescription())
                .loudPushEnabled(pushCategoryUserSetting.isLoudPushEnabled())
                .parentPushCategoryId(BaseEntity.getIdOf(pushCategory.getParentCategory()))
                .orderValue(pushCategory.getOrderValue())
                .build();
    }

}
