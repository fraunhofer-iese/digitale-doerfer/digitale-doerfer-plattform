/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Matthias Gerbershagen, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.address.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.AddressClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.address.clientmodel.ClientAddressListEntry;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/person/address")
@Api(tags = {"participants.person.addresslist"})
class AddressListController extends BaseController {

    @Autowired
    private IPersonService personService;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private AddressClientModelMapper addressClientModelMapper;

    @ApiOperation(value = "Returns all AddressListEntries",
            notes = "Returns all AddressListEntries of the current user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping
    public List<ClientAddressListEntry> getAddressListEntries() {

        Person person = getCurrentPersonNotNull();

        return personService.getAddressListEntries(person)
                .stream()
                .sorted(Comparator.comparingLong(BaseEntity::getCreated).reversed())
                .map(addressClientModelMapper::createClientAddressListEntry)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Returns the default AddressListEntry",
            notes = "Returns the default AddressListEntry of the current user, if there is any.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("default")
    public ClientAddressListEntry getDefaultAddressListEntry() {

        Person person = getCurrentPersonNotNull();
        return addressClientModelMapper.createClientAddressListEntry(personService.getDefaultAddressListEntry(person));
    }

    @ApiOperation(value = "Deletes the default AddressListEntry",
            notes = "Deletes the default AddressListEntry of the current user, if there is any.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping("default")
    public void deleteDefaultAddressListEntry() {

        Person person = getCurrentPersonNotNull();
        personService.deleteDefaultAddress(person);
    }

    @ApiOperation(value = "Returns an AddressListEntry by id",
            notes = "Returns the AddressListEntry identified by the given id of the current user, if there is any with this id.")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("{id}")
    public ClientAddressListEntry getAddressListEntry(
            @PathVariable String id) {

        Person person = getCurrentPersonNotNull();
        AddressListEntry addressListEntry = personService.getAddressListEntryById(person, id);
        if(addressListEntry == null){
            throw new NotFoundException("No AddressListEntry found with the given {1} {2} for the person.", "id", id);
        }
        return addressClientModelMapper.createClientAddressListEntry(addressListEntry);
    }

    @ApiOperation(value = "Adds a new AddressListEntry",
            notes = "Adds a new AddressListEntry to the AddressList of the current user.")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping
    public ResponseEntity<ClientAddressListEntry> addAddressListEntry(
            @Valid @RequestBody ClientAddressListEntry clientAddressListEntry) {

        Person person = getCurrentPersonNotNull();
        checkClientAddressListEntry(clientAddressListEntry);

        AddressListEntry addressListEntry = new AddressListEntry();
        addressListEntry.setName(clientAddressListEntry.getName());
        //we do not need to use addressService.findOrCreateAddress here, this is done in the person service already
        addressListEntry.setAddress(addressClientModelMapper.toAddress(clientAddressListEntry.getAddress()));

        AddressListEntry personAddressListEntry = personService.addAddressListEntry(person, addressListEntry);
        return new ResponseEntity<>(addressClientModelMapper.createClientAddressListEntry(personAddressListEntry),
                HttpStatus.CREATED);
    }

    @ApiOperation(value = "Adds or updates the default AddressListEntry",
            notes = "Adds or updates the default AddressListEntry of the current user, the name of the entry is ignored.")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("default")
    public ClientAddressListEntry addDefaultAddressListEntry(
            @Valid @RequestBody ClientAddressListEntry clientAddressListEntry) {

        Person person = getCurrentPersonNotNull();

        IAddressService.AddressDefinition address =
                addressClientModelMapper.toAddressDefinition(clientAddressListEntry.getAddress());
        addressService.checkAddress(address);

        AddressListEntry personAddressListEntry = personService.setDefaultAddress(person, address);
        return addressClientModelMapper.createClientAddressListEntry(personAddressListEntry);
    }

    @ApiOperation(value = "Change an AddressListEntry",
            notes = "Change an AddressListEntry identified by the id.")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_NOT_FOUND,
            ClientExceptionType.ADDRESS_INVALID,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("{id}")
    public ClientAddressListEntry changeAddressListEntry(
            @PathVariable String id,
            @Valid @RequestBody ClientAddressListEntry clientAddressListEntry) {

        Person person = getCurrentPersonNotNull();
        checkClientAddressListEntry(clientAddressListEntry);

        AddressListEntry oldEntry = personService.getAddressListEntryById(person, id);
        if (oldEntry == null) {
            throw new NotFoundException("No AddressListEntry found with the given id {} for the person.", id);
        }
        //we do not need to use addressService.findOrCreateAddress here, this is done in the person service already
        AddressListEntry newEntry = addressClientModelMapper.createAddressListEntry(clientAddressListEntry);

        AddressListEntry personAddressListEntry = personService.changeAddressListEntry(person, oldEntry, newEntry);
        return addressClientModelMapper.createClientAddressListEntry(personAddressListEntry);
    }

    @ApiOperation(value = "Delete an AddressListEntry",
            notes = "Delete an AddressListEntry identified by the id.")
    @ApiExceptions({
            ClientExceptionType.UNSPECIFIED_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping("{id}")
    public void deleteAddressListEntry(
            @PathVariable String id) {

        Person person = getCurrentPersonNotNull();
        personService.deleteAddressListEntryById(person, id);
    }

    private void checkClientAddressListEntry(ClientAddressListEntry clientAddressListEntry) {

        if (StringUtils.isEmpty(clientAddressListEntry.getName())) {
            throw new InvalidEventAttributeException("The attribute may not be empty/null!").withDetail("name");
        }
        addressService.checkAddress(IAddressService.AddressDefinition.fromAddress(
                        addressClientModelMapper.toAddress(clientAddressListEntry.getAddress())),
                "Contained address is invalid");
    }

}
