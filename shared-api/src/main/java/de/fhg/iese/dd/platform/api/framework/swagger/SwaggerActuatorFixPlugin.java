/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.swagger;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriTemplate;

import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.service.contexts.OperationContext;

/**
 * With the current version of springfox the actuator endpoints by spring are not generated correctly in swagger.
 * See
 * https://github.com/springfox/springfox/issues/2390
 * and
 * https://github.com/rlippolis/spring-boot2-actuator-swagger/blob/master/src/main/java/com/rlippolis/spring/springboot2actuatorswagger/config/DirtyFixConfig.java
 * for details about the issue.
 *
 * REMOVE it when the bug is fixed.
 */
@Component
@Order(3)
public class SwaggerActuatorFixPlugin extends BaseSwaggerPlugin  {

    @Autowired
    private TypeResolver typeResolver;

    @Override
    public void extendDocumentation(final OperationContext context) {
        if(StringUtils.startsWith(context.requestMappingPattern(), "/manage/")) {
            removeBodyParametersForReadMethods(context);
            addOperationParametersForPathParams(context);
        }
    }

    private void removeBodyParametersForReadMethods(final OperationContext context) {
        if (HttpMethod.GET.equals(context.httpMethod()) || HttpMethod.HEAD.equals(context.httpMethod())) {
            final List<Parameter> parameters = getParameters(context);
            parameters.removeIf(param -> "body".equals(param.getName()));
        }
    }

    private void addOperationParametersForPathParams(final OperationContext context) {
        final UriTemplate uriTemplate = new UriTemplate(context.requestMappingPattern());

        final List<Parameter> pathParams = uriTemplate.getVariableNames().stream()
                .map(this::createPathParameter)
                .collect(Collectors.toList());

        context.operationBuilder().parameters(pathParams);
    }

    private Parameter createPathParameter(final String pathParam) {
        return new ParameterBuilder()
                .name(pathParam)
                .description(pathParam)
                .required(true)
                .modelRef(new ModelRef("string"))
                .type(typeResolver.resolve(String.class))
                .parameterType("path")
                .build();
    }

}
