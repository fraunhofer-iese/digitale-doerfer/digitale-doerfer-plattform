/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.legal.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.legal.clientevent.ClientLegalTextAcceptRequest;
import de.fhg.iese.dd.platform.api.shared.legal.clientevent.ClientLegalTextAcceptResponse;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalText;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.ClientLegalTextStatus;
import de.fhg.iese.dd.platform.api.shared.legal.clientmodel.LegalClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.legal.LegalTextStatus;
import de.fhg.iese.dd.platform.business.shared.legal.services.ILegalTextService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/legal")
@Api(tags = {"shared.legal"})
public class LegalController extends BaseController {

    @Autowired
    private ILegalTextService legalTextService;
    @Autowired
    private LegalClientModelMapper legalClientModelMapper;

    @ApiOperation(value = "Current legal texts for app variant",
            notes = "All current active legal texts for this app variant, independent of the current user.")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "current")
    public List<ClientLegalText> getCurrentLegalTexts() {

        AppVariant appVariant = getAppVariantNotNull();

        List<LegalText> currentLegalTexts = legalTextService.getCurrentLegalTexts(appVariant);

        return legalClientModelMapper.toClientLegalTexts(currentLegalTexts);
    }

    @ApiOperation(value = "Accept legal texts",
            notes = "Accepts legal texts for this user. " +
                    "All legal texts can be accepted, also those that are not required. " +
                    "If a text already was accepted, the timestamp is **not** updated.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "event/legalTextAcceptRequest")
    public ClientLegalTextAcceptResponse acceptLegalTexts(
            @RequestBody(required = true)
            @ApiParam ClientLegalTextAcceptRequest legalTextAcceptRequest) {

        Person person = getCurrentPersonNotNull();

        List<LegalText> legalTextsToAccept =
                legalTextService.getLegalTextsByIdentifier(legalTextAcceptRequest.getLegalTextIdentifiersToAccept());

        //if there already was an acceptance it is not updated, but the original one is returned
        Map<LegalText, LegalTextAcceptance> acceptances = legalTextService.acceptLegalTexts(person, legalTextsToAccept);

        List<ClientLegalText> clientLegalTexts = legalClientModelMapper.toClientLegalTexts(acceptances);

        return ClientLegalTextAcceptResponse.builder()
                .acceptedLegalTexts(clientLegalTexts)
                .build();
    }

    @ApiOperation(value = "Current status of legal texts for app variant and user",
            notes = "Acceptance status of all current active legal texts for this app variant.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "status")
    public ClientLegalTextStatus getCurrentLegalTextStatus() {

        AppVariant appVariant = getAppVariantNotNull();
        Person person = getCurrentPersonNotNull();

        LegalTextStatus legalTextStatus = legalTextService.getCurrentLegalTextStatus(appVariant, person);

        return legalClientModelMapper.toClientLegalTextStatus(legalTextStatus);
    }

}
