/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.services;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.business.framework.caching.CacheChecker;
import de.fhg.iese.dd.platform.business.framework.caching.CacheCheckerIncrementalCache;
import de.fhg.iese.dd.platform.business.framework.caching.ICachingComponent;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
class ClientAppService extends BaseService implements IClientAppService {

    //the cache check is not going to the DB, only to a field in the app service
    private static final Duration CACHE_CHECK_INTERVAL = Duration.ofSeconds(10);

    private Map<AppVariant, List<ClientGeoArea>> availableClientGeoAreasByAppVariant;

    @Autowired
    private IAppService appService;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;
    @Autowired
    private CacheCheckerIncrementalCache cacheCheckerGeoAreasByAppVariant;

    @PostConstruct
    private void initialize() {
        cacheCheckerGeoAreasByAppVariant.clearCheckQueries();
        cacheCheckerGeoAreasByAppVariant.addCheckQuery(appService::checkCacheAvailableGeoAreasByAppVariant);
        cacheCheckerGeoAreasByAppVariant.setCheckInterval(CACHE_CHECK_INTERVAL);
        //this cache needs to be concurrent, since they are incrementally filled with new values
        availableClientGeoAreasByAppVariant = new ConcurrentHashMap<>();
    }

    @Override
    public ICachingComponent.CacheConfiguration getCacheConfiguration() {

        return CacheConfiguration.builder()
                .usedCache(IGeoAreaService.class)
                .usedCache(IAppService.class)
                .build();
    }

    @Override
    public List<ClientGeoArea> getAvailableClientGeoAreas(AppVariant appVariant) {
        // since the cache is incrementally filled it is okay to clear it and not fill it
        cacheCheckerGeoAreasByAppVariant.clearCacheIfStale(
                () -> CacheChecker.clearIfNotNull(availableClientGeoAreasByAppVariant));

        return availableClientGeoAreasByAppVariant.computeIfAbsent(appVariant,
                // fetch the available geo areas from the service if they were not fetched already (or the cache was just cleared)
                this::internalGetAvailableClientGeoAreas);
    }

    private List<ClientGeoArea> internalGetAvailableClientGeoAreas(AppVariant appVariant) {
        final Set<GeoArea> availableGeoAreas = appService.getAvailableGeoAreas(appVariant);
        return geoAreaClientModelMapper.toClientGeoAreas(availableGeoAreas, false);
    }

    @Override
    public void invalidateCache() {

        cacheCheckerGeoAreasByAppVariant.resetCacheChecks();
    }

}
