/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2023 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import de.fhg.iese.dd.platform.api.framework.ClientExceptionEntity;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithOauthIdNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.*;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.TreeMap;

/**
 * Logs requests and their metric values
 * <p>
 * In same package as base controller to allow accessing package-private methods
 */
@Log4j2
public class LogRequestsInterceptor implements HandlerInterceptor {

    public static final String REQUEST_ATTRIBUTE_ADDITIONAL_LOG_DETAILS = "de.fhg.iese.dd.platform.additionallog";
    public static final String REQUEST_ATTRIBUTE_EXCEPTION_ENTITY = "de.fhg.iese.dd.platform.exceptionentity";
    private static final String REQUEST_ATTRIBUTE_START_TIME = "de.fhg.iese.dd.platform.requeststarttime";

    //these markers are used in the log configuration to control which type of log message should go into which log
    /**
     * Messages with this marker are machine readable log messages of requests
     */
    private static final Marker REQUEST_JSON_MARKER = MarkerManager.getMarker("REQUEST_JSON");

    /**
     * Messages with this marker are textual, human readable log messages of requests.
     * <p>
     * They are excluded from the json log, since they do not contain additional information
     */
    public static final Marker REQUEST_MESSAGE_MARKER = MarkerManager.getMarker("REQUEST_MESSAGE");

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        try {
            if (!log.isDebugEnabled()) {
                return true;
            }
            if (!(handler instanceof HandlerMethod handlerMethod)) {
                return true;
            }
            Object bean = handlerMethod.getBean();
            if (!(bean instanceof BaseController controller)) {
                return true;
            }
            Method handlerMethodMethod = handlerMethod.getMethod();
            Logger controllerLog = getControllerLog(bean);
            request.setAttribute(REQUEST_ATTRIBUTE_START_TIME, System.currentTimeMillis());
            String appVariantIdentifier = getAppVariantIdentifier(controller);
            String oauthClientIdentifier = getOauthClientIdentifier(controller);
            Map<String, String> logMap = new TreeMap<>();
            logMap.put("direction", "IN");
            logMap.put("appVariantIdentifier", appVariantIdentifier);
            logMap.put("oauthClientIdentifier", oauthClientIdentifier);
            logMap.put("method", request.getMethod());
            logMap.put("requestPattern", getRequestPattern(request));
            logMap.put("requestURI", getRequestURI(request));
            logMap.put("personId", getCurrentPersonId(controller));

            controllerLog.log(Level.DEBUG, REQUEST_JSON_MARKER, logMap);
            controllerLog.log(Level.DEBUG, REQUEST_MESSAGE_MARKER,
                    "<--- {} {} ({}) by {} using {} ({})",
                    request.getMethod(),
                    getRequestURI(request),
                    handlerMethodMethod.getDeclaringClass().getSimpleName() + "#" + handlerMethodMethod.getName(),
                    getCurrentPersonInformation(controller),
                    appVariantIdentifier,
                    oauthClientIdentifier);
            return true;
        } catch (Exception e) {
            log.error("Failed to log preHandle", e);
            return true;
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception ex) {
        try {
            if (!log.isDebugEnabled()) {
                return;
            }
            if (!(handler instanceof HandlerMethod handlerMethod)) {
                return;
            }
            Object bean = handlerMethod.getBean();
            if (!(bean instanceof BaseController controller)) {
                return;
            }
            Logger controllerLog = getControllerLog(bean);
            int statusCode = response.getStatus();
            String statusName = toHttpStatusName(statusCode);

            String appVariantIdentifier = getAppVariantIdentifier(controller);
            String oauthClientIdentifier = getOauthClientIdentifier(controller);
            ClientExceptionEntity exceptionEntity =
                    getRequestAttribute(request, REQUEST_ATTRIBUTE_EXCEPTION_ENTITY, ClientExceptionEntity.class);
            //this should always be a map of that type (or null) since we create it
            @SuppressWarnings("unchecked")
            Map<String, String> additionalLogValues =
                    getRequestAttribute(request, REQUEST_ATTRIBUTE_ADDITIONAL_LOG_DETAILS, Map.class);
            long duration;
            Long startTime = getRequestAttribute(request, REQUEST_ATTRIBUTE_START_TIME, Long.class);
            if (startTime != null) {
                duration = System.currentTimeMillis() - startTime;
            } else {
                duration = 0;
            }

            Map<String, String> logMap = new TreeMap<>();
            // always add the details if they are there
            // they are added first, so that it is impossible to overwrite the values below
            if (!CollectionUtils.isEmpty(additionalLogValues)) {
                logMap.putAll(additionalLogValues);
            }
            logMap.put("direction", "OUT");
            logMap.put("appVariantIdentifier", appVariantIdentifier);
            logMap.put("oauthClientIdentifier", oauthClientIdentifier);
            logMap.put("method", request.getMethod());
            logMap.put("requestPattern", getRequestPattern(request));
            logMap.put("requestURI", getRequestURI(request));
            // if a person id was already provided in the additional log details, do not overwrite it
            if (!logMap.containsKey("personId")) {
                logMap.put("personId", getCurrentPersonId(controller));
            }
            logMap.put("duration", Long.toString(duration));
            logMap.put("status", Integer.toString(statusCode));
            if (exceptionEntity != null) {
                logMap.put("exceptionType", exceptionEntity.getType().name());
                logMap.put("exceptionMessage", exceptionEntity.getMessage());
            }

            Level logLevel;
            // server errors should be logged as error
            if (statusCode >= 500) {
                logLevel = Level.ERROR;
            } else {
                logLevel = Level.DEBUG;
            }
            // machine readable json log
            controllerLog.log(logLevel, REQUEST_JSON_MARKER, logMap);
            // human readable textual log entry, not part of the json log
            controllerLog.log(logLevel, REQUEST_MESSAGE_MARKER,
                    "---> {} {} status {} ({}) in {} ms",
                    request.getMethod(),
                    getRequestURI(request),
                    statusCode,
                    statusName,
                    duration);
        } catch (Exception e) {
            log.error("Failed to log afterCompletion", e);
        }
    }

    private String getRequestURI(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        //in this case the request is internally forwarded to the error page
        if ("/error".equals(requestURI)) {
            //this is the original request uri
            Object errorUri = request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);
            if (errorUri instanceof String) {
                return (String) errorUri;
            }
        }
        return requestURI;
    }

    private <T> T getRequestAttribute(HttpServletRequest request, String attributeName, Class<T> expectedClass) {
        Object attribute = request.getAttribute(attributeName);
        if (attribute != null && expectedClass.isAssignableFrom(attribute.getClass())) {
            @SuppressWarnings("unchecked")
            T result = (T) attribute;
            return result;
        }
        return null;
    }

    private Logger getControllerLog(Object bean) {
        Logger controllerLog = LogManager.getLogger(bean.getClass());
        if (controllerLog == null) {
            return log;
        }
        return controllerLog;
    }

    private String getCurrentPersonInformation(BaseController controller) {
        try {
            Person person;
            try {
                person = controller.getRequestContext().getPerson().orElse(null);
            } catch (PersonWithOauthIdNotFoundException e) {
                // This happens due to a special case, when we create a new user.
                // In this case an oAuthId is provided, but we don't find a person in the database
                return "new user";
            }
            if (person != null) {
                return String.format("user %s (%s) [%s]", person.getId(), person.getEmail(), person.getOauthId());
            } else {
                return "anonymous user";
            }
        } catch (Exception e) {
            log.error("Failed to get person information", e);
        }
        return "unknown person";
    }

    private String getCurrentPersonId(BaseController controller) {
        try {
            Person person;
            try {
                person = controller.getRequestContext().getPerson().orElse(null);
            } catch (PersonWithOauthIdNotFoundException e) {
                // This happens due to a special case, when we create a new user.
                // In this case an oAuthId is provided, but we don't find a person in the database
                return "new";
            }
            if (person != null) {
                return person.getId();
            } else {
                return "anonymous";
            }
        } catch (Exception e) {
            log.error("Failed to get person id", e);
        }
        return "unknown";
    }

    private String getRequestPattern(HttpServletRequest request) {
        Object requestPatternObject = request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String requestPattern;
        //in the case of "/error" it was internally forwarded
        if (requestPatternObject instanceof String && !"/error".equals(requestPatternObject)) {
            requestPattern = (String) requestPatternObject;
        } else {
            requestPattern = getRequestURI(request);
        }
        return request.getMethod() + " " + requestPattern;
    }

    private String getAppVariantIdentifier(BaseController controller) {
        try {
            return controller.getRequestContext().getAppVariant()
                    .map(AppVariant::getAppVariantIdentifier)
                    // if we can not find the app variant we do not show the app variant identifier header, this would be confusing
                    .orElse("unknown");
        } catch (Exception e) {
            log.error("Failed to get app variant identifier", e);
        }
        return "unknown";
    }

    private String getOauthClientIdentifier(BaseController controller) {
        try {
            return controller.getRequestContext().getOAuthClient()
                    .map(OauthClient::getOauthClientIdentifier)
                    // if we can not find the oauth client we do not show the oauth client identifier of the access token, this would be confusing
                    .orElse("unknown");
        } catch (Exception e) {
            log.error("Failed to get oauth client identifier", e);
        }
        return "unknown";
    }

    private String toHttpStatusName(int statusCode) {
        //for the most common ones we use a fast switch
        switch (statusCode) {
            case 200:
                return "OK";
            case 201:
                return "CREATED";
            case 400:
                return "BAD_REQUEST";
            case 401:
                return "UNAUTHORIZED";
            case 403:
                return "FORBIDDEN";
            case 404:
                return "NOT_FOUND";
        }
        HttpStatus status = HttpStatus.resolve(statusCode);
        if (status == null) {
            return "unknown";
        } else {
            return status.name();
        }
    }

}
