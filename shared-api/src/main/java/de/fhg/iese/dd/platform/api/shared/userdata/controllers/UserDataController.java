/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2017 Matthias Gerbershagen, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.userdata.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.userdata.clientmodel.ClientUserDataKeyValueEntry;
import de.fhg.iese.dd.platform.api.shared.userdata.clientmodel.UserDataClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.userdata.exceptions.CategoryNameInvalidException;
import de.fhg.iese.dd.platform.api.shared.userdata.exceptions.KeyNameInvalidException;
import de.fhg.iese.dd.platform.business.shared.userdata.services.IUserDataService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataCategory;
import de.fhg.iese.dd.platform.datamanagement.shared.userdata.model.UserDataKeyValueEntry;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({"userdata"})
@Api(tags = {"shared.userdata"})
public class UserDataController extends BaseController {

    @Autowired
    private IUserDataService userDataService;
    @Autowired
    private UserDataClientModelMapper userDataClientModelMapper;

    @ApiOperation(value = "Get all categories",
            notes = "Get all category names for the current user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/category")
    public List<String> getAllCategories(
            @RequestParam(required = false) Long since) {

        Person owner = getCurrentPersonNotNull();
        List<UserDataCategory> userDataCategories;
        if(since != null) {
            userDataCategories = userDataService.getAllCategoriesSince(owner, since);
        } else {
            userDataCategories = userDataService.getAllCategories(owner);
        }
        return userDataCategories.stream()
                .map(UserDataCategory::getName)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Add a category",
            notes = "Create a new category with the given name for the current user")
    @ApiExceptions({
        ClientExceptionType.CATEGORY_NAME_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("/category/{categoryName}")
    @ResponseStatus(HttpStatus.CREATED)
    public void addCategory(
            @PathVariable String categoryName) {

        if(StringUtils.isEmpty(categoryName)) {
            throw new CategoryNameInvalidException("The name should not be empty or null");
        }
        Person owner = getCurrentPersonNotNull();
        userDataService.createCategory(owner, categoryName);
    }

    @ApiOperation(value = "Remove a category",
            notes = "Remove a category by the given name for the current user."
                    + "All contained keys and their values are also removed!")
    @ApiExceptions({
        ClientExceptionType.CATEGORY_NAME_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping("/category/{categoryName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCategory(
            @PathVariable String categoryName) {

        if (StringUtils.isEmpty(categoryName)) {
            throw new CategoryNameInvalidException("The name should not be empty or null");
        }
        Person owner = getCurrentPersonNotNull();
        userDataService.removeCategory(owner, categoryName);
    }

    @ApiOperation(value = "Get all keys and values of a category",
            notes = "Get all key value pairs for the given category for the current user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/category/{categoryName}/key")
    public List<ClientUserDataKeyValueEntry> getKeyValueEntries(
            @PathVariable String categoryName,
            @RequestParam(required = false) Long since) {

        Person owner = getCurrentPersonNotNull();
        List<UserDataKeyValueEntry> userDataKeyValueEntries;
        if (since != null) {
            userDataKeyValueEntries = userDataService.getKeyValueEntriesSince(owner, categoryName, since);
        } else {
            userDataKeyValueEntries = userDataService.getKeyValueEntries(owner, categoryName);
        }
        return userDataKeyValueEntries.stream()
                .map(userDataClientModelMapper::toKeyValuePair)
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Set value of a key",
            notes = "Set the value of the given key for the given category for the current user.")
    @ApiExceptions({
        ClientExceptionType.CATEGORY_NAME_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/category/{categoryName}/key/{keyName}/value")
    public void setValueOfKey(
            @PathVariable String categoryName,
            @PathVariable String keyName,
            @RequestParam String value) {

        if (StringUtils.isEmpty(categoryName)) {
            throw new CategoryNameInvalidException("The name should not be empty or null");
        }
        Person owner = getCurrentPersonNotNull();
        userDataService.setValueOfKey(owner, categoryName, keyName, value);
    }

    @ApiOperation(value = "Get a value",
            notes = "Get a value of the given key for the given category for the current user")
    @ApiExceptions({
        ClientExceptionType.CATEGORY_NAME_INVALID,
        ClientExceptionType.KEY_NAME_INVALID,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/category/{categoryName}/key/{keyName}/value")
    public ClientUserDataKeyValueEntry getValue(
            @PathVariable String categoryName,
            @PathVariable String keyName) {

        if (StringUtils.isEmpty(categoryName)) {
            throw new CategoryNameInvalidException("The category name should not be empty or null");
        }
        if (StringUtils.isEmpty(keyName)) {
            throw new KeyNameInvalidException("The key name should not be empty or null");
        }

        Person owner = getCurrentPersonNotNull();
        UserDataKeyValueEntry userDataKeyValueEntry;
        userDataKeyValueEntry = userDataService.getValueOfKey(owner, categoryName, keyName);
        return userDataClientModelMapper.toKeyValuePair(userDataKeyValueEntry);
    }

    @ApiOperation(value = "Remove a key",
            notes = "Remove a given key and the attached value for the given category for the current user")
    @ApiExceptions({
        ClientExceptionType.CATEGORY_NAME_INVALID
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/category/{categoryName}/key/{keyName}")
    public void removeKeyValue(
            @PathVariable String categoryName,
            @PathVariable String keyName) {

        if (StringUtils.isEmpty(categoryName)) {
            throw new CategoryNameInvalidException("The name should not be empty or null");
        }
        Person owner = getCurrentPersonNotNull();
        userDataService.removeKeyValue(owner, categoryName, keyName);
    }

}
