/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Stefan Schweitzer, Danielle Korth, Balthasar Weitzel, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.ClientGeoArea;
import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@Api(tags = {"participants.geoarea"})
public class GeoAreaController extends BaseController {

    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;

    @ApiOperation(value = "Returns a geo area by id without children",
            notes = "Returns the geo area with the given id without children")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "geoArea/{geoAreaId}")
    public ClientGeoArea getGeoArea(
            @PathVariable String geoAreaId,
            @RequestParam(
                    required = false,
                    defaultValue = "false")
                    boolean includeBoundaryPoints) {

        return geoAreaClientModelMapper.toClientGeoArea(geoAreaService.findGeoAreaById(geoAreaId),
                includeBoundaryPoints);
    }

    @ApiOperation(value = "Returns geo areas by id without children",
            notes = "Returns geo areas with the given ids without children. If the geo areas have a parent-child " +
                    "relationship they are contained in each other as children. In this case the children attribute only " +
                    "contains those children that are in the list of ids.")
    @ApiExceptions({
            ClientExceptionType.GEO_AREA_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC)
    @GetMapping(value = "geoAreas")
    public List<ClientGeoArea> getGeoAreas(
            @RequestParam List<String> geoAreaIds,
            @RequestParam(
                    required = false,
                    defaultValue = "false")
                    boolean includeBoundaryPoints) {
        return geoAreaClientModelMapper.toClientGeoAreas(geoAreaService.findAllById(geoAreaIds),
                includeBoundaryPoints);
    }

}
