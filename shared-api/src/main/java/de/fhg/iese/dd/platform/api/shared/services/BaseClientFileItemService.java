/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Ben Burkhard, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.IClientFileItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.files.exceptions.FileItemNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.BaseFileItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IBaseFileItemService;

abstract class BaseClientFileItemService<F extends BaseFileItem, P extends BaseFileItemPlaceHolder<F>> implements IBaseClientFileItemService<F, P> {

    protected abstract IBaseFileItemService<F, ? extends TemporaryBaseFileItem<F>> getItemService();

    protected abstract P createPlaceHolder(F item, TemporaryBaseFileItem<F> temporaryItem);

    @Override
    public List<P> toItemPlaceHolders(List<? extends IClientFileItemPlaceHolder> clientItemPlaceHolders,
            Collection<F> existingItems, Person temporaryItemOwner) {
        if (clientItemPlaceHolders == null) {
            return null;
        } else {
            //create a map for faster lookup of item by id
            Map<String, F> itemIdsToItems;
            if (CollectionUtils.isEmpty(existingItems)) {
                itemIdsToItems = Collections.emptyMap();
            } else {
                itemIdsToItems = existingItems.stream()
                        .collect(Collectors.toMap(BaseEntity::getId, Function.identity()));
            }
            List<P> result = new ArrayList<>(clientItemPlaceHolders.size());
            for (IClientFileItemPlaceHolder clientItemPlaceHolder : clientItemPlaceHolders) {

                String itemId = clientItemPlaceHolder.getItemId();
                String temporaryItemId = clientItemPlaceHolder.getTemporaryItemId();

                checkPlaceHolder(clientItemPlaceHolder);

                if (StringUtils.isNotEmpty(itemId)) {
                    //lookup the existing item in the map
                    F item = itemIdsToItems.get(itemId);
                    if (item == null) {
                        throw new FileItemNotFoundException("The entity does not contain item with id {}", itemId);
                    }
                    result.add(createPlaceHolder(item, null));
                }
                if (StringUtils.isNotEmpty(temporaryItemId)) {
                    //get the temporary item, if it is available
                    TemporaryBaseFileItem<F> temporaryMediaItem =
                            getItemService().findTemporaryItemById(temporaryItemOwner, temporaryItemId);
                    result.add(createPlaceHolder(null, temporaryMediaItem));
                }
            }
            return result;
        }
    }

    @Override
    public P toItemPlaceHolder(IClientFileItemPlaceHolder clientItemPlaceHolder, F existingItem,
            Person temporaryItemOwner) {

        if (clientItemPlaceHolder == null) {
            return null;
        }
        checkPlaceHolder(clientItemPlaceHolder);

        String itemId = clientItemPlaceHolder.getItemId();
        String temporaryItemId = clientItemPlaceHolder.getTemporaryItemId();

        if (StringUtils.isNotEmpty(itemId)) {
            if (existingItem == null || !StringUtils.equals(BaseEntity.getIdOf(existingItem), itemId)) {
                throw new FileItemNotFoundException("The entity does not contain item with id {}", itemId);
            }
            return createPlaceHolder(existingItem, null);
        }

        if (StringUtils.isNotEmpty(temporaryItemId)) {
            //get the temporary media item, if it is available
            TemporaryBaseFileItem<F> temporaryMediaItem =
                    getItemService().findTemporaryItemById(temporaryItemOwner, temporaryItemId);
            return createPlaceHolder(null, temporaryMediaItem);
        }
        return null;
    }

    private void checkPlaceHolder(IClientFileItemPlaceHolder clientItemPlaceHolder) {

        if (StringUtils.isNotEmpty(clientItemPlaceHolder.getItemId()) ==
                StringUtils.isNotEmpty(clientItemPlaceHolder.getTemporaryItemId())) {
            throw new InvalidEventAttributeException("Invalid place holder, exactly one id needs to be set: {}",
                    clientItemPlaceHolder);
        }
    }

}
