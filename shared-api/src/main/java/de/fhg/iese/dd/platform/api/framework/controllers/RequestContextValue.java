/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import java.util.Optional;
import java.util.function.Supplier;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class RequestContextValue<T> {

    T value;
    Supplier<T> valueSupplier;
    Supplier<RuntimeException> exceptionWhenFetchingValue;
    Runnable afterFetchAction;
    boolean fetched;

    RequestContextValue(Supplier<T> valueSupplier) {
        this(valueSupplier, null);

    }

    RequestContextValue(Supplier<T> valueSupplier, Runnable afterFetchAction) {
        this.value = null;
        this.exceptionWhenFetchingValue = null;
        this.afterFetchAction = afterFetchAction;
        this.valueSupplier = valueSupplier;
        fetched = false;
    }

    private T getCachedValueOrFetchValue() {
        if (!fetched) {
            //we need to call the actual supplier method to get the value
            try {
                value = valueSupplier.get();
            } catch (RuntimeException e) {
                //we cache the exception here, so that we can throw it when the value is accessed
                exceptionWhenFetchingValue = () -> e;
            }
            fetched = true;
            if (afterFetchAction != null) {
                afterFetchAction.run();
            }
        }
        return value;
    }

    /**
     * Returns an optional for the value, suppressing the exception that was thrown when getting the value.
     *
     * @return the value
     */
    Optional<T> getValueOptionalSuppressingException() {
        return Optional.ofNullable(getCachedValueOrFetchValue());
    }

    /**
     * Returns the value or throws an exception if it could not retrieved. The exception is determined by the actual
     * method the value is retrieved.
     *
     * @return the value, never null
     *
     * @throws RuntimeException if there was an exception by the actual method that retrieved the value
     */
    T getValueNotNull() throws RuntimeException {
        if (exceptionWhenFetchingValue != null) {
            throw exceptionWhenFetchingValue.get();
        }
        return getValueOptionalSuppressingException().orElseThrow(exceptionWhenFetchingValue);
    }

}
