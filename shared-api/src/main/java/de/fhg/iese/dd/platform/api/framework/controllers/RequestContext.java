/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import de.fhg.iese.dd.platform.api.framework.modifiers.IContentFilterSettings;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthClientNotFoundException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;
import java.util.Set;

/**
 * Context of a call that serves as a cache for all relevant information that is processed in that call. The request
 * context holds proxies for the actual methods in {@link BaseController} and executes them once their value is
 * required. The result of these calls is then cached, also if the result was an exception.
 */
@Setter(AccessLevel.PACKAGE)
@Getter(AccessLevel.PACKAGE)
@Builder
public class RequestContext implements IContentFilterSettings {

    private RequestContextValue<Person> personValue;
    private RequestContextValue<String> oAuthUserIdValue;
    private RequestContextValue<String> oAuthClientIdentifierValue;
    private RequestContextValue<OauthClient> oAuthClientValue;
    private RequestContextValue<AppVariant> appVariantValue;
    private RequestContextValue<String> appVariantIdentifierValue;
    private RequestContextValue<String> apiKeyValue;
    private RequestContextValue<AppVariant> authenticatedAppVariantValue;
    private RequestContextValue<Set<String>> blockedPersonIdsValue;
    private RequestContextValue<Set<String>> flaggedEntityIdsValue;

    public Optional<Person> getPerson() {
        return personValue.getValueOptionalSuppressingException();
    }

    public Person getPersonNotNull() throws RuntimeException {
        return personValue.getValueNotNull();
    }

    public Optional<String> getOAuthUserId() {
        return oAuthUserIdValue.getValueOptionalSuppressingException();
    }

    public String getOAuthUserIdNotNull() throws RuntimeException {
        return oAuthUserIdValue.getValueNotNull();
    }

    public Optional<String> getOAuthClientIdentifier() {
        return oAuthClientIdentifierValue.getValueOptionalSuppressingException();
    }

    public String getOAuthClientIdentifierNotNull() throws RuntimeException {
        return oAuthClientIdentifierValue.getValueNotNull();
    }

    public Optional<OauthClient> getOAuthClient() {
        return oAuthClientValue.getValueOptionalSuppressingException();
    }

    public OauthClient getOAuthClientNotNull() throws OauthClientNotFoundException {
        return oAuthClientValue.getValueNotNull();
    }

    public Optional<AppVariant> getAppVariant() {
        return appVariantValue.getValueOptionalSuppressingException();
    }

    public AppVariant getAppVariantNotNull() throws RuntimeException {
        return appVariantValue.getValueNotNull();
    }

    public Optional<String> getAppVariantIdentifier() {
        return appVariantIdentifierValue.getValueOptionalSuppressingException();
    }

    public String getAppVariantIdentifierNotNull() throws RuntimeException {
        return appVariantIdentifierValue.getValueNotNull();
    }

    public Optional<String> getApiKey() {
        return apiKeyValue.getValueOptionalSuppressingException();
    }

    public String getApiKeyNotNull() throws RuntimeException {
        return apiKeyValue.getValueNotNull();
    }

    public Optional<AppVariant> getAuthenticatedAppVariant() {
        return authenticatedAppVariantValue.getValueOptionalSuppressingException();
    }

    public AppVariant getAuthenticatedAppVariantNotNull() throws RuntimeException {
        return authenticatedAppVariantValue.getValueNotNull();
    }

    @Override
    public String getCallingPersonId() {

        return getPerson().map(Person::getId).orElse(null);
    }

    @Override
    public Set<String> getBlockedPersonIds() {
        return blockedPersonIdsValue.getValueNotNull();
    }

    @Override
    public Set<String> getFlaggedEntityIds() {
        return flaggedEntityIdsValue.getValueNotNull();
    }

}
