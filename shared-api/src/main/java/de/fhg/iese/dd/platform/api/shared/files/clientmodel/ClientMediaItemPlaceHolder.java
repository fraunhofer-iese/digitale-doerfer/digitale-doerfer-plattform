/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Adeline Silva Schäfer, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientMediaItemPlaceHolder implements IClientFileItemPlaceHolder {

    private String mediaItemId;

    private String temporaryMediaItemId;

    @Override
    @JsonIgnore
    public String getItemId() {
        return mediaItemId;
    }

    @Override
    @JsonIgnore
    public String getTemporaryItemId() {
        return temporaryMediaItemId;
    }

    @Override
    public String toString() {
        return "ClientMediaItemPlaceHolder ["
                + (mediaItemId != null ? "mediaItemId=" + mediaItemId + ", " : "")
                + (temporaryMediaItemId != null ? "temporaryMediaItemId=" + temporaryMediaItemId : "")
                + "]";
    }

}
