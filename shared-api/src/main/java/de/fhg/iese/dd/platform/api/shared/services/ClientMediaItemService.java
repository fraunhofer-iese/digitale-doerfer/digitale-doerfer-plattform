/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.MediaItemPlaceHolder;
import de.fhg.iese.dd.platform.datamanagement.shared.files.model.TemporaryBaseFileItem;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IBaseFileItemService;
import de.fhg.iese.dd.platform.datamanagement.shared.files.services.IMediaItemService;

@Service
class ClientMediaItemService extends BaseClientFileItemService<MediaItem, MediaItemPlaceHolder> implements IClientMediaItemService {

    @Autowired
    private IMediaItemService mediaItemService;

    @Override
    protected IBaseFileItemService<MediaItem, ? extends TemporaryBaseFileItem<MediaItem>> getItemService() {
        return mediaItemService;
    }

    @Override
    protected MediaItemPlaceHolder createPlaceHolder(MediaItem item, TemporaryBaseFileItem<MediaItem> temporaryItem) {
        return MediaItemPlaceHolder.builder()
                .fileItem(item)
                .temporaryFileItem(temporaryItem)
                .build();
    }

}
