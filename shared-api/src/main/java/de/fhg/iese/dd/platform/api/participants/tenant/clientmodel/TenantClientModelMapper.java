/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.tenant.clientmodel;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TenantClientModelMapper {

    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;
    //only used to get the legacy root geo area
    @Autowired
    private IGeoAreaService geoAreaService;

    public ClientTenantExtended toClientTenantExtended(Tenant tenant) {
        if (tenant == null) {
            return null;
        }
        GeoArea rootGeoArea = geoAreaService.getLegacyRootGeoAreaOfTenant(tenant);
        return ClientTenantExtended.builder()
                .id(tenant.getId())
                .name(tenant.getName())
                .profilePicture(fileClientModelMapper.toClientMediaItem(tenant.getProfilePicture()))
                .tag(tenant.getTag())
                //this is legacy and will be removed
                .rootArea(geoAreaClientModelMapper.toClientGeoAreaExtended(rootGeoArea, false))
                .build();
    }

    public ClientCommunity createClientCommunity(Tenant tenant) {
        if (tenant == null) {
            return null;
        }
        return ClientCommunity.builder()
                .id(tenant.getId())
                .name(tenant.getName())
                .profilePicture(fileClientModelMapper.toClientMediaItem(tenant.getProfilePicture()))
                .build();
    }

    public ClientTenant toClientTenant(Tenant tenant) {
        if (tenant == null) {
            return null;
        }
        return ClientTenant.builder()
                .id(tenant.getId())
                .name(tenant.getName())
                .profilePicture(fileClientModelMapper.toClientMediaItem(tenant.getProfilePicture()))
                .build();
    }

}
