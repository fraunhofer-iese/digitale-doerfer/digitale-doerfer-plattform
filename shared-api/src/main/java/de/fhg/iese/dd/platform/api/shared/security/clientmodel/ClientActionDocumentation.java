/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Danielle Korth
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.clientmodel;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ClientActionDocumentation {

    @NotEmpty
    private final String name;
    @NotEmpty
    private final String description;
    @NotEmpty
    private final String permissionDescription;
    @NotNull
    private final List<String> endpointsActionRequired;
    @NotNull
    private final List<String> endpointsActionOptional;

}
