/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.clientmodel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientAppVariantWithSecrets extends ClientAppVariant {

    private String apiKey1;
    private Long apiKey1Created;
    private String apiKey2;
    private Long apiKey2Created;
    private String callBackUrl;
    private String externalApiKey;
    private String externalBasicAuthUsername;
    private String externalBasicAuthPassword;

}
