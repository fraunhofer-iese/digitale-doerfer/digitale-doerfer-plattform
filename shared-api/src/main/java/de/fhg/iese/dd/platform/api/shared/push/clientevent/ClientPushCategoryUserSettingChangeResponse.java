/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.push.clientevent;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.shared.push.clientmodel.ClientPushCategoryUserSetting;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientPushCategoryUserSettingChangeResponse extends ClientBaseEvent {

    @ApiModelProperty("deprecated, used changedPushCategoryUserSetting instead")
    public String getId() {
        return changedPushCategoryUserSetting.getId();
    }

    @ApiModelProperty("deprecated, used changedPushCategoryUserSetting instead")
    public String getName() {
        return changedPushCategoryUserSetting.getName();
    }

    @ApiModelProperty("deprecated, used changedPushCategoryUserSetting instead")
    public String getDescription() {
        return changedPushCategoryUserSetting.getDescription();
    }

    @ApiModelProperty("deprecated, used changedPushCategoryUserSetting instead")
    public boolean isLoudPushEnabled() {
        return changedPushCategoryUserSetting.isLoudPushEnabled();
    }

    @ApiModelProperty("deprecated, used changedPushCategoryUserSetting instead")
    public String getParentPushCategoryId() {
        return changedPushCategoryUserSetting.getParentPushCategoryId();
    }

    @NotNull
    private ClientPushCategoryUserSetting changedPushCategoryUserSetting;

    @NotEmpty
    @ApiModelProperty("categories that got changed too because they are child categories")
    private List<ClientPushCategoryUserSetting> changedDependentPushCategoryUserSettings;

}
