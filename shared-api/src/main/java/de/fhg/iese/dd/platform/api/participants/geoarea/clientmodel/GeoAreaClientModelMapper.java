/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;

@Component
public class GeoAreaClientModelMapper {

    private static final Comparator<BaseClientGeoArea<?>> COMPARE_GEO_AREA_BY_NAME =
            Comparator.comparing(BaseClientGeoArea::getName);

    @Autowired
    private FileClientModelMapper fileClientModelMapper;

    public List<ClientGeoAreaExtended> toClientGeoAreasExtended(final Collection<GeoArea> geoAreas,
            final boolean includeBoundaryPoints) {

        return toClientGeoAreaTree(geoAreas, g -> toClientGeoAreaExtended(g, includeBoundaryPoints));
    }

    public List<ClientGeoArea> toClientGeoAreas(final Collection<GeoArea> geoAreas,
            final boolean includeBoundaryPoints) {

        return toClientGeoAreaTree(geoAreas, g -> toClientGeoArea(g, includeBoundaryPoints));
    }

    private <T extends BaseClientGeoArea<T>> List<T> toClientGeoAreaTree(final Collection<GeoArea> geoAreas,
            Function<GeoArea, T> toClientGeoAreaMapper) {

        if (CollectionUtils.isEmpty(geoAreas)) {
            return Collections.emptyList();
        }
        //convert all GeoAreas to ClientGeoAreas and put them into a map for fast referencing by id
        final Map<String, T> clientGeoAreaById = geoAreas.stream()
                .map(toClientGeoAreaMapper)
                .collect(Collectors.toMap(ClientBaseEntity::getId, Function.identity()));

        //pre-processing: Divide list of areas into root areas and child areas
        final List<T> rootAreas = new ArrayList<>();
        final Set<GeoArea> availableChildAreas =
                new HashSet<>(); //we need to call .getParentArea on these elements, hence, we store GeoAreas here

        for (GeoArea geoArea : geoAreas) {
            //An area is a root area (regarding the current set of geoAreas) if its parent is null or if its
            //parent is not contained in geoAreas. The latter check is made more efficient by using the already
            //built hash map of ids.
            if (geoArea.getParentArea() == null || !clientGeoAreaById.containsKey(geoArea.getParentArea().getId())) {
                rootAreas.add(clientGeoAreaById.get(geoArea.getId()));
            } else {
                availableChildAreas.add(geoArea);
            }
        }
        //sort rootAreas by name
        rootAreas.sort(COMPARE_GEO_AREA_BY_NAME);

        //process all geoAreas and build the list of childAreas
        for (GeoArea area : geoAreas) {

            final T clientGeoArea = clientGeoAreaById.get(area.getId());

            //optimization: every geoArea can only be added once as a child. So we store already "assigned" child areas
            //in this set and remove them from the list of available child areas in every iteration of the outer loop
            final Set<GeoArea> childAreasToRemove = new HashSet<>();

            for (GeoArea aPossibleChildArea : availableChildAreas) {
                if (aPossibleChildArea.getParentArea().equals(area)) { //we have a parent-child relation
                    if (clientGeoArea.getChildGeoAreas() == null) {
                        clientGeoArea.setChildGeoAreas(new ArrayList<>());
                    }
                    clientGeoArea.getChildGeoAreas().add(clientGeoAreaById.get(aPossibleChildArea.getId()));
                    childAreasToRemove.add(aPossibleChildArea); //"mark" area for deletion
                }
            }
            //sort by name
            if (clientGeoArea.getChildGeoAreas() != null) {
                clientGeoArea.getChildGeoAreas().sort(COMPARE_GEO_AREA_BY_NAME);
            }

            availableChildAreas.removeAll(childAreasToRemove); //see "optimization" above
        }

        return rootAreas;
    }

    public ClientGeoArea toClientGeoArea(final GeoArea geoArea, final boolean includeGeoJson) {
        if (geoArea == null) {
            return null;
        }
        return ClientGeoArea.builder()
                .id(geoArea.getId())
                .name(geoArea.getName())
                .geoAreaType(geoArea.getType())
                .logo(fileClientModelMapper.toClientMediaItem(geoArea.getLogo()))
                .geoJson(includeGeoJson ? geoArea.getBoundaryJson() : null)
                .zip(geoArea.getZip())
                .center(geoArea.getCenter() == null ? null :
                        new ClientGPSLocation(geoArea.getCenter().getLatitude(), geoArea.getCenter().getLongitude()))
                .leaf(geoArea.isCached() ? geoArea.isLeaf() : null)
                .depth(geoArea.isCached() ? geoArea.getDepth() : null)
                .childGeoAreas(null)
                .customAttributesJson(geoArea.getCustomAttributesJson())
                .build();
    }

    @SuppressWarnings("deprecation")
    public ClientGeoAreaExtended toClientGeoAreaExtended(final GeoArea geoArea, final boolean includeGeoJson) {
        if (geoArea == null) {
            return null;
        }
        return ClientGeoAreaExtended.builder()
                .id(geoArea.getId())
                .parentId(BaseEntity.getIdOf(geoArea.getParentArea()))
                .name(geoArea.getName())
                .geoAreaType(geoArea.getType())
                .logo(fileClientModelMapper.toClientMediaItem(geoArea.getLogo()))
                .geoJson(includeGeoJson ? geoArea.getBoundaryJson() : null)
                .zip(geoArea.getZip())
                .center(geoArea.getCenter() == null ? null :
                        new ClientGPSLocation(geoArea.getCenter().getLatitude(), geoArea.getCenter().getLongitude()))
                .leaf(geoArea.isCached() ? geoArea.isLeaf() : null)
                .depth(geoArea.isCached() ? geoArea.getDepth() : null)
                .childGeoAreas(null)
                .customAttributesJson(geoArea.getCustomAttributesJson())
                .tenantId(BaseEntity.getIdOf(geoArea.getTenant()))
                .countDirectChildren(geoArea.isCached() ? geoArea.getCountDirectChildren() : null)
                .countChildren(geoArea.isCached() ? geoArea.getCountChildren() : null)
                .build();
    }

}
