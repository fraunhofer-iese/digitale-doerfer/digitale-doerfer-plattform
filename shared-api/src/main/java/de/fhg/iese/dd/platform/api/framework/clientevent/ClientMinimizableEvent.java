/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.clientevent;

/**
 * An event that is able to reduce it's size to a minimum without loosing the overall semantic.
 * <p/>
 * This is required for sending push messages with entities. If the event is too big for being sent via push, it can be
 * minimized. In the minimized state the event (often) only contains the ids of entities instead the entity itself.
 */
public interface ClientMinimizableEvent {

    ClientBaseEvent toMinimizedEvent();

}
