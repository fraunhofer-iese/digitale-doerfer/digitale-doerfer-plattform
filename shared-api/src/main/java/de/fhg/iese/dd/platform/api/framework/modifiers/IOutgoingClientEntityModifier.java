/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2024 Johannes Schneider, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;
import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientFilterStatus;
import de.fhg.iese.dd.platform.api.framework.clientmodel.FilteredClientBaseEntity;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.AntPathMatcher;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * Implement this interface to modify client entities before they are serialized and returned via a rest controller
 *
 * @param <E> objects of this type or sub types will be handled by this modifier
 */
public interface IOutgoingClientEntityModifier<E extends ClientBaseEntity> {

    /**
     * This modifier is applied to the given type and its subtypes
     *
     * @return never return {@code null}
     */
    Class<E> getType();

    /**
     * This modifier is applied if a {@link ClientBaseEvent} is pushed and the class name of the event starts with one
     * of the prefixes.
     *
     * @return never return {@code null}. The empty set matches nothing.
     */
    default Set<String> getPushEventPackagePrefixes() {
        return Collections.emptySet();
    }

    /**
     * This modifier is applied if the outgoing entity is produced by a REST controller extending {@link BaseController}
     * that is registered to a path matching the returned path patterns. Matching is performed in the {@link
     * AntPathMatcher} sense (hence you can use something like "/grapevine/**").
     * </p>
     * To exclude patterns and avoid many specific patterns here (e.g. if just one endpoint should not be modified), use
     * {@link #getExcludedPathPatterns()}.
     *
     * @return never return {@code null}. The empty set matches nothing. Due to performance reasons, only use path "/**"
     *         when it is not possible to use a more specific pattern.
     */
    Set<String> getPathPatterns();

    /**
     * This modifier can be further restricted from the matches by {@link #getPathPatterns()} by providing patterns that
     * exclude specific paths.
     *
     * @return never return {@code null}. The empty set excludes nothing and is returned in the default implementation.
     */
    default Set<String> getExcludedPathPatterns() {
        return Collections.emptySet();
    }

    /**
     * Modify the entity in the desired way, either when it is returned by a REST call or pushed to a client.
     *
     * @param clientEntity          the entity to be modified
     * @param contentFilterSettings settings for the receiver of the clientEntity, never {@code null}
     *
     * @return true if you want other modifiers for the same type to be executed or false if not. You can control the
     *         order in which multiple modifiers are applied to the same entity by using {@link
     *         org.springframework.core.annotation.Order}.
     */
    boolean modify(E clientEntity, IContentFilterSettings contentFilterSettings);

    /**
     * Utility method that can be called by implementing classes to handle flagging and blocking. If the creatorId is in
     * the list of blocked person IDs in the given request context, the filter status of the client entity is set to
     * {@link ClientFilterStatus#BLOCKED}. If the id of entity is in the list of flagged entity IDs in the given request
     * context, the filter status of the client entity is set to {@link ClientFilterStatus#FLAGGED}. Otherwise, nothing
     * is changed.
     *
     * @param entity                 must not be {@code null}
     * @param creatorId              can be {@code null} - in this case, blocking is not handled
     * @param filterSettings must not be {@code null}
     */
    static void handleFlaggingAndBlocking(FilteredClientBaseEntity entity, String creatorId, IContentFilterSettings filterSettings) {
        Objects.requireNonNull(entity);
        Objects.requireNonNull(filterSettings);
        if (creatorId != null && filterSettings.getBlockedPersonIds().contains(creatorId)) {
            //blocked precedes over filtered
            entity.setFilterStatus(ClientFilterStatus.BLOCKED);
        } else {
            if (filterSettings.getFlaggedEntityIds().contains(entity.getId()) &&
                    //if the creator flagged his own entity we do not mark it as flagged
                    !StringUtils.equals(creatorId, filterSettings.getCallingPersonId())) {
                entity.setFilterStatus(ClientFilterStatus.FLAGGED);
            }
        }
    }

}
