/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.clientevent;

import de.fhg.iese.dd.platform.api.framework.clientevent.ClientBaseEvent;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class ClientReferenceDataDeletionRequest extends ClientBaseEvent {

    @ApiModelProperty(value = "If true the entities that are referencing the entity to be deleted are deleted, too. " +
            "Default is false")
    @Builder.Default
    private boolean deleteDependentEntities = false;

    @ApiModelProperty(required = true, value = "Fully qualified class name of the entity")
    private String classNameEntity;

    @ApiModelProperty(required = true, value = "Id of the entity to be deleted")
    private String idEntityToBeDeleted;

    @ApiModelProperty(value = "Id of the the entity that should be used instead. " +
            "Must be provided if deleteDependentEntities is false. " +
            "All references to the entity to be deleted will point to this one if deleteDependentEntities is false.")
    private String idEntityToBeUsedInstead;

}
