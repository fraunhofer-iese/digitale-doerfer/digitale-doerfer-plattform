/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2017 - 2019 Johannes Schneider, Dominik Schnier
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.clientmodel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.RoleAssignment;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;

@Component
public class RoleClientModelMapper {

    @Autowired
    private IRoleService roleService;

    public ClientRoleInformation toClientRoleInformation(BaseRole<? extends NamedEntity> role)
            throws IllegalArgumentException {

        if (role.getKey() == null || role.getDisplayName() == null || role.getDescription() == null ||
                role.getRelatedEntityClass() == null) {
            throw new IllegalArgumentException("attribute must not be null");
        }

        return ClientRoleInformation.builder()
                .key(role.getKey())
                .displayName(role.getDisplayName())
                .description(role.getDescription())
                .relatedEntity(role.getRelatedEntityClass().getSimpleName())
                .build();
    }

    public ClientRoleInformation toClientRoleInformation(RoleAssignment roleAssignment) {
        return toClientRoleInformation(roleService.getRole(roleAssignment.getRole()));
    }

    public ClientRoleAssignment toClientRoleAssignment(RoleAssignment roleAssignment) throws IllegalArgumentException {

        ClientRoleAssignment clientRoleAssignment = ClientRoleAssignment.builder()
                .personId(roleAssignment.getPerson().getId())
                .relatedEntityId(roleAssignment.getRelatedEntityId())
                .roleKey(roleService.getKeyForRole(roleAssignment.getRole()))
                .build();

        if (clientRoleAssignment.getRoleKey() == null || clientRoleAssignment.getPersonId() == null) {
            throw new IllegalArgumentException("attribute must not be null");
        }

        clientRoleAssignment.setId(roleAssignment.getId());
        if (roleAssignment.getRelatedEntity() != null) {
            clientRoleAssignment.setRelatedEntityName(roleAssignment.getRelatedEntity().getName());
        }
        if (roleAssignment.getRelatedEntityTenant() != null) {
            clientRoleAssignment.setRelatedEntityTenantId(roleAssignment.getRelatedEntityTenant().getId());
            clientRoleAssignment.setRelatedEntityTenantName(roleAssignment.getRelatedEntityTenant().getName());
        }
        return clientRoleAssignment;
    }

}
