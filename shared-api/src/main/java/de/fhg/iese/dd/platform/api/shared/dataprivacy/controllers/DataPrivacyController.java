/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2023 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.dataprivacy.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.dataprivacy.clientmodel.ClientDataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.DataPrivacyReportRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.events.PersonDeleteRequest;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.ICommonDataPrivacyService;
import de.fhg.iese.dd.platform.business.shared.dataprivacy.services.IExternalDataPrivacyService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataCollection;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyDataDeletion;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.DataPrivacyWorkflowAppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowAppVariantStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.dataprivacy.model.enums.DataPrivacyWorkflowTrigger;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.EnumSet;

@RestController
@RequestMapping("/dataprivacy")
@Api(tags = {"shared.dataprivacy"})
public class DataPrivacyController extends BaseController {

    private static final int MAX_DESCRIPTION_LENGTH = 255;
    private static final int MAX_STATUS_MESSAGE_LENGTH = 512;
    private static final EnumSet<ClientDataPrivacyWorkflowAppVariantStatus> POSITIVE_STATUS =
            EnumSet.of(ClientDataPrivacyWorkflowAppVariantStatus.IN_PROGRESS,
                    ClientDataPrivacyWorkflowAppVariantStatus.FINISHED);
    private static final EnumSet<ClientDataPrivacyWorkflowAppVariantStatus> NEGATIVE_STATUS =
            EnumSet.of(ClientDataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND,
                    ClientDataPrivacyWorkflowAppVariantStatus.FAILED);

    @Autowired
    private ICommonDataPrivacyService commonDataPrivacyService;
    @Autowired
    private IExternalDataPrivacyService externalDataPrivacyService;

    @ApiOperation(value = "Sends the data privacy report via mail asynchronously",
            notes = """
                    The report is sent to the email of the given person.

                    Returns OK when the action could be started successfully. Runs the delete process asynchronously.""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping(value = "report/mail")
    public void collectUserDataAndSendMail() {

        final Person person = getCurrentPersonNotNull();

        notify(DataPrivacyReportRequest.builder()
                .person(person)
                .trigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                .build());
    }

    @ApiOperation(value = "Receives a response to a data privacy collection",
            notes = """
                    External systems can respond in multiple ways (see parameter description for more details):
                    - With a *file* and *description* and status `IN_PROGRESS` or `FINISHED`.
                    - With a *description* and status `IN_PROGRESS` or `FINISHED`.
                    - With a *statusMessage* and status `PERSON_NOT_FOUND` or `FAILED`.
                    - A *statusMessage* can always be added, it is only for monitoring and logging and not given to the user.

                    It is possible to call this endpoint multiple times, in order to add multiple files. In case the data collection on the external system is finished, the last call should have the status `FINISHED`. In case the person was not found on the external system, the call should have the status `PERSON_NOT_FOUND`. After all external systems signaled that they are finished, the data privacy report is generated and sent to the user.""")
    @ApiExceptions({
            ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND,
            ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED,
            ClientExceptionType.DATA_PRIVACY_WORKFLOW_RESPONSE_REJECTED
    })
    @ApiException(value = ClientExceptionType.DATA_PRIVACY_WORKFLOW_RESPONSE_REJECTED,
            reason = "The files have to be in the list of allowed mime types and extensions, otherwise they are rejected.")
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY)
    @PostMapping(value = "report/external")
    public void receiveCollectionResponseFromExternalSystem(
            @RequestParam
            @ApiParam("Id of the data collection as it was sent to the external system. This is *not* the person id.")
                    String dataCollectionId,
            @RequestParam(required = false)
            @ApiParam("""
                    This file will be added to the data privacy report section about the calling app variant and provided to the user.
                    A description is optional (but recommended) so that the user understands what the file is about.
                    If no file name is provided, it is generated. If it is provided, it is trimmed to a length of 100 and the extension is normalized based on the file content.
                    Allowed file types are html, pdf, images""")
                    MultipartFile file,
            @RequestParam(required = false)
            @ApiParam("This description will be added to the data privacy report section about the calling app " +
                    "variant and provided to the user.\n" +
                    "max length: " + MAX_DESCRIPTION_LENGTH)
                    String description,
            @RequestParam(required = false)
            @ApiParam("This status message is only for internal monitoring and logging. " +
                    "It will not be shown to the user and is only logged.\n" +
                    "max length: " + MAX_STATUS_MESSAGE_LENGTH + " (will be truncated)")
                    String statusMessage,
            @RequestParam ClientDataPrivacyWorkflowAppVariantStatus status
    ) {

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        // in positive cases either a file or a description is required
        if (POSITIVE_STATUS.contains(status)
                && (file == null || file.isEmpty())
                && StringUtils.isEmpty(description)) {
            throw new BadRequestException(
                    "No file and no description was provided in positive response for data collection '{}'",
                    dataCollectionId);
        }
        //in a negative case no file or description should be provided
        if (NEGATIVE_STATUS.contains(status)
                && (!(file == null || file.isEmpty())
                || StringUtils.isNotBlank(description))) {
            throw new BadRequestException(
                    "File or description was provided in negative response for data collection '{}'",
                    dataCollectionId);
        }
        //validation does not work for request parameters (also with various annotations)
        if (StringUtils.length(description) > MAX_DESCRIPTION_LENGTH) {
            throw new BadRequestException("Description is too long: {}/{}",
                    StringUtils.length(description), MAX_DESCRIPTION_LENGTH);
        }
        String truncatedStatusMessage = StringUtils.abbreviate(statusMessage, MAX_STATUS_MESSAGE_LENGTH);

        DataPrivacyDataCollection dataCollection = commonDataPrivacyService.findDataCollectionById(dataCollectionId);
        final DataPrivacyWorkflowAppVariant dataCollectionAppVariant = commonDataPrivacyService
                .findDataPrivacyWorkflowAppVariant(dataCollection, appVariant);

        externalDataPrivacyService.addDataCollectionExternalSystemResponse(
                dataCollectionAppVariant,
                toDataPrivacyWorkflowAppVariantStatus(status),
                truncatedStatusMessage,
                file,
                description);
    }

    @ApiOperation(value = "Deletes the given person",
            notes = "Returns OK when the action could be started successfully. Runs the delete process asynchronously.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @DeleteMapping(value = "myPersonAndAllMyData")
    public void deletePerson() {

        final Person person = getCurrentPersonNotNull();

        notify(PersonDeleteRequest.builder()
                .person(person)
                .trigger(DataPrivacyWorkflowTrigger.USER_TRIGGERED)
                .build());
    }

    @ApiOperation(value = "Receives a response to a data privacy deletion",
            notes = "External systems can respond with current status of the data privacy deletion.\n" +
                    "In case the data deletion is finished, the last call should have the status `FINISHED`. " +
                    "After all external systems signaled that they are finished, the internal data privacy deletion is processed.")
    @ApiExceptions({
            ClientExceptionType.DATA_PRIVACY_WORKFLOW_NOT_FOUND,
            ClientExceptionType.DATA_PRIVACY_WORKFLOW_ALREADY_FINISHED
    })
    @ApiAuthentication(value = ApiAuthenticationType.API_KEY)
    @PostMapping(value = "deletion/external")
    public void receiveDeletionResponseFromExternalSystem(
            @RequestParam
            @ApiParam("Id of the data deletion as it was sent to the external system. This is *not* the person id.")
                    String dataDeletionId,
            @RequestParam(required = false)
            @ApiParam("This status message is only for internal monitoring and logging. " +
                    "It will not be shown to the user and is only logged.\n" +
                    "max length: " + MAX_STATUS_MESSAGE_LENGTH + " (will be truncated)")
                    String statusMessage,
            @RequestParam ClientDataPrivacyWorkflowAppVariantStatus status
    ) {

        final AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        final DataPrivacyDataDeletion dataDeletion = commonDataPrivacyService.findDataDeletionById(dataDeletionId);
        final DataPrivacyWorkflowAppVariant dataDeletionAppVariant = commonDataPrivacyService
                .findDataPrivacyWorkflowAppVariant(dataDeletion, appVariant);

        String truncatedStatusMessage = StringUtils.abbreviate(statusMessage, MAX_STATUS_MESSAGE_LENGTH);

        externalDataPrivacyService.addDataDeletionExternalSystemResponse(
                dataDeletionAppVariant,
                toDataPrivacyWorkflowAppVariantStatus(status),
                truncatedStatusMessage);
    }

    private DataPrivacyWorkflowAppVariantStatus toDataPrivacyWorkflowAppVariantStatus(
            ClientDataPrivacyWorkflowAppVariantStatus status) {

        switch (status) {
            case IN_PROGRESS:
                return DataPrivacyWorkflowAppVariantStatus.IN_PROGRESS;
            case FINISHED:
                return DataPrivacyWorkflowAppVariantStatus.FINISHED;
            case PERSON_NOT_FOUND:
                return DataPrivacyWorkflowAppVariantStatus.PERSON_NOT_FOUND;
            case FAILED:
                return DataPrivacyWorkflowAppVariantStatus.FAILED;
            default:
                throw new BadRequestException("Status '{}' is not a valid data collection status",
                        status.name());
        }
    }

}
