/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2015 - 2024 Steffen Hupp, Torsten Lenhart, Balthasar Weitzel, Johannes Schneider
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.misc.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.misc.clientevent.ClientReferenceDataDeletionRequest;
import de.fhg.iese.dd.platform.api.shared.push.services.IClientPushService;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.IReferenceDataDeletionService;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataChangeResult;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataCheckDeletionImpactRequest;
import de.fhg.iese.dd.platform.business.framework.referencedata.deletion.ReferenceDataDeletionRequest;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.BaseAdminTask;
import de.fhg.iese.dd.platform.business.shared.admintasks.services.IAdminTaskService;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailSenderService;
import de.fhg.iese.dd.platform.business.shared.init.IDataInitializer.DataInitializerResult;
import de.fhg.iese.dd.platform.business.shared.init.services.IDataInitializerService;
import de.fhg.iese.dd.platform.business.shared.phone.services.ISMSSenderService;
import de.fhg.iese.dd.platform.business.shared.security.SuperAdminAction;
import de.fhg.iese.dd.platform.datamanagement.framework.JsonMapping;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotFoundException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.framework.services.IPersistenceSupportService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.SuperAdmin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/administration")
@Api(tags = {"admin", "shared.admin"})
class SharedAdminController extends BaseController {

    @Autowired
    private IDataInitializerService dataInitializerService;

    @Autowired
    private IAdminTaskService adminTaskService;

    @Autowired
    private IEmailSenderService emailSenderService;

    @Autowired
    private ISMSSenderService smsSenderService;

    @Autowired
    private IClientPushService clientPushService;

    @Autowired
    private IReferenceDataDeletionService referenceDataDeletionService;

    @Autowired
    private IPersistenceSupportService genericRepositoryService;

    @ApiOperation(value = "List all Client Exception Types",
            notes = "Returns a list of all client exception types (name, explanation, http status code)")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredRoles = {SuperAdmin.class}, requiredActions = {
            SuperAdminAction.class})
    @GetMapping(value = "info/clientexceptiontypes")
    public List<String> getClientExceptionTypes() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return Stream.of(ClientExceptionType.values())
                .map(cet -> "(" + cet.name() + "','" + cet.getExplanation() + "'," + cet.getHttpStatus() + ")")
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "List available admin tasks",
            notes = "List all registered admin tasks and their description")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "adminTasks")
    public String getAdminTaskDescriptions() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return adminTaskService.getAllAdminTaskIdsAndDescriptions();
    }

    @ApiOperation(value = "Execute admin task",
            notes = """
                    Execute an admin task identified by its name. The summary of the task is returned.

                    If dryRun is true the task does not change any data. The interpretation of the parameters is left open to the actual admin task""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping(value = "adminTasks/execute")
    public String executeAdminTask(
            @RequestParam String adminTaskName,

            @RequestParam(required = false, defaultValue = "true")
            @ApiParam("Flag to signal the task to not change any data")
                    boolean dryRun,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "Page to start from (including), starts from 0", allowableValues = "range[0, infinity]")
                    int pageFrom,

            @RequestParam(required = false, defaultValue = "0")
            @ApiParam(value = "Page to end with (including), starts from 0", allowableValues = "range[0, infinity]")
                    int pageTo,

            @RequestParam(required = false, defaultValue = "50")
            @ApiParam(value = "Number of entities on a page [" +
                    BaseAdminTask.AdminTaskParameters.MIN_PAGE_SIZE + ", " +
                    BaseAdminTask.AdminTaskParameters.MAX_PAGE_SIZE + "]")
                    int pageSize,

            @RequestParam(required = false, defaultValue = "3")
            @ApiParam(value = "Parallelism for actions per page [" +
                    BaseAdminTask.AdminTaskParameters.MIN_PARALLELISM_PER_PAGE + ", " +
                    BaseAdminTask.AdminTaskParameters.MAX_PARALLELISM_PER_PAGE + "]")
                    int parallelismPerPage,

            @RequestParam(required = false, defaultValue = "10")
            @ApiParam(value = "Timeout for all actions for all items on one page in seconds [" +
                    BaseAdminTask.AdminTaskParameters.MIN_PAGE_TIMEOUT_SECONDS + ", " +
                    BaseAdminTask.AdminTaskParameters.MAX_PAGE_TIMEOUT_SECONDS + "]")
                    long pageTimeoutSeconds,

            @RequestParam(required = false, defaultValue = "2000")
            @ApiParam(value = "Wait time between processing pages in milliseconds [" +
                    BaseAdminTask.AdminTaskParameters.MIN_DELAY_BETWEEN_PAGES_MILLISECONDS + ", " +
                    BaseAdminTask.AdminTaskParameters.MAX_DELAY_BETWEEN_PAGES_MILLISECONDS + "]")
                    long delayBetweenPagesMilliseconds) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        BaseAdminTask.AdminTaskParameters adminTaskParameters = BaseAdminTask.AdminTaskParameters.builder()
                .dryRun(dryRun)
                .pageFrom(pageFrom)
                .pageTo(pageTo)
                .pageSize(pageSize)
                .parallelismPerPage(parallelismPerPage)
                .pageTimeoutSeconds(pageTimeoutSeconds)
                .delayBetweenPagesMilliseconds(delayBetweenPagesMilliseconds)
                .build();

        return adminTaskService.executeAdminTask(adminTaskName, adminTaskParameters);
    }

    @ApiOperation(value = "List available data initializers",
            notes = "List all registered data initializers and their topics")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "initialize")
    public List<String> getDataInitializerIds() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return dataInitializerService.getAllDataInitializerIds();
    }

    @ApiOperation(value = "List available data initializer topics",
            notes = "List all topics of registered data initializers")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "initialize/topic")
    public List<String> getDataInitializerTopics() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return dataInitializerService.getAllDataInitializerTopics();
    }

    @ApiOperation(value = "List available data initializer use cases",
            notes = "List all use cases of registered data initializers")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "initialize/useCase")
    public List<String> getDataInitializerUseCases() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return dataInitializerService.getAllDataInitializerUseCases();
    }

    @ApiOperation(value = "Creates or updates initial data",
            notes = """
                    If * is used, for every topic the initial data is created or updated.\s

                    The data initializes are constructed in a way that this method can be called multiple times.
                    If a use case is provided, the topics are ignored.""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PutMapping(value = "initialize/createInitialData")
    public ResponseEntity<String> createInitialData(
            @RequestParam(required = false) List<String> topics,
            @RequestParam(required = false) String useCase) {

        Person caller = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, caller);
        DataInitializerResult dataInitializerResult;
        if (StringUtils.isNotEmpty(useCase)) {
            dataInitializerResult = dataInitializerService.createInitialDataForUseCase(useCase, caller);
        } else {
            if (CollectionUtils.isEmpty(topics)) {
                dataInitializerResult = dataInitializerService.createInitialData("", Collections.emptySet(), caller);
            } else {
                dataInitializerResult = dataInitializerService.createInitialData("", new HashSet<>(topics), caller);
            }
        }
        if (dataInitializerResult.wasSuccessful()) {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Drops data",
            notes = "If * is used, for every topic the data is dropped. \n"
                    + "This will not work for most of the topics, these have to be deleted in the DB.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "initialize/dropData")
    public ResponseEntity<String> dropAllData(
            @RequestParam String check,
            @RequestParam(required = false) List<String> topics) {

        Person caller = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, caller);
        if (!"i am sure to drop all data".equals(check)) {
            return new ResponseEntity<>("skipped", HttpStatus.UNAUTHORIZED);
        }
        DataInitializerResult dataInitializerResult;
        if (topics == null || topics.isEmpty()) {
            dataInitializerResult = dataInitializerService.dropData(Collections.emptySet(), caller);
        } else {
            dataInitializerResult = dataInitializerService.dropData(new HashSet<>(topics), caller);
        }

        if (dataInitializerResult.wasSuccessful()) {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(dataInitializerResult.message(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Send an email for testing purposes",
            notes = "Send an email using the configured mail account.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "sendEmail")
    public String sendEmail(
            @RequestParam(required = false) @ApiParam("If left blank the default address is used") String fromEmail,
            @RequestParam(required = false) String toName,
            @RequestParam String toEmail,
            @RequestParam String subject,
            @RequestBody String text) throws MessagingException {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        String fromEmailAddress;
        if (StringUtils.isBlank(fromEmail)) {
            fromEmailAddress = getConfig().getEmail().getAddress();
        } else {
            fromEmailAddress = fromEmail;
        }
        emailSenderService.sendEmail(fromEmailAddress, toName, toEmail, subject, text, Collections.emptyList(), false);
        if (!getConfig().getEmail().isEnabled()) {
            return String.format(
                    "email is deactivated, team notification sent with text '%s' and subject '%s' and text '%s'",
                    toEmail, subject, text);
        } else {
            return String.format(
                    "email sent from '%s' to '%s' with subject '%s' and text '%s'",
                    fromEmailAddress, toEmail, subject, text);
        }
    }

    @ApiOperation(value = "Send an sms for testing purposes",
            notes = "Send an sms using the configured sms sender. " +
                    "This operation costs money! Only use own numbers as recipients!")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "/shared/sms/send")
    public String sendSms(
            @RequestParam String senderId,
            @RequestParam String recipientNumber,
            @RequestParam String check,
            @RequestBody String text) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        if (!"i know what i am doing".equals(check)) {
            throw new NotAuthorizedException("Seems you don't know what you are doing!");
        }

        smsSenderService.sendSMS(senderId, recipientNumber, text);
        if (!getConfig().getTwilioSms().isEnabled()) {
            return "SMS is deactivated";
        } else {
            return "SMS sent";
        }
    }

    @ApiOperation(value = "Validate a phone number for testing purposes",
            notes = "Verify a phone number and query the details. This operation costs money!")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @PostMapping(value = "/shared/sms/validate")
    public String validatePhoneNumber(
            @RequestParam String number,
            @RequestParam String check,
            @RequestParam(required = false) String countryCode) {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());

        if (!"i know what i am doing".equals(check)) {
            throw new NotAuthorizedException("Seems you don't know what you are doing!");
        }

        try {
            return JsonMapping.defaultJsonWriter()
                    .writeValueAsString(smsSenderService.validateLocalPhoneNumber(number, countryCode));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @ApiOperation(value = "Push events overview as plain text",
            notes = "Overview of all pushed events and a brief documentation when they are sent in plain text format.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @GetMapping(value = "info/pushEvents/plain", produces = MediaType.TEXT_PLAIN_VALUE)
    public String getPushEventOverview() {

        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, getCurrentPersonNotNull());
        return clientPushService.getPushEventDocumentationPlain();
    }

    @ApiOperation(value = "Delete reference data",
            notes = """
                    Deletes a reference data entity by either making all dependent entities point to another entity or deleting all dependent entities. In both cases the entity is finally deleted.
                    *Needs to be called two times*:
                    - First the impact of the operation is checked
                    - A check code is obtained by this first call
                    - When entering the check code in the second call the actual operation is performed""")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2, requiredActions = SuperAdminAction.class)
    @DeleteMapping(value = "referenceData/delete")
    public ResponseEntity<String> deleteReferenceData(
            @RequestParam(required = false)
            @ApiParam("Required to execute the actual deletion. Obtained upon first call.")
            String checkCode,
            @RequestBody ClientReferenceDataDeletionRequest referenceDataDeletionRequest
    ) {

        Person caller = getCurrentPersonNotNull();
        getRoleService().decidePermissionAndThrowNotAuthorized(SuperAdminAction.class, caller);
        String idEntityToBeDeleted = referenceDataDeletionRequest.getIdEntityToBeDeleted();
        String idEntityToBeUsedInstead = referenceDataDeletionRequest.getIdEntityToBeUsedInstead();
        String classNameEntity = referenceDataDeletionRequest.getClassNameEntity();
        boolean deleteDependentEntities = referenceDataDeletionRequest.isDeleteDependentEntities();

        BaseEntity entityToBeDeleted = genericRepositoryService.findById(classNameEntity, idEntityToBeDeleted);

        if (entityToBeDeleted == null) {
            throw new NotFoundException("Could not find 'entity to be deleted' of type {} with id {}",
                    classNameEntity, idEntityToBeDeleted);
        }
        BaseEntity entityToUseInstead;
        String expectedCheckCode;
        if (deleteDependentEntities) {
            entityToUseInstead = null;
            //in this case the check should not include the entityToUseInstead
            expectedCheckCode =
                    Integer.toHexString((idEntityToBeDeleted + classNameEntity).hashCode());
        } else {
            if (StringUtils.isBlank(idEntityToBeUsedInstead)) {
                throw new BadRequestException(
                        "If deleteDependentEntities is false, idEntityToBeUsedInstead needs to be provided");
            }
            entityToUseInstead = genericRepositoryService.findById(classNameEntity, idEntityToBeUsedInstead);
            if (entityToUseInstead == null) {
                throw new NotFoundException("Could not find 'entity to use instead' of type {} with id {}",
                        classNameEntity, idEntityToBeUsedInstead);
            }
            expectedCheckCode =
                    Integer.toHexString((idEntityToBeDeleted + idEntityToBeUsedInstead + classNameEntity).hashCode());
        }
        ReferenceDataChangeResult referenceDataDeletionResult;
        String message;
        if (StringUtils.equals(checkCode, expectedCheckCode)) {
            message = "*Check code correct, performing actual deletion.*\n";
            referenceDataDeletionResult = referenceDataDeletionService.deleteReferenceData(
                    ReferenceDataDeletionRequest.builder()
                            .deleteDependentEntities(deleteDependentEntities)
                            .entityToBeDeleted(entityToBeDeleted)
                            .entityToBeUsedInstead(entityToUseInstead)
                            .build(),
                    caller);
        } else {
            message = "*No deletion performed, only checking impact! " +
                    "Use check code (see end of message) to perform the actual deletion.*\n";
            referenceDataDeletionResult = referenceDataDeletionService.checkReferenceDataDeletionImpact(
                    ReferenceDataCheckDeletionImpactRequest.builder()
                            .deleteDependentEntities(deleteDependentEntities)
                            .entityToBeDeleted(entityToBeDeleted)
                            .entityToBeUsedInstead(entityToUseInstead)
                            .build(),
                    caller);

        }
        message += StringUtils.repeat('-', 33) + "\n";
        message += referenceDataDeletionResult.getMessage();
        message += StringUtils.repeat('-', 33) + "\n";
        message += "Check code for deletion of this entity: " + expectedCheckCode;
        if (referenceDataDeletionResult.wasSuccessful()) {
            return new ResponseEntity<>(message, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

