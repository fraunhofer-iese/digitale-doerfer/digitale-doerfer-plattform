/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.legal.clientmodel;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import de.fhg.iese.dd.platform.business.shared.legal.LegalTextStatus;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalText;
import de.fhg.iese.dd.platform.datamanagement.shared.legal.model.LegalTextAcceptance;

@Component
public class LegalClientModelMapper {
    
    public List<ClientLegalText> toClientLegalTexts(List<LegalText> legalTexts) {
        if (CollectionUtils.isEmpty(legalTexts)) {
            return Collections.emptyList();
        }
        return legalTexts.stream()
                .sorted(Comparator.comparing(LegalText::getOrderValue))
                //we lookup the acceptance, if there is any
                .map(l -> toClientLegalText(l, null))
                .collect(Collectors.toList());
    }

    public List<ClientLegalText> toClientLegalTexts(Map<LegalText, LegalTextAcceptance> acceptances) {
        if (CollectionUtils.isEmpty(acceptances)) {
            return Collections.emptyList();
        }
        return acceptances.keySet().stream()
                .sorted(Comparator.comparing(LegalText::getOrderValue))
                //we lookup the acceptance, if there is any
                .map(l -> toClientLegalText(l, acceptances.get(l)))
                .collect(Collectors.toList());
    }

    public ClientLegalText toClientLegalText(LegalText legalText, LegalTextAcceptance acceptance) {
        if (legalText == null) {
            return null;
        }
        if (acceptance != null && !acceptance.getLegalText().equals(legalText)) {
            //we need to check that it is not possible to attach the wrong acceptance to the legal text
            throw new IllegalArgumentException("LegalTextAcceptance does not match with LegalText");
        }
        return ClientLegalText.builder()
                .id(legalText.getId())
                .legalTextIdentifier(legalText.getLegalTextIdentifier())
                .name(legalText.getName())
                .legalTextType(legalText.getLegalTextType())
                .orderValue(legalText.getOrderValue())
                .required(legalText.isRequired())
                .urlText(legalText.getUrlText())
                .acceptance(toClientLegalTextAcceptance(acceptance))
                .build();
    }

    public ClientLegalTextAcceptance toClientLegalTextAcceptance(LegalTextAcceptance acceptance) {
        if (acceptance == null) {
            return null;
        }
        return ClientLegalTextAcceptance.builder()
                .id(acceptance.getId())
                .timestamp(acceptance.getTimestamp())
                .build();
    }

    public ClientLegalTextStatus toClientLegalTextStatus(LegalTextStatus legalTextStatus) {
        if (legalTextStatus == null) {
            return null;
        }
        return ClientLegalTextStatus.builder()
                //we create a static id based on person and app variant
                .id(UUID.nameUUIDFromBytes(
                        (legalTextStatus.getPerson().getId() + legalTextStatus.getAppVariant().getId())
                                .getBytes(StandardCharsets.US_ASCII)).toString())
                .allRequiredAccepted(legalTextStatus.isAllRequiredAccepted())
                .acceptedLegalTexts(toClientLegalTexts(legalTextStatus.getAcceptedLegalTexts()))
                .openLegalTexts(toClientLegalTexts(legalTextStatus.getOpenLegalTexts()))
                .build();
    }

}
