/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2024 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.modifiers;

import de.fhg.iese.dd.platform.api.framework.clientmodel.ClientBaseEntity;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Set;

/**
 * Filter settings for a specific user. They are used to control how client entities are filtered for a person due to
 * blocked persons or flagged entities.
 * <p>
 * Especially relevant for {@link IOutgoingClientEntityModifier#modify(ClientBaseEntity, IContentFilterSettings)}.
 */
public interface IContentFilterSettings {

    /**
     * Filter settings that do not match anything.
     */
    IContentFilterSettings EMPTY_SETTINGS = new IContentFilterSettings() {

        @Override
        public String getCallingPersonId() {

            return null;
        }

        @Override
        public Set<String> getBlockedPersonIds() {
            return Collections.emptySet();
        }

        @Override
        public Set<String> getFlaggedEntityIds() {
            return Collections.emptySet();
        }
    };

    @Nullable
    String getCallingPersonId();

    /**
     * All blocked person ids for the current person and app.
     */
    Set<String> getBlockedPersonIds();

    /**
     * All flagged entity ids of the current person.
     */
    Set<String> getFlaggedEntityIds();

}
