/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Danielle Korth, Tahmid Ekram, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.security.services;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientActionDocumentation;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleActionDocumentation;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleInformation;
import de.fhg.iese.dd.platform.business.framework.caching.ExpiringResourceReference;
import de.fhg.iese.dd.platform.business.framework.reflection.services.IReflectionService;
import de.fhg.iese.dd.platform.business.framework.services.BaseService;
import de.fhg.iese.dd.platform.business.shared.security.services.Action;
import de.fhg.iese.dd.platform.business.shared.security.services.IAuthorizationDocumentationService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import lombok.Builder;
import lombok.Getter;

@Service
class ClientAuthorizationDocumentationService extends BaseService implements IClientAuthorizationDocumentationService {

    @Autowired
    private IRoleService roleService;
    @Autowired
    private IReflectionService reflectionService;
    @Autowired
    private IAuthorizationDocumentationService authorizationDocumentationService;
    @Autowired
    private TaskScheduler taskScheduler;

    private ExpiringResourceReference<Reflections> reflectionsReference;

    private MultiValueMap<Class<? extends Action<?>>, Method> endpointMethodsByRequiredAction = null;
    private MultiValueMap<Class<? extends Action<?>>, Method> endpointMethodsByOptionalAction = null;

    private MultiValueMap<Class<? extends BaseRole<?>>, Class<? extends Action<?>>> actionsUnrestrictedByRole = null;
    private MultiValueMap<Class<? extends BaseRole<?>>, Class<? extends Action<?>>> actionsEntityRestrictedByRole =
            null;

    @Getter
    @Builder
    private static class ActionDocumentation {

        private final String name;
        private final String description;
        private final String permissionDescription;
        private final List<Method> endpointMethodsActionRequired;
        private final List<Method> endpointMethodsActionOptional;
    }

    @Getter
    @Builder
    private static class RoleActionDocumentation {

        private final String key;
        private final String displayName;
        private final String description;
        private final String relatedEntity;
        private final List<ActionDocumentation> actionsUnrestricted;
        private final List<ActionDocumentation> actionsEntityRestricted;

    }

    @PostConstruct
    private void initialize() {

        reflectionsReference =
                new ExpiringResourceReference<>(this::createReflections, null, Duration.ofMinutes(1), taskScheduler);
    }

    private Reflections createReflections() {

        return new Reflections(new ConfigurationBuilder()
                .forPackage("de.fhg.iese.dd.platform")
                .filterInputsBy(new FilterBuilder()
                        .includePackage("de.fhg.iese.dd.platform.business")
                        .includePackage("de.fhg.iese.dd.platform.api"))
                .setScanners(Scanners.SubTypes, Scanners.MethodsAnnotated));
    }

    private List<Class<? extends Action<?>>> findAllActionClasses() {
        @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
        //rawtypes is required to prevent the compiler at jenkins to generate warnings
        List<Class<? extends Action<?>>> actions = reflectionsReference.get().getSubTypesOf(Action.class).stream()
                .map(actionClass -> (Class<? extends Action<?>>) actionClass)
                //abstract classes can not be instantiated (and do not need to be documented)
                .filter(actionClass -> !Modifier.isAbstract(actionClass.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .collect(Collectors.toList());
        return actions;
    }

    private List<Class<? extends BaseRole<?>>> findAllRoleClasses() {

        @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
        List<Class<? extends BaseRole<?>>> roleClasses =
                reflectionsReference.get().getSubTypesOf(BaseRole.class).stream()
                        .map(roleClass -> (Class<? extends BaseRole<?>>) roleClass)
                        .sorted(Comparator.comparing(Class::getName))
                        .collect(Collectors.toList());

        return roleClasses;
    }

    @Override
    public List<ClientActionDocumentation> getAllClientActionDocumentations() {

        collectMethodsByRequiredAndOptionalActions();

        return findAllActionClasses().stream()
                .map(this::toActionDocumentation)
                .sorted(Comparator.comparing(ActionDocumentation::getName))
                .map(this::toClientActionDocumentation)
                .collect(Collectors.toList());
    }

    @Override
    public List<ClientRoleActionDocumentation> getAllClientRoleActionDocumentation() {

        collectMethodsByRequiredAndOptionalActions();
        collectRolesByActions(findAllActionClasses());

        return findAllRoleClasses().stream()
                .map(this::toRoleActionDocumentation)
                .sorted(Comparator.comparing(RoleActionDocumentation::getDisplayName))
                .map(this::toClientRoleActionDocumentation)
                .collect(Collectors.toList());
    }

    private RoleActionDocumentation toRoleActionDocumentation(Class<? extends BaseRole<?>> roleClass) {

        BaseRole<?> role = roleService.getRole(roleClass);

        return RoleActionDocumentation.builder()
                .key(role.getKey())
                .displayName(role.getDisplayName())
                .description(role.getDescription())
                .relatedEntity(role.getRelatedEntityClass().getSimpleName())
                .actionsUnrestricted(actionsUnrestrictedByRole.getOrDefault(roleClass, Collections.emptyList())
                        .stream()
                        .map(this::toActionDocumentation)
                        .collect(Collectors.toList()))
                .actionsEntityRestricted(actionsEntityRestrictedByRole.getOrDefault(roleClass, Collections.emptyList())
                        .stream()
                        .map(this::toActionDocumentation)
                        .collect(Collectors.toList()))
                .build();
    }

    private ClientRoleActionDocumentation toClientRoleActionDocumentation(RoleActionDocumentation roleActionDocumentation) {

        return ClientRoleActionDocumentation.builder()
                .clientRoleInformation(ClientRoleInformation.builder()
                        .description(roleActionDocumentation.getDescription())
                        .displayName(roleActionDocumentation.getDisplayName())
                        .key(roleActionDocumentation.getKey())
                        .relatedEntity(roleActionDocumentation.getRelatedEntity())
                        .build())
                .actionsUnrestricted(roleActionDocumentation.getActionsUnrestricted().stream()
                        .map(this::toClientActionDocumentation)
                        .collect(Collectors.toList()))
                .actionsEntityRestricted(roleActionDocumentation.getActionsEntityRestricted().stream()
                        .map(this::toClientActionDocumentation)
                        .collect(Collectors.toList()))
                .build();
    }

    private ActionDocumentation toActionDocumentation(Class<? extends Action<?>> actionClass) {

        Action<?> action = roleService.getAction(actionClass);

        return ActionDocumentation.builder()
                .name(reflectionService.getModuleName(actionClass) + "::" + action.getClass().getSimpleName())
                .description(action.getActionDescription())
                .permissionDescription(action.getPermissionDescription(roleService::getRole))
                .endpointMethodsActionRequired(endpointMethodsByRequiredAction.getOrDefault(actionClass,
                        Collections.emptyList()))
                .endpointMethodsActionOptional(endpointMethodsByOptionalAction.getOrDefault(actionClass,
                        Collections.emptyList()))
                .build();
    }

    private ClientActionDocumentation toClientActionDocumentation(ActionDocumentation actionDocumentation) {

        return ClientActionDocumentation.builder()
                .name(actionDocumentation.getName())
                .description(actionDocumentation.description)
                .permissionDescription(actionDocumentation.permissionDescription)
                .endpointsActionRequired(actionDocumentation.getEndpointMethodsActionRequired()
                        .stream()
                        .flatMap(this::endpointMethodToEndpointPatterns)
                        .collect(Collectors.toList()))
                .endpointsActionOptional(actionDocumentation.getEndpointMethodsActionOptional()
                        .stream()
                        .flatMap(this::endpointMethodToEndpointPatterns)
                        .collect(Collectors.toList()))
                .build();
    }

    private void collectMethodsByRequiredAndOptionalActions() {

        if (endpointMethodsByRequiredAction != null && endpointMethodsByOptionalAction != null) {
            return;
        }
        endpointMethodsByRequiredAction = new LinkedMultiValueMap<>();
        endpointMethodsByOptionalAction = new LinkedMultiValueMap<>();

        Set<Method> endpointMethods = reflectionsReference.get().getMethodsAnnotatedWith(ApiAuthentication.class);

        for (Method endpointMethod : endpointMethods) {
            ApiAuthentication apiAuthentication = endpointMethod.getAnnotation(ApiAuthentication.class);
            Class<? extends Action<?>>[] requiredActions = apiAuthentication.requiredActions();
            Class<? extends Action<?>>[] optionalActions = apiAuthentication.optionalActions();

            for (Class<? extends Action<?>> actionClass : requiredActions) {
                endpointMethodsByRequiredAction.add(actionClass, endpointMethod);
            }

            for (Class<? extends Action<?>> actionClass : optionalActions) {
                endpointMethodsByOptionalAction.add(actionClass, endpointMethod);
            }
        }
    }

    private void collectRolesByActions(List<Class<? extends Action<?>>> actions) {

        if (actionsUnrestrictedByRole != null && actionsEntityRestrictedByRole != null) return;

        actionsUnrestrictedByRole = new LinkedMultiValueMap<>();
        actionsEntityRestrictedByRole = new LinkedMultiValueMap<>();

        for (Class<? extends Action<?>> action : actions) {

            Collection<Class<? extends BaseRole<?>>> unrestrictedRoles =
                    authorizationDocumentationService.getRolesUnrestricted(action);
            Collection<Class<? extends BaseRole<?>>> restrictedRoles =
                    authorizationDocumentationService.getRolesEntityRestricted(action);

            for (Class<? extends BaseRole<?>> unrestrictedRole : unrestrictedRoles) {
                actionsUnrestrictedByRole.add(unrestrictedRole, action);
            }

            for (Class<? extends BaseRole<?>> restrictedRole : restrictedRoles) {
                actionsEntityRestrictedByRole.add(restrictedRole, action);
            }
        }
    }

    /**
     * Get the full endpoint patterns of a given controller method
     *
     * @param method A Method from Reflections
     *
     * @return "{method-requestMethod} /{class-level-pattern}/{method-level-pattern}"
     */
    private Stream<String> endpointMethodToEndpointPatterns(Method method) {

        String[] classLevelPatterns;
        RequestMapping classLevelAnnotation =
                AnnotatedElementUtils.findMergedAnnotation(method.getDeclaringClass(), RequestMapping.class);
        if (classLevelAnnotation != null) {
            classLevelPatterns = classLevelAnnotation.value();
        } else {
            classLevelPatterns = new String[]{"/"};
        }

        RequestMapping methodLevelAnnotation = AnnotatedElementUtils.findMergedAnnotation(method, RequestMapping.class);

        String[] methodLevelPatterns;
        RequestMethod[] methodLevelMethods;
        if (methodLevelAnnotation != null) {
            methodLevelPatterns = methodLevelAnnotation.value();
            methodLevelMethods = methodLevelAnnotation.method();
        } else {
            methodLevelPatterns = new String[]{"/"};
            methodLevelMethods = new RequestMethod[]{RequestMethod.GET};
        }

        List<String> result =
                new ArrayList<>(classLevelPatterns.length * methodLevelPatterns.length * methodLevelMethods.length);

        for (String classLevelPattern : classLevelPatterns) {
            for (String methodLevelPattern : methodLevelPatterns) {
                for (RequestMethod methodLevelMethod : methodLevelMethods) {
                    String normalizedClassLevelPattern = StringUtils.removeEnd(
                            StringUtils.prependIfMissing(classLevelPattern, "/"),
                            "/");
                    String normalizedMethodLevelPattern = StringUtils.prependIfMissing(methodLevelPattern, "/");
                    String endpointPattern = methodLevelMethod.toString() + " " + normalizedClassLevelPattern +
                            normalizedMethodLevelPattern;
                    result.add(endpointPattern);
                }
            }
        }
        return result.stream();
    }

}
