/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.address.clientmodel;

import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.shared.misc.clientmodel.ClientGPSLocation;
import de.fhg.iese.dd.platform.business.shared.address.services.IAddressService;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.Address;
import de.fhg.iese.dd.platform.datamanagement.shared.address.model.AddressListEntry;
import de.fhg.iese.dd.platform.datamanagement.shared.misc.model.GPSLocation;

@Component
public class AddressClientModelMapper {

    public ClientAddress toClientAddress(Address address) {
        if (address == null) {
            return null;
        }
        return ClientAddress.builder()
                .id(address.getId())
                .name(address.getName())
                .street(address.getStreet())
                .zip(address.getZip())
                .city(address.getCity())
                .verified(address.isVerified())
                .gpsLocation(toClientGPSLocation(address.getGpsLocation()))
                .build();
    }

    public Address toAddress(ClientAddress clientAddress) {
        if (clientAddress == null) {
            return null;
        }
        return Address.builder()
                .id(clientAddress.getId())
                .name(clientAddress.getName())
                .street(clientAddress.getStreet())
                .zip(clientAddress.getZip())
                .city(clientAddress.getCity())
                .verified(clientAddress.isVerified())
                .gpsLocation(toGPSLocation(clientAddress.getGpsLocation()))
                .build();
    }

    public IAddressService.AddressDefinition toAddressDefinition(ClientAddressDefinition clientAddressDefinition) {
        if (clientAddressDefinition == null) {
            return null;
        }
        return IAddressService.AddressDefinition.builder()
                .name(clientAddressDefinition.getName())
                .street(clientAddressDefinition.getStreet())
                .zip(clientAddressDefinition.getZip())
                .city(clientAddressDefinition.getCity())
                .gpsLocation(toGPSLocation(clientAddressDefinition.getGpsLocation()))
                .build();
    }

    public IAddressService.AddressDefinition toAddressDefinition(ClientAddress clientAddress) {
        if (clientAddress == null) {
            return null;
        }
        return IAddressService.AddressDefinition.builder()
                .name(clientAddress.getName())
                .street(clientAddress.getStreet())
                .zip(clientAddress.getZip())
                .city(clientAddress.getCity())
                .gpsLocation(toGPSLocation(clientAddress.getGpsLocation()))
                .build();
    }

    public IAddressService.LocationDefinition toLocationDefinition(ClientLocationDefinition clientLocationDefinition) {
        if (clientLocationDefinition == null) {
            return null;
        }
        return IAddressService.LocationDefinition.builder()
                .locationName(clientLocationDefinition.getLocationName())
                .locationLookupString(clientLocationDefinition.getLocationLookupString())
                .gpsLocation(toGPSLocation(clientLocationDefinition.getGpsLocation()))
                .addressStreet(clientLocationDefinition.getAddressStreet())
                .addressZip(clientLocationDefinition.getAddressZip())
                .addressCity(clientLocationDefinition.getAddressCity())
                .build();
    }
    
    public ClientAddressListEntry createClientAddressListEntry(AddressListEntry addressListEntry) {
        if (addressListEntry == null) {
            return null;
        }
        return ClientAddressListEntry.builder()
                .id(addressListEntry.getId())
                .name(addressListEntry.getName())
                .address(toClientAddress(addressListEntry.getAddress()))
                .build();
    }

    public AddressListEntry createAddressListEntry(ClientAddressListEntry clientAddressListEntry) {
        if (clientAddressListEntry == null) {
            return null;
        }
        return AddressListEntry.builder()
                .id(clientAddressListEntry.getId())
                .name(clientAddressListEntry.getName())
                .address(toAddress(clientAddressListEntry.getAddress()))
                .build();
    }

    public ClientGPSLocation toClientGPSLocation(GPSLocation clientGpsLocation) {
        if (clientGpsLocation == null) {
            return null;
        }
        return new ClientGPSLocation(clientGpsLocation.getLatitude(), clientGpsLocation.getLongitude());
    }

    public GPSLocation toGPSLocation(ClientGPSLocation gpsLocation) {
        if (gpsLocation == null) {
            return null;
        }
        return new GPSLocation(gpsLocation.getLatitude(), gpsLocation.getLongitude());
    }

}
