/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.feature.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAppVariantIdentification;
import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.ClientFeature;
import de.fhg.iese.dd.platform.api.shared.feature.clientmodel.FeatureClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.geoarea.services.IGeoAreaService;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.shared.feature.model.BaseFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/feature")
@Api(tags = {"shared.feature"})
public class FeatureController extends BaseController {

    @Autowired
    private IFeatureService featureService;
    @Autowired
    private IGeoAreaService geoAreaService;
    @Autowired
    private FeatureClientModelMapper featureClientModelMapper;

    @ApiOperation(value = "Feature configuration for appvariant and user",
            notes = "All features relevant for this app variant in the configuration of this user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @GetMapping(value = "/")
    public List<ClientFeature> getFeatures(
            @RequestParam(required = false, defaultValue = "false")
            @ApiParam("Only return features that are enabled")
                    boolean onlyEnabled
    ) {

        final List<BaseFeature> featuresVisibleForClient = featureService.getAllFeaturesForTarget(
                        FeatureTarget.of(
                                getCurrentPersonNotNull(),
                                getAppVariantNotNull()),
                        onlyEnabled)
                .stream()
                .filter(BaseFeature::isVisibleForClient)
                .collect(Collectors.toList());
        return featureClientModelMapper.toClientFeatures(featuresVisibleForClient);
    }

    @ApiOperation(value = "Default feature configuration for appvariant",
            notes = "All features relevant for this app variant in the default configuration.")
    @ApiAuthentication(value = ApiAuthenticationType.PUBLIC,
            appVariantIdentification = ApiAppVariantIdentification.IDENTIFICATION_ONLY)
    @ApiExceptions(ClientExceptionType.GEO_AREA_NOT_FOUND)
    @GetMapping(value = "/default")
    public List<ClientFeature> getDefaultFeatures(
            @RequestParam(required = false, defaultValue = "false")
            @ApiParam("Only return features that are enabled")
                    boolean onlyEnabled,
            @RequestParam(required = false)
            @ApiParam("The home area the potential new user already selected")
                    String homeAreaId
    ) {

        FeatureTarget featureTarget;
        if (StringUtils.isNotEmpty(homeAreaId)) {
            final GeoArea geoArea = geoAreaService.findGeoAreaById(homeAreaId);
            featureTarget = FeatureTarget.of(geoArea, getAppVariantNotNull());
        } else {
            featureTarget = FeatureTarget.of(getAppVariantNotNull());
        }
        final List<BaseFeature> featuresVisibleForClient =
                featureService.getAllFeaturesForTarget(featureTarget, onlyEnabled)
                        .stream()
                        .filter(BaseFeature::isVisibleForClient)
                        .collect(Collectors.toList());
        return featureClientModelMapper.toClientFeatures(featuresVisibleForClient);
    }

}
