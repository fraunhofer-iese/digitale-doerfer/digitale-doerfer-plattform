/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 - 2020 Johannes Schneider, Dominik Schnier, Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.controllers.BaseController;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.ClientUserGeneratedContentFlagForFlagCreator;
import de.fhg.iese.dd.platform.api.shared.usergeneratedcontentflag.clientmodel.UserGeneratedContentFlagClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/flag")
@Api(tags = {"shared.flag"})
public class UserGeneratedContentFlagController extends BaseController {

    @Autowired
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;
    @Autowired
    private UserGeneratedContentFlagClientModelMapper userGeneratedContentFlagClientModelMapper;

    @ApiOperation(value = "gets all own user generated content flags",
            notes = "Lists all user generated content flags created by the current user. " +
                    "They do not contain the status records and comments.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @GetMapping("/")
    public List<ClientUserGeneratedContentFlagForFlagCreator> getFlagsByFlagCreator() {

        final Person currentPerson = getCurrentPersonNotNull();

        return userGeneratedContentFlagService.findByFlagCreator(currentPerson).stream()
                .map(userGeneratedContentFlagClientModelMapper::toClientUserGeneratedContentFlagForFlagCreator)
                .collect(Collectors.toList());
    }

}
