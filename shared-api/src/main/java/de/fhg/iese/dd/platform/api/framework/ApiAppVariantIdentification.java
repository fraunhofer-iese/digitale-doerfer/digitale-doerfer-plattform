/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2018 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework;

public enum ApiAppVariantIdentification {

    NONE,
    /**
     * Possible identifications are:
     * <li>Oauth Client Identifier in access token</li>
     * <li>Api Key in header</li>
     * <li>App Variant Identifier in header</li>
     */
    IDENTIFICATION_ONLY,
    /**
     * Possible identifications are:
     * <li>Api Key in header</li>
     */
    AUTHENTICATION

}
