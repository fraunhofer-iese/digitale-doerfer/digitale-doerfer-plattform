/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2023 Johannes Eveslage, Ben Burkhard
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.files.clientmodel;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientRelativeImageCropDefinition {

    @ApiModelProperty("relative x offset of the top left corner of the cropped image, must be between 0 and 1")
    @PositiveOrZero(message = "relative x offset must not be negative!")
    private Double relativeOffsetX;
    @ApiModelProperty("relative y offset of the top left corner of the cropped image, must be between 0 and 1")
    @PositiveOrZero(message = "relative y offset must not be negative!")
    private Double relativeOffsetY;
    @ApiModelProperty("width of the cropped image, must in (0,1]")
    @Positive(message = "relative width must not be greater than 0!")
    private Double relativeWidth;
    @ApiModelProperty("width of the cropped image, must be in (0,1]")
    @Positive(message = "relative height must not be greater than 0!")
    private Double relativeHeight;

}
