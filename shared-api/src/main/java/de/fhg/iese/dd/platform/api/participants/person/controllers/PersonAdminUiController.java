/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Johannes Schneider, Balthasar Weitzel, Dominik Schnier, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.ClientPersonExtended;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.AppClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.app.clientmodel.ClientAppVariantUsageExtended;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientOauthAccount;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.ClientRoleAssignment;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.RoleClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.security.clientmodel.SecurityClientModelMapper;
import de.fhg.iese.dd.platform.business.participants.person.security.GetOauthAccountByEmailAction;
import de.fhg.iese.dd.platform.business.participants.person.security.ListPersonsAction;
import de.fhg.iese.dd.platform.business.participants.person.security.ListPersonsDetailsAction;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.model.NamedEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthAccount;
import de.fhg.iese.dd.platform.datamanagement.shared.security.roles.BaseRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/adminui")
@Api(tags = {"participants.adminui.person"})
public class PersonAdminUiController extends BaseAdminUiController {

    private static final int MIN_SEARCH_LENGTH = 3;

    enum PersonSortColumn {
        LAST_NAME("lastName"),
        FIRST_NAME("firstName"),
        EMAIL("email");

        private final String column;

        PersonSortColumn(String column) {
            this.column = column;
        }

        public String getColumn() {
            return column;
        }
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    static class ParsedSearchParameter {

        /**
         * if true, a search should be performed by firstNameInfix and lastNameInfix. Otherwise, a general search
         * for generalInfix should be performed.
         */
        private final boolean nameSearch;
        private final String generalInfix;
        private final String firstNameInfix;
        private final String lastNameInfix;

        /**
         * If the search is null of empty or only spaces, null is returned. Otherwise, if the search term contains a
         * space, it is split into the parts before and after the first space
         */
        public static ParsedSearchParameter checkAndParse(String search) {
            if (StringUtils.isAllBlank(search)) {
                return null;
            }
            final String trimmed = search.trim();
            if (trimmed.length() < MIN_SEARCH_LENGTH) {
                throw new SearchParameterTooShortException("search", MIN_SEARCH_LENGTH);
            }
            final String[] searchStrings = StringUtils.split(trimmed, " ", 2);
            if (searchStrings.length == 1) {
                return new ParsedSearchParameter(false, searchStrings[0], null, null);
            }
            return new ParsedSearchParameter(true, null, searchStrings[0],
                    StringUtils.normalizeSpace(searchStrings[1]));
        }

    }

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private RoleClientModelMapper roleClientModelMapper;
    @Autowired
    private AppClientModelMapper appClientModelMapper;
    @Autowired
    private SecurityClientModelMapper securityClientModelMapper;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Returns person details",
            notes = "Also queries information from Auth0. Do not rely on any sub-field in oauthAccount to be present!")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListPersonsAction.class},
            optionalActions = {ListPersonsDetailsAction.class},
            notes = """
                    If the person is super admin or global user admin, it is allowed query information for all users.
                    User admins and role managers are only allowed to query information on users that belong to a tenant that they are user admin for (i.e. persons with homeCommunity=tenant or homeArea.tenant=tenant).
                    Additionally, role managers are only presented a subset of the ClientPerson attributes, i.e. id, firstName, lastName, eMail, profilePicture, status, roleAssignments.
                    Role manager and user admins get only those roles listed that belong to a tenant that they are admin for (see above).""")
    @GetMapping("/person/{personId}")
    public ClientPersonExtended getPersonExtended(@PathVariable String personId) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted listPersonPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListPersonsAction.class, currentPerson);

        PermissionTenantRestricted listPersonDetailsPermission = getRoleService()
                .decidePermission(ListPersonsDetailsAction.class, currentPerson);

        final Person person = personService.findPersonById(personId);

        if (listPersonPermission.isTenantDenied(person.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to get person {}", person.getId());
        }

        // build role assignments
        final List<ClientRoleAssignment> roleAssignments = getRoleService()
                .getExtendedRoleAssignmentsOfPersonForListingPermission(listPersonPermission, person)
                .stream()
                .map(roleClientModelMapper::toClientRoleAssignment)
                .sorted(Comparator.comparing(ClientRoleAssignment::getRoleKey)
                        .thenComparing(ClientRoleAssignment::getRelatedEntityId))
                .collect(Collectors.toList());

        // build app variant usages
        final List<AppVariantUsage> appVariantUsages = getAppService().getAppVariantUsages(person);
        final List<ClientAppVariantUsageExtended> clientAppVariantUsages = new ArrayList<>(appVariantUsages.size());
        for (AppVariantUsage appVariantUsage : appVariantUsages) {
            Set<GeoArea> selectedGeoAreas =
                    getAppService().getSelectedGeoAreas(appVariantUsage.getAppVariant(), person);
            clientAppVariantUsages.add(appClientModelMapper.toClientAppVariantUsageExtended(appVariantUsage,
                    selectedGeoAreas));
        }
        //sort the usages so that they are not randomly ordered
        clientAppVariantUsages.sort(Comparator.comparing(ClientAppVariantUsageExtended::getLastUsage)
                .thenComparing(c -> c.getAppVariant().getAppVariantIdentifier()));

        if (listPersonDetailsPermission.isTenantDenied(person.getTenant())) {
            return personClientModelMapper.createClientPersonExtendedWithLimitedAttributes(person)
                    .withRoleAssignments(roleAssignments);
        }

        return retrieveOauthAccountAndConvertToClientPerson(person)
                .withRoleAssignments(roleAssignments)
                .withAppVariantUsages(clientAppVariantUsages);
    }

    @ApiOperation(value = "Returns details about the oauth user account",
            notes = "Queries information from Auth0. Do not rely on any sub-field in oauthAccount to be present!")
    @ApiExceptions({
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {GetOauthAccountByEmailAction.class},
            notes = "May only be called by global user admins")
    @GetMapping("/oauthUserByEmail/{email}")
    public ClientOauthAccount getOauthUser(@PathVariable String email) {

        getRoleService().decidePermissionAndThrowNotAuthorized(GetOauthAccountByEmailAction.class,
                getCurrentPersonNotNull());

        final OauthAccount oauthAccount = oauthManagementService.queryUserByEmail(email);
        return securityClientModelMapper.toClientOauthAccount(oauthAccount);
    }

    @ApiOperation(value = "Returns the list of persons that the caller has access to",
            notes = "In this listing endpoint, oauthAccount and roleAssignments of the returned persons " +
                    "is always null, and deleted persons are only listed when the search parameter is used. ")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListPersonsAction.class},
            optionalActions = {ListPersonsDetailsAction.class},
            notes = """
                    If the caller is super admin or global user admin, all persons with all data are returned.
                    If the caller is role manager or user admin for some tenants, only persons belonging to the tenant are returned (i.e. persons with person.tenant=tenant or person.homeArea.tenant=tenant).
                    Additionally, role managers are only presented a subset of the ClientPerson attributes, i.e. id, firstName, lastName, eMail, profilePicture, status.""")
    @GetMapping("/person")
    public Page<ClientPersonExtended> getPersons(
            @ApiParam("number of page to retrieve. First page has number 0")
            @RequestParam(required = false, defaultValue = "0") int page,

            @ApiParam("number of elements per page")
            @RequestParam(required = false, defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "LAST_NAME")
                    PersonSortColumn sortColumn,

            @RequestParam(required = false, defaultValue = "ASC")
                    Sort.Direction sortDirection,

            @ApiParam("if left empty, all persons are shown. If search={string1}{space}{string2} with string1 not " +
                    "containing space, it is searched for persons where string1 is contained in firstName and " +
                    "string2 ist contained in p.lastName. Otherwise, all persons are returned that have the " +
                    "search string in either id, oauthId, firstName, lastName or email.")
            @RequestParam(required = false)
                    String search
    ) {

        checkPageAndCountValues(page, count);
        final ParsedSearchParameter searchParameter = ParsedSearchParameter.checkAndParse(search);

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted listPersonPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListPersonsAction.class, currentPerson);

        PermissionTenantRestricted listPersonDetailsPermission = getRoleService()
                .decidePermission(ListPersonsDetailsAction.class, currentPerson);

        final PageRequest pageRequest = PageRequest.of(page, count, Sort.by(sortDirection, sortColumn.getColumn()));

        final Function<Person, ClientPersonExtended> personMapper =
                personMapperForPermission(listPersonDetailsPermission);

        if (listPersonPermission.isAllTenantsAllowed()) {
            if (searchParameter == null) {
                return personService.findAllNotDeleted(pageRequest)
                        .map(personMapper);
            }
            if (searchParameter.isNameSearch()) {
                return personService.findAllByCaseInsensitiveInfixSearchInFirstNameAndLastName(
                        searchParameter.getFirstNameInfix(), searchParameter.getLastNameInfix(), pageRequest)
                        .map(personMapper);
            }
            //general infix search
            return personService.findAllByCaseInsensitiveInfixSearchInMainFields(
                    searchParameter.getGeneralInfix(), pageRequest)
                    .map(personMapper);
        } else {
            //searching must take into accounts which tenant IDs are allowed
            if (searchParameter == null) {
                return personService.findAllNotDeletedBelongingToTenantIn(listPersonPermission.getAllowedTenantIds(),
                        pageRequest)
                        .map(personMapper);
            }
            if (searchParameter.isNameSearch()) {
                return personService.findAllByCaseInsensitiveInfixSearchInFirstNameAndLastNameAndBelongingToTenantIn(
                        searchParameter.getFirstNameInfix(), searchParameter.getLastNameInfix(),
                        listPersonPermission.getAllowedTenantIds(),
                        pageRequest)
                        .map(personMapper);
            }
            //general infix search
            return personService.findAllByCaseInsensitiveInfixSearchInMainFieldsAndBelongingToTenantIn(
                    searchParameter.getGeneralInfix(), listPersonPermission.getAllowedTenantIds(), pageRequest)
                    .map(personMapper);
        }
    }

    @ApiOperation(value = "Returns the list of persons that the caller has access to, filtered by role",
            notes = "See role description for details.")
    @ApiExceptions({
            ClientExceptionType.ROLE_NOT_FOUND,
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListPersonsAction.class},
            optionalActions = {ListPersonsDetailsAction.class},
            notes = """
                    If the caller is super admin or global user admin, all persons matching the filter with all data are returned.
                    If the caller is role manager or user admin for some tenants, only persons belonging to the tenant are returned (i.e. persons with person.tenant=tenant or person.homeArea.tenant=tenant),
                    additionally filtered by the given parameters.
                    Additionally, role managers are only presented a subset of the ClientPerson attributes, i.e. id, firstName, lastName, eMail, profilePicture, status.""")
    @GetMapping("/personByRole")
    public Page<ClientPersonExtended> getPersonsFilteredByRole(
            @ApiParam("number of page to retrieve. First page has number 0")
            @RequestParam(required = false, defaultValue = "0") int page,

            @ApiParam("number of elements per page")
            @RequestParam(required = false, defaultValue = "10") int count,

            @RequestParam(required = false, defaultValue = "LAST_NAME")
                    PersonSortColumn sortColumn,

            @RequestParam(required = false, defaultValue = "ASC")
                    Sort.Direction sortDirection,

            @ApiParam("only list persons to which this role is assigned to")
            @RequestParam(required = true)
                    String roleKey,

            @ApiParam("only list persons with person.tenant=tenant or person.homeArea.tenant=tenant")
            @RequestParam(required = false)
                    String tenantId
    ) {

        checkPageAndCountValues(page, count);

        final Person currentPerson = getCurrentPersonNotNull();
        final BaseRole<? extends NamedEntity> role = getRoleService().getRoleForKey(roleKey);
        if (StringUtils.isNotEmpty(tenantId)) {
            //looking up the tenant to ensure that the tenant id is valid
            tenantService.checkTenantByIdExists(tenantId);
        }

        PermissionTenantRestricted listPersonPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ListPersonsAction.class, currentPerson);

        PermissionTenantRestricted listPersonDetailsPermission = getRoleService()
                .decidePermission(ListPersonsDetailsAction.class, currentPerson);

        final PageRequest pageRequest = PageRequest.of(page, count, Sort.by(sortDirection, sortColumn.getColumn()));

        if (StringUtils.isNotEmpty(tenantId) && listPersonPermission.isTenantDenied(tenantId)) {
            //the permission to view this tenant is not given
            throw new NotAuthorizedException("Insufficient privileges to get persons of tenant {}", tenantId);
        }

        final Function<Person, ClientPersonExtended> personMapper =
                personMapperForPermission(listPersonDetailsPermission);

        if (StringUtils.isNotEmpty(tenantId)) {
            if (listPersonPermission.isTenantAllowed(tenantId)) {
                return getRoleService()
                        .findAllByAssignedRoleAndBelongingToTenantIn(role.getRoleClass(),
                                Collections.singleton(tenantId), pageRequest)
                        .map(personMapper);
            } else {
                //the permission to view this tenant is not given
                throw new NotAuthorizedException("Insufficient privileges to get persons of tenant {}", tenantId);
            }
        } else {
            if (listPersonPermission.isAllTenantsAllowed()) {
                return getRoleService().findAllByAssignedRole(role.getRoleClass(), pageRequest)
                        .map(personMapper);
            } else {
                return getRoleService()
                        .findAllByAssignedRoleAndBelongingToTenantIn(role.getRoleClass(),
                                listPersonPermission.getAllowedTenantIds(), pageRequest)
                        .map(personMapper);
            }
        }
    }

    /**
     * Decides on the person mapping function based on the permission of the requester
     */
    private Function<Person, ClientPersonExtended> personMapperForPermission(
            PermissionTenantRestricted listPersonDetailsPermission) {
        if (listPersonDetailsPermission.isDenied()) {
            //no permission for details at all
            return personClientModelMapper::createClientPersonExtendedWithLimitedAttributes;
        } else {
            return p -> {
                if (listPersonDetailsPermission.isAllTenantsAllowed()) {
                    //details for all tenants allowed
                    return personClientModelMapper.createClientPersonExtended(
                            personService.fetchPersonExtendedAttributes(p));
                } else {
                    //only details for those tenants that are allowed
                    if (p.getTenant() != null &&
                            listPersonDetailsPermission.getAllowedTenantIds().contains(p.getTenant().getId())) {
                        return personClientModelMapper.createClientPersonExtended(
                                personService.fetchPersonExtendedAttributes(p));
                    } else {
                        return personClientModelMapper.createClientPersonExtendedWithLimitedAttributes(p);
                    }
                }
            };
        }
    }

}
