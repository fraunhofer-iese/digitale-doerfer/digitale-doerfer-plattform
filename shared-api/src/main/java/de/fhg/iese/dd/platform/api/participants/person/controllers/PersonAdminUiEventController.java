/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2019 - 2023 Dominik Schnier, Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.person.controllers;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiException;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.participants.person.clientevent.*;
import de.fhg.iese.dd.platform.api.participants.person.clientmodel.PersonClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.participants.person.events.*;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.EMailAlreadyUsedException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonHasNoPendingNewEmailException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonInformationInvalidException;
import de.fhg.iese.dd.platform.business.participants.person.security.*;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.shared.email.services.IEmailAddressVerificationService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/adminui/person/event")
@Api(tags = {"participants.adminui.person.events"})
public class PersonAdminUiEventController extends BaseAdminUiController {

    @Autowired
    private IEmailAddressVerificationService emailAddressVerificationService;
    @Autowired
    private PersonClientModelMapper personClientModelMapper;

    @ApiOperation(value = "Change email address of a user")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.EMAIL_ALREADY_REGISTERED,
            ClientExceptionType.EMAIL_CHANGE_NOT_POSSIBLE,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND,
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ChangePersonEmailAddressByAdminAction.class})
    @PostMapping("/changeEmailAddressByAdminRequest")
    public ClientPersonChangeEmailAddressByAdminResponse onChangeEmailAddressRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonChangeEmailAddressByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted changeEmailAddressPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ChangePersonEmailAddressByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (changeEmailAddressPermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to change email address of person {}",
                    personToBeChanged.getId());
        }

        final String newEmailAddress = checkAndTrim(request.getNewEmailAddress(), "newEmailAddress");
        String normalizedEmail = emailAddressVerificationService.ensureCorrectEmailSyntaxAndNormalize(newEmailAddress);
        //check early if the email address is already used, this is less expensive than checking it in the event processor
        if (StringUtils.equalsIgnoreCase(personToBeChanged.getEmail(), newEmailAddress) ||
                personService.existsPersonByEmail(normalizedEmail)) {
            throw new EMailAlreadyUsedException(newEmailAddress);
        }

        PersonChangeEmailAddressByAdminRequest personChangeEmailAddressByAdminRequest =
                PersonChangeEmailAddressByAdminRequest.builder()
                        .person(personToBeChanged)
                        .newEmailAddress(normalizedEmail)
                        .newEmailAddressVerified(request.isNewEmailAddressVerified())
                        .build();

        PersonChangeEmailAddressConfirmation personChangeEmailAddressByAdminConfirmation =
                notifyAndWaitForReply(PersonChangeEmailAddressConfirmation.class,
                        personChangeEmailAddressByAdminRequest);

        return ClientPersonChangeEmailAddressByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personChangeEmailAddressByAdminConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Cancel the change to pending new email address",
            notes = "Cancel the change to the pending new email address. The current mail address is continued to be used and the pending new email address is deleted.")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiException(value = ClientExceptionType.PERSON_HAS_NO_PENDING_NEW_EMAIL,
            reason = "Thrown if the person is currently not changing the email address and thus has no pending new email.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2)
    @PostMapping("cancelChangeEmailAddressByAdminRequest")
    public ClientPersonCancelChangeEmailAddressByAdminConfirmation onCancelChangeEmailAddressRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonCancelChangeEmailAddressByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted cancelChangePersonEmailAddressPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(CancelChangePersonEmailAddressByAdminAction.class,
                        currentPerson);

        final Person person = getPersonService().findPersonById(request.getPersonId());

        if (cancelChangePersonEmailAddressPermission.isTenantDenied(person.getTenant())) {
            throw new NotAuthorizedException(
                    "Insufficient privileges to cancel the change of the pending new email of person '{}'",
                    person.getId());
        }

        if (StringUtils.isEmpty(person.getPendingNewEmail())) {
            throw new PersonHasNoPendingNewEmailException(
                    "Person '{}' is currently not changing the email address and has no pending new email address",
                    person.getId());
        }

        final PersonCancelChangeEmailAddressConfirmation personCancelChangeEmailAddressConfirmation =
                notifyAndWaitForReply(PersonCancelChangeEmailAddressConfirmation.class,
                        PersonCancelChangeEmailAddressRequest.builder()
                                .person(person)
                                .build());
        return ClientPersonCancelChangeEmailAddressByAdminConfirmation.builder()
                .person(personClientModelMapper.createClientPersonReferenceWithEmail(
                        personCancelChangeEmailAddressConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Change firstName and lastName of a user")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ChangePersonNameByAdminAction.class})
    @PostMapping("/changeNameByAdminRequest")
    public ClientPersonChangeNameByAdminResponse onChangeNameRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonChangeNameByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted changeNamePermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ChangePersonNameByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (changeNamePermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to change name of person {}",
                    personToBeChanged.getId());
        }

        PersonChangeNameByAdminRequest personChangeNameByAdminRequest =
                PersonChangeNameByAdminRequest.builder()
                        .person(personToBeChanged)
                        .firstName(checkAndTrim(request.getFirstName(), "firstName"))
                        .lastName(checkAndTrim(request.getLastName(), "lastName"))
                        .build();

        PersonChangeNameByAdminConfirmation personChangeNameByAdminConfirmation =
                notifyAndWaitForReply(PersonChangeNameByAdminConfirmation.class, personChangeNameByAdminRequest);

        return ClientPersonChangeNameByAdminResponse.builder()
                .updatedPerson(
                        retrieveOauthAccountAndConvertToClientPerson(personChangeNameByAdminConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Blocks a user so that it can not log in anymore",
            notes = """
                    Prevents a user from login by blocking the account at Auth0 and adds the person status 'LOGIN_BLOCKED'.
                    The users account at Auth0 is blocked and all the refresh tokens are revoked. The access tokens are still valid and can be used to authorize the person until they expire. Typical expiration times are some hours (configured at Auth0). Nevertheless calling endpoints at the backend is not possible anymore, since the status of the person is checked and NotAuthorizedException is thrown.
                    If the blocked person is deleted afterwards, the status 'LOGIN_BLOCKED' is not removed.""")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {BlockLoginAction.class})
    @PostMapping("/blockLoginByAdminRequest")
    public ClientPersonBlockLoginByAdminResponse onBlockLoginRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonBlockLoginByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted blockLoginPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(BlockLoginAction.class, currentPerson);

        final Person personToBeBlocked = getPersonService().findPersonById(request.getPersonId());

        if (blockLoginPermission.isTenantDenied(personToBeBlocked.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to block person {}",
                    personToBeBlocked.getId());
        }

        PersonBlockLoginByAdminRequest personBlockLoginByAdminRequest =
                PersonBlockLoginByAdminRequest.builder()
                        .person(personToBeBlocked)
                        .build();

        PersonBlockLoginByAdminConfirmation personBlockLoginByAdminConfirmation =
                notifyAndWaitForReply(PersonBlockLoginByAdminConfirmation.class, personBlockLoginByAdminRequest);

        return ClientPersonBlockLoginByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personService.fetchPersonExtendedAttributes(personBlockLoginByAdminConfirmation.getPerson())))
                .build();
    }

    @ApiOperation(value = "Unblocks a user so that it can log in again",
            notes = """
                    Allows a user to log in again by unblocking the account at Auth0 and removing the status 'LOGIN_BLOCKED'.
                    The users account at Auth0 is unblocked. The user has to log in again to get refresh or access tokens.
                    If the person was deleted before the status 'LOGIN_BLOCKED' is removed, but the person stays deleted.The blocked account at Auth0 is deleted, so that the user can register again.""")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {UnblockLoginAction.class})
    @PostMapping("/unblockLoginByAdminRequest")
    public ClientPersonUnblockLoginByAdminResponse onUnblockLoginRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonUnblockLoginByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted unblockLoginPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(UnblockLoginAction.class, currentPerson);

        final Person personToBeUnblocked = getPersonService().findPersonById(request.getPersonId());

        if (unblockLoginPermission.isTenantDenied(personToBeUnblocked.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to unblock person {}",
                    personToBeUnblocked.getId());
        }

        PersonUnblockLoginByAdminRequest personUnblockLoginByAdminRequest =
                PersonUnblockLoginByAdminRequest.builder()
                        .person(personToBeUnblocked)
                        .build();

        PersonUnblockLoginByAdminConfirmation personUnblockLoginByAdminConfirmation =
                notifyAndWaitForReply(PersonUnblockLoginByAdminConfirmation.class, personUnblockLoginByAdminRequest);

        return ClientPersonUnblockLoginByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personUnblockLoginByAdminConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Unblocks a user from an automatically block event so that it can log in again",
            notes = "Allows a user to log in again by unblocking the account at Auth0 after it has been " +
                    "automatically blocked (e.g. due to too many failed login attempts).")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {UnblockAutomaticallyBlockedLoginAction.class})
    @PostMapping("/unblockAutomaticallyBlockedLoginByAdminRequest")
    public ClientPersonUnblockAutomaticallyBlockedLoginByAdminResponse onUnblockAutomaticallyBlockedLoginRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonUnblockAutomaticallyBlockedLoginByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted unblockAutomaticallyBlockedLoginPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(UnblockAutomaticallyBlockedLoginAction.class, currentPerson);

        final Person personToBeUnblocked = getPersonService().findPersonById(request.getPersonId());

        if (unblockAutomaticallyBlockedLoginPermission.isTenantDenied(personToBeUnblocked.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to unblock person {}",
                    personToBeUnblocked.getId());
        }

        PersonUnblockAutomaticallyBlockedLoginByAdminRequest personUnblockAutomaticallyBlockedLoginByAdminRequest =
                PersonUnblockAutomaticallyBlockedLoginByAdminRequest.builder()
                        .person(personToBeUnblocked)
                        .build();

        PersonUnblockAutomaticallyBlockedLoginByAdminConfirmation
                personUnblockAutomaticallyBlockedLoginByAdminConfirmation =
                notifyAndWaitForReply(PersonUnblockAutomaticallyBlockedLoginByAdminConfirmation.class,
                        personUnblockAutomaticallyBlockedLoginByAdminRequest);

        return ClientPersonUnblockAutomaticallyBlockedLoginByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personUnblockAutomaticallyBlockedLoginByAdminConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Set a users email address verification status",
            notes = "If there is a pending new email address for this person, " +
                    "it is set to verified and the email address of the person is set to this new email address.")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {SetEmailVerifiedByAdminAction.class})
    @PostMapping("/setEmailVerifiedByAdminRequest")
    public ClientPersonSetEmailVerifiedByAdminResponse onSetEmailVerifiedRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonSetEmailVerifiedByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted setEmailVerifiedPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(SetEmailVerifiedByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (setEmailVerifiedPermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to set email verification status of person {}",
                    personToBeChanged.getId());
        }
        final String emailAddress =
                StringUtils.isEmpty(personToBeChanged.getPendingNewEmail()) ? personToBeChanged.getEmail() :
                        personToBeChanged.getPendingNewEmail();

        PersonChangeEmailAddressVerificationStatusByAdminRequest personChangeEmailVerificationStatusByAdminRequest =
                PersonChangeEmailAddressVerificationStatusByAdminRequest.builder()
                        .person(personToBeChanged)
                        .emailAddress(emailAddress)
                        .emailAddressVerified(request.isEmailAddressVerified())
                        .build();

        PersonChangeEmailAddressVerificationStatusConfirmation personChangeEmailAddressVerificationStatusConfirmation =
                notifyAndWaitForReply(PersonChangeEmailAddressVerificationStatusConfirmation.class,
                        personChangeEmailVerificationStatusByAdminRequest);

        return ClientPersonSetEmailVerifiedByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personChangeEmailAddressVerificationStatusConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Trigger a users email verification mail")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND
    })
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {SendVerificationMailByAdminAction.class})
    @PostMapping("/sendVerificationMailByAdminRequest")
    public ClientPersonSendVerificationMailByAdminResponse onSendVerificationMailRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonSendVerificationMailByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted sendVerificationMailPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(SendVerificationMailByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (sendVerificationMailPermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to trigger email verification mail of person {}",
                    personToBeChanged.getId());
        }

        final PersonResendVerificationEmailByAdminRequest personResendVerificationEmailByAdminRequest =
                PersonResendVerificationEmailByAdminRequest.builder()
                        .person(personToBeChanged)
                        .build();

        final PersonResendVerificationEmailConfirmation personResendVerificationEmailConfirmation =
                notifyAndWaitForReply(PersonResendVerificationEmailConfirmation.class,
                        personResendVerificationEmailByAdminRequest);

        return ClientPersonSendVerificationMailByAdminResponse.builder()
                .person(retrieveOauthAccountAndConvertToClientPerson(
                        personResendVerificationEmailConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Changes a users password and triggers password reset mail",
            notes = "Changes the users password to a random password and sends a password reset mail to the user. " +
                    "The current password of the user is changed. This only works for username-password accounts.")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND,
            ClientExceptionType.PASSWORD_CHANGE_NOT_POSSIBLE})
    @ApiException(value = ClientExceptionType.PASSWORD_CHANGE_NOT_POSSIBLE,
            reason = "The password could not be changed because there is no username-password authentication for this user.")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ResetPasswordByAdminAction.class})
    @PostMapping("/resetPasswordByAdminRequest")
    public ClientPersonResetPasswordByAdminResponse onResetPasswordRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonResetPasswordByAdminRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted resetPasswordPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ResetPasswordByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (resetPasswordPermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to reset password of person {}",
                    personToBeChanged.getId());
        }

        PersonResetPasswordByAdminRequest personResetPasswordByAdminRequest =
                PersonResetPasswordByAdminRequest.builder()
                        .person(personToBeChanged)
                        .build();

        PersonResetPasswordByAdminConfirmation personResetPasswordByAdminConfirmation =
                notifyAndWaitForReply(PersonResetPasswordByAdminConfirmation.class,
                        personResetPasswordByAdminRequest);

        return ClientPersonResetPasswordByAdminResponse.builder()
                .updatedPerson(retrieveOauthAccountAndConvertToClientPerson(
                        personResetPasswordByAdminConfirmation.getPerson()))
                .build();
    }

    @ApiOperation(value = "Changes the statuses of a user")
    @ApiExceptions({
            ClientExceptionType.PERSON_NOT_FOUND,
            ClientExceptionType.OAUTH_ACCOUNT_NOT_FOUND})
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ChangePersonStatusByAdminAction.class})
    @PostMapping("/personChangeStatusRequest")
    ClientPersonChangeStatusConfirmation onPersonChangeStatusRequest(
            @Valid @RequestBody
            @ApiParam(required = true)
                    ClientPersonChangeStatusRequest request
    ) {

        final Person currentPerson = getCurrentPersonNotNull();

        PermissionTenantRestricted changePersonStatusPermission = getRoleService()
                .decidePermissionAndThrowNotAuthorized(ResetPasswordByAdminAction.class, currentPerson);

        final Person personToBeChanged = getPersonService().findPersonById(request.getPersonId());

        if (changePersonStatusPermission.isTenantDenied(personToBeChanged.getTenant())) {
            throw new NotAuthorizedException("Insufficient privileges to change statuses of person {}",
                    personToBeChanged.getId());
        }

        final PersonChangeStatusRequest personChangeStatusRequest = PersonChangeStatusRequest.builder()
                .person(personToBeChanged)
                .statusesToAdd(personClientModelMapper.toPersonStatuses(request.getStatusesToAdd()))
                .statusesToRemove(personClientModelMapper.toPersonStatuses(request.getStatusesToRemove()))
                .build();

        final PersonChangeStatusConfirmation personChangeStatusConfirmation =
                notifyAndWaitForReply(PersonChangeStatusConfirmation.class,
                        personChangeStatusRequest);

        return ClientPersonChangeStatusConfirmation.builder()
                .updatedPerson(
                        retrieveOauthAccountAndConvertToClientPerson(personChangeStatusConfirmation.getUpdatedPerson()))
                .build();
    }

    private String checkAndTrim(String input, String inputName) {
        final String result = StringUtils.trim(input);
        if (StringUtils.isEmpty(result)) {
            throw PersonInformationInvalidException.forMissingField(inputName);
        }
        return result;
    }

}
