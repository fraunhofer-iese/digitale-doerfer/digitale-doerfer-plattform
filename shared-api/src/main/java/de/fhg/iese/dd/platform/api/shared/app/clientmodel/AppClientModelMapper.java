/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2022 Balthasar Weitzel
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.app.clientmodel;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhg.iese.dd.platform.api.participants.geoarea.clientmodel.GeoAreaClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.files.clientmodel.FileClientModelMapper;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppVariantVersionService;
import de.fhg.iese.dd.platform.datamanagement.framework.model.BaseEntity;
import de.fhg.iese.dd.platform.datamanagement.participants.geoarea.model.GeoArea;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantGeoAreaMapping;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantTenantContract;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariantUsage;

@Component
public class AppClientModelMapper {

    @Autowired
    private FileClientModelMapper fileClientModelMapper;
    @Autowired
    private GeoAreaClientModelMapper geoAreaClientModelMapper;

    public ClientAppVariant toClientAppVariant(AppVariant appVariant) {
        if (appVariant == null) {
            return null;
        }
        return ClientAppVariant.builder()
                .id(appVariant.getId())
                .appVariantIdentifier(appVariant.getAppVariantIdentifier())
                .name(appVariant.getName())
                .logo(fileClientModelMapper.toClientMediaItem(appVariant.getLogo()))
                .app(toClientApp(appVariant.getApp()))
                .build();
    }

    public ClientAppVariantWithSecrets toClientAppVariantWithSecrets(AppVariant appVariant) {
        if (appVariant == null) {
            return null;
        }
        return ClientAppVariantWithSecrets.builder()
                .id(appVariant.getId())
                .appVariantIdentifier(appVariant.getAppVariantIdentifier())
                .name(appVariant.getName())
                .logo(fileClientModelMapper.toClientMediaItem(appVariant.getLogo()))
                .app(toClientApp(appVariant.getApp()))
                .apiKey1(appVariant.getApiKey1())
                .apiKey2(appVariant.getApiKey2())
                .callBackUrl(appVariant.getCallBackUrl())
                .externalApiKey(appVariant.getExternalApiKey())
                .externalBasicAuthUsername(appVariant.getExternalBasicAuthUsername())
                .externalBasicAuthPassword(appVariant.getExternalBasicAuthPassword())
                .build();
    }

    public ClientApp toClientApp(App app) {
        if (app == null) {
            return null;
        }
        return ClientApp.builder()
                .id(app.getId())
                .appIdentifier(app.getAppIdentifier())
                .name(app.getName())
                .logo(fileClientModelMapper.toClientMediaItem(app.getLogo()))
                .build();
    }

    public ClientAppVariantUsageExtended toClientAppVariantUsageExtended(AppVariantUsage appVariantUsage,
            Set<GeoArea> selectedGeoAreas) {
        if (appVariantUsage == null) {
            return null;
        }
        return ClientAppVariantUsageExtended.builder()
                .appVariant(toClientAppVariant(appVariantUsage.getAppVariant()))
                .lastUsage(appVariantUsage.getLastUsage())
                .selectedGeoAreas(geoAreaClientModelMapper.toClientGeoAreas(selectedGeoAreas, false))
                .build();
    }

    public ClientAppVariantTenantContract toClientAppVariantTenantContract(
            AppVariantTenantContract appVariantTenantContract) {
        if (appVariantTenantContract == null) {
            return null;
        }

        List<String> geoAreaIdsIncluded;
        List<String> geoAreaIdsExcluded;
        if (appVariantTenantContract.getMappings() == null) {
            geoAreaIdsIncluded = Collections.emptyList();
            geoAreaIdsExcluded = Collections.emptyList();
        } else {
            geoAreaIdsExcluded = appVariantTenantContract.getMappings().stream()
                    .filter(AppVariantGeoAreaMapping::isExcluded)
                    .map(m -> m.getGeoArea().getId())
                    .sorted()
                    .collect(Collectors.toList());
            geoAreaIdsIncluded = appVariantTenantContract.getMappings().stream()
                    .filter(m -> !m.isExcluded())
                    .map(m -> m.getGeoArea().getId())
                    .sorted()
                    .collect(Collectors.toList());
        }

        return ClientAppVariantTenantContract.builder()
                .id(appVariantTenantContract.getId())
                .created(appVariantTenantContract.getCreated())
                .appVariantId(BaseEntity.getIdOf(appVariantTenantContract.getAppVariant()))
                .tenantId(BaseEntity.getIdOf(appVariantTenantContract.getTenant()))
                .notes(appVariantTenantContract.getNotes())
                .additionalNotes(appVariantTenantContract.getAdditionalNotes())
                .geoAreaIdsIncluded(geoAreaIdsIncluded)
                .geoAreaIdsExcluded(geoAreaIdsExcluded)
                .build();
    }

    public ClientAppVariantVersionCheckResult toClientAppVariantVersionCheckResult(
            IAppVariantVersionService.CheckVersionResult checkVersionResult) {
        if (checkVersionResult == null) {
            return null;
        }
        return ClientAppVariantVersionCheckResult.builder()
                .supported(checkVersionResult.isSupported())
                .text(checkVersionResult.getText())
                .url(checkVersionResult.getUrl())
                .build();
    }

    public ClientTenantAndAppVariantEntry toClientTenantAndAppVariantEntry(AppVariant appVariant, Tenant tenant) {
        final ClientTenantAndAppVariantEntry.ClientTenantAndAppVariantEntryBuilder
                clientTenantAndAppVariantEntryBuilder = ClientTenantAndAppVariantEntry.builder()
                .appVariantId(StringUtils.defaultString(appVariant.getId()))
                .appVariantIdentifier(StringUtils.defaultString(appVariant.getAppVariantIdentifier()))
                .appVariantName(StringUtils.defaultString(appVariant.getName()))
                .tenantId(StringUtils.defaultString(tenant.getId()))
                .tenantName(StringUtils.defaultString(tenant.getName()));
        if (appVariant.getApp() != null) {
            clientTenantAndAppVariantEntryBuilder
                    .appId(StringUtils.defaultString(appVariant.getApp().getId()))
                    .appIdentifier(StringUtils.defaultString(appVariant.getApp().getAppIdentifier()))
                    .appName(StringUtils.defaultString(appVariant.getApp().getName()));
        }
        return clientTenantAndAppVariantEntryBuilder
                .build();
    }

}
