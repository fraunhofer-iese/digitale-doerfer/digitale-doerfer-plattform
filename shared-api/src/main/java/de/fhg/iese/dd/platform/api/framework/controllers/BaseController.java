/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 - 2023 Balthasar Weitzel, Johannes Schneider, Dominik Schnier, Steffen Hupp
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.framework.controllers;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.spring.security.api.authentication.JwtAuthentication;
import de.fhg.iese.dd.platform.api.framework.exceptions.BadPageParameterException;
import de.fhg.iese.dd.platform.api.framework.exceptions.InvalidEventAttributeException;
import de.fhg.iese.dd.platform.business.framework.events.BaseEvent;
import de.fhg.iese.dd.platform.business.framework.events.EventBusAccessor;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingException;
import de.fhg.iese.dd.platform.business.framework.events.exceptions.EventProcessingTimeoutException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonVerificationStatusInsufficientException;
import de.fhg.iese.dd.platform.business.participants.person.exceptions.PersonWithOauthIdNotFoundException;
import de.fhg.iese.dd.platform.business.participants.person.services.IPersonService;
import de.fhg.iese.dd.platform.business.participants.personblocking.services.IPersonBlockingService;
import de.fhg.iese.dd.platform.business.shared.app.exceptions.AppVariantNotFoundException;
import de.fhg.iese.dd.platform.business.shared.app.services.IAppService;
import de.fhg.iese.dd.platform.business.shared.feature.exceptions.FeatureNotFoundException;
import de.fhg.iese.dd.platform.business.shared.feature.services.FeatureTarget;
import de.fhg.iese.dd.platform.business.shared.feature.services.IFeatureService;
import de.fhg.iese.dd.platform.business.shared.security.exceptions.OauthClientNotFoundException;
import de.fhg.iese.dd.platform.business.shared.security.services.IOauthClientService;
import de.fhg.iese.dd.platform.business.shared.security.services.IRoleService;
import de.fhg.iese.dd.platform.business.shared.usergeneratedcontentflags.services.IUserGeneratedContentFlagService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.InternalServerErrorException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthenticatedException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.framework.services.ITimeService;
import de.fhg.iese.dd.platform.datamanagement.participants.feature.PersonVerificationStatusRestrictionFeature;
import de.fhg.iese.dd.platform.datamanagement.participants.person.model.Person;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.App;
import de.fhg.iese.dd.platform.datamanagement.shared.app.model.AppVariant;
import de.fhg.iese.dd.platform.datamanagement.shared.config.ApplicationConfig;
import de.fhg.iese.dd.platform.datamanagement.shared.security.model.OauthClient;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Supplier;

public abstract class BaseController {

    protected final Logger log = LogManager.getLogger(this.getClass());

    public static final String HEADER_NAME_APP_VARIANT_IDENTIFIER = "appVariantIdentifier";
    public static final String HEADER_NAME_API_KEY = "apiKey";
    public static final String JWT_CLAIM_NAME_AUTHORIZED_PARTY = "azp";
    public static final String REQUEST_ATTRIBUTE_NAME_REQUEST_CONTEXT = "de.fhg.iese.dd.platform.requestcontext";

    @Autowired
    private EventBusAccessor eventBusAccessor;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private ApplicationConfig config;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IRoleService roleService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IPersonService personService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IAppService appService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IOauthClientService oauthClientService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IFeatureService featureService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IPersonBlockingService personBlockingService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private IUserGeneratedContentFlagService userGeneratedContentFlagService;

    @Autowired
    @Getter(AccessLevel.PROTECTED)
    private ITimeService timeService;

    @PostConstruct
    private void initialize(){
        eventBusAccessor.setOwner(this);
    }

    /**
     * Publishes the given event and does not wait for a reply. Exceptions caused by this event will not be received.
     *
     * @param <E>
     *         type of the event
     * @param event
     *         The event to be sent out
     */
    final protected <E extends BaseEvent> void notify(E event) {
        eventBusAccessor.notifyTrigger(event);
    }

    /**
     * Publishes the given events and waits for a reply of a specific type to occur.
     *
     * @param eventToWaitFor
     *         The type of event that should occur
     * @param eventsToNotify
     *         The events that are published and should trigger the eventToWaitFor
     * @param <E>
     *         type of the event
     *
     * @return The event that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         if a timeout occurred while waiting for the reply
     * @throws EventProcessingException
     *         if any involved event processor threw an exception
     */
    final protected <E extends BaseEvent> E notifyAndWaitForReply(Class<E> eventToWaitFor, BaseEvent... eventsToNotify)
            throws EventProcessingTimeoutException, EventProcessingException {
        @SuppressWarnings("unchecked")
        E reply = (E) eventBusAccessor.notifyTriggerAndWaitForAnyReply(Arrays.asList(eventsToNotify),
                Collections.singletonList(eventToWaitFor));
        return reply;
    }

    /**
     * Publishes the given event and waits for any reply of the specified types to occur.
     *
     * @param eventToNotify
     *         The event that is published and should trigger one of the eventsToWaitFor
     * @param eventsToWaitFor
     *         The types of event that should occur
     *
     * @return The first event of the specific type that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         if a timeout occurred while waiting for the reply
     * @throws EventProcessingException
     *         if any involved event processor threw an exception
     */
    @SafeVarargs
    @SuppressWarnings("varargs")
    final protected BaseEvent notifyAndWaitForAnyReply(BaseEvent eventToNotify,
            Class<? extends BaseEvent>... eventsToWaitFor)
            throws EventProcessingTimeoutException, EventProcessingException {
        return eventBusAccessor.notifyTriggerAndWaitForAnyReply(Collections.singletonList(eventToNotify),
                Arrays.asList(eventsToWaitFor));
    }

    /**
     * Publishes the given events and waits for any reply of the specified types to occur.
     *
     * @param eventsToWaitFor
     *         The types of event that should occur
     * @param eventsToNotify
     *         The events that are published and should trigger one of the eventsToWaitFor
     *
     * @return The first event of the specific type that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         if a timeout occurred while waiting for the reply
     * @throws EventProcessingException
     *         if any involved event processor threw an exception
     */
    @SafeVarargs
    @SuppressWarnings("varargs")
    final protected BaseEvent notifyAndWaitForAnyReply(Iterable<? extends BaseEvent> eventsToNotify,
            Class<? extends BaseEvent>... eventsToWaitFor)
            throws EventProcessingTimeoutException, EventProcessingException {
        return eventBusAccessor.notifyTriggerAndWaitForAnyReply(eventsToNotify, Arrays.asList(eventsToWaitFor));
    }

    /**
     * Publishes the given events and waits for any reply of the specified types to occur.
     *
     * @param eventsToWaitFor
     *         The types of event that should occur
     * @param eventsToNotify
     *         The events that are published and should trigger one of the eventsToWaitFor
     *
     * @return The first event of the specific type that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         if a timeout occurred while waiting for the reply
     * @throws EventProcessingException
     *         if any involved event processor threw an exception
     */
    final protected BaseEvent notifyAndWaitForAnyReply(Iterable<? extends BaseEvent> eventsToNotify,
            Collection<Class<? extends BaseEvent>> eventsToWaitFor)
            throws EventProcessingTimeoutException, EventProcessingException {
        return eventBusAccessor.notifyTriggerAndWaitForAnyReply(eventsToNotify, eventsToWaitFor);
    }

    /**
     * Publishes the given events and waits for any reply of the specified types to occur.
     *
     * @param eventsToWaitFor
     *         The types of event that should occur
     * @param eventsToNotify
     *         The events that are published and should trigger one of the eventsToWaitFor
     *
     * @return The first event of the specific type that was waited for
     *
     * @throws EventProcessingTimeoutException
     *         if a timeout occurred while waiting for the reply
     * @throws EventProcessingException
     *         if any involved event processor threw an exception
     */
    final protected BaseEvent notifyAndWaitForAnyReply(Collection<Class<? extends BaseEvent>> eventsToWaitFor,
            BaseEvent... eventsToNotify) throws EventProcessingTimeoutException, EventProcessingException {
        return eventBusAccessor.notifyTriggerAndWaitForAnyReply(Arrays.asList(eventsToNotify), eventsToWaitFor);
    }

    protected String checkAttributeNotNullOrEmpty(String attribute, String attributeName) throws InvalidEventAttributeException {
        if(StringUtils.isEmpty(attribute)) {
            throw new InvalidEventAttributeException("The attribute {} may not be empty/null!", attributeName);
        }
        return attribute;
    }

    protected static void checkPageAndCountValues(int page, int count) {
        if (page < 0) {
            throw new BadPageParameterException("'page' must be 0 or greater.").withDetail("page");
        }
        if (count < 1) {
            throw new BadPageParameterException("'count' must be 1 or greater.").withDetail("count");
        }
    }

    /**
     * Returns the OAuth user id that is available when a REST call is made
     * providing a valid JWT token whose "sub" value of the payload is not
     * empty. The token is extracted from the {@link SecurityContext} of this
     * REST call.
     * <p/>
     * This method will never return {@code null}
     *
     * @return OAuth user id
     * @throws NotAuthorizedException
     *             no valid JWT token was provided in the REST call (this case
     *             should be avoided by proper configuration in
     *             {@link WebSecurityConfiguration} by defining the according
     *             method {@code authenticated()}). If this is done the call is
     *             already rejected by Spring before it hits the controller.
     *             Nevertheless http basic authentication is not rejected, so
     *             this needs to be rejected here.
     */
    final protected String getOAuthUserIdNotNull() throws NotAuthorizedException {
        return getRequestContext().getOAuthUserIdNotNull();
    }

    /**
     * Returns the OAuth user id that is available when a REST call is made
     * providing a valid JWT token whose "sub" value of the payload is not
     * empty. The token is extracted from the {@link SecurityContext} of this
     * REST call.
     * <p/>
     * This method will never throw an exception.
     *
     * @return OAuth user id if present, {@code null} otherwise
     */
    final protected String getOAuthUserIdOrNull() {
        return getRequestContext().getOAuthUserId().orElse(null);
    }

    /**
     * Returns the current person if possible. The person can be determined, if
     * the REST method was called providing a valid JWT token and the OAuth user
     * id of that token can be found in the person table. The token is extracted
     * from the {@link SecurityContext} of this REST call.
     * <p/>
     * Use this method within REST methods that are configured to be accessible
     * both anonymous and authorized. This configuration is done in
     * {@link WebSecurityConfiguration} by defining them {@code permitAll()}.
     *
     * @return {@code null} if the REST method was called without user
     *         authentication, the person otherwise.
     * @throws PersonWithOauthIdNotFoundException
     *             if the REST call was made with a valid JWT token, but the
     *             OAuth user id has no corresponding person in the database.
     *             This can be the case if the user registration process was
     *             canceled.
     */
    final protected Person getCurrentPersonOrNull() throws PersonWithOauthIdNotFoundException {
        return getRequestContext().getPerson().orElse(null);
    }

    /**
     * Returns the current person that is expected to be available. The person
     * can be determined, if the REST method was called providing a valid JWT
     * token and the OAuth user id can be found in the person table. The token is
     * extracted from the {@link SecurityContext} of this REST call.
     * <p/>
     * Use this method within REST methods that are configured to be only
     * accessible after authorization. This configuration is done in
     * {@link WebSecurityConfiguration} by defining them
     * {@code authenticated()}.
     *
     *
     * @return the current person (never {@code null})
     * @throws PersonWithOauthIdNotFoundException
     *             if the provided OAuth user id has no corresponding person in
     *             the database. This can be the case if the user registration
     *             process was canceled.
     * @throws NotAuthorizedException
     *             no valid JWT token was provided in the REST call (this case
     *             should be avoided by proper configuration in
     *             {@link WebSecurityConfiguration} by defining the according
     *             method {@code authenticated()}). If this is done the call is
     *             already rejected by Spring before it hits the controller.
     *             Nevertheless http basic authentication is not rejected, so
     *             this needs to be rejected here.
     */
    final protected Person getCurrentPersonNotNull() throws PersonWithOauthIdNotFoundException, NotAuthorizedException {
        return getRequestContext().getPersonNotNull();
    }

    /**
     * Gets the current app variant.
     * <p/>
     * The app variant is determined based on
     * <ul>
     *     <li>the oauth client that was used when requesting the access token (claim "azp"=authorized party)</li>
     *     <li>the app variant identifier given in the header {@value #HEADER_NAME_APP_VARIANT_IDENTIFIER}</li>
     *     <li>the api key given in the header {@value #HEADER_NAME_API_KEY}</li>
     * </ul>
     *
     * @return the current app variant
     *
     * @throws AppVariantNotFoundException if there is no app variant with the given identifier
     * @see IAppService#findAppVariantByOauthClient(OauthClient, Optional, Optional)
     */
    protected AppVariant getAppVariantNotNull() throws AppVariantNotFoundException {
        return getRequestContext().getAppVariantNotNull();
    }

    /**
     * Gets the current app variant if possible.
     * <p/>
     * The app variant is determined based on
     * <ul>
     *     <li>the oauth client that was used when requesting the access token (claim "azp"=authorized party)</li>
     *     <li>the app variant identifier given in the header {@value #HEADER_NAME_APP_VARIANT_IDENTIFIER}</li>
     *     <li>the api key given in the header {@value #HEADER_NAME_API_KEY}</li>
     * </ul>
     * *
     *
     * @return {@code null} if the REST method was called without app variant identifier, the app variant otherwise.
     *
     * @throws AppVariantNotFoundException if there is no app variant with the given identifier
     */
    protected AppVariant getAppVariantOrNull() throws AppVariantNotFoundException {
        return getRequestContext().getAppVariant().orElse(null);
    }

    /**
     * Gets the current oauth client.
     * <p/>
     * The oauth client is determined by using the oauth client identifier contained in the access token (claim
     * "azp"=authorized part)
     *
     * @return the current oauth client
     *
     * @throws OauthClientNotFoundException if there is no oauth client with the given identifier
     * @see IOauthClientService#findByOauthClientIdentifier(String)
     */
    protected OauthClient getOauthClientNotNull() throws OauthClientNotFoundException {
        return getRequestContext().getOAuthClientNotNull();
    }

    /**
     * Gets the provided api key.
     * <p/>
     * Requires the header {@value #HEADER_NAME_API_KEY} to be set.
     *
     * @return the current api key
     *
     * @throws NotAuthenticatedException if no api key was provided in the request
     */
    protected String getApiKeyNotNull() throws NotAuthenticatedException {
        return getRequestContext().getApiKeyNotNull();
    }

    /**
     * Gets the current app variant that is authenticated with a matching api key.
     * <p/>
     * Requires the header {@value #HEADER_NAME_API_KEY} to be set.
     *
     * @return the current authenticated app variant
     *
     * @throws NotAuthenticatedException if no api key was provided in the request
     * @throws NotAuthorizedException    if no matching app variant was found
     */
    protected AppVariant getAuthenticatedAppVariantNotNull()
            throws NotAuthenticatedException, NotAuthorizedException {
        return getRequestContext().getAuthenticatedAppVariantNotNull();
    }
    
    final public void validateApiKey(Supplier<String> expectedApiKey, String actualApiKey, String apiKeyName) {
        if (StringUtils.isBlank(actualApiKey)) {
            throw new NotAuthenticatedException("Empty api key");
        }

        if (!StringUtils.equals(expectedApiKey.get(), actualApiKey)) {
            throw new NotAuthorizedException("Wrong api key: check configuration {}", apiKeyName);
        }
    }

    /**
     * Checks if the person is allowed to use this endpoint, based on the feature configuration of the given feature.
     * The check uses  {@link Person#getVerificationStatuses()} and matches them with {@link
     * PersonVerificationStatusRestrictionFeature#getAllowedStatusesAnyOf()} and {@link
     * PersonVerificationStatusRestrictionFeature#getAllowedStatusesAllOf()}
     *
     * @param feature    the feature configuration to use for the check
     * @param person     the person that should be checked
     * @param appVariant the app variant that is used to determine the feature configuration
     *
     * @throws PersonVerificationStatusInsufficientException if the verification status is insufficient
     */
    protected void checkVerificationStatusRestriction(
            Class<? extends PersonVerificationStatusRestrictionFeature> feature,
            Person person, AppVariant appVariant) throws PersonVerificationStatusInsufficientException {
        try {
            PersonVerificationStatusRestrictionFeature featureForPerson =
                    featureService.getFeature(feature, FeatureTarget.of(person, appVariant));
            if (!featureForPerson.isAllowed(person)) {
                throw new PersonVerificationStatusInsufficientException(featureForPerson);
            }
        } catch (FeatureNotFoundException e) {
            //if the feature is not found we assume that no restriction is set
        }
    }

    /**
     * Gets the request context of the current call that contains all relevant entities for that call, e.g. Person that
     * made the call, AppVariant that was used.
     *
     * @return the current request context
     */
    RequestContext getRequestContext() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            Object requestContext = request.getAttribute(REQUEST_ATTRIBUTE_NAME_REQUEST_CONTEXT);
            if (requestContext != null) {
                if (requestContext instanceof RequestContext) {
                    return (RequestContext) requestContext;
                } else {
                    log.error("RequestContext is of wrong type, creating new one: {}", requestContext);
                }
            }
            RequestContext newRequestContext = RequestContext.builder()
                    .oAuthUserIdValue(new RequestContextValue<>(this::internalGetOAuthUserIdNotNull))
                    .oAuthClientIdentifierValue(
                            new RequestContextValue<>(this::internalGetOAuthClientIdentifierNotNull))
                    .oAuthClientValue(new RequestContextValue<>(this::internalGetOauthClientNotNull))
                    .personValue(new RequestContextValue<>(this::internalGetCurrentPersonNotNull,
                            this::updateAppVariantLastUsageTimeIfPersonAndAppVariantAreFetched))
                    .appVariantIdentifierValue(new RequestContextValue<>(this::internalGetAppVariantIdentifierNotNull))
                    .appVariantValue(new RequestContextValue<>(this::internalGetAppVariantNotNull,
                            this::updateAppVariantLastUsageTimeIfPersonAndAppVariantAreFetched))
                    .apiKeyValue(new RequestContextValue<>(this::internalGetApiKeyNotNull))
                    .authenticatedAppVariantValue(
                            new RequestContextValue<>(this::internalGetAuthenticatedAppVariantNotNull))
                    .blockedPersonIdsValue(new RequestContextValue<>(this::internalGetBlockedPersonIds))
                    .flaggedEntityIdsValue(new RequestContextValue<>(this::internalGetFlaggedEntityIds))
                    .build();
            request.setAttribute(REQUEST_ATTRIBUTE_NAME_REQUEST_CONTEXT, newRequestContext);
            return newRequestContext;
        }
        log.error("RequestAttributes is of wrong type: {}", requestAttributes);
        throw new InternalServerErrorException("Could not get request context");
    }

    private AppVariant internalGetAuthenticatedAppVariantNotNull() {
        try {
            return appService.findAppVariantByApiKey(getRequestContext().getApiKeyNotNull());
        } catch (AppVariantNotFoundException e) {
            throw new NotAuthorizedException(e);
        }
    }

    private String internalGetApiKeyNotNull() throws NotAuthenticatedException {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        String apiKey = null;
        if (requestAttributes instanceof ServletRequestAttributes servletRequestAttributes) {
            HttpServletRequest request = servletRequestAttributes.getRequest();
            apiKey = request.getHeader(HEADER_NAME_API_KEY);
        }
        if (StringUtils.isBlank(apiKey)) {
            throw new NotAuthenticatedException("Empty api key");
        }
        return apiKey;
    }

    private String internalGetOAuthUserIdNotNull() throws NotAuthorizedException {
        final SecurityContext context = SecurityContextHolder.getContext();
        if (!isAuthenticatedWithJwtToken(context)) {
            // when we do not have a JwtAuthentication we can not extract the oAuthId, so we throw an exception
            throw new NotAuthorizedException("Wrong authentication method for this kind of resources");
        }
        String oAuthUserId = context.getAuthentication().getName();
        if (oAuthUserId == null || oAuthUserId.length() == 0) {
            throw new NotAuthorizedException("No oAuth user id provided");
        }
        return oAuthUserId;
    }

    private boolean isAuthenticatedWithJwtToken(SecurityContext context) {
        return context != null
                && context.getAuthentication() != null
                && (context.getAuthentication() instanceof JwtAuthentication); //see http://stackoverflow.com/a/26117007
    }

    private String internalGetOAuthClientIdentifierNotNull() throws NotAuthorizedException {
        final SecurityContext context = SecurityContextHolder.getContext();
        if (context != null
                && context.getAuthentication() != null
                && context.getAuthentication().getDetails() instanceof DecodedJWT decodedJWT) {
            String oauthClientIdentifier = decodedJWT.getClaim(JWT_CLAIM_NAME_AUTHORIZED_PARTY).asString();
            if (StringUtils.isNotEmpty(oauthClientIdentifier)) {
                return oauthClientIdentifier;
            }
        }
        throw new OauthClientNotFoundException("No oauth client identifier provided in access token");
    }

    private OauthClient internalGetOauthClientNotNull() {
        return oauthClientService.findByOauthClientIdentifier(getRequestContext().getOAuthClientIdentifierNotNull());
    }

    private Person internalGetCurrentPersonNotNull() {
        return personService.findCurrentlyLoggedInPerson(getOAuthUserIdNotNull());
    }

    private String internalGetAppVariantIdentifierNotNull() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        String appVariantIdentifier = null;
        if (requestAttributes instanceof ServletRequestAttributes servletRequestAttributes) {
            HttpServletRequest request = servletRequestAttributes.getRequest();
            appVariantIdentifier = request.getHeader(HEADER_NAME_APP_VARIANT_IDENTIFIER);
        }
        if (StringUtils.isEmpty(appVariantIdentifier)) {
            throw new AppVariantNotFoundException("AppVariant not found, appVariantIdentifier is empty").withDetail("");
        }
        return appVariantIdentifier;
    }

    private AppVariant internalGetAppVariantNotNull() {
        if (isAuthenticatedWithJwtToken(SecurityContextHolder.getContext())) {
            OauthClient oauthClient;
            try {
                oauthClient = getRequestContext().getOAuthClientNotNull();
            } catch (OauthClientNotFoundException e) {
                throw new AppVariantNotFoundException(e);
            }
            return appService.findAppVariantByOauthClient(
                    oauthClient,
                    getRequestContext().getAppVariantIdentifier(),
                    getRequestContext().getApiKey());
        } else {
            return appService.findAppVariantByIdentifierOrApiKey(
                    getRequestContext().getAppVariantIdentifier(),
                    getRequestContext().getApiKey());
        }
    }

    /**
     * When we have both the person and the app variant, we can update the usage time
     */
    private void updateAppVariantLastUsageTimeIfPersonAndAppVariantAreFetched() {
        RequestContext requestContext = getRequestContext();
        if (requestContext.getPersonValue().getValue() != null &&
                requestContext.getAppVariantValue().getValue() != null) {
            appService.createOrUpdateAppVariantUsage(getAppVariantNotNull(), getCurrentPersonNotNull(), false);
        }
    }

    private Set<String> internalGetBlockedPersonIds() {

        final Optional<Person> person = getRequestContext().getPerson();
        final Optional<AppVariant> appVariant = getRequestContext().getAppVariant();

        //this is not a showcase for how to use Optionals, but more performant than using functional notation
        if (person.isPresent() && appVariant.isPresent()) {
            return personBlockingService.getBlockedNotDeletedPersonIds(appVariant.get().getApp(), person.get());
        }

        return Collections.emptySet();
    }

    private Set<String> internalGetFlaggedEntityIds() {

        //this is not a showcase for how to use Optionals, but more performant than using functional notation
        final Optional<Person> person = getRequestContext().getPerson();
        if (person.isPresent()) {
            return userGeneratedContentFlagService.findFlaggedEntityIdsByFlagCreator(person.get());
        }

        return Collections.emptySet();
    }

    /**
     * Adds an additional log value that is added to the json request log message.
     * <p>
     * If there are already log values with the same key the new ones are appended.
     * <p>
     * Do not use the values used in {@link LogRequestsInterceptor}, since they would be overwritten:
     * <p>
     * direction, appVariantIdentifier, method, requestPattern, requestURI, personId, duration, status, exceptionType,
     * exceptionMessage
     */
    protected void addAdditionalLogValue(String key, Object value) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        final String stringValue = String.valueOf(value);
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            Object existingLogAttribute =
                    request.getAttribute(LogRequestsInterceptor.REQUEST_ATTRIBUTE_ADDITIONAL_LOG_DETAILS);
            Map<String, String> additionalLogValues;
            if (!(existingLogAttribute instanceof Map)) {
                //no additional attributes existing, create them
                additionalLogValues = new TreeMap<>();
                request.setAttribute(LogRequestsInterceptor.REQUEST_ATTRIBUTE_ADDITIONAL_LOG_DETAILS,
                        additionalLogValues);
            } else {
                //they exist and need to be casted
                @SuppressWarnings("unchecked")
                Map<String, String> castedExistingLogAttributes = (Map<String, String>) existingLogAttribute;
                additionalLogValues = castedExistingLogAttributes;
            }
            //if no value exists it gets set, if a value exists, the new ones gets appended
            additionalLogValues.compute(key,
                    (k, existingValue) -> (existingValue == null) ? stringValue : existingValue + ", " + stringValue);
        }
    }

    /**
     * Adds an additional log value with the key "details" that is added to the json request log message.
     * <p>
     * If there are already log details the new one is appended.
     */
    protected void addAdditionalLogDetails(String message, Object... arguments) {
        String formattedMessage = new ParameterizedMessage(message, arguments).getFormattedMessage();
        addAdditionalLogValue("details", formattedMessage);
    }

    /**
     * Validates the current AppVariant that is authenticated with a matching api key against the given App
     * <p/>
     * Requires the header {@value #HEADER_NAME_API_KEY} to be set.
     *
     * @param app the App against which the AppVariant is validated against
     *
     * @throws NotAuthenticatedException if no api key was provided in the request
     * @throws NotAuthorizedException    if no matching app variant was found or the app variant does not belong to the
     *                                   given App
     */
    protected void validateAuthenticatedAppVariantAgainstApp(App app)
            throws NotAuthenticatedException, NotAuthorizedException {
        AppVariant appVariant = getAuthenticatedAppVariantNotNull();
        if(!Objects.equals(appVariant.getApp(), app)){
            throw new NotAuthorizedException("AppVariant for app {} is not authorized for this resource",
                    appVariant.getApp().getAppIdentifier());
        }
    }

    /**
     * Validates the current AppVariant that is authenticated with a matching api key against the given App
     * <p/>
     * Requires the header {@value #HEADER_NAME_API_KEY} to be set.
     *
     * @param app    the App against which the AppVariant is validated against
     * @param tenant the Tenant against which the AppVariant is validated against
     *
     * @throws NotAuthenticatedException if no api key was provided in the request
     * @throws NotAuthorizedException    if no matching app variant was found or the app variant does not belong to the
     *                                   given App
     */
    protected void validateAuthenticatedAppVariantAgainstAppAndTenant(App app, Tenant tenant)
            throws NotAuthenticatedException, NotAuthorizedException {
        AppVariant appVariant = getAuthenticatedAppVariantNotNull();

        if (!Objects.equals(appVariant.getApp(), app)) {
            throw new NotAuthorizedException("AppVariant for app {} is not authorized for this resource",
                    appVariant.getApp().getAppIdentifier());
        }

        if (!appService.getAvailableTenants(appVariant).contains(tenant)) {
            log.warn("{} is not in the available tenants in {}", tenant, appVariant);
            //If we need this as a hard requirement uncomment this:
            /*throw new NotAuthorizedException("AppVariant for app {} is not authorized for resources in tenant {}",
                    appVariant.getApp().getAppIdentifier(), community.getName());*/
        }
    }

}
