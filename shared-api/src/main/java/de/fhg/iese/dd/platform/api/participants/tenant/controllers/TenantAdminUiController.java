/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2020 - 2022 Balthasar Weitzel, Benjamin Hassenfratz
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.participants.tenant.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhg.iese.dd.platform.api.framework.ApiAuthentication;
import de.fhg.iese.dd.platform.api.framework.ApiAuthenticationType;
import de.fhg.iese.dd.platform.api.framework.ApiExceptions;
import de.fhg.iese.dd.platform.api.framework.exceptions.SearchParameterTooShortException;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientTenant;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.ClientTenantExtended;
import de.fhg.iese.dd.platform.api.participants.tenant.clientmodel.TenantClientModelMapper;
import de.fhg.iese.dd.platform.api.shared.misc.controllers.BaseAdminUiController;
import de.fhg.iese.dd.platform.business.participants.tenant.security.ListTenantsAction;
import de.fhg.iese.dd.platform.business.participants.tenant.security.ListTenantsExtendedAction;
import de.fhg.iese.dd.platform.business.participants.tenant.security.PermissionTenantRestricted;
import de.fhg.iese.dd.platform.business.participants.tenant.services.ITenantService;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.NotAuthorizedException;
import de.fhg.iese.dd.platform.datamanagement.participants.tenant.model.Tenant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/adminui/tenant")
@Api(tags = {"participants.adminui.tenant"})
public class TenantAdminUiController extends BaseAdminUiController {

    private static final int MIN_SEARCH_LENGTH = 2;
    private static final Sort DEFAULT_TENANT_SORT = Sort.by("name");

    @Autowired
    private ITenantService tenantService;
    @Autowired
    private TenantClientModelMapper tenantClientModelMapper;

    @ApiOperation(value = "Returns all tenants",
            notes = "The tenants are sorted by name, ascending")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListTenantsAction.class},
            notes = "If callers are restricted to some tenants only these tenants are returned.")
    @GetMapping
    public Page<ClientTenant> getAllTenants(
            @ApiParam("number of page to retrieve. First page has number 0")
            @RequestParam(required = false, defaultValue = "0")
                    int page,

            @ApiParam("number of elements per page")
            @RequestParam(required = false, defaultValue = "10")
                    int count,

            @ApiParam("If empty, all tenants are returned. If search={string} all tenants are returned that have " +
                    "the search string in id, name or tag.")
            @RequestParam(required = false)
                    String search
    ) {
        checkPageAndCountValues(page, count);
        final String searchString = checkAndParseSearchParameter(search);

        PermissionTenantRestricted permissionTenantRestricted =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListTenantsAction.class,
                        getCurrentPersonNotNull());

        Page<Tenant> tenants;

        if (permissionTenantRestricted.isAllTenantsAllowed()) {
            if (searchString == null) {
                tenants = tenantService.findAll(PageRequest.of(page, count, DEFAULT_TENANT_SORT));
            } else {
                // general infix search
                tenants = tenantService.findAllByInfixSearch(searchString,
                        PageRequest.of(page, count, DEFAULT_TENANT_SORT));
            }
        } else {
            if (searchString == null) {
                tenants = tenantService.findAllById(permissionTenantRestricted.getAllowedTenantIds(),
                        PageRequest.of(page, count, DEFAULT_TENANT_SORT));
            } else {
                // general infix search
                tenants = tenantService.findAllByIdInfixSearch(searchString,
                        permissionTenantRestricted.getAllowedTenantIds(),
                        PageRequest.of(page, count, DEFAULT_TENANT_SORT));
            }
        }
        return tenants.map(tenantClientModelMapper::toClientTenant);
    }

    @ApiOperation(value = "Returns all tenants extended",
            notes = "The tenants are sorted by name, ascending")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListTenantsExtendedAction.class})
    @GetMapping("/extended")
    public Page<ClientTenantExtended> getAllTenantsExtended(
            @ApiParam("number of page to retrieve. First page has number 0")
            @RequestParam(required = false, defaultValue = "0")
                    int page,

            @ApiParam("number of elements per page")
            @RequestParam(required = false, defaultValue = "10")
                    int count,

            @ApiParam("If empty, all tenants are returned. If search={string} all tenants are returned that have " +
                    "the search string in id, name or tag.")
            @RequestParam(required = false)
                    String search
    ) {
        checkPageAndCountValues(page, count);
        final String searchString = checkAndParseSearchParameter(search);

        getRoleService().decidePermissionAndThrowNotAuthorized(ListTenantsExtendedAction.class,
                getCurrentPersonNotNull());

        if (searchString == null) {
            return tenantService.findAll(PageRequest.of(page, count, DEFAULT_TENANT_SORT))
                    .map(tenantClientModelMapper::toClientTenantExtended);
        }
        // general infix search
        return tenantService.findAllByInfixSearch(searchString,
                        PageRequest.of(page, count, DEFAULT_TENANT_SORT))
                .map(tenantClientModelMapper::toClientTenantExtended);
    }

    @ApiOperation(value = "Returns a tenant by id",
            notes = "Returns the tenant identified by the given id")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListTenantsAction.class},
            notes = "If callers are restricted to some tenants only these tenants are returned.")
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @GetMapping("/{tenantId}")
    public ClientTenant getTenantById(
            @PathVariable String tenantId) {

        PermissionTenantRestricted permissionTenantRestricted =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListTenantsAction.class,
                        getCurrentPersonNotNull());

        if (permissionTenantRestricted.isTenantDenied(tenantId)) {
            throw new NotAuthorizedException("Insufficient privileges to get tenant {}", tenantId);
        }

        return tenantClientModelMapper.toClientTenant(tenantService.findTenantById(tenantId));
    }

    @ApiOperation(value = "Returns a tenant extended by id",
            notes = "Returns the tenant identified by the given id")
    @ApiAuthentication(value = ApiAuthenticationType.OAUTH2,
            requiredActions = {ListTenantsExtendedAction.class})
    @ApiExceptions({
            ClientExceptionType.TENANT_NOT_FOUND
    })
    @GetMapping("/{tenantId}/extended")
    public ClientTenantExtended getTenantExtendedById(
            @PathVariable String tenantId) {

        PermissionTenantRestricted permissionTenantRestricted =
                getRoleService().decidePermissionAndThrowNotAuthorized(ListTenantsExtendedAction.class,
                        getCurrentPersonNotNull());

        if (permissionTenantRestricted.isTenantDenied(tenantId)) {
            throw new NotAuthorizedException("Insufficient privileges to get tenant {}", tenantId);
        }

        return tenantClientModelMapper.toClientTenantExtended(tenantService.findTenantById(tenantId));
    }

    private String checkAndParseSearchParameter(String search) {
        if (StringUtils.isAllBlank(search)) {
            return null;
        }
        final String trimmedSearchString = search.trim();
        if (trimmedSearchString.length() < MIN_SEARCH_LENGTH) {
            throw new SearchParameterTooShortException("search", MIN_SEARCH_LENGTH);
        }
        return trimmedSearchString;
    }

}
