/*
 * Digitale Dörfer Plattform
 * Copyright (C) Fraunhofer IESE 2016 Matthias Koch
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhg.iese.dd.platform.api.shared.userdata.exceptions;

import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.BadRequestException;
import de.fhg.iese.dd.platform.datamanagement.framework.exceptions.ClientExceptionType;

public class KeyNameInvalidException extends BadRequestException {

    private static final long serialVersionUID = 5936271879740931720L;

    public KeyNameInvalidException(String name) {
        super(name);
    }

    @Override
    public ClientExceptionType getClientExceptionType() {
        return ClientExceptionType.KEY_NAME_INVALID;
    }

}
